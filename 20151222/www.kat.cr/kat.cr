<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-0883175.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-0883175.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-0883175.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-0883175.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-0883175.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '0883175',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-0883175.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <div  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/bajirao%20mastani/" class="tag5">bajirao mastani</a>
	<a href="/search/christmas/" class="tag5">christmas</a>
	<a href="/search/creed/" class="tag2">creed</a>
	<a href="/search/dilwale/" class="tag2">dilwale</a>
	<a href="/search/dilwale%202015/" class="tag3">dilwale 2015</a>
	<a href="/search/discography/" class="tag2">discography</a>
	<a href="/search/dragon%20ball%20super/" class="tag2">dragon ball super</a>
	<a href="/search/dual%20audio%20hindi/" class="tag3">dual audio hindi</a>
	<a href="/search/dvdscr/" class="tag6">dvdscr</a>
	<a href="/search/etrg/" class="tag2">etrg</a>
	<a href="/search/force%20awakens/" class="tag1">force awakens</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/homeland/" class="tag3">homeland</a>
	<a href="/search/homeland%20s05e12/" class="tag3">homeland s05e12</a>
	<a href="/search/in%20the%20heart%20of%20the%20sea/" class="tag2">in the heart of the sea</a>
	<a href="/search/into%20the%20badlands/" class="tag2">into the badlands</a>
	<a href="/search/is%20safe%201/" class="tag2">is safe 1</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/legend/" class="tag2">legend</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/one%20punch%20man/" class="tag3">one punch man</a>
	<a href="/search/quantico/" class="tag2">quantico</a>
	<a href="/search/sicario/" class="tag2">sicario</a>
	<a href="/search/spectre/" class="tag3">spectre</a>
	<a href="/search/star%20wars/" class="tag9">star wars</a>
	<a href="/search/star%20wars%20the%20force%20awakens/" class="tag5">star wars the force awakens</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20affair/" class="tag2">the affair</a>
	<a href="/search/the%20flash/" class="tag1">the flash</a>
	<a href="/search/the%20force%20awakens/" class="tag2">the force awakens</a>
	<a href="/search/the%20hateful%20eight/" class="tag2">the hateful eight</a>
	<a href="/search/the%20martian/" class="tag3">the martian</a>
	<a href="/search/the%20revenant/" class="tag2">the revenant</a>
	<a href="/search/the%20walking%20dead/" class="tag3">the walking dead</a>
	<a href="/search/yify/" class="tag9">yify</a>
	<a href="/search/yify%201080p/" class="tag3">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11784744,0" class="icommentjs kaButton smallButton rightButton" href="/creed-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11784744.html#comment">192 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/creed-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11784744.html" class="cellMainLink">Creed 2015 DVDScr XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1874705006">1.75 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T07:39:16+00:00">20 Dec 2015, 07:39:16</span></td>
			<td class="green center">18994</td>
			<td class="red lasttd center">16249</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782570,0" class="icommentjs kaButton smallButton rightButton" href="/the-revenant-2015-dvdscr-x264-dd5-1-nogrp-t11782570.html#comment">364 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-revenant-2015-dvdscr-x264-dd5-1-nogrp-t11782570.html" class="cellMainLink">The Revenant (2015) DVDSCR x264 DD5.1-NoGrp</a></div>
			</td>
			<td class="nobr center" data-sort="2442506560">2.27 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T19:24:06+00:00">19 Dec 2015, 19:24:06</span></td>
			<td class="green center">20164</td>
			<td class="red lasttd center">11757</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11789868,0" class="icommentjs kaButton smallButton rightButton" href="/in-the-heart-of-the-sea-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11789868.html#comment">139 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/in-the-heart-of-the-sea-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11789868.html" class="cellMainLink">In The Heart of the Sea 2015 DVDScr XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1913672722">1.78 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T04:30:58+00:00">21 Dec 2015, 04:30:58</span></td>
			<td class="green center">14740</td>
			<td class="red lasttd center">18013</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11793027,0" class="icommentjs kaButton smallButton rightButton" href="/the-good-dinosaur-2015-hdrip-xvid-ac3-evo-t11793027.html#comment">56 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-good-dinosaur-2015-hdrip-xvid-ac3-evo-t11793027.html" class="cellMainLink">The Good Dinosaur 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1496952229">1.39 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T19:44:11+00:00">21 Dec 2015, 19:44:11</span></td>
			<td class="green center">12331</td>
			<td class="red lasttd center">22111</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11788895,0" class="icommentjs kaButton smallButton rightButton" href="/star-wars-the-force-awakens-2015-hd-cam-xvid-hqmic-ac3-cpg-avi-t11788895.html#comment">210 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/star-wars-the-force-awakens-2015-hd-cam-xvid-hqmic-ac3-cpg-avi-t11788895.html" class="cellMainLink">Star Wars The Force Awakens 2015 HD-CAM XViD HQMic AC3-CPG.avi</a></div>
			</td>
			<td class="nobr center" data-sort="2001411406">1.86 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T23:48:39+00:00">20 Dec 2015, 23:48:39</span></td>
			<td class="green center">15865</td>
			<td class="red lasttd center">7854</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11793874,0" class="icommentjs kaButton smallButton rightButton" href="/bridge-of-spies-2015-hdrip-xvid-ac3-evo-avi-t11793874.html#comment">43 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bridge-of-spies-2015-hdrip-xvid-ac3-evo-avi-t11793874.html" class="cellMainLink">Bridge of Spies 2015 HDRip XviD AC3-EVO avi</a></div>
			</td>
			<td class="nobr center" data-sort="1629626354">1.52 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T01:35:04+00:00">22 Dec 2015, 01:35:04</span></td>
			<td class="green center">10918</td>
			<td class="red lasttd center">15732</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790940,0" class="icommentjs kaButton smallButton rightButton" href="/joy-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11790940.html#comment">90 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/joy-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11790940.html" class="cellMainLink">Joy 2015 DVDScr XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1593559633">1.48 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T09:59:09+00:00">21 Dec 2015, 09:59:09</span></td>
			<td class="green center">9475</td>
			<td class="red lasttd center">10823</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11793827,0" class="icommentjs kaButton smallButton rightButton" href="/the-martian-2015-1080p-web-dl-dd5-1-h264-rarbg-t11793827.html#comment">108 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-martian-2015-1080p-web-dl-dd5-1-h264-rarbg-t11793827.html" class="cellMainLink">The Martian 2015 1080p WEB-DL DD5 1 H264-RARBG</a></div>
			</td>
			<td class="nobr center" data-sort="5044179181">4.7 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T01:13:03+00:00">22 Dec 2015, 01:13:03</span></td>
			<td class="green center">5178</td>
			<td class="red lasttd center">17655</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790943,0" class="icommentjs kaButton smallButton rightButton" href="/steve-jobs-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11790943.html#comment">66 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/steve-jobs-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11790943.html" class="cellMainLink">Steve Jobs 2015 DVDScr XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1776747133">1.65 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T09:59:53+00:00">21 Dec 2015, 09:59:53</span></td>
			<td class="green center">7856</td>
			<td class="red lasttd center">9722</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11793667,0" class="icommentjs kaButton smallButton rightButton" href="/concussion-2015-dvdscr-xvid-ac3-hq-hive-cm8-avi-t11793667.html#comment">44 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/concussion-2015-dvdscr-xvid-ac3-hq-hive-cm8-avi-t11793667.html" class="cellMainLink">Concussion 2015 DVDScr XVID AC3 HQ Hive-CM8 avi</a></div>
			</td>
			<td class="nobr center" data-sort="1812346880">1.69 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T23:55:14+00:00">21 Dec 2015, 23:55:14</span></td>
			<td class="green center">6298</td>
			<td class="red lasttd center">8459</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11792601,0" class="icommentjs kaButton smallButton rightButton" href="/rani-padmini-2015-malayalam-dvdrip-1cd-x264-700mb-esubs-t11792601.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/rani-padmini-2015-malayalam-dvdrip-1cd-x264-700mb-esubs-t11792601.html" class="cellMainLink">Rani Padmini [2015] Malayalam DVDRip 1CD x264 700MB ESubs</a></div>
			</td>
			<td class="nobr center" data-sort="731279645">697.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T18:03:13+00:00">21 Dec 2015, 18:03:13</span></td>
			<td class="green center">4822</td>
			<td class="red lasttd center">3821</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11785380,0" class="icommentjs kaButton smallButton rightButton" href="/he-never-died-2015-hdrip-xvid-ac3-evo-t11785380.html#comment">65 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/he-never-died-2015-hdrip-xvid-ac3-evo-t11785380.html" class="cellMainLink">He Never Died 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1430791012">1.33 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T10:33:25+00:00">20 Dec 2015, 10:33:25</span></td>
			<td class="green center">4356</td>
			<td class="red lasttd center">4348</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781130,0" class="icommentjs kaButton smallButton rightButton" href="/everest-2015-1080p-bluray-x264-dts-jyk-t11781130.html#comment">61 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/everest-2015-1080p-bluray-x264-dts-jyk-t11781130.html" class="cellMainLink">Everest 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3281056435">3.06 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T12:48:52+00:00">19 Dec 2015, 12:48:52</span></td>
			<td class="green center">3877</td>
			<td class="red lasttd center">2712</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11793600,0" class="icommentjs kaButton smallButton rightButton" href="/sicario-2015-brrip-xvid-toro-avi-t11793600.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/sicario-2015-brrip-xvid-toro-avi-t11793600.html" class="cellMainLink">Sicario 2015 BRRiP XviD-ToRo avi</a></div>
			</td>
			<td class="nobr center" data-sort="1477764114">1.38 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T23:32:02+00:00">21 Dec 2015, 23:32:02</span></td>
			<td class="green center">3008</td>
			<td class="red lasttd center">2015</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11795291,0" class="icommentjs kaButton smallButton rightButton" href="/the-intern-2015-1080p-web-dl-dd5-1-h264-rarbg-t11795291.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-intern-2015-1080p-web-dl-dd5-1-h264-rarbg-t11795291.html" class="cellMainLink">The Intern 2015 1080p WEB-DL DD5 1 H264-RARBG</a></div>
			</td>
			<td class="nobr center" data-sort="4974755780">4.63 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T08:00:29+00:00">22 Dec 2015, 08:00:29</span></td>
			<td class="green center">2311</td>
			<td class="red lasttd center">3397</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11789364,0" class="icommentjs kaButton smallButton rightButton" href="/homeland-s05e12-web-dl-x264-fum-ettv-t11789364.html#comment">61 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/homeland-s05e12-web-dl-x264-fum-ettv-t11789364.html" class="cellMainLink">Homeland S05E12 WEB-DL x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="302622511">288.6 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T01:56:25+00:00">21 Dec 2015, 01:56:25</span></td>
			<td class="green center">11579</td>
			<td class="red lasttd center">1327</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11794690,0" class="icommentjs kaButton smallButton rightButton" href="/wwe-raw-2015-12-21-hdtv-x264-ebi-sparrow-t11794690.html#comment">38 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-raw-2015-12-21-hdtv-x264-ebi-sparrow-t11794690.html" class="cellMainLink">WWE RAW 2015 12 21 HDTV x264-Ebi -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1538317412">1.43 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T05:21:02+00:00">22 Dec 2015, 05:21:02</span></td>
			<td class="green center">4981</td>
			<td class="red lasttd center">4772</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11779920,0" class="icommentjs kaButton smallButton rightButton" href="/ash-vs-evil-dead-s01e08-hdtv-x264-killers-ettv-t11779920.html#comment">115 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ash-vs-evil-dead-s01e08-hdtv-x264-killers-ettv-t11779920.html" class="cellMainLink">Ash vs Evil Dead S01E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="216760903">206.72 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T07:08:26+00:00">19 Dec 2015, 07:08:26</span></td>
			<td class="green center">4532</td>
			<td class="red lasttd center">310</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11789580,0" class="icommentjs kaButton smallButton rightButton" href="/agent-x-us-s01e08-hdtv-x264-lol-ettv-t11789580.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/agent-x-us-s01e08-hdtv-x264-lol-ettv-t11789580.html" class="cellMainLink">Agent X US S01E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="289425513">276.02 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T03:01:24+00:00">21 Dec 2015, 03:01:24</span></td>
			<td class="green center">3944</td>
			<td class="red lasttd center">658</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11784114,0" class="icommentjs kaButton smallButton rightButton" href="/ufc-on-fox-17-dos-anjos-vs-cerrone-main-card-webrip-x264-miztery-incl-ads-sparrow-t11784114.html#comment">53 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-on-fox-17-dos-anjos-vs-cerrone-main-card-webrip-x264-miztery-incl-ads-sparrow-t11784114.html" class="cellMainLink">UFC On FOX 17 Dos Anjos vs Cerrone Main Card WebRip x264-Miztery (Incl. Ads) -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1903874665">1.77 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T04:04:23+00:00">20 Dec 2015, 04:04:23</span></td>
			<td class="green center">3118</td>
			<td class="red lasttd center">432</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790523,0" class="icommentjs kaButton smallButton rightButton" href="/keeping-up-with-the-kardashians-s11e06-hdtv-x264-crimson-rartv-t11790523.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/keeping-up-with-the-kardashians-s11e06-hdtv-x264-crimson-rartv-t11790523.html" class="cellMainLink">Keeping up with the Kardashians S11E06 HDTV x264-CRiMSON[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="383326044">365.57 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T07:59:04+00:00">21 Dec 2015, 07:59:04</span></td>
			<td class="green center">2811</td>
			<td class="red lasttd center">393</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11794383,0" class="icommentjs kaButton smallButton rightButton" href="/legends-2014-s02e07-hdtv-x264-killers-ettv-t11794383.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/legends-2014-s02e07-hdtv-x264-killers-ettv-t11794383.html" class="cellMainLink">Legends 2014 S02E07 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="226829308">216.32 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T04:05:21+00:00">22 Dec 2015, 04:05:21</span></td>
			<td class="green center">2227</td>
			<td class="red lasttd center">689</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11789831,0" class="icommentjs kaButton smallButton rightButton" href="/the-affair-s02e12-hdtv-x264-killers-ettv-t11789831.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-affair-s02e12-hdtv-x264-killers-ettv-t11789831.html" class="cellMainLink">The Affair S02E12 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="382395382">364.68 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T04:15:24+00:00">21 Dec 2015, 04:15:24</span></td>
			<td class="green center">2360</td>
			<td class="red lasttd center">297</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11789476,0" class="icommentjs kaButton smallButton rightButton" href="/the-librarians-us-s02e09-hdtv-x264-killers-ettv-t11789476.html#comment">27 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-librarians-us-s02e09-hdtv-x264-killers-ettv-t11789476.html" class="cellMainLink">The Librarians US S02E09 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="398286056">379.84 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T02:21:27+00:00">21 Dec 2015, 02:21:27</span></td>
			<td class="green center">2231</td>
			<td class="red lasttd center">340</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11779306,0" class="icommentjs kaButton smallButton rightButton" href="/z-nation-s02e15-hdtv-x264-killers-ettv-t11779306.html#comment">65 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/z-nation-s02e15-hdtv-x264-killers-ettv-t11779306.html" class="cellMainLink">Z Nation S02E15 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="408122550">389.22 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T04:04:20+00:00">19 Dec 2015, 04:04:20</span></td>
			<td class="green center">2259</td>
			<td class="red lasttd center">218</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11789767,0" class="icommentjs kaButton smallButton rightButton" href="/csi-cyber-s02e10-hdtv-x264-lol-ettv-t11789767.html#comment">30 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/csi-cyber-s02e10-hdtv-x264-lol-ettv-t11789767.html" class="cellMainLink">CSI Cyber S02E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="339480118">323.75 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T03:59:24+00:00">21 Dec 2015, 03:59:24</span></td>
			<td class="green center">2144</td>
			<td class="red lasttd center">316</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11785930,0" class="icommentjs kaButton smallButton rightButton" href="/saturday-night-live-s41e09-tina-fey-and-amy-poehler-bruce-springsteen-720p-nbc-webrip-aac2-0-x264-monkee-t11785930.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/saturday-night-live-s41e09-tina-fey-and-amy-poehler-bruce-springsteen-720p-nbc-webrip-aac2-0-x264-monkee-t11785930.html" class="cellMainLink">Saturday Night Live S41E09 Tina Fey and Amy Poehler Bruce Springsteen 720p NBC WEBRip AAC2.0 x264-monkee</a></div>
			</td>
			<td class="nobr center" data-sort="893791366">852.39 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T13:15:41+00:00">20 Dec 2015, 13:15:41</span></td>
			<td class="green center">1755</td>
			<td class="red lasttd center">268</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11779435,0" class="icommentjs kaButton smallButton rightButton" href="/the-knick-s02e10-hdtv-x264-killers-ettv-t11779435.html#comment">38 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-knick-s02e10-hdtv-x264-killers-ettv-t11779435.html" class="cellMainLink">The Knick S02E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="308834143">294.53 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T05:01:27+00:00">19 Dec 2015, 05:01:27</span></td>
			<td class="green center">1791</td>
			<td class="red lasttd center">134</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790734,0" class="icommentjs kaButton smallButton rightButton" href="/the-royals-2015-s02e06-web-dl-x264-fum-ettv-t11790734.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-royals-2015-s02e06-web-dl-x264-fum-ettv-t11790734.html" class="cellMainLink">The Royals 2015 S02E06 WEB-DL x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="325509707">310.43 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T08:54:34+00:00">21 Dec 2015, 08:54:34</span></td>
			<td class="green center">1171</td>
			<td class="red lasttd center">211</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11794117,0" class="icommentjs kaButton smallButton rightButton" href="/major-crimes-s04e18-hdtv-x264-lol-ettv-t11794117.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/major-crimes-s04e18-hdtv-x264-lol-ettv-t11794117.html" class="cellMainLink">Major Crimes S04E18 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="354004439">337.6 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T03:02:24+00:00">22 Dec 2015, 03:02:24</span></td>
			<td class="green center">1094</td>
			<td class="red lasttd center">307</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11780552,0" class="icommentjs kaButton smallButton rightButton" href="/the-official-uk-top-40-singles-chart-18th-december-18-12-2015-glodls-t11780552.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-official-uk-top-40-singles-chart-18th-december-18-12-2015-glodls-t11780552.html" class="cellMainLink">The Official UK Top 40 Singles Chart - 18th-December [18-12-2015] - [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="352160884">335.85 <span>MB</span></td>
			<td class="center">42</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T10:20:26+00:00">19 Dec 2015, 10:20:26</span></td>
			<td class="green center">1342</td>
			<td class="red lasttd center">791</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781375,0" class="icommentjs kaButton smallButton rightButton" href="/va-100-hits-total-rock-5cd-2015-mp3-320-kbps-t11781375.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-100-hits-total-rock-5cd-2015-mp3-320-kbps-t11781375.html" class="cellMainLink">VA - 100 Hits Total Rock 5CD (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="955480513">911.22 <span>MB</span></td>
			<td class="center">108</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T13:55:39+00:00">19 Dec 2015, 13:55:39</span></td>
			<td class="green center">881</td>
			<td class="red lasttd center">426</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11778334,0" class="icommentjs kaButton smallButton rightButton" href="/mp3-new-releases-2015-week-50-amazeballs-glodls-t11778334.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-50-amazeballs-glodls-t11778334.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 50 - AMAZEBALLS [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4186087828">3.9 <span>GB</span></td>
			<td class="center">558</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-18T22:44:25+00:00">18 Dec 2015, 22:44:25</span></td>
			<td class="green center">452</td>
			<td class="red lasttd center">439</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11777621,0" class="icommentjs kaButton smallButton rightButton" href="/christmas-hits-volume-2-mp3-divxtotal-t11777621.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/christmas-hits-volume-2-mp3-divxtotal-t11777621.html" class="cellMainLink">Christmas Hits Volume 2 MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="112331594">107.13 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-18T18:23:22+00:00">18 Dec 2015, 18:23:22</span></td>
			<td class="green center">497</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781388,0" class="icommentjs kaButton smallButton rightButton" href="/va-99-party-hits-moments-2015-mp3-252-320-kbps-t11781388.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-99-party-hits-moments-2015-mp3-252-320-kbps-t11781388.html" class="cellMainLink">VA - 99 Party Hits Moments (2015) MP3 [252-320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1125133313">1.05 <span>GB</span></td>
			<td class="center">100</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T14:01:34+00:00">19 Dec 2015, 14:01:34</span></td>
			<td class="green center">406</td>
			<td class="red lasttd center">258</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781520,0" class="icommentjs kaButton smallButton rightButton" href="/pop-rock-hits-100-colours-t11781520.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/pop-rock-hits-100-colours-t11781520.html" class="cellMainLink">Pop Rock Hits! 100 Colours</a></div>
			</td>
			<td class="nobr center" data-sort="1071624623">1021.98 <span>MB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T14:24:08+00:00">19 Dec 2015, 14:24:08</span></td>
			<td class="green center">353</td>
			<td class="red lasttd center">93</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782417,0" class="icommentjs kaButton smallButton rightButton" href="/david-gilmour-greatest-hits-japanese-2-cd-edition-2015-320ak-t11782417.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/david-gilmour-greatest-hits-japanese-2-cd-edition-2015-320ak-t11782417.html" class="cellMainLink">David Gilmour - Greatest Hits (Japanese 2-CD Edition)2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="211571718">201.77 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T18:30:50+00:00">19 Dec 2015, 18:30:50</span></td>
			<td class="green center">363</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782396,0" class="icommentjs kaButton smallButton rightButton" href="/trance-va-armin-van-buuren-a-state-of-trance-year-mix-2015-mp3-320-kbps-tracks-image-cue-hb-edm-rg-t11782396.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/trance-va-armin-van-buuren-a-state-of-trance-year-mix-2015-mp3-320-kbps-tracks-image-cue-hb-edm-rg-t11782396.html" class="cellMainLink">(Trance) VA - Armin Van Buuren - A State of Trance Year Mix 2015 - MP3, 320 Kbps, Tracks+Image+cue. HB [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="1175247861">1.09 <span>GB</span></td>
			<td class="center">114</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T18:25:59+00:00">19 Dec 2015, 18:25:59</span></td>
			<td class="green center">255</td>
			<td class="red lasttd center">219</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11780717,0" class="icommentjs kaButton smallButton rightButton" href="/va-now-that-s-what-i-call-christmas-2015-3cd-320kbps-itake-glodls-t11780717.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-that-s-what-i-call-christmas-2015-3cd-320kbps-itake-glodls-t11780717.html" class="cellMainLink">VA - Now Thatâs What I Call Christmas 2015 [3CD] [320kbps] - iTake [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="559964643">534.02 <span>MB</span></td>
			<td class="center">76</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T11:03:48+00:00">19 Dec 2015, 11:03:48</span></td>
			<td class="green center">291</td>
			<td class="red lasttd center">136</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11778317,0" class="icommentjs kaButton smallButton rightButton" href="/john-williams-star-wars-the-force-awakens-ost-2015-flac-24-96-t11778317.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/john-williams-star-wars-the-force-awakens-ost-2015-flac-24-96-t11778317.html" class="cellMainLink">John Williams - Star Wars: The Force Awakens OST (2015) FLAC 24-96</a></div>
			</td>
			<td class="nobr center" data-sort="1608638672">1.5 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-18T22:34:10+00:00">18 Dec 2015, 22:34:10</span></td>
			<td class="green center">292</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/latin-hits-2016-winter-edition-mp3-divxtotal-t11783003.html" class="cellMainLink">Latin Hits 2016 [Winter Edition] MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="273646513">260.97 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T21:45:01+00:00">19 Dec 2015, 21:45:01</span></td>
			<td class="green center">275</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-top-club-40-december-2015-2015-mp3-t11791017.html" class="cellMainLink">VA - Top Club 40 - December 2015 (2015) MP3</a></div>
			</td>
			<td class="nobr center" data-sort="687795221">655.93 <span>MB</span></td>
			<td class="center">63</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T10:23:16+00:00">21 Dec 2015, 10:23:16</span></td>
			<td class="green center">229</td>
			<td class="red lasttd center">120</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11777562,0" class="icommentjs kaButton smallButton rightButton" href="/va-vocal-trance-top-40-2016-2015-mp3-t11777562.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-vocal-trance-top-40-2016-2015-mp3-t11777562.html" class="cellMainLink">VA - Vocal Trance Top 40 2016 (2015) Mp3</a></div>
			</td>
			<td class="nobr center" data-sort="333898934">318.43 <span>MB</span></td>
			<td class="center">40</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-18T18:03:46+00:00">18 Dec 2015, 18:03:46</span></td>
			<td class="green center">206</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11791979,0" class="icommentjs kaButton smallButton rightButton" href="/axel-rudi-pell-game-of-sins-deluxe-edition-2016-320ak-t11791979.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/axel-rudi-pell-game-of-sins-deluxe-edition-2016-320ak-t11791979.html" class="cellMainLink">Axel Rudi Pell - Game Of Sins (Deluxe Edition) 2016 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="155008072">147.83 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T14:37:35+00:00">21 Dec 2015, 14:37:35</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11794020,0" class="icommentjs kaButton smallButton rightButton" href="/from-motown-with-love-mp3-divxtotal-t11794020.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/from-motown-with-love-mp3-divxtotal-t11794020.html" class="cellMainLink">From Motown With Love MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="224580788">214.18 <span>MB</span></td>
			<td class="center">70</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T02:31:27+00:00">22 Dec 2015, 02:31:27</span></td>
			<td class="green center">170</td>
			<td class="red lasttd center">25</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781959,0" class="icommentjs kaButton smallButton rightButton" href="/the-long-dark-0-301-salat-production-t11781959.html#comment">30 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-long-dark-0-301-salat-production-t11781959.html" class="cellMainLink">The Long Dark 0.301 [Salat-Production]</a></div>
			</td>
			<td class="nobr center" data-sort="1076499772">1 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T16:01:37+00:00">19 Dec 2015, 16:01:37</span></td>
			<td class="green center">419</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11786332,0" class="icommentjs kaButton smallButton rightButton" href="/half-life-2-2004-pc-cpul-t11786332.html#comment">42 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/half-life-2-2004-pc-cpul-t11786332.html" class="cellMainLink">Half-Life 2 (2004) PC [CPUL]</a></div>
			</td>
			<td class="nobr center" data-sort="2878509530">2.68 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T15:37:41+00:00">20 Dec 2015, 15:37:41</span></td>
			<td class="green center">329</td>
			<td class="red lasttd center">164</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11785964,0" class="icommentjs kaButton smallButton rightButton" href="/prison-architect-multi22-postmortem-t11785964.html#comment">9 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/prison-architect-multi22-postmortem-t11785964.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/prison-architect-multi22-postmortem-t11785964.html" class="cellMainLink">Prison.Architect.MULTI22-POSTMORTEM</a></div>
			</td>
			<td class="nobr center" data-sort="857061273">817.36 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T13:35:19+00:00">20 Dec 2015, 13:35:19</span></td>
			<td class="green center">289</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11779862,0" class="icommentjs kaButton smallButton rightButton" href="/the-sims-4-version-1-13-104-1010-get-together-dlc-spooky-stuff-all-dlcs-repack-mr-dj-t11779862.html#comment">35 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/the-sims-4-version-1-13-104-1010-get-together-dlc-spooky-stuff-all-dlcs-repack-mr-dj-t11779862.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-sims-4-version-1-13-104-1010-get-together-dlc-spooky-stuff-all-dlcs-repack-mr-dj-t11779862.html" class="cellMainLink">The Sims 4 version 1.13.104.1010 + Get Together DLC + Spooky Stuff [All DLCs] repack Mr DJ</a></div>
			</td>
			<td class="nobr center" data-sort="14432880995">13.44 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T06:47:31+00:00">19 Dec 2015, 06:47:31</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">312</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782566,0" class="icommentjs kaButton smallButton rightButton" href="/beamng-drive-v0-5-0-t11782566.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/beamng-drive-v0-5-0-t11782566.html" class="cellMainLink">BeamNG drive v0.5.0</a></div>
			</td>
			<td class="nobr center" data-sort="1451408002">1.35 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T19:22:50+00:00">19 Dec 2015, 19:22:50</span></td>
			<td class="green center">246</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11791705,0" class="icommentjs kaButton smallButton rightButton" href="/dungeon-lords-steam-edition-plaza-t11791705.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/dungeon-lords-steam-edition-plaza-t11791705.html" class="cellMainLink">Dungeon Lords Steam Edition-PLAZA</a></div>
			</td>
			<td class="nobr center" data-sort="3932393229">3.66 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T13:29:56+00:00">21 Dec 2015, 13:29:56</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">115</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11777985,0" class="icommentjs kaButton smallButton rightButton" href="/underrail-gog-t11777985.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/underrail-gog-t11777985.html" class="cellMainLink">UnderRail-GOG</a></div>
			</td>
			<td class="nobr center" data-sort="2274431402">2.12 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-18T20:35:09+00:00">18 Dec 2015, 20:35:09</span></td>
			<td class="green center">111</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11780323,0" class="icommentjs kaButton smallButton rightButton" href="/ominous-objects-3-trail-of-time-ce-2015-pc-final-t11780323.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ominous-objects-3-trail-of-time-ce-2015-pc-final-t11780323.html" class="cellMainLink">Ominous Objects 3: Trail of Time CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="1086069670">1.01 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T08:59:47+00:00">19 Dec 2015, 08:59:47</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/rust-v1340-2014-pc-repack-Ð¾Ñ-r-g-alkad-t11793907.html" class="cellMainLink">Rust [v1340] (2014) PC | RePack Ð¾Ñ R.G. Alkad</a></div>
			</td>
			<td class="nobr center" data-sort="1672115105">1.56 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T01:47:51+00:00">22 Dec 2015, 01:47:51</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782944,0" class="icommentjs kaButton smallButton rightButton" href="/assetto-corsa-update-v1-4-incl-dream-pack-3-dlc-bat-t11782944.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assetto-corsa-update-v1-4-incl-dream-pack-3-dlc-bat-t11782944.html" class="cellMainLink">Assetto Corsa Update v1 4 Incl Dream Pack 3 DLC-BAT</a></div>
			</td>
			<td class="nobr center" data-sort="6226341350">5.8 <span>GB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T21:23:04+00:00">19 Dec 2015, 21:23:04</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11785829,0" class="icommentjs kaButton smallButton rightButton" href="/ÐÐ¾Ð´Ñ-Ð´Ð»Ñ-world-of-tanks-0-9-13-t11785829.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ÐÐ¾Ð´Ñ-Ð´Ð»Ñ-world-of-tanks-0-9-13-t11785829.html" class="cellMainLink">ÐÐ¾Ð´Ñ Ð´Ð»Ñ World of Tanks [0.9.13]</a></div>
			</td>
			<td class="nobr center" data-sort="390365528">372.28 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T12:40:13+00:00">20 Dec 2015, 12:40:13</span></td>
			<td class="green center">80</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11793884,0" class="icommentjs kaButton smallButton rightButton" href="/minecraft-pocket-edition-0-13-1-alpha-apk-t11793884.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/minecraft-pocket-edition-0-13-1-alpha-apk-t11793884.html" class="cellMainLink">Minecraft Pocket Edition 0.13.1 alpha.apk</a></div>
			</td>
			<td class="nobr center" data-sort="18825259">17.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T01:39:10+00:00">22 Dec 2015, 01:39:10</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11780243,0" class="icommentjs kaButton smallButton rightButton" href="/phantasmat-5-behind-the-mask-ce-2015-pc-final-t11780243.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/phantasmat-5-behind-the-mask-ce-2015-pc-final-t11780243.html" class="cellMainLink">Phantasmat 5: Behind The Mask CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="935041722">891.73 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T08:35:07+00:00">19 Dec 2015, 08:35:07</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11784807,0" class="icommentjs kaButton smallButton rightButton" href="/badland-full-v3-2-0-8-mod-apk-obb-xpoz-t11784807.html#comment">2 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/badland-full-v3-2-0-8-mod-apk-obb-xpoz-t11784807.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/badland-full-v3-2-0-8-mod-apk-obb-xpoz-t11784807.html" class="cellMainLink">BADLAND (Full) v3.2.0.8 Mod [Apk+Obb]-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="186720534">178.07 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T07:59:46+00:00">20 Dec 2015, 07:59:46</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11792064,0" class="icommentjs kaButton smallButton rightButton" href="/hitman-sniper-v1-5-54790-no-mod-mod-apk-obb-xpoz-t11792064.html#comment">4 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/hitman-sniper-v1-5-54790-no-mod-mod-apk-obb-xpoz-t11792064.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/hitman-sniper-v1-5-54790-no-mod-mod-apk-obb-xpoz-t11792064.html" class="cellMainLink">Hitman Sniper v1.5.54790 [No Mod+Mod] (Apk+Obb)-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="319451724">304.65 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T15:03:25+00:00">21 Dec 2015, 15:03:25</span></td>
			<td class="green center">56</td>
			<td class="red lasttd center">37</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781241,0" class="icommentjs kaButton smallButton rightButton" href="/internet-download-manager-idm-6-25-build-9-registered-32bit-64bit-patch-crackingpatching-t11781241.html#comment">65 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-25-build-9-registered-32bit-64bit-patch-crackingpatching-t11781241.html" class="cellMainLink">Internet Download Manager (IDM) 6.25 Build 9 Registered (32bit + 64bit Patch) [CrackingPatching]</a></div>
			</td>
			<td class="nobr center" data-sort="10646135">10.15 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T13:20:55+00:00">19 Dec 2015, 13:20:55</span></td>
			<td class="green center">715</td>
			<td class="red lasttd center">184</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790777,0" class="icommentjs kaButton smallButton rightButton" href="/wintousb-enterprise-2-6-release-1-key-4realtorrentz-t11790777.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/wintousb-enterprise-2-6-release-1-key-4realtorrentz-t11790777.html" class="cellMainLink">WinToUSB Enterprise 2.6 Release 1 + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="5246523">5 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T09:13:13+00:00">21 Dec 2015, 09:13:13</span></td>
			<td class="green center">289</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11784107,0" class="icommentjs kaButton smallButton rightButton" href="/windows-7-8-1-10-x86-x64-aio-70in1-by-adguard-v15-12-19-eng-rus-team-os-t11784107.html#comment">31 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-8-1-10-x86-x64-aio-70in1-by-adguard-v15-12-19-eng-rus-team-os-t11784107.html" class="cellMainLink">Windows 7-8.1-10 (x86-x64) AIO [70in1] by adguard (v15.12.19) [Eng-Rus]-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="11404306534">10.62 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T04:03:06+00:00">20 Dec 2015, 04:03:06</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">330</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11778782,0" class="icommentjs kaButton smallButton rightButton" href="/spyhunter-4-21-10-4585-portable-by-wood-t11778782.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/spyhunter-4-21-10-4585-portable-by-wood-t11778782.html" class="cellMainLink">SpyHunter 4.21.10.4585 Portable by wood</a></div>
			</td>
			<td class="nobr center" data-sort="70697698">67.42 <span>MB</span></td>
			<td class="center">48</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T01:36:26+00:00">19 Dec 2015, 01:36:26</span></td>
			<td class="green center">270</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11791867,0" class="icommentjs kaButton smallButton rightButton" href="/revo-uninstaller-pro-3-1-5-final-crack-techtools-t11791867.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/revo-uninstaller-pro-3-1-5-final-crack-techtools-t11791867.html" class="cellMainLink">Revo Uninstaller Pro 3.1.5 FINAL + Crack [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="12943679">12.34 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T14:08:21+00:00">21 Dec 2015, 14:08:21</span></td>
			<td class="green center">252</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11785781,0" class="icommentjs kaButton smallButton rightButton" href="/keys-for-eset-kaspersky-avast-dr-web-avira-december-20-2015-pc-t11785781.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/keys-for-eset-kaspersky-avast-dr-web-avira-december-20-2015-pc-t11785781.html" class="cellMainLink">Keys for ESET, Kaspersky, Avast, Dr.Web, Avira [December 20] (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="3356551">3.2 <span>MB</span></td>
			<td class="center">119</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T12:31:22+00:00">20 Dec 2015, 12:31:22</span></td>
			<td class="green center">157</td>
			<td class="red lasttd center">152</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790677,0" class="icommentjs kaButton smallButton rightButton" href="/aquasoft-slideshow-premium-7-8-02-eng-serial-at-team-t11790677.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/aquasoft-slideshow-premium-7-8-02-eng-serial-at-team-t11790677.html" class="cellMainLink">AquaSoft SlideShow Premium 7.8.02 [ENG] [Serial] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="58837810">56.11 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T08:34:09+00:00">21 Dec 2015, 08:34:09</span></td>
			<td class="green center">159</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11785388,0" class="icommentjs kaButton smallButton rightButton" href="/iobit-driver-booster-3-1-1-450-multilingual-keys-4realtorrentz-t11785388.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/iobit-driver-booster-3-1-1-450-multilingual-keys-4realtorrentz-t11785388.html" class="cellMainLink">IObit Driver Booster 3.1.1.450 Multilingual + Keys [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="13709404">13.07 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T10:35:29+00:00">20 Dec 2015, 10:35:29</span></td>
			<td class="green center">157</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11778813,0" class="icommentjs kaButton smallButton rightButton" href="/mozilla-firefox-43-0-1-final-t11778813.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mozilla-firefox-43-0-1-final-t11778813.html" class="cellMainLink">Mozilla Firefox 43.0.1 Final</a></div>
			</td>
			<td class="nobr center" data-sort="93582272">89.25 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T01:45:38+00:00">19 Dec 2015, 01:45:38</span></td>
			<td class="green center">127</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11792354,0" class="icommentjs kaButton smallButton rightButton" href="/ccleaner-v5-13-5460-crack-inc-pro-business-addition-by-ssec-t11792354.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ccleaner-v5-13-5460-crack-inc-pro-business-addition-by-ssec-t11792354.html" class="cellMainLink">CCleaner v5.13.5460 -Crack Inc.Pro &amp; Business Addition - By~[SSEC]</a></div>
			</td>
			<td class="nobr center" data-sort="7484647">7.14 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T16:52:24+00:00">21 Dec 2015, 16:52:24</span></td>
			<td class="green center">128</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11778842,0" class="icommentjs kaButton smallButton rightButton" href="/skype-7-17-0-105-final-exe-t11778842.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/skype-7-17-0-105-final-exe-t11778842.html" class="cellMainLink">Skype 7.17.0.105 Final.exe</a></div>
			</td>
			<td class="nobr center" data-sort="46901368">44.73 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T01:53:24+00:00">19 Dec 2015, 01:53:24</span></td>
			<td class="green center">123</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11791298,0" class="icommentjs kaButton smallButton rightButton" href="/pc-auto-shutdown-v6-3-key-keygen-4realtorrentz-t11791298.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pc-auto-shutdown-v6-3-key-keygen-4realtorrentz-t11791298.html" class="cellMainLink">PC Auto Shutdown v6.3 Key + Keygen [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="1170404">1.12 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T11:35:47+00:00">21 Dec 2015, 11:35:47</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11792538,0" class="icommentjs kaButton smallButton rightButton" href="/daz3d-poser-everyday-toiletries-t11792538.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-poser-everyday-toiletries-t11792538.html" class="cellMainLink">DAZ3D - Poser Everyday Toiletries</a></div>
			</td>
			<td class="nobr center" data-sort="9815406">9.36 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T17:48:10+00:00">21 Dec 2015, 17:48:10</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11783046,0" class="icommentjs kaButton smallButton rightButton" href="/poser-daz3d-i13-bachelor-living-ro-104650-t11783046.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/poser-daz3d-i13-bachelor-living-ro-104650-t11783046.html" class="cellMainLink">Poser ~ DAZ3D : i13 Bachelor Living [RO 104650]</a></div>
			</td>
			<td class="nobr center" data-sort="12542531">11.96 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T21:57:59+00:00">19 Dec 2015, 21:57:59</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11793733,0" class="icommentjs kaButton smallButton rightButton" href="/daz3d-sexy-platform-sandals-2-for-g2f-19654-t11793733.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-sexy-platform-sandals-2-for-g2f-19654-t11793733.html" class="cellMainLink">daz3d - Sexy Platform Sandals 2 For G2F 19654</a></div>
			</td>
			<td class="nobr center" data-sort="29926762">28.54 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T00:15:32+00:00">22 Dec 2015, 00:15:32</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">7</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11788069,0" class="icommentjs kaButton smallButton rightButton" href="/leopard-raws-one-punch-man-12-end-tx-1280x720-x264-aac-mp4-t11788069.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-one-punch-man-12-end-tx-1280x720-x264-aac-mp4-t11788069.html" class="cellMainLink">[Leopard-Raws] One-Punch Man - 12 END (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="565814504">539.6 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T19:55:02+00:00">20 Dec 2015, 19:55:02</span></td>
			<td class="green center">3000</td>
			<td class="red lasttd center">398</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11788156,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-one-punch-man-12-720p-mkv-t11788156.html#comment">42 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-punch-man-12-720p-mkv-t11788156.html" class="cellMainLink">[AnimeRG] One Punch Man - 12 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="334664060">319.16 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T20:19:26+00:00">20 Dec 2015, 20:19:26</span></td>
			<td class="green center">1986</td>
			<td class="red lasttd center">380</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11786113,0" class="icommentjs kaButton smallButton rightButton" href="/kaerizaki-fansub-one-piece-723-french-subbed-hd-1280x720-mp4-t11786113.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kaerizaki-fansub-one-piece-723-french-subbed-hd-1280x720-mp4-t11786113.html" class="cellMainLink">[Kaerizaki-Fansub] One Piece 723 [FRENCH SUBBED][HD_1280x720].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="294026027">280.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T14:36:40+00:00">20 Dec 2015, 14:36:40</span></td>
			<td class="green center">1056</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790256,0" class="icommentjs kaButton smallButton rightButton" href="/dragon-ball-super-024-english-subbed-720p-arrg-lucifer22-t11790256.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-024-english-subbed-720p-arrg-lucifer22-t11790256.html" class="cellMainLink">Dragon Ball Super - 024 English Subbed [720P][ARRG][Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="168089024">160.3 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T06:30:22+00:00">21 Dec 2015, 06:30:22</span></td>
			<td class="green center">1013</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11786795,0" class="icommentjs kaButton smallButton rightButton" href="/one-punch-man-12-english-subbed-720p-arrg-lucifer22-t11786795.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-punch-man-12-english-subbed-720p-arrg-lucifer22-t11786795.html" class="cellMainLink">One Punch Man - 12 English Subbed [720P][ARRG][Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="177559551">169.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T17:25:04+00:00">20 Dec 2015, 17:25:04</span></td>
			<td class="green center">839</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782716,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-haikyuu-s2-12-720p-mkv-t11782716.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-haikyuu-s2-12-720p-mkv-t11782716.html" class="cellMainLink">[HorribleSubs] Haikyuu!! S2 - 12 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="342791398">326.91 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T20:10:02+00:00">19 Dec 2015, 20:10:02</span></td>
			<td class="green center">608</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790608,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-dragon-ball-super-24-english-subbed-720p-soft-subs-sehjada-t11790608.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-dragon-ball-super-24-english-subbed-720p-soft-subs-sehjada-t11790608.html" class="cellMainLink">[ARRG]Dragon Ball Super - 24 English Subbed [720P] (Soft Subs) (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="422070315">402.52 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T08:13:37+00:00">21 Dec 2015, 08:13:37</span></td>
			<td class="green center">242</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-owari-no-seraph-23-87d93684-mkv-t11789556.html" class="cellMainLink">[Vivid] Owari no Seraph - 23 [87D93684].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="474252790">452.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T02:50:02+00:00">21 Dec 2015, 02:50:02</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11786892,0" class="icommentjs kaButton smallButton rightButton" href="/bakedfish-one-punch-man-12-720p-aac-mp4-t11786892.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-one-punch-man-12-720p-aac-mp4-t11786892.html" class="cellMainLink">[BakedFish] One Punch Man - 12 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="563527289">537.42 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T17:41:40+00:00">20 Dec 2015, 17:41:40</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11786887,0" class="icommentjs kaButton smallButton rightButton" href="/blaze077-one-punch-man-2015-720p-complete-t11786887.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blaze077-one-punch-man-2015-720p-complete-t11786887.html" class="cellMainLink">[Blaze077] One Punch Man (2015) [720p][Complete]</a></div>
			</td>
			<td class="nobr center" data-sort="3003094616">2.8 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T17:40:53+00:00">20 Dec 2015, 17:40:53</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">170</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11783990,0" class="icommentjs kaButton smallButton rightButton" href="/one-piece-723-english-subbed-480p-arrg-lucifer22-t11783990.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-723-english-subbed-480p-arrg-lucifer22-t11783990.html" class="cellMainLink">One Piece - 723 English Subbed 480p [ARRG][Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="83306098">79.45 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T03:20:05+00:00">20 Dec 2015, 03:20:05</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11786711,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-one-punch-man-12-english-subbed-360p-sehjada-t11786711.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-one-punch-man-12-english-subbed-360p-sehjada-t11786711.html" class="cellMainLink">[ARRG] One Punch Man - 12 English Subbed 360P (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="81574263">77.8 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T17:09:20+00:00">20 Dec 2015, 17:09:20</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-teekyuu-72-6b56e3f8-mkv-t11793107.html" class="cellMainLink">[Commie] Teekyuu - 72 [6B56E3F8].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="41355035">39.44 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T20:05:02+00:00">21 Dec 2015, 20:05:02</span></td>
			<td class="green center">86</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11792777,0" class="icommentjs kaButton smallButton rightButton" href="/iorchid-one-piece-tv-special-adventure-of-nebulandia-720p-engsub-mkv-t11792777.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/iorchid-one-piece-tv-special-adventure-of-nebulandia-720p-engsub-mkv-t11792777.html" class="cellMainLink">[iORcHiD] One Piece (TV Special) - Adventure of Nebulandia [720p][EngSub].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="733063620">699.1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T18:41:50+00:00">21 Dec 2015, 18:41:50</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">84</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11789653,0" class="icommentjs kaButton smallButton rightButton" href="/ae-one-punch-man-12-720p-mp4-t11789653.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ae-one-punch-man-12-720p-mp4-t11789653.html" class="cellMainLink">[AE] One-Punch Man - 12 [720p].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="250135113">238.55 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T03:23:16+00:00">21 Dec 2015, 03:23:16</span></td>
			<td class="green center">80</td>
			<td class="red lasttd center">5</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782458,0" class="icommentjs kaButton smallButton rightButton" href="/speed-reading-the-comprehensive-guide-to-speed-reading-increase-your-reading-speed-by-300-in-less-than-24-hours-epub-mobi-ertb-t11782458.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pictureType">
                    <a href="/speed-reading-the-comprehensive-guide-to-speed-reading-increase-your-reading-speed-by-300-in-less-than-24-hours-epub-mobi-ertb-t11782458.html" class="cellMainLink">Speed Reading - The Comprehensive Guide To Speed Reading - Increase Your Reading Speed By 300% In Less Than 24 Hours [ePUB + MOBI] {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="1349367">1.29 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T18:44:28+00:00">19 Dec 2015, 18:44:28</span></td>
			<td class="green center">587</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11783744,0" class="icommentjs kaButton smallButton rightButton" href="/the-new-york-times-best-sellers-december-27-2015-combined-fiction-non-fiction-t11783744.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-new-york-times-best-sellers-december-27-2015-combined-fiction-non-fiction-t11783744.html" class="cellMainLink">The New York Times Best Sellers â December 27, 2015 (Combined Fiction / Non-Fiction)</a></div>
			</td>
			<td class="nobr center" data-sort="318556826">303.8 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T01:47:31+00:00">20 Dec 2015, 01:47:31</span></td>
			<td class="green center">512</td>
			<td class="red lasttd center">135</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782318,0" class="icommentjs kaButton smallButton rightButton" href="/microsoft-excel-essential-hints-and-tips-fundamental-hints-and-tips-to-kick-start-your-excel-skills-pdf-epub-mobi-t11782318.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-excel-essential-hints-and-tips-fundamental-hints-and-tips-to-kick-start-your-excel-skills-pdf-epub-mobi-t11782318.html" class="cellMainLink">Microsoft Excel Essential Hints and Tips: Fundamental hints and tips to kick start your Excel skills [pdf,epub,mobi]</a></div>
			</td>
			<td class="nobr center" data-sort="16224758">15.47 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T17:54:17+00:00">19 Dec 2015, 17:54:17</span></td>
			<td class="green center">447</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11778126,0" class="icommentjs kaButton smallButton rightButton" href="/the-asian-grandmothers-cookbook-home-cooking-from-asian-american-kitchens-2009-pdf-epub-mobi-gooner-t11778126.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-asian-grandmothers-cookbook-home-cooking-from-asian-american-kitchens-2009-pdf-epub-mobi-gooner-t11778126.html" class="cellMainLink">The Asian Grandmothers Cookbook - Home Cooking from Asian American Kitchens (2009) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="28082964">26.78 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-18T21:22:49+00:00">18 Dec 2015, 21:22:49</span></td>
			<td class="green center">314</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11783130,0" class="icommentjs kaButton smallButton rightButton" href="/the-cooking-of-brazil-china-greece-italy-and-thailand-2nd-editions-2011-pdf-gooner-t11783130.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-cooking-of-brazil-china-greece-italy-and-thailand-2nd-editions-2011-pdf-gooner-t11783130.html" class="cellMainLink">The Cooking of Brazil, China, Greece, Italy and Thailand (2nd Editions) (2011) (Pdf) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="79704978">76.01 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T22:45:11+00:00">19 Dec 2015, 22:45:11</span></td>
			<td class="green center">326</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782385,0" class="icommentjs kaButton smallButton rightButton" href="/black-decker-codes-for-homeowners-updated-3rd-edition-electrical-mechanical-plumbing-building-current-with-2015-2017-codes-pdf-epub-t11782385.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/black-decker-codes-for-homeowners-updated-3rd-edition-electrical-mechanical-plumbing-building-current-with-2015-2017-codes-pdf-epub-t11782385.html" class="cellMainLink">Black &amp; Decker Codes for Homeowners, Updated 3rd Edition: Electrical - Mechanical - Plumbing - Building - Current with 2015-2017 Codes [pdf,epub]</a></div>
			</td>
			<td class="nobr center" data-sort="114899069">109.58 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T18:21:15+00:00">19 Dec 2015, 18:21:15</span></td>
			<td class="green center">309</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11778562,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-group-1-december-19-2015-true-pdf-deluxas-t11778562.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-group-1-december-19-2015-true-pdf-deluxas-t11778562.html" class="cellMainLink">Assorted Magazines Bundle [group 1] - December 19 2015 (True PDF) - DeLUXAS</a></div>
			</td>
			<td class="nobr center" data-sort="410870243">391.84 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T00:11:10+00:00">19 Dec 2015, 00:11:10</span></td>
			<td class="green center">241</td>
			<td class="red lasttd center">124</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11778061,0" class="icommentjs kaButton smallButton rightButton" href="/the-complete-chile-pepper-book-a-gardener-s-guide-to-choosing-growing-preserving-and-cooking-2009-pdf-epub-mobi-gooner-t11778061.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-complete-chile-pepper-book-a-gardener-s-guide-to-choosing-growing-preserving-and-cooking-2009-pdf-epub-mobi-gooner-t11778061.html" class="cellMainLink">The Complete Chile Pepper Book - A Gardener&#039;s Guide to Choosing, Growing, Preserving and Cooking (2009) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="47412963">45.22 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-18T20:57:20+00:00">18 Dec 2015, 20:57:20</span></td>
			<td class="green center">254</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11788030,0" class="icommentjs kaButton smallButton rightButton" href="/borg-sonia-8-oral-sex-games-for-ultimate-fun-pleasure-epub-zeke23-t11788030.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/borg-sonia-8-oral-sex-games-for-ultimate-fun-pleasure-epub-zeke23-t11788030.html" class="cellMainLink">Borg, Sonia-8 Oral Sex Games For Ultimate Fun &amp; Pleasure - epub - zeke23</a></div>
			</td>
			<td class="nobr center" data-sort="2335397">2.23 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T19:38:59+00:00">20 Dec 2015, 19:38:59</span></td>
			<td class="green center">267</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11780513,0" class="icommentjs kaButton smallButton rightButton" href="/alan-dean-foster-star-wars-the-force-awakens-delshady-digital-epub-t11780513.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/alan-dean-foster-star-wars-the-force-awakens-delshady-digital-epub-t11780513.html" class="cellMainLink">Alan Dean Foster - Star Wars: The Force Awakens [Delshady Digital] [EPUB]</a></div>
			</td>
			<td class="nobr center" data-sort="653421">638.11 <span>KB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T10:05:38+00:00">19 Dec 2015, 10:05:38</span></td>
			<td class="green center">223</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790873,0" class="icommentjs kaButton smallButton rightButton" href="/a-beautiful-bowl-of-soup-the-best-vegetarian-recipes-by-william-meppem-t11790873.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/a-beautiful-bowl-of-soup-the-best-vegetarian-recipes-by-william-meppem-t11790873.html" class="cellMainLink">A Beautiful Bowl of Soup The Best Vegetarian Recipes by William Meppem</a></div>
			</td>
			<td class="nobr center" data-sort="37451985">35.72 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T09:45:44+00:00">21 Dec 2015, 09:45:44</span></td>
			<td class="green center">206</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11792270,0" class="icommentjs kaButton smallButton rightButton" href="/injustice-gods-among-us-year-five-001-2015-digital-son-of-ultron-empire-cbr-nem-t11792270.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/injustice-gods-among-us-year-five-001-2015-digital-son-of-ultron-empire-cbr-nem-t11792270.html" class="cellMainLink">Injustice - Gods Among Us- Year Five 001 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="22605617">21.56 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T16:23:46+00:00">21 Dec 2015, 16:23:46</span></td>
			<td class="green center">204</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11779763,0" class="icommentjs kaButton smallButton rightButton" href="/aa-star-wars-the-force-awakens-alan-dean-foster-vong-t11779763.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/aa-star-wars-the-force-awakens-alan-dean-foster-vong-t11779763.html" class="cellMainLink">AA - Star Wars The Force Awakens - Alan Dean Foster - (vonG)</a></div>
			</td>
			<td class="nobr center" data-sort="297347490">283.57 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T06:15:40+00:00">19 Dec 2015, 06:15:40</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11790931,0" class="icommentjs kaButton smallButton rightButton" href="/wok-cooking-made-easy-delicious-meals-in-minutes-t11790931.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/wok-cooking-made-easy-delicious-meals-in-minutes-t11790931.html" class="cellMainLink">Wok Cooking Made Easy Delicious Meals in Minutes</a></div>
			</td>
			<td class="nobr center" data-sort="7092340">6.76 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T09:59:00+00:00">21 Dec 2015, 09:59:00</span></td>
			<td class="green center">133</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-invention-of-science-a-new-history-of-the-scientific-revolution-epub-ertb-requested-t11793875.html" class="cellMainLink">The Invention of Science - A New History of the Scientific Revolution [ePUB] {{ERTB}} (Requested)</a></div>
			</td>
			<td class="nobr center" data-sort="5748544">5.48 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-22T01:35:14+00:00">22 Dec 2015, 01:35:14</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">54</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11786268,0" class="icommentjs kaButton smallButton rightButton" href="/the-london-symphony-orchestra-christmas-seasons-2002-flac-tfm-t11786268.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pictureType">
                    <a href="/the-london-symphony-orchestra-christmas-seasons-2002-flac-tfm-t11786268.html" class="cellMainLink">The London Symphony Orchestra - Christmas Seasons - (2002) - [FLAC] - [TFM]</a></div>
			</td>
			<td class="nobr center" data-sort="349100653">332.93 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T15:14:13+00:00">20 Dec 2015, 15:14:13</span></td>
			<td class="green center">314</td>
			<td class="red lasttd center">85</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781605,0" class="icommentjs kaButton smallButton rightButton" href="/amy-winehouse-albums-discography-2003-2012-flac-r-b-neo-soul-jazz-blues-reggae-t11781605.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/amy-winehouse-albums-discography-2003-2012-flac-r-b-neo-soul-jazz-blues-reggae-t11781605.html" class="cellMainLink">Amy Winehouse - Albums Discography (2003-2012) FLAC [R&amp;B, Neo soul, Jazz, Blues, Reggae]</a></div>
			</td>
			<td class="nobr center" data-sort="4762772120">4.44 <span>GB</span></td>
			<td class="center">98</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T14:40:55+00:00">19 Dec 2015, 14:40:55</span></td>
			<td class="green center">166</td>
			<td class="red lasttd center">121</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781562,0" class="icommentjs kaButton smallButton rightButton" href="/stevie-wonder-12-albums-collection-shm-cd-2012-flac-soul-smooth-soul-funk-pop-soul-r-b-jazz-t11781562.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/stevie-wonder-12-albums-collection-shm-cd-2012-flac-soul-smooth-soul-funk-pop-soul-r-b-jazz-t11781562.html" class="cellMainLink">Stevie Wonder - 12 Albums Collection SHM-CD (2012) FLAC [Soul, Smooth soul, Funk, Pop soul, R&amp;B, Jazz]</a></div>
			</td>
			<td class="nobr center" data-sort="6048730352">5.63 <span>GB</span></td>
			<td class="center">506</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T14:32:22+00:00">19 Dec 2015, 14:32:22</span></td>
			<td class="green center">134</td>
			<td class="red lasttd center">154</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11780480,0" class="icommentjs kaButton smallButton rightButton" href="/diana-krall-discography-1993-2012-flac-vocal-jazz-contemporary-jazz-smooth-jazz-bossa-nova-pop-t11780480.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/diana-krall-discography-1993-2012-flac-vocal-jazz-contemporary-jazz-smooth-jazz-bossa-nova-pop-t11780480.html" class="cellMainLink">Diana Krall - Discography (1993-2012) FLAC [Vocal Jazz, Contemporary Jazz, Smooth Jazz, Bossa Nova, Pop]</a></div>
			</td>
			<td class="nobr center" data-sort="4998936226">4.66 <span>GB</span></td>
			<td class="center">353</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T09:52:05+00:00">19 Dec 2015, 09:52:05</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">116</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11786046,0" class="icommentjs kaButton smallButton rightButton" href="/j-s-bach-magnificat-christmas-cantata-dunedin-consort-john-butt-2015-24-96-hd-flac-t11786046.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/j-s-bach-magnificat-christmas-cantata-dunedin-consort-john-butt-2015-24-96-hd-flac-t11786046.html" class="cellMainLink">J. S. Bach - Magnificat, Christmas Cantata (Dunedin Consort, John Butt) (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1909148408">1.78 <span>GB</span></td>
			<td class="center">59</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T14:07:23+00:00">20 Dec 2015, 14:07:23</span></td>
			<td class="green center">165</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11781510,0" class="icommentjs kaButton smallButton rightButton" href="/barry-white-discography-1973-1999-flac-soul-blues-disco-urban-t11781510.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/barry-white-discography-1973-1999-flac-soul-blues-disco-urban-t11781510.html" class="cellMainLink">Barry White - Discography (1973-1999) FLAC [Soul, Blues, Disco, Urban]</a></div>
			</td>
			<td class="nobr center" data-sort="6090327232">5.67 <span>GB</span></td>
			<td class="center">257</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T14:23:07+00:00">19 Dec 2015, 14:23:07</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">135</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11788053,0" class="icommentjs kaButton smallButton rightButton" href="/the-smiths-complete-2013-24-96-hd-flac-t11788053.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-smiths-complete-2013-24-96-hd-flac-t11788053.html" class="cellMainLink">The Smiths - Complete (2013) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3826325629">3.56 <span>GB</span></td>
			<td class="center">68</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T19:46:40+00:00">20 Dec 2015, 19:46:40</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">86</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11792271,0" class="icommentjs kaButton smallButton rightButton" href="/tom-petty-the-heartbreakers-feat-john-lee-hooker-live-at-the-fillmore-1997-sbd-flac-t11792271.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tom-petty-the-heartbreakers-feat-john-lee-hooker-live-at-the-fillmore-1997-sbd-flac-t11792271.html" class="cellMainLink">Tom Petty &amp; The Heartbreakers feat. John Lee Hooker - Live at the Fillmore 1997 (SBD) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1163717612">1.08 <span>GB</span></td>
			<td class="center">64</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T16:24:08+00:00">21 Dec 2015, 16:24:08</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11782691,0" class="icommentjs kaButton smallButton rightButton" href="/debussy-la-mer-stravinsky-the-firebird-dudamel-la-philharmonic-2014-24-96-hd-flac-t11782691.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/debussy-la-mer-stravinsky-the-firebird-dudamel-la-philharmonic-2014-24-96-hd-flac-t11782691.html" class="cellMainLink">Debussy - La Mer, Stravinsky - The Firebird - Dudamel LA Philharmonic (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1305421869">1.22 <span>GB</span></td>
			<td class="center">43</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-19T20:02:54+00:00">19 Dec 2015, 20:02:54</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11785145,0" class="icommentjs kaButton smallButton rightButton" href="/prince-the-hits-2-flac-tntvillage-t11785145.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/prince-the-hits-2-flac-tntvillage-t11785145.html" class="cellMainLink">Prince - The Hits 2 [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="488214971">465.6 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T09:18:57+00:00">20 Dec 2015, 09:18:57</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11785121,0" class="icommentjs kaButton smallButton rightButton" href="/bill-evans-trio-explorations-2011-24-88-hd-flac-t11785121.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bill-evans-trio-explorations-2011-24-88-hd-flac-t11785121.html" class="cellMainLink">Bill Evans Trio - Explorations (2011) [24-88 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1196251807">1.11 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-20T09:11:22+00:00">20 Dec 2015, 09:11:22</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11777757,0" class="icommentjs kaButton smallButton rightButton" href="/oscar-peterson-plays-count-basie-2015-24-192-hd-flac-t11777757.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/oscar-peterson-plays-count-basie-2015-24-192-hd-flac-t11777757.html" class="cellMainLink">Oscar Peterson - Plays Count Basie (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1804630694">1.68 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-18T19:11:37+00:00">18 Dec 2015, 19:11:37</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11793050,0" class="icommentjs kaButton smallButton rightButton" href="/beethoven-symphonies-nos-4-6-wiener-philharmoniker-christian-thielemann-2015-24-48-hd-flac-t11793050.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/beethoven-symphonies-nos-4-6-wiener-philharmoniker-christian-thielemann-2015-24-48-hd-flac-t11793050.html" class="cellMainLink">Beethoven - Symphonies Nos.4-6 - Wiener Philharmoniker, Christian Thielemann (2015) [24-48 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1173570242">1.09 <span>GB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T19:49:08+00:00">21 Dec 2015, 19:49:08</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11791463,0" class="icommentjs kaButton smallButton rightButton" href="/georgie-fame-the-whole-world-s-shaking-5cd-box-2015-flac-t11791463.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/georgie-fame-the-whole-world-s-shaking-5cd-box-2015-flac-t11791463.html" class="cellMainLink">Georgie Fame - The Whole World&#039;s Shaking (5CD Box 2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1420239337">1.32 <span>GB</span></td>
			<td class="center">143</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T12:21:01+00:00">21 Dec 2015, 12:21:01</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11792314,0" class="icommentjs kaButton smallButton rightButton" href="/va-ultimate-divas-4cds-of-the-greatest-female-voices-2015-flac-t11792314.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ultimate-divas-4cds-of-the-greatest-female-voices-2015-flac-t11792314.html" class="cellMainLink">VA - Ultimate... Divas: 4CDs Of The Greatest Female Voices (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="1891400974">1.76 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-21T16:37:01+00:00">21 Dec 2015, 16:37:01</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">19</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-games-are-you-playing-right-now-v2/?unread=17240173">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What Games are you playing right now? V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/driver777/">driver777</a></span></span> <time class="timeago" datetime="2015-12-22T18:03:23+00:00">22 Dec 2015, 18:03</time></span>
	</li>
		<li>
		<a href="/community/show/darky-s-book-store/?unread=17240172">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Darky&#039;s Book Store
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/DarkUS3R/">DarkUS3R</a></span></span> <time class="timeago" datetime="2015-12-22T18:03:18+00:00">22 Dec 2015, 18:03</time></span>
	</li>
		<li>
		<a href="/community/show/courtesies-downloading/?unread=17240171">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Courtesies of Downloading
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/SantaMG./">SantaMG.</a></span></span> <time class="timeago" datetime="2015-12-22T18:03:13+00:00">22 Dec 2015, 18:03</time></span>
	</li>
		<li>
		<a href="/community/show/official-movie-request-thread-v2/?unread=17240169">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Official Movie Request Thread V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Zvyozdochkova/">Zvyozdochkova</a></span></span> <time class="timeago" datetime="2015-12-22T18:02:31+00:00">22 Dec 2015, 18:02</time></span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v12/?unread=17240165">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V12
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/hermes010/">hermes010</a></span></span> <time class="timeago" datetime="2015-12-22T18:00:21+00:00">22 Dec 2015, 18:00</time></span>
	</li>
		<li>
		<a href="/community/show/post-threads-need-be-moved-or-trashed-v2-thread-108123/?unread=17240161">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Post Threads that Need to be Moved or Trashed V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/Ajay.Ghale/">Ajay.Ghale</a></span></span> <time class="timeago" datetime="2015-12-22T17:58:23+00:00">22 Dec 2015, 17:58</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/TheDels/post/star-wars-the-force-awakens-review-no-spoilers/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Star Wars: The Force Awakens Review [NO SPOILERS]</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-12-22T14:03:22+00:00">22 Dec 2015, 14:03</time></span></li>
	<li><a href="/blog/iKatKit/post/a-question-to-all-experienced/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> A question to all âexperiencedâ</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/iKatKit/">iKatKit</a> <time class="timeago" datetime="2015-12-22T05:08:58+00:00">22 Dec 2015, 05:08</time></span></li>
	<li><a href="/blog/SushiKushi/post/december-announcements-recruitment-winter-plans/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> December Announcements: Recruitment / Winter Plans</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/SushiKushi/">SushiKushi</a> <time class="timeago" datetime="2015-12-22T03:43:32+00:00">22 Dec 2015, 03:43</time></span></li>
	<li><a href="/blog/Bayfia/post/hearts-of-space-christmas-program-hos-1068-caritas-re-uploaded/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Hearts of Space - Christmas Program (HOS #1068 - Caritas) - RE-UPLOADED</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/Bayfia/">Bayfia</a> <time class="timeago" datetime="2015-12-21T21:14:16+00:00">21 Dec 2015, 21:14</time></span></li>
	<li><a href="/blog/iORcHiD/post/about-my-uploads/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> About My Uploads..</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/iORcHiD/">iORcHiD</a> <time class="timeago" datetime="2015-12-21T19:47:58+00:00">21 Dec 2015, 19:47</time></span></li>
	<li><a href="/blog/RuneMagle/post/the-bathroom-ghost-true-story/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The Bathroom Ghost (True Story)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/RuneMagle/">RuneMagle</a> <time class="timeago" datetime="2015-12-21T10:39:36+00:00">21 Dec 2015, 10:39</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/rape%20scenes/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				rape scenes
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/insidious%202013/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				insidious 2013
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/girlsdoporn%20e266/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				girlsdoporn e266
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/hans%20zimmer/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				hans zimmer
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/stranded%20deep/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Stranded Deep
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/avg/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				avg
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/adobe%20reader%20pro/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				adobe reader pro
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/mockingjay/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Mockingjay
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/maya/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				maya
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20balcklist/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the balcklist
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/jurassic%20world%202015%20hindi/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				jurassic world 2015 hindi
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Serbian-Cyrillic (ijekavica)</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.kat.cr');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></div>
        <div  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
