<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.27" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.27" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.27"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.27"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.27"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.27"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.27"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-10-07 06:20:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             17 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/nasa-just-put-a-picture-of-the-india-pak-border-and-itll-make-you-realise-just-how-close-we-actually-are-245914.html" class=" tint" title="NASA Just Put A Picture Of The India Pak Border And It'll Make You Realise Just How Close We Actually Are!">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/nasa502_1444114975_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/nasa502_1444114975_236x111.jpg"  border="0" alt="NASA Just Put A Picture Of The India Pak Border And It'll Make You Realise Just How Close We Actually Are!"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/nasa-just-put-a-picture-of-the-india-pak-border-and-itll-make-you-realise-just-how-close-we-actually-are-245914.html" title="NASA Just Put A Picture Of The India Pak Border And It'll Make You Realise Just How Close We Actually Are!">
                            NASA Just Put A Picture Of The India Pak Border And It'll Make You Realise Just How Close We Actually Are!                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/sachin-tendulkar-shane-warne-face-off-to-be-named-cricket-all-star-series-3-t20s-in-usa-to-be-played-next-month-245953.html" title="Sachin Tendulkar, Shane Warne Face-Off To Be Named 'Cricket All-Star Series'. 3 T20s In USA To Be Played Next Month" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/sachinwarnelegends_1444138036_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/sachinwarnelegends_1444138036_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/sachin-tendulkar-shane-warne-face-off-to-be-named-cricket-all-star-series-3-t20s-in-usa-to-be-played-next-month-245953.html" title="Sachin Tendulkar, Shane Warne Face-Off To Be Named 'Cricket All-Star Series'. 3 T20s In USA To Be Played Next Month">
                            Sachin Tendulkar, Shane Warne Face-Off To Be Named 'Cricket All-Star Series'. 3 T20s In USA To Be Played Next Month                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/29-years-after-the-nuclear-disaster-wildlife-is-thriving-in-chernobyl-245945.html" title="29 Years After The Nuclear Disaster, Wildlife Is Thriving In Chernobyl" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/cp_1444132151_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/cp_1444132151_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/29-years-after-the-nuclear-disaster-wildlife-is-thriving-in-chernobyl-245945.html" title="29 Years After The Nuclear Disaster, Wildlife Is Thriving In Chernobyl">
                            29 Years After The Nuclear Disaster, Wildlife Is Thriving In Chernobyl                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/2016-homeless-world-cup-of-football-to-be-hosted-by-kolkata-giving-hundreds-of-street-kids-hope-of-glory-245943.html" title="2016 Homeless World Cup of Football To Be Hosted By Kolkata Giving Hundreds Of Street Kids Hope Of Glory" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/homeless2016_1444130369_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/homeless2016_1444130369_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/2016-homeless-world-cup-of-football-to-be-hosted-by-kolkata-giving-hundreds-of-street-kids-hope-of-glory-245943.html" title="2016 Homeless World Cup of Football To Be Hosted By Kolkata Giving Hundreds Of Street Kids Hope Of Glory">
                            2016 Homeless World Cup of Football To Be Hosted By Kolkata Giving Hundreds Of Street Kids Hope Of Glory                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            14 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/instagram-ceo-says-we-want-to-freethenipple-but-apple-won-t-let-us-245940.html" title="Instagram CEO Says We Want To #FreeTheNipple But Apple Won't Let Us!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/freeythenipple-502_1444135512_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/freeythenipple-502_1444135512_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/instagram-ceo-says-we-want-to-freethenipple-but-apple-won-t-let-us-245940.html" title="Instagram CEO Says We Want To #FreeTheNipple But Apple Won't Let Us!">
                            Instagram CEO Says We Want To #FreeTheNipple But Apple Won't Let Us!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/lifestyle/style/ace-designers-pankaj-and-nidhi-team-up-with-a-fashion-etailer-for-a-new-collection-and-the-price-will-shock-you-245784.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/style/ace-designers-pankaj-and-nidhi-team-up-with-a-fashion-etailer-for-a-new-collection-and-the-price-will-shock-you-245784.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Oct/card_1444112061_1444112064_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/card_1444112061_1444112064_502x234.jpg"  border="0" alt="Ace Designers Pankaj And Nidhi Team Up With A Fashion E-tailer For A New Collection. And The Price Will Shock You!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/lifestyle/style/ace-designers-pankaj-and-nidhi-team-up-with-a-fashion-etailer-for-a-new-collection-and-the-price-will-shock-you-245784.html" title="Ace Designers Pankaj And Nidhi Team Up With A Fashion E-tailer For A New Collection. And The Price Will Shock You!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/style/ace-designers-pankaj-and-nidhi-team-up-with-a-fashion-etailer-for-a-new-collection-and-the-price-will-shock-you-245784.html');">Ace Designers Pankaj And Nidhi Team Up With A Fashion E-tailer For A New Collection. And The Price Will Shock You!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-mugged-at-a-mumbai-nightclub-by-four-girls-how-s-that-for-women-s-lib-truestory-245954.html" class="tint" title="Salman Khan Mugged At A Mumbai Nightclub By Four Girls - How's That For Women's Lib? #TrueStory" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/celebs/salman-khan-mugged-at-a-mumbai-nightclub-by-four-girls-how-s-that-for-women-s-lib-truestory-245954.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/salman-wallet502_1444146637_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/salman-wallet502_1444146637_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-mugged-at-a-mumbai-nightclub-by-four-girls-how-s-that-for-women-s-lib-truestory-245954.html" title="Salman Khan Mugged At A Mumbai Nightclub By Four Girls - How's That For Women's Lib? #TrueStory" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/celebs/salman-khan-mugged-at-a-mumbai-nightclub-by-four-girls-how-s-that-for-women-s-lib-truestory-245954.html');">
                            Salman Khan Mugged At A Mumbai Nightclub By Four Girls - How's That For Women's Lib? #TrueStory                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/tips-tricks/14-hacks-you-can-rely-on-for-your-beauty-and-health-fixes-245426.html" class="tint" title="14 Hacks You Can Rely On For Your Beauty And Health Fixes" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/tips-tricks/14-hacks-you-can-rely-on-for-your-beauty-and-health-fixes-245426.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/card10_1442830984_1442830989_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card10_1442830984_1442830989_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/14-hacks-you-can-rely-on-for-your-beauty-and-health-fixes-245426.html" title="14 Hacks You Can Rely On For Your Beauty And Health Fixes" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/tips-tricks/14-hacks-you-can-rely-on-for-your-beauty-and-health-fixes-245426.html');">
                            14 Hacks You Can Rely On For Your Beauty And Health Fixes                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/after-his-post-on-dadri-lynching-went-viral-farhan-akhtar-gives-a-fitting-reply-to-the-man-who-called-him-pakistani-245949.html" class="tint" title="After His Post On Dadri Lynching Went Viral, Farhan Akhtar Gives A Fitting Reply To The Man Who Called Him 'Pakistani'">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/farhan-crd_1444134758_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/farhan-crd_1444134758_218x102.jpg" border="0" alt="After His Post On Dadri Lynching Went Viral, Farhan Akhtar Gives A Fitting Reply To The Man Who Called Him 'Pakistani'"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/after-his-post-on-dadri-lynching-went-viral-farhan-akhtar-gives-a-fitting-reply-to-the-man-who-called-him-pakistani-245949.html" title="After His Post On Dadri Lynching Went Viral, Farhan Akhtar Gives A Fitting Reply To The Man Who Called Him 'Pakistani'">
                            After His Post On Dadri Lynching Went Viral, Farhan Akhtar Gives A Fitting Reply To The Man Who Called Him 'Pakistani'                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/14-simple-ways-you-can-kill-that-nervousness-before-a-big-presentation-245713.html" class="tint" title="16 Simple Ways You Can Kill That Nervousness Before A Big Presentation">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/cc_1444126277_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/cc_1444126277_218x102.jpg" border="0" alt="16 Simple Ways You Can Kill That Nervousness Before A Big Presentation"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/14-simple-ways-you-can-kill-that-nervousness-before-a-big-presentation-245713.html" title="16 Simple Ways You Can Kill That Nervousness Before A Big Presentation">
                            16 Simple Ways You Can Kill That Nervousness Before A Big Presentation                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/25-iconic-films-you-won-t-believe-were-actually-rejected-by-these-actors-245843.html" class="tint" title="25 Iconic Films You Won't Believe Were Actually Rejected By These Actors!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/card_1444127810_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/card_1444127810_218x102.jpg" border="0" alt="25 Iconic Films You Won't Believe Were Actually Rejected By These Actors!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/25-iconic-films-you-won-t-believe-were-actually-rejected-by-these-actors-245843.html" title="25 Iconic Films You Won't Believe Were Actually Rejected By These Actors!">
                            25 Iconic Films You Won't Believe Were Actually Rejected By These Actors!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-mugged-at-a-mumbai-nightclub-by-four-girls-how-s-that-for-women-s-lib-truestory-245954.html" class="tint" title="Salman Khan Mugged At A Mumbai Nightclub By Four Girls - How's That For Women's Lib? #TrueStory">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/salman-wallet502_1444146637_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/salman-wallet502_1444146637_218x102.jpg" border="0" alt="Salman Khan Mugged At A Mumbai Nightclub By Four Girls - How's That For Women's Lib? #TrueStory"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-mugged-at-a-mumbai-nightclub-by-four-girls-how-s-that-for-women-s-lib-truestory-245954.html" title="Salman Khan Mugged At A Mumbai Nightclub By Four Girls - How's That For Women's Lib? #TrueStory">
                            Salman Khan Mugged At A Mumbai Nightclub By Four Girls - How's That For Women's Lib? #TrueStory                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-good-samaritan-forces-traffic-police-to-issue-a-challan-to-a-law-breaking-cop-245938.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/this-good-samaritan-forces-traffic-police-to-issue-a-challan-to-a-law-breaking-cop-245938.html" class="tint" title="This Good Samaritan Forces Traffic Police To Issue A Challan To A Law-Breaking Cop!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Oct/police_challan_card_1444127004_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Oct/police_challan_card_1444127004_218x102.jpg" border="0" alt="This Good Samaritan Forces Traffic Police To Issue A Challan To A Law-Breaking Cop!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-good-samaritan-forces-traffic-police-to-issue-a-challan-to-a-law-breaking-cop-245938.html" title="This Good Samaritan Forces Traffic Police To Issue A Challan To A Law-Breaking Cop!">
                            This Good Samaritan Forces Traffic Police To Issue A Challan To A Law-Breaking Cop!                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/a-21-year-old-indian-smashes-china-s-guinness-world-record-for-memorising-the-value-of-pi-245935.html" title="A 21-Year-Old Indian Smashes Chinese Guinness World Record For Memorising The Value Of Pi!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage-55_1444126223_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/a-21-year-old-indian-smashes-china-s-guinness-world-record-for-memorising-the-value-of-pi-245935.html" title="A 21-Year-Old Indian Smashes Chinese Guinness World Record For Memorising The Value Of Pi!">
                            A 21-Year-Old Indian Smashes Chinese Guinness World Record For Memorising The Value Of Pi!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/a-facebook-glitch-logged-a-man-into-a-woman-s-account-he-eventually-ended-up-marrying-her-245931.html" title="A Facebook Glitch Logged A Man Into A Woman's Account, He Eventually Ended Up Marrying Her" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/love502_1444122588_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/a-facebook-glitch-logged-a-man-into-a-woman-s-account-he-eventually-ended-up-marrying-her-245931.html" title="A Facebook Glitch Logged A Man Into A Woman's Account, He Eventually Ended Up Marrying Her">
                            A Facebook Glitch Logged A Man Into A Woman's Account, He Eventually Ended Up Marrying Her                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/meet-abdul-gani-the-man-who-performs-last-rites-of-patients-whose-relatives-don-t-return-to-claim-the-dead-bodies-245936.html" title="Meet Abdul Gani, The Man Who Performs Last Rites Of Patients Whose Relatives Don't Return To Claim The Dead Bodies!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/gani-502_1444125872_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/meet-abdul-gani-the-man-who-performs-last-rites-of-patients-whose-relatives-don-t-return-to-claim-the-dead-bodies-245936.html" title="Meet Abdul Gani, The Man Who Performs Last Rites Of Patients Whose Relatives Don't Return To Claim The Dead Bodies!">
                            Meet Abdul Gani, The Man Who Performs Last Rites Of Patients Whose Relatives Don't Return To Claim The Dead Bodies!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            15 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/this-four-year-old-boy-had-a-dead-foetus-inside-him-245933.html" title="This Four-Year-Old Boy From West Bengal Had A Dead Foetus Inside Him!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/reuter-6_1444127365_1444127370_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/this-four-year-old-boy-had-a-dead-foetus-inside-him-245933.html" title="This Four-Year-Old Boy From West Bengal Had A Dead Foetus Inside Him!">
                            This Four-Year-Old Boy From West Bengal Had A Dead Foetus Inside Him!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            16 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/delhi-hc-helps-19-yo-nri-transgender-to-get-back-his-passport-and-freedom-from-regressive-parents-245929.html" title="Delhi HC Helps 19-YO NRI Transgender To Get Back His Passport And Freedom From Regressive Parents" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/shivi-502_1444121876_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/delhi-hc-helps-19-yo-nri-transgender-to-get-back-his-passport-and-freedom-from-regressive-parents-245929.html" title="Delhi HC Helps 19-YO NRI Transgender To Get Back His Passport And Freedom From Regressive Parents">
                            Delhi HC Helps 19-YO NRI Transgender To Get Back His Passport And Freedom From Regressive Parents                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/a-bollywood-actress-shares-her-gut-wrenching-experience-of-being-in-the-same-room-with-nirbhaya-s-rapists-245950.html" class="tint" title="A Bollywood Actress Shares Her Gut-Wrenching Experience Of Being In The Same Room With Nirbhaya's Rapists!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/ridhima-card_1444134685_1444134694_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/a-bollywood-actress-shares-her-gut-wrenching-experience-of-being-in-the-same-room-with-nirbhaya-s-rapists-245950.html" title="A Bollywood Actress Shares Her Gut-Wrenching Experience Of Being In The Same Room With Nirbhaya's Rapists!">
                            A Bollywood Actress Shares Her Gut-Wrenching Experience Of Being In The Same Room With Nirbhaya's Rapists!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/technology/planning-to-buy-the-new-iphone-this-one-tiny-glitch-might-ruin-it-245952.html" class="tint" title="Planning To Buy The New iPhone? This One Tiny Glitch Might Ruin It">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/ap_2-5_1444135959_1444135963_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/planning-to-buy-the-new-iphone-this-one-tiny-glitch-might-ruin-it-245952.html" title="Planning To Buy The New iPhone? This One Tiny Glitch Might Ruin It">
                            Planning To Buy The New iPhone? This One Tiny Glitch Might Ruin It                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/13-things-youll-relate-to-if-youre-living-with-the-bwoyfriend-245041.html" class="tint" title="13 Things You'll Relate To If You're Living With The Bwoyfriend">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/2_1441785950_1441785957_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/13-things-youll-relate-to-if-youre-living-with-the-bwoyfriend-245041.html" title="13 Things You'll Relate To If You're Living With The Bwoyfriend">
                            13 Things You'll Relate To If You're Living With The Bwoyfriend                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-the-quickest-lungi-exchange-in-the-history-of-mankind-get-ready-to-be-amazed-245918.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/this-is-the-quickest-lungi-exchange-in-the-history-of-mankind-get-ready-to-be-amazed-245918.html" class="tint" title="This Is The Quickest Lungi Exchange In The History Of Mankind. Get Ready To Be Amazed!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/lungi_card_1444116090_218x102.jpg" border="0" alt="This Is The Quickest Lungi Exchange In The History Of Mankind. Get Ready To Be Amazed!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-is-the-quickest-lungi-exchange-in-the-history-of-mankind-get-ready-to-be-amazed-245918.html" title="This Is The Quickest Lungi Exchange In The History Of Mankind. Get Ready To Be Amazed!">
                            This Is The Quickest Lungi Exchange In The History Of Mankind. Get Ready To Be Amazed!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/who-we-are/a-bollywood-actress-shares-her-gut-wrenching-experience-of-being-in-the-same-room-with-nirbhaya-s-rapists-245950.html" class="tint" title="A Bollywood Actress Shares Her Gut-Wrenching Experience Of Being In The Same Room With Nirbhaya's Rapists!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/ridhima-card_1444134685_1444134694_218x102.jpg" border="0" alt="A Bollywood Actress Shares Her Gut-Wrenching Experience Of Being In The Same Room With Nirbhaya's Rapists!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/a-bollywood-actress-shares-her-gut-wrenching-experience-of-being-in-the-same-room-with-nirbhaya-s-rapists-245950.html" title="A Bollywood Actress Shares Her Gut-Wrenching Experience Of Being In The Same Room With Nirbhaya's Rapists!">
                            A Bollywood Actress Shares Her Gut-Wrenching Experience Of Being In The Same Room With Nirbhaya's Rapists!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/this-2minute-video-teaches-you-an-easy-trick-that-will-make-you-less-forgetful-245585.html'>video</a>
                        <a href="http://www.indiatimes.com/health/tips-tricks/this-2minute-video-teaches-you-an-easy-trick-that-will-make-you-less-forgetful-245585.html" class="tint" title="This 2-Minute Video Teaches You An Easy Trick That Will Make You Less Forgetful!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/cover_1443179753_218x102.jpg" border="0" alt="This 2-Minute Video Teaches You An Easy Trick That Will Make You Less Forgetful!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/this-2minute-video-teaches-you-an-easy-trick-that-will-make-you-less-forgetful-245585.html" title="This 2-Minute Video Teaches You An Easy Trick That Will Make You Less Forgetful!">
                            This 2-Minute Video Teaches You An Easy Trick That Will Make You Less Forgetful!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/13-things-youll-relate-to-if-youre-living-with-the-bwoyfriend-245041.html" class="tint" title="13 Things You'll Relate To If You're Living With The Bwoyfriend">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/2_1441785950_1441785957_218x102.jpg" border="0" alt="13 Things You'll Relate To If You're Living With The Bwoyfriend"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/13-things-youll-relate-to-if-youre-living-with-the-bwoyfriend-245041.html" title="13 Things You'll Relate To If You're Living With The Bwoyfriend">
                            13 Things You'll Relate To If You're Living With The Bwoyfriend                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/7-paintings-that-prove-you-dont-need-a-canvas-to-create-a-masterpiece-245842.html" class="tint" title="7 Paintings That Prove You Don't Need A Canvas To Create A Masterpiece">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1443875302_218x102.jpg" border="0" alt="7 Paintings That Prove You Don't Need A Canvas To Create A Masterpiece"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/7-paintings-that-prove-you-dont-need-a-canvas-to-create-a-masterpiece-245842.html" title="7 Paintings That Prove You Don't Need A Canvas To Create A Masterpiece">
                            7 Paintings That Prove You Don't Need A Canvas To Create A Masterpiece                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/sports/9-things-india-must-do-to-improve-their-performances-in-t20s-245916.html" title="9 Things India Must Do To Improve Their Performances In T20s" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/indiat20card_1444118881_236x111.jpg" border="0" alt="9 Things India Must Do To Improve Their Performances In T20s"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/9-things-india-must-do-to-improve-their-performances-in-t20s-245916.html" title="9 Things India Must Do To Improve Their Performances In T20s">
                            9 Things India Must Do To Improve Their Performances In T20s                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/meet-anmol-swami-the-google-boy-of-meerut-245924.html" title="Meet Anmol Swami, The 'Google Boy' Of Meerut" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/news18-5_1444118579_1444118582_236x111.jpg" border="0" alt="Meet Anmol Swami, The 'Google Boy' Of Meerut"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/meet-anmol-swami-the-google-boy-of-meerut-245924.html" title="Meet Anmol Swami, The 'Google Boy' Of Meerut">
                            Meet Anmol Swami, The 'Google Boy' Of Meerut                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/uttar-pradesh-police-ask-twitter-to-delete-âprovocativeâ-posts-on-beefmurder-of-dadri-245921.html" title="Uttar Pradesh Police Ask Twitter To Delete âProvocativeâ Posts On #BeefMurder, #DadriLynching" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1444120128_236x111.jpg" border="0" alt="Uttar Pradesh Police Ask Twitter To Delete âProvocativeâ Posts On #BeefMurder, #DadriLynching"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/uttar-pradesh-police-ask-twitter-to-delete-âprovocativeâ-posts-on-beefmurder-of-dadri-245921.html" title="Uttar Pradesh Police Ask Twitter To Delete âProvocativeâ Posts On #BeefMurder, #DadriLynching">
                            Uttar Pradesh Police Ask Twitter To Delete âProvocativeâ Posts On #BeefMurder, #DadriLynching                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/employees-attack-air-france-executives-after-company-culls-2900-jobs-times-when-indian-workers-took-part-in-murderous-protests-245919.html" title="Employees Attack Air France Executives After Company Culls 2900 Jobs + Times When India Witnessed Murderous Protests" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/air502_1444117271_236x111.jpg" border="0" alt="Employees Attack Air France Executives After Company Culls 2900 Jobs + Times When India Witnessed Murderous Protests"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/employees-attack-air-france-executives-after-company-culls-2900-jobs-times-when-indian-workers-took-part-in-murderous-protests-245919.html" title="Employees Attack Air France Executives After Company Culls 2900 Jobs + Times When India Witnessed Murderous Protests">
                            Employees Attack Air France Executives After Company Culls 2900 Jobs + Times When India Witnessed Murderous Protests                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/no-takers-for-the-ancient-language-this-155year-old-sanskrit-college-has-only-3-students-and-a-principal-245922.html" title="No Takers For The Ancient Language. This 155-Year-Old Sanskrit College Has Only 3 Students And A Principal!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/college-502_1444117313_236x111.jpg" border="0" alt="No Takers For The Ancient Language. This 155-Year-Old Sanskrit College Has Only 3 Students And A Principal!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/no-takers-for-the-ancient-language-this-155year-old-sanskrit-college-has-only-3-students-and-a-principal-245922.html" title="No Takers For The Ancient Language. This 155-Year-Old Sanskrit College Has Only 3 Students And A Principal!">
                            No Takers For The Ancient Language. This 155-Year-Old Sanskrit College Has Only 3 Students And A Principal!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/after-his-post-on-dadri-lynching-went-viral-farhan-akhtar-gives-a-fitting-reply-to-the-man-who-called-him-pakistani-245949.html" class="tint" title="After His Post On Dadri Lynching Went Viral, Farhan Akhtar Gives A Fitting Reply To The Man Who Called Him 'Pakistani'">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/farhan-crd_1444134758_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bollywood/after-his-post-on-dadri-lynching-went-viral-farhan-akhtar-gives-a-fitting-reply-to-the-man-who-called-him-pakistani-245949.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/after-his-post-on-dadri-lynching-went-viral-farhan-akhtar-gives-a-fitting-reply-to-the-man-who-called-him-pakistani-245949.html" title="After His Post On Dadri Lynching Went Viral, Farhan Akhtar Gives A Fitting Reply To The Man Who Called Him 'Pakistani'">
                            After His Post On Dadri Lynching Went Viral, Farhan Akhtar Gives A Fitting Reply To The Man Who Called Him 'Pakistani'                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/farida-khanum-singing-aaj-jane-ki-zid-na-karo-for-coke-studio-will-remind-you-of-the-one-who-got-away-245942.html" class="tint" title="Farida Khanum Singing 'Aaj Jane Ki Zid Na Karo' For Coke Studio Will Remind You Of The One Who Got Away">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/shahid-and-alia_1444130138_1444130143_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/farida-khanum-singing-aaj-jane-ki-zid-na-karo-for-coke-studio-will-remind-you-of-the-one-who-got-away-245942.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/farida-khanum-singing-aaj-jane-ki-zid-na-karo-for-coke-studio-will-remind-you-of-the-one-who-got-away-245942.html" title="Farida Khanum Singing 'Aaj Jane Ki Zid Na Karo' For Coke Studio Will Remind You Of The One Who Got Away">
                            Farida Khanum Singing 'Aaj Jane Ki Zid Na Karo' For Coke Studio Will Remind You Of The One Who Got Away                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/kamal-haasan-donates-crores-worth-of-revenue-from-his-first-ever-tv-ad-for-a-good-cause-245941.html" class="tint" title="Kamal Haasan Donates Crores Worth Of Revenue From His First Ever TV Ad For A Good Cause!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/original_1444128990_1444128994_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/celebs/kamal-haasan-donates-crores-worth-of-revenue-from-his-first-ever-tv-ad-for-a-good-cause-245941.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/kamal-haasan-donates-crores-worth-of-revenue-from-his-first-ever-tv-ad-for-a-good-cause-245941.html" title="Kamal Haasan Donates Crores Worth Of Revenue From His First Ever TV Ad For A Good Cause!">
                            Kamal Haasan Donates Crores Worth Of Revenue From His First Ever TV Ad For A Good Cause!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/deepikas-transformation-into-newage-madhubala-is-making-us-curious-excited-at-the-same-time-245915.html" class="tint" title="Deepika's Transformation Into New-Age Madhubala Is Making Us Curious & Excited At The Same Time!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/deepika-as-madhubala_1444115302_1444115310_218x102.jpg" border="0" alt="Deepika's Transformation Into New-Age Madhubala Is Making Us Curious & Excited At The Same Time!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/deepikas-transformation-into-newage-madhubala-is-making-us-curious-excited-at-the-same-time-245915.html" title="Deepika's Transformation Into New-Age Madhubala Is Making Us Curious & Excited At The Same Time!">
                            Deepika's Transformation Into New-Age Madhubala Is Making Us Curious & Excited At The Same Time!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khans-lawyer-says-the-word-alcohol-was-added-later-in-hospital-records-245925.html" class="tint" title="Salman Khan's Lawyer Says The Word 'Alcohol' Was Added Later In Hospital Records">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/fb_1444118548_1444118554_218x102.jpg" border="0" alt="Salman Khan's Lawyer Says The Word 'Alcohol' Was Added Later In Hospital Records"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khans-lawyer-says-the-word-alcohol-was-added-later-in-hospital-records-245925.html" title="Salman Khan's Lawyer Says The Word 'Alcohol' Was Added Later In Hospital Records">
                            Salman Khan's Lawyer Says The Word 'Alcohol' Was Added Later In Hospital Records                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/healthyliving/10-stunning-black-and-white-pictures-that-showcase-anxiety-will-take-your-breath-away-245795.html" class="tint" title="10 Stunning Black And White Pictures Showcasing Anxiety That  Will Take Your Breath Away">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cover_1443697071_218x102.jpg" border="0" alt="10 Stunning Black And White Pictures Showcasing Anxiety That  Will Take Your Breath Away"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/10-stunning-black-and-white-pictures-that-showcase-anxiety-will-take-your-breath-away-245795.html" title="10 Stunning Black And White Pictures Showcasing Anxiety That  Will Take Your Breath Away">
                            10 Stunning Black And White Pictures Showcasing Anxiety That  Will Take Your Breath Away                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-calls-herself-the-female-jason-bourne-wait-what-just-happened-245917.html" class="tint" title="Priyanka Chopra Calls Herself The Female Jason Bourne. Wait, What Just Happened?">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1444117984_218x102.jpg" border="0" alt="Priyanka Chopra Calls Herself The Female Jason Bourne. Wait, What Just Happened?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-calls-herself-the-female-jason-bourne-wait-what-just-happened-245917.html" title="Priyanka Chopra Calls Herself The Female Jason Bourne. Wait, What Just Happened?">
                            Priyanka Chopra Calls Herself The Female Jason Bourne. Wait, What Just Happened?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/11-thoughts-that-go-through-your-mind-when-you-get-invited-to-your-college-reunion-245721.html" class="tint" title="11 Thoughts That Go Through Your Mind When You Get Invited To Your College Reunion">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/468298_478ca4_1443593785_218x102.jpg" border="0" alt="11 Thoughts That Go Through Your Mind When You Get Invited To Your College Reunion"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-thoughts-that-go-through-your-mind-when-you-get-invited-to-your-college-reunion-245721.html" title="11 Thoughts That Go Through Your Mind When You Get Invited To Your College Reunion">
                            11 Thoughts That Go Through Your Mind When You Get Invited To Your College Reunion                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/with-580-fake-credit-cards-thieves-stole-284-crore-from-kotak-mahindra-bank-245912.html" title="With 580 Fake Credit Cards, Thieves Stole 2.84 Crore From Kotak Mahindra Bank!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/thetechjournal-6_1444114340_1444114344_236x111.jpg" border="0" alt="With 580 Fake Credit Cards, Thieves Stole 2.84 Crore From Kotak Mahindra Bank!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/with-580-fake-credit-cards-thieves-stole-284-crore-from-kotak-mahindra-bank-245912.html" title="With 580 Fake Credit Cards, Thieves Stole 2.84 Crore From Kotak Mahindra Bank!">
                            With 580 Fake Credit Cards, Thieves Stole 2.84 Crore From Kotak Mahindra Bank!                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/iit-madras-plans-to-predict-earthquakes-through-satellites-in-space-245911.html" title="IIT- Madras Plans To Predict Earthquakes Through Satellites In Space" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/earth-quake-502_1444113776_236x111.jpg" border="0" alt="IIT- Madras Plans To Predict Earthquakes Through Satellites In Space"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/iit-madras-plans-to-predict-earthquakes-through-satellites-in-space-245911.html" title="IIT- Madras Plans To Predict Earthquakes Through Satellites In Space">
                            IIT- Madras Plans To Predict Earthquakes Through Satellites In Space                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/startups-eye-top-engineering-colleges-bschools-as-campus-recruitment-targets-grow-threefold-245913.html" title="Startups Eye Top Engineering Colleges, B-Schools As Campus Recruitment Targets Grow Three-Fold" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/untitled-502_1444119677_236x111.jpg" border="0" alt="Startups Eye Top Engineering Colleges, B-Schools As Campus Recruitment Targets Grow Three-Fold"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/startups-eye-top-engineering-colleges-bschools-as-campus-recruitment-targets-grow-threefold-245913.html" title="Startups Eye Top Engineering Colleges, B-Schools As Campus Recruitment Targets Grow Three-Fold">
                            Startups Eye Top Engineering Colleges, B-Schools As Campus Recruitment Targets Grow Three-Fold                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/indian-woman-slapped-with-a-hefty-fine-of-rs-17000-at-nz-airport-for-not-declaring-cow-urine-245909.html" title="Indian Woman Slapped With A Hefty Fine Of Rs 17,000 At NZ Airport For Not Declaring Cow Urine" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cow502_1444112635_236x111.jpg" border="0" alt="Indian Woman Slapped With A Hefty Fine Of Rs 17,000 At NZ Airport For Not Declaring Cow Urine"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/indian-woman-slapped-with-a-hefty-fine-of-rs-17000-at-nz-airport-for-not-declaring-cow-urine-245909.html" title="Indian Woman Slapped With A Hefty Fine Of Rs 17,000 At NZ Airport For Not Declaring Cow Urine">
                            Indian Woman Slapped With A Hefty Fine Of Rs 17,000 At NZ Airport For Not Declaring Cow Urine                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            19 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/delhi-police-issues-notice-to-flipkart-for-selling-stolen-phones-to-customers-245907.html" title="Delhi Police Issues Notice To Flipkart For Selling Stolen Phones To Customers!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/flip-502_1444112328_236x111.jpg" border="0" alt="Delhi Police Issues Notice To Flipkart For Selling Stolen Phones To Customers!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/delhi-police-issues-notice-to-flipkart-for-selling-stolen-phones-to-customers-245907.html" title="Delhi Police Issues Notice To Flipkart For Selling Stolen Phones To Customers!">
                            Delhi Police Issues Notice To Flipkart For Selling Stolen Phones To Customers!                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-good-samaritan-forces-traffic-police-to-issue-a-challan-to-a-law-breaking-cop-245938.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-good-samaritan-forces-traffic-police-to-issue-a-challan-to-a-law-breaking-cop-245938.html" class="tint" title="This Good Samaritan Forces Traffic Police To Issue A Challan To A Law-Breaking Cop!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/police_challan_card_1444127004_502x234.jpg" border="0" alt="This Good Samaritan Forces Traffic Police To Issue A Challan To A Law-Breaking Cop!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-good-samaritan-forces-traffic-police-to-issue-a-challan-to-a-law-breaking-cop-245938.html" title="This Good Samaritan Forces Traffic Police To Issue A Challan To A Law-Breaking Cop!">
                        This Good Samaritan Forces Traffic Police To Issue A Challan To A Law-Breaking Cop!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/shahid-alia-are-all-set-to-make-jhalak-finale-a-shaandaar-one-here-s-all-the-dope-245930.html" class="tint" title="Shahid & Alia Are All Set To Make Jhalak Finale A 'Shaandaar' One! Here's All The Dope">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/jhalak-card_1444122409_1444122421_502x234.jpg" border="0" alt="Shahid & Alia Are All Set To Make Jhalak Finale A 'Shaandaar' One! Here's All The Dope" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/shahid-alia-are-all-set-to-make-jhalak-finale-a-shaandaar-one-here-s-all-the-dope-245930.html" title="Shahid & Alia Are All Set To Make Jhalak Finale A 'Shaandaar' One! Here's All The Dope">
                        Shahid & Alia Are All Set To Make Jhalak Finale A 'Shaandaar' One! Here's All The Dope                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/25-iconic-films-you-won-t-believe-were-actually-rejected-by-these-actors-245843.html" class="tint" title="25 Iconic Films You Won't Believe Were Actually Rejected By These Actors!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1444127810_502x234.jpg" border="0" alt="25 Iconic Films You Won't Believe Were Actually Rejected By These Actors!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/25-iconic-films-you-won-t-believe-were-actually-rejected-by-these-actors-245843.html" title="25 Iconic Films You Won't Believe Were Actually Rejected By These Actors!">
                        25 Iconic Films You Won't Believe Were Actually Rejected By These Actors!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-stunning-black-and-white-pictures-that-showcase-anxiety-will-take-your-breath-away-245795.html" class="tint" title="10 Stunning Black And White Pictures Showcasing Anxiety That  Will Take Your Breath Away">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cover_1443697071_502x234.jpg" border="0" alt="10 Stunning Black And White Pictures Showcasing Anxiety That  Will Take Your Breath Away" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-stunning-black-and-white-pictures-that-showcase-anxiety-will-take-your-breath-away-245795.html" title="10 Stunning Black And White Pictures Showcasing Anxiety That  Will Take Your Breath Away">
                        10 Stunning Black And White Pictures Showcasing Anxiety That  Will Take Your Breath Away                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/aditya-pancholi-to-face-eviction-after-losing-rights-to-his-juhu-home-in-a-legal-battle-245926.html" class="tint" title="Aditya Pancholi To Face Eviction After Losing Rights To His Juhu Home In A Legal Battle">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/22_1444119567_1444119579_502x234.jpg" border="0" alt="Aditya Pancholi To Face Eviction After Losing Rights To His Juhu Home In A Legal Battle" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/aditya-pancholi-to-face-eviction-after-losing-rights-to-his-juhu-home-in-a-legal-battle-245926.html" title="Aditya Pancholi To Face Eviction After Losing Rights To His Juhu Home In A Legal Battle">
                        Aditya Pancholi To Face Eviction After Losing Rights To His Juhu Home In A Legal Battle                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/7-paintings-that-prove-you-dont-need-a-canvas-to-create-a-masterpiece-245842.html" class="tint" title="7 Paintings That Prove You Don't Need A Canvas To Create A Masterpiece">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1443875302_502x234.jpg" border="0" alt="7 Paintings That Prove You Don't Need A Canvas To Create A Masterpiece" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/7-paintings-that-prove-you-dont-need-a-canvas-to-create-a-masterpiece-245842.html" title="7 Paintings That Prove You Don't Need A Canvas To Create A Masterpiece">
                        7 Paintings That Prove You Don't Need A Canvas To Create A Masterpiece                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-calls-herself-the-female-jason-bourne-wait-what-just-happened-245917.html" class="tint" title="Priyanka Chopra Calls Herself The Female Jason Bourne. Wait, What Just Happened?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1444117984_502x234.jpg" border="0" alt="Priyanka Chopra Calls Herself The Female Jason Bourne. Wait, What Just Happened?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-calls-herself-the-female-jason-bourne-wait-what-just-happened-245917.html" title="Priyanka Chopra Calls Herself The Female Jason Bourne. Wait, What Just Happened?">
                        Priyanka Chopra Calls Herself The Female Jason Bourne. Wait, What Just Happened?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-the-quickest-lungi-exchange-in-the-history-of-mankind-get-ready-to-be-amazed-245918.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-is-the-quickest-lungi-exchange-in-the-history-of-mankind-get-ready-to-be-amazed-245918.html" class="tint" title="This Is The Quickest Lungi Exchange In The History Of Mankind. Get Ready To Be Amazed!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/lungi_card_1444116090_502x234.jpg" border="0" alt="This Is The Quickest Lungi Exchange In The History Of Mankind. Get Ready To Be Amazed!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-is-the-quickest-lungi-exchange-in-the-history-of-mankind-get-ready-to-be-amazed-245918.html" title="This Is The Quickest Lungi Exchange In The History Of Mankind. Get Ready To Be Amazed!">
                        This Is The Quickest Lungi Exchange In The History Of Mankind. Get Ready To Be Amazed!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-early-symptoms-of-heart-disease-that-will-help-you-prepare-you-for-the-worst-245788.html" class="tint" title="10 Early Symptoms Of Heart Disease That Will Help You Prepare You For The Worst!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-7_1443693749_502x234.jpg" border="0" alt="10 Early Symptoms Of Heart Disease That Will Help You Prepare You For The Worst!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-early-symptoms-of-heart-disease-that-will-help-you-prepare-you-for-the-worst-245788.html" title="10 Early Symptoms Of Heart Disease That Will Help You Prepare You For The Worst!">
                        10 Early Symptoms Of Heart Disease That Will Help You Prepare You For The Worst!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khans-lawyer-says-the-word-alcohol-was-added-later-in-hospital-records-245925.html" class="tint" title="Salman Khan's Lawyer Says The Word 'Alcohol' Was Added Later In Hospital Records">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/fb_1444118548_1444118554_502x234.jpg" border="0" alt="Salman Khan's Lawyer Says The Word 'Alcohol' Was Added Later In Hospital Records" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khans-lawyer-says-the-word-alcohol-was-added-later-in-hospital-records-245925.html" title="Salman Khan's Lawyer Says The Word 'Alcohol' Was Added Later In Hospital Records">
                        Salman Khan's Lawyer Says The Word 'Alcohol' Was Added Later In Hospital Records                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/deepikas-transformation-into-newage-madhubala-is-making-us-curious-excited-at-the-same-time-245915.html" class="tint" title="Deepika's Transformation Into New-Age Madhubala Is Making Us Curious & Excited At The Same Time!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/deepika-as-madhubala_1444115302_1444115310_502x234.jpg" border="0" alt="Deepika's Transformation Into New-Age Madhubala Is Making Us Curious & Excited At The Same Time!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/deepikas-transformation-into-newage-madhubala-is-making-us-curious-excited-at-the-same-time-245915.html" title="Deepika's Transformation Into New-Age Madhubala Is Making Us Curious & Excited At The Same Time!">
                        Deepika's Transformation Into New-Age Madhubala Is Making Us Curious & Excited At The Same Time!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/banhubali-has-arrived-to-put-an-end-to-all-the-bans-keeping-him-company-is-kattapa-245910.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/banhubali-has-arrived-to-put-an-end-to-all-the-bans-keeping-him-company-is-kattapa-245910.html" class="tint" title="'Banhubali' Has Arrived To Put An End To All The Bans & Keeping Him Company Is Kattapa!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/banhubali_card_1444113794_502x234.jpg" border="0" alt="'Banhubali' Has Arrived To Put An End To All The Bans & Keeping Him Company Is Kattapa!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/banhubali-has-arrived-to-put-an-end-to-all-the-bans-keeping-him-company-is-kattapa-245910.html" title="'Banhubali' Has Arrived To Put An End To All The Bans & Keeping Him Company Is Kattapa!">
                        'Banhubali' Has Arrived To Put An End To All The Bans & Keeping Him Company Is Kattapa!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/14-simple-ways-you-can-kill-that-nervousness-before-a-big-presentation-245713.html" class="tint" title="16 Simple Ways You Can Kill That Nervousness Before A Big Presentation">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cc_1444126277_502x234.jpg" border="0" alt="16 Simple Ways You Can Kill That Nervousness Before A Big Presentation" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/14-simple-ways-you-can-kill-that-nervousness-before-a-big-presentation-245713.html" title="16 Simple Ways You Can Kill That Nervousness Before A Big Presentation">
                        16 Simple Ways You Can Kill That Nervousness Before A Big Presentation                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/x-oldie-actors-who-got-raunchy-for-shoots-and-still-nailed-it-245790.html" class="tint" title="7 Age-Defying Actors Who Got Raunchy For Photoshoots & Gave Young Stars A Run For Their Money!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/download-3_1444048703_1444048711_502x234.jpg" border="0" alt="7 Age-Defying Actors Who Got Raunchy For Photoshoots & Gave Young Stars A Run For Their Money!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/x-oldie-actors-who-got-raunchy-for-shoots-and-still-nailed-it-245790.html" title="7 Age-Defying Actors Who Got Raunchy For Photoshoots & Gave Young Stars A Run For Their Money!">
                        7 Age-Defying Actors Who Got Raunchy For Photoshoots & Gave Young Stars A Run For Their Money!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/this-2minute-video-teaches-you-an-easy-trick-that-will-make-you-less-forgetful-245585.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/this-2minute-video-teaches-you-an-easy-trick-that-will-make-you-less-forgetful-245585.html" class="tint" title="This 2-Minute Video Teaches You An Easy Trick That Will Make You Less Forgetful!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/cover_1443179753_502x234.jpg" border="0" alt="This 2-Minute Video Teaches You An Easy Trick That Will Make You Less Forgetful!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/this-2minute-video-teaches-you-an-easy-trick-that-will-make-you-less-forgetful-245585.html" title="This 2-Minute Video Teaches You An Easy Trick That Will Make You Less Forgetful!">
                        This 2-Minute Video Teaches You An Easy Trick That Will Make You Less Forgetful!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-thoughts-that-go-through-your-mind-when-you-get-invited-to-your-college-reunion-245721.html" class="tint" title="11 Thoughts That Go Through Your Mind When You Get Invited To Your College Reunion">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/468298_478ca4_1443593785_502x234.jpg" border="0" alt="11 Thoughts That Go Through Your Mind When You Get Invited To Your College Reunion" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-thoughts-that-go-through-your-mind-when-you-get-invited-to-your-college-reunion-245721.html" title="11 Thoughts That Go Through Your Mind When You Get Invited To Your College Reunion">
                        11 Thoughts That Go Through Your Mind When You Get Invited To Your College Reunion                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/6-ways-you-use-basic-kitchen-ingredients-for-naturally-soft-hands-and-feet-245774.html" class="tint" title="6 Ways You Use Basic Kitchen Ingredients For Naturally Soft Hands And Feet">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-2_1443684006_502x234.jpg" border="0" alt="6 Ways You Use Basic Kitchen Ingredients For Naturally Soft Hands And Feet" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/6-ways-you-use-basic-kitchen-ingredients-for-naturally-soft-hands-and-feet-245774.html" title="6 Ways You Use Basic Kitchen Ingredients For Naturally Soft Hands And Feet">
                        6 Ways You Use Basic Kitchen Ingredients For Naturally Soft Hands And Feet                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/she-chose-her-passion-over-a-succescful-job-and-won-the-respect-of-everyone-who-ridiculed-her-245878.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/she-chose-her-passion-over-a-succescful-job-and-won-the-respect-of-everyone-who-ridiculed-her-245878.html" class="tint" title="She Chose Her Passion Over A Successful Job And Won The Respect Of Everyone Who Ridiculed Her">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/shecan_card_1444028936_502x234.jpg" border="0" alt="She Chose Her Passion Over A Successful Job And Won The Respect Of Everyone Who Ridiculed Her" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/she-chose-her-passion-over-a-succescful-job-and-won-the-respect-of-everyone-who-ridiculed-her-245878.html" title="She Chose Her Passion Over A Successful Job And Won The Respect Of Everyone Who Ridiculed Her">
                        She Chose Her Passion Over A Successful Job And Won The Respect Of Everyone Who Ridiculed Her                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/13-epic-music-festivals-in-india-you-shouldnt-miss-this-winter-245830.html" class="tint" title="13 Epic Music Festivals In India You Shouldn't Miss This Winter">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cp_1443853911_502x234.jpg" border="0" alt="13 Epic Music Festivals In India You Shouldn't Miss This Winter" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/13-epic-music-festivals-in-india-you-shouldnt-miss-this-winter-245830.html" title="13 Epic Music Festivals In India You Shouldn't Miss This Winter">
                        13 Epic Music Festivals In India You Shouldn't Miss This Winter                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/this-short-film-says-so-much-about-a-fatherdaughter-relationship-that-too-in-just-15-minutes-245876.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-short-film-says-so-much-about-a-fatherdaughter-relationship-that-too-in-just-15-minutes-245876.html" class="tint" title="This Short Film Says So Much About A Father-Daughter Relationship, That Too In Just 15 Minutes!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/screenshot_7_1444027551_1444027556_502x234.jpg" border="0" alt="This Short Film Says So Much About A Father-Daughter Relationship, That Too In Just 15 Minutes!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-short-film-says-so-much-about-a-fatherdaughter-relationship-that-too-in-just-15-minutes-245876.html" title="This Short Film Says So Much About A Father-Daughter Relationship, That Too In Just 15 Minutes!">
                        This Short Film Says So Much About A Father-Daughter Relationship, That Too In Just 15 Minutes!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/22-facts-about-sex-that-will-make-you-want-more-of-it-245685.html" class="tint" title="22 Facts About Sex That Will Make You Want More Of It">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443523365_502x234.jpg" border="0" alt="22 Facts About Sex That Will Make You Want More Of It" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/22-facts-about-sex-that-will-make-you-want-more-of-it-245685.html" title="22 Facts About Sex That Will Make You Want More Of It">
                        22 Facts About Sex That Will Make You Want More Of It                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/kunal-khemus-new-film-is-all-about-mens-obsession-with-their-guns-literally-245891.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/kunal-khemus-new-film-is-all-about-mens-obsession-with-their-guns-literally-245891.html" class="tint" title="Kunal Khemu's New Film Is All About Men's Obsession With Their 'Guns'. Literally!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/screenshot_3_1444040019_1444040043_502x234.jpg" border="0" alt="Kunal Khemu's New Film Is All About Men's Obsession With Their 'Guns'. Literally!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/kunal-khemus-new-film-is-all-about-mens-obsession-with-their-guns-literally-245891.html" title="Kunal Khemu's New Film Is All About Men's Obsession With Their 'Guns'. Literally!">
                        Kunal Khemu's New Film Is All About Men's Obsession With Their 'Guns'. Literally!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/nasa-just-put-8400-images-from-apollo-missions-on-flickr-in-all-their-hiresolution-glory-245892.html" class="tint" title="NASA Just Put 8,400 Images From Apollo Missions On Flickr In All Their Hi-Resolution Glory">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/moon-card_1444040476_502x234.jpg" border="0" alt="NASA Just Put 8,400 Images From Apollo Missions On Flickr In All Their Hi-Resolution Glory" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/nasa-just-put-8400-images-from-apollo-missions-on-flickr-in-all-their-hiresolution-glory-245892.html" title="NASA Just Put 8,400 Images From Apollo Missions On Flickr In All Their Hi-Resolution Glory">
                        NASA Just Put 8,400 Images From Apollo Missions On Flickr In All Their Hi-Resolution Glory                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/after-masaans-successful-stint-at-cannes-aligarh-steals-the-show-at-busan-film-festival-245877.html" class="tint" title="After Masaan's Successful Stint At Cannes, 'Aligarh' Steals The Show At Busan Film Festival">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/aligarh!_1444032604_1444032616_502x234.jpg" border="0" alt="After Masaan's Successful Stint At Cannes, 'Aligarh' Steals The Show At Busan Film Festival" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/after-masaans-successful-stint-at-cannes-aligarh-steals-the-show-at-busan-film-festival-245877.html" title="After Masaan's Successful Stint At Cannes, 'Aligarh' Steals The Show At Busan Film Festival">
                        After Masaan's Successful Stint At Cannes, 'Aligarh' Steals The Show At Busan Film Festival                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-short-horror-film-shows-what-happened-at-a-noida-office-after-two-sisters-were-stabbed-there-*chills*-245888.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-short-horror-film-shows-what-happened-at-a-noida-office-after-two-sisters-were-stabbed-there-*chills*-245888.html" class="tint" title="This Short Horror Film Shows What Happened At A Noida Office After Two Sisters Were Stabbed There! *Chills*">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/horror_film_card_1444035985_502x234.jpg" border="0" alt="This Short Horror Film Shows What Happened At A Noida Office After Two Sisters Were Stabbed There! *Chills*" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-short-horror-film-shows-what-happened-at-a-noida-office-after-two-sisters-were-stabbed-there-*chills*-245888.html" title="This Short Horror Film Shows What Happened At A Noida Office After Two Sisters Were Stabbed There! *Chills*">
                        This Short Horror Film Shows What Happened At A Noida Office After Two Sisters Were Stabbed There! *Chills*                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/satyajit-rays-apu-trilogy-finds-place-in-top-5-greatest-asian-films-time-to-be-proud-of-indian-cinema-245883.html" class="tint" title="Satyajit Ray's 'Apu Trilogy' Finds Place In Top 5 Greatest Asian Films! Time To Be Proud Of Indian Cinema!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1444031738_1444031744_502x234.jpg" border="0" alt="Satyajit Ray's 'Apu Trilogy' Finds Place In Top 5 Greatest Asian Films! Time To Be Proud Of Indian Cinema!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/satyajit-rays-apu-trilogy-finds-place-in-top-5-greatest-asian-films-time-to-be-proud-of-indian-cinema-245883.html" title="Satyajit Ray's 'Apu Trilogy' Finds Place In Top 5 Greatest Asian Films! Time To Be Proud Of Indian Cinema!">
                        Satyajit Ray's 'Apu Trilogy' Finds Place In Top 5 Greatest Asian Films! Time To Be Proud Of Indian Cinema!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/this-simple-exercise-can-be-done-anywhere-and-it-is-ahmazing-for-your-butt-245693.html" class="tint" title="This Simple Exercise Can Be Done Anywhere And It Is Ah-Mazing For Your Butt!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/patrick_1443519571_502x234.jpg" border="0" alt="This Simple Exercise Can Be Done Anywhere And It Is Ah-Mazing For Your Butt!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/this-simple-exercise-can-be-done-anywhere-and-it-is-ahmazing-for-your-butt-245693.html" title="This Simple Exercise Can Be Done Anywhere And It Is Ah-Mazing For Your Butt!">
                        This Simple Exercise Can Be Done Anywhere And It Is Ah-Mazing For Your Butt!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/11-lowbudget-bollywood-films-you-cant-afford-to-miss-if-youre-a-trueblue-film-buff-234148.html" class="tint" title="Talvar + 11 Low-Budget Bollywood Films Which Became Unexpected Hits. All Thanks To A Powerful Story!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/talvar-card_1444026716_1444026722_502x234.jpg" border="0" alt="Talvar + 11 Low-Budget Bollywood Films Which Became Unexpected Hits. All Thanks To A Powerful Story!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/11-lowbudget-bollywood-films-you-cant-afford-to-miss-if-youre-a-trueblue-film-buff-234148.html" title="Talvar + 11 Low-Budget Bollywood Films Which Became Unexpected Hits. All Thanks To A Powerful Story!">
                        Talvar + 11 Low-Budget Bollywood Films Which Became Unexpected Hits. All Thanks To A Powerful Story!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-salary-negotiation-tips-that-will-help-you-get-the-package-you-deserve-245799.html" class="tint" title="11 Salary Negotiation Tips That Will Help You Get The Package You Deserve">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1443699685_502x234.jpg" border="0" alt="11 Salary Negotiation Tips That Will Help You Get The Package You Deserve" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-salary-negotiation-tips-that-will-help-you-get-the-package-you-deserve-245799.html" title="11 Salary Negotiation Tips That Will Help You Get The Package You Deserve">
                        11 Salary Negotiation Tips That Will Help You Get The Package You Deserve                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/king-khan-teaches-his-trolls-a-lesson-which-theyd-never-want-to-forget-burn-245870.html" class="tint" title="King Khan Teaches His Trolls A Lesson Which They'd Never Want To Forget! #Burn">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/srk-card_1444023067_1444023075_502x234.jpg" border="0" alt="King Khan Teaches His Trolls A Lesson Which They'd Never Want To Forget! #Burn" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/king-khan-teaches-his-trolls-a-lesson-which-theyd-never-want-to-forget-burn-245870.html" title="King Khan Teaches His Trolls A Lesson Which They'd Never Want To Forget! #Burn">
                        King Khan Teaches His Trolls A Lesson Which They'd Never Want To Forget! #Burn                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/5-ways-in-which-a-5minute-break-at-work-could-recharge-your-batteries-245753.html" class="tint" title="5 Ways In Which A 5-Minute Break At Work Could Recharge Your Batteries">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/breakcover_1444109064_502x234.jpg" border="0" alt="5 Ways In Which A 5-Minute Break At Work Could Recharge Your Batteries" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/5-ways-in-which-a-5minute-break-at-work-could-recharge-your-batteries-245753.html" title="5 Ways In Which A 5-Minute Break At Work Could Recharge Your Batteries">
                        5 Ways In Which A 5-Minute Break At Work Could Recharge Your Batteries                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/17-things-that-will-inspire-you-to-do-more-with-your-life-245750.html" class="tint" title="17 Things That Will Inspire You To Do More With Your Life">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1443611571_502x234.jpg" border="0" alt="17 Things That Will Inspire You To Do More With Your Life" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/17-things-that-will-inspire-you-to-do-more-with-your-life-245750.html" title="17 Things That Will Inspire You To Do More With Your Life">
                        17 Things That Will Inspire You To Do More With Your Life                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/5-reasons-sleeping-naked-is-the-nicest-thing-you-can-do-for-your-body-245827.html" class="tint" title="5 Reasons Sleeping Naked Is The Nicest Thing You Can Do For Your Body">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1443791434_502x234.jpg" border="0" alt="5 Reasons Sleeping Naked Is The Nicest Thing You Can Do For Your Body" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/5-reasons-sleeping-naked-is-the-nicest-thing-you-can-do-for-your-body-245827.html" title="5 Reasons Sleeping Naked Is The Nicest Thing You Can Do For Your Body">
                        5 Reasons Sleeping Naked Is The Nicest Thing You Can Do For Your Body                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/everybody-loves-nut-butter-and-this-easy-recipe-lets-you-make-it-in-minutes-245745.html" class="tint" title="Everybody Loves Nut Butter, And This Easy Recipe Lets You Make It In Minutes!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-6_1443607304_502x234.jpg" border="0" alt="Everybody Loves Nut Butter, And This Easy Recipe Lets You Make It In Minutes!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/everybody-loves-nut-butter-and-this-easy-recipe-lets-you-make-it-in-minutes-245745.html" title="Everybody Loves Nut Butter, And This Easy Recipe Lets You Make It In Minutes!">
                        Everybody Loves Nut Butter, And This Easy Recipe Lets You Make It In Minutes!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-daring-animal-rescues-will-inspire-you-to-help-those-street-animals-who-have-noone-to-care-for-them-245854.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/these-daring-animal-rescues-will-inspire-you-to-help-those-street-animals-who-have-noone-to-care-for-them-245854.html" class="tint" title="These Daring Animal Rescues Will Inspire You To Help Those Street Animals Who Have Noone To Care For Them">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/animal_rescued_card_1443961061_502x234.jpg" border="0" alt="These Daring Animal Rescues Will Inspire You To Help Those Street Animals Who Have Noone To Care For Them" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/these-daring-animal-rescues-will-inspire-you-to-help-those-street-animals-who-have-noone-to-care-for-them-245854.html" title="These Daring Animal Rescues Will Inspire You To Help Those Street Animals Who Have Noone To Care For Them">
                        These Daring Animal Rescues Will Inspire You To Help Those Street Animals Who Have Noone To Care For Them                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-reasons-its-perfectly-okay-to-be-a-couch-potato-couple-245775.html" class="tint" title="7 Reasons It's Perfectly Okay To Be A Couch Potato Couple">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cp_1443684482_1443684496_502x234.jpg" border="0" alt="7 Reasons It's Perfectly Okay To Be A Couch Potato Couple" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-reasons-its-perfectly-okay-to-be-a-couch-potato-couple-245775.html" title="7 Reasons It's Perfectly Okay To Be A Couch Potato Couple">
                        7 Reasons It's Perfectly Okay To Be A Couch Potato Couple                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/5-things-that-prove-beyond-doubt-that-salman-khan-is-the-rajinikanth-of-bollywood-245863.html" class="tint" title="5 Things That Prove Beyond Doubt That Salman Khan Is The Rajinikanth Of Bollywood!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cad1_1443953801_1443953820_502x234.jpg" border="0" alt="5 Things That Prove Beyond Doubt That Salman Khan Is The Rajinikanth Of Bollywood!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/5-things-that-prove-beyond-doubt-that-salman-khan-is-the-rajinikanth-of-bollywood-245863.html" title="5 Things That Prove Beyond Doubt That Salman Khan Is The Rajinikanth Of Bollywood!">
                        5 Things That Prove Beyond Doubt That Salman Khan Is The Rajinikanth Of Bollywood!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-breathtaking-aerial-view-of-the-himalayas-from-20000-feet-will-leave-you-speechless-245864.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-breathtaking-aerial-view-of-the-himalayas-from-20000-feet-will-leave-you-speechless-245864.html" class="tint" title="This Breathtaking Aerial View Of The Himalayas From 20,000 Feet Will Leave You Speechless!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/mountain_card_1443955833_502x234.jpg" border="0" alt="This Breathtaking Aerial View Of The Himalayas From 20,000 Feet Will Leave You Speechless!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-breathtaking-aerial-view-of-the-himalayas-from-20000-feet-will-leave-you-speechless-245864.html" title="This Breathtaking Aerial View Of The Himalayas From 20,000 Feet Will Leave You Speechless!">
                        This Breathtaking Aerial View Of The Himalayas From 20,000 Feet Will Leave You Speechless!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/10-ways-you-can-use-haldi-to-get-beautiful-glowing-skin-245765.html" class="tint" title="10 Ways You Can Use Haldi To Get Beautiful, Glowing Skin">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1443679404_502x234.jpg" border="0" alt="10 Ways You Can Use Haldi To Get Beautiful, Glowing Skin" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/10-ways-you-can-use-haldi-to-get-beautiful-glowing-skin-245765.html" title="10 Ways You Can Use Haldi To Get Beautiful, Glowing Skin">
                        10 Ways You Can Use Haldi To Get Beautiful, Glowing Skin                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/anu-maliks-daughter-anmol-ripped-off-coldplays-paradise-and-failed-miserably-245858.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/anu-maliks-daughter-anmol-ripped-off-coldplays-paradise-and-failed-miserably-245858.html" class="tint" title="Anu Malik's Daughter Anmol Ripped Off Coldplay's 'Paradise' And Failed Miserably!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/lamhein_card_1443945034_502x234.jpg" border="0" alt="Anu Malik's Daughter Anmol Ripped Off Coldplay's 'Paradise' And Failed Miserably!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/anu-maliks-daughter-anmol-ripped-off-coldplays-paradise-and-failed-miserably-245858.html" title="Anu Malik's Daughter Anmol Ripped Off Coldplay's 'Paradise' And Failed Miserably!">
                        Anu Malik's Daughter Anmol Ripped Off Coldplay's 'Paradise' And Failed Miserably!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/11-bollywood-celebs-their-unconditional-love-for-pets-will-make-you-want-one-right-now-245855.html" class="tint" title="11 Bollywood Celebs Showing Unconditional Love For Their Pets Will Make You Want One Right Now!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/pet-card_1443950039_1443950047_502x234.jpg" border="0" alt="11 Bollywood Celebs Showing Unconditional Love For Their Pets Will Make You Want One Right Now!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/11-bollywood-celebs-their-unconditional-love-for-pets-will-make-you-want-one-right-now-245855.html" title="11 Bollywood Celebs Showing Unconditional Love For Their Pets Will Make You Want One Right Now!">
                        11 Bollywood Celebs Showing Unconditional Love For Their Pets Will Make You Want One Right Now!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-nudging-quotes-that-will-jolt-your-inner-procrastinator-into-action-245785.html" class="tint" title="11 Nudging Quotes That Will Jolt Your Inner Procrastinator Into Action">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cp_1443690420_502x234.jpg" border="0" alt="11 Nudging Quotes That Will Jolt Your Inner Procrastinator Into Action" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-nudging-quotes-that-will-jolt-your-inner-procrastinator-into-action-245785.html" title="11 Nudging Quotes That Will Jolt Your Inner Procrastinator Into Action">
                        11 Nudging Quotes That Will Jolt Your Inner Procrastinator Into Action                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/mahatma-gandhi-meets-rahul-gandhi-in-heaven-and-teaches-him-a-lesson-of-politics-245850.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/mahatma-gandhi-meets-rahul-gandhi-in-heaven-and-teaches-him-a-lesson-of-politics-245850.html" class="tint" title="Mahatma Gandhi Meets Rahul Gandhi In Heaven And Teaches Him A Lesson Of Politics!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/ragamaga_card_1443938055_502x234.jpg" border="0" alt="Mahatma Gandhi Meets Rahul Gandhi In Heaven And Teaches Him A Lesson Of Politics!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/mahatma-gandhi-meets-rahul-gandhi-in-heaven-and-teaches-him-a-lesson-of-politics-245850.html" title="Mahatma Gandhi Meets Rahul Gandhi In Heaven And Teaches Him A Lesson Of Politics!">
                        Mahatma Gandhi Meets Rahul Gandhi In Heaven And Teaches Him A Lesson Of Politics!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/9-celebrities-who-work-for-a-cause-not-applause-245742.html" class="tint" title="9 Celebrities Who Work For A Cause, Not Applause">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card1_1443680491_1443680507_502x234.jpg" border="0" alt="9 Celebrities Who Work For A Cause, Not Applause" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/9-celebrities-who-work-for-a-cause-not-applause-245742.html" title="9 Celebrities Who Work For A Cause, Not Applause">
                        9 Celebrities Who Work For A Cause, Not Applause                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/9-ways-in-which-praying-is-actually-good-for-your-health-backed-by-science-245787.html" class="tint" title="9 Ways In Which Praying Is Actually Good For Your Health - Backed By Science!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cover_1443691745_502x234.jpg" border="0" alt="9 Ways In Which Praying Is Actually Good For Your Health - Backed By Science!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/9-ways-in-which-praying-is-actually-good-for-your-health-backed-by-science-245787.html" title="9 Ways In Which Praying Is Actually Good For Your Health - Backed By Science!">
                        9 Ways In Which Praying Is Actually Good For Your Health - Backed By Science!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/a-popular-tv-actor-arrested-for-molesting-his-estranged-wife-in-mumbai-245851.html" class="tint" title="A Popular TV Actor Arrested For Molesting His Estranged Wife In Mumbai!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/8688895711_f59a8f5168_b_1443938885_1443938897_502x234.jpg" border="0" alt="A Popular TV Actor Arrested For Molesting His Estranged Wife In Mumbai!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/a-popular-tv-actor-arrested-for-molesting-his-estranged-wife-in-mumbai-245851.html" title="A Popular TV Actor Arrested For Molesting His Estranged Wife In Mumbai!">
                        A Popular TV Actor Arrested For Molesting His Estranged Wife In Mumbai!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-catfight-between-deepika-and-katrina-is-so-bad-that-even-ranbir-would-want-to-kill-himself-245848.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-catfight-between-deepika-and-katrina-is-so-bad-that-even-ranbir-would-want-to-kill-himself-245848.html" class="tint" title="This Cat-Fight Between Deepika And Katrina Is So Bad That Even Ranbir Would Want To Kill Himself!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/de_vs_kat_card_1443934216_502x234.jpg" border="0" alt="This Cat-Fight Between Deepika And Katrina Is So Bad That Even Ranbir Would Want To Kill Himself!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-catfight-between-deepika-and-katrina-is-so-bad-that-even-ranbir-would-want-to-kill-himself-245848.html" title="This Cat-Fight Between Deepika And Katrina Is So Bad That Even Ranbir Would Want To Kill Himself!">
                        This Cat-Fight Between Deepika And Katrina Is So Bad That Even Ranbir Would Want To Kill Himself!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/7-things-you-should-never-say-to-anyone-trying-to-lose-weight-if-you-value-your-life-245738.html" class="tint" title="7 Things You Should Never Say To Anyone Trying To Lose Weight If You Value Your Life">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-5_1443605293_502x234.jpg" border="0" alt="7 Things You Should Never Say To Anyone Trying To Lose Weight If You Value Your Life" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/7-things-you-should-never-say-to-anyone-trying-to-lose-weight-if-you-value-your-life-245738.html" title="7 Things You Should Never Say To Anyone Trying To Lose Weight If You Value Your Life">
                        7 Things You Should Never Say To Anyone Trying To Lose Weight If You Value Your Life                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/14-times-reading-between-the-lines-is-just-a-colossal-waste-of-time-245776.html" class="tint" title="14 Times Reading Between The Lines Is Just A Colossal Waste Of Time">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/1_1443685232_502x234.jpg" border="0" alt="14 Times Reading Between The Lines Is Just A Colossal Waste Of Time" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/14-times-reading-between-the-lines-is-just-a-colossal-waste-of-time-245776.html" title="14 Times Reading Between The Lines Is Just A Colossal Waste Of Time">
                        14 Times Reading Between The Lines Is Just A Colossal Waste Of Time                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/25-films-that-became-legendary-for-their-memorable-double-roles-245836.html" class="tint" title="25 Films That Became Legendary For Their Memorable Double Roles">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1443867513_502x234.jpg" border="0" alt="25 Films That Became Legendary For Their Memorable Double Roles" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/25-films-that-became-legendary-for-their-memorable-double-roles-245836.html" title="25 Films That Became Legendary For Their Memorable Double Roles">
                        25 Films That Became Legendary For Their Memorable Double Roles                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/this-short-film-says-so-much-about-a-fatherdaughter-relationship-that-too-in-just-15-minutes-245876.html'>video</a>					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/this-short-film-says-so-much-about-a-fatherdaughter-relationship-that-too-in-just-15-minutes-245876.html" class="tint" title="This Short Film Says So Much About A Father-Daughter Relationship, That Too In Just 15 Minutes!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/screenshot_7_1444027551_1444027556_218x102.jpg" border="0" alt="This Short Film Says So Much About A Father-Daughter Relationship, That Too In Just 15 Minutes!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/this-short-film-says-so-much-about-a-fatherdaughter-relationship-that-too-in-just-15-minutes-245876.html" title="This Short Film Says So Much About A Father-Daughter Relationship, That Too In Just 15 Minutes!">
                            This Short Film Says So Much About A Father-Daughter Relationship, That Too In Just 15 Minutes!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/x-oldie-actors-who-got-raunchy-for-shoots-and-still-nailed-it-245790.html" class="tint" title="7 Age-Defying Actors Who Got Raunchy For Photoshoots & Gave Young Stars A Run For Their Money!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/download-3_1444048703_1444048711_218x102.jpg" border="0" alt="7 Age-Defying Actors Who Got Raunchy For Photoshoots & Gave Young Stars A Run For Their Money!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/x-oldie-actors-who-got-raunchy-for-shoots-and-still-nailed-it-245790.html" title="7 Age-Defying Actors Who Got Raunchy For Photoshoots & Gave Young Stars A Run For Their Money!">
                            7 Age-Defying Actors Who Got Raunchy For Photoshoots & Gave Young Stars A Run For Their Money!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/style/ace-designers-pankaj-and-nidhi-team-up-with-a-fashion-etailer-for-a-new-collection-and-the-price-will-shock-you-245784.html" class="tint" title="Ace Designers Pankaj And Nidhi Team Up With A Fashion E-tailer For A New Collection. And The Price Will Shock You!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1444112061_1444112064_218x102.jpg" border="0" alt="Ace Designers Pankaj And Nidhi Team Up With A Fashion E-tailer For A New Collection. And The Price Will Shock You!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/style/ace-designers-pankaj-and-nidhi-team-up-with-a-fashion-etailer-for-a-new-collection-and-the-price-will-shock-you-245784.html" title="Ace Designers Pankaj And Nidhi Team Up With A Fashion E-tailer For A New Collection. And The Price Will Shock You!">
                            Ace Designers Pankaj And Nidhi Team Up With A Fashion E-tailer For A New Collection. And The Price Will Shock You!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-salary-negotiation-tips-that-will-help-you-get-the-package-you-deserve-245799.html" class="tint" title="11 Salary Negotiation Tips That Will Help You Get The Package You Deserve">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1443699685_218x102.jpg" border="0" alt="11 Salary Negotiation Tips That Will Help You Get The Package You Deserve"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-salary-negotiation-tips-that-will-help-you-get-the-package-you-deserve-245799.html" title="11 Salary Negotiation Tips That Will Help You Get The Package You Deserve">
                            11 Salary Negotiation Tips That Will Help You Get The Package You Deserve                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/health/tips-tricks/6-ways-you-use-basic-kitchen-ingredients-for-naturally-soft-hands-and-feet-245774.html" class="tint" title="6 Ways You Use Basic Kitchen Ingredients For Naturally Soft Hands And Feet">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-2_1443684006_218x102.jpg" border="0" alt="6 Ways You Use Basic Kitchen Ingredients For Naturally Soft Hands And Feet"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/6-ways-you-use-basic-kitchen-ingredients-for-naturally-soft-hands-and-feet-245774.html" title="6 Ways You Use Basic Kitchen Ingredients For Naturally Soft Hands And Feet">
                            6 Ways You Use Basic Kitchen Ingredients For Naturally Soft Hands And Feet                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '245784', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage-55_1444126223_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/love502_1444122588_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/gani-502_1444125872_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/reuter-6_1444127365_1444127370_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/shivi-502_1444121876_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/ridhima-card_1444134685_1444134694_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/ap_2-5_1444135959_1444135963_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/2_1441785950_1441785957_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/lungi_card_1444116090_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/ridhima-card_1444134685_1444134694_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/cover_1443179753_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/2_1441785950_1441785957_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1443875302_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Oct/indiat20card_1444118881_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/news18-5_1444118579_1444118582_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1444120128_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/air502_1444117271_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/college-502_1444117313_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/farhan-crd_1444134758_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/shahid-and-alia_1444130138_1444130143_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/original_1444128990_1444128994_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/deepika-as-madhubala_1444115302_1444115310_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/fb_1444118548_1444118554_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/cover_1443697071_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1444117984_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/468298_478ca4_1443593785_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Oct/thetechjournal-6_1444114340_1444114344_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/earth-quake-502_1444113776_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/untitled-502_1444119677_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/cow502_1444112635_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/flip-502_1444112328_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/police_challan_card_1444127004_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/jhalak-card_1444122409_1444122421_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1444127810_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cover_1443697071_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/22_1444119567_1444119579_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1443875302_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1444117984_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/lungi_card_1444116090_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-7_1443693749_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/fb_1444118548_1444118554_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/deepika-as-madhubala_1444115302_1444115310_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/banhubali_card_1444113794_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cc_1444126277_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/download-3_1444048703_1444048711_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/cover_1443179753_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/468298_478ca4_1443593785_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-2_1443684006_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/shecan_card_1444028936_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cp_1443853911_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/screenshot_7_1444027551_1444027556_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443523365_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/screenshot_3_1444040019_1444040043_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/moon-card_1444040476_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/aligarh!_1444032604_1444032616_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/horror_film_card_1444035985_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1444031738_1444031744_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/patrick_1443519571_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/talvar-card_1444026716_1444026722_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1443699685_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/srk-card_1444023067_1444023075_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/breakcover_1444109064_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1443611571_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1443791434_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-6_1443607304_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/animal_rescued_card_1443961061_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cp_1443684482_1443684496_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cad1_1443953801_1443953820_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/mountain_card_1443955833_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1443679404_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/lamhein_card_1443945034_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/pet-card_1443950039_1443950047_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cp_1443690420_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/ragamaga_card_1443938055_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card1_1443680491_1443680507_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cover_1443691745_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/8688895711_f59a8f5168_b_1443938885_1443938897_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/de_vs_kat_card_1443934216_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-5_1443605293_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/1_1443685232_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1443867513_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/screenshot_7_1444027551_1444027556_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/download-3_1444048703_1444048711_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1444112061_1444112064_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1443699685_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-2_1443684006_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,663,498<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>49,880 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.27" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.27"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.27"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.27"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.27"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>