



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='20/10/2015 12:22:46' /><meta property='busca:modified' content='20/10/2015 12:22:46' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/47876b251b81.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/e9597fc8c97a.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "{\"adUnit\":\"tvg_Globo.com.Home\", \"adPositions\":[\"banner_mobile_topo\",\"banner_mobile_meio\",\"banner_mobile_fim\",\"banner_slim_topo\",\"banner_slb_meio\",\"banner_slb_fim\",\"banner_selo4\",\"banner_floating\"]}", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><header id="header-produto" class="header-navegacao header-home" data-analytics="T"><div id="glbbarrawidget"></div><div class="floating-bar"><div class="header-bar"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-floating" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-floating" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div></div></div></div></div></div><div class="cube-container"><div class="cube"><div class="face front"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-top" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-top" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div><div id="busca-padrao"></div></div></div></div></div></div></div></header><script>
(function(window,document) {try{new CustomEvent("test");} catch(e){var CustomEvent=function(event,params) {var evt;params=params || {bubbles: false,
cancelable: false,
detail: undefined};evt=document.createEvent("CustomEvent");evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);return evt;};
CustomEvent.prototype=window.Event.prototype;window.CustomEvent=CustomEvent;}
document.dispatchEvent(new CustomEvent('glb.headerDom.ready'));})(window,document);</script><style type="text/css">
    /*
       evita que o markup do menu seja exibido sem estilo enquanto
       ele nÃ£o foi carregado
    */
    #menu-container {
        display: none;
    }
    #menu-fonts {
        height: 1px;
        width: 1px;
        color: transparent;
        overflow: hidden;
        position: absolute;
    }
</style>

<nav id="menu-container" class="regua-navegacao-tab menu-no-animation">
    

<script id="menu-carousel-template" type="x-tmpl-mustache">

<div id="menu-carousel-header-sizes-infos" class="menu-carousel-header-sizes-infos">
</div>
<div id="menu-carousel-header" class="menu-carousel menu-carousel-header swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <a href="{{link}}" class="menu-carousel-link" data-index="{{index}}">
                <svg class="brand-icon">
                    <use class="menu-logo" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-{{name}}"></use>
                </svg>
            </a>
            <div class="menu-carousel-link-shadow"></div>
        </li>
        {{/portals}}
    </ul>
</div>
<div id="menu-carousel-body" class="menu-carousel menu-carousel-body swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <div id="menu-rounder" class="menu-rounder">{{{menu}}}</div>
        </li>
        {{/portals}}
    </ul>
</div>

</script>

<script id="menu-rounder-template" type="x-tmpl-mustache">

  <div id="menu-cascade" class="menu-cascade" data-menu-index="{{index}}">
    <ul class="menu-root menu-level menu-level-0">{{#items}}{{>recursive_partial}}{{/items}}</ul>
  </div>
  <div id="menu-addon" class="menu-addon"></div>

</script>

<script id="menu-custom-template" type="x-tmpl-mustache">

    {{#items}}{{>recursive_partial}}{{/items}}

</script>


<script id="menu-dinamico-template" type="x-tmpl-mustache">

<li id="menu-{{deepness}}-{{tituloSlug}}" class="menu-item {{#separador}} menu-item-separator{{/separador}}{{#destacado}} menu-item-highlighted{{/destacado}}{{#agrupador}} menu-item-grouped{{/agrupador}}{{#hasChildren}} is-father{{/hasChildren}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}}">

    {{#shouldBeLink}}<a href="{{link}}" class="menu-item-link">{{/shouldBeLink}}
    {{^shouldBeLink}}<span class="menu-item-link">{{/shouldBeLink}}
        <span class="menu-item-title">{{titulo}}<span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xlink:href="#regua-arrow" /></svg></span></span>
    {{#shouldBeLink}}</a>{{/shouldBeLink}}
    {{^shouldBeLink}}</span>{{/shouldBeLink}}

    {{#hasChildren}}
        <div class="menu-level menu-submenu menu-submenu-level{{deepness}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}} menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back"><span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xlink:href="#regua-arrow" /></svg></span><span class="regua-navegacao-menu-item">{{titulo}}</span></a>
            <ul class="menu-submenu-vertical-scroll">
            {{#children}}
                {{>recursive_partial}}
            {{/children}}
            <ul>
        </div>
    {{/hasChildren}}
</li>

</script>


    <div id="menu-rounder" class="menu-rounder">
        <div id="menu-cascade" class="menu-cascade" data-menu-index="0">
            

<ul class="menu-root menu-level menu-level-0">
    
        




<li class="menu-item is-father " id="menu-1-famosos">

    <a class="menu-item-link">
        <span class="menu-item-title">Famosos</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Famosos</a>
            
                

                




<li class="menu-item " id="menu-2-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-paparazzo">

    <a href="http://ego.globo.com/paparazzo/index.html" class="menu-item-link">
        <span class="menu-item-title">Paparazzo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/noticias-da-tv/index.html" class="menu-item-link">
        <span class="menu-item-title">Patricia Kogut</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Quem</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-retratos-da-vida">

    <a href="http://extra.globo.com/famosos/" class="menu-item-link">
        <span class="menu-item-title">Retratos da Vida</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-moda-estilo">

    <a class="menu-item-link">
        <span class="menu-item-title">Moda &amp; Estilo</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Moda &amp; Estilo</a>
            
                

                




<li class="menu-item " id="menu-2-ela-no-o-globo">

    <a href="http://ela.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ela no O Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-estilo-no-gshow">

    <a href="http://gshow.globo.com/Estilo/" class="menu-item-link">
        <span class="menu-item-title">Estilo no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-ego">

    <a href="http://ego.globo.com/moda/index.html" class="menu-item-link">
        <span class="menu-item-title">Moda no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-gnt">

    <a href="http://gnt.globo.com/moda/" class="menu-item-link">
        <span class="menu-item-title">Moda no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-glamour">

    <a href="http://revistaglamour.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Glamour</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-gq-brasil">

    <a href="http://gq.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista GQ Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-marie-claire">

    <a href="http://revistamarieclaire.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Marie Claire</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-vogue">

    <a href="http://vogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Vogue</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-bem-estar-saude">

    <a class="menu-item-link">
        <span class="menu-item-title">Bem-Estar &amp; SaÃºde</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Bem-Estar &amp; SaÃºde</a>
            
                

                




<li class="menu-item " id="menu-2-beleza-no-ego">

    <a href="http://ego.globo.com/beleza/index.html" class="menu-item-link">
        <span class="menu-item-title">Beleza no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gnt">

    <a href="http://gnt.globo.com/beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gshow">

    <a href="http://gshow.globo.com/Estilo/Beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-g1">

    <a href="http://g1.globo.com/bemestar/" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no G1</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-gnt">

    <a href="http://gnt.com.br/bemestar" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-horoscopo-no-ego">

    <a href="http://horoscopo.ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">HorÃ³scopo no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-maes-no-gnt">

    <a href="http://gnt.globo.com/maes-e-filhos/" class="menu-item-link">
        <span class="menu-item-title">MÃ£es no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-meus-5-minutos">

    <a href="http://meus5minutos.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Meus 5 Minutos</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-crescer">

    <a href="http://revistacrescer.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Crescer</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-casa-decoracao">

    <a class="menu-item-link">
        <span class="menu-item-title">Casa &amp; DecoraÃ§Ã£o</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Casa &amp; DecoraÃ§Ã£o</a>
            
                

                




<li class="menu-item " id="menu-2-casa-no-gnt">

    <a href="http://gnt.globo.com/casa-e-decoracao/" class="menu-item-link">
        <span class="menu-item-title">Casa no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-casa-vogue">

    <a href="http://casavogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Casa Vogue</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-casa-e-jardim">

    <a href="http://revistacasaejardim.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Casa e Jardim</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-do-zap">

    <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=revista-imoveis" class="menu-item-link">
        <span class="menu-item-title">Revista do Zap</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-gastronomia">

    <a class="menu-item-link">
        <span class="menu-item-title">Gastronomia</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Gastronomia</a>
            
                

                




<li class="menu-item " id="menu-2-casa-e-comida">

    <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/" class="menu-item-link">
        <span class="menu-item-title">Casa e Comida</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-colheradas">

    <a href="http://meus5minutos.globo.com/blogs/Colheradas/" class="menu-item-link">
        <span class="menu-item-title">Colheradas</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-da-ana-maria">

    <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/" class="menu-item-link">
        <span class="menu-item-title">Receitas da Ana Maria</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitascom">

    <a href="http://gshow.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas.com</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-no-gnt">

    <a href="http://gnt.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas no GNT</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-musica">

    <a class="menu-item-link">
        <span class="menu-item-title">MÃºsica</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">MÃºsica</a>
            
                

                




<li class="menu-item " id="menu-2-altas-horas">

    <a href=" http://gshow.globo.com/programas/altas-horas/" class="menu-item-link">
        <span class="menu-item-title">Altas Horas </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-musica">

    <a href="http://g1.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">G1 MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-gshow-musica">

    <a href="http://gshow.globo.com/Musica/" class="menu-item-link">
        <span class="menu-item-title">Gshow MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musica-no-multishow">

    <a href="http://multishow.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica no Multishow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musicacombr">

    <a href="http://musica.com.br/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica.com.br</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-radiobeat">

    <a href="http://radiobeat.com.br/" class="menu-item-link">
        <span class="menu-item-title">RADIOBEAT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-som-brasil">

    <a href="http://globotv.globo.com/rede-globo/som-brasil/" class="menu-item-link">
        <span class="menu-item-title">Som Brasil</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-cinema-teatro">

    <a class="menu-item-link">
        <span class="menu-item-title">Cinema &amp; Teatro</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Cinema &amp; Teatro</a>
            
                

                




<li class="menu-item " id="menu-2-canal-brasil">

    <a href="http://canalbrasil.globo.com/ " class="menu-item-link">
        <span class="menu-item-title">Canal Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-canal-universal">

    <a href="http://universal.globo.com" class="menu-item-link">
        <span class="menu-item-title">Canal Universal</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-filmes-na-globo">

    <a href="http://redeglobo.globo.com/filmes/index.html" class="menu-item-link">
        <span class="menu-item-title">Filmes na Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-cinema">

    <a href="http://g1.globo.com/pop-arte/cinema/" class="menu-item-link">
        <span class="menu-item-title">G1 Cinema</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-globo-filmes">

    <a href=" http://globofilmes.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Globo Filmes</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-megapix">

    <a href="http://megapix.globo.com/" class="menu-item-link">
        <span class="menu-item-title">MegaPix</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-monet">

    <a href="http://revistamonet.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Monet</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-teatro">

    <a href="http://www.agentesevenoteatro.com.br/" class="menu-item-link">
        <span class="menu-item-title">Teatro</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-telecine">

    <a href="http://telecine.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Telecine</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">ego</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">quem</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">patricia kogut</span>
    </a>

    
</li>

    
</ul>

        </div>
        <div id="menu-addon" class="menu-addon"></div>
    </div>

    
</nav>

<div id="menu-fonts">
    
    
    <span class="open-sans">BESbswy</span>
    <span class="open-sans-bold">BESbswy</span>
    <span class="open-sans-light">BESbswy</span>
    <span class="roboto-slab">BESbswy</span>
    <span class="proximanova-semibold">BESbswy</span>
</div>

<div id="menu-content-overlay"></div>
<script>
(function (window) {window.REGUA_SETTINGS=window.REGUA_SETTINGS || {};window.REGUA_SETTINGS.portalName = "home";window.REGUA_SETTINGS.portalHome = "http://www.globo.com/";window.REGUA_SETTINGS.portalsList = ["home","g1","ge","gshow","famosos","techtudo","globotv","globosatplay"];window.REGUA_SETTINGS.portalsLinkList = ["http://www.globo.com/","http://g1.globo.com/index.html","http://globoesporte.globo.com/","http://gshow.globo.com/","http://famosos.globo.com/","http://www.techtudo.com.br/","http://globotv.globo.com/","http://globosatplay.globo.com/"];window.REGUA_SETTINGS.staticUrl = "http://s.glbimg.com/en/ho/static/";window.REGUA_SETTINGS.svgSpriteName = "sprite-43e3e93f50.svg";window.REGUA_SETTINGS.suggestUrl = "";})(window);
</script><script>(function(){var e,t,n,o;e=function(){function e(){this.name="Android",this.bridge=window.GloboBridge||{navigated:function(e){},menuAction:function(e){},reguaAction:function(e){}}}return e.prototype.navigateTo=function(e){this.bridge.navigated(JSON.stringify(e))},e.prototype.menuAction=function(e){this.bridge.menuAction(JSON.stringify(e))},e.prototype.reguaAction=function(e){this.bridge.reguaAction(JSON.stringify(e))},e}(),t=function(){function e(){this.name="iOS",this.connectWebViewJavascriptBridge(function(e){return e.init(function(e,t){return window.glb.masterApp.manageMessage(e)})})}return e.prototype.connectWebViewJavascriptBridge=function(e){window.WebViewJavascriptBridge?e(WebViewJavascriptBridge):document.addEventListener("WebViewJavascriptBridgeReady",function(){e(WebViewJavascriptBridge)},!1)},e.prototype.brigdeSendJson=function(e){return this.connectWebViewJavascriptBridge(function(t){t.send(JSON.stringify(e))})},e.prototype.navigateTo=function(e){this.brigdeSendJson(e)},e.prototype.menuAction=function(e){this.brigdeSendJson(e)},e.prototype.reguaAction=function(e){this.brigdeSendJson(e)},e}(),n=function(){function n(e){this.platformApp=e,this.initCalled=!1,null!=this.platformApp&&this.bind()}return n.prototype.init=function(){this.initCalled||(this.initCalled=!0,this.header&&(this.processHeaderInfo(),this.bindAfterInit()))},n.prototype.processHeaderInfo=function(){this.countHeaderTries=0,this.maxHeaderTries=100,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer?this.processHeaderInfoAction():setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)},n.prototype.processHeaderInfoAction=function(){var e;if(this.countHeaderTries++,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer)this.cacheVariables(),e=this.getHeaderData(),this.navigateTo(e);else{if(this.countHeaderTries>this.maxHeaderTries)return void console.log("Nao achou o markup interno do header");setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)}},n.prototype.bind=function(){var e;return this.bindAppEvents(),this.header=document.querySelector("#header-produto"),e=function(e){return function(){return e.header||(e.header=document.querySelector("#header-produto")),e.init()}}(this),this.header?this.init():(document.addEventListener("glb.headerDom.ready",e),document.addEventListener("DOMContentLoaded",e))},n.prototype.cacheVariables=function(){return this.elements={},this.elements.header=this.header,this.elements.headerCubeContainer=this.headerCubeContainer,this.elements.headerFront=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".front"):null,this.elements.headerLogoProduto=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-produto-container"):null,this.elements.headerLogoProdutoLink=this.elements.headerLogoProduto?this.elements.headerLogoProduto.parentNode:null,this.elements.headerLogoArea=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-area .logo"):null,this.elements.headerLogoAreaLink=this.elements.headerLogoProduto?this.elements.headerLogoArea.parentNode:null,this.elements.headerSubeditoria=this.elements.headerFront?this.elements.headerFront.querySelector(".menu-subeditoria"):null,this.elements.headerSubeditoriaLink=this.elements.headerSubeditoria?this.elements.headerSubeditoria.querySelector("a"):null,!0},n.prototype.bindAppEvents=function(){document.addEventListener("glb.menu-carousel.ready",function(e){return function(){return e.platformApp.menuAction({action:"menuReady"})}}(this)),document.addEventListener("glb.menu-carousel.shown.before",function(e){return function(){return e.platformApp.menuAction({action:"menuShownBefore"})}}(this)),document.addEventListener("glb.menu-carousel.shown.after",function(e){return function(){return e.platformApp.menuAction({action:"menuShownAfter"})}}(this)),document.addEventListener("glb.menu-carousel.off.before",function(e){return function(){return e.platformApp.menuAction({action:"menuOffBefore"})}}(this)),document.addEventListener("glb.menu-carousel.off.after",function(e){return function(){return e.platformApp.menuAction({action:"menuOffAfter"})}}(this)),document.addEventListener("glb.regua.exists",function(e){return function(){return e.platformApp.reguaAction({action:"reguaExists"})}}(this))},n.prototype.bindAfterInit=function(){},n.prototype._getText=function(e){return e?(e.innerText||e.textContent).trim()||"":""},n.prototype._getHref=function(e){return e?e.getAttribute("href")||"":""},n.prototype._getCssProp=function(e,t){var n;return e?(n=window.getComputedStyle(e),n.getPropertyValue(t)||""):""},n.prototype._getBackgroundColor=function(e){return this._getCssProp(e,"background-color")},n.prototype.getHeaderData=function(){var e;return e={color:this._getBackgroundColor(this.elements.headerFront),editoria:"",editoriaHref:"",subeditoria:"",subeditoriaHref:"",logo:"",logoHref:""},this.elements.headerLogoProduto?(e.logo=this._getText(this.elements.headerLogoProduto),e.logoHref=this._getHref(this.elements.headerLogoProdutoLink),e.editoria=this._getText(this.elements.headerLogoArea),e.editoriaHref=this._getHref(this.elements.headerLogoArea),this.elements.headerSubeditoria&&(e.subeditoria=this._getText(this.elements.headerSubeditoria),e.subeditoriaHref=this._getHref(this.elements.headerSubeditoriaLink))):(e.logo=this._getText(this.elements.headerLogoArea),e.logoHref=this._getHref(this.elements.headerLogoAreaLink)),e},n.prototype.navigateTo=function(e){e.action="navigateTo",e.url=window.location.href,this.platformApp?this.platformApp.navigateTo(e):console.log("Sem Plataforma")},n.prototype.manageMessage=function(e){var t,n;return e&&(t=JSON.parse(e),t.action="regua-change")?(n=t.rulerItem||"home",null!=window.$?$(document).trigger("glb.regua.change",[n]):console.log("Sem jQuery")):void 0},n.prototype.getUserAgent=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n.prototype.getPlatformApp=function(){var n;return n=this.getUserAgent(),n.match(/(iPad|iPhone|iPod)/g)?new t:n.match(/(Android)/g)?new e:void 0},n}(),window.glb=window.glb||{},window.glb.MasterApp=n,window.glb.AndroidApp=e,window.glb.IosApp=t,window.initMasterApp=function(){var e;if(!window.glb.masterApp)return e=n.prototype.getPlatformApp(),window.glb.masterApp=new n(e)},o=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,o||window.initMasterApp()}).call(this);</script><script>(function(){var n,o,e,t;e=function(){return/MobApp_Android|MobApp_iOS|MobApp_WP/},o=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n=function(){var n,t,a,i,r;i=o(),r=i.split(" ");for(a in r)if(n=r[a],e().test(n))return t=n.split("/"),{name:t[0].replace("_"," "),version:t[1],frameworkVersion:t[2]};return null},window.addGlbOnAppClass=function(){var o;window.glb=window.glb||{},window.glb.extractAppFromUserAgent||(window.glb.extractAppFromUserAgent=n,window.glb.nativeAppInfo=n(),null!==window.glb.nativeAppInfo&&(window.glb.nativeAppInfo.frameworkVersion?(o=parseInt(window.glb.nativeAppInfo.frameworkVersion,10),o>=1&&(document.documentElement.className+=" glb-on-app")):document.documentElement.className+=" glb-on-app-comnio"))},t=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,t||window.addGlbOnAppClass()}).call(this);</script><script>(function(){var n,o,e,i,w,t,d,a,r,s,u,l;try{new CustomEvent("test")}catch(c){e=c,n=function(n,o){var e;return e=void 0,o=o||{bubbles:!1,cancelable:!1,detail:void 0},e=document.createEvent("CustomEvent"),e.initCustomEvent(n,o.bubbles,o.cancelable,o.detail),e},n.prototype=window.Event.prototype,window.CustomEvent=n}r=function(n,o){var e;return null==n||null==o?!1:(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)"),!!n.className.match(e))},o=function(n,o){r(n,o)||(n.className+=" "+o)},u=function(n,o){var e;r(n,o)&&(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)","g"),n.className=n.className.replace(e,""))},w=function(){return window.myInnerWidth||window.innerWidth},i=function(){return window.myInnerHeight||window.innerHeight},t=function(){var n;return null!=window.isAndroidBrowser?window.isAndroidBrowser:(n=navigator.userAgent,window.isAndroidBrowser=n.indexOf("Mozilla/5.0")>-1&&n.indexOf("Android ")>-1&&n.indexOf("AppleWebKit")>-1&&-1===n.indexOf("Chrome"),window.isAndroidBrowser)},d=function(){return null!=window.isPortrait?window.isPortrait:(window.isPortrait=w()<=i(),window.isPortrait)},a=function(){return null!=window.isTouchable?window.isTouchable:(window.isTouchable="ontouchstart"in window||navigator.msMaxTouchPoints||window.DocumentTouch&&document instanceof window.DocumentTouch,window.isTouchable)},s=function(){var n,o;return window.REGUAMAXWIDTHPORTRAIT=window.REGUAMAXWIDTHPORTRAIT||640,window.REGUAMAXWIDTHLANDSCAPE=window.REGUAMAXWIDTHLANDSCAPE||767,window.isPortrait=d(),window.isTouchable=a(),window.isAndroidBrowser=t(),n=w(),o=-1!==window.location.host.indexOf("g1.globo.com"),(window.isPortrait&&n<=window.REGUAMAXWIDTHPORTRAIT||!window.isPortrait&&n<=window.REGUAMAXWIDTHLANDSCAPE)&&!window.isAndroidBrowser&&document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")&&(null==window.glb.nativeAppInfo||window.glb.nativeAppInfo.frameworkVersion)&&!o},window.glb=window.glb||{},window.glb.hasClass=r,window.glb.addClass=o,window.glb.removeClass=u,window.addHasReguaClass=function(){window.glb.reguaShouldStart||(window.glb.reguaShouldStart=s,window.glb.reguaShouldStart()?(window.glb.addClass(document.documentElement,"has-regua"),window.glb.addClass(document.documentElement,"svg-support"),window.glb.removeClass(document.documentElement,"has-not-regua"),document.dispatchEvent(new CustomEvent("glb.regua.exists"))):(window.glb.removeClass(document.documentElement,"has-regua"),window.glb.removeClass(document.documentElement,"svg-support"),window.glb.addClass(document.documentElement,"has-not-regua")))},l=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,l||window.addHasReguaClass()}).call(this);</script><nav id="regua-navegacao" class="regua-navegacao "><div id="regua-svg-container" class="regua-svg-container"><svg xmlns="http://www.w3.org/2000/svg"><symbol id="eixo-icone-busca" viewBox="0 0 24 24"><path d="M21.67 20.27l-5.72-5.73c.98-1.26 1.55-2.83 1.55-4.54 0-4.14-3.36-7.5-7.5-7.5-4.15 0-7.5 3.36-7.5 7.5 0 4.14 3.35 7.5 7.5 7.5 1.7 0 3.27-.57 4.53-1.54l5.73 5.73c.42.41 1.07.43 1.46.04.39-.39.37-1.04-.05-1.46zM10 15.5c-3.04 0-5.5-2.46-5.5-5.5S6.96 4.5 10 4.5c3.03 0 5.5 2.46 5.5 5.5s-2.47 5.5-5.5 5.5z"/></symbol><symbol id="eixo-icone-feed" viewBox="0 0 24 24"><path d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path fill="#FFF" d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path d="M19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1zM17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1z"/><path d="M4 3v4H0v11c0 1.66 1.34 3 3 3h19c1.1 0 2-.9 2-2V3H4zm0 15.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2v9.09zM22 19H6V5h16v14z"/><path fill="#FFF" d="M4 9v9.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2zM6 5v14h16V5H6zm10 10h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1s-.45 1-1 1zm2-4h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1z"/></symbol><symbol id="eixo-icone-menu" viewBox="0 0 24 24"><path d="M22 6c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 12c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 18c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1z"/></symbol><symbol id="eixo-icone-usuario" viewBox="0 0 24 24"><path d="M12 2C8.4 2 5.5 4.9 5.5 8.5c0 3.61 2.9 6.5 6.5 6.5s6.5-2.89 6.5-6.5C18.5 4.9 15.6 2 12 2zm0 11.07c-2.49 0-4.5-2.02-4.5-4.5s2.01-4.5 4.5-4.5 4.5 2.02 4.5 4.5-2.01 4.5-4.5 4.5zM22.893 23c-.733 0-1.074-.455-1.074-.86C21.817 20.26 18.22 19 12 19s-9.818 1.136-9.818 3.14c0 .406-.41.86-1.092.86C.358 23 0 22.513 0 22.108 0 18.958 4.598 17 12 17s12 1.958 12 5.108c0 .405-.398.892-1.107.892z"/></symbol><symbol id="regua-arrow" viewBox="0 0 5 14"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.902 6.64L1.422.386C1.217.017.762-.11.402.104.05.317-.074.79.133 1.16l3.25 5.843L.1 12.847c-.208.368-.085.838.274 1.05.357.212.814.086 1.02-.28l3.412-6.074c.21-.243.26-.606.096-.904z"/></symbol></svg></div><div class="regua-navegacao-container"><ul class="regua-lista regua-lista-home"><li id="regua-navegacao-item-home" class="regua-navegacao-item active"><span data-href="http://www.globo.com/"
                                            data-target="regua-tab-home" class="home-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-feed"/></svg></span></li><li id="regua-navegacao-item-menu" class="regua-navegacao-item "><span data-target="regua-tab-menu" class="menu-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-menu"/></svg></span></li><li id="regua-navegacao-item-busca" class="regua-navegacao-item "><span data-target="regua-tab-busca" class="busca-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-busca"/></svg></span></li><li id="regua-navegacao-item-usuario" class="regua-navegacao-item "><span data-target="regua-tab-usuario" class="usuario-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-usuario"/></svg></span></li></ul></div></nav><nav id="regua-user-container" class="regua-user-container regua-user-home regua-navegacao-tab"><div id="regua-user-controls" class="regua-user-controls"></div><div id="regua-user-tabs" class="regua-tabs regua-user-tab-2"><div id="tab-labels" class="tab-labels"><span class="tab-label tab-label-1" data-index="1">notificaÃ§Ãµes</span><span class="tab-label tab-label-2" data-index="2">minha conta</span></div><div class="regua-tab regua-tab-1" data-index="1"><div class="regua-content"><ul id="regua-user-profile-notifications" class="regua-user-row regua-user-profile-notifications"></ul></div></div><div class="regua-tab regua-tab-2" data-index="2"><div class="regua-content"><ul id="regua-user-profile-list" class="regua-user-row regua-user-profile-list"></ul><div id="regua-user-logout" class="regua-user-logout"></div></div></div></div></nav><div id="regua-tab-busca" class="regua-navegacao-tab regua-tab-busca regua-busca-home"><div class="regua-search-header"><div class="regua-search-box"><form class="regua-search-form" action="http://globo.com/busca/" method="GET"><div class="regua-search-form-container regua-table"><div class="regua-search-input-box regua-table-cell"><input class="regua-search-input" name="q" type="search" placeholder="o que vocÃª procura?" autocorrect="off" autocapitalize="off" autocomplete="off"/></div><div class="regua-search-buttons-container regua-table-cell"><div class="regua-search-clear-container"><a href="#" class="regua-search-clear-button">clear</a></div><div class="search-button-icon"><input type="submit" value="go" class="regua-search-submit"/><div class="regua-icon-block"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></div><a href="#" class="regua-icon-block search-button-go"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></a></div></div></div></form></div></div><div class="regua-container-search-body"><ul class="regua-pre-suggest"></ul><div class="regua-container-suggest"><ul class="regua-suggest"></ul></div><div class="regua-container-results"></div></div></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/bom-dia-brasil/">
                                                <span class="titulo">Bom dia Brasil</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://fantastico.globo.com/">
                                                <span class="titulo">FantÃ¡stico</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://especiais.g1.globo.com/educacao/app-g1-enem/">
                                                    <span class="titulo">Aplicativo G1 Enem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/volei/">
                                                <span class="titulo">VÃ´lei</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-sul-americana/">
                                                    <span class="titulo">Copa Sul-Americana</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                <span class="titulo">The Voice</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/webseries/">
                                                <span class="titulo">WebsÃ©ries</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                    <span class="titulo">The Voice</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/mister-brau/2015/">
                                                    <span class="titulo">Mister Brau</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/pe-na-cova/2015/">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://horoscopo.ego.globo.com/">
                                                <span class="titulo">HorÃ³scopo no Ego</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-10-2012:15:17Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/10/morre-no-rio-atriz-yona-magalhaes.html" class="foto " title="Morre aos 80 a atriz YonÃ¡ MagalhÃ£es (ReproduÃ§Ã£o TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/cWMXHVRdgVg2bjCQH2gEdXDoCPo=/filters:quality(10):strip_icc()/s2.glbimg.com/eMTyugiC7Ma6nMhMDuXR7SONbj0=/193x164:830x374/335x110/g.glbimg.com/og/gs/gsat2/f/original/2015/03/31/14.png" alt="Morre aos 80 a atriz YonÃ¡ MagalhÃ£es (ReproduÃ§Ã£o TV Globo)" title="Morre aos 80 a atriz YonÃ¡ MagalhÃ£es (ReproduÃ§Ã£o TV Globo)"
         data-original-image="s2.glbimg.com/eMTyugiC7Ma6nMhMDuXR7SONbj0=/193x164:830x374/335x110/g.glbimg.com/og/gs/gsat2/f/original/2015/03/31/14.png" data-url-smart_horizontal="pFrCfNBK48HsHnmK34sULyR51wY=/400xorig/smart/filters:strip_icc()/" data-url-smart="pFrCfNBK48HsHnmK34sULyR51wY=/400xorig/smart/filters:strip_icc()/" data-url-feature="pFrCfNBK48HsHnmK34sULyR51wY=/400xorig/smart/filters:strip_icc()/" data-url-tablet="vayXBSQMFg8ytp9nuIymIEJvKmw=/340xorig/smart/filters:strip_icc()/" data-url-desktop="I3MLdKXfF21syL8pT2uHtd3ApRo=/335xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Morre aos 80 a atriz YonÃ¡ MagalhÃ£es</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="&#39;Estava otimista pela recuperaÃ§Ã£o&#39;, diz Arlete Salles" href="http://ego.globo.com/famosos/noticia/2015/10/arlete-salles-sobre-yona-magalhaes-estava-otimista-pela-recuperacao-dela.html">&#39;Estava otimista pela recuperaÃ§Ã£o&#39;, diz Arlete Salles</a></div></li></ul></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/10/meu-governo-nao-esta-envolvido-em-escandalo-de-corrupcao-diz-dilma.html" class=" " title="Dilma diz que governo nÃ£o tem escÃ¢ndalo"><div class="conteudo"><h2>Dilma diz que governo nÃ£o tem escÃ¢ndalo</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/sao-paulo/noticia/2015/10/haddad-exonera-secretario-municipal-de-seguranca-urbana-de-sao-paulo.html" class=" " title="ApÃ³s denÃºncias, secretÃ¡rio de SP Ã© exonerado"><div class="conteudo"><h2>ApÃ³s denÃºncias, secretÃ¡rio de SP Ã© exonerado</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/10/pegacao-na-cama-provoca-ciume-e-desperta-desconfianca.html" class="foto " title="&#39;A Regra&#39;: Mel vÃª &#39;irmÃ£o&#39; com rival (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/4lRT_S5XzcFaLzIZ_zbbVPlYws4=/filters:quality(10):strip_icc()/s2.glbimg.com/nV3ro_1OJ-SEm0OPwPevFrYEBeM=/0x154:690x422/155x60/s.glbimg.com/et/gs/f/original/2015/10/19/janete-nenemzinho.jpg" alt="&#39;A Regra&#39;: Mel vÃª &#39;irmÃ£o&#39; com rival (TV Globo)" title="&#39;A Regra&#39;: Mel vÃª &#39;irmÃ£o&#39; com rival (TV Globo)"
         data-original-image="s2.glbimg.com/nV3ro_1OJ-SEm0OPwPevFrYEBeM=/0x154:690x422/155x60/s.glbimg.com/et/gs/f/original/2015/10/19/janete-nenemzinho.jpg" data-url-smart_horizontal="d3nPysvxR0FKpRycoTcTEU78TUk=/90x56/smart/filters:strip_icc()/" data-url-smart="d3nPysvxR0FKpRycoTcTEU78TUk=/90x56/smart/filters:strip_icc()/" data-url-feature="d3nPysvxR0FKpRycoTcTEU78TUk=/90x56/smart/filters:strip_icc()/" data-url-tablet="0xRNKGqt2wwpuaTOCL8CxsF-XoU=/160xorig/smart/filters:strip_icc()/" data-url-desktop="KTrSP5EG2wBZ-Ux_KP_nbUMCloQ=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;A Regra&#39;: Mel vÃª &#39;irmÃ£o&#39; com rival</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Juliano desmascara" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/10/juliano-vai-descobrir-verdade-e-desmascarar-ze-maria.html">Juliano desmascara</a></div></li><li><div class="mobile-grid-partial"><a title="Linzmeyer fala sobre nu" href="http://extra.globo.com/tv-e-lazer/com-cenas-quentes-em-regra-do-jogo-bruna-linzmeyer-fala-sobre-nu-nao-tenho-medo-nem-vergonha-de-um-corpo-17820903.html">Linzmeyer fala sobre nu</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/livia-se-declara-vitoria-e-minha-avo-e-eu-amo.html" class="foto " title="&#39;AlÃ©m&#39;: LÃ­via se declara a VitÃ³ria (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/ho5gQgF64rjFWkwedZOKqKjJ3ek=/filters:quality(10):strip_icc()/s2.glbimg.com/5oVH9ViPKl4LTcFuQze6Y8xvfmg=/0x226:690x493/155x60/s.glbimg.com/et/gs/f/original/2015/10/19/livia-vitoria.jpg" alt="&#39;AlÃ©m&#39;: LÃ­via se declara a VitÃ³ria (TV Globo)" title="&#39;AlÃ©m&#39;: LÃ­via se declara a VitÃ³ria (TV Globo)"
         data-original-image="s2.glbimg.com/5oVH9ViPKl4LTcFuQze6Y8xvfmg=/0x226:690x493/155x60/s.glbimg.com/et/gs/f/original/2015/10/19/livia-vitoria.jpg" data-url-smart_horizontal="EOIuFLlJluLmtN193Zt-DIHunmI=/90x56/smart/filters:strip_icc()/" data-url-smart="EOIuFLlJluLmtN193Zt-DIHunmI=/90x56/smart/filters:strip_icc()/" data-url-feature="EOIuFLlJluLmtN193Zt-DIHunmI=/90x56/smart/filters:strip_icc()/" data-url-tablet="JPIVFdGUA1iAt0dxK89XVZ7Ln7A=/160xorig/smart/filters:strip_icc()/" data-url-desktop="ex-k3hoXlbokqc9HzevdV3XgcKI=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;AlÃ©m&#39;: LÃ­via se declara a VitÃ³ria</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Afonso e Felipe irmÃ£os" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/10/afonso-sera-irmao-de-felipe-ator-fala-de-nova-fase.html">Afonso e Felipe irmÃ£os</a></div></li><li><div class="mobile-grid-partial"><a title="LÃ­via dÃ¡ tapa em rival" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/livia-perde-cabeca-e-da-tapa-na-cara-de-melissa.html">LÃ­via dÃ¡ tapa em rival</a></div></li></ul></div></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/rio/pizzaria-que-explodiu-nao-passa-por-vistoria-ha-13-anos-17822401" class="foto " title="Local explodido nÃ£o passava por vistoria hÃ¡ anos (Pablo Jacob / O Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/JXh7VbifwO27I0ubi23GrmSjr90=/filters:quality(10):strip_icc()/s2.glbimg.com/i6S2SZ-r3PMzdaWIYTKzwKUvYFY=/50x74:465x408/155x125/s.glbimg.com/en/ho/f/original/2015/10/20/retirada-de-escombros.jpg" alt="Local explodido nÃ£o passava por vistoria hÃ¡ anos (Pablo Jacob / O Globo)" title="Local explodido nÃ£o passava por vistoria hÃ¡ anos (Pablo Jacob / O Globo)"
         data-original-image="s2.glbimg.com/i6S2SZ-r3PMzdaWIYTKzwKUvYFY=/50x74:465x408/155x125/s.glbimg.com/en/ho/f/original/2015/10/20/retirada-de-escombros.jpg" data-url-smart_horizontal="EwG90wfq7zRb4u9LiY5FFEtH5es=/90x56/smart/filters:strip_icc()/" data-url-smart="EwG90wfq7zRb4u9LiY5FFEtH5es=/90x56/smart/filters:strip_icc()/" data-url-feature="EwG90wfq7zRb4u9LiY5FFEtH5es=/90x56/smart/filters:strip_icc()/" data-url-tablet="XgbbuVBIq-SepR2ujc5Ru2M2z7U=/160xorig/smart/filters:strip_icc()/" data-url-desktop="2xh02gp5sXnnh1xY7m4R8YIzKG4=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Local explodido nÃ£o passava por vistoria hÃ¡ anos</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://epocanegocios.globo.com/Informacao/Dilemas/noticia/2015/10/brasileiro-que-morreu-em-voo-para-dublin-tinha-40-mil-libras-em-cocaina-no-estomago-diz-jornal.html" class="foto " title="AutÃ³psia diz que morto em voo levava cocaÃ­na (ReproduÃ§Ã£o/Facebook)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/LQfc7BN6X2oSPnWzcj0kfejag8M=/filters:quality(10):strip_icc()/s2.glbimg.com/7hArKDZs6SS6O4KnGICjuNKd9IM=/57x15:451x333/155x125/e.glbimg.com/og/ed/f/original/2015/10/20/jonh-kennedy-santos-gurjao.jpg" alt="AutÃ³psia diz que morto em voo levava cocaÃ­na (ReproduÃ§Ã£o/Facebook)" title="AutÃ³psia diz que morto em voo levava cocaÃ­na (ReproduÃ§Ã£o/Facebook)"
         data-original-image="s2.glbimg.com/7hArKDZs6SS6O4KnGICjuNKd9IM=/57x15:451x333/155x125/e.glbimg.com/og/ed/f/original/2015/10/20/jonh-kennedy-santos-gurjao.jpg" data-url-smart_horizontal="Y5-AIMMBtLrGL0cHZq9_uq8DaYk=/90x56/smart/filters:strip_icc()/" data-url-smart="Y5-AIMMBtLrGL0cHZq9_uq8DaYk=/90x56/smart/filters:strip_icc()/" data-url-feature="Y5-AIMMBtLrGL0cHZq9_uq8DaYk=/90x56/smart/filters:strip_icc()/" data-url-tablet="qBKNRMKXD55RZN_syHEJDyxE9Go=/160xorig/smart/filters:strip_icc()/" data-url-desktop="HoLQ_udaP_Gn1yXMZO6v6i3KPQg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>AutÃ³psia diz que morto em voo levava cocaÃ­na</h2></div></a></div></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/olimpiadas/cobertura.html" class="foto " title="Rio-2016 jÃ¡ tem ingressos diretos esgotados; SIGA (ReproduÃ§Ã£o)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/s4jQ_3wjy2bWG0pjjStH037HHRI=/filters:quality(10):strip_icc()/s2.glbimg.com/vVMtTx4U41HxWhN0eJiOC71VbqM=/44x314:442x506/155x75/s.glbimg.com/es/ge/f/original/2015/10/20/abertura.jpg" alt="Rio-2016 jÃ¡ tem ingressos diretos esgotados; SIGA (ReproduÃ§Ã£o)" title="Rio-2016 jÃ¡ tem ingressos diretos esgotados; SIGA (ReproduÃ§Ã£o)"
         data-original-image="s2.glbimg.com/vVMtTx4U41HxWhN0eJiOC71VbqM=/44x314:442x506/155x75/s.glbimg.com/es/ge/f/original/2015/10/20/abertura.jpg" data-url-smart_horizontal="bagGM_PVIRUS2hZrba4-JTcHDrs=/90x56/smart/filters:strip_icc()/" data-url-smart="bagGM_PVIRUS2hZrba4-JTcHDrs=/90x56/smart/filters:strip_icc()/" data-url-feature="bagGM_PVIRUS2hZrba4-JTcHDrs=/90x56/smart/filters:strip_icc()/" data-url-tablet="g1fLifG_T4hYg4AZ8c4QYGXTgbY=/160xorig/smart/filters:strip_icc()/" data-url-desktop="WgA3bQZIeABy-WZqVSpwzq5Ev00=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Rio-2016 jÃ¡ tem ingressos diretos esgotados; SIGA</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/blogs/especial-blog/bastidores-fc/post/apesar-do-racha-cbf-convoca-assembleia-para-votar-sul-minas-rio-para-o-dia-27.html" class=" " title="Apesar do racha por Liga, CBF convoca reuniÃ£o (EFE)"><div class="conteudo"><h2>Apesar do racha por Liga, CBF convoca reuniÃ£o</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="&#39;Gravatinhas de rabo preso&#39;, dispara Kalil" href="http://globoesporte.globo.com/blogs/especial-blog/bastidores-fc/post/kalil-ignora-assembleia-e-dispara-contra-ferj-e-cbf-gravatinhas-de-rabo-preso.html">&#39;Gravatinhas de rabo preso&#39;, dispara Kalil</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://extra.globo.com/esporte/flamengo/longe-do-4-flamengo-muda-perfil-de-elenco-para-2016-intensifica-barca-paulinho-cirino-gabriel-negociaveis-17821030.html" class=" " title="Longe do G-4, Fla amplia barca e pÃµe mais nomes"><div class="conteudo"><h2>Longe do G-4, Fla amplia barca e pÃµe  mais nomes</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Vagner Love bate Pato e <br />&#39;atropela&#39; Guerrero" href="http://globoesporte.globo.com/futebol/times/corinthians/noticia/2015/10/criticado-love-tem-tres-vezes-mais-gols-que-guerrero-e-supera-ate-pato.html">Vagner Love bate Pato e 
&#39;atropela&#39; Guerrero</a></div></li></ul></div></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://sportv.globo.com/site/combate/noticia/2015/10/mais-17-lutadores-sao-demitidos-do-ultimate-incluindo-cinco-brasileiros.html" class=" " title="UFC demite 5 lutadores brasileiros (AndrÃ© DurÃ£o / Globoesporte.com)"><div class="conteudo"><h2>UFC demite 5 lutadores brasileiros</h2></div></a></div></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/">encontro com fÃ¡tima</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/cleo-pires-participa-do-ola-lair/4539848/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/cleo-pires-participa-do-ola-lair/4539848/"><div class="image-container"><div class="image-wrapper"><img src="http://s2.glbimg.com/I08PIPDGbAsRXZwqXOWNmIM74l0=/0x0:335x175/335x175/s.glbimg.com/en/ho/f/original/2015/10/20/cleo7.jpg" alt="Cleo Pires planeja &#39;assinar papel&#39; com RÃ´mulo, mas descarta vÃ©u e grinalda" /><div class="time">01:52</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">bastidores com lair</span><span class="title">Cleo Pires planeja &#39;assinar papel&#39; com RÃ´mulo, mas descarta vÃ©u e grinalda</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/tais-araujo-conta-que-gosta-de-acordar-bem-cedo-as-6h/4550375/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/tais-araujo-conta-que-gosta-de-acordar-bem-cedo-as-6h/4550375/"><div class="image-container"><div class="image-wrapper"><img src="http://s2.glbimg.com/z1CFeon8gSngr0CDDolpvC2Mdj8=/0x0:335x175/335x175/s.glbimg.com/en/ho/f/original/2015/10/20/lazaro6.jpg" alt="TaÃ­s AraÃºjo conta que acorda cedo, Ã s 6h, e LÃ¡zaro sÃ³ vai dormir Ã s 2h" /><div class="time">01:38</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">efeitos do sono</span><span class="title">TaÃ­s AraÃºjo conta que acorda cedo, Ã s 6h, e LÃ¡zaro sÃ³ vai dormir Ã s 2h</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/quanto-tempo-o-corpo-leva-para-se-adaptar-ao-horario-de-verao/4550405/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/quanto-tempo-o-corpo-leva-para-se-adaptar-ao-horario-de-verao/4550405/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4550405.jpg" alt="Quanto tempo o corpo leva para se adaptar ao horÃ¡rio de verÃ£o?" /><div class="time">05:14</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">uma hora que faz muita diferenÃ§a</span><span class="title">Quanto tempo o corpo leva para se adaptar ao horÃ¡rio de verÃ£o?</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/cleo-pires-participa-do-ola-lair/4539848/"><span class="subtitle">bastidores com lair</span><span class="title">Cleo Pires planeja &#39;assinar papel&#39; com RÃ´mulo, mas descarta vÃ©u e grinalda</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/tais-araujo-conta-que-gosta-de-acordar-bem-cedo-as-6h/4550375/"><span class="subtitle">efeitos do sono</span><span class="title">TaÃ­s AraÃºjo conta que acorda cedo, Ã s 6h, e LÃ¡zaro sÃ³ vai dormir Ã s 2h</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/quanto-tempo-o-corpo-leva-para-se-adaptar-ao-horario-de-verao/4550405/"><span class="subtitle">uma hora que faz muita diferenÃ§a</span><span class="title">Quanto tempo o corpo leva para se adaptar ao horÃ¡rio de verÃ£o?</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-entretenimento"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/cleo-pires-participa-do-ola-lair/4539848/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/tais-araujo-conta-que-gosta-de-acordar-bem-cedo-as-6h/4550375/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/v/quanto-tempo-o-corpo-leva-para-se-adaptar-ao-horario-de-verao/4550405/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais vÃ­deos <span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/planeta-bizarro/noticia/2015/10/chineses-usam-peixe-enorme-para-preparar-sopa-de-3-toneladas.html" class="foto" title="Chefs preparam sopa de 3 toneladas com peixe gigante iÃ§ado por guindaste (Reuters)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/f3goHIZLMVRLG_6HDb7hbC6ovpY=/filters:quality(10):strip_icc()/s2.glbimg.com/5bEkMZpnuimgRkM2zPjsXWr57SM=/26x27:600x335/335x180/s.glbimg.com/jo/g1/f/original/2015/10/19/peixe-sopa.jpg" alt="Chefs preparam sopa de 3 toneladas com peixe gigante iÃ§ado por guindaste (Reuters)" title="Chefs preparam sopa de 3 toneladas com peixe gigante iÃ§ado por guindaste (Reuters)"
                data-original-image="s2.glbimg.com/5bEkMZpnuimgRkM2zPjsXWr57SM=/26x27:600x335/335x180/s.glbimg.com/jo/g1/f/original/2015/10/19/peixe-sopa.jpg" data-url-smart_horizontal="OWgtDbJN3AzaFZHHIX89JL_WOhs=/90x0/smart/filters:strip_icc()/" data-url-smart="OWgtDbJN3AzaFZHHIX89JL_WOhs=/90x0/smart/filters:strip_icc()/" data-url-feature="OWgtDbJN3AzaFZHHIX89JL_WOhs=/90x0/smart/filters:strip_icc()/" data-url-tablet="W2Y_8EkBoUuy7M0CLpcd9sRZEjE=/220x125/smart/filters:strip_icc()/" data-url-desktop="M-jJbI4DHUlnWbWhtggpbJ3ZuW8=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Chefs preparam sopa de 3 toneladas com peixe gigante iÃ§ado por guindaste</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 08:41:04" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pi/piaui/noticia/2015/10/nao-existe-justica-para-quem-tem-dinheiro-diz-filho-de-operario-morto.html" class="foto" title="Filho de funcionÃ¡rio morto em ciclofaixa se revolta ao saber que jovem serÃ¡ solta (Luiz ClÃ¡udio Barbosa/CÃ³digo 19/EstadÃ£o ConteÃºdo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/iW8qnGtMR8DQzy5Js39WJWpEr0E=/filters:quality(10):strip_icc()/s2.glbimg.com/mUGs_-IDqhNy6cMaMzv2Q-tbJrg=/221x46:510x239/120x80/s.glbimg.com/jo/g1/f/original/2015/10/19/atropelamento-ciclovia-sp_luiz_claudio_barbosa_estadao_conteudo.jpg" alt="Filho de funcionÃ¡rio morto em ciclofaixa se revolta ao saber que jovem serÃ¡ solta (Luiz ClÃ¡udio Barbosa/CÃ³digo 19/EstadÃ£o ConteÃºdo)" title="Filho de funcionÃ¡rio morto em ciclofaixa se revolta ao saber que jovem serÃ¡ solta (Luiz ClÃ¡udio Barbosa/CÃ³digo 19/EstadÃ£o ConteÃºdo)"
                data-original-image="s2.glbimg.com/mUGs_-IDqhNy6cMaMzv2Q-tbJrg=/221x46:510x239/120x80/s.glbimg.com/jo/g1/f/original/2015/10/19/atropelamento-ciclovia-sp_luiz_claudio_barbosa_estadao_conteudo.jpg" data-url-smart_horizontal="uCoMEug5-auSqJaaixAv9DvxYTk=/90x0/smart/filters:strip_icc()/" data-url-smart="uCoMEug5-auSqJaaixAv9DvxYTk=/90x0/smart/filters:strip_icc()/" data-url-feature="uCoMEug5-auSqJaaixAv9DvxYTk=/90x0/smart/filters:strip_icc()/" data-url-tablet="lLrpFDO1HNc-nkIKB1JhynT8gNE=/70x50/smart/filters:strip_icc()/" data-url-desktop="go8MN5K2W2bYbZbqlOX9IdR9PrE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Filho de funcionÃ¡rio  morto em ciclofaixa se revolta ao saber que jovem serÃ¡ solta</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 08:23:14" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/casos-de-policia/parece-que-minha-vida-parou-desabafa-jornalista-estuprada-torturada-por-sogra-dos-filhos-namorado-dela-17821337.html" class="foto" title="Jornalista Ã© estuprada e torturada por sogra dos filhos e namorado no Rio (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/aKS_k3ScUDZdQlJKxLYwyz9jfPA=/filters:quality(10):strip_icc()/s2.glbimg.com/u_xOswDV7aJPcTjJDcohnb4yWic=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/10/20/jornalista3.jpg" alt="Jornalista Ã© estuprada e torturada por sogra dos filhos e namorado no Rio (ReproduÃ§Ã£o)" title="Jornalista Ã© estuprada e torturada por sogra dos filhos e namorado no Rio (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/u_xOswDV7aJPcTjJDcohnb4yWic=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/10/20/jornalista3.jpg" data-url-smart_horizontal="Re-4hBtVT5hHlsGQPi8V8rClaKE=/90x0/smart/filters:strip_icc()/" data-url-smart="Re-4hBtVT5hHlsGQPi8V8rClaKE=/90x0/smart/filters:strip_icc()/" data-url-feature="Re-4hBtVT5hHlsGQPi8V8rClaKE=/90x0/smart/filters:strip_icc()/" data-url-tablet="LVBcTzBOjJgy1_qYJ5hiUqGQqO4=/70x50/smart/filters:strip_icc()/" data-url-desktop="InAh9iKtzxdbG7HvD96O2u3zQcc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jornalista Ã© estuprada e torturada por sogra dos filhos e namorado no Rio</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 11:15:32" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/mundo/noticia/2015/10/numero-de-mortos-na-peregrinacao-meca-se-aproxima-de-2-mil.html" class="" title="BalanÃ§o feito por 34 paÃ­ses diz que mortos na peregrinaÃ§Ã£o a Meca se aproximam de 2 mil (Guilherme Ferrari/ A Gazeta)" rel="bookmark"><span class="conteudo"><p>BalanÃ§o feito por 34 paÃ­ses diz que mortos na peregrinaÃ§Ã£o a Meca se aproximam de 2 mil</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/ma/maranhao/noticia/2015/10/promotor-denuncia-caso-de-canibalismo-no-presidio-de-pedrinhas.html" class="foto" title="Promotor denuncia que presos comeram fÃ­gado de detento em Pedrinhas, MA (MÃ¡rcio Fernandes/AE)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/aXp8ejJ5Hk1kF7Iharws0eIH7Yg=/filters:quality(10):strip_icc()/s2.glbimg.com/G2oVfDaqdQtsl4GI9u264rWuq_o=/174x121:457x310/120x80/e.glbimg.com/og/ed/f/original/2015/07/17/pedrinhas1.jpg" alt="Promotor denuncia que presos comeram fÃ­gado de detento em Pedrinhas, MA (MÃ¡rcio Fernandes/AE)" title="Promotor denuncia que presos comeram fÃ­gado de detento em Pedrinhas, MA (MÃ¡rcio Fernandes/AE)"
                data-original-image="s2.glbimg.com/G2oVfDaqdQtsl4GI9u264rWuq_o=/174x121:457x310/120x80/e.glbimg.com/og/ed/f/original/2015/07/17/pedrinhas1.jpg" data-url-smart_horizontal="lc41w-zOl7pZkMhwMe06hPv98os=/90x0/smart/filters:strip_icc()/" data-url-smart="lc41w-zOl7pZkMhwMe06hPv98os=/90x0/smart/filters:strip_icc()/" data-url-feature="lc41w-zOl7pZkMhwMe06hPv98os=/90x0/smart/filters:strip_icc()/" data-url-tablet="byNP4fyaIRSbSkeiISU1Y5Av2-E=/70x50/smart/filters:strip_icc()/" data-url-desktop="xcGSSoe2ax8BGNEdpiOSgxVlcPU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Promotor denuncia que presos comeram fÃ­gado de detento em Pedrinhas, MA</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 11:03:21" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/10/cameras-dos-smartphones-tem-funcoes-secretas-confira-lista.html" class="foto" title="Veja as funÃ§Ãµes &#39;secretas&#39; 
que a cÃ¢mera do seu smart tem e vocÃª nÃ£o imaginava (TechTudo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/yyHCkjkiQcn-fEKr9nFjHxRqMdE=/filters:quality(10):strip_icc()/s2.glbimg.com/5p898t2Md7rxNXm1jvjKPxMxTbA=/1x0:645x430/120x80/s.glbimg.com/po/tt2/f/original/2015/10/20/img_0171.jpg" alt="Veja as funÃ§Ãµes &#39;secretas&#39; 
que a cÃ¢mera do seu smart tem e vocÃª nÃ£o imaginava (TechTudo)" title="Veja as funÃ§Ãµes &#39;secretas&#39; 
que a cÃ¢mera do seu smart tem e vocÃª nÃ£o imaginava (TechTudo)"
                data-original-image="s2.glbimg.com/5p898t2Md7rxNXm1jvjKPxMxTbA=/1x0:645x430/120x80/s.glbimg.com/po/tt2/f/original/2015/10/20/img_0171.jpg" data-url-smart_horizontal="NVTEA_28yAHgu8OGPXFdu7BRJfQ=/90x0/smart/filters:strip_icc()/" data-url-smart="NVTEA_28yAHgu8OGPXFdu7BRJfQ=/90x0/smart/filters:strip_icc()/" data-url-feature="NVTEA_28yAHgu8OGPXFdu7BRJfQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="mqgRhobcATD_SL76MyXrs9agWa4=/70x50/smart/filters:strip_icc()/" data-url-desktop="sUvCWecXdCTP_OxlspJAdTR4IgA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Veja as funÃ§Ãµes &#39;secretas&#39; <br />que a cÃ¢mera do seu smart <br />tem e vocÃª nÃ£o imaginava</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/economia/negocios/noticia/2015/10/deutsche-bank-transfere-us-6-bi-acidentalmente-um-unico-cliente.html" class="" title="Maior banco alemÃ£o transfere US$ 6 bi a um Ãºnico cliente; erro foi de funcionÃ¡rio jÃºnior (ReproduÃ§Ã£o TV Globo)" rel="bookmark"><span class="conteudo"><p>Maior banco alemÃ£o transfere US$ 6 bi a um Ãºnico cliente; erro foi de funcionÃ¡rio jÃºnior</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 11:21:41" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pernambuco/noticia/2015/10/estrangeiros-sao-presos-no-recife-apos-explosao-de-veleiro.html" class="foto" title="Estrangeiros sÃ£o suspeitos de sabotar e afundar veleiro para evitar flagra (DivulgaÃ§Ã£o/ PolÃ­cia Federal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/JpN9i2ya5nSSJgOhVsIm-0gBNVg=/filters:quality(10):strip_icc()/s2.glbimg.com/LozWn2K7bGBxJNU_lxtiBKmWoF0=/36x95:461x379/120x80/s.glbimg.com/jo/g1/f/original/2015/10/20/pf.jpg" alt="Estrangeiros sÃ£o suspeitos de sabotar e afundar veleiro para evitar flagra (DivulgaÃ§Ã£o/ PolÃ­cia Federal)" title="Estrangeiros sÃ£o suspeitos de sabotar e afundar veleiro para evitar flagra (DivulgaÃ§Ã£o/ PolÃ­cia Federal)"
                data-original-image="s2.glbimg.com/LozWn2K7bGBxJNU_lxtiBKmWoF0=/36x95:461x379/120x80/s.glbimg.com/jo/g1/f/original/2015/10/20/pf.jpg" data-url-smart_horizontal="Y9qVS-pggsoAwfqugAM0oHRKzaA=/90x0/smart/filters:strip_icc()/" data-url-smart="Y9qVS-pggsoAwfqugAM0oHRKzaA=/90x0/smart/filters:strip_icc()/" data-url-feature="Y9qVS-pggsoAwfqugAM0oHRKzaA=/90x0/smart/filters:strip_icc()/" data-url-tablet="gRNgA3PXinO-ecIjCHMglOPW0F4=/70x50/smart/filters:strip_icc()/" data-url-desktop="zi3VcW75YbMRLPVIjZUnT5_zfDw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Estrangeiros sÃ£o suspeitos de sabotar e afundar veleiro para evitar flagra</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry publieditorial"><div data-photo-subtitle="2015-10-19 10:32:12" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/carros/especial-publicitario/dunlop/giro-de-noticias-dunlop/noticia/2015/10/teste-seus-conhecimentos-sobre-o-kit-obrigatorio-do-carro.html?utm_source=home-globocom&amp;utm_medium=fakebanner&amp;utm_term=15-10-20&amp;utm_content=dunlop&amp;utm_campaign=giro-de-noticias" class="foto" title="ObrigatÃ³rio, kit do carro evita multa de quase R$ 200; saiba usÃ¡-lo (DivulgaÃ§Ã£o Dunlop)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/RL6x6dtjcI_cXmvzpDdglNkybTY=/filters:quality(10):strip_icc()/s2.glbimg.com/xyBcIGuiBqkdIkUjqJHZtFP2-P8=/0x0:3857x2572/120x80/s.glbimg.com/jo/g1/f/original/2015/10/19/dunlop_kit.jpg" alt="ObrigatÃ³rio, kit do carro evita multa de quase R$ 200; saiba usÃ¡-lo (DivulgaÃ§Ã£o Dunlop)" title="ObrigatÃ³rio, kit do carro evita multa de quase R$ 200; saiba usÃ¡-lo (DivulgaÃ§Ã£o Dunlop)"
                data-original-image="s2.glbimg.com/xyBcIGuiBqkdIkUjqJHZtFP2-P8=/0x0:3857x2572/120x80/s.glbimg.com/jo/g1/f/original/2015/10/19/dunlop_kit.jpg" data-url-smart_horizontal="MxDMnlQ01hX6S0EuKgE2Bn9J6Ek=/90x0/smart/filters:strip_icc()/" data-url-smart="MxDMnlQ01hX6S0EuKgE2Bn9J6Ek=/90x0/smart/filters:strip_icc()/" data-url-feature="MxDMnlQ01hX6S0EuKgE2Bn9J6Ek=/90x0/smart/filters:strip_icc()/" data-url-tablet="B4CONZZ_6v0Sxgp3LefPQVuEcf0=/70x50/smart/filters:strip_icc()/" data-url-desktop="DVKvsAKGfCqMEIHy1o9YcZ4yesw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><span>ESPECIAL PUBLICITÃRIO</span><p>ObrigatÃ³rio, kit do carro<br /> evita multa de quase <br />R$ 200; saiba usÃ¡-lo</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/sp/campinas-e-regiao/futebol/noticia/2015/10/filho-de-amoroso-ganha-destaque-no-udinese-e-sonha-com-selecao-italiana.html" class="foto" title="Preterido em duas Copas, Amoroso vÃª o filho de olho em vaga na Azzurra (Arquivo pessoal)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/imjFI3mmUM4addt3bC8GSh05_Xk=/filters:quality(10):strip_icc()/s2.glbimg.com/ov15WRzj8S4aG-DvjiZrzxvAP98=/85x142:767x509/335x180/s.glbimg.com/es/ge/f/original/2015/10/19/amoroso5.jpg" alt="Preterido em duas Copas, Amoroso vÃª o filho de olho em vaga na Azzurra (Arquivo pessoal)" title="Preterido em duas Copas, Amoroso vÃª o filho de olho em vaga na Azzurra (Arquivo pessoal)"
                data-original-image="s2.glbimg.com/ov15WRzj8S4aG-DvjiZrzxvAP98=/85x142:767x509/335x180/s.glbimg.com/es/ge/f/original/2015/10/19/amoroso5.jpg" data-url-smart_horizontal="phB_CmZJG3dNbaJv0n1bi8ctgvI=/90x0/smart/filters:strip_icc()/" data-url-smart="phB_CmZJG3dNbaJv0n1bi8ctgvI=/90x0/smart/filters:strip_icc()/" data-url-feature="phB_CmZJG3dNbaJv0n1bi8ctgvI=/90x0/smart/filters:strip_icc()/" data-url-tablet="XGOIMc1thpBXXuiJCGPAEHw8b98=/220x125/smart/filters:strip_icc()/" data-url-desktop="Ob07raRKgBJZA4q2WRGk4mfvopU=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Preterido em duas Copas, Amoroso <br />vÃª o filho de olho em vaga na Azzurra</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/vasco/noticia/2015/10/em-dois-meses-de-vasco-jorginho-supera-pontuacao-de-roth-e-doriva.html" class="foto" title="Em dois meses de Vasco, Jorginho tem mais pontos que Roth e Doriva juntos (Miguel Schincariol)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/7dAD9XQ6qRM8aH8K8yQAmMz5488=/filters:quality(10):strip_icc()/s2.glbimg.com/gVvSJia0Jqed5vwkd2aFdV3BZDs=/869x172:1679x712/120x80/s.glbimg.com/es/ge/f/original/2015/10/18/jorginho.jpg" alt="Em dois meses de Vasco, Jorginho tem mais pontos que Roth e Doriva juntos (Miguel Schincariol)" title="Em dois meses de Vasco, Jorginho tem mais pontos que Roth e Doriva juntos (Miguel Schincariol)"
                data-original-image="s2.glbimg.com/gVvSJia0Jqed5vwkd2aFdV3BZDs=/869x172:1679x712/120x80/s.glbimg.com/es/ge/f/original/2015/10/18/jorginho.jpg" data-url-smart_horizontal="g6Jq85QtxIEyNOk-3MYg0LDQl7k=/90x0/smart/filters:strip_icc()/" data-url-smart="g6Jq85QtxIEyNOk-3MYg0LDQl7k=/90x0/smart/filters:strip_icc()/" data-url-feature="g6Jq85QtxIEyNOk-3MYg0LDQl7k=/90x0/smart/filters:strip_icc()/" data-url-tablet="VitFi-Ocajqu5jQ0IXFP85mvAvE=/70x50/smart/filters:strip_icc()/" data-url-desktop="2gqFo0kfIpE3QVh7yk-TS9sCjPQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Em dois meses de Vasco, Jorginho tem mais pontos que Roth e Doriva juntos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/10/doriva-e-o-4-tecnico-do-sao-paulo-contra-o-santos-no-ano-so-um-venceu.html" class="foto" title="Doriva Ã© o 4Âº tÃ©cnico do SÃ£o Paulo contra o Santos no ano; sÃ³ um venceu (Miguel Schincariol)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/iBPAidC0tA0BtPLQ2-j5h0BaaQc=/filters:quality(10):strip_icc()/s2.glbimg.com/T5ryneEYygruOHgLjyYMzjMtu6s=/77x237:1113x927/120x80/s.glbimg.com/es/ge/f/original/2015/10/18/mig8704.jpg" alt="Doriva Ã© o 4Âº tÃ©cnico do SÃ£o Paulo contra o Santos no ano; sÃ³ um venceu (Miguel Schincariol)" title="Doriva Ã© o 4Âº tÃ©cnico do SÃ£o Paulo contra o Santos no ano; sÃ³ um venceu (Miguel Schincariol)"
                data-original-image="s2.glbimg.com/T5ryneEYygruOHgLjyYMzjMtu6s=/77x237:1113x927/120x80/s.glbimg.com/es/ge/f/original/2015/10/18/mig8704.jpg" data-url-smart_horizontal="t4tfcnwxa-Utx66nRlSxq0Z0T74=/90x0/smart/filters:strip_icc()/" data-url-smart="t4tfcnwxa-Utx66nRlSxq0Z0T74=/90x0/smart/filters:strip_icc()/" data-url-feature="t4tfcnwxa-Utx66nRlSxq0Z0T74=/90x0/smart/filters:strip_icc()/" data-url-tablet="lIHAUxglxXjsg5ylZUFfjUp0H4g=/70x50/smart/filters:strip_icc()/" data-url-desktop="JPmqfajm7TYh2z3dX-LTQd4eP8A=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Doriva Ã© o 4Âº tÃ©cnico do SÃ£o Paulo contra o Santos no ano; sÃ³ um venceu</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 10:24:58" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/palmeiras/noticia/2015/10/palmeiras-aposta-em-dna-ofensivo-para-abrir-vantagem-contra-o-flu.html" class="" title="Fora, VerdÃ£o aposta em DNA ofensivo para encaminhar vaga na final da Copa do Brasil (Miguel Schincariol)" rel="bookmark"><span class="conteudo"><p>Fora, VerdÃ£o aposta em DNA ofensivo para encaminhar vaga na final da Copa do Brasil</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 10:13:54" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/atletico-mg/noticia/2015/10/nada-e-impossivel-sete-motivos-para-o-atleticano-manter-o-eu-acredito.html" class="foto" title="Final com TimÃ£o e volta de Luan estÃ£o entre motivos para o &#39;Eu (ainda) acredito&#39; (GloboEsporte.com)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/5gJRjBf0E9mY7yzSptEKJtVH-PQ=/filters:quality(10):strip_icc()/s2.glbimg.com/07ByLa-fdtG8jNFdD18eACnrX2M=/23x162:365x390/120x80/s.glbimg.com/es/ge/f/original/2015/10/19/15_10_carrossel_atleticomg_euacredito.jpg" alt="Final com TimÃ£o e volta de Luan estÃ£o entre motivos para o &#39;Eu (ainda) acredito&#39; (GloboEsporte.com)" title="Final com TimÃ£o e volta de Luan estÃ£o entre motivos para o &#39;Eu (ainda) acredito&#39; (GloboEsporte.com)"
                data-original-image="s2.glbimg.com/07ByLa-fdtG8jNFdD18eACnrX2M=/23x162:365x390/120x80/s.glbimg.com/es/ge/f/original/2015/10/19/15_10_carrossel_atleticomg_euacredito.jpg" data-url-smart_horizontal="qxfk-zMzyxAeKUo8RJ8KEkeSucs=/90x0/smart/filters:strip_icc()/" data-url-smart="qxfk-zMzyxAeKUo8RJ8KEkeSucs=/90x0/smart/filters:strip_icc()/" data-url-feature="qxfk-zMzyxAeKUo8RJ8KEkeSucs=/90x0/smart/filters:strip_icc()/" data-url-tablet="a4qk7iubTb_u4Z8WUnB3Tp_uZJM=/70x50/smart/filters:strip_icc()/" data-url-desktop="8OtJqCKrAmoqkapZ9VoRaz2hZqo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Final com TimÃ£o e volta de Luan estÃ£o entre motivos <br />para o &#39;Eu (ainda) acredito&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 10:08:05" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/atletico-mg/noticia/2015/10/nada-e-impossivel-sete-motivos-para-o-atleticano-manter-o-eu-acredito.html" class="foto" title="&#39;Like&#39; de Hazard em post sobre Real esquenta boatos de negociaÃ§Ã£o (ReproduÃ§Ã£o / Facebook)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/iGq5oTXqj8fddX_5Fil1FQz-w4I=/filters:quality(10):strip_icc()/s2.glbimg.com/G8mhQEiYB-6Vdva2ckIWLvXPgX8=/879x160:1397x505/120x80/s.glbimg.com/es/ge/f/original/2015/10/19/hazard_treino_chelsea.jpg" alt="&#39;Like&#39; de Hazard em post sobre Real esquenta boatos de negociaÃ§Ã£o (ReproduÃ§Ã£o / Facebook)" title="&#39;Like&#39; de Hazard em post sobre Real esquenta boatos de negociaÃ§Ã£o (ReproduÃ§Ã£o / Facebook)"
                data-original-image="s2.glbimg.com/G8mhQEiYB-6Vdva2ckIWLvXPgX8=/879x160:1397x505/120x80/s.glbimg.com/es/ge/f/original/2015/10/19/hazard_treino_chelsea.jpg" data-url-smart_horizontal="BHCYPp9Pb7TwSSInXQ53o1KAF8U=/90x0/smart/filters:strip_icc()/" data-url-smart="BHCYPp9Pb7TwSSInXQ53o1KAF8U=/90x0/smart/filters:strip_icc()/" data-url-feature="BHCYPp9Pb7TwSSInXQ53o1KAF8U=/90x0/smart/filters:strip_icc()/" data-url-tablet="1742aWYzKOSzldVf4o5JRQvNrMQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="YL3QyifnE9b2uhIhfdjBk44rxzA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>&#39;Like&#39; de Hazard em post sobre Real esquenta boatos de negociaÃ§Ã£o</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 10:31:24" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/romario-compra-parcela-mansao-de-7-milhoes-que-foi-de-edmundo-no-rio-17822354.html" class="" title="RomÃ¡rio compra mansÃ£o que pertenceu a Edmundo; valor de R$ 7 mi foi parcelado (ReproduÃ§Ã£o/MÃ¡laga CF)" rel="bookmark"><span class="conteudo"><p>RomÃ¡rio compra mansÃ£o que pertenceu a Edmundo; valor de R$ 7 mi foi parcelado</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 10:28:49" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/retratos-da-bola/pai-de-ronaldo-fenomeno-nelio-nazario-preocupa-amigos-familiares-por-causa-da-saude-17821988.html" class="foto" title="Inconformado com nova rotina apÃ³s cirurgia, pai de Ronaldo preocupa famÃ­lia (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/7bHw8XaTzq6zaUKgqCyqDy2Plyk=/filters:quality(10):strip_icc()/s2.glbimg.com/BLr9VvQMGKuDUEkXrAP_sTYwCrw=/0x82:448x381/120x80/s.glbimg.com/en/ho/f/original/2015/10/20/nelio.jpg" alt="Inconformado com nova rotina apÃ³s cirurgia, pai de Ronaldo preocupa famÃ­lia (ReproduÃ§Ã£o)" title="Inconformado com nova rotina apÃ³s cirurgia, pai de Ronaldo preocupa famÃ­lia (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/BLr9VvQMGKuDUEkXrAP_sTYwCrw=/0x82:448x381/120x80/s.glbimg.com/en/ho/f/original/2015/10/20/nelio.jpg" data-url-smart_horizontal="5nUcX2NJrEaY1Qs7faJJZKSDzGg=/90x0/smart/filters:strip_icc()/" data-url-smart="5nUcX2NJrEaY1Qs7faJJZKSDzGg=/90x0/smart/filters:strip_icc()/" data-url-feature="5nUcX2NJrEaY1Qs7faJJZKSDzGg=/90x0/smart/filters:strip_icc()/" data-url-tablet="82XkAzP2D3x5GG5PGFOFUkCI79A=/70x50/smart/filters:strip_icc()/" data-url-desktop="mhaGL1ea2u-bV6TO-GnOW5IoR5c=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Inconformado com nova rotina apÃ³s cirurgia, pai de Ronaldo preocupa famÃ­lia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 09:29:57" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/noticia/2015/10/sage-me-lembra-muito-st-pierre-diz-treinador-do-ex-campeao-do-ultimate.html" class="foto" title="TÃ©cnico de mito do UFC enche Sage de elogios: &#39;Me lembra muito St-Pierre&#39; (Gettymages)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/H5W84SQ2nwY-LX0GO3R0RJCuJTY=/filters:quality(10):strip_icc()/s2.glbimg.com/jIQJ9hhXjSOsVBQR7icguv2Ulic=/0x73:244x236/120x80/s.glbimg.com/es/ge/f/original/2015/10/13/montagem_sage-northcut_e_kevin-lee.jpg" alt="TÃ©cnico de mito do UFC enche Sage de elogios: &#39;Me lembra muito St-Pierre&#39; (Gettymages)" title="TÃ©cnico de mito do UFC enche Sage de elogios: &#39;Me lembra muito St-Pierre&#39; (Gettymages)"
                data-original-image="s2.glbimg.com/jIQJ9hhXjSOsVBQR7icguv2Ulic=/0x73:244x236/120x80/s.glbimg.com/es/ge/f/original/2015/10/13/montagem_sage-northcut_e_kevin-lee.jpg" data-url-smart_horizontal="kA7j0W1wuujiAxEvEwLDe9kuSlA=/90x0/smart/filters:strip_icc()/" data-url-smart="kA7j0W1wuujiAxEvEwLDe9kuSlA=/90x0/smart/filters:strip_icc()/" data-url-feature="kA7j0W1wuujiAxEvEwLDe9kuSlA=/90x0/smart/filters:strip_icc()/" data-url-tablet="zw4sJuMalYrLDofD8uYgOjLo1zo=/70x50/smart/filters:strip_icc()/" data-url-desktop="YtnzOdW8IV_4pdBUCFpKFFr4YYo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>TÃ©cnico de mito do UFC enche Sage de elogios: &#39;Me lembra muito St-Pierre&#39;</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/carnaval/2016/noticia/2015/10/cacau-colucci-faz-danca-do-ventre-para-soltar-mais-o-quadril-no-carnaval.html" class="foto" title="Dona de corpÃ£o, Cacau Colucci aposta na danÃ§a do ventre para se soltar mais (Iwi Onodera / EGO)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/B1SmQY_RpBgAIUTdDDk-q8w-ow8=/filters:quality(10):strip_icc()/s2.glbimg.com/kKIpnnZPfr_1GRtpePE7zh_NoJw=/25x98:620x418/335x180/s.glbimg.com/jo/eg/f/original/2015/10/14/cacau08-grande.jpg" alt="Dona de corpÃ£o, Cacau Colucci aposta na danÃ§a do ventre para se soltar mais (Iwi Onodera / EGO)" title="Dona de corpÃ£o, Cacau Colucci aposta na danÃ§a do ventre para se soltar mais (Iwi Onodera / EGO)"
                data-original-image="s2.glbimg.com/kKIpnnZPfr_1GRtpePE7zh_NoJw=/25x98:620x418/335x180/s.glbimg.com/jo/eg/f/original/2015/10/14/cacau08-grande.jpg" data-url-smart_horizontal="6ndVAFfGyI2Sl4wu1JiFkW1U1A8=/90x0/smart/filters:strip_icc()/" data-url-smart="6ndVAFfGyI2Sl4wu1JiFkW1U1A8=/90x0/smart/filters:strip_icc()/" data-url-feature="6ndVAFfGyI2Sl4wu1JiFkW1U1A8=/90x0/smart/filters:strip_icc()/" data-url-tablet="GxzN1LT9GRVAxueD-0PfmCFvwY4=/220x125/smart/filters:strip_icc()/" data-url-desktop="JQ62bszaScydydPYA4PlAg5-DlE=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Dona de corpÃ£o, Cacau Colucci aposta na danÃ§a do ventre para se soltar mais</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 12:22:11" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/10/fernanda-keulla-nega-anorexia-nunca-estive-tao-saudavel.html" class="foto" title="Ex-BBB Fernanda rebate crÃ­ticas Ã  magreza em foto: &#39;Nunca estive tÃ£o saudÃ¡vel&#39; (Instagram / ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/gHzHEV0HjPnHOMIuvH13oTR-Dnw=/filters:quality(10):strip_icc()/s2.glbimg.com/rLld6YLOGSVNnl_fMx0YAwRhOMQ=/307x172:857x539/120x80/s.glbimg.com/jo/eg/f/original/2015/10/19/12107433_816131298504832_1209073509_n.jpg" alt="Ex-BBB Fernanda rebate crÃ­ticas Ã  magreza em foto: &#39;Nunca estive tÃ£o saudÃ¡vel&#39; (Instagram / ReproduÃ§Ã£o)" title="Ex-BBB Fernanda rebate crÃ­ticas Ã  magreza em foto: &#39;Nunca estive tÃ£o saudÃ¡vel&#39; (Instagram / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/rLld6YLOGSVNnl_fMx0YAwRhOMQ=/307x172:857x539/120x80/s.glbimg.com/jo/eg/f/original/2015/10/19/12107433_816131298504832_1209073509_n.jpg" data-url-smart_horizontal="EQveDjOx89IY_3ElqpTO9niiY64=/90x0/smart/filters:strip_icc()/" data-url-smart="EQveDjOx89IY_3ElqpTO9niiY64=/90x0/smart/filters:strip_icc()/" data-url-feature="EQveDjOx89IY_3ElqpTO9niiY64=/90x0/smart/filters:strip_icc()/" data-url-tablet="5gv2nz0iWPPQPDxR_V6V7ToT_3A=/70x50/smart/filters:strip_icc()/" data-url-desktop="XdzrHSGA95RXk1JErc6-pYQ7PLk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ex-BBB Fernanda rebate crÃ­ticas Ã  magreza em foto: &#39;Nunca estive tÃ£o saudÃ¡vel&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://kogut.oglobo.globo.com/noticias-da-tv/noticia/2015/10/ex-malhacao-anna-rita-cerqueira-conta-que-perdeu-papel-em-verdades-secretas-por-causa-da-idade.html" class="foto" title="Ex-&#39;MalhaÃ§Ã£o&#39; conta que ficou fora de &#39;Verdades&#39; pela idade: &#39;Complicado&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/qg7zEEo2QYfZIHHDVUm_4R2b6HE=/filters:quality(10):strip_icc()/s2.glbimg.com/_Q0HQM6z8JB_6Rd8JP7AM2tgHU8=/120x31:535x308/120x80/i.glbimg.com/og/ig/infoglobo/f/original/2015/10/14/annarita.jpg" alt="Ex-&#39;MalhaÃ§Ã£o&#39; conta que ficou fora de &#39;Verdades&#39; pela idade: &#39;Complicado&#39; (ReproduÃ§Ã£o)" title="Ex-&#39;MalhaÃ§Ã£o&#39; conta que ficou fora de &#39;Verdades&#39; pela idade: &#39;Complicado&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/_Q0HQM6z8JB_6Rd8JP7AM2tgHU8=/120x31:535x308/120x80/i.glbimg.com/og/ig/infoglobo/f/original/2015/10/14/annarita.jpg" data-url-smart_horizontal="rCWs6roePQdgPOLV_K1fuqTJ6-4=/90x0/smart/filters:strip_icc()/" data-url-smart="rCWs6roePQdgPOLV_K1fuqTJ6-4=/90x0/smart/filters:strip_icc()/" data-url-feature="rCWs6roePQdgPOLV_K1fuqTJ6-4=/90x0/smart/filters:strip_icc()/" data-url-tablet="s2hsHsbzLoCs3UwFsrKAvzfgyxs=/70x50/smart/filters:strip_icc()/" data-url-desktop="sdlkbiA02n46X3B_AKK2xBkrEr4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ex-&#39;MalhaÃ§Ã£o&#39; conta que ficou fora de &#39;Verdades&#39; pela idade: &#39;Complicado&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 11:50:20" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/apos-crise-renal-alinne-moraes-recebe-alta-se-recupera-em-casa-17822873.html" class="" title="ApÃ³s ficar internada com crise renal, Alinne Moraes recebe alta e se recupera em casa (TV Globo/DivulgaÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>ApÃ³s ficar internada com crise renal, Alinne Moraes recebe alta e se recupera em casa</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 10:54:15" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/Estilo/noticia/2015/10/juliana-paes-esbanja-estilo-na-coletiva-de-totalmente-demais.html" class="foto" title="Atrizes esbanjam estilo no lanÃ§amento de nova novela das 19h; siga aqui (Raphael Dias/Gshow)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/543wmk8Vo7Y5bwjEtvEswduYK8w=/filters:quality(10):strip_icc()/s2.glbimg.com/BQ6kmKcssHRHGRjg0MrRDFPT5lU=/136x86:507x333/120x80/s.glbimg.com/et/gs/f/original/2015/10/20/rd_9125.jpg" alt="Atrizes esbanjam estilo no lanÃ§amento de nova novela das 19h; siga aqui (Raphael Dias/Gshow)" title="Atrizes esbanjam estilo no lanÃ§amento de nova novela das 19h; siga aqui (Raphael Dias/Gshow)"
                data-original-image="s2.glbimg.com/BQ6kmKcssHRHGRjg0MrRDFPT5lU=/136x86:507x333/120x80/s.glbimg.com/et/gs/f/original/2015/10/20/rd_9125.jpg" data-url-smart_horizontal="dUFwp-Bj14LMGjQrc3LtottVj_o=/90x0/smart/filters:strip_icc()/" data-url-smart="dUFwp-Bj14LMGjQrc3LtottVj_o=/90x0/smart/filters:strip_icc()/" data-url-feature="dUFwp-Bj14LMGjQrc3LtottVj_o=/90x0/smart/filters:strip_icc()/" data-url-tablet="rNl93krn-E3vfQSSES6_SV-tLAg=/70x50/smart/filters:strip_icc()/" data-url-desktop="acBAxPmP4eXbJzwZPnsvwoHq6pk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Atrizes esbanjam estilo <br />no lanÃ§amento de nova novela das 19h; siga aqui</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 09:55:46" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/10/carolina-dieckmann-desce-ate-o-chao-em-show-de-preta-gil.html" class="foto" title="Dieckmann sobe ao palco e arrasa no rebolado cercada de baianos ilustres (Felipe Souto Maior/Agnews)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/vzRVBSKou8B6F8uOzZTuffPPDok=/filters:quality(10):strip_icc()/s2.glbimg.com/9t005_zVvmeuxWMuSVlXucmaJIM=/499x254:731x409/120x80/s.glbimg.com/jo/eg/f/original/2015/10/20/caroldieckmann8.jpg" alt="Dieckmann sobe ao palco e arrasa no rebolado cercada de baianos ilustres (Felipe Souto Maior/Agnews)" title="Dieckmann sobe ao palco e arrasa no rebolado cercada de baianos ilustres (Felipe Souto Maior/Agnews)"
                data-original-image="s2.glbimg.com/9t005_zVvmeuxWMuSVlXucmaJIM=/499x254:731x409/120x80/s.glbimg.com/jo/eg/f/original/2015/10/20/caroldieckmann8.jpg" data-url-smart_horizontal="_MKr1qjnD27OW0xH3sZRugA_PSw=/90x0/smart/filters:strip_icc()/" data-url-smart="_MKr1qjnD27OW0xH3sZRugA_PSw=/90x0/smart/filters:strip_icc()/" data-url-feature="_MKr1qjnD27OW0xH3sZRugA_PSw=/90x0/smart/filters:strip_icc()/" data-url-tablet="fWGkVfX38zwncfoAsyXj67m-hpY=/70x50/smart/filters:strip_icc()/" data-url-desktop="uiWvKUdcZ1d6DfvEaKhCnt-zz3k=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Dieckmann sobe ao palco e arrasa no rebolado cercada de baianos ilustres</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/moda/noticia/2015/10/gravida-sophie-charlotte-disfarca-barriguinha-com-look-larguinho-no-spfw.html" class="" title="GrÃ¡vida do primeiro filho, Sophie Charlotte revela desejos: &#39;Muita comida do ParÃ¡&#39; (Celso Tavares / Ego)" rel="bookmark"><span class="conteudo"><p>GrÃ¡vida do primeiro filho, Sophie Charlotte revela desejos: &#39;Muita comida do ParÃ¡&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/10/com-os-cabelos-grisalhos-brad-pitt-recebe-gemeos-em-set-de-filmagens.html" class="foto" title="Os gÃªmeos e o galÃ£ grisalho: Brad Pitt recebe os caÃ§ulas em Londres (Grosby Group)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/R3uKms25nNFicf77ip_GzFnvjqA=/filters:quality(10):strip_icc()/s2.glbimg.com/eIw5j29jbkSNfjsnDb1K5OvHfUE=/58x87:492x377/120x80/e.glbimg.com/og/ed/f/original/2015/10/20/bp3.jpg" alt="Os gÃªmeos e o galÃ£ grisalho: Brad Pitt recebe os caÃ§ulas em Londres (Grosby Group)" title="Os gÃªmeos e o galÃ£ grisalho: Brad Pitt recebe os caÃ§ulas em Londres (Grosby Group)"
                data-original-image="s2.glbimg.com/eIw5j29jbkSNfjsnDb1K5OvHfUE=/58x87:492x377/120x80/e.glbimg.com/og/ed/f/original/2015/10/20/bp3.jpg" data-url-smart_horizontal="W91xDIXmUigTa0OsQoZInOv0_oI=/90x0/smart/filters:strip_icc()/" data-url-smart="W91xDIXmUigTa0OsQoZInOv0_oI=/90x0/smart/filters:strip_icc()/" data-url-feature="W91xDIXmUigTa0OsQoZInOv0_oI=/90x0/smart/filters:strip_icc()/" data-url-tablet="wxnOWB5VkyKeV1WKmN-zfukJ3HY=/70x50/smart/filters:strip_icc()/" data-url-desktop="84tpSQ96XTzNb-Hyv55p60INar0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Os gÃªmeos e o galÃ£ grisalho: Brad Pitt recebe os caÃ§ulas em Londres</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-20 09:12:17" class="chamada chamada-principal mobile-grid-full"><a href="http://revistacasaejardim.globo.com/Casa-e-Comida/Receitas/Sobremesas-com-chocolate/noticia/2013/04/sanduiche-de-sorvete.html" class="foto" title="Geladinho, encorpado e delÃ­cia! Aprenda a preparar o &#39;sanduÃ­che de sorvete&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/iVi6TU4Q5Ld-2tBEOhWmbuR83SA=/filters:quality(10):strip_icc()/s2.glbimg.com/07_wRMaWVuvMVW-_ik2oEPxfD6s=/35x153:277x314/120x80/s.glbimg.com/en/ho/f/original/2015/10/20/sanduiche_de_sorvete.jpeg" alt="Geladinho, encorpado e delÃ­cia! Aprenda a preparar o &#39;sanduÃ­che de sorvete&#39; (ReproduÃ§Ã£o)" title="Geladinho, encorpado e delÃ­cia! Aprenda a preparar o &#39;sanduÃ­che de sorvete&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/07_wRMaWVuvMVW-_ik2oEPxfD6s=/35x153:277x314/120x80/s.glbimg.com/en/ho/f/original/2015/10/20/sanduiche_de_sorvete.jpeg" data-url-smart_horizontal="HcmTFPXuTMXvt6bAgMFOU7xOZ_s=/90x0/smart/filters:strip_icc()/" data-url-smart="HcmTFPXuTMXvt6bAgMFOU7xOZ_s=/90x0/smart/filters:strip_icc()/" data-url-feature="HcmTFPXuTMXvt6bAgMFOU7xOZ_s=/90x0/smart/filters:strip_icc()/" data-url-tablet="orGQ8ppUPLdwDbscNTqB83YR-dU=/70x50/smart/filters:strip_icc()/" data-url-desktop="il1ERIZ57qIDOVAaBrx2_p8Ts8o=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Geladinho, encorpado e delÃ­cia! Aprenda a preparar o &#39;sanduÃ­che de sorvete&#39;</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/10/nos-30-anos-do-nintendinho-relembre-10-curiosidades-do-console.html" title="&#39;Nintendinho&#39; faz 30 anos: veja maiores curiosidades do console"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/jdk82dTP5Xh82-JCGEvgHC7dbqw=/filters:quality(10):strip_icc()/s2.glbimg.com/1-ByezkgcyuOmFnO5dCQFe1M5YI=/0x26:5559x2974/245x130/s.glbimg.com/po/tt2/f/original/2015/10/19/nes-console-set_1.png" alt="&#39;Nintendinho&#39; faz 30 anos: veja maiores curiosidades do console (DivulgaÃ§Ã£o)" title="&#39;Nintendinho&#39; faz 30 anos: veja maiores curiosidades do console (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/1-ByezkgcyuOmFnO5dCQFe1M5YI=/0x26:5559x2974/245x130/s.glbimg.com/po/tt2/f/original/2015/10/19/nes-console-set_1.png" data-url-smart_horizontal="nErY2rlYGlja59unM5_7YRCqVLg=/90x56/smart/filters:strip_icc()/" data-url-smart="nErY2rlYGlja59unM5_7YRCqVLg=/90x56/smart/filters:strip_icc()/" data-url-feature="nErY2rlYGlja59unM5_7YRCqVLg=/90x56/smart/filters:strip_icc()/" data-url-tablet="ac4LHpEdDv6dUAkBQ6l6YWE8Ebc=/160x96/smart/filters:strip_icc()/" data-url-desktop="tfHWTiTAt7mrZZkcUkSt7mEG7a4=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>&#39;Nintendinho&#39; faz 30 anos: veja maiores curiosidades do console</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/10/como-descobrir-quais-apps-estao-consumindo-mais-espaco-no-android.html" title="Apps podem estar &#39;sugando&#39; o espaÃ§o do seu smart; saiba ver"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/SiJILJHsn4z-NsYRu88k_qGDIb8=/filters:quality(10):strip_icc()/s2.glbimg.com/nSV6uFWfuGRNwDiTxqrbvOk8HHo=/0x22:694x391/245x130/s.glbimg.com/po/tt2/f/original/2015/09/17/galaxy_note_3_pegada_uso.jpg" alt="Apps podem estar &#39;sugando&#39; o espaÃ§o do seu smart; saiba ver (TechTudo)" title="Apps podem estar &#39;sugando&#39; o espaÃ§o do seu smart; saiba ver (TechTudo)"
             data-original-image="s2.glbimg.com/nSV6uFWfuGRNwDiTxqrbvOk8HHo=/0x22:694x391/245x130/s.glbimg.com/po/tt2/f/original/2015/09/17/galaxy_note_3_pegada_uso.jpg" data-url-smart_horizontal="pMPKMBYkopphbJD94dQvVUTVZto=/90x56/smart/filters:strip_icc()/" data-url-smart="pMPKMBYkopphbJD94dQvVUTVZto=/90x56/smart/filters:strip_icc()/" data-url-feature="pMPKMBYkopphbJD94dQvVUTVZto=/90x56/smart/filters:strip_icc()/" data-url-tablet="Xw6mgKG9pLHOvDmbUtKeVcq9-_8=/160x96/smart/filters:strip_icc()/" data-url-desktop="pS71A_gZhNATBzfDgMeDhOqsmRQ=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Apps podem estar &#39;sugando&#39; o <br />espaÃ§o do seu smart; saiba ver</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/10/entenda-diferenca-entre-memoria-ram-e-hd-veja-como-funcionam.html" title="RAM ou HD? ConheÃ§a cada tipo de memÃ³ria e saiba as diferenÃ§as"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/00BMrhWSdQwj83Ma2u7Z7jEqKtY=/filters:quality(10):strip_icc()/s2.glbimg.com/VSWCiuM1wKc6K1lAGJ_Ordi3eWE=/0x17:695x386/245x130/s.glbimg.com/po/tt2/f/original/2015/09/09/memoria-ram.png" alt="RAM ou HD? ConheÃ§a cada tipo de memÃ³ria e saiba as diferenÃ§as (Pentes de memÃ³ria mal instalados, ou incompatÃ­veis com a placa-mÃ£e, podem provocar tela azul (Foto: Filipe Garrett/TechTudo))" title="RAM ou HD? ConheÃ§a cada tipo de memÃ³ria e saiba as diferenÃ§as (Pentes de memÃ³ria mal instalados, ou incompatÃ­veis com a placa-mÃ£e, podem provocar tela azul (Foto: Filipe Garrett/TechTudo))"
             data-original-image="s2.glbimg.com/VSWCiuM1wKc6K1lAGJ_Ordi3eWE=/0x17:695x386/245x130/s.glbimg.com/po/tt2/f/original/2015/09/09/memoria-ram.png" data-url-smart_horizontal="VbSOyRVJB7G_B6pSJFjCFK7LESo=/90x56/smart/filters:strip_icc()/" data-url-smart="VbSOyRVJB7G_B6pSJFjCFK7LESo=/90x56/smart/filters:strip_icc()/" data-url-feature="VbSOyRVJB7G_B6pSJFjCFK7LESo=/90x56/smart/filters:strip_icc()/" data-url-tablet="-AncUc061lHupIxfhHT7BDT2dAU=/160x96/smart/filters:strip_icc()/" data-url-desktop="PRhGA6tmXO3hkEsBCYdF4kdMtTY=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>RAM ou HD? ConheÃ§a cada tipo <br />de memÃ³ria e saiba as diferenÃ§as</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/tudo-sobre/called-it.html" title="App &#39;bizarro&#39; prevÃª o seu futuro e o de seus amigos; baixe e teste"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/5Lpc3MP9xHR3JhQCh22p1zQiuZs=/filters:quality(10):strip_icc()/s2.glbimg.com/g1FUPx1e9aUtoK1SV6cedJgeaw4=/0x9:695x377/245x130/s.glbimg.com/po/tt2/f/original/2015/07/31/iphone_surpresa.jpg" alt="App &#39;bizarro&#39; prevÃª o seu futuro e o de seus amigos; baixe e teste (Luciana Maline/TechTudo)" title="App &#39;bizarro&#39; prevÃª o seu futuro e o de seus amigos; baixe e teste (Luciana Maline/TechTudo)"
             data-original-image="s2.glbimg.com/g1FUPx1e9aUtoK1SV6cedJgeaw4=/0x9:695x377/245x130/s.glbimg.com/po/tt2/f/original/2015/07/31/iphone_surpresa.jpg" data-url-smart_horizontal="dtmV2IPcPMR29tBi5FBHJkg7V6k=/90x56/smart/filters:strip_icc()/" data-url-smart="dtmV2IPcPMR29tBi5FBHJkg7V6k=/90x56/smart/filters:strip_icc()/" data-url-feature="dtmV2IPcPMR29tBi5FBHJkg7V6k=/90x56/smart/filters:strip_icc()/" data-url-tablet="AcImXtrNjRc7V7hlupyRqymgo84=/160x96/smart/filters:strip_icc()/" data-url-desktop="pkOV5yK3bSLDlRgDZkbdrFyjmWU=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>App &#39;bizarro&#39; prevÃª o seu futuro e o de seus amigos; baixe e teste</p></span></a></li></ul></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://blogs.oglobo.globo.com/ela-de-batom/post/modelos-e-cabeleireiros-ensinam-cuidar-dos-fios-crespos.html" alt="Quanto mais volume, melhor! Veja dicas para valorizar os fios crespos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/FKti0GL5GngznDim3Uu5t0_RsX8=/filters:quality(10):strip_icc()/s2.glbimg.com/xyhZ1Umxa8vbtdXLIfWN2OLGWlo=/213x75:616x335/155x100/s.glbimg.com/en/ho/f/original/2015/10/20/cabelos_crespo_spfw_-_moda_ela.jpg" alt="Quanto mais volume, melhor! Veja dicas para valorizar os fios crespos (Ari Westphal na Animale | DivulgaÃ§Ã£o)" title="Quanto mais volume, melhor! Veja dicas para valorizar os fios crespos (Ari Westphal na Animale | DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/xyhZ1Umxa8vbtdXLIfWN2OLGWlo=/213x75:616x335/155x100/s.glbimg.com/en/ho/f/original/2015/10/20/cabelos_crespo_spfw_-_moda_ela.jpg" data-url-smart_horizontal="jR9DjHy5l8p4XEV3iQ_ugdDkojU=/90x56/smart/filters:strip_icc()/" data-url-smart="jR9DjHy5l8p4XEV3iQ_ugdDkojU=/90x56/smart/filters:strip_icc()/" data-url-feature="jR9DjHy5l8p4XEV3iQ_ugdDkojU=/90x56/smart/filters:strip_icc()/" data-url-tablet="cXDKAGwyibZDr5gLAFbyhXmbYk0=/122x75/smart/filters:strip_icc()/" data-url-desktop="qtUotT3cQ4h4KEsBbMFmEOG6MTM=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>chapinha em baixa</h3><p>Quanto mais volume, melhor! Veja dicas para valorizar os fios crespos</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Moda/noticia/2015/10/mamilos-roubam-cena-e-revelam-transparencia-ousada-como-tendencia-de-inverno.html" alt="Movimento #FreeNipple rouba cena nos primeiros desfiles do SPFW; veja"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/WxTiTcZlkLWF_sCiuRDwdT6hnK4=/filters:quality(10):strip_icc()/s2.glbimg.com/t419etx3-TjoGuQorBxfEDCZi5s=/17x27:371x263/120x80/s.glbimg.com/en/ho/f/original/2015/10/20/alexandre-transp.jpg" alt="Movimento #FreeNipple rouba cena nos primeiros desfiles do SPFW; veja (Imaxtree)" title="Movimento #FreeNipple rouba cena nos primeiros desfiles do SPFW; veja (Imaxtree)"
            data-original-image="s2.glbimg.com/t419etx3-TjoGuQorBxfEDCZi5s=/17x27:371x263/120x80/s.glbimg.com/en/ho/f/original/2015/10/20/alexandre-transp.jpg" data-url-smart_horizontal="hb--QHIewtW_4y5NqqHJMpRtqTo=/90x56/smart/filters:strip_icc()/" data-url-smart="hb--QHIewtW_4y5NqqHJMpRtqTo=/90x56/smart/filters:strip_icc()/" data-url-feature="hb--QHIewtW_4y5NqqHJMpRtqTo=/90x56/smart/filters:strip_icc()/" data-url-tablet="pyrYfXV9glYBsfVGPniQ5ZMbfQQ=/122x75/smart/filters:strip_icc()/" data-url-desktop="csuoQTBTo6ixyEa0o-8W7E4fKrw=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>transparÃªncias sÃ£o destaque</h3><p>Movimento #FreeNipple rouba cena nos primeiros desfiles do SPFW; veja</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/bem-estar/materias/partir-dos-25-saiba-como-cuidar-da-pele-em-todas-idades.htm" alt="Especialistas indicam cuidados com a pele a partir dos 25 anos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/BHSbSskMt3ZXqcCLv-8_PE9ontM=/filters:quality(10):strip_icc()/s2.glbimg.com/p2636QVlBQc6H4CL5eFb0vw6bpk=/54x67:546x385/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/06/23/pele_limpa.jpg" alt="Especialistas indicam cuidados com a pele a partir dos 25 anos (ReproduÃ§Ã£o / Getty Images)" title="Especialistas indicam cuidados com a pele a partir dos 25 anos (ReproduÃ§Ã£o / Getty Images)"
            data-original-image="s2.glbimg.com/p2636QVlBQc6H4CL5eFb0vw6bpk=/54x67:546x385/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/06/23/pele_limpa.jpg" data-url-smart_horizontal="YyqIR2MuL5n1oGYNeqbPmZEt3_c=/90x56/smart/filters:strip_icc()/" data-url-smart="YyqIR2MuL5n1oGYNeqbPmZEt3_c=/90x56/smart/filters:strip_icc()/" data-url-feature="YyqIR2MuL5n1oGYNeqbPmZEt3_c=/90x56/smart/filters:strip_icc()/" data-url-tablet="3aVoWejUufy7U9cLHumuzuG81k4=/122x75/smart/filters:strip_icc()/" data-url-desktop="XHpjwTP9r3rwTSahYST31Ww9uNM=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>saudÃ¡vel e bonita</h3><p>Especialistas indicam cuidados com a pele a partir dos 25 anos</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Jardim/Decoracao/noticia/2015/10/revestimentos-parede-piso-cozinha-banheiro-e-area-externa.html" alt="Especial traz as melhores opÃ§Ãµes de revestimentos para cada ambiente"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/duWP3sm8cMQFx2eApVAk7lsqY0k=/filters:quality(10):strip_icc()/s2.glbimg.com/HhvkOn_TXUWpyqXmjCZGNGeZQ-0=/0x386:620x785/155x100/e.glbimg.com/og/ed/f/original/2015/09/02/cj723_paisagismocate_1.jpg" alt="Especial traz as melhores opÃ§Ãµes de revestimentos para cada ambiente (Gui Morelli/Editora Globo)" title="Especial traz as melhores opÃ§Ãµes de revestimentos para cada ambiente (Gui Morelli/Editora Globo)"
            data-original-image="s2.glbimg.com/HhvkOn_TXUWpyqXmjCZGNGeZQ-0=/0x386:620x785/155x100/e.glbimg.com/og/ed/f/original/2015/09/02/cj723_paisagismocate_1.jpg" data-url-smart_horizontal="Dp-ZIEr5_5K4gDS91Yai04NPa8o=/90x56/smart/filters:strip_icc()/" data-url-smart="Dp-ZIEr5_5K4gDS91Yai04NPa8o=/90x56/smart/filters:strip_icc()/" data-url-feature="Dp-ZIEr5_5K4gDS91Yai04NPa8o=/90x56/smart/filters:strip_icc()/" data-url-tablet="qWPzr3tbWLjtDwTU_fhJZ5MNQo8=/122x75/smart/filters:strip_icc()/" data-url-desktop="htJNU4XQQQCHEKdj1cUPnu5Iz6E=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>capriche </h3><p>Especial traz as melhores opÃ§Ãµes de revestimentos para cada ambiente</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Casa-Vogue-Experience/noticia/2015/10/um-bate-papo-com-paulo-mendes-da-rocha.html" alt="Paulo Mendes da Rocha fala sobre a carreira e a forma como vivemos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/DG-KtYPwOGatl7txZ3vzjfGqPKc=/filters:quality(10):strip_icc()/s2.glbimg.com/uXGMvb1bpunP_uIj6sZvj30fUgg=/205x142:557x370/155x100/e.glbimg.com/og/ed/f/original/2015/10/16/paulo-mendes-da-rocha-05.jpg" alt="Paulo Mendes da Rocha fala sobre a carreira e a forma como vivemos (Cristiano Mascaro)" title="Paulo Mendes da Rocha fala sobre a carreira e a forma como vivemos (Cristiano Mascaro)"
            data-original-image="s2.glbimg.com/uXGMvb1bpunP_uIj6sZvj30fUgg=/205x142:557x370/155x100/e.glbimg.com/og/ed/f/original/2015/10/16/paulo-mendes-da-rocha-05.jpg" data-url-smart_horizontal="1LMRWnsdkziory_GsY-ZRHBp2Zg=/90x56/smart/filters:strip_icc()/" data-url-smart="1LMRWnsdkziory_GsY-ZRHBp2Zg=/90x56/smart/filters:strip_icc()/" data-url-feature="1LMRWnsdkziory_GsY-ZRHBp2Zg=/90x56/smart/filters:strip_icc()/" data-url-tablet="4U37WvHPZT1F8xVmESMZ1sbRlKw=/122x75/smart/filters:strip_icc()/" data-url-desktop="rV5nKLY9qA5rkPAUI-a-GVW1Fuc=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>arquiteto CONSAGRADO</h3><p>Paulo Mendes da Rocha fala sobre a carreira e a forma como vivemos</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/fronhas-divertidas-para-a-cama/" alt="Mude o visual da sua cama com fronhas divertidas; veja opÃ§Ãµes"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/1nR_w4ECgSQQfWkQH-2YPSFWEh4=/filters:quality(10):strip_icc()/s2.glbimg.com/vm6Vf9sNi0Iw7z2mpHT4PG0RAp4=/55x50:531x358/155x100/s.glbimg.com/en/ho/f/original/2015/10/20/fronha-oculos.jpg" alt="Mude o visual da sua cama com fronhas divertidas; veja opÃ§Ãµes (ReproduÃ§Ã£o/Pinterest)" title="Mude o visual da sua cama com fronhas divertidas; veja opÃ§Ãµes (ReproduÃ§Ã£o/Pinterest)"
            data-original-image="s2.glbimg.com/vm6Vf9sNi0Iw7z2mpHT4PG0RAp4=/55x50:531x358/155x100/s.glbimg.com/en/ho/f/original/2015/10/20/fronha-oculos.jpg" data-url-smart_horizontal="9uqazF73SPyYTkODw__od8B2eps=/90x56/smart/filters:strip_icc()/" data-url-smart="9uqazF73SPyYTkODw__od8B2eps=/90x56/smart/filters:strip_icc()/" data-url-feature="9uqazF73SPyYTkODw__od8B2eps=/90x56/smart/filters:strip_icc()/" data-url-tablet="BDijLoP_Bw5OrUN1i9i_FuHYizY=/122x75/smart/filters:strip_icc()/" data-url-desktop="HKaTNrTb7ovUWHLhhcP8DcLJWHQ=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>engraÃ§adinho</h3><p>Mude o visual da sua cama com fronhas divertidas; veja opÃ§Ãµes</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://epoca.globo.com/colunas-e-blogs/bruno-astuto/noticia/2015/10/antonio-fagundes-nunca-fui-meu-tipo-de-homem.html" title="&#39;Nunca fui meu tipo de homem&#39;, diz Fagundes em entrevista; leia"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/xMkqs5m5pUjKkZoSWkr1doeoXKc=/filters:quality(10):strip_icc()/s2.glbimg.com/qePoM6SToQbVwnWE4TpryYxX8Gg=/221x48:995x459/245x130/e.glbimg.com/og/ed/f/original/2014/06/17/fagundes.jpg" alt="&#39;Nunca fui meu tipo de homem&#39;, diz Fagundes em entrevista; leia (DivulgaÃ§Ã£o)" title="&#39;Nunca fui meu tipo de homem&#39;, diz Fagundes em entrevista; leia (DivulgaÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/qePoM6SToQbVwnWE4TpryYxX8Gg=/221x48:995x459/245x130/e.glbimg.com/og/ed/f/original/2014/06/17/fagundes.jpg" data-url-smart_horizontal="-PuEx0fGgt_DrwQ-7l-n8wbt5Mo=/90x56/smart/filters:strip_icc()/" data-url-smart="-PuEx0fGgt_DrwQ-7l-n8wbt5Mo=/90x56/smart/filters:strip_icc()/" data-url-feature="-PuEx0fGgt_DrwQ-7l-n8wbt5Mo=/90x56/smart/filters:strip_icc()/" data-url-tablet="pw_cTorzl-x3tHiGYWzRHg4f8EE=/160x95/smart/filters:strip_icc()/" data-url-desktop="cz4Brq5bkpYn5igw42fV54jyD7o=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;Nunca fui meu tipo de homem&#39;, diz Fagundes em entrevista; leia</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/moda/noticia/2015/10/veja-os-looks-das-famosas-no-segundo-dia-do-spfw-inverno-2016.html" title="Carol Celico arrasou no visual; veja os looks do 2Âº dia de SPFW"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/OBumw3_ytoCndbcdEQ2MzoThHI4=/filters:quality(10):strip_icc()/s2.glbimg.com/z5k0K-1jjHmHWAXyVArn7GsdTms=/135x95:473x275/245x130/e.glbimg.com/og/ed/f/original/2015/10/19/img_6293-_raphaelcastello_-_alta.jpg" alt="Carol Celico arrasou no visual; veja os looks do 2Âº dia de SPFW (Raphael Castello/AgNews)" title="Carol Celico arrasou no visual; veja os looks do 2Âº dia de SPFW (Raphael Castello/AgNews)"
                    data-original-image="s2.glbimg.com/z5k0K-1jjHmHWAXyVArn7GsdTms=/135x95:473x275/245x130/e.glbimg.com/og/ed/f/original/2015/10/19/img_6293-_raphaelcastello_-_alta.jpg" data-url-smart_horizontal="JMQ9g3MFsM2a7DaGi3gflwrjw4s=/90x56/smart/filters:strip_icc()/" data-url-smart="JMQ9g3MFsM2a7DaGi3gflwrjw4s=/90x56/smart/filters:strip_icc()/" data-url-feature="JMQ9g3MFsM2a7DaGi3gflwrjw4s=/90x56/smart/filters:strip_icc()/" data-url-tablet="0kU-y7_eg4Q1f4VCX2rojDeT8KM=/160x95/smart/filters:strip_icc()/" data-url-desktop="lxDkAGdrnOE1MJotV8ePxa5hCXM=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Carol Celico arrasou no visual; veja os looks do 2Âº dia de SPFW</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gnt.globo.com/programas/calada-noite/videos/4548674.htm" title="Wagner Moura diz que filho o ajudou a lidar com gravaÃ§Ãµes"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/HaarMaFGeOqMazq7hnt6-DSidAI=/filters:quality(10):strip_icc()/s2.glbimg.com/O52m5Geij__JRigo4XtwNencuD0=/715x175:1534x609/245x130/g.glbimg.com/og/gs/gsat5/f/original/2015/10/20/calada_noite_-_ep._10_-_wagner_moura.jpg" alt="Wagner Moura diz que filho o ajudou a lidar com gravaÃ§Ãµes (Flavia Montenegro )" title="Wagner Moura diz que filho o ajudou a lidar com gravaÃ§Ãµes (Flavia Montenegro )"
                    data-original-image="s2.glbimg.com/O52m5Geij__JRigo4XtwNencuD0=/715x175:1534x609/245x130/g.glbimg.com/og/gs/gsat5/f/original/2015/10/20/calada_noite_-_ep._10_-_wagner_moura.jpg" data-url-smart_horizontal="Ndr-IgTONwm0nvK-y-DoCpZKE8M=/90x56/smart/filters:strip_icc()/" data-url-smart="Ndr-IgTONwm0nvK-y-DoCpZKE8M=/90x56/smart/filters:strip_icc()/" data-url-feature="Ndr-IgTONwm0nvK-y-DoCpZKE8M=/90x56/smart/filters:strip_icc()/" data-url-tablet="RmPPuyRxQS7JQYdCzUNidTC2waA=/160x95/smart/filters:strip_icc()/" data-url-desktop="A-2ptFDlOjKedCaw8Z-isvRRUSQ=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Wagner Moura diz que filho o ajudou a lidar com gravaÃ§Ãµes</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/QUEM-News/noticia/2015/10/viviane-araujo-usa-macacao-brilhante-e-decotado-em-gravacao.html" title="Vivi escolhe macacÃ£o brilhante e decotado para gravaÃ§Ã£o"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/Ay3F8wegahCF00asY5HavQFn26M=/filters:quality(10):strip_icc()/s2.glbimg.com/mLp691G6bVSeosP9tvDllvRX5pk=/154x192:1040x662/245x130/s.glbimg.com/jo/eg/f/original/2015/10/20/img_9230.jpg" alt="Vivi escolhe macacÃ£o brilhante e decotado para gravaÃ§Ã£o (Alex Nunes/DivulgaÃ§Ã£o )" title="Vivi escolhe macacÃ£o brilhante e decotado para gravaÃ§Ã£o (Alex Nunes/DivulgaÃ§Ã£o )"
                    data-original-image="s2.glbimg.com/mLp691G6bVSeosP9tvDllvRX5pk=/154x192:1040x662/245x130/s.glbimg.com/jo/eg/f/original/2015/10/20/img_9230.jpg" data-url-smart_horizontal="_dBZd-VAhRqNmxCfBONLxSUifXI=/90x56/smart/filters:strip_icc()/" data-url-smart="_dBZd-VAhRqNmxCfBONLxSUifXI=/90x56/smart/filters:strip_icc()/" data-url-feature="_dBZd-VAhRqNmxCfBONLxSUifXI=/90x56/smart/filters:strip_icc()/" data-url-tablet="g7aq7YTtvBHSJpZtj-9zFdTPdd0=/160x95/smart/filters:strip_icc()/" data-url-desktop="tSUQC_1YZAwlvb4PVva_GWJu1Ys=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Vivi escolhe macacÃ£o brilhante <br />e decotado para gravaÃ§Ã£o</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/10/soraya-e-gabo-se-beijam-e-armam-contra-dom-peppino.html" title="&#39;I Love ParaisÃ³polis&#39;: casal se beija e arma contra D. Peppino"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ZI5JFuTRDraAjfq-lAab4nI14_s=/filters:quality(10):strip_icc()/s2.glbimg.com/FnGTy_2mdWw6wxDROZub2iadZdc=/92x26:627x311/245x130/s.glbimg.com/et/gs/f/original/2015/10/19/soraya-gabo-beijo.jpg" alt="&#39;I Love ParaisÃ³polis&#39;: casal se beija e arma contra D. Peppino (Felipe Monteiro/Gshow)" title="&#39;I Love ParaisÃ³polis&#39;: casal se beija e arma contra D. Peppino (Felipe Monteiro/Gshow)"
                    data-original-image="s2.glbimg.com/FnGTy_2mdWw6wxDROZub2iadZdc=/92x26:627x311/245x130/s.glbimg.com/et/gs/f/original/2015/10/19/soraya-gabo-beijo.jpg" data-url-smart_horizontal="Z46FoNc6LnFW_XRsxKzm36NG_IA=/90x56/smart/filters:strip_icc()/" data-url-smart="Z46FoNc6LnFW_XRsxKzm36NG_IA=/90x56/smart/filters:strip_icc()/" data-url-feature="Z46FoNc6LnFW_XRsxKzm36NG_IA=/90x56/smart/filters:strip_icc()/" data-url-tablet="6wL4zt2dLDb9YuIyJ6pRPAglU1M=/160x95/smart/filters:strip_icc()/" data-url-desktop="c9y2qkTITnDyAWz0JNQ96LbDgjQ=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;I Love ParaisÃ³polis&#39;: casal se beija e arma contra D. Peppino</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/10/nanda-e-livia-discutem-por-causa-de-roger.html" title="Nanda e LÃ­via discutem por causa de Roger em &#39;MalhaÃ§Ã£o&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/2ySbdqVVBdk23HxZ8A0oRJJzNt4=/filters:quality(10):strip_icc()/s2.glbimg.com/KNHE5Amu745K0RTm3rZZOYY9rCA=/0x26:690x393/245x130/s.glbimg.com/et/gs/f/original/2015/10/19/nanda-malhacao-2015.jpg" alt="Nanda e LÃ­via discutem por causa de Roger em &#39;MalhaÃ§Ã£o&#39; (TV Globo)" title="Nanda e LÃ­via discutem por causa de Roger em &#39;MalhaÃ§Ã£o&#39; (TV Globo)"
                    data-original-image="s2.glbimg.com/KNHE5Amu745K0RTm3rZZOYY9rCA=/0x26:690x393/245x130/s.glbimg.com/et/gs/f/original/2015/10/19/nanda-malhacao-2015.jpg" data-url-smart_horizontal="fHGZMmm-1m6hKWFQPnKhd5fW-Q8=/90x56/smart/filters:strip_icc()/" data-url-smart="fHGZMmm-1m6hKWFQPnKhd5fW-Q8=/90x56/smart/filters:strip_icc()/" data-url-feature="fHGZMmm-1m6hKWFQPnKhd5fW-Q8=/90x56/smart/filters:strip_icc()/" data-url-tablet="hsnn8yt9W1NwIUJQIj5zKg-4k00=/160x95/smart/filters:strip_icc()/" data-url-desktop="g76Yj6aiJfRPR-GcWJ9qn9pcEd4=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Nanda e LÃ­via discutem por causa de Roger em &#39;MalhaÃ§Ã£o&#39;</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/10/ex-bbb-amanda-djehdian-revela-rotina-que-mantera-para-desfilar-em-duas-escolas-de-samba.html" title="Animada para o carnaval, ex-BBB Amanda intensifica treinos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/4KRBpjaeH1ZSp_JcSxjG2v3P9I0=/filters:quality(10):strip_icc()/s2.glbimg.com/oxH0iabVOUBzzbjHo6QLmsyKN_U=/208x135:453x265/245x130/s.glbimg.com/et/gs/f/original/2015/10/19/amanda_gavioes_1.jpg" alt="Animada para o carnaval, ex-BBB Amanda intensifica treinos (Arquivo Pessoal)" title="Animada para o carnaval, ex-BBB Amanda intensifica treinos (Arquivo Pessoal)"
                    data-original-image="s2.glbimg.com/oxH0iabVOUBzzbjHo6QLmsyKN_U=/208x135:453x265/245x130/s.glbimg.com/et/gs/f/original/2015/10/19/amanda_gavioes_1.jpg" data-url-smart_horizontal="m5FdU63OynK3Y-GSEP7H2ETr6QA=/90x56/smart/filters:strip_icc()/" data-url-smart="m5FdU63OynK3Y-GSEP7H2ETr6QA=/90x56/smart/filters:strip_icc()/" data-url-feature="m5FdU63OynK3Y-GSEP7H2ETr6QA=/90x56/smart/filters:strip_icc()/" data-url-tablet="OrI4jCMNlOK7cRoiJn001A8akYw=/160x95/smart/filters:strip_icc()/" data-url-desktop="__eCap-zzhkPRRxOZgsHgzaUfBE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Animada para o carnaval, ex-BBB Amanda intensifica treinos</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/tv/noticia/2015/10/tais-araujo-arrasa-com-vestido-de-noiva-em-episodio-de-mister-brau.html" title="TaÃ­s AraÃºjo arrasa com vestido de noiva em &#39;Mister Brau&#39;; fotos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/6kk0wfNFynuZL_B8TGcnzhgnfhU=/filters:quality(10):strip_icc()/s2.glbimg.com/Q-WrEK7czZ0CsDnZUGxgCkAaSgM=/0x51:690x418/245x130/s.glbimg.com/et/gs/f/original/2015/10/19/tais_araujo_noiva.jpg" alt="TaÃ­s AraÃºjo arrasa com vestido de noiva em &#39;Mister Brau&#39;; fotos (JoÃ£o Miguel Junior / TV Globo)" title="TaÃ­s AraÃºjo arrasa com vestido de noiva em &#39;Mister Brau&#39;; fotos (JoÃ£o Miguel Junior / TV Globo)"
                    data-original-image="s2.glbimg.com/Q-WrEK7czZ0CsDnZUGxgCkAaSgM=/0x51:690x418/245x130/s.glbimg.com/et/gs/f/original/2015/10/19/tais_araujo_noiva.jpg" data-url-smart_horizontal="all_B7mc-GVvIpziHFdRMiFNvNQ=/90x56/smart/filters:strip_icc()/" data-url-smart="all_B7mc-GVvIpziHFdRMiFNvNQ=/90x56/smart/filters:strip_icc()/" data-url-feature="all_B7mc-GVvIpziHFdRMiFNvNQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="Ebm1rxcoHEYrVLSwsuKc8x6nAFg=/160x95/smart/filters:strip_icc()/" data-url-desktop="B7TeZVS59Fmj4wwK5h30qhvUmcY=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>TaÃ­s AraÃºjo arrasa com vestido de noiva em &#39;Mister Brau&#39;; fotos</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/musica/noticia/2015/10/demi-lovato-faz-show-fechado-em-sp-nesta-terca-com-transmissao-na-web.html" title="Demi Lovato lanÃ§a seu 5Âº disco com show fechado em SP e transmissÃ£o pela web (Getty Images/ReproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/yJfo2-xWkU4a4TSSBt1Qj0RJKlQ=/filters:quality(10):strip_icc()/s2.glbimg.com/L9JSpyf7WTeOurEQLK5LEZdLqE4=/312x54:592x223/215x130/e.glbimg.com/og/ed/f/original/2015/09/18/lovato.jpg"
                data-original-image="s2.glbimg.com/L9JSpyf7WTeOurEQLK5LEZdLqE4=/312x54:592x223/215x130/e.glbimg.com/og/ed/f/original/2015/09/18/lovato.jpg" data-url-smart_horizontal="IUM5Xl924lQnUVnhxOQGxZCxZlI=/90x56/smart/filters:strip_icc()/" data-url-smart="IUM5Xl924lQnUVnhxOQGxZCxZlI=/90x56/smart/filters:strip_icc()/" data-url-feature="IUM5Xl924lQnUVnhxOQGxZCxZlI=/90x56/smart/filters:strip_icc()/" data-url-tablet="JaRVILvQwwqX4FPaUyElW_KU-rk=/120x80/smart/filters:strip_icc()/" data-url-desktop="PYcRYAthV1WrWE4phG62t3_wAgs=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Demi Lovato lanÃ§a seu 5Âº disco com show fechado em SP e transmissÃ£o pela web</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/cultura/musica/critica-africa-pop-no-piano-de-maira-freitas-17820833" title="Filha de Martinho da Vila, MaÃ­ra Freitas traz &#39;Ãfrica pianÃ­stica&#39; em seu 2Âº Ã¡lbum (ReproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/lHV0sOkYdfM2RuipNpZuTtktgcM=/filters:quality(10):strip_icc()/s2.glbimg.com/5_ZQXmeNccXqe6wXlJeLV63fBZo=/140x69:691x402/215x130/s.glbimg.com/en/ho/f/original/2015/10/20/maira.jpg"
                data-original-image="s2.glbimg.com/5_ZQXmeNccXqe6wXlJeLV63fBZo=/140x69:691x402/215x130/s.glbimg.com/en/ho/f/original/2015/10/20/maira.jpg" data-url-smart_horizontal="2k5JOwwvQx__yqee7Il0FjshWrM=/90x56/smart/filters:strip_icc()/" data-url-smart="2k5JOwwvQx__yqee7Il0FjshWrM=/90x56/smart/filters:strip_icc()/" data-url-feature="2k5JOwwvQx__yqee7Il0FjshWrM=/90x56/smart/filters:strip_icc()/" data-url-tablet="Y1d4ZTrf9-IX5p5Tt_9oZ-razrw=/120x80/smart/filters:strip_icc()/" data-url-desktop="Pm8zEE5ClEFdq-svWfJ8t3ozf8s=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Filha de Martinho da Vila, MaÃ­ra Freitas traz &#39;Ãfrica pianÃ­stica&#39; em seu 2Âº Ã¡lbum</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/musica/noticia/2015/10/bruno-barretto-defendem-sertanejo-bruto-com-hit-farra-pinga-e-foguete.html" title="&#39;Farra, pinga e foguete&#39; Ã© hit do &#39;sertanejo bruto&#39;;  look raÃ­z Ã© oposto aos &#39;arrumadinhos&#39; (Arte G1)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/_VVMG85zgZR9AWAjU8ysZ8nE4pw=/filters:quality(10):strip_icc()/s2.glbimg.com/_9YnqrfadMye9fmSMXv-NtCe3Yg=/0x192:620x567/215x130/s.glbimg.com/jo/g1/f/original/2015/10/13/os-brutos-tambem-cantam.jpg"
                data-original-image="s2.glbimg.com/_9YnqrfadMye9fmSMXv-NtCe3Yg=/0x192:620x567/215x130/s.glbimg.com/jo/g1/f/original/2015/10/13/os-brutos-tambem-cantam.jpg" data-url-smart_horizontal="12scXDcGyDSwFlzYl2DFVaUhsFM=/90x56/smart/filters:strip_icc()/" data-url-smart="12scXDcGyDSwFlzYl2DFVaUhsFM=/90x56/smart/filters:strip_icc()/" data-url-feature="12scXDcGyDSwFlzYl2DFVaUhsFM=/90x56/smart/filters:strip_icc()/" data-url-tablet="p5Fu3nnyzM7IeQ_oIQ3jPQCfios=/120x80/smart/filters:strip_icc()/" data-url-desktop="TOQEOHnFp62pOl5hhIuQOLiJwaw=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">&#39;Farra, pinga e foguete&#39; Ã© hit do &#39;sertanejo bruto&#39;;  look raÃ­z Ã© oposto aos &#39;arrumadinhos&#39;</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/casos-de-policia/parece-que-minha-vida-parou-desabafa-jornalista-estuprada-torturada-por-sogra-dos-filhos-namorado-dela-17821337.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Jornalista Ã© estuprada e torturada por<br />sogra dos filhos e namorado no Rio</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/ma/maranhao/noticia/2015/10/promotor-denuncia-caso-de-canibalismo-no-presidio-de-pedrinhas.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Promotor denuncia caso de canibalismo<br />no PresÃ­dio de Pedrinhas, no MaranhÃ£o<br /></span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/planeta-bizarro/noticia/2015/10/chineses-usam-peixe-enorme-para-preparar-sopa-de-3-toneladas.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Chefs preparam sopa de trÃªs toneladas <br />com peixe gigante iÃ§ado por guindaste</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/10/morre-no-rio-atriz-yona-magalhaes.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Atriz YonÃ¡ MagalhÃ£es morre aos 80 anos<br /> em casa de saÃºde no Rio de Janeiro<br /></span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/mundo/noticia/2015/10/meu-governo-nao-esta-envolvido-em-escandalo-de-corrupcao-diz-dilma.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;Meu governo nÃ£o estÃ¡ envolvido em <br />escÃ¢ndalo de corrupÃ§Ã£o&#39;, afirma Dilma</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/noticia/2015/10/mais-17-lutadores-sao-demitidos-do-ultimate-incluindo-cinco-brasileiros.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Mais 17 lutadores sÃ£o demitidos do <br />Ultimate, incluindo cinco brasileiros<br /></span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/esporte/flamengo/longe-do-4-flamengo-muda-perfil-de-elenco-para-2016-intensifica-barca-paulinho-cirino-gabriel-negociaveis-17821030.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Longe do G-4, Flamengo muda perfil de<br /> elenco para 2016 e intensifica a barca</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/programas/sportv-news/noticia/2015/10/fifa-divulga-lista-e-neymar-e-o-unico-brasileiro-entre-23-melhores-de-2015.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Fifa divulga lista, e Neymar Ã© o Ãºnico do <br />Brasil entre os 23 melhores de 2015<br /></span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/sp/campinas-e-regiao/futebol/noticia/2015/10/filho-de-amoroso-ganha-destaque-no-udinese-e-sonha-com-selecao-italiana.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Filho de Amoroso ganha destaque no<br /> Udinese e sonha com seleÃ§Ã£o italiana<br /></span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/famosos/retratos-da-bola/pai-de-ronaldo-fenomeno-nelio-nazario-preocupa-amigos-familiares-por-causa-da-saude-17821988.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Pai de Ronaldo FenÃ´meno preocupa <br />amigos e familiares por causa da saÃºde</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/livia-se-declara-vitoria-e-minha-avo-e-eu-amo.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">LÃ­via se declara a VitÃ³ria em &#39;AlÃ©m do<br />Tempo&#39; e diz: âÃ minha avÃ³, e eu a amoâ</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/10/fernanda-keulla-posa-de-top-e-chama-atencao-por-magreza-excessiva.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Exagero? Ex-BBB Fernanda posa de top <br />e chama a atenÃ§Ã£o por magreza; confira</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://kogut.oglobo.globo.com/noticias-da-tv/noticia/2015/10/ex-malhacao-anna-rita-cerqueira-conta-que-perdeu-papel-em-verdades-secretas-por-causa-da-idade.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ex-&#39;MalhaÃ§Ã£o&#39; conta que ficou fora de &#39;Verdades&#39; por nÃ£o ter 18: &#39;Complicado&#39;</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/10/afonso-sera-irmao-de-felipe-ator-fala-de-nova-fase.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Afonso voltarÃ¡ como irmÃ£o de Felipe na<br />2Âª fase de &#39;AlÃ©m do Tempo&#39;; ator comenta</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/10/juliano-vai-descobrir-verdade-e-desmascarar-ze-maria.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;A Regra do Jogo&#39;: Juliano vai descobrir <br />a verdade e desmascarar o pai ZÃ© Maria<br /></span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://gshow.globo.com/series/pe-na-cova/2015/">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://gshow.globo.com/realities/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Totalmente Demais" href="http://gshow.globo.com/novelas/totalmente-demais/">Totalmente Demais</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/61f703452bf6.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 6000);}
</script><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 0};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 20/10/2015 12:22:48 -->
