<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.92" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.92" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.92"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.92"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.92"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.92"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');fbq("track","ViewContent");</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.92"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0;
var isNewYear = 1;
</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle">
				</span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    
</div><!--pull-ad end-->
</div>
    <!--testing 16-02-15 23:50:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             11 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/isro-to-expand-operations-by-privatising-its-most-successful-launcher-pslv-by-2020_-250723.html" class=" tint" title="ISRO To Expand Operations By Privatising Its Most Successful Launcher PSLV By 2020">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Feb/card_1455518142_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/card_1455518142_236x111.jpg"  border="0" alt="ISRO To Expand Operations By Privatising Its Most Successful Launcher PSLV By 2020"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/isro-to-expand-operations-by-privatising-its-most-successful-launcher-pslv-by-2020_-250723.html" title="ISRO To Expand Operations By Privatising Its Most Successful Launcher PSLV By 2020">
                            ISRO To Expand Operations By Privatising Its Most Successful Launcher PSLV By 2020                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            4 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/bras-and-fake-boobs-find-a-new-purpose-to-traffic-drugs-across-borders-250753.html" title="Bras And Fake Boobs Find A New Purpose, To Traffic Drugs Across Borders" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Feb/epa600_1455556428_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/epa600_1455556428_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/bras-and-fake-boobs-find-a-new-purpose-to-traffic-drugs-across-borders-250753.html" title="Bras And Fake Boobs Find A New Purpose, To Traffic Drugs Across Borders">
                            Bras And Fake Boobs Find A New Purpose, To Traffic Drugs Across Borders                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/spanish-employee-didn-t-turn-up-for-work-for-6-years-but-got-paid-anyway-250745.html" title="Spanish Employee Didn't Turn Up For Work For 6 Years But Got Paid Anyway!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Feb/out-of-office-sign1_1455542348_1455542354_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/out-of-office-sign1_1455542348_1455542354_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/spanish-employee-didn-t-turn-up-for-work-for-6-years-but-got-paid-anyway-250745.html" title="Spanish Employee Didn't Turn Up For Work For 6 Years But Got Paid Anyway!">
                            Spanish Employee Didn't Turn Up For Work For 6 Years But Got Paid Anyway!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/india-bids-teary-eyed-farewell-to-the-nine-siachen-bravehearts-of-the-indian-army-250750.html" title="India Bids Teary-Eyed Farewell To The Nine Siachen Bravehearts Of The Indian Army" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Feb/card_1455540260_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/card_1455540260_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/india-bids-teary-eyed-farewell-to-the-nine-siachen-bravehearts-of-the-indian-army-250750.html" title="India Bids Teary-Eyed Farewell To The Nine Siachen Bravehearts Of The Indian Army">
                            India Bids Teary-Eyed Farewell To The Nine Siachen Bravehearts Of The Indian Army                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            6 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/how-australia-s-adam-voges-made-the-most-of-an-umpiring-error-to-break-sachin-tendulkar-s-record-250749.html" title="How Australia's Adam Voges Made The Most Of An Umpiring Error To Break Sachin Tendulkarâs Record" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Feb/voges640_1455538327_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/voges640_1455538327_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/how-australia-s-adam-voges-made-the-most-of-an-umpiring-error-to-break-sachin-tendulkar-s-record-250749.html" title="How Australia's Adam Voges Made The Most Of An Umpiring Error To Break Sachin Tendulkarâs Record">
                            How Australia's Adam Voges Made The Most Of An Umpiring Error To Break Sachin Tendulkarâs Record                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/lifestyle/11-gifs-that-prove-that-luck-can-change-in-just-an-instant-250527.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/11-gifs-that-prove-that-luck-can-change-in-just-an-instant-250527.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2016/Feb/card_1455538536_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/card_1455538536_502x234.jpg"  border="0" alt="11 GIFs That Prove That Luck Can Change In Just An Instant" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/lifestyle/11-gifs-that-prove-that-luck-can-change-in-just-an-instant-250527.html" title="11 GIFs That Prove That Luck Can Change In Just An Instant" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/11-gifs-that-prove-that-luck-can-change-in-just-an-instant-250527.html');">11 GIFs That Prove That Luck Can Change In Just An Instant</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-indian-art-forms-that-could-disappear-forever-if-they-re-not-saved-250533.html" class="tint" title="9 Indian Art Forms That Could Disappear Forever If They're Not Saved" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/9-indian-art-forms-that-could-disappear-forever-if-they-re-not-saved-250533.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Feb/handicrafts-2_1455103391_1455103406_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/handicrafts-2_1455103391_1455103406_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-indian-art-forms-that-could-disappear-forever-if-they-re-not-saved-250533.html" title="9 Indian Art Forms That Could Disappear Forever If They're Not Saved" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/9-indian-art-forms-that-could-disappear-forever-if-they-re-not-saved-250533.html');">
                            9 Indian Art Forms That Could Disappear Forever If They're Not Saved                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/11-frustrating-inconveniences-that-should-really-not-be-making-sense-in-2016_-250441.html" class="tint" title="11 Frustrating Inconveniences That Should Really Not Be Making Sense In 2016" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/11-frustrating-inconveniences-that-should-really-not-be-making-sense-in-2016_-250441.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Feb/cp_1455011620_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/cp_1455011620_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-frustrating-inconveniences-that-should-really-not-be-making-sense-in-2016_-250441.html" title="11 Frustrating Inconveniences That Should Really Not Be Making Sense In 2016" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/11-frustrating-inconveniences-that-should-really-not-be-making-sense-in-2016_-250441.html');">
                            11 Frustrating Inconveniences That Should Really Not Be Making Sense In 2016                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/health/buzz/this-woman-says-she-bought-a-mcdonald-s-happy-meal-in-2010-and-it-hasn-t-rotted-yet-250737.html" class="tint" title="This Woman Says She Bought A McDonald's Happy Meal In 2010, And It Hasnât Rotted Yet!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Feb/card_1455530008_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/card_1455530008_218x102.jpg" border="0" alt="This Woman Says She Bought A McDonald's Happy Meal In 2010, And It Hasnât Rotted Yet!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/this-woman-says-she-bought-a-mcdonald-s-happy-meal-in-2010-and-it-hasn-t-rotted-yet-250737.html" title="This Woman Says She Bought A McDonald's Happy Meal In 2010, And It Hasnât Rotted Yet!">
                            This Woman Says She Bought A McDonald's Happy Meal In 2010, And It Hasnât Rotted Yet!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/11-gifs-that-prove-that-luck-can-change-in-just-an-instant-250527.html" class="tint" title="11 GIFs That Prove That Luck Can Change In Just An Instant">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Feb/card_1455538536_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/card_1455538536_218x102.jpg" border="0" alt="11 GIFs That Prove That Luck Can Change In Just An Instant"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/11-gifs-that-prove-that-luck-can-change-in-just-an-instant-250527.html" title="11 GIFs That Prove That Luck Can Change In Just An Instant">
                            11 GIFs That Prove That Luck Can Change In Just An Instant                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/7-bollywood-films-that-inspired-real-life-crimes-250726.html" class="tint" title="7 Bollywood Films That Inspired Real-Life Crimes!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Feb/card_1455532177_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/card_1455532177_218x102.jpg" border="0" alt="7 Bollywood Films That Inspired Real-Life Crimes!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/7-bollywood-films-that-inspired-real-life-crimes-250726.html" title="7 Bollywood Films That Inspired Real-Life Crimes!">
                            7 Bollywood Films That Inspired Real-Life Crimes!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/the-science-behind-why-we-fall-in-love-250712.html" class="tint" title="The Science Behind Why We Fall In Love">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Feb/srk_1455453237_1455453248_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/srk_1455453237_1455453248_218x102.jpg" border="0" alt="The Science Behind Why We Fall In Love"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/the-science-behind-why-we-fall-in-love-250712.html" title="The Science Behind Why We Fall In Love">
                            The Science Behind Why We Fall In Love                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/19-beautiful-quotes-that-attempt-to-describe-the-meaning-of-true-love-250495.html" class="tint" title="19 Beautiful Quotes That Attempt To Describe The Meaning Of True Love">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Feb/ssa_1455021299_1455021308_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Feb/ssa_1455021299_1455021308_218x102.jpg" border="0" alt="19 Beautiful Quotes That Attempt To Describe The Meaning Of True Love"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/19-beautiful-quotes-that-attempt-to-describe-the-meaning-of-true-love-250495.html" title="19 Beautiful Quotes That Attempt To Describe The Meaning Of True Love">
                            19 Beautiful Quotes That Attempt To Describe The Meaning Of True Love                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf " ><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            6 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/sports/here-s-what-rahul-dravid-had-to-say-to-indian-under-19-players-after-they-failed-to-win-world-cup-250746.html" title="Hereâs What Rahul Dravid Had To Say To Indian Under-19 Players After They Failed To Win World Cup" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/wall640_1455536854_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/here-s-what-rahul-dravid-had-to-say-to-indian-under-19-players-after-they-failed-to-win-world-cup-250746.html" title="Hereâs What Rahul Dravid Had To Say To Indian Under-19 Players After They Failed To Win World Cup">
                            Hereâs What Rahul Dravid Had To Say To Indian Under-19 Players After They Failed To Win World Cup                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-5"  data-slot="129061" data-position="5" data-section="0" data-cb="adwidgetNew"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            6 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/film-on-conservation-efforts-by-nagaland-farmers-bags-awards-at-a-film-festival-in-mumbai-250740.html" title="Film On Conservation Efforts By Nagaland Farmers Bags Awards At A Film Festival In Mumbai!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/na1_1455535991_1455535995_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/film-on-conservation-efforts-by-nagaland-farmers-bags-awards-at-a-film-festival-in-mumbai-250740.html" title="Film On Conservation Efforts By Nagaland Farmers Bags Awards At A Film Festival In Mumbai!">
                            Film On Conservation Efforts By Nagaland Farmers Bags Awards At A Film Festival In Mumbai!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf " ><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/mysuru-chandigarh-emerge-as-india-s-cleanest-cities-bengaluru-best-state-capital-250742.html" title="Mysuru, Chandigarh And Visakhapatnam Are India's Cleanest Cities. Modiji's Varanasi Amongst The Dirtiest" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455534915_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/mysuru-chandigarh-emerge-as-india-s-cleanest-cities-bengaluru-best-state-capital-250742.html" title="Mysuru, Chandigarh And Visakhapatnam Are India's Cleanest Cities. Modiji's Varanasi Amongst The Dirtiest">
                            Mysuru, Chandigarh And Visakhapatnam Are India's Cleanest Cities. Modiji's Varanasi Amongst The Dirtiest                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf " ><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/hafeez-saeed-issues-video-statement-says-he-didn-t-tweet-in-support-of-jnu-protest-250743.html" title="Hafeez Saeed Issues Video Statement, Says He Didn't Tweet In Support Of JNU Protest" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455534993_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/hafeez-saeed-issues-video-statement-says-he-didn-t-tweet-in-support-of-jnu-protest-250743.html" title="Hafeez Saeed Issues Video Statement, Says He Didn't Tweet In Support Of JNU Protest">
                            Hafeez Saeed Issues Video Statement, Says He Didn't Tweet In Support Of JNU Protest                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf " ><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/these-exclusive-pictures-prove-that-titanic-ii-is-a-marvellous-replica-of-the-original-250735.html" title="These Exclusive Pictures Prove That 'Titanic II' Is A Marvellous Replica Of The Original!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/tit600_1455525620_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/these-exclusive-pictures-prove-that-titanic-ii-is-a-marvellous-replica-of-the-original-250735.html" title="These Exclusive Pictures Prove That 'Titanic II' Is A Marvellous Replica Of The Original!">
                            These Exclusive Pictures Prove That 'Titanic II' Is A Marvellous Replica Of The Original!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/ki-ka-trailer-is-out-the-role-reversal-between-kareena-and-arjun-looks-weirdly-awesome-250744.html" class="tint" title="'Ki & Ka' Trailer Is Out & The Role Reversal Between Kareena And Arjun Looks Weirdly Awesome!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/screenshot_3_1455534897_1455534904_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/ki-ka-trailer-is-out-the-role-reversal-between-kareena-and-arjun-looks-weirdly-awesome-250744.html" title="'Ki & Ka' Trailer Is Out & The Role Reversal Between Kareena And Arjun Looks Weirdly Awesome!">
                            'Ki & Ka' Trailer Is Out & The Role Reversal Between Kareena And Arjun Looks Weirdly Awesome!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/sports-illustrated-magazine-creates-history-by-putting-a-plus-size-model-on-its-cover-for-the-first-time-250739.html" class="tint" title="Sports Illustrated Magazine Creates History By Putting A Plus Size Model On Its Cover For The First Time!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/si-cover-main-image-2-2016_1455531913_1455531919_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/sports-illustrated-magazine-creates-history-by-putting-a-plus-size-model-on-its-cover-for-the-first-time-250739.html" title="Sports Illustrated Magazine Creates History By Putting A Plus Size Model On Its Cover For The First Time!">
                            Sports Illustrated Magazine Creates History By Putting A Plus Size Model On Its Cover For The First Time!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-tinder-date-reveals-why-dating-a-perfect-girl-is-a-bad-idea-in-reality-250567.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/this-tinder-date-reveals-why-dating-a-perfect-girl-is-a-bad-idea-in-reality-250567.html" class="tint" title="This Tinder Date Reveals Why Dating 'A Perfect Girl' Is A Bad Idea In Reality!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/tinderdate_card_1455174200_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-tinder-date-reveals-why-dating-a-perfect-girl-is-a-bad-idea-in-reality-250567.html" title="This Tinder Date Reveals Why Dating 'A Perfect Girl' Is A Bad Idea In Reality!">
                            This Tinder Date Reveals Why Dating 'A Perfect Girl' Is A Bad Idea In Reality!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                           
                <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/love-is-more-than-shiny-ruby-red-roses-shiny-presents-says-twinkle-khanna-in-her-v-day-blog-250691.html" class="tint" title="Love Is More Than Shiny Ruby-Red Roses & Shiny Presents, Says Twinkle Khanna In Her V-Day Blog!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/tk_1455434520_1455434526_218x102.jpg" border="0" alt="Love Is More Than Shiny Ruby-Red Roses & Shiny Presents, Says Twinkle Khanna In Her V-Day Blog!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/love-is-more-than-shiny-ruby-red-roses-shiny-presents-says-twinkle-khanna-in-her-v-day-blog-250691.html" title="Love Is More Than Shiny Ruby-Red Roses & Shiny Presents, Says Twinkle Khanna In Her V-Day Blog!">
                            Love Is More Than Shiny Ruby-Red Roses & Shiny Presents, Says Twinkle Khanna In Her V-Day Blog!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               
                <div class="trending-panel-list cf colombia"  id="div-clmb-ctn-129061-6"  data-slot="129061" data-position="6" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/rishi-kapoor-neetu-kapoor-disclosed-the-details-of-their-new-house-it-s-grandeur-will-leave-you-speechless-250689.html" class="tint" title="Rishi Kapoor & Neetu Kapoor Disclosed The Details Of Their New House & It's Grandeur Will Leave You Speechless!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/a123_1455430870_1455430876_218x102.jpg" border="0" alt="Rishi Kapoor & Neetu Kapoor Disclosed The Details Of Their New House & It's Grandeur Will Leave You Speechless!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/rishi-kapoor-neetu-kapoor-disclosed-the-details-of-their-new-house-it-s-grandeur-will-leave-you-speechless-250689.html" title="Rishi Kapoor & Neetu Kapoor Disclosed The Details Of Their New House & It's Grandeur Will Leave You Speechless!">
                            Rishi Kapoor & Neetu Kapoor Disclosed The Details Of Their New House & It's Grandeur Will Leave You Speechless!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               
                <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/instead-of-moping-around-after-her-break-up-anushka-sharma-is-chilling-with-dude-on-valentine-s-day-250695.html" class="tint" title="Instead Of Moping Around After Her Break-Up, Anushka Sharma Is Chilling With 'Dude' On Valentine's Day!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/as_1455518275_1455518281_218x102.jpg" border="0" alt="Instead Of Moping Around After Her Break-Up, Anushka Sharma Is Chilling With 'Dude' On Valentine's Day!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/instead-of-moping-around-after-her-break-up-anushka-sharma-is-chilling-with-dude-on-valentine-s-day-250695.html" title="Instead Of Moping Around After Her Break-Up, Anushka Sharma Is Chilling With 'Dude' On Valentine's Day!">
                            Instead Of Moping Around After Her Break-Up, Anushka Sharma Is Chilling With 'Dude' On Valentine's Day!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               
                <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/perfect-husband-illustrates-every-moment-spent-with-his-wife-in-365-drawings-250607.html" class="tint" title="Perfect Husband Illustrates Every Moment Spent With His Wife In 365 Drawings">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/ads_1455200150_1455200165_218x102.jpg" border="0" alt="Perfect Husband Illustrates Every Moment Spent With His Wife In 365 Drawings"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/perfect-husband-illustrates-every-moment-spent-with-his-wife-in-365-drawings-250607.html" title="Perfect Husband Illustrates Every Moment Spent With His Wife In 365 Drawings">
                            Perfect Husband Illustrates Every Moment Spent With His Wife In 365 Drawings                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               
                <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-song-exactly-captures-what-we-feel-about-valentine-s-day-our-feet-haven-t-stopped-moving-since-we-discovered-this-250702.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/this-song-exactly-captures-what-we-feel-about-valentine-s-day-our-feet-haven-t-stopped-moving-since-we-discovered-this-250702.html" class="tint" title="This Song Exactly Captures What We Feel About Valentineâs Day. Our Feet Havenât Stopped Moving Since We Discovered This!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/card_1455443974_218x102.jpg" border="0" alt="This Song Exactly Captures What We Feel About Valentineâs Day. Our Feet Havenât Stopped Moving Since We Discovered This!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-song-exactly-captures-what-we-feel-about-valentine-s-day-our-feet-haven-t-stopped-moving-since-we-discovered-this-250702.html" title="This Song Exactly Captures What We Feel About Valentineâs Day. Our Feet Havenât Stopped Moving Since We Discovered This!">
                            This Song Exactly Captures What We Feel About Valentineâs Day. Our Feet Havenât Stopped Moving Since We Discovered This!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <div class="news-panel-list cf " >
				
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/now-a-video-goes-viral-alleging-it-was-abvp-activists-who-shouted-pakistan-zindabad-not-jnu-students-250736.html" title="Now, A Video Goes Viral Alleging It Was ABVP Activists Who Shouted 'Pakistan Zindabad' Not JNU Students" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455532291_236x111.jpg" border="0" alt="Now, A Video Goes Viral Alleging It Was ABVP Activists Who Shouted 'Pakistan Zindabad' Not JNU Students"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/now-a-video-goes-viral-alleging-it-was-abvp-activists-who-shouted-pakistan-zindabad-not-jnu-students-250736.html" title="Now, A Video Goes Viral Alleging It Was ABVP Activists Who Shouted 'Pakistan Zindabad' Not JNU Students">
                            Now, A Video Goes Viral Alleging It Was ABVP Activists Who Shouted 'Pakistan Zindabad' Not JNU Students                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-10"  data-slot="129061" data-position="10" data-section="0" data-cb="adwidgetNew">
				
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/railways-to-invest-rs-9-800-crore-in-mumbai-ahmedabad-bullet-train-project-construction-begins-in-2018_-250734.html" title="Railways To Invest Rs 9,800 Crore In Mumbai-Ahmedabad Bullet Train Project, Construction Begins In 2018" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455524785_236x111.jpg" border="0" alt="Railways To Invest Rs 9,800 Crore In Mumbai-Ahmedabad Bullet Train Project, Construction Begins In 2018"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/railways-to-invest-rs-9-800-crore-in-mumbai-ahmedabad-bullet-train-project-construction-begins-in-2018_-250734.html" title="Railways To Invest Rs 9,800 Crore In Mumbai-Ahmedabad Bullet Train Project, Construction Begins In 2018">
                            Railways To Invest Rs 9,800 Crore In Mumbai-Ahmedabad Bullet Train Project, Construction Begins In 2018                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf " >
				
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/this-blog-by-a-self-confessed-sickular-libtard-on-the-jnu-controversy-raises-some-very-valid-issues-250728.html" title="This Blog By A Self Confessed Sickular Libtard On The JNU Controversy Raises Some Very Valid Issues!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/jnu_1455527786_1455527792_236x111.jpg" border="0" alt="This Blog By A Self Confessed Sickular Libtard On The JNU Controversy Raises Some Very Valid Issues!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-blog-by-a-self-confessed-sickular-libtard-on-the-jnu-controversy-raises-some-very-valid-issues-250728.html" title="This Blog By A Self Confessed Sickular Libtard On The JNU Controversy Raises Some Very Valid Issues!">
                            This Blog By A Self Confessed Sickular Libtard On The JNU Controversy Raises Some Very Valid Issues!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf " >
				
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/chandigarh-born-sri-srinivasan-may-be-obama-s-top-choice-for-us-supreme-court-justice-250724.html" title="Chandigarh-Born 'Sri' Srinivasan May Be Obama's Top Choice For US Supreme Court Justice" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/600_1455517789_236x111.jpg" border="0" alt="Chandigarh-Born 'Sri' Srinivasan May Be Obama's Top Choice For US Supreme Court Justice"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/chandigarh-born-sri-srinivasan-may-be-obama-s-top-choice-for-us-supreme-court-justice-250724.html" title="Chandigarh-Born 'Sri' Srinivasan May Be Obama's Top Choice For US Supreme Court Justice">
                            Chandigarh-Born 'Sri' Srinivasan May Be Obama's Top Choice For US Supreme Court Justice                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf " >
				
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/former-dacoit-dadua-honoured-with-an-idol-in-a-up-temple-built-by-him-authorities-can-t-stop-it-250731.html" title="Former Dacoit Dadua Honoured With An Idol In A UP Temple Built By Him, Authorities Can't Stop It" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455521297_236x111.jpg" border="0" alt="Former Dacoit Dadua Honoured With An Idol In A UP Temple Built By Him, Authorities Can't Stop It"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/former-dacoit-dadua-honoured-with-an-idol-in-a-up-temple-built-by-him-authorities-can-t-stop-it-250731.html" title="Former Dacoit Dadua Honoured With An Idol In A UP Temple Built By Him, Authorities Can't Stop It">
                            Former Dacoit Dadua Honoured With An Idol In A UP Temple Built By Him, Authorities Can't Stop It                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/7-bollywood-films-that-inspired-real-life-crimes-250726.html" class="tint" title="7 Bollywood Films That Inspired Real-Life Crimes!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455532177_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bollywood/7-bollywood-films-that-inspired-real-life-crimes-250726.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/7-bollywood-films-that-inspired-real-life-crimes-250726.html" title="7 Bollywood Films That Inspired Real-Life Crimes!">
                            7 Bollywood Films That Inspired Real-Life Crimes!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/buzz/way-back-in-1669-one-man-s-experiments-with-pee-led-to-the-discovery-of-phosphorus-nojokes-250259.html" class="tint" title="Way Back In 1669, One Manâs Experiments With Pee Led To The Discovery Of Phosphorus! #NoJokes">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455532838_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/buzz/way-back-in-1669-one-man-s-experiments-with-pee-led-to-the-discovery-of-phosphorus-nojokes-250259.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/way-back-in-1669-one-man-s-experiments-with-pee-led-to-the-discovery-of-phosphorus-nojokes-250259.html" title="Way Back In 1669, One Manâs Experiments With Pee Led To The Discovery Of Phosphorus! #NoJokes">
                            Way Back In 1669, One Manâs Experiments With Pee Led To The Discovery Of Phosphorus! #NoJokes                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/working-from-home-will-change-your-life-even-if-it-doesn-t-change-your-boss-mustread-250151.html" class="tint" title="Working From Home Will Change Your Life, Even If It Doesn't Change Your Boss'! #MustRead">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455282148_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/self/working-from-home-will-change-your-life-even-if-it-doesn-t-change-your-boss-mustread-250151.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/working-from-home-will-change-your-life-even-if-it-doesn-t-change-your-boss-mustread-250151.html" title="Working From Home Will Change Your Life, Even If It Doesn't Change Your Boss'! #MustRead">
                            Working From Home Will Change Your Life, Even If It Doesn't Change Your Boss'! #MustRead                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                           <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/technology/sundar-pichai-s-salary-package-will-really-make-you-curl-up-in-a-corner-and-cry-250677.html" class="tint" title="Sundar Pichai's Salary Package Will Really Make You Curl Up In A Corner And Cry">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455358758_218x102.jpg" border="0" alt="Sundar Pichai's Salary Package Will Really Make You Curl Up In A Corner And Cry"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/sundar-pichai-s-salary-package-will-really-make-you-curl-up-in-a-corner-and-cry-250677.html" title="Sundar Pichai's Salary Package Will Really Make You Curl Up In A Corner And Cry">
                            Sundar Pichai's Salary Package Will Really Make You Curl Up In A Corner And Cry                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-11"  data-slot="129061" data-position="11" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/questions-asked-by-indian-men-to-foreign-women-on-online-chats-are-not-just-personal-but-offensive-and-obscene-250693.html" class="tint" title="Questions Asked By Indian Men To Foreign Women On Online Chats Are Not Just Personal, But Offensive And Obscene">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/sd_1455434835_1455434845_218x102.jpg" border="0" alt="Questions Asked By Indian Men To Foreign Women On Online Chats Are Not Just Personal, But Offensive And Obscene"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/questions-asked-by-indian-men-to-foreign-women-on-online-chats-are-not-just-personal-but-offensive-and-obscene-250693.html" title="Questions Asked By Indian Men To Foreign Women On Online Chats Are Not Just Personal, But Offensive And Obscene">
                            Questions Asked By Indian Men To Foreign Women On Online Chats Are Not Just Personal, But Offensive And Obscene                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/25-bollywood-psychological-thrillers-you-should-watch-at-least-once-in-your-life-250637.html" class="tint" title="25 Bollywood Psychological Thrillers You Should Watch At Least Once In Your Life">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/thriller-card_1455289996_1455290001_218x102.jpg" border="0" alt="25 Bollywood Psychological Thrillers You Should Watch At Least Once In Your Life"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/25-bollywood-psychological-thrillers-you-should-watch-at-least-once-in-your-life-250637.html" title="25 Bollywood Psychological Thrillers You Should Watch At Least Once In Your Life">
                            25 Bollywood Psychological Thrillers You Should Watch At Least Once In Your Life                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/travel/11-historical-sites-in-india-and-the-fascinating-stories-behind-them-250598.html" class="tint" title="11 Historical Sites In India And The Fascinating Stories Behind Them">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455191102_218x102.jpg" border="0" alt="11 Historical Sites In India And The Fascinating Stories Behind Them"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/11-historical-sites-in-india-and-the-fascinating-stories-behind-them-250598.html" title="11 Historical Sites In India And The Fascinating Stories Behind Them">
                            11 Historical Sites In India And The Fascinating Stories Behind Them                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/these-valentine-cards-for-unromantic-people-are-truly-wicked-250583.html" class="tint" title="These Valentine Cards For Unromantic People Are Truly Wicked!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_final_1455362414_218x102.jpg" border="0" alt="These Valentine Cards For Unromantic People Are Truly Wicked!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/these-valentine-cards-for-unromantic-people-are-truly-wicked-250583.html" title="These Valentine Cards For Unromantic People Are Truly Wicked!">
                            These Valentine Cards For Unromantic People Are Truly Wicked!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf " >
			
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/chris-gayle-tells-tom-moody-not-to-blush-evokes-laughter-with-reference-to-old-sexism-controversy-250725.html" title="Chris Gayle Tells Yet Another Australian Reporter 'Not To Blush'. But This Time There Is No Controversy, Just Laughter" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/cg640_1455523154_236x111.jpg" border="0" alt="Chris Gayle Tells Yet Another Australian Reporter 'Not To Blush'. But This Time There Is No Controversy, Just Laughter"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/chris-gayle-tells-tom-moody-not-to-blush-evokes-laughter-with-reference-to-old-sexism-controversy-250725.html" title="Chris Gayle Tells Yet Another Australian Reporter 'Not To Blush'. But This Time There Is No Controversy, Just Laughter">
                            Chris Gayle Tells Yet Another Australian Reporter 'Not To Blush'. But This Time There Is No Controversy, Just Laughter                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-15"  data-slot="129061" data-position="15" data-section="0" data-cb="adwidgetNew">
			
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/women-these-days-are-no-longer-restricted-to-singing-acting-hair-makeup-says-javed-akhtar-250721.html" title="Women These Days Are No Longer Restricted To Singing, Acting & Hair-Makeup, Says Javed Akhtar" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/ja-card_1455516290_1455516298_236x111.jpg" border="0" alt="Women These Days Are No Longer Restricted To Singing, Acting & Hair-Makeup, Says Javed Akhtar"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/women-these-days-are-no-longer-restricted-to-singing-acting-hair-makeup-says-javed-akhtar-250721.html" title="Women These Days Are No Longer Restricted To Singing, Acting & Hair-Makeup, Says Javed Akhtar">
                            Women These Days Are No Longer Restricted To Singing, Acting & Hair-Makeup, Says Javed Akhtar                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf " >
			
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/lionel-messi-passes-on-penalty-kick-to-luis-suarez-and-helps-him-net-a-hat-trick-250717.html" title="Lionel Messi Passes On Penalty Kick To Luis Suarez And Helps Him Net A Hat-Trick" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/messi640_1455512606_236x111.jpg" border="0" alt="Lionel Messi Passes On Penalty Kick To Luis Suarez And Helps Him Net A Hat-Trick"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/lionel-messi-passes-on-penalty-kick-to-luis-suarez-and-helps-him-net-a-hat-trick-250717.html" title="Lionel Messi Passes On Penalty Kick To Luis Suarez And Helps Him Net A Hat-Trick">
                            Lionel Messi Passes On Penalty Kick To Luis Suarez And Helps Him Net A Hat-Trick                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf " >
			
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/it-was-not-rabindranath-tagore-who-gave-gandhi-the-title-of-mahatma-it-was-a-not-so-famous-man-250727.html" title="It Was Not Rabindranath Tagore Who Gave Gandhi The Title Of Mahatma, It Was A Not So Famous Man" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/rabindra--800_1455519037_1455519043_236x111.jpg" border="0" alt="It Was Not Rabindranath Tagore Who Gave Gandhi The Title Of Mahatma, It Was A Not So Famous Man"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/it-was-not-rabindranath-tagore-who-gave-gandhi-the-title-of-mahatma-it-was-a-not-so-famous-man-250727.html" title="It Was Not Rabindranath Tagore Who Gave Gandhi The Title Of Mahatma, It Was A Not So Famous Man">
                            It Was Not Rabindranath Tagore Who Gave Gandhi The Title Of Mahatma, It Was A Not So Famous Man                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf " >
			
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/chinese-scientists-just-created-a-hotter-sun-in-a-nuclear-reactor-and-it-held-up-250718.html" title="Chinese Scientists Just Created A Hotter Sun In A Nuclear Reactor And It Held Up!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/600_1455513235_236x111.jpg" border="0" alt="Chinese Scientists Just Created A Hotter Sun In A Nuclear Reactor And It Held Up!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/chinese-scientists-just-created-a-hotter-sun-in-a-nuclear-reactor-and-it-held-up-250718.html" title="Chinese Scientists Just Created A Hotter Sun In A Nuclear Reactor And It Held Up!">
                            Chinese Scientists Just Created A Hotter Sun In A Nuclear Reactor And It Held Up!                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/jiah-khan-s-mother-files-fresh-petition-wants-fbi-to-intervene-as-she-was-a-us-citizen-250732.html" class="tint" title="Jiah Khan's Mother Files Fresh Petition, Wants FBI To Intervene As She Was A US Citizen">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/13-jiah-khan-b-130613_1455523505_1455523515_502x234.jpg" border="0" alt="Jiah Khan's Mother Files Fresh Petition, Wants FBI To Intervene As She Was A US Citizen" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/jiah-khan-s-mother-files-fresh-petition-wants-fbi-to-intervene-as-she-was-a-us-citizen-250732.html" title="Jiah Khan's Mother Files Fresh Petition, Wants FBI To Intervene As She Was A US Citizen">
                        Jiah Khan's Mother Files Fresh Petition, Wants FBI To Intervene As She Was A US Citizen                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/slumdog-millionaire-actor-dev-patel-is-set-to-star-in-a-film-based-on-the-2008-mumbai-terror-attacks-250729.html" class="tint" title="'Slumdog Millionaire' Actor Dev Patel Is Set To Star In A Film Based On The 2008 Mumbai Terror Attacks!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/picmonkey-collage_1455519071_1455519076_502x234.jpg" border="0" alt="'Slumdog Millionaire' Actor Dev Patel Is Set To Star In A Film Based On The 2008 Mumbai Terror Attacks!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/slumdog-millionaire-actor-dev-patel-is-set-to-star-in-a-film-based-on-the-2008-mumbai-terror-attacks-250729.html" title="'Slumdog Millionaire' Actor Dev Patel Is Set To Star In A Film Based On The 2008 Mumbai Terror Attacks!">
                        'Slumdog Millionaire' Actor Dev Patel Is Set To Star In A Film Based On The 2008 Mumbai Terror Attacks!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/aamir-khan-and-kangana-ranaut-just-had-a-dinner-date-with-pm-modi-250722.html" class="tint" title="Aamir Khan Attends Dinner With PM Modi And Tries To Put Intolerance Controversy To Rest!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/picmonkey-collage_1455516520_1455516527_502x234.jpg" border="0" alt="Aamir Khan Attends Dinner With PM Modi And Tries To Put Intolerance Controversy To Rest!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/aamir-khan-and-kangana-ranaut-just-had-a-dinner-date-with-pm-modi-250722.html" title="Aamir Khan Attends Dinner With PM Modi And Tries To Put Intolerance Controversy To Rest!">
                        Aamir Khan Attends Dinner With PM Modi And Tries To Put Intolerance Controversy To Rest!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/11-beautiful-places-to-visit-around-india-this-spring-250621.html" class="tint" title="11 Beautiful Places To Visit Around India This Spring">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455261643_502x234.jpg" border="0" alt="11 Beautiful Places To Visit Around India This Spring" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/11-beautiful-places-to-visit-around-india-this-spring-250621.html" title="11 Beautiful Places To Visit Around India This Spring">
                        11 Beautiful Places To Visit Around India This Spring                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/this-woman-says-she-bought-a-mcdonald-s-happy-meal-in-2010-and-it-hasn-t-rotted-yet-250737.html" class="tint" title="This Woman Says She Bought A McDonald's Happy Meal In 2010, And It Hasnât Rotted Yet!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455530008_502x234.jpg" border="0" alt="This Woman Says She Bought A McDonald's Happy Meal In 2010, And It Hasnât Rotted Yet!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/this-woman-says-she-bought-a-mcdonald-s-happy-meal-in-2010-and-it-hasn-t-rotted-yet-250737.html" title="This Woman Says She Bought A McDonald's Happy Meal In 2010, And It Hasnât Rotted Yet!">
                        This Woman Says She Bought A McDonald's Happy Meal In 2010, And It Hasnât Rotted Yet!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/24-signs-that-indicate-it-s-been-too-damn-long-since-your-last-vacation-250606.html" class="tint" title="24 Signs That Indicate Itâs Been Too Damn Long Since Your Last Vacation">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/asa_1455199466_1455199478_502x234.jpg" border="0" alt="24 Signs That Indicate Itâs Been Too Damn Long Since Your Last Vacation" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/24-signs-that-indicate-it-s-been-too-damn-long-since-your-last-vacation-250606.html" title="24 Signs That Indicate Itâs Been Too Damn Long Since Your Last Vacation">
                        24 Signs That Indicate Itâs Been Too Damn Long Since Your Last Vacation                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-signs-that-prove-our-generation-actually-works-harder-to-make-relationships-work-250580.html" class="tint" title="8 Signs That Prove Our Generation Has It Tougher In Relationships But Makes It Work Anyway">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455195836_502x234.jpg" border="0" alt="8 Signs That Prove Our Generation Has It Tougher In Relationships But Makes It Work Anyway" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-signs-that-prove-our-generation-actually-works-harder-to-make-relationships-work-250580.html" title="8 Signs That Prove Our Generation Has It Tougher In Relationships But Makes It Work Anyway">
                        8 Signs That Prove Our Generation Has It Tougher In Relationships But Makes It Work Anyway                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-short-film-will-make-you-rethink-everything-you-know-about-love-acid-attacks-mustwatch-250714.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-short-film-will-make-you-rethink-everything-you-know-about-love-acid-attacks-mustwatch-250714.html" class="tint" title="This Short Film Will Make You Rethink Everything You Know About Love & Acid Attacks #MustWatch">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/acidattack1_1455465606_502x234.jpg" border="0" alt="This Short Film Will Make You Rethink Everything You Know About Love & Acid Attacks #MustWatch" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-short-film-will-make-you-rethink-everything-you-know-about-love-acid-attacks-mustwatch-250714.html" title="This Short Film Will Make You Rethink Everything You Know About Love & Acid Attacks #MustWatch">
                        This Short Film Will Make You Rethink Everything You Know About Love & Acid Attacks #MustWatch                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/artist-tries-to-save-the-sounds-of-india-s-streets-before-they-are-lost-forever-250555.html" class="tint" title="Artist Tries To Save The Sounds Of India's Streets Before They Are Lost Forever">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/1-carmd-1_1455171298_502x234.jpg" border="0" alt="Artist Tries To Save The Sounds Of India's Streets Before They Are Lost Forever" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/artist-tries-to-save-the-sounds-of-india-s-streets-before-they-are-lost-forever-250555.html" title="Artist Tries To Save The Sounds Of India's Streets Before They Are Lost Forever">
                        Artist Tries To Save The Sounds Of India's Streets Before They Are Lost Forever                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/barack-obama-michelle-express-their-love-by-reciting-valentine-s-day-poems-for-each-other-250707.html" class="tint" title="Barack Obama & Michelle Express Their Love By Reciting Valentine's Day Poems For Each Other!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/rtr1vz47_1455447271_1455447275_502x234.jpg" border="0" alt="Barack Obama & Michelle Express Their Love By Reciting Valentine's Day Poems For Each Other!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/barack-obama-michelle-express-their-love-by-reciting-valentine-s-day-poems-for-each-other-250707.html" title="Barack Obama & Michelle Express Their Love By Reciting Valentine's Day Poems For Each Other!">
                        Barack Obama & Michelle Express Their Love By Reciting Valentine's Day Poems For Each Other!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/the-science-behind-why-we-fall-in-love-250712.html" class="tint" title="The Science Behind Why We Fall In Love">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/srk_1455453237_1455453248_502x234.jpg" border="0" alt="The Science Behind Why We Fall In Love" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/the-science-behind-why-we-fall-in-love-250712.html" title="The Science Behind Why We Fall In Love">
                        The Science Behind Why We Fall In Love                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-song-exactly-captures-what-we-feel-about-valentine-s-day-our-feet-haven-t-stopped-moving-since-we-discovered-this-250702.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-song-exactly-captures-what-we-feel-about-valentine-s-day-our-feet-haven-t-stopped-moving-since-we-discovered-this-250702.html" class="tint" title="This Song Exactly Captures What We Feel About Valentineâs Day. Our Feet Havenât Stopped Moving Since We Discovered This!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/card_1455443974_502x234.jpg" border="0" alt="This Song Exactly Captures What We Feel About Valentineâs Day. Our Feet Havenât Stopped Moving Since We Discovered This!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-song-exactly-captures-what-we-feel-about-valentine-s-day-our-feet-haven-t-stopped-moving-since-we-discovered-this-250702.html" title="This Song Exactly Captures What We Feel About Valentineâs Day. Our Feet Havenât Stopped Moving Since We Discovered This!">
                        This Song Exactly Captures What We Feel About Valentineâs Day. Our Feet Havenât Stopped Moving Since We Discovered This!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/19-beautiful-quotes-that-attempt-to-describe-the-meaning-of-true-love-250495.html" class="tint" title="19 Beautiful Quotes That Attempt To Describe The Meaning Of True Love">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/ssa_1455021299_1455021308_502x234.jpg" border="0" alt="19 Beautiful Quotes That Attempt To Describe The Meaning Of True Love" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/19-beautiful-quotes-that-attempt-to-describe-the-meaning-of-true-love-250495.html" title="19 Beautiful Quotes That Attempt To Describe The Meaning Of True Love">
                        19 Beautiful Quotes That Attempt To Describe The Meaning Of True Love                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/pakistani-kids-sent-greeting-cards-to-their-indian-friends-proved-that-love-conquers-all-250698.html" class="tint" title="Pakistani Kids Sent Greeting Cards To Their Indian Friends & Proved That Love Conquers All!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/zx_1455438402_1455438417_502x234.jpg" border="0" alt="Pakistani Kids Sent Greeting Cards To Their Indian Friends & Proved That Love Conquers All!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/pakistani-kids-sent-greeting-cards-to-their-indian-friends-proved-that-love-conquers-all-250698.html" title="Pakistani Kids Sent Greeting Cards To Their Indian Friends & Proved That Love Conquers All!">
                        Pakistani Kids Sent Greeting Cards To Their Indian Friends & Proved That Love Conquers All!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/instead-of-moping-around-after-her-break-up-anushka-sharma-is-chilling-with-dude-on-valentine-s-day-250695.html" class="tint" title="Instead Of Moping Around After Her Break-Up, Anushka Sharma Is Chilling With 'Dude' On Valentine's Day!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/as_1455518275_1455518281_502x234.jpg" border="0" alt="Instead Of Moping Around After Her Break-Up, Anushka Sharma Is Chilling With 'Dude' On Valentine's Day!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/instead-of-moping-around-after-her-break-up-anushka-sharma-is-chilling-with-dude-on-valentine-s-day-250695.html" title="Instead Of Moping Around After Her Break-Up, Anushka Sharma Is Chilling With 'Dude' On Valentine's Day!">
                        Instead Of Moping Around After Her Break-Up, Anushka Sharma Is Chilling With 'Dude' On Valentine's Day!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/good-news-ladies-if-you-suffer-from-period-cramps-weed-pills-may-come-to-your-rescue-250161.html" class="tint" title="Good News Ladies, If You Suffer From Period Cramps, Weed Pills May Come To Your Rescue!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/cover_1454413078_502x234.jpg" border="0" alt="Good News Ladies, If You Suffer From Period Cramps, Weed Pills May Come To Your Rescue!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/good-news-ladies-if-you-suffer-from-period-cramps-weed-pills-may-come-to-your-rescue-250161.html" title="Good News Ladies, If You Suffer From Period Cramps, Weed Pills May Come To Your Rescue!">
                        Good News Ladies, If You Suffer From Period Cramps, Weed Pills May Come To Your Rescue!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/love-is-more-than-shiny-ruby-red-roses-shiny-presents-says-twinkle-khanna-in-her-v-day-blog-250691.html" class="tint" title="Love Is More Than Shiny Ruby-Red Roses & Shiny Presents, Says Twinkle Khanna In Her V-Day Blog!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/tk_1455434520_1455434526_502x234.jpg" border="0" alt="Love Is More Than Shiny Ruby-Red Roses & Shiny Presents, Says Twinkle Khanna In Her V-Day Blog!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/love-is-more-than-shiny-ruby-red-roses-shiny-presents-says-twinkle-khanna-in-her-v-day-blog-250691.html" title="Love Is More Than Shiny Ruby-Red Roses & Shiny Presents, Says Twinkle Khanna In Her V-Day Blog!">
                        Love Is More Than Shiny Ruby-Red Roses & Shiny Presents, Says Twinkle Khanna In Her V-Day Blog!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/rishi-kapoor-neetu-kapoor-disclosed-the-details-of-their-new-house-it-s-grandeur-will-leave-you-speechless-250689.html" class="tint" title="Rishi Kapoor & Neetu Kapoor Disclosed The Details Of Their New House & It's Grandeur Will Leave You Speechless!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/a123_1455430870_1455430876_502x234.jpg" border="0" alt="Rishi Kapoor & Neetu Kapoor Disclosed The Details Of Their New House & It's Grandeur Will Leave You Speechless!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/rishi-kapoor-neetu-kapoor-disclosed-the-details-of-their-new-house-it-s-grandeur-will-leave-you-speechless-250689.html" title="Rishi Kapoor & Neetu Kapoor Disclosed The Details Of Their New House & It's Grandeur Will Leave You Speechless!">
                        Rishi Kapoor & Neetu Kapoor Disclosed The Details Of Their New House & It's Grandeur Will Leave You Speechless!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/questions-asked-by-indian-men-to-foreign-women-on-online-chats-are-not-just-personal-but-offensive-and-obscene-250693.html" class="tint" title="Questions Asked By Indian Men To Foreign Women On Online Chats Are Not Just Personal, But Offensive And Obscene">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/sd_1455434835_1455434845_502x234.jpg" border="0" alt="Questions Asked By Indian Men To Foreign Women On Online Chats Are Not Just Personal, But Offensive And Obscene" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/questions-asked-by-indian-men-to-foreign-women-on-online-chats-are-not-just-personal-but-offensive-and-obscene-250693.html" title="Questions Asked By Indian Men To Foreign Women On Online Chats Are Not Just Personal, But Offensive And Obscene">
                        Questions Asked By Indian Men To Foreign Women On Online Chats Are Not Just Personal, But Offensive And Obscene                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/11-carnivals-around-the-world-that-will-force-you-to-pack-your-bags-right-away-250485.html" class="tint" title="11 Carnivals Around The World That Will Force You To Pack Your Bags Right Away">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/jdf1_1455089029_502x234.jpg" border="0" alt="11 Carnivals Around The World That Will Force You To Pack Your Bags Right Away" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/11-carnivals-around-the-world-that-will-force-you-to-pack-your-bags-right-away-250485.html" title="11 Carnivals Around The World That Will Force You To Pack Your Bags Right Away">
                        11 Carnivals Around The World That Will Force You To Pack Your Bags Right Away                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-island-celebrates-valentine-s-day-by-conducting-underwater-wedding-festival-for-couples-250688.html" class="tint" title="This Island Celebrates Valentine's Day By Conducting Underwater Wedding Festival For Couples!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/fcc_1455427182_1455427191_502x234.jpg" border="0" alt="This Island Celebrates Valentine's Day By Conducting Underwater Wedding Festival For Couples!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-island-celebrates-valentine-s-day-by-conducting-underwater-wedding-festival-for-couples-250688.html" title="This Island Celebrates Valentine's Day By Conducting Underwater Wedding Festival For Couples!">
                        This Island Celebrates Valentine's Day By Conducting Underwater Wedding Festival For Couples!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/15-struggles-you-ll-relate-to-if-you-grew-up-with-strict-parents-250524.html" class="tint" title="15 Struggles You'll Relate To If You Grew Up With Strict Parents">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455099624_502x234.jpg" border="0" alt="15 Struggles You'll Relate To If You Grew Up With Strict Parents" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/15-struggles-you-ll-relate-to-if-you-grew-up-with-strict-parents-250524.html" title="15 Struggles You'll Relate To If You Grew Up With Strict Parents">
                        15 Struggles You'll Relate To If You Grew Up With Strict Parents                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-beautiful-short-film-about-an-innocent-kid-and-his-remarkable-kindness-will-inspire-you-250593.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-beautiful-short-film-about-an-innocent-kid-and-his-remarkable-kindness-will-inspire-you-250593.html" class="tint" title="This Beautiful Short Film About An Innocent Kid And His Remarkable Kindness Will Inspire You">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/theblanket_card_1455187702_502x234.jpg" border="0" alt="This Beautiful Short Film About An Innocent Kid And His Remarkable Kindness Will Inspire You" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-beautiful-short-film-about-an-innocent-kid-and-his-remarkable-kindness-will-inspire-you-250593.html" title="This Beautiful Short Film About An Innocent Kid And His Remarkable Kindness Will Inspire You">
                        This Beautiful Short Film About An Innocent Kid And His Remarkable Kindness Will Inspire You                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/10-things-that-happen-to-your-body-when-you-die-244871.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-things-that-happen-to-your-body-when-you-die-244871.html" class="tint" title="10 Things That Happen To Your Body When You Die">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/diefb_1441348214_502x234.jpg" border="0" alt="10 Things That Happen To Your Body When You Die" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-things-that-happen-to-your-body-when-you-die-244871.html" title="10 Things That Happen To Your Body When You Die">
                        10 Things That Happen To Your Body When You Die                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/11-historical-sites-in-india-and-the-fascinating-stories-behind-them-250598.html" class="tint" title="11 Historical Sites In India And The Fascinating Stories Behind Them">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455191102_502x234.jpg" border="0" alt="11 Historical Sites In India And The Fascinating Stories Behind Them" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/11-historical-sites-in-india-and-the-fascinating-stories-behind-them-250598.html" title="11 Historical Sites In India And The Fascinating Stories Behind Them">
                        11 Historical Sites In India And The Fascinating Stories Behind Them                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/25-bollywood-psychological-thrillers-you-should-watch-at-least-once-in-your-life-250637.html" class="tint" title="25 Bollywood Psychological Thrillers You Should Watch At Least Once In Your Life">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/thriller-card_1455289996_1455290001_502x234.jpg" border="0" alt="25 Bollywood Psychological Thrillers You Should Watch At Least Once In Your Life" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/25-bollywood-psychological-thrillers-you-should-watch-at-least-once-in-your-life-250637.html" title="25 Bollywood Psychological Thrillers You Should Watch At Least Once In Your Life">
                        25 Bollywood Psychological Thrillers You Should Watch At Least Once In Your Life                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/these-valentine-cards-for-unromantic-people-are-truly-wicked-250583.html" class="tint" title="These Valentine Cards For Unromantic People Are Truly Wicked!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_final_1455362414_502x234.jpg" border="0" alt="These Valentine Cards For Unromantic People Are Truly Wicked!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/these-valentine-cards-for-unromantic-people-are-truly-wicked-250583.html" title="These Valentine Cards For Unromantic People Are Truly Wicked!">
                        These Valentine Cards For Unromantic People Are Truly Wicked!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/the-new-cast-of-hip-hip-hurray-2-is-gearing-up-for-the-reboot-of-the-blockbuster-tv-series-250670.html" class="tint" title="The New Cast Of 'Hip Hip Hurray 2' Is Gearing Up For The Reboot Of The Blockbuster TV Series!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/hip-hip-card_1455369657_1455369663_502x234.jpg" border="0" alt="The New Cast Of 'Hip Hip Hurray 2' Is Gearing Up For The Reboot Of The Blockbuster TV Series!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/the-new-cast-of-hip-hip-hurray-2-is-gearing-up-for-the-reboot-of-the-blockbuster-tv-series-250670.html" title="The New Cast Of 'Hip Hip Hurray 2' Is Gearing Up For The Reboot Of The Blockbuster TV Series!">
                        The New Cast Of 'Hip Hip Hurray 2' Is Gearing Up For The Reboot Of The Blockbuster TV Series!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/the-latest-nigerian-scam-that-s-doing-the-rounds-is-literally-out-of-this-world-250680.html" class="tint" title="The Latest Nigerian Scam That's Doing The Rounds Is Literally Out Of This World!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455362720_502x234.jpg" border="0" alt="The Latest Nigerian Scam That's Doing The Rounds Is Literally Out Of This World!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/the-latest-nigerian-scam-that-s-doing-the-rounds-is-literally-out-of-this-world-250680.html" title="The Latest Nigerian Scam That's Doing The Rounds Is Literally Out Of This World!">
                        The Latest Nigerian Scam That's Doing The Rounds Is Literally Out Of This World!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/a-guy-lied-about-his-cousin-s-death-to-skip-work-now-his-boss-wants-a-death-certificate-250662.html" class="tint" title="A Guy Lied About His Cousin's Death To Skip Work. Now, He Needs Your Help To Prove It!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/image_1455440326_1455440335_502x234.jpg" border="0" alt="A Guy Lied About His Cousin's Death To Skip Work. Now, He Needs Your Help To Prove It!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/a-guy-lied-about-his-cousin-s-death-to-skip-work-now-his-boss-wants-a-death-certificate-250662.html" title="A Guy Lied About His Cousin's Death To Skip Work. Now, He Needs Your Help To Prove It!">
                        A Guy Lied About His Cousin's Death To Skip Work. Now, He Needs Your Help To Prove It!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/sundar-pichai-s-salary-package-will-really-make-you-curl-up-in-a-corner-and-cry-250677.html" class="tint" title="Sundar Pichai's Salary Package Will Really Make You Curl Up In A Corner And Cry">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455358758_502x234.jpg" border="0" alt="Sundar Pichai's Salary Package Will Really Make You Curl Up In A Corner And Cry" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/sundar-pichai-s-salary-package-will-really-make-you-curl-up-in-a-corner-and-cry-250677.html" title="Sundar Pichai's Salary Package Will Really Make You Curl Up In A Corner And Cry">
                        Sundar Pichai's Salary Package Will Really Make You Curl Up In A Corner And Cry                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/if-soft-drink-advertisements-were-honest-this-is-what-they-would-say-250216.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/if-soft-drink-advertisements-were-honest-this-is-what-they-would-say-250216.html" class="tint" title="If Soft Drink Advertisements Were Honest, This Is What They Would Say!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/card-3_1454502640_502x234.jpg" border="0" alt="If Soft Drink Advertisements Were Honest, This Is What They Would Say!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/if-soft-drink-advertisements-were-honest-this-is-what-they-would-say-250216.html" title="If Soft Drink Advertisements Were Honest, This Is What They Would Say!">
                        If Soft Drink Advertisements Were Honest, This Is What They Would Say!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/legendary-professor-asks-the-most-hilarious-questions-to-help-his-students-get-extra-credit-250655.html" class="tint" title="Legendary Professor Asks The Most Hilarious Questions To Help His Students Get Extra Credit">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455282514_502x234.jpg" border="0" alt="Legendary Professor Asks The Most Hilarious Questions To Help His Students Get Extra Credit" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/legendary-professor-asks-the-most-hilarious-questions-to-help-his-students-get-extra-credit-250655.html" title="Legendary Professor Asks The Most Hilarious Questions To Help His Students Get Extra Credit">
                        Legendary Professor Asks The Most Hilarious Questions To Help His Students Get Extra Credit                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/perfect-husband-illustrates-every-moment-spent-with-his-wife-in-365-drawings-250607.html" class="tint" title="Perfect Husband Illustrates Every Moment Spent With His Wife In 365 Drawings">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/ads_1455200150_1455200165_502x234.jpg" border="0" alt="Perfect Husband Illustrates Every Moment Spent With His Wife In 365 Drawings" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/perfect-husband-illustrates-every-moment-spent-with-his-wife-in-365-drawings-250607.html" title="Perfect Husband Illustrates Every Moment Spent With His Wife In 365 Drawings">
                        Perfect Husband Illustrates Every Moment Spent With His Wife In 365 Drawings                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/fawad-khan-s-little-gig-while-dubbing-for-kapoor-and-sons-will-leave-you-in-splits-250671.html" class="tint" title="Fawad Khan's Little Gig While Dubbing For 'Kapoor And Sons' Will Leave You In Splits!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/fw-sing-card_1455351765_1455351769_502x234.jpg" border="0" alt="Fawad Khan's Little Gig While Dubbing For 'Kapoor And Sons' Will Leave You In Splits!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/fawad-khan-s-little-gig-while-dubbing-for-kapoor-and-sons-will-leave-you-in-splits-250671.html" title="Fawad Khan's Little Gig While Dubbing For 'Kapoor And Sons' Will Leave You In Splits!">
                        Fawad Khan's Little Gig While Dubbing For 'Kapoor And Sons' Will Leave You In Splits!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/5-amazing-uses-of-lemon-we-bet-you-didn-t-know-about-250657.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/5-amazing-uses-of-lemon-we-bet-you-didn-t-know-about-250657.html" class="tint" title="5 Amazing Uses Of Lemon We Bet You Didnât Know About!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/cover_1455284197_502x234.jpg" border="0" alt="5 Amazing Uses Of Lemon We Bet You Didnât Know About!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/5-amazing-uses-of-lemon-we-bet-you-didn-t-know-about-250657.html" title="5 Amazing Uses Of Lemon We Bet You Didnât Know About!">
                        5 Amazing Uses Of Lemon We Bet You Didnât Know About!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/katrina-kaif-s-hired-bouncers-to-keep-the-paparazzi-away-on-the-sets-of-jagga-jasoos-250665.html" class="tint" title="Katrina Kaif's Hired Bouncers To Keep The Paparazzi Away On The Sets Of 'Jagga Jasoos'">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/kaaat-crd_1455347665_1455347671_502x234.jpg" border="0" alt="Katrina Kaif's Hired Bouncers To Keep The Paparazzi Away On The Sets Of 'Jagga Jasoos'" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/katrina-kaif-s-hired-bouncers-to-keep-the-paparazzi-away-on-the-sets-of-jagga-jasoos-250665.html" title="Katrina Kaif's Hired Bouncers To Keep The Paparazzi Away On The Sets Of 'Jagga Jasoos'">
                        Katrina Kaif's Hired Bouncers To Keep The Paparazzi Away On The Sets Of 'Jagga Jasoos'                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-reasons-it-s-time-bollywood-made-a-biopic-on-pritilata-waddedar-bengal-s-first-woman-martyr-250573.html" class="tint" title="8 Facts About Pritilata Waddedar - Bengal's First Woman Martyr">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/priti-card_1455176706_1455176715_502x234.jpg" border="0" alt="8 Facts About Pritilata Waddedar - Bengal's First Woman Martyr" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-reasons-it-s-time-bollywood-made-a-biopic-on-pritilata-waddedar-bengal-s-first-woman-martyr-250573.html" title="8 Facts About Pritilata Waddedar - Bengal's First Woman Martyr">
                        8 Facts About Pritilata Waddedar - Bengal's First Woman Martyr                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/these-6-companies-have-promised-to-do-away-with-artificial-colours-in-their-products-250656.html" class="tint" title="These 6 Companies Have Promised To Do Away With Artificial Colours In Their Products">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455283318_502x234.jpg" border="0" alt="These 6 Companies Have Promised To Do Away With Artificial Colours In Their Products" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/these-6-companies-have-promised-to-do-away-with-artificial-colours-in-their-products-250656.html" title="These 6 Companies Have Promised To Do Away With Artificial Colours In Their Products">
                        These 6 Companies Have Promised To Do Away With Artificial Colours In Their Products                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/8-reasons-everyday-needs-to-be-valentine-s-day-250490.html" class="tint" title="8 Reasons Everyday Needs To Be Valentineâs Day">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455021660_502x234.jpg" border="0" alt="8 Reasons Everyday Needs To Be Valentineâs Day" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/8-reasons-everyday-needs-to-be-valentine-s-day-250490.html" title="8 Reasons Everyday Needs To Be Valentineâs Day">
                        8 Reasons Everyday Needs To Be Valentineâs Day                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/style/12-billionaires-and-their-swanky-cars-will-make-you-go-green-with-envy-250266.html" class="tint" title="12 Billionaires And Their Swanky Cars Will Make You Go Green With Envy">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card-srk_1454926138_502x234.jpg" border="0" alt="12 Billionaires And Their Swanky Cars Will Make You Go Green With Envy" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/style/12-billionaires-and-their-swanky-cars-will-make-you-go-green-with-envy-250266.html" title="12 Billionaires And Their Swanky Cars Will Make You Go Green With Envy">
                        12 Billionaires And Their Swanky Cars Will Make You Go Green With Envy                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/12-powerful-ways-in-which-love-is-actually-good-for-your-health-250647.html" class="tint" title="12 Powerful Ways In Which Love Is Actually Good For Your Health!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455278276_502x234.jpg" border="0" alt="12 Powerful Ways In Which Love Is Actually Good For Your Health!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/12-powerful-ways-in-which-love-is-actually-good-for-your-health-250647.html" title="12 Powerful Ways In Which Love Is Actually Good For Your Health!">
                        12 Powerful Ways In Which Love Is Actually Good For Your Health!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/nana-patekar-questions-india-s-caste-and-religion-says-indian-should-be-our-only-religion-250653.html" class="tint" title="Nana Patekar Questions India's Caste And Religion, Says 'Indian' Should Be Our Only Religion">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/408798-nana_1455281342_1455281352_502x234.jpg" border="0" alt="Nana Patekar Questions India's Caste And Religion, Says 'Indian' Should Be Our Only Religion" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/nana-patekar-questions-india-s-caste-and-religion-says-indian-should-be-our-only-religion-250653.html" title="Nana Patekar Questions India's Caste And Religion, Says 'Indian' Should Be Our Only Religion">
                        Nana Patekar Questions India's Caste And Religion, Says 'Indian' Should Be Our Only Religion                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/25-questions-that-will-reveal-your-relationship-intelligence-250492.html" class="tint" title="25 Questions That Will Reveal Your Relationship Intelligence">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/quiz/2016/Feb/1_1455088156_502x234.jpg" border="0" alt="25 Questions That Will Reveal Your Relationship Intelligence" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/25-questions-that-will-reveal-your-relationship-intelligence-250492.html" title="25 Questions That Will Reveal Your Relationship Intelligence">
                        25 Questions That Will Reveal Your Relationship Intelligence                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/if-you-hate-valentine-s-week-with-a-passion-this-guy-is-probably-your-number-one-fan-250489.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/if-you-hate-valentine-s-week-with-a-passion-this-guy-is-probably-your-number-one-fan-250489.html" class="tint" title="If You Hate Valentine's Week With A Passion, This Guy Is Probably Your Number One Fan!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/valentineweek_card_1455019589_502x234.jpg" border="0" alt="If You Hate Valentine's Week With A Passion, This Guy Is Probably Your Number One Fan!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/if-you-hate-valentine-s-week-with-a-passion-this-guy-is-probably-your-number-one-fan-250489.html" title="If You Hate Valentine's Week With A Passion, This Guy Is Probably Your Number One Fan!">
                        If You Hate Valentine's Week With A Passion, This Guy Is Probably Your Number One Fan!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-unknown-people-behind-some-of-india-s-most-iconic-monuments-250388.html" class="tint" title="11 Unknown People Behind Some Of India's Most Iconic Monuments">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/tm_1454909691_502x234.jpg" border="0" alt="11 Unknown People Behind Some Of India's Most Iconic Monuments" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-unknown-people-behind-some-of-india-s-most-iconic-monuments-250388.html" title="11 Unknown People Behind Some Of India's Most Iconic Monuments">
                        11 Unknown People Behind Some Of India's Most Iconic Monuments                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/wow-turns-out-sonakshi-sinha-is-one-fine-doodler-250634.html" class="tint" title="Wow! Turns Out Sonakshi Sinha Is One Fine Doodler!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/picmonkey-collage_1455271155_1455271159_502x234.jpg" border="0" alt="Wow! Turns Out Sonakshi Sinha Is One Fine Doodler!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/wow-turns-out-sonakshi-sinha-is-one-fine-doodler-250634.html" title="Wow! Turns Out Sonakshi Sinha Is One Fine Doodler!">
                        Wow! Turns Out Sonakshi Sinha Is One Fine Doodler!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/arnab-takes-on-jnu-student-activist-on-l-nk-hanumanthappa-says-what-s-on-everyone-s-mind-250632.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/arnab-takes-on-jnu-student-activist-on-l-nk-hanumanthappa-says-what-s-on-everyone-s-mind-250632.html" class="tint" title="Arnab Takes On JNU Student Activist On L/Nk Hanumanthappa, Says What's On Everyone's Mind">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/arnab_card_1455267653_502x234.jpg" border="0" alt="Arnab Takes On JNU Student Activist On L/Nk Hanumanthappa, Says What's On Everyone's Mind" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/arnab-takes-on-jnu-student-activist-on-l-nk-hanumanthappa-says-what-s-on-everyone-s-mind-250632.html" title="Arnab Takes On JNU Student Activist On L/Nk Hanumanthappa, Says What's On Everyone's Mind">
                        Arnab Takes On JNU Student Activist On L/Nk Hanumanthappa, Says What's On Everyone's Mind                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/this-80-year-old-man-did-something-for-people-who-could-not-afford-basic-medication-his-journey-will-make-you-proud-250604.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-80-year-old-man-did-something-for-people-who-could-not-afford-basic-medication-his-journey-will-make-you-proud-250604.html" class="tint" title="This 80-Year-Old Man Did Something For People Who Could Not Afford Basic Medication. His Journey Will Make You Proud.">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/card_1455282536_502x234.jpg" border="0" alt="This 80-Year-Old Man Did Something For People Who Could Not Afford Basic Medication. His Journey Will Make You Proud." class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-80-year-old-man-did-something-for-people-who-could-not-afford-basic-medication-his-journey-will-make-you-proud-250604.html" title="This 80-Year-Old Man Did Something For People Who Could Not Afford Basic Medication. His Journey Will Make You Proud.">
                        This 80-Year-Old Man Did Something For People Who Could Not Afford Basic Medication. His Journey Will Make You Proud.                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/you-should-really-check-out-shield-5-the-first-ever-scripted-series-on-instagram-250639.html" class="tint" title="You Should Really Check Out 'Shield 5', The First-Ever Scripted Series On Instagram">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/picmonkey-collage_1455275464_1455275471_502x234.jpg" border="0" alt="You Should Really Check Out 'Shield 5', The First-Ever Scripted Series On Instagram" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/you-should-really-check-out-shield-5-the-first-ever-scripted-series-on-instagram-250639.html" title="You Should Really Check Out 'Shield 5', The First-Ever Scripted Series On Instagram">
                        You Should Really Check Out 'Shield 5', The First-Ever Scripted Series On Instagram                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                           <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/legendary-professor-asks-the-most-hilarious-questions-to-help-his-students-get-extra-credit-250655.html" class="tint" title="Legendary Professor Asks The Most Hilarious Questions To Help His Students Get Extra Credit">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455282514_218x102.jpg" border="0" alt="Legendary Professor Asks The Most Hilarious Questions To Help His Students Get Extra Credit"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/legendary-professor-asks-the-most-hilarious-questions-to-help-his-students-get-extra-credit-250655.html" title="Legendary Professor Asks The Most Hilarious Questions To Help His Students Get Extra Credit">
                            Legendary Professor Asks The Most Hilarious Questions To Help His Students Get Extra Credit                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>               <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-16"  data-slot="129061" data-position="16" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/technology/the-latest-nigerian-scam-that-s-doing-the-rounds-is-literally-out-of-this-world-250680.html" class="tint" title="The Latest Nigerian Scam That's Doing The Rounds Is Literally Out Of This World!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/card_1455362720_218x102.jpg" border="0" alt="The Latest Nigerian Scam That's Doing The Rounds Is Literally Out Of This World!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/the-latest-nigerian-scam-that-s-doing-the-rounds-is-literally-out-of-this-world-250680.html" title="The Latest Nigerian Scam That's Doing The Rounds Is Literally Out Of This World!">
                            The Latest Nigerian Scam That's Doing The Rounds Is Literally Out Of This World!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/here-s-the-account-of-every-single-rupee-spent-on-modi-s-foreign-visits-to-37-countries-250627.html'>video</a>					
                        <a href="http://www.indiatimes.com/videocafe/here-s-the-account-of-every-single-rupee-spent-on-modi-s-foreign-visits-to-37-countries-250627.html" class="tint" title="Here's The Account Of Every Single Rupee Spent On Modi's Foreign Visits To 37 Countries!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/modiforeignvisits_card_1455263479_218x102.jpg" border="0" alt="Here's The Account Of Every Single Rupee Spent On Modi's Foreign Visits To 37 Countries!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/here-s-the-account-of-every-single-rupee-spent-on-modi-s-foreign-visits-to-37-countries-250627.html" title="Here's The Account Of Every Single Rupee Spent On Modi's Foreign Visits To 37 Countries!">
                            Here's The Account Of Every Single Rupee Spent On Modi's Foreign Visits To 37 Countries!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/fawad-khan-s-little-gig-while-dubbing-for-kapoor-and-sons-will-leave-you-in-splits-250671.html" class="tint" title="Fawad Khan's Little Gig While Dubbing For 'Kapoor And Sons' Will Leave You In Splits!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Feb/fw-sing-card_1455351765_1455351769_218x102.jpg" border="0" alt="Fawad Khan's Little Gig While Dubbing For 'Kapoor And Sons' Will Leave You In Splits!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/fawad-khan-s-little-gig-while-dubbing-for-kapoor-and-sons-will-leave-you-in-splits-250671.html" title="Fawad Khan's Little Gig While Dubbing For 'Kapoor And Sons' Will Leave You In Splits!">
                            Fawad Khan's Little Gig While Dubbing For 'Kapoor And Sons' Will Leave You In Splits!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                               <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/arnab-takes-on-jnu-student-activist-on-l-nk-hanumanthappa-says-what-s-on-everyone-s-mind-250632.html'>video</a>					
                        <a href="http://www.indiatimes.com/videocafe/arnab-takes-on-jnu-student-activist-on-l-nk-hanumanthappa-says-what-s-on-everyone-s-mind-250632.html" class="tint" title="Arnab Takes On JNU Student Activist On L/Nk Hanumanthappa, Says What's On Everyone's Mind">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/arnab_card_1455267653_218x102.jpg" border="0" alt="Arnab Takes On JNU Student Activist On L/Nk Hanumanthappa, Says What's On Everyone's Mind"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/arnab-takes-on-jnu-student-activist-on-l-nk-hanumanthappa-says-what-s-on-everyone-s-mind-250632.html" title="Arnab Takes On JNU Student Activist On L/Nk Hanumanthappa, Says What's On Everyone's Mind">
                            Arnab Takes On JNU Student Activist On L/Nk Hanumanthappa, Says What's On Everyone's Mind                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '250527', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2016/Feb/wall640_1455536854_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/na1_1455535991_1455535995_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455534915_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455534993_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/tit600_1455525620_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/screenshot_3_1455534897_1455534904_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/si-cover-main-image-2-2016_1455531913_1455531919_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/tinderdate_card_1455174200_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/tk_1455434520_1455434526_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/a123_1455430870_1455430876_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/as_1455518275_1455518281_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/ads_1455200150_1455200165_218x102.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/card_1455443974_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2016/Feb/card_1455532291_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455524785_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/jnu_1455527786_1455527792_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/600_1455517789_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455521297_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455532177_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455532838_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455282148_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455358758_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/sd_1455434835_1455434845_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/thriller-card_1455289996_1455290001_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455191102_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_final_1455362414_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2016/Feb/cg640_1455523154_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/ja-card_1455516290_1455516298_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/messi640_1455512606_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/rabindra--800_1455519037_1455519043_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/600_1455513235_236x111.jpg","http://media.indiatimes.in/media/content/2016/Feb/13-jiah-khan-b-130613_1455523505_1455523515_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/picmonkey-collage_1455519071_1455519076_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/picmonkey-collage_1455516520_1455516527_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455261643_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455530008_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/asa_1455199466_1455199478_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455195836_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/acidattack1_1455465606_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/1-carmd-1_1455171298_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/rtr1vz47_1455447271_1455447275_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/srk_1455453237_1455453248_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/card_1455443974_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/ssa_1455021299_1455021308_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/zx_1455438402_1455438417_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/as_1455518275_1455518281_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/cover_1454413078_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/tk_1455434520_1455434526_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/a123_1455430870_1455430876_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/sd_1455434835_1455434845_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/jdf1_1455089029_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/fcc_1455427182_1455427191_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455099624_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/theblanket_card_1455187702_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/diefb_1441348214_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455191102_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/thriller-card_1455289996_1455290001_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_final_1455362414_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/hip-hip-card_1455369657_1455369663_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455362720_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/image_1455440326_1455440335_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455358758_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/card-3_1454502640_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455282514_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/ads_1455200150_1455200165_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/fw-sing-card_1455351765_1455351769_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/cover_1455284197_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/kaaat-crd_1455347665_1455347671_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/priti-card_1455176706_1455176715_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455283318_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455021660_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card-srk_1454926138_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455278276_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/408798-nana_1455281342_1455281352_502x234.jpg","http://media.indiatimes.in/media/quiz/2016/Feb/1_1455088156_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/valentineweek_card_1455019589_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/tm_1454909691_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/picmonkey-collage_1455271155_1455271159_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/arnab_card_1455267653_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/card_1455282536_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/picmonkey-collage_1455275464_1455275471_502x234.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455282514_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/card_1455362720_218x102.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/modiforeignvisits_card_1455263479_218x102.jpg","http://media.indiatimes.in/media/content/2016/Feb/fw-sing-card_1455351765_1455351769_218x102.jpg","http://media.indiatimes.in/media/videocafe/2016/Feb/arnab_card_1455267653_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    4,461,321<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>52,393 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2016 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    function socialRHSHide(){}
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.92" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.92"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.92"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.92"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.92"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>