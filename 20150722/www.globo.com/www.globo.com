



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='21/07/2015 22:09:30' /><meta property='busca:modified' content='21/07/2015 22:09:30' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/10dd23bf1f59.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/12ba0da8a716.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class=""><div class="v-separator"></div><a target="_top" href="http://m.g1.globo.com" accesskey="n" class="barra-item-g1 link-produto">g1</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://m.globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto">globoesporte</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto">gshow</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto">famosos &amp; etc</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://m.techtudo.com.br" accesskey="b" class="barra-item-tech link-produto">tecnologia</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "globo.com/globo.com/home", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo" href="http://m.globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://fantastico.globo.com/">
                                                <span class="titulo">FantÃ¡stico</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/radar-g1/platb/">
                                                    <span class="titulo">TrÃ¢nsito</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                <span class="titulo">Copa do Brasil</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/volei/">
                                                <span class="titulo">VÃ´lei</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-america/">
                                                    <span class="titulo">Copa AmÃ©rica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                <span class="titulo">Estrelas</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/superstar/2015">
                                                <span class="titulo">SuperStar</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/esquenta/">
                                                    <span class="titulo">Esquenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/superstar/2015">
                                                    <span class="titulo">SuperStar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/pe-na-cova/index.html">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistacasaejardim.globo.com/">
                                                <span class="titulo">Revista Casa e Jardim</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-07-2122:09:55Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"><div class="glb-box-eventos-maker"><div class="glb-container" style="border-color: #FF7400;"><a class="content-box-evento" href="http://globoesporte.globo.com/cartola-fc/"><span class="logo" style="background: url(http://s2.glbimg.com/J7EmQ8qEpKZNWcwDO_oDRzAgYzs=/0x0:100x75/100x75/s.glbimg.com/en/ho/f/original/2015/05/18/sprite.png) 0 0 no-repeat; width: 100px; height: 75px;"></span><span class="title-parent"><span class="logo-title" style="color: #FF7400;">mercado aberto</span><span class="title" data-hover-color="#FF7400">Veja como estÃ¡ sua pontuaÃ§Ã£o e escale jogadores para a prÃ³xima rodada</span></span><span class="subtitle" style="color: #FF7400;">monte o seu time<span class="arrow"> âº</span></span></a></div></div></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/07/supremo-informa-que-dilma-vetou-reajuste-para-servidores-do-judiciario.html" class=" " title="Supremo afirma que Dilma vetou reajuste para o judiciÃ¡rio"><div class="conteudo"><h2>Supremo afirma que Dilma vetou reajuste para o judiciÃ¡rio</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/07/cunha-pede-celeridade-para-ministro-decidir-sobre-envio-de-acao-ao-stf.html" class=" " title="Cunha encontra presidente do STF e pede decisÃ£o rÃ¡pida sobre aÃ§Ã£o"><div class="conteudo"><h2>Cunha encontra presidente do STF<br /> e pede decisÃ£o rÃ¡pida sobre aÃ§Ã£o</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Supremo pede que Moro esclareÃ§a citaÃ§Ã£o de Cunha" href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/07/stf-pede-para-moro-esclarecer-citacao-de-cunha-em-depoimento.html">Supremo pede que Moro esclareÃ§a citaÃ§Ã£o de Cunha</a></div></li></ul></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/economia/empresas-tem-de-esgotar-banco-de-horas-ferias-para-participar-de-plano-de-protecao-ao-emprego-16867271" class=" " title="Governo define regras de plano antidemissÃµes"><div class="conteudo"><h2>Governo define regras de plano antidemissÃµes</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/sao-paulo/noticia/2015/07/governo-de-sp-passara-operacao-da-linha-5-do-metro-iniciativa-privada.html" class=" " title="SP privatizarÃ¡ linha 5 do metrÃ´ atrasada 3 anos (Tatiana Santiago/G1)"><div class="conteudo"><h2>SP privatizarÃ¡ linha 5 do metrÃ´ atrasada 3 anos</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/sociedade/ciencia/nasa-divulga-nova-foto-da-terra-16862800" class="foto " title="Veja a 1Âª foto da Ã¡rea iluminada da Terra inteira (Nasa)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/QG7yVWuw5uLkh7rPAYu8Dl9N_j0=/filters:quality(10):strip_icc()/s2.glbimg.com/1QS1ApwaoifSgCI99CB6UUXEknM=/0x502:2048x1493/155x75/e.glbimg.com/og/ed/f/original/2015/07/20/187_1003705_americas_dxm.png" alt="Veja a 1Âª foto da Ã¡rea iluminada da Terra inteira (Nasa)" title="Veja a 1Âª foto da Ã¡rea iluminada da Terra inteira (Nasa)"
         data-original-image="s2.glbimg.com/1QS1ApwaoifSgCI99CB6UUXEknM=/0x502:2048x1493/155x75/e.glbimg.com/og/ed/f/original/2015/07/20/187_1003705_americas_dxm.png" data-url-smart_horizontal="jxXNyuOqryMO5ABkgw0izXU56og=/90x56/smart/filters:strip_icc()/" data-url-smart="jxXNyuOqryMO5ABkgw0izXU56og=/90x56/smart/filters:strip_icc()/" data-url-feature="jxXNyuOqryMO5ABkgw0izXU56og=/90x56/smart/filters:strip_icc()/" data-url-tablet="saHKuyBx2slNmrICCqyE54S6tBA=/160xorig/smart/filters:strip_icc()/" data-url-desktop="AQDqpwaqY3WwqJ2MX8IemYyY_LM=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Veja a 1Âª foto da Ã¡rea iluminada da Terra inteira</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/am/amazonas/noticia/2015/07/mais-de-30-sao-detidos-em-flagrante-em-operacao-da-pm-em-manaus.html" class="foto " title="ApÃ³s mortes em sÃ©rie, operaÃ§Ã£o prende 32 no AM (DivulgaÃ§Ã£o/PolÃ­cia Militar)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/oHx7iDqC7eRwYS4G0KYVXCKiyLY=/filters:quality(10):strip_icc()/s2.glbimg.com/N1WfSrnMID1wtX698qerGo4j8jE=/22x144:864x551/155x75/s.glbimg.com/jo/g1/f/original/2015/07/21/20150721100959_1.jpg" alt="ApÃ³s mortes em sÃ©rie, operaÃ§Ã£o prende 32 no AM (DivulgaÃ§Ã£o/PolÃ­cia Militar)" title="ApÃ³s mortes em sÃ©rie, operaÃ§Ã£o prende 32 no AM (DivulgaÃ§Ã£o/PolÃ­cia Militar)"
         data-original-image="s2.glbimg.com/N1WfSrnMID1wtX698qerGo4j8jE=/22x144:864x551/155x75/s.glbimg.com/jo/g1/f/original/2015/07/21/20150721100959_1.jpg" data-url-smart_horizontal="gRKuf4mZWWk-3uCYWlaM9wrgDYc=/90x56/smart/filters:strip_icc()/" data-url-smart="gRKuf4mZWWk-3uCYWlaM9wrgDYc=/90x56/smart/filters:strip_icc()/" data-url-feature="gRKuf4mZWWk-3uCYWlaM9wrgDYc=/90x56/smart/filters:strip_icc()/" data-url-tablet="FiCYEwO-NxMOrZWlawgPyE58k3A=/160xorig/smart/filters:strip_icc()/" data-url-desktop="7tCEuUKElCLZxwPZyIOpeZIynF0=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>ApÃ³s mortes em sÃ©rie, operaÃ§Ã£o prende 32 no AM</h2></div></a></div></div></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/libertadores/jogo/21-07-2015/guarani-do-paraguai-river-plate/" class="foto " title="GuaranÃ­ e River decidem finalista da Liberta; SIGA (Reuters)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/jOD021ZHxL6hdG6GeRsQnYXkfHw=/filters:quality(10):strip_icc()/s2.glbimg.com/CWYkZsch-NkgVuLyhIG7XyNIjS8=/269x44:1632x1152/155x126/s.glbimg.com/es/ge/f/original/2015/07/21/gabriel_mercado_julian_benitez_river_plate_guarani_libertadores.jpg" alt="GuaranÃ­ e River decidem finalista da Liberta; SIGA (Reuters)" title="GuaranÃ­ e River decidem finalista da Liberta; SIGA (Reuters)"
         data-original-image="s2.glbimg.com/CWYkZsch-NkgVuLyhIG7XyNIjS8=/269x44:1632x1152/155x126/s.glbimg.com/es/ge/f/original/2015/07/21/gabriel_mercado_julian_benitez_river_plate_guarani_libertadores.jpg" data-url-smart_horizontal="aEkmEa-1DI0KmT6_5QeWZmz6eMk=/90x56/smart/filters:strip_icc()/" data-url-smart="aEkmEa-1DI0KmT6_5QeWZmz6eMk=/90x56/smart/filters:strip_icc()/" data-url-feature="aEkmEa-1DI0KmT6_5QeWZmz6eMk=/90x56/smart/filters:strip_icc()/" data-url-tablet="ldaIYUYoG6qzYXvJ3N_qh3OzG20=/160xorig/smart/filters:strip_icc()/" data-url-desktop="8HmiBHBVEKqP1BCaS1xfWSOKJYY=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>GuaranÃ­ e River decidem finalista da Liberta; SIGA</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/sc/futebol/copa-do-brasil/jogo/21-07-2015/criciuma-gremio/" class="foto " title="GrÃªmio faz 1Âº no CriciÃºma; siga a Copa do Brasil (ReproduÃ§Ã£o/RBS TV)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/sj-S0sH_Q6cnlfHxtEjqwqI-JLs=/filters:quality(10):strip_icc()/s2.glbimg.com/NhlskZfEEVJOcmyfA5F47oi1oxY=/461x13:896x367/155x126/s.glbimg.com/es/ge/f/original/2015/07/21/screen_shot_2015-07-21_at_9.43.48_pm.png" alt="GrÃªmio faz 1Âº no CriciÃºma; siga a Copa do Brasil (ReproduÃ§Ã£o/RBS TV)" title="GrÃªmio faz 1Âº no CriciÃºma; siga a Copa do Brasil (ReproduÃ§Ã£o/RBS TV)"
         data-original-image="s2.glbimg.com/NhlskZfEEVJOcmyfA5F47oi1oxY=/461x13:896x367/155x126/s.glbimg.com/es/ge/f/original/2015/07/21/screen_shot_2015-07-21_at_9.43.48_pm.png" data-url-smart_horizontal="vLl8-vm3LiUPG4vtD1G_PzIJz4c=/90x56/smart/filters:strip_icc()/" data-url-smart="vLl8-vm3LiUPG4vtD1G_PzIJz4c=/90x56/smart/filters:strip_icc()/" data-url-feature="vLl8-vm3LiUPG4vtD1G_PzIJz4c=/90x56/smart/filters:strip_icc()/" data-url-tablet="K1z5gxnmcIfZWEJS3j8yw07wnhQ=/160xorig/smart/filters:strip_icc()/" data-url-desktop="DRwudEHSHCcZwPS2tN2szN9T7fM=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>GrÃªmio faz 1Âº no CriciÃºma; siga <br />a Copa do Brasil</h2></div></a></div></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/jogos-pan-americanos/no-ar/brasil-x-mexico-final-do-volei-de-praia-jogos-pan-americanos-de-toronto.html" class=" " title="Brasil disputa o tÃ­tulo no vÃ´lei de praia; tempo real (William Lucas/inovafoto)"><div class="conteudo"><h2>Brasil disputa o tÃ­tulo no vÃ´lei de praia; tempo real</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/jogos-pan-americanos/no-ar/jogos-pan-americanos-toronto-2015.html#/glb-feed-post/55aee98ac22d627b996e08de" class=" " title="Brasil conquista o ouro no tÃªnis de mesa do Pan (AP)"><div class="conteudo"><h2>Brasil conquista o ouro no tÃªnis de mesa do Pan</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/babilonia/Vem-por-ai/noticia/2015/07/ivan-e-sergio-tem-jantar-romantico.html" class="foto " title="Ivan passa a 1Âª noite com SÃ©rgio (InÃ¡cio Moraes/Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/WT1dEJK98g2rNm2RxNCWNSp2FYw=/filters:quality(10):strip_icc()/s2.glbimg.com/JvrXdHTFKS_duc2SGbJVcw7qUd0=/0x53:690x320/155x60/s.glbimg.com/et/gs/f/original/2015/07/20/ivan-e-sergio.jpg" alt="Ivan passa a 1Âª noite com SÃ©rgio (InÃ¡cio Moraes/Gshow)" title="Ivan passa a 1Âª noite com SÃ©rgio (InÃ¡cio Moraes/Gshow)"
         data-original-image="s2.glbimg.com/JvrXdHTFKS_duc2SGbJVcw7qUd0=/0x53:690x320/155x60/s.glbimg.com/et/gs/f/original/2015/07/20/ivan-e-sergio.jpg" data-url-smart_horizontal="LWVT4FDqwDssrxO5so_O_HlXibA=/90x56/smart/filters:strip_icc()/" data-url-smart="LWVT4FDqwDssrxO5so_O_HlXibA=/90x56/smart/filters:strip_icc()/" data-url-feature="LWVT4FDqwDssrxO5so_O_HlXibA=/90x56/smart/filters:strip_icc()/" data-url-tablet="KdtbrBKYnOBgUQCGHJ7yruGyDzM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="46rZLggFfejpdmSlPfLdj_mtQWU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Ivan passa a 1Âª noite com SÃ©rgio</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="VinÃ­cius dÃ¡ chance a Cris" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/07/vinicius-decidira-dar-nova-chance-cris.html">VinÃ­cius dÃ¡ chance a Cris</a></div></li><li><div class="mobile-grid-partial"><a title="Cris tenta matar modelo" href="http://extra.globo.com/tv-e-lazer/telinha/babilonia-cris-tenta-matar-modelo-para-incriminar-regina-16853490.html">Cris tenta matar modelo</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/verdades-secretas/Vem-por-ai/noticia/2015/07/alex-deixa-angel-arrasada-durante-jantar-veja-previa-do-capitulo.html" class="foto " title="&#39;Verdades&#39;: Alex choca Angel (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/GQTPTNRUxTa0wbG4ebqp3lHlkUU=/filters:quality(10):strip_icc()/s2.glbimg.com/ddeKxPDPVgwvffILWB8c3lfMepw=/0x111:690x378/155x60/s.glbimg.com/et/gs/f/original/2015/07/21/angel.jpg" alt="&#39;Verdades&#39;: Alex choca Angel (TV Globo)" title="&#39;Verdades&#39;: Alex choca Angel (TV Globo)"
         data-original-image="s2.glbimg.com/ddeKxPDPVgwvffILWB8c3lfMepw=/0x111:690x378/155x60/s.glbimg.com/et/gs/f/original/2015/07/21/angel.jpg" data-url-smart_horizontal="tbF0OYjjf7HW6X_S4x3ugIjyeoY=/90x56/smart/filters:strip_icc()/" data-url-smart="tbF0OYjjf7HW6X_S4x3ugIjyeoY=/90x56/smart/filters:strip_icc()/" data-url-feature="tbF0OYjjf7HW6X_S4x3ugIjyeoY=/90x56/smart/filters:strip_icc()/" data-url-tablet="bSrB6tFvwUNP3-3KkaHj75ArlHU=/160xorig/smart/filters:strip_icc()/" data-url-desktop="n8S-r7r94DVov48kfnEIhl2Uefc=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Verdades&#39;: Alex choca Angel</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Alex fala verdade a Angel" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/07/alex-admitira-que-se-casou-com-carolina-por-causa-de-angel.html">Alex fala verdade a Angel</a></div></li><li><div class="mobile-grid-partial"><a title="Carolina esnoba RogÃ©rio" href="http://extra.globo.com/tv-e-lazer/telinha/verdades-secretas-carolina-esnoba-ex-devolve-carro-rogerio-pensa-em-tirar-vantagem-16847183.html">Carolina esnoba RogÃ©rio</a></div></li></ul></div></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/">encontro com fÃ¡tima</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/entenda-o-funcionamento-da-memoria-humana/4336155/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/entenda-o-funcionamento-da-memoria-humana/4336155/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4336155.jpg" alt="MÃ©dico explica o funcionamento da memÃ³ria humana; entenda" /><div class="time">05:12</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">Ã¡reas importantes para cada situaÃ§Ã£o</span><span class="title">MÃ©dico explica o funcionamento da memÃ³ria humana; entenda</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/amnesia-digital-marcos-veras-da-dica-para-nao-esquecer-senhas/4336113/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/amnesia-digital-marcos-veras-da-dica-para-nao-esquecer-senhas/4336113/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4336113.jpg" alt="Esqueceu? Marcos Veras dÃ¡ dica para ajudar a lembrar senhas" /><div class="time">03:32</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">AmnÃ©sia Digital</span><span class="title">Esqueceu? Marcos Veras dÃ¡ dica para ajudar a lembrar senhas</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/desafio-da-memoria-gabriel-leone-acertou-em-cheio-as-respostas/4336162/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/desafio-da-memoria-gabriel-leone-acertou-em-cheio-as-respostas/4336162/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4336162.jpg" alt="Bruna Linzmeyer e Gabriel Leone passam por desafio com senha" /><div class="time">01:24</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">teste de memÃ³ria</span><span class="title">Bruna Linzmeyer e Gabriel Leone passam por desafio com senha</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/entenda-o-funcionamento-da-memoria-humana/4336155/"><span class="subtitle">Ã¡reas importantes para cada situaÃ§Ã£o</span><span class="title">MÃ©dico explica o funcionamento da memÃ³ria humana; entenda</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/amnesia-digital-marcos-veras-da-dica-para-nao-esquecer-senhas/4336113/"><span class="subtitle">AmnÃ©sia Digital</span><span class="title">Esqueceu? Marcos Veras dÃ¡ dica para ajudar a lembrar senhas</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/desafio-da-memoria-gabriel-leone-acertou-em-cheio-as-respostas/4336162/"><span class="subtitle">teste de memÃ³ria</span><span class="title">Bruna Linzmeyer e Gabriel Leone passam por desafio com senha</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-entretenimento"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/entenda-o-funcionamento-da-memoria-humana/4336155/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/amnesia-digital-marcos-veras-da-dica-para-nao-esquecer-senhas/4336113/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/desafio-da-memoria-gabriel-leone-acertou-em-cheio-as-respostas/4336162/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais<strong> vÃ­deos</strong><span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://oglobo.globo.com/rio/ladraoflagrado-em-nova-tentativa-de-assalto-na-avenida-presidente-vargas-no-centro-16875520" class="foto" title="De fora, ladrÃ£o tenta roubar idosa dentro de Ã´nibus no Rio; veja como foi o flagra (Domingos Peixoto / AgÃªncia O Globo)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/EQ1Xjh9DUxRAmset9aNF0pxgsaY=/filters:quality(10):strip_icc()/s2.glbimg.com/c_Nbb0qJYSPsWB9vp1wyFhwXfKc=/0x42:698x417/335x180/s.glbimg.com/en/ho/f/original/2015/07/21/flagraoni.jpg" alt="De fora, ladrÃ£o tenta roubar idosa dentro de Ã´nibus no Rio; veja como foi o flagra (Domingos Peixoto / AgÃªncia O Globo)" title="De fora, ladrÃ£o tenta roubar idosa dentro de Ã´nibus no Rio; veja como foi o flagra (Domingos Peixoto / AgÃªncia O Globo)"
                data-original-image="s2.glbimg.com/c_Nbb0qJYSPsWB9vp1wyFhwXfKc=/0x42:698x417/335x180/s.glbimg.com/en/ho/f/original/2015/07/21/flagraoni.jpg" data-url-smart_horizontal="XWbRpFGGT09YNn79muBnWP7Crpo=/90x0/smart/filters:strip_icc()/" data-url-smart="XWbRpFGGT09YNn79muBnWP7Crpo=/90x0/smart/filters:strip_icc()/" data-url-feature="XWbRpFGGT09YNn79muBnWP7Crpo=/90x0/smart/filters:strip_icc()/" data-url-tablet="yeqqPeXkYR87j6Q7UioTc10dLn0=/220x125/smart/filters:strip_icc()/" data-url-desktop="MTvA3ncp-ccwXzRhUdygTKDyTsQ=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>De fora, ladrÃ£o tenta roubar idosa dentro de Ã´nibus no Rio; veja como foi o flagra</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 16:07:46" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/casos-de-policia/policia-descarta-sequestro-no-caso-de-estudante-de-marica-16868392.html" class="foto" title="PolÃ­cia descarta rapto; garota de 11 teria admitido ter se envolvido com jovem de 20 (Wilson Mendes)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/vEomsdw9UY8u0fo_6pxF8dP5Ar4=/filters:quality(10):strip_icc()/s2.glbimg.com/-RYtJk0BWjnVxeBYsa6PAempXoE=/160x57:483x273/120x80/s.glbimg.com/en/ho/f/original/2015/07/20/reecontro-familia-rio_.jpg" alt="PolÃ­cia descarta rapto; garota de 11 teria admitido ter se envolvido com jovem de 20 (Wilson Mendes)" title="PolÃ­cia descarta rapto; garota de 11 teria admitido ter se envolvido com jovem de 20 (Wilson Mendes)"
                data-original-image="s2.glbimg.com/-RYtJk0BWjnVxeBYsa6PAempXoE=/160x57:483x273/120x80/s.glbimg.com/en/ho/f/original/2015/07/20/reecontro-familia-rio_.jpg" data-url-smart_horizontal="1AtuFfj3rwQ6XZA08j4I5f4UIN4=/90x0/smart/filters:strip_icc()/" data-url-smart="1AtuFfj3rwQ6XZA08j4I5f4UIN4=/90x0/smart/filters:strip_icc()/" data-url-feature="1AtuFfj3rwQ6XZA08j4I5f4UIN4=/90x0/smart/filters:strip_icc()/" data-url-tablet="x8i1bIb2f97MHRrihhN7xk5xB_4=/70x50/smart/filters:strip_icc()/" data-url-desktop="WDtUbw_yEwLRhEcxKuAW_s9Zaxs=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PolÃ­cia descarta rapto; garota de 11 teria admitido ter se envolvido com jovem de 20</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 18:52:39" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/santos-regiao/noticia/2015/07/suspeito-e-flagrado-pulando-muro-de-predio-para-abusar-de-criancas-video.html" class="foto" title="Suspeito Ã© flagrado pulando muro de prÃ©dio para abusar de crianÃ§as em Santos; veja (ReproduÃ§Ã£o/G1)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/MmYqPfDlld5Vbc-C4Ob4lZNC_Vc=/filters:quality(10):strip_icc()/s2.glbimg.com/tb1-O-8WcWK75j2hF_Df7POSFM4=/294x205:620x422/120x80/s.glbimg.com/jo/g1/f/original/2015/07/21/frame_abuso.jpg" alt="Suspeito Ã© flagrado pulando muro de prÃ©dio para abusar de crianÃ§as em Santos; veja (ReproduÃ§Ã£o/G1)" title="Suspeito Ã© flagrado pulando muro de prÃ©dio para abusar de crianÃ§as em Santos; veja (ReproduÃ§Ã£o/G1)"
                data-original-image="s2.glbimg.com/tb1-O-8WcWK75j2hF_Df7POSFM4=/294x205:620x422/120x80/s.glbimg.com/jo/g1/f/original/2015/07/21/frame_abuso.jpg" data-url-smart_horizontal="7J9qDFrHNCh5SAaSy3q2kGmryfI=/90x0/smart/filters:strip_icc()/" data-url-smart="7J9qDFrHNCh5SAaSy3q2kGmryfI=/90x0/smart/filters:strip_icc()/" data-url-feature="7J9qDFrHNCh5SAaSy3q2kGmryfI=/90x0/smart/filters:strip_icc()/" data-url-tablet="5jVs3hzA5589dH1k_D0YLpwXIXs=/70x50/smart/filters:strip_icc()/" data-url-desktop="-IcTvFg8f2lEn14Sir6Cq-BiEzQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Suspeito Ã© flagrado pulando muro de prÃ©dio para abusar de crianÃ§as em Santos; veja</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 20:52:47" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/goias/noticia/2015/07/empresaria-e-filho-sao-encontrados-mortos-dentro-de-casa-em-goias.html" class="" title="EmpresÃ¡ria e filho sÃ£o encontrados mortos em casa em GO; companheiro da mulher Ã© suspeito" rel="bookmark"><span class="conteudo"><p>EmpresÃ¡ria e filho sÃ£o encontrados mortos em casa em GO; companheiro da mulher Ã© suspeito</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 15:40:49" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/campinas-regiao/noticia/2015/07/clube-demite-jogador-de-futebol-que-filmou-debaixo-de-saia-de-adolescente-americana.html" class="foto" title="Jogador que teria filmado sob saia de menor em fila de mercado Ã© demitido em SP (Assessoria de Imprensa / Rio Branco)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/p8rEK3wPQ9dmbc3yP1vaobnT2fo=/filters:quality(10):strip_icc()/s2.glbimg.com/D2w2vGVSeM1EdMj6wmymGpL-YVw=/89x9:457x254/120x80/s.glbimg.com/jo/g1/f/original/2015/07/21/luizriobranco.jpg" alt="Jogador que teria filmado sob saia de menor em fila de mercado Ã© demitido em SP (Assessoria de Imprensa / Rio Branco)" title="Jogador que teria filmado sob saia de menor em fila de mercado Ã© demitido em SP (Assessoria de Imprensa / Rio Branco)"
                data-original-image="s2.glbimg.com/D2w2vGVSeM1EdMj6wmymGpL-YVw=/89x9:457x254/120x80/s.glbimg.com/jo/g1/f/original/2015/07/21/luizriobranco.jpg" data-url-smart_horizontal="inSYL6BnDZ9vLntj2PWSNh_6NZ8=/90x0/smart/filters:strip_icc()/" data-url-smart="inSYL6BnDZ9vLntj2PWSNh_6NZ8=/90x0/smart/filters:strip_icc()/" data-url-feature="inSYL6BnDZ9vLntj2PWSNh_6NZ8=/90x0/smart/filters:strip_icc()/" data-url-tablet="tgmOiOaXlgmJiDvFLNApEDf8hnc=/70x50/smart/filters:strip_icc()/" data-url-desktop="vqnpuzsQmOqdRjJHbVlaQLRyLAE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jogador que teria filmado sob saia de menor em fila de mercado Ã© demitido em  SP</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 19:56:45" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/07/moto-g-3-tera-o-dobro-de-memoria-ram-diz-site.html" class="foto" title="Novo Moto G, baratinho da Motorola, pode ter cÃ¢mera 
&#39;top&#39; e muito mais memÃ³ria (Moto G tambÃ©m oferece trÃªs opÃ§Ãµes com versÃ£o bÃ¡sica, TV Digital e 4G (Foto: Isadora DÃ­az/TechTudo))" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/_0n0277InajOrVOXbs2tSCSHpUY=/filters:quality(10):strip_icc()/s2.glbimg.com/MvSs1xNxc68l0cvfbC0BzN3h87k=/0x0:899x600/120x80/s.glbimg.com/po/tt2/f/original/2015/03/12/moto-g-tela.jpg" alt="Novo Moto G, baratinho da Motorola, pode ter cÃ¢mera 
&#39;top&#39; e muito mais memÃ³ria (Moto G tambÃ©m oferece trÃªs opÃ§Ãµes com versÃ£o bÃ¡sica, TV Digital e 4G (Foto: Isadora DÃ­az/TechTudo))" title="Novo Moto G, baratinho da Motorola, pode ter cÃ¢mera 
&#39;top&#39; e muito mais memÃ³ria (Moto G tambÃ©m oferece trÃªs opÃ§Ãµes com versÃ£o bÃ¡sica, TV Digital e 4G (Foto: Isadora DÃ­az/TechTudo))"
                data-original-image="s2.glbimg.com/MvSs1xNxc68l0cvfbC0BzN3h87k=/0x0:899x600/120x80/s.glbimg.com/po/tt2/f/original/2015/03/12/moto-g-tela.jpg" data-url-smart_horizontal="wC6c05IDEJyxL2DeeJN-TgTTyFI=/90x0/smart/filters:strip_icc()/" data-url-smart="wC6c05IDEJyxL2DeeJN-TgTTyFI=/90x0/smart/filters:strip_icc()/" data-url-feature="wC6c05IDEJyxL2DeeJN-TgTTyFI=/90x0/smart/filters:strip_icc()/" data-url-tablet="QU1qdc4GJhcqRSR-fUV5dsjp7c4=/70x50/smart/filters:strip_icc()/" data-url-desktop="muMDc4i69vRwdfJzhAh_Joq9_L0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Novo Moto G, baratinho da Motorola, pode ter cÃ¢mera <br />&#39;top&#39; e muito mais memÃ³ria</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 19:17:49" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/bahia/noticia/2015/07/jovem-se-finge-de-morta-e-escapa-de-atentado-na-bahia-namorado-morreu.html" class="" title="Garota de 17 anos se finge de morta e escapa de atentado que matou o namorado na Bahia (ReproduÃ§Ã£o/TV Tem)" rel="bookmark"><span class="conteudo"><p>Garota de 17 anos se finge de morta e escapa<br /> de atentado que matou o namorado na Bahia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 18:50:13" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rs/rio-grande-do-sul/noticia/2015/07/homem-usa-onibus-para-salvar-mais-de-100-caes-de-enchente-no-rs.html" class="foto" title="Morador usa Ã´nibus para salvar mais de 100 cÃ£es de enchente no RS; veja vÃ­deo (G1)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/iQavW8hmygFfgbISwiXKr27Z00Y=/filters:quality(10):strip_icc()/s2.glbimg.com/dHjS_Lm0cSNeF6QAEQafHdaXRhE=/0x201:298x400/120x80/s.glbimg.com/en/ho/f/original/2015/07/21/rs-homem-usa-onibus-para-salvar-cachorros-enchente.jpg" alt="Morador usa Ã´nibus para salvar mais de 100 cÃ£es de enchente no RS; veja vÃ­deo (G1)" title="Morador usa Ã´nibus para salvar mais de 100 cÃ£es de enchente no RS; veja vÃ­deo (G1)"
                data-original-image="s2.glbimg.com/dHjS_Lm0cSNeF6QAEQafHdaXRhE=/0x201:298x400/120x80/s.glbimg.com/en/ho/f/original/2015/07/21/rs-homem-usa-onibus-para-salvar-cachorros-enchente.jpg" data-url-smart_horizontal="GVUkFvT-glhIqXQosgUEaVmBSHY=/90x0/smart/filters:strip_icc()/" data-url-smart="GVUkFvT-glhIqXQosgUEaVmBSHY=/90x0/smart/filters:strip_icc()/" data-url-feature="GVUkFvT-glhIqXQosgUEaVmBSHY=/90x0/smart/filters:strip_icc()/" data-url-tablet="OUI5MmV2j3ud6P0i7unwHgjmZhI=/70x50/smart/filters:strip_icc()/" data-url-desktop="RxarCNWQLhA6IcF6vbGxwEd-60E=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Morador usa Ã´nibus para <br />salvar mais de 100 cÃ£es de enchente no RS; veja vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 14:31:18" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rr/roraima/noticia/2015/07/estudante-de-17-anos-e-aprovado-em-2-lugar-no-concurso-do-tre-de-rr.html" class="foto" title="Estudante de 17 anos Ã© aprovado em segundo lugar em concurso do TRE (Emily Costa/ G1 RR)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/kH3gxpE5CD4GuyaxExAWM4TEtx8=/filters:quality(10):strip_icc()/s2.glbimg.com/YJQUJox16ol-hC16BtYB1Jh0TX8=/1180x527:4009x2413/120x80/s.glbimg.com/jo/g1/f/original/2015/07/21/dsc_6631.jpg" alt="Estudante de 17 anos Ã© aprovado em segundo lugar em concurso do TRE (Emily Costa/ G1 RR)" title="Estudante de 17 anos Ã© aprovado em segundo lugar em concurso do TRE (Emily Costa/ G1 RR)"
                data-original-image="s2.glbimg.com/YJQUJox16ol-hC16BtYB1Jh0TX8=/1180x527:4009x2413/120x80/s.glbimg.com/jo/g1/f/original/2015/07/21/dsc_6631.jpg" data-url-smart_horizontal="cRUYcHQAM55uaTjOA1iq89BnpOY=/90x0/smart/filters:strip_icc()/" data-url-smart="cRUYcHQAM55uaTjOA1iq89BnpOY=/90x0/smart/filters:strip_icc()/" data-url-feature="cRUYcHQAM55uaTjOA1iq89BnpOY=/90x0/smart/filters:strip_icc()/" data-url-tablet="Bco6_xM0Pp5XjMmS9izxk1IgNmE=/70x50/smart/filters:strip_icc()/" data-url-desktop="ePNJQgeFYTMNgdDctIOWBBQOWrA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Estudante de 17 anos Ã© aprovado em segundo <br />lugar em concurso do TRE</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/rs/futebol/libertadores/noticia/2015/07/imprensa-mexicana-exalta-valdivia-e-sasha-prontos-para-liquidar-o-tigres.html" class="foto" title="Imprensa mexicana dÃ¡ destaque a dupla do Inter: &#39;Prontos para liquidar&#39; o Tigres (Diego Guichard)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/0noFVuTPNuAToVxH0NMJc4UA8jw=/filters:quality(10):strip_icc()/s2.glbimg.com/RhgoVU_yzuU9QmydJ2_8ftvLW4U=/0x125:1999x1198/335x180/s.glbimg.com/es/ge/f/original/2015/07/21/jor2.jpg" alt="Imprensa mexicana dÃ¡ destaque a dupla do Inter: &#39;Prontos para liquidar&#39; o Tigres (Diego Guichard)" title="Imprensa mexicana dÃ¡ destaque a dupla do Inter: &#39;Prontos para liquidar&#39; o Tigres (Diego Guichard)"
                data-original-image="s2.glbimg.com/RhgoVU_yzuU9QmydJ2_8ftvLW4U=/0x125:1999x1198/335x180/s.glbimg.com/es/ge/f/original/2015/07/21/jor2.jpg" data-url-smart_horizontal="g2HBgGvaY42SgpwCuz8zR90O_S4=/90x0/smart/filters:strip_icc()/" data-url-smart="g2HBgGvaY42SgpwCuz8zR90O_S4=/90x0/smart/filters:strip_icc()/" data-url-feature="g2HBgGvaY42SgpwCuz8zR90O_S4=/90x0/smart/filters:strip_icc()/" data-url-tablet="sI0iaZ1GB6FJpAzwtZXz7aFzuGY=/220x125/smart/filters:strip_icc()/" data-url-desktop="IK7kMoSY18ee1owyiS9TKHDmzso=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Imprensa mexicana dÃ¡ destaque a dupla do Inter: &#39;Prontos para liquidar&#39; o Tigres</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 19:17:49" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/flamengo/flamengo-acerta-contratacao-do-meia-ederson-que-assinara-contrato-valido-ate-2017-16873779.html" class="foto" title="Fla confirma contrataÃ§Ã£o de Ederson para assumir a camisa 10 do Rubro-Negro (Arquivo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/S8XrdzrJjC-NFE3_XXx1naAOjMI=/filters:quality(10):strip_icc()/s2.glbimg.com/scQfXvwhPijRF-qMRMm_YzArxq0=/19x42:184x152/120x80/i.glbimg.com/og/ig/infoglobo1/f/original/2015/06/25/235229-lyon-reuters.jpg" alt="Fla confirma contrataÃ§Ã£o de Ederson para assumir a camisa 10 do Rubro-Negro (Arquivo)" title="Fla confirma contrataÃ§Ã£o de Ederson para assumir a camisa 10 do Rubro-Negro (Arquivo)"
                data-original-image="s2.glbimg.com/scQfXvwhPijRF-qMRMm_YzArxq0=/19x42:184x152/120x80/i.glbimg.com/og/ig/infoglobo1/f/original/2015/06/25/235229-lyon-reuters.jpg" data-url-smart_horizontal="K9GbsnlqLkoULsWQoPBNHMaE2-o=/90x0/smart/filters:strip_icc()/" data-url-smart="K9GbsnlqLkoULsWQoPBNHMaE2-o=/90x0/smart/filters:strip_icc()/" data-url-feature="K9GbsnlqLkoULsWQoPBNHMaE2-o=/90x0/smart/filters:strip_icc()/" data-url-tablet="GkOUzpn3Eb-k-iXpfHGb_Q_8pys=/70x50/smart/filters:strip_icc()/" data-url-desktop="V3YD65MHwbiNA5_VTBFnPorFRSM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fla confirma contrataÃ§Ã£o <br />de Ederson para assumir a camisa 10 do Rubro-Negro</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/07/orlando-diz-que-sao-paulo-tem-ate-quinta-para-aceitar-oferta-por-ganso.html" class="foto" title="Orlando dÃ¡ prazo para SÃ£o Paulo confirmar cessÃ£o de Ganso em acordo por dÃ­vida (Site oficial do SPFC)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/7aiQr-877ynTJt4q8tqTeLLe0uM=/filters:quality(10):strip_icc()/s2.glbimg.com/WfrEa-OZgvbJeJbIRXU219fv3Ns=/601x95:843x256/120x80/s.glbimg.com/es/ge/f/original/2015/05/07/ganso_pato_centurion.jpg" alt="Orlando dÃ¡ prazo para SÃ£o Paulo confirmar cessÃ£o de Ganso em acordo por dÃ­vida (Site oficial do SPFC)" title="Orlando dÃ¡ prazo para SÃ£o Paulo confirmar cessÃ£o de Ganso em acordo por dÃ­vida (Site oficial do SPFC)"
                data-original-image="s2.glbimg.com/WfrEa-OZgvbJeJbIRXU219fv3Ns=/601x95:843x256/120x80/s.glbimg.com/es/ge/f/original/2015/05/07/ganso_pato_centurion.jpg" data-url-smart_horizontal="bTTkvryO3Xivy-bxiBuLhTrIBqk=/90x0/smart/filters:strip_icc()/" data-url-smart="bTTkvryO3Xivy-bxiBuLhTrIBqk=/90x0/smart/filters:strip_icc()/" data-url-feature="bTTkvryO3Xivy-bxiBuLhTrIBqk=/90x0/smart/filters:strip_icc()/" data-url-tablet="zkuAshHpBwzTddyXrI2pScxJ5n4=/70x50/smart/filters:strip_icc()/" data-url-desktop="tuQq1eQXgOFFpd4DruPO9KdMR90=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Orlando dÃ¡ prazo para SÃ£o Paulo confirmar cessÃ£o de Ganso em acordo por dÃ­vida</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 19:17:49" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/07/se-nao-fosse-eu-duas-bandejas-de-chulapa-nos-mundiais-do-sao-paulo.html" class="" title="Se nÃ£o fosse eu... ConheÃ§a as duas bandejas de AloÃ­sio Chulapa nos Mundiais do SÃ£o Paulo (Alexandre Alliatti)" rel="bookmark"><span class="conteudo"><p>Se nÃ£o fosse eu... ConheÃ§a as duas bandejas<br /> de AloÃ­sio Chulapa nos Mundiais do SÃ£o Paulo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 17:56:42" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/palmeiras/noticia/2015/07/imbativel-em-classicos-palmeiras-usa-twitter-para-tirar-onda-com-rivais.html" class="foto" title="ImbatÃ­vel em clÃ¡ssicos no Brasileiro, Palmeiras tira onda com rivais na internet (reproduÃ§Ã£o / Twitter)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ZBzmIJITOEhXP9PA7S4JUd8nY-k=/filters:quality(10):strip_icc()/s2.glbimg.com/aFdy9fMYI8JESCD8-aHUeqVy7pQ=/96x0:459x241/120x80/s.glbimg.com/es/ge/f/original/2015/07/21/palm.jpg" alt="ImbatÃ­vel em clÃ¡ssicos no Brasileiro, Palmeiras tira onda com rivais na internet (reproduÃ§Ã£o / Twitter)" title="ImbatÃ­vel em clÃ¡ssicos no Brasileiro, Palmeiras tira onda com rivais na internet (reproduÃ§Ã£o / Twitter)"
                data-original-image="s2.glbimg.com/aFdy9fMYI8JESCD8-aHUeqVy7pQ=/96x0:459x241/120x80/s.glbimg.com/es/ge/f/original/2015/07/21/palm.jpg" data-url-smart_horizontal="2rS03OXaSwemT5nmfG4TDCuHHU8=/90x0/smart/filters:strip_icc()/" data-url-smart="2rS03OXaSwemT5nmfG4TDCuHHU8=/90x0/smart/filters:strip_icc()/" data-url-feature="2rS03OXaSwemT5nmfG4TDCuHHU8=/90x0/smart/filters:strip_icc()/" data-url-tablet="hwXWKVeUFOmQtLiRqB9Ph8OMj8Y=/70x50/smart/filters:strip_icc()/" data-url-desktop="dW7Fld5_pURCEm8KfQX4u9MJsxc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ImbatÃ­vel em clÃ¡ssicos no Brasileiro, Palmeiras tira <br />onda com rivais na internet</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 19:27:50" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/botafogo/noticia/2015/07/luis-henrique-ganha-aumento-no-bota-e-multa-passa-ser-de-r-60-mi.html" class="foto" title="Botafogo dÃ¡ aumento para revelaÃ§Ã£o, e multa pula de R$ 1 milhÃ£o para R$ 60 milhÃµes (EstadÃ£o ConteÃºdo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/zH7UuCrgjAxJEcVlvkFI5nbkEbc=/filters:quality(10):strip_icc()/s2.glbimg.com/cXlg-JJzXsd8gX8NlzGNYVf2n54=/526x198:1998x1178/120x80/s.glbimg.com/es/ge/f/original/2015/07/03/luis_henrique.jpg" alt="Botafogo dÃ¡ aumento para revelaÃ§Ã£o, e multa pula de R$ 1 milhÃ£o para R$ 60 milhÃµes (EstadÃ£o ConteÃºdo)" title="Botafogo dÃ¡ aumento para revelaÃ§Ã£o, e multa pula de R$ 1 milhÃ£o para R$ 60 milhÃµes (EstadÃ£o ConteÃºdo)"
                data-original-image="s2.glbimg.com/cXlg-JJzXsd8gX8NlzGNYVf2n54=/526x198:1998x1178/120x80/s.glbimg.com/es/ge/f/original/2015/07/03/luis_henrique.jpg" data-url-smart_horizontal="PSfC8m7Qlw76zFTYbjEixJn0nBY=/90x0/smart/filters:strip_icc()/" data-url-smart="PSfC8m7Qlw76zFTYbjEixJn0nBY=/90x0/smart/filters:strip_icc()/" data-url-feature="PSfC8m7Qlw76zFTYbjEixJn0nBY=/90x0/smart/filters:strip_icc()/" data-url-tablet="HWG70F0czJ-mIanBRP_OVk6OmF8=/70x50/smart/filters:strip_icc()/" data-url-desktop="7V_emXvVLltzlGeeWl6Ngdq307I=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Botafogo dÃ¡ aumento para revelaÃ§Ã£o, e multa pula de R$ 1 milhÃ£o para R$ 60 milhÃµes</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 19:56:45" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/corinthians/noticia/2015/07/sem-acordo-salarial-corinthians-descarta-contratacao-de-rene-junior.html" class="" title="No Ãºltimo dia da janela, Corinthians nÃ£o chega a acordo e desiste de volante do futebol chinÃªs" rel="bookmark"><span class="conteudo"><p>No Ãºltimo dia da janela, Corinthians nÃ£o chega <br />a acordo e desiste de volante do futebol chinÃªs</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 18:47:10" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/ronda-rousey-chora-ao-comentar-provocacao-de-bethe-sobre-suicidio.html" class="foto" title="ProvocaÃ§Ã£o de Bethe sobre suicÃ­dio faz Ronda chorar: &#39;Desrespeita tragÃ©dia familiar&#39; (ReproduÃ§Ã£o/Youtube)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/oMTI907sufr9sGdaLG3Q9pPHL5Y=/filters:quality(10):strip_icc()/s2.glbimg.com/zUA-_UmFquexe1zlz-C2ixMIBuk=/603x179:1442x739/120x80/s.glbimg.com/es/ge/f/original/2015/07/21/ronda-chora.jpg" alt="ProvocaÃ§Ã£o de Bethe sobre suicÃ­dio faz Ronda chorar: &#39;Desrespeita tragÃ©dia familiar&#39; (ReproduÃ§Ã£o/Youtube)" title="ProvocaÃ§Ã£o de Bethe sobre suicÃ­dio faz Ronda chorar: &#39;Desrespeita tragÃ©dia familiar&#39; (ReproduÃ§Ã£o/Youtube)"
                data-original-image="s2.glbimg.com/zUA-_UmFquexe1zlz-C2ixMIBuk=/603x179:1442x739/120x80/s.glbimg.com/es/ge/f/original/2015/07/21/ronda-chora.jpg" data-url-smart_horizontal="-0_3XqK-mTMXCzlp5dfIjjtrDq8=/90x0/smart/filters:strip_icc()/" data-url-smart="-0_3XqK-mTMXCzlp5dfIjjtrDq8=/90x0/smart/filters:strip_icc()/" data-url-feature="-0_3XqK-mTMXCzlp5dfIjjtrDq8=/90x0/smart/filters:strip_icc()/" data-url-tablet="h0BTDLagl0WCSjiu2pUh_3xvBVI=/70x50/smart/filters:strip_icc()/" data-url-desktop="9M4-NE-1aVKSVJ4KOPBZMCAC-f0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ProvocaÃ§Ã£o de Bethe sobre suicÃ­dio faz Ronda chorar: &#39;Desrespeita tragÃ©dia familiar&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 19:17:49" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/radicais/surfe/mundial-de-surfe/noticia/2015/07/medina-tira-titulo-da-meta-e-ve-inseguranca-no-surfe-contra-tubaroes.html" class="foto" title="Medina fala sobre falta de seguranÃ§a e prevÃª: &#39;Poucos vÃ£o retornar a Jeffreys Bay&#39; (Marcos Guerra)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/3POGpHX5WIfYpqh9u69G9LNPX2o=/filters:quality(10):strip_icc()/s2.glbimg.com/PLsGU5_S-RJFLzpvg_ECafRMo6U=/206x69:824x482/120x80/s.glbimg.com/es/ge/f/original/2015/07/21/gabriel_medina_livro_2.jpg" alt="Medina fala sobre falta de seguranÃ§a e prevÃª: &#39;Poucos vÃ£o retornar a Jeffreys Bay&#39; (Marcos Guerra)" title="Medina fala sobre falta de seguranÃ§a e prevÃª: &#39;Poucos vÃ£o retornar a Jeffreys Bay&#39; (Marcos Guerra)"
                data-original-image="s2.glbimg.com/PLsGU5_S-RJFLzpvg_ECafRMo6U=/206x69:824x482/120x80/s.glbimg.com/es/ge/f/original/2015/07/21/gabriel_medina_livro_2.jpg" data-url-smart_horizontal="AlhQKw7qeNwsVbvwnAg31oMmJdM=/90x0/smart/filters:strip_icc()/" data-url-smart="AlhQKw7qeNwsVbvwnAg31oMmJdM=/90x0/smart/filters:strip_icc()/" data-url-feature="AlhQKw7qeNwsVbvwnAg31oMmJdM=/90x0/smart/filters:strip_icc()/" data-url-tablet="dtLUqyh4hKAyWEgoReZS8o_WR30=/70x50/smart/filters:strip_icc()/" data-url-desktop="STWC-MlRGPF_FMjNRPnOl93e5J8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Medina fala sobre falta de seguranÃ§a e prevÃª: &#39;Poucos vÃ£o retornar a Jeffreys Bay&#39;</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/tv/noticia/2015/07/patricia-franca-chora-ao-rever-1-filha-da-ficcao-ela-nasceu-atriz.html" class="foto" title="PatrÃ­cia FranÃ§a chora ao rever sua 1Âª filha da ficÃ§Ã£o, 22 anos depois: &#39;Nasceu atriz&#39; (CEDOC / VÃ­deo Show / TV Globo)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/gAl9QULpdgds3QioF6SR7g4Cd4g=/filters:quality(10):strip_icc()/s2.glbimg.com/54PpFWw6AgaNEceazzGXwoL8qJU=/0x14:690x385/335x180/s.glbimg.com/et/gs/f/original/2015/07/21/carolina-pavanelli-patricia.jpg" alt="PatrÃ­cia FranÃ§a chora ao rever sua 1Âª filha da ficÃ§Ã£o, 22 anos depois: &#39;Nasceu atriz&#39; (CEDOC / VÃ­deo Show / TV Globo)" title="PatrÃ­cia FranÃ§a chora ao rever sua 1Âª filha da ficÃ§Ã£o, 22 anos depois: &#39;Nasceu atriz&#39; (CEDOC / VÃ­deo Show / TV Globo)"
                data-original-image="s2.glbimg.com/54PpFWw6AgaNEceazzGXwoL8qJU=/0x14:690x385/335x180/s.glbimg.com/et/gs/f/original/2015/07/21/carolina-pavanelli-patricia.jpg" data-url-smart_horizontal="5D5UzltREIgtpRsud6jjIHGwPGg=/90x0/smart/filters:strip_icc()/" data-url-smart="5D5UzltREIgtpRsud6jjIHGwPGg=/90x0/smart/filters:strip_icc()/" data-url-feature="5D5UzltREIgtpRsud6jjIHGwPGg=/90x0/smart/filters:strip_icc()/" data-url-tablet="fy-hY0Y4EsgxJ3SBuaWr29sf_Kw=/220x125/smart/filters:strip_icc()/" data-url-desktop="hhAtV-6a_IKYmJtxUnbk3p8S53U=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PatrÃ­cia FranÃ§a chora ao rever sua 1Âª filha da ficÃ§Ã£o, 22 anos depois: &#39;Nasceu atriz&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 21:25:48" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/07/dudu-nobre-muda-agenda-para-esperar-nascimento-da-filha.html" class="foto" title="Dudu Nobre faz obra em casa e muda agenda de shows para chegada do quarto filho (Nila Costa/DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/bOo__g7lI3FSECXe9TNCZYj6CCQ=/filters:quality(10):strip_icc()/s2.glbimg.com/zvsQdc2piGtv6XdUeC606OL0zWY=/0x766:2920x2710/120x80/s.glbimg.com/jo/eg/f/original/2015/07/21/mg_0878.jpg" alt="Dudu Nobre faz obra em casa e muda agenda de shows para chegada do quarto filho (Nila Costa/DivulgaÃ§Ã£o)" title="Dudu Nobre faz obra em casa e muda agenda de shows para chegada do quarto filho (Nila Costa/DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/zvsQdc2piGtv6XdUeC606OL0zWY=/0x766:2920x2710/120x80/s.glbimg.com/jo/eg/f/original/2015/07/21/mg_0878.jpg" data-url-smart_horizontal="8vkXOxjBNrhC1tXLdw4IZjibZ7s=/90x0/smart/filters:strip_icc()/" data-url-smart="8vkXOxjBNrhC1tXLdw4IZjibZ7s=/90x0/smart/filters:strip_icc()/" data-url-feature="8vkXOxjBNrhC1tXLdw4IZjibZ7s=/90x0/smart/filters:strip_icc()/" data-url-tablet="2-IGC1xmENAIZKflFw4XSXjbJQw=/70x50/smart/filters:strip_icc()/" data-url-desktop="i06QhGW9QoFIZl4gGd1FRSmZxvk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Dudu Nobre faz obra em casa e muda agenda de shows para chegada do quarto filho</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 18:36:21" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/moda/noticia/2015/07/look-do-dia-camila-queiroz-de-verdades-secretas-usa-calca-flare.html" class="foto" title="No &#39;Look do dia&#39;, Camila Queiroz usa calÃ§a flare, queridinha das famosas (Raphael Castello/AgNews)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/UzloSHfdqTe8V7AAipD97ejBxSM=/filters:quality(10):strip_icc()/s2.glbimg.com/7S9hfsNoUNN6_O7BIPIcKr_W68g=/133x25:429x223/120x80/s.glbimg.com/jo/eg/f/original/2015/07/21/56grtf.jpg" alt="No &#39;Look do dia&#39;, Camila Queiroz usa calÃ§a flare, queridinha das famosas (Raphael Castello/AgNews)" title="No &#39;Look do dia&#39;, Camila Queiroz usa calÃ§a flare, queridinha das famosas (Raphael Castello/AgNews)"
                data-original-image="s2.glbimg.com/7S9hfsNoUNN6_O7BIPIcKr_W68g=/133x25:429x223/120x80/s.glbimg.com/jo/eg/f/original/2015/07/21/56grtf.jpg" data-url-smart_horizontal="E1uL7rV2lLZCHWAUIs5tz6-3Jfc=/90x0/smart/filters:strip_icc()/" data-url-smart="E1uL7rV2lLZCHWAUIs5tz6-3Jfc=/90x0/smart/filters:strip_icc()/" data-url-feature="E1uL7rV2lLZCHWAUIs5tz6-3Jfc=/90x0/smart/filters:strip_icc()/" data-url-tablet="spaDkhppR9YcFMzM_E7llWsfe0c=/70x50/smart/filters:strip_icc()/" data-url-desktop="a--mTQkZGnX3zaESo4rSWQsqUpM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>No &#39;Look do dia&#39;, Camila Queiroz usa calÃ§a flare, queridinha das famosas</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/casamento/noticia/2015/07/casamento-de-juju-salimeni-tera-cardapio-fitness-e-show-de-anitta.html" class="" title="Casamento de Juju e Felipe Franco terÃ¡ buffet fitness com picolÃ© de whey protein (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class="conteudo"><p>Casamento de Juju e Felipe Franco terÃ¡ buffet fitness com picolÃ© de whey protein</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/07/com-look-decotado-e-transparente-aline-gotschalg-vai-evento-de-moda.html" class="foto" title="De vestido transparente e decotÃ£o, Aline rouba cena com beijÃ£o em FÃª em desfile (Francisco Cepeda /  AgNews)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/h2Rlqm1pb6lce8ASOLfWN8gSdDA=/filters:quality(10):strip_icc()/s2.glbimg.com/iTjm4rIvsSQjhEMBYPlioVLMaa0=/299x78:687x336/120x80/s.glbimg.com/jo/eg/f/original/2015/07/21/aline_e_fernando_exbbbs-34_1.jpg" alt="De vestido transparente e decotÃ£o, Aline rouba cena com beijÃ£o em FÃª em desfile (Francisco Cepeda /  AgNews)" title="De vestido transparente e decotÃ£o, Aline rouba cena com beijÃ£o em FÃª em desfile (Francisco Cepeda /  AgNews)"
                data-original-image="s2.glbimg.com/iTjm4rIvsSQjhEMBYPlioVLMaa0=/299x78:687x336/120x80/s.glbimg.com/jo/eg/f/original/2015/07/21/aline_e_fernando_exbbbs-34_1.jpg" data-url-smart_horizontal="xYqzPgo6GaBD-NLq6yg7IE44lJ0=/90x0/smart/filters:strip_icc()/" data-url-smart="xYqzPgo6GaBD-NLq6yg7IE44lJ0=/90x0/smart/filters:strip_icc()/" data-url-feature="xYqzPgo6GaBD-NLq6yg7IE44lJ0=/90x0/smart/filters:strip_icc()/" data-url-tablet="YLfmDWMrJ482qEA8kY7886_3F1c=/70x50/smart/filters:strip_icc()/" data-url-desktop="dfZdhGz5Bq8LM2e2Z9wLCeTrMgM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>De vestido transparente e decotÃ£o, Aline rouba cena com beijÃ£o em FÃª em desfile</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 18:52:45" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/tamara-ecclestone-compartilha-fotos-amamentando-filha-e-e-criticada.html" class="foto" title="Socialite filha de chefÃ£o da F-1 posta foto amamentando e vira alvo de crÃ­ticas na web (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/yFtiIFpBbf5q1lYRp6zgNexxTCA=/filters:quality(10):strip_icc()/s2.glbimg.com/3witY9T2XQCQmEwuwYc5iFJpdwc=/0x213:639x640/120x80/e.glbimg.com/og/ed/f/original/2015/07/21/tamara.jpg" alt="Socialite filha de chefÃ£o da F-1 posta foto amamentando e vira alvo de crÃ­ticas na web (ReproduÃ§Ã£o)" title="Socialite filha de chefÃ£o da F-1 posta foto amamentando e vira alvo de crÃ­ticas na web (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/3witY9T2XQCQmEwuwYc5iFJpdwc=/0x213:639x640/120x80/e.glbimg.com/og/ed/f/original/2015/07/21/tamara.jpg" data-url-smart_horizontal="lHpLczCrNghzrrhLqYtg1FqG1tU=/90x0/smart/filters:strip_icc()/" data-url-smart="lHpLczCrNghzrrhLqYtg1FqG1tU=/90x0/smart/filters:strip_icc()/" data-url-feature="lHpLczCrNghzrrhLqYtg1FqG1tU=/90x0/smart/filters:strip_icc()/" data-url-tablet="CGjDXtkdb8bx68BLzA9C9NcjpPE=/70x50/smart/filters:strip_icc()/" data-url-desktop="QAcnrFTazSphRlJroDG7rESZ_HU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Socialite filha de chefÃ£o da F-1 posta foto amamentando e vira alvo de crÃ­ticas na web</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 17:25:49" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/gravida-de-uma-menina-nora-de-preta-gil-curte-piscina-em-orlando.html" class="" title="GrÃ¡vida de uma menina, nora de Preta Gil exibe barriga ao posar &#39;relax&#39; em piscina em Orlando (Junior SchÃ¶n/DivulgaÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>GrÃ¡vida de uma menina, nora de Preta Gil exibe barriga ao posar &#39;relax&#39; em piscina em Orlando</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 19:49:03" class="chamada chamada-principal mobile-grid-full"><a href="http://globosatplay.globo.com/multishow/v/4333121/" class="foto" title="Estreia do programa de CearÃ¡ tem participaÃ§Ã£o de Danilo Gentili; veja na Ã­ntegra (Multishow)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/2dUwJmvErhFLnxeqkWsqxXc87Hg=/filters:quality(10):strip_icc()/s2.glbimg.com/lCN10SoxAr4GlCzGV9sHZOtlOVI=/385x224:934x590/120x80/s.glbimg.com/en/ho/f/original/2015/07/21/danilogentini.jpg" alt="Estreia do programa de CearÃ¡ tem participaÃ§Ã£o de Danilo Gentili; veja na Ã­ntegra (Multishow)" title="Estreia do programa de CearÃ¡ tem participaÃ§Ã£o de Danilo Gentili; veja na Ã­ntegra (Multishow)"
                data-original-image="s2.glbimg.com/lCN10SoxAr4GlCzGV9sHZOtlOVI=/385x224:934x590/120x80/s.glbimg.com/en/ho/f/original/2015/07/21/danilogentini.jpg" data-url-smart_horizontal="78qD5qPUeP0VQk5q0HXAkg_d7AQ=/90x0/smart/filters:strip_icc()/" data-url-smart="78qD5qPUeP0VQk5q0HXAkg_d7AQ=/90x0/smart/filters:strip_icc()/" data-url-feature="78qD5qPUeP0VQk5q0HXAkg_d7AQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="SWXJLWI341ICg5yoU8sDVi8MsQk=/70x50/smart/filters:strip_icc()/" data-url-desktop="BsZLkDLv1VdQfjv2dLqwzFzLjA0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Estreia do programa de CearÃ¡ tem participaÃ§Ã£o de Danilo Gentili; veja na Ã­ntegra</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-21 14:31:18" class="chamada chamada-principal mobile-grid-full"><a href="http://revistacasaejardim.globo.com/Casa-e-Comida/Receitas/noticia/2015/07/o-ovo-e-estrela-do-prato.html" class="foto" title="O ovo Ã© a estrela do prato: confira como preparar 10 receitas prÃ¡ticas e saborosas (Casa e Comida)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/7sLyc8wixJ7kpdLLSWnJrz6V7Uc=/filters:quality(10):strip_icc()/s2.glbimg.com/pZMrtr9Yc2ZRTxzuFsMCGaTftuk=/0x142:350x375/120x80/e.glbimg.com/og/ed/f/original/2015/07/21/receitas-ovo.jpeg" alt="O ovo Ã© a estrela do prato: confira como preparar 10 receitas prÃ¡ticas e saborosas (Casa e Comida)" title="O ovo Ã© a estrela do prato: confira como preparar 10 receitas prÃ¡ticas e saborosas (Casa e Comida)"
                data-original-image="s2.glbimg.com/pZMrtr9Yc2ZRTxzuFsMCGaTftuk=/0x142:350x375/120x80/e.glbimg.com/og/ed/f/original/2015/07/21/receitas-ovo.jpeg" data-url-smart_horizontal="ntpPzMTRX-66mecri7UUPB1XpjA=/90x0/smart/filters:strip_icc()/" data-url-smart="ntpPzMTRX-66mecri7UUPB1XpjA=/90x0/smart/filters:strip_icc()/" data-url-feature="ntpPzMTRX-66mecri7UUPB1XpjA=/90x0/smart/filters:strip_icc()/" data-url-tablet="S0QerExHc_elStRuHlFWmgl0_m8=/70x50/smart/filters:strip_icc()/" data-url-desktop="9F4kUqmg8kXDEyYP21UtELW4QOw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>O ovo Ã© a estrela do prato: confira como preparar 10 receitas prÃ¡ticas e saborosas</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2014/index.html", "logo": "http://s2.glbimg.com/kX8ylHaY-BqxGG2GesdMgR7SxQc=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_38x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2014/rss", "logo_tv": "http://s2.glbimg.com/tKm4gbARSp2eHSq6zr2alVV82hc=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2014/index.html", "logo": "http://s2.glbimg.com/kX8ylHaY-BqxGG2GesdMgR7SxQc=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_38x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2014/rss", "logo_tv": "http://s2.glbimg.com/tKm4gbARSp2eHSq6zr2alVV82hc=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior "><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="tecnologia &amp; games"><span class="word word-0">tecnologia</span><span class="word word-1">&</span><span class="word word-2">games</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/07/atualizacao-do-playstation-4-pode-causar-problemas-de-leitura-de-discos.html" title="PS4 ganha atualizaÃ§Ã£o &#39;perigosa&#39; que pode te impedir de jogar"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/W6Pq5I3fm_NdETdB4ZXsqLGwgyg=/filters:quality(10):strip_icc()/s2.glbimg.com/YSUFSKiB-CW1-4RP_1VfJ8nCQvI=/8x0:579x302/245x130/s.glbimg.com/po/tt2/f/original/2015/07/21/dsc_9035.jpg" alt="PS4 ganha atualizaÃ§Ã£o &#39;perigosa&#39; que pode te impedir de jogar (Matheus Vasconcelos/TechTudo)" title="PS4 ganha atualizaÃ§Ã£o &#39;perigosa&#39; que pode te impedir de jogar (Matheus Vasconcelos/TechTudo)"
             data-original-image="s2.glbimg.com/YSUFSKiB-CW1-4RP_1VfJ8nCQvI=/8x0:579x302/245x130/s.glbimg.com/po/tt2/f/original/2015/07/21/dsc_9035.jpg" data-url-smart_horizontal="78C6DvyCUZtO0SZ264R2_Gno_30=/90x56/smart/filters:strip_icc()/" data-url-smart="78C6DvyCUZtO0SZ264R2_Gno_30=/90x56/smart/filters:strip_icc()/" data-url-feature="78C6DvyCUZtO0SZ264R2_Gno_30=/90x56/smart/filters:strip_icc()/" data-url-tablet="x5gtkIgbgKq786KcSvi96NSPNKQ=/160x96/smart/filters:strip_icc()/" data-url-desktop="acrggSnlu9PRsXCFBogGpZs71X8=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>PS4 ganha atualizaÃ§Ã£o &#39;perigosa&#39; que pode te impedir de jogar</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/07/gopro-libera-assistencia-tecnica-local-e-sem-custos-para-brasileiros.html" title="GoPro anuncia que irÃ¡ consertar suas cÃ¢meras de graÃ§a no Brasil"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/H_H_8G9WaApoRv-z04oiT6gkY2E=/filters:quality(10):strip_icc()/s2.glbimg.com/ZMx-COg0qOxFqoIwQzLFEfTEgcU=/0x420:5008x3080/245x130/s.glbimg.com/po/tt2/f/original/2015/03/17/gopro_hero_4_black_6_4.jpg" alt="GoPro anuncia que irÃ¡ consertar suas cÃ¢meras de graÃ§a no Brasil (Luciana Maline/TechTudo)" title="GoPro anuncia que irÃ¡ consertar suas cÃ¢meras de graÃ§a no Brasil (Luciana Maline/TechTudo)"
             data-original-image="s2.glbimg.com/ZMx-COg0qOxFqoIwQzLFEfTEgcU=/0x420:5008x3080/245x130/s.glbimg.com/po/tt2/f/original/2015/03/17/gopro_hero_4_black_6_4.jpg" data-url-smart_horizontal="HG1h2INfNVYDBn2Q_nnHUNSlJ0c=/90x56/smart/filters:strip_icc()/" data-url-smart="HG1h2INfNVYDBn2Q_nnHUNSlJ0c=/90x56/smart/filters:strip_icc()/" data-url-feature="HG1h2INfNVYDBn2Q_nnHUNSlJ0c=/90x56/smart/filters:strip_icc()/" data-url-tablet="8OQ-C2DxdGrl9BDdKfecF6DoiPk=/160x96/smart/filters:strip_icc()/" data-url-desktop="miIljeShNZWqazL_0KspZqVxfbE=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>GoPro anuncia que irÃ¡ consertar <br />suas cÃ¢meras de graÃ§a no Brasil</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/07/confira-evolucao-de-graficos-da-franquia-de-corrida-gran-turismo.html" title="Gran Turismo: veja evoluÃ§Ã£o dos grÃ¡ficos dos jogos superrealistas"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/EdHIdRGPtkejD4gTqbR5nYD0KeI=/filters:quality(10):strip_icc()/s2.glbimg.com/14qr_wBXYnccn-Rq-77186nAces=/0x0:694x368/245x130/s.glbimg.com/po/tt2/f/original/2015/07/06/gran-turismo-evolucao-grafica3.jpg" alt="Gran Turismo: veja evoluÃ§Ã£o dos grÃ¡ficos dos jogos superrealistas (Gran Turismo 6 tem resoluÃ§Ã£o maior e efeitos mais caprichados (Foto: ReproduÃ§Ã£o/Filipe Garrett))" title="Gran Turismo: veja evoluÃ§Ã£o dos grÃ¡ficos dos jogos superrealistas (Gran Turismo 6 tem resoluÃ§Ã£o maior e efeitos mais caprichados (Foto: ReproduÃ§Ã£o/Filipe Garrett))"
             data-original-image="s2.glbimg.com/14qr_wBXYnccn-Rq-77186nAces=/0x0:694x368/245x130/s.glbimg.com/po/tt2/f/original/2015/07/06/gran-turismo-evolucao-grafica3.jpg" data-url-smart_horizontal="LhDnLqb1Gn_xktCVsnHtMxl0nIg=/90x56/smart/filters:strip_icc()/" data-url-smart="LhDnLqb1Gn_xktCVsnHtMxl0nIg=/90x56/smart/filters:strip_icc()/" data-url-feature="LhDnLqb1Gn_xktCVsnHtMxl0nIg=/90x56/smart/filters:strip_icc()/" data-url-tablet="_RVfDI_64fqxFJn321zfQ_ZSt5M=/160x96/smart/filters:strip_icc()/" data-url-desktop="GrcZUcJRpxQ_W7qwHj7jKPcUg88=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Gran Turismo: veja evoluÃ§Ã£o dos grÃ¡ficos dos jogos superrealistas</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/tudo-sobre/speakingpal.html" title="Aplicativo dÃ¡ aulas completas de inglÃªs para vocÃª de graÃ§a; baixe"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/DE_yZZ2wLje8ENhb1jHnu761lvM=/filters:quality(10):strip_icc()/s2.glbimg.com/bVcVrGvbXfdEPE7a2OTxzBgrTyg=/139x66:642x333/245x130/s.glbimg.com/po/tt2/f/original/2015/01/21/mulher-bonita-smartphone.jpeg" alt="Aplicativo dÃ¡ aulas completas de inglÃªs para vocÃª de graÃ§a; baixe (Pond5)" title="Aplicativo dÃ¡ aulas completas de inglÃªs para vocÃª de graÃ§a; baixe (Pond5)"
             data-original-image="s2.glbimg.com/bVcVrGvbXfdEPE7a2OTxzBgrTyg=/139x66:642x333/245x130/s.glbimg.com/po/tt2/f/original/2015/01/21/mulher-bonita-smartphone.jpeg" data-url-smart_horizontal="RE70IpEOWr-_HtFJR6bM9PoZXog=/90x56/smart/filters:strip_icc()/" data-url-smart="RE70IpEOWr-_HtFJR6bM9PoZXog=/90x56/smart/filters:strip_icc()/" data-url-feature="RE70IpEOWr-_HtFJR6bM9PoZXog=/90x56/smart/filters:strip_icc()/" data-url-tablet="C32RpVJNHkMK7Uvn2Qg0uYIdBYE=/160x96/smart/filters:strip_icc()/" data-url-desktop="edEXVLmWEH0WmMmAVainHccnmE8=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Aplicativo dÃ¡ aulas completas de inglÃªs para vocÃª de graÃ§a; baixe</p></span></a></li></ul><div class="destaque-inferior analytics-area analytics-id-B"><ul><li class="techtudo"><a href="http://www.techtudo.com.br/" title="Teste agora a velocidade da sua internet"><span class="logo"></span><span class="title">Teste agora a velocidade da sua internet &rsaquo;</span></a></li><li class="exty"><a href="http://www.techtudo.com.br/tudo-sobre/extensao-globo-com.html" title="Adicione e veja as notÃ­cias em tempo real"><span class="logo"></span><span class="title">Adicione e veja as notÃ­cias em tempo real &rsaquo;</span></a></li></ul></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Beleza/noticia/2015/07/praticidade-4-truques-de-make-que-facilitam-vida-de-toda-mulher.html" alt="ConheÃ§a pequenos segredos de make que facilitam a vida de toda mulher"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/DDoc9KAeLyi6y7tC8-OnM3G6Zlc=/filters:quality(10):strip_icc()/s2.glbimg.com/k_mH83Y3JiUGL8TKGOlLYnUCkwA=/0x39:620x439/155x100/e.glbimg.com/og/ed/f/original/2015/07/20/thinkstockphotos-482425187.jpg" alt="ConheÃ§a pequenos segredos de make que facilitam a vida de toda mulher (THINKSTOCK)" title="ConheÃ§a pequenos segredos de make que facilitam a vida de toda mulher (THINKSTOCK)"
            data-original-image="s2.glbimg.com/k_mH83Y3JiUGL8TKGOlLYnUCkwA=/0x39:620x439/155x100/e.glbimg.com/og/ed/f/original/2015/07/20/thinkstockphotos-482425187.jpg" data-url-smart_horizontal="pWpJ_Y5qsQXWFTBKfmvAP2pjnzc=/90x56/smart/filters:strip_icc()/" data-url-smart="pWpJ_Y5qsQXWFTBKfmvAP2pjnzc=/90x56/smart/filters:strip_icc()/" data-url-feature="pWpJ_Y5qsQXWFTBKfmvAP2pjnzc=/90x56/smart/filters:strip_icc()/" data-url-tablet="E4As83Y5bBfv0DRweAzSMMDRHIM=/122x75/smart/filters:strip_icc()/" data-url-desktop="lXsnxxrjVzmbbYW8fCVI1iSCzS0=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>simples e eficaz</h3><p>ConheÃ§a pequenos segredos de make que facilitam a vida de toda mulher</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Moda/Siga-o-estilo/noticia/2015/07/5-pecas-do-street-style-pra-sobreviver-mudancas-de-clima-com-estilo.html" alt="SeleÃ§Ã£o traz 5 peÃ§as-chave para sobreviver Ã s mudanÃ§as de clima"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/TS2COPTEOSLleHWdCRwt4z93QQ4=/filters:quality(10):strip_icc()/s2.glbimg.com/BjY9MPfwTS_k8tUmFSpdypKAK0s=/0x0:607x391/155x100/e.glbimg.com/og/ed/f/original/2015/07/20/street-style.jpg" alt="SeleÃ§Ã£o traz 5 peÃ§as-chave para sobreviver Ã s mudanÃ§as de clima (AgÃªncia Fotosite)" title="SeleÃ§Ã£o traz 5 peÃ§as-chave para sobreviver Ã s mudanÃ§as de clima (AgÃªncia Fotosite)"
            data-original-image="s2.glbimg.com/BjY9MPfwTS_k8tUmFSpdypKAK0s=/0x0:607x391/155x100/e.glbimg.com/og/ed/f/original/2015/07/20/street-style.jpg" data-url-smart_horizontal="QTqpCdzVZTsKiz-dC3ZSezSVn7k=/90x56/smart/filters:strip_icc()/" data-url-smart="QTqpCdzVZTsKiz-dC3ZSezSVn7k=/90x56/smart/filters:strip_icc()/" data-url-feature="QTqpCdzVZTsKiz-dC3ZSezSVn7k=/90x56/smart/filters:strip_icc()/" data-url-tablet="MQmntI6Hh4zu6mhYRj2kO7hMubM=/122x75/smart/filters:strip_icc()/" data-url-desktop="4VTkzd1guBD80bgyeIGAdKXOaUw=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>com estilo em qualquer temperatura</h3><p>SeleÃ§Ã£o traz 5 peÃ§as-chave para sobreviver Ã s mudanÃ§as de clima</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/gente/noticia/2015/07/microbiquinis-sao-tendencia-no-verao-do-hemisferio-norte.html" alt="MicrobiquÃ­nis surgem como tendÃªncia nas passarelas de Miami; vocÃª usaria?"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/RpcR35eeOJ-XDM7__TpucgEzzLE=/filters:quality(10):strip_icc()/s2.glbimg.com/wcXf2pAHfN1EiEHCMANWza0L1qw=/0x150:1800x1310/155x100/e.glbimg.com/og/ed/f/original/2015/07/20/mia-marcelle-01.jpg" alt="MicrobiquÃ­nis surgem como tendÃªncia nas passarelas de Miami; vocÃª usaria? (Getty Images)" title="MicrobiquÃ­nis surgem como tendÃªncia nas passarelas de Miami; vocÃª usaria? (Getty Images)"
            data-original-image="s2.glbimg.com/wcXf2pAHfN1EiEHCMANWza0L1qw=/0x150:1800x1310/155x100/e.glbimg.com/og/ed/f/original/2015/07/20/mia-marcelle-01.jpg" data-url-smart_horizontal="e4TmemvxPxFak16cfhaw2vQULes=/90x56/smart/filters:strip_icc()/" data-url-smart="e4TmemvxPxFak16cfhaw2vQULes=/90x56/smart/filters:strip_icc()/" data-url-feature="e4TmemvxPxFak16cfhaw2vQULes=/90x56/smart/filters:strip_icc()/" data-url-tablet="sM9GtU9SGDyb4FLZd-PGa0RPHhA=/122x75/smart/filters:strip_icc()/" data-url-desktop="Ld42czCo4qB-auc4xNPWtlS34aU=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>peÃ§as pequenas</h3><p>MicrobiquÃ­nis surgem como tendÃªncia nas passarelas de Miami; vocÃª usaria?</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/Ambientes/noticia/2015/07/dicas-para-fazer-bonito-no-cha-de-bebe.html" alt="Blogueira dÃ¡ dicas valiosas de comes, bebes e decoraÃ§Ã£o para chÃ¡ de bebÃª"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/mYt14yAYJw_uAgX0Lav-lSLd5QQ=/filters:quality(10):strip_icc()/s2.glbimg.com/8hkHi9J-ZBZprRdXp89DwF8nnEE=/78x73:543x373/155x100/e.glbimg.com/og/ed/f/original/2015/07/15/decoracao-cha-de-bebe-10.jpg" alt="Blogueira dÃ¡ dicas valiosas de comes, bebes e decoraÃ§Ã£o para chÃ¡ de bebÃª (Fernanda Petelinkar / divulgaÃ§)" title="Blogueira dÃ¡ dicas valiosas de comes, bebes e decoraÃ§Ã£o para chÃ¡ de bebÃª (Fernanda Petelinkar / divulgaÃ§)"
            data-original-image="s2.glbimg.com/8hkHi9J-ZBZprRdXp89DwF8nnEE=/78x73:543x373/155x100/e.glbimg.com/og/ed/f/original/2015/07/15/decoracao-cha-de-bebe-10.jpg" data-url-smart_horizontal="LhrmmUm3c9OX9GAcpPxkwajMuKw=/90x56/smart/filters:strip_icc()/" data-url-smart="LhrmmUm3c9OX9GAcpPxkwajMuKw=/90x56/smart/filters:strip_icc()/" data-url-feature="LhrmmUm3c9OX9GAcpPxkwajMuKw=/90x56/smart/filters:strip_icc()/" data-url-tablet="l4h6L7a-lEwALtPPtzdL8CqDOiY=/122x75/smart/filters:strip_icc()/" data-url-desktop="eGnMuUzZcDw8r8EYMuRU6vZOIy8=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>festa com barrigÃ£o</h3><p>Blogueira dÃ¡ dicas valiosas de comes, bebes e decoraÃ§Ã£o para chÃ¡ de bebÃª</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Comida/Reportagens/Comida/noticia/2015/07/7-dicas-para-organizar-despensa.html" alt="Veja 7 dicas para deixar os alimentos e utensÃ­lios bem organizados na cozinha"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/7w76A_5bXQ6J8YqmxY7q6pQ_NOs=/filters:quality(10):strip_icc()/s2.glbimg.com/FBeN2sD7ArVq9Fb-6mrF2boNoOU=/0x218:620x618/155x100/e.glbimg.com/og/ed/f/original/2015/06/23/despensa.jpg" alt="Veja 7 dicas para deixar os alimentos e utensÃ­lios bem organizados na cozinha (Stock Photos)" title="Veja 7 dicas para deixar os alimentos e utensÃ­lios bem organizados na cozinha (Stock Photos)"
            data-original-image="s2.glbimg.com/FBeN2sD7ArVq9Fb-6mrF2boNoOU=/0x218:620x618/155x100/e.glbimg.com/og/ed/f/original/2015/06/23/despensa.jpg" data-url-smart_horizontal="HApZzw64LO9YYzzc_LJ63k47FhM=/90x56/smart/filters:strip_icc()/" data-url-smart="HApZzw64LO9YYzzc_LJ63k47FhM=/90x56/smart/filters:strip_icc()/" data-url-feature="HApZzw64LO9YYzzc_LJ63k47FhM=/90x56/smart/filters:strip_icc()/" data-url-tablet="bjXWhadvBV_T9L-ZzsR7IEaqC8k=/122x75/smart/filters:strip_icc()/" data-url-desktop="Po1-2D2vc0_NVn2Dxqqp0CJMWsQ=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>BagunÃ§a, nunca mais!</h3><p>Veja 7 dicas para deixar os alimentos e utensÃ­lios bem organizados na cozinha</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/saiba-quais-flores-perfumadas-cultivar-em-casa/" alt="Suaves ou mais fortes? Saiba quais flores perfumadas cultivar em casa"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/s5ZK6znwpUBKuEQOF98IaRwDjes=/filters:quality(10):strip_icc()/s2.glbimg.com/Cd09U1Dc7uYrV7jX_MQcAlkAuvE=/48x29:541x348/155x100/s.glbimg.com/en/ho/f/original/2015/07/21/flores-na-decor.jpg" alt="Suaves ou mais fortes? Saiba quais flores perfumadas cultivar em casa (DivulgaÃ§Ã£o)" title="Suaves ou mais fortes? Saiba quais flores perfumadas cultivar em casa (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/Cd09U1Dc7uYrV7jX_MQcAlkAuvE=/48x29:541x348/155x100/s.glbimg.com/en/ho/f/original/2015/07/21/flores-na-decor.jpg" data-url-smart_horizontal="GDG3G-EYl0xMRdrMH0aH_3Gd6ws=/90x56/smart/filters:strip_icc()/" data-url-smart="GDG3G-EYl0xMRdrMH0aH_3Gd6ws=/90x56/smart/filters:strip_icc()/" data-url-feature="GDG3G-EYl0xMRdrMH0aH_3Gd6ws=/90x56/smart/filters:strip_icc()/" data-url-tablet="HJ8gl7oEBQTFb8tHevAYVvQdFgs=/122x75/smart/filters:strip_icc()/" data-url-desktop="Lb9iKRAWn1FByjfbw9guYxxpn2o=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>perfume natural</h3><p>Suaves ou mais fortes? Saiba quais flores perfumadas cultivar em casa</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/Entrevista/noticia/2015/07/nao-tem-nada-mais-gostoso-do-que-beijo-diz-fernanda-nobre.html" title="&#39;NÃ£o tem nada mais gostoso do que beijo&#39;, diz Fernanda Nobre"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/DqKFXTn5b-esfIpkRRzkURA12NY=/filters:quality(10):strip_icc()/s2.glbimg.com/G7ntXLW8pwwdv11IzZpcMqEbeE4=/215x167:512x325/245x130/e.glbimg.com/og/ed/f/original/2015/07/17/qa062010.jpg" alt="&#39;NÃ£o tem nada mais gostoso do que beijo&#39;, diz Fernanda Nobre (Selmy Yassuda, Manoel Marques Guto Seixas ( Ed. Globo))" title="&#39;NÃ£o tem nada mais gostoso do que beijo&#39;, diz Fernanda Nobre (Selmy Yassuda, Manoel Marques Guto Seixas ( Ed. Globo))"
                    data-original-image="s2.glbimg.com/G7ntXLW8pwwdv11IzZpcMqEbeE4=/215x167:512x325/245x130/e.glbimg.com/og/ed/f/original/2015/07/17/qa062010.jpg" data-url-smart_horizontal="YMU7iAoFEQdBa-zSkHrFzyVOM-I=/90x56/smart/filters:strip_icc()/" data-url-smart="YMU7iAoFEQdBa-zSkHrFzyVOM-I=/90x56/smart/filters:strip_icc()/" data-url-feature="YMU7iAoFEQdBa-zSkHrFzyVOM-I=/90x56/smart/filters:strip_icc()/" data-url-tablet="4EVcEDUaEtLBl5Jz-Go1hfdoTjc=/160x95/smart/filters:strip_icc()/" data-url-desktop="S8zIxg_06f4UdEm8l1r4Zz2A1ww=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;NÃ£o tem nada mais gostoso do que beijo&#39;, diz Fernanda Nobre</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/beleza/noticia/2015/07/gisele-frade-ex-malhacao-revela-segredos-de-boa-forma-na-3-gestacao.html" title="Ex-&#39;MalhaÃ§Ã£o&#39; mostra como mantÃ©m forma na 3Âª gestaÃ§Ã£o"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/F7V0s2YIjyspHQblfFBvvT7xcg8=/filters:quality(10):strip_icc()/s2.glbimg.com/D7poYcHxbv9gRNZRQ_sBDeIN43U=/285x46:613x221/245x130/s.glbimg.com/jo/eg/f/original/2015/07/20/img_4927.jpg" alt="Ex-&#39;MalhaÃ§Ã£o&#39; mostra como mantÃ©m forma na 3Âª gestaÃ§Ã£o (ReproduÃ§Ã£o do Instagram)" title="Ex-&#39;MalhaÃ§Ã£o&#39; mostra como mantÃ©m forma na 3Âª gestaÃ§Ã£o (ReproduÃ§Ã£o do Instagram)"
                    data-original-image="s2.glbimg.com/D7poYcHxbv9gRNZRQ_sBDeIN43U=/285x46:613x221/245x130/s.glbimg.com/jo/eg/f/original/2015/07/20/img_4927.jpg" data-url-smart_horizontal="HdJvvosQOI3gyGY5d23ayAJ3UlY=/90x56/smart/filters:strip_icc()/" data-url-smart="HdJvvosQOI3gyGY5d23ayAJ3UlY=/90x56/smart/filters:strip_icc()/" data-url-feature="HdJvvosQOI3gyGY5d23ayAJ3UlY=/90x56/smart/filters:strip_icc()/" data-url-tablet="-RgO0TuYL_ibumMlU1amn9r_4BM=/160x95/smart/filters:strip_icc()/" data-url-desktop="FpHhXJhHF6OK13xROc0lAb8QR8s=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Ex-&#39;MalhaÃ§Ã£o&#39; mostra como mantÃ©m forma na 3Âª gestaÃ§Ã£o</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/07/thaila-ayala-rebate-criticas-e-desabafa-sei-o-valor-do-dinheiro.html" title="&#39;Minha mÃ£e foi empregada. Sei o valor do dinheiro&#39;, diz Thaila Ayala"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/N2hG0g-pCoTdlucg9CamM0ha2vs=/filters:quality(10):strip_icc()/s2.glbimg.com/D2XS_KjeZZORAgTylpY7dgW27AY=/0x65:690x431/245x130/s.glbimg.com/et/gs/f/original/2015/07/20/thaila.jpg" alt="&#39;Minha mÃ£e foi empregada. Sei o valor do dinheiro&#39;, diz Thaila Ayala (Arquivo Pessoal)" title="&#39;Minha mÃ£e foi empregada. Sei o valor do dinheiro&#39;, diz Thaila Ayala (Arquivo Pessoal)"
                    data-original-image="s2.glbimg.com/D2XS_KjeZZORAgTylpY7dgW27AY=/0x65:690x431/245x130/s.glbimg.com/et/gs/f/original/2015/07/20/thaila.jpg" data-url-smart_horizontal="o1LqOENvuK6_gBY0RUD8bH2BjGs=/90x56/smart/filters:strip_icc()/" data-url-smart="o1LqOENvuK6_gBY0RUD8bH2BjGs=/90x56/smart/filters:strip_icc()/" data-url-feature="o1LqOENvuK6_gBY0RUD8bH2BjGs=/90x56/smart/filters:strip_icc()/" data-url-tablet="tcsozc67QOTRUragNIsNQxvbsNs=/160x95/smart/filters:strip_icc()/" data-url-desktop="DWDhCPIBMrzRZc7tnSaKiYX3aGE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;Minha mÃ£e foi empregada. Sei o valor do dinheiro&#39;, diz Thaila Ayala</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/gente/noticia/2015/07/vestido-complica-vida-phoebe-price-em-passeio-com-pet.html" title="Atriz luta para manter no lugar vestido com fenda dupla; fotos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ON0jn16tTbaS-ZOJANFoUQVnPT4=/filters:quality(10):strip_icc()/s2.glbimg.com/x4H0A5qNiLpwSnOaKYrrcn6JX1A=/0x121:620x449/245x130/e.glbimg.com/og/ed/f/original/2015/07/21/481461772.jpg" alt="Atriz luta para manter no lugar vestido com fenda dupla; fotos (Getty Images)" title="Atriz luta para manter no lugar vestido com fenda dupla; fotos (Getty Images)"
                    data-original-image="s2.glbimg.com/x4H0A5qNiLpwSnOaKYrrcn6JX1A=/0x121:620x449/245x130/e.glbimg.com/og/ed/f/original/2015/07/21/481461772.jpg" data-url-smart_horizontal="0zY_GF5RyCck3m3xVG31ellc2TY=/90x56/smart/filters:strip_icc()/" data-url-smart="0zY_GF5RyCck3m3xVG31ellc2TY=/90x56/smart/filters:strip_icc()/" data-url-feature="0zY_GF5RyCck3m3xVG31ellc2TY=/90x56/smart/filters:strip_icc()/" data-url-tablet="d7lcEZHwuL3hoYxnMZ1aYMgxoos=/160x95/smart/filters:strip_icc()/" data-url-desktop="a8v2FbDFuNOFNishXYEYaU25JR8=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Atriz luta para manter no lugar vestido com fenda dupla; fotos</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/babilonia/index.html">BABILÃNIA</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2014/index.html">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/07/margot-se-abala-ao-ver-beijo-de-mari-e-benjamin.html" title="&#39;I Love ParaisÃ³polis&#39;: Margot se abala ao ver beijo de Mari e Ben"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ZXIrMPjrT6T_tlJOt-D_CRySUlI=/filters:quality(10):strip_icc()/s2.glbimg.com/wY9kYuEYNxDRvRrQfCQxVOuW6tU=/0x17:690x384/245x130/s.glbimg.com/et/gs/f/original/2015/07/20/margot-flagra.jpg" alt="&#39;I Love ParaisÃ³polis&#39;: Margot se abala ao ver beijo de Mari e Ben (Tv Globo)" title="&#39;I Love ParaisÃ³polis&#39;: Margot se abala ao ver beijo de Mari e Ben (Tv Globo)"
                    data-original-image="s2.glbimg.com/wY9kYuEYNxDRvRrQfCQxVOuW6tU=/0x17:690x384/245x130/s.glbimg.com/et/gs/f/original/2015/07/20/margot-flagra.jpg" data-url-smart_horizontal="gSGEJ0q-ZJOb4lTNloZxn2VMGwQ=/90x56/smart/filters:strip_icc()/" data-url-smart="gSGEJ0q-ZJOb4lTNloZxn2VMGwQ=/90x56/smart/filters:strip_icc()/" data-url-feature="gSGEJ0q-ZJOb4lTNloZxn2VMGwQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="FV6UpnzrxZ62c16BvJr0vCcRArM=/160x95/smart/filters:strip_icc()/" data-url-desktop="mrXdXCc7xGBfF3tWbhpDAmfnoCo=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;I Love ParaisÃ³polis&#39;: Margot se abala ao ver beijo de Mari e Ben</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/07/livia-e-emilia-discutem.html" title="LÃ­via cobra explicaÃ§Ãµes e discute com a mÃ£e em &#39;AlÃ©m do Tempo&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/XC4mpjbKttfzKKmY8_jvX3rFZ7o=/filters:quality(10):strip_icc()/s2.glbimg.com/AokJMh8dHa__6Ek0vuR_2EOceE8=/0x113:690x479/245x130/s.glbimg.com/et/gs/f/original/2015/07/20/livia-pede-explicacoes-a-ma.jpg" alt="LÃ­via cobra explicaÃ§Ãµes e discute com a mÃ£e em &#39;AlÃ©m do Tempo&#39; (TV Globo)" title="LÃ­via cobra explicaÃ§Ãµes e discute com a mÃ£e em &#39;AlÃ©m do Tempo&#39; (TV Globo)"
                    data-original-image="s2.glbimg.com/AokJMh8dHa__6Ek0vuR_2EOceE8=/0x113:690x479/245x130/s.glbimg.com/et/gs/f/original/2015/07/20/livia-pede-explicacoes-a-ma.jpg" data-url-smart_horizontal="7Q-XBdTH9VLHZ0Pfm5lwlshPGSU=/90x56/smart/filters:strip_icc()/" data-url-smart="7Q-XBdTH9VLHZ0Pfm5lwlshPGSU=/90x56/smart/filters:strip_icc()/" data-url-feature="7Q-XBdTH9VLHZ0Pfm5lwlshPGSU=/90x56/smart/filters:strip_icc()/" data-url-tablet="3yW4Z0LPXOtau_LhrpVOuN_xI4Y=/160x95/smart/filters:strip_icc()/" data-url-desktop="6dCOKvjMtIxFFeRA-g9oEF9Wv4g=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>LÃ­via cobra explicaÃ§Ãµes e discute com a mÃ£e em &#39;AlÃ©m do Tempo&#39;</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2014/Vem-por-ai/noticia/2015/07/bebe-dandael-em-perigo-dandara-passa-mal-e-desmaia.html" title="&#39;MalhaÃ§Ã£o&#39;: Dandara passa mal e LobÃ£o se aproveita da situaÃ§Ã£o"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/x4DbeHD4v5jGBe8Id6V88hFzq2c=/filters:quality(10):strip_icc()/s2.glbimg.com/mPhD6XPmb6yOjBiJxf-a2xlOdQQ=/0x129:690x495/245x130/s.glbimg.com/et/gs/f/original/2015/07/21/dandara_passa_mal_e_desmaia.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Dandara passa mal e LobÃ£o se aproveita da situaÃ§Ã£o (Juliana Alcantara/Gshow)" title="&#39;MalhaÃ§Ã£o&#39;: Dandara passa mal e LobÃ£o se aproveita da situaÃ§Ã£o (Juliana Alcantara/Gshow)"
                    data-original-image="s2.glbimg.com/mPhD6XPmb6yOjBiJxf-a2xlOdQQ=/0x129:690x495/245x130/s.glbimg.com/et/gs/f/original/2015/07/21/dandara_passa_mal_e_desmaia.jpg" data-url-smart_horizontal="hRZo2BvzKnHRLYSbUnpSDzoy6iY=/90x56/smart/filters:strip_icc()/" data-url-smart="hRZo2BvzKnHRLYSbUnpSDzoy6iY=/90x56/smart/filters:strip_icc()/" data-url-feature="hRZo2BvzKnHRLYSbUnpSDzoy6iY=/90x56/smart/filters:strip_icc()/" data-url-tablet="q1pq1MWFl_Pv-M3fiHq0mxRE1rE=/160x95/smart/filters:strip_icc()/" data-url-desktop="Xinf0n9_5IqmtJy0Hl2xkq5jqug=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Dandara passa mal e LobÃ£o se aproveita da situaÃ§Ã£o</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/07/nicolas-prattes-estreia-em-malhacao-ao-lado-de-livian-aragao-ajuda-no-relacionamento.html" title="Namorado de LÃ­vian AragÃ£o diz como Ã© trabalhar com a amada"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/WqnC9fqiEdhFJOvamlPPienAyDQ=/filters:quality(10):strip_icc()/s2.glbimg.com/Pm1qeAgOXsFQLsbHBr6-R_17GvQ=/186x87:690x355/245x130/s.glbimg.com/et/gs/f/original/2015/07/21/nicollas-prattes_livian-aragao.jpg" alt="Namorado de LÃ­vian AragÃ£o diz como Ã© trabalhar com a amada (Arquivo pessoal)" title="Namorado de LÃ­vian AragÃ£o diz como Ã© trabalhar com a amada (Arquivo pessoal)"
                    data-original-image="s2.glbimg.com/Pm1qeAgOXsFQLsbHBr6-R_17GvQ=/186x87:690x355/245x130/s.glbimg.com/et/gs/f/original/2015/07/21/nicollas-prattes_livian-aragao.jpg" data-url-smart_horizontal="iAjav-O52wDcKE_ff41mF2FJbt4=/90x56/smart/filters:strip_icc()/" data-url-smart="iAjav-O52wDcKE_ff41mF2FJbt4=/90x56/smart/filters:strip_icc()/" data-url-feature="iAjav-O52wDcKE_ff41mF2FJbt4=/90x56/smart/filters:strip_icc()/" data-url-tablet="HimZTmeVU0bQ5fs--VWQFA249vs=/160x95/smart/filters:strip_icc()/" data-url-desktop="MPWFi1QLXrnDUmggLf0-_lSJNT4=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Namorado de LÃ­vian AragÃ£o diz como Ã© trabalhar com a amada</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://extra.globo.com/tv-e-lazer/musica/tie-manda-beijo-para-anitta-ao-comemorar-primeiro-lugar-de-noite-entre-as-mais-vendidas-fas-criticam-16874296.html" title="TiÃª manda beijo para Anitta ao comemorar 1Âº lugar entre mais vendidas e fÃ£s criticam (ReproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/U1PFuwcgF0GC0RYMeXeb79zbDDo=/filters:quality(10):strip_icc()/s2.glbimg.com/VIzJES0KhGb1hWmCwjogI-_5xOc=/0x209:448x480/215x130/s.glbimg.com/en/ho/f/original/2015/06/17/tie.jpg"
                data-original-image="s2.glbimg.com/VIzJES0KhGb1hWmCwjogI-_5xOc=/0x209:448x480/215x130/s.glbimg.com/en/ho/f/original/2015/06/17/tie.jpg" data-url-smart_horizontal="duXxGYstFvmoh9hFm3XHUDHqNgQ=/90x56/smart/filters:strip_icc()/" data-url-smart="duXxGYstFvmoh9hFm3XHUDHqNgQ=/90x56/smart/filters:strip_icc()/" data-url-feature="duXxGYstFvmoh9hFm3XHUDHqNgQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="ICFUeW4WQM17AIogQs0FtnruTrc=/120x80/smart/filters:strip_icc()/" data-url-desktop="D9V9T9DVBsOOuSHd6I0U4ZLXIGo=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">TiÃª manda beijo para Anitta ao comemorar 1Âº lugar entre mais vendidas e fÃ£s criticam</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://gq.globo.com/Cultura/noticia/2015/07/show-dos-rolling-stones-em-sp-deve-ocorrer-nos-dias-21-e-23-de-fevereiro-de-2016.html" title="Show dos Rolling Stones em SP deve ocorrer nos dias 21 e 23 de fevereiro de 2016
 (Getty Images)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/eJv7gsdxpPDkhKZRqE4IE1aasA8=/filters:quality(10):strip_icc()/s2.glbimg.com/3r-USFtdLkeKu32l2tVS6xR1uqs=/24x8:591x351/215x130/e.glbimg.com/og/ed/f/original/2015/07/20/rolling-stones.jpg"
                data-original-image="s2.glbimg.com/3r-USFtdLkeKu32l2tVS6xR1uqs=/24x8:591x351/215x130/e.glbimg.com/og/ed/f/original/2015/07/20/rolling-stones.jpg" data-url-smart_horizontal="w_mhuP8xrPTsI-rwPsBImNB2KnU=/90x56/smart/filters:strip_icc()/" data-url-smart="w_mhuP8xrPTsI-rwPsBImNB2KnU=/90x56/smart/filters:strip_icc()/" data-url-feature="w_mhuP8xrPTsI-rwPsBImNB2KnU=/90x56/smart/filters:strip_icc()/" data-url-tablet="cadT8Jo_pXj1o-EyMENts3Wkx3Q=/120x80/smart/filters:strip_icc()/" data-url-desktop="GtqZnX7SlRIkowmcklGy-GhZFHE=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Show dos Rolling Stones em SP deve ocorrer nos dias 21 e 23 de fevereiro de 2016<br /></div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Musica/noticia/2015/07/ex-voice-rully-anne-tem-filho-de-uniao-de-um-ano-e-monta-banda.html" title="Ex-&#39;The Voice Brasil&#39;, Rully Anne mostra o filho recÃ©m-nascido e fala de nova banda (Arquivo Pessoal)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/QHwNgW__Fpsf0QOf4LfrlbkhRvM=/filters:quality(10):strip_icc()/s2.glbimg.com/ziFu45SWLP7poNQw9nyM-EP7o2c=/0x39:690x456/215x130/s.glbimg.com/et/gs/f/original/2015/07/16/rully-anne-the-voice-brasil.jpg"
                data-original-image="s2.glbimg.com/ziFu45SWLP7poNQw9nyM-EP7o2c=/0x39:690x456/215x130/s.glbimg.com/et/gs/f/original/2015/07/16/rully-anne-the-voice-brasil.jpg" data-url-smart_horizontal="z0pHh48SRlGFPDitEGfnyMETlnQ=/90x56/smart/filters:strip_icc()/" data-url-smart="z0pHh48SRlGFPDitEGfnyMETlnQ=/90x56/smart/filters:strip_icc()/" data-url-feature="z0pHh48SRlGFPDitEGfnyMETlnQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="rUg0SfZ8JB_XbO-3YyYUkHz-Y-c=/120x80/smart/filters:strip_icc()/" data-url-desktop="GPhFN1vYOV-lFM6j0qwma-OcPCU=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Ex-&#39;The Voice Brasil&#39;, Rully Anne mostra o filho recÃ©m-nascido e fala de nova banda</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/bemestar/noticia/2015/07/cientistas-nao-podem-ter-cabeca-quadrada-diz-bela-gil-apos-criticas.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Escovar os dentes com cÃºrcuma: &#39;faz quem quer&#39;, diz Bela Gil apÃ³s crÃ­ticas<br /></span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://oglobo.globo.com/economia/empresas-tem-de-esgotar-banco-de-horas-ferias-para-participar-de-plano-de-protecao-ao-emprego-16867271" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Empresas tÃªm de esgotar banco de horas e fÃ©rias para participar de plano de emprego</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sp/santos-regiao/noticia/2015/07/acusada-de-atacar-amiga-com-oleo-e-libertada-e-posta-na-web-de-volta.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Acusada de atacar amiga com Ã³leo Ã© libertada e comemora na web: &#39;TÃ´ de volta&#39;</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://oglobo.globo.com/rio/morre-luiz-paulo-conde-ex-prefeito-do-rio-16865050" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ex-prefeito do Rio, arquiteto Luiz Paulo Conde morre aos 80 anos de cÃ¢ncer</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/noticia/2015/07/prisao-de-4-chineses-faz-policia-de-sp-retomar-investigacao-sobre-mafia.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">VÃ­deo mostra aÃ§Ã£o da PM em resgate de<br />vÃ­tima da mÃ¡fia chinesa em SÃ£o Paulo</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/motor/formula-1/noticia/2015/07/pilotos-comecam-chegar-para-o-funeral-de-jules-bianchi-na-franca.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Pilotos chegam para funeral e Massa chora e leva caixÃ£o no adeus a Bianchi</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/ronda-rousey-chora-ao-comentar-provocacao-de-bethe-sobre-suicidio.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">ProvocaÃ§Ã£o de Bethe sobre suicÃ­dio faz Ronda chorar: &#39;Desrespeita tragÃ©dia familiar&#39;</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/blogs/especial-blog/meio-de-campo/post/carregando-provocacoes-nao-acabam-vasco-usa-titulos-para-ironizar-o-flu.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Vasco cita &#39;histÃ³ria&#39; em foto com trÃªs taÃ§as para zoar Fluminense na web; confira</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/07/orlando-cobra-sao-paulo-na-justica-mas-aceita-acordo-se-receber-ganso.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Orlando cobra SÃ£o Paulo na JustiÃ§a, mas aceita acordo se receber Ganso</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/jogos-pan-americanos/noticia/2015/07/fe-garay-e-adenizia-resolvem-brasil-vence-os-eua-e-vai-semi-no-volei.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">FÃª Garay e AdenÃ­zia resolvem, Brasil vence os EUA e vai Ã  semi do vÃ´lei no Pan</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/casamento/noticia/2015/07/casamento-de-juju-salimeni-tera-cardapio-fitness-e-show-de-anitta.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Casamento de Juju e Felipe Franco terÃ¡ buffet fitness com picolÃ© de whey protein</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/07/com-look-decotado-e-transparente-aline-gotschalg-vai-evento-de-moda.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">De vestido transparente e decotÃ£o, Aline rouba cena com beijÃ£o em FÃª em desfile</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/07/alex-admitira-que-se-casou-com-carolina-por-causa-de-angel.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Em &#39;Verdades Secretas&#39;, Alex admitirÃ¡ que se casou com Carolina por causa de Angel</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/beleza/noticia/2015/07/lorena-improta-finalista-do-bailarina-do-faustao-diz-ter-perdido-12-quilos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Finalista do &#39;Bailarina do FaustÃ£o&#39; diz que Ã© campeÃ£ de karatÃª e perdeu 12kg; veja</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/noite/noticia/2015/07/prevenida-carla-perez-deixa-shortinho-mostra-em-show-de-xanddy-no-rio.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Carla Perez rouba a cena e se previne com shortinho ao descer atÃ© o chÃ£o; veja fotos</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://tvg.globo.com/programas/pe-na-cova/index.html">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://tvg.globo.com/programas/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/e3ddebc999f7.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){setTimeout(function(){var bannerMobiles = $(".opec-mobile .tag-manager-publicidade-container");bannerMobiles.each(function(index,banner){var $banner = $(banner);if($banner.height() < 15){$banner.parent().addClass('without-opec');}});}, 1300);}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 6000);}
</script><style> .opec-x60{height:auto}</style><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 10};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 21/07/2015 22:09:31 -->
