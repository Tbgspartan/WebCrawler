<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.1" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.1" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.1"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.1"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.1"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><script>//$( document ).ready(function() {//      $('#top-slide').show();//        $('.marqueeSlides').marquee({//      speed: 14000,//      gap: 50,//      delayBeforeStart: 0,//      direction: 'left',//      pauseOnHover: true//});//});</script><script>(function() {var _fbq = window._fbq || (window._fbq = []);if (!_fbq.loaded) {var fbds = document.createElement('script');fbds.async = true;fbds.src = '//connect.facebook.net/en_US/fbds.js';var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(fbds, s);_fbq.loaded = true;}_fbq.push(['addPixelId', '624029644406124']);})();window._fbq = window._fbq || [];window._fbq.push(['track', 'PixelInitialized', {}]);</script><noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=624029644406124&amp;ev=PixelInitialized" /></noscript>       <script type="text/javascript">    var IMAGESITEPATH = 'http://media.indiatimes.in/resources/images';    $("document").ready(function() {                    $("img.lazy").lazyload({                    effect: "fadeIn",                    placeholder:null,                    threshold : 100,                    failurelimit: 1000                });        $("img.photolazy").lazyload({                    effect: "fadeIn",                    threshold : 100,                    placeholder:null,                                                            failurelimit: 1000                });       });</script>    <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.1"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" >Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
                    <!-- <a  id='headerSocialFb' href="javascript:void(0);" title="facebook" class="sprite fbs" data-social='{"type":"facebook", "url":"", "text": "", "image":"","desc":""}'>facebook</a>-->
                    <!-- <a id='headerSocialTwitter' title="twitter" class="sprite twts" href="https://twitter.com/intent/tweet?url=&text=">twitter</a> -->
            </div>
                        
            
			 <div class="social fr">
			    <!-- <a title="video" class="sprite video-top" target="_blank" href="">video</a> -->
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                            <!-- <a title="user" class="sprite bell" href="javascript:void(0);">user<span class="not">34</span></a> -->
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
                <!-- <a href="javascript:void(0)" class="gleft sprite"> gplus</a> --> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->        
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
    </div><!--pull-ad end-->
</div>
    <!--testing 15-07-22 06:20:02-->    <section class="topslide-cont cf"><!--container start-->

         
 <div class="top-slide cf" id="top-slide" style="display:none;"><!--top-slide start-->
        <div class="top-slide-left pink-bg"><span class="yellow">TRENDING TODAY</span></div>
        <div class="top-slide-right skyblue-bg"><!--top-slide-right start-->
            <div class="flexslider">
                <div class="slides marqueeSlides" style=" max-width:800px;margin:auto; overflow:hidden">
				      
                    <span style="padding-right:50px;"><a title="Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over" href="http://www.indiatimes.com/videocafe/sujoy-ghoshs-epic-thriller-ahalya-starring-radhika-apte-will-leave-you-shivering-all-over-235121.html">Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over</a></span>
					 
                    <span style="padding-right:50px;"><a title="51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!" href="http://www.indiatimes.com/lifestyle/self/40-vintage-life-hacks-that-are-100-years-old-but-just-as-useful-235068.html">51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!</a></span>
					 
                    <span style="padding-right:50px;"><a title="11 Trucks That Are Having A Way Worse Day Than You" href="http://www.indiatimes.com/culture/who-we-are/11-trucks-that-are-having-a-way-worse-day-than-you-235103.html">11 Trucks That Are Having A Way Worse Day Than You</a></span>
					 
                    <span style="padding-right:50px;"><a title="Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!" href="http://www.indiatimes.com/lifestyle/self/neha-dhupia-talking-about-good-governance-on-social-media-is-definitely-a-first-for-us-235119.html">Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!</a></span>
					 
                    <span style="padding-right:50px;"><a title="Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra" href="http://www.indiatimes.com/entertainment/meet-the-small-wonder-behind-the-big-success-bajrangi-bhaijaan-munni-aka-harshaali-malhotra-235104.html">Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra</a></span>
					 
                    <span style="padding-right:50px;"><a title="12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try" href="http://www.indiatimes.com/lifestyle/style/12-horrible-hairstyles-inspired-by-bollywood-no-indian-man-should-try-234984.html">12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try</a></span>
					 
                    <span style="padding-right:50px;"><a title="Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why" href="http://www.indiatimes.com/entertainment/director-kabir-khan-loses-his-cool-over-shobhaa-des-review-of-bajrangi-bhaijaan-heres-why-235099.html">Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why</a></span>
					 
                    <span style="padding-right:50px;"><a title="A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less" href="http://www.indiatimes.com/entertainment/a-motorist-blamed-big-b-for-frequent-traffic-snarls-but-mr-bachchan-couldnt-care-less-235079.html">A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less</a></span>
					 
                    <span style="padding-right:50px;"><a title="Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now" href="http://www.indiatimes.com/videocafe/way-we-treat-indians-from-north-east-states-is-appalling-and-it-needs-to-stop-now-235044.html">Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now</a></span>
					 
                    <span style="padding-right:50px;"><a title="10 Things To Do To Make It The Most Memorable 24 Hours In Delhi" href="http://www.indiatimes.com/culture/travel/10-things-to-do-to-make-it-the-most-memorable-24-hours-in-delhi-233651.html">10 Things To Do To Make It The Most Memorable 24 Hours In Delhi</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly" href="http://www.indiatimes.com/health/tips-tricks/9-everyday-habits-we-didnt-know-are-damaging-our-brains-slowly-235039.html">9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly</a></span>
					 
                    <span style="padding-right:50px;"><a title="Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!" href="http://www.indiatimes.com/lifestyle/self/heres-the-comic-book-that-inspired-the-rs-300-crore-opus-bahubali-235017.html">Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!</a></span>
					 
                    <span style="padding-right:50px;"><a title="Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'" href="http://www.indiatimes.com/entertainment/father-son-duo-behind-two-biggest-hits-of-the-season-baahubali-and-bajrangi-bhaijaan-235051.html">Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Tips To Double The Effectiveness Of Your Workout" href="http://www.indiatimes.com/health/tips-tricks/9-tips-to-double-the-effectiveness-of-your-workout-235048.html">9 Tips To Double The Effectiveness Of Your Workout</a></span>
					 
                    <span style="padding-right:50px;"><a title="Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!" href="http://www.indiatimes.com/entertainment/bollywood/bhai-does-it-again-salmans-bajrangi-bhaijaan-is-crushing-all-boxoffice-records-235019.html">Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!</a></span>
					 
                    <span style="padding-right:50px;"><a title="8 Bollywood A-listers Remember Their First Pay Packet" href="http://www.indiatimes.com/entertainment/bollywood/8-bollywood-alisters-remember-their-first-pay-packet-235053.html">8 Bollywood A-listers Remember Their First Pay Packet</a></span>
					 
                    <span style="padding-right:50px;"><a title="21 Weird Looking Things You Won't Believe Were Actually Designed To Fly" href="http://www.indiatimes.com/lifestyle/technology/21-weird-looking-things-you-wont-believe-were-actually-designed-to-fly-234888.html">21 Weird Looking Things You Won't Believe Were Actually Designed To Fly</a></span>
					 
                    <span style="padding-right:50px;"><a title="10 Powerful Quotes That Will Change The Way You Live And Think" href="http://www.indiatimes.com/culture/travel/10-powerful-quotes-that-will-change-the-way-you-live-and-think-234925.html">10 Powerful Quotes That Will Change The Way You Live And Think</a></span>
					 
                    <span style="padding-right:50px;"><a title="7 Easy And Healthy Salad Recipes For Every Day Of The Week" href="http://www.indiatimes.com/culture/food/7-easy-and-healthy-salad-recipes-for-every-day-of-the-week-235030.html">7 Easy And Healthy Salad Recipes For Every Day Of The Week</a></span>
					 
                    <span style="padding-right:50px;"><a title="Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters" href="http://www.indiatimes.com/entertainment/bollywood/pakistan-censor-board-chops-off-reference-to-kashmir-before-the-screening-of-bajrangi-bhaijaan-in-theaters-235041.html">Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Less Famous Life Partners Of Some Famous Bollywood Stars" href="http://www.indiatimes.com/entertainment/bollywood/9-less-famous-life-partners-of-some-famous-bollywood-stars-235049.html">9 Less Famous Life Partners Of Some Famous Bollywood Stars</a></span>
					 
                    <span style="padding-right:50px;"><a title="Working Too Hard And Too Much Might Be Making You Infertile!" href="http://www.indiatimes.com/health/healthyliving/working-too-hard-and-too-much-might-be-making-you-infertile-235036.html">Working Too Hard And Too Much Might Be Making You Infertile!</a></span>
					 
                    <span style="padding-right:50px;"><a title="11 Bollywood Actors Who Totally Rocked The Moustached Look On Screen!" href="http://www.indiatimes.com/entertainment/bollywood/11-bollywood-actors-who-totally-rocked-the-moustached-look-on-screen-233573.html">11 Bollywood Actors Who Totally Rocked The Moustached Look On Screen!</a></span>
					 
                    <span style="padding-right:50px;"><a title="11 Hidden Treasure Troves That, If Found, Could Make You Really, Really Rich" href="http://www.indiatimes.com/lifestyle/self/11-hidden-treasure-troves-that-if-found-could-make-you-really-really-rich-234961.html">11 Hidden Treasure Troves That, If Found, Could Make You Really, Really Rich</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Daily Struggles Of People Who Are Easily Distracted" href="http://www.indiatimes.com/lifestyle/self/9-daily-struggles-of-people-who-are-easily-distracted-234971.html">9 Daily Struggles Of People Who Are Easily Distracted</a></span>
					                    
                </div>
            </div>
            <a title="close" class="sprite close" href="javascript:void(0);">close</a>
        </div><!--top-slide-right end-->
    </div><!--top-slide end-->
    </section>

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             19 hours ago                         </div>
						 						                            <!--<a class="news_numbers" title="" href="">.</a>  -->      
                        <a href="http://www.indiatimes.com/news/india/sanskrit-was-first-spoken-in-syria-not-india-+-5-more-history-facts-that-will-shock-you-235071.html" class=" tint" title="Sanskrit Was First Spoken In Syria, Not India + 5 More History Facts That Will Shock You!">
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Jul/vina-cc-6_1437456722_1437456726_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/vina-cc-6_1437456722_1437456726_236x111.jpg"  border="0" alt="Sanskrit Was First Spoken In Syria, Not India + 5 More History Facts That Will Shock You!"/></a>
                    </figure>
                    <!--div class="hash-tag"><a href="javascript:void(0);" title="" class="pink">#HEROISL</a></div-->
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/sanskrit-was-first-spoken-in-syria-not-india-+-5-more-history-facts-that-will-shock-you-235071.html" title="Sanskrit Was First Spoken In Syria, Not India + 5 More History Facts That Will Shock You!">
                            Sanskrit Was First Spoken In Syria, Not India + 5 More History Facts That Will Shock You!                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        
			<!-- <a class="news_numbers" title="" href="">.</a> -->			
						 	                        <a href="http://www.indiatimes.com/news/india/this-man-tried-to-protect-his-daughter-from-eve-teasers-in-retaliation-they-set-him-on-fire-235124.html" title="This Man Tried To Protect His Daughter From Eve Teasers. In Retaliation, They Set Him On Fire" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Jul/500_1437482852_1437482865_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/500_1437482852_1437482865_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-man-tried-to-protect-his-daughter-from-eve-teasers-in-retaliation-they-set-him-on-fire-235124.html" title="This Man Tried To Protect His Daughter From Eve Teasers. In Retaliation, They Set Him On Fire">
                            This Man Tried To Protect His Daughter From Eve Teasers. In Retaliation, They Set Him On Fire                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        
			<!-- <a class="news_numbers" title="" href="">.</a> -->			
						 	                        <a href="http://www.indiatimes.com/news/sports/15yearold-sensation-making-waves-in-indian-swimming-235123.html" title="15-Year-Old Sensation Making Waves In Indian Swimming" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Jul/maanadna502_1437481880_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/maanadna502_1437481880_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/15yearold-sensation-making-waves-in-indian-swimming-235123.html" title="15-Year-Old Sensation Making Waves In Indian Swimming">
                            15-Year-Old Sensation Making Waves In Indian Swimming                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        
			<!-- <a class="news_numbers" title="" href="">.</a> -->			
						 	                        <a href="http://www.indiatimes.com/news/india/in-this-karnataka-village-you-dont-need-a-stove-to-cook-just-light-a-match-to-the-ground-235118.html" title="In This Karnataka Village You Don't Need A Stove To Cook, Just Light A Match To The Ground" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage-5_1437480249_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage-5_1437480249_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/in-this-karnataka-village-you-dont-need-a-stove-to-cook-just-light-a-match-to-the-ground-235118.html" title="In This Karnataka Village You Don't Need A Stove To Cook, Just Light A Match To The Ground">
                            In This Karnataka Village You Don't Need A Stove To Cook, Just Light A Match To The Ground                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        
			<!-- <a class="news_numbers" title="" href="">.</a> -->			
						 	                                <a href="http://www.indiatimes.com/news/sports/ashes-2015-this-lady-brought-the-lords-test-to-a-halt-with-just-a-slight-misjudgement-235122.html" class="lol sticker">&nbsp;</a>
                            <a class='video-btn sprite' href='http://www.indiatimes.com/news/sports/ashes-2015-this-lady-brought-the-lords-test-to-a-halt-with-just-a-slight-misjudgement-235122.html'>video</a>                        <a href="http://www.indiatimes.com/news/sports/ashes-2015-this-lady-brought-the-lords-test-to-a-halt-with-just-a-slight-misjudgement-235122.html" title="Ashes 2015: This Lady Brought The Lord's Test To A Halt With Just A Slight Misjudgement" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/videocafe/2015/Jul/ashesladychair2_1437481387_236x111.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Jul/ashesladychair2_1437481387_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/ashes-2015-this-lady-brought-the-lords-test-to-a-halt-with-just-a-slight-misjudgement-235122.html" title="Ashes 2015: This Lady Brought The Lord's Test To A Halt With Just A Slight Misjudgement">
                            Ashes 2015: This Lady Brought The Lord's Test To A Halt With Just A Slight Misjudgement                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/travel/10-powerful-quotes-that-will-change-the-way-you-live-and-think-234925.html" class=" tint" title="10 Powerful Quotes That Will Change The Way You Live And Think" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/10-powerful-quotes-that-will-change-the-way-you-live-and-think-234925.html');">
                                <img class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Jul/card_1437471700_1437471709_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/card_1437471700_1437471709_502x234.jpg"  border="0" alt="10 Powerful Quotes That Will Change The Way You Live And Think" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/travel/10-powerful-quotes-that-will-change-the-way-you-live-and-think-234925.html" title="10 Powerful Quotes That Will Change The Way You Live And Think" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/10-powerful-quotes-that-will-change-the-way-you-live-and-think-234925.html');">10 Powerful Quotes That Will Change The Way You Live And Think</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/travel/10-things-to-do-to-make-it-the-most-memorable-24-hours-in-delhi-233651.html" class=" tint" title="10 Things To Do To Make It The Most Memorable 24 Hours In Delhi" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/10-things-to-do-to-make-it-the-most-memorable-24-hours-in-delhi-233651.html');">
                                <img class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Jun/nargis-card_1434547518_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jun/nargis-card_1434547518_502x234.jpg"  border="0" alt="10 Things To Do To Make It The Most Memorable 24 Hours In Delhi" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/travel/10-things-to-do-to-make-it-the-most-memorable-24-hours-in-delhi-233651.html" title="10 Things To Do To Make It The Most Memorable 24 Hours In Delhi" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/10-things-to-do-to-make-it-the-most-memorable-24-hours-in-delhi-233651.html');">10 Things To Do To Make It The Most Memorable 24 Hours In Delhi</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/11-types-of-service-dogs-who-make-the-lives-of-their-humans-beautiful-235064.html" class="tint" title="11 Types Of Service Dogs Who Make The Lives Of Their Humans Beautiful" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/11-types-of-service-dogs-who-make-the-lives-of-their-humans-beautiful-235064.html');">
                            <img class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Jul/card-image_1437393062_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/card-image_1437393062_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-types-of-service-dogs-who-make-the-lives-of-their-humans-beautiful-235064.html" title="11 Types Of Service Dogs Who Make The Lives Of Their Humans Beautiful" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/11-types-of-service-dogs-who-make-the-lives-of-their-humans-beautiful-235064.html');">
                            11 Types Of Service Dogs Who Make The Lives Of Their Humans Beautiful                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/sujoy-ghoshs-epic-thriller-ahalya-starring-radhika-apte-will-leave-you-shivering-all-over-235121.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/sujoy-ghoshs-epic-thriller-ahalya-starring-radhika-apte-will-leave-you-shivering-all-over-235121.html" class="tint" title="Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over">


                            <img class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Jul/ahlya_card_1437481138_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Jul/ahlya_card_1437481138_218x102.jpg" border="0" alt="Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/sujoy-ghoshs-epic-thriller-ahalya-starring-radhika-apte-will-leave-you-shivering-all-over-235121.html" title="Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over">
                            Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/40-vintage-life-hacks-that-are-100-years-old-but-just-as-useful-235068.html" class="tint" title="51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!">


                            <img class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Jul/jug502_1437473421_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/jug502_1437473421_218x102.jpg" border="0" alt="51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/40-vintage-life-hacks-that-are-100-years-old-but-just-as-useful-235068.html" title="51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!">
                            51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/11-trucks-that-are-having-a-way-worse-day-than-you-235103.html" class="tint" title="11 Trucks That Are Having A Way Worse Day Than You">


                            <img class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Jul/truck_1437469546_1437469558_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/truck_1437469546_1437469558_218x102.jpg" border="0" alt="11 Trucks That Are Having A Way Worse Day Than You"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-trucks-that-are-having-a-way-worse-day-than-you-235103.html" title="11 Trucks That Are Having A Way Worse Day Than You">
                            11 Trucks That Are Having A Way Worse Day Than You                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/neha-dhupia-talking-about-good-governance-on-social-media-is-definitely-a-first-for-us-235119.html" class="tint" title="Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!">


                            <img class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Jul/neha-d502_1437485871_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/neha-d502_1437485871_218x102.jpg" border="0" alt="Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/neha-dhupia-talking-about-good-governance-on-social-media-is-definitely-a-first-for-us-235119.html" title="Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!">
                            Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/meet-the-small-wonder-behind-the-big-success-bajrangi-bhaijaan-munni-aka-harshaali-malhotra-235104.html" class="tint" title="Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra">


                            <img class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Jul/hm-card_1437474034_1437474045_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/hm-card_1437474034_1437474045_218x102.jpg" border="0" alt="Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/meet-the-small-wonder-behind-the-big-success-bajrangi-bhaijaan-munni-aka-harshaali-malhotra-235104.html" title="Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra">
                            Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!---------------------    end Block 1  --------------------------------------->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!---------------------    start Block 2  --------------------------------------->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/heres-how-singapore-is-going-to-create-a-high-tech-amravati-for-andhra-pradesh-235110.html" title="Here's How Singapore Is Going To Create A High Tech Capital For Andhra Pradesh" class=" tint">
                           

                            <img class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage-5_1437475295_1437475312_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/heres-how-singapore-is-going-to-create-a-high-tech-amravati-for-andhra-pradesh-235110.html" title="Here's How Singapore Is Going To Create A High Tech Capital For Andhra Pradesh">
                            Here's How Singapore Is Going To Create A High Tech Capital For Andhra Pradesh                        </a>
                    </figcaption>  
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/no-monkey-business-family-car-hijacked-by-a-band-of-pirate-baboons-235117.html" title="No Monkey Business: Family Car Hijacked By A Band Of Pirate Baboons" class=" tint">
                           

                            <img class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/500_1437478366_1437478373_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/no-monkey-business-family-car-hijacked-by-a-band-of-pirate-baboons-235117.html" title="No Monkey Business: Family Car Hijacked By A Band Of Pirate Baboons">
                            No Monkey Business: Family Car Hijacked By A Band Of Pirate Baboons                        </a>
                    </figcaption>  
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/wellofdeath-oneyearold-dead-5-others-injured-after-stuntman-crashes-into-crowd-235111.html" title="#WellOfDeath One-Year-Old Dead, 5 Others Injured After Stuntman Crashes Into Crowd" class=" tint">
                           

                            <img class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/500_1437475619_1437475623_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/wellofdeath-oneyearold-dead-5-others-injured-after-stuntman-crashes-into-crowd-235111.html" title="#WellOfDeath One-Year-Old Dead, 5 Others Injured After Stuntman Crashes Into Crowd">
                            #WellOfDeath One-Year-Old Dead, 5 Others Injured After Stuntman Crashes Into Crowd                        </a>
                    </figcaption>  
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/sports/story-of-paul-van-ass-when-its-coaches-vs-babus-in-indian-hockey-coaches-always-lose-235107.html" title="Story Of Paul Van Ass: When It's Coaches Vs Babus In Indian Hockey, Coaches Always Lose" class=" tint">
                           

                            <img class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/narinderbatravanass_1437472821_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/story-of-paul-van-ass-when-its-coaches-vs-babus-in-indian-hockey-coaches-always-lose-235107.html" title="Story Of Paul Van Ass: When It's Coaches Vs Babus In Indian Hockey, Coaches Always Lose">
                            Story Of Paul Van Ass: When It's Coaches Vs Babus In Indian Hockey, Coaches Always Lose                        </a>
                    </figcaption>  
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/22-blind-students-get-full-scholarships-to-study-in-shimlas-leading-universities-235109.html" title="22 Blind Students Get Full Scholarships To Study In Shimla's Leading Universities" class=" tint">
                           

                            <img class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/asunews-5_1437472883_1437472891_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/22-blind-students-get-full-scholarships-to-study-in-shimlas-leading-universities-235109.html" title="22 Blind Students Get Full Scholarships To Study In Shimla's Leading Universities">
                            22 Blind Students Get Full Scholarships To Study In Shimla's Leading Universities                        </a>
                    </figcaption>  
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/tips-tricks/follow-these-simple-steps-to-correct-your-posture-while-using-the-computer-235033.html" class="tint" title="Follow These Simple Steps To Correct Your Posture While Using The Computer">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/posture_1437377797_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/follow-these-simple-steps-to-correct-your-posture-while-using-the-computer-235033.html" title="Follow These Simple Steps To Correct Your Posture While Using The Computer">
                            Follow These Simple Steps To Correct Your Posture While Using The Computer                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-trucks-that-are-having-a-way-worse-day-than-you-235103.html" class="tint" title="11 Trucks That Are Having A Way Worse Day Than You">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/truck_1437469546_1437469558_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-trucks-that-are-having-a-way-worse-day-than-you-235103.html" title="11 Trucks That Are Having A Way Worse Day Than You">
                            11 Trucks That Are Having A Way Worse Day Than You                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/sujoy-ghoshs-epic-thriller-ahalya-starring-radhika-apte-will-leave-you-shivering-all-over-235121.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/sujoy-ghoshs-epic-thriller-ahalya-starring-radhika-apte-will-leave-you-shivering-all-over-235121.html" class="tint" title="Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/ahlya_card_1437481138_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/sujoy-ghoshs-epic-thriller-ahalya-starring-radhika-apte-will-leave-you-shivering-all-over-235121.html" title="Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over">
                            Sujoy Ghosh's Epic Thriller 'Ahalya' Starring Radhika Apte Will Leave You Shivering All Over                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/style/12-horrible-hairstyles-inspired-by-bollywood-no-indian-man-should-try-234984.html" class="tint" title="12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/cp_1437219258_218x102.jpg" border="0" alt="12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/style/12-horrible-hairstyles-inspired-by-bollywood-no-indian-man-should-try-234984.html" title="12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try">
                            12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/director-kabir-khan-loses-his-cool-over-shobhaa-des-review-of-bajrangi-bhaijaan-heres-why-235099.html" class="tint" title="Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage_1437468511_1437468520_218x102.jpg" border="0" alt="Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/director-kabir-khan-loses-his-cool-over-shobhaa-des-review-of-bajrangi-bhaijaan-heres-why-235099.html" title="Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why">
                            Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/a-motorist-blamed-big-b-for-frequent-traffic-snarls-but-mr-bachchan-couldnt-care-less-235079.html" class="tint" title="A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/bachchan_660_082912051941_1437456288_1437456295_218x102.jpg" border="0" alt="A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/a-motorist-blamed-big-b-for-frequent-traffic-snarls-but-mr-bachchan-couldnt-care-less-235079.html" title="A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less">
                            A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/way-we-treat-indians-from-north-east-states-is-appalling-and-it-needs-to-stop-now-235044.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/way-we-treat-indians-from-north-east-states-is-appalling-and-it-needs-to-stop-now-235044.html" class="tint" title="Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/northeast_card_1437383422_218x102.jpg" border="0" alt="Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/way-we-treat-indians-from-north-east-states-is-appalling-and-it-needs-to-stop-now-235044.html" title="Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now">
                            Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/travel/10-things-to-do-to-make-it-the-most-memorable-24-hours-in-delhi-233651.html" class="tint" title="10 Things To Do To Make It The Most Memorable 24 Hours In Delhi">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jun/nargis-card_1434547518_218x102.jpg" border="0" alt="10 Things To Do To Make It The Most Memorable 24 Hours In Delhi"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/10-things-to-do-to-make-it-the-most-memorable-24-hours-in-delhi-233651.html" title="10 Things To Do To Make It The Most Memorable 24 Hours In Delhi">
                            10 Things To Do To Make It The Most Memorable 24 Hours In Delhi                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            15 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/few-years-down-you-will-be-able-to-take-a-ropeway-cab-from-delhi-to-haryana-235106.html" title="Few Years Down, You Will Be Able To Take A Ropeway Cab From Delhi To Haryana!" class=" tint">
                            

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/mygola-6_1437471663_1437471667_236x111.jpg" border="0" alt="Few Years Down, You Will Be Able To Take A Ropeway Cab From Delhi To Haryana!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/few-years-down-you-will-be-able-to-take-a-ropeway-cab-from-delhi-to-haryana-235106.html" title="Few Years Down, You Will Be Able To Take A Ropeway Cab From Delhi To Haryana!">
                            Few Years Down, You Will Be Able To Take A Ropeway Cab From Delhi To Haryana!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            15 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/sports/sad-isis-destroys-iraqi-olympic-stadium-235101.html" title="#Sad ISIS Destroys Iraqi Olympic Stadium" class=" tint">
                            

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/ramadistadium2502_1437470165_236x111.jpg" border="0" alt="#Sad ISIS Destroys Iraqi Olympic Stadium"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/sad-isis-destroys-iraqi-olympic-stadium-235101.html" title="#Sad ISIS Destroys Iraqi Olympic Stadium">
                            #Sad ISIS Destroys Iraqi Olympic Stadium In Ramadi                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            15 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/horror-in-agra-a-dalit-woman-was-gangraped-chopped-up-and-burnt-235097.html" title="Horror In Agra: A Dalit Woman Was Gangraped, Chopped Up And Burnt" class=" tint">
                            

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/loukavar_1437465667_1437465679_236x111.jpg" border="0" alt="Horror In Agra: A Dalit Woman Was Gangraped, Chopped Up And Burnt"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/horror-in-agra-a-dalit-woman-was-gangraped-chopped-up-and-burnt-235097.html" title="Horror In Agra: A Dalit Woman Was Gangraped, Chopped Up And Burnt">
                            Horror In Agra: A Dalit Woman Was Gangraped, Chopped Up And Burnt                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/nasas-latest-image-of-mother-earth-+-6-other-magnificent-earth-shots-that-are-absolute-stunners-235091.html" title="NASA's Latest Image Of Mother Earth + 6 Other Magnificent Earth Shots That Are Absolute Stunners" class=" tint">
                            

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card-502_1437461447_236x111.jpg" border="0" alt="NASA's Latest Image Of Mother Earth + 6 Other Magnificent Earth Shots That Are Absolute Stunners"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/nasas-latest-image-of-mother-earth-+-6-other-magnificent-earth-shots-that-are-absolute-stunners-235091.html" title="NASA's Latest Image Of Mother Earth + 6 Other Magnificent Earth Shots That Are Absolute Stunners">
                            NASA's Latest Image Of Mother Earth + 6 Other Magnificent Earth Shots That Are Absolute Stunners                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/sports/dreams-of-being-indias-manny-pacquiao-and-other-reasons-why-vijender-singh-took-to-proboxing-235095.html" title="Dreams Of Being India's Manny Pacquiao And Other Reasons Why Vijender Singh Took To Proboxing" class=" tint">
                            

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/vijenderprobox_1437463384_236x111.jpg" border="0" alt="Dreams Of Being India's Manny Pacquiao And Other Reasons Why Vijender Singh Took To Proboxing"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/dreams-of-being-indias-manny-pacquiao-and-other-reasons-why-vijender-singh-took-to-proboxing-235095.html" title="Dreams Of Being India's Manny Pacquiao And Other Reasons Why Vijender Singh Took To Proboxing">
                            Dreams Of Being India's Manny Pacquiao And Other Reasons Why Vijender Singh Took To Proboxing                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/neha-dhupia-talking-about-good-governance-on-social-media-is-definitely-a-first-for-us-235119.html" class="tint" title="Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!">
                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/neha-d502_1437485871_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/self/neha-dhupia-talking-about-good-governance-on-social-media-is-definitely-a-first-for-us-235119.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/neha-dhupia-talking-about-good-governance-on-social-media-is-definitely-a-first-for-us-235119.html" title="Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!">
                            Neha Dhupia Talking About Good Governance On Social Media Is Definitely A First For Us!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-very-odd-and-unreal-experiences-of-people-coming-out-of-coma-234988.html" class="tint" title="9 Very Odd And Unreal Experiences Of People Coming Out Of Coma">
                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card_1437222951_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/who-we-are/9-very-odd-and-unreal-experiences-of-people-coming-out-of-coma-234988.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-very-odd-and-unreal-experiences-of-people-coming-out-of-coma-234988.html" title="9 Very Odd And Unreal Experiences Of People Coming Out Of Coma">
                            9 Very Odd And Unreal Experiences Of People Coming Out Of Coma                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-two-people-love-each-other-and-want-to-get-married-but-yet-again-society-has-a-problem-235102.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/these-two-people-love-each-other-and-want-to-get-married-but-yet-again-society-has-a-problem-235102.html" class="tint" title="These Two People Love Each Other, And Want To Get Married. But Yet Again, Society Has A Problem">
                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/samesex_card_1437470241_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/these-two-people-love-each-other-and-want-to-get-married-but-yet-again-society-has-a-problem-235102.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/these-two-people-love-each-other-and-want-to-get-married-but-yet-again-society-has-a-problem-235102.html" title="These Two People Love Each Other, And Want To Get Married. But Yet Again, Society Has A Problem">
                            These Two People Love Each Other, And Want To Get Married. But Yet Again, Society Has A Problem                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/tips-tricks/9-everyday-habits-we-didnt-know-are-damaging-our-brains-slowly-235039.html" class="tint" title="9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/header_1435325204_980x457_1437380611_1437380616_218x102.jpg" border="0" alt="9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/9-everyday-habits-we-didnt-know-are-damaging-our-brains-slowly-235039.html" title="9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly">
                            9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/heres-the-comic-book-that-inspired-the-rs-300-crore-opus-bahubali-235017.html" class="tint" title="Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/bahu502_1437374617_1437374622_218x102.jpg" border="0" alt="Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/heres-the-comic-book-that-inspired-the-rs-300-crore-opus-bahubali-235017.html" title="Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!">
                            Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/father-son-duo-behind-two-biggest-hits-of-the-season-baahubali-and-bajrangi-bhaijaan-235051.html" class="tint" title="Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/2014-10-27_499_home_rajamouli-card_1437385884_1437385887_1437385894_218x102.jpg" border="0" alt="Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/father-son-duo-behind-two-biggest-hits-of-the-season-baahubali-and-bajrangi-bhaijaan-235051.html" title="Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'">
                            Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/tips-tricks/9-tips-to-double-the-effectiveness-of-your-workout-235048.html" class="tint" title="9 Tips To Double The Effectiveness Of Your Workout">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/shutterstock_167065445_1437384114_1437384123_218x102.jpg" border="0" alt="9 Tips To Double The Effectiveness Of Your Workout"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/9-tips-to-double-the-effectiveness-of-your-workout-235048.html" title="9 Tips To Double The Effectiveness Of Your Workout">
                            9 Tips To Double The Effectiveness Of Your Workout                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/bhai-does-it-again-salmans-bajrangi-bhaijaan-is-crushing-all-boxoffice-records-235019.html" class="tint" title="Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/bajrangi-bhaijaan-poster-story-collection_1437371540_1437371548_218x102.jpg" border="0" alt="Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/bhai-does-it-again-salmans-bajrangi-bhaijaan-is-crushing-all-boxoffice-records-235019.html" title="Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!">
                            Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/mistakes-caused-15-times-more-deaths-than-natural-calamities-last-year-in-india-235092.html" title="Human Mistakes Caused 15 Times More Deaths Than Natural Calamities In India Last Year" class=" tint">
                            
                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage-5_1437461799_236x111.jpg" border="0" alt="Human Mistakes Caused 15 Times More Deaths Than Natural Calamities In India Last Year"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/mistakes-caused-15-times-more-deaths-than-natural-calamities-last-year-in-india-235092.html" title="Human Mistakes Caused 15 Times More Deaths Than Natural Calamities In India Last Year">
                            Human Mistakes Caused 15 Times More Deaths Than Natural Calamities In India Last Year                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/shame-21yearold-hacked-to-death-on-a-crowded-street-road-in-mumbais-nala-sopara-yet-nobody-came-to-his-rescue-235055.html" title="#Shame 21-Year-Old Hacked To Death On A Crowded Street Road, Yet Nobody Came To His Rescue" class=" tint">
                            
                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/honour_killing_2154-card-_1437457513_1437457522_236x111.jpg" border="0" alt="#Shame 21-Year-Old Hacked To Death On A Crowded Street Road, Yet Nobody Came To His Rescue"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/shame-21yearold-hacked-to-death-on-a-crowded-street-road-in-mumbais-nala-sopara-yet-nobody-came-to-his-rescue-235055.html" title="#Shame 21-Year-Old Hacked To Death On A Crowded Street Road, Yet Nobody Came To His Rescue">
                            #Shame 21-Year-Old Hacked To Death On A Crowded Street Road, Yet Nobody Came To His Rescue                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/mumbai-factories-might-soon-allow-women-employees-to-work-night-shifts-235085.html" title="Mumbai Factories Might Soon Allow Women Employees To Work Night Shifts" class=" tint">
                            
                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/rtr32gee-5_1437459129_1437459133_236x111.jpg" border="0" alt="Mumbai Factories Might Soon Allow Women Employees To Work Night Shifts"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/mumbai-factories-might-soon-allow-women-employees-to-work-night-shifts-235085.html" title="Mumbai Factories Might Soon Allow Women Employees To Work Night Shifts">
                            Mumbai Factories Might Soon Allow Women Employees To Work Night Shifts                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            19 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/inderjeet-singh-will-represent-india-at-the-2016-olympics-next-year-but-india-has-kept-him-jobless-235083.html" title="Inderjeet Singh Will Represent India At The 2016 Olympics, But India Has Kept Him Jobless" class=" tint">
                            
                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/1-6_1437457868_1437457873_236x111.jpg" border="0" alt="Inderjeet Singh Will Represent India At The 2016 Olympics, But India Has Kept Him Jobless"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/inderjeet-singh-will-represent-india-at-the-2016-olympics-next-year-but-india-has-kept-him-jobless-235083.html" title="Inderjeet Singh Will Represent India At The 2016 Olympics, But India Has Kept Him Jobless">
                            Inderjeet Singh Will Represent India At The Olympics Next Year. India Has Kept Him Jobless                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            19 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/british-comedian-jason-bent-embarasses-fifa-president-sepp-blatter-who-is-jason-bent-235084.html" title="British Comedian, Jason Bent, Embarasses FIFA President Sepp Blatter. Who is Jason Bent?" class=" tint">
                            
                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/leenelson502_1437458002_236x111.jpg" border="0" alt="British Comedian, Jason Bent, Embarasses FIFA President Sepp Blatter. Who is Jason Bent?"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/british-comedian-jason-bent-embarasses-fifa-president-sepp-blatter-who-is-jason-bent-235084.html" title="British Comedian, Jason Bent, Embarasses FIFA President Sepp Blatter. Who is Jason Bent?">
                            British Comedian, Jason Bent, Embarasses FIFA President Sepp Blatter. Who is Jason Bent?                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/meet-the-small-wonder-behind-the-big-success-bajrangi-bhaijaan-munni-aka-harshaali-malhotra-235104.html" class="tint" title="Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/hm-card_1437474034_1437474045_502x234.jpg" border="0" alt="Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/meet-the-small-wonder-behind-the-big-success-bajrangi-bhaijaan-munni-aka-harshaali-malhotra-235104.html" title="Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra">
                        Meet The Small Wonder Behind The Big Success 'Bajrangi Bhaijaan' - Munni AKA Harshaali Malhotra                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/9-tips-to-double-the-effectiveness-of-your-workout-235048.html" class="tint" title="9 Tips To Double The Effectiveness Of Your Workout">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/shutterstock_167065445_1437384114_1437384123_502x234.jpg" border="0" alt="9 Tips To Double The Effectiveness Of Your Workout" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/9-tips-to-double-the-effectiveness-of-your-workout-235048.html" title="9 Tips To Double The Effectiveness Of Your Workout">
                        9 Tips To Double The Effectiveness Of Your Workout                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/director-kabir-khan-loses-his-cool-over-shobhaa-des-review-of-bajrangi-bhaijaan-heres-why-235099.html" class="tint" title="Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage_1437468511_1437468520_502x234.jpg" border="0" alt="Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/director-kabir-khan-loses-his-cool-over-shobhaa-des-review-of-bajrangi-bhaijaan-heres-why-235099.html" title="Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why">
                        Director Kabir Khan Loses His Cool Over Shobhaa De's Review Of 'Bajrangi Bhaijaan'. Here's Why                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/style/12-horrible-hairstyles-inspired-by-bollywood-no-indian-man-should-try-234984.html" class="tint" title="12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/cp_1437219258_502x234.jpg" border="0" alt="12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/style/12-horrible-hairstyles-inspired-by-bollywood-no-indian-man-should-try-234984.html" title="12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try">
                        12 Horrible Hairstyles Inspired By Bollywood No Indian Man Should Try                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/9-everyday-habits-we-didnt-know-are-damaging-our-brains-slowly-235039.html" class="tint" title="9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/header_1435325204_980x457_1437380611_1437380616_502x234.jpg" border="0" alt="9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/9-everyday-habits-we-didnt-know-are-damaging-our-brains-slowly-235039.html" title="9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly">
                        9 Everyday Habits We Didn't Know Are Damaging Our Brains Slowly                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/a-motorist-blamed-big-b-for-frequent-traffic-snarls-but-mr-bachchan-couldnt-care-less-235079.html" class="tint" title="A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/bachchan_660_082912051941_1437456288_1437456295_502x234.jpg" border="0" alt="A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/a-motorist-blamed-big-b-for-frequent-traffic-snarls-but-mr-bachchan-couldnt-care-less-235079.html" title="A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less">
                        A Motorist Blamed Big B For Frequent Traffic Snarls, But Mr. Bachchan Couldn't Care Less                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/40-vintage-life-hacks-that-are-100-years-old-but-just-as-useful-235068.html" class="tint" title="51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/jug502_1437473421_502x234.jpg" border="0" alt="51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/40-vintage-life-hacks-that-are-100-years-old-but-just-as-useful-235068.html" title="51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!">
                        51 Life Hacks That Are So Simple, You'll Kick Yourself For Not Knowing Them!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/16-examples-that-prove-bollywood-needs-bhojpuri-titles-231544.html" class="tint" title="17 Times Bhojpuri Movies Proved Bollywood Knew Nothing About Movie Titles">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/bajrangicard_1437457661_502x234.jpg" border="0" alt="17 Times Bhojpuri Movies Proved Bollywood Knew Nothing About Movie Titles" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/16-examples-that-prove-bollywood-needs-bhojpuri-titles-231544.html" title="17 Times Bhojpuri Movies Proved Bollywood Knew Nothing About Movie Titles">
                        17 Times Bhojpuri Movies Proved Bollywood Knew Nothing About Movie Titles                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/a-scriptwriter-wins-an-8month-long-battle-against-hum-tum-director-kunal-kohli-235081.html" class="tint" title="A Scriptwriter Wins An 8-Month Long Battle Against 'Hum Tum' Director Kunal Kohli.">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/kohli-news_1437457461_1437457465_502x234.jpg" border="0" alt="A Scriptwriter Wins An 8-Month Long Battle Against 'Hum Tum' Director Kunal Kohli." class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/a-scriptwriter-wins-an-8month-long-battle-against-hum-tum-director-kunal-kohli-235081.html" title="A Scriptwriter Wins An 8-Month Long Battle Against 'Hum Tum' Director Kunal Kohli.">
                        A Scriptwriter Wins An 8-Month Long Battle Against 'Hum Tum' Director Kunal Kohli.                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/a-step-by-step-guide-to-relaxing-and-rejuvenating-yourself-with-savasana-235050.html" class="tint" title="A Step By Step Guide To Relaxing And Rejuvenating Yourself With Savasana">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/yogacard_1437386105_502x234.jpg" border="0" alt="A Step By Step Guide To Relaxing And Rejuvenating Yourself With Savasana" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/a-step-by-step-guide-to-relaxing-and-rejuvenating-yourself-with-savasana-235050.html" title="A Step By Step Guide To Relaxing And Rejuvenating Yourself With Savasana">
                        A Step By Step Guide To Relaxing And Rejuvenating Yourself With Savasana                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-daily-struggles-of-people-who-are-easily-distracted-234971.html" class="tint" title="9 Daily Struggles Of People Who Are Easily Distracted">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/cp_1437197237_1437197244_502x234.jpg" border="0" alt="9 Daily Struggles Of People Who Are Easily Distracted" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-daily-struggles-of-people-who-are-easily-distracted-234971.html" title="9 Daily Struggles Of People Who Are Easily Distracted">
                        9 Daily Struggles Of People Who Are Easily Distracted                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-musical-instruments-that-shouldnt-be-musical-instruments-but-are-brilliant-anyway-234915.html" class="tint" title="7 Musical Instruments That Shouldn't Be Musical Instruments But Are Brilliant Anyway">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/condomcard_1437050894_502x234.jpg" border="0" alt="7 Musical Instruments That Shouldn't Be Musical Instruments But Are Brilliant Anyway" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-musical-instruments-that-shouldnt-be-musical-instruments-but-are-brilliant-anyway-234915.html" title="7 Musical Instruments That Shouldn't Be Musical Instruments But Are Brilliant Anyway">
                        7 Musical Instruments That Shouldn't Be Musical Instruments But Are Brilliant Anyway                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/7-easy-and-healthy-salad-recipes-for-every-day-of-the-week-235030.html" class="tint" title="7 Easy And Healthy Salad Recipes For Every Day Of The Week">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/shutterstock_246281896_1437376671_1437376679_502x234.jpg" border="0" alt="7 Easy And Healthy Salad Recipes For Every Day Of The Week" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/7-easy-and-healthy-salad-recipes-for-every-day-of-the-week-235030.html" title="7 Easy And Healthy Salad Recipes For Every Day Of The Week">
                        7 Easy And Healthy Salad Recipes For Every Day Of The Week                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-hidden-treasure-troves-that-if-found-could-make-you-really-really-rich-234961.html" class="tint" title="11 Hidden Treasure Troves That, If Found, Could Make You Really, Really Rich">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card_1437133828_502x234.jpg" border="0" alt="11 Hidden Treasure Troves That, If Found, Could Make You Really, Really Rich" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-hidden-treasure-troves-that-if-found-could-make-you-really-really-rich-234961.html" title="11 Hidden Treasure Troves That, If Found, Could Make You Really, Really Rich">
                        11 Hidden Treasure Troves That, If Found, Could Make You Really, Really Rich                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/21-weird-looking-things-you-wont-believe-were-actually-designed-to-fly-234888.html" class="tint" title="21 Weird Looking Things You Won't Believe Were Actually Designed To Fly">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card_1437039558_502x234.jpg" border="0" alt="21 Weird Looking Things You Won't Believe Were Actually Designed To Fly" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/21-weird-looking-things-you-wont-believe-were-actually-designed-to-fly-234888.html" title="21 Weird Looking Things You Won't Believe Were Actually Designed To Fly">
                        21 Weird Looking Things You Won't Believe Were Actually Designed To Fly                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-60yearold-from-bhubaneshwar-overpowers-his-own-hunger-to-feed-dogs-first-235058.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-60yearold-from-bhubaneshwar-overpowers-his-own-hunger-to-feed-dogs-first-235058.html" class="tint" title="This 60-Year-Old From Bhubaneshwar Overpowers His Own Hunger To Feed Dogs First">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/lochan502_1437392012_1437392112_502x234.jpg" border="0" alt="This 60-Year-Old From Bhubaneshwar Overpowers His Own Hunger To Feed Dogs First" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-60yearold-from-bhubaneshwar-overpowers-his-own-hunger-to-feed-dogs-first-235058.html" title="This 60-Year-Old From Bhubaneshwar Overpowers His Own Hunger To Feed Dogs First">
                        This 60-Year-Old From Bhubaneshwar Overpowers His Own Hunger To Feed Dogs First                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/working-too-hard-and-too-much-might-be-making-you-infertile-235036.html" class="tint" title="Working Too Hard And Too Much Might Be Making You Infertile!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/overwork_1437379925_502x234.jpg" border="0" alt="Working Too Hard And Too Much Might Be Making You Infertile!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/working-too-hard-and-too-much-might-be-making-you-infertile-235036.html" title="Working Too Hard And Too Much Might Be Making You Infertile!">
                        Working Too Hard And Too Much Might Be Making You Infertile!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-bollywood-alisters-remember-their-first-pay-packet-235053.html" class="tint" title="8 Bollywood A-listers Remember Their First Pay Packet">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/600_1437395169_1437395179_502x234.jpg" border="0" alt="8 Bollywood A-listers Remember Their First Pay Packet" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-bollywood-alisters-remember-their-first-pay-packet-235053.html" title="8 Bollywood A-listers Remember Their First Pay Packet">
                        8 Bollywood A-listers Remember Their First Pay Packet                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/father-son-duo-behind-two-biggest-hits-of-the-season-baahubali-and-bajrangi-bhaijaan-235051.html" class="tint" title="Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/2014-10-27_499_home_rajamouli-card_1437385884_1437385887_1437385894_502x234.jpg" border="0" alt="Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/father-son-duo-behind-two-biggest-hits-of-the-season-baahubali-and-bajrangi-bhaijaan-235051.html" title="Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'">
                        Father Son Duo Behind Two Biggest Hits Of The Season 'Baahubali' And 'Bajrangi Bhaijaan'                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/pakistan-censor-board-chops-off-reference-to-kashmir-before-the-screening-of-bajrangi-bhaijaan-in-theaters-235041.html" class="tint" title="Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/11352351_1662155247351861_1951424272_n_1437390119_1437390128_502x234.jpg" border="0" alt="Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/pakistan-censor-board-chops-off-reference-to-kashmir-before-the-screening-of-bajrangi-bhaijaan-in-theaters-235041.html" title="Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters">
                        Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/9-clever-uses-for-pads-and-tampons-â-besides-the-obvious-235032.html" class="tint" title="9 Clever Uses For Pads And Tampons â Besides The Obvious!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/cover_1437376558_502x234.jpg" border="0" alt="9 Clever Uses For Pads And Tampons â Besides The Obvious!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/9-clever-uses-for-pads-and-tampons-â-besides-the-obvious-235032.html" title="9 Clever Uses For Pads And Tampons â Besides The Obvious!">
                        9 Clever Uses For Pads And Tampons â Besides The Obvious!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/9-less-famous-life-partners-of-some-famous-bollywood-stars-235049.html" class="tint" title="9 Less Famous Life Partners Of Some Famous Bollywood Stars">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage-card_1437382664_1437382667_502x234.jpg" border="0" alt="9 Less Famous Life Partners Of Some Famous Bollywood Stars" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/9-less-famous-life-partners-of-some-famous-bollywood-stars-235049.html" title="9 Less Famous Life Partners Of Some Famous Bollywood Stars">
                        9 Less Famous Life Partners Of Some Famous Bollywood Stars                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/rgv-goes-on-another-twitter-rant-blames-lord-ganesha-for-pushkar-stampede-235038.html" class="tint" title="RGV Goes On Another Twitter Rant, Blames Lord Ganesha For Pushkar Stampede!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/main-capture-card-_1437379882_1437379884_502x234.jpg" border="0" alt="RGV Goes On Another Twitter Rant, Blames Lord Ganesha For Pushkar Stampede!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/rgv-goes-on-another-twitter-rant-blames-lord-ganesha-for-pushkar-stampede-235038.html" title="RGV Goes On Another Twitter Rant, Blames Lord Ganesha For Pushkar Stampede!">
                        RGV Goes On Another Twitter Rant, Blames Lord Ganesha For Pushkar Stampede!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/way-we-treat-indians-from-north-east-states-is-appalling-and-it-needs-to-stop-now-235044.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/way-we-treat-indians-from-north-east-states-is-appalling-and-it-needs-to-stop-now-235044.html" class="tint" title="Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/northeast_card_1437383422_502x234.jpg" border="0" alt="Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/way-we-treat-indians-from-north-east-states-is-appalling-and-it-needs-to-stop-now-235044.html" title="Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now">
                        Way We Treat Indians From North East States Is Appalling, And It Needs To Stop Now                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/16-movies-that-blew-our-minds-with-the-power-of-visual-effects-234885.html" class="tint" title="16 Movies That Blew Our Minds With The Power Of Visual Effects">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/collage-card_1437038391_1437038411_502x234.jpg" border="0" alt="16 Movies That Blew Our Minds With The Power Of Visual Effects" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/16-movies-that-blew-our-minds-with-the-power-of-visual-effects-234885.html" title="16 Movies That Blew Our Minds With The Power Of Visual Effects">
                        16 Movies That Blew Our Minds With The Power Of Visual Effects                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-bollywood-actors-who-totally-rocked-the-moustached-look-on-screen-233573.html" class="tint" title="11 Bollywood Actors Who Totally Rocked The Moustached Look On Screen!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jun/screenshot_1_1434103982_1434103992_502x234.jpg" border="0" alt="11 Bollywood Actors Who Totally Rocked The Moustached Look On Screen!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-bollywood-actors-who-totally-rocked-the-moustached-look-on-screen-233573.html" title="11 Bollywood Actors Who Totally Rocked The Moustached Look On Screen!">
                        11 Bollywood Actors Who Totally Rocked The Moustached Look On Screen!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/indian-comedian-finds-a-pakistani-in-the-crowd-makes-him-feel-right-at-home-235034.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/indian-comedian-finds-a-pakistani-in-the-crowd-makes-him-feel-right-at-home-235034.html" class="tint" title="Indian Comedian Finds A Pakistani In The Crowd. Makes Him Feel Right At Home">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/vikramjit_pak_card_1437379254_502x234.jpg" border="0" alt="Indian Comedian Finds A Pakistani In The Crowd. Makes Him Feel Right At Home" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/indian-comedian-finds-a-pakistani-in-the-crowd-makes-him-feel-right-at-home-235034.html" title="Indian Comedian Finds A Pakistani In The Crowd. Makes Him Feel Right At Home">
                        Indian Comedian Finds A Pakistani In The Crowd. Makes Him Feel Right At Home                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/veteran-actor-dilip-kumar-was-the-first-choice-for-lawrence-of-arabia-and-not-omar-sharif-235028.html" class="tint" title="Did You Know Dilip Kumar Was The First Choice For 'Lawrence Of Arabia' And Not Omar Sharif?">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage_1437376525_1437376540_502x234.jpg" border="0" alt="Did You Know Dilip Kumar Was The First Choice For 'Lawrence Of Arabia' And Not Omar Sharif?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/veteran-actor-dilip-kumar-was-the-first-choice-for-lawrence-of-arabia-and-not-omar-sharif-235028.html" title="Did You Know Dilip Kumar Was The First Choice For 'Lawrence Of Arabia' And Not Omar Sharif?">
                        Did You Know Dilip Kumar Was The First Choice For 'Lawrence Of Arabia' And Not Omar Sharif?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-signs-of-prediabetes-that-you-didnt-know-about-234959.html" class="tint" title="5 Signs Of Prediabetes That You Didn't Know About">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/diabetes_1437133302_502x234.jpg" border="0" alt="5 Signs Of Prediabetes That You Didn't Know About" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-signs-of-prediabetes-that-you-didnt-know-about-234959.html" title="5 Signs Of Prediabetes That You Didn't Know About">
                        5 Signs Of Prediabetes That You Didn't Know About                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/2-foreigners-taking-on-govindas-brand-of-comedy-is-the-funniest-dubsmash-youll-ever-see-235029.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/2-foreigners-taking-on-govindas-brand-of-comedy-is-the-funniest-dubsmash-youll-ever-see-235029.html" class="tint" title="2 Foreigners Taking On Govinda's Brand Of Comedy Is The Funniest Dubsmash You'll Ever See">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/bwld_dubsmash_card_1437376625_502x234.jpg" border="0" alt="2 Foreigners Taking On Govinda's Brand Of Comedy Is The Funniest Dubsmash You'll Ever See" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/2-foreigners-taking-on-govindas-brand-of-comedy-is-the-funniest-dubsmash-youll-ever-see-235029.html" title="2 Foreigners Taking On Govinda's Brand Of Comedy Is The Funniest Dubsmash You'll Ever See">
                        2 Foreigners Taking On Govinda's Brand Of Comedy Is The Funniest Dubsmash You'll Ever See                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/bhai-does-it-again-salmans-bajrangi-bhaijaan-is-crushing-all-boxoffice-records-235019.html" class="tint" title="Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/bajrangi-bhaijaan-poster-story-collection_1437371540_1437371548_502x234.jpg" border="0" alt="Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/bhai-does-it-again-salmans-bajrangi-bhaijaan-is-crushing-all-boxoffice-records-235019.html" title="Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!">
                        Bhai Does It Again. Salman's 'Bajrangi Bhaijaan' Is Crushing All Box-Office Records!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/14-water-slides-you-need-crazy-levels-of-adrenaline-to-ride-234909.html" class="tint" title="14 Water Slides You Need Crazy Levels Of Adrenaline To Ride">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/waterride_1437048850_1437048866_502x234.jpg" border="0" alt="14 Water Slides You Need Crazy Levels Of Adrenaline To Ride" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/14-water-slides-you-need-crazy-levels-of-adrenaline-to-ride-234909.html" title="14 Water Slides You Need Crazy Levels Of Adrenaline To Ride">
                        14 Water Slides You Need Crazy Levels Of Adrenaline To Ride                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/heres-the-comic-book-that-inspired-the-rs-300-crore-opus-bahubali-235017.html" class="tint" title="Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/bahu502_1437374617_1437374622_502x234.jpg" border="0" alt="Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/heres-the-comic-book-that-inspired-the-rs-300-crore-opus-bahubali-235017.html" title="Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!">
                        Here's The Comic Book That Inspired The Rs 300 Crore Opus Baahubali!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/9-dialogues-from-naseeruddin-shah-films-that-prove-he-is-the-king-of-versatility-234980.html" class="tint" title="9 Dialogues From Naseeruddin Shah Films That Prove He Is The King Of Versatility">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card_1437212893_1437212901_502x234.jpg" border="0" alt="9 Dialogues From Naseeruddin Shah Films That Prove He Is The King Of Versatility" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/9-dialogues-from-naseeruddin-shah-films-that-prove-he-is-the-king-of-versatility-234980.html" title="9 Dialogues From Naseeruddin Shah Films That Prove He Is The King Of Versatility">
                        9 Dialogues From Naseeruddin Shah Films That Prove He Is The King Of Versatility                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-stages-romantics-go-through-when-they-develop-a-crush-on-someone-234865.html" class="tint" title="11 Stages Romantics Go Through When They Develop A Crush On Someone">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/cp_1437026071_502x234.jpg" border="0" alt="11 Stages Romantics Go Through When They Develop A Crush On Someone" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-stages-romantics-go-through-when-they-develop-a-crush-on-someone-234865.html" title="11 Stages Romantics Go Through When They Develop A Crush On Someone">
                        11 Stages Romantics Go Through When They Develop A Crush On Someone                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/meet-kareena-kapoor-the-walk-out-queen-of-bollywood-234931.html" class="tint" title="Meet Kareena Kapoor, The Walk Out Queen Of Bollywood">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage-card_1437116699_502x234.jpg" border="0" alt="Meet Kareena Kapoor, The Walk Out Queen Of Bollywood" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/meet-kareena-kapoor-the-walk-out-queen-of-bollywood-234931.html" title="Meet Kareena Kapoor, The Walk Out Queen Of Bollywood">
                        Meet Kareena Kapoor, The Walk Out Queen Of Bollywood                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/17-struggles-of-forgetful-people-that-might-just-make-you-feel-sympathetic-towards-them-234921.html" class="tint" title="17 Struggles Of Forgetful People That Might Just Make You Feel Sympathetic Towards Them">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/cp_1437130886_502x234.jpg" border="0" alt="17 Struggles Of Forgetful People That Might Just Make You Feel Sympathetic Towards Them" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/17-struggles-of-forgetful-people-that-might-just-make-you-feel-sympathetic-towards-them-234921.html" title="17 Struggles Of Forgetful People That Might Just Make You Feel Sympathetic Towards Them">
                        17 Struggles Of Forgetful People That Might Just Make You Feel Sympathetic Towards Them                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/7-music-directors-who-came-conquered-and-then-sort-of-disappeared-234857.html" class="tint" title="7 Music Directors Who Came, Conquered, And Then Sort Of Disappeared">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/hrcard1_1436965815_1436965818_502x234.jpg" border="0" alt="7 Music Directors Who Came, Conquered, And Then Sort Of Disappeared" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/7-music-directors-who-came-conquered-and-then-sort-of-disappeared-234857.html" title="7 Music Directors Who Came, Conquered, And Then Sort Of Disappeared">
                        7 Music Directors Who Came, Conquered, And Then Sort Of Disappeared                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/9-eccentric-things-that-happen-in-kolkata-during-the-monsoons-234754.html" class="tint" title="9 Eccentric Things That Happen In Kolkata During The Monsoons">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/cp_1436849927_502x234.jpg" border="0" alt="9 Eccentric Things That Happen In Kolkata During The Monsoons" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/9-eccentric-things-that-happen-in-kolkata-during-the-monsoons-234754.html" title="9 Eccentric Things That Happen In Kolkata During The Monsoons">
                        9 Eccentric Things That Happen In Kolkata During The Monsoons                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/a-couple-hitchhiked-all-the-way-from-bulgaria-to-india-in-a-span-of-two-years-235012.html" class="tint" title="A Couple Hitchhiked All The Way From Bulgaria To India In A Span Of Two Years!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/boris_1437306983_1437306993_502x234.jpg" border="0" alt="A Couple Hitchhiked All The Way From Bulgaria To India In A Span Of Two Years!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/a-couple-hitchhiked-all-the-way-from-bulgaria-to-india-in-a-span-of-two-years-235012.html" title="A Couple Hitchhiked All The Way From Bulgaria To India In A Span Of Two Years!">
                        A Couple Hitchhiked All The Way From Bulgaria To India In A Span Of Two Years!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/whoa-baahubali-crosses-200-crores-at-the-box-office-in-just-five-days-234863.html" class="tint" title="Whoa! Baahubali Crosses 300 Crores At The Box Office In A Week!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/baahubali_640x480_81436502453_1437025527_1437025537_502x234.jpg" border="0" alt="Whoa! Baahubali Crosses 300 Crores At The Box Office In A Week!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/whoa-baahubali-crosses-200-crores-at-the-box-office-in-just-five-days-234863.html" title="Whoa! Baahubali Crosses 300 Crores At The Box Office In A Week!">
                        Whoa! Baahubali Crosses 300 Crores At The Box Office In A Week!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/11-healthy-and-delicious-paneer-recipes-you-have-to-try-at-least-once-234945.html" class="tint" title="11 Healthy And Delicious Paneer Recipes You Have To Try At Least Once">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/paneer_1437127136_502x234.jpg" border="0" alt="11 Healthy And Delicious Paneer Recipes You Have To Try At Least Once" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/11-healthy-and-delicious-paneer-recipes-you-have-to-try-at-least-once-234945.html" title="11 Healthy And Delicious Paneer Recipes You Have To Try At Least Once">
                        11 Healthy And Delicious Paneer Recipes You Have To Try At Least Once                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/nawazuddin-siddiqui-in-bajrangi-vs-the-original-chand-nawaz-who-did-a-better-piece-to-camera-235010.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/nawazuddin-siddiqui-in-bajrangi-vs-the-original-chand-nawaz-who-did-a-better-piece-to-camera-235010.html" class="tint" title="Nawazuddin Siddiqui In Bajrangi Vs The Original Chand Nawaz. Who Did A Better Piece To Camera?">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/bb_reporting_card_1437305618_502x234.jpg" border="0" alt="Nawazuddin Siddiqui In Bajrangi Vs The Original Chand Nawaz. Who Did A Better Piece To Camera?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/nawazuddin-siddiqui-in-bajrangi-vs-the-original-chand-nawaz-who-did-a-better-piece-to-camera-235010.html" title="Nawazuddin Siddiqui In Bajrangi Vs The Original Chand Nawaz. Who Did A Better Piece To Camera?">
                        Nawazuddin Siddiqui In Bajrangi Vs The Original Chand Nawaz. Who Did A Better Piece To Camera?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-difference-in-the-way-a-boy-talks-to-a-girl-vs-his-friend-will-make-you-rofl-235006.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-difference-in-the-way-a-boy-talks-to-a-girl-vs-his-friend-will-make-you-rofl-235006.html" class="tint" title="The Difference In The Way A Boy Talks To A Girl Vs His Friend Will Make You ROFL!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/boys_card_1437299728_502x234.jpg" border="0" alt="The Difference In The Way A Boy Talks To A Girl Vs His Friend Will Make You ROFL!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-difference-in-the-way-a-boy-talks-to-a-girl-vs-his-friend-will-make-you-rofl-235006.html" title="The Difference In The Way A Boy Talks To A Girl Vs His Friend Will Make You ROFL!">
                        The Difference In The Way A Boy Talks To A Girl Vs His Friend Will Make You ROFL!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/i-actually-forgot-the-way-to-the-office-excuses-that-all-of-us-have-tried-when-we-are-late-at-least-once-cmon-admit-it-235003.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/i-actually-forgot-the-way-to-the-office-excuses-that-all-of-us-have-tried-when-we-are-late-at-least-once-cmon-admit-it-235003.html" class="tint" title="'I Actually Forgot The Way To The Office!' Excuses That All Of Us Have Tried When We Are Late At Least Once! C'mon Admit">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/exuses_card_1437293625_502x234.jpg" border="0" alt="'I Actually Forgot The Way To The Office!' Excuses That All Of Us Have Tried When We Are Late At Least Once! C'mon Admit" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/i-actually-forgot-the-way-to-the-office-excuses-that-all-of-us-have-tried-when-we-are-late-at-least-once-cmon-admit-it-235003.html" title="'I Actually Forgot The Way To The Office!' Excuses That All Of Us Have Tried When We Are Late At Least Once! C'mon Admit">
                        'I Actually Forgot The Way To The Office!' Excuses That All Of Us Have Tried When We Are Late At Least Once! C'mon Admit                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/13-smart-ways-to-spot-adulterated-food-235008.html" class="tint" title="13 Smart Ways To Spot Adulterated Food">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/apple_1437299998_1437300009_502x234.jpg" border="0" alt="13 Smart Ways To Spot Adulterated Food" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/13-smart-ways-to-spot-adulterated-food-235008.html" title="13 Smart Ways To Spot Adulterated Food">
                        13 Smart Ways To Spot Adulterated Food                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/whoa-you-never-knew-creatures-like-these-still-existed-on-earth-235009.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/whoa-you-never-knew-creatures-like-these-still-existed-on-earth-235009.html" class="tint" title="Whoa! You Never Knew Creatures Like These Still Existed on Earth!">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/uc_card_1437302080_502x234.jpg" border="0" alt="Whoa! You Never Knew Creatures Like These Still Existed on Earth!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/whoa-you-never-knew-creatures-like-these-still-existed-on-earth-235009.html" title="Whoa! You Never Knew Creatures Like These Still Existed on Earth!">
                        Whoa! You Never Knew Creatures Like These Still Existed on Earth!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/study-finds-that-dinosaurs-are-still-alive-this-video-has-the-evidence-234883.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/study-finds-that-dinosaurs-are-still-alive-this-video-has-the-evidence-234883.html" class="tint" title="Study Finds That Dinosaurs Are Still Alive! This Video Has The Evidence">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Jul/dinosaure_alive_card_1437036508_502x234.jpg" border="0" alt="Study Finds That Dinosaurs Are Still Alive! This Video Has The Evidence" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/study-finds-that-dinosaurs-are-still-alive-this-video-has-the-evidence-234883.html" title="Study Finds That Dinosaurs Are Still Alive! This Video Has The Evidence">
                        Study Finds That Dinosaurs Are Still Alive! This Video Has The Evidence                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-images-from-an-indian-circus-that-will-take-you-back-to-when-you-were-a-kid-234955.html" class="tint" title="11 Images From An Indian Circus That Will Take You Back To When You Were A Kid">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/rtr2tv7v_1437132563_502x234.jpg" border="0" alt="11 Images From An Indian Circus That Will Take You Back To When You Were A Kid" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-images-from-an-indian-circus-that-will-take-you-back-to-when-you-were-a-kid-234955.html" title="11 Images From An Indian Circus That Will Take You Back To When You Were A Kid">
                        11 Images From An Indian Circus That Will Take You Back To When You Were A Kid                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/everything-you-need-to-know-about-the-ohsoimportant-calorie-234963.html" class="tint" title="Everything You Need To Know About The Oh-So-Important Calorie">
                        <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/caloriescard_1437134892_502x234.jpg" border="0" alt="Everything You Need To Know About The Oh-So-Important Calorie" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/everything-you-need-to-know-about-the-ohsoimportant-calorie-234963.html" title="Everything You Need To Know About The Oh-So-Important Calorie">
                        Everything You Need To Know About The Oh-So-Important Calorie                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/8-bollywood-alisters-remember-their-first-pay-packet-235053.html" class="tint" title="8 Bollywood A-listers Remember Their First Pay Packet">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/600_1437395169_1437395179_218x102.jpg" border="0" alt="8 Bollywood A-listers Remember Their First Pay Packet"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/8-bollywood-alisters-remember-their-first-pay-packet-235053.html" title="8 Bollywood A-listers Remember Their First Pay Packet">
                            8 Bollywood A-listers Remember Their First Pay Packet                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/technology/21-weird-looking-things-you-wont-believe-were-actually-designed-to-fly-234888.html" class="tint" title="21 Weird Looking Things You Won't Believe Were Actually Designed To Fly">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card_1437039558_218x102.jpg" border="0" alt="21 Weird Looking Things You Won't Believe Were Actually Designed To Fly"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/21-weird-looking-things-you-wont-believe-were-actually-designed-to-fly-234888.html" title="21 Weird Looking Things You Won't Believe Were Actually Designed To Fly">
                            21 Weird Looking Things You Won't Believe Were Actually Designed To Fly                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/travel/10-powerful-quotes-that-will-change-the-way-you-live-and-think-234925.html" class="tint" title="10 Powerful Quotes That Will Change The Way You Live And Think">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card_1437471700_1437471709_218x102.jpg" border="0" alt="10 Powerful Quotes That Will Change The Way You Live And Think"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/10-powerful-quotes-that-will-change-the-way-you-live-and-think-234925.html" title="10 Powerful Quotes That Will Change The Way You Live And Think">
                            10 Powerful Quotes That Will Change The Way You Live And Think                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/food/7-easy-and-healthy-salad-recipes-for-every-day-of-the-week-235030.html" class="tint" title="7 Easy And Healthy Salad Recipes For Every Day Of The Week">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/shutterstock_246281896_1437376671_1437376679_218x102.jpg" border="0" alt="7 Easy And Healthy Salad Recipes For Every Day Of The Week"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/food/7-easy-and-healthy-salad-recipes-for-every-day-of-the-week-235030.html" title="7 Easy And Healthy Salad Recipes For Every Day Of The Week">
                            7 Easy And Healthy Salad Recipes For Every Day Of The Week                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/pakistan-censor-board-chops-off-reference-to-kashmir-before-the-screening-of-bajrangi-bhaijaan-in-theaters-235041.html" class="tint" title="Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters">

                            <img class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/11352351_1662155247351861_1951424272_n_1437390119_1437390128_218x102.jpg" border="0" alt="Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/pakistan-censor-board-chops-off-reference-to-kashmir-before-the-screening-of-bajrangi-bhaijaan-in-theaters-235041.html" title="Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters">
                            Pakistan Censor Board Chops Off Reference To Kashmir Before The Screening Of Bajrangi Bhaijaan In Theaters                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   $('#hp_block_1').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
       //showBigAD1('bigAd1_slot');
     // showLhsNativeAd('column1_11',3);
      //showLhsNativeAd('column1_16',4);
      //showRhsNativeAd('column3_6',2);       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#hp_block_2').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
      //showLhsNativeAd('column1_21',5);
      //showLhsNativeAd('column1_26',6);
      //showRhsNativeAd('column3_11',3);      
       showBigAD2('bigAd2_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#hp_block_3').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
      //showLhsNativeAd('column1_31',7);
      //showLhsNativeAd('column1_36',8);
      //showRhsNativeAd('column3_16',4);  
      showBigAD3('bigAd3_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#container4').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible

       BADros('badRos_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});    
    var settings = {
        limit     : 5, // The number of record we want to get
        offset  : 18 // this is start  // select * from tbl limit 0,2
    };
    var is_loading = 0;   
    $( document ).ready(function() {
		url = window.location.href;
		/* spotlight onload tracking homepage */
		//console.log("homepage");
					ga('send', 'event', 'OnLoad Partner Stories', '234925', 'homepage', {'nonInteraction': 1});
					ga('send', 'event', 'OnLoad Partner Stories', '233651', 'homepage', {'nonInteraction': 1});
		   
        $(window).scroll(function() {
            var scroll_pos = $(window).scrollTop()+600;
            //console.log(scroll_pos+"-------------------------------"+0.9 * ($(document).height() - $(window).height()))
            if (scroll_pos >= 0.9 * ($(document).height() - $(window).height()-400)) {
                if (is_loading == 0) {
                    //loadMore();
                }
            }
        });        
    });//---- end document ready ----------
    
    function loadMore(){
        $('#loader').show();  
        $('.last-container').hide();
              
        is_loading = 1;
        //var url = "/apis/newsFeed.php";
        //var url = "/apis/getMoreData.php";
        var url = "/ajax/get_hp_contents.php";
        var offset = settings.offset;
        //alert('limit '+settings.limit+' offset '+settings.offset);
        var dataQuery = {
            limit: settings.limit,
            offset: offset,
            pageType: 'home'
            //hndContentID: hndContentID
        };
        var jqxhr = $.getJSON(url, dataQuery)
        .done(function(data) {
            $('#loader').hide();  
            $('.last-container').show();
            //var jObj = eval("(" + data + ")");
            //alert(JSON.stringify(data));
            var msg = data['MoreData'].msg;
            //alert(msg);
            if(msg=='success'){
                delete data['MoreData'].msg;// remove extra node
                //delete data['HpMiddleBlock'][hndContentID]; // remove first time loaded article form response
                //console.log('-------jmd------------------------------------');
                //alert((JSON.stringify(data)));
                var source = $("#HpMiddleBlock_tpl").html();
                var template = Handlebars.compile(source);
                //   alert(JSON.stringify(res_action));
                var moreNewsFeedHtml = template(data);
                //alert(moreFromIndiaHtml);
                $("#b4c2").append(moreNewsFeedHtml);
                is_loading = 0;
                settings.offset = offset+settings.limit;
                $('.last-container').show();
                $("img.lazy").lazyload({
                    effect: "fadeIn",
                    placeholder:"",
                    failurelimit: 1000
                })                 
                
            }
        })
        .fail(function() {
            console.log("error in Json");
        })
        .always(function() {
            //is_loading = 0;
            console.log("complete");
            //$('.last-container').show();
        });


    }
</script>





    <div class="clr"></div>
            <script> 
               // showFooterCode();
				                    showFooterCode();
					            </script>
<div class="last-container">
    <!--<br>-->    <!--section class="big-ads" <?if (isset ($homepage)&&!empty ($homepage)){echo "id='remove-fixed'";}else{echo "id='aaaaaaa'";}?>>
        <a href="javascript:void(0)"><img src="/images/big-ads.jpg"></a>  
    </section-->    
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,305,359<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>9756  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>47,620 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get the Weekly Dose sent to your inbox every week! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health Me Up</a> 
                        
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://healthmeup.com"   target="_blank" >Health me up</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://timescity.com"  target="_blank">TimesCity</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                        


                            <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                        


                            <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                        


                            <a href='http://www.indiatimes.com/mobile/' >On Mobile</a> 
                        


                            <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                        


                            <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                        


                            <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                        


                            <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://www.indiatimes.com/mobile/">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.1" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.1"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>
<script defer src="http://media.indiatimes.in/resources/js/jquery.flexslider-min.js?v=1421222060"></script>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.1"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.1"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.1"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>