<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>    Imgur</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="robots" content="follow,index" />
    <meta name="keywords" content="images, funny, image host, image sharing, reaction gif, viral images, current events, cute, visual storytelling, gif" />
    <meta name="description" content="The Internet's visual storytelling community. Explore, share, and discuss the best visual stories the Internet has to offer." />
    <meta name="copyright" content="Copyright 2015 Imgur, Inc." />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;" />
    
    <link rel="alternate" type="application/rss+xml" title="Imgur Gallery" href="http://feeds.feedburner.com/ImgurGallery?format=xml" />
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-16x16.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="//s.imgur.com/images/favicon-152.png">
    <meta name="msapplication-TileColor" content="#2cd63c">
    <meta name="msapplication-TileImage" content="//s.imgur.com/images/favicon-144.png">
            <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/global.css?1437516888" />

        
        
    
        <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/gallery.css?1437516888" />

            <!--[if IE 9]><link rel="stylesheet" href="//s.imgur.com/include/css/ie-sucks.css?0" type="text/css" /><![endif]-->
    
            <link rel="canonical" href="http://imgur.com/" />
        <meta property="og:url" content="http://imgur.com/" />
    
    <meta name="p:domain_verify" content="a1e7abe8af908cc6dfaf935dd9a20384"/>
    <meta property="og:site_name" content="Imgur" />
    <meta property="fb:admins" content="12331492,12301369" />
    <meta name="twitter:site" content="@imgur" />
    <meta name="twitter:domain" content="imgur.com" />
    <meta name="twitter:creator" content="@imgur" />
    <meta name="twitter:app:id:googleplay" content="com.imgur.mobile" />
        <meta name="twitter:card" content="summary_large_image" />
    <meta property="og:type" content="website" />
            <meta property="og:title" content="Imgur" />
        <meta name="twitter:title" content="Imgur" />
    
            <meta property="og:description" content="Imgur: full of all the magic and wonders of the Internet." />
        <meta name="twitter:description" content="Imgur: full of all the magic and wonders of the Internet." />
    
    <meta property="og:image" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image:width" content="1200" />
    <meta name="twitter:image:src" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta name="twitter:image:height" content="630" />
    <meta name="twitter:image:width" content="1200" />
    <!--[if lte IE 8]><script type="text/javascript" src="//s.imgur.com/min/iepoly.js?1437516888"></script>
<![endif]-->
    
        
</head>
<body>
            
            <noscript>
                <iframe src="//www.googletagmanager.com/ns.html?id=GTM-W9LTJC" height="0" width="0" style="display:none;visibility:hidden"></iframe>
            </noscript>
            <script type="text/javascript">
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-W9LTJC');
            </script>
        
    

                
    <div id="topbar">
    <div class="header-center">
        <div id="header-logo">

            <a href="//imgur.com" class="logo " data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@logo@@}}">
                
                <span class="logo-icon"></span>
            </a>
        </div>
        <div id="main-nav">
            <ul>
                <li class="menu-btn">
                    <div class="menu-icon">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                        
                    </div>
                    <div class="link-menu">
                        <div class="test-1868-show">
                            <ul class="topics cf">
                                <li class="item bold" data-value="hot"><a href="/hot" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@hot@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a></li>
                                <li class="item bold" data-value="new"><a href="/new" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@new@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a></li>
                                                                                                            <li class="item" data-value="Funny">
                                            <a href="/topic/Funny" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Awesome">
                                            <a href="/topic/Awesome" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Aww">
                                            <a href="/topic/Aww" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="The More You Know">
                                            <a href="/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Storytime">
                                            <a href="/topic/Storytime" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Current Events">
                                            <a href="/topic/Current_Events" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Design &amp; Art">
                                            <a href="/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Reaction">
                                            <a href="/topic/Reaction" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                                        </li>
                                    
                                                                    
                                
                            </ul>
                            <ul class="dropdown-footer cf">
                                <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">blog</a></li>
                                <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">about</a></li>
                                <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">store</a></li>
                                <li><a href="//imgur.com/jobs"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">jobs</a></li>
                                <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">help</a></li>
                                <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">apps</a></li>
                                <li><a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">terms</a></li>
                                <li><a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">privacy</a></li>
                                <li><a href="//imgur.com/removalrequest" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@deletion@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">request deletion</a></li>
                                <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">api</a></li>
                                <li><a href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">ad choices</a></li>
                            </ul>
                        </div>
                        <ul class="test-1868-hide">
                            <li><a href="//imgur.com/jobs" class="new" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@}}">we're hiring!</a></li>
                                                            <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@new@@:false}}">blog</a></li>
                            
                            <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@}}">about imgur</a></li>
                            <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@}}">imgur store</a></li>
                            <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@}}">official apps</a></li>
                            <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@}}">developer api</a></li>
                            <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@}}">need help?</a></li>
                        </ul>
                        <div class="clear test-1868-hide"></div>
                        <div class="dropdown-footer hamburger-menu test-1868-hide">
                            <div class="terms-footer">
                                <a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@}}">terms</a>
                                <a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@}}">privacy</a>
                                <a class="small-margin-top" href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@}}">ad choices</a>
                            </div>
                            <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fimgur&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="display:inline-block; border:none; overflow:hidden; height:20px; width:50px; margin-right:7px;" allowTransparency="true"></iframe>
                            <a href="https://twitter.com/imgur" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false"></a>
                        </div>
                    </div>
                </li>
                <li class="upload-button-container">
                    <a class="upload-button disabled upload-blue" href="javascript:;">
                        <div class="icon icon-upload"></div>
                        <span class="upload-btn-text">upload images</span>
                        <span class="upload-queue-length">23</span>
                        <div id="upload-global-top-bar-mini-progress"></div>
                    </a>
                    <div class="creation-dropdown upload-blue">
                        <div class="selection icon icon-arrow-down"></div>
                        <div class="options">
                            <ul>
                                <li class="item">
                                    <a class="creation-dropdown-upload" href="javascript:;">Upload Images</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/vidgif">Video to GIF</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/memegen" data-jafo="{@@event@@:@@makeAMeme@@,@@meta@@:{@@control@@:@@header@@}}">Make a Meme</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="random-button title-n " title="random mode">
                    <a href="//imgur.com/random" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@random@@}}">
                        <div class="random-icon"></div>
                    </a>
                </li>
            </ul>
        </div>
        <div id="secondary-nav">
            <ul class="user-nav">
                <li id="global-search-container">
                    <form class="search-form" method="get" action="//imgur.com/search">
                        <input id="search-global-input" type="text" name="q" class="search" blur placeholder="Search images"  />
                    </form>

                    <div class="search-icon-container">
                        <div class="search-icon-container-icon"></div>
                    </div>
                </li>
                                    <li class="signin-link"><a class="" href="https://imgur.com/signin" data-jafo="{@@event@@:@@signinButtonClick@@}">sign in</a></li>
                    <li id="register"><a class="" href="https://imgur.com/register" data-jafo="{@@event@@:@@registerButtonClick@@}">sign up</a></li>
                
            </ul>
        </div> <!-- #secondary-nav -->
    </div> <!-- .header-center -->
</div> <!-- #topbar -->
<div id="cta-container-placeholder"></div>

    

    

            
        
        
    

            <a href="javascript:;" id="upload-global-album-tipsy" class="title-nw nodisplay" title="You can't create an album with just one image"></a>

<div id="upload-global" class="popup">
    <form method="post" id="upload-global-form" enctype="multipart/form-data" action="http://imgur.com/upload">
        

        <div id="upload-global-buttons">
            <div id="upload-global-upload-loader" class="textbox">
                Loading...
            </div>

            <div id="upload-global-file-wrapper">
                <span class="computerIcon"></span>
                <span class="green">browse</span> your computer

                <input accept="image/*" type="file" name="Filedata" multiple="multiple" id="upload-global-file">
            </div>

            <div class="left">
                <div id="upload-global-url">
                    <span class="webIcon"></span>
                    <input type="text" id="upload-global-link-input" class="placeholder-onkeydown" title="enter image URLs" placeholder="enter image URLs" />
                </div>
            </div>

            <div class="clear"></div>

            <div id="upload-global-dragdrop">
                <span class="dragdropIcon"></span>
                <span class="green">drag and drop</span> here
            </div>

            <div class="left">
                <div id="upload-global-clipboard">
                    <div style="text-align: center"><span class="clipboardIcons">Ctrl + V</span></div>
                    <span class="green">paste</span> from your clipboard
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="textbox center" id="upload-global-instructions"></div>

        <!-- gallery submit vars -->
        <input type="hidden" name="current_upload" value="0"/>
        <input type="hidden" name="total_uploads" value="0"/>
        <input type="hidden" name="terms" value="0"/>
        <input type="hidden" name="gallery_type" value=""/>


        <div id="upload-global-errors" class="textbox error" style="display: none;"></div>

        <div id="upload-global-queue-description" class="textbox nodisplay"></div>

        <div id="upload-global-start-upload">
            <div class="panel">
                <div>

                    <table id="upload-global-actions">
                        <tbody>
                            <tr>

                                <td width="50.4%">

                                    <div id="upload-global-album-checkbox-div">
                                        <input type="hidden" name="gallery_submit" value="0"/>

                                        <button id="upload-global-gallery-checkbox" type="button">
                                            <span class="gallery-icon"></span>publish to Imgur
                                        </button>

                                        <div id="upload-global-gallery-checkbox-overlay"></div>

                                        <input type="hidden" name="create_album" value="0"/>

                                        <button id="upload-global-album-checkbox" type="button">
                                            <span class="createAlbum-icon"></span>create album
                                        </button>

                                    </div>

                                </td>
                                <td width="49.6%">
                                    <button type="button" id="upload-global-start-button" class="button-big green" type="submit">
                                        <span class="start-btn-icon"></span>
                                        Start Upload
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div id="upload-global-album-options">
                    







<table id="upload-global-album-layout">
    <tr>
        <td id="upload-global-album-td">
            <input name="album_title" type="text" title="Optional Album Title" class="placeholder-onkeydown" id="upload-global-album-title" maxlength="55" />
        </td>

        <td class="center title" title="<table><tr><td style='vertical-align: middle'>&lt;img src=&#039;//s.imgur.com/images/album-layout-blog.png&#039; style=&#039;padding-right: 2px;&#039;/&gt;</td><td>&lt;h1&gt;Blog Layout&lt;/h1&gt;Shows all the images on one page in a blog post style layout. Best for albums with a small to medium amount of images.</td></tr></table>">
            <input id="blog-layout-upload" type="radio" name="layout" value="b" checked="checked"/>
            <label for="blog-layout-upload">Blog</label>
        </td>

        <td class="center title" title="<table><tr><td style='vertical-align: middle'>&lt;img src=&#039;//s.imgur.com/images/album-layout-horizontal.png&#039;  style=&#039;padding-right: 6px;&#039;/&gt;</td><td>&lt;h1&gt;Horizontal Layout&lt;/h1&gt;Shows one image at a time in a traditional style, with thumbnails on top. Best for albums that have high resolution photos.</td></tr></table>">
            <input id="horizontal-layout-upload" type="radio" name="layout" value="h"/>
            <label for="horizontal-layout-upload">Horizontal</label>
        </td>

        <td class="center title" title="<table><tr><td style='vertical-align: middle'>&lt;img src=&#039;//s.imgur.com/images/album-layout-grid.png&#039;  style=&#039;padding-right: 6px;&#039;/&gt;</td><td>&lt;h1&gt;Grid Layout&lt;/h1&gt;Shows all the images on one page as thumbnails that expand when clicked on. Best for albums with lots of images.</td></tr></table>">
            <input id="grid-layout-upload" type="radio" name="layout" value="g"/>
            <label for="grid-layout-upload">Grid</label>
        </td>

        <td width="15"></td>
    </tr>
</table>

                </div>

                <div class="textbox list">
                    <table id="upload-global-file-list-header">
                        <tr class="top-tr">
                            <td width="45" class="edit-td"><h2>Edit</h2></td>
                            <td><h2>Images <span class="upload-queue-length">(0)</span></h2></td>
                            <td width="40"></td>
                        </tr>
                    </table>

                    <table id="upload-global-file-list">
                    </table>
                                            <div class="agreement-notice">By uploading, you agree to our <a href="//imgur.com/tos" target="_blank">terms of service</a></div>
                    
                </div>
            </div>
        </div>

        <div id="upload-global-progress-wrapper">
            <div id="upload-global-progressbar">
                <div class="progressBarBG">
                    <div class="progressBar"></div>
                </div>
            </div>
            <div id="upload-global-upload-queue"></div>

            <div id="upload-global-converting">
                <p>
                    Optimizing your large GIFs...
                </p>
            </div>
        </div>
    </form>
</div>

<div id="upload-global-FF-paste-box"></div>

<div id="hiddenDropZone" class="nodisplay"></div>

<div class="nodisplay">
    <div id="upload-global-image-formats">
         That file type is not supported!

        <div class="textbox supported-formats">
            Supported formats: JPEG, GIF, PNG, APNG, TIFF, BMP, PDF, XCF
        </div>
    </div>
</div>

<div id="upload-global-flash-container" title="upload images from your computer">
    <div id="upload-global-flash-browse"></div>
</div>

    

        <div class="nodisplay">
	<div id="colorbox-confirm" class="popup">
		<h3 id="colorbox-confirm-title"></h3>
		
		<div class="textbox" id="colorbox-confirm-message"></div>

		<div class="margin-top">
			<div class="left">
				<a href="javascript:;" class="colorbox-no-confirm" id="colorbox-confirm-no">No way!</a>
			</div>
			
			<div class="right">
				<div class="small-loader"></div>
				<a href="javascript:;" class="colorbox-confirm" id="colorbox-confirm-yes">I'm sure</a>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</div>


            
    

    
            
    

    
    
    
    

    <div id="fullbleed-bg"></div>
    <div id="content" class="outside main">
        
                    <div class="sentence-sorting">
            <span class="before-text">
    The

</span>
<div id="section" class="combobox front-page-section sorting-text-align" name="section">
    <div class="selection">
                <div data-type="base" data-value="hot">
            <span class="name">Most Viral</span>
        </div>
        
        
    </div>

    <div class="options nano">
        <div class="js-galleries content">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                                            Most Viral
                    
                </div>
            </div>
            <ul>
                <li class="bold item nodisplay" data-type="base" data-value="hot">
                    <a href="/hot/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a>
                </li>
                <li class="bold item" data-type="base" data-value="new">
                                            <a href="/new/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@new@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a>
                    
                </li>
                <li class="combobox-hairline"></li>
                                                            <li class="item" data-value="Funny">
                            <a href="/topic/Funny" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                        </li>
                    
                                                            <li class="item" data-value="Awesome">
                            <a href="/topic/Awesome" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                        </li>
                    
                                                            <li class="item" data-value="Aww">
                            <a href="/topic/Aww" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                        </li>
                    
                                                            <li class="item" data-value="The More You Know">
                            <a href="/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                        </li>
                    
                                                            <li class="item" data-value="Storytime">
                            <a href="/topic/Storytime" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                        </li>
                    
                                                            <li class="item" data-value="Current Events">
                            <a href="/topic/Current_Events" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                        </li>
                    
                                                            <li class="item" data-value="Design &amp; Art">
                            <a href="/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                        </li>
                    
                                                            <li class="item" data-value="Reaction">
                            <a href="/topic/Reaction" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                        </li>
                    
                                    
                
            </ul>
        </div>
        <div id="custom-group" class="item new-group"><a href="/gallery/custom" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@custom@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Custom Gallery</a></div>
    </div>
    <input type="hidden" name="section" value="hot">
</div><span class="middle-text"> <!-- intentionally not closed -->

    images on the Internet, 


    
    sorted by</span> <!-- .middle-text -->
    
    
        
            
    

            
    
    <div id="sort" class="combobox">
        <div class="selection">
            popularity
        </div>
        <div class="options sorting-text-align">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                    popularity
                </div>
            </div>
            <ul>
                                    <li class="item nodisplay" value="viral">
                                                    <a href="/hot/viral">popularity</a>
                        
                    </li>
                    <li class="item" value="time">
                                                    <a href="/hot/time">newest first</a>
                        
                    </li>
                                        <li class="item" value="top">
                                                    <a href="/top">highest scoring</a>
                        
                    </li>
                    
                
            </ul>
        </div>
        <input type="hidden" name="sort" value="viral">
    </div>

    

    <div class="sort-options">
                                    <ul>
                                            <li><a href="/gallery/random" id="random-button" class="title-n" title="random mode"><span></span></a></li>
                    
                                            <li><a href="javascript:void(0)" id="past-link" class="title-n" title="past images"><span></span></a></li>
                    
                </ul>
            
        
    </div>
    <div id="user-gallery-message">
        
    </div>
</div>

               
        

                    <div class="panel hidden">
                <div id="imagelist" class="home-gallery">
                                            <script type="text/tag-group-data">[]</script>


<div class="posts br10 first-child">
    

            
        <div class="outside-loader"></div>
        
                    
        

        <div class="cards">
                            <div id="9CIDs" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/9CIDs" data-page="0">
        <img alt="" src="//i.imgur.com/9Ixnjvxb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="9CIDs" type="image" data-up="14749">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="9CIDs" type="image" data-downs="221">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-9CIDs">14,528</span>
                            <span class="points-text-9CIDs">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Electronic Music I know to get stuff done with.</p>
        
        
        <div class="post-info">
            album &middot; 243,637 views
        </div>
    </div>
    
</div>

                            <div id="yX2lLxA" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/yX2lLxA" data-page="0">
        <img alt="" src="//i.imgur.com/yX2lLxAb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="yX2lLxA" type="image" data-up="3846">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="yX2lLxA" type="image" data-downs="45">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-yX2lLxA">3,801</span>
                            <span class="points-text-yX2lLxA">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Not Something You See Walking To Work Everyday!!!</p>
        
        
        <div class="post-info">
            image &middot; 1,489,292 views
        </div>
    </div>
    
</div>

                            <div id="k85fsFc" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/k85fsFc" data-page="0">
        <img alt="" src="//i.imgur.com/k85fsFcb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="k85fsFc" type="image" data-up="4326">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="k85fsFc" type="image" data-downs="249">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-k85fsFc">4,077</span>
                            <span class="points-text-k85fsFc">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>This poem reads negatively downward, but positively upward.</p>
        
        
        <div class="post-info">
            image &middot; 688,878 views
        </div>
    </div>
    
</div>

                            <div id="gezEc" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/gezEc" data-page="0">
        <img alt="" src="//i.imgur.com/rXSAHiqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="gezEc" type="image" data-up="12368">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="gezEc" type="image" data-downs="243">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-gezEc">12,125</span>
                            <span class="points-text-gezEc">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>30 Most Beautiful Words To Add To Your Vocabulary.</p>
        
        
        <div class="post-info">
            album &middot; 247,165 views
        </div>
    </div>
    
</div>

                            <div id="WvktW" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/WvktW" data-page="0">
        <img alt="" src="//i.imgur.com/D1ggKwhb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="WvktW" type="image" data-up="5914">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="WvktW" type="image" data-downs="139">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-WvktW">5,775</span>
                            <span class="points-text-WvktW">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Growing up shy</p>
        
        
        <div class="post-info">
            album &middot; 150,812 views
        </div>
    </div>
    
</div>

                            <div id="iwDC0" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/iwDC0" data-page="0">
        <img alt="" src="//i.imgur.com/A948duUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="iwDC0" type="image" data-up="8655">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="iwDC0" type="image" data-downs="347">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-iwDC0">8,308</span>
                            <span class="points-text-iwDC0">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>So I do Photoshop, and I need help.</p>
        
        
        <div class="post-info">
            album &middot; 216,139 views
        </div>
    </div>
    
</div>

                            <div id="EOf3F" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/EOf3F" data-page="0">
        <img alt="" src="//i.imgur.com/BAhqsAYb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="EOf3F" type="image" data-up="8092">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="EOf3F" type="image" data-downs="120">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-EOf3F">7,972</span>
                            <span class="points-text-EOf3F">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;Animal gifs with text on it? What does that even mean?&quot; Here let me show you.</p>
        
        
        <div class="post-info">
            album &middot; 202,512 views
        </div>
    </div>
    
</div>

                            <div id="mOspmJW" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/mOspmJW" data-page="0">
        <img alt="" src="//i.imgur.com/mOspmJWb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="mOspmJW" type="image" data-up="7651">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="mOspmJW" type="image" data-downs="110">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-mOspmJW">7,541</span>
                            <span class="points-text-mOspmJW">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Get into position! The Google truck is here...</p>
        
        
        <div class="post-info">
            animated &middot; 1,904,040 views
        </div>
    </div>
    
</div>

                            <div id="wStnZvi" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/wStnZvi" data-page="0">
        <img alt="" src="//i.imgur.com/wStnZvib.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="wStnZvi" type="image" data-up="3927">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="wStnZvi" type="image" data-downs="608">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-wStnZvi">3,319</span>
                            <span class="points-text-wStnZvi">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Why are women and children allowed to evacuate early?</p>
        
        
        <div class="post-info">
            image &middot; 576,684 views
        </div>
    </div>
    
</div>

                            <div id="EMjx84a" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/EMjx84a" data-page="0">
        <img alt="" src="//i.imgur.com/EMjx84ab.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="EMjx84a" type="image" data-up="4024">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="EMjx84a" type="image" data-downs="65">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-EMjx84a">3,959</span>
                            <span class="points-text-EMjx84a">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Just another brick</p>
        
        
        <div class="post-info">
            image &middot; 1,396,826 views
        </div>
    </div>
    
</div>

                            <div id="tcr3c" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/tcr3c" data-page="0">
        <img alt="" src="//i.imgur.com/9Iyef4tb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="tcr3c" type="image" data-up="4652">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="tcr3c" type="image" data-downs="164">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-tcr3c">4,488</span>
                            <span class="points-text-tcr3c">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&gt;.&lt;</p>
        
        
        <div class="post-info">
            album &middot; 153,322 views
        </div>
    </div>
    
</div>

                            <div id="lfqsFEL" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/lfqsFEL" data-page="0">
        <img alt="" src="//i.imgur.com/lfqsFELb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="lfqsFEL" type="image" data-up="7799">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="lfqsFEL" type="image" data-downs="330">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-lfqsFEL">7,469</span>
                            <span class="points-text-lfqsFEL">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Today, we remember Robin Williams on his birthday -- he would have been 64 years old.</p>
        
        
        <div class="post-info">
            image &middot; 359,957 views
        </div>
    </div>
    
</div>

                            <div id="EbEOymH" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/EbEOymH" data-page="0">
        <img alt="" src="//i.imgur.com/EbEOymHb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="EbEOymH" type="image" data-up="4841">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="EbEOymH" type="image" data-downs="57">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-EbEOymH">4,784</span>
                            <span class="points-text-EbEOymH">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I can&#039;t find MY bowl :&#039;(</p>
        
        
        <div class="post-info">
            animated &middot; 439,908 views
        </div>
    </div>
    
</div>

                            <div id="nUCiE7y" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/nUCiE7y" data-page="0">
        <img alt="" src="//i.imgur.com/nUCiE7yb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="nUCiE7y" type="image" data-up="3520">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="nUCiE7y" type="image" data-downs="67">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-nUCiE7y">3,453</span>
                            <span class="points-text-nUCiE7y">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Near Collision, Instant Karma</p>
        
        
        <div class="post-info">
            animated &middot; 1,682,806 views
        </div>
    </div>
    
</div>

                            <div id="eo72EMC" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/eo72EMC" data-page="0">
        <img alt="" src="//i.imgur.com/eo72EMCb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="eo72EMC" type="image" data-up="5189">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="eo72EMC" type="image" data-downs="100">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-eo72EMC">5,089</span>
                            <span class="points-text-eo72EMC">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Down voting a Front Page post</p>
        
        
        <div class="post-info">
            animated &middot; 371,495 views
        </div>
    </div>
    
</div>

                            <div id="phezeLF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/phezeLF" data-page="0">
        <img alt="" src="//i.imgur.com/phezeLFb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="phezeLF" type="image" data-up="2889">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="phezeLF" type="image" data-downs="147">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-phezeLF">2,742</span>
                            <span class="points-text-phezeLF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Dear Makers of Cetaphil, please add some dye to your product so it doesn&#039;t feel like I&#039;m starring in a porn film when I wash my face.</p>
        
        
        <div class="post-info">
            image &middot; 928,863 views
        </div>
    </div>
    
</div>

                            <div id="ow09wpM" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ow09wpM" data-page="0">
        <img alt="" src="//i.imgur.com/ow09wpMb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ow09wpM" type="image" data-up="3843">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ow09wpM" type="image" data-downs="154">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ow09wpM">3,689</span>
                            <span class="points-text-ow09wpM">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Robert Downey Jr just posted this</p>
        
        
        <div class="post-info">
            animated &middot; 254,631 views
        </div>
    </div>
    
</div>

                            <div id="adiZs" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/adiZs" data-page="0">
        <img alt="" src="//i.imgur.com/DtOZ65Pb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="adiZs" type="image" data-up="7824">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="adiZs" type="image" data-downs="200">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-adiZs">7,624</span>
                            <span class="points-text-adiZs">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Oooops, didn&#039;t mean to accidentally open this can of whoop ass!</p>
        
        
        <div class="post-info">
            album &middot; 268,172 views
        </div>
    </div>
    
</div>

                            <div id="fcxSj34" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/fcxSj34" data-page="0">
        <img alt="" src="//i.imgur.com/fcxSj34b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="fcxSj34" type="image" data-up="3218">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="fcxSj34" type="image" data-downs="159">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-fcxSj34">3,059</span>
                            <span class="points-text-fcxSj34">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Highly skilled giraffes training for the olympic championship</p>
        
        
        <div class="post-info">
            animated &middot; 269,825 views
        </div>
    </div>
    
</div>

                            <div id="XuscsaQ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/XuscsaQ" data-page="0">
        <img alt="" src="//i.imgur.com/XuscsaQb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="XuscsaQ" type="image" data-up="6829">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="XuscsaQ" type="image" data-downs="272">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-XuscsaQ">6,557</span>
                            <span class="points-text-XuscsaQ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>20/20</p>
        
        
        <div class="post-info">
            image &middot; 345,563 views
        </div>
    </div>
    
</div>

                            <div id="RHkga2q" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/RHkga2q" data-page="0">
        <img alt="" src="//i.imgur.com/RHkga2qb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="RHkga2q" type="image" data-up="4957">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="RHkga2q" type="image" data-downs="197">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-RHkga2q">4,760</span>
                            <span class="points-text-RHkga2q">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Walking into the office and realizing it&#039;s only Tuesday</p>
        
        
        <div class="post-info">
            animated &middot; 391,543 views
        </div>
    </div>
    
</div>

                            <div id="Bf6dYSJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Bf6dYSJ" data-page="0">
        <img alt="" src="//i.imgur.com/Bf6dYSJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Bf6dYSJ" type="image" data-up="4025">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Bf6dYSJ" type="image" data-downs="100">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Bf6dYSJ">3,925</span>
                            <span class="points-text-Bf6dYSJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My gf gave me this....</p>
        
        
        <div class="post-info">
            image &middot; 217,858 views
        </div>
    </div>
    
</div>

                            <div id="hFei4AW" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/hFei4AW" data-page="0">
        <img alt="" src="//i.imgur.com/hFei4AWb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="hFei4AW" type="image" data-up="4439">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="hFei4AW" type="image" data-downs="129">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-hFei4AW">4,310</span>
                            <span class="points-text-hFei4AW">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>TIL: Home Depot is puppy friendly</p>
        
        
        <div class="post-info">
            image &middot; 1,777,853 views
        </div>
    </div>
    
</div>

                            <div id="FIAXE" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FIAXE" data-page="0">
        <img alt="" src="//i.imgur.com/L8Harjpb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FIAXE" type="image" data-up="3533">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FIAXE" type="image" data-downs="135">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FIAXE">3,398</span>
                            <span class="points-text-FIAXE">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Old School</p>
        
        
        <div class="post-info">
            album &middot; 118,997 views
        </div>
    </div>
    
</div>

                            <div id="akNdYL0" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/akNdYL0" data-page="0">
        <img alt="" src="//i.imgur.com/akNdYL0b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="akNdYL0" type="image" data-up="2231">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="akNdYL0" type="image" data-downs="74">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-akNdYL0">2,157</span>
                            <span class="points-text-akNdYL0">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>One of my favorite tweets.</p>
        
        
        <div class="post-info">
            image &middot; 92,154 views
        </div>
    </div>
    
</div>

                            <div id="o0P6uDZ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/o0P6uDZ" data-page="0">
        <img alt="" src="//i.imgur.com/o0P6uDZb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="o0P6uDZ" type="image" data-up="7928">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="o0P6uDZ" type="image" data-downs="77">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-o0P6uDZ">7,851</span>
                            <span class="points-text-o0P6uDZ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Gotta go fast!</p>
        
        
        <div class="post-info">
            animated &middot; 679,592 views
        </div>
    </div>
    
</div>

                            <div id="Gda2sH5" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Gda2sH5" data-page="0">
        <img alt="" src="//i.imgur.com/Gda2sH5b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Gda2sH5" type="image" data-up="4818">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Gda2sH5" type="image" data-downs="101">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Gda2sH5">4,717</span>
                            <span class="points-text-Gda2sH5">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>English is rough, but I can never get enough of the stuff</p>
        
        
        <div class="post-info">
            image &middot; 263,291 views
        </div>
    </div>
    
</div>

                            <div id="Hj6wB" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Hj6wB" data-page="0">
        <img alt="" src="//i.imgur.com/e0yyrUGb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Hj6wB" type="image" data-up="2470">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Hj6wB" type="image" data-downs="105">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Hj6wB">2,365</span>
                            <span class="points-text-Hj6wB">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Celebrity PokÃ©dex</p>
        
        
        <div class="post-info">
            album &middot; 61,109 views
        </div>
    </div>
    
</div>

                            <div id="8LGJ09x" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/8LGJ09x" data-page="0">
        <img alt="" src="//i.imgur.com/8LGJ09xb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="8LGJ09x" type="image" data-up="3441">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="8LGJ09x" type="image" data-downs="100">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-8LGJ09x">3,341</span>
                            <span class="points-text-8LGJ09x">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Terrific</p>
        
        
        <div class="post-info">
            animated &middot; 337,244 views
        </div>
    </div>
    
</div>

                            <div id="8cDPt9C" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/8cDPt9C" data-page="0">
        <img alt="" src="//i.imgur.com/8cDPt9Cb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="8cDPt9C" type="image" data-up="4154">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="8cDPt9C" type="image" data-downs="162">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-8cDPt9C">3,992</span>
                            <span class="points-text-8cDPt9C">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Another greentext</p>
        
        
        <div class="post-info">
            image &middot; 223,900 views
        </div>
    </div>
    
</div>

                            <div id="PTewAV9" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/PTewAV9" data-page="0">
        <img alt="" src="//i.imgur.com/PTewAV9b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="PTewAV9" type="image" data-up="5528">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="PTewAV9" type="image" data-downs="206">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-PTewAV9">5,322</span>
                            <span class="points-text-PTewAV9">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW I see my coworker leaving early.</p>
        
        
        <div class="post-info">
            animated &middot; 972,902 views
        </div>
    </div>
    
</div>

                            <div id="xKXX2LR" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/xKXX2LR" data-page="0">
        <img alt="" src="//i.imgur.com/xKXX2LRb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="xKXX2LR" type="image" data-up="8592">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="xKXX2LR" type="image" data-downs="216">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-xKXX2LR">8,376</span>
                            <span class="points-text-xKXX2LR">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>the fish are single</p>
        
        
        <div class="post-info">
            image &middot; 408,459 views
        </div>
    </div>
    
</div>

                            <div id="FZab5Fa" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FZab5Fa" data-page="0">
        <img alt="" src="//i.imgur.com/FZab5Fab.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FZab5Fa" type="image" data-up="10907">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FZab5Fa" type="image" data-downs="176">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FZab5Fa">10,731</span>
                            <span class="points-text-FZab5Fa">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Chubs says one final goodbye to his old bed, so cute.</p>
        
        
        <div class="post-info">
            animated &middot; 872,213 views
        </div>
    </div>
    
</div>

                            <div id="yJzhVF3" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/yJzhVF3" data-page="0">
        <img alt="" src="//i.imgur.com/yJzhVF3b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="yJzhVF3" type="image" data-up="4038">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="yJzhVF3" type="image" data-downs="116">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-yJzhVF3">3,922</span>
                            <span class="points-text-yJzhVF3">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Buzz Aldrin punches conspriacy theorist</p>
        
        
        <div class="post-info">
            animated &middot; 385,145 views
        </div>
    </div>
    
</div>

                            <div id="0BYJc" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/0BYJc" data-page="0">
        <img alt="" src="//i.imgur.com/mBDS6zLb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="0BYJc" type="image" data-up="1781">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="0BYJc" type="image" data-downs="40">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-0BYJc">1,741</span>
                            <span class="points-text-0BYJc">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>being positive</p>
        
        
        <div class="post-info">
            album &middot; 33,893 views
        </div>
    </div>
    
</div>

                            <div id="wfZh286" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/wfZh286" data-page="0">
        <img alt="" src="//i.imgur.com/wfZh286b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="wfZh286" type="image" data-up="4412">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="wfZh286" type="image" data-downs="198">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-wfZh286">4,214</span>
                            <span class="points-text-wfZh286">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Mother&#039;s Touch</p>
        
        
        <div class="post-info">
            animated &middot; 1,169,664 views
        </div>
    </div>
    
</div>

                            <div id="xrhFGTR" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/xrhFGTR" data-page="0">
        <img alt="" src="//i.imgur.com/xrhFGTRb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="xrhFGTR" type="image" data-up="4183">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="xrhFGTR" type="image" data-downs="163">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-xrhFGTR">4,020</span>
                            <span class="points-text-xrhFGTR">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I have fantasized about this my entire life. It has finally happened.</p>
        
        
        <div class="post-info">
            image &middot; 235,979 views
        </div>
    </div>
    
</div>

                            <div id="RWeoHGf" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/RWeoHGf" data-page="0">
        <img alt="" src="//i.imgur.com/RWeoHGfb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="RWeoHGf" type="image" data-up="2274">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="RWeoHGf" type="image" data-downs="248">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-RWeoHGf">2,026</span>
                            <span class="points-text-RWeoHGf">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>.ecneinevnocni eht rof ezigolopa I .sdrawkcab si eltit sihT</p>
        
        
        <div class="post-info">
            animated &middot; 159,465 views
        </div>
    </div>
    
</div>

                            <div id="m8ukims" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/m8ukims" data-page="0">
        <img alt="" src="//i.imgur.com/m8ukimsb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="m8ukims" type="image" data-up="3195">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="m8ukims" type="image" data-downs="53">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-m8ukims">3,142</span>
                            <span class="points-text-m8ukims">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Apparently it&#039;s my cakeday today, so here&#039;s a gif of astronaut Harrison Schmitt falling on the moon</p>
        
        
        <div class="post-info">
            animated &middot; 266,099 views
        </div>
    </div>
    
</div>

                            <div id="b3R9qjd" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/b3R9qjd" data-page="0">
        <img alt="" src="//i.imgur.com/b3R9qjdb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="b3R9qjd" type="image" data-up="2775">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="b3R9qjd" type="image" data-downs="306">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-b3R9qjd">2,469</span>
                            <span class="points-text-b3R9qjd">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Good morning Imgur</p>
        
        
        <div class="post-info">
            animated &middot; 249,316 views
        </div>
    </div>
    
</div>

                            <div id="DF77ar3" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/DF77ar3" data-page="0">
        <img alt="" src="//i.imgur.com/DF77ar3b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="DF77ar3" type="image" data-up="1020">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="DF77ar3" type="image" data-downs="61">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-DF77ar3">959</span>
                            <span class="points-text-DF77ar3">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Razer with the burns... also, don&#039;t be this guy.</p>
        
        
        <div class="post-info">
            image &middot; 296,289 views
        </div>
    </div>
    
</div>

                            <div id="PU5BE" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/PU5BE" data-page="0">
        <img alt="" src="//i.imgur.com/zZ3vYjkb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="PU5BE" type="image" data-up="10230">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="PU5BE" type="image" data-downs="209">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-PU5BE">10,021</span>
                            <span class="points-text-PU5BE">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Wasted.</p>
        
        
        <div class="post-info">
            album &middot; 633,709 views
        </div>
    </div>
    
</div>

                            <div id="5cjAGnp" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/5cjAGnp" data-page="0">
        <img alt="" src="//i.imgur.com/5cjAGnpb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="5cjAGnp" type="image" data-up="7690">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="5cjAGnp" type="image" data-downs="332">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-5cjAGnp">7,358</span>
                            <span class="points-text-5cjAGnp">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>How to Make a Fly-Plane</p>
        
        
        <div class="post-info">
            image &middot; 432,556 views
        </div>
    </div>
    
</div>

                            <div id="lucoyw5" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/lucoyw5" data-page="0">
        <img alt="" src="//i.imgur.com/lucoyw5b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="lucoyw5" type="image" data-up="3634">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="lucoyw5" type="image" data-downs="73">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-lucoyw5">3,561</span>
                            <span class="points-text-lucoyw5">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Young ladies taunting the law, 1934, Miami Beach</p>
        
        
        <div class="post-info">
            image &middot; 1,475,834 views
        </div>
    </div>
    
</div>

                            <div id="paDhBAh" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/paDhBAh" data-page="0">
        <img alt="" src="//i.imgur.com/paDhBAhb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="paDhBAh" type="image" data-up="4781">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="paDhBAh" type="image" data-downs="313">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-paDhBAh">4,468</span>
                            <span class="points-text-paDhBAh">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The other side if the coin</p>
        
        
        <div class="post-info">
            image &middot; 284,599 views
        </div>
    </div>
    
</div>

                            <div id="YltyJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/YltyJ" data-page="0">
        <img alt="" src="//i.imgur.com/t393GFSb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="YltyJ" type="image" data-up="3299">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="YltyJ" type="image" data-downs="106">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-YltyJ">3,193</span>
                            <span class="points-text-YltyJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Took me a while</p>
        
        
        <div class="post-info">
            album &middot; 117,574 views
        </div>
    </div>
    
</div>

                            <div id="lEX8FmA" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/lEX8FmA" data-page="0">
        <img alt="" src="//i.imgur.com/lEX8FmAb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="lEX8FmA" type="image" data-up="1820">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="lEX8FmA" type="image" data-downs="190">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-lEX8FmA">1,630</span>
                            <span class="points-text-lEX8FmA">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>me irl</p>
        
        
        <div class="post-info">
            image &middot; 398,503 views
        </div>
    </div>
    
</div>

                            <div id="FVaF4dg" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FVaF4dg" data-page="0">
        <img alt="" src="//i.imgur.com/FVaF4dgb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FVaF4dg" type="image" data-up="5169">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FVaF4dg" type="image" data-downs="105">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FVaF4dg">5,064</span>
                            <span class="points-text-FVaF4dg">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Feeding the beast.</p>
        
        
        <div class="post-info">
            animated &middot; 518,773 views
        </div>
    </div>
    
</div>

                            <div id="ouJRRJW" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ouJRRJW" data-page="0">
        <img alt="" src="//i.imgur.com/ouJRRJWb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ouJRRJW" type="image" data-up="5524">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ouJRRJW" type="image" data-downs="241">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ouJRRJW">5,283</span>
                            <span class="points-text-ouJRRJW">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Never thought Mega-Man and Samus would look so cute together</p>
        
        
        <div class="post-info">
            image &middot; 359,543 views
        </div>
    </div>
    
</div>

                            <div id="mUN72aU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/mUN72aU" data-page="0">
        <img alt="" src="//i.imgur.com/mUN72aUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="mUN72aU" type="image" data-up="1783">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="mUN72aU" type="image" data-downs="127">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-mUN72aU">1,656</span>
                            <span class="points-text-mUN72aU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Saw this on the interwebs... made me chuckle!</p>
        
        
        <div class="post-info">
            image &middot; 76,215 views
        </div>
    </div>
    
</div>

                            <div id="rBmKELV" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/rBmKELV" data-page="0">
        <img alt="" src="//i.imgur.com/rBmKELVb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="rBmKELV" type="image" data-up="994">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="rBmKELV" type="image" data-downs="36">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-rBmKELV">958</span>
                            <span class="points-text-rBmKELV">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My first honey harvest, nearly 30 lb of the stuff</p>
        
        
        <div class="post-info">
            image &middot; 626,877 views
        </div>
    </div>
    
</div>

                            <div id="HIxzLOQ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/HIxzLOQ" data-page="0">
        <img alt="" src="//i.imgur.com/HIxzLOQb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="HIxzLOQ" type="image" data-up="3947">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="HIxzLOQ" type="image" data-downs="143">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-HIxzLOQ">3,804</span>
                            <span class="points-text-HIxzLOQ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW someone asks me to hold their baby</p>
        
        
        <div class="post-info">
            animated &middot; 414,120 views
        </div>
    </div>
    
</div>

                            <div id="50zEEXq" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/50zEEXq" data-page="0">
        <img alt="" src="//i.imgur.com/50zEEXqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="50zEEXq" type="image" data-up="2569">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="50zEEXq" type="image" data-downs="57">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-50zEEXq">2,512</span>
                            <span class="points-text-50zEEXq">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;When out on the roof there arose such a clatter, I sprang from my bed to see what was the matter.&quot;</p>
        
        
        <div class="post-info">
            animated &middot; 646,416 views
        </div>
    </div>
    
</div>

                            <div id="oSynR" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/oSynR" data-page="0">
        <img alt="" src="//i.imgur.com/FIVUNcKb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="oSynR" type="image" data-up="3498">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="oSynR" type="image" data-downs="153">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-oSynR">3,345</span>
                            <span class="points-text-oSynR">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The Golden Ratio</p>
        
        
        <div class="post-info">
            album &middot; 122,311 views
        </div>
    </div>
    
</div>

                            <div id="Am0ihkU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Am0ihkU" data-page="0">
        <img alt="" src="//i.imgur.com/Am0ihkUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Am0ihkU" type="image" data-up="3891">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Am0ihkU" type="image" data-downs="59">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Am0ihkU">3,832</span>
                            <span class="points-text-Am0ihkU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW I have to do everything. And my roommate just hangs out with his friends all day.</p>
        
        
        <div class="post-info">
            animated &middot; 315,709 views
        </div>
    </div>
    
</div>

                            <div id="yNQirIJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/yNQirIJ" data-page="0">
        <img alt="" src="//i.imgur.com/yNQirIJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="yNQirIJ" type="image" data-up="5064">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="yNQirIJ" type="image" data-downs="107">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-yNQirIJ">4,957</span>
                            <span class="points-text-yNQirIJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Thanks for telling me, bro</p>
        
        
        <div class="post-info">
            animated &middot; 649,515 views
        </div>
    </div>
    
</div>

                            <div id="PlyDHM6" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/PlyDHM6" data-page="0">
        <img alt="" src="//i.imgur.com/PlyDHM6b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="PlyDHM6" type="image" data-up="2814">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="PlyDHM6" type="image" data-downs="324">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-PlyDHM6">2,490</span>
                            <span class="points-text-PlyDHM6">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Im scared of people finding out</p>
        
        
        <div class="post-info">
            image &middot; 175,671 views
        </div>
    </div>
    
</div>

                            <div id="HpOeXoo" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/HpOeXoo" data-page="0">
        <img alt="" src="//i.imgur.com/HpOeXoob.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="HpOeXoo" type="image" data-up="1810">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="HpOeXoo" type="image" data-downs="298">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-HpOeXoo">1,512</span>
                            <span class="points-text-HpOeXoo">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;Bernie Sanders&quot; is now the fifth most popular search on NYTimes.com! Up from eighth most popular yesterday. Keep up those searches to end the</p>
        
        
        <div class="post-info">
            image &middot; 279,768 views
        </div>
    </div>
    
</div>

                            <div id="vdC7V" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/vdC7V" data-page="0">
        <img alt="" src="//i.imgur.com/sKV54POb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="vdC7V" type="image" data-up="15877">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="vdC7V" type="image" data-downs="206">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-vdC7V">15,671</span>
                            <span class="points-text-vdC7V">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I have special tastes in backgrounds</p>
        
        
        <div class="post-info">
            album &middot; 323,385 views
        </div>
    </div>
    
</div>

                            <div id="jjnlI" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/jjnlI" data-page="0">
        <img alt="" src="//i.imgur.com/TNmdbG4b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="jjnlI" type="image" data-up="3505">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="jjnlI" type="image" data-downs="80">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-jjnlI">3,425</span>
                            <span class="points-text-jjnlI">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Some words of wisdom for all you degenerates.</p>
        
        
        <div class="post-info">
            album &middot; 98,587 views
        </div>
    </div>
    
</div>

            
        </div>

        <div class="clear"></div>

        <div class="imagelist-loader textbox center lineheight small-margin-top">
            <img src="//s.imgur.com/images/loaders/ddddd1_181817/48.gif" />
        </div>
    


    </div>
            <div class="page-divider"><a href="javascript:;" class="scroll-top-text" data-jafo="{@@event@@:@@galleryBackToTop@@,@@meta@@:{@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@pageNum@@:0}}">back to top</a></div>
    


                    
                </div>
            
                                                            <div id="top-comments" class="left-column">
                                                                                    <div id="outside-tagging" class="br10">
                                    <div class="group-add-done nodisplay"></div>
        
                                    <div id="tags-current-list" class="tag-list tag-list-immutable">
                                                                                        <div class="panel">
        <div class="textbox">
            <h2>Today&#039;s Best Comments</h2>
        </div>
    
        <div class="captions best-captions">
                            <div class="caption-container rank-0">
                    <p class="clickability">
                        <a href="/gallery/Wqbhl/comment/447754747"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="10 Horror Movies You Must See" src="//i.imgur.com/9nkVyekb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/Nostradamuswaswrong">Nostradamuswaswrong</a> 6,079 points
                        </div>
        
                                                    I think I&#039;m noticing a trend with these images.  I haven&#039;t quite put my finger on it, but I&#039;m circling the area around it.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-1">
                    <p class="clickability">
                        <a href="/gallery/qkaBxVc/comment/447852336"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Sometimes guys need to be shown a little unnecessary love too." src="//i.imgur.com/qkaBxVcb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/ebearce">ebearce</a> 4,220 points
                        </div>
        
                                                    It&#039;s almost as if a relationship is about two people who need to work together to maintain it.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-2">
                    <p class="clickability">
                        <a href="/gallery/1RBO7ql/comment/447928111"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I told my grandfather that I was going to build a PC, I then received this in the mail a few days after..." src="//i.imgur.com/1RBO7qlb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/genericusername5">genericusername5</a> 3,804 points
                        </div>
        
                                                    THAT IS A THOUGHTFUL PRESENT, YOU GO THANK HIM!
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/Af8iH34/comment/447801270"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="MRW I have a meeting with my boss, my boss&#039;s boss, my boss&#039;s boss&#039;s boss and HR about a tanktop" src="//i.imgur.com/Af8iH34b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/flowseeker">flowseeker</a> 3,679 points
                        </div>
        
                                                    If looking at a woman in a tank top prevents you from doing your job, you seriously need to find a way to release that sexual frustration.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/EnpentG/comment/447728672"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Worst Muslim Ever" src="//i.imgur.com/EnpentGb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/Weeabooninja">Weeabooninja</a> 3,330 points
                        </div>
        
                                                    But you HAVE heard of him
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/epe2GKo/comment/447835105"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Every night this majestic beast emerges from his cave to guard my apartment." src="//i.imgur.com/epe2GKob.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/QuincyHarvey">QuincyHarvey</a> 2,760 points
                        </div>
        
                                                    From Reddit: <a class="imgur-image" data-hash="2GsShRc" href="//i.imgur.com/2GsShRc.jpg">http://i.imgur.com/2GsShRc.jpg</a>
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container last-child">
                    <p class="clickability">
                        <a href="/gallery/TMHDvqO/comment/447802864"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I&#039;m gonna repurpose a tablet... or, or  make a weather station! or build a robot! or..." src="//i.imgur.com/TMHDvqOb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/Naugle">Naugle</a> 2,681 points
                        </div>
        
                                                    I was waiting for eggs to start flying everywhere
                        
                    </div>
                </div>

                <div class="clear"></div>
            
    
            
        </div>
    </div>


                                        
                                    </div>
                                </div>
        
                                <div id="outside-tagging-showhide" class="no-select title-e short" title=" Show Sidebar">
                                                                            <div id="comment-tab">
                                            <div id="comment-icon-one"></div>
                                            <div id="comment-icon-two"></div>
                                            <div id="comment-icon-three"></div>
                                        </div>
                                    
                                </div>
                            
                        
                        </div>
                    
                
            </div>
        

        <div class="clear"></div>
    </div>

    <div class="nodisplay">
        <div id="past-wrapper" class="popup">
            <div>
                <div class="textbox left">
                    

                    <input type="text" id="days" />

                                            days ago
                    
                </div>
                
                <div id="past-calendar-wrapper" class="textbox left">
                    <div id="past-calendar"></div>
                </div>

                <div id="slider-wrapper" class="textbox left">
                    <div class="ticker negative">&laquo;</div>
                    <div id="slider"></div>
                    <div class="ticker positive">&raquo;</div>
                </div>

                <div id="button-wrapper" class="textbox left">
                    <input type="button" class="button-medium" value="Go" id="past-submit" />
                </div>

                <div class="clear"></div>
            </div>

            <div id="past-loader"></div>
            <div id="past-preview"></div>
        </div>
    </div>

    <div class="tag-group-selector-container"></div>
            <div id="top-tag-container"></div>
    

    <input id="sid" type="hidden" value="3a343c40138c409717df5b87960d9dd7" />
        

    

            
    
    
    

            <script type="text/javascript">
            (function() {
                var roll = Math.random();
                if(roll < 0.05) {
                    
                    (function(_,e,rr,s){
                        _errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                        _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)
                    })(window,document,"script","51d1e360b05ccb10310000d2");
                    
                }
            })();
        </script>

        
<!--[if lte IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery-1.10.2.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if gt IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<!--<![endif]-->



        <script type="text/javascript" src="//s.imgur.com/min/global.js?1437516888"></script>

        <script type="text/javascript">
            Imgur.Environment = {
                isDev:         false,
                isSubdomain:   Imgur.Util.isSubdomain('imgur.com'),
                host:          'imgur.com',
                cdnUrl:        '//i.imgur.com',
                signed:        false,
                auth:          {},
                recaptcha_key: '6LeZbt4SAAAAAKEsafT3QzEFp5vJ1-Z23uy5mPDz',
                msid:          '3a343c40138c409717df5b87960d9dd7',
                beta:          {
                    enabled:   true,
                    inBeta:    false
                },
                
                ctaConfig:     {"anonymous":[{"active":true,"type":"button","trackingName":"anonymous","localStorageName":"cta-anonymous","url":"\/","buttonText":"start exploring","title":"Welcome to Imgur!","description":"Your new home for the Internet's best visual stories.","background":"\/\/s.imgur.com\/images\/house-cta\/cta-background.jpg","newTab":false,"buttonColor":"#77ac21"}],"user":[{"active":false,"type":"button","trackingName":"campImgur","localStorageName":"cta-camp1418760143","customClass":"u-pl95","url":"\/\/imgur.com\/camp","buttonText":"Get More Details","title":"Camp Imgur, August 2015","description":"We're 50% sold out! Reserve your space now.","background":"\/\/s.imgur.com\/images\/house-cta\/cta-camp-imgur.jpg","newTab":true,"buttonColor":"#cf5033"}],"gaming":[{"active":true,"type":"button","trackingName":"gaming","localStorageName":"cta-gaming","url":"\/\/imgur.com\/t\/gaming\/top\/week","buttonText":"Check it out!","title":"You are now browsing the Internet on God Mode.","description":"Awesome unlocked! Imgur has gaming content like you wouldn't believe.","background":"\/\/s.imgur.com\/images\/house-cta\/cta-twitch.jpg","newTab":false,"buttonColor":"#4d3280"}],"facebook":[{"active":true,"type":"facebook","trackingName":"facebook","localStorageName":"cta-fb141008","title":"Like us on Facebook!","description":"We're bringing funny to your newsfeed. Like Imgur on Facebook!","background":"\/\/s.imgur.com\/images\/house-cta\/facebook-day.jpg"}],"pinterest":[{"active":true,"type":"pinterest","trackingName":"pinterest","localStorageName":"cta-pinterest-2015-07-14","title":"Follow us on Pinterest!","background":"\/\/s.imgur.com\/images\/house-cta\/pinterest.png","customClass":"pinterest"}],"promoted":[{"active":true,"type":"promoted","trackingName":"promoted","localStorageName":false,"buttonText":"Learn More","title":"What Are Promoted Posts?","description":"And why am I seeing them?","background":"\/\/s.imgur.com\/images\/house-cta\/cta-promoted.jpg","url":"\/\/imgur.com\/blog\/?p=6101","newTab":true,"customClass":"pp-banner"}],"global":[{"active":false,"trackingName":"inCaseYouMissedIt","localStorageName":"cta-icymi-2015-07-14","type":"button","subtype":"app","background":"\/\/s.imgur.com\/images\/house-cta\/cta-icymi.png","url":"\/\/imgur.com\/topic\/Hand_Picked\/","buttonText":"Take me there","title":"Catch up on the awesome posts you might have missed!","description":"Now featured in the Hand Picked topic.","newTab":false,"customClass":"u-pl260 icymi","buttonColor":"#252525","frontPageOnly":true},{"active":true,"trackingName":"mobileApps","localStorageName":"apps1433176979","type":"button","background":"\/\/s.imgur.com\/images\/house-cta\/cta-apps.jpg?1433176979","url":"\/\/imgur.com\/apps","buttonText":"Count me in!","title":"Get the Imgur Mobile App!","description":"Fully Native. Totally Awesome.","newTab":true,"customClass":"u-pl260"}]},
                isGalleryAdmin:false,
                ppBlog: '//imgur.com/blog/?p=6101'
            };

            var imgur = Imgur.getInstance();
            imgur.init(_.merge(Imgur.Environment, {
                
                
                beta: {"enabled":true,"inBeta":false,"showInvite":false,"ctaInvite":[],"ctaWelcome":[]},
            }));

                            var uploader = Imgur.Upload.Global.getInstance(Imgur.getInstance()._);

                if(uploader._.el.$uploadGlobal.length > 0) {
                    uploader.bindPasteUpload();
                    uploader.generalInit();
                }
            

            imgur.generalInit();

            $(function() {
                
            });

            
            

            var _widgetFactory = new Imgur.Factory.Widget(Imgur.Environment);

            _widgetFactory.mergeConfig('global', Imgur.Environment);

            
        </script>
                                
    
    
                    <script type="text/javascript">
            (function(widgetFactory) { 
                widgetFactory.mergeConfig('analytics', {
                    isAdmin: false,
                    logPixel: '//p.imgur.com/lumbar.gif'
                });
                widgetFactory.mergeConfig('global', {
                    analyticsAccountId: 'UA-6671908-2'
                });
    
                widgetFactory.mergeConfig('search', {
                    type: 'global', 
                    q: ''
                });

                widgetFactory.produceJafo();

                                    widgetFactory.produceCtaBanner("/gallery/" + "9CIDs");
                
            })(_widgetFactory);
        </script>
    

    <script type="text/javascript">
    (function(widgetFactory) { 
        widgetFactory.mergeConfig('global', {
            sid: '3a343c40138c409717df5b87960d9dd7'
        });

        widgetFactory.mergeConfig('gallery', {
            sid         : '3a343c40138c409717df5b87960d9dd7',
            signed      : false,
            baseURL     : decodeURIComponent('%2Fgallery'),
            sort        : 'viral',
            window      : 'day',
            page        : 0,
            isPro       : false,
            maxPage     : 1662,
            showPast    : true,
            searchQuery : '',
            inSearch    : false,
            advSearch   : null,
            bestCaps    : true
        });

        widgetFactory.mergeConfig('groups', {
            groups: {
                
            }
        });

        widgetFactory.mergeConfig('search', {
            
        });
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
        (function(widgetFactory) {
            var group_type = 'base';

            var group = {
                id: '[hot]',
                tags: [
                    
                ]
            }

            widgetFactory.mergeConfig('gallery', {
                section            : 'hot',
                showGalleryNewInfo : false,
                gallery_type       : null,
                tag                : null,
                sponsoredTag       : null,
                tagFollower        : false,
                group_type         : group_type,
                group              : group
            });
        })(_widgetFactory);
    </script>


    <script type="text/javascript">
        if (typeof _widgetFactory !== 'undefined') {
            _widgetFactory.initNotifications({"post":{"postUpvote":[100,500,1000,5000],"postComment":[10,50,100],"postView":[100000,500000,1000000,5000000],"postEmbed":[1],"userMention":[1],"postViral":[1]},"comment":{"commentUpvote":[10,100,500,1000,5000,10000],"commentReply":[1],"commentTop7":[1]},"trophy":{"userTrophy":[1]},"account":{"userNotoriety":[-1,399,999,1999,3999,7999,19999,9223372036854775807]},"message":{"message-msg":[1]}}, [{"name":"Forever Alone","reputation":-1},{"name":"Neutral","reputation":399},{"name":"Accepted","reputation":999},{"name":"Liked","reputation":1999},{"name":"Trusted","reputation":3999},{"name":"Idolized","reputation":7999},{"name":"Renowned","reputation":19999},{"name":"Glorious","reputation":9223372036854775807}]);
        }
    </script>

        <script type="text/javascript" src="//s.imgur.com/min/index.js?1437516888"></script>

    <script type="text/javascript" src="//s.imgur.com/min/gallery.js?1437516888"></script>

    <script type="text/javascript">
    (function(widgetFactory) {
        var emitter = new Imgur.Emitter();

        widgetFactory.produceLocationCommand(emitter);

        var galleryFactory = widgetFactory.produceOutsideGalleryFactory();
        var galleryGraph = galleryFactory.produceOutsideGallery(emitter);

        var gallery = galleryGraph.gallery,
            history = galleryGraph.history;

        var outsideTaggingWidget = galleryFactory.produceOutsideTaggingWidget(emitter, galleryGraph);

        gallery.outsideInit();

        Imgur.Upload.Index.getInstance().generalInit();
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
    $(function() {
        if(!/^([^:]+:)\/\/([^.]+\.)*imgur(-dev)?\.com(\/.*)?$/.test(document.referrer)) {
            Imgur.Util.jafoLog({ event: 'galleryDisplay', meta: { gallerySort: 'viral', galleryType: 'hot' }});
        }
    });
    </script>
    
    

            <script type="text/javascript">
        (function(widgetFactory) {
            widgetFactory.produceSearch();
        })(_widgetFactory);
        </script>
    

        
    

                        
            <script type="text/javascript">
                (function(widgetFactory) {
                    var inIframe = false;
                    try {
                        inIframe = window.self !== window.top;
                    } catch (e) {
                        inIframe = true;
                    }
                    if(!inIframe) {
                        $.getScript('//cdn.optimizely.com/js/1507425748.js', function(){
                            Imgur.Util.getGoogleTracker();
                            __ga('send', 'pageview');
                            Imgur.Util.Tests.activate();
                        });
                    } else {
                        Imgur.Util.getGoogleTracker();
                        __ga('send', 'pageview');
                    }
                })(_widgetFactory);
            </script>
        
        
        
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        

        

        
    

        <script type='text/javascript'>
        
        (function(){
            try{
                var msg = '\n      _\n     (_)\n      _ _ __ ___   __ _ _   _ _ __\n     | | \'_ ` _ \\ / _` | | | | \'__|\n     | | | | | | | (_| | |_| | |\n     |_|_| |_| |_|\\__, |\\__,_|_|\n                   __/ |\n                  |___/\n';
                msg += '========================================\nYou opened the console! Know some code,\ndo you? Want to work for one of the best\nstartups around? http://imgur.com/jobs\n========================================\n';
                imcon.log(msg);
            }catch(e){}
        })()
        
    </script>
    

        

</body>
</html>
