



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>


    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "800-2634352-2692804";
                var ue_id = "1NJ3NGT9VV7C40XCC6KZ";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        

        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1NJ3NGT9VV7C40XCC6KZ" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-40f9beaa.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1965580546._CB290605844_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-1180111305._CB293333875_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-520887519._CB293333839_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['d'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['365844446355'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-672141567._CB292180313_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"8d7fece907dedcaf415730932d50551b2d588871",
"2015-10-18T00%3A26%3A56GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 23584;
generic.days_to_midnight = 0.2729629576206207;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'd']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=365844446355;ord=365844446355?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;ord=365844446355?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;ord=365844446355?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                            <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/falltv/?ref_=nv_tvv_fall_1"
>Fall TV</a></li>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_2"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_3"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_4"
>Top Rated TV Shows</a></li>
                            <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_5"
>Most Popular TV Shows</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_6"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_7"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/family-entertainment-guide/?ref_=nv_sf_feg_1"
>Family Entertainment Guide</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_3"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=10-18&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59112672/?ref_=nv_nw_tn_1"
> Amazonâs IMDb Celebrates Milestone 25th Anniversary â Digital Paradise For Movie Geeks
</a><br />
                        <span class="time">3 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59112378/?ref_=nv_nw_tn_2"
> Box Office: âGoosebumpsâ Wins Friday, Edging Out âCrimson Peak,â âBridge of Spiesâ
</a><br />
                        <span class="time">9 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59111753/?ref_=nv_nw_tn_3"
> âAnabelleâ Sequel In the Works
</a><br />
                        <span class="time">22 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0078748/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjE1OTc3OTAxN15BMl5BanBnXkFtZTcwMDM2NTUyMw@@._V1._SY315_CR30,0,410,315_BT-10_CT10_.jpg",
            titleYears : "1979",
            rank : 52,
                    headline : "Alien"
    },
    nameAd : {
            clickThru : "/name/nm0000375/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTk5MjkxNDYzNV5BMl5BanBnXkFtZTcwNDk2NjU5OA@@._V1._SX750_CR150,70,250,315_CT20_.jpg",
            rank : 154,
            headline : "Robert Downey Jr."
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYhIka781RZBMSOWBCEKrlK_X3CMeLCKAHfmtPGhkGLpGgz5lr-v5yU-WWX5XiedjcZmbZXbG9Q%0D%0ASAlHuDs6VBp0E1AuqLpnA-JGyaKuT8ol-p8Z1gYTiMHcuXW3sE-krUMy8KMACLLgym8kdudBIPHL%0D%0AahsQ30A-S7xXZWmzqpIG2kjoMDwSmyesrK6LhTA0mmJch479udbgv_L2MaPjziyYCg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYq9iBtQYe2cfsmvy4ZDuH4jvgrM_2wYqGAim_WOvIYzFI-te0z2kJDBq-Ji1s9r1_kOs8tHVm9%0D%0AjBH9st_oSHsncIdvTBj7CqMGj1V7NRwJKTLW8I01MsO7NZDkrSgNSWyMNZunscWzUJ9fYguEApJD%0D%0AdVQBhnv9gjlMzbtj94l3TJtwaqBvz5gecQIN1I4JuE6_%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYkBi_OpQmdVP4owX4bvi7gurtNfDolBe-sFDWL_MOe2DwYdXsJKIaLluEvs6nDR9N4TRpjBBKM%0D%0A1dlL8OOhOPXdDhaxF1PMfWQkfjYxRCLVGGSOs9ksAIN-0rcO8q0jRm-JXoEUNb5X7U8pfnq378Aj%0D%0AQrHxU75oBSO-Q-lMZHUw856VcsEVytgQynwFzdRyCm3dipAhKcX9vmJpsLiT-F2s1Q%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=365844446355;ord=365844446355?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=365844446355;ord=365844446355?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi121942809?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi121942809" data-source="bylist" data-id="ls079842544" data-rid="1NJ3NGT9VV7C40XCC6KZ" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="From Spike Lee to Chris Pratt, from Jennifer Connelly to James Franco to Emily Blunt, over a dozen stars take time to celebrate IMDb's 25th anniversary." alt="From Spike Lee to Chris Pratt, from Jennifer Connelly to James Franco to Emily Blunt, over a dozen stars take time to celebrate IMDb's 25th anniversary." src="http://ia.media-imdb.com/images/M/MV5BMTg0MDY2MTgzM15BMl5BanBnXkFtZTgwMTUzMTkxNDE@._V1_SY298_CR123,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0MDY2MTgzM15BMl5BanBnXkFtZTgwMTUzMTkxNDE@._V1_SY298_CR123,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="From Spike Lee to Chris Pratt, from Jennifer Connelly to James Franco to Emily Blunt, over a dozen stars take time to celebrate IMDb's 25th anniversary." title="From Spike Lee to Chris Pratt, from Jennifer Connelly to James Franco to Emily Blunt, over a dozen stars take time to celebrate IMDb's 25th anniversary." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="From Spike Lee to Chris Pratt, from Jennifer Connelly to James Franco to Emily Blunt, over a dozen stars take time to celebrate IMDb's 25th anniversary." title="From Spike Lee to Chris Pratt, from Jennifer Connelly to James Franco to Emily Blunt, over a dozen stars take time to celebrate IMDb's 25th anniversary." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/anniversary/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Happy Birthday, IMDb! </a> </div> </div> <div class="secondary ellipsis"> Celebrity Birthday Greetings </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1044951833?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1044951833" data-source="bylist" data-id="ls079148381" data-rid="1NJ3NGT9VV7C40XCC6KZ" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Last night we celebrated our 25th anniversary at a private industry event in Los Angeles. Attendees included Amazon Original Series cast members Dana Delany and Alona Tal, as well as Peter Fonda, Mira Sorvino, O'Shea Jackson Jr, and Tommy Davidson." alt="Last night we celebrated our 25th anniversary at a private industry event in Los Angeles. Attendees included Amazon Original Series cast members Dana Delany and Alona Tal, as well as Peter Fonda, Mira Sorvino, O'Shea Jackson Jr, and Tommy Davidson." src="http://ia.media-imdb.com/images/M/MV5BODU4MTAzODYwM15BMl5BanBnXkFtZTgwNzA2NTUwNzE@._V1_SY298_CR6,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODU4MTAzODYwM15BMl5BanBnXkFtZTgwNzA2NTUwNzE@._V1_SY298_CR6,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Last night we celebrated our 25th anniversary at a private industry event in Los Angeles. Attendees included Amazon Original Series cast members Dana Delany and Alona Tal, as well as Peter Fonda, Mira Sorvino, O'Shea Jackson Jr, and Tommy Davidson." title="Last night we celebrated our 25th anniversary at a private industry event in Los Angeles. Attendees included Amazon Original Series cast members Dana Delany and Alona Tal, as well as Peter Fonda, Mira Sorvino, O'Shea Jackson Jr, and Tommy Davidson." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Last night we celebrated our 25th anniversary at a private industry event in Los Angeles. Attendees included Amazon Original Series cast members Dana Delany and Alona Tal, as well as Peter Fonda, Mira Sorvino, O'Shea Jackson Jr, and Tommy Davidson." title="Last night we celebrated our 25th anniversary at a private industry event in Los Angeles. Attendees included Amazon Original Series cast members Dana Delany and Alona Tal, as well as Peter Fonda, Mira Sorvino, O'Shea Jackson Jr, and Tommy Davidson." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/anniversary/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > IMDb 25th Anniversary Party </a> </div> </div> <div class="secondary ellipsis"> Go Inside the Party </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2541466393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2541466393" data-source="bylist" data-id="ls079148381" data-rid="1NJ3NGT9VV7C40XCC6KZ" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="While he was promoting 'Moneyball' we asked Chris Pratt to tell us the first movie he saw a movie theater." alt="While he was promoting 'Moneyball' we asked Chris Pratt to tell us the first movie he saw a movie theater." src="http://ia.media-imdb.com/images/M/MV5BNDIwMTg1MDIyM15BMl5BanBnXkFtZTgwODk0NTg1NDE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDIwMTg1MDIyM15BMl5BanBnXkFtZTgwODk0NTg1NDE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="While he was promoting 'Moneyball' we asked Chris Pratt to tell us the first movie he saw a movie theater." title="While he was promoting 'Moneyball' we asked Chris Pratt to tell us the first movie he saw a movie theater." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="While he was promoting 'Moneyball' we asked Chris Pratt to tell us the first movie he saw a movie theater." title="While he was promoting 'Moneyball' we asked Chris Pratt to tell us the first movie he saw a movie theater." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt5070048/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "IMDb Asks" </a> </div> </div> <div class="secondary ellipsis"> With Chris Pratt </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243908742&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/anniversary/25-anniversary-col-letter?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243914842&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_col_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243914842&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>A Letter From Our Founder & CEO, Col Needham</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/25-anniversary-col-letter?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243914842&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_col_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243914842&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ3MzQ0NzUxN15BMl5BanBnXkFtZTgwMjM3NDUwNzE@._V1._CR227,100,900,900_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3MzQ0NzUxN15BMl5BanBnXkFtZTgwMjM3NDUwNzE@._V1._CR227,100,900,900_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <p class="blurb">"My all-time favourite director is <a href="/name/nm0000033/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243914842&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_col_lk1">Alfred Hitchcock</a>, and as well as leaving us with 53 feature films (including my <a href="/title/tt0052357/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243914842&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_col_lk2">all-time favourite film</a> and many other classics), Hitchcock holds the record for the shortest ever Oscar acceptance speech. While accepting the Irving Thalberg Memorial Award at the Oscar ceremony in 1968, he simply said two words: 'Thank you.' Though Hitchcock's appreciation may have been tinged with some irony, mine is not. This is a whole-hearted, grateful 'thank you' that comes on the occasion of IMDb's 25th anniversary. 'Thank you' is mostly what I want to say here on behalf of the whole IMDb team to you, our 250 million monthly worldwide customers."</p> <p class="seemore"> <a href="/anniversary/25-anniversary-col-letter?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243914842&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_col_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243914842&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" > Read more </a> </p></div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Col Needham's Top 25 Lists</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/col-needham-top-25-movies/ls079119294?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sl_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Descendants (2011)" alt="The Descendants (2011)" src="http://ia.media-imdb.com/images/M/MV5BMjAyNTA1MTcyN15BMl5BanBnXkFtZTcwNjEyODczNQ@@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAyNTA1MTcyN15BMl5BanBnXkFtZTcwNjEyODczNQ@@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/anniversary/col-needham-top-25-movies/ls079119294?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sl_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Col Needham's Top 25 Movies from the Last 25 Years </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/col-needham-hidden-gems/ls079014514?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sl_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Good Thief (2002)" alt="The Good Thief (2002)" src="http://ia.media-imdb.com/images/M/MV5BMTY2NDUyNDM5NV5BMl5BanBnXkFtZTYwODEzNTk5._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY2NDUyNDM5NV5BMl5BanBnXkFtZTYwODEzNTk5._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/anniversary/col-needham-hidden-gems/ls079014514?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sl_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Col Needham's Top 25 Hidden Gem Movies from the Last 25 Years </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/col-needham-top-international-movies/ls079014683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sl_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="FortÃ¦l det ikke til nogen (2006)" alt="FortÃ¦l det ikke til nogen (2006)" src="http://ia.media-imdb.com/images/M/MV5BMjA1NDcyMzY1Ml5BMl5BanBnXkFtZTcwNjc1MDY3MQ@@._V1_SY298_CR1,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA1NDcyMzY1Ml5BMl5BanBnXkFtZTcwNjc1MDY3MQ@@._V1_SY298_CR1,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/anniversary/col-needham-top-international-movies/ls079014683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sl_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2242553042&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Col Needham's Top 25 International Movies from the Last 25 Years </a> </div> </div> </div> </div> </div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59112672?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BOTQ0MTk3NTQ4N15BMl5BanBnXkFtZTcwMzA4OTkxOQ@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59112672?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Amazonâs IMDb Celebrates Milestone 25th Anniversary â Digital Paradise For Movie Geeks</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Deadline</a></span>
    </div>
                                </div>
<p>Itâs hard to believe but today, October 17, marks the exact day 25 years ago that IMDb (aka Internet Movie Database, but thatâs not the preferred name for these film geeks) made its debut in the world and changed everything for those who live, breathe and write about film and awards like I do. That...                                        <span class="nobr"><a href="/news/ni59112672?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59112378?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Box Office: âGoosebumpsâ Wins Friday, Edging Out âCrimson Peak,â âBridge of Spiesâ</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59111753?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >âAnabelleâ Sequel In the Works</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59112353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Assassin's Creed movie: Everything you need to know about Michael Fassbender and the cast, spoilers and release date</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000023?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Digital Spy</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59112352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Ford are offering customers a flux capacitor upgrade that's Back To The Future come to life</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000023?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Digital Spy</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59111753?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjM2MTYyMzk1OV5BMl5BanBnXkFtZTgwNDg2MjMyMjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59111753?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >âAnabelleâ Sequel In the Works</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>New Line is moving forward with a sequel to last yearâs horror hit â<a href="/title/tt3322940?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Annabelle</a>â with <a href="/name/nm2477891?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Gary Dauberman</a>Â  returning to write a script.â<a href="/title/tt3322940?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Annabelle</a>,â directed by <a href="/name/nm0502954?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk4">John R. Leonetti</a> with Peter Safaran and <a href="/name/nm1490123?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk5">James Wan</a> producing, generated over $250 million in worldwide grosses on a $6.5 million budget as a ...                                        <span class="nobr"><a href="/news/ni59111753?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59112378?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Box Office: âGoosebumpsâ Wins Friday, Edging Out âCrimson Peak,â âBridge of Spiesâ</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59112353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Assassin's Creed movie: Everything you need to know about Michael Fassbender and the cast, spoilers and release date</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000023?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Digital Spy</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59112352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Ford are offering customers a flux capacitor upgrade that's Back To The Future come to life</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000023?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Digital Spy</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59111734?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >The Criterion Collection Adds âInside Llewyn Davis,' Wim Wendersâ âThe American Friendâ For January 2016 & More</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>The Playlist</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59112413?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk5ODgwNTAwNl5BMl5BanBnXkFtZTgwMDgwMDI2NjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59112413?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Ratings: NBC Comedy âTruth Be Toldâ Opens Quietly; âDr. Kenâ Solid for ABC</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p>ABC was the clear winner in the battle of multi-camera comedy hours leading off Friday night, as its combo of âLast Man Standingâ and rookie â<a href="/title/tt3216608?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Dr. Ken</a>â bested NBCâs second week of âUndateableâ and the premiere of â<a href="/title/tt4481248?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Truth Be Told</a>.âAccording to preliminary national estimates from Nielsen, â<a href="/title/tt4481248?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Truth Be Told</a>...                                        <span class="nobr"><a href="/news/ni59112413?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59112425?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Top TV Ghosts From Ghostbusters, American Horror Story and More</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59112392?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Performer of the Week: Phoebe Tonkin</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59111743?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Fox Buys Vigilante Drama âWatchdogâ From Jason Winer</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59111742?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Autobiographical Comedy From âCooper Barrettâs Guideâ Producer Set At Fox</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59112316?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BODg3MzYwMjE4N15BMl5BanBnXkFtZTcwMjU5NzAzNw@@._V1_SY150_CR10,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59112316?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Angelina Jolie Wears Wet White Dress For Vogue, Looks Sexier Than Ever!</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004699?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>TooFab</a></span>
    </div>
                                </div>
<p>Now this is the <a href="/name/nm0001401?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Angelina Jolie</a> we know and love! Â  The actress covers the November issue of Vogue magazine, where she shows off her gorgeous curves in a new revealing pic from the spread.Â  Photographed by the famed <a href="/name/nm0973425?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Annie Leibovitz</a>, Jolie looks absolutely stunning in the cinematicÂ shot, wearing only...                                        <span class="nobr"><a href="/news/ni59112316?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58936305?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >36 Real People Who Nailed Their Celebrity Halloween Costumes</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59111726?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Did Lindsay Lohan Just Announce She's Running for President in 2020?</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59105099?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >The Latest on Lamar Odom's Condition</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59112546?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Zac Efron -- Extra Fired From 'Neighbors 2' After Posting Fake Weed Pic</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000358?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>TMZ</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/anniversary/anniversary-party-gallery/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb's 25th Anniversary Party Co-Hosted by Amazon Studios and Presented by VISINE &reg;</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/anniversary/anniversary-party-gallery?imageid=rm1648094464&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Mira Sorvino" alt="Mira Sorvino" src="http://ia.media-imdb.com/images/M/MV5BMTUyNDI0MzcwNF5BMl5BanBnXkFtZTgwNTI3NDUwNzE@._V1_SY201_CR42,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUyNDI0MzcwNF5BMl5BanBnXkFtZTgwNTI3NDUwNzE@._V1_SY201_CR42,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/anniversary/anniversary-party-gallery?imageid=rm3996970240&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Gage Golightly" alt="Gage Golightly" src="http://ia.media-imdb.com/images/M/MV5BODg3NjU0NTQyOF5BMl5BanBnXkFtZTgwMjE2NTUwNzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODg3NjU0NTQyOF5BMl5BanBnXkFtZTgwMjE2NTUwNzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/anniversary/anniversary-party-gallery?imageid=rm1681648896&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTY3NDkxNzgyOV5BMl5BanBnXkFtZTgwMzI3NDUwNzE@._V1_SY201_CR49,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY3NDkxNzgyOV5BMl5BanBnXkFtZTgwMzI3NDUwNzE@._V1_SY201_CR49,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Thursday night we celebrated our 25th anniversary at a private industry event in Los Angeles. Attendees included Amazon Original Series cast members Dana Delany and Alona Tal, as well as Peter Fonda, Mira Sorvino, O'Shea Jackson Jr, and Tommy Davidson. Check out photos from the festivities!</p> <p class="seemore"> <a href="http://www.imdb.com/anniversary/anniversary-party-gallery/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243916802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse photos from the event </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=10-17&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0428065?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Felicity Jones" alt="Felicity Jones" src="http://ia.media-imdb.com/images/M/MV5BMjI2NDMwNTk0N15BMl5BanBnXkFtZTcwNjI5Njg5OA@@._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI2NDMwNTk0N15BMl5BanBnXkFtZTcwNjI5Njg5OA@@._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0428065?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Felicity Jones</a> (32) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0532193?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Matthew Macfadyen" alt="Matthew Macfadyen" src="http://ia.media-imdb.com/images/M/MV5BMTY1ODExNzU0OF5BMl5BanBnXkFtZTcwOTkzNTg1OA@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY1ODExNzU0OF5BMl5BanBnXkFtZTcwOTkzNTg1OA@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0532193?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Matthew Macfadyen</a> (41) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005172?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Norm MacDonald" alt="Norm MacDonald" src="http://ia.media-imdb.com/images/M/MV5BMTMyMzg1NDg5OF5BMl5BanBnXkFtZTYwMDAyODU3._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMyMzg1NDg5OF5BMl5BanBnXkFtZTYwMDAyODU3._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005172?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Norm MacDonald</a> (52) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0004896?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Eminem" alt="Eminem" src="http://ia.media-imdb.com/images/M/MV5BMjI0MDAxMDYwOV5BMl5BanBnXkFtZTcwMTQ1MjE2Mw@@._V1_SY172_CR5,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0MDAxMDYwOV5BMl5BanBnXkFtZTcwMTQ1MjE2Mw@@._V1_SY172_CR5,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0004896?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Eminem</a> (43) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1796057?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Max Irons" alt="Max Irons" src="http://ia.media-imdb.com/images/M/MV5BMTY5MDM1MTkwN15BMl5BanBnXkFtZTcwNDg2Mzk3NA@@._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY5MDM1MTkwN15BMl5BanBnXkFtZTcwNDg2Mzk3NA@@._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1796057?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Max Irons</a> (30) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=10-17&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/anniversary/movies-turning-25-in-2015?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_ph_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Movies That Turn 25 in 2015</h3> </a> </span> </span> <p class="blurb">In honor of our 25th anniversary, check out our photo gallery of posters of movies that turn 25 in 2015.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/movies-turning-25-in-2015?imageid=rm2471023616&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_ph_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ghost (1990)" alt="Ghost (1990)" src="http://ia.media-imdb.com/images/M/MV5BMTU0NzQzODUzNl5BMl5BanBnXkFtZTgwMjc5NTYxMTE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0NzQzODUzNl5BMl5BanBnXkFtZTgwMjc5NTYxMTE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/movies-turning-25-in-2015?imageid=rm3447182336&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_ph_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Goodfellas (1990)" alt="Goodfellas (1990)" src="http://ia.media-imdb.com/images/M/MV5BMTY2OTE5MzQ3MV5BMl5BanBnXkFtZTgwMTY2NTYxMTE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY2OTE5MzQ3MV5BMl5BanBnXkFtZTgwMTY2NTYxMTE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/movies-turning-25-in-2015?imageid=rm2467678208&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_ph_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Wild at Heart (1990)" alt="Wild at Heart (1990)" src="http://ia.media-imdb.com/images/M/MV5BMjM5Mjg4MTgxNV5BMl5BanBnXkFtZTgwNTg3MjA2MTE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM5Mjg4MTgxNV5BMl5BanBnXkFtZTgwNTg3MjA2MTE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/anniversary/movies-turning-25-in-2015?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_ph_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898802&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See full gallery </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/renewed-canceled-and-on-the-bubble?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_rcb_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>2015-16 TV Season: Renewed, Canceled and on the Bubble</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/renewed-canceled-and-on-the-bubble?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_rcb_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage#1" > <img itemprop="image" class="pri_image" title="America's Next Top Model (2003-)" alt="America's Next Top Model (2003-)" src="http://ia.media-imdb.com/images/M/MV5BMjI1NzkzODcwNF5BMl5BanBnXkFtZTgwMTM5MzEwNzE@._V1_SY201_CR33,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI1NzkzODcwNF5BMl5BanBnXkFtZTgwMTM5MzEwNzE@._V1_SY201_CR33,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/renewed-canceled-and-on-the-bubble?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_rcb_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage#15" > <img itemprop="image" class="pri_image" title="Zoo (2015-)" alt="Zoo (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTY2NjY3OTY3OF5BMl5BanBnXkFtZTgwNjE3MDk1NjE@._V1_SY201_CR48,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY2NjY3OTY3OF5BMl5BanBnXkFtZTgwNjE3MDk1NjE@._V1_SY201_CR48,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/renewed-canceled-and-on-the-bubble?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_rcb_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage#5" > <img itemprop="image" class="pri_image" title="Dominion (2014-)" alt="Dominion (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMTc3MzgyOTk2NF5BMl5BanBnXkFtZTgwODk4OTUzNjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc3MzgyOTk2NF5BMl5BanBnXkFtZTgwODk4OTUzNjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Want to know which of your favorite shows will survive? We're keeping track of this season's broadcast, cable, and streaming renewals and cancellations.</p> <p class="seemore"> <a href="/falltv/renewed-canceled-and-on-the-bubble?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_rcb_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243899142&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Read more </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-9"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Indie Focus: 'Woodlawn' - Official Trailer</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt4183692/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898982&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898982&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Woodlawn (2015)" alt="Woodlawn (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTA0ODM5MTM5MTleQTJeQWpwZ15BbWU4MDAwNzk1NDYx._V1_SY250_CR5,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA0ODM5MTM5MTleQTJeQWpwZ15BbWU4MDAwNzk1NDYx._V1_SY250_CR5,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi860271385?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898982&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243898982&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi860271385" data-rid="1NJ3NGT9VV7C40XCC6KZ" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Woodlawn (2015)" alt="Woodlawn (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUzMDk1NDQ3OV5BMl5BanBnXkFtZTgwMjQ5OTU0NjE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzMDk1NDQ3OV5BMl5BanBnXkFtZTgwMjQ5OTU0NjE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Woodlawn (2015)" title="Woodlawn (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Woodlawn (2015)" title="Woodlawn (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">A gifted high school football player must learn to embrace his talent and his faith as he battles racial tensions on and off the field.</p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt3136112/trivia?item=tr2270679&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3136112?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Shelter (2014)" alt="Shelter (2014)" src="http://ia.media-imdb.com/images/M/MV5BMzEwNTc2NjIwNV5BMl5BanBnXkFtZTgwMjYyMTk4NjE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzEwNTc2NjIwNV5BMl5BanBnXkFtZTgwMjYyMTk4NjE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt3136112?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Shelter</a></strong> <p class="blurb">This is the first film Paul Bettany directed.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt3136112/trivia?item=tr2270679&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-25"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;iov7Wr6aMwU&quot;}">
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/iov7Wr6aMwU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Surprising titles of the Top 250</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/iov7Wr6aMwU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="ÐÑÐ¹Ð±: Ð§ÐµÑÐ²ÐµÑÐ¾Ð½Ð¾Ð³Ð¸Ð¹ Ð¼Ð°Ð»ÑÑ (1995)" alt="ÐÑÐ¹Ð±: Ð§ÐµÑÐ²ÐµÑÐ¾Ð½Ð¾Ð³Ð¸Ð¹ Ð¼Ð°Ð»ÑÑ (1995)" src="http://ia.media-imdb.com/images/M/MV5BMTIwNzY2OTIzN15BMl5BanBnXkFtZTcwNjcxODAzMQ@@._V1_SY207_CR4,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTIwNzY2OTIzN15BMl5BanBnXkFtZTcwNjcxODAzMQ@@._V1_SY207_CR4,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/iov7Wr6aMwU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Romeo + Juliet (1996)" alt="Romeo + Juliet (1996)" src="http://ia.media-imdb.com/images/M/MV5BMTI2ODc3NDI4OV5BMl5BanBnXkFtZTYwMTY1MjQ5._V1._CR1,1,341,448_SY207_CR9,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI2ODc3NDI4OV5BMl5BanBnXkFtZTYwMTY1MjQ5._V1._CR1,1,341,448_SY207_CR9,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/iov7Wr6aMwU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="That Thing You Do! (1996)" alt="That Thing You Do! (1996)" src="http://ia.media-imdb.com/images/M/MV5BMTczMzM2MTM0NF5BMl5BanBnXkFtZTcwMTIwMDQ0MQ@@._V1_SY207_CR6,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTczMzM2MTM0NF5BMl5BanBnXkFtZTcwMTIwMDQ0MQ@@._V1_SY207_CR6,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/iov7Wr6aMwU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Empire Records (1995)" alt="Empire Records (1995)" src="http://ia.media-imdb.com/images/M/MV5BMjE5NzgxODA2OV5BMl5BanBnXkFtZTcwMDE4NjUxMQ@@._V1_SY207_CR1,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE5NzgxODA2OV5BMl5BanBnXkFtZTcwMDE4NjUxMQ@@._V1_SY207_CR1,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/iov7Wr6aMwU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ransom (1996)" alt="Ransom (1996)" src="http://ia.media-imdb.com/images/M/MV5BMjEzMzE2NjY0NF5BMl5BanBnXkFtZTcwMzU2MDUyMQ@@._V1_SY207_CR3,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzMzE2NjY0NF5BMl5BanBnXkFtZTcwMzU2MDUyMQ@@._V1_SY207_CR3,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">You may find it surprising, but all the following titles were present at least once in the IMDb Top 250. Which movie surprises you the most? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/248816710?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"> <a href="/poll/iov7Wr6aMwU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243909102&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=365844446355;ord=365844446355?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;ord=365844446355?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;ord=365844446355?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2554274"></div> <div class="title"> <a href="/title/tt2554274?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Crimson Peak </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2554274?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1051904"></div> <div class="title"> <a href="/title/tt1051904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Goosebumps </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3682448"></div> <div class="title"> <a href="/title/tt3682448?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Bridge of Spies </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3859076"></div> <div class="title"> <a href="/title/tt3859076?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Truth </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3170832"></div> <div class="title"> <a href="/title/tt3170832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Room </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3272570"></div> <div class="title"> <a href="/title/tt3272570?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> All Things Must Pass: The Rise and Fall of Tower Records </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1870548"></div> <div class="title"> <a href="/title/tt1870548?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> This Changes Everything </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4183692"></div> <div class="title"> <a href="/title/tt4183692?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Woodlawn </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3181776"></div> <div class="title"> <a href="/title/tt3181776?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t8"> Momentum </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243262022&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3659388"></div> <div class="title"> <a href="/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Martian </a> <span class="secondary-text">$37.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2510894"></div> <div class="title"> <a href="/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Hotel Transylvania 2 </a> <span class="secondary-text">$20.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3332064"></div> <div class="title"> <a href="/title/tt3332064?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Pan </a> <span class="secondary-text">$15.3M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2361509"></div> <div class="title"> <a href="/title/tt2361509?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> The Intern </a> <span class="secondary-text">$8.7M</span> </div> <div class="action"> <a href="/showtimes/title/tt2361509?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3397884"></div> <div class="title"> <a href="/title/tt3397884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Sicario </a> <span class="secondary-text">$7.6M</span> </div> <div class="action"> <a href="/showtimes/title/tt3397884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2080374"></div> <div class="title"> <a href="/title/tt2080374?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Steve Jobs </a> <span class="secondary-text"></span> </div> <div class="action"> Nationwide Expansion </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2473510"></div> <div class="title"> <a href="/title/tt2473510?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Paranormal Activity: The Ghost Dimension </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3614530"></div> <div class="title"> <a href="/title/tt3614530?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Jem and the Holograms </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1618442"></div> <div class="title"> <a href="/title/tt1618442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> The Last Witch Hunter </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3164256"></div> <div class="title"> <a href="/title/tt3164256?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Rock the Kasbah </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/anniversary/top-25-movies/ls079342176?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243913582&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243913582&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Top 25 Movies From the Last 25 Years</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/top-25-movies/ls079342176?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243913582&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243913582&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Inception (2010)" alt="Inception (2010)" src="http://ia.media-imdb.com/images/M/MV5BMTI3ODA1ODkwN15BMl5BanBnXkFtZTcwODQ0OTk1Mw@@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI3ODA1ODkwN15BMl5BanBnXkFtZTcwODQ0OTk1Mw@@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Here's a rundown of the top 25 movies by year from 1990 to 2014 according to all-time user ratings.</p> <p class="seemore"> <a href="/anniversary/top-25-movies/ls079342176?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243913582&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2243913582&pf_rd_r=1NJ3NGT9VV7C40XCC6KZ&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See the full list </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYkXF6P4lh1JMUw_3oxTg_d9keNXCqOLdweVQzCuy_5Ki9rsCIMglwG-MdNLJoxJk1vLWGnSVvi%0D%0A6OR0QWWGgIQJDECVZo1gNh4loLc0Rs5tz-TZi6MIYC_rRRtq4paJJ4knI1V9QyKmZbCqxZlnZvL8%0D%0AHJIrwoc5LWoiLF4_q5zRUCosfIE_VvGBd05QT1oEfqutusP_HjgUGj8LMU5qNJVaXy2syIo5HIea%0D%0AeXZ4VDTUX8Q%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYjxN0M3QApTwul0OtbljGFeFiec59i1qVQ3HpHynoiCeDq1EVVYbeiar1BUjlYhxHMMULBq3KR%0D%0AtNn29lVGPBWqs5zzqx8W34OXCoWrliV_e8NUJ5g9_TrsysK0cuhi2fqgRSt0v7QBiBdfAzF7iAox%0D%0AedSkpU0OksCOIGazsRSBDc2Fl1ouCBRnC76QYYCGzwstU75LW1mv2N6DC73AjTg1ig%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYhBHNXQlLohYbf1Zq312QNotCEvDmyJjZJ25pBtcTWWbwJNkK-zyKEyJnz_YRtOSa-2-_fqgwP%0D%0AJ4mX5roK_O_osJq9JbPnYn06NWau5B04PgRjqNmK_BWuQ-6LOMQCZYlkLG0de5t4IBGTCByWb3Ng%0D%0AVPQEeJUfaAoCHxqv1jjXrefIEhRx6sWJS6ea3W92t6BgVitiSAj5P22ewxguNGLw_bgCnUIUa_fA%0D%0As-0IhNaZ38wr_K3O8NrNS8PsNf5bvn_AIS8lIVWoRVtzLcmEgQ8kFVZNIgccHz2VRafA-hACtWFO%0D%0Anq2AM6a9RJWdHVt7IYoNo8isZegyYnIg2bNFhmjK6PwRJl4XNUKKojOuNgz3jn3tsieQSa44_B3F%0D%0Av5EabioyL923p_Wu_iaBOtvkBj--gg%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYthpTf896dNRyDnTOfPb_3IUhFWEe1i9m8gXEvMPuy7IzhvjRcJrF6NB8CAYJvv7WWs1kar6I0%0D%0AtlBgg3vN76gI2CjUuQT3vGweQT2PilKLukp4Bxc7v3xy7aKxlLJfoZ80_SqL1HnLpAfxE4_h3peu%0D%0AtG8iZJ0EAqMIDf2X-wox0epyvy5iK9VtWLf-BV0oM7Wp8rQWNnjwv6Kv6reC7i95eA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYsw4Lv4JHM7hWil8kxn7AqJOAv6E8rRFQGJOMh-cm94s594h89_H-CmlyHBTngXcWDXY2Ejah6%0D%0AyCv3F34duY6Jg2MEMttqN-jirPcRY9FUmsNwyUe2VhLAbM0WE_rIh8Iwpexd5dyD886TpwWU5RgP%0D%0AqBs6ruCdnKM4gwUvCyXtANBVDvYmupFPHJINjepYDaOt%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYoxN1R7LkB5JYLLuZ1PjbWMsWaKtH6rapl-GTo-bOhE1zPlZb7ro0gkEzBBlzdzl2-WpphHntN%0D%0A1a-JQxhCsjceWkZJyS1JcoHSCg8E7rIQuNW2Kc0wbAtAoqJrz6yq0p_9z_NwYhxb2Vf-OJIcr76U%0D%0AFlGY9j6BYQLEOn8rqxdiIKw6W-V1XA8-q6accyeUbCWLfghwvsf81nal1ZxZHZEntQ%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYojw3zDQSZNLGU4rPxRMKvKo-A5QYJhH8geBfOV00NLRLXpa3LNORLnDoCbx3QeIf0hSwWJK1W%0D%0ASKKIYL_FYoqM08iGXwE3bEeQknL_yKuwYcq10coXtGaygaHp6Di6XdWB3wwZgpaTfoQAnl75UJpN%0D%0A28-TCidGCbl1VbsI-Wb9AxfK8XxnyJdN2IJEeCDJOBRnX89sW8pOEpQW0QQoup_E3LhuqXkIWe-8%0D%0A-gCH_A9A4JkzoXPslC_ti_30O6YSiZvEw2ltOJAhYJVNLSyJr96_-Q%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYlAqQ2hg1N5NJG6vHXTm90bBYby1vbWo1nTrSUvLud5KyBiZX2ZgujVTaWrZtIhDjG4yPEXk8m%0D%0AudAElRm3j_jxL5QnRbvWeujesiFW-95vzKGApN8YQyoLfr-a8I2eQeZCUNupi_A7dsM55JDBMCIR%0D%0AQHom-FzgVHUKVIyvBZuyniNyuagiLnr25_F8ba2sD8JBLSMxGlCSf_V6M0H29FzotA%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYiue4IMfCwdN5R4L4vOIrFxqPtfux9lxInhZUgQgLaxVu1vUJzhqqojwyd4uDz_VaVxltvan08%0D%0AseFCGwS0Kbh_Eb_g3wYK08HsbFW7hlnP2FCT1ye1OSg7LY2Sek3_G9ZDDI60qG1_jap6zVIpao85%0D%0AuanmITw8bnkpeZHbbeNq9ABqrmWZZ_IzWxUH3VLpL9dD%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYus_5hcg0lSNnsejyz41OKWAtpu6JOLGD4HMqkKU8X6SQtCoI8Q4HkdwyN1u_Tff6gWDdiNPt9%0D%0AcfinBGL1jyROOVZEjK3S3dMDlZNO0Gvl00g7PfUhOj1azY_-1WIvZYUC6lZ5oEWwluiBqJ8-ZkSy%0D%0Awf-ueZBLEOt5DejWC9MlrAGqPrCMrb6d34LC9dM8YCeiD_65lS0tqfTS9y9llqQLQUd94cOrLnvx%0D%0Ae_WZm11IUlppl1nrnsI4eTOGYQ9kdo0r%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYgNZj25ZX9S9q0kFbD-OU2cvFvvz5nnXbNYJaEQ8r_Ze275MVlvZX_TgRH1HfQU8qN8R-hqkaw%0D%0AUL6GvPl9NF0LGJN2abBB86sbHMiKcR1o2L3sL-AiNvQafz35dxiHzyp33s1gQZGJmSlUQbcNPBBY%0D%0AvimGzKC12iN0rr8ACuV1KCXzC6SegkO7z4O_yy90cKqiVp2rHjYRvzXtWgC9S5HD837s6bpnwHnL%0D%0APMDlr7qhk60%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYmhnyYGYVm8WWzNqCaXDOTFvVG-njEiJAyVHJnXgzDxINxNCzwilhnCd8VTp5ADJ_o4QkOSck2%0D%0AHRR0AU8CJ9RyBkSyciLnrT6Us7tVe5YaNFo5HTmSqC7mNHg5IAcfQlOsoRSXQfRcrF2N93jIg4e8%0D%0As6iWdb29iM3M2KBvpibkWANR4K1NNtcY5PEsF8Ita8bQqFsuDR-brK0yqWmiaXufyx8U_XJAbdcX%0D%0ASRfM9oYT_UQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYg9k9zmffLsPZSsQLtvVDXIkYh51vSab233a6gneRJarLrVeF6_OO6TRf2XO4Gk9OFSLtK6-D9%0D%0AcPN5PY2Mn5aQFyOkEDTqZNdxKdUNACmwWtfKtOryn0Ze-UemN1ybJv8N-2hLylPV2xtwtVg03xky%0D%0AS7pPzgC9sY3a-67VJ_RHNZon3uEkqXA4HfvqRdfFxfn8wofBMivMXR_ddLM4tUaqWADkFNg0_DiM%0D%0AB0IJFN3M08g%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYvZ2W_sNb4dzS215H07UOAgPLSJ7-XPVl1N66di7342T8BgTS2Oev7V9ZI8kuUGWhPmQhwnj3E%0D%0AFWKHwTDJ596QD1k4QalUU2pQtJvut-CScD8yHOSPkiqN_uApPlkxj04aS2abbIoThBbQoLMgP5mS%0D%0AiagS1z4x3HP1lf1gLtyZ29WNfnkZR5n82TtzBOLChTXl5-LUlPXZaa83FvdYUIEDS88fsKyGxJCP%0D%0AvhQEWxGwZQc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYleB8ybVj0RAUKt_b5qPRaUWQ5yfiWfB7plEkjkz3QMfVPFy-JqOus6vWtLCOud7PN-wtal_MN%0D%0Au4sGLeCe3tYoYEqiE2WijmxIOyauXcCIqqlo6JiAUCnHW8KNY-sayzaXj2Ccrl6dNufFlBMTiIjY%0D%0ASBTj75qsY1KZepoBNkZzO-EgzAF6cuwH0soxTk1Cs0ceJcdc3y2Jgx43rZkztM0SzafmdtEriZa_%0D%0AwdYZMt99fDk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYqUfkhzt4u361YGgFSlm0a2t2uuelsBD5IiORdy4BwSpqvVzejRl9tI3yGkXDasIgkxn1eLGBu%0D%0A-VU-ZP1qrrDOfTydSWJmt8_0hVUAaukKqfRuX8l8Zw6_oXI5uhjBYvvJG7Yx4t-TThJhZgGVnHhL%0D%0AX7qYWdbLzpxXdh-fBoulU8tj4vGgkIqZHjsCUWk-nb5fI2T9KrH-vG16G3NqLvY_44pOK40ycTuF%0D%0AVapvjojMNmD1jYm6nQjLNKSt8CJSCRjR%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYglDnAehMIYmpNiBaISgMoo2AWc-xC9pE-vn8mfc8pxZfFXvrxSoPBkEuN3G-3j6DCzr8Jgtep%0D%0A3amynBNYQYl9DfkzLjtwQe6A9N3X4QGbmm-PeJtURVYMllh8iXtKxk-9YbPs9qxP1bBS8bPmg0x3%0D%0A_rylCjEcHPbtO4bisuyBX-s%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYm-VIgtY-p4C1_aZPYNPcWU1EXyelrUksrY7NCQ3knNK__EKQS27ucarIYN36nQR9g--5Rf0bC%0D%0A2SoEfGJQCPbEpn7XGbCzG0bvFISu4Gl0P1nt-4UwRN9Tzfz9YC718BzD1y-oyLkdIrtQjsxYCBRH%0D%0AAq_1uS9NKL7KASJK-tsJkKk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-2313516719._CB292181657_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-4011021425._CB290592603_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1163490387._CB290592593_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3920146857._CB292796746_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-3229803205._CB292800882_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=010183d6f1aaf806b142239a392eba827749e0339dbeef6810247f3320578ff9351e",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=365844446355"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=365844446355&ord=365844446355";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="626"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
