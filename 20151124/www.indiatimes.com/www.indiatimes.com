<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.6" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.6" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.6"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.6"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.6"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.6"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.6"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle">
				</span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-11-25 00:00:05-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             10 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/this-24-year-old-delhi-guy-who-adopted-and-developed-a-tribal-village-in-karnataka-is-an-inspiration-for-entire-young-india-247662.html" class=" tint" title="This 24-Year-Old Delhi Guy Who Uplifted An Entire Tribal Village In Karnataka Is An Inspiration For Entire Young India">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/bglr-cg-card_1448353931_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/bglr-cg-card_1448353931_236x111.jpg"  border="0" alt="This 24-Year-Old Delhi Guy Who Uplifted An Entire Tribal Village In Karnataka Is An Inspiration For Entire Young India"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-24-year-old-delhi-guy-who-adopted-and-developed-a-tribal-village-in-karnataka-is-an-inspiration-for-entire-young-india-247662.html" title="This 24-Year-Old Delhi Guy Who Uplifted An Entire Tribal Village In Karnataka Is An Inspiration For Entire Young India">
                            This 24-Year-Old Delhi Guy Who Uplifted An Entire Tribal Village In Karnataka Is An Inspiration For Entire Young India                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            4 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/an-aamir-khan-fan-tells-us-why-his-recent-comments-hurt-her-more-than-an-intolerant-india-247683.html" title="An Aamir Khan Fan Tells Us Why His Recent Comments Hurt Her More Than An Intolerant India" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/opo_1448373759_1448373769_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/opo_1448373759_1448373769_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/an-aamir-khan-fan-tells-us-why-his-recent-comments-hurt-her-more-than-an-intolerant-india-247683.html" title="An Aamir Khan Fan Tells Us Why His Recent Comments Hurt Her More Than An Intolerant India">
                            An Aamir Khan Fan Tells Us Why His Recent Comments Hurt Her More Than An Intolerant India                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/aamir-khan-s-recent-comments-wreck-havoc-on-snapdeal-bhakts-uninstall-apps-and-launch-appwapsi-247681.html" title="Aamir Khan's Recent Comments Wreck Havoc On Snapdeal. Many Uninstall Apps, Launch #AppWapsi" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/anp_1448371546_1448371558_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/anp_1448371546_1448371558_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/aamir-khan-s-recent-comments-wreck-havoc-on-snapdeal-bhakts-uninstall-apps-and-launch-appwapsi-247681.html" title="Aamir Khan's Recent Comments Wreck Havoc On Snapdeal. Many Uninstall Apps, Launch #AppWapsi">
                            Aamir Khan's Recent Comments Wreck Havoc On Snapdeal. Many Uninstall Apps, Launch #AppWapsi                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            6 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/meet-lucy-our-oldest-ape-ancestor-who-walked-the-earth-3-2-million-years-ago-247673.html" title="Meet Lucy, Our Oldest Ape-Ancestor, Who Walked The Earth 3.2 Million Years Ago!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/ape502_1448365193_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/ape502_1448365193_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/meet-lucy-our-oldest-ape-ancestor-who-walked-the-earth-3-2-million-years-ago-247673.html" title="Meet Lucy, Our Oldest Ape-Ancestor, Who Walked The Earth 3.2 Million Years Ago!">
                            Meet Lucy, Our Oldest Ape-Ancestor, Who Walked The Earth 3.2 Million Years Ago!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            6 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/india-is-one-of-the-countries-worst-affected-by-weather-related-disasters-says-un-study-247677.html" title="India Is One Of The Countries Worst Affected By Weather Related Disasters Says UN Study" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/kashmir-floods-card_1448368312_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/kashmir-floods-card_1448368312_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/india-is-one-of-the-countries-worst-affected-by-weather-related-disasters-says-un-study-247677.html" title="India Is One Of The Countries Worst Affected By Weather Related Disasters Says UN Study">
                            India Is One Of The Countries Worst Affected By Weather Related Disasters Says UN Study                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

            

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/beyonce-dancing-to-pinga-is-the-best-mashup-of-the-year-no-questions-asked-247679.html'>video</a>                           
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/beyonce-dancing-to-pinga-is-the-best-mashup-of-the-year-no-questions-asked-247679.html" class="tint" title="Beyonce Dancing To 'Pinga' Is The Best Mashup Of The Year. No Questions Asked" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/beyonce-dancing-to-pinga-is-the-best-mashup-of-the-year-no-questions-asked-247679.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/pinga-card_1448369366_1448369377_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/pinga-card_1448369366_1448369377_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/beyonce-dancing-to-pinga-is-the-best-mashup-of-the-year-no-questions-asked-247679.html" title="Beyonce Dancing To 'Pinga' Is The Best Mashup Of The Year. No Questions Asked" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/beyonce-dancing-to-pinga-is-the-best-mashup-of-the-year-no-questions-asked-247679.html');">
                            Beyonce Dancing To 'Pinga' Is The Best Mashup Of The Year. No Questions Asked                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/6-foods-you-should-avoid-during-winters-to-avoid-congestion-247650.html" class="tint" title="6 Foods You Should Avoid During Winters To Avoid Congestion" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/6-foods-you-should-avoid-during-winters-to-avoid-congestion-247650.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/card-1_1448350503_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card-1_1448350503_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/6-foods-you-should-avoid-during-winters-to-avoid-congestion-247650.html" title="6 Foods You Should Avoid During Winters To Avoid Congestion" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/6-foods-you-should-avoid-during-winters-to-avoid-congestion-247650.html');">
                            6 Foods You Should Avoid During Winters To Avoid Congestion                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/this-song-is-an-ode-to-all-those-who-couldn-t-muster-the-courage-to-confess-their-love-247680.html'>video</a>                           
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/this-song-is-an-ode-to-all-those-who-couldn-t-muster-the-courage-to-confess-their-love-247680.html" class="tint" title="This Song Is An Ode To All Those Who Couldn't Muster The Courage To Confess Their Love" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/this-song-is-an-ode-to-all-those-who-couldn-t-muster-the-courage-to-confess-their-love-247680.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/weer_1448369216_1448369223_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/weer_1448369216_1448369223_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/this-song-is-an-ode-to-all-those-who-couldn-t-muster-the-courage-to-confess-their-love-247680.html" title="This Song Is An Ode To All Those Who Couldn't Muster The Courage To Confess Their Love" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/this-song-is-an-ode-to-all-those-who-couldn-t-muster-the-courage-to-confess-their-love-247680.html');">
                            This Song Is An Ode To All Those Who Couldn't Muster The Courage To Confess Their Love                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/this-song-is-an-ode-to-all-those-who-couldn-t-muster-the-courage-to-confess-their-love-247680.html'>video</a>                        <a href="http://www.indiatimes.com/entertainment/this-song-is-an-ode-to-all-those-who-couldn-t-muster-the-courage-to-confess-their-love-247680.html" class="tint" title="This Song Is An Ode To All Those Who Couldn't Muster The Courage To Confess Their Love">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/weer_1448369216_1448369223_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/weer_1448369216_1448369223_218x102.jpg" border="0" alt="This Song Is An Ode To All Those Who Couldn't Muster The Courage To Confess Their Love"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/this-song-is-an-ode-to-all-those-who-couldn-t-muster-the-courage-to-confess-their-love-247680.html" title="This Song Is An Ode To All Those Who Couldn't Muster The Courage To Confess Their Love">
                            This Song Is An Ode To All Those Who Couldn't Muster The Courage To Confess Their Love                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/dear-aamir-when-did-incredible-india-become-intolerant-india-for-you-asks-anupam-kher-247642.html" class="tint" title="Dear Aamir, When Did âIncredible Indiaâ Become âIntolerant Indiaâ For You, Asks Anupam Kher!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/card123_1448341604_1448341611_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card123_1448341604_1448341611_218x102.jpg" border="0" alt="Dear Aamir, When Did âIncredible Indiaâ Become âIntolerant Indiaâ For You, Asks Anupam Kher!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/dear-aamir-when-did-incredible-india-become-intolerant-india-for-you-asks-anupam-kher-247642.html" title="Dear Aamir, When Did âIncredible Indiaâ Become âIntolerant Indiaâ For You, Asks Anupam Kher!">
                            Dear Aamir, When Did âIncredible Indiaâ Become âIntolerant Indiaâ For You, Asks Anupam Kher!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/a-lake-in-bengaluru-is-so-full-of-toxic-foam-that-it-is-now-spilling-into-the-streets-247659.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/a-lake-in-bengaluru-is-so-full-of-toxic-foam-that-it-is-now-spilling-into-the-streets-247659.html" class="tint" title="A Lake In Bengaluru Is So Full Of Toxic Foam That It Is Now Spilling Into The Streets!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/bengaluru_1448351087_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/bengaluru_1448351087_218x102.jpg" border="0" alt="A Lake In Bengaluru Is So Full Of Toxic Foam That It Is Now Spilling Into The Streets!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/a-lake-in-bengaluru-is-so-full-of-toxic-foam-that-it-is-now-spilling-into-the-streets-247659.html" title="A Lake In Bengaluru Is So Full Of Toxic Foam That It Is Now Spilling Into The Streets!">
                            A Lake In Bengaluru Is So Full Of Toxic Foam That It Is Now Spilling Into The Streets!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/travel/there-are-musical-highways-around-the-world-that-play-melodies-when-cars-drive-on-them-247516.html" class="tint" title="There Are Musical Highways Around The World That Play Melodies When Cars Drive On Them!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/musicalroad_1447944791_1447944802_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/musicalroad_1447944791_1447944802_218x102.jpg" border="0" alt="There Are Musical Highways Around The World That Play Melodies When Cars Drive On Them!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/there-are-musical-highways-around-the-world-that-play-melodies-when-cars-drive-on-them-247516.html" title="There Are Musical Highways Around The World That Play Melodies When Cars Drive On Them!">
                            There Are Musical Highways Around The World That Play Melodies When Cars Drive On Them!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/11-things-that-say-there-is-a-doctor-inside-all-indians-247486.html" class="tint" title="11 Things That Say There Is A Doctor Inside All Indians">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/card_1447929116_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card_1447929116_218x102.jpg" border="0" alt="11 Things That Say There Is A Doctor Inside All Indians"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-things-that-say-there-is-a-doctor-inside-all-indians-247486.html" title="11 Things That Say There Is A Doctor Inside All Indians">
                            11 Things That Say There Is A Doctor Inside All Indians                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            6 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/india-may-soon-be-part-of-an-exclusive-nuclear-trading-nations-club-move-may-escalate-tensions-in-region-247676.html" title="India May Soon Be Part Of An Exclusive Nuclear Trading Nations Club, Move May Escalate Tensions In Region" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/moo_1448367664_1448367675_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/india-may-soon-be-part-of-an-exclusive-nuclear-trading-nations-club-move-may-escalate-tensions-in-region-247676.html" title="India May Soon Be Part Of An Exclusive Nuclear Trading Nations Club, Move May Escalate Tensions In Region">
                            India May Soon Be Part Of An Exclusive Nuclear Trading Nations Club, Move May Escalate Tensions In Region                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            6 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/no-space-for-congress-bhu-political-science-department-not-to-have-images-of-nehru-indira-on-its-wall-247675.html" title="No Space For Congress? BHU Political Science Department Not To Have Images Of Nehru, Indira On Its Wall" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/3005bhu-card_1448367002_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/no-space-for-congress-bhu-political-science-department-not-to-have-images-of-nehru-indira-on-its-wall-247675.html" title="No Space For Congress? BHU Political Science Department Not To Have Images Of Nehru, Indira On Its Wall">
                            No Space For Congress? BHU Political Science Department Not To Have Images Of Nehru, Indira On Its Wall                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            6 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/sports/sakshi-dhoni-s-latest-photos-of-baby-ziva-are-the-absolute-cutest-247670.html" title="Sakshi Dhoni's Latest Photos Of Baby Ziva Are The Absolute Cutest" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/zivadhoni_1448361311_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/sakshi-dhoni-s-latest-photos-of-baby-ziva-are-the-absolute-cutest-247670.html" title="Sakshi Dhoni's Latest Photos Of Baby Ziva Are The Absolute Cutest">
                            Sakshi Dhoni's Latest Photos Of Baby Ziva Are The Absolute Cutest                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/aib-s-new-website-promises-to-take-you-on-a-trip-so-perfect-it-will-blow-you-away-247665.html" title="AIB's New Website Promises To Take You On A Trip So Perfect, It Will Just 'Blow You Away'" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/team-aib502_1448360073_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/aib-s-new-website-promises-to-take-you-on-a-trip-so-perfect-it-will-blow-you-away-247665.html" title="AIB's New Website Promises To Take You On A Trip So Perfect, It Will Just 'Blow You Away'">
                            AIB's New Website Promises To Take You On A Trip So Perfect, It Will Just 'Blow You Away'                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/a-reality-check-on-rising-intolerance-home-ministry-data-shows-increase-in-communal-incidents-247671.html" title="A Reality Check On Rising Intolerance, Home Ministry Data Shows Increase In Communal Incidents" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/communal-card_1448363074_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/a-reality-check-on-rising-intolerance-home-ministry-data-shows-increase-in-communal-incidents-247671.html" title="A Reality Check On Rising Intolerance, Home Ministry Data Shows Increase In Communal Incidents">
                            A Reality Check On Rising Intolerance, Home Ministry Data Shows Increase In Communal Incidents                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-things-that-say-there-is-a-doctor-inside-all-indians-247486.html" class="tint" title="11 Things That Say There Is A Doctor Inside All Indians">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447929116_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-things-that-say-there-is-a-doctor-inside-all-indians-247486.html" title="11 Things That Say There Is A Doctor Inside All Indians">
                            11 Things That Say There Is A Doctor Inside All Indians                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/8-upcoming-remakes-in-bollywood-we-cannot-wait-for-247502.html" class="tint" title="Sanjay Leela Bhansali Interested In A 'Khalnayak' Remake + 7 Upcoming Bollywood Remakes We Can't Wait For">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447935445_1447935450_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/8-upcoming-remakes-in-bollywood-we-cannot-wait-for-247502.html" title="Sanjay Leela Bhansali Interested In A 'Khalnayak' Remake + 7 Upcoming Bollywood Remakes We Can't Wait For">
                            Sanjay Leela Bhansali Interested In A 'Khalnayak' Remake + 7 Upcoming Bollywood Remakes We Can't Wait For                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/now-you-can-scale-mount-everest-from-the-comfort-of-your-home-247667.html" class="tint" title="Now You Can Scale Mount Everest From The Comfort Of Your Home">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/everest-card_1448361366_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/now-you-can-scale-mount-everest-from-the-comfort-of-your-home-247667.html" title="Now You Can Scale Mount Everest From The Comfort Of Your Home">
                            Now You Can Scale Mount Everest From The Comfort Of Your Home                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-of-barack-obama-singing-lean-on-is-the-funniest-thing-you-ll-watch-today-247645.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/this-video-of-barack-obama-singing-lean-on-is-the-funniest-thing-you-ll-watch-today-247645.html" class="tint" title="This Video Of Barack Obama Singing 'Lean On' Is The Funniest Thing You'll Watch Today!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/potus_leanon_card_1448343280_218x102.jpg" border="0" alt="This Video Of Barack Obama Singing 'Lean On' Is The Funniest Thing You'll Watch Today!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-video-of-barack-obama-singing-lean-on-is-the-funniest-thing-you-ll-watch-today-247645.html" title="This Video Of Barack Obama Singing 'Lean On' Is The Funniest Thing You'll Watch Today!">
                            This Video Of Barack Obama Singing 'Lean On' Is The Funniest Thing You'll Watch Today!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/who-we-are/the-hero-of-nathula-pass-ghost-of-baba-harbhajan-singh-that-guards-india-s-border-247635.html" class="tint" title="The Hero Of Nathula Pass â Ghost Of Baba Harbhajan Singh That Guards Indiaâs Border">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/baba---card_1448278866_218x102.jpg" border="0" alt="The Hero Of Nathula Pass â Ghost Of Baba Harbhajan Singh That Guards Indiaâs Border"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/the-hero-of-nathula-pass-ghost-of-baba-harbhajan-singh-that-guards-india-s-border-247635.html" title="The Hero Of Nathula Pass â Ghost Of Baba Harbhajan Singh That Guards Indiaâs Border">
                            The Hero Of Nathula Pass â Ghost Of Baba Harbhajan Singh That Guards Indiaâs Border                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/videos/at-104-this-man-is-still-running-marathons-respect-247615.html'>video</a>
                        <a href="http://www.indiatimes.com/health/videos/at-104-this-man-is-still-running-marathons-respect-247615.html" class="tint" title="At 104, This Man Is Still Running Marathons! #Respect">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/cover_1448263407_218x102.jpg" border="0" alt="At 104, This Man Is Still Running Marathons! #Respect"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/videos/at-104-this-man-is-still-running-marathons-respect-247615.html" title="At 104, This Man Is Still Running Marathons! #Respect">
                            At 104, This Man Is Still Running Marathons! #Respect                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/bigg-boss-9-wild-card-entrant-priya-malik-drops-the-first-bomb-accuses-rishabh-of-sexual-abuse-247661.html" class="tint" title="Bigg Boss 9 Wild Card Entrant Priya Malik Drops The First Bomb, Accuses Rishabh Of Sexual Abuse!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/bigg-boss-card_1448353870_1448353876_218x102.jpg" border="0" alt="Bigg Boss 9 Wild Card Entrant Priya Malik Drops The First Bomb, Accuses Rishabh Of Sexual Abuse!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bigg-boss-9-wild-card-entrant-priya-malik-drops-the-first-bomb-accuses-rishabh-of-sexual-abuse-247661.html" title="Bigg Boss 9 Wild Card Entrant Priya Malik Drops The First Bomb, Accuses Rishabh Of Sexual Abuse!">
                            Bigg Boss 9 Wild Card Entrant Priya Malik Drops The First Bomb, Accuses Rishabh Of Sexual Abuse!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/amitabh-bachchan-reveals-how-that-one-accident-on-the-sets-of-coolie-changed-his-life-247647.html" class="tint" title="Amitabh Bachchan Reveals How That One Accident On The Sets Of Coolie Changed His Life!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448344926_218x102.jpg" border="0" alt="Amitabh Bachchan Reveals How That One Accident On The Sets Of Coolie Changed His Life!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/amitabh-bachchan-reveals-how-that-one-accident-on-the-sets-of-coolie-changed-his-life-247647.html" title="Amitabh Bachchan Reveals How That One Accident On The Sets Of Coolie Changed His Life!">
                            Amitabh Bachchan Reveals How That One Accident On The Sets Of Coolie Changed His Life!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            7 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/turkey-shoots-down-russian-warplane-over-violation-of-air-space-russia-denies-crossing-turkish-borders-247668.html" title="Turkey Shoots Down Russian Warplane Over Violation Of Air Space, Russia Denies Crossing Turkish Borders" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/su-60_1448361977_236x111.jpg" border="0" alt="Turkey Shoots Down Russian Warplane Over Violation Of Air Space, Russia Denies Crossing Turkish Borders"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/turkey-shoots-down-russian-warplane-over-violation-of-air-space-russia-denies-crossing-turkish-borders-247668.html" title="Turkey Shoots Down Russian Warplane Over Violation Of Air Space, Russia Denies Crossing Turkish Borders">
                            Turkey Shoots Down Russian Warplane Over Violation Of Air Space, Russia Denies Crossing Turkish Borders                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/this-84-yo-man-has-taken-care-of-his-paralysed-wife-for-56-years-and-plans-to-do-so-forever-247652.html" title="This 84-YO Man Has Taken Care Of His Paralysed Wife For 56 Years, And Plans To Do So Forever" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/502_1448348330_236x111.jpg" border="0" alt="This 84-YO Man Has Taken Care Of His Paralysed Wife For 56 Years, And Plans To Do So Forever"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/this-84-yo-man-has-taken-care-of-his-paralysed-wife-for-56-years-and-plans-to-do-so-forever-247652.html" title="This 84-YO Man Has Taken Care Of His Paralysed Wife For 56 Years, And Plans To Do So Forever">
                            This 84-YO Man Has Taken Care Of His Paralysed Wife For 56 Years, And Plans To Do So Forever                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/union-minister-smriti-irani-lashes-out-on-a-journalist-who-reported-irregularity-in-kendriya-vidyalaya-admissions-247658.html" title="Union Minister Smriti Irani Lashes Out On A Journalist Who Reported Irregularity In Kendriya Vidyalaya Admissions" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/smriti1-card_1448350872_236x111.jpg" border="0" alt="Union Minister Smriti Irani Lashes Out On A Journalist Who Reported Irregularity In Kendriya Vidyalaya Admissions"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/union-minister-smriti-irani-lashes-out-on-a-journalist-who-reported-irregularity-in-kendriya-vidyalaya-admissions-247658.html" title="Union Minister Smriti Irani Lashes Out On A Journalist Who Reported Irregularity In Kendriya Vidyalaya Admissions">
                            Union Minister Smriti Irani Lashes Out On A Journalist Who Reported Irregularity In Kendriya Vidyalaya Admissions                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/the-muslim-teenager-whose-clock-was-labelled-a-bomb-is-suing-his-school-for-15-million-in-damages-247653.html" title="The Muslim Teenager Whose Clock Was Labelled A Bomb Is Suing His School For $15 Million In Damages" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448351124_236x111.jpg" border="0" alt="The Muslim Teenager Whose Clock Was Labelled A Bomb Is Suing His School For $15 Million In Damages"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/the-muslim-teenager-whose-clock-was-labelled-a-bomb-is-suing-his-school-for-15-million-in-damages-247653.html" title="The Muslim Teenager Whose Clock Was Labelled A Bomb Is Suing His School For $15 Million In Damages">
                            The Muslim Teenager Whose Clock Was Labelled A Bomb Is Suing His School For $15 Million In Damages                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/the-coimbatore-police-were-frantically-searching-for-her-killer-when-this-murder-victim-called-them-to-say-i-am-alive-247651.html" title="Police Were Frantically Searching For Her Killer, When This Murder Victim Called Them To Say 'I Am Alive'" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/police-card_1448347209_236x111.jpg" border="0" alt="Police Were Frantically Searching For Her Killer, When This Murder Victim Called Them To Say 'I Am Alive'"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/the-coimbatore-police-were-frantically-searching-for-her-killer-when-this-murder-victim-called-them-to-say-i-am-alive-247651.html" title="Police Were Frantically Searching For Her Killer, When This Murder Victim Called Them To Say 'I Am Alive'">
                            Police Were Frantically Searching For Her Killer, When This Murder Victim Called Them To Say 'I Am Alive'                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bigg-boss-9-wild-card-entrant-priya-malik-drops-the-first-bomb-accuses-rishabh-of-sexual-abuse-247661.html" class="tint" title="Bigg Boss 9 Wild Card Entrant Priya Malik Drops The First Bomb, Accuses Rishabh Of Sexual Abuse!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/bigg-boss-card_1448353870_1448353876_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bigg-boss-9-wild-card-entrant-priya-malik-drops-the-first-bomb-accuses-rishabh-of-sexual-abuse-247661.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bigg-boss-9-wild-card-entrant-priya-malik-drops-the-first-bomb-accuses-rishabh-of-sexual-abuse-247661.html" title="Bigg Boss 9 Wild Card Entrant Priya Malik Drops The First Bomb, Accuses Rishabh Of Sexual Abuse!">
                            Bigg Boss 9 Wild Card Entrant Priya Malik Drops The First Bomb, Accuses Rishabh Of Sexual Abuse!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/videos/7-ways-to-survive-in-a-job-you-actually-hate-247609.html" class="tint" title="7 Ways To Survive In A Job You Actually Hate">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448270762_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/videos/7-ways-to-survive-in-a-job-you-actually-hate-247609.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/videos/7-ways-to-survive-in-a-job-you-actually-hate-247609.html" title="7 Ways To Survive In A Job You Actually Hate">
                            7 Ways To Survive In A Job You Actually Hate                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/ever-wonder-what-ll-happen-if-you-try-to-sharpen-the-apple-pencil-well-this-man-just-did-247663.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/ever-wonder-what-ll-happen-if-you-try-to-sharpen-the-apple-pencil-well-this-man-just-did-247663.html" class="tint" title="Ever Wonder What'll Happen If You Try To Sharpen The Apple Pencil ? Well, This Man Just Did!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/apple_pencil_card1_1448355282_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/ever-wonder-what-ll-happen-if-you-try-to-sharpen-the-apple-pencil-well-this-man-just-did-247663.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/ever-wonder-what-ll-happen-if-you-try-to-sharpen-the-apple-pencil-well-this-man-just-did-247663.html" title="Ever Wonder What'll Happen If You Try To Sharpen The Apple Pencil ? Well, This Man Just Did!">
                            Ever Wonder What'll Happen If You Try To Sharpen The Apple Pencil ? Well, This Man Just Did!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/sonakshi-sinha-asks-censor-chief-pahlaj-nihalani-to-stop-playing-nanny-high-time-someone-told-him-that-247649.html" class="tint" title="Sonakshi Sinha Asks Censor Chief Pahlaj Nihalani To Stop Playing Nanny! High Time Someone Told Him That!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448346898_1448346904_218x102.jpg" border="0" alt="Sonakshi Sinha Asks Censor Chief Pahlaj Nihalani To Stop Playing Nanny! High Time Someone Told Him That!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/sonakshi-sinha-asks-censor-chief-pahlaj-nihalani-to-stop-playing-nanny-high-time-someone-told-him-that-247649.html" title="Sonakshi Sinha Asks Censor Chief Pahlaj Nihalani To Stop Playing Nanny! High Time Someone Told Him That!">
                            Sonakshi Sinha Asks Censor Chief Pahlaj Nihalani To Stop Playing Nanny! High Time Someone Told Him That!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/25-people-who-had-just-one-job-and-failed-marvelously-at-it-247512.html" class="tint" title="25 People Who Had Just One Job And Failed Marvelously At It">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/aza_1447939068_1447939082_218x102.jpg" border="0" alt="25 People Who Had Just One Job And Failed Marvelously At It"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/25-people-who-had-just-one-job-and-failed-marvelously-at-it-247512.html" title="25 People Who Had Just One Job And Failed Marvelously At It">
                            25 People Who Had Just One Job And Failed Marvelously At It                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/11-of-the-cleanest-public-places-in-india-247412.html" class="tint" title="11 Of The Cleanest Public Places In India">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447758154_218x102.jpg" border="0" alt="11 Of The Cleanest Public Places In India"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-of-the-cleanest-public-places-in-india-247412.html" title="11 Of The Cleanest Public Places In India">
                            11 Of The Cleanest Public Places In India                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/this-man-is-walking-10-000-km-to-create-awareness-about-water-conservation-247627.html" class="tint" title="This Man Is Walking 10,000 km To Create Awareness About Water Conservation">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/walk_1448274738_1448274746_218x102.jpg" border="0" alt="This Man Is Walking 10,000 km To Create Awareness About Water Conservation"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/this-man-is-walking-10-000-km-to-create-awareness-about-water-conservation-247627.html" title="This Man Is Walking 10,000 km To Create Awareness About Water Conservation">
                            This Man Is Walking 10,000 km To Create Awareness About Water Conservation                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/7-really-bizarre-things-scientists-have-discovered-in-the-recent-past-247514.html" class="tint" title="7 Really Bizarre Things Scientists Have Discovered In The Recent Past">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cnvyb_1447941034_1447941039_218x102.jpg" border="0" alt="7 Really Bizarre Things Scientists Have Discovered In The Recent Past"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/7-really-bizarre-things-scientists-have-discovered-in-the-recent-past-247514.html" title="7 Really Bizarre Things Scientists Have Discovered In The Recent Past">
                            7 Really Bizarre Things Scientists Have Discovered In The Recent Past                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/game-of-thrones-teases-fans-with-jon-snow-s-fate-in-this-new-poster-and-everyone-goes-berserk-247644.html" title="Game Of Thrones Teases Fans With Jon Snow's Fate In This New Poster And Everyone Goes Berserk!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/snow502_1448343134_236x111.jpg" border="0" alt="Game Of Thrones Teases Fans With Jon Snow's Fate In This New Poster And Everyone Goes Berserk!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/game-of-thrones-teases-fans-with-jon-snow-s-fate-in-this-new-poster-and-everyone-goes-berserk-247644.html" title="Game Of Thrones Teases Fans With Jon Snow's Fate In This New Poster And Everyone Goes Berserk!">
                            Game Of Thrones Teases Fans With Jon Snow's Fate In This New Poster And Everyone Goes Berserk!                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/isis-thinks-indians-aren-t-good-enough-for-combat-247646.html" title="ISIS Thinks Indians Aren't Good Enough For Combat" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/isis-flag-tnl_1448344363_236x111.jpg" border="0" alt="ISIS Thinks Indians Aren't Good Enough For Combat"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/isis-thinks-indians-aren-t-good-enough-for-combat-247646.html" title="ISIS Thinks Indians Aren't Good Enough For Combat">
                            ISIS Thinks Indians Aren't Good Enough For Combat                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/mark-zuckerberg-s-internet-org-brings-connectivity-to-india-free-internet-services-become-available-to-all-247640.html" title="Mark Zuckerberg's Internet.org Brings Connectivity To India, Free Internet Services Become Available To All" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/502_1448340981_236x111.jpg" border="0" alt="Mark Zuckerberg's Internet.org Brings Connectivity To India, Free Internet Services Become Available To All"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/mark-zuckerberg-s-internet-org-brings-connectivity-to-india-free-internet-services-become-available-to-all-247640.html" title="Mark Zuckerberg's Internet.org Brings Connectivity To India, Free Internet Services Become Available To All">
                            Mark Zuckerberg's Internet.org Brings Connectivity To India, Free Internet Services Become Available To All                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/after-delhi-now-pune-engineering-student-gets-rs-2-crore-job-offer-from-google-247641.html" title="After Delhi, Now Pune Engineering Student Gets Rs 2 Crore Job Offer From Google!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/google-card_1448349326_236x111.jpg" border="0" alt="After Delhi, Now Pune Engineering Student Gets Rs 2 Crore Job Offer From Google!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/after-delhi-now-pune-engineering-student-gets-rs-2-crore-job-offer-from-google-247641.html" title="After Delhi, Now Pune Engineering Student Gets Rs 2 Crore Job Offer From Google!">
                            After Delhi, Now Pune Engineering Student Gets Rs 2 Crore Job Offer From Google!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/armed-with-only-a-khukri-this-gurkha-soldier-took-on-40-men-and-saved-a-girl-from-being-gang-raped-247639.html" title="Armed With Only A Khukri This Gurkha Soldier Took On 40 Men And Saved A Girl From Being Gang Raped" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/1_1448292499_1448292511_236x111.jpg" border="0" alt="Armed With Only A Khukri This Gurkha Soldier Took On 40 Men And Saved A Girl From Being Gang Raped"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/armed-with-only-a-khukri-this-gurkha-soldier-took-on-40-men-and-saved-a-girl-from-being-gang-raped-247639.html" title="Armed With Only A Khukri This Gurkha Soldier Took On 40 Men And Saved A Girl From Being Gang Raped">
                            Armed With Only A Khukri This Gurkha Soldier Took On 40 Men And Saved A Girl From Being Gang Raped                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/there-are-musical-highways-around-the-world-that-play-melodies-when-cars-drive-on-them-247516.html" class="tint" title="There Are Musical Highways Around The World That Play Melodies When Cars Drive On Them!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/musicalroad_1447944791_1447944802_502x234.jpg" border="0" alt="There Are Musical Highways Around The World That Play Melodies When Cars Drive On Them!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/there-are-musical-highways-around-the-world-that-play-melodies-when-cars-drive-on-them-247516.html" title="There Are Musical Highways Around The World That Play Melodies When Cars Drive On Them!">
                        There Are Musical Highways Around The World That Play Melodies When Cars Drive On Them!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/13-herbal-teas-that-can-help-you-fight-stress-247622.html" class="tint" title="13 Herbal Teas That Can Help You Fight Stress!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1448270722_502x234.jpg" border="0" alt="13 Herbal Teas That Can Help You Fight Stress!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/13-herbal-teas-that-can-help-you-fight-stress-247622.html" title="13 Herbal Teas That Can Help You Fight Stress!">
                        13 Herbal Teas That Can Help You Fight Stress!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bihar-police-uses-salman-s-prdp-to-teach-children-about-the-importance-of-education-family-247657.html" class="tint" title="Bihar Police Uses Salman's PRDP To Teach Children About The Importance Of Education & Family">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448351585_502x234.jpg" border="0" alt="Bihar Police Uses Salman's PRDP To Teach Children About The Importance Of Education & Family" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bihar-police-uses-salman-s-prdp-to-teach-children-about-the-importance-of-education-family-247657.html" title="Bihar Police Uses Salman's PRDP To Teach Children About The Importance Of Education & Family">
                        Bihar Police Uses Salman's PRDP To Teach Children About The Importance Of Education & Family                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/a-lake-in-bengaluru-is-so-full-of-toxic-foam-that-it-is-now-spilling-into-the-streets-247659.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/a-lake-in-bengaluru-is-so-full-of-toxic-foam-that-it-is-now-spilling-into-the-streets-247659.html" class="tint" title="A Lake In Bengaluru Is So Full Of Toxic Foam That It Is Now Spilling Into The Streets!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/bengaluru_1448351087_502x234.jpg" border="0" alt="A Lake In Bengaluru Is So Full Of Toxic Foam That It Is Now Spilling Into The Streets!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/a-lake-in-bengaluru-is-so-full-of-toxic-foam-that-it-is-now-spilling-into-the-streets-247659.html" title="A Lake In Bengaluru Is So Full Of Toxic Foam That It Is Now Spilling Into The Streets!">
                        A Lake In Bengaluru Is So Full Of Toxic Foam That It Is Now Spilling Into The Streets!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/sonakshi-sinha-asks-censor-chief-pahlaj-nihalani-to-stop-playing-nanny-high-time-someone-told-him-that-247649.html" class="tint" title="Sonakshi Sinha Asks Censor Chief Pahlaj Nihalani To Stop Playing Nanny! High Time Someone Told Him That!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448346898_1448346904_502x234.jpg" border="0" alt="Sonakshi Sinha Asks Censor Chief Pahlaj Nihalani To Stop Playing Nanny! High Time Someone Told Him That!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/sonakshi-sinha-asks-censor-chief-pahlaj-nihalani-to-stop-playing-nanny-high-time-someone-told-him-that-247649.html" title="Sonakshi Sinha Asks Censor Chief Pahlaj Nihalani To Stop Playing Nanny! High Time Someone Told Him That!">
                        Sonakshi Sinha Asks Censor Chief Pahlaj Nihalani To Stop Playing Nanny! High Time Someone Told Him That!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/amitabh-bachchan-reveals-how-that-one-accident-on-the-sets-of-coolie-changed-his-life-247647.html" class="tint" title="Amitabh Bachchan Reveals How That One Accident On The Sets Of Coolie Changed His Life!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448344926_502x234.jpg" border="0" alt="Amitabh Bachchan Reveals How That One Accident On The Sets Of Coolie Changed His Life!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/amitabh-bachchan-reveals-how-that-one-accident-on-the-sets-of-coolie-changed-his-life-247647.html" title="Amitabh Bachchan Reveals How That One Accident On The Sets Of Coolie Changed His Life!">
                        Amitabh Bachchan Reveals How That One Accident On The Sets Of Coolie Changed His Life!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-of-barack-obama-singing-lean-on-is-the-funniest-thing-you-ll-watch-today-247645.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-barack-obama-singing-lean-on-is-the-funniest-thing-you-ll-watch-today-247645.html" class="tint" title="This Video Of Barack Obama Singing 'Lean On' Is The Funniest Thing You'll Watch Today!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/potus_leanon_card_1448343280_502x234.jpg" border="0" alt="This Video Of Barack Obama Singing 'Lean On' Is The Funniest Thing You'll Watch Today!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-barack-obama-singing-lean-on-is-the-funniest-thing-you-ll-watch-today-247645.html" title="This Video Of Barack Obama Singing 'Lean On' Is The Funniest Thing You'll Watch Today!">
                        This Video Of Barack Obama Singing 'Lean On' Is The Funniest Thing You'll Watch Today!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/dear-aamir-when-did-incredible-india-become-intolerant-india-for-you-asks-anupam-kher-247642.html" class="tint" title="Dear Aamir, When Did âIncredible Indiaâ Become âIntolerant Indiaâ For You, Asks Anupam Kher!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card123_1448341604_1448341611_502x234.jpg" border="0" alt="Dear Aamir, When Did âIncredible Indiaâ Become âIntolerant Indiaâ For You, Asks Anupam Kher!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/dear-aamir-when-did-incredible-india-become-intolerant-india-for-you-asks-anupam-kher-247642.html" title="Dear Aamir, When Did âIncredible Indiaâ Become âIntolerant Indiaâ For You, Asks Anupam Kher!">
                        Dear Aamir, When Did âIncredible Indiaâ Become âIntolerant Indiaâ For You, Asks Anupam Kher!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/19-reasons-you-need-to-get-off-your-bum-and-start-walking-every-day-247540.html" class="tint" title="19 Reasons You Need To Get Off Your Bum And Start Walking Every Day">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448274607_502x234.jpg" border="0" alt="19 Reasons You Need To Get Off Your Bum And Start Walking Every Day" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/19-reasons-you-need-to-get-off-your-bum-and-start-walking-every-day-247540.html" title="19 Reasons You Need To Get Off Your Bum And Start Walking Every Day">
                        19 Reasons You Need To Get Off Your Bum And Start Walking Every Day                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-ways-to-tell-your-boss-to-f-k-off-without-actually-telling-him-to-do-so-247533.html" class="tint" title="9 Ways To Tell Your Boss To F**k Off Without Actually Telling Him To Do So">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448022971_502x234.jpg" border="0" alt="9 Ways To Tell Your Boss To F**k Off Without Actually Telling Him To Do So" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-ways-to-tell-your-boss-to-f-k-off-without-actually-telling-him-to-do-so-247533.html" title="9 Ways To Tell Your Boss To F**k Off Without Actually Telling Him To Do So">
                        9 Ways To Tell Your Boss To F**k Off Without Actually Telling Him To Do So                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-really-bizarre-things-scientists-have-discovered-in-the-recent-past-247514.html" class="tint" title="7 Really Bizarre Things Scientists Have Discovered In The Recent Past">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cnvyb_1447941034_1447941039_502x234.jpg" border="0" alt="7 Really Bizarre Things Scientists Have Discovered In The Recent Past" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-really-bizarre-things-scientists-have-discovered-in-the-recent-past-247514.html" title="7 Really Bizarre Things Scientists Have Discovered In The Recent Past">
                        7 Really Bizarre Things Scientists Have Discovered In The Recent Past                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/videos/at-104-this-man-is-still-running-marathons-respect-247615.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/at-104-this-man-is-still-running-marathons-respect-247615.html" class="tint" title="At 104, This Man Is Still Running Marathons! #Respect">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/cover_1448263407_502x234.jpg" border="0" alt="At 104, This Man Is Still Running Marathons! #Respect" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/at-104-this-man-is-still-running-marathons-respect-247615.html" title="At 104, This Man Is Still Running Marathons! #Respect">
                        At 104, This Man Is Still Running Marathons! #Respect                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/the-hero-of-nathula-pass-ghost-of-baba-harbhajan-singh-that-guards-india-s-border-247635.html" class="tint" title="The Hero Of Nathula Pass â Ghost Of Baba Harbhajan Singh That Guards Indiaâs Border">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/baba---card_1448278866_502x234.jpg" border="0" alt="The Hero Of Nathula Pass â Ghost Of Baba Harbhajan Singh That Guards Indiaâs Border" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/the-hero-of-nathula-pass-ghost-of-baba-harbhajan-singh-that-guards-india-s-border-247635.html" title="The Hero Of Nathula Pass â Ghost Of Baba Harbhajan Singh That Guards Indiaâs Border">
                        The Hero Of Nathula Pass â Ghost Of Baba Harbhajan Singh That Guards Indiaâs Border                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-of-the-cleanest-public-places-in-india-247412.html" class="tint" title="11 Of The Cleanest Public Places In India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447758154_502x234.jpg" border="0" alt="11 Of The Cleanest Public Places In India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-of-the-cleanest-public-places-in-india-247412.html" title="11 Of The Cleanest Public Places In India">
                        11 Of The Cleanest Public Places In India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/this-behind-the-scenes-video-of-tamasha-will-make-you-crave-for-a-corsica-trip-right-now-247634.html" class="tint" title="This Behind The Scenes Video Of Tamasha Will Make You Crave For A Corsica Trip Right Now!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ttt_1448279186_1448279195_502x234.jpg" border="0" alt="This Behind The Scenes Video Of Tamasha Will Make You Crave For A Corsica Trip Right Now!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/this-behind-the-scenes-video-of-tamasha-will-make-you-crave-for-a-corsica-trip-right-now-247634.html" title="This Behind The Scenes Video Of Tamasha Will Make You Crave For A Corsica Trip Right Now!">
                        This Behind The Scenes Video Of Tamasha Will Make You Crave For A Corsica Trip Right Now!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-new-cfl-tvc-beats-all-the-fairness-creams-ads-to-the-punch-247611.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-new-cfl-tvc-beats-all-the-fairness-creams-ads-to-the-punch-247611.html" class="tint" title="This New CFL TVC Beats All The Fairness Creams' Ads To The Punch">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/clf_ad_card_1448261910_502x234.jpg" border="0" alt="This New CFL TVC Beats All The Fairness Creams' Ads To The Punch" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-new-cfl-tvc-beats-all-the-fairness-creams-ads-to-the-punch-247611.html" title="This New CFL TVC Beats All The Fairness Creams' Ads To The Punch">
                        This New CFL TVC Beats All The Fairness Creams' Ads To The Punch                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/shahrukh-khan-has-given-his-nod-to-imtiaz-ali-s-next-film-now-that-will-be-one-amazing-collaboration-247628.html" class="tint" title="Shahrukh Khan Has Given His Nod To Imtiaz Ali's Next Film. Now That Will Be One Amazing Collaboration!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448275825_1448275842_502x234.jpg" border="0" alt="Shahrukh Khan Has Given His Nod To Imtiaz Ali's Next Film. Now That Will Be One Amazing Collaboration!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/shahrukh-khan-has-given-his-nod-to-imtiaz-ali-s-next-film-now-that-will-be-one-amazing-collaboration-247628.html" title="Shahrukh Khan Has Given His Nod To Imtiaz Ali's Next Film. Now That Will Be One Amazing Collaboration!">
                        Shahrukh Khan Has Given His Nod To Imtiaz Ali's Next Film. Now That Will Be One Amazing Collaboration!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bhansali-s-bajirao-mastani-lands-in-another-soup-as-history-purists-demand-a-ban-on-the-film-247625.html" class="tint" title="Bhansali's Bajirao Mastani Lands In Another Soup As History Purists Demand A Ban On The Film!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448275709_502x234.jpg" border="0" alt="Bhansali's Bajirao Mastani Lands In Another Soup As History Purists Demand A Ban On The Film!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bhansali-s-bajirao-mastani-lands-in-another-soup-as-history-purists-demand-a-ban-on-the-film-247625.html" title="Bhansali's Bajirao Mastani Lands In Another Soup As History Purists Demand A Ban On The Film!">
                        Bhansali's Bajirao Mastani Lands In Another Soup As History Purists Demand A Ban On The Film!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-man-is-walking-10-000-km-to-create-awareness-about-water-conservation-247627.html" class="tint" title="This Man Is Walking 10,000 km To Create Awareness About Water Conservation">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/walk_1448274738_1448274746_502x234.jpg" border="0" alt="This Man Is Walking 10,000 km To Create Awareness About Water Conservation" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-man-is-walking-10-000-km-to-create-awareness-about-water-conservation-247627.html" title="This Man Is Walking 10,000 km To Create Awareness About Water Conservation">
                        This Man Is Walking 10,000 km To Create Awareness About Water Conservation                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/sometimes-i-feel-i-don-t-get-to-do-the-films-i-want-to-says-srk-8-other-revelations-by-him-247608.html" class="tint" title="Shah Rukh Khan Says He Never Spoke About India Being Intolerant!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/srk1_1448371769_1448371776_502x234.jpg" border="0" alt="Shah Rukh Khan Says He Never Spoke About India Being Intolerant!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/sometimes-i-feel-i-don-t-get-to-do-the-films-i-want-to-says-srk-8-other-revelations-by-him-247608.html" title="Shah Rukh Khan Says He Never Spoke About India Being Intolerant!">
                        Shah Rukh Khan Says He Never Spoke About India Being Intolerant!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/here-s-how-learning-a-new-language-makes-you-a-smarter-person-247613.html" class="tint" title="Here's How Learning A New Language Makes You A Smarter Person">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-1_1448262985_502x234.jpg" border="0" alt="Here's How Learning A New Language Makes You A Smarter Person" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/here-s-how-learning-a-new-language-makes-you-a-smarter-person-247613.html" title="Here's How Learning A New Language Makes You A Smarter Person">
                        Here's How Learning A New Language Makes You A Smarter Person                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/whoa-is-preity-zinta-all-set-tie-the-knot-with-her-american-boyfriend-247623.html" class="tint" title="Whoa! Is Preity Zinta All Set Tie The Knot With Her American Boyfriend?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/11058404_808070099305571_584068163314477266_o_1448271647_1448271651_502x234.jpg" border="0" alt="Whoa! Is Preity Zinta All Set Tie The Knot With Her American Boyfriend?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/whoa-is-preity-zinta-all-set-tie-the-knot-with-her-american-boyfriend-247623.html" title="Whoa! Is Preity Zinta All Set Tie The Knot With Her American Boyfriend?">
                        Whoa! Is Preity Zinta All Set Tie The Knot With Her American Boyfriend?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/these-15-pictures-from-masaba-gupta-s-wedding-prove-that-it-was-a-grand-bollywood-affair-247612.html" class="tint" title="These 15 Pictures From Masaba Gupta's Wedding Prove That It Was A Grand Bollywood Affair!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448262005_1448262011_502x234.jpg" border="0" alt="These 15 Pictures From Masaba Gupta's Wedding Prove That It Was A Grand Bollywood Affair!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/these-15-pictures-from-masaba-gupta-s-wedding-prove-that-it-was-a-grand-bollywood-affair-247612.html" title="These 15 Pictures From Masaba Gupta's Wedding Prove That It Was A Grand Bollywood Affair!">
                        These 15 Pictures From Masaba Gupta's Wedding Prove That It Was A Grand Bollywood Affair!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/25-people-who-had-just-one-job-and-failed-marvelously-at-it-247512.html" class="tint" title="25 People Who Had Just One Job And Failed Marvelously At It">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/aza_1447939068_1447939082_502x234.jpg" border="0" alt="25 People Who Had Just One Job And Failed Marvelously At It" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/25-people-who-had-just-one-job-and-failed-marvelously-at-it-247512.html" title="25 People Who Had Just One Job And Failed Marvelously At It">
                        25 People Who Had Just One Job And Failed Marvelously At It                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-unhealthy-foods-you-should-never-give-your-children-247478.html" class="tint" title="10 Unhealthy Foods You Should Never Give Your Children">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-ss_1447915662_502x234.jpg" border="0" alt="10 Unhealthy Foods You Should Never Give Your Children" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-unhealthy-foods-you-should-never-give-your-children-247478.html" title="10 Unhealthy Foods You Should Never Give Your Children">
                        10 Unhealthy Foods You Should Never Give Your Children                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/angelina-jolie-talks-about-her-menopause-there-is-a-reason-why-every-woman-should-read-it-247617.html" class="tint" title="Angelina Jolie Talks About Her Menopause & There Is A Reason Why Every Woman Should Read It!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448265317_502x234.jpg" border="0" alt="Angelina Jolie Talks About Her Menopause & There Is A Reason Why Every Woman Should Read It!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/angelina-jolie-talks-about-her-menopause-there-is-a-reason-why-every-woman-should-read-it-247617.html" title="Angelina Jolie Talks About Her Menopause & There Is A Reason Why Every Woman Should Read It!">
                        Angelina Jolie Talks About Her Menopause & There Is A Reason Why Every Woman Should Read It!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-hilarious-spoof-of-dilwale-is-dedicated-to-every-engineer-who-has-a-degree-but-no-job-247619.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-hilarious-spoof-of-dilwale-is-dedicated-to-every-engineer-who-has-a-degree-but-no-job-247619.html" class="tint" title="This Hilarious Spoof Of 'Dilwale' Is Dedicated To Every Engineer Who Has A Degree But No Job!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card_1448265960_502x234.jpg" border="0" alt="This Hilarious Spoof Of 'Dilwale' Is Dedicated To Every Engineer Who Has A Degree But No Job!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-hilarious-spoof-of-dilwale-is-dedicated-to-every-engineer-who-has-a-degree-but-no-job-247619.html" title="This Hilarious Spoof Of 'Dilwale' Is Dedicated To Every Engineer Who Has A Degree But No Job!">
                        This Hilarious Spoof Of 'Dilwale' Is Dedicated To Every Engineer Who Has A Degree But No Job!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-anonymous-guy-calls-out-to-all-those-people-who-purchased-overpriced-tickets-of-prdp-247607.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-anonymous-guy-calls-out-to-all-those-people-who-purchased-overpriced-tickets-of-prdp-247607.html" class="tint" title="This Anonymous Guy Calls Out To All Those People Who Purchased Overpriced Tickets Of PRDP!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/anoymous_prdp_card_1448260228_502x234.jpg" border="0" alt="This Anonymous Guy Calls Out To All Those People Who Purchased Overpriced Tickets Of PRDP!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-anonymous-guy-calls-out-to-all-those-people-who-purchased-overpriced-tickets-of-prdp-247607.html" title="This Anonymous Guy Calls Out To All Those People Who Purchased Overpriced Tickets Of PRDP!">
                        This Anonymous Guy Calls Out To All Those People Who Purchased Overpriced Tickets Of PRDP!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/deepika-ranbir-did-some-tamasha-in-a-train-and-took-the-passengers-on-a-joyous-ride-247600.html" class="tint" title="Deepika, Ranbir Did Some 'Tamasha' In A Train And Took The Passengers On A Joyous Ride!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/rb123_1448256108_1448256114_502x234.jpg" border="0" alt="Deepika, Ranbir Did Some 'Tamasha' In A Train And Took The Passengers On A Joyous Ride!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/deepika-ranbir-did-some-tamasha-in-a-train-and-took-the-passengers-on-a-joyous-ride-247600.html" title="Deepika, Ranbir Did Some 'Tamasha' In A Train And Took The Passengers On A Joyous Ride!">
                        Deepika, Ranbir Did Some 'Tamasha' In A Train And Took The Passengers On A Joyous Ride!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-mistakes-you-absolutely-must-avoid-while-giving-a-presentation-247424.html" class="tint" title="11 Mistakes You Absolutely Must Avoid While Giving A Presentation">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/slides_1447766047_1447766056_502x234.jpg" border="0" alt="11 Mistakes You Absolutely Must Avoid While Giving A Presentation" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-mistakes-you-absolutely-must-avoid-while-giving-a-presentation-247424.html" title="11 Mistakes You Absolutely Must Avoid While Giving A Presentation">
                        11 Mistakes You Absolutely Must Avoid While Giving A Presentation                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-can-do-to-make-your-life-simpler-247543.html" class="tint" title="11 Things You Can Do To Make Your Life Simpler">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/akshay_1448020302_502x234.jpg" border="0" alt="11 Things You Can Do To Make Your Life Simpler" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-can-do-to-make-your-life-simpler-247543.html" title="11 Things You Can Do To Make Your Life Simpler">
                        11 Things You Can Do To Make Your Life Simpler                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/how-to-seduce-and-get-each-zodiac-sign-to-go-on-a-date-with-you-247492.html" class="tint" title="How To Seduce And Get Each Zodiac Sign To Go On A Date With You">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448001198_502x234.jpg" border="0" alt="How To Seduce And Get Each Zodiac Sign To Go On A Date With You" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/how-to-seduce-and-get-each-zodiac-sign-to-go-on-a-date-with-you-247492.html" title="How To Seduce And Get Each Zodiac Sign To Go On A Date With You">
                        How To Seduce And Get Each Zodiac Sign To Go On A Date With You                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/get-a-full-ab-workout-done-in-just-4-minutes-yes-really-247500.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/get-a-full-ab-workout-done-in-just-4-minutes-yes-really-247500.html" class="tint" title="Get A Full Ab Workout Done In Just 4 Minutes! Yes, Really!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card-1_1447932222_502x234.jpg" border="0" alt="Get A Full Ab Workout Done In Just 4 Minutes! Yes, Really!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/get-a-full-ab-workout-done-in-just-4-minutes-yes-really-247500.html" title="Get A Full Ab Workout Done In Just 4 Minutes! Yes, Really!">
                        Get A Full Ab Workout Done In Just 4 Minutes! Yes, Really!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/matrix-star-keanu-reeves-s-heart-wrenching-note-about-life-will-inspire-you-to-never-give-up-247597.html" class="tint" title="Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/download_1448194822_1448194828_502x234.jpg" border="0" alt="Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/matrix-star-keanu-reeves-s-heart-wrenching-note-about-life-will-inspire-you-to-never-give-up-247597.html" title="Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!">
                        Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/11-of-the-longest-non-stop-flights-in-the-world-247446.html" class="tint" title="11 Of The Longest Non-Stop Flights In The World">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/dubpam_1447837812_1447837823_502x234.jpg" border="0" alt="11 Of The Longest Non-Stop Flights In The World" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/11-of-the-longest-non-stop-flights-in-the-world-247446.html" title="11 Of The Longest Non-Stop Flights In The World">
                        11 Of The Longest Non-Stop Flights In The World                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/salman-khan-turns-down-deepika-padukone-s-marriage-proposal-could-it-get-any-worse-247594.html" class="tint" title="Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/deepika-salman_1448191918_1448191927_502x234.jpg" border="0" alt="Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/salman-khan-turns-down-deepika-padukone-s-marriage-proposal-could-it-get-any-worse-247594.html" title="Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?">
                        Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bajrangi-bhaijaan-s-munni-pulled-off-a-madhuri-dixit-for-salman-uncle-it-s-totally-adorbs-247578.html" class="tint" title="Bajrangi Bhaijaan's Munni Pulled Off A 'Madhuri Dixit' For Salman Uncle & It's Totally Adorbs!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/bm1_1448176311_1448176319_502x234.jpg" border="0" alt="Bajrangi Bhaijaan's Munni Pulled Off A 'Madhuri Dixit' For Salman Uncle & It's Totally Adorbs!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bajrangi-bhaijaan-s-munni-pulled-off-a-madhuri-dixit-for-salman-uncle-it-s-totally-adorbs-247578.html" title="Bajrangi Bhaijaan's Munni Pulled Off A 'Madhuri Dixit' For Salman Uncle & It's Totally Adorbs!">
                        Bajrangi Bhaijaan's Munni Pulled Off A 'Madhuri Dixit' For Salman Uncle & It's Totally Adorbs!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/after-kaho-na-pyaar-hai-hrithik-roshan-rakesh-roshan-to-join-hands-for-a-romantic-film-247589.html" class="tint" title="After Kaho Na Pyaar Hai, Hrithik Roshan & Rakesh Roshan To Join Hands For A Romantic Film!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/hh_1448185990_1448186003_502x234.jpg" border="0" alt="After Kaho Na Pyaar Hai, Hrithik Roshan & Rakesh Roshan To Join Hands For A Romantic Film!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/after-kaho-na-pyaar-hai-hrithik-roshan-rakesh-roshan-to-join-hands-for-a-romantic-film-247589.html" title="After Kaho Na Pyaar Hai, Hrithik Roshan & Rakesh Roshan To Join Hands For A Romantic Film!">
                        After Kaho Na Pyaar Hai, Hrithik Roshan & Rakesh Roshan To Join Hands For A Romantic Film!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/here-s-how-you-can-manage-weight-loss-according-to-your-different-body-type-247526.html" class="tint" title="Here's How You Can Manage Weight Loss According To Your Different Body Type">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-3_1448011823_502x234.jpg" border="0" alt="Here's How You Can Manage Weight Loss According To Your Different Body Type" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/here-s-how-you-can-manage-weight-loss-according-to-your-different-body-type-247526.html" title="Here's How You Can Manage Weight Loss According To Your Different Body Type">
                        Here's How You Can Manage Weight Loss According To Your Different Body Type                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html" class="tint" title="This Is What Happens When You Question Medical Students' General Knowledge!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/medicalstudetns_card_1448173900_502x234.jpg" border="0" alt="This Is What Happens When You Question Medical Students' General Knowledge!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html" title="This Is What Happens When You Question Medical Students' General Knowledge!">
                        This Is What Happens When You Question Medical Students' General Knowledge!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html" class="tint" title="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/help_card_1448178961_502x234.jpg" border="0" alt="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html" title="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!">
                        He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/there-has-to-be-a-lakshman-rekha-says-censor-board-chief-pahlaj-nihalani-on-censorship-in-films-247584.html" class="tint" title="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/censor_1448183646_1448183652_502x234.jpg" border="0" alt="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/there-has-to-be-a-lakshman-rekha-says-censor-board-chief-pahlaj-nihalani-on-censorship-in-films-247584.html" title="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films">
                        There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/this-social-networking-app-designed-by-iitians-is-helping-neighbours-connect-and-stay-safe-247580.html" class="tint" title="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ggk_1448176880_1448176907_502x234.jpg" border="0" alt="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/this-social-networking-app-designed-by-iitians-is-helping-neighbours-connect-and-stay-safe-247580.html" title="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe">
                        This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/4-life-saving-self-defence-techniques-every-women-needs-to-know-247530.html" class="tint" title="4 Life-Saving Self-Defence Techniques Every Women Needs To Know">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448009294_502x234.jpg" border="0" alt="4 Life-Saving Self-Defence Techniques Every Women Needs To Know" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/4-life-saving-self-defence-techniques-every-women-needs-to-know-247530.html" title="4 Life-Saving Self-Defence Techniques Every Women Needs To Know">
                        4 Life-Saving Self-Defence Techniques Every Women Needs To Know                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/priyanka-chopra-broke-down-on-the-sets-of-bajirao-mastani-almost-quit-the-film-247576.html" class="tint" title="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/pc_1448173395_1448173404_502x234.jpg" border="0" alt="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/priyanka-chopra-broke-down-on-the-sets-of-bajirao-mastani-almost-quit-the-film-247576.html" title="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!">
                        Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/8-heartwarming-stories-pakistani-citizens-have-had-to-share-about-india-247283.html" class="tint" title="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447419551_502x234.jpg" border="0" alt="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/8-heartwarming-stories-pakistani-citizens-have-had-to-share-about-india-247283.html" title="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India">
                        8 Heartwarming Stories Pakistani Citizens Have Had To Share About India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/a-selfie-with-kareena-kapoor-lands-chhattisgarh-cm-in-major-trouble-faces-flak-247572.html" class="tint" title="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kareena_1448170924_1448170935_502x234.jpg" border="0" alt="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/a-selfie-with-kareena-kapoor-lands-chhattisgarh-cm-in-major-trouble-faces-flak-247572.html" title="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!">
                        A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/11-simple-but-powerful-remedies-to-stop-hair-loss-in-men-247447.html" class="tint" title="11 Simple But Powerful Remedies To Stop Hair Loss In Men">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/hair-loss-card_1448002568_502x234.jpg" border="0" alt="11 Simple But Powerful Remedies To Stop Hair Loss In Men" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/11-simple-but-powerful-remedies-to-stop-hair-loss-in-men-247447.html" title="11 Simple But Powerful Remedies To Stop Hair Loss In Men">
                        11 Simple But Powerful Remedies To Stop Hair Loss In Men                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/13-foods-that-promise-a-clean-stomach-first-thing-in-the-morning-247490.html" class="tint" title="13 Foods That Promise A Clean Stomach First Thing In The Morning">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447929131_502x234.jpg" border="0" alt="13 Foods That Promise A Clean Stomach First Thing In The Morning" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/13-foods-that-promise-a-clean-stomach-first-thing-in-the-morning-247490.html" title="13 Foods That Promise A Clean Stomach First Thing In The Morning">
                        13 Foods That Promise A Clean Stomach First Thing In The Morning                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-things-you-ll-relate-to-if-you-were-raised-by-really-artsy-parents-247453.html" class="tint" title="9 Things You'll Relate To If You Were Raised By Really Artsy Parents">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cp_1447842144_502x234.jpg" border="0" alt="9 Things You'll Relate To If You Were Raised By Really Artsy Parents" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-things-you-ll-relate-to-if-you-were-raised-by-really-artsy-parents-247453.html" title="9 Things You'll Relate To If You Were Raised By Really Artsy Parents">
                        9 Things You'll Relate To If You Were Raised By Really Artsy Parents                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/8-upcoming-remakes-in-bollywood-we-cannot-wait-for-247502.html" class="tint" title="Sanjay Leela Bhansali Interested In A 'Khalnayak' Remake + 7 Upcoming Bollywood Remakes We Can't Wait For">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447935445_1447935450_218x102.jpg" border="0" alt="Sanjay Leela Bhansali Interested In A 'Khalnayak' Remake + 7 Upcoming Bollywood Remakes We Can't Wait For"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/8-upcoming-remakes-in-bollywood-we-cannot-wait-for-247502.html" title="Sanjay Leela Bhansali Interested In A 'Khalnayak' Remake + 7 Upcoming Bollywood Remakes We Can't Wait For">
                            Sanjay Leela Bhansali Interested In A 'Khalnayak' Remake + 7 Upcoming Bollywood Remakes We Can't Wait For                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/now-you-can-scale-mount-everest-from-the-comfort-of-your-home-247667.html" class="tint" title="Now You Can Scale Mount Everest From The Comfort Of Your Home">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/everest-card_1448361366_218x102.jpg" border="0" alt="Now You Can Scale Mount Everest From The Comfort Of Your Home"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/now-you-can-scale-mount-everest-from-the-comfort-of-your-home-247667.html" title="Now You Can Scale Mount Everest From The Comfort Of Your Home">
                            Now You Can Scale Mount Everest From The Comfort Of Your Home                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/shahrukh-khan-has-given-his-nod-to-imtiaz-ali-s-next-film-now-that-will-be-one-amazing-collaboration-247628.html" class="tint" title="Shahrukh Khan Has Given His Nod To Imtiaz Ali's Next Film. Now That Will Be One Amazing Collaboration!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448275825_1448275842_218x102.jpg" border="0" alt="Shahrukh Khan Has Given His Nod To Imtiaz Ali's Next Film. Now That Will Be One Amazing Collaboration!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/shahrukh-khan-has-given-his-nod-to-imtiaz-ali-s-next-film-now-that-will-be-one-amazing-collaboration-247628.html" title="Shahrukh Khan Has Given His Nod To Imtiaz Ali's Next Film. Now That Will Be One Amazing Collaboration!">
                            Shahrukh Khan Has Given His Nod To Imtiaz Ali's Next Film. Now That Will Be One Amazing Collaboration!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-can-do-to-make-your-life-simpler-247543.html" class="tint" title="11 Things You Can Do To Make Your Life Simpler">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/akshay_1448020302_218x102.jpg" border="0" alt="11 Things You Can Do To Make Your Life Simpler"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-can-do-to-make-your-life-simpler-247543.html" title="11 Things You Can Do To Make Your Life Simpler">
                            11 Things You Can Do To Make Your Life Simpler                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/whoa-is-preity-zinta-all-set-tie-the-knot-with-her-american-boyfriend-247623.html" class="tint" title="Whoa! Is Preity Zinta All Set Tie The Knot With Her American Boyfriend?">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/11058404_808070099305571_584068163314477266_o_1448271647_1448271651_218x102.jpg" border="0" alt="Whoa! Is Preity Zinta All Set Tie The Knot With Her American Boyfriend?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/whoa-is-preity-zinta-all-set-tie-the-knot-with-her-american-boyfriend-247623.html" title="Whoa! Is Preity Zinta All Set Tie The Knot With Her American Boyfriend?">
                            Whoa! Is Preity Zinta All Set Tie The Knot With Her American Boyfriend?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/moo_1448367664_1448367675_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/3005bhu-card_1448367002_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/zivadhoni_1448361311_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/team-aib502_1448360073_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/communal-card_1448363074_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447929116_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447935445_1447935450_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/everest-card_1448361366_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/potus_leanon_card_1448343280_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/baba---card_1448278866_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/cover_1448263407_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/bigg-boss-card_1448353870_1448353876_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448344926_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/su-60_1448361977_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/502_1448348330_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/smriti1-card_1448350872_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448351124_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/police-card_1448347209_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/bigg-boss-card_1448353870_1448353876_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448270762_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/apple_pencil_card1_1448355282_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448346898_1448346904_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/aza_1447939068_1447939082_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447758154_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/walk_1448274738_1448274746_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/cnvyb_1447941034_1447941039_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/snow502_1448343134_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/isis-flag-tnl_1448344363_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/502_1448340981_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/google-card_1448349326_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/1_1448292499_1448292511_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/musicalroad_1447944791_1447944802_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1448270722_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448351585_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/bengaluru_1448351087_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448346898_1448346904_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448344926_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/potus_leanon_card_1448343280_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card123_1448341604_1448341611_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448274607_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448022971_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cnvyb_1447941034_1447941039_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/cover_1448263407_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/baba---card_1448278866_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447758154_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/ttt_1448279186_1448279195_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/clf_ad_card_1448261910_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448275825_1448275842_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448275709_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/walk_1448274738_1448274746_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/srk1_1448371769_1448371776_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-1_1448262985_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/11058404_808070099305571_584068163314477266_o_1448271647_1448271651_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448262005_1448262011_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/aza_1447939068_1447939082_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-ss_1447915662_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448265317_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card_1448265960_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/anoymous_prdp_card_1448260228_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/rb123_1448256108_1448256114_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/slides_1447766047_1447766056_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/akshay_1448020302_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448001198_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card-1_1447932222_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/download_1448194822_1448194828_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/dubpam_1447837812_1447837823_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/deepika-salman_1448191918_1448191927_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/bm1_1448176311_1448176319_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/hh_1448185990_1448186003_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-3_1448011823_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/medicalstudetns_card_1448173900_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/help_card_1448178961_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/censor_1448183646_1448183652_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/ggk_1448176880_1448176907_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448009294_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/pc_1448173395_1448173404_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447419551_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/kareena_1448170924_1448170935_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/hair-loss-card_1448002568_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447929131_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cp_1447842144_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447935445_1447935450_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/everest-card_1448361366_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448275825_1448275842_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/akshay_1448020302_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/11058404_808070099305571_584068163314477266_o_1448271647_1448271651_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,945,359<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>50,733 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.6" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.6"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.6"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.6"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.6"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>