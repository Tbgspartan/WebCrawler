



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "981-7820829-1493713";
                var ue_id = "0HXXTT2G0PZNCRRM2VTW";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        

        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="0HXXTT2G0PZNCRRM2VTW" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-95791825.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2992789944._CB289247368_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['f'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['639354095701'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-2412576157._CB287435025_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"6e8f2231bd61a93b4e3dd3c0cb53e856ff63b1d4",
"2015-11-24T18%3A02%3A41GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50239;
generic.days_to_midnight = 0.5814698934555054;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'f']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=639354095701;ord=639354095701?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=639354095701?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=639354095701?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/falltv/?ref_=nv_tvv_fall_1"
>Fall TV</a></li>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_2"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_3"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_4"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_5"
>Most Popular TV Shows</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_6"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_7"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_3"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=11-24&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59228546/?ref_=nv_nw_tn_1"
> Jennifer Lawrence Named Entertainment Weekly's 2015 Entertainer of the Year
</a><br />
                        <span class="time">4 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59226009/?ref_=nv_nw_tn_2"
> Universal Backed 'By The Sea' Hoping Angelina Jolie Would Star In 'Bride Of Frankenstein' Or 'Wanted 2'
</a><br />
                        <span class="time">22 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59225655/?ref_=nv_nw_tn_3"
> âHunger Games: Mockingjay â Part 2â Box Office Predictions Lowered on Weaker Opening
</a><br />
                        <span class="time">23 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0060196/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTMxMDM1OTQxM15BMl5BanBnXkFtZTcwNDc0Mzg4Mw@@._V1._SX420_CR5,5,410,315_.jpg",
            titleYears : "1966",
            rank : 8,
                    headline : "The Good, the Bad and the Ugly"
    },
    nameAd : {
            clickThru : "/name/nm0005109/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BNDc5MTIwMzg1MF5BMl5BanBnXkFtZTcwMzg0MjQ5NQ@@._V1._SX270_CR15,0,250,315_.jpg",
            rank : 174,
            headline : "Mila Kunis"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYpYCrGbaq-E_pXcS4YUoUdisvXJHciOjsQrGCgS2vS61CnsK6obLtUKf2heAzWrbCUUX-Z-Yat%0D%0A3Oai34weN-UFu5i2094_-hYt0nMwj0YmynsFGiKkTilbDdJWB1omgszoZq0dUfzWU83NvQTDXFh2%0D%0AFxaWtGp1o4x_WVmWWDYlE3vOqCnuZ04VlwVRa9m1_aS9evtf1g2yIW6ghQLyOK-EfQ%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYnzUTcmxxZLtDEtRHm3NExKaaiQpcHN3Eotwf1AhZLatj0rxRmG6-Q00qgSQ2vXOpngnYDIVvh%0D%0ANTDgb2HOrhNRG1KVLz6tYqcFbtVvkqXRNKq4-Lyj0pjio8WNl7KkZehjzZhNfB--DnANQKW9tUtJ%0D%0AzsYPXDGuqIuAPth37s24GzcNSB1RtF1y790O0pJiJqFp%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYgIquz1rJ5DZjeMdnOPARMTIYYaMWiGVGY9Jgd_L2kHkoz1p8xi_2D2t1l8IDwrJ-yh4TabxKi%0D%0ADyT5kefpfHvUj7zgIz62RY_Cedw2Iss9UcXA1pdt4cjNm-XhMAAFzBrAK10_xZ_n3WLJVwaYkEwr%0D%0AXKoP5Xh2SuOUsm7hg1UWoe1ZvmmZFXbT2UBjWZh62HZN%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=639354095701;ord=639354095701?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=639354095701;ord=639354095701?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4210930201?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi4210930201" data-source="bylist" data-id="ls002922459" data-rid="0HXXTT2G0PZNCRRM2VTW" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="A screenwriter living in LA tries to make sense of the strange events occurring around him." alt="A screenwriter living in LA tries to make sense of the strange events occurring around him." src="http://ia.media-imdb.com/images/M/MV5BNDQ2NTM3MTAxNV5BMl5BanBnXkFtZTgwNTY1NjgzNDE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDQ2NTM3MTAxNV5BMl5BanBnXkFtZTgwNTY1NjgzNDE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A screenwriter living in LA tries to make sense of the strange events occurring around him." title="A screenwriter living in LA tries to make sense of the strange events occurring around him." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A screenwriter living in LA tries to make sense of the strange events occurring around him." title="A screenwriter living in LA tries to make sense of the strange events occurring around him." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2101383/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Knight of Cups </a> </div> </div> <div class="secondary ellipsis"> Official U.S. Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi217887257?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi217887257" data-source="bylist" data-id="ls002309697" data-rid="0HXXTT2G0PZNCRRM2VTW" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Officer Judy Hopps, the very first bunny on Zootopia's police force, jumps at the opportunity to crack her first case -- even if it means partnering with fast-talking, scam-artist fox Nick Wilde to solve the mystery." alt="Officer Judy Hopps, the very first bunny on Zootopia's police force, jumps at the opportunity to crack her first case -- even if it means partnering with fast-talking, scam-artist fox Nick Wilde to solve the mystery." src="http://ia.media-imdb.com/images/M/MV5BMjk4MTgwNDI2Ml5BMl5BanBnXkFtZTgwODU4Njg4NjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjk4MTgwNDI2Ml5BMl5BanBnXkFtZTgwODU4Njg4NjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Officer Judy Hopps, the very first bunny on Zootopia's police force, jumps at the opportunity to crack her first case -- even if it means partnering with fast-talking, scam-artist fox Nick Wilde to solve the mystery." title="Officer Judy Hopps, the very first bunny on Zootopia's police force, jumps at the opportunity to crack her first case -- even if it means partnering with fast-talking, scam-artist fox Nick Wilde to solve the mystery." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Officer Judy Hopps, the very first bunny on Zootopia's police force, jumps at the opportunity to crack her first case -- even if it means partnering with fast-talking, scam-artist fox Nick Wilde to solve the mystery." title="Officer Judy Hopps, the very first bunny on Zootopia's police force, jumps at the opportunity to crack her first case -- even if it means partnering with fast-talking, scam-artist fox Nick Wilde to solve the mystery." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2948356/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Zootopia </a> </div> </div> <div class="secondary ellipsis"> New Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1593618969?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1593618969" data-source="bylist" data-id="ls002922459" data-rid="0HXXTT2G0PZNCRRM2VTW" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="When four outsiders saw what the big banks, media and government refused to, the global collapse of the economy, they had an idea: The Big Short. Their bold investment leads them into the dark underbelly of modern banking where they must question everyone and everything." alt="When four outsiders saw what the big banks, media and government refused to, the global collapse of the economy, they had an idea: The Big Short. Their bold investment leads them into the dark underbelly of modern banking where they must question everyone and everything." src="http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="When four outsiders saw what the big banks, media and government refused to, the global collapse of the economy, they had an idea: The Big Short. Their bold investment leads them into the dark underbelly of modern banking where they must question everyone and everything." title="When four outsiders saw what the big banks, media and government refused to, the global collapse of the economy, they had an idea: The Big Short. Their bold investment leads them into the dark underbelly of modern banking where they must question everyone and everything." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="When four outsiders saw what the big banks, media and government refused to, the global collapse of the economy, they had an idea: The Big Short. Their bold investment leads them into the dark underbelly of modern banking where they must question everyone and everything." title="When four outsiders saw what the big banks, media and government refused to, the global collapse of the economy, they had an idea: The Big Short. Their bold investment leads them into the dark underbelly of modern banking where they must question everyone and everything." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1596363?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > The Big Short </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2299153462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/offsite/?page-action=offsite-amazon&token=BCYvGW1bz8nfMZzd02hETbP1NkNVp-LuGysT6wfItv-pQyesbTBixGT66xKNQfmnNaGgMJtSwC00%0D%0Ag5FxRq_Sv_eFXsCCni2eI8YplskK6OztmDORZ8GBHP3JJCW3RiQEZV2oeI_WmehYDMgIJDEBqY9h%0D%0AyCKm1RWdRgfEL53aSZH879p4hntOYunG00-VSQvQpdWwgU5o9UycyL2YAmL22hJRtAQVZRBWqdX2%0D%0AtKPbLbYa3uvfx5zZ_G1FYOBWzE1xbNMM%0D%0A&ref_=hm_hm_pop_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2292275742&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb Talks With Zachary Levi and Mena Suvari</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYu9onJ_lQxn5xcTv9kWaefWWS6qr9Ehi6i2rRMiecqOoO2B-mbfQDmMskqtIX2ovuz1kP7TKQL%0D%0AII0XnYaSl-thXRTCbUyhtDeF4kJ4nixuZDSlNv5atTeOSOiOWoFbu6xYq8roFD8BcwJpYFYakMKM%0D%0AzhCAWXP0DtECnzETICyZN4VBPcg6JIHzi149o4rFO8UK11nayul0NaQZGX3rtmDlQXV62Gy41-D6%0D%0AksezNk2Tdj91EztZF3AwAo-C2eEQx8TI%0D%0A&ref_=hm_hm_pop_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2292275742&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzA5OTc0Mzk2OV5BMl5BanBnXkFtZTgwNjE5NjAzNzE@._UX223_CR65,0,116,172_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzA5OTc0Mzk2OV5BMl5BanBnXkFtZTgwNjE5NjAzNzE@._UX223_CR65,0,116,172_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYhaGSD7gmfFNXCph5F6Ha7yjl_idvaLYnzFyB3XhdfOVSm9QW4bgO3Uj6ahcWQBx201j8XqrQg%0D%0AJIr23vcdGaNWfpXHxvyn4JHCV2xUtwhQ7h0v85bUCkxU56jfUKXvgLLFd7_6IPz7Ii6ahHshgzaj%0D%0A8oolxS5Tb58h9ZO69XxNfi10f3afeI8Sn-sATjw0Ve-kJ-2dVtQ6uruWZ7QYHi0hAcwazt1KFq5E%0D%0AnF9KfS6GYaISEPF9KzRY9OV0Xryn9s4s%0D%0A&ref_=hm_hm_pop_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2292275742&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk4OTA2MzYxNl5BMl5BanBnXkFtZTgwOTM1NDAzNzE@._SX250_CR73,13,116,172_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk4OTA2MzYxNl5BMl5BanBnXkFtZTgwOTM1NDAzNzE@._SX250_CR73,13,116,172_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <p class="blurb">On Tuesday, Nov. 24, at 7 p.m. ET/4 p.m. PT, IMDb brings you a livestream Q&A and online chat with <a href="/name/nm1157048/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2292275742&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_pop_lk1">Zachary Levi</a>, star of "<a href="/title/tt3556944/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2292275742&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_pop_lk2">Heroes Reborn</a>." Tune in to <a href="/offsite/?page-action=offsite-amazon&token=BCYtw734g9QRrkBXlKn0_D62vLLt3kldVnYIXI7MDnZDNeYCqWbpSlq273IStoBXkajGFXiNkuXk%0D%0AX6sAdAMD--3DGbFLjy4TNZxCTibYkgl8-LXkPBioOz0gDeJRtPM4zsaZFoBzYeYlZe4WU_imtnP4%0D%0A3VqqoOsPrdwu-cSpdTSXbtyKbq0COPOXh8hNPAcwPNAvsKn3EIfpHZuUxR1Ub47sTvaU6X9HM_8c%0D%0A8gICqGXQri86O7jJ9gFQc8LyuwdFlqjb%0D%0A&ref_=hm_hm_pop_lk3">Amazon.com/ZacharyLevi</a> to participate in the live conversation and even ask a question yourself. Plus, catch up with <a href="/name/nm0002546/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2292275742&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_pop_lk4">Mena Suvari</a>, star of "<a href="/title/tt4147780/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2292275742&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_pop_lk5">South of Hell</a>." The livestream is best viewed on laptops, desktops, and tablets.</p> <p class="seemore"> <a href="/offsite/?page-action=offsite-amazon&token=BCYp5qQWHunRV8YmhiknwwFBUc_pG5MVF_uv6d-aAJrqhDMr9u8UdJl00C8TlszEDOF1ZUQM4p7x%0D%0ATbPVLN3-3gqgvXSggZum9UrHi3e6377FYXkSksAlkgyEEzB60Nab63sgnB9yocb3x98IkgAskW7w%0D%0AWE94nc2i-w95nvNCL4ZHMLSVCGF8wZWgGTlV1BVkPJufiHVE6f_rt6GRL9PDEgSkrSEXMpkkx7ZP%0D%0AsV3tat3CBR3YHCTSpGQtDoywqIimBrUn%0D%0A&ref_=hm_hm_pop_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2292275742&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" > Tune in here for the one-on-one interviews </a> </p></div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Spotlight on Fall TV</h3> </a> </span> </span> <p class="blurb">Catch up on your favorite fall shows by taking a look at photos from the current seasons of "<a href="/title/tt3107288/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_lk1">The Flash</a>," "<a href="/title/tt1844624/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_lk2">American Horror Story: Hotel</a>," and "<a href="/title/tt0898266/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_lk3">The Big Bang Theory</a>."</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/the-flash-rm23126784?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="&quot;The Flash&quot;" alt="&quot;The Flash&quot;" src="http://ia.media-imdb.com/images/M/MV5BMjEwNzg1MDc2Nl5BMl5BanBnXkFtZTgwNDU5NzUyNzE@._V1_SY150_CR12,0,201,150_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEwNzg1MDc2Nl5BMl5BanBnXkFtZTgwNDU5NzUyNzE@._V1_SY150_CR12,0,201,150_AL_UY300_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/falltv/galleries/the-flash/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > "The Flash" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/american-horror-story-hotel-rm117891840?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="&quot;American Horror Story: Hotel&quot;" alt="&quot;American Horror Story: Hotel&quot;" src="http://ia.media-imdb.com/images/M/MV5BMjQzMzg1NTE5Ml5BMl5BanBnXkFtZTgwNTI2NDAzNzE@._V1_SY150_CR12,0,201,150_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQzMzg1NTE5Ml5BMl5BanBnXkFtZTgwNTI2NDAzNzE@._V1_SY150_CR12,0,201,150_AL_UY300_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/falltv/galleries/american-horror-story-hotel/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > "American Horror Story: Hotel" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/big-bang-theory-rm1937498880?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="&quot;The Big Bang Theory&quot;" alt="&quot;The Big Bang Theory&quot;" src="http://ia.media-imdb.com/images/M/MV5BMjI1MzM4NTkwMF5BMl5BanBnXkFtZTgwMzY3MzYyNzE@._V1_SY150_CR22,0,201,150_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI1MzM4NTkwMF5BMl5BanBnXkFtZTgwMzY3MzYyNzE@._V1_SY150_CR22,0,201,150_AL_UY300_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/falltv/galleries/big-bang-theory/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > "The Big Bang Theory" </a> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/falltv/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777702&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse our Fall TV guide </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59228546?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTM4OTY2MDY1M15BMl5BanBnXkFtZTcwNDYyNDM3NA@@._V1_SY150_CR1,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59228546?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Jennifer Lawrence Named Entertainment Weekly's 2015 Entertainer of the Year</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm2225369?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Jennifer Lawrence</a> has yet one more title to add to her never-ending list of accomplishments! Entertainment WeeklyÂ just announced the 25-year-old actress as their 2015 Entertainer of the Year. Sitting down with the publication, Lawrence opens up about life after her rise to immense fame, how she's ...                                        <span class="nobr"><a href="/news/ni59228546?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59226009?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Universal Backed 'By The Sea' Hoping Angelina Jolie Would Star In 'Bride Of Frankenstein' Or 'Wanted 2'</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>The Playlist</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59225655?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >âHunger Games: Mockingjay â Part 2â Box Office Predictions Lowered on Weaker Opening</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59228390?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Chiwetel Ejiofor to Be Honored at British Independent Film Awards</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59225815?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >âAmy,â âThe Hunting Groundâ Nominated for Producers Guildâs Documentary Award</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59227554?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTc5MTg2NjQ4MV5BMl5BanBnXkFtZTgwNzcxOTY5NjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59227554?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Will Pixarâs âThe Good Dinosaurâ Gobble Up Thanksgiving Box Office?</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>Animated movies from Disney like Pixarâs â<a href="/title/tt1979388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">The Good Dinosaur</a>,â which opens nationwide on Wednesday, are up there with turkey, pilgrims and green bean casseroles as Thanksgiving perennials. â<a href="/title/tt1979388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">The Good Dinosaur</a>,â in 3D and computer-animated, is about toÂ join the long list of Disney films that launched ...                                        <span class="nobr"><a href="/news/ni59227554?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59228369?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >'The Revenant' first Oscar screenings: Raves for Leonardo DiCaprio and Alejandro Gonzalez Inarritu</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Gold Derby</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59225453?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Tom Hardy's 'Taboo' begins London shoot; cast revealed</a>
    <div class="infobar">
            <span class="text-muted">23 November 2015 6:30 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0056136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>ScreenDaily</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59228584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >China beats Hollywood to the punch with unauthorised Tupac Shakur biopic</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000680?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>The Guardian - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59228390?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Chiwetel Ejiofor to Be Honored at British Independent Film Awards</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59227795?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTg1OTc2ODkyN15BMl5BanBnXkFtZTcwNDU5ODcyNA@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59227795?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Felicia Day Has Answered Joel Hodgson's Prayers and Joined the Mystery Science Theater 3000 Revival</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Vulture</a></span>
    </div>
                                </div>
<p><a href="/name/nm0388273?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Joel Hodgson</a>'s <a href="/title/tt0094517?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Mystery Science Theater 3000</a> revival has found its new villain, along with the voices behind Crow T. Robot and Tom Servo, according to a hefty update from the show's Kickstarter page. The Mst creator semi-confirmed three casting rumors behind the roles (semi because he has to wait ...                                        <span class="nobr"><a href="/news/ni59227795?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59226481?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >iZombie, Crazy Ex-Girlfriend Snag Additional Episode Orders at The CW</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59226015?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Fargo Renewed for Season 3 at FX</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59225359?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Ratings: âAmerican Music Awardsâ on ABC Solid, But Down From Last Year</a>
    <div class="infobar">
            <span class="text-muted">23 November 2015 6:08 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59227419?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >TV News Roundup: âLost in Spaceâ Gets Netflix Reboot, âPrancing Elitesâ Sets Premiere</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59228546?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTM4OTY2MDY1M15BMl5BanBnXkFtZTcwNDYyNDM3NA@@._V1_SY150_CR1,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59228546?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Jennifer Lawrence Named Entertainment Weekly's 2015 Entertainer of the Year</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm2225369?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Jennifer Lawrence</a> has yet one more title to add to her never-ending list of accomplishments! Entertainment WeeklyÂ just announced the 25-year-old actress as their 2015 Entertainer of the Year. Sitting down with the publication, Lawrence opens up about life after her rise to immense fame, how she's ...                                        <span class="nobr"><a href="/news/ni59228546?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59225438?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >The 10 Best Instagram Moments Captured at Sofia Vergara and Joe Manganiello's Wedding</a>
    <div class="infobar">
            <span class="text-muted">23 November 2015 6:19 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59225468?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Charlie Puth Finally Reveals Whether or Not He's Dating Meghan Trainor</a>
    <div class="infobar">
            <span class="text-muted">23 November 2015 6:12 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59225879?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Chris Hemsworth's Insane Weight Loss, Plus 11 Other Things We Learned From His In the Heart of the Sea Twitter Q&A</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59225936?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Matt Damon and Luciana Barrosoâ Have the Look of Love in Berlin</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4257996800/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Game of Thrones (2011-)" alt="Game of Thrones (2011-)" src="http://ia.media-imdb.com/images/M/MV5BMTYwOTEzMDMzMl5BMl5BanBnXkFtZTgwNzExODIzNzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwOTEzMDMzMl5BMl5BanBnXkFtZTgwNzExODIzNzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm4257996800/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2932596736/rg4191001344?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Chi-Raq (2015)" alt="Chi-Raq (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjIyODYxOTYyNl5BMl5BanBnXkFtZTgwODc5NzIzNzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIyODYxOTYyNl5BMl5BanBnXkFtZTgwODc5NzIzNzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2932596736/rg4191001344?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > 'Chi-Raq' Chicago Premiere </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1674371072/rg4174224128?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Doctor Who (2005-)" alt="Doctor Who (2005-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ5MDE3MDUzOV5BMl5BanBnXkFtZTgwMDgzODIzNzE@._V1_SY201_CR64,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ5MDE3MDUzOV5BMl5BanBnXkFtZTgwMDgzODIzNzE@._V1_SY201_CR64,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1674371072/rg4174224128?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2298870002&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "Doctor Who" - Latest Photos </a> </div> </div> </div> </div> </div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=11-24&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0405103?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Sarah Hyland" alt="Sarah Hyland" src="http://ia.media-imdb.com/images/M/MV5BMTc1NDg1MjU0M15BMl5BanBnXkFtZTcwMTk0NzM0Mg@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc1NDg1MjU0M15BMl5BanBnXkFtZTcwMTk0NzM0Mg@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0405103?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Sarah Hyland</a> (25) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001337?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Katherine Heigl" alt="Katherine Heigl" src="http://ia.media-imdb.com/images/M/MV5BNTM0NDEwMjA1MV5BMl5BanBnXkFtZTcwMzQxMDI3Mg@@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTM0NDEwMjA1MV5BMl5BanBnXkFtZTcwMzQxMDI3Mg@@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001337?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Katherine Heigl</a> (37) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0004988?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Colin Hanks" alt="Colin Hanks" src="http://ia.media-imdb.com/images/M/MV5BMTc5OTcxNjIzM15BMl5BanBnXkFtZTcwNDI0NjMyMw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc5OTcxNjIzM15BMl5BanBnXkFtZTcwNDI0NjMyMw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0004988?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Colin Hanks</a> (38) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0580351?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Stephen Merchant" alt="Stephen Merchant" src="http://ia.media-imdb.com/images/M/MV5BMTkyNzIwNjcxNV5BMl5BanBnXkFtZTcwOTQzMDQ1NA@@._V1_SY172_CR9,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkyNzIwNjcxNV5BMl5BanBnXkFtZTcwOTQzMDQ1NA@@._V1_SY172_CR9,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0580351?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Stephen Merchant</a> (41) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0226813?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Garret Dillahunt" alt="Garret Dillahunt" src="http://ia.media-imdb.com/images/M/MV5BNTM1MTgzNDI2OV5BMl5BanBnXkFtZTgwMDIzNzIxMDE@._V1_SY172_CR19,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTM1MTgzNDI2OV5BMl5BanBnXkFtZTgwMDIzNzIxMDE@._V1_SY172_CR19,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0226813?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Garret Dillahunt</a> (51) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=11-24&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/galleries/limitless?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lim_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>TV Spotlight: Latest "Limitless" Photos</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/limitless-rm4129809152?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lim_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Limitless (2015-)" alt="Limitless (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTcyODM3ODU5OV5BMl5BanBnXkFtZTgwNjA4OTgyNzE@._V1_SX307_CR0,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcyODM3ODU5OV5BMl5BanBnXkFtZTgwNjA4OTgyNzE@._V1_SX307_CR0,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/limitless-rm3928482560?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lim_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Limitless (2015-)" alt="Limitless (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ2NTM1MTMyNF5BMl5BanBnXkFtZTgwNDA4OTgyNzE@._V1_SX307_CR0,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2NTM1MTMyNF5BMl5BanBnXkFtZTgwNDA4OTgyNzE@._V1_SX307_CR0,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/falltv/galleries/limitless?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lim_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295777622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more photos from this season </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Indie Focus: Unusual Romance in 'Touched With Fire'</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2848324/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2296566622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2296566622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Touched With Fire (2015)" alt="Touched With Fire (2015)" src="http://ia.media-imdb.com/images/M/MV5BNzE1MzU1MDEzNl5BMl5BanBnXkFtZTgwNDM3ODkyNzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzE1MzU1MDEzNl5BMl5BanBnXkFtZTgwNDM3ODkyNzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1234940441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2296566622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2296566622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1234940441" data-rid="0HXXTT2G0PZNCRRM2VTW" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Mania Days (2015)" alt="Mania Days (2015)" src="http://ia.media-imdb.com/images/M/MV5BNDk5NzU4NDUwOV5BMl5BanBnXkFtZTgwNjU1MDMzNzE@._V1_SY250_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDk5NzU4NDUwOV5BMl5BanBnXkFtZTgwNjU1MDMzNzE@._V1_SY250_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Mania Days (2015)" title="Mania Days (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Mania Days (2015)" title="Mania Days (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb"><a href="/name/nm0005017/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2296566622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk1">Katie Holmes</a> and <a href="/name/nm0456186/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2296566622&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk2">Luke Kirby</a> star as two manic depressives who meet in a psychiatric hospital and begin a romance.</p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt1670345/trivia?item=tr2333105&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1670345?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Los ilusionistas: Nada es lo que parece (2013)" alt="Los ilusionistas: Nada es lo que parece (2013)" src="http://ia.media-imdb.com/images/M/MV5BMTY0NDY3MDMxN15BMl5BanBnXkFtZTcwOTM5NzMzOQ@@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0NDY3MDMxN15BMl5BanBnXkFtZTcwOTM5NzMzOQ@@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt1670345?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Insaisissables</a></strong> <p class="blurb">After The 4 Horsemen get arrested, Merrit McKinley (<a href="/name/nm0000437?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Woody Harrelson</a>) tells the FBI agent there's no shame for a man to wear a dress. This is a reference to <a href="/name/nm0393890?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk2">J. Edgar Hoover</a>, the long time director of the FBI, who was revealed to be a cross dresser after his death.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt1670345/trivia?item=tr2333105&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-25"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;Na_SrApqAA0&quot;}">
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/Na_SrApqAA0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: With what family would you LEAST like to spend the holidays?</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Na_SrApqAA0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Addams Family (1964-1966)" alt="The Addams Family (1964-1966)" src="http://ia.media-imdb.com/images/M/MV5BMjE5OTQyMjgwNl5BMl5BanBnXkFtZTgwNTkwNzc3MjE@._V1_SY207_CR68,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE5OTQyMjgwNl5BMl5BanBnXkFtZTgwNTkwNzc3MjE@._V1_SY207_CR68,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Na_SrApqAA0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="This Is Where I Leave You (2014)" alt="This Is Where I Leave You (2014)" src="http://ia.media-imdb.com/images/M/MV5BMjkzNzQ2NDMyNl5BMl5BanBnXkFtZTgwMTY3MTcxMjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjkzNzQ2NDMyNl5BMl5BanBnXkFtZTgwMTY3MTcxMjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Na_SrApqAA0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Squid and the Whale (2005)" alt="The Squid and the Whale (2005)" src="http://ia.media-imdb.com/images/M/MV5BMTg1NTE4NTc3NV5BMl5BanBnXkFtZTgwNDA1NDI1MDE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg1NTE4NTc3NV5BMl5BanBnXkFtZTgwNDA1NDI1MDE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Na_SrApqAA0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Rachel Getting Married (2008)" alt="Rachel Getting Married (2008)" src="http://ia.media-imdb.com/images/M/MV5BMjc1MTQ5MDI4NF5BMl5BanBnXkFtZTcwMzMxNDQxNw@@._V1_SY207_CR117,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjc1MTQ5MDI4NF5BMl5BanBnXkFtZTcwMzMxNDQxNw@@._V1_SY207_CR117,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Na_SrApqAA0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Parenthood (1989)" alt="Parenthood (1989)" src="http://ia.media-imdb.com/images/M/MV5BMTYzMTQzOTU0MV5BMl5BanBnXkFtZTYwMzQwNjM2._V1_SY207_CR49,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYzMTQzOTU0MV5BMl5BanBnXkFtZTYwMzQwNjM2._V1_SY207_CR49,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Which of these movie/TV families would you least want to spend your holidays with? Discuss <a href="http://www.imdb.com/board/bd0000088/thread/237363473?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"> <a href="/poll/Na_SrApqAA0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295118462&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=639354095701;ord=639354095701?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=639354095701?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=639354095701?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1979388"></div> <div class="title"> <a href="/title/tt1979388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> The Good Dinosaur </a> <span class="secondary-text"></span> </div> <div class="action"> Opens 11/25 </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3076658"></div> <div class="title"> <a href="/title/tt3076658?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Creed </a> <span class="secondary-text"></span> </div> <div class="action"> Opens 11/25 </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1976009"></div> <div class="title"> <a href="/title/tt1976009?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Victor Frankenstein </a> <span class="secondary-text"></span> </div> <div class="action"> Opens 11/25 </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0810819"></div> <div class="title"> <a href="/title/tt0810819?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> The Danish Girl </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3707114"></div> <div class="title"> <a href="/title/tt3707114?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Janis: Little Girl Blue </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4498460"></div> <div class="title"> <a href="/title/tt4498460?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Killing Them Safely </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495382&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295495482&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1951266"></div> <div class="title"> <a href="/title/tt1951266?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Hunger Games: Mockingjay - Part 2 </a> <span class="secondary-text">$102.7M</span> </div> <div class="action"> <a href="/showtimes/title/tt1951266?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2379713"></div> <div class="title"> <a href="/title/tt2379713?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Spectre </a> <span class="secondary-text">$15.0M</span> </div> <div class="action"> <a href="/showtimes/title/tt2379713?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2452042"></div> <div class="title"> <a href="/title/tt2452042?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> The Peanuts Movie </a> <span class="secondary-text">$13.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3530002"></div> <div class="title"> <a href="/title/tt3530002?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> The Night Before </a> <span class="secondary-text">$9.9M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1741273"></div> <div class="title"> <a href="/title/tt1741273?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Secret in Their Eyes </a> <span class="secondary-text">$6.7M</span> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2884018"></div> <div class="title"> <a href="/title/tt2884018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Macbeth </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2884018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3850590"></div> <div class="title"> <a href="/title/tt3850590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Krampus </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4594834"></div> <div class="title"> <a href="/title/tt4594834?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Chi-Raq </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1445208"></div> <div class="title"> <a href="/title/tt1445208?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> The Letters </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3312830"></div> <div class="title"> <a href="/title/tt3312830?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Youth </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/video/imdb/vi1190244889/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295455782&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_wtw_mithc_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295455782&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>What to Watch: "The Man in the High Castle"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1190244889?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295455782&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_wtw_mithc_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295455782&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1190244889" data-rid="0HXXTT2G0PZNCRRM2VTW" data-type="single" class="video-colorbox" data-refsuffix="hm_wtw_mithc" data-ref="hm_wtw_mithc_i_1"> <img itemprop="image" class="pri_image" title="IMDb: What to Watch (2013-)" alt="IMDb: What to Watch (2013-)" src="http://ia.media-imdb.com/images/M/MV5BMTUwODI1NjExM15BMl5BanBnXkFtZTgwODI4MTMxNzE@._V1_SY525_CR29,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwODI1NjExM15BMl5BanBnXkFtZTgwODI4MTMxNzE@._V1_SY525_CR29,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> <img alt="IMDb: What to Watch (2013-)" title="IMDb: What to Watch (2013-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb: What to Watch (2013-)" title="IMDb: What to Watch (2013-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="http://www.imdb.com/video/imdb/vi1190244889/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295455782&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_wtw_mithc_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295455782&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > "The Man in the High Castle" </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">In this episode of "What to Watch," we talk with the cast of Amazon Studios' "The Man in the High Castle" and the show's creator, Frank Spotnitz.</p> <p class="seemore"> <a href="http://www.imdb.com/video/imdb/vi1190244889/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295455782&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_wtw_mithc_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2295455782&pf_rd_r=0HXXTT2G0PZNCRRM2VTW&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Watch our interview </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYhrcp_qXD8kQB2K72J8hbE3a4g0EL_ove64OHCOQup_gL8b-dPua-8pscCB_90OxOn6x1KR5k7%0D%0AxX9aLuHVCdoKhZbS4EuWSVAV7AHQieN_KoaXUUXJiLSmSqNNdgXtXGyR2t31XD4eKVT-SVBt5esZ%0D%0A-5Gy3OdgTc3k9RAWkx2O6NuoFRru_260C3FfR5HOh64gCe5heAl4ZzOS0QZu8lkFGJ613HmGdJkl%0D%0AzI2dPwX5Zrw%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYhpXa3eqKTQSZ7NRq9Riqo9x2SNb3fzeDIQPueAknnb5VajmkvTvGVRJGVh5VIH7xqKUN0TcQA%0D%0AVhGB0zaKicMeKp5k2lpk1cg4AaT2tPYGY9ce-5RWRi-kJzl-5w7kqJEogRxhz_wyXnzgm3FlJPxH%0D%0AF1LJmfbo7pxGXsJP4MYnQBQyzY9jCHirMnldct1LZRQEFl4abNXOGEYWyqk0JOBeqA%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYtbiyx9vRV_8WeAIEydRvBdHvkezKUTBnLJ_Mj1k0OLXJMCiOVJ0gvCRLN_jDYoaRK7T-Kf2UK%0D%0A4X6XPGaBpoe8ZgFX-8C7iczaLbaQYdbJCL15CphFJqaMks3Y5uq6meqqDWmCWJsudDowAnpvTyzh%0D%0A_7ox35OtK3djfFXiQNwpxapF9BRTfUcXfAmOSA0spKOevCmzvWVjPxDDrggRpRu8czakA6BPE5-N%0D%0Au655CHO8uA27OU6n6AcP7_gB3LS-AI3TPnSKnjhcCe0o6wEyPal2BDHE-txKnj1qJHHZf8Cn6K45%0D%0Abco3gCP4w_fLvxiGuqYPFwIFwLyurE1EmdGM7-h8T-Ho6D5iwE0thDPkjw3arYPRr-CCCNhgSEvE%0D%0AsqwlfPclcB_HzsY9be-aL7fPA0zlnw%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYiTQdW5vDkvQEu-UhoDb7w5sGH2pybsl2Rw8TmJan2FKu98xmFCUuw0CpBhT2MNro_3dE4f5ui%0D%0AA1Fql69RkTKMuB1tFyP7GxPT_4j46yN1HY_IGUKGIpNqgTXGNAMbGyVP3YEnMh8zKdDAA4zaQE_U%0D%0Atdm-1h7whzqj4e5-emcaJ_CiOGbCkdTDILGimHvbwb5FCjrniyYM2qI7ePnzjiWXbA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYu1rc94IzyHPXLi8t5t07RMfsu9cwzZLL_5Jn0kyAvlstRvFnC2oxfcRk2DSqz-epm8ri63o6e%0D%0AsdC8thJUKywjbcLoEsF_4xFd8beqjnieQs3wye704DaHfmlYAp5yiKnwAMY9C3UfzsxnSZcErV1t%0D%0AEJYOmXBBkiTN3-1XZz60qwmQZ6HcxqpWSQfko4tZVK0w%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYgAbnhSlJIo7q9GidGUvpJkWuLXP8xxDeCCBU2rsXRa6Z8ZMZ0b-lv27fJmTDXySWRpK49upcr%0D%0Amh7q6fRPoMq6x5CszXiuEgLY3Nsbn7M1bwGjantvvxhenbTmC0YxJQty6AzNAmzLhPFzNJfkofi5%0D%0AC4ILyIwtMrfGKCqhYN4eTrlackrFTgX08EfYGSrbUKpb%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYgvhXp7Qjx2-SextAOfB5e1S0I1DZCzbAuVsWJeOMOy0SX8gL4uGkRxqUvDBKOxPaA8SkMfapJ%0D%0AIQ8ydW6nvGPrAkSyHl_CZTYDDPTY6hWDZfU3iXIE1dciUhq6z-ls7RPv2ihmRDLEhrfu-lJxObxw%0D%0AoQVBiF5h0XsPiLT0Y9vUrfsw0PXDETYsLuGk98aznL33DUioLt3-wW4QLNxfaJxNqw%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYmNYo_GIU9cFjmJEZHktUCvhhI6BRruy_o-EM2sFQxTGVkM33g6pfriWBi75OGenaVfcTl_2ho%0D%0A14RRbEOViZ_ds5gXdahnnkeoa3D--GesXwzqj3FYAhvrTTMhvY9QxGWbBkAdIGT-da5PiyTELqUm%0D%0A2wgQdnbTJXSt1ECxkTdD3VdMzbw0FxoIdBzJZgtyAbYG%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYj9zSe_rlmoAJe-jO2aOPYiCDCj4unvE2_KZjP_P8ScV6jBoSEQIDM4I5N7Xdk-I-vvq4h93z8%0D%0AAKgIp2djoHPZuMWVizUmTdzkzunKtlueB-UxpDonDMq4FmDjqWxZRvLAPHDxibGtKDAXf2e5pN5j%0D%0AgnWgGtQ4HcC7xbjxsN15zNW3uBOEBgrfNO3pvoGlGoya1d7PGYLIYOYREKBjb9wTs86GuCu61fVF%0D%0A7FmHwfx6kUuJWjikqgwKzm6IYelJDvkv%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYngJa6tC_99zSoBfvvAZhqW34Dt5SKV6h2EesITovalsJb15PJFr8jQu5F23I8DAnoaafVG66W%0D%0AMPtFtHNQUb4GLcDnDla_xRaZ0EHKfkxtPHAWemImuURdnm97l7_zZXQhxMBGStC9CF20siVs-THh%0D%0AykndqrQa70SCqNTteJBPzYz0b5Jm5jWETwOEnrCd0kBEMP-LEO66PmW2WySU2DQ6ZNU-y56uemRm%0D%0APzOhH3UyNjc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYh7Y21wiW9DnLAX5i_ypW0IsgMRlp2cqPA1mcL3RRtTFpw3a5-yvRKvkigxmTSpdFiBLE5DBwy%0D%0ABiYnnjGAmx2fluIogVMFzA1wOrI7TTgIGGcgGwBaUAmQHjZptBjf67sdGfbEhYJDvQgEP2CAI0z_%0D%0ABJyjidUr-IPFShL178LC7g4pFXz9j4uF7XwZBRp8mOf3AW_YG3CXX2vheUDpDUqLhsCCqIxGygYY%0D%0A-yYM6DQZggs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYjrEHTaUthBu0NE7FdRzfRUhF0IMeovzS85BRvj4ln_cgx1EN3JhspMmDaGrumzl6XlPAw_pUh%0D%0Aj49rdBfV19e7FHJmpZYHVOOH3_ii_yTkyLiPeiRik1GuvhT1BnGGUS3jgF902o8XvokNWI8aE-CF%0D%0A9MjeNNqeYW3eoeO_2cL3Ev6ggQkWcBAOuz0Xgi1LYemHkdbbUjS8bjRai7DCnb_IPtfD1NuV60tv%0D%0A1c4tx_2J8Qs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYi8dvUeH1vkZkURbjV4EM2twNxtOXZH_LA65lp8w_b2zTL-RYzxZuJ_DCKhVK5z471aDNbA7CZ%0D%0A_yE-4hmPfbGbJVkvLWcpEcywZWKV1rYWJ_90yRyEr2Pl4GsWsM40_7oIggLvc1emUJL9Zmr_Qkyn%0D%0AO4dr_bN2r7OzN2vdVmnUVFIYZkMk2gV04ChZlyBHWr6G_A10iZ3_LPKKwb3nsWS2jVmWAX8TDvWw%0D%0AcGAeSs7JIns%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYmkJ2t9nuRB1JzOoy7yj4UmIQD3J-McnWigCUmWqTKNM0X_dmBoXJUp-Q_SGkZYM_Gw8_Djp2d%0D%0AoVZiOK110YFocm-F615JYbgvWNh2k2GEb3O9AVctxJBdoNXPXbcVmAgbPdeOKQnAlEzKR3EIwt6Q%0D%0AIV-BR8WFxvoxFs1T2v7UtETjEu7TRoS0XDnVV7WmBOfU01v_yEaAVALk35d8SJG-nJpx4a-w_JGO%0D%0AYDPqoepjI0I%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYh1qhicDexe1tHyu8T7TFVhRaCkJYj20MTvQC0iWpCeYnFodh9z_pUgsuiFXfOeQYnpu0W1sht%0D%0AQ8LrRMwIQg6MCenYUfO8gE0WIipTXTcXtXNAiFyUMHVD5FfInWybDxk_EBHhRUJ2yGapiaPWtIti%0D%0AaemFj1nN-ayUSJmHenHapQ_pXLtqvzCx6Uj8C9CDnKyOc46637_ihsZLs7PijxqUeLltIj74j8Rz%0D%0Ak2N4WiW-Ou5lo0tuedUvdGobKpQJm5tu%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYkDN11UwsUj0CoXdWM6IXutRHLr0aTHyO7k1-fpRAqz9Aj_Hhr7gtohT-YSDHhbocoV2sz6xuT%0D%0AFnvK4uNt3DXC_bXMpT087OyZosQQz8ubHAgfYhi0qT_yl0Xp149dE7rSixolqHSS-8s-lvDr7lK5%0D%0AWi-zJrrl3ZpKcvDd2A5sQw8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYlStTXlpMAiusnv9dHXa-n6Scx-KKbnmqsFtCUOuEMmLLFw0y6lEKLx7MUvERqy2cFhhaGRkpm%0D%0ABHokjGxIgoWhcUY3vp6dO6jBfM-zFDoCWKCo5v1XdPECcuSM_NlG1MnCiQuApXsQPky90Ksz7_YG%0D%0AKck16T3YPBIKDub2TY67xi4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-4155841819._CB289323147_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2028687744._CB289323184_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=01015eb71ec5d3e954c9a9575dea0507b8c1665e732f7c450dd9a4671dc3e935336f",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=639354095701"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=639354095701&ord=639354095701";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="333"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
