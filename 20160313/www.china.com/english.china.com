ï»¿<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>China.com - Your guide on traveling and living in China</title>
<meta name="keywords" content="China,travel,lifestyle,learn Chinese,news,videos,business,films,sports,reports" />
<meta name="description" content="English.china.com is a one-stop shop for everything about China â news, events, culture, people, lifestyle, language. It also provides information about traveling and living in China." />
<meta name="auther" content="F7 13489" />
<!-- /etc/htmlhead.shtml Start -->
<link href="/css/style.css?20141205.1" rel="stylesheet" />
<!--[if lte IE 6]> 
<script src="/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.page-select-language-list em, .page-nav .page-openSearch em, .page-focus-prevnext a, .page-video-right .item-text, .page-watched .play, .page-video-right .item-icon, .page-video-right .item-watch'); 
</script>
<![endif]-->
<!-- /etc/htmlhead.shtml End -->
<!-- /etc/goto3g.shtml Start -->

<!-- /etc/goto3g.shtml End -->
</head>

<body>
<!-- /etc/channelhomehead.shtml Start -->
<div class="page-head">
  <div class="page-top-bg" id="page-top">
    <div class="maxWidth page-top">
      <div class="page-logo"><a href="/index.html"><img src="/img/logo.png" alt="china.com" width="208" height="46" /></a></div>
      <div class="page-language">
        <em>language:English</em>
      </div>
      <div class="page-top-right">
          <div class="page-top-time">Thursday, October 24, 2013</div>
          <div class="page-select-language" id="page-select-language">
              <div class="page-show-language" style="display: block;">Language</div>
              <div class="page-select-language-tit" style="display: none;"><i>Language</i></div>
              <div class="page-select-language-list" style="display: none;">
                <a href="http://www.china.com/index.html" class="langCn"><em>www</em></a>
                <a href="http://english.china.com/index.html" class="langEn"><em>english</em></a>
                <a href="http://german.china.com/index.html" class="langDe"><em>german</em></a>
                <a href="http://italy.china.com/index.html" class="langIt"><em>italy</em></a>
                <a href="http://portuguese.china.com/index.html" class="langPt"><em>portuguese</em></a>
                <a href="http://french.china.com/index.html" class="langFr"><em>french</em></a>
                <a href="http://russian.china.com/index.html" class="langRu"><em>russian</em></a>
                <a href="http://espanol.china.com/index.html" class="langEs"><em>espanol</em></a>
                <a href="http://malay.china.com/index.html" class="langMy"><em>malay</em></a>
                <a href="http://vietnamese.china.com/index.html" class="langVn"><em>vietnamese</em></a>
                <a href="http://laos.china.com/index.html" class="langLa"><em>laos</em></a>
                <a href="http://cambodian.china.com/index.html" class="langKh"><em>cambodian</em></a>
                <a href="http://thai.china.com/index.html" class="langTh"><em>thai</em></a>
                <a href="http://indonesian.china.com/index.html" class="langId"><em>indonesian</em></a>
                <a href="http://filipino.china.com/index.html" class="langPh"><em>filipino</em></a>
                <a href="http://myanmar.china.com/index.html" class="langMm"><em>myanmar</em></a>
                <a href="http://japanese.china.com/index.html" class="langJp"><em>japanese</em></a>
                <a href="http://korean.china.com/index.html" class="langKr"><em>korean</em></a>
                <a href="http://mongol.china.com/index.html" class="langMn"><em>mongol</em></a>
                <a href="http://nepal.china.com/index.html" class="langNp"><em>nepal</em></a>
                <a href="http://hindi.china.com/index.html" class="langIn"><em>hindi</em></a>
                <a href="http://bengali.china.com/index.html" class="langMd"><em>bengali</em></a>
                <a href="http://turkish.china.com/index.html" class="langTr"><em>turkish</em></a>
                <a href="http://persian.china.com/index.html" class="langIr"><em>persian</em></a>
                <a href="http://arabic.china.com/index.html" class="langAe"><em>arabic</em></a>
              </div>
            </div>
            <div class="page-search">
              <form id="web-search" name="web-search" method="get" action="http://www.google.com/search">
                <input type="hidden" name="sitesearch" value="english.china.com" />
                <input type="text" name="q" class="web-search-keyword" placeholder="Search here..." />
                <input type="submit" name="button" class="web-search-but" value="Search Site" />
              </form>
            </div>
      </div>
    </div>
  </div>
  <div class="page-nav-bg" id="page-nav">
    <ul class="page-nav maxWidth">
      <li class="page-openSearch"><a href="#"><em>Open search</em></a></li>
      <li class="small-logo"><a href="/index.html"><img src="/img/small-logo.png" /></a></li>
      <li><a href="/news/index.html">News</a></li>
      <li><a href="/video/index.html">Video</a></li>
      <!--<li><a href="/audio/index.html">Audio</a></li>-->
      <!--<li><a href="/photos/index.html">Photos</a></li>-->
      <li><a href="/travel/index.html">Travel</a></li>
      <!--<li><a href="/lifestyle/index.html">Lifestyle</a></li>-->
      <li><a href="/chinese/index.html">Learn Chinese</a></li>
	  <li><a href="http://english.china.com/cityguide/">City Guide</a></li>
      <li><a href="http://mail.china.com/en/" class="ext" target="_blank">Free Mail</a></li>
    </ul>
  </div>
</div><!-- page-head End -->
<!-- /etc/channelhomehead.shtml End -->

<div class="maxWidth">
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 #17286  Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=206" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 End -->
</div>

<div class="page-main maxWidth">
  <div class="page-left">
    <div class="page-focus" id="page-focus">
      <div class="page-focus-body" id="page-focus-body">
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2016-03/13/c_135182447.htm" title="Maslenitsa celebrated in Moscow to welcome spring"><img src="http://img04.mini.abroad.imgcdc.com/english/news/topphotos/world/1208/20160313/595650_155927.jpg.680x330.jpg" width="565" height="250" alt="Maslenitsa celebrated in Moscow to welcome spring" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2016-03/13/c_135182447.htm" title="Maslenitsa celebrated in Moscow to welcome spring" class="title_default">Maslenitsa celebrated in Moscow to welcome spring</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/03/12/4261s920096.htm" title="Cherry Blossoms of Taiziwan Park, E China"><img src="http://img04.mini.abroad.imgcdc.com/english/news/topphotos/china/189/20160312/595164_155827.jpg.680x330.jpg" width="680" height="330" alt="Cherry Blossoms of Taiziwan Park, E China" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/03/12/4261s920096.htm" title="Cherry Blossoms of Taiziwan Park, E China" class="title_default">Cherry Blossoms of Taiziwan Park, E China</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/03/10/4082s919957.htm" title="Multi-level Overpass Built in Southwest China"><img src="http://img02.mini.abroad.imgcdc.com/english/home/topphoto/1295/20160311/594497_155593.jpg.680x330.jpg" width="680" height="330" alt="Multi-level Overpass Built in Southwest China" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/03/10/4082s919957.htm" title="Multi-level Overpass Built in Southwest China" class="title_default">Multi-level Overpass Built in Southwest China</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/03/11/3685s919996.htm" title="Young Chinese Player Fancies Chances Against AlphaGo"><img src="http://img02.mini.abroad.imgcdc.com/english/home/topphoto/1295/20160311/594462_155585.jpg.680x330.jpg" width="680" height="330" alt="Young Chinese Player Fancies Chances Against AlphaGo" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/03/11/3685s919996.htm" title="Young Chinese Player Fancies Chances Against AlphaGo" class="title_default">Young Chinese Player Fancies Chances Against AlphaGo</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/03/10/3685s919851.htm" title="3,492 Reflector Panels of China's Mega Telescope Installed"><img src="http://img04.mini.abroad.imgcdc.com/english/home/topphoto/1295/20160310/593691_155359.jpg.680x330.jpg" width="680" height="330" alt="3,492 Reflector Panels of China's Mega Telescope Installed" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/03/10/3685s919851.htm" title="3,492 Reflector Panels of China's Mega Telescope Installed" class="title_default">3,492 Reflector Panels of China's Mega Telescope Installed</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
      </div>
      <div id="page-focus-console"></div>
      <div class="page-focus-prevnext">
        <a href="#" id="page-focus-prev">Previous focus</a>
        <a href="#" id="page-focus-next">Next focus</a>
      </div>
    </div><!-- page-focus End -->
    
    <div class="page-latest">
      <h2 class="modTit"><strong><a href="/news/index.html">LATEST NEWS</a></strong></h2>
      <div class="page-latest-body" id="page-latest">
        <div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/13/4001s920284.htm" title="'Star Wars Episode VIII' Unveils Behind-the-scenes Photo"><img src="http://img03.mini.abroad.imgcdc.com/english/news/showbiz/58/20160313/596009_155998.jpg.200x120.jpg" width="200" height="120" alt="'Star Wars Episode VIII' Unveils Behind-the-scenes Photo" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/03/13 20:35:00</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/13/4001s920284.htm" title="'Star Wars Episode VIII' Unveils Behind-the-scenes Photo" class="title_default">'Star Wars Episode VIII' Unveils Behind-the-scenes Photo</a></h3>
            <p class="item-infor" title="Director Rian Johnson thrilled Star Wars fans by sharing a behind-the-scenes photo from the filming of 'Episode VIII' on his Tumblr page.

">Director Rian Johnson thrilled Star Wars fans by sharing a behind-the-scenes photo from the filming of 'Episode VIII' on his Tumblr page.

</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/13/4161s920257.htm" title="Changchun Team Gives Gold Commemoration Medals to Soccer Olympians"><img src="http://img02.mini.abroad.imgcdc.com/english/news/sports/57/20160313/596006_155997.jpg.200x120.jpg" width="200" height="120" alt="Changchun Team Gives Gold Commemoration Medals to Soccer Olympians" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/03/13 20:31:09</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/13/4161s920257.htm" title="Changchun Team Gives Gold Commemoration Medals to Soccer Olympians" class="title_default">Changchun Team Gives Gold Commemoration Medals to Soccer Olympians</a></h3>
            <p class="item-infor" title="Changchun club of Chinese women's league announced on Sunday that they will present pure gold commemorative medals to their members in Chinese national team, who had anchored a berth for the Rio Olympics.

">Changchun club of Chinese women's league announced on Sunday that they will present pure gold commemorative medals to their members in Chinese national team, who had anchored a berth for the Rio Olympics.

</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-03/12/c_135181862.htm" title="Risks in China's Banking Industry Controllable: CBRC Chairman"><img src="http://img04.mini.abroad.imgcdc.com/english/news/business/56/20160313/596004_155995.jpg.200x120.jpg" width="200" height="120" alt="Risks in China's Banking Industry Controllable: CBRC Chairman" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/03/13 20:28:22</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-03/12/c_135181862.htm" title="Risks in China's Banking Industry Controllable: CBRC Chairman" class="title_default">Risks in China's Banking Industry Controllable: CBRC Chairman</a></h3>
            <p class="item-infor" title="Risks in China's banking industry are generally controllable and the authorities will work to prevent systemic financial risks, the head of the country's banking regulator said Saturday.
">Risks in China's banking industry are generally controllable and the authorities will work to prevent systemic financial risks, the head of the country's banking regulator said Saturday.
</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/13/4201s920269.htm" title="Chinese Student Robbed in Melbourne's Chaotic Night"><img src="http://img01.mini.abroad.imgcdc.com/english/news/world/55/20160313/596002_155992.jpg.200x120.jpg" width="200" height="120" alt="Chinese Student Robbed in Melbourne's Chaotic Night" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/03/13 20:22:49</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/13/4201s920269.htm" title="Chinese Student Robbed in Melbourne's Chaotic Night" class="title_default">Chinese Student Robbed in Melbourne's Chaotic Night</a></h3>
            <p class="item-infor" title="A Chinese student has been robbed in Melbourne, Australia's second-biggest city, while a street gang clash erupted in the city's CBD, sources confirmed on Sunday.

">A Chinese student has been robbed in Melbourne, Australia's second-biggest city, while a street gang clash erupted in the city's CBD, sources confirmed on Sunday.

</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-03/13/c_135183862.htm" title="Chinese Judiciary Vows to Keep up "High Pressure" on Corruption"><img src="http://img03.mini.abroad.imgcdc.com/english/news/china/54/20160313/595997_155990.jpg.200x120.jpg" width="200" height="120" alt="Chinese Judiciary Vows to Keep up "High Pressure" on Corruption" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/03/13 20:19:25</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-03/13/c_135183862.htm" title="Chinese Judiciary Vows to Keep up "High Pressure" on Corruption" class="title_default">Chinese Judiciary Vows to Keep up "High Pressure" on Corruption</a></h3>
            <p class="item-infor" title="China's judicial authorities said Sunday that the country will continue to maintain "high pressure" on corruption as authorities press on with a sweeping anti-graft drive.
">China's judicial authorities said Sunday that the country will continue to maintain "high pressure" on corruption as authorities press on with a sweeping anti-graft drive.
</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/13/2702s920209.htm" title="Zootopia Sets Single-day Animation Record"><img src="http://img02.abroad.imgcdc.com/english/news/showbiz/58/20160313/595654_155929_200x120.jpg" width="200" height="120" alt="Zootopia Sets Single-day Animation Record" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/03/13 08:59:17</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/13/2702s920209.htm" title="Zootopia Sets Single-day Animation Record" class="title_default">Zootopia Sets Single-day Animation Record</a></h3>
            <p class="item-infor" title="Disney charmer Zootopia has drawn the highest single-day gross ever for an animated film in China on Saturday.">Disney charmer Zootopia has drawn the highest single-day gross ever for an animated film in China on Saturday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/business/tech/2016-03/12/content_23835359.htm" title="WeChat set to launch app for enterprise users"><img src="http://img01.mini.abroad.imgcdc.com/english/news/business/56/20160313/595652_155928.jpg.200x120.jpg" width="200" height="129" alt="WeChat set to launch app for enterprise users" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/03/13 08:57:50</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/business/tech/2016-03/12/content_23835359.htm" title="WeChat set to launch app for enterprise users" class="title_default">WeChat set to launch app for enterprise users</a></h3>
            <p class="item-infor" title="The app, named Qiye Weixin or Enterprise WeChat in English, is under beta testing and is expected to be rolled out in the next couple of months.">The app, named Qiye Weixin or Enterprise WeChat in English, is under beta testing and is expected to be rolled out in the next couple of months.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/world/AlphaGo-takes-30-lead-in-historic-match-with-Lee-Sedol/shdaily.shtml" title="AlphaGo takes 3-0 lead in historic match with Lee Sedol"><img src="" width="" height="" alt="AlphaGo takes 3-0 lead in historic match with Lee Sedol" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/03/13 08:56:14</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/world/AlphaGo-takes-30-lead-in-historic-match-with-Lee-Sedol/shdaily.shtml" title="AlphaGo takes 3-0 lead in historic match with Lee Sedol" class="title_default">AlphaGo takes 3-0 lead in historic match with Lee Sedol</a></h3>
            <p class="item-infor" title="GOOGLE'S computer program AlphaGo took a 3-0 lead as it won the third game in a historic five-game match with South Korean Lee Sedol, the world's champion of the ancient Chinese board game.">GOOGLE'S computer program AlphaGo took a 3-0 lead as it won the third game in a historic five-game match with South Korean Lee Sedol, the world's champion of the ancient Chinese board game.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/12/4001s920181.htm" title="Ben Affleck and Henry Cavill Promote New Superhero Film in Beijing"><img src="http://img02.mini.abroad.imgcdc.com/english/news/showbiz/58/20160312/595599_155909.jpg.200x120.jpg" width="200" height="120" alt="Ben Affleck and Henry Cavill Promote New Superhero Film in Beijing" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/03/12 20:27:21</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/12/4001s920181.htm" title="Ben Affleck and Henry Cavill Promote New Superhero Film in Beijing" class="title_default">Ben Affleck and Henry Cavill Promote New Superhero Film in Beijing</a></h3>
            <p class="item-infor" title="Director Zack Snyder teamed up with Ben Affleck and Henry Cavill, bringing their new film 'Batman v Superman: Dawn of Justice' to China's capital city.

">Director Zack Snyder teamed up with Ben Affleck and Henry Cavill, bringing their new film 'Batman v Superman: Dawn of Justice' to China's capital city.

</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/12/2702s920120.htm" title="na's Lin Dan Beats Jorgensen to Meet Teammate Xue at All England Semis"><img src="http://img01.mini.abroad.imgcdc.com/english/news/sports/57/20160312/595598_155908.jpg.200x120.jpg" width="200" height="120" alt="na's Lin Dan Beats Jorgensen to Meet Teammate Xue at All England Semis" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/03/12 20:23:54</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/12/2702s920120.htm" title="na's Lin Dan Beats Jorgensen to Meet Teammate Xue at All England Semis" class="title_default">na's Lin Dan Beats Jorgensen to Meet Teammate Xue at All England Semis</a></h3>
            <p class="item-infor" title="Five times champion Lin Dan fought back from one set down to beat third seed Jan O Jorgensen of Denmark in the quarterfinals of the All England badminton championships on Friday. 

">Five times champion Lin Dan fought back from one set down to beat third seed Jan O Jorgensen of Denmark in the quarterfinals of the All England badminton championships on Friday. 

</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/12/3381s920135.htm" title="China Can Meet Growth Targets without Stimulus: PBOC Governor"><img src="http://img04.mini.abroad.imgcdc.com/english/news/business/56/20160312/595597_155907.jpg.200x120.jpg" width="200" height="120" alt="China Can Meet Growth Targets without Stimulus: PBOC Governor" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/03/12 20:20:03</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/12/3381s920135.htm" title="China Can Meet Growth Targets without Stimulus: PBOC Governor" class="title_default">China Can Meet Growth Targets without Stimulus: PBOC Governor</a></h3>
            <p class="item-infor" title="The growth targets China has set for this year and the 13th Five-Year Plan period (2016-2020) can be realized through improving domestic demand, consumption and innovation, without big stimulus, central bank governor said Saturday">The growth targets China has set for this year and the 13th Five-Year Plan period (2016-2020) can be realized through improving domestic demand, consumption and innovation, without big stimulus, central bank governor said Saturday</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/12/3801s920125.htm" title="400 Migrants Stranded on Macedonia-Serbia Border"><img src="http://img03.mini.abroad.imgcdc.com/english/news/world/55/20160312/595596_155906.jpg.200x120.jpg" width="200" height="120" alt="400 Migrants Stranded on Macedonia-Serbia Border" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/03/12 20:17:43</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/12/3801s920125.htm" title="400 Migrants Stranded on Macedonia-Serbia Border" class="title_default">400 Migrants Stranded on Macedonia-Serbia Border</a></h3>
            <p class="item-infor" title="Some 400 refugees are now stranded in a camp on the border between Macedonia and Serbia.

">Some 400 refugees are now stranded in a camp on the border between Macedonia and Serbia.

</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-03/12/c_135181654.htm" title="No Massive Layoffs Expected in SOE Reform: Official"><img src="http://img02.mini.abroad.imgcdc.com/english/news/china/54/20160312/595594_155905.jpg.200x120.jpg" width="200" height="120" alt="No Massive Layoffs Expected in SOE Reform: Official" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/03/12 20:12:11</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-03/12/c_135181654.htm" title="No Massive Layoffs Expected in SOE Reform: Official" class="title_default">No Massive Layoffs Expected in SOE Reform: Official</a></h3>
            <p class="item-infor" title="China will not experience another upsurge in layoffs like the one seen in the 1990s during the ongoing reform to revitalize inefficient and overstaffed state-owned enterprises (SOE), a senior official said Saturday.
">China will not experience another upsurge in layoffs like the one seen in the 1990s during the ongoing reform to revitalize inefficient and overstaffed state-owned enterprises (SOE), a senior official said Saturday.
</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/12/2743s920111.htm" title="Tropical Tibet Builds Roots for Blooming Ecotourism"><img src="http://img03.abroad.imgcdc.com/english/news/china/54/20160312/595167_155830_200x120.jpg" width="200" height="120" alt="Tropical Tibet Builds Roots for Blooming Ecotourism" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/03/12 08:49:50</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/12/2743s920111.htm" title="Tropical Tibet Builds Roots for Blooming Ecotourism" class="title_default">Tropical Tibet Builds Roots for Blooming Ecotourism</a></h3>
            <p class="item-infor" title="With jagged mountain ranges, lush forests and rare tropical plants, it's no wonder Nyingchi was chosen by ancient Tibetans to settle almost 5,000 years ago.">With jagged mountain ranges, lush forests and rare tropical plants, it's no wonder Nyingchi was chosen by ancient Tibetans to settle almost 5,000 years ago.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/2016-03/12/content_23834656.htm" title="Biggest cruise ship tests waters"><img src="http://img02.mini.abroad.imgcdc.com/english/news/world/55/20160312/595166_155829.jpg.200x120.jpg" width="160" height="103" alt="Biggest cruise ship tests waters" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/03/12 08:46:48</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/2016-03/12/content_23834656.htm" title="Biggest cruise ship tests waters" class="title_default">Biggest cruise ship tests waters</a></h3>
            <p class="item-infor" title="The Harmony of the Seas, the world's largest cruise ship, set off on Thursday on its first sea trial from Saint-Nazaire in western France, with just two months to go before delivery.">The Harmony of the Seas, the world's largest cruise ship, set off on Thursday on its first sea trial from Saint-Nazaire in western France, with just two months to go before delivery.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-03/11/c_135179807.htm" title="China snatches four synchro titles at FINA Diving World Series in Beijing"><img src="http://img01.abroad.imgcdc.com/english/news/sports/57/20160312/595165_155828_200x120.jpg" width="200" height="120" alt="China snatches four synchro titles at FINA Diving World Series in Beijing" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/03/12 08:45:30</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-03/11/c_135179807.htm" title="China snatches four synchro titles at FINA Diving World Series in Beijing" class="title_default">China snatches four synchro titles at FINA Diving World Series in Beijing</a></h3>
            <p class="item-infor" title="The Chinese divers wrapped up four gold medals in synchro events on offer while Ukraine took two silvers and Britain one silver and one bronze on the first day of the FINA/NVC Diving World Series.">The Chinese divers wrapped up four gold medals in synchro events on offer while Ukraine took two silvers and Britain one silver and one bronze on the first day of the FINA/NVC Diving World Series.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/11/4001s920065.htm" title="Top Actress Zhao Wei's Second Directorial Work Starts Filming"><img src="http://img03.mini.abroad.imgcdc.com/english/news/showbiz/58/20160311/595045_155782.jpg.200x120.jpg" width="200" height="120" alt="Top Actress Zhao Wei's Second Directorial Work Starts Filming" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/03/11 20:38:08</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/11/4001s920065.htm" title="Top Actress Zhao Wei's Second Directorial Work Starts Filming" class="title_default">Top Actress Zhao Wei's Second Directorial Work Starts Filming</a></h3>
            <p class="item-infor" title="Chinese actress Zhao Wei held an opening ceremony in Beijing, marking the start of filming for her second directorial work 'No Other Love.'

">Chinese actress Zhao Wei held an opening ceremony in Beijing, marking the start of filming for her second directorial work 'No Other Love.'

</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/sports/2016-03/11/content_23821664.htm" title="Liverpool Beats Man United 2-0 in Europa League"><img src="http://img02.mini.abroad.imgcdc.com/english/news/sports/57/20160311/595042_155781.jpg.200x120.jpg" width="200" height="120" alt="Liverpool Beats Man United 2-0 in Europa League" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/03/11 20:35:50</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/sports/2016-03/11/content_23821664.htm" title="Liverpool Beats Man United 2-0 in Europa League" class="title_default">Liverpool Beats Man United 2-0 in Europa League</a></h3>
            <p class="item-infor" title="Liverpool outclassed Manchester United but relied on two defensive blunders to win 2-0 in a high-profile all-English match in the Europa League last 16 on Thursday.">Liverpool outclassed Manchester United but relied on two defensive blunders to win 2-0 in a high-profile all-English match in the Europa League last 16 on Thursday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/11/4081s920061.htm" title="Smartphone-based VR Goggles on HTC Agenda"><img src="http://img03.mini.abroad.imgcdc.com/english/news/business/56/20160311/595034_155778.jpg.200x120.jpg" width="200" height="120" alt="Smartphone-based VR Goggles on HTC Agenda" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/03/11 20:32:29</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/11/4081s920061.htm" title="Smartphone-based VR Goggles on HTC Agenda" class="title_default">Smartphone-based VR Goggles on HTC Agenda</a></h3>
            <p class="item-infor" title="Taiwan-based electronic product manufacturer HTC is working on developing smartphone-based virtual reality goggles.

">Taiwan-based electronic product manufacturer HTC is working on developing smartphone-based virtual reality goggles.

</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/03/11/3561s920068.htm" title="Japan Tsunami Remembered Five Years On"><img src="http://img04.mini.abroad.imgcdc.com/english/news/world/55/20160311/595032_155775.jpg.200x120.jpg" width="200" height="120" alt="Japan Tsunami Remembered Five Years On" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/03/11 20:30:48</em><em class="hide">March 14 2016 02:46:16</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/03/11/3561s920068.htm" title="Japan Tsunami Remembered Five Years On" class="title_default">Japan Tsunami Remembered Five Years On</a></h3>
            <p class="item-infor" title="Japan is marking the fifth anniversary of the earthquake and tsunami that left more than 18,000 dead or missing. 

">Japan is marking the fifth anniversary of the earthquake and tsunami that left more than 18,000 dead or missing. 

</p>
          </div>
        </div>
      </div>
      <div class="page-latest-more">
        <a href="#" id="page-latest-show-more"><em class="page-latest-more-icon">&nbsp;</em></a>
        <a href="/news/index.html" id="page-latest-click-more"><em class="page-latest-more-icon">Show More</em></a>
      </div>
    </div><!-- page-latest End -->
    <div class="page-left-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=207" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 End -->
	</div> 
    <div class="page-video">
      <h2 class="modTit"><strong><a href="/video/index.html">VIDEO</a></strong></h2>
      <div class="page-video-body">
        <div class="page-video-left">
          <script type="text/javascript" src="http://c.wrating.com/v2_pre.js"></script>
<!--noscript-->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="420" height="234">
            <param name="movie" value="http://english.china.com/videoPlayer/video.swf"/>
            <param name="quality" value="high"/>
            <param name="bgcolor" value="#ffffff"/>
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="true"/>
            <param name="wmode" value="Opaque">
            <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/mychineselife/2015/12/1230joshweb.mp4&loop=1&autoplay=0"/>
            <!--[if!IE]>
            -->
            <object type="application/x-shockwave-flash" data="http://english.china.com/videoPlayer/video.swf" width="420" height="234">
              <param name="quality" value="high"/>
              <param name="bgcolor" value="#ffffff"/>
              <param name="allowScriptAccess" value="sameDomain"/>
              <param name="allowFullScreen" value="true"/>
              <param name="wmode" value="Opaque">
              <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/mychineselife/2015/12/1230joshweb.mp4&loop=1&autoplay=0"/>
              <!--<![endif]-->
              <!--[if gte IE 6]>
              -->
              <p>
                Either scripts and active content are not permitted to run or Adobe Flash Player version 11.4.0 or greater is not installed.
              </p>
              <!--<![endif]-->
              <a href="http://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player"/>
              </a>
              <!--[if!IE]>--></object>
            <!--<![endif]-->
          </object>
          <!--/noscript-->
          <a href="/home/videobig/1299/20151231/537959.html" class="video-tit">Along the Silk Road: Josh Summers--Rediscover Xinjiang</a>
        </div>
        <div class="page-video-right" id="page-video-right">
          <div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20160128/559220.html"><img src="http://img02.abroad.imgcdc.com/english/home/videosmall/1301/20160128/559293_147037.jpg" width="245" height="125" alt="Along the Silk Road: Elise Anderson--An American Xinjiang Idol" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20160128/559220.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road: Elise Anderson--An American Xinjiang Idol</strong></h3>
              <p class="item-infor">Elise Anderson became a local celebrity because of her passion for the Uyghur performing arts. </p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div><div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20151231/537864.html"><img src="http://img03.abroad.imgcdc.com/english/home/videosmall/1301/20151231/537873_141302.jpg" width="245" height="125" alt="Along the Silk Road: Joy Bostwick--The Silk Road Artist" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20151231/537864.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road: Joy Bostwick--The Silk Road Artist</strong></h3>
              <p class="item-infor">American artist documents the beauty of Xinjiang with her paintings.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div><!-- page-video End -->
    <div class="page-mods">
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/travel/index.html">TRAVEL</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners"><img src="http://img03.mini.abroad.imgcdc.com/english/travel/listright/mostpopular/1534/20150506/366181_88898.jpg.330x190.jpg" width="330" height="190" alt="Food Awards 2015: the winners" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners" class="title_default">Food Awards 2015: the winners</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12394/2015/08/11/2743s891245.htm" title="Japanese War Orphan Recounts Past" class="title_default">Japanese War Orphan Recounts Past</a></li><li><a href="http://english.cri.cn/6566/2014/12/25/44s858261.htm" title="Winter Nadam Kicks Off in North China" class="title_default">Winter Nadam Kicks Off in North China</a></li><li><a href="http://english.cri.cn/6566/2014/09/29/44s845986.htm" title="To Experience Authentic Taiwan Folk Art in Beijing" class="title_default">To Experience Authentic Taiwan Folk Art in Beijing</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/chinese/index.html">LEARN CHINESE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love"><img src="http://img03.mini.abroad.imgcdc.com/english/home/learnpic/1315/20141124/211729_51886.jpg.330x190.jpg" width="330" height="190" alt="China's First Love" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love" class="title_default">China's First Love</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="/news/china/54/20160225/578841.html" title="Spring Festival: Through My Lens" class="title_default">Spring Festival: Through My Lens</a></li><li><a href="http://english.cri.cn/12514/2014/10/16/2001s848110.htm" title="Top 10 Popular Chinese TV Dramas Overseas" class="title_default">Top 10 Popular Chinese TV Dramas Overseas</a></li><li><a href="http://english.cri.cn/12514/2014/10/17/2001s848240.htm" title="çµç¶ Chinese Pipa" class="title_default">çµç¶ Chinese Pipa</a></li>
          </ul>
        </div>

      </div>
      
      
    </div><!-- page-mods End -->
    <!--<div class="page-left-ad"><a href="#"><img src="/file/left-ad-2.jpg" /></a></div> page-left-ad End -->
    <!-- page-photos End -->
  </div><!-- page-left End -->
  <div class="page-right">
    
    <div class="page-right-ad noMarginTop">
      <!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=208" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 End -->
    </div><!-- page-right-ad End -->
    <!--include virtual="/etc/right_top_ad_index.shtml" -->
      
    ï»¿<!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://english.cri.cn/12394/2015/09/21/Zt2821s896890.htm"><img src="http://img04.abroad.imgcdc.com/english/home/imgtj/5829/20151014/482283_124639.jpg" width="293" height="88" alt="20151014" /></a><a href="http://english.cri.cn/12394/2015/09/02/Zt2821s894283.htm"><img src="http://img01.abroad.imgcdc.com/english/home/imgtj/5829/20150908/482287_124640.jpg" width="293" height="88" alt="20150908" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->

    <!-- page-right-ad End -->
    
    <div class="page-watched">
      <h2 class="modTit"><strong>Most Watched</strong></h2>
      <div class="page-watched-body" id="rank-video">
      </div>
    </div><!-- page-watched End -->
    <div class="page-popular">
      <h2 class="modTit"><strong>Most Popular</strong></h2>
      <ul class="page-popular-body" id="rank-list"></ul>
    </div><!-- page-popular End -->
    <div class="page-right-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=209" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 End -->
	</div> 
    <div class="page-tochina">
      <h2 class="modTit"><strong>Tune in to China</strong></h2>
      <div class="page-tochina-body">
        <div class="item radio-news">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="title">-</h3>
              <h4 id="playtime">-</h4>
              <a href="http://english.cri.cn/7146/2012/12/03/301s736372.htm" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/am846.wsx" class="item-play"></a>
          </div>
        </div>
        <div class="item radio-ez">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="nexttitle">-</h3>
              <h4 id="nexttime">-</h4>
              <a href="http://english.cri.cn/easyfm/ezplaytime.html" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/fm915.wsx" class="item-play"></a>
          </div>
        </div>
      </div>
    </div><!-- page-tochina End -->

    
    <div class="page-hotListening">
      <h2 class="page-hotListening-tit"><strong>Hot Listening</strong></h2>
      <div class="page-hotListening-body">
        <div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/pik.htm"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4933_1370.jpg" width="120" height="90" alt="People in the Know" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/pik.htm" class="title_default">People in the Know</a><a href="http://english.cri.cn/cribb/plus/pik.htm" class="icon-horn"></a></h3>
            <p class="item-infor">PIK gives you insights to the world through interviews.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/today.htm"><img src="http://img02.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4931_1369.jpg" width="120" height="90" alt="Today" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/today.htm" class="title_default">Today</a><a href="http://english.cri.cn/cribb/plus/today.htm" class="icon-horn"></a></h3>
            <p class="item-infor">A news magazine show with in-depth panel discussions.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/easymorning.html"><img src="http://img01.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4928_1368.jpg" width="120" height="90" alt="EZ Morning" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/easymorning.html" class="title_default">EZ Morning</a><a href="http://english.cri.cn/easyfm/easymorning.html" class="icon-horn"></a></h3>
            <p class="item-infor">It lights up your mornings with interesting chit-chats.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/ezwheel.html"><img src="http://img04.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4927_1367.jpg" width="120" height="90" alt="More to Learn" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/ezwheel.html" class="title_default">More to Learn</a><a href="http://english.cri.cn/easyfm/ezwheel.html" class="icon-horn"></a></h3>
            <p class="item-infor">More to Learn is filled up with English stories and anecdotes.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/hour.html"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4923_1366.jpg" width="120" height="90" alt="The Beijing Hour" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/hour.html" class="title_default">The Beijing Hour</a><a href="http://english.cri.cn/easyfm/hour.html" class="icon-horn"></a></h3>
            <p class="item-infor">It opens up the world to you with latest news updates.</p>
          </div>
        </div>
      </div>
    </div><!-- page-hotListening End -->

    <div class="page-mobile">
      <h2 class="modTit"><strong>Mobile</strong></h2>
      <div class="page-mobile-body">
        <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=539404062&mt=8&s=143441" class="iphone" title="Mobile Iphone">Iphone</a>
      </div>
    </div><!-- page-mobile End -->

    <div class="page-cooperation">
      <h2 class="modTit"><strong>Cooperation</strong></h2>
      <div class="page-cooperation-body">
        <a href="http://gc.wrating.com/click.php?a=&c=860099-1000099998&cs=341_285_2559_860010_400000000&ds=354_355_356_357_358&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dmobi.mgeek.TunnyBrowser%26referrer%3Dchannel_id%253Dchinacom%2526utm_source%253Dchinacom"><img src="/file/logo-dolphin-140-90.png" height="90" width="140"></a>
      </div>
    </div><!-- page-cooperation End -->

  </div><!-- page-right End -->
</div><!-- page-main End -->

<script>
window.collectMethod_rank = window.collectMethod_rank || [];
collectMethod_rank.push(function () {
  // setRank("rank-video", 3, "http://english.china.com/js/english_43_day.js", "video", function(){
  //   setRank("rank-list", 5, "http://english.china.com/js/english_22_day.js", "list");
  // });

  setRank2015("rank-video", 3, "day_top", "194", "http://rank.china.com/rank/cri/english/day/rank.js", "video", function(){
    setRank2015("rank-list", 5, "day_top", "104", "http://rank.china.com/rank/cri/english/day/rank.js", "list");
  });
});
</script>

<!-- /etc/channelsitemap.shtml Start -->
<div class="page-map">
  <div class="page-map-body maxWidth">
  <dl class="item">
      <dt><a href="http://english.china.com/news/index.html">News:</a></dt>
      <dd>
        <a href="http://english.china.com/news/china/index.html">China</a>
        <a href="http://english.china.com/news/world/index.html">World</a>
        <a href="http://english.china.com/news/business/index.html">Business</a>
        <a href="http://english.china.com/news/sports/index.html">Sports</a>
        <a href="http://english.china.com/news/showbiz/index.html">Showbiz</a>
		<a href="http://english.china.com/audio/index.html">Audio</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/video/index.html">Video:</a></dt>
      <dd>
        <a href="http://english.china.com/video/c4/index.html">C4</a>
        <a href="http://english.china.com/video/life/index.html">My Chinese Life</a>
        <a href="http://english.china.com/video/thesoundstage/index.html">The Sound Stage</a>
        <a href="http://english.china.com/video/chinarevealed/index.html">China Revealed</a>
        <a href="http://english.china.com/video/showbiz/index.html">Showbiz Video</a>
        <a href="http://english.china.com/video/tour/index.html">Travel Video</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/photos/index.html">Photos:</a></dt>
      <dd>
        <a href="http://english.china.com/photos/china/index.html">China</a>
        <a href="http://english.china.com/photos/world/index.html">World</a>
        <a href="http://english.china.com/photos/fun/index.html">Fun</a>
        <a href="http://english.china.com/photos/travel/index.html">Travel</a>
        <a href="http://english.china.com/photos/entertainment/index.html">Entertainment</a>
        <a href="http://english.china.com/photos/sports/index.html">Sports</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/travel/index.html">Travel:</a></dt>
      <dd>
        <a href="http://english.china.com/travel/beijing/index.html">Beijing</a>
        <a href="http://english.china.com/travel/shanghai/index.html">Shanghai</a>
        <a href="http://english.china.com/travel/guangzhou/index.html">Guangzhou</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/lifestyle/index.html">Lifestyle:</a></dt>
      <dd>        
        <a href="http://english.china.com/lifestyle/livemusic/index.html">Live Music</a>
        <a href="http://english.china.com/lifestyle/opera/index.html">Opera & Classical</a>
        <a href="http://english.china.com/lifestyle/movies/index.html">Movies</a>
        <a href="http://english.china.com/lifestyle/traditionalshows/index.html">Traditional Shows</a>
        <a href="http://english.china.com/lifestyle/exhibitions/index.html">Exhibitions</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/chinese/index.html">Learn Chinese:</a></dt>
      <dd>
        <a href="http://english.china.com/chinese/studio/index.html">Chinese Studio</a>
        <a href="http://english.china.com/chinese/living/index.html">Living Chinese</a>
        <a href="http://english.china.com/chinese/everyday/index.html">Everyday Chinese</a>
        <a href="http://english.china.com/chinese/justforfun/index.html">Just For Fun</a>
        <a href="http://english.china.com/chinese/culture/index.html">Chinese Culture</a>
        <a href="http://english.china.com/chinese/buzzwords/index.html">Buzzwords</a>        
      </dd>
    </dl>      
  </div>
</div><!-- page-map End -->
<!-- /etc/channelsitemap.shtml End -->

<div class="page-link">
  <div class="page-link-body maxWidth">
    <a href="http://english.cri.cn/">CRIENGLISH.com</a>|<a href="http://www.chinadaily.com.cn/">China Daily</a>|<a href="http://www.xinhuanet.com/english/">Xinhua</a>|<a href="http://www.china.org.cn/index.htm">China.org.cn</a>|<a href="http://english.cntv.cn/">CNTV</a>
  </div>
</div><!-- page-link End -->

<!-- /etc/channelcopyright.shtml Start -->
<div class="page-footer">
  <div class="page-foot-link">
    <a href="/about/">About China.com</a>|<a href="/about/gmg.html">About GMG</a>|<a href="/ad/">Ad Services</a>|<a href="/contact/">Contact Information</a>|<a href="/copyright/">Copyright Notice</a>
  </div>
  <p><!--E-mail to:<a href="mailto:english@bj.china.com">english@bj.china.com</a><br />-->Copyright &copy; China.com All Rights Reserved</p>
</div><!-- page-footer End -->

<script src="/js/require.min.js" data-main="/js/main"></script>

<!-- START WRating v1.0 -->
<script type="text/javascript" src="http://c.wrating.com/a1.js">
</script>
<script type="text/javascript">
var vjAcc="860010-0446010000";
var wrUrl="http://c.wrating.com/";
vjTrack("");
</script>
<noscript><img src="http://c.wrating.com/a.gif?a=&c=860010-0446010000" width="1" 
height="1"/></noscript>
<!-- END WRating v1.0 -->

<!-- Start Alexa Certify Javascript #13481-->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S6Upi1awA+00a/", domain:"china.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S6Upi1awA+00a/" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript #13481-->


<!-- Start Google Analytics #16010-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60581848-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics #16010-->

<!-- /etc/channelcopyright.shtml End -->
</body>
</html>