



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='21/08/2015 21:58:58' /><meta property='busca:modified' content='21/08/2015 21:58:58' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/1bb8d3bb72f3.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/12ba0da8a716.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "globo.com/globo.com/home", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-nacional/">
                                                <span class="titulo">Jornal Nacional</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                <span class="titulo">Copa do Brasil</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/tenis/">
                                                <span class="titulo">TÃªnis</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-america/">
                                                    <span class="titulo">Copa AmÃ©rica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/receitas/">
                                                <span class="titulo">Receitas.com</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/esquenta/">
                                                    <span class="titulo">Esquenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/pe-na-cova/index.html">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-08-2122:03:49Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/pr/parana/noticia/2015/08/camargo-correa-firma-acordo-para-devolucao-de-r-700-milhoes.html" class=" " title="Camargo CorrÃªa firma acordo para devolver R$ 700 mi"><div class="conteudo"><h2>Camargo CorrÃªa firma acordo para devolver R$ 700 mi</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/blog/cristiana-lobo/post/temer-recomenda-serenidade-cunha.html" class=" " title="Temer pede para Eduardo Cunha ter &#39;serenidade&#39;"><div class="conteudo"><h2>Temer pede para Eduardo Cunha ter &#39;serenidade&#39;</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/08/stf-manda-notificar-cunha-e-collor-sobre-denuncias.html" class=" " title="Cunha e Collor tÃªm 15 dias para defesa no STF (O Globo)"><div class="conteudo"><h2>Cunha e Collor tÃªm 15 dias para defesa no STF</h2></div></a></div></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/brasil/gilmar-mendes-manda-pgr-investigar-se-campanha-de-dilma-recebeu-dinheiro-da-petrobras-17268856?" class=" " title="Ministro do TSE pede investigaÃ§Ã£o das contas de campanha de Dilma"><div class="conteudo"><h2>Ministro do TSE pede investigaÃ§Ã£o das contas de campanha de Dilma</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Conselho arquiva reclamaÃ§Ã£o de Lula contra procurador" href="http://g1.globo.com/politica/noticia/2015/08/conselho-do-mp-arquiva-reclamacao-de-lula-contra-procurador.html">Conselho arquiva reclamaÃ§Ã£o de Lula contra procurador</a></div></li></ul></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/verdades-secretas/Vem-por-ai/noticia/2015/08/larissa-e-roy-alugam-quarto-na-cracolandia.html" class="foto " title="Casal se muda para cracolÃ¢ndia (Felipe Monteiro / Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/AAGGfBD5aMtegARSc3Dk9EUl1qg=/filters:quality(10):strip_icc()/s2.glbimg.com/sCkfZ_Gd_9SbOlKl3gqUo-U9fHg=/54x25:649x275/155x65/s.glbimg.com/et/gs/f/original/2015/08/20/larissa-roy-cracolandia.jpg" alt="Casal se muda para cracolÃ¢ndia (Felipe Monteiro / Gshow)" title="Casal se muda para cracolÃ¢ndia (Felipe Monteiro / Gshow)"
         data-original-image="s2.glbimg.com/sCkfZ_Gd_9SbOlKl3gqUo-U9fHg=/54x25:649x275/155x65/s.glbimg.com/et/gs/f/original/2015/08/20/larissa-roy-cracolandia.jpg" data-url-smart_horizontal="TMs5KrVh8EWQUB5ruCdrK5sVwbw=/90x56/smart/filters:strip_icc()/" data-url-smart="TMs5KrVh8EWQUB5ruCdrK5sVwbw=/90x56/smart/filters:strip_icc()/" data-url-feature="TMs5KrVh8EWQUB5ruCdrK5sVwbw=/90x56/smart/filters:strip_icc()/" data-url-tablet="6h_IrZCZkuScSbBk4O-YTiGyW58=/160xorig/smart/filters:strip_icc()/" data-url-desktop="PoCnfxN8jYGTD_keaDn0C_3y3Ls=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Casal se muda para cracolÃ¢ndia</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Carol descobre mentira" href="http://extra.globo.com/tv-e-lazer/telinha/verdades-secretas-angel-fica-com-alex-mente-para-carolina-mas-ela-descobre-17248953.html">Carol descobre mentira</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/babilonia/Vem-por-ai/noticia/2015/08/ultimos-capitulos-beatriz-sequestra-regina-e-mata-carlos.html" class="foto " title="&#39;BabilÃ´nia&#39;: vilÃ£ faz nova vÃ­tima (Fabio Rocha/Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/_qcTT5zx_YP8yT9-RmxIyBp6v68=/filters:quality(10):strip_icc()/s2.glbimg.com/JDI8zgth5kqZyUKFUXn2CaUpd8w=/53x65:553x275/155x65/s.glbimg.com/et/gs/f/original/2015/08/19/02carlos.jpg" alt="&#39;BabilÃ´nia&#39;: vilÃ£ faz nova vÃ­tima (Fabio Rocha/Gshow)" title="&#39;BabilÃ´nia&#39;: vilÃ£ faz nova vÃ­tima (Fabio Rocha/Gshow)"
         data-original-image="s2.glbimg.com/JDI8zgth5kqZyUKFUXn2CaUpd8w=/53x65:553x275/155x65/s.glbimg.com/et/gs/f/original/2015/08/19/02carlos.jpg" data-url-smart_horizontal="jLHkOS60UMUYj9_45cQYrTIaB1E=/90x56/smart/filters:strip_icc()/" data-url-smart="jLHkOS60UMUYj9_45cQYrTIaB1E=/90x56/smart/filters:strip_icc()/" data-url-feature="jLHkOS60UMUYj9_45cQYrTIaB1E=/90x56/smart/filters:strip_icc()/" data-url-tablet="fKn5F1-bLBMJTLSywPosGTS465k=/160xorig/smart/filters:strip_icc()/" data-url-desktop="PN1vqPQ6sesN1pOSRUyN550Tghs=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;BabilÃ´nia&#39;: vilÃ£ faz nova vÃ­tima</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Estela desmascara Bia" href="http://extra.globo.com/tv-e-lazer/telinha/babilonia-estela-descobre-que-beatriz-mandou-matar-evandro-17249277.html">Estela desmascara Bia</a></div></li></ul></div></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/08/morte-de-pm-no-rio-pode-ter-ligacao-com-mulheres-milicia-e-jogo-do-bicho.html" class="foto " title="PM pode ter sido morto por bater na ex ou traiÃ§Ã£o (ReproduÃ§Ã£o/Instagram)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/bepDtax8Cuq8eA8itRN3dnr0ewo=/filters:quality(10):strip_icc()/s2.glbimg.com/xfkBmOJi3bTb3hlVNWzE77nx1jo=/29x25:363x297/155x126/s.glbimg.com/jo/g1/f/original/2015/08/20/pmmorto.jpg" alt="PM pode ter sido morto por bater na ex ou traiÃ§Ã£o (ReproduÃ§Ã£o/Instagram)" title="PM pode ter sido morto por bater na ex ou traiÃ§Ã£o (ReproduÃ§Ã£o/Instagram)"
         data-original-image="s2.glbimg.com/xfkBmOJi3bTb3hlVNWzE77nx1jo=/29x25:363x297/155x126/s.glbimg.com/jo/g1/f/original/2015/08/20/pmmorto.jpg" data-url-smart_horizontal="-23t-RziHEDRB6_Mup7SRacMNjQ=/90x56/smart/filters:strip_icc()/" data-url-smart="-23t-RziHEDRB6_Mup7SRacMNjQ=/90x56/smart/filters:strip_icc()/" data-url-feature="-23t-RziHEDRB6_Mup7SRacMNjQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="T9uANsZTlsM6Dta6_BzAvksSxH8=/160xorig/smart/filters:strip_icc()/" data-url-desktop="cmog8ZOVYs5q5pxVbJvekB-Bw-s=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>PM pode ter sido morto por bater na ex ou traiÃ§Ã£o</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/08/incendio-atinge-vegetacao-do-morro-da-urca-ao-lado-do-pao-de-acucar.html" class="foto " title="IncÃªndio antige Morro da Urca, no RJ; assista (ReproduÃ§Ã£o / Enviado por Whatsapp)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/0XxgCYvEb6llvdjyW0rOe8E8BM4=/filters:quality(10):strip_icc()/s2.glbimg.com/mG0cZfnj1M3Pr4Kx9V_t7Sdk6Vo=/52x94:474x438/155x126/s.glbimg.com/jo/g1/f/original/2015/08/21/fogourca.jpg" alt="IncÃªndio antige Morro da Urca, no RJ; assista (ReproduÃ§Ã£o / Enviado por Whatsapp)" title="IncÃªndio antige Morro da Urca, no RJ; assista (ReproduÃ§Ã£o / Enviado por Whatsapp)"
         data-original-image="s2.glbimg.com/mG0cZfnj1M3Pr4Kx9V_t7Sdk6Vo=/52x94:474x438/155x126/s.glbimg.com/jo/g1/f/original/2015/08/21/fogourca.jpg" data-url-smart_horizontal="HXUCTXh2vbl_7W_mjR_3l4T4ahc=/90x56/smart/filters:strip_icc()/" data-url-smart="HXUCTXh2vbl_7W_mjR_3l4T4ahc=/90x56/smart/filters:strip_icc()/" data-url-feature="HXUCTXh2vbl_7W_mjR_3l4T4ahc=/90x56/smart/filters:strip_icc()/" data-url-tablet="NInZKcRY1-2gz_4OH-pni506ecg=/160xorig/smart/filters:strip_icc()/" data-url-desktop="pHDP-1kf2kSF7gPdpZdle6b1ZG4=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>IncÃªndio antige Morro da Urca, no RJ; assista</h2></div></a></div></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/noticia/2015/08/pontos-corridos-75-dos-campeoes-mantiveram-tecnico-ate-virada-de-turno.html" class=" " title="Pontos corridos: 75% dos campeÃµes mantiveram treinador atÃ© virar turno"><div class="conteudo"><h2>Pontos corridos: 75% dos campeÃµes mantiveram treinador atÃ© virar turno</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Vasco precisa trilhar caminho que sÃ³ dois conseguiram" href="http://globoesporte.globo.com/futebol/times/vasco/noticia/2015/08/vasco-precisa-trilhar-caminho-que-so-dois-conseguiram-nos-pontos-corridos.html">Vasco precisa trilhar caminho que sÃ³ dois conseguiram</a></div></li></ul></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/no-ar/central-do-mercado-futebol-internacional.html#/glb-feed-post/55d78cb1c22d626957658ea0" class="foto " title="Neymar e United tÃªm &#39;conversa secreta&#39;, diz site (ReproduÃ§Ã£o / Twitter)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/JM8HT_yDcP-CPDFmfCcQZ5Ay0YY=/filters:quality(10):strip_icc()/s2.glbimg.com/J7WQKlrA-Gk_FlWjENX3g8T_qOs=/0x13:940x438/155x70/s.glbimg.com/es/ge/f/original/2015/08/21/reproducao_the_sun_neymar_manchester_united.png" alt="Neymar e United tÃªm &#39;conversa secreta&#39;, diz site (ReproduÃ§Ã£o / Twitter)" title="Neymar e United tÃªm &#39;conversa secreta&#39;, diz site (ReproduÃ§Ã£o / Twitter)"
         data-original-image="s2.glbimg.com/J7WQKlrA-Gk_FlWjENX3g8T_qOs=/0x13:940x438/155x70/s.glbimg.com/es/ge/f/original/2015/08/21/reproducao_the_sun_neymar_manchester_united.png" data-url-smart_horizontal="p0QCCR4608DMdde15txUo30ypRk=/90x56/smart/filters:strip_icc()/" data-url-smart="p0QCCR4608DMdde15txUo30ypRk=/90x56/smart/filters:strip_icc()/" data-url-feature="p0QCCR4608DMdde15txUo30ypRk=/90x56/smart/filters:strip_icc()/" data-url-tablet="GyA_Jy_AFNylXqYpARz4YxErdSM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="OYmYEDR6WLL1pZydi9sjF9aTUCI=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Neymar e United tÃªm &#39;conversa secreta&#39;, diz site</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/no-ar/central-do-mercado-futebol-internacional.html#/glb-feed-post/55d78f2cc22d6269453f2852" class="foto " title="Pogba vai para o Chelsea por R$ 378 mi, diz jornal (JOHANNES EISELE / AFP)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/yQRRwjp-aengQEn7w7Y4okNSii0=/filters:quality(10):strip_icc()/s2.glbimg.com/ZgPvIstpl8yvJ7qOq_x-BChZXA0=/1656x1470:4696x2844/155x70/s.glbimg.com/es/ge/f/original/2015/08/08/000_del6435982.jpg" alt="Pogba vai para o Chelsea por R$ 378 mi, diz jornal (JOHANNES EISELE / AFP)" title="Pogba vai para o Chelsea por R$ 378 mi, diz jornal (JOHANNES EISELE / AFP)"
         data-original-image="s2.glbimg.com/ZgPvIstpl8yvJ7qOq_x-BChZXA0=/1656x1470:4696x2844/155x70/s.glbimg.com/es/ge/f/original/2015/08/08/000_del6435982.jpg" data-url-smart_horizontal="Lu5xZNo633dJHmzTB2eGrv4uYWs=/90x56/smart/filters:strip_icc()/" data-url-smart="Lu5xZNo633dJHmzTB2eGrv4uYWs=/90x56/smart/filters:strip_icc()/" data-url-feature="Lu5xZNo633dJHmzTB2eGrv4uYWs=/90x56/smart/filters:strip_icc()/" data-url-tablet="7eVrHrbwGQWb9HY8CW0ka6UeHA8=/160xorig/smart/filters:strip_icc()/" data-url-desktop="QyDdAY5VRoGWUYHl3huRHMUoN08=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Pogba vai para o Chelsea por R$ 378 mi, diz jornal</h2></div></a></div></div></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/rede-globo/video-show/">vÃ­deo show</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/maira-charken-leva-curriculo-para-o-video-show/4410938/"><a href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/maira-charken-leva-curriculo-para-o-video-show/4410938/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4410938.jpg" alt="&#39;Novela estÃ¡ acabando. Estou desempregada&#39;, brinca a atriz" /><div class="time">04:28</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">&#39;Delegata&#39; de BabilÃ´nia</span><span class="title">&#39;Novela estÃ¡ acabando. Estou desempregada&#39;, brinca a atriz</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/sheron-menezzes-mostra-acessorios-da-personagem-paula-de-babilonia/4410817/"><a href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/sheron-menezzes-mostra-acessorios-da-personagem-paula-de-babilonia/4410817/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4410817.jpg" alt="Sheron mostra acessÃ³rios cheios de estilo da sua personagem" /><div class="time">05:37</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">turbante colorido</span><span class="title">Sheron mostra acessÃ³rios cheios de estilo da sua personagem</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/michel-telo-e-recebido-pelos-jurados-do-the-voice-brasil/4410863/"><a href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/michel-telo-e-recebido-pelos-jurados-do-the-voice-brasil/4410863/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4410863.jpg" alt="Michel TelÃ³ Ã© recebido pelos jurados do &#39;The Voice Brasil&#39;" /><div class="time">00:53</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">calouro da turma</span><span class="title">Michel TelÃ³ Ã© recebido pelos jurados do &#39;The Voice Brasil&#39;</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/maira-charken-leva-curriculo-para-o-video-show/4410938/"><span class="subtitle">&#39;Delegata&#39; de BabilÃ´nia</span><span class="title">&#39;Novela estÃ¡ acabando. Estou desempregada&#39;, brinca a atriz</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/sheron-menezzes-mostra-acessorios-da-personagem-paula-de-babilonia/4410817/"><span class="subtitle">turbante colorido</span><span class="title">Sheron mostra acessÃ³rios cheios de estilo da sua personagem</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/video-show/t/programas/v/michel-telo-e-recebido-pelos-jurados-do-the-voice-brasil/4410863/"><span class="subtitle">calouro da turma</span><span class="title">Michel TelÃ³ Ã© recebido pelos jurados do &#39;The Voice Brasil&#39;</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-entretenimento"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/rede-globo/video-show/t/programas/v/maira-charken-leva-curriculo-para-o-video-show/4410938/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/rede-globo/video-show/t/programas/v/sheron-menezzes-mostra-acessorios-da-personagem-paula-de-babilonia/4410817/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/rede-globo/video-show/t/programas/v/michel-telo-e-recebido-pelos-jurados-do-the-voice-brasil/4410863/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais<strong> vÃ­deos</strong><span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://oglobo.globo.com/mundo/homem-fere-duas-pessoas-tiros-e-preso-no-trem-amsterda-paris-17262041" class="foto" title="Atirador fere trÃªs em trem de AmsterdÃ£ a Paris; caso Ã© apurado como terrorismo (ReproduÃ§Ã£o/Twitter/FreedomFilmLLC.com)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/11IQpskGadYipDyz7f4XzfZomt8=/filters:quality(10):strip_icc()/s2.glbimg.com/px8hcUCHmho4N0qlh36w0S5k-rI=/0x24:619x357/335x180/s.glbimg.com/jo/g1/f/original/2015/08/21/tremparisamsterdam.jpg" alt="Atirador fere trÃªs em trem de AmsterdÃ£ a Paris; caso Ã© apurado como terrorismo (ReproduÃ§Ã£o/Twitter/FreedomFilmLLC.com)" title="Atirador fere trÃªs em trem de AmsterdÃ£ a Paris; caso Ã© apurado como terrorismo (ReproduÃ§Ã£o/Twitter/FreedomFilmLLC.com)"
                data-original-image="s2.glbimg.com/px8hcUCHmho4N0qlh36w0S5k-rI=/0x24:619x357/335x180/s.glbimg.com/jo/g1/f/original/2015/08/21/tremparisamsterdam.jpg" data-url-smart_horizontal="jSKsBoyA71EcES5BQWgIie96L_Q=/90x0/smart/filters:strip_icc()/" data-url-smart="jSKsBoyA71EcES5BQWgIie96L_Q=/90x0/smart/filters:strip_icc()/" data-url-feature="jSKsBoyA71EcES5BQWgIie96L_Q=/90x0/smart/filters:strip_icc()/" data-url-tablet="6MytMsEnGrQF_WL9QgF9EjlQats=/220x125/smart/filters:strip_icc()/" data-url-desktop="DJZRNUyziyMHBTyzzEz-vijihnM=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Atirador fere trÃªs em trem de AmsterdÃ£ a Paris; caso Ã© apurado como terrorismo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 20:12:37" class="chamada chamada-principal mobile-grid-full"><a href="http://epoca.globo.com/tempo/noticia/2015/08/agencia-contratada-pelo-pt-paga-r-20-mil-de-salario-dilma-bolada.html" class="foto" title="Criador da Dilma Bolada tem o maior salÃ¡rio de agÃªncia contratada pelo PT: R$ 20 mil ( Roberto Stuckert Filho/PR)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ki-Hzh2oXi7ByfR6bG4T0qqGGYI=/filters:quality(10):strip_icc()/s2.glbimg.com/8GbYqI5U9_PLstsYEkKWG1TY4ls=/0x8:560x381/120x80/e.glbimg.com/og/ed/f/original/2015/08/21/dilmabolada.jpg" alt="Criador da Dilma Bolada tem o maior salÃ¡rio de agÃªncia contratada pelo PT: R$ 20 mil ( Roberto Stuckert Filho/PR)" title="Criador da Dilma Bolada tem o maior salÃ¡rio de agÃªncia contratada pelo PT: R$ 20 mil ( Roberto Stuckert Filho/PR)"
                data-original-image="s2.glbimg.com/8GbYqI5U9_PLstsYEkKWG1TY4ls=/0x8:560x381/120x80/e.glbimg.com/og/ed/f/original/2015/08/21/dilmabolada.jpg" data-url-smart_horizontal="zXyWBx2_ozDObBo6maV5YxHvHL0=/90x0/smart/filters:strip_icc()/" data-url-smart="zXyWBx2_ozDObBo6maV5YxHvHL0=/90x0/smart/filters:strip_icc()/" data-url-feature="zXyWBx2_ozDObBo6maV5YxHvHL0=/90x0/smart/filters:strip_icc()/" data-url-tablet="LSBSdpCu7rlNs8CWaNb5vwuwq7E=/70x50/smart/filters:strip_icc()/" data-url-desktop="FRz_ivqif_FoSun0ntcLKlQOLkg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Criador da Dilma Bolada tem o maior salÃ¡rio de agÃªncia contratada pelo PT: R$ 20 mil</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 19:34:10" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/casos-de-policia/homem-preso-acusado-de-esfaquear-advogada-na-regiao-metropolitana-do-rio-17264971.html" class="foto" title="PolÃ­cia acha suspeito de matar advogada a facadas no RJ em hospital psiquiÃ¡trico (ReproduÃ§Ã£o Facebook)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/NWWir88TMi7fCbEIkCDGMiXQC3E=/filters:quality(10):strip_icc()/s2.glbimg.com/XYvmwN4Do-_Wu8vPfAFGuXRH5KA=/157x18:587x305/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/maria_tavares_asssassinada_em_niteroi.jpg" alt="PolÃ­cia acha suspeito de matar advogada a facadas no RJ em hospital psiquiÃ¡trico (ReproduÃ§Ã£o Facebook)" title="PolÃ­cia acha suspeito de matar advogada a facadas no RJ em hospital psiquiÃ¡trico (ReproduÃ§Ã£o Facebook)"
                data-original-image="s2.glbimg.com/XYvmwN4Do-_Wu8vPfAFGuXRH5KA=/157x18:587x305/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/maria_tavares_asssassinada_em_niteroi.jpg" data-url-smart_horizontal="fUAcj6v_sB7hAIcArlhXAePJUNw=/90x0/smart/filters:strip_icc()/" data-url-smart="fUAcj6v_sB7hAIcArlhXAePJUNw=/90x0/smart/filters:strip_icc()/" data-url-feature="fUAcj6v_sB7hAIcArlhXAePJUNw=/90x0/smart/filters:strip_icc()/" data-url-tablet="dJLUguiBBRnX-C_6LNIfhJA6Zlk=/70x50/smart/filters:strip_icc()/" data-url-desktop="tTpwtXaN6IvVYTFFh7N8PkGH3Lg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PolÃ­cia acha suspeito de matar advogada a facadas no RJ em hospital psiquiÃ¡trico</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/mundo/noticia/2015/08/milhares-de-peixes-aparecem-mortos-em-rio-de-tianjin-na-china.html" class="" title="Milhares de peixes aparecem mortos em rio apÃ³s explosÃ£o na China; imagem impressiona (Reuters/Stringer)" rel="bookmark"><span class="conteudo"><p>Milhares de peixes aparecem mortos em rio apÃ³s explosÃ£o na China; imagem impressiona</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 19:22:14" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pr/norte-noroeste/noticia/2015/08/mulher-se-revolta-com-precos-e-quebra-vinhos-de-mercado-do-parana.html" class="foto" title="Mulher se revolta com preÃ§os e quebra dezenas de vinhos em mercado no PR; assista (ReproduÃ§Ã£o/Youtube)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/XDKBrLbF4dnaNMHGd7ZB6oBP9-A=/filters:quality(10):strip_icc()/s2.glbimg.com/s83o32OfTjgD2IPeJwduxCYpJ8E=/274x125:437x234/120x80/s.glbimg.com/jo/g1/f/original/2015/08/21/sem_titulo_1.jpg" alt="Mulher se revolta com preÃ§os e quebra dezenas de vinhos em mercado no PR; assista (ReproduÃ§Ã£o/Youtube)" title="Mulher se revolta com preÃ§os e quebra dezenas de vinhos em mercado no PR; assista (ReproduÃ§Ã£o/Youtube)"
                data-original-image="s2.glbimg.com/s83o32OfTjgD2IPeJwduxCYpJ8E=/274x125:437x234/120x80/s.glbimg.com/jo/g1/f/original/2015/08/21/sem_titulo_1.jpg" data-url-smart_horizontal="WXSImNue8HILPul1sFEErmOHWnQ=/90x0/smart/filters:strip_icc()/" data-url-smart="WXSImNue8HILPul1sFEErmOHWnQ=/90x0/smart/filters:strip_icc()/" data-url-feature="WXSImNue8HILPul1sFEErmOHWnQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="xDcvy5X9exNQdMS_v3NdDdqx6Rs=/70x50/smart/filters:strip_icc()/" data-url-desktop="b2si5hEDfXpAjg4YwG5RaASZGZI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Mulher se revolta com preÃ§os e quebra dezenas de vinhos em mercado no PR; assista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 15:06:57" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/08/smartphone-da-blackberry-com-android-e-slider-aparece-na-web-veja.html" class="foto" title="Antigo sucesso, BlackBerry tem novo smart com Android e teclado fÃ­sico vazado; veja (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/LsH5k0l_JdPmqMvM3l3-3dCl-yU=/filters:quality(10):strip_icc()/s2.glbimg.com/wucijuLz5__Z9FGI4oEVzgIWU1o=/0x0:731x487/120x80/s.glbimg.com/po/tt2/f/original/2015/08/21/b_kq6idwkaareuy.jpg-large.0.0.jpeg" alt="Antigo sucesso, BlackBerry tem novo smart com Android e teclado fÃ­sico vazado; veja (DivulgaÃ§Ã£o)" title="Antigo sucesso, BlackBerry tem novo smart com Android e teclado fÃ­sico vazado; veja (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/wucijuLz5__Z9FGI4oEVzgIWU1o=/0x0:731x487/120x80/s.glbimg.com/po/tt2/f/original/2015/08/21/b_kq6idwkaareuy.jpg-large.0.0.jpeg" data-url-smart_horizontal="h5yY8Y-f6Bn5Bzc6SpV2NitlgbY=/90x0/smart/filters:strip_icc()/" data-url-smart="h5yY8Y-f6Bn5Bzc6SpV2NitlgbY=/90x0/smart/filters:strip_icc()/" data-url-feature="h5yY8Y-f6Bn5Bzc6SpV2NitlgbY=/90x0/smart/filters:strip_icc()/" data-url-tablet="VWfBS1qCChC0zzM_tgylRnofLmQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="UlWWpQ3KwdRDRwuZh4CeqdU-LHE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Antigo sucesso, BlackBerry tem novo smart com Android e teclado fÃ­sico vazado; veja</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://oglobo.globo.com/blogs/pagenotfound/posts/2015/08/21/christian-grey-da-vida-real-condenado-14-anos-de-prisao-570058.asp" class="" title="&#39;Christian Grey real&#39; mantinha &#39;escrava&#39; e mandou atacar ex affairs da jovem com Ã¡cido (ReproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>&#39;Christian Grey real&#39; mantinha &#39;escrava&#39; e<br /> mandou atacar ex affairs da jovem com Ã¡cido</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 15:09:00" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/08/detran-cassara-carteira-de-empresario-que-atropelou-e-matou-operario-no-rio.html" class="foto" title="Detran do Rio diz que habilitaÃ§Ã£o do filho de Pitanguy vai ser cassada (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/1Ou-jCrvdD4MgodAQWKpx3Hsepg=/filters:quality(10):strip_icc()/s2.glbimg.com/YwvM3myQKu394wrijDUv8zqkOCo=/69x0:699x419/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/acidente_gavea.jpg" alt="Detran do Rio diz que habilitaÃ§Ã£o do filho de Pitanguy vai ser cassada (ReproduÃ§Ã£o)" title="Detran do Rio diz que habilitaÃ§Ã£o do filho de Pitanguy vai ser cassada (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/YwvM3myQKu394wrijDUv8zqkOCo=/69x0:699x419/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/acidente_gavea.jpg" data-url-smart_horizontal="W-aOrz1OjzWw6-64soqTteONfeo=/90x0/smart/filters:strip_icc()/" data-url-smart="W-aOrz1OjzWw6-64soqTteONfeo=/90x0/smart/filters:strip_icc()/" data-url-feature="W-aOrz1OjzWw6-64soqTteONfeo=/90x0/smart/filters:strip_icc()/" data-url-tablet="ULtoJs3hEJOiBZAeyls_Ahv3YoA=/70x50/smart/filters:strip_icc()/" data-url-desktop="Mg2vJ6DfhZFpX93Nm9m7LJ-fDp0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Detran do Rio diz que<br /> habilitaÃ§Ã£o do filho de Pitanguy vai ser cassada</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 17:29:50" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rs/rio-grande-do-sul/noticia/2015/08/com-video-policia-busca-suspeitos-de-assalto-e-morte-em-onibus-no-rs.html" class="foto" title="VÃ­deo: assaltantes matam deficiente auditivo e passam por cima de vÃ­tima em Ã´nibus (G1)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/i1O-936ICoHuvKk44gg07vl9Gq8=/filters:quality(10):strip_icc()/s2.glbimg.com/CswCMZWpv3a4b_O801etEiJp7nw=/145x41:476x262/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/assalto.jpg" alt="VÃ­deo: assaltantes matam deficiente auditivo e passam por cima de vÃ­tima em Ã´nibus (G1)" title="VÃ­deo: assaltantes matam deficiente auditivo e passam por cima de vÃ­tima em Ã´nibus (G1)"
                data-original-image="s2.glbimg.com/CswCMZWpv3a4b_O801etEiJp7nw=/145x41:476x262/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/assalto.jpg" data-url-smart_horizontal="B7SqqMfmhD5nV0gXyxET73vnHw4=/90x0/smart/filters:strip_icc()/" data-url-smart="B7SqqMfmhD5nV0gXyxET73vnHw4=/90x0/smart/filters:strip_icc()/" data-url-feature="B7SqqMfmhD5nV0gXyxET73vnHw4=/90x0/smart/filters:strip_icc()/" data-url-tablet="YYAI0PpCLnBoDWaeHGTOtbtKRgg=/70x50/smart/filters:strip_icc()/" data-url-desktop="x-SOxw7djlwdQ671HJ52zt4iyUg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>VÃ­deo: assaltantes matam deficiente auditivo e passam por cima de vÃ­tima em Ã´nibus</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/rs/noticia/2015/08/provocacao-em-classico-local-pode-ter-motivado-tiros-contra-jogadores-veja.html" class="foto" title="ProvocaÃ§Ã£o em clÃ¡ssico local pode ter motivado tiros contra jogadores; vÃ­deo (RBS TV)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/M8zvm7gOZRpusI5qG7YpuGoMGsE=/filters:quality(10):strip_icc()/s2.glbimg.com/iEKfgjkKPpuMGjizVlJMc4fSfR0=/0x0:335x180/335x180/s.glbimg.com/en/ho/f/original/2015/08/21/tiro.jpg" alt="ProvocaÃ§Ã£o em clÃ¡ssico local pode ter motivado tiros contra jogadores; vÃ­deo (RBS TV)" title="ProvocaÃ§Ã£o em clÃ¡ssico local pode ter motivado tiros contra jogadores; vÃ­deo (RBS TV)"
                data-original-image="s2.glbimg.com/iEKfgjkKPpuMGjizVlJMc4fSfR0=/0x0:335x180/335x180/s.glbimg.com/en/ho/f/original/2015/08/21/tiro.jpg" data-url-smart_horizontal="Wvu30Mv2M4EhNPcd7ezhJ5U0KKw=/90x0/smart/filters:strip_icc()/" data-url-smart="Wvu30Mv2M4EhNPcd7ezhJ5U0KKw=/90x0/smart/filters:strip_icc()/" data-url-feature="Wvu30Mv2M4EhNPcd7ezhJ5U0KKw=/90x0/smart/filters:strip_icc()/" data-url-tablet="Axer7RFuiT59hq7OvyvEUys5Rd0=/220x125/smart/filters:strip_icc()/" data-url-desktop="CghtUinX86owE_ILuk3-C9OfgJ0=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ProvocaÃ§Ã£o em clÃ¡ssico local pode ter motivado tiros contra jogadores; vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 19:34:10" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/corinthians/noticia/2015/08/nova-parcial-timao-vende-38-mil-ingressos-para-duelo-com-o-cruzeiro.html" class="foto" title="Corinthians vende 38 mil ingressos e fica perto de quebrar recorde da Arena (Filipe Rodrigues)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Yx9q6ZpgCeU52jWGH7ydiAvo1_w=/filters:quality(10):strip_icc()/s2.glbimg.com/Z0Cn3l4HnGrL65omWZbCROav3EI=/0x777:3027x2794/120x80/s.glbimg.com/es/ge/f/original/2014/09/04/dsc_2447.jpg" alt="Corinthians vende 38 mil ingressos e fica perto de quebrar recorde da Arena (Filipe Rodrigues)" title="Corinthians vende 38 mil ingressos e fica perto de quebrar recorde da Arena (Filipe Rodrigues)"
                data-original-image="s2.glbimg.com/Z0Cn3l4HnGrL65omWZbCROav3EI=/0x777:3027x2794/120x80/s.glbimg.com/es/ge/f/original/2014/09/04/dsc_2447.jpg" data-url-smart_horizontal="N31Z0WR6rXIC8ubs6BSNcJq-xxo=/90x0/smart/filters:strip_icc()/" data-url-smart="N31Z0WR6rXIC8ubs6BSNcJq-xxo=/90x0/smart/filters:strip_icc()/" data-url-feature="N31Z0WR6rXIC8ubs6BSNcJq-xxo=/90x0/smart/filters:strip_icc()/" data-url-tablet="8tRkGnjNhbxgMbq_kOIRtlILl6M=/70x50/smart/filters:strip_icc()/" data-url-desktop="P1uRutwaECvHll39urEyjmu0HOA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Corinthians vende 38 mil ingressos e fica perto de quebrar recorde da Arena</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 19:22:27" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/08/apos-primeiro-treino-oswaldo-volta-revela-primeira-impressao-aplicados.html" class="foto" title="Oswaldo comanda primeiro treino no Fla e analisa atletas: &#39;Bom nÃ­vel e sÃ£o aplicados&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/4Bp-YlgZbt6Xiro9tHshEaOh0z4=/filters:quality(10):strip_icc()/s2.glbimg.com/Ek_BiKYsbcUiQWzzS-d0gLvAJag=/120x75:477x313/120x80/s.glbimg.com/es/ge/f/original/2015/08/21/sem_titulo_3.jpg" alt="Oswaldo comanda primeiro treino no Fla e analisa atletas: &#39;Bom nÃ­vel e sÃ£o aplicados&#39; (ReproduÃ§Ã£o)" title="Oswaldo comanda primeiro treino no Fla e analisa atletas: &#39;Bom nÃ­vel e sÃ£o aplicados&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/Ek_BiKYsbcUiQWzzS-d0gLvAJag=/120x75:477x313/120x80/s.glbimg.com/es/ge/f/original/2015/08/21/sem_titulo_3.jpg" data-url-smart_horizontal="CU1KpgHvkL3lSVXJDYxtpg86JTo=/90x0/smart/filters:strip_icc()/" data-url-smart="CU1KpgHvkL3lSVXJDYxtpg86JTo=/90x0/smart/filters:strip_icc()/" data-url-feature="CU1KpgHvkL3lSVXJDYxtpg86JTo=/90x0/smart/filters:strip_icc()/" data-url-tablet="Wrv200zEtn_oGAEiIueBrElhGDU=/70x50/smart/filters:strip_icc()/" data-url-desktop="Elc2VgbenlfIIQpMhtOx5_krcsk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Oswaldo comanda primeiro treino no Fla e analisa atletas: &#39;Bom nÃ­vel e sÃ£o aplicados&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/rs/futebol/noticia/2015/08/sandro-ve-volta-ao-inter-como-sonho-para-futuro-telefone-sempre-ligado.html" class="" title="CampeÃ£o pelo Inter, Sandro treina no clube Ã  espera de passaporte e sonha voltar no futuro (TomÃ¡s Hammes / GloboEsporte.com)" rel="bookmark"><span class="conteudo"><p>CampeÃ£o pelo Inter, Sandro treina no clube Ã  espera de passaporte e sonha voltar no futuro</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 17:53:53" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/retratos-da-bola/namorada-de-schneiderlin-do-manchester-united-vira-noticia-na-inglaterra-por-manter-emprego-de-caixa-em-loja-17265101.html" class="foto" title="Namorada de reforÃ§o do Manchester vira notÃ­cia por seguir como caixa de loja (Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/QfVCejoqo2_3_mcU1K3rIBf6nBU=/filters:quality(10):strip_icc()/s2.glbimg.com/FcEMofys8yJ0DbkRvnAKp8bVU_k=/22x49:425x317/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/schneiderlin.jpg" alt="Namorada de reforÃ§o do Manchester vira notÃ­cia por seguir como caixa de loja (Instagram)" title="Namorada de reforÃ§o do Manchester vira notÃ­cia por seguir como caixa de loja (Instagram)"
                data-original-image="s2.glbimg.com/FcEMofys8yJ0DbkRvnAKp8bVU_k=/22x49:425x317/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/schneiderlin.jpg" data-url-smart_horizontal="ktOc8HRGZNolEit9vNoQDJuToHk=/90x0/smart/filters:strip_icc()/" data-url-smart="ktOc8HRGZNolEit9vNoQDJuToHk=/90x0/smart/filters:strip_icc()/" data-url-feature="ktOc8HRGZNolEit9vNoQDJuToHk=/90x0/smart/filters:strip_icc()/" data-url-tablet="391Yyr9DcAmBDiaoGTtw3xoB1W8=/70x50/smart/filters:strip_icc()/" data-url-desktop="cl2PRWijo0UjUHUtuKRyCC1teMw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Namorada de reforÃ§o do Manchester vira notÃ­cia por seguir como caixa de loja</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 16:37:19" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/futebol-ingles/noticia/2015/08/kleberson-anderson-robinho-jo-manchester-ainda-busca-idolo-brasuca.html" class="foto" title="Kleberson, JÃ´... Manchester ainda busca Ã­dolo brasileiro nos clubes: &#39;Nomes errados&#39; (Reuters)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/0vJJWuEsXPuVEWiM3my8mcEOIJU=/filters:quality(10):strip_icc()/s2.glbimg.com/4n-pPnaG5YV0ZaonhX9WKySP-X8=/352x66:1622x912/120x80/s.glbimg.com/es/ge/f/original/2015/08/17/kleberson-e-cristiano-ronaldo-editorial-use-only.-reuters-paul-sanders-1_1.jpg" alt="Kleberson, JÃ´... Manchester ainda busca Ã­dolo brasileiro nos clubes: &#39;Nomes errados&#39; (Reuters)" title="Kleberson, JÃ´... Manchester ainda busca Ã­dolo brasileiro nos clubes: &#39;Nomes errados&#39; (Reuters)"
                data-original-image="s2.glbimg.com/4n-pPnaG5YV0ZaonhX9WKySP-X8=/352x66:1622x912/120x80/s.glbimg.com/es/ge/f/original/2015/08/17/kleberson-e-cristiano-ronaldo-editorial-use-only.-reuters-paul-sanders-1_1.jpg" data-url-smart_horizontal="zwHhKF_tkNbbvmVXPwJGssMUXWw=/90x0/smart/filters:strip_icc()/" data-url-smart="zwHhKF_tkNbbvmVXPwJGssMUXWw=/90x0/smart/filters:strip_icc()/" data-url-feature="zwHhKF_tkNbbvmVXPwJGssMUXWw=/90x0/smart/filters:strip_icc()/" data-url-tablet="ahWi6USNhLbhc3dJ2zce-we9sS4=/70x50/smart/filters:strip_icc()/" data-url-desktop="iY4C-kZ55eXY6PK2cMpWqbo1wtM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Kleberson, JÃ´... Manchester ainda busca Ã­dolo brasileiro nos clubes: &#39;Nomes errados&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/casemiro-ex-sao-paulo-da-balao-em-marcelo-faz-gol-de-placa-no-treino-do-real-madrid-veja-video-17262725.html" class="" title="Casemiro, ex-SÃ£o Paulo, dÃ¡ chapÃ©u em Marcelo e faz golaÃ§o em treino do Real Madrid; veja o lance (ReproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>Casemiro, ex-SÃ£o Paulo, dÃ¡ chapÃ©u em Marcelo e faz golaÃ§o em treino do Real Madrid; veja o lance</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 17:30:37" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/miesha-tate-lamenta-perda-de-luta-pelo-cinturao-estou-extremamente-decepcionada.html" class="foto" title="Miesha Tate lamenta ter sido rejeitada para nova luta com Ronda: &#39;Muito decepcionada&#39; (Evelyn Rodrigues)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/HKI-Xzlf6nGfL3u4l5jvfkQqhco=/filters:quality(10):strip_icc()/s2.glbimg.com/Ek65JRyhhrvWif221kvNO2Gmopo=/217x66:1209x728/120x80/s.glbimg.com/es/ge/f/original/2015/07/23/miesha5.jpeg" alt="Miesha Tate lamenta ter sido rejeitada para nova luta com Ronda: &#39;Muito decepcionada&#39; (Evelyn Rodrigues)" title="Miesha Tate lamenta ter sido rejeitada para nova luta com Ronda: &#39;Muito decepcionada&#39; (Evelyn Rodrigues)"
                data-original-image="s2.glbimg.com/Ek65JRyhhrvWif221kvNO2Gmopo=/217x66:1209x728/120x80/s.glbimg.com/es/ge/f/original/2015/07/23/miesha5.jpeg" data-url-smart_horizontal="kf_VX30cmHvu3y220sXPCNNNg7k=/90x0/smart/filters:strip_icc()/" data-url-smart="kf_VX30cmHvu3y220sXPCNNNg7k=/90x0/smart/filters:strip_icc()/" data-url-feature="kf_VX30cmHvu3y220sXPCNNNg7k=/90x0/smart/filters:strip_icc()/" data-url-tablet="AQhq_aD_3WhW2jW6yOf9GZDujr0=/70x50/smart/filters:strip_icc()/" data-url-desktop="1cBbtt5FMegZ9pHI70zY2_7t_tI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Miesha Tate lamenta ter sido rejeitada para nova luta com Ronda: &#39;Muito decepcionada&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/noticia/2015/08/claudia-gadelha-enfrenta-campea-joanna-jedrzejczyk-no-ufc-195.html" class="foto" title="ClÃ¡udia Gadelha enfrenta a campeÃ£ Joanna Jedrzejczyk apÃ³s polÃªmica no 1Âº duelo (Globoesporte.com)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/OQmqSWtRDZQnf4RIbg022Wur21U=/filters:quality(10):strip_icc()/s2.glbimg.com/y5pqpGaHvsh5Ke1CRmV6pZAwGV8=/0x0:690x459/120x80/s.glbimg.com/es/ge/f/original/2015/08/21/montagme0ufc.jpg" alt="ClÃ¡udia Gadelha enfrenta a campeÃ£ Joanna Jedrzejczyk apÃ³s polÃªmica no 1Âº duelo (Globoesporte.com)" title="ClÃ¡udia Gadelha enfrenta a campeÃ£ Joanna Jedrzejczyk apÃ³s polÃªmica no 1Âº duelo (Globoesporte.com)"
                data-original-image="s2.glbimg.com/y5pqpGaHvsh5Ke1CRmV6pZAwGV8=/0x0:690x459/120x80/s.glbimg.com/es/ge/f/original/2015/08/21/montagme0ufc.jpg" data-url-smart_horizontal="o9rVo_kPMWeNJu4qfCgX1eFdxIc=/90x0/smart/filters:strip_icc()/" data-url-smart="o9rVo_kPMWeNJu4qfCgX1eFdxIc=/90x0/smart/filters:strip_icc()/" data-url-feature="o9rVo_kPMWeNJu4qfCgX1eFdxIc=/90x0/smart/filters:strip_icc()/" data-url-tablet="IHXoz_GOeFSW2y1fvXaLvdivExY=/70x50/smart/filters:strip_icc()/" data-url-desktop="Bs_PPCSIPyH8cqRIyg1SkLlcCSE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ClÃ¡udia Gadelha enfrenta a campeÃ£ Joanna Jedrzejczyk apÃ³s polÃªmica no 1Âº duelo</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/em-livro-thammy-miranda-mostra-resultado-da-cirurgia-de-retirada-de-seios-17266508.html" class="foto" title="Thammy revela o resultado da cirurgia de retirada de seios em sua biografia (ReproduÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/x2_xL8YtR9foyG9vktzNeuuXVw0=/filters:quality(10):strip_icc()/s2.glbimg.com/6M969HZy0q0LJRt43c0RCerD2w4=/4x4:339x183/335x180/s.glbimg.com/en/ho/f/original/2015/08/21/thammy_2.jpg" alt="Thammy revela o resultado da cirurgia de retirada de seios em sua biografia (ReproduÃ§Ã£o)" title="Thammy revela o resultado da cirurgia de retirada de seios em sua biografia (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/6M969HZy0q0LJRt43c0RCerD2w4=/4x4:339x183/335x180/s.glbimg.com/en/ho/f/original/2015/08/21/thammy_2.jpg" data-url-smart_horizontal="9r1B9KMDdeZhlED2pHrlMIf3pOg=/90x0/smart/filters:strip_icc()/" data-url-smart="9r1B9KMDdeZhlED2pHrlMIf3pOg=/90x0/smart/filters:strip_icc()/" data-url-feature="9r1B9KMDdeZhlED2pHrlMIf3pOg=/90x0/smart/filters:strip_icc()/" data-url-tablet="HUVnnoywUg9OTDU4um7zalha2sA=/220x125/smart/filters:strip_icc()/" data-url-desktop="kDiyO5EYP7HeZtwihXX9d-Zrlgo=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Thammy revela o resultado da cirurgia de retirada de seios em sua biografia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 21:04:44" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/08/de-lingerie-ticiane-pinheiro-mostra-corpao-em-bastidores-de-ensaio.html" class="foto" title="Ticiane Pinheiro exibe corpÃ£o e diz que cortou carboidratos para tirar fotos de lingerie (Manuela Scarpa/Photo Rio News)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/SP-GN8I8LRNrJ-OohBELcdoGki0=/filters:quality(10):strip_icc()/s2.glbimg.com/1MjcZbFCmdk1zSxnmOyMVYOY8g4=/166x12:436x192/120x80/e.glbimg.com/og/ed/f/original/2015/08/21/making_of_de_ticiane_pinheiro_para_outlet_lingerie-0852.jpg" alt="Ticiane Pinheiro exibe corpÃ£o e diz que cortou carboidratos para tirar fotos de lingerie (Manuela Scarpa/Photo Rio News)" title="Ticiane Pinheiro exibe corpÃ£o e diz que cortou carboidratos para tirar fotos de lingerie (Manuela Scarpa/Photo Rio News)"
                data-original-image="s2.glbimg.com/1MjcZbFCmdk1zSxnmOyMVYOY8g4=/166x12:436x192/120x80/e.glbimg.com/og/ed/f/original/2015/08/21/making_of_de_ticiane_pinheiro_para_outlet_lingerie-0852.jpg" data-url-smart_horizontal="CO8Sq_pqWq1ZGX1jN05Xd56eszs=/90x0/smart/filters:strip_icc()/" data-url-smart="CO8Sq_pqWq1ZGX1jN05Xd56eszs=/90x0/smart/filters:strip_icc()/" data-url-feature="CO8Sq_pqWq1ZGX1jN05Xd56eszs=/90x0/smart/filters:strip_icc()/" data-url-tablet="Iv0jJr92vwVExDEfqKdXHSNoek4=/70x50/smart/filters:strip_icc()/" data-url-desktop="g0-HM2n_NeUYOaWifP0l4Cp6wMk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ticiane Pinheiro exibe corpÃ£o e diz que cortou carboidratos para tirar fotos de lingerie</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 19:39:47" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/08/bruna-marquezine-posta-foto-e-chama-atencao-pelo-corpo-em-forma.html" class="foto" title="Bruna Marquezine posa de vestido decotado e deixa pernas Ã  mostra; amplie (Instagram / ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/X4m6Hs5yFbVCwgz2SP2UyfwLIG0=/filters:quality(10):strip_icc()/s2.glbimg.com/EFroL7BGaL1NwkkGEYaVm2pbm4A=/59x8:247x134/120x80/s.glbimg.com/jo/eg/f/original/2015/08/21/bruna_marquezine.jpg" alt="Bruna Marquezine posa de vestido decotado e deixa pernas Ã  mostra; amplie (Instagram / ReproduÃ§Ã£o)" title="Bruna Marquezine posa de vestido decotado e deixa pernas Ã  mostra; amplie (Instagram / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/EFroL7BGaL1NwkkGEYaVm2pbm4A=/59x8:247x134/120x80/s.glbimg.com/jo/eg/f/original/2015/08/21/bruna_marquezine.jpg" data-url-smart_horizontal="1HBaVeviagflgCCny-scnNAGh0s=/90x0/smart/filters:strip_icc()/" data-url-smart="1HBaVeviagflgCCny-scnNAGh0s=/90x0/smart/filters:strip_icc()/" data-url-feature="1HBaVeviagflgCCny-scnNAGh0s=/90x0/smart/filters:strip_icc()/" data-url-tablet="PLY-JysRACGLZi8Nic5-snNmKEk=/70x50/smart/filters:strip_icc()/" data-url-desktop="Q_Lfk_53yTI2WKIqEIFypAgvOnQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Bruna Marquezine posa de vestido decotado e deixa pernas Ã  mostra; amplie</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 08:51:54" class="chamada chamada-principal mobile-grid-full"><a href="http://kogut.oglobo.globo.com/noticias-da-tv/noticia/2015/08/gravida-sophie-charlotte-deixa-serie-e-sera-substituida-por-bruna-marquezine.html" class="" title="GrÃ¡vida, Sophie Charlotte serÃ¡ substituÃ­da por Marquezine como par de Daniel de Oliveira na TV (Isac Luz / EGO)" rel="bookmark"><span class="conteudo"><p>GrÃ¡vida, Sophie Charlotte serÃ¡ substituÃ­da por Marquezine como par de Daniel de Oliveira na TV</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 19:22:27" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/08/viviane-araujo-coloca-mega-hair-e-faz-comparacao-com-o-antes-careca.html" class="foto" title="Viviane AraÃºjo tira o mega hair e posta fotos no salÃ£o: &#39;Olha como estou careca&#39; (Instagram / ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/cg9t5CNf89u8k15J2BbsPlhnFV4=/filters:quality(10):strip_icc()/s2.glbimg.com/MQOyLpXwLn5ry6Oz6AXyu5TSgq8=/0x86:650x519/120x80/s.glbimg.com/jo/eg/f/original/2015/08/21/viviane_araujo.jpg" alt="Viviane AraÃºjo tira o mega hair e posta fotos no salÃ£o: &#39;Olha como estou careca&#39; (Instagram / ReproduÃ§Ã£o)" title="Viviane AraÃºjo tira o mega hair e posta fotos no salÃ£o: &#39;Olha como estou careca&#39; (Instagram / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/MQOyLpXwLn5ry6Oz6AXyu5TSgq8=/0x86:650x519/120x80/s.glbimg.com/jo/eg/f/original/2015/08/21/viviane_araujo.jpg" data-url-smart_horizontal="QsXVjr2Wi4G0Kpf3AGP33RWdAYs=/90x0/smart/filters:strip_icc()/" data-url-smart="QsXVjr2Wi4G0Kpf3AGP33RWdAYs=/90x0/smart/filters:strip_icc()/" data-url-feature="QsXVjr2Wi4G0Kpf3AGP33RWdAYs=/90x0/smart/filters:strip_icc()/" data-url-tablet="m7x3vFeDOZS6CfcMBfhbvq0f_Ts=/70x50/smart/filters:strip_icc()/" data-url-desktop="49RKMliPfMaHzafWI7hR0BPjVVw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Viviane AraÃºjo tira o mega hair e posta fotos no salÃ£o: &#39;Olha como estou careca&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 21:07:26" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/moda/noticia/2015/08/look-do-dia-giovanna-ewbank-usa-decotao-em-evento-em-sao-paulo.html" class="foto" title="Giovanna Ewbank aposta em vestidÃ£o decotado com calÃ§a em evento em SP; veja look (Denilson Santos e Raphael Castello/AgNews)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/aVaAMejpTIeaqtqHOreVuqgtTa4=/filters:quality(10):strip_icc()/s2.glbimg.com/mykxgDxziWzl8bnrZ6I3x9d3-po=/271x385:1429x1157/120x80/s.glbimg.com/jo/eg/f/original/2015/08/21/img_3562.jpg" alt="Giovanna Ewbank aposta em vestidÃ£o decotado com calÃ§a em evento em SP; veja look (Denilson Santos e Raphael Castello/AgNews)" title="Giovanna Ewbank aposta em vestidÃ£o decotado com calÃ§a em evento em SP; veja look (Denilson Santos e Raphael Castello/AgNews)"
                data-original-image="s2.glbimg.com/mykxgDxziWzl8bnrZ6I3x9d3-po=/271x385:1429x1157/120x80/s.glbimg.com/jo/eg/f/original/2015/08/21/img_3562.jpg" data-url-smart_horizontal="gq-x4mC-qEWvjHwW6-6y6WdP6xs=/90x0/smart/filters:strip_icc()/" data-url-smart="gq-x4mC-qEWvjHwW6-6y6WdP6xs=/90x0/smart/filters:strip_icc()/" data-url-feature="gq-x4mC-qEWvjHwW6-6y6WdP6xs=/90x0/smart/filters:strip_icc()/" data-url-tablet="Yu6DKeOObisDaKIRoK7BRYsVrpk=/70x50/smart/filters:strip_icc()/" data-url-desktop="eAbylr8PBMPgB5UCvAYHjv0YCoo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Giovanna Ewbank aposta em vestidÃ£o decotado com calÃ§a em evento em SP; veja look</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 21:11:39" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/casada-ha-cinco-anos-sandra-de-sa-lembra-descoberta-da-bissexualidade-adorava-aula-de-uma-professora-de-historia-17262198.html" class="" title="Casada hÃ¡ cinco anos, cantora Sandra de SÃ¡  abre o coraÃ§Ã£o e lembra descoberta da bissexualidade (ReproduÃ§Ã£o/ Instagram)" rel="bookmark"><span class="conteudo"><p>Casada hÃ¡ cinco anos, cantora Sandra de SÃ¡  abre o coraÃ§Ã£o e lembra descoberta da bissexualidade</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/dudu-azevedo-rafaela-vidal-terminam-namoro-tres-meses-do-casamento-17263524.html" class="foto" title="Com casamento marcado, Dudu Azevedo se separa, e ex curte solteirice na Europa (Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/3NPfwyEascwrFt178Sdv9CG7WHs=/filters:quality(10):strip_icc()/s2.glbimg.com/zN-86YNNFz-ru_WCwWy4-QsHZB4=/0x8:448x306/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/rafaela-vidal.jpg" alt="Com casamento marcado, Dudu Azevedo se separa, e ex curte solteirice na Europa (Instagram)" title="Com casamento marcado, Dudu Azevedo se separa, e ex curte solteirice na Europa (Instagram)"
                data-original-image="s2.glbimg.com/zN-86YNNFz-ru_WCwWy4-QsHZB4=/0x8:448x306/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/rafaela-vidal.jpg" data-url-smart_horizontal="dwb-0mwkutScdKbOK-UhR-UDhDM=/90x0/smart/filters:strip_icc()/" data-url-smart="dwb-0mwkutScdKbOK-UhR-UDhDM=/90x0/smart/filters:strip_icc()/" data-url-feature="dwb-0mwkutScdKbOK-UhR-UDhDM=/90x0/smart/filters:strip_icc()/" data-url-tablet="pI8KRF5VYuanKpiLj9hdUuYp45E=/70x50/smart/filters:strip_icc()/" data-url-desktop="zZr7lyFOghh3i5h2mSNTFA-q1Uo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Com casamento marcado, Dudu Azevedo se separa, e ex curte solteirice na Europa</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-21 16:52:51" class="chamada chamada-principal mobile-grid-full"><a href="http://casavogue.globo.com/LazerCultura/Comida-bebida/noticia/2015/03/para-comer-e-brindar-com-alegria.html" class="foto" title="Aprenda a fazer um delicioso hambÃºrguer com cheddar e drinks para acompanhar (Casa Vogue)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/F4acswsMoXLxxDefvxKxBkO9MOE=/filters:quality(10):strip_icc()/s2.glbimg.com/pDcuxXTzsj0Ipb-Zpkz9eB8S1ps=/0x0:618x412/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/burger.jpg" alt="Aprenda a fazer um delicioso hambÃºrguer com cheddar e drinks para acompanhar (Casa Vogue)" title="Aprenda a fazer um delicioso hambÃºrguer com cheddar e drinks para acompanhar (Casa Vogue)"
                data-original-image="s2.glbimg.com/pDcuxXTzsj0Ipb-Zpkz9eB8S1ps=/0x0:618x412/120x80/s.glbimg.com/en/ho/f/original/2015/08/21/burger.jpg" data-url-smart_horizontal="i259zFde3bZXxabHk03VDTAFNys=/90x0/smart/filters:strip_icc()/" data-url-smart="i259zFde3bZXxabHk03VDTAFNys=/90x0/smart/filters:strip_icc()/" data-url-feature="i259zFde3bZXxabHk03VDTAFNys=/90x0/smart/filters:strip_icc()/" data-url-tablet="ne-XzYwYRNkbkeHWgI36uNdfIAU=/70x50/smart/filters:strip_icc()/" data-url-desktop="iumVLAzATmSUZc_eKVWJo-Kz314=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Aprenda a fazer um delicioso hambÃºrguer com cheddar e drinks para acompanhar</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior "><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="tecnologia &amp; games"><span class="word word-0">tecnologia</span><span class="word word-1">&</span><span class="word word-2">games</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/08/galaxy-note-5-active-com-superbateria-pode-chegar-ainda-neste-ano.html" title="Galaxy com bateria que &#39;humilha&#39; rivais pode vir ainda em 2015"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/g5tOnxX1PJiYjbQvKISO5OPFvts=/filters:quality(10):strip_icc()/s2.glbimg.com/YzbGGTK9UeZLo0Ua4yIFhfUyPZg=/116x225:1141x769/245x130/s.glbimg.com/po/tt2/f/original/2015/08/15/galaxy_note_5_sem_marca_dagua.jpg" alt="Galaxy com bateria que &#39;humilha&#39; rivais pode vir ainda em 2015 (ThÃ¡ssius Veloso/TechTudo)" title="Galaxy com bateria que &#39;humilha&#39; rivais pode vir ainda em 2015 (ThÃ¡ssius Veloso/TechTudo)"
             data-original-image="s2.glbimg.com/YzbGGTK9UeZLo0Ua4yIFhfUyPZg=/116x225:1141x769/245x130/s.glbimg.com/po/tt2/f/original/2015/08/15/galaxy_note_5_sem_marca_dagua.jpg" data-url-smart_horizontal="o84qizGMVVdVqJ-CDn8vdsI-nkc=/90x56/smart/filters:strip_icc()/" data-url-smart="o84qizGMVVdVqJ-CDn8vdsI-nkc=/90x56/smart/filters:strip_icc()/" data-url-feature="o84qizGMVVdVqJ-CDn8vdsI-nkc=/90x56/smart/filters:strip_icc()/" data-url-tablet="ABCZoUm_SQeAfYsijnWZVnw4wd8=/160x96/smart/filters:strip_icc()/" data-url-desktop="_5R0b1L-qZGCrBBReyT2on8r7tQ=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Galaxy com bateria que &#39;humilha&#39; rivais pode vir ainda em 2015</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/08/unicode-9-tem-os-38-novos-emojis-revelados-de-facepalm-selfie.html" title="GrÃ¡vida, selfie e mais: confira os novos emojis que serÃ£o lanÃ§ados"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/0p1nxZyNBiI9BWteJn4p23E531w=/filters:quality(10):strip_icc()/s2.glbimg.com/LvQh4xqMsQqAcuI7ysLvC5AbV3k=/0x0:694x368/245x130/s.glbimg.com/po/tt2/f/original/2015/06/16/imagemabertura.jpg" alt="GrÃ¡vida, selfie e mais: confira os novos emojis que serÃ£o lanÃ§ados (ConsÃ³rcio Unicode libera 37 novos emojis (Foto: DivulgaÃ§Ã£o/ Emoji Keyboard))" title="GrÃ¡vida, selfie e mais: confira os novos emojis que serÃ£o lanÃ§ados (ConsÃ³rcio Unicode libera 37 novos emojis (Foto: DivulgaÃ§Ã£o/ Emoji Keyboard))"
             data-original-image="s2.glbimg.com/LvQh4xqMsQqAcuI7ysLvC5AbV3k=/0x0:694x368/245x130/s.glbimg.com/po/tt2/f/original/2015/06/16/imagemabertura.jpg" data-url-smart_horizontal="so3Y-3b_wKYQvwC2XPlXE00ajuQ=/90x56/smart/filters:strip_icc()/" data-url-smart="so3Y-3b_wKYQvwC2XPlXE00ajuQ=/90x56/smart/filters:strip_icc()/" data-url-feature="so3Y-3b_wKYQvwC2XPlXE00ajuQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="KRh-04zC3QJDXstmPAYyx9sgb2k=/160x96/smart/filters:strip_icc()/" data-url-desktop="-qFM0uTGXzL7zOe-lVnkwRZvG3s=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>GrÃ¡vida, selfie e mais: confira os <br />novos emojis que serÃ£o lanÃ§ados</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/08/conheca-truques-e-funcoes-do-ps4-para-aproveitar-mais-o-seu-console.html" title="PlayStation 4: truques &#39;secretos&#39; 
para aproveitar mais seu console"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/GwyC9_HswL-6WE90cwJ_EdUGU9g=/filters:quality(10):strip_icc()/s2.glbimg.com/l8uppK6lemmhGOaWEdQxQuMY-OQ=/28x52:505x305/245x130/s.glbimg.com/po/tt2/f/original/2015/06/22/ps4-fly.jpg" alt="PlayStation 4: truques &#39;secretos&#39; 
para aproveitar mais seu console (DivulgaÃ§Ã£o)" title="PlayStation 4: truques &#39;secretos&#39; 
para aproveitar mais seu console (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/l8uppK6lemmhGOaWEdQxQuMY-OQ=/28x52:505x305/245x130/s.glbimg.com/po/tt2/f/original/2015/06/22/ps4-fly.jpg" data-url-smart_horizontal="DBh7TiATDR22Wl9A78RDiy8hTpg=/90x56/smart/filters:strip_icc()/" data-url-smart="DBh7TiATDR22Wl9A78RDiy8hTpg=/90x56/smart/filters:strip_icc()/" data-url-feature="DBh7TiATDR22Wl9A78RDiy8hTpg=/90x56/smart/filters:strip_icc()/" data-url-tablet="WJZRQcWuFz_4Q0cjmD78nPaE_e0=/160x96/smart/filters:strip_icc()/" data-url-desktop="7ewof_OET5F_f5rXuorsSOc_-Wo=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>PlayStation 4: truques &#39;secretos&#39; <br />para aproveitar mais seu console</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://forum.techtudo.com.br/perguntas/199206/roteador-com-pouco-sinal-como-resolver" title="Roteador ruim? UsuÃ¡rio ajudam a deixar sua internet &#39;voando&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/fFvG2rXHWlLmv1aHf4UHl5ceQAE=/filters:quality(10):strip_icc()/s2.glbimg.com/mIQkAM6fk-rCpBeDpp9Vo6ga9JQ=/80x124:546x372/245x130/s.glbimg.com/po/tt2/f/original/2015/03/18/02-pond5.jpg" alt="Roteador ruim? UsuÃ¡rio ajudam a deixar sua internet &#39;voando&#39; (Veja se precisa de um roteador novo (Foto: Pond5))" title="Roteador ruim? UsuÃ¡rio ajudam a deixar sua internet &#39;voando&#39; (Veja se precisa de um roteador novo (Foto: Pond5))"
             data-original-image="s2.glbimg.com/mIQkAM6fk-rCpBeDpp9Vo6ga9JQ=/80x124:546x372/245x130/s.glbimg.com/po/tt2/f/original/2015/03/18/02-pond5.jpg" data-url-smart_horizontal="JuXYCNXEvQiM2fKNm00QVSMooTI=/90x56/smart/filters:strip_icc()/" data-url-smart="JuXYCNXEvQiM2fKNm00QVSMooTI=/90x56/smart/filters:strip_icc()/" data-url-feature="JuXYCNXEvQiM2fKNm00QVSMooTI=/90x56/smart/filters:strip_icc()/" data-url-tablet="XJ69xWdN_t_E6GRaEXOv28dp6jw=/160x96/smart/filters:strip_icc()/" data-url-desktop="IPVjTKc9qiMLJv1X3whFpeTgXyk=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Roteador ruim? UsuÃ¡rio ajudam <br />a deixar sua internet &#39;voando&#39;</p></span></a></li></ul><div class="destaque-inferior analytics-area analytics-id-B"><ul><li class="techtudo"><a href="http://www.techtudo.com.br/" title="Teste agora a velocidade da sua internet"><span class="logo"></span><span class="title">Teste agora a velocidade da sua internet &rsaquo;</span></a></li><li class="exty"><a href="http://www.techtudo.com.br/tudo-sobre/extensao-globo-com.html" title="Adicione e veja as notÃ­cias em tempo real"><span class="logo"></span><span class="title">Adicione e veja as notÃ­cias em tempo real &rsaquo;</span></a></li></ul></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Moda/noticia/2015/08/nova-era-do-aluguel-de-roupas.html" alt="Nova era de aluguel de roupas dÃ¡ 
vida para peÃ§as pouco usadas"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/hPqGgeSV-nXROefTP-kXB_MM32A=/filters:quality(10):strip_icc()/s2.glbimg.com/d69iFVNb1AALO0-8h03uTflfWq8=/51x0:559x328/155x100/e.glbimg.com/og/ed/f/original/2015/08/20/pour-vous-aluguel.jpg" alt="Nova era de aluguel de roupas dÃ¡ 
vida para peÃ§as pouco usadas (DviulgaÃ§Ã£o)" title="Nova era de aluguel de roupas dÃ¡ 
vida para peÃ§as pouco usadas (DviulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/d69iFVNb1AALO0-8h03uTflfWq8=/51x0:559x328/155x100/e.glbimg.com/og/ed/f/original/2015/08/20/pour-vous-aluguel.jpg" data-url-smart_horizontal="2nCBzdNbKbHyFO6nTqXgPYfAxEA=/90x56/smart/filters:strip_icc()/" data-url-smart="2nCBzdNbKbHyFO6nTqXgPYfAxEA=/90x56/smart/filters:strip_icc()/" data-url-feature="2nCBzdNbKbHyFO6nTqXgPYfAxEA=/90x56/smart/filters:strip_icc()/" data-url-tablet="NehS0PxDkCegyLUmX9J3M2PxRUo=/122x75/smart/filters:strip_icc()/" data-url-desktop="DyGtAFeBRECPhgRNxqZ9g4OyJXw=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>adeus consumismo!</h3><p>Nova era de aluguel de roupas dÃ¡ <br />vida para peÃ§as pouco usadas</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Beleza/noticia/2015/08/glow-maximo-conheca-diferentes-texturas-de-iluminador-e-aprenda-usa-las-seu-favor.html" alt="ConheÃ§a as diferentes texturas de iluminador e aprenda como usÃ¡-las"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/xI-1jtwsDQmk66hhKWYxf18-fEU=/filters:quality(10):strip_icc()/s2.glbimg.com/6SZerUlcjiD2siR3QJRDFk2dHYU=/0x6:620x406/155x100/e.glbimg.com/og/ed/f/original/2015/08/20/iluminador.jpg" alt="ConheÃ§a as diferentes texturas de iluminador e aprenda como usÃ¡-las (Trunk Archive)" title="ConheÃ§a as diferentes texturas de iluminador e aprenda como usÃ¡-las (Trunk Archive)"
            data-original-image="s2.glbimg.com/6SZerUlcjiD2siR3QJRDFk2dHYU=/0x6:620x406/155x100/e.glbimg.com/og/ed/f/original/2015/08/20/iluminador.jpg" data-url-smart_horizontal="dVEfTFCzrG29_XsD7MPgrb4d0sE=/90x56/smart/filters:strip_icc()/" data-url-smart="dVEfTFCzrG29_XsD7MPgrb4d0sE=/90x56/smart/filters:strip_icc()/" data-url-feature="dVEfTFCzrG29_XsD7MPgrb4d0sE=/90x56/smart/filters:strip_icc()/" data-url-tablet="eYxeZ1JsKN_MazXfc4oT-h1_ux4=/122x75/smart/filters:strip_icc()/" data-url-desktop="UWfpxsuknBSVJGrrjUfkAnFl748=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>item prÃ¡tico</h3><p>ConheÃ§a as diferentes texturas de iluminador e aprenda como usÃ¡-las</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/joias/noticia/2015/08/sobreposicao-de-colares-e-nova-mania-entre-modelos-e-celebs.html" alt="SobreposiÃ§Ã£o de colares Ã© a nova mania entre as modelos e celebs"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/wX5tMcLsQBx7X00FQaTZT1QKIT8=/filters:quality(10):strip_icc()/s2.glbimg.com/H6iI15nu5mbEYZ4tTuGzsXfc5Os=/0x252:1205x1029/155x100/e.glbimg.com/og/ed/f/original/2015/08/20/ale_alnmrbsoio_22.jpg" alt="SobreposiÃ§Ã£o de colares Ã© a nova mania entre as modelos e celebs (DivulgaÃ§Ã£o)" title="SobreposiÃ§Ã£o de colares Ã© a nova mania entre as modelos e celebs (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/H6iI15nu5mbEYZ4tTuGzsXfc5Os=/0x252:1205x1029/155x100/e.glbimg.com/og/ed/f/original/2015/08/20/ale_alnmrbsoio_22.jpg" data-url-smart_horizontal="FscCg0RnbrXzxoB6zkXFGtVzQOM=/90x56/smart/filters:strip_icc()/" data-url-smart="FscCg0RnbrXzxoB6zkXFGtVzQOM=/90x56/smart/filters:strip_icc()/" data-url-feature="FscCg0RnbrXzxoB6zkXFGtVzQOM=/90x56/smart/filters:strip_icc()/" data-url-tablet="O9aOwvo7aEGOdF9ckYzTz4yM27o=/122x75/smart/filters:strip_icc()/" data-url-desktop="ioX375c40mJCpG-A4GKStW-EdWA=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>look de verÃ£o</h3><p>SobreposiÃ§Ã£o de colares Ã© a nova mania entre as modelos e celebs</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/ela/decoracao/casas-futuristas-tem-mesas-de-jantar-que-surgem-do-piso-cama-que-brota-do-teto-17257500" alt="Casas futuristas tÃªm mesas que surgem do piso e cama que sai do teto"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/LDARHf7WyftqGmCkxz2BhQgyJw4=/filters:quality(10):strip_icc()/s2.glbimg.com/VV6Oq1A0QZDpox22mO7B0aqmvrw=/78x16:604x355/155x100/s.glbimg.com/en/ho/f/original/2015/08/21/2b8654c500000578-3204956-yo_homes_is_a_convertible_and_compact_apartment_that_will_be_bui-a-6_1440144163078.jpg" alt="Casas futuristas tÃªm mesas que surgem do piso e cama que sai do teto (DivulgaÃ§Ã£o)" title="Casas futuristas tÃªm mesas que surgem do piso e cama que sai do teto (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/VV6Oq1A0QZDpox22mO7B0aqmvrw=/78x16:604x355/155x100/s.glbimg.com/en/ho/f/original/2015/08/21/2b8654c500000578-3204956-yo_homes_is_a_convertible_and_compact_apartment_that_will_be_bui-a-6_1440144163078.jpg" data-url-smart_horizontal="mMjamr9oJjz73HHEtdrTC2kp5QQ=/90x56/smart/filters:strip_icc()/" data-url-smart="mMjamr9oJjz73HHEtdrTC2kp5QQ=/90x56/smart/filters:strip_icc()/" data-url-feature="mMjamr9oJjz73HHEtdrTC2kp5QQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="TeFcWtwrI-2_7IGGqvlSMKys_z0=/122x75/smart/filters:strip_icc()/" data-url-desktop="qid4EtWX_H_ZB7pTaDSaokUYmSo=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>o que vem por aÃ­...</h3><p>Casas futuristas tÃªm mesas que surgem do piso e cama que sai do teto</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Jardim/Decoracao/noticia/2015/08/base-neutra-e-cores-na-decoracao-transformam-apartamento.html" alt="Base neutra e cores na decoraÃ§Ã£o transformam apartamento; veja"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/XNMBqT8OXtIr_oGGe2hM3EmW0pc=/filters:quality(10):strip_icc()/s2.glbimg.com/YmzGIZBuxQhmhl-pBXMH5bbuVFk=/0x0:930x600/155x100/e.glbimg.com/og/ed/f/original/2015/08/17/cj724_dgm_02.jpg" alt="Base neutra e cores na decoraÃ§Ã£o transformam apartamento; veja (Edu Castello/Editora Globo)" title="Base neutra e cores na decoraÃ§Ã£o transformam apartamento; veja (Edu Castello/Editora Globo)"
            data-original-image="s2.glbimg.com/YmzGIZBuxQhmhl-pBXMH5bbuVFk=/0x0:930x600/155x100/e.glbimg.com/og/ed/f/original/2015/08/17/cj724_dgm_02.jpg" data-url-smart_horizontal="N4lwG987Sf_SoH-l57dcLCPbwzA=/90x56/smart/filters:strip_icc()/" data-url-smart="N4lwG987Sf_SoH-l57dcLCPbwzA=/90x56/smart/filters:strip_icc()/" data-url-feature="N4lwG987Sf_SoH-l57dcLCPbwzA=/90x56/smart/filters:strip_icc()/" data-url-tablet="5BAnJX5dtLbH8H5cvJ5Wx58LJmM=/122x75/smart/filters:strip_icc()/" data-url-desktop="l9FG85TfY264Qjh5IMo-dcR-W_c=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>ousadia decorativa</h3><p>Base neutra e cores na decoraÃ§Ã£o transformam apartamento; veja</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/apartamentos/noticia/2015/08/minimalismo-no-apartamento-preto-e-branco.html" alt="Minimalismo Ã© a escolha em apartamento preto e branco "><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/FTxXI9tBY-QS42n21Q8A1LOmnl0=/filters:quality(10):strip_icc()/s2.glbimg.com/1JM_B-ze85Rs3S0ZvCxImV5W3BM=/0x0:620x400/155x100/e.glbimg.com/og/ed/f/original/2015/08/18/casa-katty-schiebeck-aragon-01.jpg" alt="Minimalismo Ã© a escolha em apartamento preto e branco  (Ruben Ortiz / DivulgaÃ§Ã£o)" title="Minimalismo Ã© a escolha em apartamento preto e branco  (Ruben Ortiz / DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/1JM_B-ze85Rs3S0ZvCxImV5W3BM=/0x0:620x400/155x100/e.glbimg.com/og/ed/f/original/2015/08/18/casa-katty-schiebeck-aragon-01.jpg" data-url-smart_horizontal="3luB6J5GxdlG590v2cOpFfDD7es=/90x56/smart/filters:strip_icc()/" data-url-smart="3luB6J5GxdlG590v2cOpFfDD7es=/90x56/smart/filters:strip_icc()/" data-url-feature="3luB6J5GxdlG590v2cOpFfDD7es=/90x56/smart/filters:strip_icc()/" data-url-tablet="MJ7VS-Ynu5P3Y_FmOn1VG5bt-O0=/122x75/smart/filters:strip_icc()/" data-url-desktop="uOnusDyjSimpGkJDnyfXeabW9F8=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>na espanha...</h3><p>Minimalismo Ã© a escolha em apartamento preto e branco </p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/gente/noticia/2015/08/plastica-courteney-cox-surge-quase-irreconhecivel-em-evento.html" title="Courtney Cox surge com maÃ§Ã£ do rosto maior e levanta suspeitas"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/1DeBgaEk8td1-psV75h3I8OR420=/filters:quality(10):strip_icc()/s2.glbimg.com/BLs8ypDqus3IBqxrPHcqnA5hYyo=/0x23:972x540/245x130/e.glbimg.com/og/ed/f/original/2015/08/21/cox.jpg" alt="Courtney Cox surge com maÃ§Ã£ do rosto maior e levanta suspeitas (ReproduÃ§Ã£o)" title="Courtney Cox surge com maÃ§Ã£ do rosto maior e levanta suspeitas (ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/BLs8ypDqus3IBqxrPHcqnA5hYyo=/0x23:972x540/245x130/e.glbimg.com/og/ed/f/original/2015/08/21/cox.jpg" data-url-smart_horizontal="PJf4At5E-iLzm3Z4Dzj2SeHISbk=/90x56/smart/filters:strip_icc()/" data-url-smart="PJf4At5E-iLzm3Z4Dzj2SeHISbk=/90x56/smart/filters:strip_icc()/" data-url-feature="PJf4At5E-iLzm3Z4Dzj2SeHISbk=/90x56/smart/filters:strip_icc()/" data-url-tablet="J34A2DohRru5IuzQcZcP1luICVs=/160x95/smart/filters:strip_icc()/" data-url-desktop="4UYFKN8KQdaH3RFA6HQmz4zOcYg=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Courtney Cox surge com maÃ§Ã£ do rosto maior e levanta suspeitas</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://globosatplay.globo.com/multishow/v/4385533/?utm_source=globo.com&amp;utm_medium=onias&amp;utm_campaign=ferdinando_cumpadi" title="Cumpadi Washington revela que busca nova danÃ§arina do Tchan"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/hUy4qml1fmnd8UHIfOeu3hYsMwY=/filters:quality(10):strip_icc()/s2.glbimg.com/Bo774L_LP8aXjEjtDHA_faWDo68=/18x5:529x276/245x130/s.glbimg.com/en/ho/f/original/2015/08/21/washington.jpg" alt="Cumpadi Washington revela que busca nova danÃ§arina do Tchan (Globosat)" title="Cumpadi Washington revela que busca nova danÃ§arina do Tchan (Globosat)"
                    data-original-image="s2.glbimg.com/Bo774L_LP8aXjEjtDHA_faWDo68=/18x5:529x276/245x130/s.glbimg.com/en/ho/f/original/2015/08/21/washington.jpg" data-url-smart_horizontal="NAat8K2DE5rvfAiwUiXxXiYDPU0=/90x56/smart/filters:strip_icc()/" data-url-smart="NAat8K2DE5rvfAiwUiXxXiYDPU0=/90x56/smart/filters:strip_icc()/" data-url-feature="NAat8K2DE5rvfAiwUiXxXiYDPU0=/90x56/smart/filters:strip_icc()/" data-url-tablet="azDsdayHUQKQLt75g9OQEYRwJ9A=/160x95/smart/filters:strip_icc()/" data-url-desktop="N2osznVdg8WhzF_for9M2ASF2Z0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Cumpadi Washington revela que busca nova danÃ§arina do Tchan</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/08/laura-keller-fala-do-visual-antes-da-fama-eu-era-o-patinho-feio.html" title="Laura Keller mostra visual antes da fama: &#39;Eu era o patinho feio&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/2LSUC14rPvgXNG_M1RF5zG2_mZo=/filters:quality(10):strip_icc()/s2.glbimg.com/tKXto0x4kJiR_L1RupbQ82dmdjE=/0x37:620x366/245x130/s.glbimg.com/jo/eg/f/original/2015/08/21/02-final.jpg" alt="Laura Keller mostra visual antes da fama: &#39;Eu era o patinho feio&#39; (DivulgaÃ§Ã£o)" title="Laura Keller mostra visual antes da fama: &#39;Eu era o patinho feio&#39; (DivulgaÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/tKXto0x4kJiR_L1RupbQ82dmdjE=/0x37:620x366/245x130/s.glbimg.com/jo/eg/f/original/2015/08/21/02-final.jpg" data-url-smart_horizontal="ww_JHsbukngmNkGf6ynrRFrbO50=/90x56/smart/filters:strip_icc()/" data-url-smart="ww_JHsbukngmNkGf6ynrRFrbO50=/90x56/smart/filters:strip_icc()/" data-url-feature="ww_JHsbukngmNkGf6ynrRFrbO50=/90x56/smart/filters:strip_icc()/" data-url-tablet="1GkX9Mpp-ArdJ0NAD5TluH1TvBc=/160x95/smart/filters:strip_icc()/" data-url-desktop="IdWeCUgCyXoTz1Vdt0oHNNoT_s4=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Laura Keller mostra visual antes da fama: &#39;Eu era o patinho feio&#39;</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://revistamonet.globo.com/Listas/noticia/2015/08/existe-amor-em-hollywood-os-casais-de-celebs-internacionais-mais-duradouros.html" title="Lista reÃºne os casais de astros internacionais mais duradouros
"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/LBOwcsHgzf3DzvxdVdV0h_jMu_M=/filters:quality(10):strip_icc()/s2.glbimg.com/3WaQGM956IL8Vs8Mh67yGDUcPSQ=/0x29:1200x666/245x130/e.glbimg.com/og/ed/f/original/2015/08/20/casais-duradouros-de-hollywood.jpg" alt="Lista reÃºne os casais de astros internacionais mais duradouros
 (Getty Images)" title="Lista reÃºne os casais de astros internacionais mais duradouros
 (Getty Images)"
                    data-original-image="s2.glbimg.com/3WaQGM956IL8Vs8Mh67yGDUcPSQ=/0x29:1200x666/245x130/e.glbimg.com/og/ed/f/original/2015/08/20/casais-duradouros-de-hollywood.jpg" data-url-smart_horizontal="veu85Sa0-GjHWqfwGZxeRWe0ta0=/90x56/smart/filters:strip_icc()/" data-url-smart="veu85Sa0-GjHWqfwGZxeRWe0ta0=/90x56/smart/filters:strip_icc()/" data-url-feature="veu85Sa0-GjHWqfwGZxeRWe0ta0=/90x56/smart/filters:strip_icc()/" data-url-tablet="ee0Y3YktuaP2JMUXBpmvtoEcCuQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="6XkKJKYUpYK-ZSL6XBzyqtJNUJA=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Lista reÃºne os casais de astros internacionais mais duradouros<br /></p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/babilonia/index.html">BABILÃNIA</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/08/danda-foge-disfarcada-de-sp-para-despistar-mafioso.html" title="&#39;I Love ParaisÃ³polis&#39;: Danda foge disfarÃ§ada para despistar mafioso"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/4IRw9sxL-0WzKj7buqPojBYpK-w=/filters:quality(10):strip_icc()/s2.glbimg.com/0j2Z1fJFa0pKh-BmMpSg_6cacoA=/68x10:631x309/245x130/s.glbimg.com/et/gs/f/original/2015/08/20/danda-armandinho.jpg" alt="&#39;I Love ParaisÃ³polis&#39;: Danda foge disfarÃ§ada para despistar mafioso (Raphael Dias/Gshow)" title="&#39;I Love ParaisÃ³polis&#39;: Danda foge disfarÃ§ada para despistar mafioso (Raphael Dias/Gshow)"
                    data-original-image="s2.glbimg.com/0j2Z1fJFa0pKh-BmMpSg_6cacoA=/68x10:631x309/245x130/s.glbimg.com/et/gs/f/original/2015/08/20/danda-armandinho.jpg" data-url-smart_horizontal="tV6LQnSl9JJ58xssUG9xtG6BkNc=/90x56/smart/filters:strip_icc()/" data-url-smart="tV6LQnSl9JJ58xssUG9xtG6BkNc=/90x56/smart/filters:strip_icc()/" data-url-feature="tV6LQnSl9JJ58xssUG9xtG6BkNc=/90x56/smart/filters:strip_icc()/" data-url-tablet="iq0ec45v3_6vSN4YZcC3ln3WHw0=/160x95/smart/filters:strip_icc()/" data-url-desktop="zVDllgTsKFOZ-9vX_xG2KFjj_Ek=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;I Love ParaisÃ³polis&#39;: Danda foge disfarÃ§ada para despistar mafioso</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/08/homem-se-aproxima-de-tapera-e-emilia-fica-desesperada.html" title="&#39;AlÃ©m&#39;: homem se aproxima de tapera e EmÃ­lia fica desesperada"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/RxbKDoLGf6BV_2HJ4uD-B6g3rws=/filters:quality(10):strip_icc()/s2.glbimg.com/bvSL3VwTNsVU2Or02ZbsIgxKlwU=/0x49:690x415/245x130/s.glbimg.com/et/gs/f/original/2015/08/20/emilia-ana-beatriz-nogueira.jpg" alt="&#39;AlÃ©m&#39;: homem se aproxima de tapera e EmÃ­lia fica desesperada (TV Globo)" title="&#39;AlÃ©m&#39;: homem se aproxima de tapera e EmÃ­lia fica desesperada (TV Globo)"
                    data-original-image="s2.glbimg.com/bvSL3VwTNsVU2Or02ZbsIgxKlwU=/0x49:690x415/245x130/s.glbimg.com/et/gs/f/original/2015/08/20/emilia-ana-beatriz-nogueira.jpg" data-url-smart_horizontal="nZw79kShTQj3fZCcq9MGjdd1APk=/90x56/smart/filters:strip_icc()/" data-url-smart="nZw79kShTQj3fZCcq9MGjdd1APk=/90x56/smart/filters:strip_icc()/" data-url-feature="nZw79kShTQj3fZCcq9MGjdd1APk=/90x56/smart/filters:strip_icc()/" data-url-tablet="E9feGIzbn1j9rG5K-dypS0WunZA=/160x95/smart/filters:strip_icc()/" data-url-desktop="A-6FOg1B9PyGc_sYCMg9MpeQ5iw=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;AlÃ©m&#39;: homem se aproxima de tapera e EmÃ­lia fica desesperada</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/08/rodrigo-e-luciana-se-estranham-na-entrada-do-colegio.html" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo e Luciana se estranham na entrada do colÃ©gio"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/zmbrrssk7HNfubPKCvW68ukhPgM=/filters:quality(10):strip_icc()/s2.glbimg.com/Xv0A1c-Cj-soB_y8rzMDcN0RJ9o=/66x93:590x372/245x130/s.glbimg.com/et/gs/f/original/2015/08/21/rodrigo_e_luciana_se_enfrentam.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Rodrigo e Luciana se estranham na entrada do colÃ©gio (Isabella Pinheiro/Gshow)" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo e Luciana se estranham na entrada do colÃ©gio (Isabella Pinheiro/Gshow)"
                    data-original-image="s2.glbimg.com/Xv0A1c-Cj-soB_y8rzMDcN0RJ9o=/66x93:590x372/245x130/s.glbimg.com/et/gs/f/original/2015/08/21/rodrigo_e_luciana_se_enfrentam.jpg" data-url-smart_horizontal="YiE9Ex-z3LhX1nPSVQHJAhxVN4c=/90x56/smart/filters:strip_icc()/" data-url-smart="YiE9Ex-z3LhX1nPSVQHJAhxVN4c=/90x56/smart/filters:strip_icc()/" data-url-feature="YiE9Ex-z3LhX1nPSVQHJAhxVN4c=/90x56/smart/filters:strip_icc()/" data-url-tablet="ZGXC60a9wWk3eTnG_lMDshuxY6c=/160x95/smart/filters:strip_icc()/" data-url-desktop="k7LapnyAoCZMKb9c75Q4_nLy3Bo=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Rodrigo e Luciana se estranham na entrada do colÃ©gio</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/08/fernando-rocha-requebra-ate-o-chao-com-mariana-ferrao.html" title="Fernando Rocha requebra atÃ© o chÃ£o e ensina passos do &#39;DanÃ§a&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/Rb3rEGfDvwhZH7tP-0cYvUGRxnI=/filters:quality(10):strip_icc()/s2.glbimg.com/Ad8BMNyBehYYtm8M-VOOp6_TFxk=/101x53:521x276/245x130/s.glbimg.com/et/gs/f/original/2015/08/21/mariana-ferrao-fernando-roc.jpg" alt="Fernando Rocha requebra atÃ© o chÃ£o e ensina passos do &#39;DanÃ§a&#39; (Arquivo Pessoal)" title="Fernando Rocha requebra atÃ© o chÃ£o e ensina passos do &#39;DanÃ§a&#39; (Arquivo Pessoal)"
                    data-original-image="s2.glbimg.com/Ad8BMNyBehYYtm8M-VOOp6_TFxk=/101x53:521x276/245x130/s.glbimg.com/et/gs/f/original/2015/08/21/mariana-ferrao-fernando-roc.jpg" data-url-smart_horizontal="yx7mIgdhQDlE8xKE1Mn7hJawzrg=/90x56/smart/filters:strip_icc()/" data-url-smart="yx7mIgdhQDlE8xKE1Mn7hJawzrg=/90x56/smart/filters:strip_icc()/" data-url-feature="yx7mIgdhQDlE8xKE1Mn7hJawzrg=/90x56/smart/filters:strip_icc()/" data-url-tablet="QkC4tp8TLEN9EenCjv5j4GIS9AQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="rYVEbjg_v7NJ3RNzCkq_E-WmWIM=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Fernando Rocha requebra atÃ© o chÃ£o e ensina passos do &#39;DanÃ§a&#39;</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/cultura/musica/jon-bon-jovi-grava-musica-em-mandarim-17258130" title="Jon Bon Jovi grava mÃºsica em mandarim e vai fazer apresentaÃ§Ã£o na China (Owen Sweeney/Invision/AP)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/G003PUhOKXjetFJKSNcR4rnC-ok=/filters:quality(10):strip_icc()/s2.glbimg.com/oIg0cPrCtM-xtLL2HABkGt-oCbQ=/211x90:514x272/215x130/s.glbimg.com/en/ho/f/original/2015/08/21/20150820202600455ap.jpg"
                data-original-image="s2.glbimg.com/oIg0cPrCtM-xtLL2HABkGt-oCbQ=/211x90:514x272/215x130/s.glbimg.com/en/ho/f/original/2015/08/21/20150820202600455ap.jpg" data-url-smart_horizontal="B_NinqD4F0RSNgjCmWXEie990L4=/90x56/smart/filters:strip_icc()/" data-url-smart="B_NinqD4F0RSNgjCmWXEie990L4=/90x56/smart/filters:strip_icc()/" data-url-feature="B_NinqD4F0RSNgjCmWXEie990L4=/90x56/smart/filters:strip_icc()/" data-url-tablet="_oHBeAXxJb3uYUMRXIu0Je-uDFE=/120x80/smart/filters:strip_icc()/" data-url-desktop="ajrtXC3CLZVyTCqXfT-7WAWk97s=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Jon Bon Jovi grava mÃºsica em mandarim e vai fazer apresentaÃ§Ã£o na China</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/musica/noticia/2015/08/nx-zero-supera-crise-existencial-e-diz-ter-lutado-para-nao-virar-um-produto.html" title="NX Zero supera &#39;crise existencial&#39; e diz ter lutado para nÃ£o &#39;virar um produto&#39; (Marcelo Brandt / G1)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/DLvnTSnghXAoOeeKsRkTMMnlySg=/filters:quality(10):strip_icc()/s2.glbimg.com/clJICQl7waRKKcVr1PvkNBK_IzY=/93x53:512x307/215x130/s.glbimg.com/jo/g1/f/original/2015/08/18/img_3331_nx-zero_marcelo_brandt_g1.jpg"
                data-original-image="s2.glbimg.com/clJICQl7waRKKcVr1PvkNBK_IzY=/93x53:512x307/215x130/s.glbimg.com/jo/g1/f/original/2015/08/18/img_3331_nx-zero_marcelo_brandt_g1.jpg" data-url-smart_horizontal="Bqp0cLmV0FYYlE3JBftCENa4LA4=/90x56/smart/filters:strip_icc()/" data-url-smart="Bqp0cLmV0FYYlE3JBftCENa4LA4=/90x56/smart/filters:strip_icc()/" data-url-feature="Bqp0cLmV0FYYlE3JBftCENa4LA4=/90x56/smart/filters:strip_icc()/" data-url-tablet="fFGI_oZ1Etl68JBbSz0uJA3OvTo=/120x80/smart/filters:strip_icc()/" data-url-desktop="u746n_ltuqFmey_0pckdrvRyNPA=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">NX Zero supera &#39;crise existencial&#39; e diz ter lutado para nÃ£o &#39;virar um produto&#39;</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/rio-de-janeiro/noticia/2015/08/funk-gospel-atrai-fas-e-fieis-com-o-passinho-do-abencoado-no-rio.html" title="Funk gospel atrai fÃ£s e 
fiÃ©is com o &#39;Passinho do abenÃ§oado&#39; no Rio; assista (DivulgaÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/QQx3ru6Px-iUttAtVhQTxg_Ynsk=/filters:quality(10):strip_icc()/s2.glbimg.com/CEUouCxuFh3M594iRKUVlstM-UM=/0x114:300x296/215x130/s.glbimg.com/jo/g1/f/original/2015/08/21/funk-gospel-dupla2.jpg"
                data-original-image="s2.glbimg.com/CEUouCxuFh3M594iRKUVlstM-UM=/0x114:300x296/215x130/s.glbimg.com/jo/g1/f/original/2015/08/21/funk-gospel-dupla2.jpg" data-url-smart_horizontal="gHL5B8cuhd29cpNQl21sX3Jc6Xo=/90x56/smart/filters:strip_icc()/" data-url-smart="gHL5B8cuhd29cpNQl21sX3Jc6Xo=/90x56/smart/filters:strip_icc()/" data-url-feature="gHL5B8cuhd29cpNQl21sX3Jc6Xo=/90x56/smart/filters:strip_icc()/" data-url-tablet="gDqQ9fjlf1QU1EL2e4XR4S2LYlg=/120x80/smart/filters:strip_icc()/" data-url-desktop="FncN43eEndlxeEW3D9WqYA6TDRk=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Funk gospel atrai fÃ£s e <br />fiÃ©is com o &#39;Passinho do abenÃ§oado&#39; no Rio; assista</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/noticia/2015/08/gaeco-pede-explicacoes-sobre-exploracao-sexual-no-jardim-paraiso.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">PolÃ­cia pede explicaÃ§Ãµes sobre alvarÃ¡s em bairro com prostituiÃ§Ã£o e trÃ¡fico</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/bemestar/noticia/2015/08/grupo-altera-foto-de-mulher-plus-size-e-prega-beleza-magra-em-rede-social.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">EmpresÃ¡rio Ã© preso em flagrante em hospital do Rio apÃ³s atropelamento</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/rs/rio-grande-do-sul/noticia/2015/08/com-video-policia-busca-suspeitos-de-assalto-e-morte-em-onibus-no-rs.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">VÃ­deo: assaltantes matam deficiente auditivo e passam por cima de vÃ­tima</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/08/bmw-de-pm-assassinado-em-praia-do-rio-e-apreendido-para-pericia.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">BMW de PM assassinado em praia do Rio Ã© apreendido para perÃ­cia</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/bemestar/noticia/2015/08/grupo-altera-foto-de-mulher-plus-size-e-prega-beleza-magra-em-rede-social.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Grupo altera foto de mulher plus size <br />e prega &#39;beleza magra&#39; em rede social</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/rs/noticia/2015/08/apos-tiro-em-carro-jogadores-cogitam-deixar-o-juventude-nao-tem-clima.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">ApÃ³s tiro em carro, jogadores cogitam deixar o Juventude: &#39;NÃ£o tem clima&#39;</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/david-brazil-posta-foto-de-neymar-recuperado-de-caxumba.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Neymar aparece apÃ³s a caxumba e faz careta em foto de amigo: &#39;O major voltou&#39;</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/programas/redacao-sportv/noticia/2015/08/hoje-yago-pikachu-e-melhor-que-ronaldinho-gaucho-diz-andre-rizek.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Pikachu Ã© comparado a R10: &#39;Por que esse cara nunca foi convocado?&#39;</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/08/ganso-esbraveja-com-torcedor-e-discute-com-carlinhos-no-morumbi.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ganso esbraveja com torcedor e discute <br />com Carlinhos no Morumbi; veja o vÃ­deo </span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globotv.globo.com/rede-globo/jornal-da-globo/t/edicoes/v/confira-os-gols-das-oitavas-de-final-da-copa-do-brasil-desta-quinta-feira-20/4409425/" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">ChutaÃ§o aos 48 do 2Âº tempo dÃ¡ vitÃ³ria ao Flu; <br />veja gols pela Copa do Brasil e Sul-Americana</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/08/denise-rocha-exibe-bumbum-empinado-e-cintura-fina-na-praia.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;FuracÃ£o&#39; chama atenÃ§Ã£o em praia de Miami com cintura finÃ­ssima e muitas curvas</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/babilonia/Vem-por-ai/noticia/2015/08/ultimos-capitulos-beatriz-sequestra-regina-e-mata-carlos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ãltimos capÃ­tulos de &#39;BabilÃ´nia&#39;: Beatriz sequestra Regina e mata Carlos</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/paparazzo/ensaio/aryane-steinkopf-e-beto-malfacini.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Aryane Steinkopf e Beto Malfacini fazem 1Âº Paparazzo sem Photoshop; veja fotos</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/gravidez/noticia/2015/08/bolsa-dagua-de-bella-falconi-estoura-bebe-chega-no-final-da-tarde.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Em trabalho de parto desde ontem, bolsa de Bella Falconi rompe, e marido posta</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/verdades-secretas/Vem-por-ai/noticia/2015/08/hilda-revela-segredo-vou-morrer.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;Verdades Secretas&#39;: Hilda diz que vai <br />morrer e recusa pedido de casamento </span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://tvg.globo.com/programas/pe-na-cova/index.html">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://tvg.globo.com/programas/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/b43da6b3453a.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><style> @media (max-width:959px){#banner_slb_fim>div>iframe,#banner_slb_meio>div>iframe{-ms-zoom:.72;-moz-transform:scale(.72);-moz-transform-origin:0 0;-o-transform:scale(.72);-o-transform-origin:0 0;-webkit-transform:scale(.72);-webkit-transform-origin:0 0;vertical-align:middle;display:inline-block}#banner_slb_fim>div,#banner_slb_meio>div{background-color:white;overflow:hidden}#banner_slb_fim,#banner_slb_meio{overflow:visible}}.without-opec{display:block !important}.opec-x60{height:auto}</style><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 10};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 21/08/2015 21:59:00 -->
