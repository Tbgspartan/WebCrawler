<!DOCTYPE html>
<html id="atomic" lang="en-US" class="atomic my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr desktop Desktop bkt201">
<head>
    <title>Yahoo</title><meta http-equiv="x-dns-prefetch-control" content="on"><link rel="dns-prefetch" href="//s.yimg.com"><link rel="preconnect" href="//s.yimg.com"><link rel="dns-prefetch" href="//y.analytics.yahoo.com"><link rel="preconnect" href="//y.analytics.yahoo.com"><link rel="dns-prefetch" href="//geo.query.yahoo.com"><link rel="preconnect" href="//geo.query.yahoo.com"><link rel="dns-prefetch" href="//csc.beap.bc.yahoo.com"><link rel="preconnect" href="//csc.beap.bc.yahoo.com"><link rel="dns-prefetch" href="//geo.yahoo.com"><link rel="preconnect" href="//geo.yahoo.com"><link rel="dns-prefetch" href="//comet.yahoo.com"><link rel="preconnect" href="//comet.yahoo.com">    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="description" content="News, email and search are just the beginning. Discover more every day. Find your yodel.">
    <meta name="keywords" content="yahoo, yahoo home page, yahoo homepage, yahoo search, yahoo mail, yahoo messenger, yahoo games, news, finance, sport, entertainment">
    <meta property="og:title" content="Yahoo" />
    <meta property="og:type" content='website' />
    <meta property="og:url" content="http://www.yahoo.com" />
    <meta property="og:description" content="News, email and search are just the beginning. Discover more every day. Find your yodel."/>
    <meta property="og:image" content="https://s.yimg.com/dh/ap/default/130909/y_200_a.png"/>
    <meta property="og:site_name" content="Yahoo" />
    <meta property="fb:app_id" content="90376669494" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="icon" sizes="any" mask href="/sy/os/mit/media/p/common/images/favicon_new-7483e38.svg">
<meta name="theme-color" content="#400090">
    <link rel="shortcut icon" href="/sy/rz/l/favicon.ico" />
    <link rel="canonical" href="https://www.yahoo.com/" />        <link href="/sy/os/fp/atomic-css.d1bbf0c4.css" rel="stylesheet" type="text/css">
            
    
    
    
    <!-- streaming unlocked -->

    <!-- MapleTop -->
    
    
<link rel="stylesheet" type="text/css" href="/sy/zz/combo?nn/lib/metro/g/myy/advance_base_rc4_0.0.31.css&nn/lib/metro/g/myy/font_rc4_spdy_0.0.31.css&nn/lib/metro/g/myy/yahoo20_grid_0.0.96.css&nn/lib/metro/g/myy/video_styles_0.0.23.css&nn/lib/metro/g/myy/advance_color_0.0.7.css&nn/lib/metro/g/theme/viewer_modal_0.0.29.css&nn/lib/metro/g/theme/yglyphs-legacy_0.0.5.css&nn/lib/metro/g/sda/fp_sda_0.0.4.css&nn/lib/metro/g/sda/sda_advance_0.0.8.css&nn/lib/metro/g/fpfooter/advance_0.0.4.css&/os/stencil/3.1.0/styles-ltr.css&/os/yc/css/bundle.c60a6d54.css" />
    <link rel="search" type="application/opensearchdescription+xml" href="https://search.yahoo.com/opensearch.xml" title="Yahoo Search" />
    
    <script>
    var myYahoostartTime = new Date(),
        afPerfHeadStart=new Date().getTime(),
        ie;

    
    document.documentElement.className += ' JsEnabled jsenabled';</script>
    

<style>.breakingnews.gradient-1 {
    background: #ff7617; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #ff7617 0%, #ff9b00 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#ff7617), color-stop(65%,#ff9b00)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* IE10+ */
    background: linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7617', endColorstr='#ff9b00',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}

.breakingnews.gradient-2 {
    background: #e91857; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #e91857 0%, #ff353c 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#e91857), color-stop(65%,#ff353c)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* IE10+ */
    background: linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e91857', endColorstr='#ff353c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}</style>
<style>.js-stream-adfdb-reason {
    display: none;
}
.js-stream-adfdb-options .js-stream-adfdb-other-selected + .js-stream-adfdb-reason {
    display: block;
}

.stream-collapse {
    max-height: 0;
    -webkit-transition: max-height 0.3s ease;
    -moz-transition: max-height 0.3s ease;
    -o-transition: max-height 0.3s ease;
    transition: max-height 0.3s ease;
}

.RevealNested-on .ActionDislike {
    display: none;
}

.streamv2 .stream-share-open .js-stream-share-panel {
    height: auto !important;
    opacity: 1 !important;
}

/**
* Tooltips
*/
.js-stream-actions a:hover .ActionTooltip.hide,
.js-stream-actions .ActionTooltip,
.js-stream-actions a:active .ActionTooltip,
.js-stream-actions a:focus .ActionTooltip {
    clip: rect(1px 1px 1px 1px);
    clip: rect(1px,1px,1px,1px);
    height: 1px;
    width: 1px;
    overflow: hidden;
    line-height: 1.4;
    white-space: nowrap
}

.js-stream-actions a:hover .ActionTooltip {
    clip: rect(auto auto auto auto);
    clip: auto;
    height: auto;
    width: auto;
    overflow: visible;
    -webkit-transform: translateX(-50%) !important;
    -ms-transform: translateX(-50%) !important;
    transform: translateX(-50%) !important;
    *min-width: 80px;
    *margin-right: -48px;
    width: 136px;
    text-align: center;
}

.streamv2 .js-stream-dense .strm-left {
    max-width: 190px;
    width: 29%;
}

.streamv2 .js-stream-sparse .strm-left {
    width: 33%;
    max-width: 230px;
}
.streamv2 .js-stream-sparse .strm-right {
    width: 57%;
}
.streamv2 .js-stream-sparse .strm-full {
    width: 90%;
}

.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-left {
    width: 62.825%;
    max-width: 441px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-left {
    width: 72%;
    max-width: 508px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-right,
.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-right {
    width: auto;
    max-width: none;
}


.streamv2 .strm-right-menu-roundup .strm-right .js-stream-content-link:before {
    content: '';
    vertical-align: middle;
    height: 100%;
    display: inline-block;
}

.streamv2 .strm-chevron {
    display: none;
}

.streamv2 .strm-right-menu-roundup .js-stream-content-link.selected .strm-chevron,
.streamv2 .strm-right-menu-roundup .js-stream-content-link:hover .strm-chevron {
    display: block;
}

.streamv2 .rounded-img {
    border-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-main-img .rounded-img {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.streamv2 .js-stream-sparse .storyline-img-0 {
    border-bottom-left-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-img-1 {
    border-bottom-right-radius: 3px;
}

.ua-ff#atomic .strm-headline-label {
    margin-bottom: 4px;
}
/* cluster image gradient transparent to dark overlay */
.streamv2 .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 32%, rgba(0,0,0,0.65) 97%, rgba(0,0,0,0.65) 98%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(32%,rgba(0,0,0,0)), color-stop(97%,rgba(0,0,0,0.65)), color-stop(98%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

/* cluster image gradient transparent to dark overlay */
.streamv2 .js-stream-roundup .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 52%, rgba(0,0,0,0.85) 107%, rgba(0,0,0,0.85) 78%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(52%,rgba(0,0,0,0)), color-stop(107%,rgba(0,0,0,0.85)), color-stop(78%,rgba(0,0,0,0.85))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

.ua-ie10 .streamv2 .js-stream-related-content ul,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip > ul {
    margin-right: -7px !important;
}

.ua-ie10 .streamv2 .js-stream-related-content li,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip .W\(25\%\) {
    width: 24.5% !important;
}

.streamv2 .js-stream-side-buttons li:nth-child(1) .ActionTooltip {
    top: -1px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(1) .ActionTooltip {
    top: 0px;
}

.streamv2 .js-stream-side-buttons li:nth-child(2) .ActionTooltip {
    top: 22px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(2) .ActionTooltip {
    top: 40px;
}
.streamv2 .js-stream-side-buttons li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons li:nth-child(3) .js-stream-share-panel {
    top: 45px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .js-stream-share-panel {
    top: 64px;
}
.streamv2 .js-stream-side-buttons li:nth-child(4) .js-stream-share-panel {
    top: 86px;
}

.streamv2 .js-stream-side-buttons .ActionComments {
    margin-top: 6px;
}
.streamv2 .js-stream-side-buttons.has-comments .ActionComments {
    margin-top: 0px;
}
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\$c_icon\),
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\#96989f\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\$c_icon\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\#96989f\) {
    color: inherit !important;
}
.streamv2 .js-stream-side-buttons .ActionComments .ActionTooltip {
    margin-top: 15px;
}

.js-stream-comment-counter-update {
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
}

/* Only set opacity to 0 for animation when js is enabled and css3 supported */
.JsEnabled .streamv2 .js-stream-comment-hidden:nth-of-type(1n) {
    opacity: 0;
}

.JsEnabled .streamv2 .animated {
    -webkit-animation-duration: 1.5s;
    animation-duration: 1.5s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
}

@-webkit-keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

@keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

.JsEnabled .streamv2 .fadeOut {
    -webkit-animation-name: fadeOut;
    animation-name: fadeOut;
}

@-webkit-keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@-webkit-keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

@keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

.JsEnabled .streamv2 .fadeIn {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
}


.JsEnabled .streamv2 .js-stream-roundup-filmstrip .fadeIn {
    -webkit-animation-name: fadeInNtk;
    animation-name: fadeInNtk;
}

.streamv2 .js-stream-comment .js-stream-cmnt-up:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-up.selected {
    color: #1AC567 !important;
}
.streamv2 .js-stream-comment .js-stream-cmnt-down:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-down.selected,
.streamv2 .js-stream-comment .js-stream-cmnt-flag:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-flag.selected {
    color: #F0162F !important;
}

/* to show the dislike ad tooltip */
#atomic .Collapse-opened .js-stream-related-item-ad {
    overflow: visible;
}
</style>
 

<style>.js-activitylist-item .orb {
    background-position: 0 0;
    width: 80px;
    height: 80px;
}

.js-activitylist-item .hexagon {
    background-position: -81px 0;
    width: 80px;
    height: 90px;
}

.js-activitylist-item .ribbon {
    background-position: -162px 0;
    width: 91px;
    height: 31px;
}
</style>
 

<style>#uh-search .yui3-aclist {
    width: inherit !important;
}
#uh-search .yui3-aclist-content {
    background-color: #fff;
    text-align: left;
    border: 1px solid #ccc;
    margin-top: 1px;
    border-top: 0;
}
#uh-search .yui3-aclist-list {
    margin: 0;
    line-height: 1.1;
}
#uh-search .yui3-aclist-item {
    padding: 5px 0 6px 0;
    font-size: 18px;
    font-weight: bold;
}
#uh-search .yui3-aclist-item:hover,
#uh-search .yui3-aclist-item-active {
    background-color: #c6d7ff;
}
#uh-search .yui3-aclist-item {
    padding-left: 10px;
    padding-right: 10px;
}
#uh-search .yui3-highlight {
    font-weight: 200;
}
#uh-search-box::-ms-clear {
    display: none;
}


/* Instant Search Styles */

#uh-search .yui3-aclist-item {
    line-height: 0.9;
}
#InstantSearchMask {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s linear 0.15s, opacity 0.15s linear;
}
.iSearch-display .Col1,
.iSearch-display .Col2,
.iSearch-display .Col3 {
    height: 0;
    overflow: hidden;
}
.iSearch-display #InstantSearch {
    visibility: visible;
}
.iSearch-mask #InstantSearchMask {
    visibility: visible;
    opacity: 1;
    transition: visibility 0s linear, opacity 0.15s linear;
}
.iSearch-loading.iSearch-mask #InstantSearchMask,
.iSearch-loading #InstantSearchMask {
    visibility: visible;
    opacity: .7;
}


/* Firefox 28 and below fix */
#uh-search-box,
#uh-ghost-box,
.instant-filters,
.instant-results {
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
/* ----------------------- */
</style>
 

<style>@charset "UTF-8";.Lb-Close-Icon,.icon-share-close,.icons-arrow,.icons-slideshow-icon{background-color:transparent;background-image:url(/sy/os/publish-images/news/2014-04-23/65fcff60-cb23-11e3-bead-55c0602d5659_icons-sb930b067ee.png);background-repeat:no-repeat}#Share .icon-share-close{width:30px;height:25px;background-size:cover;background-position:8px -72px}.Hl-Viewer{background:#fff}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay{position:absolute;color:#fff;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodâ¦EiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDAwMDAiIHN0b3Atb3BhY2l0eT0iMC45NSIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==);background:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,rgba(0,0,0,0)),color-stop(100%,rgba(0,0,0,.95)));background:-moz-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:-webkit-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95))}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline{text-shadow:0 1px 0 #000}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline-Box{position:absolute}.Hl-Viewer .Content-Body{color:#000;font-weight:300;letter-spacing:.3px;word-wrap:break-word;word-break:break-word}.Hl-Viewer .Content-Body>p{margin-bottom:1.45em;margin-top:1.45em}.Hl-Viewer .Content-Body h1,.Hl-Viewer .Content-Body h2,.Hl-Viewer .Content-Body h3,.Hl-Viewer .Content-Body h4,.Hl-Viewer .Content-Body h5,.Hl-Viewer .Content-Body h6{font-weight:400}.Hl-Viewer .Content-Body h1{font-size:18px;font-size:1.8rem}.Hl-Viewer .Content-Body h2{font-size:17px;font-size:1.7rem}.Hl-Viewer .Content-Body h3{font-size:16px;font-size:1.6rem}.Hl-Viewer .Content-Body h4{font-size:15px;font-size:1.5rem}.Hl-Viewer .Content-Body h5{font-size:14px;font-size:1.4rem}.Hl-Viewer .Content-Body h6{font-size:13px;font-size:1.3rem}.Hl-Viewer .icons-slideshow-icon{left:5%;top:5%;height:46px;width:47px;background-size:cover}.Hl-Viewer blockquote{padding:5px 10px;quotes:"\201C" "\201C" "\201C" "\201C";line-height:18px}.Hl-Viewer blockquote:before{color:#ccc;content:open-quote;font-size:3em;line-height:.1em;vertical-align:-.4em}.Hl-Viewer blockquote p{display:inline;color:#747474;font-weight:400}.Hl-Viewer blockquote p:after{content:"\A";white-space:pre}.Hl-Viewer .Credit,.Hl-Viewer .Provider{letter-spacing:.5px}.JsEnabled .ImageLoader-Delayed,.JsEnabled .ImageLoader-Loaded{-moz-transition-duration:.2s;-o-transition-duration:.2s;-webkit-transition-duration:.2s;transition-duration:.2s;-moz-transition-property:opacity;-o-transition-property:opacity;-webkit-transition-property:opacity;transition-property:opacity;-moz-transition-timing-function:ease-out;-o-transition-timing-function:ease-out;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}#Stencil .Bdrs-100{border-top-left-radius:100px;border-top-right-radius:100px;border-bottom-left-radius:100px;border-bottom-right-radius:100px}.Slideshow-Lightbox .lb-meta-content{padding-bottom:30px;background:#fff}.Slideshow-Lightbox .lb-meta-content .lb-meta-txt-container-full,.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-short{display:none}.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-full{display:block}.Slideshow-Lightbox .hide-meta .lb-meta-content{display:none}.Slideshow-Lightbox .Lb-Close-Icon{background-size:cover;width:47px;height:47px;z-index:202;background-position:0 -46px}.Reader-open .BrandBar,.Reader-open .rmp-TDYDBM{display:none}.Content-Body .Editorial-Left{float:left;margin-right:30px}.Content-Body .Editorial-Right{float:right;margin-left:30px}.Content-Body .Editorial-Left,.Content-Body .Editorial-Right{margin-top:0}#hl-viewer .Content-Col,.MagOn .Content-Col{margin-left:300px;margin-right:300px;min-height:100px}.HideCentralColumn .Content-Col{margin-left:150px;margin-right:150px}#hl-viewer{min-height:500px}.ShareBtns a:hover{opacity:.3;filter:alpha(opacity=33)}.modal-actions{bottom:3px}.modal-actions .ShareBtns a:hover{opacity:1;filter:alpha(opacity=100)}#hl-viewer:focus{outline:0}#hl-viewer .Timestamp{color:#abaeb7}#hl-viewer .Viewer-Close-Btn{position:fixed;top:10px;width:28px;height:28px;color:#fff;background-color:#2D1152;font-size:16px;margin-left:11px;z-index:5}#hl-viewer .ShareBtns .Share-Btn{border-radius:50px;width:17px;height:17px;line-height:1.3}#hl-viewer .ShareBtns .Share-Btn:hover{text-decoration:none}#hl-viewer .slideshow-carousel{background:#212124;padding-bottom:66%}#hl-viewer .lb-meta-content{border-bottom:1px solid #e8e8e8}#hl-viewer .lb-meta-caption-container{-webkit-font-smoothing:antialiased}#hl-viewer .lb-meta-txt-container.caption-scroll{max-height:4.3em;overflow-y:auto}#hl-viewer .viewer-wrapper{background:#fff;margin:0 0 30px 58px;min-width:980px}#hl-viewer .viewer-wrapper.mega-modal{min-height:750px}#hl-viewer .content-modal,#hl-viewer .js-viewer-slot-readMore{display:inline-block;width:640px;margin-right:29px;vertical-align:top}#hl-viewer .js-slider-item.first .content-modal{padding-top:0}#hl-viewer .Content-Body{padding-bottom:50px;border-bottom:1px solid #c8c8c8}#hl-viewer .Content-Body i{font-style:italic}#hl-viewer .js-slider-item.last .Content-Body{padding-bottom:0;border-bottom:none}#hl-viewer .Content-Body p:last-child{margin-bottom:0!important}#hl-viewer .js-slider-item.multirightrail .Content-Body.multirightrail{border-bottom:none}#hl-viewer .js-slider-item.multirightrail .content-modal{padding-top:0}#hl-viewer .js-slider-item.multirightrail{border-top:1px solid #B2B2B2;padding-top:50px}#hl-viewer .js-slider-item.multirightrail.first{padding-top:0;border-top:none}#hl-viewer .modal-aside.single{position:absolute;right:0;float:none;width:300px}#hl-viewer .modal-aside{display:inline-block;width:300px;position:relative;vertical-align:top;z-index:1}#hl-viewer .slideshow-carousel .main-col{padding-top:15px}#hl-viewer .index-count{color:#878c91;position:absolute;left:12px;bottom:7px;-webkit-font-smoothing:antialiased}#hl-viewer .js-lb-next,#hl-viewer .js-lb-prev{top:50%;z-index:3;width:28px;color:#878c91;font-size:30px;margin-top:-17px}#hl-viewer .js-lb-next:hover,#hl-viewer .js-lb-prev:hover{color:#fff}#hl-viewer .js-lb-next.Disabled,#hl-viewer .js-lb-prev.Disabled{display:none}#hl-viewer .js-lb-next:focus,#hl-viewer .js-lb-prev:focus{outline:0}#hl-viewer .slideshowv2 .slv2-carousel{padding-bottom:78%}#hl-viewer .slideshowv2 .slv2-carousel .main-col{padding-top:20px;padding-bottom:60px}#hl-viewer .slideshowv2 .slv2-carousel .js-thm-sl-icon{color:#fff;font-size:25px;top:12px;display:none}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-slider-ct{background-color:rgba(33,33,35,.5);border-radius:10px}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-car-mask{width:300px;opacity:1}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .js-thm-sl-icon{display:inline-block}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-pic{width:52px;height:52px}#hl-viewer .slideshowv2 .js-lb-next,#hl-viewer .slideshowv2 .js-lb-prev{opacity:.8;background:#444;color:#fff;width:45px;height:45px;padding:0;border-radius:3px}#hl-viewer .slideshowv2 .js-lb-next:hover,#hl-viewer .slideshowv2 .js-lb-prev:hover{background:#333;opacity:1}#hl-viewer .slideshowv2 .index-count{bottom:inherit;left:inherit;color:#000;position:relative;font-size:19px;font-size:1.9rem;font-weight:500;margin-bottom:8px}#hl-viewer .slideshowv2 .slide-title{font-weight:700;margin-bottom:4px}#hl-viewer .slideshowv2 .lb-meta-txt-container a:hover,#hl-viewer .slideshowv2 .lb-meta-txt-container a:link,#hl-viewer .slideshowv2 .lb-meta-txt-container a:visited{color:#188fff}#hl-viewer .slideshowv2 .sl-thumbnails{bottom:10px}#hl-viewer .slideshowv2 .sl-slider-ct{width:350px}#hl-viewer .slideshowv2 .sl-car-mask{width:150px;opacity:.5;transition:height .2s}#hl-viewer .slideshowv2 .sl-carousel .Selected .sl-pic{border:2px solid #fff}#hl-viewer .slideshowv2 .sl-carousel .sl-pic{width:22px;height:22px;border:2px solid transparent}#hl-viewer .Headline{line-height:1.2;margin-bottom:12px;font-size:30px;font-size:3rem}#hl-viewer .Headline-Box{margin-bottom:10px}#hl-viewer .Off-Network{border:1px solid #188FFF;padding:8px 20px;color:#188FFF!important;font-weight:700;text-decoration:none}#hl-viewer .Off-Network:hover{color:#fff!important;background:#0078ff;border-color:#0078ff}#hl-viewer .Content-Body p,#hl-viewer .remaining-body p{margin-bottom:1.1em;margin-top:0}#hl-viewer .Content-Body p p,#hl-viewer .remaining-body p p{margin:0}#hl-viewer img.Mb-20+p{margin-top:0}#hl-viewer .Content-Body,#hl-viewer .remaining-body{font-weight:400;font-size:15px;font-size:1.55rem;line-height:1.5;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .Content-Body #photo_copyright,#hl-viewer .Content-Body .caption,#hl-viewer .Content-Body .credit,#hl-viewer .Content-Body .source,#hl-viewer .remaining-body #photo_copyright,#hl-viewer .remaining-body .caption,#hl-viewer .remaining-body .credit,#hl-viewer .remaining-body .source{font-size:11px;font-size:1.1rem;color:#b2b2b2}#hl-viewer h2,#hl-viewer h3{font-size:22px;font-size:2.2rem;margin-bottom:.6em}#hl-viewer h4,#hl-viewer h5,#hl-viewer h6{font-size:19px;font-size:1.9rem;margin-bottom:.6em}#hl-viewer .Embed-Img{margin-bottom:20px}#hl-viewer .image{margin-bottom:20px;display:block;line-height:1}#hl-viewer .image .Embed-Img{margin-bottom:0}#hl-viewer .sidekick.fp h4{font-size:13px;font-size:1.3rem;margin-bottom:0}#hl-viewer .twitter-tweet-rendered{margin:10px auto}#hl-viewer #photo_copyright{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .alignright{float:right;margin:0 0 10px 10px}#hl-viewer .alignleft{float:left;margin:0 10px 10px 0}#hl-viewer .pmc-related-type{margin-right:10px}#hl-viewer blockquote{line-height:1.6}#hl-viewer p:empty{display:none}#hl-viewer .Ad-Viewer blockquote p,#hl-viewer .Hl-Viewer blockquote p{display:block}#hl-viewer .hl-ad-LREC{height:250px}#hl-viewer .source{font-size:14px;font-size:1.4rem}#hl-viewer .attribution{font-size:12px}#hl-viewer .first .Fixed-Header{top:-100px}#hl-viewer .Fixed-Header{transition:opacity .8s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .8s ease-in-out,top .6s ease-in-out;position:fixed;top:0;width:600px;padding:14px 0 0}#hl-viewer .Fixed-Header .source{max-width:540px}#hl-viewer .Fixed-Header .modal-actions{top:11px;left:540px;width:100px!important}#hl-viewer .Fixed-Header-Wrap{position:fixed;top:-100px;width:100%;z-index:4;height:48px;background-color:#fff;margin-left:-57px;border-bottom:1px solid #e5e5e5;transition:opacity .6s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .6s ease-in-out,top .6s ease-in-out;opacity:0;box-shadow:0 2px 4px -1px rgba(0,9,30,.1)}#hl-viewer .fadein{opacity:1}#hl-viewer .backfill-video-ads{width:300px;height:169px}#hl-viewer .backfill-video-ads h3{font-size:16px;font-size:1.6rem}#hl-viewer .backfill-video-ads h4{font-size:15px;font-size:1.5rem}#hl-viewer .backfill-video-ads .yvp-setting-btn{display:none}#hl-viewer .SidekickTV .list-view-item{display:inline-block;zoom:1;letter-spacing:normal;word-spacing:normal;text-rendering:auto;vertical-align:top}#hl-viewer .SidekickTV .SidekickTVArrow{border-bottom:40px solid transparent;border-left:40px solid}#hl-viewer .SidekickTV .ad-sponsored{color:#959595;font-size:11px;margin-right:0;padding-right:0}#hl-viewer .continue_reading .arraw,#hl-viewer .js-viewer-view-article .arraw{border-left:5px solid transparent;border-right:5px solid transparent;border-top:6px solid #000;vertical-align:middle}#hl-viewer .continue_reading:hover{color:#188FFF}#hl-viewer .continue_reading:hover .arraw{border-top-color:#188FFF}#hl-viewer .Mt-neg-40{margin-top:-40px}#hl-viewer .Mt-neg-30{margin-top:-30px}#hl-viewer .modal-sidekick-following .sidekick.fp{margin-top:-20px}#hl-viewer figure{margin:0}#hl-viewer iframe{border:none}#hl-viewer #viewer-end{height:1px;width:1px;display:block}#hl-viewer #viewer-end:focus{outline:0}.ShareBtns .Share-Btn{padding:5px;border:1px solid #abaeb7}.ShareBtns .Share-Btn:hover{opacity:1;filter:alpha(opacity=100)}.ShareBtns .Tumblr-Btn{color:#35506d}.ShareBtns .Tumblr-Btn:hover{color:#35506d;background-color:#3593d3}.ShareBtns .Facebook-Btn{color:#3b5998}.ShareBtns .Facebook-Btn:hover{color:#3b5998;background-color:#4e91f2}.ShareBtns .Twitter-Btn{color:#00aced}.ShareBtns .Twitter-Btn:hover{color:#00aced;background-color:#94e8ff}.ShareBtns .Mail-Btn{background-position:-1px 161px;color:#0a80e3}.ShareBtns .Mail-Btn:hover{color:#0a80e3;background-color:#94e8ff}.ShareBtns .StickyPholder{display:inline-block}.ShareBtns .Viewer-Close-Btn{background-position:0 185px;padding:5px;margin-left:40px}.ShareBtns .Viewer-Close-Btn.Sticky{margin-left:-19px}#ModalSticker{max-width:1180px}#ModalSticker.Sticky{display:block!important;opacity:1;filter:alpha(opacity=100)}#ModalSticker .content-modal{border-bottom-width:3px;border-color:#000}#ModalSticker .ShareBtns{position:relative;float:right;margin-top:5px}.Modal-Credit{max-width:200px}.video-stage{margin-right:240px}.video-side-stage{width:240px}.Nav-Start{display:none}.First-Item .Nav-Start{display:block}.First-Item .Nav-Next,.First-Item .Nav-Prev{display:none}.slide-nav-arrow{background-color:#009bfb}.slide-nav-arrow:hover{opacity:1;filter:alpha(opacity=100)}.modal-sidekick,.modal-sidekick-following{display:inline-block}.js-sidekick-container{border-top:1px solid #f1f1f5}.modal-tooltip:hover .modal-tooltip\:h_H\(a\){height:auto!important}.modal-tooltip:hover .modal-tooltip\:h_Op\(1\){opacity:1!important}.c-modal-red{color:#ff156f}.lrec-before-loading{width:300px;height:250px;border:1px solid #e0e4e9;-webkit-transition:height 1.5s;-moz-transition:height 1.5s;-ms-transition:height 1.5s;-o-transition:height 1.5s;transition:height 1.5s}.js-header-searchbox{display:none;position:absolute;height:30px;left:730px;top:9px;border-radius:2px}.js-header-searchbox .srch-input{width:268px;height:100%;line-height:inherit;vertical-align:top;outline:0;box-shadow:none;border:none;border:1px solid #b0b0b0;border-radius:2px 0 0 2px;padding:0 10px}.js-header-searchbox .srch-input:focus{border-color:#0179ff}.js-header-searchbox button{width:32px;height:100%;background-color:#0179ff;font-size:16px;font-weight:700;color:#fff;border-radius:0 2px 2px 0;margin-left:-4px}.Z-6{z-index:6}.Z-7{z-index:7}.Z-8{z-index:8}.Z-9{z-index:9}.fixZindex{z-index:5!important}.viewer-right .js-slider-item.multirightrail.first{margin-top:25px!important}.viewer-right .Viewer-Close-Btn{border-radius:20px!important;font-size:17px!important;height:37px!important;margin-left:-18px!important;top:87px!important;width:37px!important}.viewer-right .viewer-wrapper{border-radius:6px;margin:1px!important;padding:0 0 30px 40px!important}.viewer-right.stream-sparse .viewer-wrapper{top:-12px}@media screen and (min-width:1100px){.js-header-searchbox{display:inline-block}}.Pt-35{padding-top:35px}.Pt-7{padding-top:7px}.JsEnabled .ImageLoader-Delayed{background:#f5f5f5}</style>
 

<style>#applet_p_63794 .Bd-0{border:0}#applet_p_63794 .Bd-1{border-width:1px}#applet_p_63794 .Bdx-1{border-right-width:1px;border-left-width:1px}#applet_p_63794 .Bdy-1{border-top-width:1px;border-bottom-width:1px}#applet_p_63794 .Bd-t{border-top-width:1px}#applet_p_63794 .Bd-end{border-right-width:1px}#applet_p_63794 .Bd-b{border-bottom-width:1px}#applet_p_63794 .Bd-start{border-left-width:1px}#applet_p_63794 .Bdt-0{border-top:0}#applet_p_63794 .Bdend-0{border-right:0}#applet_p_63794 .Bdb-0{border-bottom:0}#applet_p_63794 .Bdstart-0{border-left:0}#applet_p_63794 .Bdrs-0{border-radius:0}#applet_p_63794 .Bdtrrs-0{border-top-right-radius:0}#applet_p_63794 .Bdtlrs-0{border-top-left-radius:0}#applet_p_63794 .Bdbrrs-0{border-bottom-right-radius:0}#applet_p_63794 .Bdblrs-0{border-bottom-left-radius:0}#applet_p_63794 .Bdrs-100{border-radius:100px}#applet_p_63794 .Bdtrrs-100{border-top-right-radius:100px}#applet_p_63794 .Bdtlrs-100{border-top-left-radius:100px}#applet_p_63794 .Bdbrrs-100{border-bottom-right-radius:100px}#applet_p_63794 .Bdblrs-100{border-bottom-left-radius:100px}#applet_p_63794 .Bdrs-300{border-radius:300px}#applet_p_63794 .Bdtrrs-300{border-top-right-radius:300px}#applet_p_63794 .Bdtlrs-300{border-top-left-radius:300px}#applet_p_63794 .Bdbrrs-300{border-bottom-right-radius:300px}#applet_p_63794 .Bdblrs-300{border-bottom-left-radius:300px}#applet_p_63794 .Bdrs{border-radius:3px}#applet_p_63794 .Bdtrrs{border-top-right-radius:3px}#applet_p_63794 .Bdtlrs{border-top-left-radius:3px}#applet_p_63794 .Bdbrrs{border-bottom-right-radius:3px}#applet_p_63794 .Bdblrs{border-bottom-left-radius:3px}#applet_p_63794 .Ff{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-a{font-family:Georgia,"Times New Roman",serif}#applet_p_63794 .Ff-b{font-family:Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-c{font-family:"Monotype Corsiva","Comic Sans MS",cursive}#applet_p_63794 .Ff-d{font-family:Capitals,Impact,fantasy}#applet_p_63794 .Ff-e{font-family:Monaco,"Courier New",monospace}#applet_p_63794 .Fz-0{font-size:0}#applet_p_63794 .Fz-3xs{font-size:7px}#applet_p_63794 .Fz-2xs{font-size:9px}#applet_p_63794 .Fz-xs{font-size:11px;}#applet_p_63794 .Fz-s{font-size:13px;}#applet_p_63794 .Fz-m{font-size:15px;}#applet_p_63794 .Bgc-t{background-color:transparent}#applet_p_63794 .Bgi-n{background-image:none}#applet_p_63794 .Bg-n{background:0 0}#applet_p_63794 .Bgcp-bb{background-clip:border-box}#applet_p_63794 .Bgcp-pb{background-clip:padding-box}#applet_p_63794 .Bgcp-cb{background-clip:content-box}#applet_p_63794 .Bgo-pb{background-origin:padding-box}#applet_p_63794 .Bgo-bb{background-origin:border-box}#applet_p_63794 .Bgo-cb{background-origin:content-box}#applet_p_63794 .Bgz-a{background-size:auto}#applet_p_63794 .Bgz-ct{background-size:contain}#applet_p_63794 .Bgz-cv{background-size:cover;background-position:50% 15%}#applet_p_63794 .Bdcl-c{border-collapse:collapse}#applet_p_63794 .Bdcl-s{border-collapse:separate}#applet_p_63794 .Bxz-cb{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}#applet_p_63794 .Bxz-bb{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#applet_p_63794 .Bxsh-n{box-shadow:none}#applet_p_63794 .Cl-n{clear:none}#applet_p_63794 .Cl-b{clear:both}#applet_p_63794 .Cl-start{clear:left}#applet_p_63794 .Cl-end{clear:right}#applet_p_63794 .Cur-d{cursor:default}#applet_p_63794 .Cur-he{cursor:help}#applet_p_63794 .Cur-m{cursor:move}#applet_p_63794 .Cur-na{cursor:not-allowed}#applet_p_63794 .Cur-nsr{cursor:ns-resize}#applet_p_63794 .Cur-p{cursor:pointer}#applet_p_63794 .Cur-cr{cursor:col-resize}#applet_p_63794 .Cur-w{cursor:wait}#applet_p_63794 .Cur-a{cursor:auto!important}#applet_p_63794 .D-n{display:none}#applet_p_63794 .D-b{display:block}#applet_p_63794 .D-i{display:inline}#applet_p_63794 .D-ib{display:inline-block;*display:inline;zoom:1}#applet_p_63794 .D-tb{display:table}#applet_p_63794 .D-tbr{display:table-row}#applet_p_63794 .D-tbc{display:table-cell}#applet_p_63794 .D-li{display:list-item}#applet_p_63794 .D-ri{display:run-in}#applet_p_63794 .D-cp{display:compact}#applet_p_63794 .D-itb{display:inline-table}#applet_p_63794 .D-tbcl{display:table-column}#applet_p_63794 .D-tbclg{display:table-column-group}#applet_p_63794 .D-tbhg{display:table-header-group}#applet_p_63794 .D-tbfg{display:table-footer-group}#applet_p_63794 .D-tbrg{display:table-row-group}#applet_p_63794 .Fl-n{float:none}#applet_p_63794 .Fl-start{float:left;_display:inline}#applet_p_63794 .Fl-end{float:right;_display:inline}#applet_p_63794 .Fw-n{font-weight:400}#applet_p_63794 .Fw-b{font-weight:700;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}#applet_p_63794 .Fw-100{font-weight:100;*font-weight:normal}#applet_p_63794 .Fw-200{font-weight:200;*font-weight:normal}#applet_p_63794 .Fw-400{font-weight:400;*font-weight:normal}#applet_p_63794 .Fw-br{font-weight:bolder}#applet_p_63794 .Fw-lr{font-weight:lighter}#applet_p_63794 .Fs-n{font-style:normal}#applet_p_63794 .Fs-i{font-style:italic}#applet_p_63794 .Fv-sc{font-variant:small-caps}#applet_p_63794 .Fv-n{font-variant:normal}#applet_p_63794 .H-0{height:0}#applet_p_63794 .H-50{height:50%}#applet_p_63794 .H-100{height:100%}#applet_p_63794 .H-a{height:auto}#applet_p_63794 .H-n,#applet_p_63794 .h-n{-webkit-hyphens:none;-moz-hyphens:none;-ms-hyphens:none;hyphens:none}#applet_p_63794 .List-n{list-style-type:none}#applet_p_63794 .List-d{list-style-type:disc}#applet_p_63794 .List-c{list-style-type:circle}#applet_p_63794 .List-s{list-style-type:square}#applet_p_63794 .List-dc{list-style-type:decimal}#applet_p_63794 .List-dclz{list-style-type:decimal-leading-zero}#applet_p_63794 .List-lr{list-style-type:lower-roman}#applet_p_63794 .List-ur{list-style-type:upper-roman}#applet_p_63794 .Lisi-n{list-style-image:none}#applet_p_63794 .Lh-0{line-height:0}#applet_p_63794 .Lh-01{line-height:.1}#applet_p_63794 .Lh-02{line-height:.2}#applet_p_63794 .Lh-03{line-height:.3}#applet_p_63794 .Lh-04{line-height:.4}#applet_p_63794 .Lh-05{line-height:.5}#applet_p_63794 .Lh-06{line-height:.6}#applet_p_63794 .Lh-07{line-height:.7}#applet_p_63794 .Lh-08{line-height:.8}#applet_p_63794 .Lh-09{line-height:.9}#applet_p_63794 .Lh-1{line-height:1}#applet_p_63794 .Lh-11{line-height:1.1}#applet_p_63794 .Lh-12{line-height:1.2}#applet_p_63794 .Lh-125{line-height:1.25}#applet_p_63794 .Lh-13{line-height:1.3}#applet_p_63794 .Lh-14{line-height:1.4}#applet_p_63794 .Lh-15{line-height:1.5}#applet_p_63794 .Lh-16{line-height:1.6}#applet_p_63794 .Lh-17{line-height:1.7}#applet_p_63794 .Lh-18{line-height:1.8}#applet_p_63794 .Lh-19{line-height:1.9}#applet_p_63794 .Lh-2{line-height:2}#applet_p_63794 .Lh-21{line-height:2.1}#applet_p_63794 .Lh-22{line-height:2.2}#applet_p_63794 .Lh-23{line-height:2.3}#applet_p_63794 .Lh-24{line-height:2.4}#applet_p_63794 .Lh-25{line-height:2.5}#applet_p_63794 .Lh-3{line-height:3}#applet_p_63794 .Lh-reset{line-height:normal}#applet_p_63794 .M-0{margin:0}#applet_p_63794 .M-2{margin:2px}#applet_p_63794 .M-4{margin:4px}#applet_p_63794 .M-6{margin:6px}#applet_p_63794 .M-8{margin:8px}#applet_p_63794 .M-10{margin:10px}#applet_p_63794 .M-12{margin:12px}#applet_p_63794 .M-14{margin:14px}#applet_p_63794 .M-16{margin:16px}#applet_p_63794 .M-18{margin:18px}#applet_p_63794 .M-20{margin:20px}#applet_p_63794 .Mx-1{margin-right:1px;margin-left:1px}#applet_p_63794 .Mx-2{margin-right:2px;margin-left:2px}#applet_p_63794 .Mx-4{margin-right:4px;margin-left:4px}#applet_p_63794 .Mx-6{margin-right:6px;margin-left:6px}#applet_p_63794 .Mx-8{margin-right:8px;margin-left:8px}#applet_p_63794 .Mx-10{margin-right:10px;margin-left:10px}#applet_p_63794 .Mx-12{margin-right:12px;margin-left:12px}#applet_p_63794 .Mx-14{margin-right:14px;margin-left:14px}#applet_p_63794 .Mx-16{margin-right:16px;margin-left:16px}#applet_p_63794 .Mx-18{margin-right:18px;margin-left:18px}#applet_p_63794 .Mx-20{margin-right:20px;margin-left:20px}#applet_p_63794 .My-1{margin-top:1px;margin-bottom:1px}#applet_p_63794 .My-2{margin-top:2px;margin-bottom:2px}#applet_p_63794 .My-4{margin-top:4px;margin-bottom:4px}#applet_p_63794 .My-6{margin-top:6px;margin-bottom:6px}#applet_p_63794 .My-8{margin-top:8px;margin-bottom:8px}#applet_p_63794 .My-10{margin-top:10px;margin-bottom:10px}#applet_p_63794 .My-12{margin-top:12px;margin-bottom:12px}#applet_p_63794 .My-14{margin-top:14px;margin-bottom:14px}#applet_p_63794 .My-16{margin-top:16px;margin-bottom:16px}#applet_p_63794 .My-18{margin-top:18px;margin-bottom:18px}#applet_p_63794 .My-20{margin-top:20px;margin-bottom:20px}#applet_p_63794 .Mt-1{margin-top:1px}#applet_p_63794 .Mb-1{margin-bottom:1px}#applet_p_63794 .Mstart-1{margin-left:1px}#applet_p_63794 .Mend-1{margin-right:1px}#applet_p_63794 .Mt-2{margin-top:2px}#applet_p_63794 .Mb-2{margin-bottom:2px}#applet_p_63794 .Mstart-2{margin-left:2px}#applet_p_63794 .Mend-2{margin-right:2px}#applet_p_63794 .Mt-4{margin-top:4px}#applet_p_63794 .Mb-4{margin-bottom:4px}#applet_p_63794 .Mstart-4{margin-left:4px}#applet_p_63794 .Mend-4{margin-right:4px}#applet_p_63794 .Mt-6{margin-top:6px}#applet_p_63794 .Mb-6{margin-bottom:6px}#applet_p_63794 .Mstart-6{margin-left:6px}#applet_p_63794 .Mend-6{margin-right:6px}#applet_p_63794 .Mt-8{margin-top:8px}#applet_p_63794 .Mb-8{margin-bottom:8px}#applet_p_63794 .Mstart-8{margin-left:8px}#applet_p_63794 .Mend-8{margin-right:8px}#applet_p_63794 .Mt-10{margin-top:10px}#applet_p_63794 .Mb-10{margin-bottom:10px}#applet_p_63794 .Mstart-10{margin-left:10px}#applet_p_63794 .Mend-10{margin-right:10px}#applet_p_63794 .Mt-12{margin-top:12px}#applet_p_63794 .Mb-12{margin-bottom:12px}#applet_p_63794 .Mstart-12{margin-left:12px}#applet_p_63794 .Mend-12{margin-right:12px}#applet_p_63794 .Mt-14{margin-top:14px}#applet_p_63794 .Mb-14{margin-bottom:14px}#applet_p_63794 .Mstart-14{margin-left:14px}#applet_p_63794 .Mend-14{margin-right:14px}#applet_p_63794 .Mt-16{margin-top:16px}#applet_p_63794 .Mb-16{margin-bottom:16px}#applet_p_63794 .Mstart-16{margin-left:16px}#applet_p_63794 .Mend-16{margin-right:16px}#applet_p_63794 .Mt-18{margin-top:18px}#applet_p_63794 .Mb-18{margin-bottom:18px}#applet_p_63794 .Mstart-18{margin-left:18px}#applet_p_63794 .Mend-18{margin-right:18px}#applet_p_63794 .Mt-20{margin-top:20px}#applet_p_63794 .Mb-20{margin-bottom:20px}#applet_p_63794 .Mstart-20{margin-left:20px}#applet_p_63794 .Mend-20{margin-right:20px}#applet_p_63794 .Mt-30{margin-top:30px}#applet_p_63794 .Mb-30{margin-bottom:30px}#applet_p_63794 .Mstart-30{margin-left:30px}#applet_p_63794 .Mend-30{margin-right:30px}#applet_p_63794 .Mt-40{margin-top:40px}#applet_p_63794 .Mb-40{margin-bottom:40px}#applet_p_63794 .Mstart-40{margin-left:40px}#applet_p_63794 .Mend-40{margin-right:40px}#applet_p_63794 .Mt-50{margin-top:50px}#applet_p_63794 .Mb-50{margin-bottom:50px}#applet_p_63794 .Mstart-50{margin-left:50px}#applet_p_63794 .Mend-50{margin-right:50px}#applet_p_63794 .Mt-60{margin-top:60px}#applet_p_63794 .Mb-60{margin-bottom:60px}#applet_p_63794 .Mstart-60{margin-left:60px}#applet_p_63794 .Mend-60{margin-right:60px}#applet_p_63794 .Mt-70{margin-top:70px}#applet_p_63794 .Mb-70{margin-bottom:70px}#applet_p_63794 .Mstart-70{margin-left:70px}#applet_p_63794 .Mend-70{margin-right:70px}#applet_p_63794 .Mt-neg-1{margin-top:-1px}#applet_p_63794 .Mb-neg-1{margin-bottom:-1px}#applet_p_63794 .Mstart-neg-1{margin-left:-1px}#applet_p_63794 .Mend-neg-1{margin-right:-1px}#applet_p_63794 .Mt-neg-4{margin-top:-4px}#applet_p_63794 .Mb-neg-4{margin-bottom:-4px}#applet_p_63794 .Mstart-neg-4{margin-left:-4px}#applet_p_63794 .Mend-neg-4{margin-right:-4px}#applet_p_63794 .Mt-neg-6{margin-top:-6px}#applet_p_63794 .Mb-neg-6{margin-bottom:-6px}#applet_p_63794 .Mstart-neg-6{margin-left:-6px}#applet_p_63794 .Mend-neg-6{margin-right:-6px}#applet_p_63794 .Mt-neg-8{margin-top:-8px}#applet_p_63794 .Mb-neg-8{margin-bottom:-8px}#applet_p_63794 .Mstart-neg-8{margin-left:-8px}#applet_p_63794 .Mend-neg-8{margin-right:-8px}#applet_p_63794 .Mt-neg-10{margin-top:-10px}#applet_p_63794 .Mb-neg-10{margin-bottom:-10px}#applet_p_63794 .Mstart-neg-10{margin-left:-10px}#applet_p_63794 .Mend-neg-10{margin-right:-10px}#applet_p_63794 .Mt-neg-12{margin-top:-12px}#applet_p_63794 .Mb-neg-12{margin-bottom:-12px}#applet_p_63794 .Mstart-neg-12{margin-left:-12px}#applet_p_63794 .Mend-neg-12{margin-right:-12px}#applet_p_63794 .Mt-neg-14{margin-top:-14px}#applet_p_63794 .Mb-neg-14{margin-bottom:-14px}#applet_p_63794 .Mstart-neg-14{margin-left:-14px}#applet_p_63794 .Mend-neg-14{margin-right:-14px}#applet_p_63794 .Mt-neg-16{margin-top:-16px}#applet_p_63794 .Mb-neg-16{margin-bottom:-16px}#applet_p_63794 .Mstart-neg-16{margin-left:-16px}#applet_p_63794 .Mend-neg-16{margin-right:-16px}#applet_p_63794 .Mt-neg-18{margin-top:-18px}#applet_p_63794 .Mb-neg-18{margin-bottom:-18px}#applet_p_63794 .Mstart-neg-18{margin-left:-18px}#applet_p_63794 .Mend-neg-18{margin-right:-18px}#applet_p_63794 .Mt-neg-20{margin-top:-20px}#applet_p_63794 .Mb-neg-20{margin-bottom:-20px}#applet_p_63794 .Mstart-neg-20{margin-left:-20px}#applet_p_63794 .Mend-neg-20{margin-right:-20px}#applet_p_63794 .Mstart-50\%{margin-left:50%}#applet_p_63794 .M-a{margin:auto}#applet_p_63794 .Mx-a{margin-right:auto;margin-left:auto}#applet_p_63794 .\!Mx-a{margin-right:auto!important;margin-left:auto!important}#applet_p_63794 .Mstart-a{margin-left:auto}#applet_p_63794 .Mend-a{margin-right:auto}#applet_p_63794 .Mt-0,#applet_p_63794 .My-0{margin-top:0}#applet_p_63794 .Mb-0,#applet_p_63794 .My-0{margin-bottom:0}#applet_p_63794 .Mstart-0,#applet_p_63794 .Mx-0{margin-left:0}#applet_p_63794 .Mend-0,#applet_p_63794 .Mx-0{margin-right:0}#applet_p_63794 .Miw-0{min-width:0}#applet_p_63794 .Miw-10{min-width:10%}#applet_p_63794 .Miw-15{min-width:15%}#applet_p_63794 .Miw-20{min-width:20%}#applet_p_63794 .Miw-25{min-width:25%}#applet_p_63794 .Miw-30{min-width:30%}#applet_p_63794 .Miw-35{min-width:35%}#applet_p_63794 .Miw-40{min-width:40%}#applet_p_63794 .Miw-45{min-width:45%}#applet_p_63794 .Miw-50{min-width:50%}#applet_p_63794 .Miw-60{min-width:60%}#applet_p_63794 .Miw-70{min-width:70%}#applet_p_63794 .Miw-80{min-width:80%}#applet_p_63794 .Miw-90{min-width:90%}#applet_p_63794 .Miw-100{min-width:100%}#applet_p_63794 .Maw-n{max-width:none}#applet_p_63794 .Maw-10{max-width:10%}#applet_p_63794 .Maw-15{max-width:15%}#applet_p_63794 .Maw-20{max-width:20%}#applet_p_63794 .Maw-25{max-width:25%}#applet_p_63794 .Maw-30{max-width:30%}#applet_p_63794 .Maw-35{max-width:35%}#applet_p_63794 .Maw-40{max-width:40%}#applet_p_63794 .Maw-45{max-width:45%}#applet_p_63794 .Maw-50{max-width:50%}#applet_p_63794 .Maw-60{max-width:60%}#applet_p_63794 .Maw-70{max-width:70%}#applet_p_63794 .Maw-80{max-width:80%}#applet_p_63794 .Maw-90{max-width:90%}#applet_p_63794 .Maw-99{max-width:99%}#applet_p_63794 .Maw-100{max-width:100%}#applet_p_63794 .Mih-0{min-height:0}#applet_p_63794 .Mih-50{min-height:50%;_height:50%}#applet_p_63794 .Mih-60{min-height:60%;_height:60%}#applet_p_63794 .Mih-100{min-height:100%;_height:100%}#applet_p_63794 .Mah-n{max-height:none}#applet_p_63794 .Mah-0{max-height:0}#applet_p_63794 .Mah-50{max-height:50%}#applet_p_63794 .Mah-60{max-height:60%}#applet_p_63794 .Mah-100{max-height:100%}#applet_p_63794 .O-0{outline:0}#applet_p_63794 .T-0{top:0}#applet_p_63794 .B-0{bottom:0}#applet_p_63794 .Start-0{left:0}#applet_p_63794 .End-0{right:0}#applet_p_63794 .T-10{top:10%}#applet_p_63794 .B-10{bottom:10%}#applet_p_63794 .Start-10{left:10%}#applet_p_63794 .End-10{right:10%}#applet_p_63794 .T-25{top:25%}#applet_p_63794 .B-25{bottom:25%}#applet_p_63794 .Start-25{left:25%}#applet_p_63794 .End-25{right:25%}#applet_p_63794 .T-50{top:50%}#applet_p_63794 .B-50{bottom:50%}#applet_p_63794 .Start-50{left:50%}#applet_p_63794 .End-50{right:50%}#applet_p_63794 .T-75{top:75%}#applet_p_63794 .B-75{bottom:75%}#applet_p_63794 .Start-75{left:75%}#applet_p_63794 .End-75{right:75%}#applet_p_63794 .T-100{top:100%}#applet_p_63794 .B-100{bottom:100%}#applet_p_63794 .Start-100{left:100%}#applet_p_63794 .End-100{right:100%}#applet_p_63794 .T-a{top:auto!important}#applet_p_63794 .B-a{bottom:auto!important}#applet_p_63794 .Start-a{left:auto!important}#applet_p_63794 .End-a{right:auto!important}#applet_p_63794 .Op-0{opacity:0;filter:alpha(opacity=0)}#applet_p_63794 .Op-33{opacity:.33;filter:alpha(opacity=33)}#applet_p_63794 .Op-50{opacity:.5;filter:alpha(opacity=50)}#applet_p_63794 .Op-66{opacity:.66;filter:alpha(opacity=66)}#applet_p_63794 .Op-100{opacity:1;filter:alpha(opacity=100)}#applet_p_63794 .Ov-h{overflow:hidden;zoom:1}#applet_p_63794 .Ov-v{overflow:visible}#applet_p_63794 .Ov-s{overflow:scroll}#applet_p_63794 .Ov-a{overflow:auto}#applet_p_63794 .Ovs-t{-webkit-overflow-scrolling:touch}#applet_p_63794 .Ovx-v{overflow-x:visible}#applet_p_63794 .Ovx-h{overflow-x:hidden}#applet_p_63794 .Ovx-s{overflow-x:scroll}#applet_p_63794 .Ovx-a{overflow-x:auto}#applet_p_63794 .Ovy-v{overflow-y:visible}#applet_p_63794 .Ovy-h{overflow-y:hidden}#applet_p_63794 .Ovy-s{overflow-y:scroll}#applet_p_63794 .Ovy-a{overflow-y:auto}#applet_p_63794 .P-0{padding:0}#applet_p_63794 .P-1{padding:1px}#applet_p_63794 .P-2{padding:2px}#applet_p_63794 .P-4{padding:4px}#applet_p_63794 .P-6{padding:6px}#applet_p_63794 .P-8{padding:8px}#applet_p_63794 .P-10{padding:10px}#applet_p_63794 .P-12{padding:12px}#applet_p_63794 .P-14{padding:14px}#applet_p_63794 .P-16{padding:16px}#applet_p_63794 .P-18{padding:18px}#applet_p_63794 .P-20{padding:20px}#applet_p_63794 .P-30{padding:30px}#applet_p_63794 .Px-1{padding-right:1px;padding-left:1px}#applet_p_63794 .Px-2{padding-right:2px;padding-left:2px}#applet_p_63794 .Px-4{padding-right:4px;padding-left:4px}#applet_p_63794 .Px-6{padding-right:6px;padding-left:6px}#applet_p_63794 .Px-8{padding-right:8px;padding-left:8px}#applet_p_63794 .Px-10{padding-right:10px;padding-left:10px}#applet_p_63794 .Px-12{padding-right:12px;padding-left:12px}#applet_p_63794 .Px-14{padding-right:14px;padding-left:14px}#applet_p_63794 .Px-16{padding-right:16px;padding-left:16px}#applet_p_63794 .Px-18{padding-right:18px;padding-left:18px}#applet_p_63794 .Px-20{padding-right:20px;padding-left:20px}#applet_p_63794 .Px-30{padding-right:30px;padding-left:30px}#applet_p_63794 .Py-1{padding-top:1px;padding-bottom:1px}#applet_p_63794 .Py-2{padding-top:2px;padding-bottom:2px}#applet_p_63794 .Py-4{padding-top:4px;padding-bottom:4px}#applet_p_63794 .Py-6{padding-top:6px;padding-bottom:6px}#applet_p_63794 .Py-8{padding-top:8px;padding-bottom:8px}#applet_p_63794 .Py-10{padding-top:10px;padding-bottom:10px}#applet_p_63794 .Py-12{padding-top:12px;padding-bottom:12px}#applet_p_63794 .Py-14{padding-top:14px;padding-bottom:14px}#applet_p_63794 .Py-16{padding-top:16px;padding-bottom:16px}#applet_p_63794 .Py-18{padding-top:18px;padding-bottom:18px}#applet_p_63794 .Py-20{padding-top:20px;padding-bottom:20px}#applet_p_63794 .Py-30{padding-top:30px;padding-bottom:30px}#applet_p_63794 .Pt-1{padding-top:1px}#applet_p_63794 .Pb-1{padding-bottom:1px}#applet_p_63794 .Pstart-1{padding-left:1px}#applet_p_63794 .Pend-1{padding-right:1px}#applet_p_63794 .Pt-2{padding-top:2px}#applet_p_63794 .Pb-2{padding-bottom:2px}#applet_p_63794 .Pstart-2{padding-left:2px}#applet_p_63794 .Pend-2{padding-right:2px}#applet_p_63794 .Pt-4{padding-top:4px}#applet_p_63794 .Pb-4{padding-bottom:4px}#applet_p_63794 .Pstart-4{padding-left:4px}#applet_p_63794 .Pend-4{padding-right:4px}#applet_p_63794 .Pt-6{padding-top:6px}#applet_p_63794 .Pb-6{padding-bottom:6px}#applet_p_63794 .Pstart-6{padding-left:6px}#applet_p_63794 .Pend-6{padding-right:6px}#applet_p_63794 .Pt-8{padding-top:8px}#applet_p_63794 .Pb-8{padding-bottom:8px}#applet_p_63794 .Pstart-8{padding-left:8px}#applet_p_63794 .Pend-8{padding-right:8px}#applet_p_63794 .Pt-10{padding-top:10px}#applet_p_63794 .Pb-10{padding-bottom:10px}#applet_p_63794 .Pstart-10{padding-left:10px}#applet_p_63794 .Pend-10{padding-right:10px}#applet_p_63794 .Pt-12{padding-top:12px}#applet_p_63794 .Pb-12{padding-bottom:12px}#applet_p_63794 .Pstart-12{padding-left:12px}#applet_p_63794 .Pend-12{padding-right:12px}#applet_p_63794 .Pt-14{padding-top:14px}#applet_p_63794 .Pb-14{padding-bottom:14px}#applet_p_63794 .Pstart-14{padding-left:14px}#applet_p_63794 .Pend-14{padding-right:14px}#applet_p_63794 .Pt-16{padding-top:16px}#applet_p_63794 .Pb-16{padding-bottom:16px}#applet_p_63794 .Pstart-16{padding-left:16px}#applet_p_63794 .Pend-16{padding-right:16px}#applet_p_63794 .Pt-18{padding-top:18px}#applet_p_63794 .Pb-18{padding-bottom:18px}#applet_p_63794 .Pstart-18{padding-left:18px}#applet_p_63794 .Pend-18{padding-right:18px}#applet_p_63794 .Pt-20{padding-top:20px}#applet_p_63794 .Pend-20{padding-right:20px}#applet_p_63794 .Pb-20{padding-bottom:20px}#applet_p_63794 .Pstart-20{padding-left:20px}#applet_p_63794 .Pt-30{padding-top:30px}#applet_p_63794 .Pend-30{padding-right:30px}#applet_p_63794 .Pb-30{padding-bottom:30px}#applet_p_63794 .Pstart-30{padding-left:30px}#applet_p_63794 .Pt-0,#applet_p_63794 .Py-0{padding-top:0}#applet_p_63794 .Pb-0,#applet_p_63794 .Py-0{padding-bottom:0}#applet_p_63794 .Pstart-0,#applet_p_63794 .Px-0{padding-left:0}#applet_p_63794 .Pend-0,#applet_p_63794 .Px-0{padding-right:0}#applet_p_63794 .Pe-n{pointer-events:none}#applet_p_63794 .Pe-a{pointer-events:auto}#applet_p_63794 .Pos-s{position:static}#applet_p_63794 .Pos-a{position:absolute}#applet_p_63794 .Pos-r{position:relative}#applet_p_63794 .Pos-f{position:fixed}#applet_p_63794 .Ws-n::-webkit-scrollbar{-webkit-appearance:none}#applet_p_63794 .Tbl-f{table-layout:fixed}#applet_p_63794 .Tbl-a{table-layout:auto}#applet_p_63794 .Ta-c{text-align:center}#applet_p_63794 .Ta-j{text-align:justify}#applet_p_63794 .Ta-start{text-align:left}#applet_p_63794 .Ta-end{text-align:right}#applet_p_63794 .Td-n{text-decoration:none!important}#applet_p_63794 .Td-u,#applet_p_63794 .Td-u\:h:hover{text-decoration:underline}#applet_p_63794 .Tr-a{-webkit-text-rendering:auto;-moz-text-rendering:auto;-ms-text-rendering:auto;-o-text-rendering:auto;text-rendering:auto}#applet_p_63794 .Tr-os{-webkit-text-rendering:optimizeSpeed;-moz-text-rendering:optimizeSpeed;-ms-text-rendering:optimizeSpeed;-o-text-rendering:optimizeSpeed;text-rendering:optimizeSpeed}#applet_p_63794 .Tr-ol{-webkit-text-rendering:optimizeLegibility;-moz-text-rendering:optimizeLegibility;-ms-text-rendering:optimizeLegibility;-o-text-rendering:optimizeLegibility;text-rendering:optimizeLegibility}#applet_p_63794 .Tr-gp{-webkit-text-rendering:geometricPrecision;-moz-text-rendering:geometricPrecision;-ms-text-rendering:geometricPrecision;-o-text-rendering:geometricPrecision;text-rendering:geometricPrecision}#applet_p_63794 .Tr-i{-webkit-text-rendering:inherit;-moz-text-rendering:inherit;-ms-text-rendering:inherit;-o-text-rendering:inherit;text-rendering:inherit}#applet_p_63794 .Tt-n{text-transform:none}#applet_p_63794 .Tt-u{text-transform:uppercase}#applet_p_63794 .Tt-c{text-transform:capitalize}#applet_p_63794 .Tt-l{text-transform:lowercase}#applet_p_63794 .Tsh{text-shadow:0 1px 1px #000}#applet_p_63794 .Tsh-n{text-shadow:none}#applet_p_63794 .Us-n{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}#applet_p_63794 .Va-sup{vertical-align:super}#applet_p_63794 .Va-t{vertical-align:top}#applet_p_63794 .Va-tt{vertical-align:text-top}#applet_p_63794 .Va-m{vertical-align:middle}#applet_p_63794 .Va-bl{vertical-align:baseline}#applet_p_63794 .Va-b{vertical-align:bottom}#applet_p_63794 .Va-tb{vertical-align:text-bottom}#applet_p_63794 .Va-sub{vertical-align:sub}#applet_p_63794 .V-v{visibility:visible}#applet_p_63794 .V-h{visibility:hidden}#applet_p_63794 .V-c{visibility:collapse}#applet_p_63794 .Whs-nw{white-space:nowrap}#applet_p_63794 .Whs-n{white-space:normal}#applet_p_63794 .W-a{width:auto}#applet_p_63794 .W-0{width:0}#applet_p_63794 .W-1{width:1%}#applet_p_63794 .W-10{width:10%;*width:9.6%}#applet_p_63794 .W-15{width:15%;*width:14.8%}#applet_p_63794 .W-20{width:20%;*width:19.5%}#applet_p_63794 .W-25{width:25%;*width:24.5%}#applet_p_63794 .W-30{width:30%;*width:29.6%}#applet_p_63794 .W-33{width:33.33%;*width:33%}#applet_p_63794 .W-35{width:35%;*width:34.9%}#applet_p_63794 .W-40{width:40%;*width:39.5%}#applet_p_63794 .W-45{width:45%;*width:44.9%}#applet_p_63794 .W-50{width:50%;*width:49.5%}#applet_p_63794 .W-55{width:55%;*width:54.8%}#applet_p_63794 .W-60{width:60%}#applet_p_63794 .W-66{width:66.66%}#applet_p_63794 .W-70{width:70%}#applet_p_63794 .W-75{width:75%}#applet_p_63794 .W-80{width:80%}#applet_p_63794 .W-90{width:90%}#applet_p_63794 .W-100{width:100%}#applet_p_63794 .Wpx-1{width:1px}#applet_p_63794 .Wpx-2{width:2px}#applet_p_63794 .Wpx-4{width:4px}#applet_p_63794 .Wpx-6{width:6px}#applet_p_63794 .Wpx-8{width:8px}#applet_p_63794 .Wpx-10{width:10px}#applet_p_63794 .Wpx-12{width:12px}#applet_p_63794 .Wpx-14{width:14px}#applet_p_63794 .Wpx-16{width:16px}#applet_p_63794 .Wpx-18{width:18px}#applet_p_63794 .Wpx-20{width:20px}#applet_p_63794 .Wpx-24{width:24px}#applet_p_63794 .Wpx-26{width:26px}#applet_p_63794 .Wpx-28{width:28px}#applet_p_63794 .Wpx-30{width:30px}#applet_p_63794 .Wpx-32{width:32px}#applet_p_63794 .Wob-ba{word-break:break-all}#applet_p_63794 .Wob-n{word-break:normal!important}#applet_p_63794 .Wow-bw{word-wrap:break-word}#applet_p_63794 .Wow-n{word-wrap:normal!important}#applet_p_63794 .Z-0{z-index:0}#applet_p_63794 .Z-1{z-index:1}#applet_p_63794 .Z-3{z-index:3}#applet_p_63794 .Z-5{z-index:5}#applet_p_63794 .Z-7{z-index:7}#applet_p_63794 .Z-10{z-index:10}#applet_p_63794 .Z-a{z-index:auto!important}#applet_p_63794 .Zoom-1{zoom:1}

#applet_p_63794 .StencilRoot input {
    font-size: 13px;    
}

#applet_p_63794 .StencilRoot button {
    font-size: 14px;
    line-height: normal;
}
</style>

    
    </head>
    <body class="my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr    stream-dense" dir="ltr">
        
        
        
                    <div id="darla-assets-top">
            <script type='text/javascript' src='/sy/rq/darla/2-9-4/js/g-r-min.js'></script>
            <script>
    var resourceTimingAssets = null;
    if (window.performance && window.performance.mark && window.performance.getEntriesByName) {
        resourceTimingAssets = {'darlaJsLoaded' : 'https://www.yahoo.com/sy/rq/darla/2-9-4/js/g-r-min.js'};
        window.performance.mark('darlaJsLoaded');
    }
</script>
            </div>
                <script type="text/javascript">
        var rapidPageConfig = {
            rapidEarlyConfig : {},
            rapidConfig: {"compr_type":"deflate","tracked_mods":[],"spaceid":2023538075,"ywa":{"project_id":"10001806365479","host":"y.analytics.yahoo.com"},"click_timeout":300,"track_right_click":true,"apv":true,"apv_time":0,"yql_host":"","test_id":"201","client_only":0,"pageview_on_init":true,"perf_navigationtime":2,"addmodules_timeout":500,"keys":{"_rid":"3p231htbc1es4","mrkt":"us","pt":1,"ver":"megastrm","uh_vw":0,"colo":"slw83.fp.ne1.yahoo.com","navtype":"server","nob":1},"viewability":true},
            rapidSingleInstance: 0,
            ywaCF: {"mrkt":12,"pt":13,"test":14,"sec":18,"slk":19,"cpos":21,"pkgt":22,"ct":23,"bpos":24,"cat":25,"dcl":26,"f":40,"prfm1":41,"site":42,"prfm2":43,"prfm3":44,"prfm4":45,"pct":48,"ver":49,"ft":51,"sca":53,"elm":56,"elmt":57,"ad":58,"tsrc":69,"err":72,"bkpt":73,"bw":98,"bh":99,"olncust":100,"noct":101,"uh_vw":102,"rspns":107,"grpt":109,"itc":111,"enr":112,"tar":113,"sp":115,"pos":117,"cposx":118,"cw":119,"ch":120,"t1":121,"t2":122,"t3":123,"t4":124,"t5":125,"refcnt":135,"tar_qa":145,"navtype":146},
            ywaActionMap: {"swp":103,"click":12,"secvw":18,"hvr":115,"error":99},
            ywaOutcomeMap: {"fetch":30,"end":31,"dclent":101,"dclitm":102,"op":105,"cl":106,"nav":108,"svct":109,"unsvct":110,"rmsvct":117,"slct":121,"imprt":123,"lgn":125,"lgo":126,"flagitm":129,"unflagitm":130,"flatcat":131,"unflagcat":132,"slctfltr":133} 
        };
        if (rapidPageConfig.rapidEarlyConfig.keys) {
            rapidPageConfig.rapidEarlyConfig.keys.bw = document.body.offsetWidth;
            rapidPageConfig.rapidEarlyConfig.keys.bh = document.body.offsetHeight;
        }
        rapidPageConfig.rapidConfig.keys.bw = document.body.offsetWidth;
        rapidPageConfig.rapidConfig.keys.bh = document.body.offsetHeight;
        </script>
        
                            
                    
                    
                    
                    
                    
                    

    <div id="UH">
        <div id="UH-ColWrap">
            <div id="applet_p_30345894" class=" M-0 js-applet header Zoom-1  Mb-0 " data-applet-guid="p_30345894" data-applet-type="header" data-applet-params="_suid:30345894" data-i13n="auto:true;sec:hd" data-i13n-sec="hd" data-ylk="rspns:nav;t1:a1;t2:hd;itc:0;"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-uh-wrapper" class="Bds(n) Bdc($headerBdr) Scrolling_Bdc($headerBdrScroll) has-scrolled_Bdc($headerBdrScroll) Bdbw(1px) ua-ie7_Bdbs(s)! ua-ie8_Bdbs(s)! Scrolling_Bxsh($headerShadow) has-scrolled_Bxsh($headerShadow) Bgc(#fff) T(0) Start(0) End(0) Pos(f) Z(3) Zoom">
    
    <div id="mega-topbar" class="Pos(r) H(22px) mini-header_Mt(-19px) Reader-open_Mt(-19px) Bg($topbarBgc) Bxsh($topbarShadow) Z(7)"   data-ylk="rspns:nav;t1:a1;t2:hd;t3:tb;sec:hd;itc:0;elm:itm;elmt:pty;">
    <ul class="Pos(r) Miw(1000px) Pstart(9px) Lh(1.7) Reader-open_Op(0) mini-header_Op(0)" role="navigation">
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:home;t5:home;cpos:1;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) Icon-Fp2 IconHome"></i>Home</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mail.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mail;t5:mail;cpos:2;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mail</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.flickr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:flickr;t5:flickr;cpos:3;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Flickr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.tumblr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:tumblr;t5:tumblr;cpos:4;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Tumblr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://answers.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:answers;t5:answers;cpos:5;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Answers</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://groups.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:groups;t5:groups;cpos:6;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Groups</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mobile.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mobile;t5:mobile;cpos:7;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mobile</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(10px) navigation-menu">
            <a href="https://everything.yahoo.com/" class="navigation-menu-title Pos(r) C(#fff) Pstart(12px) Pend(24px) Py(4px) Fz(13px) menu-open_C($topbarMenu) menu-open_Bgc(#fff) menu-open_Z(8) rapidnofollow"   data-ylk="rspns:op;t5:more;slk:more;itc:1;elmt:mu;cpos:8;" tabindex="1">More<i class="Pos(a) Pt(2px) Pstart(6px) Fw(b) Icon-Fp2 IconDownCaret"></i></a>
            <div class="Pos(a) Bgc(#fff) Bxsh($topbarMenuShadow) Z(7) V(h) Op(0) menu-open_V(v) menu-open_Op(1)">
                <ul class="Px(12px) Py(5px)">
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/politics/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:politics;t5:politics;cpos:9;" tabindex="1">Politics</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/celebrity/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:celebrity;t5:celebrity;cpos:10;" tabindex="1">Celebrity</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/movies/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:movies;t5:movies;cpos:11;" tabindex="1">Movies</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/music/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:music;t5:music;cpos:12;" tabindex="1">Music</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tv/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tv;t5:tv;cpos:13;" tabindex="1">TV</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/health/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:health;t5:health;cpos:14;" tabindex="1">Health</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/style/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:style;t5:style;cpos:15;" tabindex="1">Style</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/beauty/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:beauty;t5:beauty;cpos:16;" tabindex="1">Beauty</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/food/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:food;t5:food;cpos:17;" tabindex="1">Food</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/parenting/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:parenting;t5:parenting;cpos:18;" tabindex="1">Parenting</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/makers/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:makers;t5:makers;cpos:19;" tabindex="1">Makers</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tech/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tech;t5:tech;cpos:20;" tabindex="1">Tech</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://shopping.yahoo.com/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:shopping;t5:shopping;cpos:21;" tabindex="1">Shopping</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/travel/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:travel;t5:travel;cpos:22;" tabindex="1">Travel</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/autos/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:autos;t5:autos;cpos:23;" tabindex="1">Autos</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/realestate/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:homes;t5:homes;cpos:24;" tabindex="1">Real Estate</a>
                    </li>
                
                </ul>
            </div>
        </li>
    
    
    
    </ul>
</div>

    
    <div id="mega-uh" class="M(a) Maw(1256px) Miw(1000px) Pos(r) Pt(22px) Pb(24px) Reader-open_Pt(13px) mini-header_Pt(13px) Reader-open_Pb(14px) mini-header_Pb(14px) Z(6)">
        <h1 class="Fz(0) Pos(a) Pstart(15px) Reader-open_Pt(0px) mini-header_Pt(0px) search-mini-header_Pstart(10px) ua-ie7_Mt(4px)">
    <a id="uh-logo" href="https://www.yahoo.com/" class="D(ib) H(47px) W($bigLogoWidth) Bgi($logoImage) Bgr(nr) Bgz($bigLogoWidth) Reader-open_Bgz($mediumLogoWidth) mini-header_Bgz($mediumLogoWidth) search-mini-header_Bgz($searchLogoWidth) Bgz($smallLogoWidth)!--sm1024 Bgp($bigLogoPos) Reader-open_Bgp($mediumLogoPos) mini-header_Bgp($mediumLogoPos) search-mini-header_Bgp($searchLogoPos) Bgp($searchLogoPos)!--sm1024 ua-ie7_Bgi($logoImageIe) ua-ie7_Mstart(-170px) ua-ie8_Bgi($logoImageIe) "   data-ylk="rspns:nav;t1:a1;t2:hd;sec:hd;itc:0;slk:logo;elm:img;elmt:logo;" tabindex="1">
        <b class="Hidden">Yahoo</b>
    </a>
</h1>

        <ul class="Pos(a) End(15px) List(n) Mt(3px) Reader-open_Mt(1px) mini-header_Mt(1px)" role="menubar">
            
    <li class="Pos(r) Fl(start) Mend(26px)">
        <a id="uh-signin" class="Bdc($signInBtn) Bdrs(5px) Bds(s) Bdw(2px) Bgc($signInBtn):h C($signInBtn) C(#fff):h D(ib) Ell Fz(14px) Fw(b) Py(2px) Mt(5px) Ta(c) Td(n):h Miw(78px) H(18px)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;" tabindex="4"><div class="Miw(78px) Ta(c) Pos(a) T(0) Lh($userNavTextLh)">Sign in</div></a>
    </li>


            
    <li id="uh-notifications" class="uh-menu uh-notifications Fl(start) D(ib) Mend(13px) Cur(p) ua-ie7_D(n) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <button class="uh-menu-btn Pos(r) Bd(0) Px(8px) Pt(1px) Lh(1.3)" aria-label="Notifications" aria-haspopup="true" aria-expanded="true" tabindex="5" title="Notifications">
            <i class="Lh($userNotifIconLh) C($mailBtn) Fz(28px) Grid-U Icon-Fp2 IconBell uh-notifications-icon"></i>
            <span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) End(-4px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-notification-count W(22px)"></span>
        </button>
        <div class="uh-notification-panel uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Ovy(a) Trs($menuTransition) Cur(d) V(h) Op(0) Mah(0) uh-notifications:h_V(v) uh-notifications:h_Op(1) uh-notifications:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" aria-label="Notifications" role="menu" tabindex="5">
            <div class="uh-notifications-loading Py(16px) Px(24px) Ta(c)">
                <div class="W(30px) H(30px) Mx(a) My(12px) Bgz(30px) Bgi($animatedSpinner) ua-ie8_D(n)"></div>
                <span class="C($mailTstamp) D(n) ua-ie8_D(b)">Loading Updates</span>
            </div>
        </div>
    </li>


            
    <li id="uh-mail" class="uh-menu uh-mail D(ib) Mstart(14px) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <a id="uh-mail-link" href="https://mail.yahoo.com/" class="Pos(r) D(ib) Ta(s) Td(n):h uh-menu-btn" aria-label="Mail" aria-haspopup="true" role="button" aria-expanded="true" tabindex="6"><i class="Lh($userNavIconLh) W(28px) Mend(8px) C($mailBtn) Fz(30px) Grid-U Icon-Fp2 IconMail uh-mail-icon"></i><span class="Lh($userNavTextLh) D(ib) C($mailBtn) Fz(14px) Fw(b) Va(t)">Mail</span><span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) Start(16px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-mail-count W(22px)"></span>
        </a>
        
            <div class="uh-mail-preview uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Trs($menuTransition) V(h) Op(0) Mah(0) uh-mail:h_V(v) uh-mail:h_Op(1) uh-mail:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" role="menu" aria-label="Mail" tabindex="6">
                
                    <div class="Px(24px) Py(20px) Ta(c)">
                        <a class="C($menuLink) Fw(b) Td(n)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;">Sign in</a>  to view your mail
                    </div>
                
            </div>
        
    </li>


        </ul>
        <div id="uh-search" class="Pos(r) Mend(375px) Mstart(206px) search-mini-header_Mstart(112px) Mstart(120px)--sm1024 Maw(595px) H(40px) Reader-open_H(40px) mini-header_H(40px) search-mini-header_Maw(685px) Maw(745px)--sm1024 Va(t)">
    <form name="input" class="glowing glow" action="https://search.yahoo.com/search;_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-" method="get" data-assist-action="https://search.yahoo.com/search;_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-">
        <table class="Bdcl(s)" role="presentation">
            <tbody>
                <tr>
                    <td class="Pos(r) P(0) W(100%) H(40px) Reader-open_H(40px) mini-header_H(40px) D(tb) ua-safari_D(tbc) Tbl(f)">
                        <input id="uh-search-box" type="text" name="p" class="Pos(a) T(0) Start(0) Bd Bdc($searchBdrFoc):f Bdc($searchBdr) glow_Bdc($searchBtnGlow) Bgc(t) Bxsh(n) Fz(18px) M(0) O(0) Px(10px) W(100%) H(inh) Z(2) Bxz(bb) Bdrs(2px) ua-ie7_H(36px) ua-ie7_Lh(36px) ua-ie8_Lh(36px) ua-ie7_W(92%)" autofocus autocomplete="off" autocapitalize="off" aria-label="Search" tabindex="2">
                        
                        
                            <input type="hidden" data-fr="yfp-t-201" name="fr" value="yfp-t-201" />
                        
                            <input type="hidden" data-fp="1" name="fp" value="1" />
                        
                            <input type="hidden" data-toggle="1" name="toggle" value="1" />
                        
                            <input type="hidden" data-cop="mss" name="cop" value="mss" />
                        
                            <input type="hidden" data-ei="UTF-8" name="ei" value="UTF-8" />
                        
                    </td>
                    <td class="Miw(5px)"></td>
                    <td>
                        <button id="uh-search-button" type="submit" class="rapid-noclick-resp C(#fff) Fz(16px) H(40px) W(140px) Reader-open_H(40px) mini-header_H(40px) Px(26px) Py(0) Whs(nw) Bdrs(3px) Bdw(0) Bgc($searchBtn) glow_Bgc($searchBtnGlow) Bxsh($searchBtnShadow) glow_Bxsh($searchBtnShadowGlow) Lh(1)"   data-ylk="rspns:nav;t1:a1;t2:srch;sec:srch;slk:srchweb;elm:btn;elmt:srch;tar:search.yahoo.com;tar_uri:/search;itc:0;" tabindex="3">
                        <span class="search-default-label Fw(500) ">Search Web</span>
                        <span class="search-short-label Fw(500) D(n) ">Search</span>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    
    <ul id="Skip-links" class="Pos(a)">
        <li><a href="#Navigation" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Navigation</a></li>
        <li><a href="#Main" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Main content</a></li>
        <li><a href="#Aside" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Right rail</a></li>
    </ul>
    
</div>

    <script>
!function(){var e,t,a,n,c=window,o=document,r=o.getElementById("uh-search-box"),g=function(c){try{a=c.keyCode,n=c.target?c.target:c.srcElement,e=r.value.length,n===r&&0===e&&a>=32&&40>=a?r.blur():"EMBED"===n.tagName||"OBJECT"===n.tagName||"INPUT"===n.tagName||"TEXTAREA"===n.tagName||c.altKey||c.metaKey||c.ctrlKey||!(32>a||a>40)||9==a||13==a||16==a||27==a||(e&&(r.setSelectionRange?r.setSelectionRange(e,e):r.createTextRange&&(t=r.createTextRange(),t.moveStart("character",e),t.select())),r.focus())}catch(c){}};r&&(c.addEventListener?c.addEventListener("keydown",g,!1):c.attachEvent&&o.attachEvent("onkeydown",g),setTimeout(function(){r.focus()},500))}();
</script>



    </div>
</div>

 </div> </div> </div>            <!-- App close -->
            </div>
        </div>
    </div><!-- Header -->
    <div id="Stencil">
    <div id="Reader" class="js-viewer-viewerwrapper">
        <div class="ReaderWrap">
            <div class="InnerWrap">
                <div id="applet_p_50000101" class=" App_v2  M-0 js-applet viewer Zoom-1  App-Chromeless " data-applet-guid="p_50000101" data-applet-type="viewer" data-applet-params="_suid:50000101" data-i13n="auto:true;sec:app-view" data-i13n-sec="app-view" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div id="hl-viewer" class="js-slider Hl-Viewer Ov-h desktop" role="document" aria-labelledby="modal-header" aria-live="polite" aria-hidden="true" tabindex="-1">
    
    
        <button id="closebtn" class="Viewer-Close-Btn Bdr-0 D-b Fw(b) js-close-content-viewer rapid-noclick-resp Z-7">
            <i class="Icon-Fp2 IconActionCross"></i>
            <b class="Td(n) Hidden">Close this content, you can also use the Escape key at anytime</b>
        </button>
        <div class="viewer-wrapper Ov-h mega-modal">
            <div class="">
                
                    <div class="content-container"></div>
                
                
                
                
                <div class="footer-ads">
                    <div id="hl-ad-FSRVY-0" class="D-ib">
                        <div id="hl-ad-FSRVY-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT9-0" class="D-ib">
                        <div id="hl-ad-FOOT9-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT-0" class="D-ib">
                        <div id="hl-ad-FOOT-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                </div>
                <span id="viewer-end" tabindex="0"></span>
            </div>
        </div>
    

    


</div>

 </div> </div> </div> <!-- yrid: bn2r1rlbc1eo3 | edgepipe: 0 | authed: 0 | ynet: 1 | ssl: 1 | spdy: 0 | ytee: 0 | bucket: 201 | colo: ne1 | device: desktop | bot: 0 | environment: prod | lang: en-US | partner: none | site: fp | region: US | intl: us | tz: America/Los_Angeles -->
<!-- tdserver-my.fp.production.manhattan.ne1.yahoo.com (pprd3-node5062-lh1.manhattan.ne1.yahoo.com) | Served by ynodejs | Sun Feb 14 2016 17:36:03 GMT+0000 (UTC) -->            <!-- App close -->
            </div>
            </div>
        </div>
        <div id="Overlay"></div>
        <div class="Reader-bg"></div>
    </div>
</div>
    
    <div id="Masterwrap" class="slider js-viewer-pagewrapper">
        <div class="page">
            <div id="Billboard-ad">
                                                    <div id="my-adsMAST" class="D-n">
                    <div id="my-adsMAST-iframe">
                        
                        <!-- MAST has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136h20o12(gid%241jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st%241455471493103052,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2412543bmvc,aid%24nsdYUWKKamo-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=MAST)"></noscript>
                    </div>
                </div>
                                <div id="ad-north-base" class=""><div id="ad-north" class="BillboardAd"></div></div>
            </div>
            
            <div class="Col1" role="navigation" id="Navigation" tabindex="-1">
                <div class="Col1-stack" data-plugin="sticker" data-sticker-top="75px">
                                <div id="applet_p_50000195" class=" M-0 js-applet navrailv2 Zoom-1  Mb-20 " data-applet-guid="p_50000195" data-applet-type="navrailv2" data-applet-params="_suid:50000195" data-i13n="auto:true;sec:nav" data-i13n-sec="nav" data-ylk="rspns:nav;itc:0;t1:a2;t2:nav;elm:itm;"> <!-- App open -->
        <!-- src=mdbm type=popular mod=td-applet-navlinks-atomic ins=p_50000195 ts=1455470686.2748 --><div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

    

    

    <ul class="navlinks-list D(tbc) W(134px) Mstart(6px)">
    
        <li class="navlink  Py(5px)">
            <a href=https://mail.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMail C($MailNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Mail</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://news.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconNews C($NewsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">News</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://sports.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconSports C($SportsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Sports</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://finance.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFinance C($FinanceNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Finance</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/autos/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconAutos C($AutosNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Autos</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/celebrity/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconCelebrity C($CelebrityNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Celebrity</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://shopping.yahoo.com class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconShopping C($ShoppingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Shopping</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/movies/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMovies C($MoviesNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Movies</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/politics/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconPolitics C($PoliticsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Politics</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/beauty/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconBeauty C($BeautyNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Beauty</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Style</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Tech</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/travel/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTravel C($TravelNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Travel</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh770 Py(5px)">
            <a href=https://www.yahoo.com/food/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFood C($FoodNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Food</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh770 Py(5px)">
            <a href=https://www.yahoo.com/health/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconHealth C($HealthNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Health</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh860 Py(5px)">
            <a href=https://www.yahoo.com/makers/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMakers C($MakersNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Makers</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh860 Py(5px)">
            <a href=https://www.yahoo.com/parenting/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconParenting C($ParentingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Parenting</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">TV</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://www.yahoo.com/realestate/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconRealEstate C($RealEstateNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Real Estate</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">More on Yahoo</span>
            </a>
        </li>
    
        <li class="D(b)--maxh900 Py(5px) js-navlinks-seemore Pos(r) D(n) O(0)"  role="button" tabindex="0" aria-haspopup="true" aria-label="More Navlinks">
            <i class="Icon-Fp2 IconStreamShare Va(m) Pend(6px) Fz(22px) D(tbc) Miw(29px) Ta(c) C($MoreNav)"></i>
            <span class="Va(m) D(tbc) Fz(14px) Fw(b) V(h)--sm1024 C($navlink) Col1-stack:h_V(v) C($navlinkHover):h Cur(p)">More</span>

            <div class="js-navlinks-seemore-menu D(n) js-navlinks-menu-open_D(ib) Va(b) Pos(a) B(-14px) Start(60px) Pstart(60px)">
                <ul class="Bxsh($menuShadow) Bd($submenuBdr) Bdrs(3px) Bgc(#fff) Cur(d) Py(13px) Px(20px) Miw(140px)">
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Style</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Tech</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/travel/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTravel C($TravelNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Travel</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh770">
                        <a href=https://www.yahoo.com/food/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconFood C($FoodNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Food</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh770">
                        <a href=https://www.yahoo.com/health/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconHealth C($HealthNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Health</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh860">
                        <a href=https://www.yahoo.com/makers/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconMakers C($MakersNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Makers</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh860">
                        <a href=https://www.yahoo.com/parenting/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconParenting C($ParentingNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Parenting</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">TV</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://www.yahoo.com/realestate/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconRealEstate C($RealEstateNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Real Estate</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b) js-navlinks-lastitem">
                            <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">More on Yahoo</span>
                        </a>
                    </li>
                
                </ul>
            </div>
        </li>
    </ul>


 </div> </div> </div> <!-- yrid: XrjAVgAAAADjWQAAzBLgVg.. | edgepipe: 0 | authed: 0 | ynet: 1 | ssl: 1 | spdy: 0 | ytee: 0 | bucket: 201 | colo: ne1 | device: desktop | bot: 0 | environment: prod | lang: en-US | partner: default | site: fp | region: US | intl: us | tz: America/Los_Angeles | mode: fallback -->
<!-- tdserver-my.fp.production.manhattan.ne1.yahoo.com (pprd3-node5006-lh1.manhattan.ne1.yahoo.com) | Served by ynodejs | Sun Feb 14 2016 17:24:46 GMT+0000 (UTC) -->            <!-- App close -->
            </div>                           <div id="my-adsTXTL" class="Mt-10 Mb-20 Pt-20 Pos-r ad-txtl D-n">
                    <div class="Mx-a" id="my-adsTXTL-iframe">
                        <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<style>
	#fc_align{
		position: static !important;
		width: 120px !important;
		margin: auto !important;
	}
</style>
<!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136h20o12(gid%241jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st%241455471493103052,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413aar03r1,aid%24LM9YUWKKamo-,bi%242275188051,agp%243476502551,cr%244451009051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=TXTL)"></noscript>
                    </div>
                </div>
                </div>
            </div>
            <div class="Col2">
                <div id="Main" class="Col2-stack" role="content" tabindex="-1">
                    <div id="Banner">
                        
                    </div><!-- Banner -->
                                <div id="applet_p_50000173" class=" M-0 js-applet streamv2 Zoom-1  Mb-20 " data-applet-guid="p_50000173" data-applet-type="streamv2" data-applet-params="_suid:50000173" data-i13n="auto:true;sec:strm;useViewability:true" data-i13n-sec="strm" data-ylk="t1:a3;t2:strm;t3:ct;cat:default;rspns:nav;itc:0;" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div data-uuid-list="p_50000173">
    
      
<div class="Px(15px) Py(11px) Mb(22px) C(#fff) breakingnews gradient-1  js-stream-followable Bdrs(3px) Pos(r)" data-followid="d44fbfea-d29f-11e5-ac3f-fa163e6f4a7e" data-followname="Antonin Scalia dies">
    <div>
        <span class="D(tbc) Fz(15px) Fw(b) Lh(20px) Pend(8px) Whs(nw)">Watch live:</span>
        <div class="D(tbc) Lh(20px)">
            
                <a class="js-stream-follow-name Fz(13px) C(#fff)  js-content-viewer rapid-noclick-resp rapidnofollow" href="https://www.yahoo.com/katiecouric/yahoo-news-special-report-antonin-scalias-012302091.html" data-uuid="748e13e0-0aae-3a3d-a620-f8351ede2ea7">Antonin Scalia and the political repercussions of his death at 1p.m. ET</a>
            
            
                <span class="btnWrap">
                    <button title="unfollow" class="Fw(b) followed_D(i) D(n) Va(b) P(0) Bd(0) unfollowBtn C(#fff) O(n) ua-ie7_W(100%)"   data-ylk="rapid-base:undefined;r:false;ccode:false;t4:false;elm:btn;elmt:unfollow;itc:1;aid:d44fbfea-d29f-11e5-ac3f-fa163e6f4a7e;" data-action-outcome="dclent">
                    <i class="unfollowIcon unfollowBtn:h_D(b) D(n) Fl(start) Fz(12px) Fw(b) W(19px) H(19px) Lh(20px) Ta(c) Icon-Fp2 IconStarOutline C(#fff)"></i><i class="followingIcon unfollowBtn:h_D(n) Icon-Fp2 Fl(start) IconStarFilled Fz(12px) Fw(b) W(19px) H(19px) Lh(20px) Ta(c) C(#fff)"></i>
                    <div class="unfollowTxt Mstart(20px) Fz(13px) Tt(c) C(#fff) H(20px) Lh(20px) W(56px)">
                        <div class="Fl(start) Lh(20px)">Unfollow</div>
                        </div>
                    </button>
                    <button title="follow" class="Va(b) P(0) Bd(0) Fw(b) followed_D(n) D(i) followBtn C(#fff) O(n) ua-ie7_W(100%)"   data-ylk="rapid-base:undefined;r:false;ccode:false;t4:false;elm:btn;elmt:follow;itc:1;aid:d44fbfea-d29f-11e5-ac3f-fa163e6f4a7e;" data-action-outcome="dclent">
                        <i class="followBtn:h_D(n) followed_D(n) Icon-Fp2 Fl(start) IconStarOutline Fz(13px) W(19px) H(19px) Lh(20px) Ta(c)"></i><i class="followBtn:h_D(b) followed_D(n) D(n) Icon-Fp2 Fl(start) IconStarFilled Fz(13px) W(19px) H(19px) Lh(20px) Ta(c) followBtn:h_C(#fff)"></i><div class="followed_D(n) Mstart(20px) Fz(13px) Tt(c) H(20px) W(54px) Trsdu(.1s) Trsp($trsp_width)"><div class="Fl(start) Lh(20px)">Follow</div></div>
                    </button>
                </span>
            
        </div>
    </div>
</div>


    

    
    <ul class=" js-stream-tmpl-items js-stream-dense  js-stream-hover-enable My(0) Mb(0) Wow(bw)" id="Stream">
    
        
    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Supreme_Court_of_the_United_States|WIKIID:Barack_Obama|WIKIID:Antonin_Scalia|WIKIID:United_States_Senate_Committee_on_the_Judiciary|WIKIID:United_States_Senate|WIKIID:Republican_Party_%28United_States%29|YCT:001000661|YCT:001000671|YCT:001000681"  data-uuid="b7afc2cf-b7cd-333d-ae2f-f21f016f6dca" data-cauuid="b7afc2cf-b7cd-333d-ae2f-f21f016f6dca" data-type="article" data-cluster="b7afc2cf-b7cd-333d-ae2f-f21f016f6dca"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Pt(12px) Pb(17px)"><div class="js-stream-roundup js-stream-roundup-filmstrip Pos(r) Wow(bw) Cf">
    <div class="strm-headline Pos(r)">
        <a href="https://www.yahoo.com/politics/republicans-vow-to-block-obama-supreme-court-011758577.html" class="Pos(r) D(ib) Z(1) js-stream-content-link js-content-title js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;refcnt:4;subsec:8499;imgt:ss;g:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;aid:id-3594690;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:img;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
        
        <img src="/sy/uu/api/res/1.2/_gq.j0JEvvP_11L4yqV3SQ--/Zmk9c3RyaW07aD0yNzQ7cHlvZmY9MDtxPTk1O3c9NzAyO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021416/images/smush/mcconnell_ipad_1455417700.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
        
        <div class="Pos(a) T(0) Start(0) W(100%) H(100%) Ov(h)">
            <div class="Pos(a) B(0) M(15px) Mb(11px) Mend(120px) C(#fff) Z(1)">
                <h2 class="js-stream-item-title Fz(21px) Fw(b) Mb(3px) Lh(24px) Td(u):h">Risks with blocking Obama's SCOTUS nominee</h2>
                <span class="stream-summary Fz(13px) C(#d9d9db) Mend(5px)">Mitch McConnell says the Senate should not fill the Supreme Court vacancy until a new president is elected.</span><span class="Fw(b) D(ib) Va(b) Lh(18px) Td(u):h">'People should have a voice' Â»</span></div>
            <div class="strm-img-gradient W(100%) H(100%) rounded-img"></div>
        </div>
        </a>
        <div class="Pos(a) End(13px) T(10px) W(30px) Mend(2px) Ta(end) Z(2)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) C(white)" data-cmntnum="8499"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow) js-stream-comment-hidden">8499</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C(white) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;refcnt:4;subsec:8499;imgt:ss;g:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;aid:id-3594690;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) Tsh($comment_shadow) ActionComments:h_C($signin_blue)"></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C(white) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) Tsh($comment_shadow)"></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C(white) P(14px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;refcnt:4;subsec:8499;imgt:ss;g:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;aid:id-3594690;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) Tsh($comment_shadow)"></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C(white) Tsh($comment_shadow) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
    </div>
    <ul class="P(0) Mt(7px) Mstart(-2px) Mend(7px) Fz(12px) Whs(nw) Lts(-.31em)">
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/politics/what-does-justice-scalias-death-mean-for-pending-000410173.html" data-uuid="c6f5963c-4cf7-3861-9bd3-021648b72376" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:2;bpos:1;pos:2;ss_cid:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;refcnt:4;subsec:8499;imgt:ss;g:c6f5963c-4cf7-3861-9bd3-021648b72376;aid:id-3594682;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/v3Q7xFi__xJOMVDVHuCwNw--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021416/images/smush/supreme-court-flag_ipad_1455410003.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">What Scalia's death means for SCOTUS cases</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://gma.yahoo.com/woman-loving-last-name-kissing-booth-yard-valentines-181648270--abc-news-house-and-home.html" data-uuid="af0e3efa-7bd8-36c2-8329-2ddea343e6b9" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:3;bpos:1;pos:3;ss_cid:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;refcnt:4;subsec:8499;imgt:ss;g:af0e3efa-7bd8-36c2-8329-2ddea343e6b9;aid:id-3594642;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/klYHYzcYGmQFB675hLibFg--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021316/images/smush/Kissing2ipad_ipad_1455349058.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Family lives up to its name on Valentine's Day</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/music/12-intriguing-grammy-records-may-194941142.html" data-uuid="bece9969-1d9f-385f-98e9-587620019027" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:4;bpos:1;pos:4;ss_cid:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;refcnt:4;subsec:8499;imgt:ss;g:bece9969-1d9f-385f-98e9-587620019027;aid:id-3594645;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/bZkWBWxDIi3yyTKXj71oMw--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021316/images/smush/Bob-Dylan2ipad_ipad_1455360827.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">12 Grammy records that may be set</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/celebrity/sports-illustrated-unveils-3-swimsuit-issue-045726283.html" data-uuid="4a03c2dc-72db-3c0d-8ffd-72fefd948b25" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:5;bpos:1;pos:5;ss_cid:b7afc2cf-b7cd-333d-ae2f-f21f016f6dca;refcnt:4;subsec:8499;imgt:ss;g:4a03c2dc-72db-3c0d-8ffd-72fefd948b25;aid:id-3594702;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/PfnEKgvR8vPOB56E7WkVhw--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021416/images/smush/sigals_ipad_1455427592.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">3 models chosen for 2016 SI swimsuit cover</h3>
            </a>
        </li>
        
        
    </ul>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Donald_Trump|WIKIID:George_W._Bush|WIKIID:Republican_Party_%28United_States%29|YCT:001000661|YCT:001000708" data-offnet="1" data-uuid="f9bc79d3-84f7-31c4-a5ed-d3f76241b64a" data-type="article" data-cluster="3c0ad7b7-f487-42fa-8e07-08f29cc6328e"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://www.vox.com/2016/2/14/10988380/donald-trump-9-11" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:3c0ad7b7-f487-42fa-8e07-08f29cc6328e;refcnt:2;imgt:ss;g:f9bc79d3-84f7-31c4-a5ed-d3f76241b64a;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000025860S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/mvFVro6P6_Waf78dt3vyIg--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/5afee85e36baf062185b74c4caaee6fd" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.vox.com/2016/2/14/10988380/donald-trump-9-11" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:3c0ad7b7-f487-42fa-8e07-08f29cc6328e;refcnt:2;imgt:ss;g:f9bc79d3-84f7-31c4-a5ed-d3f76241b64a;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000025860S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Donald Trump finally went too far for Republicans</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Donald Trump finally made some bold and provocative claims that were largely true, and the Republican Party finally closed ranks to attack him. Saying Mexican immigrants are rapists didn&#39;t do it. Calling for a return of torture didn&#39;t do it. Calling for a ban on Muslim immigration didn&#39;t do it. Raising questions about Barack Obama&#39;s status as an American citizen didn&#39;t do it. Pretending that thousands of Muslims in New Jersey cheered 9/11 didn&#39;t do it. So what did? Trump said that invading Iraq was a disaster, that the country was misled into invading Iraq by the Bush administration, and that the claim that Bush kept the country safe from terrorism is ridiculous because 9/11 happened on his watch.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Vox</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://finance.yahoo.com/news/donald-trump-confronts-audience-another-025035298.html" data-uuid="1355cd52-a446-3147-8f4e-a5c46ea086ff" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:2;cposy:7;bpos:1;pos:2;ss_cid:3c0ad7b7-f487-42fa-8e07-08f29cc6328e;refcnt:2;imgt:ss;g:1355cd52-a446-3147-8f4e-a5c46ea086ff;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000025860S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/1b1GLY.udiBd6lMMxFg9DQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/Donald_Trump_confronts_audience_in-3895b84d156e05ab38ef57b340538e02" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Donald Trump confronts audience in another GOP debate after loud boos</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Business Insider</span></div></a>
            
            
            
                <a href="https://www.washingtonpost.com/news/the-fix/wp/2016/02/13/in-2008-donald-trump-said-george-w-bush-shouldve-been-impeached/" data-uuid="b06cff58-a3cd-3aa9-a38d-91c6bd4d7526" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:2;cposy:8;bpos:1;pos:3;ss_cid:3c0ad7b7-f487-42fa-8e07-08f29cc6328e;refcnt:2;imgt:ss;g:b06cff58-a3cd-3aa9-a38d-91c6bd4d7526;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000025860S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/JLDWR8RIbP8OCTm567NZVQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/008705d0adfbf29bc448c422a6a91f3d" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">In 2008, Donald Trump said George W. Bush shouldâve been impeached</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Washington Post</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:3c0ad7b7-f487-42fa-8e07-08f29cc6328e;refcnt:2;imgt:ss;g:f9bc79d3-84f7-31c4-a5ed-d3f76241b64a;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;r:4000025860S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad  Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=sHGRjcsGIS9x0m74sjFA.ZqHodqY5uHljgi57_Hb53LPbutyWsdH0Yu8uWqqHrnh695Cikymf0WMxYCMCy3eM39i0mO32HyCy6cu4r6NKMGhTg27ObMxORBvaDxPewkZAW5Ef.EwWD3BXIIJi4YMu5rO3fNEZQKUGquF9jjCQFQnWZmkjeABPRSUrlFpcOhoT3g.sndQPZtfXyeTz01HOQ9jZaCaEmY7KwhpNMzR.swpCtrhuLZdoVCp3McviLpunK2VdRjpgYm1hn9QXqNiK5iSOaKRO486ZjvGJdxc2yLI51739axYZE2YYeWslFYn9O28CLH3olsCa2NIjAd3Dhx1iJny1OnraR_FK5dAWMNAIUmqCAi96L0atKR3JQZv2yw2Uxcs&amp;ap=4" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15l41c1pc(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31674818529,dmn$sarenza.com,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$919929,pbid$1,seid$4250754))&amp;r=1455471493151&amp;al=$(AD_FEEDBACK)" data-uuid="31674818529">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=aLrGPpUGIS.WYzCfDC60U6dhBIx37qzgH8iejahB1phrIdNG1qFl.ul7Cyx9Ib6wy9RADC_bstP265_H9pCaEqZ0AWK4nEgPQcuJBBbdesNbbVPPdTx00XWQrirmabHJsfro1P2SBnf5q1uB3VfjRxkW4PfVIdml1HzyWhgXyhj3yfCPZU9rpFkHc3eHdYMEV61W_Foj5rCaBbqFa9M6reZVwlCR02Kt_oARWCXFP8uDKVFPFfmTwm9Axcn6n7o.ulerhjTUB3m._cYYq_JUWdAqh10FslYfQ3XMgQsGrvxS0lLMupiprYcnHtPVmHBT95lyMGUevhLKt_NUSee5Be_LmFyszVoRpfLGPz3iZG1d.mi4PD0KH0uSyx_CD_IBeo5liwd4yuxefLGFPkjasgezej56SJgIvoL.HBFfjQdyEeRC7IDPTOp6Rj1bQTve3ziED697JT6p5AXbn_LV5JCLqUlEwh2SuCgz8AsZz_UqEGKgnI51ibTX0voMpjRmpuz_9igfWgMMXW9tp8MaOzEZXKLlCaC_IhVv2Xed.kFW7aB7JWtbYtjm0495TzD7Tt6AD9aFPfqKsVGQBYY4DypJrFzqFuDg5Y.kraOG1igM0Z8oUzogvjZxp0QF330hL1_EH5MlFzVKvcA0m_DO6ZGCLC5.vr3oU3.rRO3oT6m.ZGiGX9Z4Q160RRyz_oc8mqkzcbjuRCYc9ipW9.1E0bBLJIES0J.Sq75Wr0Wqpolcn6M8mERhjA--%26lp=http%3A%2F%2Feulerian.sarenza.com%2Fdynclick%2Fsarenza%2F%3Fead-publisher%3DYahoo%26ead-name%3DYahoo-Streamads%26ead-location%3DMixte%26ead-creative%3Dsoldes-pe15-generique-texte%26ead-creativetype%3DTexte%26eurl%3Dhttp%253A%252F%252Fwww.sarenza.com%252Fchaussure-pas-cher-femme" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i)" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ct;g:31674818529;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/uu/api/res/1.2/L_Lc0TBqPzFfIc1pIJHydw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452785587794-6701.jpg.cf.jpg" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img" alt="">
                </a>
            </div>
            
            <div class="strm-right Pos(r) Mstart(29%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:sp;g:31674818529;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:info;g:31674818529;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=aLrGPpUGIS.WYzCfDC60U6dhBIx37qzgH8iejahB1phrIdNG1qFl.ul7Cyx9Ib6wy9RADC_bstP265_H9pCaEqZ0AWK4nEgPQcuJBBbdesNbbVPPdTx00XWQrirmabHJsfro1P2SBnf5q1uB3VfjRxkW4PfVIdml1HzyWhgXyhj3yfCPZU9rpFkHc3eHdYMEV61W_Foj5rCaBbqFa9M6reZVwlCR02Kt_oARWCXFP8uDKVFPFfmTwm9Axcn6n7o.ulerhjTUB3m._cYYq_JUWdAqh10FslYfQ3XMgQsGrvxS0lLMupiprYcnHtPVmHBT95lyMGUevhLKt_NUSee5Be_LmFyszVoRpfLGPz3iZG1d.mi4PD0KH0uSyx_CD_IBeo5liwd4yuxefLGFPkjasgezej56SJgIvoL.HBFfjQdyEeRC7IDPTOp6Rj1bQTve3ziED697JT6p5AXbn_LV5JCLqUlEwh2SuCgz8AsZz_UqEGKgnI51ibTX0voMpjRmpuz_9igfWgMMXW9tp8MaOzEZXKLlCaC_IhVv2Xed.kFW7aB7JWtbYtjm0495TzD7Tt6AD9aFPfqKsVGQBYY4DypJrFzqFuDg5Y.kraOG1igM0Z8oUzogvjZxp0QF330hL1_EH5MlFzVKvcA0m_DO6ZGCLC5.vr3oU3.rRO3oT6m.ZGiGX9Z4Q160RRyz_oc8mqkzcbjuRCYc9ipW9.1E0bBLJIES0J.Sq75Wr0Wqpolcn6M8mERhjA--%26lp=http%3A%2F%2Feulerian.sarenza.com%2Fdynclick%2Fsarenza%2F%3Fead-publisher%3DYahoo%26ead-name%3DYahoo-Streamads%26ead-location%3DMixte%26ead-creative%3Dsoldes-pe15-generique-texte%26ead-creativetype%3DTexte%26eurl%3Dhttp%253A%252F%252Fwww.sarenza.com%252Fchaussure-pas-cher-femme" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ad;g:31674818529;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class="Pos(r) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>Soldes chaussures : jusqu'Ã  -60% sur Sarenza.com</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u>
                    </a>
                </h3>
                <div>
                    <p class="Pos(r) stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">Sneakers colorÃ©es, mocassins, bottines trendy ou sacs Ã  main, vous ferez forcÃ©ment une belle affaire pendant les soldes sur Sarenza !</p>
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=aLrGPpUGIS.WYzCfDC60U6dhBIx37qzgH8iejahB1phrIdNG1qFl.ul7Cyx9Ib6wy9RADC_bstP265_H9pCaEqZ0AWK4nEgPQcuJBBbdesNbbVPPdTx00XWQrirmabHJsfro1P2SBnf5q1uB3VfjRxkW4PfVIdml1HzyWhgXyhj3yfCPZU9rpFkHc3eHdYMEV61W_Foj5rCaBbqFa9M6reZVwlCR02Kt_oARWCXFP8uDKVFPFfmTwm9Axcn6n7o.ulerhjTUB3m._cYYq_JUWdAqh10FslYfQ3XMgQsGrvxS0lLMupiprYcnHtPVmHBT95lyMGUevhLKt_NUSee5Be_LmFyszVoRpfLGPz3iZG1d.mi4PD0KH0uSyx_CD_IBeo5liwd4yuxefLGFPkjasgezej56SJgIvoL.HBFfjQdyEeRC7IDPTOp6Rj1bQTve3ziED697JT6p5AXbn_LV5JCLqUlEwh2SuCgz8AsZz_UqEGKgnI51ibTX0voMpjRmpuz_9igfWgMMXW9tp8MaOzEZXKLlCaC_IhVv2Xed.kFW7aB7JWtbYtjm0495TzD7Tt6AD9aFPfqKsVGQBYY4DypJrFzqFuDg5Y.kraOG1igM0Z8oUzogvjZxp0QF330hL1_EH5MlFzVKvcA0m_DO6ZGCLC5.vr3oU3.rRO3oT6m.ZGiGX9Z4Q160RRyz_oc8mqkzcbjuRCYc9ipW9.1E0bBLJIES0J.Sq75Wr0Wqpolcn6M8mERhjA--%26lp=http%3A%2F%2Feulerian.sarenza.com%2Fdynclick%2Fsarenza%2F%3Fead-publisher%3DYahoo%26ead-name%3DYahoo-Streamads%26ead-location%3DMixte%26ead-creative%3Dsoldes-pe15-generique-texte%26ead-creativetype%3DTexte%26eurl%3Dhttp%253A%252F%252Fwww.sarenza.com%252Fchaussure-pas-cher-femme" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ad;g:31674818529;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sarenza.com</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:op;g:31674818529;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Charles_Barkley|YCT:001000001"  data-uuid="745a2501-59a3-3d81-a9e7-3783b281fb94" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://news.yahoo.com/shaq-threw-down-monster-two-131503811.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:4;cposy:10;bpos:1;pos:1;subsec:221;imgt:ss;g:745a2501-59a3-3d81-a9e7-3783b281fb94;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Sports;elm:img;elmt:ct;r:4000024910S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/uu/api/res/1.2/MNrKrAS9cEUAyy25mQIpmw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/video.woven.com/1056166858f9e2eb106fd46c10b9f11f" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_sports) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="sports">Sports</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://news.yahoo.com/shaq-threw-down-monster-two-131503811.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:4;cposy:10;bpos:1;pos:1;subsec:221;imgt:ss;g:745a2501-59a3-3d81-a9e7-3783b281fb94;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Sports;elm:hdln;elmt:ct;r:4000024910S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Shaq Threw Down This Monster Two Hand Dunk After Charles Barkley Called Him Out</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Charles Barkley didnât think that Shaq could knock down a two handed dunk in his advanced age. It isnât like the big man has been out of the league for too long, but heâs certainly far from his playing days. And knowing Barkley, he canât refuse a little</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">UPROXX</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="221"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">221</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:4;cposy:10;bpos:1;pos:1;subsec:221;imgt:ss;g:745a2501-59a3-3d81-a9e7-3783b281fb94;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Sports;r:4000024910S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:4;cposy:10;bpos:1;pos:1;subsec:221;imgt:ss;g:745a2501-59a3-3d81-a9e7-3783b281fb94;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Sports;r:4000024910S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Craigslist|YCT:001000667|YCT:001000780" data-offnet="1" data-uuid="b3407979-8eae-3b31-a6df-34445ad667f4" data-type="article" data-cluster="98d73825-29a6-4547-b82b-125a8085f6a5"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://www.sbnation.com/lookit/2016/2/13/10983686/air-jordans-robbery-craigslist-arm-severed-brooklyn" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:98d73825-29a6-4547-b82b-125a8085f6a5;refcnt:2;imgt:ss;g:b3407979-8eae-3b31-a6df-34445ad667f4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000026600S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><img src="/sy/uu/api/res/1.2/mnlOgv1Q1yLLxRDzxzm7wA--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/f2f2e2915213e4913c3473ec062c998f.cf.jpg" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_us) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.sbnation.com/lookit/2016/2/13/10983686/air-jordans-robbery-craigslist-arm-severed-brooklyn" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:98d73825-29a6-4547-b82b-125a8085f6a5;refcnt:2;imgt:ss;g:b3407979-8eae-3b31-a6df-34445ad667f4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000026600S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><span>Teenager steals Air Jordans in Craigslist robbery and has his arm severed in horrific incident</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">A 17-year-old Brooklyn teenager is missing an arm, and a 39-year-old man is facing attempted murder charges. This is the result of a horrific scene in New York on Friday that unfolded from a seemingly routine Craigslist transaction. The 39-year-old man known only as &quot;Phil&quot; is a regular buyer and seller of sneakers on Craigslist. He agreed to a sale with the teenager and the pair met in the middle of the day on a busy street. It&#39;s here that things went wrong. Climbing into Phil&#39;s SUV, it&#39;s reported that the 17-year-old pulled a gun on the man, took the sneakers and walked off. Rather than calling the police, Phil took things into his own hands. He did a quick u-turn and ran over the teenager attempting</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">SB Nation</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.chicagotribune.com/news/nationworld/ct-brooklyn-robbery-teen-loses-arm-20160212-story.html" data-uuid="a4dd591c-428a-326e-8f0e-37577a87e546" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:5;cposy:12;bpos:1;pos:2;ss_cid:98d73825-29a6-4547-b82b-125a8085f6a5;refcnt:2;imgt:ss;g:a4dd591c-428a-326e-8f0e-37577a87e546;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:rhdln;elmt:ct;r:4000026600S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/EPAdJqzGiuOkuTMvPlubxg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/a9c13ec274ff52add6444f09a7122eea" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Police: Teen&apos;s arm torn off after robbery goes awry in Brooklyn</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Chicago Tribune</span></div></a>
            
            
            
                <a href="http://edition.cnn.com/2016/02/13/us/new-york-craigslist-robbery/index.html" data-uuid="089d34a4-a156-3670-ab73-87c925a1dd66" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:5;cposy:13;bpos:1;pos:3;ss_cid:98d73825-29a6-4547-b82b-125a8085f6a5;refcnt:2;imgt:ss;g:089d34a4-a156-3670-ab73-87c925a1dd66;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:rhdln;elmt:ct;r:4000026600S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/VI09VyeJT8kJvrOpx_aLVw--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/6d54de945352a985a6c7dae8a30be119" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Craigslist case: Teen robbery suspect run over, loses arm</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">CNN</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:98d73825-29a6-4547-b82b-125a8085f6a5;refcnt:2;imgt:ss;g:b3407979-8eae-3b31-a6df-34445ad667f4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000026600S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000288|YCT:001000780|YCT:001000290"  data-uuid="add46721-0eb2-3f87-8d8c-8a2e586b6446" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://finance.yahoo.com/news/scientists-behavior-men-more-attractive-155809450.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:6;cposy:14;bpos:1;pos:1;subsec:149;imgt:ss;g:add46721-0eb2-3f87-8d8c-8a2e586b6446;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Lifestyle;elm:img;elmt:ct;r:4000017090S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/uu/api/res/1.2/97VTPJLLtw3A.lChQI1zvQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/Scientists_say_this_behavior_can-899250a99ab7f04c08131bc6c6d1a570" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_lifestyle) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="lifestyle">Lifestyle</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://finance.yahoo.com/news/scientists-behavior-men-more-attractive-155809450.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:6;cposy:14;bpos:1;pos:1;subsec:149;imgt:ss;g:add46721-0eb2-3f87-8d8c-8a2e586b6446;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Lifestyle;elm:hdln;elmt:ct;r:4000017090S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Scientists say this behavior can make men more attractive to women</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">From being a talented musician to walking a dog, there are plenty of traits and behaviors that can amp up your sex appeal to women - especially if you&#39;re looking to land a long-term relationship. A growing body of research on romantic attraction has focused on the importance of altruism: Men who display helping behaviors are generally perceived as more attractive than those who don&#39;t. In a 2013 study on the topic, researchers had young, heterosexual men and women rate pictures of other men and women on how attractive they would be for short- and long-term relationships.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Business Insider</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="149"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">149</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:14;bpos:1;pos:1;subsec:149;imgt:ss;g:add46721-0eb2-3f87-8d8c-8a2e586b6446;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Lifestyle;r:4000017090S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:14;bpos:1;pos:1;subsec:149;imgt:ss;g:add46721-0eb2-3f87-8d8c-8a2e586b6446;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Lifestyle;r:4000017090S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Florida|WIKIID:Palm_Beach,_Florida|WIKIID:Florida_Atlantic_University|YCT:001000638|YCT:001000643|YCT:001000637|YCT:001000644"  data-uuid="c132b8b4-53b8-3f9f-b008-7b15d53270c4" data-type="article" data-cluster="cfb56e38-9021-46f8-a537-0673f3c2f72d"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://news.yahoo.com/tens-thousands-sharks-spotted-stones-164600314.html" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:7;cposy:15;bpos:1;pos:1;ss_cid:cfb56e38-9021-46f8-a537-0673f3c2f72d;refcnt:2;subsec:15;imgt:ss;g:c132b8b4-53b8-3f9f-b008-7b15d53270c4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000020750S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/nDlUoxS3Y0EJGmOAmwLscQ--/Zmk9c3RyaW07aD0xOTA7cHlvZmY9MDtxPTk1O3c9MTkwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/inside_edition/f644a48c837d5fce56ae9bc4fa868bad')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_us) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://news.yahoo.com/tens-thousands-sharks-spotted-stones-164600314.html" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:7;cposy:15;bpos:1;pos:1;ss_cid:cfb56e38-9021-46f8-a537-0673f3c2f72d;refcnt:2;subsec:15;imgt:ss;g:c132b8b4-53b8-3f9f-b008-7b15d53270c4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000020750S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Tens of Thousands of Sharks Spotted &#39;A Stone&#39;s Throw&#39; From Florida Beaches</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">The severe cold snap may have you planning your Florida getaway, but you might want to watch this video first. Researchers flying above the Sunshine State&#39;s Atlantic coast on Friday spotted a massive migration of black tip sharks swimming uncomfortably close to popular beaches.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Inside Edition</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://edition.cnn.com/2016/02/13/us/shark-migration-florida-irpt/index.html" data-uuid="33c40593-795a-35ee-8565-51bdd9b8ab80" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:7;cposy:16;bpos:1;pos:2;ss_cid:cfb56e38-9021-46f8-a537-0673f3c2f72d;refcnt:2;subsec:15;imgt:ss;g:33c40593-795a-35ee-8565-51bdd9b8ab80;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:rhdln;elmt:ct;r:4000020750S00001;ccode:mega_global_ranking_hlv2_up_based;t4:rel;"  data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/rlYh0tPNKiwHNxRQ5C.U7w--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/12bb3c4c7508d109dab0edaa3beea688')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Thousands of sharks teem off Florida beaches</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">CNN</span></div></a>
            
            
            
                <a href="http://www.palmbeachdailynews.com/news/lifestyles/environment/fau-group-thousands-of-sharks-near-town-beaches-bu/nqPTm/" data-uuid="0a105fb9-d25f-3dd0-a461-5fc81dc4d891" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:7;cposy:17;bpos:1;pos:3;ss_cid:cfb56e38-9021-46f8-a537-0673f3c2f72d;refcnt:2;subsec:15;imgt:ss;g:0a105fb9-d25f-3dd0-a461-5fc81dc4d891;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:rhdln;elmt:ct;r:4000020750S00001;ccode:mega_global_ranking_hlv2_up_based;t4:rel;"  data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/H3W5deGCfOwXsmGUiFuRlg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/e0cfe1bc3e4b18d55ea6bad6f21cc0e3')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Thousands of sharks near Palm Beach beaches, but attacks rare</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Palm Beach Daily News</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="15"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">15</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:15;bpos:1;pos:1;ss_cid:cfb56e38-9021-46f8-a537-0673f3c2f72d;refcnt:2;subsec:15;imgt:ss;g:c132b8b4-53b8-3f9f-b008-7b15d53270c4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000020750S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:15;bpos:1;pos:1;ss_cid:cfb56e38-9021-46f8-a537-0673f3c2f72d;refcnt:2;subsec:15;imgt:ss;g:c132b8b4-53b8-3f9f-b008-7b15d53270c4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000020750S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad  Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=zKsTOF0GIS8CC4wgdSjrOHL0HeHyG8o4.9eQwDansbLv57NjOWur_JY1HnlV6TcIaqcbehVox1z6Au4FimAFA_GdcNpjrXLA3fbmUfNeKCU.JYa7Lp2nmIEHiFT9orURJJlH6toa74C4w8041WHsiBlPi5r6hnJ2YjQFLMmaxtygTZh3Pijyn0Fcnhgusv5EhEPPLXsx54EmTvWI4woEcrIo09xQBq2FErPf1g7gwwrj0Tj7qzShjACeUAdpIA6C2SR5UrBxIy_vfcnPS_4T0kuinzoDFqsU3vaFp.kvDHJSK.9cTJkqjw8DKMmk8S35Zlzy8jXo.lJrM6KBLjaUJmoavfKcD6852pCt5mcq7PyAvZLyhEOYdN3geRtRAIqrhM7tEUI.yw--&amp;ap=9" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15oh0g837(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31710498501,dmn$startupf5.net,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$1167625,pbid$1,seid$4250754))&amp;r=1455471493151&amp;al=$(AD_FEEDBACK)" data-uuid="31710498501">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=wkiGczMGIS8hrKDlXH4gtynWt_eG9VqmCuQS6pzFx2eicavTiZ5pRzUQf7SlVKj5RQvMF3vu75xt2aXAzm1Z6IedYogZ_GBzlIuGdIwiSQJzBCxWN17IDRXkP1kKHUIyYx1QkdBtUmJZjWu6MbQ0ePSY6h.5lYB0FZi4RtxjHCVmJoZs0I4yhrDXwcdiXjSBsSQoxcKkMHOFIbxNlnr1axItJPKGDw9Ysh0k7Mi84aK5somrFCqrka97kHnncTj35IK0KHgMM_ZEsNLwu.7ZCqhdcAvuZB8dgl_mLH6pCNi2ri9EcXLGU.z8pmTPP.9CQAw8_yg6t487W8qKKfDiOhsVajia8D619A2di5rQYy.tvEOOMmgUPZL6zG.DAINow5joMf6UO6a9brIkTPKsabwCb.7aKG_DsERXKzIGMp_naFmeqAYnhnQjisWjU88OebJK0NNOjk.CA93M.N_pskWllq2Oia6V1hOKM4yQfI.s4EM.ZH_odS8nTepvbw9bIbk-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F0f0868f1-4986-4ec1-be9a-d18abdda27ef%3Fad%3D1" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i)" target="_blank"   data-ylk="cpos:8;cposy:18;bpos:1;pos:1;ad:1;elmt:ct;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/ZSVHW29l5RMiZGNnhIqfRQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454992791721-5955.jpg.cf.jpg')" alt="">
                </a>
            </div>
            
            <div class="strm-right Pos(r) Mstart(29%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:8;cposy:18;bpos:1;pos:1;ad:1;elmt:sp;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:8;cposy:18;bpos:1;pos:1;ad:1;elmt:info;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=wkiGczMGIS8hrKDlXH4gtynWt_eG9VqmCuQS6pzFx2eicavTiZ5pRzUQf7SlVKj5RQvMF3vu75xt2aXAzm1Z6IedYogZ_GBzlIuGdIwiSQJzBCxWN17IDRXkP1kKHUIyYx1QkdBtUmJZjWu6MbQ0ePSY6h.5lYB0FZi4RtxjHCVmJoZs0I4yhrDXwcdiXjSBsSQoxcKkMHOFIbxNlnr1axItJPKGDw9Ysh0k7Mi84aK5somrFCqrka97kHnncTj35IK0KHgMM_ZEsNLwu.7ZCqhdcAvuZB8dgl_mLH6pCNi2ri9EcXLGU.z8pmTPP.9CQAw8_yg6t487W8qKKfDiOhsVajia8D619A2di5rQYy.tvEOOMmgUPZL6zG.DAINow5joMf6UO6a9brIkTPKsabwCb.7aKG_DsERXKzIGMp_naFmeqAYnhnQjisWjU88OebJK0NNOjk.CA93M.N_pskWllq2Oia6V1hOKM4yQfI.s4EM.ZH_odS8nTepvbw9bIbk-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F0f0868f1-4986-4ec1-be9a-d18abdda27ef%3Fad%3D1" target="_blank"   data-ylk="cpos:8;cposy:18;bpos:1;pos:1;ad:1;elmt:ad;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class="Pos(r) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>N'achetez AUCUNE action avant d'avoir vu ceci !</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u>
                    </a>
                </h3>
                <div>
                    <p class="Pos(r) stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">Ce nouvel algorithme de trading vient d'Ãªtre rÃ©vÃ©lÃ© au public et est en train de secouer toute la France.</p>
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=wkiGczMGIS8hrKDlXH4gtynWt_eG9VqmCuQS6pzFx2eicavTiZ5pRzUQf7SlVKj5RQvMF3vu75xt2aXAzm1Z6IedYogZ_GBzlIuGdIwiSQJzBCxWN17IDRXkP1kKHUIyYx1QkdBtUmJZjWu6MbQ0ePSY6h.5lYB0FZi4RtxjHCVmJoZs0I4yhrDXwcdiXjSBsSQoxcKkMHOFIbxNlnr1axItJPKGDw9Ysh0k7Mi84aK5somrFCqrka97kHnncTj35IK0KHgMM_ZEsNLwu.7ZCqhdcAvuZB8dgl_mLH6pCNi2ri9EcXLGU.z8pmTPP.9CQAw8_yg6t487W8qKKfDiOhsVajia8D619A2di5rQYy.tvEOOMmgUPZL6zG.DAINow5joMf6UO6a9brIkTPKsabwCb.7aKG_DsERXKzIGMp_naFmeqAYnhnQjisWjU88OebJK0NNOjk.CA93M.N_pskWllq2Oia6V1hOKM4yQfI.s4EM.ZH_odS8nTepvbw9bIbk-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F0f0868f1-4986-4ec1-be9a-d18abdda27ef%3Fad%3D1" target="_blank"   data-ylk="cpos:8;cposy:18;bpos:1;pos:1;ad:1;elmt:ad;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Preditrend</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:8;cposy:18;bpos:1;pos:1;ad:1;elmt:op;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Democratic_Party_%28United_States%29|YCT:001000661|YCT:001000708"  data-uuid="0838f2f4-835d-33a4-86cd-1cb89df17015" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="https://www.yahoo.com/politics/democratic-and-republican-delegate-scorecard-164410211.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:9;cposy:19;bpos:1;pos:1;subsec:7;imgt:ss;g:0838f2f4-835d-33a4-86cd-1cb89df17015;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:img;elmt:ct;r:4000016039S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/7HXo10PbdVvK7KsbRtjpVA--/Zmk9c3RyaW07aD0xMDc7cHlvZmY9MDtxPTk1O3c9MTkwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en/homerun/feed_manager_auto_publish_494/a1729696ef72832d29c951094db9c529.cf.jpg')" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_politics) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="https://www.yahoo.com/politics/democratic-and-republican-delegate-scorecard-164410211.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:9;cposy:19;bpos:1;pos:1;subsec:7;imgt:ss;g:0838f2f4-835d-33a4-86cd-1cb89df17015;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000016039S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Democratic and Republican
Delegate scorecard</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">âSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Yahoo News Photo</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="7"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">7</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:19;bpos:1;pos:1;subsec:7;imgt:ss;g:0838f2f4-835d-33a4-86cd-1cb89df17015;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;r:4000016039S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:19;bpos:1;pos:1;subsec:7;imgt:ss;g:0838f2f4-835d-33a4-86cd-1cb89df17015;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;r:4000016039S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000798|YCT:001000655|YCT:001000780" data-offnet="1" data-uuid="3da8e026-42e6-3ce9-b483-f73847d71cba" data-type="article" data-cluster="b8a2fdf5-fe34-4825-ae29-c817e78a7113"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://www.cnn.com/2016/02/14/asia/new-zealand-christchurch-earthquake/index.html" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:10;cposy:20;bpos:1;pos:1;ss_cid:b8a2fdf5-fe34-4825-ae29-c817e78a7113;refcnt:2;imgt:ss;g:3da8e026-42e6-3ce9-b483-f73847d71cba;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:img;elmt:ct;r:4000016910S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/zbcW9GVh4fn8OsCzKAksjw--/Zmk9c3RyaW07aD0xOTA7cHlvZmY9MDtxPTk1O3c9MTkwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/4a697a4d9d5faf4673f712ab6273e20e')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_world) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="world">World</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.cnn.com/2016/02/14/asia/new-zealand-christchurch-earthquake/index.html" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:10;cposy:20;bpos:1;pos:1;ss_cid:b8a2fdf5-fe34-4825-ae29-c817e78a7113;refcnt:2;imgt:ss;g:3da8e026-42e6-3ce9-b483-f73847d71cba;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:hdln;elmt:ct;r:4000016910S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Cliff collapses after 5.8-magnitude earthquake strikes New Zealand</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Some narrowly escaped the collapse. &quot;I&#39;d taken five of my rookies to do some cliff work and some cliff jumping,&quot; Craig Jamison, a member of the Taylor&#39;s Mistake Surf Life Saving Club, told the affiliate. &quot;When I look up I see three of them standing in the cave, and then a whole sheet of rock fell down over them.&quot; But they were lucky; no one was seriously injured. Elsewhere near Christchurch, the quake hurled items off store shelves. Heavy sport-utility vehicles shook like gelatin. &quot;We&#39;ve definitely had a spike in calls shortly after the quake involving what we call fainting type events, chest pain, shortness of breath and falls,&quot; local ambulance manager Kerry Mitchell told TVNZ. While no casualties</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">CNN</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://news.yahoo.com/5-8-magnitude-quake-hits-zealand-city-usgs-011110344.html" data-uuid="6335f20b-0681-3373-8a22-78662b61e536" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:10;cposy:21;bpos:1;pos:2;ss_cid:b8a2fdf5-fe34-4825-ae29-c817e78a7113;refcnt:2;imgt:ss;g:6335f20b-0681-3373-8a22-78662b61e536;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:rhdln;elmt:ct;r:4000016910S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/UiTVWcin._KsGc0ZVxcS4g--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_us/News/afp.com/Part-HKG-Hkg6976264-1-1-0.jpg.cf.jpg')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">5.8-magnitude quake hits New Zealand city: USGS</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">AFP</span></div></a>
            
            
            
                <a href="https://www.washingtonpost.com/world/magnitude-58-quake-shakes-new-zealand-city-of-christchurch/2016/02/13/a4939fa4-d2ba-11e5-90d3-34c2c42653ac_story.html" data-uuid="542a76c0-2ff9-30df-bfe1-a6a748e6e4d0" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:10;cposy:22;bpos:1;pos:3;ss_cid:b8a2fdf5-fe34-4825-ae29-c817e78a7113;refcnt:2;imgt:ss;g:542a76c0-2ff9-30df-bfe1-a6a748e6e4d0;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:rhdln;elmt:ct;r:4000016910S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/7.Bkfxrr5f7lQNIsdLhCOA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/a43044bc46f694a7c3d0a9f01e7e2eaa')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Magnitude-5.8 quake shakes New Zealand city of Christchurch</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Washington Post</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:10;cposy:20;bpos:1;pos:1;ss_cid:b8a2fdf5-fe34-4825-ae29-c817e78a7113;refcnt:2;imgt:ss;g:3da8e026-42e6-3ce9-b483-f73847d71cba;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;r:4000016910S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Ryugyong_Hotel|WIKIID:North_Korea|WIKIID:Kim_Il-sung|YCT:001000661" data-offnet="1" data-uuid="fc5ae616-6476-382f-bf58-3fc74de0b3fd" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://www.thedailybeast.com/articles/2016/02/14/north-korea-s-best-building-is-empty-the-mystery-of-the-ryugyong-hotel.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:11;cposy:23;bpos:1;pos:1;imgt:ss;g:fc5ae616-6476-382f-bf58-3fc74de0b3fd;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:img;elmt:ct;r:4000017010S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/oGcq4CF8kW4YWEWQg5pnpw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/a7c07974e19cece568d783da391afd04')" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_politics) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://www.thedailybeast.com/articles/2016/02/14/north-korea-s-best-building-is-empty-the-mystery-of-the-ryugyong-hotel.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:11;cposy:23;bpos:1;pos:1;imgt:ss;g:fc5ae616-6476-382f-bf58-3fc74de0b3fd;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000017010S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>North Koreaâs Best Building Is Empty: The Mystery of the Ryugyong Hotel</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">There are unfinished buildings all over the world, but the most mysterious-by far-is the Ryugyong Hotel in Pyongyang, the capital of North Korea. Towering over the rest of the mid-rise city at more than 1,000 feet, the 105-story pyramid-shaped building with the ballpoint-pen top remains off-limits to the public, despite decades of construction and an estimated cost in the hundreds of millions of dollars. What itâs like on the inside-like much of the cloistered country-is unknown to all but the few who&#39;ve either snuck in or taken what seems to be the single tour of the interior, sanctioned by the government in 2012. What itâs like on the outside, though, is clear to nearly everyone in Pyongyang,</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">The Daily Beast</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:11;cposy:23;bpos:1;pos:1;imgt:ss;g:fc5ae616-6476-382f-bf58-3fc74de0b3fd;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;r:4000017010S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    


    
    </ul>
</div>

<img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="16" height="16" class="D(b) Mx(a) My(12px) js-stream-load-more ImageLoader" style="background-image:url('https://s.yimg.com/zz/nn/lib/metro/g/my/anim_loading_sm.gif')" alt="">


 </div> </div> </div>  <div class="App-Ft Row"> </div>            <!-- App close -->
            </div>
                </div>
            </div>
            <div class="Col3" id="Aside" role="complementary" tabindex="-1">
                <div class="Col3-stack" data-sticker-top="75px" >
                                <div id="applet_p_32209491" class=" M-0 js-applet trending Zoom-1  Mb(30px) " data-applet-guid="p_32209491" data-applet-type="trending" data-applet-params="_suid:32209491" data-i13n="auto:true;sec:tc-ts" data-i13n-sec="tc-ts"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-trending" class="slingstone-selected"><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) gifts-selected_C($disabledHeading) Trs($trendTrs)" data-category="slingstone">Trending Now</h2><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) Pos(r) slingstone-selected_C($disabledHeading) Trs($trendTrs)" data-category="gifts">
                <i class="Icon-Fp2 IconTumblrHeart gifts-selected_C($giftsIcon) Fz(17px) Fw(100) Pos(a) Start(-19px) T(-2px)"></i>Valentine's Day</h2><ul class="Pos(r) Mt(10px)">
        <li class="trending-list" data-category="slingstone">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) gifts-selected_Op(0) gifts-selected_V(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Viola+Beach&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Viola Beach"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:1;bpos:1;ccode:trending_search;g:135f901d-f595-3882-9cd0-60f3e29987bf;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Viola Beach</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Ashley+Graham&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Ashley Graham"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:2;bpos:1;ccode:trending_search;g:d1a5c0b8-dad7-3731-baec-fadfa27c8a44;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Ashley Graham</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Ruth+Ginsburg&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Ruth Ginsburg"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:3;bpos:1;ccode:trending_search;g:7e0d8a6e-1279-3a9c-b0e5-23bcc5c61b94;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Ruth Ginsburg</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Sri+Srinivasan&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Sri Srinivasan"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:4;bpos:1;ccode:trending_search;g:98d0a643-e35a-3b99-8134-2a988fbbccda;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Sri Srinivasan</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Valentine%27s+ideas&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Valentine's ideas"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:5;bpos:1;ccode:trending_search;g:8dce1463-6aef-3630-a830-9a5994a1b426;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Valentine's ideas</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Subaru+Crosstrek&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Subaru Crosstrek"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:6;bpos:1;ccode:trending_search;g:9f6310fa-c0d5-363e-a9ca-7987feeb3bbf;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Subaru Crosstrek</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Aaron+Gordon&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Aaron Gordon"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:7;bpos:1;ccode:trending_search;g:6b10ea18-eced-3050-b06b-78bc46c65f33;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Aaron Gordon</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Barack+Obama&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Barack Obama"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:8;bpos:1;ccode:trending_search;g:68de2572-3cbf-31b8-94e6-75d8efb160e5;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Barack Obama</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Dementia&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Dementia"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:9;bpos:1;ccode:trending_search;g:6eb49acf-fdd5-3b0f-ac4f-83e59ffffe6b;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Dementia</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Maureen+Scalia&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Maureen Scalia"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:10;bpos:1;ccode:trending_search;g:bc0c16a4-be2f-3d3d-8447-deb66605b142;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Maureen Scalia</span>
                    </a>
                </li></ul>
        </li><li class="trending-list Pos(a) T(0) W(100%)" data-category="gifts">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) slingstone-selected_Op(0) slingstone-selected_V(h) slingstone-selected_H(0) Ov(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Same-day+flower+delivery&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Same-day flower delivery"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:1;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Same-day flower delivery</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Valentine%27s+baskets&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Valentine's baskets"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:2;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Valentine's baskets</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Online+gift+cards&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Online gift cards"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:3;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Online gift cards</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Breakfast+recipes&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Breakfast recipes"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:4;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Breakfast recipes</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Last-minute+hotel&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Last-minute hotel"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:5;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Last-minute hotel</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Wine-of-the-month+club&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Wine-of-the-month club"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:6;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Wine-of-the-month club</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Canon+camera&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Canon camera"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:7;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Canon camera</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Pillar+candles&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Pillar candles"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:8;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Pillar candles</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Gold+earrings&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Gold earrings"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:9;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Gold earrings</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=NBA+tickets&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="NBA tickets"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:10;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> NBA tickets</span>
                        </a>
                    </li></ul>
        </li></ul>
</div>
 </div> </div> </div>            <!-- App close -->
            </div>                            <div id="my-adsFPAD-base">
                    <div id="my-adsFPAD" class="D-n sda-DAPF">
                        <script>
                            rtAdStart = window.performance && window.performance.now && window.performance.now();
                        </script>
                        <div class="Mx-a" id="my-adsFPAD-iframe">
                            <!-- FPAD has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136h20o12(gid%241jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st%241455471493103052,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125fijjct,aid%24grhYUWKKamo-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=FPAD)"></noscript>
                        </div>
                        <script>
                            rtAdDone = window.performance && window.performance.now && window.performance.now();
                        </script>
                    </div>
                </div>                           <div id="my-adsLREC-base" class="lrec-bgcolor">
                   <div id="my-adsLREC" class="Ta-c Pos-r Z-1 Ht-250 Mb-20">
                        <div id="my-adsLREC-iframe">
                            <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<!--QYZ 2264184051,4467049051,;;LREC;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136h20o12(gid%241jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st%241455471493103052,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413av6mnf8,aid%24EMBYUWKKamo-,bi%242264184051,agp%243459460551,cr%244467049051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=LREC)"></noscript>
                        </div>
                    </div>
                </div>            <div id="applet_p_63794" class=" App_v2  M-0 js-applet weather Zoom-1  Mb(30px) " data-applet-guid="p_63794" data-applet-type="weather" data-applet-params="_suid:63794" data-i13n="auto:true;sec:app-wea" data-i13n-sec="app-wea"> <!-- App open -->
        <div class="App-Hd" data-region="header"> <div class="js-applet-view-container-header"> <div class="StencilRoot">
    
        <a href="https://weather.yahoo.com" class="C(#000) C($m_blue):h Td(n)">
            <h2 class="Fz(15px) Fw(b) Mb(18px) D(ib) Grid-U App-Title js-show-panel">
                <span class="js-city-name">
                    
                        
                            Yvrac, 33
                        
                    
                </span>
            </h2>
        </a>
    
    
    <div class="Grid-U">
        <div class="LocationPanel Pos(r) Fl(end) Z(3)"></div>
    </div>
    
</div>
 </div> </div>  <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div class="D(ib)">
    
    <ul class="P(0) Mt(0) Mend(0) Cl(b) Td(n) Mb(10px) Mstart(-2px) forecasts forecasts-12597132">
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Today</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Showers" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">56&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">46&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Sun</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png" width="43" height="43" alt="Showers" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">52&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">39&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Mon</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/partly_cloudy_day.png" width="43" height="43" alt="Showers" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">46&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">35&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) ">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Tue</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/clear_day.png" width="43" height="43" alt="Showers" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">46&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">30&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
    </ul>
    
</div>
 </div> </div> </div>            <!-- App close -->
            </div>            <div id="applet_p_50000171" class=" M-0 js-applet activitylist Zoom-1  Mb-20 " data-applet-guid="p_50000171" data-applet-type="activitylist" data-applet-params="_suid:50000171" data-i13n="auto:true;sec:ltst" data-i13n-sec="ltst"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  
<ul class="Mb(0) P(0) js-activitylist-list">

<li class="js-activitylist-item My(20px)" data-id="139295752069">
    
    <a href="https://www.yahoo.com/celebrity/sports-illustrated-unveils-3-swimsuit-issue-045726283.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/Gkrcn4vnuaCRKHC_7S.2Rw--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160214/rousey.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Celebrity</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Sports Illustrated unveils 3 swimsuit issue covers</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item My(20px)" data-id="139295291204">
    
    <a href="https://www.yahoo.com/tv/best-bachelor-moment-brad-chooses-no-one-133418739.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/a8AVXxBRvxq_DX.dFh0EeA--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160214/bach.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo TV</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">No. 1 âBachelorâ moment: Brad Womackâs double rejection</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item Mt(20px)" data-id="139294239049">
    
    <a href="http://sports.yahoo.com/blogs/nba-ball-dont-lie/zach-lavine-tops-aaron-gordon-in-a-slam-dunk-contest-for-the-ages-050805327.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/jy0mVn86lAyTD4GrvQMbfw--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160214/zach.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Sports</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Zach LaVine tops Aaron Gordon in epic slam-dunk contest</span>
    </span>
    </a>
    
</li>

</ul>

 </div> </div> </div>            <!-- App close -->
            </div>                           <div id="sticky-lrec2-footer" data-plugin="sticker" data-sticker-top="110px" data-sticker-forceposition="top">
               <div class="lrec-bgcolor">
               <div id="my-adsLREC2" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 darla" data-autorotate="1" data-autoeventrt="15000" data-config={"pos":"LREC2","id":"LREC2","clean":"my-adsLREC2","dest":"my-adsLREC2-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}} data-lrec3replace="1" >
                   <div class='Mx-a Ta-c' id="my-adsLREC2-iframe">
                        
                   </div>
               </div>
                               <div id="my-adsLREC3" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 darla D-n" data-config={"pos":"LREC3","id":"LREC3","clean":"my-adsLREC3","dest":"my-adsLREC3-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}}>
                    <div class='Mx-a Ta-c' id="my-adsLREC3-iframe">
                    </div>
               </div>
               <div id="my-adsLREC2-fallback" class="D-n">
                    <a href="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}" style="background:url(https://www.yahoo.com/sy/dh/ap/default/160201/sports-dailyfantasy-v2-a966da5__1_.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;"></a>
               </div>
               </div>
                    <div id="Footer" class="Row Pstart-20 Pend-10 Pos-r" role="contentinfo">
                         <ul class="Bdrs(3px) Px(20px) Py(14px) Ta(c)" role="contentinfo" id="fp-footer"><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/legal/us/yahoo/utos/terms/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Terms</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Privacy</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://advertising.yahoo.com/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Advertise</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/relevantads.html" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">About our Ads</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://careers.yahoo.com/us" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Careers</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://help.yahoo.com/l/us/yahoo/helpcentral/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Help</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://yahoo.uservoice.com/forums/341361-yahoo-home" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Feedback</a></li></ul></div>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>





        
                        <div id="darla-assets-bottom">
                    <script type="text/x-safeframe" id="fc" _ver="2-9-4">{ "positions": [ { "html": "<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->", "id": "FPAD", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['grhYUWKKamo-']='(as$125fijjct,aid$grhYUWKKamo-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125fijjct,aid$grhYUWKKamo-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125fijjct,aid$grhYUWKKamo-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1.4554714931031E+15", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "0", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": "{\"pt\":\"0\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"pvid\":\"1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=146750051&megamodal=${MEGAMODAL}&bucket=201&asz=300x250&u=https://www.yahoo.com&gdAdId=EMBYUWKKamo-&gdUuid=1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M&gdSt=1455471493103052&publisher_blob=${RS}|1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M|2023538075|LREC|1455471493.50508|2-9-4:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aWE0azRtYihnaWQkMWpEeDNEazRMakUzNXk4MTJibDJPUTdPTWpBd01WYkF1NFVSQ3Y4TSxzdCQxNDU1NDcxNDkzMTAzMDUyLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkSXBQX3dXRXRYZFlZcXBBQklIYkdRdyxsbmckZW4tdXMsY3IkNDQ2NzA0OTA1MSx2JDIuMCxhaWQkRU1CWVVXS0thbW8tLGJpJDIyNjQxODQwNTEsbW1lJDk1NDk0MzI2NTI4NjAyMjMxMDksciQwLHlvbyQxLGFncCQzNDU5NDYwNTUxLGFwJExSRUMpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2264184051,4467049051,;;LREC;2023538075;1-->", "id": "LREC", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['EMBYUWKKamo-']='(as$13av6mnf8,aid$EMBYUWKKamo-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13av6mnf8,aid$EMBYUWKKamo-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13av6mnf8,aid$EMBYUWKKamo-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)", "impID": "EMBYUWKKamo-", "supp_ugc": "0", "placementID": "3459460551", "creativeID": "4467049051", "serveTime": "1455471493103052", "behavior": "non_exp", "adID": "9549432652860223109", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "300x250", "bookID": "2264184051", "serveType": "-1", "slotID": "1", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(160uphlih(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,srv$1,si$4452051,ct$25,exp$1455478693103052,adv$26513753608,li$3458215051,cr$4467049051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1455478693103\", \"fdb_intl\": \"en-US\" }", "slotData": "{\"pt\":\"8\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"adjf\":\"1.000000\",\"alpha\":\"-1.000000\",\"ffrac\":\"0.999893\",\"pcpm\":\"-1.000000\",\"fc\":\"false\",\"sdate\":\"1442339887\",\"edate\":\"1498881599\",\"bimpr\":399999991808.000000,\"pimpr\":0.000000,\"spltp\":0.000000,\"frp\":\"false\",\"pvid\":\"1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "1", "userProvidedData": {} } } },{ "html": "<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->", "id": "MAST", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['nsdYUWKKamo-']='(as$12543bmvc,aid$nsdYUWKKamo-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$12543bmvc,aid$nsdYUWKKamo-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$12543bmvc,aid$nsdYUWKKamo-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1455471493103052", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "2", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": "{\"pt\":\"0\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"pvid\":\"1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<style>\n	#fc_align{\n		position: static !important;\n		width: 120px !important;\n		margin: auto !important;\n	}\n</style>\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=251157193&asz=120x60&u=https://www.yahoo.com&gdAdId=LM9YUWKKamo-&gdUuid=1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M&gdSt=1455471493103052&publisher_blob=${RS}|1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M|2023538075|TXTL|1455471493.50656|2-9-4:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aXZyY3RldChnaWQkMWpEeDNEazRMakUzNXk4MTJibDJPUTdPTWpBd01WYkF1NFVSQ3Y4TSxzdCQxNDU1NDcxNDkzMTAzMDUyLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkSXBQX3dXRXRYZFlZcXBBQklIYkdRdyxsbmckZW4tdXMsY3IkNDQ1MTAwOTA1MSx2JDIuMCxhaWQkTE05WVVXS0thbW8tLGJpJDIyNzUxODgwNTEsbW1lJDk1OTMzMjA3NzYxNzQzNDk0MDgsciQwLHlvbyQxLGFncCQzNDc2NTAyNTUxLGFwJFRYVEwpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->", "id": "TXTL", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['LM9YUWKKamo-']='(as$13aar03r1,aid$LM9YUWKKamo-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13aar03r1,aid$LM9YUWKKamo-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13aar03r1,aid$LM9YUWKKamo-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)", "impID": "LM9YUWKKamo-", "supp_ugc": "0", "placementID": "3476502551", "creativeID": "4451009051", "serveTime": "1455471493103052", "behavior": "non_exp", "adID": "9593320776174349408", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "120x45", "bookID": "2275188051", "serveType": "-1", "slotID": "3", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(160djeacr(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,srv$1,si$4452051,ct$25,exp$1455478693103052,adv$26679945979,li$3474892551,cr$4451009051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1455478693103\", \"fdb_intl\": \"en-US\" }", "slotData": "{\"pt\":\"3\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"adjf\":\"1.000000\",\"alpha\":\"1.000000\",\"ffrac\":\"1.000000\",\"pcpm\":\"-1.000000\",\"fc\":\"false\",\"ecpm\":0.000000,\"sdate\":\"1446744408\",\"edate\":\"1514782799\",\"bimpr\":0.000000,\"pimpr\":-390432480.000000,\"spltp\":100.000000,\"frp\":\"false\",\"pvid\":\"1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } } ], "meta": { "y": { "pageEndHTML": "<scr"+"ipt language=javascr"+"ipt>\n(function(){window.xzq_p=function(R){M=R};window.xzq_svr=function(R){J=R};function F(S){var T=document;if(T.xzq_i==null){T.xzq_i=new Array();T.xzq_i.c=0}var R=T.xzq_i;R[++R.c]=new Image();R[R.c].src=S}window.xzq_sr=function(){var S=window;var Y=S.xzq_d;if(Y==null){return }if(J==null){return }var T=J+M;if(T.length>P){C();return }var X=\"\";var U=0;var W=Math.random();var V=(Y.hasOwnProperty!=null);var R;for(R in Y){if(typeof Y[R]==\"string\"){if(V&&!Y.hasOwnProperty(R)){continue}if(T.length+X.length+Y[R].length<=P){X+=Y[R]}else{if(T.length+Y[R].length>P){}else{U++;N(T,X,U,W);X=Y[R]}}}}if(U){U++}N(T,X,U,W);C()};function N(R,U,S,T){if(U.length>0){R+=\"&al=\"}F(R+U+\"&s=\"+S+\"&r=\"+T)}function C(){window.xzq_d=null;M=null;J=null}function K(R){xzq_sr()}function B(R){xzq_sr()}function L(U,V,W){if(W){var R=W.toString();var T=U;var Y=R.match(new RegExp(\"\\\\\\\\(([^\\\\\\\\)]*)\\\\\\\\)\"));Y=(Y[1].length>0?Y[1]:\"e\");T=T.replace(new RegExp(\"\\\\\\\\([^\\\\\\\\)]*\\\\\\\\)\",\"g\"),\"(\"+Y+\")\");if(R.indexOf(T)<0){var X=R.indexOf(\"{\");if(X>0){R=R.substring(X,R.length)}else{return W}R=R.replace(new RegExp(\"([^a-zA-Z0-9$_])this([^a-zA-Z0-9$_])\",\"g\"),\"$1xzq_this$2\");var Z=T+\";var rv = f( \"+Y+\",this);\";var S=\"{var a0 = '\"+Y+\"';var ofb = '\"+escape(R)+\"' ;var f = new Function( a0, 'xzq_this', unescape(ofb));\"+Z+\"return rv;}\";return new Function(Y,S)}else{return W}}return V}window.xzq_eh=function(){if(E||I){this.onload=L(\"xzq_onload(e)\",K,this.onload,0);if(E&&typeof (this.onbeforeunload)!=O){this.onbeforeunload=L(\"xzq_dobeforeunload(e)\",B,this.onbeforeunload,0)}}};window.xzq_s=function(){setTimeout(\"xzq_sr()\",1)};var J=null;var M=null;var Q=navigator.appName;var H=navigator.appVersion;var G=navigator.userAgent;var A=parseInt(H);var D=Q.indexOf(\"Microsoft\");var E=D!=-1&&A>=4;var I=(Q.indexOf(\"Netscape\")!=-1||Q.indexOf(\"Opera\")!=-1)&&A>=4;var O=\"undefined\";var P=2000})();\n</scr"+"ipt><scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_svr)xzq_svr('https://csc.beap.bc.yahoo.com/');\nif(window.xzq_p)xzq_p('yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3');\nif(window.xzq_s)xzq_s();\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136h20o12(gid$1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M,st$1455471493103052,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3\"></noscr"+"ipt>", "pos_list": [ "FPAD","LREC","MAST","TXTL" ], "filtered": [  ], "spaceID": "2023538075", "host": "www.yahoo.com", "lookupTime": "230", "k2_uri": "", "fac_rt": "-1", "serveTime":"1455471493103052", "pvid": "1jDx3Dk4LjE35y812bl2OQ7OMjAwMVbAu4URCv8M", "tID": "darla_prefetch_1455471493102_2135232318_1", "npv": "0", "ep": "{\"site-attribute\":\"fdn=1 Y-BUCKET=\\\"201\\\"\",\"secure\":true,\"lang\":\"en-US\",\"ref\":\"https:\\/\\/www.yahoo.com\",\"filter\":\"no_expandable;\",\"darlaID\":\"darla_instance_1455471493101_849013219_0\"}", "pe": "CWZ1bmN0aW9uIGRwZWQoKSB7IAoJaWYod2luZG93Lnh6cV9kPT1udWxsKXdpbmRvdy54enFfZD1uZXcgT2JqZWN0KCk7CndpbmRvdy54enFfZFsnZ3JoWVVXS0thbW8tJ109JyhhcyQxMjVmaWpqY3QsYWlkJGdyaFlVV0tLYW1vLSxjciQtMSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1GUEFEKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydFTUJZVVdLS2Ftby0nXT0nKGFzJDEzYXY2bW5mOCxhaWQkRU1CWVVXS0thbW8tLGJpJDIyNjQxODQwNTEsYWdwJDM0NTk0NjA1NTEsY3IkNDQ2NzA0OTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1MUkVDKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWyduc2RZVVdLS2Ftby0nXT0nKGFzJDEyNTQzYm12YyxhaWQkbnNkWVVXS0thbW8tLGNyJC0xLGN0JDI1LGF0JEgsZW9iJGdkMV9tYXRjaF9pZD0tMTp5cG9zPU1BU1QpJztpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydMTTlZVVdLS2Ftby0nXT0nKGFzJDEzYWFyMDNyMSxhaWQkTE05WVVXS0thbW8tLGJpJDIyNzUxODgwNTEsYWdwJDM0NzY1MDI1NTEsY3IkNDQ1MTAwOTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1UWFRMKSc7CgkJIH07CmRwZWQudHJhbnNJRCA9ICJkYXJsYV9wcmVmZXRjaF8xNDU1NDcxNDkzMTAyXzIxMzUyMzIzMThfMSI7CgoJZnVuY3Rpb24gZHBlcigpIHsgCgkKaWYod2luZG93Lnh6cV9zdnIpeHpxX3N2cignaHR0cHM6Ly9jc2MuYmVhcC5iYy55YWhvby5jb20vJyk7CmlmKHdpbmRvdy54enFfcCl4enFfcCgneWk/YnY9MS4wLjAmYnM9KDEzNmgyMG8xMihnaWQkMWpEeDNEazRMakUzNXk4MTJibDJPUTdPTWpBd01WYkF1NFVSQ3Y4TSxzdCQxNDU1NDcxNDkzMTAzMDUyLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxwdiQxLHYkMi4wKSkmdD1KXzMtRF8zJyk7CmlmKHdpbmRvdy54enFfcyl4enFfcygpOwoKCgkKIH07CmRwZXIudHJhbnNJRCA9ImRhcmxhX3ByZWZldGNoXzE0NTU0NzE0OTMxMDJfMjEzNTIzMjMxOF8xIjsKCg==", "pym": "" } } } </script>    <script type="text/javascript">
        var pageloadValidAds = ["TXTL","LREC"];
        var mastRotationEnabled = 0;
        var bucketSAEnabled = true;
        var w = window, D = w.DARLA,
            C = {"useYAC":0,"usePE":0,"servicePath":"https:\/\/www.yahoo.com\/sdarla\/php\/fc.php","xservicePath":"","beaconPath":"https:\/\/www.yahoo.com\/sdarla\/php\/b.php","renderPath":"","allowFiF":false,"srenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","renderFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","sfbrenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","msgPath":"https:\/\/www.yahoo.com\/sdarla\/2-9-4\/html\/msg.html","cscPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-csc.html","root":"sdarla","edgeRoot":"http:\/\/l.yimg.com\/rq\/darla\/2-9-4","sedgeRoot":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4","version":"2-9-4","tpbURI":"","hostFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/js\/g-r-min.js","beaconsDisabled":true,"rotationTimingDisabled":true,"fdb_locale":"What don't you like about this ad?|It's offensive|Something else|Thank you for helping us improve your Yahoo experience|It's not relevant|It's distracting|I don't like this ad|Send|Done|Why do I see ads?|Learn more about your feedback.","positions":{"DEFAULT":{"supports":false},"FPAD":[],"LREC":{"w":300,"h":250},"MAST":[],"TXTL":{"w":120,"h":45}},"lang":"en-US"},
            _adPerfBeaconData,
            _pendingAds = [],
            _adLT = [];
        var safeframeOptinPositions = {"FPAD":true};
                window._navStart = function _navStart () {
            if (window.performance) {
                if (window.performance.clearMeasures) {
                    try {
                        window.performance.clearMeasures();
                    } catch (ex) {
                    }
                }
                if (window.performance.clearMarks) {
                    try {
                        window.performance.clearMarks();
                    } catch (ex) {
                    }
                }
                _perfMark('MODAL_OPEN');
            }
            window._pendingAds = [];
            window._perfBackfillAdBeacon = false;
            window._adPerfBeaconData = {positions:{}};
        };
        window._adCallStart = function _adCallStart () {
            window._perfMark('MODAL_AD_EVENT_START');
        };
        window._isModalOpen = function _isModalOpen () {
            return (document.getElementsByTagName('html')[0].className.match(/Reader-open/) && !document.getElementsByTagName('html')[0].className.match(/Reader-closed/));
        };
        window._perfMark = function _perfMark (name) {
            if (window.performance && window.performance.mark) {
                window.performance.mark(name);
            }
        };
        window._perfBackfillAd = function _perfBackfillAd() {
                for (var i in window._adPerfBeaconData.positions) {
                    if (window._adPerfBeaconData.positions.hasOwnProperty(i) && i.match(/LREC-/)) {
                        window._perfMark('PSTMSG_' + i);
                        window._adPerfBeaconData.positions[i].backfill = true;
                    } else {
                        window._adPerfBeaconData.positions[i].backfill = false;
                    }
                }
        };
        window._backfillVideoStart = function _backfillVideoStart() {
            window._perfMark('PSTMSG_VIDSTART');
        };
        window._perfModalFetchStart = function _perfModalFetchStart() {
            window._perfMark('MODAL_FETCH_START');
        };
        window._perfModalFetchEnd = function _perfModalFetchEnd() {
            window._perfMark('MODAL_FETCH_END');
        };
        window._backfillVideoPlaybackStart = function _backfillVideoPlaybackStart(isAd) {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfBackfillAdBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfBackfillAdBeacon = true;
                window._perfMark('PSTMSG_PBSTART');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                videoInit = Math.round(window.performance.getEntriesByName('PSTMSG_VIDSTART')[0].startTime - modalStart);
                playbackStart = Math.round(window.performance.getEntriesByName('PSTMSG_PBSTART')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'brightroll_backfill_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            videoInit: videoInit,
                            videoAd: isAd,
                            playbackStart: playbackStart
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._adRenderComplete = function _adRenderComplete() {
            if (window.performance && window.performance.getEntriesByName) {
                var modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0];
                var modalFetchStart = window.performance.getEntriesByName('MODAL_FETCH_START');
                var modalFetchEnd = window.performance.getEntriesByName('MODAL_FETCH_END');
                var rapidCustomData = [];
                var darlaFetchPerf= {};
                var rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                var modalStartTime = modalStart.startTime;

                darlaFetchPerf['name'] = 'darla_info';
                darlaFetchPerf['spaceid'] = window._adPerfBeaconData.spaceId;
                darlaFetchPerf['pvid'] = window._adPerfBeaconData.pvid;
                darlaFetchPerf['site'] = window._adPerfBeaconData.property || 'fp';
                darlaFetchPerf['lang'] = window.Af.context.lang;
                darlaFetchPerf['region'] =  window.Af.context.region;
                darlaFetchPerf['device'] =  window.Af.context.device;
                darlaFetchPerf['rid'] =  window.Af.context.rid;
                darlaFetchPerf['bucket'] = '201';

                if (modalFetchStart && modalFetchStart.length > 0) {
                    darlaFetchPerf['modalFetchStart'] =  Math.round(modalFetchStart[0].startTime -modalStartTime);
                    darlaFetchPerf['modalFetchEnd'] =  Math.round(modalFetchEnd[0].startTime -modalStartTime);
                }
                darlaFetchPerf['eventStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_EVENT_START')[0].startTime -modalStartTime);
                darlaFetchPerf['wsStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_START')[0].startTime - modalStartTime);
                darlaFetchPerf['wsEnd'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_END')[0].startTime - modalStartTime);
                rapidCustomData.push(darlaFetchPerf);

                for (var i in window._adPerfBeaconData.positions) {
                    var darlaPositionPerf = {};
                    var posInfo = window._adPerfBeaconData.positions.hasOwnProperty(i) && window._adPerfBeaconData.positions[i];
                    if(posInfo) {
                        darlaPositionPerf['name'] = i + '_info';
                        darlaPositionPerf['valid'] = posInfo.valid ? 1 : 0;
                        if (posInfo.valid) {
                            darlaPositionPerf['renderStart'] =  Math.round(window.performance.getEntriesByName('ADSTART_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['renderEnd'] =  Math.round(window.performance.getEntriesByName('ADEND_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['bookId'] = posInfo.bookId;
                            darlaPositionPerf['creativeId'] = posInfo.creativeId;
                            darlaPositionPerf['placementId'] = posInfo.placementId;
                            darlaPositionPerf['size'] = posInfo.size;
                            darlaPositionPerf['backfill'] = posInfo.backfill ? 1 : 0;
                            if (posInfo.backfill) {
                                darlaPositionPerf['pstmsg'] = Math.round(window.performance.getEntriesByName('PSTMSG_' + i)[0].startTime - modalStartTime);
                            }
                        }
                        rapidCustomData.push(darlaPositionPerf);
                    }
                }

                if (rapidInstance) {
                    var customPerfData = {'utm': rapidCustomData};
                    var rapidPerfData = {perf_usertime: customPerfData};
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        if (D && C) {
            C.positions = {"FPAD":{"clean":"my-adsFPAD","dest":"my-adsFPAD-iframe","metaSize":1,"fdb":"true,","supports":{"resize-to":1,"exp-ovr":1,"exp-push":1,"lyr":1}},"LREC":{"pos":"LREC","clean":"my-adsLREC","dest":"my-adsLREC-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":0,"lyr":0}},"MAST":{"pos":"MAST","clean":"my-adsMAST","dest":"my-adsMAST-iframe","fr":"expIfr_exp","rmxp":0,"metaSize":true,"w":970,"h":250,"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"closeBtn":{"mode":2,"useShow":1}},"TXTL":{"pos":"TXTL","id":"TXTL","clean":"my-adsTXTL","dest":"my-adsTXTL-iframe","w":120,"h":170,"fr":"expIfr_exp","rmxp":0,"css":".ad-tl2b {overflow:hidden; text-align:left;} p {margin:0px;} a {color:#020e65;} a:hover {color:#0078ff;} .y-fp-pg-controls {margin-top:5px; margin-bottom:5px;} #tl1_slug {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px; color:#999;} #fc_align a {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px;} a:link {text-decoration:none;}"}};
            C.k2={"res":{"rate":100,"pos":["LREC","MAST","FPAD","LREC2","LREC3","LREC-0","LREC2-0","LREC3-0","MAST-0","LDRB-0","SPL2-0","SPL-0","MON-0"]}};
            C.k2E2ERate=100;
C.k2Rate=100;
C.so=1;

            C.events = null;
            
                    C.onStartRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_START');
            }
        };
                    C.onFinishRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_END');
            }
        };
                        C.onFinishParse = function(eventName, result) {
                var ps = result.ps(),
                    modalOpen = false,
                    position, posItem, curAd, curEvt,
                    validPositions = {},
                    isMONFetch = false;
                            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('AD_PARSE');
                modalOpen = true;
                window._adPerfBeaconData.spaceId = result.spaceID;
                window._adPerfBeaconData.pvid = result.pvid;
                curEvt = DARLA.evtSettings(eventName);
                if (curEvt.property) {
                    window._adPerfBeaconData.property = curEvt.property;
                }
            }
                        function fireBeacon(position, size, pageMode) {
            if (position && size && pageMode) {
                try {
                    var img = new Image();
                    img.src = '/p.gif?beaconType=darla&pos=' + position + '&size=' + size + '&mode=' +pageMode + '&bucket=' + 201 + '&rid=' + "3p231htbc1es4";
                } catch (e) {
                    if (console) {
                        console.log('failed to send darla beacon :', e);
                    }
                }
            }
        }

                if (ps && ps.length) {
                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
            for (i = 0, l = ps.length; i < l; i++) {
                position = ps[i];
                posItem = result.item(position);
                if (posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    validPositions[position] = false;
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = false;
                    }
                } else {
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = true;
                    }
                    validPositions[position] = true;
                }
            }
            if (YMedia && YMedia.Af && YMedia.Af.Event && YMedia.Af.Event.fire) {
                YMedia.Af.Event.fire('sidekickrefresh', validPositions);
            }
        }

                    for (i = 0, l = ps.length; i < l; i++) {
                        position = ps[i];
                        posItem = result.item(position);
                                    if (modalOpen) {
                if (posItem.err || posItem.hasErr) {
                    window._adPerfBeaconData.positions[position] = {error: true};
                } else if (posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    window._adPerfBeaconData.positions[position] = {size: '1x1'};
                } else {
                    window._adPerfBeaconData.positions[position] = {valid: true};
                    if (posItem.meta && posItem.meta.y) {
                        window._adPerfBeaconData.positions[position].size = posItem.meta.y.size;
                        window._adPerfBeaconData.positions[position].bookId = posItem.meta.y.bookID;
                        window._adPerfBeaconData.positions[position].creativeId = posItem.meta.y.creativeID;
                        window._adPerfBeaconData.positions[position].placementId = posItem.meta.y.placementID;
                    }
                    window._pendingAds.push(position);
                }
            }
                        if (posItem && posItem.conf && posItem.conf.clean) {
                            curAd = document.getElementById(posItem.conf.clean);
                            if (curAd) {
                                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                var posName = position.split('-')[0];
                if ((posName === "LDRB" || posName === "MAST" || posName === "SPL" || posName === "SPL2") &&
                   (!posItem.hasErr && posItem.size + '' !== '1x1')) {
                    curAd.className = curAd.className.replace('D-n', 'D-ib');
                    if (posName !== "SPL" && posName !== "SPL2") {
                        curAd.className = curAd.className + " Bdb-Grey-1";
                    }
                }
            }
            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                if (position.match(/^SPL/)) {
                    if (posItem.hasErr || posItem.size + '' === '1x1') {
                        if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                            YMedia.one(curAd).setStyle('padding-top', '0px');
                        }
                    } else if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                        var splashWrap = YMedia.one('#' + posItem.conf.clean + '-wrap');
                        splashWrap.setStyle('padding-top', (5/12*100)+'%');
                        splashWrap.setStyle('width', '100%');
                        splashWrap.setStyle('height', '0px');
                        splashWrap.addClass('Mt-20 Mb-50');
                    }
                }
            }
            if ((eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') && position.indexOf("LREC") > -1) {
              if (posItem && posItem.conf && posItem.conf.clean) {
                var fallbackDiv = document.getElementById(posItem.conf.clean + "-fallback");
                }
                if ((posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1'))  && (!isMONFetch || position !== 'LREC-0')) {
                    var lrecBackfillString = "<a href=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}\" style=\"background:url(https:\/\/www.yahoo.com\/sy\/dh\/ap\/default\/160201\/sports-dailyfantasy-v2-a966da5__1_.jpg) 0 0 no-repeat;height:height:250px;width:300px;display:block;\"><\/a>";
                    if (fallbackDiv) {
                       fallbackDiv.innerHTML = lrecBackfillString;
                       fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                    }
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    curAd.className = curAd.className.replace(/hl-ad-LREC/, '');
                } else {
                  if (fallbackDiv && fallbackDiv.className.indexOf("D-n") < 0) {
                      fallbackDiv.className += ' D-n';
                    }
                    if (isMONFetch && position === 'LREC-0') {
                        curAd.parentElement.style.height = '600px';
                        var curAd = document.getElementById(posItem.conf.clean);
                        if (curAd.className.indexOf("D-n") < 0  && curAd.className.indexOf("D-ib") >= 0) {
                          curAd.className = curAd.className.replace(/D-ib/, 'D-n');
                        }
                    }
                }
            }
            if (eventName === 'prefetch' && position.indexOf("LREC") > -1) {
                curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
            }
            if ((eventName === 'fetch_selective_ad_lrec2' && position === 'LREC2') || (eventName === 'fetch_selective_ad_lrec3' && position === 'LREC3')) {
                var fallbackDiv = document.getElementById("my-adsLREC2-fallback");
                if (posItem.hasErr || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1')) {
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                    curAd.className = curAd.className.replace('Ht-250', '');
                    // hide div if ad fetch failed or is 1x1
                    if (curAd.className.indexOf("D-n") < 0) {
                        curAd.className += ' D-n';
                    }
                    fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                } else {
                    if (fallbackDiv.className.indexOf("D-n") < 0) {
                        fallbackDiv.className += ' D-n';
                    }
                    curAd.className = curAd.className.replace('D-n', '');
                }

                if (position === 'LREC3') {
                    var lrec2Div = document.getElementById("my-adsLREC2");
                    if (lrec2Div && lrec2Div.className.indexOf("D-n") < 0) {
                        lrec2Div.className += ' D-n';
                    }
                }
            }


                            }
                        }
                    }
                }
                            if (modalOpen && window._pendingAds.length === 0) {
                window._adRenderComplete();
            }
            };

                        C.onStartPosRender = function(posItem) {
                if (window.performance  && window.performance.now) {
                    var ltime = window.performance.now(),
                        posId = posItem && posItem.pos;
                    _adLT.push(['ADSTART_'+posId, Math.round(ltime)]);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADSTART_' + posId);
                }
            };

                        C.onBeforeStartPosRender = function(posItem) {
                if (posItem && safeframeOptinPositions && safeframeOptinPositions[posItem.pos]) {
                    if (posItem.html && posItem.html.match(/<!--[^>]*sfoptout[^>]*-->/)) {
                        return true;
                    }
                }
            };

                        C.onFinishPosRender = function(posId, reqList, posItem) {
                var curAd = document.getElementById("my-ads"+posId),
                    adIndex,
                    posSize = (posItem && posItem.meta && posItem.meta.value("size", "y"));

                // Get clean div for ad position in case defined
                if (posItem && posItem.conf && posItem.conf.clean) {
                    curAd = document.getElementById(posItem.conf.clean);
                }
                if (curAd) {
                    // Let ad take its original size, remove default height given to ad div
                    curAd.className = curAd.className.replace('Ht-250', '');
                    curAd.className = curAd.className.replace('Ht-MAST', '');

                    if(posSize && posSize != "1x1") {
                        curAd.className = curAd.className.replace('D-n', '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    }
                }

                if (window.performance !== undefined && window.performance.now !== undefined) {
                    var whiteListedAds = {"LREC":"my-adsLREC-base","MAST":"my-adsMAST","TL1":"my-adsTL1","TXTL":"my-adsTXTL","LREC-0":"hl-ad-LREC-0","MON-0":"hl-ad-MON-0","MAST-0":"hl-ad-MAST-0","LDRB-0":"hl-ad-LDRB-0","SPL2-0":"hl-ad-SPL2-0","SPL-0":"hl-ad-SPL-0"},
                      ltime = window.performance.now();
                     _adLT.push(['ADEND_'+posId, Math.round(ltime)]);
                    setTimeout(function () {
                        if (window.YAFT !== undefined && window.YAFT.isInitialized() && whiteListedAds[posId]) {
                            // Trigger custom timing for LREC ad position
                            window.YAFT.triggerCustomTiming(whiteListedAds[posId], '', ltime);
                        }
                    },300);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADEND_' + posId);
                    adIndex = window._pendingAds.indexOf(posId);
                    if (adIndex >= 0) {
                        window._pendingAds.splice(adIndex, 1);
                        if (window._pendingAds.length === 0) {
                            window._adRenderComplete();
                        }
                    }
                }
            };

            C.onBeforePosMsg = function(msg_name, position) {
        // Make these configurable for INTLS
        var maxWidth = 970,
            maxHeight = 600,
            newWidth,
            newHeight,
            origWidth,
            origHeight,
            pos;

        if('MAST' !== position) {
            return;
        }

        if (msg_name === 'resize-to') {
            newWidth = arguments[2];
            newHeight = arguments[3];
        }
        else if (msg_name === 'exp-push' || msg_name === 'exp-ovr') {
            pos = $sf.host.get('MAST'),
            origWidth = pos.conf.w;
            origHeight = pos.conf.h;
            //"exp-ovr" or "exp-push", position id, delta X, delta Y, push (true /false), top increase, left increase, right increase, bottom increase
            newWidth = origWidth + arguments[6] + arguments[7];
            newHeight = origHeight + arguments[5] + arguments[8];
        }
        if(newWidth > maxWidth || newHeight > maxHeight) {
            return true;
        }
    };
                        //call back when the ad is expanded or collapsed
            C.onPosMsg = function (msg_name, data, msg_data)  {
                var visible;
                if(msg_name == "collapse" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdExpanded', '');
                    bodyTag.className += " " +  "mastAdCollapsed";
                }
                if(msg_name == "exp-push" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdCollapsed', '');
                    bodyTag.className += " " +  "mastAdExpanded";
                }

                /* generic ad expansion logic */
                if(msg_name == "collapse") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace(data + "-ad-expanded", '');
                }

                if(msg_name == "exp-ovr") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className += " " + data + "-ad-expanded";
                }

                if (msg_name === 'geom-update') {
                    visible = D.render.RenderMgr.get(data).viewedAt > 0;
                    // geom-update event will always be available when Y is available
                    if (YMedia && visible) {
                        YMedia.Global.fire('ads:beacon', {id: data});
                    }
                }
                                if (msg_name === 'cmsg') {
                    var splashConf = DARLA.posSettings(data)
                    if (YMedia && splashConf && splashConf.clean) {
                        var splashNode = document.getElementById(splashConf.clean + '-wrap');
                        if (splashNode) {
                            if (msg_data === 'splash-expand') {
                                YMedia.one(splashNode).setStyle('padding-top', (9/16*100)+'%');
                                if (typeof splashNode.scrollIntoView === 'function') {
                                    splashNode.scrollIntoView();
                                }
                            } else if (msg_data === 'splash-collapse') {
                                YMedia.one(splashNode).setStyle('padding-top', (5/12*100)+'%');
                            }
                        }
                    }
                }
            };

            

            


            if ("OK" == D.config(C)) {
                setTimeout(function() {
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_RSTART', Math.round(ltime)]);
                    }
                    var w = window,
                        d = document,
                        e = d.documentElement,
                        g = d.getElementsByTagName('body')[0],
                        winWidth = w.innerWidth || e.clientWidth || g.clientWidth;
                    if (true && winWidth < 1024 && window.pageloadValidAds && (window.pageloadValidAds.indexOf('TXTL') >= 0)) {
                        var prefetched = D.prefetched();
                        var txtlindex = prefetched.indexOf('TXTL');
                        if (txtlindex >= 0) {
                            delete(prefetched[txtlindex]);
                            for (var i=0; i < prefetched.length; i++) {
                                if (prefetched.hasOwnProperty(i)) {
                                    D.render(prefetched[i]);
                                }
                            }
                        } else {
                            D.render();
                        }
                    } else {
                        D.render();
                    }
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_REND', Math.round(ltime)]);
                    }
                }, 2);
            }
        }
    </script>
                </div>
        
        

        
        
        <input type="hidden" id="afhistorystate">
    <!-- bottom -->
    
                    <script type="text/javascript" src="/sy/zz/combo?yui:/3.18.0/yui/yui-min.js&/ss/rapid-3.29.1.js&/os/mit/td/aperollup-min-163d3e68_desktop_advance.js"></script>
                    <script type="text/javascript" src="/sy/zz/combo?&&/os/mit/td/td-applet-stream-atomic-2.0.442/r-min.js&/os/mit/td/td-applet-mega-header-1.0.195/r-min.js&/os/mit/td/td-applet-viewer-0.1.2108/r-min.js&/os/mit/td/td-applet-navlinks-atomic-0.0.54/r-min.js" async defer></script>                                <script type="text/javascript">                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                        YUI.Env.core.push.apply(YUI.Env.core,["loader-angus","loader-ape-af","loader-ape-applet","loader-ape-location","loader-ape-pipe","loader-ape-social","loader-applet-server","loader-assembler","loader-dust-helpers","loader-finance-streamer","loader-highlander-client","loader-live-event-data","loader-mjata","loader-stencil","loader-td-api","loader-td-dev-info","loader-td-lib-entertainment","loader-td-lib-hovercards","loader-td-lib-livevideo","loader-td-lib-social","loader-td-applet-stream-atomic","loader-td-applet-mega-header","loader-td-applet-viewer","loader-td-applet-navlinks-atomic","loader-td-applet-trending-atomic","loader-td-applet-sidekick","loader-td-applet-related-content","loader-td-applet-mega-comments","loader-td-applet-weather-atomic"]);
YUI.add("loader-ape-af",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-beacon":{group:"ape-af",requires:["json-stringify","querystring-stringify-simple","io-base"]},"af-bootstrap":{group:"ape-af",requires:["af-utils"]},"af-cache":{group:"ape-af",requires:["af-beacon","json","gallery-storage-lite"]},"af-compositeview":{group:"ape-af",requires:["parallel","af-utils"]},"af-comscore":{group:"ape-af",requires:["af-beacon","af-config","querystring-stringify-simple"]},"af-config":{group:"ape-af",requires:["af-utils","cookie"]},"af-content":{group:"ape-af",requires:["af-config","af-dom","af-utils","json-parse","mjata-rest-http","node-pluginhost","querystring-stringify"]},"af-cookie":{group:"ape-af",requires:["cookie","json"]},"af-dom":{group:"ape-af",requires:["node-base","node-core"]},"af-dwelltime":{group:"ape-af",requires:["af-rapid","json-stringify"]},"af-eu-tracking":{group:"ape-af",requires:["af-beacon","media-agof-tracking"]},"af-event":{group:"ape-af",requires:["event-custom"]},"af-history":{group:"ape-af",requires:["json"]},"af-message":{group:"ape-af",langBundles:["strings"],requires:["ape-af-templates-message","af-utils","node-base","selector-css3"]},"af-pageviz":{group:"ape-af",requires:["event-custom"]},"af-poll":{group:"ape-af",requires:["af-pageviz"]},"af-rapid":{group:"ape-af",requires:["af-bootstrap","af-utils","media-rapid-tracking","node-core"]},"af-sync":{group:"ape-af",requires:["af-transport","af-utils"]},"af-trans":{group:"ape-af",requires:["node-base","transition","af-utils"]},"af-transport":{group:"ape-af",requires:["af-beacon","af-config","af-utils","json-stringify","tdapi-remotestore"]},"af-utils":{group:"ape-af",requires:["cookie","intl","oop","querystring-parse-simple"]},"af-viewport-loader":{group:"ape-af",requires:["node","event-base"]},"ape-af-lang-strings":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_de-de":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_el-gr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ae":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-au":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-gb":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ie":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-in":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-my":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-nz":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ph":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-sg":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-za":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-cl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-co":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-es":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-mx":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-pe":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ve":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-fr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_id-id":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_it-it":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-nl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_pt-br":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ro-ro":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_sv-se":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_vi-vn":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-hk":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-tw":{group:"ape-af",requires:["intl"]},"ape-af-templates-message":{group:"ape-af",requires:["template-base","dust"]},"gallery-storage-lite":{group:"ape-af",requires:["event-base","event-custom","event-custom-complex","json","node-base"]},"media-agof-tracking":{group:"ape-af"},"media-rapid-tracking":{group:"ape-af",requires:["event-custom","base","node"]},"media-rmp":{group:"ape-af",requires:["node","io-base","async-queue","get"]},"tdapi-remotestore":{group:"ape-af",requires:["mjata-store","mjata-rest-http","json"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-applet",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-applet":{group:"ape-applet",requires:["af-applet-dom","af-applet-model","af-applet-headerview","af-bootstrap","af-compositeview","af-config","af-utils","array-extras","event-custom","node","stencil-tooltip"]},"af-applet-action":{group:"ape-applet",requires:["af-applet-dom","af-utils","node-event-delegate","node-base"]},"af-applet-dom":{group:"ape-applet",requires:["node-core","af-trans"]},"af-applet-editview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-moreinfo","ape-applet-templates-remove"]},"af-applet-headerview":{group:"ape-applet",requires:["af-applet-view","stencil-selectbox"]},"af-applet-init":{group:"ape-applet",requires:["af-applet","af-beacon","af-bootstrap","af-config","af-utils","event-touch","oop"]},"af-applet-load":{group:"ape-applet",requires:["af-applet-dom","af-message","af-transport","ape-applet-templates-reload"]},"af-applet-mgr":{group:"ape-applet",requires:["af-applet-dom","af-beacon","af-content","af-transport","af-utils"]},"af-applet-model":{group:"ape-applet",langBundles:["strings"],requires:["af-bootstrap","af-config","af-sync","base-build","mjata-json","model"]},"af-applet-savesettings":{group:"ape-applet",requires:["af-dom","af-message"]},"af-applet-settingsview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-settingswrap"]},"af-applet-view":{group:"ape-applet",requires:["af-bootstrap","af-utils","base-build","dust","view"]},"af-applet-viewmgr":{group:"ape-applet",requires:["af-applet-dom"]},"af-applets":{group:"ape-applet",requires:["af-applet-action","af-applet-init","af-applet-load","af-applet-mgr","af-applet-savesettings","af-applet-viewmgr","af-dom","af-utils"]},"ape-applet-lang-strings":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_de-de":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_el-gr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ae":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-au":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-gb":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ie":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-in":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-my":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-nz":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ph":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-sg":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-za":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-cl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-co":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-es":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-mx":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-pe":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ve":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-fr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_id-id":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_it-it":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-nl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_pt-br":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ro-ro":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_sv-se":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_vi-vn":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-hk":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-tw":{group:"ape-applet",requires:["intl"]},"ape-applet-templates-moreinfo":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-reload":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-remove":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-settingswrap":{group:"ape-applet",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-location",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-location-detection":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-detection"]},"af-location-panel":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-list","ape-location-templates-location-panel","stencil-toggle","stencil-tooltip"]},"af-locations":{group:"ape-location",requires:["af-sync","af-utils","mjata-model-base","mjata-model","mjata-lazy-modellist","mjata-model-store"]},"ape-location-lang-strings":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_de-de":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_el-gr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ae":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-au":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-gb":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ie":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-in":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-my":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-nz":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ph":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-sg":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-za":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-cl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-co":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-es":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-mx":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-pe":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ve":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-fr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_id-id":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_it-it":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-nl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_pt-br":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ro-ro":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_sv-se":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_vi-vn":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-hk":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-tw":{group:"ape-location",requires:["intl"]},"ape-location-templates-location-detection":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-list":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-panel":{group:"ape-location",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-pipe",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-comet":{group:"ape-pipe",requires:["af-beacon","af-config","comet","event-custom-base","json-parse"]},"af-pipe":{group:"ape-pipe",requires:["af-beacon","af-comet","af-config","af-utils","event-custom-base"]},comet:{group:"ape-pipe"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-social":{group:"ape-social",requires:[]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-dust-helpers",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{dust:{group:"dust-helpers",requires:["stencil-imageloader","moment","intl-helper"]},"dust-helper-intl":{es:!0,group:"dust-helpers",requires:["intl-messageformat"]},"intl-helper":{group:"dust-helpers",requires:["dust-helper-intl"]},"intl-messageformat":{es:!0,group:"dust-helpers"},moment:{group:"dust-helpers"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-mjata",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mjata-bind-model2dom":{group:"mjata",requires:["mjata-model-store","mjata-template","mjata-util","node-base","node-core"]},"mjata-binder":{group:"mjata",requires:["mjata-bind-model2dom"]},"mjata-json":{group:"mjata"},"mjata-lazy-modellist":{group:"mjata",requires:["lazy-model-list","mjata-model-store"]},"mjata-model":{group:"mjata",requires:["model","mjata-json","mjata-model-store"]},"mjata-model-base":{group:"mjata",requires:["base","mjata-model","mjata-modellist","mjata-model-store"]},"mjata-model-store":{group:"mjata",requires:["event-custom","promise"]},"mjata-modellist":{group:"mjata",requires:["model-list","mjata-model-store"]},"mjata-queue":{group:"mjata"},"mjata-rest-http":{group:"mjata",requires:["json-stringify","io-base","io-xdr"]},"mjata-store":{group:"mjata",requires:["mjata-queue","mjata-util"]},"mjata-template":{group:"mjata",requires:["dust"]},"mjata-util":{group:"mjata"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-stencil",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{hammer:{group:"stencil"},stencil:{group:"stencil"},"stencil-base":{group:"stencil",requires:["stencil","yui-base","event-base","event-delegate","event-mouseenter","json-parse","dom-base","node-base","node-pluginhost","selector"]},"stencil-bquery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-style","node-base","node-pluginhost"]},"stencil-carousel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-hover"]},"stencil-fx":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","pluginhost","event-custom","selector-css3"]},"stencil-fx-collapse":{group:"stencil",requires:["stencil-fx","node-style"]},"stencil-fx-fade":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-fx-flip":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-gallery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","transition"]},"stencil-imageloader":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-screen","node-style","node-pluginhost","array-extras"]},"stencil-lightbox":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","shim-plugin"]},"stencil-scrollview":{group:"stencil",requires:["node-base","node-style","hammer"]},"stencil-selectbox":{group:"stencil",requires:["stencil-base"]},"stencil-slider":{group:"stencil",requires:["stencil"]},"stencil-source":{group:"stencil",requires:["node","oop","pluginhost","mjata-util"]},"stencil-source-af":{group:"stencil",requires:["node","stencil-source","mjata-model-store","mjata-bind-model2dom","af-applets"]},"stencil-source-dom":{group:"stencil",requires:["stencil-source"]},"stencil-source-rmp":{group:"stencil",requires:["stencil-source","stencil-source-url","media-rmp"]},"stencil-source-simple":{group:"stencil",requires:["stencil-source"]},"stencil-source-url":{group:"stencil",requires:["stencil-source","io-base","io-xdr"]},"stencil-sticker":{group:"stencil",requires:["node-base","node-style","node-screen"]},"stencil-tabpanel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-pluginhost"]},"stencil-toggle":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-focus","event-mouseenter","json-parse"]},"stencil-tooltip":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","node-style","stencil-source"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-comments",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-mega-comments-mainview":{group:"td-applet-mega-comments",requires:["af-applet-view","af-transport","af-utils","af-rapid","stencil-tooltip","td-applet-mega-comments-templates-styles"]},"td-applet-mega-comments-templates-avatar":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body"]},"td-applet-mega-comments-templates-comments-body":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-comments-editor-tools"]},"td-applet-mega-comments-templates-comments-editor-tools":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-abuse":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-comment":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-delete":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-reply":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-main":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-form-comment","td-applet-mega-comments-templates-form-reply","td-applet-mega-comments-templates-form-abuse","td-applet-mega-comments-templates-form-delete"]},"td-applet-mega-comments-templates-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-replies-body":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-styles":{group:"td-applet-mega-comments",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-header",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mega-header-plugin-account-switch":{group:"td-applet-mega-header",requires:["node","event-custom","event-move","event-mouseenter","td-applet-mega-header-constants","get","anim","json-parse","escape"]},"mega-header-plugin-autocomplete":{group:"td-applet-mega-header",requires:["node","autocomplete","autocomplete-highlighters","autocomplete-list","td-applet-mega-header-constants"]},"mega-header-plugin-avatar":{group:"td-applet-mega-header",requires:["jsonp","td-applet-mega-header-constants"]},"mega-header-plugin-instant":{group:"td-applet-mega-header",requires:["node","event","af-event","json","jsonp","querystring","td-applet-mega-header-constants"]},"mega-header-plugin-mailpreview":{group:"td-applet-mega-header",requires:["jsonp","af-event","td-applet-mega-header-constants"]},"mega-header-plugin-notifications":{group:"td-applet-mega-header",requires:["af-event","af-cache","td-applet-mega-header-constants"]},"mega-header-plugin-username":{group:"td-applet-mega-header",requires:["jsonp"]},"td-applet-mega-header-constants":{group:"td-applet-mega-header"},"td-applet-mega-header-mainview":{group:"td-applet-mega-header",requires:["af-applet-view","node","td-applet-mega-header-constants","mega-header-plugin-autocomplete","mega-header-plugin-avatar","mega-header-plugin-username","mega-header-plugin-mailpreview","mega-header-plugin-notifications","mega-header-plugin-instant","mega-header-plugin-account-switch"]},"td-applet-mega-header-templates-accountSwitchPanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-autofocus_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-firefoxPromo_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-instant_filters":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-logo":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mail":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mailpreview":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-main":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-topbar","td-applet-mega-header-templates-logo","td-applet-mega-header-templates-profile","td-applet-mega-header-templates-notifications","td-applet-mega-header-templates-mail","td-applet-mega-header-templates-search","td-applet-mega-header-templates-instant_filters"]},"td-applet-mega-header-templates-notificationpanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-notifications":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-profile":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-accountSwitchPanel","td-applet-mega-header-templates-profilePanel"]},"td-applet-mega-header-templates-profilePanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-search":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-autofocus_script"]},"td-applet-mega-header-templates-topbar":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-firefoxPromo_script"]},"td-mega-header-model":{group:"td-applet-mega-header",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-navlinks-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-navlinks-atomic-templates-main":{group:"td-applet-navlinks-atomic",requires:["template-base","dust"]},"td-applet-navlinks-mainview":{group:"td-applet-navlinks-atomic",requires:["af-applet-view"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-related-content",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-related-content-model":{group:"td-applet-related-content",requires:["model","af-event"]},"td-applet-related-content-templates-main":{group:"td-applet-related-content",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-sidekick",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-sidekick-templates-item":{group:"td-applet-sidekick",requires:["template-base","dust"]},"td-applet-sidekick-templates-main":{group:"td-applet-sidekick",requires:["template-base","dust","td-applet-sidekick-templates-item"]},"td-sidekick-mainview":{group:"td-applet-sidekick",requires:["af-applet-view","af-config","af-event","stencil"]},"td-sidekick-model":{group:"td-applet-sidekick",requires:["af-sync","base-build","model"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-stream-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"stream-actiondrawer-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-cache","af-event","af-utils","event-outside","stencil-fx","stencil-fx-collapse"]},"stream-needtoknow-anim":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-pageviz"]},"stream-onboard-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","stencil-fx","stencil-fx-collapse"]},"td-applet-comments-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-interest-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-stream-appletmodel-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-cookie","af-event","af-utils","af-applet-model","af-beacon","af-poll","td-applet-stream-model-v2","td-applet-stream-items-model-v2"]},"td-applet-stream-atomic-templates-breaking_news":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-debug":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_action":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_desktop":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_flyout":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_share":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-errormsg":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-ad_dislike":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_dislike_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-default":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_clusters":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-featured_ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-featured_ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-featured_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-filmstrip":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-followable":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-gs_tile":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-needtoknow_actions":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-play_icon":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-right_menu":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-right_menu_featured"]},"td-applet-stream-atomic-templates-item-right_menu_featured":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-storyline":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_images":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-items":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-debug"]},"td-applet-stream-atomic-templates-main":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-breaking_news","td-applet-stream-atomic-templates-errormsg"]},"td-applet-stream-atomic-templates-related":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-removeditem":{group:"td-applet-stream-atomic",requires:["template-base"
,"dust"]},"td-applet-stream-baseview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","node-scroll-info","af-beacon"]},"td-applet-stream-headerview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view"]},"td-applet-stream-items-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-mainview-v2":{group:"td-applet-stream-atomic",requires:["af-beacon","af-cookie","af-cache","af-event","af-pipe","stencil-imageloader","td-applet-stream-baseview-v2"]},"td-applet-stream-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-onboarding-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-payoff-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-related-model-atomic":{group:"td-applet-stream-atomic",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-trending-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-trending-atomic-mainview":{group:"td-applet-trending-atomic",requires:["af-applet-view","node"]},"td-applet-trending-atomic-templates-main":{group:"td-applet-trending-atomic",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-viewer",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-ads-model":{group:"td-applet-viewer",requires:["af-sync","base-build","model","af-beacon"]},"td-applet-viewer-templates-ad_fdb_thank_you":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-ads_story":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-cards":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-debug","td-applet-viewer-templates-fallback","td-applet-viewer-templates-footer"]},"td-applet-viewer-templates-carousel":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-content_body":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow","td-applet-viewer-templates-ads_story"]},"td-applet-viewer-templates-content_body_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow"]},"td-applet-viewer-templates-credit":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-debug":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-drawer_feedback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-fallback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-footer":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-header":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-share_btns_mega"]},"td-applet-viewer-templates-header_ads":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-lightbox":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel"]},"td-applet-viewer-templates-lightbox_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel","td-applet-viewer-templates-thumbnail_items"]},"td-applet-viewer-templates-mag_slideshow":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-main":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-cards","td-applet-viewer-templates-lightbox"]},"td-applet-viewer-templates-modal_aside_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-modal_lrecs_mega"]},"td-applet-viewer-templates-modal_lrecs_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-reblog":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-sidekicktv":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-slideshow":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-lightbox_mega","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story_cover":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-video","td-applet-viewer-templates-header"]},"td-applet-viewer-templates-thumbnail_items":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-video":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-content_body"]},"td-viewer-ads":{group:"td-applet-viewer",requires:["af-beacon","af-config","af-event"]},"td-viewer-mainview":{group:"td-applet-viewer",requires:["af-applet-view","af-beacon","af-cache","af-config","af-event","stencil","angus-slider","event-tap","td-viewer-ads","td-viewer-slideshow"]},"td-viewer-model":{group:"td-applet-viewer",requires:["af-cache","af-sync","base-build","model","af-beacon"]},"td-viewer-slideshow":{group:"td-applet-viewer",requires:["stencil","angus-slider","event-tap"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-weather-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-weather-atomic-appletmodel":{group:"td-applet-weather-atomic",requires:["mjata-model-base","af-applet-model","mjata-model-store"]},"td-applet-weather-atomic-headerview":{group:"td-applet-weather-atomic",requires:["af-applet-view","mjata-model-store"]},"td-applet-weather-atomic-liteview":{group:"td-applet-weather-atomic",requires:["td-applet-weather-atomic-mainview"]},"td-applet-weather-atomic-mainview":{group:"td-applet-weather-atomic",requires:["af-applet-view","af-message","stencil-fx","stencil-fx-collapse"]},"td-applet-weather-atomic-model":{group:"td-applet-weather-atomic",requires:["model","af-sync"]},"td-applet-weather-atomic-templates-header":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-list-lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.hovercard":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust","td-applet-weather-atomic-templates-list-lite"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-dev-info",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-dev-info":{group:"td-dev-info",requires:["overlay","node-core","td-dev-info-templates-perf"]},"td-dev-info-templates-init":{group:"td-dev-info",requires:["template-base","dust"]},"td-dev-info-templates-perf":{group:"td-dev-info",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-lib-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"social-sharing-lib":{group:"td-lib-social",requires:["base","node-base","event-base","event-custom","stencil-lightbox","stencil-tooltip"]},"td-lib-social-templates-mtf":{group:"td-lib-social",requires:["template-base","dust","td-lib-social-templates-mtf_styles"]},"td-lib-social-templates-mtf_styles":{group:"td-lib-social",requires:["template-base","dust"]},"td-social-email-autocomplete":{group:"td-lib-social",requires:["autocomplete","autocomplete-highlighters","io","json","lang","node-base","node-core","yql"]},"td-social-mtf-popup":{group:"td-lib-social",requires:["base","event-base","io","autosuggest-standalone-loader","autosuggest-compose-utils","td-social-email-autocomplete","node-base","gallery-node-tokeninput"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.applyConfig({"groups":{"ape-af":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-af-0.0.326/","root":"os/mit/td/ape-af-0.0.326/"}}});
YUI.applyConfig({"groups":{"ape-applet":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-applet-0.0.207/","root":"os/mit/td/ape-applet-0.0.207/"}}});
YUI.applyConfig({"groups":{"ape-location":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-location-1.0.9/","root":"os/mit/td/ape-location-1.0.9/"}}});
YUI.applyConfig({"groups":{"ape-pipe":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-pipe-0.0.64/","root":"os/mit/td/ape-pipe-0.0.64/"}}});
YUI.applyConfig({"groups":{"ape-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-social-0.0.5/","root":"os/mit/td/ape-social-0.0.5/"}}});
YUI.applyConfig({"groups":{"dust-helpers":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/dust-helpers-0.0.144/","root":"os/mit/td/dust-helpers-0.0.144/"}}});
YUI.applyConfig({"groups":{"mjata":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/mjata-0.4.35/","root":"os/mit/td/mjata-0.4.35/"}}});
YUI.applyConfig({"groups":{"stencil":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/stencil-3.1.0/","root":"os/mit/td/stencil-3.1.0/"}}});
YUI.applyConfig({"groups":{"td-dev-info":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-dev-info-0.0.30/","root":"os/mit/td/td-dev-info-0.0.30/"}}});
YUI.applyConfig({"groups":{"td-lib-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-lib-social-0.1.207/","root":"os/mit/td/td-lib-social-0.1.207/"}}});
YUI.applyConfig({"modules":{"IntlPolyfill":{"fullpath":"https:\u002F\u002Fs.yimg.com\u002Fzz\u002Fcombo?yui:platform\u002Fintl\u002F0.1.4\u002FIntl.min.js&yui:platform\u002Fintl\u002F0.1.4\u002Flocale-data\u002Fjsonp\u002F{lang}.js","condition":{"name":"IntlPolyfill","trigger":"intl-messageformat","test":function (Y) {
                        return !Y.config.global.Intl;
                    },"when":"before"},"configFn":function (mod) {
                    var lang = 'en-US';
                    if (window.YUI_config && window.YUI_config.lang && window.IntlAvailableLangs && window.IntlAvailableLangs[window.YUI_config.lang]) {
                        lang = window.YUI_config.lang;
                    }
                    mod.fullpath = mod.fullpath.replace('{lang}', lang);
                    return true;
                }}}});
                    });
                });                </script>

<script type="text/javascript">
                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                                        
                        (function(root) {
            root.YUI_config = root.YUI_config || {};
            root.YUI_config.lang = 'en-US';
        }(this));
            var YMedia = YUI({
                
                bootstrap: true,
                lang: 'en-US',
                comboBase: 'https://s.yimg.com/zz/combo?',
                comboSep: '&',
                root: 'yui:' + YUI.version + '/',
                filter: 'min',
                combine: true,
                maxURLLength: 2000,
                groups: {
                    arcade : {
                        base: 'https://s.yimg.com/nn/',
                        combine: true,
                        comboSep: '&',
                        comboBase: 'https://s.yimg.com/zz/combo?',
                        root: '',
                        modules: {
                                                'type_appscontainer_smartphone': {
                        'requires': ['node','node-event-delegate','anim','transition','event-move','stencil','stencil-scrollview'],
                        'path': '/nn/lib/metro/g/appscontainer/appscontainer_smartphone_0.0.18.js'
                    },
                    'type_customizedbutton': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/customizedbutton/customizedbutton_0.0.9.js'
                    },
                    'type_events_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/events/events_0.0.3.js'
                    },
                    'type_geminiads': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/geminiads/geminiads_0.0.4.js'
                    },
                    'type_myy': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid','af-eu-tracking'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_myy_stub_rapid': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app'],
                        'path': '/nn/lib/metro/g/myy/myy_stub_rapid_0.0.4.js'
                    },
                    'type_myy_mobile': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/myy_mobile_0.0.9.js'
                    },
                    'type_myy_viewer': {
                        'requires': ['highlander-client'],
                        'path': '/nn/lib/metro/g/myy/myy_viewer_0.0.10.js'
                    },
                    'type_advance': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/advance_0.0.4.js'
                    },
                    'type_video_manager': {
                        'requires': ['af-content','af-event','base','event-synthetic','node-core','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/video_manager_0.0.127.js'
                    },
                    'type_video_stage': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/video_stage_0.0.8.js'
                    },
                    'type_yahoodotcom_client': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/yahoodotcom_client_0.0.15.js'
                    },
                    'type_myy_scroller': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/myy_scroller_0.0.8.js'
                    },
                    'type_rapidworker_1_1': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_1_0.0.4.js'
                    },
                    'type_rapidworker_1_2': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_2_0.0.3.js'
                    },
                    'type_featurecue': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/featurecue_0.0.14.js'
                    },
                    'type_fp': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_addtomy': {
                        'requires': ['node','event','io','panel','io-base','transition','json-stringify'],
                        'path': '/nn/lib/metro/g/myy/addtomy_0.0.21.js'
                    },
                    'af-applet-basemodel': {
                        'requires': ['af-config','af-sync','af-utils','model'],
                        'path': '/nn/lib/metro/g/myy/af_applet_basemodel_0.0.3.js'
                    },
                    'af-applet-contentmodel': {
                        'requires': ['af-applet-basemodel'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentmodel_0.0.3.js'
                    },
                    'af-applet-baseview': {
                        'requires': ['af-dom','view'],
                        'path': '/nn/lib/metro/g/myy/af_applet_baseview_0.0.3.js'
                    },
                    'af-applet-contentview': {
                        'requires': ['af-applet-baseview','af-trans'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentview_0.0.3.js'
                    },
                    'af-applet-contentsettingsview': {
                        'requires': ['af-applet-contentview','af-utils','ape-applet-templates-settingswrap'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentsettingsview_0.0.3.js'
                    },
                    'af-applet-dd': {
                        'requires': ['af-applet-dom','af-message','event-custom-base'],
                        'path': '/nn/lib/metro/g/myy/af_applet_dd_0.0.4.js'
                    },
                    'type_abu': {
                        'requires': ['af-event'],
                        'path': '/nn/lib/metro/g/myy/abu_0.0.16.js'
                    },
                    'type_abu_event': {
                        'requires': ['event-synthetic','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/abu_event_0.0.2.js'
                    },
                    'type_abu_video': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_0.0.6.js'
                    },
                    'type_abu_video_manager': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','type_abu_video','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_manager_0.0.20.js'
                    },
                    'type_advance_desktop': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop_0.0.8.js'
                    },
                    'type_advance_desktop_viewer': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid','highlander-client'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop-viewer_0.0.3.js'
                    },
                    'type_app_declarations': {
                        'requires': ['af-cookie'],
                        'path': '/nn/lib/metro/g/myy/app_declarations_0.0.6.js'
                    },
                    'type_partner_att_enus_foreseealive': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-alive_0.0.3.js'
                    },
                    'type_partner_att_enus_foreseetrigger': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-trigger_0.0.3.js'
                    },
                    'pure_client_darla': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/pure_client_darla_0.0.2.js'
                    },
                    'type_idletimer': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/idletimer_0.0.1.js'
                    },
                    'type_myycontentdb': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myycontentdb/myycontentdb_0.0.54.js'
                    },
                    'type_uh_init': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myyheader/uh_init_0.0.13.js'
                    },
                    'type_myymail_mainview': {
                        'requires': ['af-applet-contentsettingsview','stencil-selectbox','stencil-toggle'],
                        'path': '/nn/lib/metro/g/myymail/myymail-mainview_0.0.23.js'
                    },
                    'type_myymail_appletmodel': {
                        'requires': ['mjata-model-base','af-applet-contentmodel'],
                        'path': '/nn/lib/metro/g/myymail/myymail-appletmodel_0.0.16.js'
                    },
                    'type_myyrss_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/myyrss/myyrss_0.0.13.js'
                    },
                    'type_nux': {
                        'requires': ['node-base','event-base','panel','io-base','json-stringify','af-transport'],
                        'path': '/nn/lib/metro/g/nux/nux_0.0.44.js'
                    },
                    'type_optin': {
                        'requires': ['node','event','io','panel','io-base'],
                        'path': '/nn/lib/metro/g/optin/optin_0.0.33.js'
                    },
                    'type_changelayout': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/pagenav/changelayout_0.0.40.js'
                    },
                    'type_changetheme': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/changetheme_0.0.50.js'
                    },
                    'type_addmodule': {
                        'requires': ['node','event','io','json-parse','json-stringify'],
                        'path': '/nn/lib/metro/g/pagenav/addmodule_0.0.16.js'
                    },
                    'type_addpage': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/addpage_0.0.27.js'
                    },
                    'type_pagenav': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/pagenav_0.0.35.js'
                    },
                    'type_reco': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/reco/reco_0.0.10.js'
                    },
                    'type_sda': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sda_0.0.29.js'
                    },
                    'type_sdarotate': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sdarotate_0.0.17.js'
                    },
                    'type_signoutcta': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/signoutcta/signoutcta_0.0.9.js'
                    },
                    'type_windowshade_js': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/windowshade/windowshade_0.0.4.js'
                    }
                        }
                    }
                }
            });

            if (!YMedia.config.patches || !YMedia.config.patches.length) {
                YMedia.config.patches = [
                    function patchLangBundlesRequires(Y, loader) {
                        var getRequires = loader.getRequires;
                        loader.getRequires = function (mod) {
                            var i, j, m, name, mods, loadDefaultBundle,
                                locales = Y.config.lang || [],
                                r = getRequires.apply(this, arguments);
                            // expanding requirements with optional requires
                            if (mod.langBundles && !mod.langBundlesExpanded) {
                                mod.langBundlesExpanded = [];
                                locales = typeof locales === 'string' ? [locales] : locales.concat();
                                for (i = 0; i < mod.langBundles.length; i += 1) {
                                    mods = [];
                                    loadDefaultBundle = false;
                                    name = mod.group + '-lang-' + mod.langBundles[i];
                                    for (j = 0; j < locales.length; j += 1) {
                                        m = this.getModule(name + '_' + locales[j].toLowerCase());
                                        if (m) {
                                            mods.push(m);
                                        } else {
                                            // if one of the requested locales is missing,
                                            // the default lang should be fetched
                                            loadDefaultBundle = true;
                                        }
                                    }
                                    if (!mods.length || loadDefaultBundle) {
                                        // falling back to the default lang bundle when needed
                                        m = this.getModule(name);
                                        if (m) {
                                            mods.push(m);
                                        }
                                    }
                                    // adding requirements for each lang bundle
                                    // (duplications are not a problem since they will be deduped)
                                    for (j = 0; j < mods.length; j += 1) {
                                        mod.langBundlesExpanded = mod.langBundlesExpanded.concat(this.getRequires(mods[j]), [mods[j].name]);
                                    }
                                }
                            }
                            return mod.langBundlesExpanded && mod.langBundlesExpanded.length ?
                                    [].concat(mod.langBundlesExpanded, r) : r;
                        };
                    }
            ];
        }
        for (var i = 0; i < YMedia.config.patches.length; i += 1) {YMedia.config.patches[i](YMedia, YMedia.Env._loader);}

        
                        YUI().use('node-base', function(Y) {
                    Y.Global.fire('ymediaReady', {e: YMedia});
                });

                    });
                });        YUI().use('node-base', function(Y) {
        Y.Global.on('ymediaReady', function(data) {
            YMedia = data.e;
    YMedia.applyConfig({"groups":{"td-applet-mega-header":{"base":"/sy/os/mit/td/td-applet-mega-header-1.0.195/","root":"os/mit/td/td-applet-mega-header-1.0.195/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_30345894"] = {"applet_type":"td-applet-mega-header","views":{"main":{"yui_module":"td-applet-mega-header-mainview","yui_class":"TD.Applet.MegaHeaderMainView","config":{"alphatar":{"enabled":true,"urlPath":"https://s.yimg.com/rz/uh/alphatars/","imgType":".png"},"avatar":{"serverCall":false,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"format":"json","_maxage":900}}},"useAvatar":true,"bucket":"201","followable":{"enabled":true,"uri":"/_td_api"},"hasMailPreview":true,"hasMail":true,"hasNotifications":true,"hasProfile":true,"iSearch":{"isEnabled":false,"instantTrending":false,"frcode":"yfp-t-201","frcodeTrending":"fp-tts-201","spaceid":97162737,"cacheMaxAge":30000,"pageViewDelay":1000,"comscoreDelay":3000,"timeout":2500,"highConfidence":true,"autocomplete":{"additionalParams":{"appid":"is","nresults":4},"max_results":4},"api":{"protocol":"https://","host":"search.yahoo.com","path":"/search?","tmpl":"ISRCHA:A0124"},"filterUrls":{"web":"https://search.yahoo.com/search?p=","images":"https://images.search.yahoo.com/search/images?p=","video":"https://video.search.yahoo.com/search/video?p=","news":"https://news.search.yahoo.com/search?p=","local":"https://search.yahoo.com/local/s?p=","answers":"https://answers.search.yahoo.com/search/search_result?p=","shopping":"https://shopping.search.yahoo.com/search?p="}},"isFallback":false,"loginUrl":"https://login.yahoo.com/config/login?.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","logoutUrl":"https://login.yahoo.com/config/login?logout=1&.direct=2&.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","mail":{"compose":{"url":"https://mrd.mail.yahoo.com/compose"},"count":{"api":{"protocol":"https","host":"mg.mail.yahoo.com","path":"/mailservices/v1/newmailcount","query":{"appid":"UnivHeader"}},"refreshInterval":120,"maxCountDisplay":99},"preview":{"prefetch":true,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"q":"select messageInfo.receivedDate, messageInfo.mid, messageInfo.flags.isRead, messageInfo.from.name, messageInfo.subject from ymail.messages where numMid=\"3\" limit 6","format":"json"}},"urls":{"message":"https://mrd.mail.yahoo.com/msg?fid=Inbox&src=hp&mid="}},"url":"https://mail.yahoo.com/"},"miniheader":true,"miniheaderYPos":0,"notifications":{"inlineReader":true,"maxDisplay":0,"maxUpsellsDisplay":10,"count":{"api":{"uri":"/_td_api","requestType":"clustersUpdatesCount"},"refreshInterval":120,"maxCountDisplay":99},"list":{"api":{"uri":"/_td_api","requestType":"clustersList","image_tag":"pc:size=square,pc:size=small_fixed"}}},"search":{"action":"https://search.yahoo.com/search","assistYlc":";_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-","ylc":";_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-","queries":[{"name":"fr","value":"yfp-t-201"},{"name":"fp","value":"1"},{"name":"toggle","value":"1"},{"name":"cop","value":"mss"},{"name":"ei","value":"UTF-8"}],"autocomplete":{"ghostEnabled":false,"host":"https://search.yahoo.com/sugg/gossip/gossip-us-ura/","crumbKey":"gossip","theme":{"highlight":{"color":"black","background":"#c6d7ff"}},"plugin":{"minQueryLength":3,"resultHighlighter":"phraseMatch"}}},"searchFormGlow":true,"searchMiniHeader":false,"accountSwitchData":{"enabled":""}}}},"templates":{"main":{"yui_module":"td-applet-mega-header-templates-main","template_name":"td-applet-mega-header-templates-main"},"mailpreview":{"yui_module":"td-applet-mega-header-templates-mailpreview","template_name":"td-applet-mega-header-templates-mailpreview"},"accountSwitchPanel":{"yui_module":"td-applet-mega-header-templates-accountSwitchPanel","template_name":"td-applet-mega-header-templates-accountSwitchPanel"},"notificationpanel":{"yui_module":"td-applet-mega-header-templates-notificationpanel","template_name":"td-applet-mega-header-templates-notificationpanel"},"topbar":{"yui_module":"td-applet-mega-header-templates-topbar","template_name":"td-applet-mega-header-templates-topbar"}},"i18n":{"ACCOUNT_INFO":"Account Info","ACCOUNT_SWITCH_WELCOME_1":"Easily switch between multiple Yahoo accounts using the new","ACCOUNT_SWITCH_WELCOME_2":"Click \"Add account\" below to get started!","ACCOUNT_SWITCH_COOKIE_ERR":"It looks like you switched accounts. Refresh the browser to view your personalized page.","ACCOUNT_SWITCH_CRUMB_ERR":"Your account data may be out of sync.<br>Refresh the page to see your accounts.","ACCOUNT_SWITCH_ACCOUNT_MANAGER":"Account Manager","ADD_ACCOUNT":"Add account","ADD_MANAGE_ACCOUNT":"Add or manage accounts","ANSWERS":"Answers","CLEAR_SEARCH":"Clear search query","COMPOSE":"Compose","COMPOSE_CAPS":"COMPOSE","CORPMAIL":"Corp Mail","DEVELOPING_NOW":"Developing Now","ENTER_KEY":"Enter","FIREFOX_PROMO_TEXT":"Install the new Firefox","FOLLOW":"Follow","FOLLOWING":"Following","FROM":"From","GO_TO_MAIL":"Go To Mail","GO_TO_MAIL_CAPS":"GO TO MAIL","HAVE_NO_NEW_MESSAGES":"You have no new messages.","HAVE_NO_UPDATES":"Check back later for updates on stories you are following.","HOME":"Home","IMAGES":"Images","LOADING_MAIL":"Loading Mail Preview","LOADING_UPDATES":"Loading Updates","LOCAL":"Local","MAIL":"Mail","MAIL_CAPS":"MAIL","MENU":"Menu","NEW":"New","NEWS":"News","NEW_MESSAGE":"new message.","NEW_MESSAGES":"new messages.","NOT_FOLLOWING":"Follow a story and get updates here.","NOTIFICATIONS":"Notifications","PRESS":"Press ","PROFILE":"Profile","REL_DAYS":"{0}d","REL_DAYS_AGO":"{0} days ago","REL_HOURS":"{0}h","REL_HOURS_AGO":"{0} hours ago","REL_MINS":"{0}m","REL_MINS_AGO":"{0} minutes ago","REL_MONTHS":"{0}mo","REL_MONTHS_AGO":"{0} months ago","REL_SECS":"{0}s","REL_SECS_AGO":"{0} seconds ago","REL_WEEKS":"{0}wk","REL_WEEKS_AGO":"{0} weeks ago","REL_YEARS":"{0}yr","REL_YEARS_AGO":"{0} years ago","SEARCH":"Search","SEARCH_WEB":"Search Web","SETTINGS":"Settings","SETTINGS_CAPS":"SETTINGS","SHOPPING":"Shopping","SIGNIN":"Sign in","SIGNIN_CAPS":"SIGN IN","SIGNOUT":"Sign Out","SIGNOUT_ALL":"Sign out all","SIGNOUT_CAPS":"SIGN OUT","START_TYPING":"Start typing...","SUBJECT":"Subject","TO_SAVE_FOLLOWS":" to save and get updates.","TO_SEARCH":" to search.","TO_VIEW_NOTIF":" to view your notifications","TO_VIEW_MAIL":" to view your mail","UNABLE_TO_PREVIEW_MAIL":"We are unable to preview your mail.","UNABLE_TO_FETCH_UPDATES":"Check back later for updates on stories you are following.","UNFOLLOW":"Unfollow","VIDEO":"Video","WEB":"Web","WELCOME_BACK":"Welcome back","YOU_HAVE":"You have","SKIP_ASIDE":"Skip to Right rail","SKIP_MAIN":"Skip to Main content","SKIP_NAV":"Skip to Navigation"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-viewer":{"base":"https://s.yimg.com/os/mit/td/td-applet-viewer-0.1.2108/","root":"os/mit/td/td-applet-viewer-0.1.2108/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000101"] = {"applet_type":"td-applet-viewer","models":{"viewer":{"yui_module":"td-viewer-model","yui_class":"TD.Viewer.Model","config":{"dataFetch":{"NUM_PREFETCH_ITEMS":20,"NUM_RENDER_ITEMS":1,"NUM_BATCH_PHOTOS":25,"STALE_DATA_FETCH":1800},"cache":{"max-age":{"SLIDESHOW":300,"STORY":600,"VIDEO":3600},"stale-while-revalidate":{"SLIDESHOW":43200,"STORY":43200,"VIDEO":43200}},"uiConfig":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalLRECBackfill":true,"enableModalOmnitureBeacon":false,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"},"enableCategories":true,"enableEntities":true,"useUuidPrefix":true,"optimisticPrefetch":false}},"ads":{"yui_module":"td-ads-model","yui_class":"TD.Ads.Model","config":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"}},"applet_model":{"models":["viewer","ads"]}},"views":{"main":{"yui_module":"td-viewer-mainview","yui_class":"TD.Viewer.MainView","config":{"ads":{"enableAdsFeedback":0,"enableVideoSpaceid":1,"curveball":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"},"displayAds":{"config":{"LDRP-9":{"w":"768","h":"1"},"LDRB-9":{"w":"728","h":"90","supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LREC-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"WPS-9":{"w":"320","h":"50"},"WP-9":{"w":"320","h":"50"},"sa":"LREC='300x250;1x1' LREC2='300x250;1x1' LREC3='300x250;1x1' MON='300x600;1x1' megamodal=true","LREC2-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"LREC3-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"MAST-9":{"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"fdb":false,"closeBtn":{"mode":2,"useShow":1},"metaSize":true,"z":11},"MON-9":{"w":"300","h":"600","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"SPL-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"SPL2-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"FSRVY-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1,"bg":1,"lyr":1},"z":11},"FOOT9-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11},"FOOT-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11}},"enabled":1,"enabledAuto":false,"enabledDeferRenderAds":false,"forced_modalspaceid":"","enabledMultiAds":true,"positions":{"aboveFold":["LREC-9"],"belowFold":[],"custom":["MAST-9","LDRB-9","SPL-9","SPL2-9"]},"modalposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FSRVY-9,FOOT9-9,MON-9","lrecTimeout":"","enableK2BeaconConf":true,"enableBucketSiteAttribute":true,"offnetSpaceid":"1197762606","offnetposition":"LREC-9","magazineposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FOOT-9,MON-9"},"YVAP":{"accountId":"904","playContext":"default","spaceId":null},"displayAdsFP":{"enabled":0,"pos":"FPAD,LREC","config":[{"id":"FPAD","dest":"my-adsFPAD","pos":"FPAD","clean":"my-adsFPAD-base","h":"250","w":"300"},{"id":"LREC","dest":"my-adsLREC","pos":"LREC","clean":"my-adsLREC-base","h":"250","w":"300"}]},"modalYVAP":{"news":"145","finance":"193","sports":"82","gma":"145","default":"904"},"modalBackfillYVAP":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalPlayContext":{"news":"default","finance":"default","sports":"default","gma":"gma","autos":"bmprpreover","beauty":"bmprpreover","celebrity":"bmprpreover","food":"bmprpreover","health":"bmprpreover","makers":"bmprpreover","movies":"bmprpreover","music":"bmprpreover","parenting":"bmprpreover","politics":"bmprpreover","style":"bmprpreover","tech":"bmprpreover","travel":"bmprpreover","tv":"bmprpreover"},"modalBackfillExpName":"sidekickTVlrecbackfillHTML5","modalBackfillExpType":"right-rail-autoplay","modalBackfillAdTitlePrefix":"UP NEXT: ","modalBackfillYVAPHTML5":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalBackfillExpNameHTML5":"sidekickTVlrecbackfillHTML5"},"category":"","hlView":true,"js":{"twitter":"https://platform.twitter.com/widgets.js","videoplayer":"https://yep.video.yahoo.com/js/3/videoplayer-min.js?r=&ypv=","omniture":"https://s.yimg.com/os/mit/media/m/news/omniture-mega-min-78f5e30.js","embed":["click-capture-min","utils"]},"redirectNoContent":true,"rendered":false,"search":{"action":"https://search.yahoo.com/search","frcode":"yfp-t-201-m"},"spaceid":0,"ui":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalLRECBackfill":true,"enableModalOmnitureBeacon":false,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"}}}},"templates":{"main":{"yui_module":"td-applet-viewer-templates-main","template_name":"td-applet-viewer-templates-main"},"cards":{"yui_module":"td-applet-viewer-templates-cards","template_name":"td-applet-viewer-templates-cards"},"carousel":{"yui_module":"td-applet-viewer-templates-carousel","template_name":"td-applet-viewer-templates-carousel"},"story":{"yui_module":"td-applet-viewer-templates-story","template_name":"td-applet-viewer-templates-story"},"reblog":{"yui_module":"td-applet-viewer-templates-reblog","template_name":"td-applet-viewer-templates-reblog"},"video":{"yui_module":"td-applet-viewer-templates-video","template_name":"td-applet-viewer-templates-video"},"slideshow":{"yui_module":"td-applet-viewer-templates-slideshow","template_name":"td-applet-viewer-templates-slideshow"},"header":{"yui_module":"td-applet-viewer-templates-header","template_name":"td-applet-viewer-templates-header"},"content_body":{"yui_module":"td-applet-viewer-templates-content_body","template_name":"td-applet-viewer-templates-content_body"},"content_body_mega":{"yui_module":"td-applet-viewer-templates-content_body_mega","template_name":"td-applet-viewer-templates-content_body_mega"},"story_cover":{"yui_module":"td-applet-viewer-templates-story_cover","template_name":"td-applet-viewer-templates-story_cover"},"lightbox":{"yui_module":"td-applet-viewer-templates-lightbox","template_name":"td-applet-viewer-templates-lightbox"},"lightbox_mega":{"yui_module":"td-applet-viewer-templates-lightbox_mega","template_name":"td-applet-viewer-templates-lightbox_mega"},"fallback":{"yui_module":"td-applet-viewer-templates-fallback","template_name":"td-applet-viewer-templates-fallback"},"ads_story":{"yui_module":"td-applet-viewer-templates-ads_story","template_name":"td-applet-viewer-templates-ads_story"},"header_ads":{"yui_module":"td-applet-viewer-templates-header_ads","template_name":"td-applet-viewer-templates-header_ads"},"footer":{"yui_module":"td-applet-viewer-templates-footer","template_name":"td-applet-viewer-templates-footer"},"share_btns":{"yui_module":"td-applet-viewer-templates-share_btns","template_name":"td-applet-viewer-templates-share_btns"},"share_btns_mega":{"yui_module":"td-applet-viewer-templates-share_btns_mega","template_name":"td-applet-viewer-templates-share_btns_mega"},"mag_slideshow":{"yui_module":"td-applet-viewer-templates-mag_slideshow","template_name":"td-applet-viewer-templates-mag_slideshow"},"drawer":{"yui_module":"td-applet-viewer-templates-drawer_feedback","template_name":"td-applet-viewer-templates-drawer_feedback"},"thank_you":{"yui_module":"td-applet-viewer-templates-ad_fdb_thank_you","template_name":"td-applet-viewer-templates-ad_fdb_thank_you"},"sidekicktv":{"yui_module":"td-applet-viewer-templates-sidekicktv","template_name":"td-applet-viewer-templates-sidekicktv"}},"i18n":{"PREVIOUS":"Previous","Next":"Next","MORE":"...","LESS":"less","NOCONTENT_FUNNY":"Feeding the engineers.  Will be back soon...","NOCONTENT_READMORE":"Read more on \"{0}\"","OFFNETWORK":"Read more on {0}","ELLIPSIS":"{0} ...","VIDEO_DURATION":"Duration {0}","SPONSORED":"Sponsored","SWIPE_FOR_NEXT":"Swipe for next article","INSTALL_NOW":"Install now","FULL_ARTICLE":"Read Full Article","ARTICLE_SUMMARY":"Read Summary","READ_MORE":"Read More","AD":"Advertisement","SHARE_EMAIL":"Share to Mail","CLOSE":"Close","RELATED_NEWS":"Related news","START_SLIDESHOW":"Start Slideshow","CONTINUE_READING":"Continue Reading","CLICK_FOR_VIDEOS":"Click here to watch the video","CLICK_FOR_PHOTOS":"Click here to view photos","AD_FDB1":"It's offensive to me","AD_FDB2":"I keep seeing this","AD_FDB3":"It's not relevant to me","AD_FDB4":"Something else","AD_FDB_HEADING":"Why don't you like this ad ?","AD_REVIEW":"We'll review and make changes needed.","AD_THANKYOU":"Thank you for your feedback","AD_SUBMIT":"Submit","AD_FDB_ERORR":"Please select option. To help us making your experience better.","AD_FDB_SOMETHING_ELSE":"Tell us, why you don't like this ad?","DONE":"Done","READ_ARTICLE":"Read more","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","REBLOG":"Reblog","REBLOG_TUMBLR":"Reblog on Tumblr","UNDO":"Undo","SIGN_IN_TO_LIKE":"Sign in to like","LIKABLE_ARTICLE_SIGN_IN":"Sign in to see more stories you like.","SIGN_IN_2":"Sign in","READ_FULL_ARTICLE":"Read full article","COMMENTS":"Comments","FACEBOOK":"Share","TWITTER":"Tweet","EMAIL":"Email","CLOSE_VIEWER":"Close this content, you can also use the Escape key at anytime"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};window.ViewerClickCapture||(window.ViewerClickCapture=function(){function a(b,c,d){return b?b.tagName===c&&b.className&&b.className.indexOf(d)>=0?b:a(b.parentNode,c,d):!1}function b(b){var c=a(b.target,"A","js-content-viewer");c&&(d=b,b.preventDefault&&b.preventDefault())}function c(){!e&&f.removeEventListener&&f.removeEventListener("click",b),e=!0}var d,e,f=document.documentElement;return f.addEventListener&&(f.addEventListener("click",b),window.setTimeout(function(){c()},4e3)),{clear:function(){d=null},last:function(){return d},disable:c}}());window.ViewerUtils||!function(){function a(){function a(a){return a&&"object"==typeof a&&"number"==typeof a.length&&g.call(a)===h||!1}function b(a){return"string"==typeof a||a&&"object"==typeof a&&g.call(a)===i||!1}function c(a){return"undefined"==typeof a}function d(a){return null===a}function e(a){return c(a)||d(a)}var f=Object.prototype,g=f.toString,h="[object Array]",i="[object String]";return{isArray:Array.isArray||a,isNull:d,isString:b,isUndefined:c,isVoid:e}}function b(b,c,d){var e=a();if(!b)return d;if(!c)return b;!e.isArray(c)&&e.isString(c)&&(c=c.split("."));for(var f=0,g=c.length;b&&g>f;f++)b=b[c[f]];return e.isVoid(b)?d:b}window.ViewerUtils={getObjValue:b}}();
YMedia.applyConfig({"groups":{"td-applet-navlinks-atomic":{"base":"https://s.yimg.com/os/mit/td/td-applet-navlinks-atomic-0.0.54/","root":"os/mit/td/td-applet-navlinks-atomic-0.0.54/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000195"] = {"applet_type":"td-applet-navlinks-atomic","views":{"main":{"yui_module":"td-applet-navlinks-mainview","yui_class":"TD.Applet.NavlinksMainView"}},"i18n":{"TITLE":"navlinks-atomic","TOPICS":"Topics"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-stream-atomic":{"base":"/sy/os/mit/td/td-applet-stream-atomic-2.0.442/","root":"os/mit/td/td-applet-stream-atomic-2.0.442/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000173"] = {"applet_type":"td-applet-stream-atomic","models":{"stream":{"yui_module":"td-applet-stream-model-v2","yui_class":"TD.Applet.StreamModel2","data":{"ccode":"mega_global_ranking_hlv2_up_based","more_items":[{"type":"unknown","id":"2f3f95ea-5da2-3136-bc55-9f81bd3b5faf","instrument":"4000006470S00001","is_eligible":"true"},{"type":"ad","id":"31355634988","title":"Retraite insuffisante? Placez votre argent","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=MPsygGYGIS.qlj9ogdYxsfFbNURKDEncs6xM3dO7sevUd2xVfh7kO_.1TuRqVohdTWsjfk0BKr2zppAAj.HetsCxB06bIc_xCQIDlju4JMPeE.roI84Qr8r0o9SMbLY60wwNxhFqm5_XwVYh3w6Q1ZeZ8mlqhReoO28XJSJRmeKLEZIsOK7lqKA9O71nCZjlQCh_kgb54XWVZ9DQDfBHA0mYamki4J1cH2sWsK0xzTfADTXCu8fiQ5AKGVW9NWjv1iqSbXdS.vpky7FcwsiV21wq5nYzZ08m3GKAqY7H5Pe3B3BNHINy1NKSuxkJHSbovkPf2W0ZxYqMSXC2.bpr.nlQo.uHIaZ9UlYQCVOZ8nur5JU8QTSpjQih9ETLbwHygdkHTzYokCLgLh1A0AwinlQSmZbbr5DEUSrpcd8ck2vpIY3G12YLX828yDk9ALiEXXrKGOLMolW72PUcO8pDDEPG4FegATRcr5NnYDyPCtp.xNtZMIRdoVAV6wzBZ65tSaMQjWIN4Csfatj_iAWWbsSpZdvN2FWVgJuo5TUcw5m0m1219h661FIJVZpeZmp6P_YqA4cOPRiAaXKh9HkG92mc0TRCnBuZQw7wnXvckUmQAmzBZmtVQIcLL8rEufWJzEh1bXpMFzSbog--%26lp=https%3A%2F%2Fserving.plexop.net%2Fpserving%2Fbridge_002.htm%3Fa%3D4%26t%3Dhttp%253a%252f%252fpreg.TradeLG.org%252faserving%252f4%252f1%252f1004%252f2_fr_2822.htm%253fSerialId%253d1115580%26adv%3D1%26f%3D75185","publisher":"TradeLG","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=VzEW4cIGIS_z6c0DBWsvhXdWCWjy1KbI1h_LLREvTPTnHXgttI6TX6hpxH8ViQb_36EURfaMWSfi2wNn4e0bsXMah2BEUD2Mh_yjE78LYGiR0VUAg5jSk_iUxaQZxVYQ4.oVHaH483ON6nfnU63xujwHbOP43WuMMS0nz5_rPqB3_atvKCbo3SPsTrHYXJHult5TyCpnV2V_lA0mR7TJir_ArTM7J5Vt0GxP.f6f0MJjwgMf.fJ3PP.iNhDgHOSCdre.VSTRyX95f1slGVArDzmiZ7SPJqGwkUPWBEBicXMvSAlt1RhIryGvE4XOd3ujmtCauDmaJyD1A4jlQceCVOlDGRBrQmXx7Cg5u67BNRIG3iqhQ.kuON02Sjg.CdA-&ap=14","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15t7meep2(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31355634988,dmn$serving.plexop.net,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$1135676,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/ioOxG20cc7.LRQz8HocIkA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1447685626349-6650.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Toutes nos astuces pour investir seulement â¬300 en bourse et multiplier son placement. Offre spÃ©ciale dÃ©butants","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31355634988","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":0},{"type":"unknown","id":"13bd92de-6013-30c4-be62-3dd6d3704864","instrument":"4000022080S00008","is_eligible":"false"},{"type":"unknown","id":"29d5d31c-17ef-33ea-a3ec-fed5cc69e367","instrument":"4000015000S00008","is_eligible":"false"},{"type":"unknown","id":"eeb9b163-de7c-3b21-a6eb-cae65438408f","instrument":"4000006760S00002","is_eligible":"true"},{"type":"unknown","id":"cf3347a6-74fa-3e27-a9bd-57cc30593b93","instrument":"4000001260S00004","is_eligible":"true"},{"type":"ad","id":"31703404649","title":"Les joueurs du monde entier ont attendu ce jeu!","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=HHIrJLoGIS8TBtNy2KtjiHw1sjXbUu4RTofit7t6BjXTjjXkhDREB8.nHIBMfinkJC2usb0cOJuYplLbtaZF9BJ3HZ7f3wR3Onahl.FZZcofrH3DjENAN9Jo4W46Ur1qmTRctZRrUb2SdQKyvaUbTqxwxiEeVK4bVCV.qcwyu8IELENeDiltEH3MRM0jacTQ9EhnaN6XlNVbNcP5j7XYHeqqhk1evscF5oy_fWKGAO9fCbVlN5j_pko19cP66YtvvVpUGedXwKi8qHTVoafEePaFzCeWDnLFRPNesZfGV6qhy2HI1U_.qUNQNneJU8PY5JnJWZcIi4t3Xd0SvcukdPybckS6vin_nSvgHviTnuC4DfxFKicSUL.JsHEPBOvpKgXqY01XM33rjAS3RgN5wQF.hlzNDY1wrQiSYhDAht3M5AVpiregWuh6Bee8Rtz5alK5cktq_hTILcyOfOZJkzRyab64tRVLcCs6ynp4YtxbbZZIL3_LA.X1dNWU.ak8tasc1ZBtEWkYqpcagOGOM1SzAmFSzxmA2ibT5jSsBI39ABNQKIlEJT91BsAipvgb6F7_QlPa5ed6JlWbk9j1Bno370M9YKcjzZCJ8pdntYJ.Zz5Fw93aJwlxgNfVRyc-%26lp=http%3A%2F%2Fwww.mnbasd77.com%2Faff_c%3Foffer_id%3D1384%26aff_id%3D1065%26url_id%3D4561%26source%3Dysa%26aff_sub%3DFR%26aff_sub2%3Dn244%26aff_sub3%3D31703404649%26aff_sub4%3DBoundary%26aff_sub5%3DB","publisher":"Plarium","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=YERNdHwGIS_7Y7pmhyMOP9WVmeIC.ut9AC_x5OpvlIuXEOZYCwYSpqTteuynJNeao67EFvVXLVNBR2gtkbZLBL0WpONrQtlzsqIxRdEX39cFqCKuCAOlECTVn46S6eAA8S0zNYubjHw2RQaNtj4RLzhfKUapl3oQsL3a5HwVzS_rE7Mgcaxp7knkqenu3JRHqQxZt830EBqrUWoWwVqrP.ygEsbokJpwTXMd.HPfn36l15tO8Yy7kT4PZZ4cRFTUAFNNaPi.8Q.XmHu8No_WD2.B9L8XQSN_D1BnnmqwDwHG7K0HuwQAUBiKO8KY2FnzpQ6S1tcsN2lSos41EKLX4GZzhUodXaNGKg7Ki3QY3l7kAvGxybB.opmZqhS4sWd.&ap=19","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15j0aqc78(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31703404649,dmn$plarium.com,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$5584,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/DgYfTZXzge.YZ1nidgd0kA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454677871802-8151.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Pourquoi il te faut jouer Ã  ce jeu MMO de stratÃ©gie gratuit et addictif? Le MMORPG le plus excitant auquel tu as jamais jouÃ© ! Ne passe pas Ã  cÃ´tÃ©!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":2,"cposy":2,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31703404649","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":1},{"type":"unknown","id":"3d765be7-9306-39e1-9cb8-12785218f432","instrument":"4000018490S00001","is_eligible":"true"},{"type":"unknown","id":"009a7f55-99c7-3a80-b5c9-2d2c967f13af","instrument":"4000012980S00008","is_eligible":"false"},{"type":"unknown","id":"92c50409-eac6-3b57-a73f-2dd8a75f45f4","instrument":"4000006580S00001","is_eligible":"true"},{"type":"unknown","id":"07a4c592-f578-39f3-a492-16c952aa8e4e","instrument":"4000011360S00001","is_eligible":"true"},{"type":"ad","id":"31700971254","title":"Buying Custom Furniture Just Got Easier","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=7snUpcoGIS8Xg9NcBQwQtu9pYS4PiE0f9k2Ewl.cJNo7RpaREfqAoHgfV52Z26d_aQq7i7SWXzrvemciJagT0ScNVDBKUj5a.M80urWUfW1V6ZSZ2M1M.7POLTNMuQTWVdtPT79Hr.TAWPtcBQmn3fzNmpP20vvwtBaaVpMOY2q6bIbDTTA7vyYOWfNL.pep360F.kmo14QWcAwUc2zUVHjOByjJyeOOHeOqRS6v45jI1ab13mwQIGMDSfVriMHMbvgTSSGdZRHtdbijycOXNrvXApUI3jangspdHvRbJcT4hsLmG4wsjX7lkLMcD5bwRxyXn61lvE5LbKMoZ432U3_ctsNEaqywNgvXRavDiXhKPMQoWDQrRNdABbZxG8DCVKzSTNmCOcEc7TPkpsUD05w18eEYLstM7pZjoGFJMMjhAbpDEMDKJawK4JF2.JCcLWYX.vjLdzB9ZcoYlO1MdCQNz8s5XwNbo4pnSg_pUzlcPq7hdYZMP6cj2LA0Ae3kodbN6GCbAHuUrQiecahMZHjeHKp7IwX_gu1BTZGXkdsbinz7BaRI.HFu4THMsJeXsWHAm5ZeatsfoP6co3hdonfWCPtLg1bOGmDu8NG3bKU50Iftz.owLCkEXzs-%26lp=http%3A%2F%2Fwww.birchlane.com%2Fgateway.php%3Frefid%3DBLYSTM1.31700971254%26image%3D27003935%26url%3Dhttp%253A%252F%252Fwww.birchlane.com%252FCustom-Upholstery-C1844682.html%26gateway%3D1","publisher":"Birch Lane","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=mrWF6eQGIS.vvkPvqzitsL8Cdgl4k6s2VHB6Av_gJ2gl.P8Af.orCKx.dLU.ZX7eZRXCd9lcyuNNPPfFyAKtE0Niu6aU9Ump79B1Bo_UbfJVprV5I59kim7B.VMYG8z5erndiCFITrP.QbPLhEGcufccaP_MGr8aWX9Zf35_Ns5DJweHxS82SZ_ZMC11TIelllyP44CTTR8yDYKs87Ty2qAdt7et_dc0ckk.RCKdVtBcF4uu0HIp5r9gM8xzZOsKUr5fTxJ4pxo9X64vVDHuu5xEiprOTh5gPMxrWWkGYGyX664z17iLJY3_YVHwgSsNTBiZrY2RjTlhX79LlFZ2n6DNRcyz4qWf2JkN8lHnRZrAoyjYu2AW2WjkOxI-&ap=24","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15nt2fmuk(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31700971254,dmn$birchlane.com,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$950319,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/6bRKN7wp5yFer.9oMJaGlA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454617940519-5883.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Design the perfect piece of furniture for your home with custom upholstery by Birch Lane. Request free fabric swatches and check out our buying guide.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":3,"cposy":3,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31700971254","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":2},{"type":"unknown","id":"2eeb33c8-5bf0-3308-affd-f7bea8cc4d30","instrument":"4000006790S00001","is_eligible":"true"},{"type":"unknown","id":"c-1a240815-adec-4f7f-8c8e-0f1c2af8b279","instrument":"4000002600S00001","is_eligible":"false"},{"type":"unknown","id":"e4638a63-3383-32ac-916d-c8610ceeac56","instrument":"4000005840S00001","is_eligible":"true"},{"type":"unknown","id":"433f0c7e-8506-3fd7-b625-f1fdb28a2491","instrument":"4000006070S00002","is_eligible":"true"},{"type":"ad","id":"30956615756","title":"Emprunter sans passer par sa banque?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=IUatENIGIS_rGJMXmuF7mYzunCqECIexO0fzqxlR..F4OrLeYUUNismIV6DwfQtXC3u6HUB3YrLKmJa.Qq6gd7XP.N9Vn3fGkTFOOjHK6rxHW78opNeX3Y7qIZ8JqDI2J46aP14H3BZiB7J.g6vttFcyoXt2QAE70s8R4XQZlASgphAAc9vPr0QlEO81PjA0O8OxFQRSH9KAFA3UT5_Vwv0I0ca6GMVOvOEBvNKGUwN5IaRQKBGa7lFGgGtGqkW2bnMdZObP1alwJrfyqEabCx81VjkAPQHaVj4p28Em41z_hYcymAc6w5qbcw57J1dJxnbpx5ajxaA.RIeYDr2XAitVNvpQ879EIkxp3.0x4lyoNwTanMR1btikW25nV07mAcMChljlY_O2IEXpSqwKOXe8Hbs.0PpUftxoySSKLrcFGMUS9HnE4U9yhdA5lw0rryaaqUQwFT3r03jLjJoWtqRaJBJkCFBzqj8x4duPVnlScFfCMBKwJLxmYeTb5YhuARafPFyQ0SULLAwAHJfksTnz87NwP0rC2GI-%26lp=http%3A%2F%2Feulerian.pret-dunion.fr%2Fclick%2Fpret-dunion%2F8lL.QlYVeQ7BL6AqQORYT.bZAMvIS20JxDE6OFjGTg--%2F","publisher":"PrÃªt d'Union","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=aESogOYGIS9MXuv8SHyi3OPcV6leXO8uar25MzyndbKytXN34gmtqR8j43kGcEsav_Uah09_9rDlDQEUuSKVESo9b1ijEi4Och2cTIxCvGR44OTJcAlbWuts0KtGC4cZqv9sbTK8x02lhkb6uu.Q8hub6r4eGm66r8wZK6zM6EZ9GQfbn5ewNo58sRR_q_2lp6DJsDy9rBtYvRTV7X_g9k5Cx8_LkJvu25dwTqSQK.s3v_qexw.N2WW53sPp1TPVE5.m4JMgj3nN.Lb6M27vv0lKXTmsJYShgOvzowfb7VE1JiI3nGUrPWnHWYnVw6.x4QlBl3vA.r7damC_RSR8sI2fWdhNUxq7J3sQ3r0ntujO5BdndrwQtLtzwgFZPQYrE686d.FG&ap=29","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15op3jl2p(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$30956615756,dmn$pret-dunion.fr,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$936665,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/PqkUXHAilhshYiRhIxQfBw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1450461635406-8419.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Oubliez les banques et profitez d'un prÃªt sÃ©curisÃ© moins cher, entre particuliers ! Enfin une alternative pour rÃ©aliser vos projets de 3000 Ã  40 000â¬.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":4,"cposy":4,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"30956615756","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":3},{"type":"unknown","id":"a55f563c-fb0e-34be-aa66-19683b48d2b0","instrument":"4000001910S00004","is_eligible":"true"},{"type":"unknown","id":"8f45665d-7d32-3cca-b5f3-118091461dbe","instrument":"4000014260S00008","is_eligible":"false"},{"type":"unknown","id":"6cf9de0d-85f8-3ab4-8098-493a631b3d9e","instrument":"4000006760S00001","is_eligible":"true"},{"type":"unknown","id":"d75c7455-c948-381c-99de-63da5371b77b","instrument":"4000007520S00001","is_eligible":"true"},{"type":"ad","id":"31590019210","title":"Plonge dans la plus grosse bataille de ta vie !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=nsCB2ckGIS9d5aUEpYitm6Emy5L9vGnLWB62n9oaoEfgAJgOfsKvJrpwbr8tUhJXjg1_pT0vJ4BGDF0MFYh5w8RbIq5g44sIJ5MwyFw392NmtVoxa7ZhETWs226Qo3kRZqCihm0K5.7byL8cTzvux8mLYFBuEZX97UT1s05zrDxvxJYP7VMUOLKkIHalxtmW1x2kfsou9eAnvyevLiiYDy8dzVjuI2E9vqA7aJAM4E4.YuzfVj_0IbOjNMMyMIq317SQrrDnw7edOqv8cmInK1l0abAi5eTnGZU8GqGCxmkBzp84ENELqGeUb3ZTXHPhZGTmsOEQzxx3iSl6SYfokRdEkXbJPhaVwoOiMGf0hRIOGOxMZ5WDAC4j279Scal5Ww3Kg4qnTo7cojoj.cP5WNYQAufTnymXFYOORozjJSKHKCUR1cEb9mgsTVQ.w56cVpUpAFAzidLi1PRzcdEr_Kksufd3d2hcidSEYUX6I5IzJsB0GKMu00pvtHuWC9h8n.eTyHJzF48AK6z_K.5S25zj7mg13Z8CcE0NodXBs8X5ECP6VJk6vN19KVpBVGODIHP66g--%26lp=http%3A%2F%2Fplarium.com%2Ffr%2Fjeux-de-strategie%2Fsparta-war-of-empires%2F%3Fplid%3D82731%26pxl%3Dyahoo%21%26publisherID%3D%23sid%23_%23creativeID%23","publisher":"Sparta : War of Empire","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=j6meanwGIS.kuWpvqUWfdIiYhJ4ACo.uBD5QEzNXWgmGhWaoauBUrLzgwzAiVp5JerVUHsh3ldk3_LyJnWy0TUKYUNtPy1DYC9W0HogM6OWtO9_h3LstFIYtZ_ag95qgZjUNudcFrZwbbbZe0Lb0uRyZ3HTq0lTejfT5INxYwFpt3AfNQrOGMkP64s9U7IbufA_BQkhuhz9mLaD6QLdj3DVzlzb6p6rleKfFzFaHXE2EbV9ht8E0q4EvIuD9msyLC7uemPhSlKsjeb2plKjgZSVZJ4ExWyGzMfNqjIU9gzc9pZgSzzxaB2pdvib6yMjZAmbHPhhQwdyAFxvXmQc.ZbJ9khPeR34q3ts8Cemuq9OREmrJKdvdtnP0zKlAYsknw1yN&ap=34","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15no3quk8(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31590019210,dmn$c31590019210,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$1280969,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/pnD7UE9ORpKoXSP2Ef3TKQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1450170379340-315.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Le succÃ¨s qui fait fureur dans le monde des MMO. Sois de la partie !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":5,"cposy":5,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31590019210","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":4},{"type":"unknown","id":"fdeca80e-fae8-3af3-b2ac-703b03dbeeeb","instrument":"4000012840S00008","is_eligible":"false"},{"type":"unknown","id":"dd514d43-b845-39b8-b1bf-bd922b01bcd6","instrument":"4000006210S00001","is_eligible":"true"},{"type":"unknown","id":"ebb0cc87-0776-350f-b612-f8fbc538b3dc","instrument":"4000013080S00008","is_eligible":"false"},{"type":"unknown","id":"17123c44-6236-3001-a7cc-20603842cac5","instrument":"4000010270S00008","is_eligible":"false"},{"type":"ad","id":"31156415448","title":"Nouveaux programmes immobiliers Ã  Bordeaux !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=yX_Zc9IGIS9IB3zpSdmtlA24reh27Xf2lS_BWxOTFwhvcrqyKeWp1R_N2x6jsH_Y4aLk8sbJrWHJiJPiVfB4q0ZhBa4himoyQCOmGfP4lngpecQSr41trPzRZn2xiM5H3.2.XlkBkDe5zfwUXKXh1HgQdQLlr5nUsJUH0WaAqLwTZw4XsngBeg54xbqzLbN10lEJ0jROeAXAzjKgGoJ.7r9Y3hpddULVuCb6RV1qaitQ6MK.UHPtrZo8HrT8cQelXZuhjfjdUYaxnKNtruMVuWeujIRoUur2iQWlJZFlTv6BAfbz1isOWW9881F1JbVeSaAMX2VDIPDVZgtMbwD9T5tgWJzRJt2s0KjNlpJtRmeR832OH6orehU8rezJeTi8pW6pYx0Zdow81MseoLGHGguqqupw1DacLKM90sPYlPfmcR42aIQZ1eykQGmBLshrOyeY23UQ9XU3bNoogcBmdmfzMRQtyhm2SIQaNLAXgwOX3hcRWES2ApWV0A--%26lp=http%3A%2F%2Fwww.pichet-immobilier.fr%2Fglp%2Fcub%2Fbordeaux%2F%3Foid%3DPI15REC0711%26xtor%3DAD-3782","publisher":"Pichet Immobilier","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=bfqaXgYGIS_b1WJqqh0nYq_YnRG3zJc99H2ELho5NvBsRu1Q5rTqYgfDoSHncWqmgg7gmQiAhZH9TqMpIphHQVC4xIrsF9onAecssVkFR_5YgfOx9w6E8ERDULLcVRymB4.9RxQLCdIpQ2JpVZHxgzcxzEcMjLmyNhl7DVgJUaH._8aVvUB3Uyvhny5.8wmS7KDNacZgub7Ku8qipDrnxYBPxzt.XjN3o2heDaQ_yqKjOGfJ.tIdh9UHXgAjDi8nQVDsDbGvq6LauU.98rSgQ20sUiAARgRpwT_.yjhOTPftZNMRXBsCphT0U9wmHNOTjbxJqb1F7eAqBOTS8s1kRwrXXZOTKAN8qehDQEEpxP2zZ0drtXGiFqb0Ti_EIJH7PCG__J3oc7UISSDBsTG_ZRR80zBnArWJm9KSDBuzbQZa&ap=39","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15v9j06r7(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31156415448,dmn$pichet-immobilier.fr,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$1022549,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/SqSRd347SgSIfNz6.hZYDA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1445594441214-1628.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ©couvrez les rÃ©sidences Pichet immobilier Ã  Bordeaux MÃ©tropole : Appartements neufs + personnalisation gratuiteâ¦ TÃ©lÃ©chargez la brochure dÃ©diÃ©e !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":6,"cposy":6,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31156415448","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":5},{"type":"unknown","id":"c-dde92254-0a1e-4af6-b305-701c18c29ef1","instrument":"4000005100S00001","is_eligible":"false"},{"type":"unknown","id":"7ccba62c-8049-314a-a64c-eee83a2852fc","instrument":"4000014960S00008","is_eligible":"false"},{"type":"unknown","id":"cc4b73bf-024c-3155-86e2-1f19a73feaa8","instrument":"4000015430S00008","is_eligible":"false"},{"type":"unknown","id":"b92cd10b-ee1d-3c2d-8813-8736bcb47bd9","instrument":"4000011140S00001","is_eligible":"true"},{"type":"ad","id":"31666936659","title":"Rachat de crÃ©dits avec Partners Finances","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=1i_RX94GIS8CD76PUqa7piuS6aWB7XAj13Vica1_zvv05_Knwn_la6ZqCMuhqW6wo8K_YxWlXowVmWFkDihMqFglLhPNPEa0x0eXQwgLKPK4RiEsmfiZF9rfJOWPzLNJiMu4ENp.N6ygCHRpsr09..yeTiMMSGP4VuuZArcrdlj1qlwjq9jMxN6eNbWy7SqXU79OgCA23zQhUERt5D_0ZqLlwfY6Is7el1.eXE7kSuKaaZqj80HTqCg712q7SVWI4Kk13u_PayEGV5S7DI630oCe7a8Yq91ucUafsR.rwf0NwsovPRfVlIljxnXUBUgnwrAMH3RX1S6IhPqhiGeUA7ks9dnZL_n.6vIprJEOtRx8qmUwJQthcZhkGxzPyrVYMEE3bX8PW3MJ_or2KoWYbFUB90jDOxV2fH9u3.iMrlzVaccBQpFGh_ekOKEF_0jm2uwQqEcGDXWcxW5IOiKfKf.uSE4kzZlYmuISG5uD_pORnJFV2zQ_eIc1K6Q.BJXIW2onj3k-%26lp=http%3A%2F%2Fwww.partners-finances.com%2Fformulaires%2F%3Fprov%3Dlienpromo%3A%3Apartners-yahoo-ads-pf-oct15","publisher":"Partners Finances","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=mkOC370GIS99WH7j2r6ltQKVyGK_WafVLEXK0RndoODNcR7nE_2TjbAD5nIdCXO9R98n6yaXMMk9fJZucWBuabis4kL4u0hPMzWAGM3QykEoRsL6vLyMFq8jBq9B_JOFx.swsYhNbMwOnUnhuHKzlXQF0cH436DJxr66BRLPwbA774jmMPYzH.wTMIqeYENmMv_6ST3aojgzdZFqxFsb_JLZ_xgWaPt6FGHwIesmSq7WvLQqFHim2Cvm1.IKL7OB4TSqrEtNHpUyipohOQZXUxjroOCR6OaU_leCdNIfFMYjFpGS62sQqyaSqO4fFbcGoHf7wRUISVyMPoVmnE.Zer4WgDeDbeyBNdsnantYuEMQv1oB8NWP3p1xg7_.gbOgKkWrXpUf3qfSNyx8qvo_U_ppXeU3me9W&ap=44","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15v2n3vcv(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31666936659,dmn$partners-finances.com,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$933642,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/XAbx8FXwj877PTM6AaxfFw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452851549768-7306.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Regroupez tous vos crÃ©dits en 1 seul et diminuez vos mensualitÃ©s jusquâÃ  -60%. Faites votre simulation Gratuite en 3 minutes chrono !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":7,"cposy":7,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31666936659","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":6},{"type":"unknown","id":"5aa920cb-8344-3f4f-8450-13a0813e2d98","instrument":"4000010520S00001","is_eligible":"true"},{"type":"unknown","id":"75dff1df-0420-3e22-a2df-642a8d5b358f","instrument":"4000000460S00008","is_eligible":"false"},{"type":"unknown","id":"8d2fb011-cf9a-3ef5-9fd8-1ac77bb3c7fb","instrument":"4000011960S00001","is_eligible":"true"},{"type":"unknown","id":"bc91387a-7c7e-387a-9aa5-85e77d130fa3","instrument":"4000010850S00008","is_eligible":"false"},{"type":"ad","id":"31687619320","title":"Besoin d'une complÃ©mentaire santÃ© ?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=Nol27m4GIS8wzXYrtPXm1Tx6gymlKMSKb0JrpUWx2t1g3C_ckH05hjXDtsEUq__5RqQOdwSTezg.3ZCsUTRkfurw12_LbYmAruG31UUGkQJsO2HBtXKMRuTl4VbHFO8xLUreVUneZ.hPDRqyjTor9.mfKSd7SyA7MtSH_kM8zeQy306_vNd.ZCRNhZOAI78JotveNlLrPbyaoqGZvhg4iXQbFw7326hLuBiLxJflObT8uwysFpd15Ie2m6KUxNW9t4ERSOANaQAUOCRs1ezy2c3sZnv7S07w88H00ddbUi6etkj3s0REFHuDqKBQOeD_Qek3Cv1WJXulmoFlVPYjcmTUGnyU9KXia8rgnxwoADOY32ZOGFxZekkEscie4ATatnLeAiz7MDEN9tJj0Xvd_XYTXnl7rRDYyXPn5isT2qPfPPml.6LalRqGIcRR38sT78nLJEJoqG7dQg.OUtJfnxeaNfE-%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F299229985%3B126242518%3Bv","publisher":"Axa.fr/SantÃ©","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=v3.8cVwGIS_bl0_EmE3K8Q4690q1eVqr9hZF9WqG9GRHqCG9zzdcvoNZ6FzePIdcLkLYL4Z7.eMNBbEXx175TVYt34d3KedoO49M_jhs14tQjyg2DvquCADuLDA1rzA8Y8BqW6IN6jM0ZkTdOJ5Q_ZWYvV7KOIg810Fcdb0_T.lE7ibrz49BQMMwzlnbvF41V2wHDNW7klkQMFp1lDTJgXB6yWTjOLZ4JJ0qkKqX_B1o0TwJsGt3OLSyOG24Ht.b7QajlmGVa6.Y5syKBvzeLlhplIlBFqysNEBPcBsw0zfOoH7MW_jwACc08QTZ4ePB2n2a0V3RvjSsuEGq4UaREMECADyzBsDT1of6m_VAhF8DrKD4L5pDWEpJypqCKMGuJol63iBT52NutaI906O53pMYHjJajvrv2uW4IQ--&ap=49","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15go07pna(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31687619320,dmn$axa.fr,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$984869,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/DQW5pFClqmlaGARpV0unxg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452592674401-1596.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"A partir de 8,10â¬ par mois, choisissez votre couverture santÃ© sur mesure au meilleur prix. Obtenez votre devis gratuit en 5 min !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":8,"cposy":8,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31687619320","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":7},{"type":"unknown","id":"f2cfa485-6a43-3d17-9982-4695c1d4ee51","instrument":"4000009460S00001","is_eligible":"true"},{"type":"unknown","id":"439cb3d1-cbf2-39ef-84a9-07669d396cc6","instrument":"4000007770S00001","is_eligible":"true"},{"type":"unknown","id":"c-ab6d2755-a652-4b43-afec-ad45f2f992d6","instrument":"4000007260S00001","is_eligible":"false"},{"type":"unknown","id":"4f75fac5-88b9-36dd-af49-f532d2d044db","instrument":"4000011440S00008","is_eligible":"false"},{"type":"ad","id":"31638159510","title":"Nissan Leaf : 250km d'autonomie !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=x0N3iNUGIS.qsCLaTyuUBwVNfbvdxOAGva7pydnztf.PKa3QxgKaCtnAX_swMwPrj1uUpwUtTYwUOjQirGwW1NHAt6xe4hZzRoUv.QvuUgakdiQu6LnPek3CuVhlLUEzHMtdyaJed5b8T8xywQCmfPsarPVbQ9v3t45pEFtsRNWo3IpEWB_DeRqk9c59hommVvVwXtPrclBsFHQZwTcr_2HoS3yzWtZRqTGfQp.Web9.eCdUcUTCRsQsR0E.myd1PXHP1v31zFqFz5TgemzOQhgrVpWgrdHOD_U.qCDtEwrw0rlnJKl0nFCVb1pkIxZ_YOJOYWUENz07nsXPlDBxj_j4dDrbe.sahfWkecJu1P3.b6g1LaZmvcGKcE8YiHN_x7iFfOLg.SK0RokVAtCxNtosxGLruDNAnwg9EIH4ZtS8Afdf45kd8t.9h1GPuwptfyIAuiQjk1aeYfTmZ8Jggd1N2goE1TYUbCk8PUiYejZunAsqnraeZPNv64bb3HoWmDOp.ll4UdN5132YJKX78TAZ3w24BTdHWscrATwXmqL9rCZt0_g-%26lp=https%3A%2F%2Fbs.serving-sys.com%2FBurstingPipe%2FadServer.bs%3Fcn%3Dtf%26c%3D20%26mc%3Dclick%26pli%3D16235306%26PluID%3D0%26ord","publisher":"NISSAN","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=YJDYsCUGIS88cF16ssp5uMKLpIIO6keCT3xMExGs8ye3Buq_4A9.L6uZKgLtkJY8dJJJPJaI_8Wav_eYPLf0F6x38E_kb2etMKYxZvOdSjeK26FmFcmC8cLmHHvXzL9udsuRmIAvgkgPT94wHf0A9_JQC8LknrxHJZJd49t8A7Q7U4ISUtHBfPufZ_VimKwOMUjIHqHQdKyBT_QYLxjtx.fY7rdrM29ZWCYMjcn9U7MdQ3D2OMC6r.H2L7MQBTT1HvHxZG2QOHhqiCVkXm5Akj4BOmOvemIZAys9q0Sa5ubysM9NcEZaY8eBDaqKbnSlfLEOcsaS1jU4kkD4JyHvVnI990Spe3rbQylZSiW8pDbKRg9SHyJNQtjpKycc0ZX4hlVpI5DS_NQ8&ap=54","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15kl339o2(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31638159510,dmn$nissan.fr,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$1155337,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/ZtukHpJ4f_ZjQFNkGNyusQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1455197173903-2513.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Avec sa batterie 30 kWh, la Nissan LEAF 2016 permet dÃ©sormais de parcourir jusquâÃ  250km avec une seule charge !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":9,"cposy":9,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31638159510","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":8},{"type":"unknown","id":"36107ac0-3bd2-333a-a59d-0b89f0696d8f","instrument":"4000012810S00001","is_eligible":"true"},{"type":"unknown","id":"aa5d5f59-c31b-3f51-ae76-98f04cf4cadd","instrument":"4000005910S00001","is_eligible":"true"},{"type":"unknown","id":"d4296f7c-961c-3683-94f0-4be0b6192000","instrument":"4000007110S00002","is_eligible":"true"},{"type":"unknown","id":"aa5d3d39-885d-364b-adfe-9157835c1c4a","instrument":"4000011920S00008","is_eligible":"false"},{"type":"ad","id":"31698410405","title":"DÃ©mÃ©nagement ÃlectricitÃ© & Gaz Naturel avec ENGIE","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=2C7vNf8GIS.o6_3xAtwRZZ348k_9hcEQJ4dB._DPTcqoYE7gAJyPyoHlOcSOKcBDGT.QCaLOXsICk28Fp7aIAyOM1g1fzaZpJJRKySo.ojLp5r9k0C7YjfP1MC0FFto8nfks9gO2BkX.Nxvcg3pedAZQ7Ejopq963sLIoNRKboW8OJ9tPAMO5k07i6ztk2luUDzxMJcvLiZ.wYjO.NCs0exhLaiFSs2ECtJr_xCr9HCTdaEdL6RmZggvRuiz9Vh2XJOJNqwHq_sR9u1gjQe3QmKKq8h_LFmd0Ou83HHPiEHopZZ63hU_Ra_UI2kKOACQYxOaiDKmTAj1NqOMIluuVQdIPThSC.qe...isB4nU1Eat26x8WxfCAMaSLR.DAY7q0T6t6BjJZejLFXgWvxxK6DmwnZEAM0mozAU4LGDdZs2QwDPluV.QXRiyEB7rzx70agDqVfIVKVMxgfXmpzi1cCEBA7u6wVT0UCj2weimFHrk30U_AyesrHD6ec53_c1tFfPXgQkk9P4SLGHSj5ActtH5DrPwm_gvpAm9O3CVZG6psk4po3vkB8NMhPSYyvWNzuZo0saSIl20honr8asXARiuBC2dTdJ_RKWLEjTXkRPebk9U8w5phdRoLZ91SvUGRquGVp5HdmF_2cRBwNe%26lp=http%3A%2F%2Fsatellites.particuliers.engie.fr%2Fdemenagement-gaz-electricite%2F%3Fns_campaign%3Ddemenagement%26ns_mchannel%3Ddisplay_native_prospecting%26ns_source%3Dyahoo%26ns_linkname%3Dgemini","publisher":"Engie","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=AM663bYGIS8lG_p56MFsWhZakP.U4HSmXJZinXgUwPu0SvMs7duPhFyPAZHGyLegJtY526DSAxxVh2UMM9tQIrsHnIDtZvSPFRNVs609XO6Skti8V.vbgqlw4T7aTZ9tQ.YvjU5l1HRkpt0JGfyMFXABa45eQuIFWSB94cly4gH1gEuF2MviaD4Qek9Q6mZZOWHqLxl0H8MbyM6QFPEN64CAAEI8VnijBoK.T5Mn33S67Q_LVu7PZLlZAAQO_3ZdijPiQdE3ca9RU5P8_uCfrBUJJ0z7ci_217Kh23rVJ9xCq0wxYk06B.PuRPlkEwJoU.Dn.8kxpbE8ByuiR92Q5fuo2hcRB40UMn9HYCPMQW30CQVGHVbWrALDdESzyTS6j6xvrKcyle45&ap=59","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(16b35anu2(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31698410405,dmn$satellites.particuliers.engie.fr,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$1135769,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/O48Zr0BaGQlj2a44lC1VbQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452843802937-1264.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ©marches SimplifiÃ©es & Rapides. Demandez votre nouveau contrat en quelques clics, rÃ©ponse en 24H ouvrÃ©es !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":10,"cposy":10,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31698410405","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":9},{"type":"unknown","id":"2e5f88d9-3e22-3364-a090-1fa40e7af9e2","instrument":"4000001870S00008","is_eligible":"false"},{"type":"unknown","id":"7d8957db-de74-3ed0-b634-534b1c28038a","instrument":"4000006510S00001","is_eligible":"true"},{"type":"unknown","id":"0a64e0ce-f973-3409-9451-bc8c4d31301d","instrument":"4000009560S00008","is_eligible":"false"},{"type":"unknown","id":"ec48199b-798b-3979-aac2-fc8d981387c7","instrument":"4000010070S00001","is_eligible":"true"},{"type":"ad","id":"31230882749","title":"-80% sur tout le bricolage !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=jrYvKokGIS9E.svoVIBegpH5D.rouqBv_RURRzTlE4OCcF407L8yOK8dmvHVFcCB49I5MPSaGerfVOpA_r6OtOfcLX11v2V8NeKaRmHc4SPnMJyOWzPizsiQGh7WWDbqJsK5pRDEEs2Ynuh55d9OF3yIViCES.cyV_tfxTNS6csbRv7bbjJ4J4RQWsaxwm5hLn6orQu2zNCLcn3yoJozbnop3n5K_hzgLiHA8rExYl3qyWtX50ktQV0P85e4ZXQ0NefASHd7J8CHLlyM3LF0nH1ipJAUwkTCut0oj3N32doqNJ106lL04Mp0g6uAPgZwGlyu9Xin3p298kXkg5YUXCxLKNcdI.UHF.O99CCx15FI6oy09qDwCKCCctCqSzCSrx48nyP5tcPhkvX0kK38jk6Lxc4od1lszWaWIPeYfBz8gV0YdnDH16s7279sZYeCE6zm9hvIiEvpjNA.0qfU2vMhn0ifbp5BVib3ySSnMytkJcfKbmlfiuxviWAxMa2r3pg1ca2STa6fYjW5_ZGX2ILj7ogS.jMdXpDe_uTwfjeSq2wep3W308jJITrNFXcLdyt4YBwcDa6akp68q1v.HhpvCooahvvnvIDs3.NbYWC8Ry8jJjZwDfj6U0Mqk_jF7KdwT2XjsEE7TBY1N0fiAnKlSEq3zlCuSXUrcxfc0MWHLckfyKRx.6kgICr4rn65%26lp=http%3A%2F%2Fwww.bricoprive.com%2Finvite%2FMjAxNS0xMi0x%3D%3DQbvNmLlZXayB3bjlmciB0bjlmcitSaulWbld2bvhWY5tybm5Wa%3Futm_source%3Demaildedie%26utm_medium%3Dcpc%26utm_campaign%3Dyahoogemini-brico%26display_hp%3D1%26register%26create_account","publisher":"Bricoprive","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=EG9VKAQGIS9cQbfbox4LhmtcI7.JfQ4Rq4GKkptVhTuynDybO5U1u6MT8N.1uR9iG9jxgiUK54KNLvop.JsGxs1pBLrHnvDEM3EWXteC.8_XC0mexasbp8v5h3YRzb35FKCOl3ZCYDkSzPDRwTkZJ_ketUKEUZuRRE.dn7ARd8uivNzM1tdbeASEakYJLqkmtxM67hivdDCBL1VFElejqWAaaIaLQKE8fd1Au7xiXo0zzpg2ALdelw21s7AtmkvH3m2CSvsj9MnS6rdTVE1KonU5X2CWMwgLpv8ekY2eKjocf6iOLMFBHmgeBJE8baWkXQYyvmDVzy9fbcs8V_kz9LoCLYyJCyy4Qdq4c1j10pVLaSpL0hyppHixuveyojfbGc1k._8.eWm_&ap=64","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15ntlu947(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31230882749,dmn$c31230882749,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$1166620,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/yRRwVNqMGHPZI74ZYNquXQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1446119101064-2724.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"40 nouvelles ventes privÃ©es chaque semaine, pour rÃ©ussir vos petits et grands projets comme un pro !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":11,"cposy":11,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31230882749","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":10},{"type":"unknown","id":"f7e367d6-04f7-3bb9-b39c-6e0405e7d6cd","instrument":"4000000720S00004","is_eligible":"true"},{"type":"unknown","id":"07e10e94-218d-36d4-a39c-ce8e8b55e60d","instrument":"4000007370S00001","is_eligible":"true"},{"type":"unknown","id":"47ccbecd-ae83-3226-9a7a-e6f8c45a2db0","instrument":"4000010800S00008","is_eligible":"false"},{"type":"unknown","id":"e2a78327-266f-350b-b70d-1243bda230c2","instrument":"4000010350S00008","is_eligible":"false"},{"type":"ad","id":"31712696752","title":"Trouvez votre prochain vol avec Easyvoyage !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=RZc2myQGIS9QNTS4rY.8vyF1MaO2HBxd29VQNETBp007IZwGnL67eyJJ8TnW5OHRYJJnEApJf7V3WccDOzXSslsOsxs85sW3uBExpE_Rp7nN09ZFsVqCaFHxYCgVsUeunGS3KNwTagzaPgEJhGEzahWivh.0bBfXHH8BjYN20.kIVsCSfGJepUHNgHR28_ge6sfhKlr5Izfjqj3vV4_NDr6UuatXJP1L6Di6S804186fPGpV_GuG6_vjhsVcNJQWTUEjh9zz.9AiXD_uVA7EeckxGh5pCKeS6rN92JDKC0WXcWZ4fhnSgX3LlzCnDgZQyKhTKmWHcrxqoeAmqmhgi88T2i3jkiCflYRpjkz9s7itQi5r5WyJbf5GmAG1LCBMeYJgUP_nQOl2MNAWKXMRpuaQRUyt0pTaB9nAtuOzk7BxdVpmILaOPjjjOLSNe2BAFuBcoD.0nZaT7mCRiCCDpGyfAG_sIF4Lhg6YItUzZWLqZ42dF5BZ6wjt8lMdlqnRJFRICBHw543t3QvMwlYCtuQKtxDs5UdoChdYO840Run07WN4sQG9CS8lCvTjjKSraShFm5yR7SylGjp.H7G0oF7BQTSoI41X2VxRZEj6m4RswA6gRK_RMN1nrEKOVwBbb24eJbZ_5SbLNrBqDmE-%26lp=http%3A%2F%2Fwww.easyvoyage.com%2Fvols%2Fcomparateur%3FclientId%3D609%26utm_medium%3Ddisplay%26utm_source%3Dyahoonative%26utm_campaign%3Dvol_comparateur_ouvert_cpm%26utm_content%3DVALISE_MOO-13880072","publisher":"Easyvoyage","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=AgKPd2AGIS9.qyM3LgdDDVztX1MbLmxeQhXDhHH6G56JXMhp0i0SS4v7CiDyMFS5665OcuYxQPFLxdgWAuhevzNhdj77.jxH7Ao.yUdberFju9UfD7HuU73K7pQ607ZG8BLJ1j.GAtPAW8Te_RYVAr4zNNXi02PL6lU8ly2WdV4JGYTuqr4ZMHqNYJaDw1FYCYb8W8Bh7ZMIc.tzN2zUsrw2kNNyOg_92IBkgXZ9Cc34GXq8kLY0xwz5KArYUEPOq91tInzmhprqcYhVgp0irzZlpWlmYGa.b8jibhWSt2erBcfxDLu6wpk2YZOmOqW3gXgQW5v5bCJNfkVG6DsRtuj3pBqUZHXU6n3HZ5F96E4aEVdv8XxnMZyT3kTYkk.sqy6wHPVl5xNnog--&ap=69","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15ofiivlm(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31712696752,dmn$easyvoyage.com,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$935245,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/M7G_vOqrEC0kpdY1GIV9Sg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1455270979983-5932.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Recherchez et comparez tous les vols pour vos prochaines vacances avec Easyvoyage!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":12,"cposy":12,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31712696752","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":11},{"type":"unknown","id":"a83efb67-bb75-3368-b706-7728c21a9bff","instrument":"4000006200S00001","is_eligible":"true"},{"type":"unknown","id":"c-956851b3-e8f7-46cd-bf82-af4d78305c46","instrument":"4000002020S00001","is_eligible":"false"},{"type":"unknown","id":"2a6b70e2-41d5-3365-9ab4-5a1861b3caa7","instrument":"4000007630S00001","is_eligible":"true"},{"type":"unknown","id":"c-970f0240-d614-44c0-8d07-e5a2fb57f12e","instrument":"4000001960S00001","is_eligible":"false"},{"type":"ad","id":"31672352907","title":"Faites des Ã©conomies grÃ¢ce Ã  votre assurance auto","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=.IXXOAUGIS_5sxt4gZOCtwVXYei1T4rU90wDgFJ.wYGdSXZcGtts3CAdpbuncKQb1lPf31Lv93ZE2eaEtLVDTerjBApB7gFJZC8p.iPqSvPLu_.xi.G6.TUXgYgzdaqJiUhVu.wu3jy7oKPAtRxAgaheJ19lSLxlLW.eCURbcfAYC.RrGKu8a5KHCENiGY1CEADo6OKwhC_s1TiePxiAU6DgdglVB_4pRIfAi8L1WqE43F42nSBUqY6yCofDCfKeOusOdl5eOx36nLHly02A1Gigpq5wiR.nQSHhfjbRexCu8UYMONJwPpB9q8KA.m1MwlEDZL24wgYoH9cNZvLWTS1uKBPD9aSAE97sUDECOqdB1fAowzt95M6wNLfBEOr6SfQZ09anqowOXPZ8QhFqCJcFkCuAI241hPJomPp3wstGoYKcT7Y6X4AZNI4Azj1ksOqtperi03186BTc6hg0EGwZavdrhH1_2ajbYwqcjPsCNGNQU6bGO5eWPGBhyBQsDpKRVwh54hY..oc.z2pB_6fpdg.Tz7SBArNdBrfQ0eVUEkAo0t780XT88iKwfgOoW.RfO2Q_ErmeJJLdiZ5Uml7mwcS2H4RO_zAPs9EjT3hVHagT9jLgR.o8LZNbs5N2_l8MeVJ6iA4FqZaYkHAvGq0-%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN3021.140905.YAHOO.FR%2FB9375649.127505902%3Bdc_trk_aid%3D300530271%3Bdc_trk_cid%3D68290868%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D","publisher":"Groupama","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=suO5voQGIS_DgtJSjhgzp0iR.MW0Y12ZXgXs9e07fHw6yP98baFxWR.IHyt0XqtUQhKe9YJ7NOAbWKD80gRHx.kr6VxqVjk4KvAE5Y3r_eFFfInaBUBcQM4ea3tV.HIQfdZ56kT3x0G5uYf0J1dPdfEIU3agJaA3N7shEfNyuGpNZbKw7LeGjPWZvLN5noQ48kEBZQGMtD7oGdmLNLLTtTny9uGElU_CTDIv4PXfqjqNFTKOmItmbwnEHbvChE5XGVCf70Ks6PmC0ozcFg0kTseV1etxA9mlp_OK0oAZFKTj76fMUvnyIengs7bo7UUdTOtpnQ70vz1ztI8XPRpiBPq55Nkt4QeEmj00xTg.Wr62HZCuZaDV_tkQWn87FMOAygxu20xadOFqyA--&ap=74","imprTrackingUrl":"https://ad.doubleclick.net/ddm/trackimp/N3021.140905.YAHOO.FR/B9375649.127505902;dc_trk_aid=300530271;dc_trk_cid=68290868;ord=1455471493;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=?","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15ng4rlha(gid$b8ca5882-d341-11e5-b414-008cfa1ef534-7f41da555700,st$1455471493151000,li$0,cr$31672352907,dmn$c31672352907,srv$3,exp$1455478693151000,ct$27,v$1.0,adv$1127964,pbid$1,seid$4250754))&r=1455471493151&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/.RsV5wbRqXDCKETeVFUhHw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1453137678600-2231.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"En passant chez Groupama, les nouveaux clients Ã©conomisent en moyenne 149â¬ sur leur assurance auto. Faites votre devis en 3 minutes.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":13,"cposy":13,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31672352907","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":12},{"type":"unknown","id":"d83b7455-86e3-3391-a0c4-faf0fb090d99","instrument":"4000000830S00004","is_eligible":"true"},{"type":"unknown","id":"4cc914f9-f2b6-3dd2-837a-8b24401305aa","instrument":"4000008240S00001","is_eligible":"true"},{"type":"unknown","id":"984cd822-ccd5-31ff-8ca8-0f6ef538a0fb","instrument":"4000009840S00001","is_eligible":"true"},{"type":"unknown","id":"1d820550-83ce-37d4-bfb5-58ab6381bed3","instrument":"4000004130S00002","is_eligible":"true"},{"type":"unknown","id":"40c3f488-36ed-36cb-a576-29cf1c58d6ea","instrument":"4000013730S00001","is_eligible":"true"},{"type":"unknown","id":"f514c932-7530-300a-be28-aba8106dec22","instrument":"4000009260S00001","is_eligible":"true"},{"type":"unknown","id":"c-7ddea504-77af-4334-b3f5-a90e79483ca9","instrument":"4000005100S00001","is_eligible":"false"},{"type":"unknown","id":"1a79bff4-a35f-3d78-a567-aea1a1f07461","instrument":"4000007410S00001","is_eligible":"true"},{"type":"unknown","id":"c-2c88f580-4f44-49ca-ad23-c9852bc8193a","instrument":"4000007070S00001","is_eligible":"false"},{"type":"unknown","id":"fef2dde5-313a-325b-9c1f-7626f2319dac","instrument":"4000008500S00001","is_eligible":"true"},{"type":"unknown","id":"89a4a69a-288d-36d4-9f0d-6d3afaefe7ea","instrument":"4000008720S00001","is_eligible":"true"},{"type":"unknown","id":"fbef3c72-26de-3734-8eca-7e3507485f03","instrument":"4000000680S00004","is_eligible":"true"},{"type":"unknown","id":"ec8c5d2e-3631-31d0-900d-cfd1b04b4ad8","instrument":"4000009270S00001","is_eligible":"true"},{"type":"unknown","id":"bc305b97-9236-3cf3-b0b2-613cfcff423e","instrument":"4000007510S00001","is_eligible":"true"},{"type":"unknown","id":"c-eb49c668-2cec-4cbe-a809-afd3514821ef","instrument":"4000005400S00001","is_eligible":"false"},{"type":"unknown","id":"34c6fc93-1eb9-3341-9b62-24b8236ef373","instrument":"4000010520S00001","is_eligible":"true"},{"type":"unknown","id":"b6063c97-3068-3a5c-b552-5bc99b9121aa","instrument":"4000017620S00008","is_eligible":"false"},{"type":"unknown","id":"b4ca7b85-ab9b-33b1-aa93-c03d2dbfbf5d","instrument":"4000011660S00008","is_eligible":"false"},{"type":"unknown","id":"6a45b16d-7902-3f2c-ad48-d70b839906b8","instrument":"4000007770S00001","is_eligible":"true"},{"type":"unknown","id":"17f0ef5e-0c3b-33fa-bd0c-451d3f8b9b23","instrument":"4000006620S00001","is_eligible":"true"},{"type":"unknown","id":"04ed83c5-ca26-3bef-ae6f-6c083787f4f8","instrument":"4000008140S00001","is_eligible":"true"},{"type":"unknown","id":"c-a4e1ba04-47e5-47bd-8e3a-215a795fedfe","instrument":"4000000570S00001","is_eligible":"false"},{"type":"unknown","id":"33985230-7b87-3eeb-a150-d36eaa86d7ce","instrument":"4000005470S00002","is_eligible":"true"},{"type":"unknown","id":"c-39cbab35-7c2e-4dbd-bcba-83fb37dc36fa","instrument":"4000004890S00001","is_eligible":"false"},{"type":"unknown","id":"c11a36f3-5157-3c5e-bff0-8537ce735e7e","instrument":"4000005500S00001","is_eligible":"true"},{"type":"unknown","id":"c-cbaf396e-bf97-4e97-9530-24381809ca20","instrument":"4000003710S00001","is_eligible":"false"},{"type":"unknown","id":"c-ef7cf526-1cbf-4d54-baf0-d089d78d8a83","instrument":"4000006300S00001","is_eligible":"false"},{"type":"unknown","id":"00a9fabe-f3b7-3115-b47d-fc9cdc4ef771","instrument":"4000006040S00001","is_eligible":"true"},{"type":"unknown","id":"c-8f9dc62d-d02b-4992-89dd-4d7c7898f043","instrument":"4000003400S00001","is_eligible":"false"},{"type":"unknown","id":"c-35e2e36f-8972-4bab-b962-52ab372e5117","instrument":"4000000690S00001","is_eligible":"false"},{"type":"unknown","id":"84959d09-0f39-31bd-a22f-cb5cb061dd34","instrument":"4000008790S00001","is_eligible":"true"},{"type":"unknown","id":"c1ba851a-807f-3b40-8002-1128be12f054","instrument":"4000009670S00001","is_eligible":"true"},{"type":"unknown","id":"c-e657cdc8-a016-46ae-9665-9ed645a32653","instrument":"4000002180S00001","is_eligible":"false"},{"type":"unknown","id":"050c9a73-f418-38b8-9d85-574afa3a89e6","instrument":"4000000890S00004","is_eligible":"true"},{"type":"unknown","id":"c-e6152e6d-43d3-4525-804e-9dddf75072f5","instrument":"4000000470S00001","is_eligible":"false"},{"type":"unknown","id":"5c8608f8-dee3-3ef9-93b5-df8b5a38fdd1","instrument":"4000008270S00001","is_eligible":"true"},{"type":"unknown","id":"c48648a0-4e50-3df2-a7f1-c7f7f2e3a8e6","instrument":"4000006720S00002","is_eligible":"true"},{"type":"unknown","id":"13b1530e-01ef-33cd-b1df-41849fbe1dad","instrument":"4000016370S00008","is_eligible":"false"},{"type":"unknown","id":"c-cc122724-eff3-407b-a7a9-3e6060cd1750","instrument":"4000011590S00001","is_eligible":"false"},{"type":"unknown","id":"33c5ec68-abc6-3c31-8213-0a1b28300039","instrument":"4000007380S00001","is_eligible":"true"},{"type":"unknown","id":"bd37428b-d704-37a1-bd32-11046a561541","instrument":"4000008010S00001","is_eligible":"true"},{"type":"unknown","id":"c951a1a8-e6bc-368f-a11b-7b697505bcc6","instrument":"4000008680S00001","is_eligible":"true"},{"type":"unknown","id":"93b469f9-fe94-39a0-a2f5-fb79ed73be06","instrument":"4000010640S00008","is_eligible":"false"},{"type":"unknown","id":"16b97e6a-5aef-3da0-bf27-ca4012bbcf06","instrument":"4000012750S00008","is_eligible":"false"},{"type":"unknown","id":"d456f27f-aca6-3eb9-8531-725bb2be2864","instrument":"4000005840S00001","is_eligible":"true"},{"type":"unknown","id":"04ca8ed8-68e6-3633-988b-e5db5484414d","instrument":"4000006240S00001","is_eligible":"true"},{"type":"unknown","id":"be8556c1-ee5c-3470-9db9-4a1280b6c6c0","instrument":"4000000870S00004","is_eligible":"true"},{"type":"unknown","id":"3fb70505-da75-3ef3-b0e8-73b5ef4afb38","instrument":"4000009590S00008","is_eligible":"false"},{"type":"unknown","id":"8ac41ae2-eb08-32bc-b049-f6dd7ead4568","instrument":"4000006570S00001","is_eligible":"true"},{"type":"unknown","id":"c2b83a98-677f-3238-b417-7df2cf14dbac","instrument":"4000009140S00001","is_eligible":"true"},{"type":"unknown","id":"5a904d1c-f928-3a39-a62c-ddd22a2f6853","instrument":"4000006210S00001","is_eligible":"true"},{"type":"unknown","id":"70538fc2-31d6-3031-910f-75b829515b3c","instrument":"4000013370S00008","is_eligible":"false"},{"type":"unknown","id":"c-be90d00e-57c2-4376-a49d-9a60f1a96772","instrument":"4000003170S00001","is_eligible":"false"},{"type":"unknown","id":"e7c8d973-895a-3fa4-b4e8-dd608847addb","instrument":"4000011440S00008","is_eligible":"false"},{"type":"unknown","id":"f5e7bd71-aaa3-35b2-866a-3ab678f80c99","instrument":"4000018890S00001","is_eligible":"true"},{"type":"unknown","id":"2fc5ed18-e684-374e-b121-bd6fb031c141","instrument":"4000006190S00001","is_eligible":"true"},{"type":"unknown","id":"3771f651-b4cf-399f-9b86-c5bdb3264f37","instrument":"4000007570S00002","is_eligible":"true"},{"type":"unknown","id":"c-0b0ea56d-c7d7-461b-a337-9e627e69c953","instrument":"4000001900S00001","is_eligible":"false"},{"type":"unknown","id":"c9d6ccc7-651f-3796-a8c0-8c1f5900ba10","instrument":"4000006260S00001","is_eligible":"true"},{"type":"unknown","id":"e46c9459-f0cd-3ae8-b281-e8242795d205","instrument":"4000007000S00001","is_eligible":"true"},{"type":"unknown","id":"7a7ab474-8b3d-3aed-b356-e5c5e4c8ae91","instrument":"4000010090S00001","is_eligible":"true"},{"type":"unknown","id":"62039bf4-53e5-3fb3-ae02-9c7d8ed32827","instrument":"4000010960S00008","is_eligible":"false"},{"type":"unknown","id":"b561fe7a-ac36-341e-92fa-ec0c5df54f56","instrument":"4000006090S00002","is_eligible":"true"},{"type":"unknown","id":"cd474db5-1721-37f9-b67e-699aad2c12d1","instrument":"4000010800S00008","is_eligible":"false"},{"type":"unknown","id":"c76de0cd-ec87-37f7-9abd-221e30a2d6e4","instrument":"4000009540S00001","is_eligible":"true"},{"type":"unknown","id":"51e09536-1e1e-3e77-b67b-33d58936965f","instrument":"4000014870S00008","is_eligible":"false"},{"type":"unknown","id":"c-3c9c26c6-6305-4395-8ee7-cfb7a991b2be","instrument":"4000005840S00001","is_eligible":"false"},{"type":"unknown","id":"ce1578cc-961c-3e7d-8708-1831b674c575","instrument":"4000001590S00004","is_eligible":"true"},{"type":"unknown","id":"b1a0d4e4-9d21-3aeb-bce7-ae583088fee8","instrument":"4000006380S00001","is_eligible":"true"},{"type":"unknown","id":"e485989f-65bf-3c57-a48e-df79edcac69d","instrument":"4000014210S00001","is_eligible":"true"},{"type":"unknown","id":"4284302c-bda6-3677-acd2-e1f2ac068786","instrument":"4000011990S00008","is_eligible":"false"},{"type":"unknown","id":"275187d0-8005-355e-837f-74038d2f3a1e","instrument":"4000005470S00001","is_eligible":"true"},{"type":"unknown","id":"71980a3d-5ef0-3810-82af-38684d54aebd","instrument":"4000006550S00001","is_eligible":"true"},{"type":"unknown","id":"6c134afc-1b7c-3f0e-8ef6-5911eacb4148","instrument":"4000010100S00001","is_eligible":"true"},{"type":"unknown","id":"ae228d6c-794d-3ac0-9dec-d47786204ba7","instrument":"4000007600S00001","is_eligible":"true"},{"type":"unknown","id":"c4fe41fb-4486-338c-b479-91bb31ea2135","instrument":"4000007800S00001","is_eligible":"true"},{"type":"unknown","id":"f8cb98c3-9b59-3233-9d1c-e1a018cf3e42","instrument":"4000009140S00001","is_eligible":"true"},{"type":"unknown","id":"f30e71cb-01c7-3fb1-a34a-addd1a1eb83d","instrument":"4000006310S00001","is_eligible":"true"},{"type":"unknown","id":"18087c6f-289c-3956-b61a-b37b27cceaf3","instrument":"4000001050S00004","is_eligible":"true"},{"type":"unknown","id":"ee622d70-960d-3cb8-b11b-4fa602cdfb22","instrument":"4000007340S00001","is_eligible":"true"},{"type":"unknown","id":"9b28b791-7342-3573-8222-cf5aadc5e4cf","instrument":"4000005610S00001","is_eligible":"true"},{"type":"unknown","id":"c-e16a058f-5b78-4da6-91f8-9b5328e683b2","instrument":"4000009640S00001","is_eligible":"false"},{"type":"unknown","id":"7948c264-5f50-3a3f-b75b-10a9c829d518","instrument":"4000007390S00002","is_eligible":"true"},{"type":"unknown","id":"f6d0f299-ea9e-39a6-9b47-97a389522b23","instrument":"4000006010S00001","is_eligible":"true"},{"type":"unknown","id":"982042d8-5417-3dd9-ad44-8a833918e1d7","instrument":"4000005470S00001","is_eligible":"true"},{"type":"unknown","id":"c95030c1-1356-36ea-8e15-cde67dfce37a","instrument":"4000007200S00001","is_eligible":"true"},{"type":"unknown","id":"9a86e880-780b-3f0f-bbb7-3532ed3b428e","instrument":"4000005740S00001","is_eligible":"true"},{"type":"unknown","id":"29a944e0-dd7e-3347-b74e-7ec829eb1ca9","instrument":"4000012450S00001","is_eligible":"true"},{"type":"unknown","id":"285a2e99-c46d-3ae6-b695-b0c26042df9e","instrument":"4000008970S00001","is_eligible":"true"},{"type":"unknown","id":"5e6ab731-41bc-3ac1-882b-91aa4774d76a","instrument":"4000008470S00001","is_eligible":"true"},{"type":"unknown","id":"73a9103c-75a2-35f3-b8cf-6df9c3b492e7","instrument":"4000008019S00001","is_eligible":"true"},{"type":"unknown","id":"ecaab5de-82f0-387b-9dce-887f00cc8a3a","instrument":"4000008080S00002","is_eligible":"true"},{"type":"unknown","id":"9c4a05bd-1f29-3b58-9552-3134b39a3bcb","instrument":"4000008120S00001","is_eligible":"true"},{"type":"unknown","id":"f46918ec-13dc-3674-af65-a0330d667750","instrument":"4000007700S00001","is_eligible":"true"},{"type":"unknown","id":"bc1cf848-21fe-34e0-8091-8ddc5ba85b35","instrument":"4000007770S00001","is_eligible":"true"},{"type":"unknown","id":"e223d42f-5859-35f4-850d-f22836bb168d","instrument":"4000008590S00001","is_eligible":"true"},{"type":"unknown","id":"57471aa1-7673-3f97-9e00-8c0c0abdc00b","instrument":"4000005770S00001","is_eligible":"true"},{"type":"unknown","id":"c-810d8e90-e516-44fa-82b0-e0ecd9999469","instrument":"4000000560S00001","is_eligible":"false"},{"type":"unknown","id":"c-804dc4e5-4ac9-4a5b-91df-a8f47fbf6c23","instrument":"4000002350S00001","is_eligible":"false"},{"type":"unknown","id":"63aa45e6-8c92-3bac-adb8-2d62777579ae","instrument":"4000001260S00004","is_eligible":"true"},{"type":"unknown","id":"e498823c-311e-3b88-92f3-c7d4e90e188b","instrument":"4000009890S00001","is_eligible":"true"},{"type":"unknown","id":"bdc1719f-74cd-3c86-b791-5f0005de7808","instrument":"4000014300S00001","is_eligible":"true"},{"type":"unknown","id":"6fcaf6e3-9bd8-35ab-a169-ba13cb53ac67","instrument":"4000007510S00001","is_eligible":"true"},{"type":"unknown","id":"96b5e616-cc69-3acb-8229-485a3b2d4a09","instrument":"4000005860S00001","is_eligible":"true"},{"type":"unknown","id":"37f2f58b-d481-356f-80bd-fd105bd25234","instrument":"4000007720S00001","is_eligible":"true"},{"type":"unknown","id":"46e18150-45b0-3fec-a568-f1660a0cceae","instrument":"4000005770S00001","is_eligible":"true"},{"type":"unknown","id":"535a12f1-f09b-3a24-81a4-75844bbae551","instrument":"4000009290S00001","is_eligible":"true"},{"type":"unknown","id":"fa23a2bc-70f1-3095-bad3-7dd9f7c2d447","instrument":"4000005100S00002","is_eligible":"true"},{"type":"unknown","id":"41b46794-30a6-3b96-b0f9-a0e0f741905d","instrument":"4000001380S00004","is_eligible":"true"},{"type":"unknown","id":"69d29038-aa69-3840-ab4e-7f433a5ade02","instrument":"4000009580S00001","is_eligible":"true"},{"type":"unknown","id":"f077346c-2490-3029-a4e6-a537f622821b","instrument":"4000007849S00001","is_eligible":"true"},{"type":"unknown","id":"94b8d4b0-214c-37fb-af9e-e6c28cd3c44e","instrument":"4000007770S00001","is_eligible":"true"}],"continuation":"","more":174,"enrichment_ids":[],"cposy":23,"stream_items":[{"id":"id-3594690","cauuid":"b7afc2cf-b7cd-333d-ae2f-f21f016f6dca","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ss_cid":"b7afc2cf-b7cd-333d-ae2f-f21f016f6dca","refcnt":4,"subsec":8499,"imgt":"ss","g":"b7afc2cf-b7cd-333d-ae2f-f21f016f6dca","aid":"id-3594690","ct":1,"pkgt":"need_to_know","grpt":"roundup","cnt_tpc":"Need To Know"},"link":"https://www.yahoo.com/politics/republicans-vow-to-block-obama-supreme-court-011758577.html","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"id-3594682","cauuid":"c6f5963c-4cf7-3861-9bd3-021648b72376","link":"https://www.yahoo.com/politics/what-does-justice-scalias-death-mean-for-pending-000410173.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3594642","cauuid":"af0e3efa-7bd8-36c2-8329-2ddea343e6b9","link":"https://gma.yahoo.com/woman-loving-last-name-kissing-booth-yard-valentines-181648270--abc-news-house-and-home.html","type":"video","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3594645","cauuid":"bece9969-1d9f-385f-98e9-587620019027","link":"https://www.yahoo.com/music/12-intriguing-grammy-records-may-194941142.html","type":"slideshow","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3594702","cauuid":"4a03c2dc-72db-3c0d-8ffd-72fefd948b25","link":"https://www.yahoo.com/celebrity/sports-illustrated-unveils-3-swimsuit-issue-045726283.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"f9bc79d3-84f7-31c4-a5ed-d3f76241b64a","i13n":{"cpos":2,"cposy":6,"bpos":1,"pos":1,"ss_cid":"3c0ad7b7-f487-42fa-8e07-08f29cc6328e","refcnt":2,"imgt":"ss","g":"f9bc79d3-84f7-31c4-a5ed-d3f76241b64a","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"http://www.vox.com/2016/2/14/10988380/donald-trump-9-11","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"1355cd52-a446-3147-8f4e-a5c46ea086ff","link":"http://finance.yahoo.com/news/donald-trump-confronts-audience-another-025035298.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"b06cff58-a3cd-3aa9-a38d-91c6bd4d7526","link":"https://www.washingtonpost.com/news/the-fix/wp/2016/02/13/in-2008-donald-trump-said-george-w-bush-shouldve-been-impeached/","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"31674818529","i13n":{"cpos":3,"cposy":9,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31674818529","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=aLrGPpUGIS.WYzCfDC60U6dhBIx37qzgH8iejahB1phrIdNG1qFl.ul7Cyx9Ib6wy9RADC_bstP265_H9pCaEqZ0AWK4nEgPQcuJBBbdesNbbVPPdTx00XWQrirmabHJsfro1P2SBnf5q1uB3VfjRxkW4PfVIdml1HzyWhgXyhj3yfCPZU9rpFkHc3eHdYMEV61W_Foj5rCaBbqFa9M6reZVwlCR02Kt_oARWCXFP8uDKVFPFfmTwm9Axcn6n7o.ulerhjTUB3m._cYYq_JUWdAqh10FslYfQ3XMgQsGrvxS0lLMupiprYcnHtPVmHBT95lyMGUevhLKt_NUSee5Be_LmFyszVoRpfLGPz3iZG1d.mi4PD0KH0uSyx_CD_IBeo5liwd4yuxefLGFPkjasgezej56SJgIvoL.HBFfjQdyEeRC7IDPTOp6Rj1bQTve3ziED697JT6p5AXbn_LV5JCLqUlEwh2SuCgz8AsZz_UqEGKgnI51ibTX0voMpjRmpuz_9igfWgMMXW9tp8MaOzEZXKLlCaC_IhVv2Xed.kFW7aB7JWtbYtjm0495TzD7Tt6AD9aFPfqKsVGQBYY4DypJrFzqFuDg5Y.kraOG1igM0Z8oUzogvjZxp0QF330hL1_EH5MlFzVKvcA0m_DO6ZGCLC5.vr3oU3.rRO3oT6m.ZGiGX9Z4Q160RRyz_oc8mqkzcbjuRCYc9ipW9.1E0bBLJIES0J.Sq75Wr0Wqpolcn6M8mERhjA--%26lp=http%3A%2F%2Feulerian.sarenza.com%2Fdynclick%2Fsarenza%2F%3Fead-publisher%3DYahoo%26ead-name%3DYahoo-Streamads%26ead-location%3DMixte%26ead-creative%3Dsoldes-pe15-generique-texte%26ead-creativetype%3DTexte%26eurl%3Dhttp%253A%252F%252Fwww.sarenza.com%252Fchaussure-pas-cher-femme","type":"ad"},{"id":"745a2501-59a3-3d81-a9e7-3783b281fb94","i13n":{"cpos":4,"cposy":10,"bpos":1,"pos":1,"subsec":221,"imgt":"ss","g":"745a2501-59a3-3d81-a9e7-3783b281fb94","ct":1,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"Sports"},"link":"http://news.yahoo.com/shaq-threw-down-monster-two-131503811.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"b3407979-8eae-3b31-a6df-34445ad667f4","i13n":{"cpos":5,"cposy":11,"bpos":1,"pos":1,"ss_cid":"98d73825-29a6-4547-b82b-125a8085f6a5","refcnt":2,"imgt":"ss","g":"b3407979-8eae-3b31-a6df-34445ad667f4","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"U.S."},"link":"http://www.sbnation.com/lookit/2016/2/13/10983686/air-jordans-robbery-craigslist-arm-severed-brooklyn","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"a4dd591c-428a-326e-8f0e-37577a87e546","link":"http://www.chicagotribune.com/news/nationworld/ct-brooklyn-robbery-teen-loses-arm-20160212-story.html","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"089d34a4-a156-3670-ab73-87c925a1dd66","link":"http://edition.cnn.com/2016/02/13/us/new-york-craigslist-robbery/index.html","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"add46721-0eb2-3f87-8d8c-8a2e586b6446","i13n":{"cpos":6,"cposy":14,"bpos":1,"pos":1,"subsec":149,"imgt":"ss","g":"add46721-0eb2-3f87-8d8c-8a2e586b6446","ct":1,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"Lifestyle"},"link":"http://finance.yahoo.com/news/scientists-behavior-men-more-attractive-155809450.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"c132b8b4-53b8-3f9f-b008-7b15d53270c4","i13n":{"cpos":7,"cposy":15,"bpos":1,"pos":1,"ss_cid":"cfb56e38-9021-46f8-a537-0673f3c2f72d","refcnt":2,"subsec":15,"imgt":"ss","g":"c132b8b4-53b8-3f9f-b008-7b15d53270c4","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"U.S."},"link":"http://news.yahoo.com/tens-thousands-sharks-spotted-stones-164600314.html","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"33c40593-795a-35ee-8565-51bdd9b8ab80","link":"http://edition.cnn.com/2016/02/13/us/shark-migration-florida-irpt/index.html","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"0a105fb9-d25f-3dd0-a461-5fc81dc4d891","link":"http://www.palmbeachdailynews.com/news/lifestyles/environment/fau-group-thousands-of-sharks-near-town-beaches-bu/nqPTm/","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"31710498501","i13n":{"cpos":8,"cposy":18,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31710498501","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=wkiGczMGIS8hrKDlXH4gtynWt_eG9VqmCuQS6pzFx2eicavTiZ5pRzUQf7SlVKj5RQvMF3vu75xt2aXAzm1Z6IedYogZ_GBzlIuGdIwiSQJzBCxWN17IDRXkP1kKHUIyYx1QkdBtUmJZjWu6MbQ0ePSY6h.5lYB0FZi4RtxjHCVmJoZs0I4yhrDXwcdiXjSBsSQoxcKkMHOFIbxNlnr1axItJPKGDw9Ysh0k7Mi84aK5somrFCqrka97kHnncTj35IK0KHgMM_ZEsNLwu.7ZCqhdcAvuZB8dgl_mLH6pCNi2ri9EcXLGU.z8pmTPP.9CQAw8_yg6t487W8qKKfDiOhsVajia8D619A2di5rQYy.tvEOOMmgUPZL6zG.DAINow5joMf6UO6a9brIkTPKsabwCb.7aKG_DsERXKzIGMp_naFmeqAYnhnQjisWjU88OebJK0NNOjk.CA93M.N_pskWllq2Oia6V1hOKM4yQfI.s4EM.ZH_odS8nTepvbw9bIbk-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F0f0868f1-4986-4ec1-be9a-d18abdda27ef%3Fad%3D1","type":"ad"},{"id":"0838f2f4-835d-33a4-86cd-1cb89df17015","i13n":{"cpos":9,"cposy":19,"bpos":1,"pos":1,"subsec":7,"imgt":"ss","g":"0838f2f4-835d-33a4-86cd-1cb89df17015","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Politics"},"link":"https://www.yahoo.com/politics/democratic-and-republican-delegate-scorecard-164410211.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"3da8e026-42e6-3ce9-b483-f73847d71cba","i13n":{"cpos":10,"cposy":20,"bpos":1,"pos":1,"ss_cid":"b8a2fdf5-fe34-4825-ae29-c817e78a7113","refcnt":2,"imgt":"ss","g":"3da8e026-42e6-3ce9-b483-f73847d71cba","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"World"},"link":"http://www.cnn.com/2016/02/14/asia/new-zealand-christchurch-earthquake/index.html","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"6335f20b-0681-3373-8a22-78662b61e536","link":"http://news.yahoo.com/5-8-magnitude-quake-hits-zealand-city-usgs-011110344.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"542a76c0-2ff9-30df-bfe1-a6a748e6e4d0","link":"https://www.washingtonpost.com/world/magnitude-58-quake-shakes-new-zealand-city-of-christchurch/2016/02/13/a4939fa4-d2ba-11e5-90d3-34c2c42653ac_story.html","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"fc5ae616-6476-382f-bf58-3fc74de0b3fd","i13n":{"cpos":11,"cposy":23,"bpos":1,"pos":1,"imgt":"ss","g":"fc5ae616-6476-382f-bf58-3fc74de0b3fd","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Politics"},"link":"http://www.thedailybeast.com/articles/2016/02/14/north-korea-s-best-building-is-empty-the-mystery-of-the-ryugyong-hotel.html","type":"article","viewer_eligible":true,"viewer_fetch":false}],"fetched":11,"interest_data":{"WIKIID:Supreme_Court_of_the_United_States":{"name":"Supreme Court"},"WIKIID:Barack_Obama":{"name":"President Obama"},"WIKIID:Antonin_Scalia":{"name":"Justice Antonin Scalia"},"WIKIID:United_States_Senate_Committee_on_the_Judiciary":{"name":"Senate Judiciary Committee"},"WIKIID:United_States_Senate":{"name":"Senate"},"WIKIID:Republican_Party_%28United_States%29":{"name":"Republicans"},"YCT:001000661":{"name":"Politics & Government"},"YCT:001000671":{"name":"Elections"},"YCT:001000681":{"name":"Government"},"WIKIID:Donald_Trump":{"name":"Donald Trump"},"WIKIID:George_W._Bush":{"name":"George W. Bush"},"YCT:001000708":{"name":"Political Parties & Movements"},"WIKIID:Charles_Barkley":{"name":"Charles Barkley"},"YCT:001000001":{"name":"Sports & Recreation"},"WIKIID:Craigslist":{"name":"Craigslist"},"YCT:001000667":{"name":"Crime & Justice"},"YCT:001000780":{"name":"Society"},"YCT:001000288":{"name":"Relationships"},"YCT:001000290":{"name":"Dating"},"WIKIID:Florida":{"name":"Florida"},"WIKIID:Palm_Beach,_Florida":{"name":"Palm Beach, Florida"},"WIKIID:Florida_Atlantic_University":{"name":"Florida Atlantic University"},"YCT:001000638":{"name":"Environment"},"YCT:001000643":{"name":"Living Nature"},"YCT:001000637":{"name":"Nature"},"YCT:001000644":{"name":"Animals"},"WIKIID:Democratic_Party_%28United_States%29":{"name":"Democratic Party (United States)"},"YCT:001000798":{"name":"Disasters & Accidents"},"YCT:001000655":{"name":"Natural Phenomena"},"WIKIID:Ryugyong_Hotel":{"name":"Ryugyong Hotel"},"WIKIID:North_Korea":{"name":"North Korea"},"WIKIID:Kim_Il-sung":{"name":"Kim Il-sung"}},"bnbData":{"title":"Antonin Scalia and the political repercussions of his death at 1p.m. ET","id":"c-9d7e56c4-903b-4ee1-93de-2be3dd9ce4e6","type":"breakingNewsBar","followId":"d44fbfea-d29f-11e5-ac3f-fa163e6f4a7e","followable":true,"userFollowing":false,"prefix":"Watch live","severity":"medium","followName":"Antonin Scalia dies","viewer_fetch":true,"viewer_eligible":true,"article_uuid":"748e13e0-0aae-3a3d-a620-f8351ede2ea7","link":"https://www.yahoo.com/katiecouric/yahoo-news-special-report-antonin-scalias-012302091.html"}}},"items":{"yui_module":"td-applet-stream-items-model-v2","yui_class":"TD.Applet.StreamItemsModel2"},"applet_model":{"yui_module":"td-applet-stream-appletmodel-v2","yui_class":"TD.Applet.StreamAppletModel2","config":{"ads":{"adchoices_text":true,"adchoices_url":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","ad_polices":true,"count":25,"contentType":"","fallback":0,"feedback":true,"frequency":4,"inline_video":true,"pu":"www.yahoo.com","se":4250754,"related_ct_se":"5454650","related_ct_single_ad":true,"related_dedupe_ads":true,"spaceid":"2023538075","sponsored_url":"http://help.yahoo.com/kb/index?page=content&y=PROD_FRONT&locale=en_US&id=SLN14553","start_index":1,"timeout":0,"type":"STRM,STRM_CONTENT,STRM_VIDEO","version":2,"related_start_index":3},"category":"","js":{"attribution_pos":"bottom","click_context":false,"content_events":true,"filters":false,"full_content_event":false,"pluck_item_fields":"id,cauuid,payoffId,events,i13n,link,type,videoUuid,viewer_eligible,viewer_fetch,video_ad_beacons,video_ad_assets","pluck_today_fields":"image,title,summary,publisher,more_link_text","poll_interval":60000,"related_collapse":true,"related_content":true,"related_count":4,"restore_filter":false,"restore_state":false,"restore_state_storage":"history","show_read":true,"sticker":false,"sticker_toptarget":"","summary_pos":"left","xhr_ss_timeout":500,"stories_like_this":0,"login_url":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US&.src=fpctx&.done="},"linkSupplement":null,"onboarding":{"count":3,"debug":false,"pos":-1,"result":3,"topic":false},"pagination":true,"remoteConfig":{"endpoint":"/_td_remote","params":{"disableAppClass":true,"footer":false,"i13n":{"auto":false},"noAbbreviation":true,"rendermode":"hovercard"},"passLocdropCrumb":true,"type":"td-applet-weather"},"request_xhtrace":0,"signed_in":false,"ss_endpoint":"ga-hr.slingstone.yahoo.com@score/v9/homerun/en-US/unified/mega","ss_timeout":300,"template":{"main":"td-applet-stream-atomic:main","drawer":"td-applet-stream-atomic:drawer_desktop","drawer_action":"td-applet-stream-atomic:drawer_action","drawer_share":"td-applet-stream-atomic:drawer_share"},"timeout":800,"total":170,"ui":{"backfill_property_region":false,"big_click_target":true,"bleed":false,"bnb_enabled":true,"cat_label_enabled":true,"comments":true,"comments_allow_supression":true,"comments_inline":true,"comments_inline_ranking":"highestRated","comments_offnet":false,"comments_editorial_suppression":true,"comments_request_count":5,"comments_update":false,"comments_update_maxidle":300000,"comments_update_monitoring":true,"comments_update_throttle":1750,"comments_writes_enabled":true,"followable":true,"followable_signedout":true,"dislike":false,"dollar_sign":true,"editorial_edition":{"label":"EDITION_DEFAULT","top":true,"shouldUpdateLVtimeInCookie":true},"enable_featured_ad_video_gap":true,"enable_hovercolor":true,"enable_stream_notify":false,"enrich_tweet":0,"enrichment":{"types":"","unique":false},"entity_max":9,"exclude_types":"","exclusive_type":"","fauxdal":true,"featured_align":"left","featured_delayed_defer":false,"featured_percent_width":45,"featured_width":460,"featured_height":258,"featured_width_portrait":200,"featured_height_portrait":200,"first_featured_treatment":true,"fixed_layout":false,"fptoday_thumbs_sparse":false,"headline_wrapper":true,"hq_ad_template":"featured_ad","i13n_key_tar_qa":false,"image_quality_override":true,"incremental_count":0,"incremental_delay":300,"incremental_history":100,"inline_filters":false,"inline_filters_article_min":10,"inline_filters_max":6,"inline_video":true,"item_template":"items","like":true,"limit_summary_height":true,"link_to_finance":false,"sticker_top":"94px","mms":false,"needtoknow_actions":true,"needtoknow_template":"filmstrip","needtoknow_storyline_min":4,"off_network_tab":false,"open_in_tab":false,"defaultDrawerAction":{"title":"ACTION_DEFAULT","description":"Content preferences","url":"https://settings.yahoo.com/interests"},"payoffDrawerActions":{"local":{"title":"ACTION_LOCAL","description":"More local news","urlPrefix":"https://news.yahoo.com/local/"},"finance":{"title":"ACTION_FINANCE","description":"Manage portfolios","url":"https://finance.yahoo.com/portfolios"},"sports":{"actionsEnabled":false,"attributionEnabled":false,"title":"ACTION_SPORTS","description":"Edit my teams","url":"https://sports.yahoo.com/myteams"}},"payoffs":"","payoffExpTime":24,"payoffFinanceArticleCount":2,"payoffInterval":6,"payoffInvalidTime":0.5,"payoffSportsLocalTeams":false,"payoffLocalArticleCount":3,"payoffLocalHeadliner":true,"payoffStartIndex":1,"pcsExclusions":false,"preview_enabled":false,"preview_lcp":true,"pubtime_format":"ONE_UNIT_ABBREVIATED","pubtime_maxage":3600,"ribbon_headers":false,"sfl":false,"sfl_get_started_string":"SFL_LINK","scrollbuffer":900,"scroll_node_selector":"body","show_tooltips":true,"smart_crop":true,"stateful_subtle_hint":true,"storyline_cap_image":2,"storyline_cap_noimage":3,"storyline_count":2,"storyline_elapsed_time":true,"storyline_elapsed_time_threshold":720,"storyline_enabled":true,"storyline_items_to_show":2,"storyline_multi_image":true,"storyline_multi_image_roundup":true,"storyline_newsy_disabled":false,"stream_actions":true,"stream_actions_minimal":true,"stream_actions_reblog":true,"stream_actions_share":false,"stream_actions_share_panel":true,"stream_actions_likable":true,"stream_actions_tumblr":true,"stream_debug":false,"stream_payoff_actions":false,"stream_item_maxwidth":"none","stream_item_image_fallback":"https://s.yimg.com/dh/ap/default/151015/stream-gradient-230x130.png","stream_item_large_image_fallback":"https://s.yimg.com/dh/ap/default/150902/stream-gradient.png","stream_single_item_image_fallback":"https://s.yimg.com/dh/ap/default/150903/stream-gradient-single-3.png","summary":true,"summary_length":160,"template_label":"js-stream-dense","template_suffix":"","today_count":5,"today_snippet_count":5,"fc_related_minCount":2,"today_featured_image_tag":"pc:size=medium","tap_for_summary":false,"templates":{"all":{"batch_max":20,"gap":0,"max":170,"start":1,"batch":2,"total":2,"last":7},"featured":{"batch_max":20,"gap":0,"max":170,"portrait":false,"start":0,"batch":0,"total":0},"featured_ad":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":2,"total":2,"last":7},"inline_video":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":0,"total":0}},"thumbnail":true,"thumbnail_align":"right","thumbnail_hover":true,"thumbnail_size":160,"thumbnail_specs":{"featured":"img:190x107|2|80","featured_square":"img:190x190|2|80","inline_video":"img:190x107|2|80","items":"img:190x107|2|80","items_large":"img:190x190|2|80","storyline_square":"img:70x70|2|90","related":"img:165x120|2|80","featured_roundup":"img:702x274|1|95","roundup_wide":"img:170x80|2|80"},"toJpg":true,"truncate_summary":false,"tweet_action":true,"uis_inferred_finance":false,"ult_count":10,"ult_host":"hsrd.yahoo.com","ult_links":false,"use_ss_roundup_slot":true,"view_article_button":false,"viewer":true,"viewer_include_all":true,"viewer_off_network":false,"viewer_reblog":false,"viewer_action":true,"visit_state_threshold":1800000,"preview":0,"related_host":"","stream_payoff_v2":1,"stream_payoff_vizify":0,"stream_rc4":0,"show_jump_to_historical_items":0,"item_templates":["items"]},"weather":false,"woeid":12597132,"tz":"Europe/Paris","xcc":"fr"},"settings":{"size":9,"woeid":12597132},"models":["stream","items","related"]},"interest":{"yui_module":"td-applet-interest-model-v2","yui_class":"TD.Applet.InterestModel2"},"comments":{"yui_module":"td-applet-comments-model-v2","yui_class":"TD.Applet.CommentsModel2"},"related":{"yui_module":"td-applet-stream-related-model-atomic","yui_class":"TD.Applet.StreamRelatedModelAtomic"}},"views":{"main":{"yui_module":"td-applet-stream-mainview-v2","yui_class":"TD.Applet.StreamMainView2"},"drawer":{"yui_module":"stream-actiondrawer-v2"}},"templates":{"main":{"yui_module":"td-applet-stream-atomic-templates-main","template_name":"td-applet-stream-atomic-templates-main"},"related":{"yui_module":"td-applet-stream-atomic-templates-related","template_name":"td-applet-stream-atomic-templates-related"},"drawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_desktop","template_name":"td-applet-stream-atomic-templates-drawer_desktop"},"removedItem":{"yui_module":"td-applet-stream-atomic-templates-removeditem","template_name":"td-applet-stream-atomic-templates-removeditem"},"drawerAction":{"yui_module":"td-applet-stream-atomic-templates-drawer_action","template_name":"td-applet-stream-atomic-templates-drawer_action"},"drawerSharePanel":{"yui_module":"td-applet-stream-atomic-templates-drawer_share","template_name":"td-applet-stream-atomic-templates-drawer_share"},"breakingNews":{"yui_module":"td-applet-stream-atomic-templates-breaking_news","template_name":"td-applet-stream-atomic-templates-breaking_news"},"flyoutDrawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_flyout","template_name":"td-applet-stream-atomic-templates-drawer_flyout"},"items":{"yui_module":"td-applet-stream-atomic-templates-items","template_name":"td-applet-stream-atomic-templates-items"},"item_default":{"yui_module":"td-applet-stream-atomic-templates-item-default","template_name":"td-applet-stream-atomic-templates-item-default"},"item_default_clusters":{"yui_module":"td-applet-stream-atomic-templates-item-default_clusters","template_name":"td-applet-stream-atomic-templates-item-default_clusters"},"item_ad":{"yui_module":"td-applet-stream-atomic-templates-item-ad","template_name":"td-applet-stream-atomic-templates-item-ad"},"item_ad_dislike":{"yui_module":"td-applet-stream-atomic-templates-item-ad_dislike","template_name":"td-applet-stream-atomic-templates-item-ad_dislike"},"item_featured_ad":{"yui_module":"td-applet-stream-atomic-templates-item-featured_ad","template_name":"td-applet-stream-atomic-templates-item-featured_ad"},"item_featured":{"yui_module":"td-applet-stream-atomic-templates-item-featured","template_name":"td-applet-stream-atomic-templates-item-featured"},"item_inline_video":{"yui_module":"td-applet-stream-atomic-templates-item-inline_video","template_name":"td-applet-stream-atomic-templates-item-inline_video"}},"i18n":{"AD_FDB_HEADING":"Why don't you like this ad?","AD_FDB_TELL_US":"Tell us why","AD_FDB_THANKYOU":"Thank you for your feedback. We will remove this and make the changes needed.","AD_FDB1":"It is offensive to me","AD_FDB2":"It is not relevant to me","AD_FDB3":"I keep seeing this","AD_FDB4":"Something else","ABCLOGO":"ABC","ACTION_MORE_LIKE_THIS":"Got it! Tell us more about what you like:","ACTION_FEWER_LIKE_THIS":"Got it! Tell us more about what you dislike:","ACTION_DEFAULT":"Content preferences Â»","ACTION_SPORTS":"Edit my teams Â»","ACTION_FINANCE":"Manage portfolios Â»","ACTION_LOCAL":"More local news Â»","ACTION_STORY_REMOVED":"Story removed","ACTION_SEE_LESS":"Okay. You'll see fewer like this.","ACTION_SEE_MORE":"Great! You'll see more like this.","ACTION_SEE_NO_MORE":"This item has been removed from your stream.","ACTION_SIGN_IN_TO_DISLIKE":"{linkstart}Sign-in{linkend} and we'll show you less like this in the future.","ACTION_SIGN_IN_TO_DISLIKE_TUMBLR":"{linkstart}Sign-in{linkend} to dislike","ACTION_SIGN_IN_TO_LIKE":"{linkstart}Sign-in{linkend} and we'll show you more like this in the future.","ACTION_SIGN_IN_TO_LIKE_SIMPLE":"Sign in to like","ACTION_SIGN_IN_TO_LIKE_TUMBLR":"{linkstart}Sign-in{linkend} to like","ACTION_SIGN_IN_TO_PERSONALIZE":"{linkstart}Sign-in{linkend} to personalize!","ACTION_SIGN_IN_TO_SAVE":"{linkstart}Sign-in{linkend} to save this story to read later.","ACTION_SIGN_IN_TO_SAVE_SHORT":"Sign in to save","ACTION_TELL_LIKE":"Tell us more about what you like:","ACTION_TELL_DISLIKE":"Tell us more about what you dislike:","ACTION_MORE_LESS":"Show more or less of","ACTION_OPTIONS":"Options","ACTION_REMOVE":"Remove from my stream","ACTION_STORIES_LIKE_THIS":"Stories like this","ACTION_OPEN":"Open","ACTION_CLOSE":"Close","ADCHOICES":"AdChoices","ALL_ARTICLES":"All Articles","ALL_CELEBRITY":"All Celebrity News","ALL_FINANCE":"All Finance","ALL_MOVIES":"All Movies News","ALL_MUSIC":"All Music News","ALL_NEWS":"All News","ALL_SPORTS":"All Sports","ALL_STORIES":"All Stories","ALL_TRAVEL":"All Travel Ideas","ALL_TV":"All TV News","AROUND_THE_WEB":"Around The Web","ALSO_LIKE":"You may also like","ASTRO_GO_TO":"Go to Horoscopes Â»","ASTRO_NAME_AQUARIUS":"Aquarius","ASTRO_NAME_ARIES":"Aries","ASTRO_NAME_CANCER":"Cancer","ASTRO_NAME_CAPRICORN":"Capricorn","ASTRO_NAME_GEMINI":"Gemini","ASTRO_NAME_LEO":"Leo","ASTRO_NAME_LIBRA":"Libra","ASTRO_NAME_PISCES":"Pisces","ASTRO_NAME_SAGITTARIUS":"Sagittarius","ASTRO_NAME_SCORPIO":"Scorpio","ASTRO_NAME_TAURUS":"Taurus","ASTRO_NAME_VIRGO":"Virgo","ASTRO_TITLE":"Today's overview","AUCTION":"Action","AUTHOR_AT_PUBLISHER":"{author} at {publisher}","BREAKING_NEWS":"BREAKING NEWS","CNBCLOGO":"","COMMENT_ARTICLE_SIGN_IN":"Sign in to perform this action.","COMMENTS":"Comments","COMMENTS_ZERO":"Start the conversation","CONTENT_PREF":"Content preferences","DISLIKE":"Dislike","DONE":"Done","DYNAMIC_FILTERS":"You Might Like","EDITION_DEFAULT":"Need To Know","FEATURED_MOVIE":"Featured Movie","FLAGGED_COMMENT":"Flagged as inappropriate. Thanks for your feedback.","FOLLOW":"Follow","FOLLOW_GAME":"Follow Game","FOLLOWING":"Following {name}","FOLLOWING_STORY":"Following","FOLLOW_UPDATES":"Follow to receive updates on","FULL_HOROSCOPE":"Full Horoscope","GAME_PERIOD:baseball":"Innings","GAME_PERIOD:football":"Quarters","GAME_PERIOD_1":"1st","GAME_PERIOD_2":"2nd","GAME_PERIOD_3":"3rd","GAME_PERIOD_4":"4th","GAME_COVERAGE":"See Full Coverage","GAME_FINAL":"Final","GAME_RECAP":"Game Recap","GAME_TITLE":"{away_team} vs. {home_team}","HALFTIME":"Halftime","HOROSCOPE":"Horoscope","IN_THEATERS_NOW":"In Theaters Now","IN_THEATERS_TODAY":"In Theaters Today","INSTALL_APP":"Install now","IN_WATCHLIST":"In watchlist","IS_IN_FAVORITES":"is in your favorites","JUMP_TO_HISTORICAL_ITEMS":"Go to where you left off","LATEST_NEWS":"Latest News","LESS":"Less","LIKABLE_ARTICLE_SIGN_IN":"Sign in to save your preferences.","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","LIVE":"Live","LIVE_GAME":"Live","LOAD_MORE":"Load more stories","LOCAL":"Local","LOCAL_BLOWING_SNOW":"Blowing Snow","LOCAL_BLUSTERY":"Blustery","LOCAL_CELSIUS":"Â°C","LOCAL_CLEAR":"Clear","LOCAL_CLOUDY":"Cloudy","LOCAL_COLD":"Cold","LOCAL_DRIZZLE":"Drizzle","LOCAL_DUST":"Dust","LOCAL_FAIR":"Fair","LOCAL_FOGGY":"Foggy","LOCAL_FAHRENHEIT":"Â°F","LOCAL_FREEZING_DRIZZLE":"Freezing Drizzle","LOCAL_FREEZING_RAIN":"Freezing Rain","LOCAL_GO_TO":"Go to Weather Â»","LOCAL_HAIL":"Hail","LOCAL_HAZE":"Haze","LOCAL_HEAVY_SNOW":"Heavy Snow","LOCAL_HOT":"Hot","LOCAL_HURRICANE":"Hurricane","LOCAL_ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","LOCAL_ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LOCAL_LIGHT_SNOW_SHOWERS":"Light Snow Showers","LOCAL_MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","LOCAL_MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","LOCAL_MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","LOCAL_MOSTLY_CLOUDY":"Mostly Cloudy","LOCAL_NEWS":"Local news","LOCAL_NO_STORIES":"Sorry, there are no news articles related to this location. Try setting your location to the nearest large city for more stories.","LOCAL_PARTLY_CLOUDY":"Partly Cloudy","LOCAL_SCATTERED_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_THUNDERSTORMS":"Scattered Thunderstorms","LOCAL_SEVERE_THUNDERSTORMS":"Severe Thunderstorms","LOCAL_SHOWERS":"Showers","LOCAL_SLEET":"Sleet","LOCAL_SMOKY":"Smoky","LOCAL_SNOW":"Snow","LOCAL_SNOW_FLURRIES":"Snow Flurries","LOCAL_SNOW_SHOWERS":"Snow Showers","LOCAL_SUNNY":"Sunny","LOCAL_THUNDERSHOWERS":"Thundershowers","LOCAL_THUNDERSTORMS":"Thunderstorms","LOCAL_TITLE":"{city} News","LOCAL_TORNADO":"Tornado","LOCAL_TROPICAL_STORM":"Tropical Storm","LOCAL_UNKNOWN":"Unknown Conditions","LOCAL_WINDY":"Windy","MIDFIELD":"midfield","MLB_BALLS":"Balls","MLB_ERRORS":"Errors","MLB_HITS":"Hits","MLB_OUTS":"Outs","MLB_RUNS":"Runs","MLB_STRIKES":"Strikes","MORE":"More","MORE_FROM_ROUNDUP":"More from this Roundup","MORE_FROM_TOPIC":"More from {topic} news","MYQUOTES_ADD_MORE":"Add more investments to your {0}Portfolio{1} to stay up to date whenever you visit Yahoo Finance.","MYQUOTES_LOGIN":"{0}Sign-in{1} to see the latest news from the investments in your portfolio.","MYTEAMS_ADD_TEAMS":"{0}Add your favorite teams{1} to start getting news about them today.","MYTEAMS_LOGIN":"{0}Sign-in{1} to get news for your favorite teams.","MYTEAMS_NO_CONTENT":"We can't find recent news for your teams. {0}Edit your teams{1} or visit the {2}All Sports news{3}","MYSAVES":"My Saves","NEW_SINCE_YOUR_LAST_VISIT":"New since your last visit","NEW_STORIES":"New for you","NEW_STORIES_COUNT":"View {new_item_count} new {new_item_count, plural, one {story} other {stories}}","NEXT":"Next","NFL_PROGRESS":"on {field_side} {yard_line}","NO_STORIES_HEADER":"We couldn't find any new stories for you.","NO_STORIES_BODY":"Please check back later or {0}try again{1}","OFF":"OFF","OFFNETWORK":"Read this on {0}","ONBOARDING_CONFIRMATION_INITIAL":"Got it! Almost there","ONBOARDING_CONFIRMATION_ONE":"Got it! One more time","ONBOARDING_CONFIRMATION_ZERO":"Got it! Youâre done","ONBOARDING_TITLE1_FINAL":"Here are some stories based on your preferences","ONBOARDING_TITLE1_INITIAL":"Which would you rather read?","ONBOARDING_TITLE1_ONE":"Now which do you prefer?","ONBOARDING_TITLE1_ZERO":"Awesome! What is your final pick?","OPENS_TODAY":"Opens Today","PORTFOLIO":"Go to your portfolio","PORTFOLIO_GAINERS":"Gainers","PORTFOLIO_LOSERS":"Losers","PORTFOLIO_MARKET_CLOSED":"Market close","PLAY":"Play","PLAY_VIDEO":"Play Video","PLAYING":"playing","PLAYING_NEAR_YOU":"Playing near you","PREVIEW_GAME":"Preview Game","PREVIOUS":"Previous","PREVIOUSLY_VIEWED":"From your last visit","PROGRESS":"{possession_team} {down} & {to_go} at {field_side} {yard_line}","READ_MORE":"Read More","REBLOG":"Reblog on Tumblr","REMOVE":"Remove","SAVE":"Save","SCORE":"Score!","SCORE_ATTRIBUTION":"Click here to track the {display_name} &rarr;","SEE_ALL_STORIES":"See all stories Â»","SEE_DETAILS":"See details Â»","SEE_MORE_STORIES":"See more stories Â»","SEE_FULL_BOXSCORE":"See full boxscore Â»","SFL_HEADER":"Hi {0}, you have no saves. Here's how to get started:","SFL_LINK":"Get started now on the {0}Yahoo Homepage{1}.","SFL_LINK_ATT":"Get started now on the {0}att.net Homepage{1}.","SFL_LINK_FRONTIER":"Get started now on the {0}Frontier Yahoo Homepage{1}.","SFL_LINK_ROGERS":"Get started now on the {0}Rogers Yahoo Homepage{1}.","SFL_LINK_VERIZON":"Get started now on the {0}Verizon Yahoo Homepage{1}.","SFL_STEP_ONE":"Step 1","SFL_STEP_TWO":"Step 2","SFL_TITLE_ONE":"Click on {0} in the stream and on articles across Yahoo to save stories for later.","SFL_TITLE_TWO":"Access your saves from the profile menu {0} on desktop & tablet or menu {1} on your smartphone.","SHARE_THIS":"SHARE THIS","SHOPPING":"Shopping","SIGN_IN":"Sign in!","SIGN_IN_2":"Sign in","SIGN_IN_FOR_MORE":"Looking for past stories?","SIGN_IN_TO_SEE_MORE_TEAMS":" to save your preferences. You'll see more of this team next time you visit.","SLIDESHOW_COUNT":"({0} photos)","SLIDESHOW_PREVIEW_COUNT":"1 of {0}","SPONSORED":"Sponsored","STATUS":"{time} {period}","STORE":"Store","STORIES":"Stories:","STORIES_ABOUT":"Stories about:","SYNOPSIS":"Synopsis","TIME_AGO":"ago","TIME_DAY":"day","TIME_H":"h","TIME_M":"m","TITLE":"Recommended for You","TITLE_LOCAL":"{city} News","TO":"to","TWITTER_REPLY":"Reply","TWITTER_RETWEET":"Retweet","TWITTER_FAVORITE":"Favorite","UNFOLLOW":"Unfollow","UNDO":"Undo","UPCOMING_GAME":"Today at {game_time}","UP_OVER":"Up over","DOWN_OVER":"Down over","VIEW":"View","VIEW_ALL":"View All","VIEW_SLIDESHOW":"View Slideshow","VIEW_FULL_ARTICLE":"VIEW FULL ARTICLE","VIEW_FULL_QUOTE":"View full quote Â»","YAHOO_MOVIES":"Yahoo Movies","YAHOO_ORIGINALS":"Yahoo Originals","YCT:001000931":"Technology","YCUSTOM:COMMERCE":"Commerce","YCUSTOM:MYQUOTES":"My Quotes","YCUSTOM:MYTEAMS":"My Teams","YLABEL:autos":"Autos","YLABEL:business":"Business","YLABEL:beauty":"Beauty","YLABEL:celebrity":"Celebrity","YLABEL:diy":"DIY","YLABEL:entertainment":"entertainment","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:lifestyle":"Lifestyle","YLABEL:makers":"Projects","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:politics":"Politics","YLABEL:science":"science","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:technology":"Technology","YLABEL:travel":"Travel","YLABEL:trending":"Trending","YLABEL:tv":"TV","YLABEL:us":"US","YLABEL:world":"World","YPROP:FINANCE":"Business","YPROP:TOPSTORIES":"All Stories","YPROP:LOCAL":"Local","YPROP:NEWS":"News","YPROP:OMG":"Entertainment","YPROP:SCIENCE":"Science","YPROP:SPORTS":"Sports","YPROV:ABCNEWS":"News","YPROV:ap.org":"AP","YPROV:CNBC":"CNBC","YPROV:NBCSPORT":"NBC Sports","YPROV:reuters.com":"Reuters","YPROV:ROLLINGSTONES":"Rolling Stone","YPROV:us.sports.yahoo.com":"Experts","YTYPE:VIDEO":"Video","YPROP:STYLE":"Style","YMAG:food":"Food","YMAG:tech":"Tech","YMAG:travel":"Travel"},"i13n":{"pv":2,"pp":{"ccode_st":"mega_global_ranking_hlv2_up_based"}},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-trending-atomic":{"base":"/sy/os/mit/td/td-applet-trending-atomic-0.0.45/","root":"os/mit/td/td-applet-trending-atomic-0.0.45/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_32209491"] = {"applet_type":"td-applet-trending-atomic","views":{"main":{"yui_module":"td-applet-trending-atomic-mainview","yui_class":"TD.Applet.TrendingAtomicMainView","config":{"clickableTitles":true,"rotationEnabled":true,"rotationInterval":12000,"giftsEnabled":true}}},"templates":{"main":{"yui_module":"td-applet-trending-atomic-templates-main","template_name":"td-applet-trending-atomic-templates-main"}},"i18n":{"GIFTS_TITLE":"Valentine's Day","TITLE":"Trending","TRENDING_NEWS":"Trending News","TRENDING_NOW":"Trending Now"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-weather-atomic":{"base":"/sy/os/mit/td/td-applet-weather-atomic-0.0.23/","root":"os/mit/td/td-applet-weather-atomic-0.0.23/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_63794"] = {"applet_type":"td-applet-weather-atomic","models":{"applet_model":{"yui_module":"td-applet-weather-atomic-appletmodel","yui_class":"TD.Applet.WeatherAppletModel","models":["weather"],"data":{"i13n":{"sec":"app-wea","bucket":"201"},"current":12597132,"favorite":null,"useplus":false},"user_settings":{"unit":"f","curloc":"12597132"},"config":{"moreinfo":{"link":"http://www.weather.com/?par=yahoonews","img":"http://l.yimg.com/dh/ap/default/130915/wcl1.gif"},"enableFav":false,"enableWeatherYQLP":true,"baseLink":"https://weather.yahoo.com","showWidgetLink":true,"urlGeneratorRules":{"enabled":false,"url_format_local":"","url_format_international":"","url_format_local_ajax":"","local_country_code":[]},"daysLimit":4,"fixed_layout":false}},"weather":{"yui_module":"td-applet-weather-atomic-model","yui_class":"TD.Applet.WeatherModel","data":{"weatherDataList":[{"location":{"woeid":"12597132","city":"Yvrac","state":"33","country":"FR"},"link":{"href":"https://weather.yahoo.com/fr/33/yvrac-12597132/"},"timezone":"CET","flickr":{"attribution":null,"image_assets":[{"width":"2048","height":"2048","type":"main","url":"https://s1.yimg.com/nn/lib/metro/rain_n.jpg"}]},"current":{"sunrise":"08:06:40","sunset":"18:26:28","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"unit":"f","now":"43"},"atmosphere":{"humidity":"93","wind_speed":"10","wind_chill":"37"}},"forecast":{"day":[{"label":"Today","day_of_week":"6","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"56","low":"46","unit":"f"}},{"label":"Sun","day_of_week":"0","condition":{"description":" Scattered Thunderstorms","code":"39","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png"}}},"temp":{"high":"52","low":"39","unit":"f"}},{"label":"Mon","day_of_week":"1","condition":{"description":"Partly Cloudy","code":"30","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/partly_cloudy_day.png"}}},"temp":{"high":"46","low":"35","unit":"f"}},{"label":"Tue","day_of_week":"2","condition":{"description":"Sunny","code":"32","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/clear_day.png"}}},"temp":{"high":"46","low":"30","unit":"f"}}]},"woeid":"12597132","accordianState":"e"}],"currentLoc":{"woeid":"12597132","city":"Yvrac","state":"33","country":"FR"}}}},"views":{"main":{"yui_module":"td-applet-weather-atomic-mainview","yui_class":"TD.Applet.WeatherMainView"},"header":{"yui_module":"td-applet-weather-atomic-headerview","yui_class":"TD.Applet.WeatherHeaderView","config":{}}},"templates":{"main":{"yui_module":"td-applet-weather-atomic-templates-main","template_name":"td-applet-weather-atomic-templates-main"}},"i18n":{"CURRENT_LOCATION":"Current Location","WEATHER":"Weather","HIGH":"High","LOW":"Low","TODAY":"Today","TOMORROW":"Tomorrow","SUNDAY":"Sunday","MONDAY":"Monday","TUESDAY":"Tuesday","WEDNESDAY":"Wednesday","THURSDAY":"Thursday","FRIDAY":"Friday","SATURDAY":"Saturday","ABBR_SUNDAY":"Sun","ABBR_MONDAY":"Mon","ABBR_TUESDAY":"Tue","ABBR_WEDNESDAY":"Wed","ABBR_THURSDAY":"Thu","ABBR_FRIDAY":"Fri","ABBR_SATURDAY":"Sat","BLOWING_SNOW":"Blowing Snow","BLUSTERY":"Blustery","CLEAR":"Clear","CLOUDY":"Cloudy","COLD":"Cold","DRIZZLE":"Drizzle","DUST":"Dust","FAIR":"Fair","FREEZING_DRIZZLE":"Freezing Drizzle","FREEZING_RAIN":"Freezing Rain","FOGGY":"Foggy","FORECAST":"Forecast","FULL_FORECAST":"Full forecast","HAIL":"Hail","HAZE":"Haze","HEAVY_SNOW":"Heavy Snow","HOT":"Hot","HURRICANE":"Hurricane","ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LIGHT_SNOW_SHOWERS":"Light Snow Showers","MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","MIXED_RAIN_AND_SLEET":"Mixed Rain and Sleet","MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","MOSTLY_CLOUDY":"Mostly Cloudy","PARTLY_CLOUDY":"Partly Cloudy","SCATTERED_SHOWERS":"Scattered Showers","SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","SCATTERED_THUNDERSTORMS":" Scattered Thunderstorms","SEARCH_CITY":"Kindly search your city","SEVERE_THUNDERSTORMS":"Severe Thunderstorms","SHOWERS":"Showers","SLEET":"Sleet","SMOKY":"Smoky","SNOW":"Snow","SNOW_FLURRIES":"Snow Flurries","SNOW_SHOWERS":"Snow Showers","SUNNY":"Sunny","THUNDERSHOWERS":"Thundershowers","THUNDERSTORMS":"Thunderstorms","TORNADO":"Tornado","TROPICAL_STORM":"Tropical Storm","WINDY":"Windy","WEATHER_CHICLET_ERROR":"Visit {linkstart}Yahoo Weather{linkend} for a detailed forecast.","GOTO_WEATHER":"See more Â»","NO_WEATHER_MESSAGE":"Visit {linkstart}Yahoo Weather{linkend} for the latest forecast or {detectstart}click here{detectend} to select your precise location.","TEN_DAY":"10 day"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-activitylist-atomic":{"base":"/sy/os/mit/td/td-applet-activitylist-atomic-0.0.60/","root":"os/mit/td/td-applet-activitylist-atomic-0.0.60/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000171"] = {"applet_type":"td-applet-activitylist-atomic","templates":{"list":{"yui_module":"td-applet-activitylist-atomic-templates-list.tumblr","template_name":"td-applet-activitylist-atomic-templates-list.tumblr"}},"i18n":{"ACTION_POST":"Posted","FAVORITE":"favorite","FIND_OUT_WHY":"Find out why Â»","LIVE":"Live","REASON:BREAKING":"Breaking","REASON:COMMENTS":"{count, number, integer} Comments","REASON:EVERGREEN":"","REASON:EXCLUSIVE":"Exclusive","REASON:NEW":"New","REASON:NEW_ON":"New on {site}","REASON:ONLYONYAHOO":"Only on Yahoo","REASON:POPULARONSEARCH":"Popular on Search","REASON:READINGNOW":"{count, number, integer} Reading Now","REASON:SEARCHVISITS":"{count, number, integer} Search Visits","REASON:SHARES":"{count, number, integer} Shares","REASON:SOCIALVISITS":"{count, number, integer} Social Visits","REASON:VIDEOPLAYS":"{count, number, integer} Video Plays","REASON:VIEWS":"{count, number, integer} Views","REASON:VISITS":"{count, number, integer} Visits","REASON:WATCHINGNOW":"{count, number, integer} Watching Now","REASON:WATCHLIVE":"Watch Live","REPLY":"reply","RETWEET":"retweet","TIME_FORMAT":"h:mm A","TIME_FORMAT_SAME_DAY":"[Today]","TIME_FORMAT_NEXT_DAY":"[Tomorrow]","TIME_FORMAT_NEXT_WEEK":"dddd","TIME_FORMAT_ELSE":"MMM D","TITLE":"Only from Yahoo","TWEET_NAVIGATE":"navigate to tweet","VIA_TWITTER":"via Twitter","YLABEL:autos":"Autos","YLABEL:beauty":"Beauty","YLABEL:diy":"DIY","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:parenting":"Parenting","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:travel":"Travel","YLABEL:tv":"TV","YPROP:autos":"Yahoo Autos","YPROP:beauty":"Yahoo Beauty","YPROP:diy":"Yahoo DIY","YPROP:finance":"Yahoo Finance","YPROP:food":"Yahoo Food","YPROP:games":"Yahoo Games","YPROP:gma":"Yahoo News","YPROP:health":"Yahoo Health","YPROP:homes":"Yahoo Homes","YPROP:ivy":"Yahoo Screen","YPROP:makers":"Yahoo Makers","YPROP:movies":"Yahoo Movies","YPROP:music":"Yahoo Music","YPROP:news":"Yahoo News","YPROP:omg":"Yahoo Celebrity","YPROP:parenting":"Yahoo Parenting","YPROP:politics":"Yahoo Politics","YPROP:shopping":"Yahoo Shopping","YPROP:sports":"Yahoo Sports","YPROP:style":"Yahoo Style","YPROP:tech":"Yahoo Tech","YPROP:travel":"Yahoo Travel","YPROP:tv":"Yahoo TV","YPROP:yahoo":"Yahoo"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
    window.Af = window.Af || {};
    window.Af.config = window.Af.config || {};
    window.Af.config.spaceid = "2023538075";
    window.Af.context= {
        crumb : 'GwvrimqK9vs',
        guid : '',
        mcCrumb: '.436xr/QBlW',
        ucsCrumb: 'GkDNbFI0ryV',
        device: 'desktop',
        rid : '3p231htbc1es4',
        default_page : 'p1',
        _p : 'p1',
        site : 'fp',
        lang : 'en-US',
        region : 'US',
        authed: 0,
        enable_dd : '',
        default_appletinit : 'viewport',
        locdrop_crumb: '', woeid: '12597132',
        ssl: 1,
        
        bucket: '201'
        
        
    };

    window.Af.config.transport = {
        crumbForGET: true,
        xhr: '/fpjs',
        consolidate: true,
        timeout: 6000
    };

    window.Af.config.onepush = {
        subscribeTimeout : 5000,
        subscribeMaxTries : 1,
        trackInitComet : true,
        trackLatency : true,
        latencySampleSize : 50,
        publicCometHost: 'https://comet.yahoo.com/comet',
        shutdown: false,
        trackShutdown: false
    };

    window.Af.config.beacon = {
        beaconUncaughtErr: true,
        sampleSizeUncaughtErr: 1,
        maxUncaughtErrCount: 5,
        beaconPageLoadPerf: false,
        sampleSize : 1,
        pathPrefix : '/_td_api/beacon',
        batch: false,
        batchInterval: 3
    };

    window.Af.config.pipe = {
        msg_throttle: 2000,
        err_maxstreak: 2
    };

    window.Af.config.td = {
        remote: '/_td_remote',
        xhr: '/_td_api'
    };

    
    YUI.namespace('Env.My.settings').context = {
        uh_js_file : '',
                videoAsyncEnabled: 0,
        videoplayerScriptElementId: 'videoplayerJs',
        videoplayerUrl: 'https://www.yahoo.com/sy/rx/builds/6.155.0.1453355318/en-us/videoplayer-nextgen-flash-min.js',
        videoAutoplay: 1,
        videoLooping: 0,
        videoForcedError: 0,
        videoFullscreen: 1,
        videoHtml5: 0,
        videoMinControls: 0,
        videoCmsEnv: 'prod',
        videoMustWatch: 1,
        videoMWSticky: 0,
        videoMute: 1,
        videoQosRate: 1,
        videoBuffering: 0,
        videoRelated: 0,
        videoYwaRate: 0,
        videoMaxLoops: 3,
        videoModeEnabled: 0,
        videoTiltbackModeEnabled: 0,
        videoSSButton: 0,
        videoPausescreen: 0,
        videoSingleVideo: 0,
        videoMaxInstances: 12,
        videoInitEvent: 'domready',
        videoExpName: 'advance',
        videoComscoreC4: 'US fp',
        videoplayerScrollThrottle: '200',
        
        themeCssEnabled : false,
        themeBgImageEnabled: false,
        transparentPixelImage: 'https://s.yimg.com/os/mit/media/m/base/images/transparent-95031.png',
        pageMessage: {
            type: '',
            msg: ''
        },
        servingBeaconUrl: 'https://bs.serving-sys.com/BurstingPipe/ActivityServer.bs',
        enablePerfBeacon: '0',
        test_id: '201',
        customizedEnabled: false,
        trendingNowOffScreen: 0
    };

    YUI.namespace('Env.Af.Perf').secondYUIUseStart = (new Date()).getTime();

    var yuiPreloadModules = ["type_advance_desktop_viewer","type_video_manager","type_sdarotate","type_sda"];

    yuiPreloadModules = yuiPreloadModules.concat((function (appletTypes) {
        if (!appletTypes || appletTypes.length === 0) {
            return [];
        }
        var yui_modules = [],
            types = YMedia.Array.hash(appletTypes);
        YMedia.Object.each(window.Af.bootstrap, function (bootstrap, guid) {
            if (!types[bootstrap.applet_type]) {
                 return;
            }
            YMedia.Array.each(['models', 'views', 'templates'], function (type) {
                YMedia.Object.each(bootstrap[type], function (config, name) {
                    if (config.yui_module) {
                         yui_modules.push(config.yui_module);
                    }
                });
            });
        });
        return yui_modules;
    })([]));

    YMedia.use(yuiPreloadModules, function (Y, NAME) {
        YUI.namespace('Env.Af.Perf').secondYUIUseStop = (new Date()).getTime();
        var pageMessage = YUI.Env.My.settings.context.pageMessage,
        enablePerfBeacon = YUI.Env.My.settings.context.enablePerfBeacon,
        test_id = YUI.Env.My.settings.context.test_id,
        pausePerfBeacon = false,
        perfBeacon = {}
                ,
        rapidConfig = rapidPageConfig.rapidConfig,
        YWA_CF_MAP = rapidPageConfig.ywaCF,
        YWA_ACTION_MAP = rapidPageConfig.ywaActionMap,
        YWA_OUTCOME_MAP = rapidPageConfig.ywaOutcomeMap;
        if(YAHOO.i13n) {
            YAHOO.i13n.WEBWORKER_FILE = '/lib/metro/g/myy/rapidworker_1_2_0.0.3.js';
            YAHOO.i13n.SPACEID = '2023538075';
            YAHOO.i13n.TEST_ID = '201';
        }
        if (pageMessage.msg != '') {
            Y.Af.Message.show('body', {level: pageMessage.type, content: pageMessage.msg});
        }

        if (1 === 1) {
            Y.Lang.later(45000, null, function() {
                var failedModules  = Y.all("#myColumns .js-applet .App-loading");
                failedModules.each(function (module) {
                    module.setContent('Sorry! We are temporarily unable to load the content. Please refresh or try again later.');
                    module.removeClass('App-loading');
                    module.addClass('App-failed');
                });
            });
        }
        
        
        
        

        YUI.namespace('Env.Af.Perf').YMyAppCreateStart = (new Date()).getTime();
        YMedia.My.App = new Y.My.App(
            {                i13nConfig: {
                    rapid: rapidConfig,
                    rapidInstance: rapidPageConfig.rapidSingleInstance && YAHOO && YAHOO.i13n && YAHOO.i13n.rapidInstance || null,
                    ywaMaps: {
                        YWA_OUTCOME_MAP: YWA_OUTCOME_MAP,
                        YWA_CF_MAP: YWA_CF_MAP,
                        YWA_ACTION_MAP: YWA_ACTION_MAP
                    }
                },
                inlineViewer: 0,
                summaryView: 0,
                stickerTarget: "#SearchBar-Wrapper-Mini",
                magazineViewerEnabled: 1,
                viewerBlacklistDisable: 1,
                viewer: {
                    viewerAnimation: "slide",
                    pageAnimation: "zoom",
                    scrollTopAmount: 0,
                    highlanderMode: "mega-modal"
                }}
        );
        

        var firePerfBeacon = function(e) {
            var key, value, beaconNode, queryString = [];

            if (Y.Object.size(perfBeacon) < 1 || pausePerfBeacon) {
                return;
            }
            pausePerfBeacon = true;
            for (key in perfBeacon) {
                if (perfBeacon.hasOwnProperty(key)) {
                    queryString.push(key+'='+perfBeacon[key]);
                }
            }
            queryString.push('test='+test_id);
            beaconNode = Y.Node.create('<img src="myperf.php?'+queryString.join('&')+'" style="width:1px;height:1px;position:absolute;left:-900px;">');
            Y.one('body').append(beaconNode);
        };

        if (enablePerfBeacon == '1') {
            Y.one('window').once('scroll', firePerfBeacon);
            Y.Lang.later(10000, null, firePerfBeacon);

            YMedia.My.App.after('appletsinit', function (e) {
                if (pausePerfBeacon == true) {
                    return;
                }
                perfBeacon[e.applet.type] = (new Date()) - myYahoostartTime;
            });
        }
        YMedia.on('appletsinit', function (e) {
            
        });
        
    });
            });
     });
</script>
    <!-- bottom -->

    <!-- Comscore -->
            <!-- Begin comScore Tag -->
		<script>
		  var _comscore = _comscore || [];
		  _comscore.push({
		    c1: "2",
		    c2: "7241469",
		    c5: "2023538075",
		    c7: "https://www.yahoo.com/"
		  });
		  (function() {
		    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		    s.src = "/sy/lq/lib/3pm/cs_0.2.js";
		    el.parentNode.insertBefore(s, el);
		  })();
		</script>
		<noscript>
		  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=7241469&c7=https%3A%2F%2Fwww.yahoo.com%2F&c5=2023538075&cv=2.0&cj=1" />
		</noscript>
		<!-- End comScore Tag -->

    <!-- Nielsen -->
    

    <!-- Lighthouse  -->
    

    <!-- yaft -->
            <script type="text/javascript" src="/sy/zz/combo?/os/mit/media/p/content/content-aft-min-a202e73.js&/os/mit/media/p/content/yaft2-aftnoad-min-e5cc02b.js&os/mit/media/p/content/yaft2-harbeacon-min-dabe12d.js"> </script>        
        <script type="text/javascript">
            if (window.YAFT !== undefined) {
                var __yaftConfig = {
                    modules: ["UH","mega-uh","mega-topbar","applet_p_","ad-north-base","fea-","my-adsFPAD-base","my-adsLREC-base","my-adsMAST","my-adsTXTL","content-modal-","hl-ad-LREC-","modal-sidekick-","hl-ad-LREC-0","hl-ad-MON-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","hl-ad-SPL-0"], 
                    modulesExclude: ["UH-Search","UH-ColWrap","mega-uh-wrapper"],
                    canShowVisualReport: false,
                    useNormalizeCoverage: true,
                    generateHAR: false,
                    includeOnlyAft2: true,
                    useNativeStartRender : true,
                    modulesAft2Container: ["hl-viewer"],
                    maxWaitTime: 6000
                };
                __yaftConfig.plugins = [];                __yaftConfig.plugins.push({
                     name: 'aftnoad',
                     isPre: true,
                     config: {
                         useNormalizeCoverage: true,
                         adModules:["ad-north-base","my-adsFPAD-base","my-adsLREC-base","my-adsTL1","my-adsMAST","hl-ad-LREC-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","my-adsTXTL","hl-ad-SPL-0"]
                     }
                });
                            __yaftConfig.plugins.push({
                name: 'har',
                config: {
                    sec: 'har',
                    rapid: function() { if(YMedia && YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) {return YMedia.My.App.getRapidTracker(); } else {return null;}},
                    aftThreshold: 10000,
                    onlySlowest: false
                }
            });
            __yaftConfig.generateHAR = true;
                        if (window.LH && window.LH.isInitialized) {
            window.LH.tag('b',{val: '201'});               
            window.LH.tag("l", {val: false});
        }
                        window.aft2CB = function(data, error) {
        if (!error) {
            var aft2 = Math.round(data.aft);
            var vic2 = data.visuallyComplete;
            var srt2 = Math.round(data.startRender);

            if (window.LH && window.LH.isInitialized) {
                var lhRecord = function lhRecord(name, type, startTime, duration) {
                    window.LH.record(name, {
                        name: name, type: type, startTime: startTime, duration: duration
                    });
                };

                lhRecord('AFT', 'mark', aft2, 0);
                lhRecord('AFT2', 'mark', aft2, 0);
                lhRecord('VIC2', 'mark', vic2, 0);
                lhRecord('STR2', 'mark', srt2, 0);
                window.LH.beacon({
                    clearMarks:false,
                    clearMeasures: false,
                    clearCustomEntries: true,
                    clearTags: false
                });
            }

            var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
            if (rapidInstance) {
                var afterPageLoad = {
                    AFT: aft2,
                    AFT2: aft2,
                    STR: srt2,
                    VIC: vic2
                };
                var perfData = {
                    perf_commontime: {afterPageLoad: afterPageLoad}
                };
                rapidInstance.beaconPerformanceData(perfData);
            }
        }
    };
                window.YAFT.init(__yaftConfig, function(data, error) {
                    if (!error) {
                        try {
                                    if (window.LH && window.LH.isInitialized) {
            var results = [0], module = '', index = '', lhRecord = '';
            for (module in data.modulesReport) {
                for (index in data.modulesReport[module].resources) {
                    results.push(Math.round(data.modulesReport[module].resources[index].durationFromNStart));
                }
            }
            lhRecord = function (ma, va, custom) {
                var res = '';                                
                if (undefined === custom) {
                    if (va && !isNaN(va)) {
                        res = Math.round(va);
                    } else {
                        res = 0;
                    }
                } else {
                    res = va;
                }
                    window.LH.record(ma, { name: ma, type: 'mark', startTime: res, duration: 0 });
            };
                            
            lhRecord('AFT', data.aft);
            lhRecord('AFT1', data.aft);
            if (data.aftNoAd) {
                lhRecord('AFTNOAD', data.aftNoAd);
            }
            lhRecord('X_FB1', 42);
            lhRecord('X_FBN', 68);
            lhRecord('PLT_CACHED_REQs', data.httpRequests.onloadCached);
            lhRecord('COSTLY_RESOURCE', Math.max.apply(null, results));
            lhRecord('StartRender', data.startRender);
            if (screen) {
                lhRecord('SCR_H', screen.height);
                lhRecord('SCR_W', screen.width);
            }
            if (window.FPAD_rendered) {
                lhRecord('xAFT', data.aft);
                lhRecord('xPLT', data.pageLoadTime);
                if (window.rtAdStart && window.rtAdDone) {
                    lhRecord('ADSTART_FPAD', Math.round(rtAdStart));
                    lhRecord('ADEND_FPAD', Math.round(rtAdDone));
                }
            }

            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    lhRecord(_adLT[i][0], _adLT[i][1]);
                }
            }
            window.LH.beacon({ clearMarks: false, clearMeasures: false, clearCustomEntries: true, clearTags: false });   
        }
                                    var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
        if(rapidInstance) {
            var initialPageLoad = {
                AFT: Math.round(data.aft),
                AFT1: Math.round(data.aft),
                STR: data.startRender,
                VIC: data.visuallyComplete,
                PLT: data.pageLoadTime,
                DOMC: data.domElementsCount,
                HTTPC: data.httpRequests.count,
                CP: data.totalCoveragePercentage,
                NCP: data.normTotalCoveragePercentage
            };

            if(data.aftNoAd) {
                initialPageLoad.AFTNOAD = Math.round(data.aftNoAd);
            }              

            var customPerfData = {},
                pagePerfData = {},
                results = [];
            
            var yaftResults = [0], yaftModule = '', yaftIndex = '';
            // Find costly resource time
            for (yaftModule in data.modulesReport) {
                for (yaftIndex in data.modulesReport[yaftModule].resources) {
                    yaftResults.push(Math.round(data.modulesReport[yaftModule].resources[yaftIndex].durationFromNStart));
                }
            }
            pagePerfData['COSTLY_RESOURCE'] = Math.max.apply(null, yaftResults);
            pagePerfData['X_FB1'] = 42;
            pagePerfData['X_FBN'] = 68; 
   
            // Log ad perf data to rapid perf metric
            if (window.FPAD_rendered) {
                pagePerfData['xAFT'] = data.aft;
                pagePerfData['xPLT'] = data.pageLoadTime;
                if (window.rtAdStart && window.rtAdDone) {
                    pagePerfData['ADSTART_FPAD'] = Math.round(rtAdStart);
                    pagePerfData['ADEND_FPAD'] = Math.round(rtAdDone);
                }
            }
            
            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    pagePerfData[_adLT[i][0]]  = _adLT[i][1];
                }
            }
                        if(resourceTimingAssets) {
                for(i in resourceTimingAssets) {
                    if (resourceTimingAssets.hasOwnProperty(i)) {
                        resourceName = window.performance.getEntriesByName(resourceTimingAssets[i]);
                        if(resourceName && resourceName.length) {
                            var resourceFinish = resourceName[0].responseEnd;
                            pagePerfData[i] = Math.round(resourceFinish);
                        }
                    }
                }
            }

            customPerfData['utm'] = pagePerfData;
            var perfData = {
                perf_commontime: {initialPageLoad: initialPageLoad},
                perf_usertime: customPerfData
            };
            rapidInstance.beaconPerformanceData(perfData);
        }
                        } catch (e) {}                                                        
                    }
               });
            }
      </script>

    <!-- Via https/1.1 ir14.fp.ir2.yahoo.com[D992B340] (YahooTrafficServer), http/1.1 usproxy2.fp.ne1.yahoo.com[628AF397] (YahooTrafficServer) -->
    <!-- sid=2023538075 -->
    

    </body>
</html>
<!-- myproperty:myservice-us:0:Success -->
<!-- slw83.fp.ne1.yahoo.com compressed/chunked Sun Feb 14 17:38:13 UTC 2016 -->
