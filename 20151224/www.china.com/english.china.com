ï»¿<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>China.com - Your guide on traveling and living in China</title>
<meta name="keywords" content="China,travel,lifestyle,learn Chinese,news,videos,business,films,sports,reports" />
<meta name="description" content="English.china.com is a one-stop shop for everything about China â news, events, culture, people, lifestyle, language. It also provides information about traveling and living in China." />
<meta name="auther" content="F7 13489" />
<!-- /etc/htmlhead.shtml Start -->
<link href="/css/style.css?20141205.1" rel="stylesheet" />
<!--[if lte IE 6]> 
<script src="/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.page-select-language-list em, .page-nav .page-openSearch em, .page-focus-prevnext a, .page-video-right .item-text, .page-watched .play, .page-video-right .item-icon, .page-video-right .item-watch'); 
</script>
<![endif]-->
<!-- /etc/htmlhead.shtml End -->
<!-- /etc/goto3g.shtml Start -->

<!-- /etc/goto3g.shtml End -->
</head>

<body>
<!-- /etc/channelhomehead.shtml Start -->
<div class="page-head">
  <div class="page-top-bg" id="page-top">
    <div class="maxWidth page-top">
      <div class="page-logo"><a href="/index.html"><img src="/img/logo.png" alt="china.com" width="208" height="46" /></a></div>
      <div class="page-language">
        <em>language:English</em>
      </div>
      <div class="page-top-right">
          <div class="page-top-time">Thursday, October 24, 2013</div>
          <div class="page-select-language" id="page-select-language">
              <div class="page-show-language" style="display: block;">Language</div>
              <div class="page-select-language-tit" style="display: none;"><i>Language</i></div>
              <div class="page-select-language-list" style="display: none;">
                <a href="http://www.china.com/index.html" class="langCn"><em>www</em></a>
                <a href="http://english.china.com/index.html" class="langEn"><em>english</em></a>
                <a href="http://german.china.com/index.html" class="langDe"><em>german</em></a>
                <a href="http://italy.china.com/index.html" class="langIt"><em>italy</em></a>
                <a href="http://portuguese.china.com/index.html" class="langPt"><em>portuguese</em></a>
                <a href="http://french.china.com/index.html" class="langFr"><em>french</em></a>
                <a href="http://russian.china.com/index.html" class="langRu"><em>russian</em></a>
                <a href="http://espanol.china.com/index.html" class="langEs"><em>espanol</em></a>
                <a href="http://malay.china.com/index.html" class="langMy"><em>malay</em></a>
                <a href="http://vietnamese.china.com/index.html" class="langVn"><em>vietnamese</em></a>
                <a href="http://laos.china.com/index.html" class="langLa"><em>laos</em></a>
                <a href="http://cambodian.china.com/index.html" class="langKh"><em>cambodian</em></a>
                <a href="http://thai.china.com/index.html" class="langTh"><em>thai</em></a>
                <a href="http://indonesian.china.com/index.html" class="langId"><em>indonesian</em></a>
                <a href="http://filipino.china.com/index.html" class="langPh"><em>filipino</em></a>
                <a href="http://myanmar.china.com/index.html" class="langMm"><em>myanmar</em></a>
                <a href="http://japanese.china.com/index.html" class="langJp"><em>japanese</em></a>
                <a href="http://korean.china.com/index.html" class="langKr"><em>korean</em></a>
                <a href="http://mongol.china.com/index.html" class="langMn"><em>mongol</em></a>
                <a href="http://nepal.china.com/index.html" class="langNp"><em>nepal</em></a>
                <a href="http://hindi.china.com/index.html" class="langIn"><em>hindi</em></a>
                <a href="http://bengali.china.com/index.html" class="langMd"><em>bengali</em></a>
                <a href="http://turkish.china.com/index.html" class="langTr"><em>turkish</em></a>
                <a href="http://persian.china.com/index.html" class="langIr"><em>persian</em></a>
                <a href="http://arabic.china.com/index.html" class="langAe"><em>arabic</em></a>
              </div>
            </div>
            <div class="page-search">
              <form id="web-search" name="web-search" method="get" action="http://www.google.com/search">
                <input type="hidden" name="sitesearch" value="english.china.com" />
                <input type="text" name="q" class="web-search-keyword" placeholder="Search here..." />
                <input type="submit" name="button" class="web-search-but" value="Search Site" />
              </form>
            </div>
      </div>
    </div>
  </div>
  <div class="page-nav-bg" id="page-nav">
    <ul class="page-nav maxWidth">
      <li class="page-openSearch"><a href="#"><em>Open search</em></a></li>
      <li class="small-logo"><a href="/index.html"><img src="/img/small-logo.png" /></a></li>
      <li><a href="/news/index.html">News</a></li>
      <li><a href="/video/index.html">Video</a></li>
      <!--<li><a href="/audio/index.html">Audio</a></li>-->
      <!--<li><a href="/photos/index.html">Photos</a></li>-->
      <li><a href="/travel/index.html">Travel</a></li>
      <!--<li><a href="/lifestyle/index.html">Lifestyle</a></li>-->
      <li><a href="/chinese/index.html">Learn Chinese</a></li>
	  <li><a href="http://english.china.com/cityguide/">City Guide</a></li>
      <li><a href="http://mail.china.com/en/" class="ext" target="_blank">Free Mail</a></li>
    </ul>
  </div>
</div><!-- page-head End -->
<!-- /etc/channelhomehead.shtml End -->

<div class="maxWidth">
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 #17286  Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=206" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 End -->
</div>

<div class="page-main maxWidth">
  <div class="page-left">
    <div class="page-focus" id="page-focus">
      <div class="page-focus-body" id="page-focus-body">
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-12/24/c_134948469.htm" title="Shenzhen landslide survivor recieves medical treatment in hospital"><img src="http://img04.mini.abroad.imgcdc.com/english/news/topphotos/china/189/20151224/533968_140131.jpg.680x330.jpg" width="565" height="250" alt="Shenzhen landslide survivor recieves medical treatment in hospital" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-12/24/c_134948469.htm" title="Shenzhen landslide survivor recieves medical treatment in hospital" class="title_default">Shenzhen landslide survivor recieves medical treatment in hospital</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.chinadaily.com.cn/culture/2015-12/24/content_22792440.htm" title="Odd Christmas traditions around the world"><img src="http://img01.abroad.imgcdc.com/english/news/topphotos/world/1208/20151224/533965_140128_680x330.jpg" width="680" height="330" alt="Odd Christmas traditions around the world" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.chinadaily.com.cn/culture/2015-12/24/content_22792440.htm" title="Odd Christmas traditions around the world" class="title_default">Odd Christmas traditions around the world</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/12/23/4201s909642.htm" title="Ocean-themed Restaurant Opens in East China"><img src="http://img03.mini.abroad.imgcdc.com/english/news/topphotos/china/189/20151223/533142_139874.jpg.680x330.jpg" width="680" height="330" alt="Ocean-themed Restaurant Opens in East China" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/12/23/4201s909642.htm" title="Ocean-themed Restaurant Opens in East China" class="title_default">Ocean-themed Restaurant Opens in East China</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-12/23/c_134944922.htm" title="Christmas light display seen in Brooklyn"><img src="http://img02.mini.abroad.imgcdc.com/english/news/topphotos/world/1208/20151223/533141_139873.jpg.680x330.jpg" width="565" height="250" alt="Christmas light display seen in Brooklyn" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-12/23/c_134944922.htm" title="Christmas light display seen in Brooklyn" class="title_default">Christmas light display seen in Brooklyn</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/12/23/195s909638.htm" title="Young Cellist Ouyang Nana Releases A MV on Her Recording An Album"><img src="http://img01.mini.abroad.imgcdc.com/english/home/topphoto/1295/20151223/533037_139840.jpg.680x330.jpg" width="680" height="330" alt="Young Cellist Ouyang Nana Releases A MV on Her Recording An Album" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/12/23/195s909638.htm" title="Young Cellist Ouyang Nana Releases A MV on Her Recording An Album" class="title_default">Young Cellist Ouyang Nana Releases A MV on Her Recording An Album</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
      </div>
      <div id="page-focus-console"></div>
      <div class="page-focus-prevnext">
        <a href="#" id="page-focus-prev">Previous focus</a>
        <a href="#" id="page-focus-next">Next focus</a>
      </div>
    </div><!-- page-focus End -->
    
    <div class="page-latest">
      <h2 class="modTit"><strong><a href="/news/index.html">LATEST NEWS</a></strong></h2>
      <div class="page-latest-body" id="page-latest">
        <div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/24/4081s909794.htm" title="Vanke Welcomes Anbang's Increase in Shareholding"><img src="http://img02.abroad.imgcdc.com/english/news/business/56/20151224/533973_140137_200x120.jpg" width="200" height="120" alt="Vanke Welcomes Anbang's Increase in Shareholding" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/12/24 17:25:06</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/24/4081s909794.htm" title="Vanke Welcomes Anbang's Increase in Shareholding" class="title_default">Vanke Welcomes Anbang's Increase in Shareholding</a></h3>
            <p class="item-infor" title="China Vanke has issued a statement on Wednesday night, welcoming Anbang Insurance to become one of its big share-holders.">China Vanke has issued a statement on Wednesday night, welcoming Anbang Insurance to become one of its big share-holders.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/national/Beijing-police-heighten-security-amid-warnings-by-western-embassies/shdaily.shtml" title="Beijing police heighten security amid warnings by western embassies"><img src="http://img01.abroad.imgcdc.com/english/news/china/54/20151224/533972_140136_200x120.jpg" width="200" height="120" alt="Beijing police heighten security amid warnings by western embassies" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/24 17:23:47</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/national/Beijing-police-heighten-security-amid-warnings-by-western-embassies/shdaily.shtml" title="Beijing police heighten security amid warnings by western embassies" class="title_default">Beijing police heighten security amid warnings by western embassies</a></h3>
            <p class="item-infor" title="Beijing police on Thursday announced âyellow security alarmâ and will guard medium to large sized shopping malls and supermarkets during Christmas and New Year.">Beijing police on Thursday announced âyellow security alarmâ and will guard medium to large sized shopping malls and supermarkets during Christmas and New Year.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/2015-12/24/content_22793876.htm" title="Russia wants Khodorkovsky arrested abroad on murder charges"><img src="http://img02.mini.abroad.imgcdc.com/english/news/world/55/20151224/533970_140133.jpg.200x120.jpg" width="160" height="103" alt="Russia wants Khodorkovsky arrested abroad on murder charges" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/12/24 17:21:24</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/2015-12/24/content_22793876.htm" title="Russia wants Khodorkovsky arrested abroad on murder charges" class="title_default">Russia wants Khodorkovsky arrested abroad on murder charges</a></h3>
            <p class="item-infor" title="Russia has issued an international arrest warrant for former oil tycoon Mikhail Khodorkovsky on suspicion of ordering a contract killing.">Russia has issued an international arrest warrant for former oil tycoon Mikhail Khodorkovsky on suspicion of ordering a contract killing.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/24/3781s909784.htm" title="Four Cities Selected to Co-Present CCTV Spring Festival Gala"><img src="http://img03.mini.abroad.imgcdc.com/english/news/showbiz/58/20151224/533512_139982.jpg.200x120.jpg" width="200" height="120" alt="Four Cities Selected to Co-Present CCTV Spring Festival Gala" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/12/24 09:26:03</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/24/3781s909784.htm" title="Four Cities Selected to Co-Present CCTV Spring Festival Gala" class="title_default">Four Cities Selected to Co-Present CCTV Spring Festival Gala</a></h3>
            <p class="item-infor" title="Four cities have been selected to co-present the annual CCTV Spring Festival Gala. Quanzhou, Xi'an, Shenzhen, and Hulunbeir will join Beijing to celebrate the Year of the Monkey.">Four cities have been selected to co-present the annual CCTV Spring Festival Gala. Quanzhou, Xi'an, Shenzhen, and Hulunbeir will join Beijing to celebrate the Year of the Monkey.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-12/24/c_134946755.htm" title="FIS bans camera drones from world cup"><img src="" width="" height="" alt="FIS bans camera drones from world cup" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/24 09:25:14</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-12/24/c_134946755.htm" title="FIS bans camera drones from world cup" class="title_default">FIS bans camera drones from world cup</a></h3>
            <p class="item-infor" title="The International Ski Federation (FIS) is banning camera drones from its World Cup races after a drone crashed and nearly hit Austrian skier Marcel Hirscher during a slalom in Italy.">The International Ski Federation (FIS) is banning camera drones from its World Cup races after a drone crashed and nearly hit Austrian skier Marcel Hirscher during a slalom in Italy.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.globaltimes.cn/content/960229.shtml" title="National carbon market coming in 2017"><img src="http://img02.mini.abroad.imgcdc.com/english/news/business/56/20151224/533510_139981.jpeg.200x120.jpeg" width="200" height="120" alt="National carbon market coming in 2017" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/12/24 09:24:04</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.globaltimes.cn/content/960229.shtml" title="National carbon market coming in 2017" class="title_default">National carbon market coming in 2017</a></h3>
            <p class="item-infor" title="China will launch a national carbon market in 2017, Xie Zhenhua, China's special representative on climate change, told a press conference Wednesday.">China will launch a national carbon market in 2017, Xie Zhenhua, China's special representative on climate change, told a press conference Wednesday.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/world/No-US-trip-for-Muslim-family-irks-British-MP/shdaily.shtml" title="No US trip for Muslim family irks British MP"><img src="" width="" height="" alt="No US trip for Muslim family irks British MP" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/12/24 09:05:10</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/world/No-US-trip-for-Muslim-family-irks-British-MP/shdaily.shtml" title="No US trip for Muslim family irks British MP" class="title_default">No US trip for Muslim family irks British MP</a></h3>
            <p class="item-infor" title="Prime Minister David Cameronâs office says he will look into a lawmakerâs claim that United States officials prevented a British Muslim family from flying to Disneyland for a planned holiday.">Prime Minister David Cameronâs office says he will look into a lawmakerâs claim that United States officials prevented a British Muslim family from flying to Disneyland for a planned holiday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/china/2015-12/24/content_22790882.htm" title="Xi stresses correct implementation of 'One country, Two systems' in HK"><img src="http://img04.mini.abroad.imgcdc.com/english/news/china/54/20151224/533467_139971.jpg.200x120.jpg" width="200" height="120" alt="Xi stresses correct implementation of 'One country, Two systems' in HK" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/24 09:00:17</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/china/2015-12/24/content_22790882.htm" title="Xi stresses correct implementation of 'One country, Two systems' in HK" class="title_default">Xi stresses correct implementation of 'One country, Two systems' in HK</a></h3>
            <p class="item-infor" title="President Xi Jinping on Wednesday urged adherence and correct implementation of the "One Country, Two Systems" principle in Hong Kong Special Administrative Region (HKSAR).">President Xi Jinping on Wednesday urged adherence and correct implementation of the "One Country, Two Systems" principle in Hong Kong Special Administrative Region (HKSAR).</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/23/4201s909613.htm" title="Over 1m Refugees, Migrants Enter Europe in 2015"><img src="http://img03.abroad.imgcdc.com/english/news/sports/57/20151223/533164_139878_200x120.jpg" width="200" height="120" alt="Over 1m Refugees, Migrants Enter Europe in 2015" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/23 16:54:53</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/23/4201s909613.htm" title="Over 1m Refugees, Migrants Enter Europe in 2015" class="title_default">Over 1m Refugees, Migrants Enter Europe in 2015</a></h3>
            <p class="item-infor" title="Migration experts have suggested that more than one million people driven out their countries by war, poverty, and persecution have entered Europe this year, more than four times as many as last year. ">Migration experts have suggested that more than one million people driven out their countries by war, poverty, and persecution have entered Europe this year, more than four times as many as last year. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/china/2015-12/23/content_22780407.htm" title="After Beijing, Tianjin and four Hebei cities issue red alert"><img src="http://img01.abroad.imgcdc.com/english/news/china/54/20151223/533152_139876_200x120.jpg" width="200" height="120" alt="After Beijing, Tianjin and four Hebei cities issue red alert" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/23 16:52:06</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/china/2015-12/23/content_22780407.htm" title="After Beijing, Tianjin and four Hebei cities issue red alert" class="title_default">After Beijing, Tianjin and four Hebei cities issue red alert</a></h3>
            <p class="item-infor" title="Tianjin issued its first red alert for smog on Tuesday, following Beijing and some cities in Hebei province that had already raised the highest-level warning.">Tianjin issued its first red alert for smog on Tuesday, following Beijing and some cities in Hebei province that had already raised the highest-level warning.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-12/23/c_134943883.htm" title="FIFA: Platini can not appeal to CAS directly"><img src="http://img04.abroad.imgcdc.com/english/news/sports/57/20151223/533149_139875_200x120.jpg" width="200" height="120" alt="FIFA: Platini can not appeal to CAS directly" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/23 16:50:22</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-12/23/c_134943883.htm" title="FIFA: Platini can not appeal to CAS directly" class="title_default">FIFA: Platini can not appeal to CAS directly</a></h3>
            <p class="item-infor" title="Suspended UEFA president Michel Platini was told by FIFA that he cannot bypass its appeals process by challenging his eight-year ban directly at the Court of Arbitration for Sport.">Suspended UEFA president Michel Platini was told by FIFA that he cannot bypass its appeals process by challenging his eight-year ban directly at the Court of Arbitration for Sport.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/23/3781s909612.htm" title="Chinese Media Regulator Takes Aim at Bootleg Recording in Cinemas"><img src="http://img01.mini.abroad.imgcdc.com/english/news/showbiz/58/20151223/532722_139748.jpg.200x120.jpg" width="200" height="120" alt="Chinese Media Regulator Takes Aim at Bootleg Recording in Cinemas" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/12/23 09:14:01</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/23/3781s909612.htm" title="Chinese Media Regulator Takes Aim at Bootleg Recording in Cinemas" class="title_default">Chinese Media Regulator Takes Aim at Bootleg Recording in Cinemas</a></h3>
            <p class="item-infor" title="China's media regulator is moving to crackdown on bootleg recordings. The move comes as several hit movies just launched have already had bootleg versions released online.">China's media regulator is moving to crackdown on bootleg recordings. The move comes as several hit movies just launched have already had bootleg versions released online.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/business/economy/GDP-growth-goal-may-be-cut-in-2016/shdaily.shtml" title="GDP growth goal may be cut in 2016"><img src="" width="" height="" alt="GDP growth goal may be cut in 2016" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/12/23 09:12:56</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/business/economy/GDP-growth-goal-may-be-cut-in-2016/shdaily.shtml" title="GDP growth goal may be cut in 2016" class="title_default">GDP growth goal may be cut in 2016</a></h3>
            <p class="item-infor" title="China may lower the target for economic growth next year to 6.5 percent from around 7 percent this year, economists said yesterday after the conclusion of the annual Central Economic Work Conference.">China may lower the target for economic growth next year to 6.5 percent from around 7 percent this year, economists said yesterday after the conclusion of the annual Central Economic Work Conference.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.globaltimes.cn/content/960055.shtml" title="Over 1 million migrants reach Europe: UN"><img src="" width="" height="" alt="Over 1 million migrants reach Europe: UN" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/12/23 09:12:14</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.globaltimes.cn/content/960055.shtml" title="Over 1 million migrants reach Europe: UN" class="title_default">Over 1 million migrants reach Europe: UN</a></h3>
            <p class="item-infor" title="More than 1 million migrants and refugees reached Europe this year, including over 970,000 who made the dangerous journey across the Mediterranean, the UN's refugee agency said Tuesday.">More than 1 million migrants and refugees reached Europe this year, including over 970,000 who made the dangerous journey across the Mediterranean, the UN's refugee agency said Tuesday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/china/2015-12/23/content_22779868.htm" title="First survivor pulled out 67 hours after Shenzhen landslide"><img src="http://img04.mini.abroad.imgcdc.com/english/news/china/54/20151223/532708_139747.jpg.200x120.jpg" width="200" height="120" alt="First survivor pulled out 67 hours after Shenzhen landslide" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/23 09:11:26</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/china/2015-12/23/content_22779868.htm" title="First survivor pulled out 67 hours after Shenzhen landslide" class="title_default">First survivor pulled out 67 hours after Shenzhen landslide</a></h3>
            <p class="item-infor" title="The first survivor was pulled out and 74 people were still missing on Wednesday at the site where a giant flow of mud and construction waste engulfed buildings in Shenzhen, Guangdong province, on Sunday.">The first survivor was pulled out and 74 people were still missing on Wednesday at the site where a giant flow of mud and construction waste engulfed buildings in Shenzhen, Guangdong province, on Sunday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/culture/2015-12/22/content_22772616.htm" title="Chinese Characters and Words of 2015 Released"><img src="http://img03.mini.abroad.imgcdc.com/english/news/china/54/20151222/532614_139722.jpg.200x120.jpg" width="200" height="100" alt="Chinese Characters and Words of 2015 Released" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/22 21:08:41</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/culture/2015-12/22/content_22772616.htm" title="Chinese Characters and Words of 2015 Released" class="title_default">Chinese Characters and Words of 2015 Released</a></h3>
            <p class="item-infor" title="China released its annual characters and words of 2015 on Monday, with å»(incorruptibility) and äºèç½+(Internet Plus) winning out among the candidate characters and words.">China released its annual characters and words of 2015 on Monday, with å»(incorruptibility) and äºèç½+(Internet Plus) winning out among the candidate characters and words.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/22/3941s909562.htm" title="New Design for 2020 Olympic Stadium Approved"><img src="http://img03.mini.abroad.imgcdc.com/english/news/sports/57/20151222/532604_139702.jpg.200x120.jpg" width="200" height="120" alt="New Design for 2020 Olympic Stadium Approved" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/22 20:00:55</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/22/3941s909562.htm" title="New Design for 2020 Olympic Stadium Approved" class="title_default">New Design for 2020 Olympic Stadium Approved</a></h3>
            <p class="item-infor" title="The new design for the 2020 Tokyo Olympics stadium has been approved. In approving the design, Japanese Prime Minister Shinzo Abe says the new stadium will leave a legacy that can be passed on to the next generation.
">The new design for the 2020 Tokyo Olympics stadium has been approved. In approving the design, Japanese Prime Minister Shinzo Abe says the new stadium will leave a legacy that can be passed on to the next generation.
</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/22/3941s909564.htm" title="Atlanta Hawks Beat Portland Trail Blazers 106-97"><img src="http://img02.mini.abroad.imgcdc.com/english/news/sports/57/20151222/532603_139701.jpg.200x120.jpg" width="200" height="120" alt="Atlanta Hawks Beat Portland Trail Blazers 106-97" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/22 19:59:50</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/22/3941s909564.htm" title="Atlanta Hawks Beat Portland Trail Blazers 106-97" class="title_default">Atlanta Hawks Beat Portland Trail Blazers 106-97</a></h3>
            <p class="item-infor" title="The Atlanta Hawks have earned their 4th straight win, downing the Portland Trail Blazers 106-97 on Tuesday.">The Atlanta Hawks have earned their 4th straight win, downing the Portland Trail Blazers 106-97 on Tuesday.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-12/22/c_134941817.htm" title="China to Sell 160,000 3D Printers in 2016"><img src="" width="" height="" alt="China to Sell 160,000 3D Printers in 2016" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/12/22 19:58:59</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-12/22/c_134941817.htm" title="China to Sell 160,000 3D Printers in 2016" class="title_default">China to Sell 160,000 3D Printers in 2016</a></h3>
            <p class="item-infor" title="Shipment of 3D printers in the Chinese market is expected to reach 160,000 units in 2016, outperforming the U.S. market, market research firm IDC said on Tuesday.">Shipment of 3D printers in the Chinese market is expected to reach 160,000 units in 2016, outperforming the U.S. market, market research firm IDC said on Tuesday.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-12/22/c_134941909.htm" title="China Vows More Efforts on City Clusters, Major Cities"><img src="" width="" height="" alt="China Vows More Efforts on City Clusters, Major Cities" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/22 19:57:25</em><em class="hide">December 25 2015 02:41:47</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-12/22/c_134941909.htm" title="China Vows More Efforts on City Clusters, Major Cities" class="title_default">China Vows More Efforts on City Clusters, Major Cities</a></h3>
            <p class="item-infor" title="China will make more effort to improve city clusters in the eastern region, and foster new clusters and key regional cities in central and western regions, said a statement issued Tuesday after the Central Urban Work Conference.">China will make more effort to improve city clusters in the eastern region, and foster new clusters and key regional cities in central and western regions, said a statement issued Tuesday after the Central Urban Work Conference.</p>
          </div>
        </div>
      </div>
      <div class="page-latest-more">
        <a href="#" id="page-latest-show-more"><em class="page-latest-more-icon">&nbsp;</em></a>
        <a href="/news/index.html" id="page-latest-click-more"><em class="page-latest-more-icon">Show More</em></a>
      </div>
    </div><!-- page-latest End -->
    <div class="page-left-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=207" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 End -->
	</div> 
    <div class="page-video">
      <h2 class="modTit"><strong><a href="/video/index.html">VIDEO</a></strong></h2>
      <div class="page-video-body">
        <div class="page-video-left">
          <script type="text/javascript" src="http://c.wrating.com/v2_pre.js"></script>
<!--noscript-->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="420" height="234">
            <param name="movie" value="http://english.china.com/videoPlayer/video.swf"/>
            <param name="quality" value="high"/>
            <param name="bgcolor" value="#ffffff"/>
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="true"/>
            <param name="wmode" value="Opaque">
            <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/12/1213todayinhistory.mp4&loop=1&autoplay=0"/>
            <!--[if!IE]>
            -->
            <object type="application/x-shockwave-flash" data="http://english.china.com/videoPlayer/video.swf" width="420" height="234">
              <param name="quality" value="high"/>
              <param name="bgcolor" value="#ffffff"/>
              <param name="allowScriptAccess" value="sameDomain"/>
              <param name="allowFullScreen" value="true"/>
              <param name="wmode" value="Opaque">
              <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/12/1213todayinhistory.mp4&loop=1&autoplay=0"/>
              <!--<![endif]-->
              <!--[if gte IE 6]>
              -->
              <p>
                Either scripts and active content are not permitted to run or Adobe Flash Player version 11.4.0 or greater is not installed.
              </p>
              <!--<![endif]-->
              <a href="http://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player"/>
              </a>
              <!--[if!IE]>--></object>
            <!--<![endif]-->
          </object>
          <!--/noscript-->
          <a href="/home/videobig/1299/20151213/525000.html" class="video-tit">Today in History: The War against Japanese Aggression, 1937.12.13</a>
        </div>
        <div class="page-video-right" id="page-video-right">
          <div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20151202/518653.html"><img src="http://img04.abroad.imgcdc.com/english/home/videosmall/1301/20151202/518679_135675.jpg" width="245" height="125" alt="My Chinese Life: Ben Giaimo--A Foreign Disciple of Confucius" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20151202/518653.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">My Chinese Life: Ben Giaimo--A Foreign Disciple of Confucius</strong></h3>
              <p class="item-infor">American student, Ben, is trying to sell Confucian wisdom to the west by using American slang.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div><div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20151028/491957.html"><img src="http://img04.abroad.imgcdc.com/english/home/videosmall/1301/20151028/491968_127479.jpg" width="245" height="125" alt="Along the Silk Road:Dev Raturi--Indian Ingenuity" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20151028/491957.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road:Dev Raturi--Indian Ingenuity</strong></h3>
              <p class="item-infor">Dev brings India's unique culture to China, showcasing its food, and rediscovering Silk Road ties.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div><!-- page-video End -->
    <div class="page-mods">
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/travel/index.html">TRAVEL</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners"><img src="http://img03.mini.abroad.imgcdc.com/english/travel/listright/mostpopular/1534/20150506/366181_88898.jpg.330x190.jpg" width="330" height="190" alt="Food Awards 2015: the winners" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners" class="title_default">Food Awards 2015: the winners</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12394/2015/08/11/2743s891245.htm" title="Japanese War Orphan Recounts Past" class="title_default">Japanese War Orphan Recounts Past</a></li><li><a href="http://english.cri.cn/6566/2014/12/25/44s858261.htm" title="Winter Nadam Kicks Off in North China" class="title_default">Winter Nadam Kicks Off in North China</a></li><li><a href="http://english.cri.cn/6566/2014/09/29/44s845986.htm" title="To Experience Authentic Taiwan Folk Art in Beijing" class="title_default">To Experience Authentic Taiwan Folk Art in Beijing</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/chinese/index.html">LEARN CHINESE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love"><img src="http://img03.mini.abroad.imgcdc.com/english/home/learnpic/1315/20141124/211729_51886.jpg.330x190.jpg" width="330" height="190" alt="China's First Love" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love" class="title_default">China's First Love</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12514/2014/10/16/2001s848110.htm" title="Top 10 Popular Chinese TV Dramas Overseas" class="title_default">Top 10 Popular Chinese TV Dramas Overseas</a></li><li><a href="http://english.cri.cn/12514/2014/10/17/2001s848240.htm" title="çµç¶ Chinese Pipa" class="title_default">çµç¶ Chinese Pipa</a></li><li><a href="http://english.cri.cn/12514/2014/09/25/2001s845407.htm" title="Useful Shopping Sentences in Chinese" class="title_default">Useful Shopping Sentences in Chinese</a></li>
          </ul>
        </div>

      </div>
      
      
    </div><!-- page-mods End -->
    <!--<div class="page-left-ad"><a href="#"><img src="/file/left-ad-2.jpg" /></a></div> page-left-ad End -->
    <!-- page-photos End -->
  </div><!-- page-left End -->
  <div class="page-right">
    
    <div class="page-right-ad noMarginTop">
      <!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=208" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 End -->
    </div><!-- page-right-ad End -->
    <!--include virtual="/etc/right_top_ad_index.shtml" -->
      
    ï»¿<!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://english.cri.cn/12394/2015/09/21/Zt2821s896890.htm"><img src="http://img04.abroad.imgcdc.com/english/home/imgtj/5829/20151014/482283_124639.jpg" width="293" height="88" alt="20151014" /></a><a href="http://english.cri.cn/12394/2015/09/02/Zt2821s894283.htm"><img src="http://img01.abroad.imgcdc.com/english/home/imgtj/5829/20150908/482287_124640.jpg" width="293" height="88" alt="20150908" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->

    <!-- page-right-ad End -->
    
    <div class="page-watched">
      <h2 class="modTit"><strong>Most Watched</strong></h2>
      <div class="page-watched-body" id="rank-video">
      </div>
    </div><!-- page-watched End -->
    <div class="page-popular">
      <h2 class="modTit"><strong>Most Popular</strong></h2>
      <ul class="page-popular-body" id="rank-list"></ul>
    </div><!-- page-popular End -->
    <div class="page-right-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=209" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 End -->
	</div> 
    <div class="page-tochina">
      <h2 class="modTit"><strong>Tune in to China</strong></h2>
      <div class="page-tochina-body">
        <div class="item radio-news">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="title">-</h3>
              <h4 id="playtime">-</h4>
              <a href="http://english.cri.cn/7146/2012/12/03/301s736372.htm" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/am846.wsx" class="item-play"></a>
          </div>
        </div>
        <div class="item radio-ez">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="nexttitle">-</h3>
              <h4 id="nexttime">-</h4>
              <a href="http://english.cri.cn/easyfm/ezplaytime.html" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/fm915.wsx" class="item-play"></a>
          </div>
        </div>
      </div>
    </div><!-- page-tochina End -->

    
    <div class="page-hotListening">
      <h2 class="page-hotListening-tit"><strong>Hot Listening</strong></h2>
      <div class="page-hotListening-body">
        <div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/pik.htm"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4933_1370.jpg" width="120" height="90" alt="People in the Know" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/pik.htm" class="title_default">People in the Know</a><a href="http://english.cri.cn/cribb/plus/pik.htm" class="icon-horn"></a></h3>
            <p class="item-infor">PIK gives you insights to the world through interviews.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/today.htm"><img src="http://img02.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4931_1369.jpg" width="120" height="90" alt="Today" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/today.htm" class="title_default">Today</a><a href="http://english.cri.cn/cribb/plus/today.htm" class="icon-horn"></a></h3>
            <p class="item-infor">A news magazine show with in-depth panel discussions.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/easymorning.html"><img src="http://img01.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4928_1368.jpg" width="120" height="90" alt="EZ Morning" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/easymorning.html" class="title_default">EZ Morning</a><a href="http://english.cri.cn/easyfm/easymorning.html" class="icon-horn"></a></h3>
            <p class="item-infor">It lights up your mornings with interesting chit-chats.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/ezwheel.html"><img src="http://img04.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4927_1367.jpg" width="120" height="90" alt="More to Learn" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/ezwheel.html" class="title_default">More to Learn</a><a href="http://english.cri.cn/easyfm/ezwheel.html" class="icon-horn"></a></h3>
            <p class="item-infor">More to Learn is filled up with English stories and anecdotes.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/hour.html"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4923_1366.jpg" width="120" height="90" alt="The Beijing Hour" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/hour.html" class="title_default">The Beijing Hour</a><a href="http://english.cri.cn/easyfm/hour.html" class="icon-horn"></a></h3>
            <p class="item-infor">It opens up the world to you with latest news updates.</p>
          </div>
        </div>
      </div>
    </div><!-- page-hotListening End -->

    <div class="page-mobile">
      <h2 class="modTit"><strong>Mobile</strong></h2>
      <div class="page-mobile-body">
        <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=539404062&mt=8&s=143441" class="iphone" title="Mobile Iphone">Iphone</a>
      </div>
    </div><!-- page-mobile End -->

    <div class="page-cooperation">
      <h2 class="modTit"><strong>Cooperation</strong></h2>
      <div class="page-cooperation-body">
        <a href="http://gc.wrating.com/click.php?a=&c=860099-1000099998&cs=341_285_2559_860010_400000000&ds=354_355_356_357_358&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dmobi.mgeek.TunnyBrowser%26referrer%3Dchannel_id%253Dchinacom%2526utm_source%253Dchinacom"><img src="/file/logo-dolphin-140-90.png" height="90" width="140"></a>
      </div>
    </div><!-- page-cooperation End -->

  </div><!-- page-right End -->
</div><!-- page-main End -->

<script>
window.collectMethod_rank = window.collectMethod_rank || [];
collectMethod_rank.push(function () {
  setRank("rank-video", 3, "http://english.china.com/js/english_43_day.js", "video", function(){
    setRank("rank-list", 5, "http://english.china.com/js/english_22_day.js", "list");
  });
});
</script>

<!-- /etc/channelsitemap.shtml Start -->
<div class="page-map">
  <div class="page-map-body maxWidth">
  <dl class="item">
      <dt><a href="http://english.china.com/news/index.html">News:</a></dt>
      <dd>
        <a href="http://english.china.com/news/china/index.html">China</a>
        <a href="http://english.china.com/news/world/index.html">World</a>
        <a href="http://english.china.com/news/business/index.html">Business</a>
        <a href="http://english.china.com/news/sports/index.html">Sports</a>
        <a href="http://english.china.com/news/showbiz/index.html">Showbiz</a>
		<a href="http://english.china.com/audio/index.html">Audio</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/video/index.html">Video:</a></dt>
      <dd>
        <a href="http://english.china.com/video/c4/index.html">C4</a>
        <a href="http://english.china.com/video/life/index.html">My Chinese Life</a>
        <a href="http://english.china.com/video/thesoundstage/index.html">The Sound Stage</a>
        <a href="http://english.china.com/video/chinarevealed/index.html">China Revealed</a>
        <a href="http://english.china.com/video/showbiz/index.html">Showbiz Video</a>
        <a href="http://english.china.com/video/tour/index.html">Travel Video</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/photos/index.html">Photos:</a></dt>
      <dd>
        <a href="http://english.china.com/photos/china/index.html">China</a>
        <a href="http://english.china.com/photos/world/index.html">World</a>
        <a href="http://english.china.com/photos/fun/index.html">Fun</a>
        <a href="http://english.china.com/photos/travel/index.html">Travel</a>
        <a href="http://english.china.com/photos/entertainment/index.html">Entertainment</a>
        <a href="http://english.china.com/photos/sports/index.html">Sports</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/travel/index.html">Travel:</a></dt>
      <dd>
        <a href="http://english.china.com/travel/beijing/index.html">Beijing</a>
        <a href="http://english.china.com/travel/shanghai/index.html">Shanghai</a>
        <a href="http://english.china.com/travel/guangzhou/index.html">Guangzhou</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/lifestyle/index.html">Lifestyle:</a></dt>
      <dd>        
        <a href="http://english.china.com/lifestyle/livemusic/index.html">Live Music</a>
        <a href="http://english.china.com/lifestyle/opera/index.html">Opera & Classical</a>
        <a href="http://english.china.com/lifestyle/movies/index.html">Movies</a>
        <a href="http://english.china.com/lifestyle/traditionalshows/index.html">Traditional Shows</a>
        <a href="http://english.china.com/lifestyle/exhibitions/index.html">Exhibitions</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/chinese/index.html">Learn Chinese:</a></dt>
      <dd>
        <a href="http://english.china.com/chinese/studio/index.html">Chinese Studio</a>
        <a href="http://english.china.com/chinese/living/index.html">Living Chinese</a>
        <a href="http://english.china.com/chinese/everyday/index.html">Everyday Chinese</a>
        <a href="http://english.china.com/chinese/justforfun/index.html">Just For Fun</a>
        <a href="http://english.china.com/chinese/culture/index.html">Chinese Culture</a>
        <a href="http://english.china.com/chinese/buzzwords/index.html">Buzzwords</a>        
      </dd>
    </dl>      
  </div>
</div><!-- page-map End -->
<!-- /etc/channelsitemap.shtml End -->

<div class="page-link">
  <div class="page-link-body maxWidth">
    <a href="http://english.cri.cn/">CRIENGLISH.com</a>|<a href="http://www.chinadaily.com.cn/">China Daily</a>|<a href="http://www.xinhuanet.com/english/">Xinhua</a>|<a href="http://www.china.org.cn/index.htm">China.org.cn</a>|<a href="http://english.cntv.cn/">CNTV</a>
  </div>
</div><!-- page-link End -->

<!-- /etc/channelcopyright.shtml Start -->
<div class="page-footer">
  <div class="page-foot-link">
    <a href="/about/">About China.com</a>|<a href="/about/gmg.html">About GMG</a>|<a href="/ad/">Ad Services</a>|<a href="/contact/">Contact Information</a>|<a href="/copyright/">Copyright Notice</a>
  </div>
  <p><!--E-mail to:<a href="mailto:english@bj.china.com">english@bj.china.com</a><br />-->Copyright &copy; China.com All Rights Reserved</p>
</div><!-- page-footer End -->

<script src="/js/require.min.js" data-main="/js/main"></script>

<!-- START WRating v1.0 -->
<script type="text/javascript" src="http://c.wrating.com/a1.js">
</script>
<script type="text/javascript">
var vjAcc="860010-0446010000";
var wrUrl="http://c.wrating.com/";
vjTrack("");
</script>
<noscript><img src="http://c.wrating.com/a.gif?a=&c=860010-0446010000" width="1" 
height="1"/></noscript>
<!-- END WRating v1.0 -->

<!-- Start Alexa Certify Javascript #13481-->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S6Upi1awA+00a/", domain:"china.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S6Upi1awA+00a/" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript #13481-->


<!-- Start Google Analytics #16010-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60581848-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics #16010-->

<!-- /etc/channelcopyright.shtml End -->
</body>
</html>