<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-0531997.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-0531997.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-0531997.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-0531997.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-0531997.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '0531997',
            detect_lang: 0,
            spare_click: 1        };
    </script>
    <script src="//kastatic.com/js/all-0531997.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom" style="color: #ffeeb4; font-size: 34px; float:left; height: 50px; line-height: 50px;"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080%20ted%202/" class="tag2">1080 ted 2</a>
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/1080p/" class="tag3">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag8">2015</a>
	<a href="/search/2015/" class="tag6">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/3d%20remux/" class="tag2">3d remux</a>
	<a href="/search/android/" class="tag6">android</a>
	<a href="/search/android/" class="tag3">android</a>
	<a href="/search/avengers/" class="tag2">avengers</a>
	<a href="/search/avengers%20age%20of%20ultron/" class="tag2">avengers age of ultron</a>
	<a href="/search/batman/" class="tag3">batman</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/fast%20and%20furious%207/" class="tag2">fast and furious 7</a>
	<a href="/search/flac/" class="tag2">flac</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/game%20of%20thrones/" class="tag3">game of thrones</a>
	<a href="/search/hindi/" class="tag9">hindi</a>
	<a href="/search/hindi/" class="tag6">hindi</a>
	<a href="/search/hindi%202015/" class="tag3">hindi 2015</a>
	<a href="/search/humans/" class="tag2">humans</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/insurgent/" class="tag3">insurgent</a>
	<a href="/search/ita/" class="tag4">ita</a>
	<a href="/search/mad%20max/" class="tag2">mad max</a>
	<a href="/search/malayalam/" class="tag2">malayalam</a>
	<a href="/search/man%20of%20steel/" class="tag2">man of steel</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag3">movies</a>
	<a href="/search/mr%20robot/" class="tag4">mr robot</a>
	<a href="/search/mr%20robot%20s01e03/" class="tag2">mr robot s01e03</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/penny%20dreadful/" class="tag2">penny dreadful</a>
	<a href="/search/san%20andreas/" class="tag2">san andreas</a>
	<a href="/search/spy/" class="tag2">spy</a>
	<a href="/search/suits/" class="tag3">suits</a>
	<a href="/search/suits%20s05e03/" class="tag2">suits s05e03</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/ted%202/" class="tag2">ted 2</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/terminator%20genisys/" class="tag2">terminator genisys</a>
	<a href="/search/the%20big%20bang%20theory/" class="tag4">the big bang theory</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="rsssign" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917129,0" class="icomment icommentjs icon16" href="/jurassic-world-2015-1080p-hdrip-korsub-x264-aac2-0-rarbg-t10917129.html#comment"> <em class="iconvalue">236</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/jurassic-world-2015-1080p-hdrip-korsub-x264-aac2-0-rarbg-t10917129.html" class="cellMainLink">Jurassic World 2015 1080p HDRip KORSUB x264 AAC2 0-RARBG</a></div>
			</td>
			<td class="nobr center">4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">14&nbsp;hours</td>
			<td class="green center">12862</td>
			<td class="red lasttd center">30874</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10916632,0" class="icomment icommentjs icon16" href="/ted-2-2015-hc-hdrip-xvid-etrg-t10916632.html#comment"> <em class="iconvalue">49</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ted-2-2015-hc-hdrip-xvid-etrg-t10916632.html" class="cellMainLink">Ted 2.2015.HC.HDRip.XViD-ETRG</a></div>
			</td>
			<td class="nobr center">1.39 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">16&nbsp;hours</td>
			<td class="green center">11927</td>
			<td class="red lasttd center">16441</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10912960,0" class="icomment icommentjs icon16" href="/final-girl-2015-brrip-xvid-ac3-evo-t10912960.html#comment"> <em class="iconvalue">44</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/final-girl-2015-brrip-xvid-ac3-evo-t10912960.html" class="cellMainLink">Final Girl 2015 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center">1.43 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">9791</td>
			<td class="red lasttd center">8916</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914215,0" class="icomment icommentjs icon16" href="/strangerland-2015-hdrip-xvid-etrg-t10914215.html#comment"> <em class="iconvalue">54</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/strangerland-2015-hdrip-xvid-etrg-t10914215.html" class="cellMainLink">Strangerland 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center">708.49 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">7626</td>
			<td class="red lasttd center">7440</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917731,0" class="icomment icommentjs icon16" href="/ted-2-2015-uncensored-1080p-hc-webrip-x264-aac2-0-rarbg-t10917731.html#comment"> <em class="iconvalue">46</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ted-2-2015-uncensored-1080p-hc-webrip-x264-aac2-0-rarbg-t10917731.html" class="cellMainLink">Ted 2 2015 UNCENSORED 1080p HC WEBRip x264 AAC2 0-RARBG</a></div>
			</td>
			<td class="nobr center">3.79 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">11&nbsp;hours</td>
			<td class="green center">4850</td>
			<td class="red lasttd center">8043</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10917557,0" class="icomment icommentjs icon16" href="/insurgent-2015-1080p-hdrip-x264-dd5-1-rarbg-t10917557.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/insurgent-2015-1080p-hdrip-x264-dd5-1-rarbg-t10917557.html" class="cellMainLink">Insurgent 2015 1080p HDRip x264 DD5 1-RARBG</a></div>
			</td>
			<td class="nobr center">4.04 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">12&nbsp;hours</td>
			<td class="green center">3006</td>
			<td class="red lasttd center">6640</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10918465,0" class="icomment icommentjs icon16" href="/american-heist-2014-truefrench-bdrip-xvid-avitech-avi-t10918465.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/american-heist-2014-truefrench-bdrip-xvid-avitech-avi-t10918465.html" class="cellMainLink">American Heist 2014 TRUEFRENCH BDRiP XViD-AViTECH avi</a></div>
			</td>
			<td class="nobr center">700.71 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">8&nbsp;hours</td>
			<td class="green center">4908</td>
			<td class="red lasttd center">4353</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908684,0" class="icomment icommentjs icon16" href="/intimate-enemies-2015-720p-hdrip-x264-aac-kth-movietam-t10908684.html#comment"> <em class="iconvalue">23</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/intimate-enemies-2015-720p-hdrip-x264-aac-kth-movietam-t10908684.html" class="cellMainLink">Intimate Enemies 2015 720p HDRip X264 AAC-KTH [MovietaM]</a></div>
			</td>
			<td class="nobr center">2.65 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">6535</td>
			<td class="red lasttd center">2231</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917218,0" class="icomment icommentjs icon16" href="/eng-jurassic-world-2015-720p-hc-hdrip-x264-ac3-evo-t10917218.html#comment"> <em class="iconvalue">21</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/eng-jurassic-world-2015-720p-hc-hdrip-x264-ac3-evo-t10917218.html" class="cellMainLink">ENG - Jurassic World 2015 720p HC HDRip X264 AC3-EVO</a></div>
			</td>
			<td class="nobr center">2.56 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">2626</td>
			<td class="red lasttd center">5721</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910226,0" class="icomment icommentjs icon16" href="/x-men-days-of-future-past-2014-the-rogue-cut-1080p-bluray-x264-sadpanda-rarbg-t10910226.html#comment"> <em class="iconvalue">50</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/x-men-days-of-future-past-2014-the-rogue-cut-1080p-bluray-x264-sadpanda-rarbg-t10910226.html" class="cellMainLink">X-Men Days of Future Past 2014 THE ROGUE CUT 1080p BluRay x264-SADPANDA[rarbg]</a></div>
			</td>
			<td class="nobr center">10.93 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">2888</td>
			<td class="red lasttd center">5078</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10912535,0" class="icomment icommentjs icon16" href="/fast-and-furious-7-hdrip-xvid-ac3-evo-t10912535.html#comment"> <em class="iconvalue">42</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fast-and-furious-7-hdrip-xvid-ac3-evo-t10912535.html" class="cellMainLink">Fast and Furious 7 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center">1.44 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">3317</td>
			<td class="red lasttd center">4309</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10916276,0" class="icomment icommentjs icon16" href="/chirakodinja-kinavukal-2015-malayalam-dvd-rip-1cd-mp4-aac-5-1-esub-team-tmr-t10916276.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/chirakodinja-kinavukal-2015-malayalam-dvd-rip-1cd-mp4-aac-5-1-esub-team-tmr-t10916276.html" class="cellMainLink">Chirakodinja Kinavukal (2015) - Malayalam - DVD Rip - 1CD - Mp4 - AAC 5.1 - Esub - Team TMR</a></div>
			</td>
			<td class="nobr center">700.14 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">18&nbsp;hours</td>
			<td class="green center">3162</td>
			<td class="red lasttd center">3394</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10916774,0" class="icomment icommentjs icon16" href="/true-story-2015-1080p-web-dl-dd5-1-h-264-rarbg-t10916774.html#comment"> <em class="iconvalue">19</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/true-story-2015-1080p-web-dl-dd5-1-h-264-rarbg-t10916774.html" class="cellMainLink">True Story 2015 1080p WEB-DL DD5 1 H 264 RARBG</a></div>
			</td>
			<td class="nobr center">3.75 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">16&nbsp;hours</td>
			<td class="green center">2375</td>
			<td class="red lasttd center">3831</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10907814,0" class="icomment icommentjs icon16" href="/terminator-genisys-2015-new-hdts-xvid-ac3-readnfo-mrg-t10907814.html#comment"> <em class="iconvalue">27</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/terminator-genisys-2015-new-hdts-xvid-ac3-readnfo-mrg-t10907814.html" class="cellMainLink">Terminator Genisys {2015} NEW HDTS XVID AC3 READNFO-MRG</a></div>
			</td>
			<td class="nobr center">1.77 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">2207</td>
			<td class="red lasttd center">3404</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10902188,0" class="icomment icommentjs icon16" href="/i-lived-2015-hdrip-xvid-ac3-evo-t10902188.html#comment"> <em class="iconvalue">39</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/i-lived-2015-hdrip-xvid-ac3-evo-t10902188.html" class="cellMainLink">I-Lived 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center">1.45 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">2828</td>
			<td class="red lasttd center">2017</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="rsssign" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910461,0" class="icomment icommentjs icon16" href="/suits-s05e03-hdtv-x264-asap-rarbg-t10910461.html#comment"> <em class="iconvalue">71</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/suits-s05e03-hdtv-x264-asap-rarbg-t10910461.html" class="cellMainLink">Suits S05E03 HDTV x264-ASAP[rarbg]</a></div>
			</td>
			<td class="nobr center">258.75 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">11002</td>
			<td class="red lasttd center">3921</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10910673,0" class="icomment icommentjs icon16" href="/mr-robot-s01e03-720p-hdtv-x264-immerse-rartv-t10910673.html#comment"> <em class="iconvalue">180</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mr-robot-s01e03-720p-hdtv-x264-immerse-rartv-t10910673.html" class="cellMainLink">Mr Robot S01E03 720p HDTV x264-IMMERSE[rartv]</a></div>
			</td>
			<td class="nobr center">658.77 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">11174</td>
			<td class="red lasttd center">1933</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10913799,0" class="icomment icommentjs icon16" href="/wayward-pines-s01e08-hdtv-x264-lol-ettv-t10913799.html#comment"> <em class="iconvalue">82</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wayward-pines-s01e08-hdtv-x264-lol-ettv-t10913799.html" class="cellMainLink">Wayward Pines S01E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center">242.23 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">9967</td>
			<td class="red lasttd center">2102</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10915446,0" class="icomment icommentjs icon16" href="/under-the-dome-s03e04-hdtv-x264-lol-rartv-t10915446.html#comment"> <em class="iconvalue">56</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/under-the-dome-s03e04-hdtv-x264-lol-rartv-t10915446.html" class="cellMainLink">Under the Dome S03E04 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center">293.97 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">23&nbsp;hours</td>
			<td class="green center">6586</td>
			<td class="red lasttd center">1468</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10915984,0" class="icomment icommentjs icon16" href="/hannibal-s03e06-720p-hdtv-x264-dimension-rartv-t10915984.html#comment"> <em class="iconvalue">32</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hannibal-s03e06-720p-hdtv-x264-dimension-rartv-t10915984.html" class="cellMainLink">Hannibal S03E06 720p HDTV X264-DIMENSION[rartv]</a></div>
			</td>
			<td class="nobr center">765.51 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">4797</td>
			<td class="red lasttd center">1488</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10915998,0" class="icomment icommentjs icon16" href="/dominion-s02e01-hdtv-x264-asap-rartv-t10915998.html#comment"> <em class="iconvalue">54</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dominion-s02e01-hdtv-x264-asap-rartv-t10915998.html" class="cellMainLink">Dominion S02E01 HDTV x264-ASAP[rartv]</a></div>
			</td>
			<td class="nobr center">408.55 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">4666</td>
			<td class="red lasttd center">1347</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10900557,0" class="icomment icommentjs icon16" href="/devious-maids-s03e06-hdtv-x264-killers-rartv-t10900557.html#comment"> <em class="iconvalue">20</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/devious-maids-s03e06-hdtv-x264-killers-rartv-t10900557.html" class="cellMainLink">Devious Maids S03E06 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">308.24 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">3879</td>
			<td class="red lasttd center">1956</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10900747,0" class="icomment icommentjs icon16" href="/teen-wolf-s05e03-hdtv-x264-killers-rartv-t10900747.html#comment"> <em class="iconvalue">69</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/teen-wolf-s05e03-hdtv-x264-killers-rartv-t10900747.html" class="cellMainLink">Teen Wolf S05E03 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">270.01 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">3903</td>
			<td class="red lasttd center">1184</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10905835,0" class="icomment icommentjs icon16" href="/zoo-s01e02-hdtv-x264-lol-rartv-t10905835.html#comment"> <em class="iconvalue">76</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/zoo-s01e02-hdtv-x264-lol-rartv-t10905835.html" class="cellMainLink">Zoo S01E02 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center">349.35 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">3966</td>
			<td class="red lasttd center">576</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10911290,0" class="icomment icommentjs icon16" href="/wwe-smackdown-2015-07-09-hdtv-x264-jkkk-sparrow-t10911290.html#comment"> <em class="iconvalue">25</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-smackdown-2015-07-09-hdtv-x264-jkkk-sparrow-t10911290.html" class="cellMainLink">WWE Smackdown 2015 07 09 HDTV x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center">1016.28 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">2114</td>
			<td class="red lasttd center">1013</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10915795,0" class="icomment icommentjs icon16" href="/mistresses-us-s03e05-hdtv-x264-killers-rartv-t10915795.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mistresses-us-s03e05-hdtv-x264-killers-rartv-t10915795.html" class="cellMainLink">Mistresses US S03E05 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">288.51 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">1970</td>
			<td class="red lasttd center">756</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10905993,0" class="icomment icommentjs icon16" href="/tyrant-s02e04-720p-hdtv-x264-killers-rartv-t10905993.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tyrant-s02e04-720p-hdtv-x264-killers-rartv-t10905993.html" class="cellMainLink">Tyrant S02E04 720p HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">930.26 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">1533</td>
			<td class="red lasttd center">1029</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10915985,0" class="icomment icommentjs icon16" href="/graceland-s03e03-hdtv-x264-2hd-rartv-t10915985.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/graceland-s03e03-hdtv-x264-2hd-rartv-t10915985.html" class="cellMainLink">Graceland S03E03 HDTV x264-2HD[rartv]</a></div>
			</td>
			<td class="nobr center">355.3 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">1914</td>
			<td class="red lasttd center">534</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10900419,0" class="icomment icommentjs icon16" href="/the-fosters-2013-s03e05-hdtv-x264-killers-rartv-t10900419.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-fosters-2013-s03e05-hdtv-x264-killers-rartv-t10900419.html" class="cellMainLink">The Fosters 2013 S03E05 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">304.2 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">1713</td>
			<td class="red lasttd center">485</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910202,0" class="icomment icommentjs icon16" href="/extant-s02e02-720p-hdtv-x264-dimension-rartv-t10910202.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/extant-s02e02-720p-hdtv-x264-dimension-rartv-t10910202.html" class="cellMainLink">Extant S02E02 720p HDTV X264-DIMENSION[rartv]</a></div>
			</td>
			<td class="nobr center">987.55 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">1321</td>
			<td class="red lasttd center">567</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="rsssign" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914974,0" class="icomment icommentjs icon16" href="/mp3-new-releases-2015-week-27-glodls-t10914974.html#comment"> <em class="iconvalue">11</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-27-glodls-t10914974.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 27 [GloDLS]</a></div>
			</td>
			<td class="nobr center">4.03 <span>GB</span></td>
			<td class="center">607</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">622</td>
			<td class="red lasttd center">1118</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10917161,0" class="icomment icommentjs icon16" href="/tyrese-black-rose-2015-l-audio-l-albumtrack-l-320kbps-l-cbr-l-mp3-l-sn3h1t87-t10917161.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tyrese-black-rose-2015-l-audio-l-albumtrack-l-320kbps-l-cbr-l-mp3-l-sn3h1t87-t10917161.html" class="cellMainLink">Tyrese - Black Rose (2015) l Audio l AlbumTrack l 320Kbps l CBR l Mp3 l sn3h1t87</a></div>
			</td>
			<td class="nobr center">142.49 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center">14&nbsp;hours</td>
			<td class="green center">512</td>
			<td class="red lasttd center">193</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10902495,0" class="icomment icommentjs icon16" href="/va-summer-dance-party-2015-mp3-320-kbps-t10902495.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-summer-dance-party-2015-mp3-320-kbps-t10902495.html" class="cellMainLink">VA - Summer Dance Party (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">990.97 <span>MB</span></td>
			<td class="center">101</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">428</td>
			<td class="red lasttd center">194</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10901022,0" class="icomment icommentjs icon16" href="/los-40-principales-del-04-al-10-de-julio-2015-mp3-divxtotal-t10901022.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/los-40-principales-del-04-al-10-de-julio-2015-mp3-divxtotal-t10901022.html" class="cellMainLink">Los 40 Principales del 04 al 10 de Julio 2015 MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">340.46 <span>MB</span></td>
			<td class="center">41</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">537</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10918108,0" class="icomment icommentjs icon16" href="/billboard-hot-100-singles-chart-18th-july-2015-cbr-320-kbps-aryan-l33t-t10918108.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-hot-100-singles-chart-18th-july-2015-cbr-320-kbps-aryan-l33t-t10918108.html" class="cellMainLink">Billboard Hot 100 Singles Chart (18th July 2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center">838.11 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center">9&nbsp;hours</td>
			<td class="green center">4</td>
			<td class="red lasttd center">580</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908541,0" class="icomment icommentjs icon16" href="/va-100-magic-tracks-chillout-2015-mp3-320-kbps-t10908541.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-100-magic-tracks-chillout-2015-mp3-320-kbps-t10908541.html" class="cellMainLink">VA - 100 Magic Tracks Chillout (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">1.06 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">312</td>
			<td class="red lasttd center">212</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10901019,0" class="icomment icommentjs icon16" href="/cafÃ©-del-mar-volÃºmen-21-2015-mp3-divxtotal-t10901019.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/cafÃ©-del-mar-volÃºmen-21-2015-mp3-divxtotal-t10901019.html" class="cellMainLink">CafÃ© del mar - VolÃºmen 21 (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">634.32 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">368</td>
			<td class="red lasttd center">110</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10906591,0" class="icomment icommentjs icon16" href="/paul-mccartney-discography-1967-2014-bbm-t10906591.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/paul-mccartney-discography-1967-2014-bbm-t10906591.html" class="cellMainLink">Paul McCartney - Discography (1967 - 2014) BBM</a></div>
			</td>
			<td class="nobr center">14.12 <span>GB</span></td>
			<td class="center">2294</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">74</td>
			<td class="red lasttd center">312</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10910528,0" class="icomment icommentjs icon16" href="/va-urban-dance-13-2015-mp3-divxtotal-t10910528.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-urban-dance-13-2015-mp3-divxtotal-t10910528.html" class="cellMainLink">VA - Urban Dance 13 (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">509.89 <span>MB</span></td>
			<td class="center">67</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">291</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10916185,0" class="icomment icommentjs icon16" href="/va-chillout-club-del-mar-beach-lounge-luxury-life-2015-mp3-divxtotal-t10916185.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-chillout-club-del-mar-beach-lounge-luxury-life-2015-mp3-divxtotal-t10916185.html" class="cellMainLink">VA - Chillout Club Del Mar Beach Lounge Luxury Life (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">265.52 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center">19&nbsp;hours</td>
			<td class="green center">211</td>
			<td class="red lasttd center">98</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10902224,0" class="icomment icommentjs icon16" href="/bar-2015-week-26-h4ckus-t10902224.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bar-2015-week-26-h4ckus-t10902224.html" class="cellMainLink">BAR 2015 Week 26 [H4CKUS]</a></div>
			</td>
			<td class="nobr center">4.18 <span>GB</span></td>
			<td class="center">536</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">165</td>
			<td class="red lasttd center">141</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908474,0" class="icomment icommentjs icon16" href="/va-drum-bass-summer-slammers-2015-mp3-320-kbps-t10908474.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-drum-bass-summer-slammers-2015-mp3-320-kbps-t10908474.html" class="cellMainLink">VA - Drum &amp; Bass Summer Slammers (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">457.35 <span>MB</span></td>
			<td class="center">33</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">189</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10908620,0" class="icomment icommentjs icon16" href="/va-mega-dance-top-50-spring-2015-2cd-2015-gnvr-t10908620.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-mega-dance-top-50-spring-2015-2cd-2015-gnvr-t10908620.html" class="cellMainLink">VA Mega Dance Top 50 Spring 2015 2CD 2015 gnvr</a></div>
			</td>
			<td class="nobr center">285.49 <span>MB</span></td>
			<td class="center">55</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">173</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10901032,0" class="icomment icommentjs icon16" href="/va-paradise-lounge-vol-6-60-fantastic-summer-tunes-2015-mp3-divxtotal-t10901032.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-paradise-lounge-vol-6-60-fantastic-summer-tunes-2015-mp3-divxtotal-t10901032.html" class="cellMainLink">VA - Paradise Lounge Vol 6 60 Fantastic Summer Tunes (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">724.99 <span>MB</span></td>
			<td class="center">60</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">110</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10919027,0" class="icomment icommentjs icon16" href="/owl-city-mobile-orchestra-japanese-version-2015-320-kbps-aryan-l33t-t10919027.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/owl-city-mobile-orchestra-japanese-version-2015-320-kbps-aryan-l33t-t10919027.html" class="cellMainLink">Owl City â Mobile Orchestra (Japanese Version) (2015) 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center">112.7 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center">6&nbsp;hours</td>
			<td class="green center">3</td>
			<td class="red lasttd center">60</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="rsssign" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10904037,0" class="icomment icommentjs icon16" href="/ark-survival-evolved-v1-84-0-x64-crack-lumaemu-kortal-t10904037.html#comment"> <em class="iconvalue">27</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ark-survival-evolved-v1-84-0-x64-crack-lumaemu-kortal-t10904037.html" class="cellMainLink">ARK Survival Evolved v1.84.0 x64 crack LumaEmu #Kortal</a></div>
			</td>
			<td class="nobr center">6.61 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">211</td>
			<td class="red lasttd center">557</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10915551,0" class="icomment icommentjs icon16" href="/f1-2015-3dm-not-yet-cracked-t10915551.html#comment"> <em class="iconvalue">46</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/f1-2015-3dm-not-yet-cracked-t10915551.html" class="cellMainLink">F1 2015-3DM [NOT YET CRACKED]</a></div>
			</td>
			<td class="nobr center">10.16 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">22&nbsp;hours</td>
			<td class="green center">116</td>
			<td class="red lasttd center">611</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10917416,0" class="icomment icommentjs icon16" href="/pc-f1-2015-full-unlocked-rldgames-t10917416.html#comment"> <em class="iconvalue">34</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pc-f1-2015-full-unlocked-rldgames-t10917416.html" class="cellMainLink">[PC] F1.2015.FULL.UNLOCKED-RLDGAMES</a></div>
			</td>
			<td class="nobr center">9.87 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">89</td>
			<td class="red lasttd center">550</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917246,0" class="icomment icommentjs icon16" href="/mortal-kombat-x-premium-edition-update-10-2015-pc-repack-Ð¾Ñ-nemos-t10917246.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/mortal-kombat-x-premium-edition-update-10-2015-pc-repack-Ð¾Ñ-nemos-t10917246.html" class="cellMainLink">Mortal Kombat X: Premium Edition [Update 10] (2015) PC | RePack Ð¾Ñ =nemos=</a></div>
			</td>
			<td class="nobr center">25.69 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">121</td>
			<td class="red lasttd center">467</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10912927,0" class="icomment icommentjs icon16" href="/final-fantasy-xiii-update-3-2014-pc-repack-Ð¾Ñ-r-g-steamgames-t10912927.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/final-fantasy-xiii-update-3-2014-pc-repack-Ð¾Ñ-r-g-steamgames-t10912927.html" class="cellMainLink">Final Fantasy XIII [Update 3] (2014) PC | RePack Ð¾Ñ R.G. Steamgames</a></div>
			</td>
			<td class="nobr center">44.43 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">158</td>
			<td class="red lasttd center">409</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10901638,0" class="icomment icommentjs icon16" href="/rocket-league-flt-t10901638.html#comment"> <em class="iconvalue">52</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/rocket-league-flt-t10901638.html" class="cellMainLink">Rocket_League-FLT</a></div>
			</td>
			<td class="nobr center">1.72 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">354</td>
			<td class="red lasttd center">168</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910447,0" class="icomment icommentjs icon16" href="/assassin-s-creed-director-s-cut-edition-2008-gog-t10910447.html#comment"> <em class="iconvalue">52</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assassin-s-creed-director-s-cut-edition-2008-gog-t10910447.html" class="cellMainLink">Assassin&#039;s Creed: Director&#039;s Cut Edition (2008) (GOG)</a></div>
			</td>
			<td class="nobr center">6.96 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">99</td>
			<td class="red lasttd center">232</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10903113,0" class="icomment icommentjs icon16" href="/pillars-of-eternity-royal-edition-v1-0-6-0617-gog-t10903113.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/pillars-of-eternity-royal-edition-v1-0-6-0617-gog-t10903113.html" class="cellMainLink">Pillars of Eternity: Royal Edition (v1.0.6.0617) (GOG)</a></div>
			</td>
			<td class="nobr center">8.41 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">106</td>
			<td class="red lasttd center">220</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10906020,0" class="icomment icommentjs icon16" href="/battle-fantasia-reloaded-t10906020.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/battle-fantasia-reloaded-t10906020.html" class="cellMainLink">Battle.Fantasia-RELOADED</a></div>
			</td>
			<td class="nobr center">2.25 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">162</td>
			<td class="red lasttd center">136</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914682,0" class="icomment icommentjs icon16" href="/the-red-solstice-reloaded-t10914682.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-red-solstice-reloaded-t10914682.html" class="cellMainLink">The.Red.Solstice-RELOADED</a></div>
			</td>
			<td class="nobr center">1 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">176</td>
			<td class="red lasttd center">116</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10903230,0" class="icomment icommentjs icon16" href="/stronghold-3-gold-multi8-prophet-t10903230.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/stronghold-3-gold-multi8-prophet-t10903230.html" class="cellMainLink">Stronghold 3 Gold MULTi8-PROPHET</a></div>
			</td>
			<td class="nobr center">4.56 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">152</td>
			<td class="red lasttd center">135</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10901894,0" class="icomment icommentjs icon16" href="/legends-of-eisenwald-update-3-rus-eng-multi-repack-by-xatab-t10901894.html#comment"> <em class="iconvalue">20</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/legends-of-eisenwald-update-3-rus-eng-multi-repack-by-xatab-t10901894.html" class="cellMainLink">Legends of Eisenwald [Update 3] [RUS | ENG | MULTi] RePack by xatab</a></div>
			</td>
			<td class="nobr center">711.72 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">202</td>
			<td class="red lasttd center">84</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10903033,0" class="icomment icommentjs icon16" href="/the-amber-throne-skidrow-t10903033.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-amber-throne-skidrow-t10903033.html" class="cellMainLink">The.Amber.Throne-SKIDROW</a></div>
			</td>
			<td class="nobr center">1.01 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">103</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10901323,0" class="icomment icommentjs icon16" href="/kholat-update-1-rus-eng-multi9-repack-by-xatab-t10901323.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/kholat-update-1-rus-eng-multi9-repack-by-xatab-t10901323.html" class="cellMainLink">Kholat [Update 1] [RUS | ENG | MULTi9] RePack by xatab</a></div>
			</td>
			<td class="nobr center">2.42 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">91</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10916806,0" class="icomment icommentjs icon16" href="/the-amazing-spider-man-2-bundle-plaza-t10916806.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-amazing-spider-man-2-bundle-plaza-t10916806.html" class="cellMainLink">The Amazing Spider Man 2 Bundle-PLAZA</a></div>
			</td>
			<td class="nobr center">7.7 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">16&nbsp;hours</td>
			<td class="green center">7</td>
			<td class="red lasttd center">78</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="rsssign" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10906060,0" class="icomment icommentjs icon16" href="/microsoft-office-2016-professional-plus-16-0-4229-1002-preview-32-64-bit-ratiborus-2-8-activator-appzdam-t10906060.html#comment"> <em class="iconvalue">77</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-office-2016-professional-plus-16-0-4229-1002-preview-32-64-bit-ratiborus-2-8-activator-appzdam-t10906060.html" class="cellMainLink">Microsoft Office 2016 Professional Plus 16.0.4229.1002 Preview [32-64 bit] (Ratiborus 2.8) + Activator - AppzDam</a></div>
			</td>
			<td class="nobr center">2.55 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">714</td>
			<td class="red lasttd center">802</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908502,0" class="icomment icommentjs icon16" href="/internet-download-manager-idm-6-23-build-14-registered-32bit-64bit-patch-crackingpatching-t10908502.html#comment"> <em class="iconvalue">95</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-23-build-14-registered-32bit-64bit-patch-crackingpatching-t10908502.html" class="cellMainLink">Internet Download Manager (IDM) 6.23 Build 14 Registered (32bit + 64bit Patch) [CrackingPatching]</a></div>
			</td>
			<td class="nobr center">9.66 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">982</td>
			<td class="red lasttd center">462</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914163,0" class="icomment icommentjs icon16" href="/windows-10-pro-insider-preview-build-10166-en-us-x64-by-whitedeath-t10914163.html#comment"> <em class="iconvalue">43</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-insider-preview-build-10166-en-us-x64-by-whitedeath-t10914163.html" class="cellMainLink">Windows 10 Pro Insider Preview Build 10166 En-us X64 By:WhiteDeath</a></div>
			</td>
			<td class="nobr center">3.73 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">312</td>
			<td class="red lasttd center">374</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10907310,0" class="icomment icommentjs icon16" href="/utorrent-pro-3-4-3-build-40633-stable-crack-s0ft4pc-t10907310.html#comment"> <em class="iconvalue">74</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/utorrent-pro-3-4-3-build-40633-stable-crack-s0ft4pc-t10907310.html" class="cellMainLink">uTorrent PRO 3.4.3 Build 40633 Stable + Crack [S0ft4PC]</a></div>
			</td>
			<td class="nobr center">4.52 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">376</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10913801,0" class="icomment icommentjs icon16" href="/amd-catalyst-display-drivers-15-7-whql-t10913801.html#comment"> <em class="iconvalue">31</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/amd-catalyst-display-drivers-15-7-whql-t10913801.html" class="cellMainLink">AMD Catalyst Display Drivers 15.7 WHQL</a></div>
			</td>
			<td class="nobr center">1.37 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">105</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-photoshop-elements-13-1-multilingual-keygen-patch-32-64-bit-appzdam-t10912089.html" class="cellMainLink">Adobe Photoshop Elements 13.1 Multilingual + Keygen/Patch [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center">3.25 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">111</td>
			<td class="red lasttd center">107</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10903551,0" class="icomment icommentjs icon16" href="/chief-architect-premier-x7-17-3-0-25-x64-patch-t10903551.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/chief-architect-premier-x7-17-3-0-25-x64-patch-t10903551.html" class="cellMainLink">Chief Architect Premier X7 17.3.0.25 (x64) + Patch</a></div>
			</td>
			<td class="nobr center">189.65 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">181</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10907172,0" class="icomment icommentjs icon16" href="/k-lite-codec-pack-11-2-0-mega-full-standard-basic-update-t10907172.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/k-lite-codec-pack-11-2-0-mega-full-standard-basic-update-t10907172.html" class="cellMainLink">K-Lite Codec Pack 11.2.0 Mega_Full_Standard_Basic + Update</a></div>
			</td>
			<td class="nobr center">150.82 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">149</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10904561,0" class="icomment icommentjs icon16" href="/hdr-darkroom-pro-3-1-1-2-x32-x64-eng-reg-key-t10904561.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/hdr-darkroom-pro-3-1-1-2-x32-x64-eng-reg-key-t10904561.html" class="cellMainLink">HDR Darkroom Pro 3.1.1.2 (x32/x64)[ENG][Reg Key]</a></div>
			</td>
			<td class="nobr center">52.18 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">113</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10907222,0" class="icomment icommentjs icon16" href="/atomix-virtualdj-pro-infinity-v8-0-2345-all-skins-samples-soundeffects-incl-crack-m4master-teamos-hkrg-t10907222.html#comment"> <em class="iconvalue">26</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/atomix-virtualdj-pro-infinity-v8-0-2345-all-skins-samples-soundeffects-incl-crack-m4master-teamos-hkrg-t10907222.html" class="cellMainLink">Atomix VirtualDJ Pro Infinity v8.0.2345 [All Skins, Samples &amp; SoundEffects] Incl. Crack [M4Master][TeamOS-HKRG]</a></div>
			</td>
			<td class="nobr center">120.03 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">119</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/any-video-converter-ultimate-5-8-2-multilingual-serial-key-dtw-t10916298.html" class="cellMainLink">Any Video Converter Ultimate 5.8.2 Multilingual +Serial Key [DTW]</a></div>
			</td>
			<td class="nobr center">36.59 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center">18&nbsp;hours</td>
			<td class="green center">88</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10902491,0" class="icomment icommentjs icon16" href="/tomtom-north-america-usa-canada-mexico-v1-4-map-v950-6558-apk-t10902491.html#comment"> <em class="iconvalue">30</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/tomtom-north-america-usa-canada-mexico-v1-4-map-v950-6558-apk-t10902491.html" class="cellMainLink">TomTom North America (USA &amp; Canada &amp; Mexico) v1.4 (Map v950.6558) APK</a></div>
			</td>
			<td class="nobr center">3.12 <span>GB</span></td>
			<td class="center">705</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">50</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ios-9-beta-3-iphone-5s-model-a1453-a1533-13a4293f-zip-t10910181.html" class="cellMainLink">iOS 9 beta 3 iPhone 5s Model A1453 A1533 13A4293f.zip</a></div>
			</td>
			<td class="nobr center">1.89 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">21</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10901152,0" class="icomment icommentjs icon16" href="/microsoft-windows-10-enterprise-insider-preview-64bit-10162-pipcad-lopatkin-eng-ru-appzdam-t10901152.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-windows-10-enterprise-insider-preview-64bit-10162-pipcad-lopatkin-eng-ru-appzdam-t10901152.html" class="cellMainLink">Microsoft Windows 10 Enterprise Insider Preview [64bit] 10162 PIPCAD (Lopatkin) [Eng-Ru] - AppzDam</a></div>
			</td>
			<td class="nobr center">1.67 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">26</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917361,0" class="icomment icommentjs icon16" href="/windows-10-build-10166-x86-aio-en-us-by-whitedeath-teamos-t10917361.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-build-10166-x86-aio-en-us-by-whitedeath-teamos-t10917361.html" class="cellMainLink">Windows 10 Build 10166 x86 AIO En-us By:WhiteDeath[TeamOS]</a></div>
			</td>
			<td class="nobr center">2.91 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">18</td>
			<td class="red lasttd center">27</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="rsssign" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914360,0" class="icomment icommentjs icon16" href="/fansub-resistance-naruto-shippuuden-419-1280x720-mp4-t10914360.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fansub-resistance-naruto-shippuuden-419-1280x720-mp4-t10914360.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 419 (1280x720).mp4</a></div>
			</td>
			<td class="nobr center">201.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">1876</td>
			<td class="red lasttd center">499</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-classroom-crisis-02-720p-mkv-t10919710.html" class="cellMainLink">[HorribleSubs] Classroom Crisis - 02 [720p].mkv</a></div>
			</td>
			<td class="nobr center">333.36 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;hours</td>
			<td class="green center">1078</td>
			<td class="red lasttd center">982</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10909749,0" class="icomment icommentjs icon16" href="/leopard-raws-gangsta-02-raw-abc-1280x720-x264-aac-mp4-t10909749.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-gangsta-02-raw-abc-1280x720-x264-aac-mp4-t10909749.html" class="cellMainLink">[Leopard-Raws] Gangsta. - 02 RAW (ABC 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">293.44 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">682</td>
			<td class="red lasttd center">184</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10912831,0" class="icomment icommentjs icon16" href="/naruto-shippuden-episode-419-english-subbed-480p-arizone-t10912831.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-episode-419-english-subbed-480p-arizone-t10912831.html" class="cellMainLink">Naruto Shippuden Episode 419 [English Subbed] 480p ~ARIZONE</a></div>
			</td>
			<td class="nobr center">53.34 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">375</td>
			<td class="red lasttd center">140</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10912738,0" class="icomment icommentjs icon16" href="/youshikibi-naruto-shippuden-419-1280x720-h264-aac-d49cb829-mp4-t10912738.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/youshikibi-naruto-shippuden-419-1280x720-h264-aac-d49cb829-mp4-t10912738.html" class="cellMainLink">[youshikibi] Naruto Shippuden - 419 (1280x720 h264 AAC) [d49cb829].mp4</a></div>
			</td>
			<td class="nobr center">256.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">178</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10913340,0" class="icomment icommentjs icon16" href="/animerg-noragami-season-1-complete-eng-dub-720p-scavvykid-t10913340.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-noragami-season-1-complete-eng-dub-720p-scavvykid-t10913340.html" class="cellMainLink">[AnimeRG] Noragami Season 1 complete [Eng Dub] [720p] [ScavvyKiD]</a></div>
			</td>
			<td class="nobr center">5.42 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">69</td>
			<td class="red lasttd center">138</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-ushio-to-tora-tv-02-720p-xcelent-mkv-t10918950.html" class="cellMainLink">[AnimeRG] Ushio to Tora (TV) - 02 [720p] [Xcelent].mkv</a></div>
			</td>
			<td class="nobr center">322.84 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">6&nbsp;hours</td>
			<td class="green center">57</td>
			<td class="red lasttd center">126</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-prison-school-01-720p-aac-mp4-t10919268.html" class="cellMainLink">[BakedFish] Prison School - 01 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center">262.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">5&nbsp;hours</td>
			<td class="green center">41</td>
			<td class="red lasttd center">119</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10912749,0" class="icomment icommentjs icon16" href="/deadfish-naruto-shippuuden-419-720p-aac-mp4-t10912749.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-naruto-shippuuden-419-720p-aac-mp4-t10912749.html" class="cellMainLink">[DeadFish] Naruto Shippuuden - 419 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center">273.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">60</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deanzel-princess-mononoke-bd-1080p-hi10p-dual-audio-flac-fd82871b-t10913711.html" class="cellMainLink">[deanzel] Princess Mononoke [BD 1080p Hi10p Dual Audio FLAC][fd82871b]</a></div>
			</td>
			<td class="nobr center">21.58 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">17</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914120,0" class="icomment icommentjs icon16" href="/deanzel-spirited-away-bd-1080p-hi10p-dual-audio-flac-72044eca-t10914120.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deanzel-spirited-away-bd-1080p-hi10p-dual-audio-flac-72044eca-t10914120.html" class="cellMainLink">[deanzel] Spirited Away [BD 1080p Hi10p Dual Audio FLAC][72044eca]</a></div>
			</td>
			<td class="nobr center">14.7 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">23</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10906239,0" class="icomment icommentjs icon16" href="/animerg-cardcaptor-sakura-complete-480p-ep-1-70-scavvykid-t10906239.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-cardcaptor-sakura-complete-480p-ep-1-70-scavvykid-t10906239.html" class="cellMainLink">[AnimeRG] Cardcaptor Sakura Complete [480p] [EP 1 - 70] [ScavvyKiD]</a></div>
			</td>
			<td class="nobr center">9.97 <span>GB</span></td>
			<td class="center">72</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">11</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-ace-of-the-diamond-second-season-14-dff899e4-mkv-t10900829.html" class="cellMainLink">[Commie] Ace of the Diamond ~Second Season~ - 14 [DFF899E4].mkv</a></div>
			</td>
			<td class="nobr center">296.3 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">6</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/porco-rosso-kurenai-no-buta-1992-1080p-esp-cat-vas-lat-ita-eng-jap-spanish-espaÃ±ol-english-italian-t10914410.html" class="cellMainLink">Porco Rosso - Kurenai no Buta (1992) [1080p ESP CAT VAS LAT ITA ENG JAP] spanish espaÃ±ol english italian</a></div>
			</td>
			<td class="nobr center">17.13 <span>GB</span></td>
			<td class="center">201</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">1</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-gakkou-gurashi-01-41249c22-mkv-t10918131.html" class="cellMainLink">[Vivid] Gakkou Gurashi! - 01 [41249C22].mkv</a></div>
			</td>
			<td class="nobr center">386.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">9&nbsp;hours</td>
			<td class="green center">1</td>
			<td class="red lasttd center">44</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="rsssign" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10909372,0" class="icomment icommentjs icon16" href="/marvel-week-07-08-2015-nem-t10909372.html#comment"> <em class="iconvalue">39</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-07-08-2015-nem-t10909372.html" class="cellMainLink">Marvel Week+ (07-08-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center">647.48 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">849</td>
			<td class="red lasttd center">372</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914826,0" class="icomment icommentjs icon16" href="/dc-week-07-08-2015-vertigo-aka-dc-you-week-06-nem-t10914826.html#comment"> <em class="iconvalue">26</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-07-08-2015-vertigo-aka-dc-you-week-06-nem-t10914826.html" class="cellMainLink">DC Week+ (07-08-2015) (+ Vertigo) (aka DC YOU Week 06) (- Nem -)</a></div>
			</td>
			<td class="nobr center">474.11 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">619</td>
			<td class="red lasttd center">449</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10910288,0" class="icomment icommentjs icon16" href="/assorted-magazines-bundle-july-9-2015-true-pdf-t10910288.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-july-9-2015-true-pdf-t10910288.html" class="cellMainLink">Assorted Magazines Bundle - July 9 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">557.8 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">311</td>
			<td class="red lasttd center">284</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914188,0" class="icomment icommentjs icon16" href="/the-complete-photographer-a-masterclass-in-every-genre-dk-publishing-2010-pdf-gooner-t10914188.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-complete-photographer-a-masterclass-in-every-genre-dk-publishing-2010-pdf-gooner-t10914188.html" class="cellMainLink">The Complete Photographer - A Masterclass In Every Genre (DK Publishing) (2010).pdf Gooner</a></div>
			</td>
			<td class="nobr center">105.09 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">382</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914423,0" class="icomment icommentjs icon16" href="/the-sewing-book-an-encyclopedic-resource-of-step-by-step-techniques-dk-publishing-2009-pdf-gooner-t10914423.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-sewing-book-an-encyclopedic-resource-of-step-by-step-techniques-dk-publishing-2009-pdf-gooner-t10914423.html" class="cellMainLink">The Sewing Book - An Encyclopedic Resource of Step-by-Step Techniques (DK Publishing) (2009).pdf Gooner</a></div>
			</td>
			<td class="nobr center">84.68 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">353</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910796,0" class="icomment icommentjs icon16" href="/home-garden-magazines-july-9-2015-true-pdf-t10910796.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-garden-magazines-july-9-2015-true-pdf-t10910796.html" class="cellMainLink">Home &amp; Garden Magazines - July 9 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">383.15 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">268</td>
			<td class="red lasttd center">111</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917299,0" class="icomment icommentjs icon16" href="/atlas-of-ancient-worlds-people-and-places-from-the-past-dk-publishing-2009-pdf-gooner-t10917299.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/atlas-of-ancient-worlds-people-and-places-from-the-past-dk-publishing-2009-pdf-gooner-t10917299.html" class="cellMainLink">Atlas of Ancient Worlds - People and Places From the Past (DK Publishing) (2009).pdf Gooner</a></div>
			</td>
			<td class="nobr center">57.53 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">276</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914361,0" class="icomment icommentjs icon16" href="/fashion-photography-101-a-complete-course-for-the-new-fashion-photographers-2012-pdf-gooner-t10914361.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/fashion-photography-101-a-complete-course-for-the-new-fashion-photographers-2012-pdf-gooner-t10914361.html" class="cellMainLink">Fashion Photography 101 - A Complete Course for the New Fashion Photographers (2012).pdf Gooner</a></div>
			</td>
			<td class="nobr center">121.16 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">245</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10910437,0" class="icomment icommentjs icon16" href="/automobile-magazines-bundle-july-9-2015-true-pdf-t10910437.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-bundle-july-9-2015-true-pdf-t10910437.html" class="cellMainLink">Automobile Magazines Bundle - July 9 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">277.02 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">156</td>
			<td class="red lasttd center">97</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10909368,0" class="icomment icommentjs icon16" href="/squadron-sinister-002-2015-digital-tlk-empire-hd-cbr-t10909368.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/squadron-sinister-002-2015-digital-tlk-empire-hd-cbr-t10909368.html" class="cellMainLink">Squadron Sinister 002(2015)(Digital)(TLK-EMPIRE-HD).cbr</a></div>
			</td>
			<td class="nobr center">37.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">178</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10913764,0" class="icomment icommentjs icon16" href="/god-is-dead-001-038-extras-2013-ongoing-digital-empire-nem-t10913764.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/god-is-dead-001-038-extras-2013-ongoing-digital-empire-nem-t10913764.html" class="cellMainLink">God is Dead (001-038+Extras)(2013-Ongoing)(Digital) (Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center">1.86 <span>GB</span></td>
			<td class="center">41</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">119</td>
			<td class="red lasttd center">97</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908443,0" class="icomment icommentjs icon16" href="/anne-mccaffrey-dragonriders-of-pern-1-12-t10908443.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/anne-mccaffrey-dragonriders-of-pern-1-12-t10908443.html" class="cellMainLink">Anne McCaffrey - Dragonriders of Pern [1-12]</a></div>
			</td>
			<td class="nobr center">4.58 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">98</td>
			<td class="red lasttd center">113</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10901350,0" class="icomment icommentjs icon16" href="/dragon-ball-z-v01-v26-newrips-2003-2006-digital-anherogold-empire-nem-t10901350.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/dragon-ball-z-v01-v26-newrips-2003-2006-digital-anherogold-empire-nem-t10901350.html" class="cellMainLink">Dragon Ball Z (v01-v26) (NewRips) (2003-2006) (Digital) (AnHeroGold-Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center">4.83 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">98</td>
			<td class="red lasttd center">106</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10906600,0" class="icomment icommentjs icon16" href="/0-day-week-of-2015-07-01-t10906600.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-07-01-t10906600.html" class="cellMainLink">0-Day Week of 2015.07.01</a></div>
			</td>
			<td class="nobr center">7 <span>GB</span></td>
			<td class="center">157</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">68</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10918746,0" class="icomment icommentjs icon16" href="/woodwork-a-step-by-step-photographic-guide-dk-publishing-2010-pdf-gooner-t10918746.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/woodwork-a-step-by-step-photographic-guide-dk-publishing-2010-pdf-gooner-t10918746.html" class="cellMainLink">Woodwork - A Step-by-Step Photographic Guide (DK Publishing) (2010).pdf Gooner</a></div>
			</td>
			<td class="nobr center">71.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">7&nbsp;hours</td>
			<td class="green center">89</td>
			<td class="red lasttd center">48</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="rsssign" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10901277,0" class="icomment icommentjs icon16" href="/rammstein-the-best-of-rammstein-vynil-rip-24-96-2015-flac-t10901277.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rammstein-the-best-of-rammstein-vynil-rip-24-96-2015-flac-t10901277.html" class="cellMainLink">Rammstein - The Best Of Rammstein [vynil rip 24 96] (2015) FLAC</a></div>
			</td>
			<td class="nobr center">3.31 <span>GB</span></td>
			<td class="center">38</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">275</td>
			<td class="red lasttd center">98</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10901363,0" class="icomment icommentjs icon16" href="/the-doors-the-best-of-the-doors-2015-audio-fidelity-sacd-flac-beolab1700-t10901363.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-doors-the-best-of-the-doors-2015-audio-fidelity-sacd-flac-beolab1700-t10901363.html" class="cellMainLink">The Doors - The Best Of The Doors (2015) Audio Fidelity SACD FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center">374.23 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">239</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10901361,0" class="icomment icommentjs icon16" href="/joe-cocker-with-a-little-help-from-my-friends-2015-audio-fidelity-sacd-flac-beolab1700-t10901361.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/joe-cocker-with-a-little-help-from-my-friends-2015-audio-fidelity-sacd-flac-beolab1700-t10901361.html" class="cellMainLink">Joe Cocker - With A Little Help From My Friends (2015) Audio Fidelity SACD FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center">323.99 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">148</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10908559,0" class="icomment icommentjs icon16" href="/sting-and-the-police-the-very-best-of-2002-flac-soup-t10908559.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/sting-and-the-police-the-very-best-of-2002-flac-soup-t10908559.html" class="cellMainLink">Sting and The Police - The Very Best Of (2002) FLAC Soup</a></div>
			</td>
			<td class="nobr center">493.07 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">155</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10903392,0" class="icomment icommentjs icon16" href="/donovan-discography-1965-2015-flac-t10903392.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/donovan-discography-1965-2015-flac-t10903392.html" class="cellMainLink">Donovan - Discography (1965-2015) [FLAC]</a></div>
			</td>
			<td class="nobr center">9.4 <span>GB</span></td>
			<td class="center">896</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">77</td>
			<td class="red lasttd center">87</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10902415,0" class="icomment icommentjs icon16" href="/va-john-lee-hooker-ruth-brown-chicago-blues-festival-1990-sbd-fm-flac-t10902415.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-john-lee-hooker-ruth-brown-chicago-blues-festival-1990-sbd-fm-flac-t10902415.html" class="cellMainLink">VA (John Lee Hooker, Ruth Brown...) - Chicago Blues Festival 1990 (SBD FM) [FLAC]</a></div>
			</td>
			<td class="nobr center">1.2 <span>GB</span></td>
			<td class="center">68</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">110</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10907149,0" class="icomment icommentjs icon16" href="/fleetwood-mac-fleetwood-mac-2011-hdtracks-24bit-96-flac-beolab1700-t10907149.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/fleetwood-mac-fleetwood-mac-2011-hdtracks-24bit-96-flac-beolab1700-t10907149.html" class="cellMainLink">Fleetwood Mac - Fleetwood Mac - (2011) [HDTracks] 24bit 96 FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center">902.07 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">114</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/buena-vista-social-club-discography-flac-vtwin88cube-t10919406.html" class="cellMainLink">Buena Vista Social Club - Discography {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">2.29 <span>GB</span></td>
			<td class="center">142</td>
			<td class="center">4&nbsp;hours</td>
			<td class="green center">5</td>
			<td class="red lasttd center">138</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10909228,0" class="icomment icommentjs icon16" href="/stevie-ray-vaughan-and-double-trouble-couldn-t-stand-the-weather-2013-hdtracks-24bit-176-beolab1700-t10909228.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/stevie-ray-vaughan-and-double-trouble-couldn-t-stand-the-weather-2013-hdtracks-24bit-176-beolab1700-t10909228.html" class="cellMainLink">Stevie Ray Vaughan and Double Trouble - Couldn&#039;t Stand the Weather (2013)[HDtracks] 24Bit 176 Beolab1700</a></div>
			</td>
			<td class="nobr center">1.64 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">69</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917093,0" class="icomment icommentjs icon16" href="/bill-withers-live-at-carnegie-hall-2014-mfsl-sacd-24bit-flac-beolab1700-t10917093.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bill-withers-live-at-carnegie-hall-2014-mfsl-sacd-24bit-flac-beolab1700-t10917093.html" class="cellMainLink">Bill Withers - Live at Carnegie Hall (2014) MFSL SACD 24bit FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center">1.52 <span>GB</span></td>
			<td class="center">32</td>
			<td class="center">14&nbsp;hours</td>
			<td class="green center">1</td>
			<td class="red lasttd center">113</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10904066,0" class="icomment icommentjs icon16" href="/4-non-blondes-bigger-better-faster-more-flac-mp3-big-papi-1992-t10904066.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/4-non-blondes-bigger-better-faster-more-flac-mp3-big-papi-1992-t10904066.html" class="cellMainLink">4 Non Blondes - Bigger, Better, Faster, More! [FLAC+MP3](Big Papi) 1992</a></div>
			</td>
			<td class="nobr center">356.74 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">69</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10912252,0" class="icomment icommentjs icon16" href="/iron-maiden-1980-2010-discography-flac-24-bit-t10912252.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/iron-maiden-1980-2010-discography-flac-24-bit-t10912252.html" class="cellMainLink">[Iron Maiden] [1980-2010] Discography [FLAC 24-bit]</a></div>
			</td>
			<td class="nobr center">13.21 <span>GB</span></td>
			<td class="center">156</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">37</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908091,0" class="icomment icommentjs icon16" href="/the-rides-stephen-stills-can-t-get-enough-vinyl-yeraycito-master-series-t10908091.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-rides-stephen-stills-can-t-get-enough-vinyl-yeraycito-master-series-t10908091.html" class="cellMainLink">The Rides (Stephen Stills) - Can&#039;t Get Enough (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center">1.02 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">50</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10915681,0" class="icomment icommentjs icon16" href="/bon-jovi-the-bon-jovi-remasters-flac-mp3-big-papi-80-s-rock-t10915681.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bon-jovi-the-bon-jovi-remasters-flac-mp3-big-papi-80-s-rock-t10915681.html" class="cellMainLink">Bon Jovi - The Bon Jovi Remasters [FLAC+MP3](Big Papi) 80&#039;s Rock</a></div>
			</td>
			<td class="nobr center">357.96 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">1</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10901282,0" class="icomment icommentjs icon16" href="/rammstein-rammstein-vynil-rip-24-96-1995-1997-flac-t10901282.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rammstein-rammstein-vynil-rip-24-96-1995-1997-flac-t10901282.html" class="cellMainLink">Rammstein - Rammstein [vynil rip 24 96] (1995-1997) flac</a></div>
			</td>
			<td class="nobr center">2.75 <span>GB</span></td>
			<td class="center">39</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">25</td>
			<td class="red lasttd center">27</td>
        </tr>
			</table>


		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
                
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-games-will-my-system-specs-handle/?unread=16657249">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What games will my system specs handle?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/crookedferret/">crookedferret</a></span></span> 13&nbsp;sec.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/google-site-ahead-contains-harmful-programs/?unread=16657248">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Google: &quot;The site ahead contains harmful programs&quot;
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/larry.the.3rd/">larry.the.3rd</a></span></span> 16&nbsp;sec.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/what-tv-show-are-you-watching-right-now-v3/?unread=16657246">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What TV show are you watching right now? V3
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/Dr.Mokum/">Dr.Mokum</a></span></span> 2&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/release-archives-condoghost/?unread=16657245">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				The Release Archives of CondoGhost
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_4"><a class="plain" href="/user/Sr.Dr4cuLa/">Sr.Dr4cuLa</a></span></span> 2&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/music-requests-new-v2/?unread=16657243">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Music Requests - New (V2)
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/ShaiyaRaina/">ShaiyaRaina</a></span></span> 3&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=16657239">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/ganesha000/">ganesha000</a></span></span> 4&nbsp;min.&nbsp;ago</span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents 2&nbsp;months&nbsp;ago</span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents 3&nbsp;months&nbsp;ago</span>
	</li>
	<li>
		<a href="/blog/post/technical-maintenance-march-17/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Technical maintenance (March, 17)
			</p>
		</a>
		<span class="explanation">by KickassTorrents 3&nbsp;months&nbsp;ago</span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/Star.Sapphire/post/random-audio-rip-thoughts-that-became-a-blog/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Random Audio-Rip Thoughts That Became A Blog</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Star.Sapphire/">Star.Sapphire</a> 52&nbsp;min.&nbsp;ago</span></li>
	<li><a href="/blog/ZombieQueen/post/how-beer-saved-the-world/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> How Beer Saved the World</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/ZombieQueen/">ZombieQueen</a> 5&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/MisterGaga/post/the-confederate-flag/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> The Confederate Flag</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/MisterGaga/">MisterGaga</a> 5&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/miok2cup/post/male-date-rape-drug/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Male Date Rape Drug</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/miok2cup/">miok2cup</a> 6&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/F1_Dance/post/when-things-are-not-what-they-seem/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> When Things Are Not What They Seem</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/F1_Dance/">F1_Dance</a> 8&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/eclipze_angel/post/respect-power-or-money/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Respect, Power or Money..</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/eclipze_angel/">eclipze_angel</a> yesterday</span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/classified%20new/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				classified new
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/n0772/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				n0772
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/elliot%20boy/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				elliot boy
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/salem%20s01e13%20720/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				salem s01e13 720
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/above%20and%20beyond%20we%20are%20all%20we%20are%20need/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				above and beyond we are all we are need
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/projota/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				projota
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/bi%20sex/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				bi sex
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/game.of.thrones.s01.hdtv.xvid%20eng/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				game.of.thrones.s01.hdtv.xvid eng
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/annabelle%202014/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				annabelle 2014
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/drunk%20shower/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				drunk shower
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/armin%20van%20buuren%20a%20state%20of%20trance%20517/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				armin van buuren a state of trance 517
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>
</body>
</html>
