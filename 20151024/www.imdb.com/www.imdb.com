



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "894-3953360-1426989";
                var ue_id = "1VNCY0X3H6HXS55B6EH1";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        

        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1VNCY0X3H6HXS55B6EH1" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-a7cbb870.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1965580546._CB290605844_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-1180111305._CB293333875_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-520887519._CB293333839_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['c'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['160222478565'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4033692611._CB290930301_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"d33169bf5b59ff75990a15e25bbbeb21b7462b6d",
"2015-10-23T22%3A31%3A45GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 30495;
generic.days_to_midnight = 0.3529513776302338;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'c']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=160222478565;ord=160222478565?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=160222478565?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=160222478565?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                            <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/falltv/?ref_=nv_tvv_fall_1"
>Fall TV</a></li>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_2"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_3"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_4"
>Top Rated TV Shows</a></li>
                            <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_5"
>Most Popular TV Shows</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_6"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_7"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_3"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=10-23&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59126404/?ref_=nv_nw_tn_1"
> âPower Rangersâ: âMe and Earl and the Dying Girlâsâ R.J. Cyler to Play Blue Ranger
</a><br />
                        <span class="time">5 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59126036/?ref_=nv_nw_tn_2"
> Michael Mooreâs âWhere to Invade Nextâ Gets Oscar-Qualifying Release (Exclusive)
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59124858/?ref_=nv_nw_tn_3"
> Bella Thorne Joins Horror-Comedy âThe Babysitterâ (Exclusive)
</a><br />
                        <span class="time">20 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0095765/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjE2OTc3NzA3MF5BMl5BanBnXkFtZTcwMDg2NzIwNw@@._V1._SY315_CR6,0,410,315_.jpg",
            titleYears : "1988",
            rank : 56,
                    headline : "Cinema Paradiso"
    },
    nameAd : {
            clickThru : "/name/nm0005109/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BNDc5MTIwMzg1MF5BMl5BanBnXkFtZTcwMzg0MjQ5NQ@@._V1._SX270_CR15,0,250,315_.jpg",
            rank : 158,
            headline : "Mila Kunis"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYrRxcY1PraNciL641ddGyDR7R8DOeITns_3RtUCK6p-eptBYrDRmX-HDo2DpOsMIDJLfQSpoDi%0D%0ADIDVB9KyU5ky45iG8FDQWoRn6PNyHnTtbmpy8ruxzd0Tg5epYamjxcA4v8-NoCnajNoYqx3kNo2d%0D%0AWilKFQrHG6K0OyXNQ8jbtoa4OxQTlX8yz5XMhzuWyqW41ErvD4o46qYtEFrAuKQtDg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYl9Qvo_i8yBELSWE6r6RihEM8f8gLTcxk4lxZpixY3PRax1jJkQtKjRHmFZjzvMXkbkmttL5ic%0D%0AIztHU0crkC1SH33RHNhkV5MxRBXlw76-G3_E9cA5cmaL4NgNHQZxhYX2RTC4vR4iZAo93B4Haw8N%0D%0AwphnzZjK0b3JE4iv7bI8gU7tTEepZrObYt0eqKmIXoPY%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYp044IT-T6s5Aors9XXJGGndyr8cMftB4AZJqXGsMZiaIZA_aUpeTaGUSGg1gM3K8FcoNrWJTC%0D%0AHBdn5Dxn9e9MCiWoUO8TH1CoyUtpSdBqCTR_oTPekc3y7MfjkEbifwSRJW1VdvduRb-KmlgCOS9l%0D%0ADcbzjt-HsH3eCiz-6q70D4u7wctkncrvtYy_7M9-aHbwbEs27LhedsGE-brhVUVQBA%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=160222478565;ord=160222478565?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=160222478565;ord=160222478565?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2042343961?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2042343961" data-source="bylist" data-id="ls002309697" data-rid="1VNCY0X3H6HXS55B6EH1" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="A glimpse into an alternate history of North America. What life after WWII may have been like if the Nazis had won the war." alt="A glimpse into an alternate history of North America. What life after WWII may have been like if the Nazis had won the war." src="http://ia.media-imdb.com/images/M/MV5BMTU0NTQ5NjU1MV5BMl5BanBnXkFtZTgwMzAxNjY5NjE@._V1_SY298_CR11,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0NTQ5NjU1MV5BMl5BanBnXkFtZTgwMzAxNjY5NjE@._V1_SY298_CR11,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A glimpse into an alternate history of North America. What life after WWII may have been like if the Nazis had won the war." title="A glimpse into an alternate history of North America. What life after WWII may have been like if the Nazis had won the war." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A glimpse into an alternate history of North America. What life after WWII may have been like if the Nazis had won the war." title="A glimpse into an alternate history of North America. What life after WWII may have been like if the Nazis had won the war." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1740299/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "The Man in the High Castle" </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1119597081?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1119597081" data-source="bylist" data-id="ls056131825" data-rid="1VNCY0X3H6HXS55B6EH1" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Watch the first full-length trailer for &quot;Jessica Jones.&quot;" alt="Watch the first full-length trailer for &quot;Jessica Jones.&quot;" src="http://ia.media-imdb.com/images/M/MV5BMTcwMzM3NTE5NF5BMl5BanBnXkFtZTgwNjUwNDkwNzE@._V1_SY298_CR3,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcwMzM3NTE5NF5BMl5BanBnXkFtZTgwNjUwNDkwNzE@._V1_SY298_CR3,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Watch the first full-length trailer for &quot;Jessica Jones.&quot;" title="Watch the first full-length trailer for &quot;Jessica Jones.&quot;" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Watch the first full-length trailer for &quot;Jessica Jones.&quot;" title="Watch the first full-length trailer for &quot;Jessica Jones.&quot;" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2357547/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Jessica Jones" </a> </div> </div> <div class="secondary ellipsis"> Series Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3343889177?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3343889177" data-source="bylist" data-id="ls002309697" data-rid="1VNCY0X3H6HXS55B6EH1" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="A continuation of the saga created by George Lucas set thirty years after 'Star Wars: Episode VI - Return of the Jedi.'" alt="A continuation of the saga created by George Lucas set thirty years after 'Star Wars: Episode VI - Return of the Jedi.'" src="http://ia.media-imdb.com/images/M/MV5BMTkwNzAwNDA4N15BMl5BanBnXkFtZTgwMTA2MDcwNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkwNzAwNDA4N15BMl5BanBnXkFtZTgwMTA2MDcwNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A continuation of the saga created by George Lucas set thirty years after 'Star Wars: Episode VI - Return of the Jedi.'" title="A continuation of the saga created by George Lucas set thirty years after 'Star Wars: Episode VI - Return of the Jedi.'" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A continuation of the saga created by George Lucas set thirty years after 'Star Wars: Episode VI - Return of the Jedi.'" title="A continuation of the saga created by George Lucas set thirty years after 'Star Wars: Episode VI - Return of the Jedi.'" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2488496/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Star Wars: Episode VII - The Force Awakens </a> </div> </div> <div class="secondary ellipsis"> Theatrical Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251673782&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/renewed-canceled-and-on-the-bubble/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_re_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>What Is the Fate of Your Favorite TV Show?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/falltv/renewed-canceled-and-on-the-bubble/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_re_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage#1" > <img itemprop="image" class="pri_image" title="MythBusters (2003-)" alt="MythBusters (2003-)" src="http://ia.media-imdb.com/images/M/MV5BMTQxNzM4MzY0N15BMl5BanBnXkFtZTgwNjYzMzQ0MzE@._V1_SY201_CR33,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQxNzM4MzY0N15BMl5BanBnXkFtZTgwNjYzMzQ0MzE@._V1_SY201_CR33,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/falltv/renewed-canceled-and-on-the-bubble/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_re_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage#6" > <img itemprop="image" class="pri_image" title="Dr. Ken (2015-)" alt="Dr. Ken (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTg3NDIzNTc4OF5BMl5BanBnXkFtZTgwNDc4MDM5NjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3NDIzNTc4OF5BMl5BanBnXkFtZTgwNDc4MDM5NjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/falltv/renewed-canceled-and-on-the-bubble/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_re_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage#16" > <img itemprop="image" class="pri_image" title="Blindspot (2015-)" alt="Blindspot (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTEyMjM1OTQ2NzReQTJeQWpwZ15BbWU4MDkwODIxOTYx._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTEyMjM1OTQ2NzReQTJeQWpwZ15BbWU4MDkwODIxOTYx._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Keep track of the broadcast, cable, and streaming shows you love with our rundown of all this season's renewals, cancellations, and more.</p> <p class="seemore"> <a href="/falltv/renewed-canceled-and-on-the-bubble/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_re_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251833822&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Find out about your favorite TV series </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59126404?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjA3MDc4OTM4MF5BMl5BanBnXkFtZTgwODY0MDgxNDE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59126404?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >âPower Rangersâ: âMe and Earl and the Dying Girlâsâ R.J. Cyler to Play Blue Ranger</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> <a href="/company/co0173285?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Lionsgate</a> has tapped â<a href="/title/tt2582496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Me and Earl and the Dying Girl</a>â actor <a href="/name/nm5518972?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">R.J. Cyler</a> as the Blue Ranger for its upcoming â<a href="/title/tt3717490?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk4">Power Rangers</a>â movie. The studio made the announcement Friday on Twitter and Instagram. Ranger Nation! Welcome @RJ_Cyler to the #PowerRangers Movie team as the Blue Ranger! More: https://...                                        <span class="nobr"><a href="/news/ni59126404?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59126036?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Michael Mooreâs âWhere to Invade Nextâ Gets Oscar-Qualifying Release (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59124858?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Bella Thorne Joins Horror-Comedy âThe Babysitterâ (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59124794?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Can 'Anomalisa' take down Pixar rivals 'Inside Out' & 'Good Dinosaur' to win Best Animated Oscar?</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Gold Derby</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59126060?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >How The Batmobile Will Evolve, According to Zack Snyder</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59126404?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjA3MDc4OTM4MF5BMl5BanBnXkFtZTgwODY0MDgxNDE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59126404?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >âPower Rangersâ: âMe and Earl and the Dying Girlâsâ R.J. Cyler to Play Blue Ranger</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> <a href="/company/co0173285?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Lionsgate</a> has tapped â<a href="/title/tt2582496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Me and Earl and the Dying Girl</a>â actor <a href="/name/nm5518972?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">R.J. Cyler</a> as the Blue Ranger for its upcoming â<a href="/title/tt3717490?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk4">Power Rangers</a>â movie. The studio made the announcement Friday on Twitter and Instagram. Ranger Nation! Welcome @RJ_Cyler to the #PowerRangers Movie team as the Blue Ranger! More: https://...                                        <span class="nobr"><a href="/news/ni59126404?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59125793?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Rhys Ifans, Ed Skrein to Star in Sci-Fi Thriller âGateway 6â</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59125920?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >How Spider-Man Was Injured Filming Captain America: Civil War</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59124793?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Edgar Ramirez in Talks to Join Emily Blunt in âThe Girl on the Trainâ</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59124711?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Matthew McConaugheyâs âFree State of Jonesâ Set Against âSnowden,â âFriday the 13thâ</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59126884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjA4OTMwNjUzNl5BMl5BanBnXkFtZTgwMzIxNjU3NTE@._V1_SY150_CR3,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59126884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Ashley Tisdaleâs âClippedâ Canceled at TBS After First Season</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p><a href="/company/co0005051?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">TBS</a> has taken the shears to â<a href="/title/tt2361094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Clipped</a>,â canceling the comedy after one season. The series starred <a href="/name/nm0864308?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Ashley Tisdale</a>, <a href="/name/nm0001841?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">George Wendt</a> and <a href="/name/nm4175414?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk5">Mike Castle</a> and was set in a Boston-area barbershop. It focused on a now-grown, formerly unpopular kid (<a href="/name/nm1331735?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk6">Ryan Pinkston</a>), who becomes his old classmatesâ boss. The series ...                                        <span class="nobr"><a href="/news/ni59126884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59125500?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Vice Plans to Launch TV Channels in U.S., Europe</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59124719?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >The Flash Stars Talk Henry Allen's 'Confusing' and Abrupt Departure</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59124642?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Agent Carter Recruits Kurtwood Smith for Recurring Season 2 Role</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59124760?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Chicago Med Taps Beauty and the Beast Actor as... the New Gregory House?</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59126031?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNDg4MzcwNjE1M15BMl5BanBnXkFtZTcwODEyMDA4MQ@@._V1_SY150_CR16,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59126031?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Lamar Odomâs $75,000 Brothel Bill May Not Be Settled, Owner Wants Khloe Kardashian to Pay</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p><a href="/name/nm1156027?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Lamar Odom</a>âs $75,000 brothel bill may not be settled, Love Ranch owner <a href="/name/nm1348053?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Dennis Hof</a> told TheWrap on Friday, and it may fall into <a href="/name/nm2835957?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk3">KhloÃ© Kardashian</a>âs lap if thatâs the case. âHe paid his credit card but then I got a call from Boulevard Management asking all these questions, âWhat were these charges for...                                        <span class="nobr"><a href="/news/ni59126031?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59124757?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Ronda Rousey Is Training Vin Diesel's 7-Year-Old Daughter in Judo: ''I'm Creating a Beast," Actor Says</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59125916?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Andy Kaufman and Redd Foxx Holograms to Tour Because People Who Crave Money Often Have Stupid Ideas</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59124573?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Yolanda Foster Gives Update Amid Lyme Disease Battle, Celebrates ''Small Victories''</a>
    <div class="infobar">
            <span class="text-muted">22 October 2015 11:16 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59124574?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Drake Is Getting His Own Lipstick Shade Thanks to Tom FordâAnd So Is Jake Gyllenhaal!</a>
    <div class="infobar">
            <span class="text-muted">22 October 2015 11:06 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/gallery/rg567122688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <h3><i>Freaks of Nature</i> - Exclusive Character Posters</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm313123328/rg567122688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Freaks of Nature (2015)" alt="Freaks of Nature (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjA0MDUwMTk1N15BMl5BanBnXkFtZTgwNTU2NjAxNzE@._V1_SY219_CR2,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0MDUwMTk1N15BMl5BanBnXkFtZTgwNTU2NjAxNzE@._V1_SY219_CR2,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm531227136/rg567122688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Freaks of Nature (2015)" alt="Freaks of Nature (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjIwOTkxOTgxNV5BMl5BanBnXkFtZTgwODU2NjAxNzE@._V1_SY219_CR2,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIwOTkxOTgxNV5BMl5BanBnXkFtZTgwODU2NjAxNzE@._V1_SY219_CR2,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm296346112/rg567122688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Freaks of Nature (2015)" alt="Freaks of Nature (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTc4MjQyODE1Ml5BMl5BanBnXkFtZTgwNjU2NjAxNzE@._V1_SY219_CR2,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc4MjQyODE1Ml5BMl5BanBnXkFtZTgwNjU2NjAxNzE@._V1_SY219_CR2,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm279568896/rg567122688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Freaks of Nature (2015)" alt="Freaks of Nature (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjA4MDM2NjE1N15BMl5BanBnXkFtZTgwNzU2NjAxNzE@._V1_SY219_CR2,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA4MDM2NjE1N15BMl5BanBnXkFtZTgwNzU2NjAxNzE@._V1_SY219_CR2,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/gallery/rg567122688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251669862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See the full gallery </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=10-23&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005351?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ryan Reynolds" alt="Ryan Reynolds" src="http://ia.media-imdb.com/images/M/MV5BOTI3ODk1MTMyNV5BMl5BanBnXkFtZTcwNDEyNTE2Mg@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTI3ODk1MTMyNV5BMl5BanBnXkFtZTcwNDEyNTE2Mg@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005351?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Ryan Reynolds</a> (39) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0263759?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Briana Evigan" alt="Briana Evigan" src="http://ia.media-imdb.com/images/M/MV5BMTY3Mzg1NzY3MV5BMl5BanBnXkFtZTcwMjU5NDE2OQ@@._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY3Mzg1NzY3MV5BMl5BanBnXkFtZTcwMjU5NDE2OQ@@._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0263759?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Briana Evigan</a> (29) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1126641?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Masiela Lusha" alt="Masiela Lusha" src="http://ia.media-imdb.com/images/M/MV5BMTQ1NjI0OTU4M15BMl5BanBnXkFtZTcwMDU4NTkzOQ@@._V1_SY172_CR23,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1NjI0OTU4M15BMl5BanBnXkFtZTcwMDU4NTkzOQ@@._V1_SY172_CR23,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1126641?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Masiela Lusha</a> (30) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1888211?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jessica Stroup" alt="Jessica Stroup" src="http://ia.media-imdb.com/images/M/MV5BMTkxMzg3Mzk5M15BMl5BanBnXkFtZTgwODcxOTQyNTE@._V1_SY172_CR1,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxMzg3Mzk5M15BMl5BanBnXkFtZTgwODcxOTQyNTE@._V1_SY172_CR1,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1888211?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Jessica Stroup</a> (29) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000600?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Sam Raimi" alt="Sam Raimi" src="http://ia.media-imdb.com/images/M/MV5BODQ0NjI0NzkzMV5BMl5BanBnXkFtZTYwMDc0ODk1._V1_SY172_CR4,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODQ0NjI0NzkzMV5BMl5BanBnXkFtZTYwMDc0ODk1._V1_SY172_CR4,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000600?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Sam Raimi</a> (56) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=10-23&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/galleries/supergirl?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249616302&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_sup_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249616302&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>TV Spotlight: Get Ready for "Supergirl"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/supergirl-rm3117539840?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249616302&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_sup_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249616302&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Supergirl (2015-)" alt="Supergirl (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMzIzODg5NTgwOF5BMl5BanBnXkFtZTgwMjYxMzAxNzE@._V1_SY230_CR13,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzIzODg5NTgwOF5BMl5BanBnXkFtZTgwMjYxMzAxNzE@._V1_SY230_CR13,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/supergirl-rm2178015744?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249616302&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_sup_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249616302&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Supergirl (2015-)" alt="Supergirl (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTcwNjU3MDMzN15BMl5BanBnXkFtZTgwODYxMzAxNzE@._V1_SX307_CR0,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcwNjU3MDMzN15BMl5BanBnXkFtZTgwODYxMzAxNzE@._V1_SX307_CR0,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/falltv/galleries/supergirl?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249616302&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_sup_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249616302&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See photos of the upcoming series </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Indie Focus: Get 'Lost in the Sun'</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3593046/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249621802&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249621802&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Lost in the Sun (2015)" alt="Lost in the Sun (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTU4NTA5Nzk3M15BMl5BanBnXkFtZTgwNDIyMDQ5NjE@._V1_SY250_CR3,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4NTA5Nzk3M15BMl5BanBnXkFtZTgwNDIyMDQ5NjE@._V1_SY250_CR3,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4189106713?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249621802&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249621802&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi4189106713" data-rid="1VNCY0X3H6HXS55B6EH1" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Lost in the Sun (2015)" alt="Lost in the Sun (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTc2NzExNTUzMl5BMl5BanBnXkFtZTgwNzM1MTMwNzE@._V1_SY250_CR101,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc2NzExNTUzMl5BMl5BanBnXkFtZTgwNzM1MTMwNzE@._V1_SY250_CR101,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Lost in the Sun (2015)" title="Lost in the Sun (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Lost in the Sun (2015)" title="Lost in the Sun (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Watch a clip from this story of a small-time crook who forges an unexpected and powerful bond with a newly-orphaned teenage boy on their open-road adventure.</p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt3672742/trivia?item=tr2585040&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3672742?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Turbo Kid (2015)" alt="Turbo Kid (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTY1NjkwNDQ0NF5BMl5BanBnXkFtZTgwMTkyOTE2NjE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY1NjkwNDQ0NF5BMl5BanBnXkFtZTgwMTkyOTE2NjE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt3672742?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Turbo Kid</a></strong> <p class="blurb">The movie was supposed to be set in a desertic wasteland, but due to rainy weather during filming, the "acid rain" was included in the storyline. Ponds and puddles were tinted green to simulate this.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt3672742/trivia?item=tr2585040&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-25"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;S8XtmW4i9BE&quot;}">
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/S8XtmW4i9BE/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Top 25 Inspirational Quotes by Famous Personalities</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/S8XtmW4i9BE/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Albert Einstein" alt="Albert Einstein" src="http://ia.media-imdb.com/images/M/MV5BMTQ5NjEzNzc3NF5BMl5BanBnXkFtZTcwMTUxMDkwOA@@._V1_SY207_CR10,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ5NjEzNzc3NF5BMl5BanBnXkFtZTcwMTUxMDkwOA@@._V1_SY207_CR10,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/S8XtmW4i9BE/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Mark Twain" alt="Mark Twain" src="http://ia.media-imdb.com/images/M/MV5BMTc3NzcyMjU3MF5BMl5BanBnXkFtZTgwNDg2OTI1MDE@._V1_SY207_CR7,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc3NzcyMjU3MF5BMl5BanBnXkFtZTgwNDg2OTI1MDE@._V1_SY207_CR7,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/S8XtmW4i9BE/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Theodore Roosevelt" alt="Theodore Roosevelt" src="http://ia.media-imdb.com/images/M/MV5BMTY4OTgyOTUxNF5BMl5BanBnXkFtZTgwMDg2OTI1MDE@._V1_SY207_CR6,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4OTgyOTUxNF5BMl5BanBnXkFtZTgwMDg2OTI1MDE@._V1_SY207_CR6,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/S8XtmW4i9BE/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Martin Luther King" alt="Martin Luther King" src="http://ia.media-imdb.com/images/M/MV5BMjAwMzYyMTkwM15BMl5BanBnXkFtZTYwNjU1OTM2._V1_SY207_CR7,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAwMzYyMTkwM15BMl5BanBnXkFtZTYwNjU1OTM2._V1_SY207_CR7,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/S8XtmW4i9BE/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="John Lennon" alt="John Lennon" src="http://ia.media-imdb.com/images/M/MV5BMTYwMDE4MzgzMF5BMl5BanBnXkFtZTYwMDQzMzU3._V1_SY207_CR31,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwMDE4MzgzMF5BMl5BanBnXkFtZTYwMDQzMzU3._V1_SY207_CR31,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Which of these 25 quotes by famous personalities do you consider as the most inspirational? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/249122997?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"> <a href="/poll/S8XtmW4i9BE/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2249486622&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=160222478565;ord=160222478565?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=160222478565?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=160222478565?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2473510"></div> <div class="title"> <a href="/title/tt2473510?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Paranormal Activity: The Ghost Dimension </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2473510?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2080374"></div> <div class="title"> <a href="/title/tt2080374?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Steve Jobs </a> <span class="secondary-text"></span> </div> <div class="action"> Nationwide Expansion </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3614530"></div> <div class="title"> <a href="/title/tt3614530?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Jem and the Holograms </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1618442"></div> <div class="title"> <a href="/title/tt1618442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> The Last Witch Hunter </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3164256"></div> <div class="title"> <a href="/title/tt3164256?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Rock the Kasbah </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3077214"></div> <div class="title"> <a href="/title/tt3077214?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Suffragette </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3640682"></div> <div class="title"> <a href="/title/tt3640682?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> I Smile Back </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2503944"></div> <div class="title"> <a href="/title/tt2503944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Burnt </a> <span class="secondary-text"></span> </div> <div class="action"> Opens in NYC/LA </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3121332"></div> <div class="title"> <a href="/title/tt3121332?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t8"> Nasty Baby </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2611390"></div> <div class="title"> <a href="/title/tt2611390?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t9"> Difret </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2251549482&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/bd-wong-not-staying-hush-hush-about-mr-robot?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248637242&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_bdw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248637242&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>BD Wong Not Staying Hush-Hush About "Mr. Robot"</h3> </a> </span> </span> <p class="blurb">BD Wong reveals to IMDb why he was initially reluctant to take on his role as Whiterose in "Mr. Robot" and shares what he knows about Season 2.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/bd-wong-not-staying-hush-hush-about-mr-robot?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248637242&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_bdw_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248637242&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Mr. Robot (2015-)" alt="Mr. Robot (2015-)" src="http://ia.media-imdb.com/images/M/MV5BOTA4Mzc4MzUxMV5BMl5BanBnXkFtZTgwNTM2NjgwNzE@._V1_SX700_CR0,0,700,393_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTA4Mzc4MzUxMV5BMl5BanBnXkFtZTgwNTM2NjgwNzE@._V1_SX700_CR0,0,700,393_AL_UY786_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/falltv/bd-wong-not-staying-hush-hush-about-mr-robot?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248637242&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_bdw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248637242&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Read more </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1051904"></div> <div class="title"> <a href="/title/tt1051904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Goosebumps </a> <span class="secondary-text">$23.6M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3659388"></div> <div class="title"> <a href="/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> The Martian </a> <span class="secondary-text">$21.3M</span> </div> <div class="action"> <a href="/showtimes/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3682448"></div> <div class="title"> <a href="/title/tt3682448?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Bridge of Spies </a> <span class="secondary-text">$15.4M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2554274"></div> <div class="title"> <a href="/title/tt2554274?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Crimson Peak </a> <span class="secondary-text">$13.1M</span> </div> <div class="action"> <a href="/showtimes/title/tt2554274?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2510894"></div> <div class="title"> <a href="/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Hotel Transylvania 2 </a> <span class="secondary-text">$12.6M</span> </div> <div class="action"> <a href="/showtimes/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1018765"></div> <div class="title"> <a href="/title/tt1018765?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Our Brand Is Crisis </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1727776"></div> <div class="title"> <a href="/title/tt1727776?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Scouts Guide to the Zombie Apocalypse </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3774694"></div> <div class="title"> <a href="/title/tt3774694?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Love </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3044244"></div> <div class="title"> <a href="/title/tt3044244?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Les merveilles </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3735398"></div> <div class="title"> <a href="/title/tt3735398?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Carter High </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/anniversary/then-and-now/ls079427874?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248638002&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_tan_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248638002&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb 25: A Look at 25 Top Stars - Then & Now</h3> </a> </span> </span> <p class="blurb">We turn a spotlight on 25 of our favorite performers and reflect on where their respective careers were at as IMDb was getting its start.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/then-and-now/ls079427874?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248638002&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_tan_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248638002&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage#image9" > <img itemprop="image" class="pri_image" title="Empire of the Sun (1987)" alt="Empire of the Sun (1987)" src="http://ia.media-imdb.com/images/M/MV5BMjA2MDkxNzQwMV5BMl5BanBnXkFtZTcwNjU4NTMyNw@@._V1_SY525_CR51,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA2MDkxNzQwMV5BMl5BanBnXkFtZTcwNjU4NTMyNw@@._V1_SY525_CR51,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/anniversary/then-and-now/ls079427874?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248638002&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_tan_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2248638002&pf_rd_r=1VNCY0X3H6HXS55B6EH1&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See the full list </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYrH72nDZnSU_Pd6kBHoGZ6wDHfK7fpK2ijRXxWVDsqfL2tTrGBHl-OD5XiMp6t0yAdB0fdOUXw%0D%0Am40moC0nSLC1WZ6m3z_Fv6BKDyRfGLHJuipqgglJ486HdHQAQf0_pVbyPhng27TiKgpay9bHkknN%0D%0ApZejTTQKzOW5pxFse_i9KLXtWxPe5b7pKDNS7ypV94l_eBVAf2AtAlDLlrsz6574gKTZgZF-yi0H%0D%0AsxJWWCaUlmg%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYklDu53nC_3od_KgJnw7Yl5aG3Qqv2U-zF1GHvfxntxrDhD4-vdzJAesasp3tDg66Z-vQd-Ao5%0D%0AdSkRTPCLdyvesUWBCJ7DCq8iycqCLVaxldx5S10h30snVcxTXdru3Q80syzLai1n2als1UYZ_3mW%0D%0AyiD8XoEBIODJjBuifJsiH67jv63N7MajUr4O09Y7nUEAxLb80Z4zU2f_qImRUQJSLA%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYkxJwastt2GXJWBRpABxvTjPKdnBP6SMvAFj8i0v-ApAZo0-va53AYpeSrje1EEUvYMtCfJqY7%0D%0ADa8kV8rW0sTPrXDDCRVxPfoMPjXx1vBV7R2Codb1qfmZj9fyWsdsTaush7bjustZkEE9-wryHd80%0D%0AZITBKiX-GNrjcNVpxyevwnO3T85Y4YrRzxQwYSaxlg3p8PsWZx66C3qgagXPJ94EDAIktIkWbmoH%0D%0AQa7yJvkypy9qyos0L6syIIpIQcwMewOcPlOvKdc4RYRNxW7IsZD3RIGtjnW1ujIT64HJ1_YN_Z9g%0D%0AyS7-CW7VdMRme31ubTMj1jlFI4EBBXLJ--wTcbckrntsCMX_IyvvKPqBSD2eC47KG8PsmnyWQWqJ%0D%0A0CwwRCXqZlCmEc1qZN_LhMY7xTmAUg%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYq7sjsVzb0yrdi-Si7TbhpFxKh45fiwYubnX_MMPFTX2FDMeb6xEprVr5BhhF4D_pLGtuPlK1C%0D%0AIMrYC0dtt5Qh77ms1G8MPs0m-dEZDDG2lwxkzhrQMrR7CKN-Bt1u23kCiIBM7j_vRBxnmZjNW_fR%0D%0AVouPAHEHYDjJ9pi8fqxUYm5jawxKFLd73crWXCBcqnxABIBEm10iU0kmQolukF-HOQ%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYjs2g32sRVul5fScPRXsIBqVyHcS16eMMAGL3WeRpmz5_KFXN_FzKcIfd6x49lIuYteVjugLiI%0D%0A04sa5Mru7fP1_-mIyS6leSgNQoGBCBjTHtB_AnTowM8Jea49yGNeghjOu1Kuef5HpMtGUxFEfkRG%0D%0AOtXx2iH6J13uL2HJxF_LE0HN6KDymn_PdPofHg1s60Wc%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYpQyT_unzPLzwL4l3Xvk-ku3cRlj11lCzzTelxRFiWm8uScbaFwUiOooYQoMvdzOhVLJ9M-6X5%0D%0A-NM6GYadw_4Wdp2X0096Emg-_A-FjWg9oHpxrgXpb53LhoWR2FW9IZMEBav_dsiJ28qgOrWnK_Ir%0D%0AgR_lvfhYWno0nHvqIBNJgl6xXpZ2ZI1bvjFGiqRDdwRYqcQ3Nr2RUJ0emYzVbUXn9Q%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYrmQr_wyyL7K-ZPQURBCy0-F67xIV6S5_VeEzxyy1CmN68jtUST5MjBmX0f01qcG68eMjnWM2a%0D%0AzpnbOo7mJme27HT1VUbMagM7-YlIs1HMy7h26NH1b2lxzNDwVi8uJpEUNPAkEwKFNf6-ps_ERndI%0D%0AMRevIKTEkMWbT67BrTY0DeCxSGicWkaZLhA9EJHfOJ7oGk_pohEmV2ZyrH9Bw_e5fDxx2T1IH5OJ%0D%0ADywemS9BwVpFIpPrTrZcGdTZVbYhLZcyGRjsbywAPoZkL7USUAMfPQ%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYrejkv7ZXhnDEQgex5Xc99y3dVwT6uTFcIM8rLH_uLDdxE7lCGUa6bBc-G9AUE0ocINVNnS4Zq%0D%0Ao7pdA4BO0XWsAyzZ0hcwmfy-jQEHhcE2GjZaJ23MTnPKbbKGBo0P0bkDOFDBo7GwEpnaZDiYcf7S%0D%0A5QtPeCQVS3oFeU73m7fVrZaRsy-4c1ASaQlE9JipPtfNF2T-BlZUEuBIHoGl0nLmDg%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYsmW5Rh4UQVWO3biXEcVXirvlNtML7TN3P0R4ZCHlMn3grlBxdChUb2OcNOa5huIXpBeKkr7GG%0D%0A72u1e1-Qcu0Hq3ZI48u_2jCwlLt6cIFX7hYIN1U9_qbhOmba8JLBzxFF-jGBKQEpgRaP2eIMZMUI%0D%0AVMe2dUqBr8cLV1thJhHeMIgk3MYAyHkRJ-VcZVl2gIW5%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYp8hUWK1hAwH9kR7re_4ePoV_k6-dU0GoMctL_Uaf4vW1l5wWgvNUIkUEnwYnPTHCXQw4Pqi6N%0D%0AaAyE9FickChOI8OVWst8pqHJYET-pQ5UOjSr7JpQVD8j2N_SXwOsMaNEnxOddSo0OlEKYH0nH0-T%0D%0AG5Dglahop8P-BW5mO4_YJfvZcJ4xEg4ZloOscYiJWnMPTnFYn5_0UQSDc5AypWOKMymq-G2IaM0O%0D%0AaoIKaaoJEIAU56kU8wngqRIhOXcfQPNB%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYgDBf87NlqzIXv5pguHpmJALN6ShmCTC-YnF1EzhB1ZOC4igyzqzJPgGAZdzjGL9tRDYDwGi8t%0D%0Abd6iQD7iLYGR_mYJV0G-ygMxAYZDPGJHJzUM6dX7Xi330Jid3BV0TXzIAVSKpA6F1dHn8y4bI6ox%0D%0APxlIrkSZJCI0pWRorQnC-S8fyqYBBFEkH7Y7Ps6QBGts5Cu8J-id7BjH9WezSkbNOrvFCuUGIuzF%0D%0A-bIMnWaqj7A%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYkpjaaGVzl8o3syk1iwKI2gbtOrwKCSN4J8zXd8U9DEwmwDRUv-ZhjxO5AfHn-U9pGNibCpOTg%0D%0AVdO_ZiXIjeCq0ozd2QrtKQ7HDvQxlzvrHLE5AlJLR5Q94GTeP1ZtQ6lzKa4nAWD8S0oA8leBDp9v%0D%0AuoNPgPm2DjcmhA_-1xNC_YlV40PjqWUySqD3QwHEt8GlwCjNYqr6uWSsKEMIFjqZG6tsxNHB9Lul%0D%0AvOy5VC0McFI%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYtZ3EhpwRtXOcHlvqrjQYc4sYrlYdsljTGbc5PBZAO5tzP9YhU68MDJ2MqtjSKccB1vIE8GOQ6%0D%0A1mrkNkbTDVDFb_7ABgpPiwop3CZTnBoT5CiFFOm9rNHw4ykqF5dYfO6fAs5901qnnCzthfwTr92M%0D%0A0RkdxLmcmkVQh_7D9lZDrXBpdA5Bnlc-twkqSWHDGOMQq-Pw77B6sPLPAWjaMRXKwu34flVZZlbO%0D%0AsTU-3g2l5AY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYhHWyYg-GmIa-jtg8-GCLOXae222hPYsNWLQeO9Rs75RZsIhdW9-6KjVycHssEhmdARdvccKX1%0D%0ABbJdJH3DY7MpToGEK7WB9pmDFghI1sMJVPnuvxwwN7H7kAsaJ11NWWvdkHocY9a9FQyF7a6BSG8N%0D%0Am_I8EDVzN-iDzNtQ1sDwyoRnA5bi55IT3GrelQf7pElzZIH4quGQmTEXaiCPlZGb2luyV9HS2fWb%0D%0AXDxO8GOSHa4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYoszupFAhC3sPUff_rSSTTmd-YYfzQJOAQtwOlnqevnmaKRF2wKIxrr23d1kyxSbYYLA7pfyY0%0D%0AtA70r5KQd2oZIOZHb4vVAOP4r1GOJ3jYFd2WKkS-H2V-TAKxST2iQtiticSjV8O7IoGq6UapQdrh%0D%0AfX9OWcRkPpnj5RNQsYG_f9FsivJnvCqG6mW9WyRvkNyCc0HhLctEm0AfIOTGwMmLlu6VBPQVHpWU%0D%0AfcjcKw4TUQI%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYhXPQ0Ap1IoeZRTgxLn_EOc64yUNTNkuO117EfQQOc25DG2MaZEVyFxzxJHYOlGYYTPPskKXS4%0D%0Alpvo8FWT4DZIY9eZ152dGoRaMeXpTU6qa56lCizArLZlXg7t1MZ1l9t609DGW6PhG6HZ7cmAn4YB%0D%0A5bCexU3iYIc9clwDCdVTZY0Z-7PC-UwHjnoosmjJ2uNyr774Dx5aNbzLNt5zQJ_fYw08iyxRmxJR%0D%0A1H68qDc65Pm3lQ_Yk7ySIRwAmyw9Zx1T%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYka11RCFDXbhMwaADXd3okXyyi8lMpGIWZ9W0Ub1gW4GECLAgwn9rQtgBim0pWzWPbxo7T0CjR%0D%0Aj-nrbicxNGFz2Y1fg8o40R9p4OWUWksdPaj7-Uk5LSR_SUOownflZHo0r034mKwzABxhUCpigzc6%0D%0AQqPn8BM9kwhxMgoEihx2FQs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYj73L2hTXlWyn_80hKDQYp4U-30JCilYBtl-Sk54BnYbszHOcWBAd1H7i7SC6GQJOKXANqdQfS%0D%0ApXOdLa_5hcxLhstOuC1VAEbxEslt9NOb6qJbUWwRV-UAekxCKG6M0Vfa-OSsaH6oSCrhwsyY444-%0D%0ADORt37G6PT-bZYkbvPT-x3k%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-1062213378._CB290996593_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-1142428784._CB290967098_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1238345358._CB290967079_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3920146857._CB292796746_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-1294823147._CB290767939_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101977d0faed24b36ae1aa1817322c0f1375184fb517639513caa2b1780fd62fe9e",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=160222478565"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=160222478565&ord=160222478565";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="245"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
