



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "627-4994776-8114275";
                var ue_id = "05248M3GAZTCVZAAM13E";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="05248M3GAZTCVZAAM13E" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-67ead2d9.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-903233829._CB298432192_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3796876714._CB298540748_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
!function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c?c:a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";a(2)},{2:2}],2:[function(a,b,c){"use strict";!function(){var a,b,c=function(a){return"[object Array]"===Object.prototype.toString.call(a)},d=function(a,b){for(var c=0;c<a.length;c++)c in a&&b.call(null,a[c],c)},e=[],f=!1,g=!1,h=function(){var a=[],b=[],c={};return d(e,function(e){var f="";d(e.dartsite.split("/"),function(b){""!==b&&(b in c||(c[b]=a.length,a.push(b)),f+="/"+c[b])}),b.push(f)}),{iu_parts:a,enc_prev_ius:b}},i=function(){var a=[];return d(e,function(b){var c=[];d(b.sizes,function(a){c.push(a.join("x"))}),a.push(c.join("|"))}),a},j=function(){var a=[];return d(e,function(b){a.push(k(b.targeting))}),a.join("|")},k=function(a,b){var c,d=[];for(var e in a){c=[];for(var f=0;f<a[e].length;f++)c.push(encodeURIComponent(a[e][f]));b?d.push(e+"="+encodeURIComponent(c.join(","))):d.push(e+"="+c.join(","))}return d.join("&")},l=function(){var a=+new Date;g||document.readyState&&"loaded"!==document.readyState||(g=!0,f&&imdbads.cmd.push(function(){for(var b=0;b<e.length;b++)generic.monitoring.record_metric(e[b].name+".fail",csm.duration(a))}))};window.tinygpt={define_slot:function(a,b,c,d){e.push({dartsite:a.replace(/\/$/,""),sizes:b,name:c,targeting:d})},set_targeting:function(b){a=b},callback:function(a){for(var c,d,f={},g=+new Date,h=0;h<e.length;h++)c=e[h].dartsite,d=e[h].name,a[h][c]?f[d]=a[h][c]:window.console&&console.error&&console.error("Unable to correlate GPT response for "+d);imdbads.cmd.push(function(){for(var a=0;a<e.length;a++)ad_utils.slot_events.trigger(e[a].name,"request",{timestamp:b}),ad_utils.slot_events.trigger(e[a].name,"tagdeliver",{timestamp:g});ad_utils.gpt.handle_response(f)})},send:function(){var d,g,m=[],n=function(a,b){c(b)&&(b=b.join(",")),b&&m.push(a+"="+encodeURIComponent(""+b))};if(0===e.length)return void tinygpt.callback({});n("gdfp_req","1"),n("correlator",Math.floor(4503599627370496*Math.random())),n("output","json_html"),n("callback","tinygpt.callback"),n("impl","fifs"),n("json_a","1");var o=h();n("iu_parts",o.iu_parts),n("enc_prev_ius",o.enc_prev_ius),n("prev_iu_szs",i()),n("prev_scp",j()),n("cust_params",k(a,!0)),d=document.createElement("script"),g=document.getElementsByTagName("script")[0],d.async=!0,d.type="text/javascript",d.src="http://pubads.g.doubleclick.net/gampad/ads?"+m.join("&"),d.id="tinygpt",d.onload=d.onerror=d.onreadystatechange=l,f=!0,g.parentNode.insertBefore(d,g),b=+new Date}}}()},{}]},{},[1]);</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['c'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['834642195967'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4261578659._CB296130991_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"68c90021a8a95d869aad2e861ce87cf1cbffb944",
"2016-04-12T17%3A02%3A35GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50245;
generic.days_to_midnight = 0.5815393328666687;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3230827532._CB299577821_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'c']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=834642195967;ord=834642195967?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript> <a href="http://pubads.g.doubleclick.net/gampad/jump?&iu=4215/imdb2.consumer.homepage/&sz=1008x150|1008x200|1008x30|970x250|9x1&t=p%3Dtop%26p%3Dt%26fv%3D1%26ab%3Dc%26bpx%3D2%26c%3D0%26s%3D3075%26s%3D32&tile=0&c=834642195967" target="_blank"> <img src="http://pubads.g.doubleclick.net/gampad/ad?&iu=4215/imdb2.consumer.homepage/&sz=1008x150|1008x200|1008x30|970x250|9x1&t=p%3Dtop%26p%3Dt%26fv%3D1%26ab%3Dc%26bpx%3D2%26c%3D0%26s%3D3075%26s%3D32&tile=0&c=834642195967" border="0" alt="advertisement" /> </a> </noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=04-12&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_3"
>Sundance</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_6"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_7"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_8"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_9"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_10"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_11"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59706443/?ref_=nv_nw_tn_1"
> Tom Cruise Promises âIncredible Set Piecesâ for âMission: Impossible 6â
</a><br />
                        <span class="time">12 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59705884/?ref_=nv_nw_tn_2"
> Barbra Streisand, Barry Levinson Reviving âGypsyâ for STX Entertainment (Exclusive)
</a><br />
                        <span class="time">18 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59704745/?ref_=nv_nw_tn_3"
> âFast 8â Creates New Character; Scott Eastwood To Star
</a><br />
                        <span class="time">11 April 2016 4:27 PM, UTC</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB276458993_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB276458997_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB276458995_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0057012/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTM1NTYzOTE4Ml5BMl5BanBnXkFtZTYwNzUwOTQ2._V1._UY315_CR0,0,410,315_.jpg",
            titleYears : "1964",
            rank : 50,
                    headline : "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb"
    },
    nameAd : {
            clickThru : "/name/nm0000128/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTQyMTExNTMxOF5BMl5BanBnXkFtZTcwNDg1NzkzNw@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 200,
            headline : "Russell Crowe"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB276459002_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYjpmVztcVu9cEAxWos3vBF3uTG0MygxvGWYTr7z37zFjnnT5XEKUAA4W2ZlP_zwXpf-uilNF6I%0D%0AwyHM43FQLd6UdJIH4n6Eol-TpzXNoB1eWflHWjBwvNMP03wj0hxU7NUYzXD7dDaAYWsMCOS-p6Hy%0D%0ARSf4078M_caxoQfw7RGjVUTzXk4C1dXyXXf5pqeWRg6G0_Q3nlWRMfP6xY9X9Z_1Eg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYiedI-WsE-dwZmG_fPv1NI-QX_-O-B2VO_JEBW0jyEWtWugSdHjhUEZBglm-WLyuEap2NHmWl0%0D%0A5kRkWPD0BRtpWAWQf0FbMT9hG_pl4sf7zwoUveJFhFn-BOgjHGQbnOnwHXmrGPydJC9JMmDOGC8I%0D%0AZUPvv1gUPV8fBj49Bv9ScSpRiLtuoauzF_YwRfqxy2Yb%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYkxFZOR5yy_cdDkB4_CmMEN3uGO699mLc0sUT6JpZatekccRQeTXF1v5RC3QrE-n5QwAVQkS7C%0D%0AMjW6DS5Mws-kZeZ2fxAtPxmQh4pCk4BjAsVL5r02QiLRsBOHxKkoSiBIY_y7UOsNijGxC_QD_TUw%0D%0A3WbyWDcqyT1xR_3QCIv9mlqO5pkGQjXiGosCXgzyQyXL%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=834642195967;ord=834642195967?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=834642195967;ord=834642195967?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi155694361?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466627422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1" data-video="vi155694361" data-source="bylist" data-id="ls002309697" data-rid="05248M3GAZTCVZAAM13E" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="It feels good to be bad... Assemble a team of the world's most dangerous, incarcerated Super Villains, provide them with the most powerful arsenal at the government's disposal, and send them off on a mission to defeat an enigmatic, insuperable entity. U.S. intelligence officer Amanda Waller has determined only a secretly convened group of disparate, despicable individuals with next to nothing to lose will do. However, once they realize they weren't picked to succeed but chosen for their patent culpability when they inevitably fail, will the Suicide Squad resolve to die trying, or decide it's every man for himself?" alt="It feels good to be bad... Assemble a team of the world's most dangerous, incarcerated Super Villains, provide them with the most powerful arsenal at the government's disposal, and send them off on a mission to defeat an enigmatic, insuperable entity. U.S. intelligence officer Amanda Waller has determined only a secretly convened group of disparate, despicable individuals with next to nothing to lose will do. However, once they realize they weren't picked to succeed but chosen for their patent culpability when they inevitably fail, will the Suicide Squad resolve to die trying, or decide it's every man for himself?" src="http://ia.media-imdb.com/images/M/MV5BOTY1MzU1MDQ1MF5BMl5BanBnXkFtZTgwNjAzMjY3NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTY1MzU1MDQ1MF5BMl5BanBnXkFtZTgwNjAzMjY3NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="It feels good to be bad... Assemble a team of the world's most dangerous, incarcerated Super Villains, provide them with the most powerful arsenal at the government's disposal, and send them off on a mission to defeat an enigmatic, insuperable entity. U.S. intelligence officer Amanda Waller has determined only a secretly convened group of disparate, despicable individuals with next to nothing to lose will do. However, once they realize they weren't picked to succeed but chosen for their patent culpability when they inevitably fail, will the Suicide Squad resolve to die trying, or decide it's every man for himself?" title="It feels good to be bad... Assemble a team of the world's most dangerous, incarcerated Super Villains, provide them with the most powerful arsenal at the government's disposal, and send them off on a mission to defeat an enigmatic, insuperable entity. U.S. intelligence officer Amanda Waller has determined only a secretly convened group of disparate, despicable individuals with next to nothing to lose will do. However, once they realize they weren't picked to succeed but chosen for their patent culpability when they inevitably fail, will the Suicide Squad resolve to die trying, or decide it's every man for himself?" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="It feels good to be bad... Assemble a team of the world's most dangerous, incarcerated Super Villains, provide them with the most powerful arsenal at the government's disposal, and send them off on a mission to defeat an enigmatic, insuperable entity. U.S. intelligence officer Amanda Waller has determined only a secretly convened group of disparate, despicable individuals with next to nothing to lose will do. However, once they realize they weren't picked to succeed but chosen for their patent culpability when they inevitably fail, will the Suicide Squad resolve to die trying, or decide it's every man for himself?" title="It feels good to be bad... Assemble a team of the world's most dangerous, incarcerated Super Villains, provide them with the most powerful arsenal at the government's disposal, and send them off on a mission to defeat an enigmatic, insuperable entity. U.S. intelligence officer Amanda Waller has determined only a secretly convened group of disparate, despicable individuals with next to nothing to lose will do. However, once they realize they weren't picked to succeed but chosen for their patent culpability when they inevitably fail, will the Suicide Squad resolve to die trying, or decide it's every man for himself?" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1386697/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466627422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1" > Suicide Squad </a> </div> </div> <div class="secondary ellipsis"> Blitz Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4115117337?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466627422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2" data-video="vi4115117337" data-source="bylist" data-id="ls056131825" data-rid="05248M3GAZTCVZAAM13E" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="A new trailer for &quot;Game of Thrones&quot; Season 6 has arrived!" alt="A new trailer for &quot;Game of Thrones&quot; Season 6 has arrived!" src="http://ia.media-imdb.com/images/M/MV5BMjM5OTQ1MTY5Nl5BMl5BanBnXkFtZTgwMjM3NzMxODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM5OTQ1MTY5Nl5BMl5BanBnXkFtZTgwMjM3NzMxODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A new trailer for &quot;Game of Thrones&quot; Season 6 has arrived!" title="A new trailer for &quot;Game of Thrones&quot; Season 6 has arrived!" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A new trailer for &quot;Game of Thrones&quot; Season 6 has arrived!" title="A new trailer for &quot;Game of Thrones&quot; Season 6 has arrived!" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt0944947/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466627422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2" > "Game of Thrones" </a> </div> </div> <div class="secondary ellipsis"> Brand New Season 6 Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi357020953?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466627422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3" data-video="vi357020953" data-source="bylist" data-id="ls002653141" data-rid="05248M3GAZTCVZAAM13E" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="'Fantastic Beasts and Where to Find Them' opens in 1926 as Newt Scamander has just completed a global excursion to find and document an extraordinary array of magical creatures. Arriving in New York for a brief stopover, he might have come and gone without incident... were it not for a No-Maj (American for Muggle) named Jacob, a misplaced magical case, and the escape of some of Newt's fantastic beasts, which could spell trouble for both the wizarding and No-Maj worlds." alt="'Fantastic Beasts and Where to Find Them' opens in 1926 as Newt Scamander has just completed a global excursion to find and document an extraordinary array of magical creatures. Arriving in New York for a brief stopover, he might have come and gone without incident... were it not for a No-Maj (American for Muggle) named Jacob, a misplaced magical case, and the escape of some of Newt's fantastic beasts, which could spell trouble for both the wizarding and No-Maj worlds." src="http://ia.media-imdb.com/images/M/MV5BMjQyMzIyMTY5NF5BMl5BanBnXkFtZTgwMDA0Mjk0NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQyMzIyMTY5NF5BMl5BanBnXkFtZTgwMDA0Mjk0NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="'Fantastic Beasts and Where to Find Them' opens in 1926 as Newt Scamander has just completed a global excursion to find and document an extraordinary array of magical creatures. Arriving in New York for a brief stopover, he might have come and gone without incident... were it not for a No-Maj (American for Muggle) named Jacob, a misplaced magical case, and the escape of some of Newt's fantastic beasts, which could spell trouble for both the wizarding and No-Maj worlds." title="'Fantastic Beasts and Where to Find Them' opens in 1926 as Newt Scamander has just completed a global excursion to find and document an extraordinary array of magical creatures. Arriving in New York for a brief stopover, he might have come and gone without incident... were it not for a No-Maj (American for Muggle) named Jacob, a misplaced magical case, and the escape of some of Newt's fantastic beasts, which could spell trouble for both the wizarding and No-Maj worlds." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="'Fantastic Beasts and Where to Find Them' opens in 1926 as Newt Scamander has just completed a global excursion to find and document an extraordinary array of magical creatures. Arriving in New York for a brief stopover, he might have come and gone without incident... were it not for a No-Maj (American for Muggle) named Jacob, a misplaced magical case, and the escape of some of Newt's fantastic beasts, which could spell trouble for both the wizarding and No-Maj worlds." title="'Fantastic Beasts and Where to Find Them' opens in 1926 as Newt Scamander has just completed a global excursion to find and document an extraordinary array of magical creatures. Arriving in New York for a brief stopover, he might have come and gone without incident... were it not for a No-Maj (American for Muggle) named Jacob, a misplaced magical case, and the escape of some of Newt's fantastic beasts, which could spell trouble for both the wizarding and No-Maj worlds." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3183660/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466627422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3" > Fantastic Beasts and Where to Find Them </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466627422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','HeroWidget',{wb:1});}
                if(typeof uet === 'function'){uet("cf");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="http://www.imdb.com/video/imdb/vi3527587097/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467762722&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_gr_hd" > <h3>Patrick Stewart Is a Very Bad Guy in 'Green Room'</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt4062536/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467762722&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_gr_i_1" > <img itemprop="image" class="pri_image" title="Green Room (2015)" alt="Green Room (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjU1ODQ5NzA0N15BMl5BanBnXkFtZTgwMDg5MTA5NzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjU1ODQ5NzA0N15BMl5BanBnXkFtZTgwMDg5MTA5NzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3527587097?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467762722&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_gr_i_2" data-video="vi3527587097" data-source="bylist" data-id="ls036252428" data-rid="05248M3GAZTCVZAAM13E" data-type="playlist" class="video-colorbox" data-refsuffix="hm_vd_gr" data-ref="hm_vd_gr_i_2"> <img itemprop="image" class="pri_image" title="Green Room (2015)" alt="Green Room (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjg3MjA2NzI1OF5BMl5BanBnXkFtZTgwMTkxMzMyODE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjg3MjA2NzI1OF5BMl5BanBnXkFtZTgwMTkxMzMyODE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Green Room (2015)" title="Green Room (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Green Room (2015)" title="Green Room (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Watch our exclusive video of <a href="/name/nm0001772/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467762722&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_gr_lk1">Patrick Stewart</a>, who discusses his role in <a href="/title/tt4062536/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467762722&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_gr_lk2"><i>Green Room</i></a> and what attracted him to the script. Stewart has earned rave reviews for his performance as Darcy Banker, a man who will do anything to protect the secrets of his nefarious enterprise.</p> <p class="seemore"><a href="http://www.imdb.com/video/imdb/vi3527587097/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467762722&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_gr_sm" class="position_bottom supplemental" >Learn more about Patrick Stewart's attraction to the role</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
                if(typeof uet === 'function'){uet("af");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/scary-good/our-favorite-vampires/ls015374354?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467122842&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_vam_hd" > <h3>Our Favorite Vampires</h3> </a> </span> </span> <p class="blurb">Movie and TV audiences have long been fascinated by mythical bloodsuckers. From <a href="/title/tt0068505/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467122842&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_vam_lk1">Count Dracula</a> and <a href="/title/tt1139797/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467122842&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_vam_lk2">Eli</a> to <a href="/title/tt1405406/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467122842&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_vam_lk3">Damon Salvatore</a>, we run down some famous and lovable leeches.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/scary-good/our-favorite-vampires/ls015374354?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467122842&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_vam_i_1#image1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk5MTg4NjAxMV5BMl5BanBnXkFtZTgwOTUzODU5NTE@._UY402_CR100,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk5MTg4NjAxMV5BMl5BanBnXkFtZTgwOTUzODU5NTE@._UY402_CR100,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/scary-good/our-favorite-vampires/ls015374354?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467122842&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_vam_i_2#image2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTMwMDM2MjU2N15BMl5BanBnXkFtZTcwNjc3MjUzNA@@._SX800_CR340,60,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMwMDM2MjU2N15BMl5BanBnXkFtZTcwNjc3MjUzNA@@._SX800_CR340,60,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/scary-good/our-favorite-vampires/ls015374354?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467122842&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_vam_i_3#image3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk0MzEwNzA3MF5BMl5BanBnXkFtZTcwMDg4NDg4Ng@@._SX900_CR320,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0MzEwNzA3MF5BMl5BanBnXkFtZTcwMDg4NDg4Ng@@._SX900_CR320,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/scary-good/our-favorite-vampires/ls015374354?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467122842&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_vam_sm" class="position_bottom supplemental" >Get a closer look at Dracula, Eli, Damon, and more</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59706443?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk1MjM3NTU5M15BMl5BanBnXkFtZTcwMTMyMjAyMg@@._V1_SY150_CR6,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59706443?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Tom Cruise Promises âIncredible Set Piecesâ for âMission: Impossible 6â</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> Last year, <a href="/name/nm0000129?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Tom Cruise</a> took CinemaCon by storm, keeping theater owners enraptured as he spun tales of how he risked life and limb by strapping himself to the side of an airplane on â<a href="/title/tt2381249?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Mission: Impossible â Rogue Nation</a>.â The actor behind the hit spy series couldnât make it to Las Vegas this go-round....                                        <span class="nobr"><a href="/news/ni59706443?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59705884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Barbra Streisand, Barry Levinson Reviving âGypsyâ for STX Entertainment (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59704745?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >âFast 8â Creates New Character; Scott Eastwood To Star</a>
    <div class="infobar">
            <span class="text-muted">11 April 2016 5:27 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59706378?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Brad Pittâs World War II Movie âAlliedâ Set for Nov. 23 Release</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59705245?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >âJungle Book 2â in the Works at Disney With Director Jon Favreau</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59706561?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjA1MjE2MTQ2MV5BMl5BanBnXkFtZTcwMjE5MDY0Nw@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59706561?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >No âSilence,â But Paramount Still Boasts Prestige Power at CinemaCon</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> Las Vegas â The oddest note in <a href="/company/co0023400?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Paramount Pictures</a>â CinemaCon presentation Monday night, kicking off four days of footage reveals and seminars at the annual motion picture exhibitors trade show, was the exclusion of <a href="/name/nm0000217?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Martin Scorsese</a>âs â<a href="/title/tt0490215?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Silence</a>.â The Oscar-winning directorâs latest film, expected to ...                                        <span class="nobr"><a href="/news/ni59706561?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59706443?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Tom Cruise Promises âIncredible Set Piecesâ for âMission: Impossible 6â</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59706084?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Steven Spielberg Sets âThe Kidnapping Of Edgardo Mortaraâ Next; Tony Kushner Script, Mark Rylance Is Pope Pius IX</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59704618?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >âSuicide Squadâ Director David Ayer Calls Reshoot Rumor âSillyâ</a>
    <div class="infobar">
            <span class="text-muted">11 April 2016 4:56 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59706378?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Brad Pittâs World War II Movie âAlliedâ Set for Nov. 23 Release</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59707283?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTU0NzUxNTA0Ml5BMl5BanBnXkFtZTgwMzQxMTc1ODE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59707283?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >âFear The Walking Deadâ Season 2 Debut Ratings Steady With Season 1 Finale</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Deadline TV</a></span>
    </div>
                                </div>
<p>Now that the action has headed to the high seas, <a href="/title/tt3743822?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Fear The Walking Dead</a> may well be called Fear The Wading Dead for awhile â especially when you look at the AMC series' ratings. Debuting for its second season on April 10, <a href="/title/tt1520211?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">The Walking Dead</a> spinoff snagged 6.7 million total viewers with 3.9 million of...                                        <span class="nobr"><a href="/news/ni59707283?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59707513?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >âMan Seeking Womanâ Renewed for Season 3 on Fxx</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59706964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >âWalking Deadâ Showrunner Says Season 6 Finale Was âNever Intended to Fâ With Peopleâ</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59705600?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Showtime Cancels âEpisodesâ After 5 Seasons</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59705887?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Louis C.K. âMillions of Dollarsâ in Debt Over âHorace & Peteâ</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59707364?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTUwMTM5Njg4OV5BMl5BanBnXkFtZTgwMjM4NDA2ODE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59707364?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >David Gest, Music Producer & Reality TV Star, Found Dead In London Hotel</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>Deadline</a></span>
    </div>
                                </div>
<p>Music producer and reality TV star <a href="/name/nm1205836?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">David Gest</a>, the onetime husband of <a href="/name/nm0591485?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Liza Minnelli</a>, has been found dead in his London hotel room. Gest was 62. His death was confirmed in a statement from friend Imad Handi. âIt is with great sadness that I can confirm that <a href="/name/nm1205836?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk3">David Gest</a> has died today,â the statement ...                                        <span class="nobr"><a href="/news/ni59707364?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59706923?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Susan Sarandon on Where Her Thelma & Louise Character Would Be Today: 'Maybe Louise Became a Lesbian, That Would Be Fabulous'</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59705887?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Louis C.K. âMillions of Dollarsâ in Debt Over âHorace & Peteâ</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59706436?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Megan Fox Opens Up About Motherhood After Debuting Baby Bump on Red Carpet</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59705860?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Scarlett Johansson Just Revealed What May Be the No. 1 Reason Celebrity Couples Fail</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NewsDeskWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/game-of-thrones-season-6-premiere/rg2076416768?imageid=rm1245977600&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466892802&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Alfie Allen, Iwan Rheon and Sophie Turner at event of Game of Thrones (2011)" alt="Alfie Allen, Iwan Rheon and Sophie Turner at event of Game of Thrones (2011)" src="http://ia.media-imdb.com/images/M/MV5BMjA0OTc5MjU5OF5BMl5BanBnXkFtZTgwODQ5NTk1ODE@._V1_SY201_CR20,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0OTc5MjU5OF5BMl5BanBnXkFtZTgwODQ5NTk1ODE@._V1_SY201_CR20,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/game-of-thrones-season-6-premiere/rg2076416768?imageid=rm1245977600&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466892802&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1" > "Game Of Thrones" - Season 6 Premiere </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm542645248/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466892802&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="The Secret Life of Pets (2016)" alt="The Secret Life of Pets (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTY1Mjk1NTIxM15BMl5BanBnXkFtZTgwNzEwNzk1ODE@._V1_SY201_CR30,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY1Mjk1NTIxM15BMl5BanBnXkFtZTgwNzEwNzk1ODE@._V1_SY201_CR30,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm542645248/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466892802&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2" > <i>The Secret Life of Pets</i> Exclusive Standee and Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm742595584/rg2126748416?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466892802&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Still of Lauren Graham and Scott Patterson in Gilmore Girls: Seasons (2016)" alt="Still of Lauren Graham and Scott Patterson in Gilmore Girls: Seasons (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjE5NDA2MzY0MF5BMl5BanBnXkFtZTgwMzI0NTk1ODE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE5NDA2MzY0MF5BMl5BanBnXkFtZTgwMzI0NTk1ODE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm742595584/rg2126748416?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466892802&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3" > "Gilmore Girls" - First Look at the Revival </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=4-12&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1519680?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Saoirse Ronan" alt="Saoirse Ronan" src="http://ia.media-imdb.com/images/M/MV5BMjExNTM5NDE4NV5BMl5BanBnXkFtZTcwNzczMzEzOQ@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjExNTM5NDE4NV5BMl5BanBnXkFtZTcwNzczMzEzOQ@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1519680?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Saoirse Ronan</a> (22) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:112px;height:auto;" > <div style="width:112px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0607185?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Jennifer Morrison" alt="Jennifer Morrison" src="http://ia.media-imdb.com/images/M/MV5BMTI0OTI2MTY0OF5BMl5BanBnXkFtZTcwNTExMTM5MQ@@._V1_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI0OTI2MTY0OF5BMl5BanBnXkFtZTcwNTExMTM5MQ@@._V1_UY340_UX224_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0607185?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Jennifer Morrison</a> (37) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000132?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Claire Danes" alt="Claire Danes" src="http://ia.media-imdb.com/images/M/MV5BMTMyMzQ1Mjk3M15BMl5BanBnXkFtZTcwNzk3ODMxNw@@._V1_SY172_CR8,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMyMzQ1Mjk3M15BMl5BanBnXkFtZTcwNzk3ODMxNw@@._V1_SY172_CR8,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000132?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Claire Danes</a> (37) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm2395937?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Brooklyn Decker" alt="Brooklyn Decker" src="http://ia.media-imdb.com/images/M/MV5BMTg3OTk0MzMwMl5BMl5BanBnXkFtZTcwMjQ2NDg0Nw@@._V1_SY172_CR8,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3OTk0MzMwMl5BMl5BanBnXkFtZTcwMjQ2NDg0Nw@@._V1_SY172_CR8,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm2395937?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Brooklyn Decker</a> (29) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1770256?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Paul Rust" alt="Paul Rust" src="http://ia.media-imdb.com/images/M/MV5BOTA2NzIyMDYxMF5BMl5BanBnXkFtZTcwODUwMTc3Mg@@._V1_SY172_CR10,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTA2NzIyMDYxMF5BMl5BanBnXkFtZTcwODUwMTc3Mg@@._V1_SY172_CR10,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1770256?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Paul Rust</a> (35) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=4-12&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BornTodayWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/scary-good/faces-of-contagion/rg1740806912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466569242&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_foc_hd" > <h3>The Faces of On-Screen Contagion</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/scary-good/faces-of-contagion/rg1740806912?imageid=rm1911611904&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466569242&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_foc_i_1" > <img itemprop="image" class="pri_image" title="Still of Greg Nicotero in The Walking Dead (2010)" alt="Still of Greg Nicotero in The Walking Dead (2010)" src="http://ia.media-imdb.com/images/M/MV5BODExMzU4NjA0OV5BMl5BanBnXkFtZTgwODY0NjgwNzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODExMzU4NjA0OV5BMl5BanBnXkFtZTgwODY0NjgwNzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/scary-good/faces-of-contagion/rg1740806912?imageid=rm264799488&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466569242&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_foc_i_2" > <img itemprop="image" class="pri_image" title="Still of Claudia Silva in [Rec] (2007)" alt="Still of Claudia Silva in [Rec] (2007)" src="http://ia.media-imdb.com/images/M/MV5BMTQwOTk2NzAyOV5BMl5BanBnXkFtZTcwODI2MTAzNA@@._V1_SY201_CR33,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQwOTk2NzAyOV5BMl5BanBnXkFtZTcwODI2MTAzNA@@._V1_SY201_CR33,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/scary-good/faces-of-contagion/rg1740806912?imageid=rm3687511040&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466569242&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_foc_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTUxNTg4Mjk5M15BMl5BanBnXkFtZTgwNjk2NTA3NTE@._SX800_CR110,12,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUxNTg4Mjk5M15BMl5BanBnXkFtZTgwNjk2NTA3NTE@._SX800_CR110,12,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Be lucky you don't have what's ailing these hapless folks! Check out some scary good photos from our favorite contagion-themed titles over the years.</p> <p class="seemore"><a href="/scary-good/faces-of-contagion/rg1740806912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466569242&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_foc_sm" class="position_bottom supplemental" >View more faces of on-screen contagion</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Indie Focus: '3rd Street Blackout' Literally a Dark Comedy</h3> </span> </span> <p class="blurb">In this romantic comedy, a technology-obsessed couple is forced to communicate without the Internet during a blackout.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3520008/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466233342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1" > <img itemprop="image" class="pri_image" title="Jeremy Redleaf and Negin Farsad in 3rd Street Blackout (2015)" alt="Jeremy Redleaf and Negin Farsad in 3rd Street Blackout (2015)" src="http://ia.media-imdb.com/images/M/MV5BYTVhYmVjYTctNWM5MS00ZDUzLWI1NzAtYzlkMzkxOTZlZGQzXkEyXkFqcGdeQXVyNzg2NDkwMw@@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BYTVhYmVjYTctNWM5MS00ZDUzLWI1NzAtYzlkMzkxOTZlZGQzXkEyXkFqcGdeQXVyNzg2NDkwMw@@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2102572313?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466233342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2" data-video="vi2102572313" data-rid="05248M3GAZTCVZAAM13E" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="3rd Street Blackout (2015)" alt="3rd Street Blackout (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTU1OTIxNjUzNl5BMl5BanBnXkFtZTgwMTgyMDk1ODE@._V1_SY250_CR71,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU1OTIxNjUzNl5BMl5BanBnXkFtZTgwMTgyMDk1ODE@._V1_SY250_CR71,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="3rd Street Blackout (2015)" title="3rd Street Blackout (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="3rd Street Blackout (2015)" title="3rd Street Blackout (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-9"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg1271700224?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466572682&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_sh_hd" > <h3>TV Spotlight: Latest "Agents of S.H.I.E.L.D." Photos</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3489602560/rg1271700224?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466572682&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_sh_i_1" > <img itemprop="image" class="pri_image" title="Still of Powers Boothe and Bethany Joy Lenz in Agents of S.H.I.E.L.D. (2013)" alt="Still of Powers Boothe and Bethany Joy Lenz in Agents of S.H.I.E.L.D. (2013)" src="http://ia.media-imdb.com/images/M/MV5BMTA3NDU2NjkzMzNeQTJeQWpwZ15BbWU4MDI4NjE0NTgx._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA3NDU2NjkzMzNeQTJeQWpwZ15BbWU4MDI4NjE0NTgx._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3422493696/rg1271700224?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466572682&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_sh_i_2" > <img itemprop="image" class="pri_image" title="Still of Luke Mitchell and Chloe Bennet in Agents of S.H.I.E.L.D. (2013)" alt="Still of Luke Mitchell and Chloe Bennet in Agents of S.H.I.E.L.D. (2013)" src="http://ia.media-imdb.com/images/M/MV5BMzIzMjg0MTc0NF5BMl5BanBnXkFtZTgwNTg2MTQ1ODE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzIzMjg0MTc0NF5BMl5BanBnXkFtZTgwNTg2MTQ1ODE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg1271700224?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2466572682&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_sh_sm" class="position_bottom supplemental" >See more photos from "Agents of S.H.I.E.L.D."</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-11"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_hd" > <h3>Now Trending on Amazon Video</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYjDjQfaiRWT7UUn_ryki5xVBMWREcWKcdYGXu1izFYL6XCdIt2sUp3MlBLacsHqqSeNJT3D6Tr%0D%0AaW_v9gmyQO1SF_nlvvTpz5RcAcq0oz--o6uvoD2_82kNwkZRGa7IfdUUf3fkV1CpUMrabC-H6Bot%0D%0AnR_IqCRL_gnWzgr-xaSg26bU0O_bgoW_TuqnytFzNdawsb0XIK-WFKWakizTd5Z1XCYkCKpyrkVI%0D%0At_5bZd_JMUP6XL7NfKyPlH8_oJT1W0FoSJKo9lCpeKX2fMJT7h6ZlA%0D%0A&ref_=hm_aiv_i_1" > <img itemprop="image" class="pri_image" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" src="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYmqdAAk0xq0nX2Gkk2GMaxKoCnkLL25E013wB77cxfsDV76w0VKDJuh4M5uaRikBi87QsIWWIl%0D%0AH_e2YXMARFVRpX9yi9KXoIg0bhl427DgTJsEKyM8dG8Q6aCqkO6tqgxbVR9RL5TqoHm_wyyYTT3N%0D%0AueykAIpKYsBlHfQy4XHHO-o0aQ0WrzHgIiln1krV3s55QG_aVEywNaS9rXaZfWQtsrDwYcp_OTsI%0D%0AAAIP30VRS4jlbAmzPAWM-LpfCGTY6BGqWQVsD1wBL96AP5ADbNvMsA%0D%0A&ref_=hm_aiv_cap_pri_1" > <i>Star Wars: The Force Awakens</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYkr2AWOsXiDY18dBr-3ocxZpDfSnGBXKIlDUIcsQqqETSdBp6IDMOj9v5bqGWtgmWbeAnlHyG-%0D%0AdE-fvq_hxcMcHmlwr04td-x_9MS6jWwm11YiXYQWISqDeaaprpEgr8Z_-S2O-O3Dw5A8pl8Dk31I%0D%0AdLefrh7xs-defDsHdODrOxRLpCn5JgMtGXSGH4r8CoiLzBVyCv9QFO-k9zZlfBdGbirHvjAw0E1C%0D%0AU5k0sDVOIfkKgyEDTP-8OWwFRMkj6HoY2w5Zp7m3vR1LvqE-44M85A%0D%0A&ref_=hm_aiv_i_2" > <img itemprop="image" class="pri_image" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYhYSXly1vddX-DAen7Po52oyHRGLX3BZ2CZUD5aiq0eFCyqaBcKRvTZffomun_rvmiYgprJHRS%0D%0AM0LtpU44vl4Du1sOHcyn-m_sPGGu6zFe1OuOIvlkwDZ7qbkwIkJ6bd9_hi1mHsvjid3HJCBZR9ZH%0D%0Ac4ofmDHEf0Ig2R24PDEaVih1ntOKhpCZxlaRp8vkSDBTOZ5PwZfMeGYRbzuyktEgOa479OUSietl%0D%0AsiGa1d3SR_m9pj1SjcbbrpSBqRxodwD2znb5Vu_w8A8ESwRRUANoXg%0D%0A&ref_=hm_aiv_cap_pri_2" > <i>The Hunger Games: Mockingjay Part 2</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYg3g_YfBMz2D-9G95dCxQGdqdewhqSlMBZPCdZlZVzcHKm1gXdrT_03uMXOYVnccl5Q366vGWh%0D%0AEeQ7rGt4vvuiZ2knBRE5bkglV0lIJRez0RQFTH1rYPQkSxaokpGPE15zRkKPxx4pU5Q27ePl1FfZ%0D%0AmKDH4AbPKtAcxrQWBW8UIeHIX8smj7flwaCirpmw7tpv--ip8uMh5v0uJMRLUv6SPjK4UrqbKvEF%0D%0AUEN-mgzdu-1yAD3iModUsxnsifz6dyVWO81HD5TzrJ6MaYC4da3B1Q%0D%0A&ref_=hm_aiv_i_3" > <img itemprop="image" class="pri_image" title="Keri Russell and Matthew Rhys in The Americans (2013)" alt="Keri Russell and Matthew Rhys in The Americans (2013)" src="http://ia.media-imdb.com/images/M/MV5BMTg3Njk0MTAwMF5BMl5BanBnXkFtZTgwMTUyNDM3NzE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3Njk0MTAwMF5BMl5BanBnXkFtZTgwMTUyNDM3NzE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Keri Russell and Matthew Rhys in The Americans (2013)" title="Keri Russell and Matthew Rhys in The Americans (2013)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Keri Russell and Matthew Rhys in The Americans (2013)" title="Keri Russell and Matthew Rhys in The Americans (2013)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYil6xRO-CMqe_fTS13s1U29z_vLFqDCnJynLiUXrxop-HYxlufmdajn2CAbI1AQ0skubLl77NW%0D%0AHrOrmVP56NUMtf0Appi3_VmB9UQXv6-ZyeiUH906jgP-6-ztc_ydW-88ii-AkFHe09xoCvZu-ttF%0D%0Ajz95A6yFnvmnnzsFQMuz45LlLskSf7oPCfCGC6pLU1tlX9U3mmF5wmwPF5TJzQ4rB-oTe6xFw0Q6%0D%0AzSKQYiLuxNKj3GNE2GCBqDuUm__aF6jxtHc3xfvJ4UlDQiq55n25Yg%0D%0A&ref_=hm_aiv_cap_pri_3" > "The Americans - Season 4" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYmfVF51ZTIsjlPNdGwweFE8s8vnbq2hgj6GcV3g-u_-q7V5WqRwthulGz5vp-hqjEtx6VE_jFa%0D%0ANhKz2MDA4FBJJvxGELddjAmCa3QqLKhCGnlrWVnIufkrjmeRSHs9A3PFtJwMOETUh0Db0JfKofoS%0D%0AhjuAogPt2dycoTGmHzMb4CI8IHIoOq5BemGU40QtSejgbiYirfBe_La59iM0AJT8TlWPI9dknGm2%0D%0AyyPwcJeQiD42BYSNeQbCmDsWGN-az2FHLz9W1RzCqtbhpt-UDDkdww%0D%0A&ref_=hm_aiv_i_4" > <img itemprop="image" class="pri_image" title="Cuba Gooding Jr. in American Crime Story (2016)" alt="Cuba Gooding Jr. in American Crime Story (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTc4Mzg0MzMwMF5BMl5BanBnXkFtZTgwNzQ3NTY3NzE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc4Mzg0MzMwMF5BMl5BanBnXkFtZTgwNzQ3NTY3NzE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Cuba Gooding Jr. in American Crime Story (2016)" title="Cuba Gooding Jr. in American Crime Story (2016)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Cuba Gooding Jr. in American Crime Story (2016)" title="Cuba Gooding Jr. in American Crime Story (2016)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYuJVPAaRHi4u9AAQGB5Lvz29eUsMgrM1HqA-Qut7V_K-LNYyTsnx6eSw2dn5cUR4GzAAUVkyPO%0D%0AN-82eHq0tYMVdJ9VVt2Cej6ZlCiTaG27-4VnFESq14258s6n0A5yNprNcsURiLfy2N6eE0YYl7iy%0D%0AXsHUuBNjcNL0PE9ndfjthW0iPod-Lp2u4Lj_R0MrxxbUy1MqFdM6F_r6Cl0IFw-0oVzmXN-6pil5%0D%0A2h2hBX61_6Lpm30Kgfq1eI1vnFdlbL2UymrIZmsEm_r9sSuYtbN-iw%0D%0A&ref_=hm_aiv_cap_pri_4" > "The People v. O.J. Simpson: American Crime Story" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYmktvSI3FOXx1AECfHVXV5pESjAnZ_pgcVN34ewSfbjG4COFh4uCZ1EjjlB0LvmVWRDoDAd5BH%0D%0AOykjie2OUio1VVtPXP8txCuNrkTFtiOnWwhbPJG-b3wqcYGueDERTAdNtudnxV_lnGjkb-vEpVnF%0D%0ArQUX3P96K42_Fu5M64oBzMriGbbe2wtHt1xB-_ofdVoHjQjynKjiNUs9lefWuMlQC5BVyQHauglF%0D%0AhJF9MF3LApk_bfuf_2gVVkwtfmckucySNEPU28rKmSnpW1YnAvSTKQ%0D%0A&ref_=hm_aiv_i_5" > <img itemprop="image" class="pri_image" title="Leonardo DiCaprio in The Revenant (2015)" alt="Leonardo DiCaprio in The Revenant (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Leonardo DiCaprio in The Revenant (2015)" title="Leonardo DiCaprio in The Revenant (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Leonardo DiCaprio in The Revenant (2015)" title="Leonardo DiCaprio in The Revenant (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYkfVIvh_VzsGvdJzWVpNfvc8NVcYzFm0rXwjdrgEEZ-04yIpRSpMAp0gyo9Gnl3JZzaFgM4DTv%0D%0Ab9axbXRF8dhX2L2L_tjhZN1OytxRcDSfGy7Pcfy2dSOOJEPBdovT5sp9xZy4n9gOcY5L7lSAIkSf%0D%0AkzmXAwafM_toKnJyEEbagY5H8SVkzsCP0IGjZnyk8uq094L71OoeEntKttpk-XhAbgpx8BY-sQX6%0D%0A0s3ocMuCfRmCZO0gfr_Cdm4bbp-3wNbkzs4DXs1Px8AHsiW8fH9lNw%0D%0A&ref_=hm_aiv_cap_pri_5" > <i>The Revenant</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">While <i>Star Wars: Episode VII - The Force Awakens</i> continues to top the charts on Amazon Video, several TV series are now trending as well, including "The Americans - Season 4" and "The People v. O.J. Simpson: American Crime Story."</p> <p class="seemore"><a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465995342&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_sm" class="position_bottom supplemental" >See which other movies and shows are trending on Amazon Video</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','RecsWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt1179933/trivia?item=tr2827008&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1179933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="ÐÐ»Ð¾Ð²ÐµÑÑÐ¸Ð»Ð´, 10 (2016)" alt="ÐÐ»Ð¾Ð²ÐµÑÑÐ¸Ð»Ð´, 10 (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjEzMjczOTIxMV5BMl5BanBnXkFtZTgwOTUwMjI3NzE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzMjczOTIxMV5BMl5BanBnXkFtZTgwOTUwMjI3NzE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt1179933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">10 Cloverfield Lane</a></strong> <p class="blurb">In one scene, Howard (<a href="/name/nm0000422?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">John Goodman</a>) is watching <a href="/title/tt0091790?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk2">Pretty in Pink</a> (1986) and Emmett (<a href="/name/nm0302330?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk3">John Gallagher Jr.</a>) mistakes it for <a href="/title/tt0088128?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk4">Sixteen Candles</a> (1984). <a href="/name/nm0935541?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk5">Mary Elizabeth Winstead</a>, who plays protagonist Michelle here, plays a character in <a href="/name/nm0000233?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk6">Quentin Tarantino</a>'s <a href="/title/tt1028528?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk7">Death Proof</a> (2007) who exclaims that Pretty in Pink is one of her favorite movies.</p> <p class="seemore"><a href="/title/tt1179933/trivia?item=tr2827008&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TriviaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;Zq6egvxpGxM&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/Zq6egvxpGxM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd" > <h3>Poll: Face-Off: Iron Man vs. Armored Batman</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:49%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Zq6egvxpGxM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="Still of Robert Downey Jr. in The Avengers (2012)" alt="Still of Robert Downey Jr. in The Avengers (2012)" src="http://ia.media-imdb.com/images/M/MV5BMjMwMzM2MTg1M15BMl5BanBnXkFtZTcwNjM4ODY3Nw@@._V1_SY462_CR255,0,311,462_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMwMzM2MTg1M15BMl5BanBnXkFtZTcwNjM4ODY3Nw@@._V1_SY462_CR255,0,311,462_AL_UY924_UX622_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:49%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Zq6egvxpGxM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Still of Ben Affleck in Batman v Superman: Dawn of Justice (2016)" alt="Still of Ben Affleck in Batman v Superman: Dawn of Justice (2016)" src="http://ia.media-imdb.com/images/M/MV5BZjZmNmIyOTMtYmFlMy00Y2QzLTk5NWMtM2EwN2Q2ODk4YmYxXkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SY462_CR213,0,311,462_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BZjZmNmIyOTMtYmFlMy00Y2QzLTk5NWMtM2EwN2Q2ODk4YmYxXkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SY462_CR213,0,311,462_AL_UY924_UX622_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Two suave billionaires strapped in high-tech metal gear. Who do you think would win in a fight?<br /><br />Discuss <a href="http://www.imdb.com/board/bd0000088/nest/254815361?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/Zq6egvxpGxM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','PollWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=834642195967;ord=834642195967?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript> <a href="http://pubads.g.doubleclick.net/gampad/jump?&iu=4215/imdb2.consumer.homepage/&sz=300x250|300x600|11x1&t=p%3Dtr%26fv%3D1%26ab%3Dc%26bpx%3D2%26c%3D0%26s%3D3075%26s%3D32&tile=1&c=834642195967" target="_blank"> <img src="http://pubads.g.doubleclick.net/gampad/ad?&iu=4215/imdb2.consumer.homepage/&sz=300x250|300x600|11x1&t=p%3Dtr%26fv%3D1%26ab%3Dc%26bpx%3D2%26c%3D0%26s%3D3075%26s%3D32&tile=1&c=834642195967" border="0" alt="advertisement" /> </a> </noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3040964"></div> <div class="title"> <a href="/title/tt3040964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> The Jungle Book </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt3040964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3628584"></div> <div class="title"> <a href="/title/tt3628584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Barbershop: The Next Cut </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3014866"></div> <div class="title"> <a href="/title/tt3014866?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Criminal </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4062536"></div> <div class="title"> <a href="/title/tt4062536?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Green Room </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3495026"></div> <div class="title"> <a href="/title/tt3495026?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Fan </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3544112"></div> <div class="title"> <a href="/title/tt3544112?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Sing Street </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1456606"></div> <div class="title"> <a href="/title/tt1456606?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Rio, I Love You </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4937156"></div> <div class="title"> <a href="/title/tt4937156?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Ultime tango </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4005402"></div> <div class="title"> <a href="/title/tt4005402?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t8"> Colonia </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2702724"></div> <div class="title"> <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Boss </a> <span class="secondary-text">Weekend: $23.6M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2975590"></div> <div class="title"> <a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Batman v Superman: Dawn of Justice </a> <span class="secondary-text">Weekend: $23.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2948356"></div> <div class="title"> <a href="/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Zootopia </a> <span class="secondary-text">Weekend: $14.3M</span> </div> <div class="action"> <a href="/showtimes/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3760922"></div> <div class="title"> <a href="/title/tt3760922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> My Big Fat Greek Wedding 2 </a> <span class="secondary-text">Weekend: $6.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt3760922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3072482"></div> <div class="title"> <a href="/title/tt3072482?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Hardcore Henry </a> <span class="secondary-text">Weekend: $5.1M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BoxOfficeListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381991"></div> <div class="title"> <a href="/title/tt2381991?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> The Huntsman: Winter's War </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2093991"></div> <div class="title"> <a href="/title/tt2093991?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Elvis & Nixon </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2980210"></div> <div class="title"> <a href="/title/tt2980210?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> A Hologram for the King </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3278330"></div> <div class="title"> <a href="/title/tt3278330?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Tale of Tales </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3367294"></div> <div class="title"> <a href="/title/tt3367294?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Compadres </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src="http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3626940295._CB298540565_.html?config=%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D"></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TwitterWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','FacebookWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/scary-good/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467703682&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_rhs_hd" > <h3>Scary Good: IMDb's Guide to Horror</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/scary-good/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467703682&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_rhs_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjA0ODIzMTcxN15BMl5BanBnXkFtZTgwNjU5MDE1MTE@._UX480_CR35,60,307,230_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0ODIzMTcxN15BMl5BanBnXkFtZTgwNjU5MDE1MTE@._UX480_CR35,60,307,230_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Can't get enough of movies and television shows that scare up a good fright? Check out <a href="/scary-good/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467703682&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_rhs_lk1">Scary Good</a>, IMDb's Horror Entertainment Guide. Being terrified was never so much fun.</p> <p class="seemore"><a href="/scary-good/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467703682&pf_rd_r=05248M3GAZTCVZAAM13E&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_rhs_sm" class="position_bottom supplemental" >Enter if you dare</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYtwY5xDDpklY0K4FO-cddD7Do3XfwG2qS1MlDqOf7auaHwTlUvKYBk9HPQHxs62UF_H8vZCREf%0D%0AbE_1oPFumgmBnuZuBZq_V9urIobyIJVpBkXq6dIaH2j5lX6LQzBfnANPPQ0O9D23OlhlRAxhooek%0D%0AEb_mmQoIPGNodSjF-aXeLqt4iSMbcgVj_SCRYft-7VEZwheoupEV5Gsj1vTJz0IuWqJwpq4yDzoW%0D%0A1r7frfkyjuU%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYubXRA6Bs2yDAuPhMoyXT14FTSUS7BYfqcwZ4sNdYw8_8lpOrO3DHX_utC_MvBaegTPP9nAbJ2%0D%0A-bitPz7kuJSPhYE__Fa2eqNn3ltWrv3GgguWnT2ZLg1iGeE6wlIwSw5nFJv7gqazeqfyqfX3sowJ%0D%0ANYmuoedGqpt4whEjExjM_bjk2l4p7chSykj4eDc8ovD-PV04o7l2on2xDWex9z4emw%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYoPzwyt55ZEmbswsKWkGt12060NIU_vqNqhy2yIBi8LrHFDvPJTEPWABW1gIiuhAyzFCegztBc%0D%0ACClft8h1nLDfaVuBinj4jzBuzb_SFOx3J5yb5ckPbHP0IXKjn7GhkOYZGFskpmW0wduuWbtQ6v-l%0D%0A7nhCcHq33SVoxE9yhC6GYuwu-ENMjWnWVbe7IpKF61cSB-Xo3eXxndUs79yGS_WyTJ7y8ZR_Lh54%0D%0AJSv7m8Be-ct5F_QEJi4sDAajtUS7XQ7P3RJs52hvwE-eLHVxVywFVb4j4SMB1G7xGSKmTyQBjuqs%0D%0AM9sIIOwSPSau4wltDcmrbIyvMK-f3L_nBKtqMAqVy4NM_SqPqZhWv1UT-sZNnhxv1co-aUFWQjLg%0D%0Ai-c5sFrgdMKyfICVM5luNr4CqeMU3aHndhxr98U-gStKDtzdAZ4%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYof1EY-RpSqeEee7yn-7uBL33N93F8d-PJrQPkF6_T5RNjrIGWFrU_LuAX_miZKy-x5RDDA5H5%0D%0ALcq7e1-3Kv6g5Jc9auVVEH8DqDdJpsWvRhclAIvCSd14SZRIhm3GVpNn60mf1xIq-LFt4T1zi814%0D%0ANAfAIW4jSghzEYCZOYwWT2_ez_2QsFr7FAuW3YqRYBhhqg2GC0GsTcteOLaH71sm1Q%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYuraa9Ln1CKh5V9jQzMkdklBmCZvYg-1ikd3hTebsM0QIEPhbYDkasrxZ0qRSejgiPkEhdDPNv%0D%0A8chdz0XknZMlm6cEQH4g2iIJG0jeuGZdIrTxpWKzjP_6agqAfdahg9nwOYehgTNALrsU8lqxFIci%0D%0ANPhp7oobHm8_GcLPs2_BYGvy2X9-zfY7o8H-0RPfJdpw%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYhKCM4MOsBHz0ZC6ApRKSg8QeiOOEHEJN3vOzpQUKeuZjdlFIUUbwW5quNLBIpmVTJxre1aZ69%0D%0AgiU3r-9HdKZ1jaWnGNxqG4PA-9t2TCzPse8bVHezQwqVr4y93wBabaJgLQsxm2vsWP089c_fvfNB%0D%0A4M9G_wIlr0PzW3Q4CtkcCycuohTDXeny4RwNkOM4c8X1%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYkA8fuViL2Thcr2z6P-Lj74gLfCKOYXO9UofyFi611gRiaqyAhUCq5F96nefUX72bUv18qOZov%0D%0ANgSfHrAmPItRQbQnjK6Iilhh4MIl3tgjJc9QVS6qTgTxHiQvDGaRFwFnXf3zuBhOCb0kmrtLYlF8%0D%0Au1CfuemqkfS6n7TZKp7GDSxkYUb-ymHhFut8iCzLm5JkBP5wOoADC42PiRWWCy86zA%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYgjBeOTlk7maL5Kp1SaN3C-2oa977Lln4wHkpyivrrL5zovjGGq3aBEgvzYfQ1J25j_A9D1fqz%0D%0Ab9ShZhZZiNK-tyxeXn9e7OjVyxX-hnz69poM4l2dYKnC9yYlY7b8MUlCn-kK8rbkqchoeuDO-4-N%0D%0Al1andKzX_-DMylFc5-77n-ivF3LwZA04F4gumZLpNN3X%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYqMMnIgEW2uoBh7ECX36BvZLGLyElc62O4Y92OFf4sdN3ErMJIOeamucTfVnyJfdtnX-3uueGk%0D%0AIKFmV9OaOlgWLM3zcFteGL3E-yq9cj0yt02jeO2s7iDjXIvulKwra4DWbvMYTbWrgAey3yaTJOBg%0D%0AC9V__mNLqFT89qEUTiZ4aHT4-RKqS4duVnJ72-pnkjG88iz0inwfTn646fnksyTjWMbTeKCza8L_%0D%0AKDNt8Agcyr0-JtGhMMwB7-0ZGnsyXhZn%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYt7Xv5ZGyqhzU7NZj4qJ0Np5y-1B7FZcjyJiczUWhP1efl_cFIu1c-6SwnnqC-uglqRgnhIdsP%0D%0AmQEjThOiOME6lXhNJMgVo4jpVNcHtY76PyiLSYMU4X6wjfMhzjSMR_1_e29qbOTVNiLjCd79yTYK%0D%0ABrNEDn2fUn5oMLm9w0r4-mC6gWX0ttE9CNLllgmb17w5Wbexa-Gmzp0A9QTyWz6zRq2KXWtLKZLd%0D%0A-YQ2Pp4FF0M%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYkH91Z4LgxmKOJ0ZCMXIURYZaGLCIsy35yHR3D5rt_CJS7kWQSK6g0fQxxeO2RyNw_w2bQ0Y6x%0D%0ACQHHfqusY-X_aTCZgmbLMGRtKRD_3SL_x4GeTtfVRuNugzj96ASFYLeFZNlcWIVUsVJWQnzyG2mw%0D%0AslsHM7ZIc98U00SH6mWE1DqP9fI_aB2QeX3nOqecUPAlljeOnhgKopGd2KIIUHccTRL0CngJZhRO%0D%0AHGCZhcP4yag%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYjV-1iE31huB5lsK7ggdj0FzbI8BXYEovrYpq8UCM-6fCQvOCjM10IKMUxAHNnLB_6yYfDQJT8%0D%0AMXRNgyC0zCPmkutK1Wthg8m2x_Y-p9TP_tPBkhWe4kWd2NT5rNrvxlDBjD2KV2UXaT9afImw-hoj%0D%0ArEjZPeuWh7tNH7az7O0rQELbE63-Z6dzUSJlYXGKQJPc4wXN6_yynNzv7wmLGY48Cw1_GbbUZDIR%0D%0A7w4yJEWvCDQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYmev1fySTUxg1BdWID1Q0i0O-euFaRVlo13LJrrfnCbQoDNYwQkFfCmrxhL-oaPFVyS-UP9fv9%0D%0A73oGz59HVaXPLWpY4X9LBMGsXdOGlywE3gRiKvVYW5uZDDkiaay1aZGOP5YVJpvEDx7SU6UPWnvm%0D%0AcnKtZ04oALbeIH47KaZge8l7Ex_GB6BFgejj3XN86zbNJulCh_QZ-1K168J_v_yOG1seHeBaFkCT%0D%0AE4x10NP5rHc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYmk2q-fdzHSma1LpZXVi5XuBVSz6iplTaLHmYHmzNHGMD--TU81u5G4GJMKw_N0LWoBX9bp585%0D%0AF2QsJwpbKxBzTSLzzNd8XdXV9gL6IFpIuQjoYddXESAMH5SbggbIxXFn22pluaZDe5BdvwsCU0-R%0D%0A-C8q9XQ0BaPFaZoj7vCb47IxuaPIuJZHChTzkNCJ34u1L-FlmlnufUZyO3BKM7Ck5hO300dDqny1%0D%0AoT-wiqOOkQY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYhaMykGIUqk55EJZOFcQiyCR4WF_z11Cv39-eRLp7NUJgBLubd6romAZjfBK07vI5c1Am5hnNC%0D%0AgctaPoeasYhBmFSiUGYe7OSFTT7BZ3L46n1s4hfv2Tjrf80mprA8oPmmgyWecRLcL4-ydDoowqIm%0D%0Akr_AO27tANH1-sZa7YjHIAXpKYwXf82zStHD8XuAKlliX2WNvam5WDeWWc5GkoXhcfJ0qx_mhcaL%0D%0ALBruo0vWq8GUqsZTGzpAwAAG1kHCEBKC%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYpKjW_FYFUX6yDxg5nIVuIiwYj_jvd-nigLsa4W3TXQzl2Yd3N8PhBmayRfQ3TpQrdfIs6tjvP%0D%0AWjAQtvK6qSO1X4lVNMpaofBY-WJw6OHs6RqGPaYLGB5wTMybwtOVdUnpyPsUI5XQmr9nbWcP0jQs%0D%0AFSjD8UQoyb18sT0fK_QfRUM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYqZ3ceHl-5rlagFpcR413GE5EbSvYWSk0dZ2gB5u0-FLzq7ZAdk3s4KiFI9rTXmmiC83oS7Zku%0D%0Ayu28CW8TrMWaJV0y4OB06P2wCCjRU0KamTNQzoglmTBkrJWF4acuuU9Cp4yAFE6XgAnwWDewygpU%0D%0A-gjNvJhPfUOeuGt2bgEqbx0%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-2997851314._CB293837866_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-3327433850._CB276437338_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101eb00f60f7cf47533c1bd40aab49bcbd9467fca5c87ce961c6eeda3d99d40733b",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=834642195967"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-458185442._CB294884543_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=834642195967&ord=834642195967";
    },"unable to request AAN pixel");
</script>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="579"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
