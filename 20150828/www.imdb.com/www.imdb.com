



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "340-7405635-6808687";
                var ue_id = "1Q0XBMB3TTZ2KAQTDEDJ";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1Q0XBMB3TTZ2KAQTDEDJ" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-608147c8.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2738789128._CB313281599_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['f'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['127391041471'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-3480509068._CB314075722_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"9fe741ec7d453368dcec7df4ed4bce4db8194373",
"2015-08-27T23%3A58%3A18GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 25302;
generic.days_to_midnight = 0.292847216129303;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'f']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=127391041471;ord=127391041471?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=127391041471?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=127391041471?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tp_tvpicks_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
>Popular Movies & TV</a></li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250 Movies</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                            <li><a href="/chart/toptv?ref_=nv_tvv_250_3"
>Top 250 TV</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_4"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_5"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_pks_2"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_3"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_4"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_5"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=08-27&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni58943657/?ref_=nv_nw_tn_1"
> âDoctor Strangeâ Eyes âHannibalâ Star Mads Mikkelsen to Play Villain (Exclusive)
</a><br />
                        <span class="time">4 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58942747/?ref_=nv_nw_tn_2"
> âGalaxy Questâ TV Series Takes Flight at Amazon
</a><br />
                        <span class="time">8 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58942759/?ref_=nv_nw_tn_3"
> Jason Reitman to Direct âPrincess Brideâ Live Read at Toronto Film Festival
</a><br />
                        <span class="time">8 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt2582802/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BNTc0NTU3NDg4Ml5BMl5BanBnXkFtZTgwOTM0NzQ5MTE@._UY340_CR80,10,410,315_HC_BR10_.jpg",
            titleYears : "2014",
            rank : 40,
                    headline : "Whiplash"
    },
    nameAd : {
            clickThru : "/name/nm0004266/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjA1NzkyNjAzNV5BMl5BanBnXkFtZTcwNzYyOTUxOQ@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 133,
            headline : "Anne Hathaway"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYouwaIzyWOODRq4W_xcBzB69YYg1xwmkBUD0RTit7osY1Lv6X1hDRA5MW6-MfIFe5J5rOlgFUn%0D%0AhhcJmXjGF4UnQQ4cq-gPApc8dG2PcJL8L4E-xUapt9LIYDlABSMVLeD7QZs4zN7tYhNXk76ZLlQj%0D%0AzSj9P-kSVrMs4NnI1USB2-UPwlOz6tfjVGd8dunJtJnQFa_Cd3_q-cyxHLgtPt_iHw%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYq-3R_VM4hRY0T9OdHaryHgj0vIBCJbzO_nAqcNd_Y09fg5PxNiGiaqhwcHHyVRH0lwnFNVFRm%0D%0AIqLwGmhTcQFPBk-WCfoAk22O3o7m_x-ghIydZD0V0ELfR7F0-gNnJ87ZXBGr33tSto3ZuPkkUROp%0D%0Ad-eBwdPbaNilOkmRjHv760o8XktDIGv4v_x3DeKLr3pr%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYt70lqxjUX83A8_D0dIMG_3uKOHvCeHSKgGaLMUukBz_D1cT7IwUVN8i9FH91kGfAvApXT4Lx3%0D%0A2sA4uG19468Z0XRtQLLCnrvQFomuLjnCiSl5XWhANIO_oiuSS5NtpPX6MkuOkzIf9Oj1pJ8HmIuK%0D%0AvdVJCx18GQPQ-aiVyaIpFkhYAWsj1mpKHOlsQfUv_Qoa7m1ja6icLaVG_axvLaLe3w%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=127391041471;ord=127391041471?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>
                    <div id="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=127391041471;ord=127391041471?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

                    
                    

<div id="content-2-wide">
    <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi221623065?ref_=hm_hp_i_1" data-video="vi221623065" data-source="bylist" data-id="ls002309697" data-rid="1Q0XBMB3TTZ2KAQTDEDJ" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Set in Brazil, three kids who make a discovery in a garbage dump soon find themselves running from the cops and trying to right a terrible wrong." alt="Set in Brazil, three kids who make a discovery in a garbage dump soon find themselves running from the cops and trying to right a terrible wrong." src="http://ia.media-imdb.com/images/M/MV5BMzEwNTE1MDM3OV5BMl5BanBnXkFtZTgwODY4NzA3MjE@._V1_SY298_CR1,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzEwNTE1MDM3OV5BMl5BanBnXkFtZTgwODY4NzA3MjE@._V1_SY298_CR1,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Set in Brazil, three kids who make a discovery in a garbage dump soon find themselves running from the cops and trying to right a terrible wrong." title="Set in Brazil, three kids who make a discovery in a garbage dump soon find themselves running from the cops and trying to right a terrible wrong." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Set in Brazil, three kids who make a discovery in a garbage dump soon find themselves running from the cops and trying to right a terrible wrong." title="Set in Brazil, three kids who make a discovery in a garbage dump soon find themselves running from the cops and trying to right a terrible wrong." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1921149/?ref_=hm_hp_cap_pri_1" > Trash </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3241456409?ref_=hm_hp_i_2" data-video="vi3241456409" data-source="bylist" data-id="ls002322762" data-rid="1Q0XBMB3TTZ2KAQTDEDJ" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." alt="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." src="http://ia.media-imdb.com/images/M/MV5BMjIwOTkxNjMxNl5BMl5BanBnXkFtZTgwMTgzMTU2NjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIwOTkxNjMxNl5BMl5BanBnXkFtZTgwMTgzMTU2NjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." title="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." title="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2279339/?ref_=hm_hp_cap_pri_2" > Love the Coopers </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1798419225?ref_=hm_hp_i_3" data-video="vi1798419225" data-source="bylist" data-id="ls002922459" data-rid="1Q0XBMB3TTZ2KAQTDEDJ" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Fred and Mick, two old friends, are on vacation in an elegant hotel at the foot of the Alps. Fred, a composer and conductor, is now retired. Mick, a film director, is still working. They look with curiosity and tenderness on their children's confused lives, Mick's enthusiastic young writers, and the other hotel guests. While Mick scrambles to finish the screenplay for what he imagines will be his last important film, Fred has no intention of resuming his musical career. But someone wants at all costs to hear him conduct again." alt="Fred and Mick, two old friends, are on vacation in an elegant hotel at the foot of the Alps. Fred, a composer and conductor, is now retired. Mick, a film director, is still working. They look with curiosity and tenderness on their children's confused lives, Mick's enthusiastic young writers, and the other hotel guests. While Mick scrambles to finish the screenplay for what he imagines will be his last important film, Fred has no intention of resuming his musical career. But someone wants at all costs to hear him conduct again." src="http://ia.media-imdb.com/images/M/MV5BMTc0ODI0OTI2OF5BMl5BanBnXkFtZTgwMTEwNzU2NjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc0ODI0OTI2OF5BMl5BanBnXkFtZTgwMTEwNzU2NjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Fred and Mick, two old friends, are on vacation in an elegant hotel at the foot of the Alps. Fred, a composer and conductor, is now retired. Mick, a film director, is still working. They look with curiosity and tenderness on their children's confused lives, Mick's enthusiastic young writers, and the other hotel guests. While Mick scrambles to finish the screenplay for what he imagines will be his last important film, Fred has no intention of resuming his musical career. But someone wants at all costs to hear him conduct again." title="Fred and Mick, two old friends, are on vacation in an elegant hotel at the foot of the Alps. Fred, a composer and conductor, is now retired. Mick, a film director, is still working. They look with curiosity and tenderness on their children's confused lives, Mick's enthusiastic young writers, and the other hotel guests. While Mick scrambles to finish the screenplay for what he imagines will be his last important film, Fred has no intention of resuming his musical career. But someone wants at all costs to hear him conduct again." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Fred and Mick, two old friends, are on vacation in an elegant hotel at the foot of the Alps. Fred, a composer and conductor, is now retired. Mick, a film director, is still working. They look with curiosity and tenderness on their children's confused lives, Mick's enthusiastic young writers, and the other hotel guests. While Mick scrambles to finish the screenplay for what he imagines will be his last important film, Fred has no intention of resuming his musical career. But someone wants at all costs to hear him conduct again." title="Fred and Mick, two old friends, are on vacation in an elegant hotel at the foot of the Alps. Fred, a composer and conductor, is now retired. Mick, a film director, is still working. They look with curiosity and tenderness on their children's confused lives, Mick's enthusiastic young writers, and the other hotel guests. While Mick scrambles to finish the screenplay for what he imagines will be his last important film, Fred has no intention of resuming his musical career. But someone wants at all costs to hear him conduct again." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3312830/?ref_=hm_hp_cap_pri_3" > Youth </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?ref_=hm_hp_sm" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58943657?ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTcyMTU5MzgxMF5BMl5BanBnXkFtZTYwMDI0NjI1._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58943657?ref_=hm_nw_tp1"
class="headlines" >âDoctor Strangeâ Eyes âHannibalâ Star Mads Mikkelsen to Play Villain (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> â<a href="/title/tt2243973?ref_=hm_nw_tp1_lk1">Hannibal</a>â star <a href="/name/nm0586568?ref_=hm_nw_tp1_lk2">Mads Mikkelsen</a> is in early talks to play one of the villainsÂ in Marvelâs â<a href="/title/tt1211837?ref_=hm_nw_tp1_lk3">Doctor Strange</a>â opposite <a href="/name/nm1212722?ref_=hm_nw_tp1_lk4">Benedict Cumberbatch</a>, Variety has learned exclusively. While sources say no offer has been delivered, one is expected shortly. Marvel had no comment on Mikkelsenâs involvement. ...                                        <span class="nobr"><a href="/news/ni58943657?ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58942747?ref_=hm_nw_tp2"
class="headlines" >âGalaxy Questâ TV Series Takes Flight at Amazon</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tp2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58942759?ref_=hm_nw_tp3"
class="headlines" >Jason Reitman to Direct âPrincess Brideâ Live Read at Toronto Film Festival</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_tp3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58942505?ref_=hm_nw_tp4"
class="headlines" >Box Office: âNo Escapeâ Runs Off With $1.2 Million on Wednesday</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58942157?ref_=hm_nw_tp5"
class="headlines" >Ridley Scott Says 'Prometheus 2' Is His Next Film</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?ref_=hm_nw_tp5_src"
>The Playlist</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58942505?ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjE0MDI3NTE5NF5BMl5BanBnXkFtZTgwNzI3ODM2NjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58942505?ref_=hm_nw_mv1"
class="headlines" >Box Office: âNo Escapeâ Runs Off With $1.2 Million on Wednesday</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> â<a href="/title/tt1781922?ref_=hm_nw_mv1_lk1">No Escape</a>â kicked off its box office run with $1.2 million on Wednesday. The action thriller is being distributed by the Weinstein Company and is expected to bring in $11 million over its first five days in theaters. The indie label picked up the film for roughly $5 million from Bold Films. It ...                                        <span class="nobr"><a href="/news/ni58942505?ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58942157?ref_=hm_nw_mv2"
class="headlines" >Ridley Scott Says 'Prometheus 2' Is His Next Film</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?ref_=hm_nw_mv2_src"
>The Playlist</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58942333?ref_=hm_nw_mv3"
class="headlines" >Mormon Comedy âOnce I Was a Beehiveâ Gets National Release</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58942501?ref_=hm_nw_mv4"
class="headlines" >âIngrid Bergman â In Her Own Wordsâ Cannes Docu Lands At Rialto Pictures For Fall Run</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?ref_=hm_nw_mv4_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58942307?ref_=hm_nw_mv5"
class="headlines" >Fest Hit 'The Second Mother' Brings Success--and Pain</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?ref_=hm_nw_mv5_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58942758?ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNDA4NjMxODU0NV5BMl5BanBnXkFtZTgwMzg0OTMzNjE@._V1_SY150_CR5,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58942758?ref_=hm_nw_tv1"
class="headlines" >Ratings: NBCâs âCarmichael Showâ Is Most-Watched Broadcast Summer Comedy Debut in 8 Years</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_tv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>NBC has proved that its comedies can get fairly decent ratings â it just has to launch them out of âAmericaâs Got Talentâ during the summertime. The networkâs <a href="/title/tt4307902?ref_=hm_nw_tv1_lk1">âThe Carmichael Showâ</a> hauled in 4.8 million total viewers with its premiere episode on Wednesday, the most eyeballs for a broadcast summer ...                                        <span class="nobr"><a href="/news/ni58942758?ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58942747?ref_=hm_nw_tv2"
class="headlines" >âGalaxy Questâ TV Series Takes Flight at Amazon</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58942497?ref_=hm_nw_tv3"
class="headlines" >âMythbustersâ Tests Out Final âBreaking Badâ Showdown</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58942507?ref_=hm_nw_tv4"
class="headlines" >Fashion Police Returns Monday! 9 Reasons We're Excited the Stylish Show Is Back</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_tv4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58942884?ref_=hm_nw_tv5"
class="headlines" >Martin Freeman on First âSherlockâ Audition: âThey Thought I Was a Moody Prickâ</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58942318?ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk2MDgxNjA4OF5BMl5BanBnXkFtZTcwMjY3ODAyNg@@._V1_SY150_CR12,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58942318?ref_=hm_nw_cel1"
class="headlines" >Lea Michele Opens Up About How She Found Love Again With Matthew Paetz After Cory Monteith Died in 2013</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p>Two years ago, <a href="/name/nm0584951?ref_=hm_nw_cel1_lk1">Lea Michele</a>'s world came crashing down. In July 2013, the actress lost <a href="/name/nm1719342?ref_=hm_nw_cel1_lk2">Cory Monteith</a>, her <a href="/title/tt1327801?ref_=hm_nw_cel1_lk3">Glee</a> co-star and boyfriend of two years, when he died of a lethal mix of heroin and alcohol. With the help of her colleagues and friends like <a href="/name/nm0005028?ref_=hm_nw_cel1_lk4">Kate Hudson</a>, she slowly began to pick up the pieces....                                        <span class="nobr"><a href="/news/ni58942318?ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58942136?ref_=hm_nw_cel2"
class="headlines" >Cate Blanchett to Receive British Film Institute Fellowship</a>
    <div class="infobar">
            <span class="text-muted">11 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_cel2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58942890?ref_=hm_nw_cel3"
class="headlines" >Gena Rowlands, Spike Lee, Debbie Reynolds to Receive Governors Awards Oscars</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_cel3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58942913?ref_=hm_nw_cel4"
class="headlines" >Buffy the Vampire Slayer Star Nicholas Brendon Explains Why He Walked Off Dr. Phil Set: He ''Went for the Jugular''</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58944667?ref_=hm_nw_cel5"
class="headlines" >The New Dragon Tattoo Book Is Out, but There's Some Major Controversy</a>
    <div class="infobar">
            <span class="text-muted">47 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1316875776/rg3653933824?ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjIzODEyMTY1M15BMl5BanBnXkFtZTgwMTEzOTU2NjE@._V1._CR0,0,1389,1389_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIzODEyMTY1M15BMl5BanBnXkFtZTgwMTEzOTU2NjE@._V1._CR0,0,1389,1389_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1316875776/rg3653933824?ref_=hm_snp_cap_pri_1" > <i>The Transporter Refueled</i> Screening & After-Party </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3044732416/rg1528338944?ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="The Program (2015)" alt="The Program (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTk0NzQyNjUxMl5BMl5BanBnXkFtZTgwNTc4NzU2NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0NzQyNjUxMl5BMl5BanBnXkFtZTgwNTc4NzU2NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm3044732416/rg1528338944?ref_=hm_snp_cap_pri_2" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1020718592/rg784964352?ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTYxMjQ1OTg3MV5BMl5BanBnXkFtZTgwNTYxNDE2NjE@._V1._CR544,0,1360,1360_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYxMjQ1OTg3MV5BMl5BanBnXkFtZTgwNTYxNDE2NjE@._V1._CR544,0,1360,1360_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1020718592/rg784964352?ref_=hm_snp_cap_pri_3" > Latest Still Photos </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-27&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0891786?ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Alexa PenaVega" alt="Alexa PenaVega" src="http://ia.media-imdb.com/images/M/MV5BMjQxMzI4MTc5N15BMl5BanBnXkFtZTgwODM3NDAxMTE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQxMzI4MTc5N15BMl5BanBnXkFtZTgwODM3NDAxMTE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0891786?ref_=hm_brn_cap_pri_lk1_1">Alexa PenaVega</a> (27) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0666739?ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Aaron Paul" alt="Aaron Paul" src="http://ia.media-imdb.com/images/M/MV5BMTY1OTY5NjI5NV5BMl5BanBnXkFtZTcwODA4MjM0OA@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY1OTY5NjI5NV5BMl5BanBnXkFtZTcwODA4MjM0OA@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0666739?ref_=hm_brn_cap_pri_lk1_2">Aaron Paul</a> (36) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1140666?ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Patrick J. Adams" alt="Patrick J. Adams" src="http://ia.media-imdb.com/images/M/MV5BMjE1MzM2MjI4NF5BMl5BanBnXkFtZTgwNDQ5ODkyMDE@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE1MzM2MjI4NF5BMl5BanBnXkFtZTgwNDQ5ODkyMDE@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1140666?ref_=hm_brn_cap_pri_lk1_3">Patrick J. Adams</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0149950?ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Sarah Chalke" alt="Sarah Chalke" src="http://ia.media-imdb.com/images/M/MV5BMjQyNjg4NDI4NV5BMl5BanBnXkFtZTcwOTAzOTUxMg@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQyNjg4NDI4NV5BMl5BanBnXkFtZTcwOTAzOTUxMg@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0149950?ref_=hm_brn_cap_pri_lk1_4">Sarah Chalke</a> (39) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001780?ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Peter Stormare" alt="Peter Stormare" src="http://ia.media-imdb.com/images/M/MV5BOTUyMDQ0MTEyNF5BMl5BanBnXkFtZTYwNDYxMDkz._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTUyMDQ0MTEyNF5BMl5BanBnXkFtZTYwNDYxMDkz._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001780?ref_=hm_brn_cap_pri_lk1_5">Peter Stormare</a> (62) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-27&ref_=hm_brn_sm" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg667654912?ref_=hm_if_hd" > <h3>Indie Focus: 'Ashby' - Exclusive Photos</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1971252736/rg667654912?ref_=hm_if_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTU2MzQ4MDIxNF5BMl5BanBnXkFtZTgwMzEwMDY2NjE@._V1._CR217,0,467,467_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU2MzQ4MDIxNF5BMl5BanBnXkFtZTgwMzEwMDY2NjE@._V1._CR217,0,467,467_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2004807168/rg667654912?ref_=hm_if_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjA5NjM1NzQyOF5BMl5BanBnXkFtZTgwMTEwMDY2NjE@._V1._CR0,0,467,467_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5NjM1NzQyOF5BMl5BanBnXkFtZTgwMTEwMDY2NjE@._V1._CR0,0,467,467_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1786703360/rg667654912?ref_=hm_if_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY5Mjc0NTQ0OF5BMl5BanBnXkFtZTgwODAwMDY2NjE@._V1._CR117,0,467,467_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY5Mjc0NTQ0OF5BMl5BanBnXkFtZTgwODAwMDY2NjE@._V1._CR117,0,467,467_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1753148928/rg667654912?ref_=hm_if_i_4" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTUwODIxODg3NF5BMl5BanBnXkFtZTgwMDEwMDY2NjE@._V1._CR117,0,467,467_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwODIxODg3NF5BMl5BanBnXkFtZTgwMDEwMDY2NjE@._V1._CR117,0,467,467_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb"><a href="/name/nm1822659/?ref_=hm_if_lk1">Nat Wolff</a> stars as Ed Wallis, a high school student who develops a friendship with his neighbor, Ashby (<a href="/name/nm0000620/?ref_=hm_if_lk2">Mickey Rourke</a>), a retired CIA assassin who only has a few months left to live. Check out eight exclusive new photos from the film.</p> <p class="seemore"> <a href="/gallery/rg667654912?ref_=hm_if_sm" class="position_bottom supplemental" > See the full gallery </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt2628232/trivia?item=tr2342067&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2628232?ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="Penny Dreadful (2014-)" alt="Penny Dreadful (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMTg3OTEyNDA3NF5BMl5BanBnXkFtZTgwOTA1NzU2NDE@._V1_SY132_CR4,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3OTEyNDA3NF5BMl5BanBnXkFtZTgwOTA1NzU2NDE@._V1_SY132_CR4,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt2628232?ref_=hm_trv_lk1">Penny Dreadful</a></strong> <p class="blurb">This is not the first time that <a href="/name/nm0684877?ref_=hm_trv_lk1">Billie Piper</a> has played a prostitute, she previously portrayed 'Belle' in <a href="/title/tt1000734?ref_=hm_trv_lk2">Secret Diary of a Call Girl</a> (2007).</p></div> </div> </div> <p class="seemore"> <a href="/title/tt2628232/trivia?item=tr2342067&ref_=hm_trv_sm" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;0XcFaothS64&quot;}"
        >
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/0XcFaothS64/?ref_=hm_poll_hd" > <h3>Poll: Choose Your Protector</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <p class="blurb">You are magically transported into a world where all these characters exist. One will protect you; the others will hunt you. Which one do you choose as your guardian?<a href="http://www.imdb.com/board/bd0000088/nest/247264099/?ref_=hm_poll_lk1">Discuss here</a> after voting.</p> <p class="seemore"> <a href="/poll/0XcFaothS64/?ref_=hm_poll_sm" class="position_blurb supplemental" > Vote now </a> </p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0XcFaothS64/?ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="Hugh Jackman is Logan/Wolverine" alt="Hugh Jackman is Logan/Wolverine" src="http://ia.media-imdb.com/images/M/MV5BMTI4NTI3MzQzNl5BMl5BanBnXkFtZTYwNjAxMjc3._V1_SY207_CR7,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI4NTI3MzQzNl5BMl5BanBnXkFtZTYwNjAxMjc3._V1_SY207_CR7,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0XcFaothS64/?ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Still of Kevin Peter Hall in Predator (1987)" alt="Still of Kevin Peter Hall in Predator (1987)" src="http://ia.media-imdb.com/images/M/MV5BMTcwMDI5NTkwMF5BMl5BanBnXkFtZTYwMjc3MDc2._V1_SY207_CR12,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcwMDI5NTkwMF5BMl5BanBnXkFtZTYwMjc3MDc2._V1_SY207_CR12,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0XcFaothS64/?ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="Star Wars: Episode VI - Return of the Jedi (1983)" alt="Star Wars: Episode VI - Return of the Jedi (1983)" src="http://ia.media-imdb.com/images/M/MV5BMjE2MjY3MjQzMV5BMl5BanBnXkFtZTcwMzAyMzA4NA@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE2MjY3MjQzMV5BMl5BanBnXkFtZTcwMzAyMzA4NA@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0XcFaothS64/?ref_=hm_poll_i_4" > <img itemprop="image" class="pri_image" title="Still of Peter Weller in RoboCop (1987)" alt="Still of Peter Weller in RoboCop (1987)" src="http://ia.media-imdb.com/images/M/MV5BMjI3MDk1NzkyNF5BMl5BanBnXkFtZTcwODMwODU2NA@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI3MDk1NzkyNF5BMl5BanBnXkFtZTcwODMwODU2NA@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0XcFaothS64/?ref_=hm_poll_i_5" > <img itemprop="image" class="pri_image" title="Still of Arnold Schwarzenegger in Terminator 2: Judgment Day (1991)" alt="Still of Arnold Schwarzenegger in Terminator 2: Judgment Day (1991)" src="http://ia.media-imdb.com/images/M/MV5BMTU5MzA5NjQwMF5BMl5BanBnXkFtZTcwOTA4MTc4Mw@@._V1_SY207_CR84,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU5MzA5NjQwMF5BMl5BanBnXkFtZTcwOTA4MTc4Mw@@._V1_SY207_CR84,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
    </div>
    <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=127391041471;ord=127391041471?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=127391041471?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=127391041471?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3787590"></div> <div class="title"> <a href="/title/tt3787590?ref_=hm_otw_t0"> We Are Your Friends </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt3787590?ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3742378"></div> <div class="title"> <a href="/title/tt3742378?ref_=hm_otw_t1"> Une seconde mÃ¨re </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt3742378?ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1781922"></div> <div class="title"> <a href="/title/tt1781922?ref_=hm_otw_t2"> No Escape </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1598642"></div> <div class="title"> <a href="/title/tt1598642?ref_=hm_otw_t3"> Z for Zachariah </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3832914"></div> <div class="title"> <a href="/title/tt3832914?ref_=hm_otw_t4"> War Room </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3993894"></div> <div class="title"> <a href="/title/tt3993894?ref_=hm_otw_t5"> Queen of Earth </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2818178"></div> <div class="title"> <a href="/title/tt2818178?ref_=hm_otw_t6"> When Animals Dream </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?ref_=hm_otw_sm" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1398426"></div> <div class="title"> <a href="/title/tt1398426?ref_=hm_cht_t0"> Straight Outta Compton </a> <span class="secondary-text">$26.4M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381249"></div> <div class="title"> <a href="/title/tt2381249?ref_=hm_cht_t1"> Mission: Impossible - Rogue Nation </a> <span class="secondary-text">$11.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt2381249?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2752772"></div> <div class="title"> <a href="/title/tt2752772?ref_=hm_cht_t2"> Sinister 2 </a> <span class="secondary-text">$10.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt2752772?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2679042"></div> <div class="title"> <a href="/title/tt2679042?ref_=hm_cht_t3"> Hitman: Agent 47 </a> <span class="secondary-text">$8.3M</span> </div> <div class="action"> <a href="/showtimes/title/tt2679042?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1638355"></div> <div class="title"> <a href="/title/tt1638355?ref_=hm_cht_t4"> The Man from U.N.C.L.E. </a> <span class="secondary-text">$7.3M</span> </div> </div> </div> </div> </div> <div><a href="/chart/?ref_=hm_cht_sm" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2938956"></div> <div class="title"> <a href="/title/tt2938956?ref_=hm_cs_t0"> Le Transporteur: HÃ©ritage </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1178665"></div> <div class="title"> <a href="/title/tt1178665?ref_=hm_cs_t1"> A Walk in the Woods </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0443465"></div> <div class="title"> <a href="/title/tt0443465?ref_=hm_cs_t2"> Before We Go </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?ref_=hm_cs_sm" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/family-entertainment-guide/?ref_=hm_m_feg_tr_hd" > <h3>Family Entertainment Guide: IMDb on the Street</h3> </a> </span> </span> <p class="blurb">IMDb visited the Santa Monica Pier to ask families about their favorite movies from this summer, as well as their favorite all-time movies, TV shows, and characters.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2630333209?ref_=hm_m_feg_tr_i_1" data-video="vi2630333209" data-rid="1Q0XBMB3TTZ2KAQTDEDJ" data-type="single" class="video-colorbox" data-refsuffix="hm_m_feg_tr" data-ref="hm_m_feg_tr_i_1"> <img itemprop="image" class="pri_image" title="IMDb on the Street (2015-)" alt="IMDb on the Street (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ0MTU3MjAwM15BMl5BanBnXkFtZTgwMTIxNTk1NjE@._V1_SY393_CR2,0,700,393_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ0MTU3MjAwM15BMl5BanBnXkFtZTgwMTIxNTk1NjE@._V1_SY393_CR2,0,700,393_AL_UY786_UX1400_AL_.jpg" /> <img alt="IMDb on the Street (2015-)" title="IMDb on the Street (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Street (2015-)" title="IMDb on the Street (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/family-entertainment-guide/?ref_=hm_m_feg_tr_sm" class="position_bottom supplemental" > See our Family Entertainment Guide </a> </p>    </div>

        </span>
        </div>
    </div>
</div>

                    
                    


                   <br class="clear" />
                </div>





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYkO0r0t7JRAKq0It0Q5LsloYGWtaOmsziCi82FXhdUY_WJqvvY01-olFmVp2PuVwxI9R8xdQCo%0D%0A3Q2qubG3WpqnhnN5H2CYPrQYOFQX7-UWETE8I88XbkJ2I7G0iIPFkSaVTAROdV3lFYJcJ9-_6Iwt%0D%0AJoIfJ2n8bG9CjmVWdInkQVqj__KxKIQuJMzECJ5Wj21kEqQ9WxeUwJG2FZnqtH1un01EYqB25LbF%0D%0Axlz0q-zeZOE%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYrJZs3SXiMn0L2LRVKqZsPK-k1YWbB2bhoKItQo2RSdCDw5CJOT34bKDwOYiwUcGxw-5BydHmk%0D%0Auuo6FyT62KwG8pNqjDcO2NXRNvbTEjVlHqMikh2-p7Dzty8ED_LPQQi8hEhP00BSXvgxLLB5tfs1%0D%0At6wCGt7w3r7J2aQFw-WzghrLeCip9cZw1_53jJBtc6GRztWrF13XZnh8hhS7MVv4sg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYk3vR9O6gcrX_8W63IjHXoYRQ6qZ-1tXMj6cP5iekrI2dO4go5jLzDOzTJRmR-PhJ3VetFhFUJ%0D%0AdaXqJbvrlNoW1QhCB84ALKymU9-06Yb-gd0Z5gGdpuTV4fnkckc6-iZBzotj979d15ggwygu132-%0D%0Acb56bGuNLmDzt3E8ZTo6xattDTVQ4UDhw7QLTrrIT6LmGGG1N-Yr0jJZDjACwPsHLwnFmdqhpvzc%0D%0A6FYKSBi-ylSmyB3YxMycwW8x-YBuqhqUWg9wE_HJvPhPlSqPB3FTnEgjdnb_jcaAbB0U8Iu2z_L5%0D%0APua0ra3-lHmv2h_vE4Ahk_WneLqQm06_RhB-J31i3eMOYLV4nomicpSwLpo2-m34gb7JMS9UCBP3%0D%0AOr-mEaIyRKmA6MZ2vNtr80gJlS3a1Q%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYuytaWhxowS97ugShb0P5m1SSjYm_WQNOt2pzQkg5vkMtthrTbOGRAL5V0quOM3uZ8orenhRT4%0D%0A7XWWWRSx2GeiWZ9kSV9XrBLyqoPtLjsYMb5Xds0Wm1CgoxpbqLNzKS2-5WmwJHwzVVu-kBd5Xa3p%0D%0ASY8EkUn99EcvqNyBOVBukHWUrMZIz1j_Y9MVB3RwDme2llZxOMCTMJku7j3GTyTVpA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYmJ8vl4JX7a7Jyald5xHaPrQAjG3wJXtPWeYBa4DEaH5R8Gxlhf5W4LGelQ0pH4vR1j1vpKWGR%0D%0AVjywupsMVz3Zw_SwRjy_iQaFVEHQFgn5zZ0-x-daqdkBTXgNBSpMCGqSeMMMv6V4mtURMJ4WY2pA%0D%0A2wowHAGWi2NtXbhUr9Az6JZgDAziNg2FQVDHVbZSILf_%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYn2-3glrXDN2eTABigu_vpya_XFcL90LZ7nqcRmgmSQsqR25I4Rquzkh2YXAyh9oQjQd4ixiO7%0D%0AbvUIJom5QbOzbGwT_Pr_Q7sBKfhVVtTgCvoDRsIom6PzxhHFkyv87yE-cxkZOhM1ZShA2gLl2u3k%0D%0AbVvcz5ZmfuDH9kh2XufBh10WECEtpSMI56Wq1Rg8pijC8aKLAQ1xMUwoy7_77IzxKQ%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYiD0THOmNI4E-uTeBFHfvAEqEWLorNAsDQmYCcWxiiAQ1SC562J9r1MzztX9QwJv1PVQ3uc-oW%0D%0ALT87UpzhknonWqNboQhKG0IzJNnGzmXNUzbAz0wUOJqBrCWC6fzVdnxLRjrZf0-SjTsHUbACx4V3%0D%0AUILTtoSyrDqGLbkGgFCVUVWSgLPkzWoCCBABIU36LQLk8tzTCw5xEG2qkGLFgcEym49m6zhaCmTl%0D%0AHoKokXL6JGCl59-5LsDpHBmxd_M89n8onoVzWj_NMcTCBIkVkMo5uQ%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYqLeo5h9c91LyJeeRxUZHo_1-NGFEQN2Z2wa65gtm6pND_GWzJCr4oTqADrjzwOCMyWXsgDLmd%0D%0ApP6rz7wXCnnd7NYuTi3ddLQ0d3mV8MM0nDX58aMOf7Fch319-W37h_hFziHQbcpGXW8BNyDHy1Ve%0D%0AtJgrDWX6Ksned8WxBuxW_Hi6yKBXZHm1frOby38cTcHjPDMQDiDHHnl6LcnjE0CFug%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYgE2hBRvziRQQe-PG8TdAz2PSTLOGTTQOD8cASR8TT5-iYnE05GbMCbEfBe_OTxIlkTHD1ImMA%0D%0ARNE06TwCLmzUV8QgMH7X03aouokShtpydC27wtZbyVL3B1Xz2KFuMvpv4N_NYIZaWUbBn7YG6yJr%0D%0AUekirB_cXFa9T8Pg3YYVcaEKGaEjiSVqGSFRznan78k7%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYhWORprnS5TCtuhPLf4MSH62bC4hZnVi64Q_w_rN8k3wsI3HP0xLmSUG0gf4Ox5VACdZaxeu20%0D%0ASC0-ft0Cg40WNE6LSJ7GKAlRT8FW74OS4JsaDXrvxfz2c9E3Yi8aU3OaiNZVSKp-k2vAa7vu5SHf%0D%0Ah4sN6DcbmAGtt6LiqWzrsJpXuXp9gkyFq4Su42-kNtW_6AsOe1ZfhDy-r2vIR6jgU-NE66kd5GrE%0D%0ACfbThxR8rOmNQ3PGkcBOZmUS31HP1VIv%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYoFf4DFru7Cju3ZekHadFEe4XlskdLNvVDB0H0CjIi38-Eh1DXXAJvqEyNRVtp0P9W1p8px_bn%0D%0AIx1TPk4RHd2j6O_7_n6tpAxuvhjORbzm4tulHZkg0QMwro-hBzTTrqkpIGP8N0L9cMTuQGN2PPRW%0D%0A2kZ7yFeh3POyheXprugkEgW91cHqRAbggookNiX_Vvc8AMvd-nr8_2O5eZFrxXBMNXHMUMyfPo8L%0D%0ANZOnTGw7Ysw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYggbe2ufXQJ5k5XWohWSjPjQnjHWyitSPn1UqAVWJXh1U9w66ivZuAjAi8YwePP8S34t-trdUR%0D%0APJ2e6Bsqy9LwW8ULLf9O7BeZpnpf3tKsp6bWzk-cDDLKFp05QTVyzyfy3U9BBQU6ZyDzlR1U04k1%0D%0A-IyVFtSaKtwoHwYecXwZtSoMTym1TviGUNUeYgXSX0av9E-MgbxE2PhbVKOR6JsLdN5kuGqaEgYl%0D%0AJCEIaFzjSRg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYrdjTENtfUNiXmAsRDkhmrCUJBG7zHfHy8rMSaDRQZFp9ZaI_9ohd3uoEgZaWZP9l7HUXQBsMS%0D%0AzQs4oCtw86ZE4qV54vDYtT4q_K4grWNdU7ag9KuPJWaFqyaWYx_3pLj9IqVqM6T0Z_twPnC-Bw3m%0D%0AavXYZUUsBK-ybBARoSTt_fQKt3qtD-mpgKa_entNzZ8dc3OlHWqfe6Ju2qCP0VEQs1_ZuRQTvYXg%0D%0AQ4kyrVUWfUY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYrIte4z03aqr0me8V8XbQdOupIPgwACjwA-Ib5B7cvSa4imsOS_6sqZdt99BWvc0hkqfZvyowj%0D%0ABvNgkAEnbODp9xIhcmatJHUta-f5Ax8YN2yYIiCm9hVYQx-7ryZdDzD-A_EAaHVEe0mKTVPG_hk3%0D%0AoPdxYz_jXKT5aeWQObnR6EvT1TA9pHNzXBoMfoiIa06Q-weSJhdGTlLjeW5cruKj4n2PTpDI4uJs%0D%0AmQm-Sgz5bW8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYiEsziUfpGSYALd7-cXIgJymzsUQmoO4hgK0lRO941TCIU8rMrclyA4qfKSG7iYZIjkY__0jeU%0D%0A3Hr0p7VXQDai8uSLB8UJqoSqELWh8ErkRn9oIVEVnwUzH9y4TeHdYTGYxbyE95U8GHptkJaA95oT%0D%0AW4LfLvvPmQeulX5M6I0V_bGg7Yc2N5UkWv79G_W1G500w2T7v-3jjjaQO-XzcDTw-k_XmrirLX8X%0D%0Av8k-u_MKm74%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYmxPUl4jLLbfrqE1HhsDnPHjnzaEe1P-P9kQmdV1DS03Q_IOWzeeM-Ba7B11Ceip7GF2B1l76N%0D%0AyEPel73E9nse16KxcBangfefl6U7tmC3yeHH11RThrSRgSXmhI0MeRQHF4EYebf6QO2vxvIfTAqZ%0D%0AJDg9i_Jm8R749tRZ9cxH5qIaJ9eSahST6v7NRQC85LgeNoSpbTwJuXlzdpMDS0iiRT-4JFYWK8KB%0D%0AlfTixqQ8LJBEMhTDGPzCdmWm7xX_Jq6C%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYgjMteQFkDB8CgYTmmlO0ifu9MyQ6krnRnR8OwhInKB-fcCxlOiZ5MnjhiPTdVpNk6nI3ANKpA%0D%0A9R31zyFktvOoh5oTis8q1OzyYxU-srrgd_mU9BdESYrF5IKTtIWBVHMY3ppdbQavqS68uoO_jdsx%0D%0AAlwxl5DQqhNPpulnZraplXU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYtmcTmDgzH9enr27IxEW1forP5N3C0GI_eUV25_JblLAN49JiETCF13g4PNV2CST3HQBWyUGBf%0D%0A1gKFWEiAmt2aAJ_beHzhLNhdiIgdxU7LMDWFMOxzjaumk62YS2FasnL1yyyB3jLek11BrCRESfbS%0D%0A07NiAgew_lK1Relji6a3Lig%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-369064624._CB313453487_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-379116075._CB313453520_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3031882257._CB313453528_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2041056462._CB313453507_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101d0d54d74de5e0c717e40beae1cf7339f396043f5bb973e08d5380a141baf457a",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=127391041471"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=127391041471&ord=127391041471";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="512"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
