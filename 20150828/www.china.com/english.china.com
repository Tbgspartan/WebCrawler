ï»¿<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>China.com - Your guide on traveling and living in China</title>
<meta name="keywords" content="China,travel,lifestyle,learn Chinese,news,videos,business,films,sports,reports" />
<meta name="description" content="English.china.com is a one-stop shop for everything about China â news, events, culture, people, lifestyle, language. It also provides information about traveling and living in China." />
<meta name="auther" content="F7 13489" />
<!-- /etc/htmlhead.shtml Start -->
<link href="/css/style.css?20141205.1" rel="stylesheet" />
<!--[if lte IE 6]> 
<script src="/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.page-select-language-list em, .page-nav .page-openSearch em, .page-focus-prevnext a, .page-video-right .item-text, .page-watched .play, .page-video-right .item-icon, .page-video-right .item-watch'); 
</script>
<![endif]-->
<!-- /etc/htmlhead.shtml End -->
<!-- /etc/goto3g.shtml Start -->

<!-- /etc/goto3g.shtml End -->
</head>

<body>
<!-- /etc/channelhomehead.shtml Start -->
<div class="page-head">
  <div class="page-top-bg" id="page-top">
    <div class="maxWidth page-top">
      <div class="page-logo"><a href="/index.html"><img src="/img/logo.png" alt="china.com" width="208" height="46" /></a></div>
      <div class="page-language">
        <em>language:English</em>
      </div>
      <div class="page-top-right">
          <div class="page-top-time">Thursday, October 24, 2013</div>
          <div class="page-select-language" id="page-select-language">
              <div class="page-show-language" style="display: block;">Language</div>
              <div class="page-select-language-tit" style="display: none;"><i>Language</i></div>
              <div class="page-select-language-list" style="display: none;">
                <a href="http://www.china.com/index.html" class="langCn"><em>www</em></a>
                <a href="http://english.china.com/index.html" class="langEn"><em>english</em></a>
                <a href="http://german.china.com/index.html" class="langDe"><em>german</em></a>
                <a href="http://italy.china.com/index.html" class="langIt"><em>italy</em></a>
                <a href="http://portuguese.china.com/index.html" class="langPt"><em>portuguese</em></a>
                <a href="http://french.china.com/index.html" class="langFr"><em>french</em></a>
                <a href="http://russian.china.com/index.html" class="langRu"><em>russian</em></a>
                <a href="http://espanol.china.com/index.html" class="langEs"><em>espanol</em></a>
                <a href="http://malay.china.com/index.html" class="langMy"><em>malay</em></a>
                <a href="http://vietnamese.china.com/index.html" class="langVn"><em>vietnamese</em></a>
                <a href="http://laos.china.com/index.html" class="langLa"><em>laos</em></a>
                <a href="http://cambodian.china.com/index.html" class="langKh"><em>cambodian</em></a>
                <a href="http://thai.china.com/index.html" class="langTh"><em>thai</em></a>
                <a href="http://indonesian.china.com/index.html" class="langId"><em>indonesian</em></a>
                <a href="http://filipino.china.com/index.html" class="langPh"><em>filipino</em></a>
                <a href="http://myanmar.china.com/index.html" class="langMm"><em>myanmar</em></a>
                <a href="http://japanese.china.com/index.html" class="langJp"><em>japanese</em></a>
                <a href="http://korean.china.com/index.html" class="langKr"><em>korean</em></a>
                <a href="http://mongol.china.com/index.html" class="langMn"><em>mongol</em></a>
                <a href="http://nepal.china.com/index.html" class="langNp"><em>nepal</em></a>
                <a href="http://hindi.china.com/index.html" class="langIn"><em>hindi</em></a>
                <a href="http://bengali.china.com/index.html" class="langMd"><em>bengali</em></a>
                <a href="http://turkish.china.com/index.html" class="langTr"><em>turkish</em></a>
                <a href="http://persian.china.com/index.html" class="langIr"><em>persian</em></a>
                <a href="http://arabic.china.com/index.html" class="langAe"><em>arabic</em></a>
              </div>
            </div>
            <div class="page-search">
              <form id="web-search" name="web-search" method="get" action="http://www.google.com/search">
                <input type="hidden" name="sitesearch" value="english.china.com" />
                <input type="text" name="q" class="web-search-keyword" placeholder="Search here..." />
                <input type="submit" name="button" class="web-search-but" value="Search Site" />
              </form>
            </div>
      </div>
    </div>
  </div>
  <div class="page-nav-bg" id="page-nav">
    <ul class="page-nav maxWidth">
      <li class="page-openSearch"><a href="#"><em>Open search</em></a></li>
      <li class="small-logo"><a href="/index.html"><img src="/img/small-logo.png" /></a></li>
      <li><a href="/news/index.html">News</a></li>
      <li><a href="/video/index.html">Video</a></li>
      <!--<li><a href="/audio/index.html">Audio</a></li>-->
      <!--<li><a href="/photos/index.html">Photos</a></li>-->
      <li><a href="/travel/index.html">Travel</a></li>
      <!--<li><a href="/lifestyle/index.html">Lifestyle</a></li>-->
      <li><a href="/chinese/index.html">Learn Chinese</a></li>
	  <li><a href="http://english.china.com/cityguide/">City Guide</a></li>
      <li><a href="http://mail.china.com/en/" class="ext" target="_blank">Free Mail</a></li>
    </ul>
  </div>
</div><!-- page-head End -->
<!-- /etc/channelhomehead.shtml End -->

<div class="maxWidth"><div id='CH_ENG_CHT_00001' class='adclass' adid='CHT00' pushtype='no'><div id='CHT00'><a href='http://statistic.dvsend.china.com/cc/CHT00?http://english.china.com/cityguide/' target='_blank'><img src='http://dvs.china.com/4662/yw_ct1_1000x90.gif' width='1000' height='90' border='0'></a></div></div></div>

<div class="page-main maxWidth">
  <div class="page-left">
    <div class="page-focus" id="page-focus">
      <div class="page-focus-body" id="page-focus-body">
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.chinadaily.com.cn/business/2015-08/27/content_21722378.htm" title="Iconic Jewish cafe 'White Horse Coffee' reopens for business"><img src="http://img04.abroad.imgcdc.com/english/news/topphotos/china/189/20150827/449060_114444_680x330.jpg" width="680" height="330" alt="Iconic Jewish cafe 'White Horse Coffee' reopens for business" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.chinadaily.com.cn/business/2015-08/27/content_21722378.htm" title="Iconic Jewish cafe 'White Horse Coffee' reopens for business" class="title_default">Iconic Jewish cafe 'White Horse Coffee' reopens for business</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.ecns.cn/visual/hd/2015/08-27/75081.shtml" title="A fisheye view of Bird's Nest"><img src="http://img01.abroad.imgcdc.com/english/news/topphotos/world/1208/20150827/449055_114443_680x330.jpg" width="680" height="330" alt="A fisheye view of Bird's Nest" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.ecns.cn/visual/hd/2015/08-27/75081.shtml" title="A fisheye view of Bird's Nest" class="title_default">A fisheye view of Bird's Nest</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-08/27/c_134559089.htm" title="Firepower-2015 Qingtongxia E Exercise"><img src="http://img01.abroad.imgcdc.com/english/home/topphoto/1295/20150827/448633_114318.jpg" width="680" height="330" alt="Firepower-2015 Qingtongxia E Exercise" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-08/27/c_134559089.htm" title="Firepower-2015 Qingtongxia E Exercise" class="title_default">Firepower-2015 Qingtongxia E Exercise</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-08/27/c_134559200.htm" title="Meet Luxury Outdoor Hotel in South Africa"><img src="http://img04.abroad.imgcdc.com/english/home/topphoto/1295/20150827/448622_114314.jpg" width="680" height="330" alt="Meet Luxury Outdoor Hotel in South Africa" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-08/27/c_134559200.htm" title="Meet Luxury Outdoor Hotel in South Africa" class="title_default">Meet Luxury Outdoor Hotel in South Africa</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/08/26/2743s893304.htm" title="China Eyes More Cooperation via Moscow Air Show"><img src="http://img04.abroad.imgcdc.com/english/home/topphoto/1295/20150826/448465_114286.jpg" width="680" height="330" alt="China Eyes More Cooperation via Moscow Air Show" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/08/26/2743s893304.htm" title="China Eyes More Cooperation via Moscow Air Show" class="title_default">China Eyes More Cooperation via Moscow Air Show</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
      </div>
      <div id="page-focus-console"></div>
      <div class="page-focus-prevnext">
        <a href="#" id="page-focus-prev">Previous focus</a>
        <a href="#" id="page-focus-next">Next focus</a>
      </div>
    </div><!-- page-focus End -->
    
    <div class="page-latest">
      <h2 class="modTit"><strong><a href="/news/index.html">LATEST NEWS</a></strong></h2>
      <div class="page-latest-body" id="page-latest">
        <div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/27/3123s893358.htm" title="Leonard Nimoy Gets Touching Tribute From Star Trek Beyond Cast"><img src="http://img04.abroad.imgcdc.com/english/news/showbiz/58/20150827/449082_114454_200x120.jpg" width="200" height="120" alt="Leonard Nimoy Gets Touching Tribute From Star Trek Beyond Cast" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/08/27 17:37:13</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/27/3123s893358.htm" title="Leonard Nimoy Gets Touching Tribute From Star Trek Beyond Cast" class="title_default">Leonard Nimoy Gets Touching Tribute From Star Trek Beyond Cast</a></h3>
            <p class="item-infor" title="The new Star Trek cast is honoring the late, great Leonard Nimoy, and are carrying on part of his legacy by highlighting a special cause that was close to his heart.">The new Star Trek cast is honoring the late, great Leonard Nimoy, and are carrying on part of his legacy by highlighting a special cause that was close to his heart.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/metro/Man-in-hospital-after-eating-3-boiled-toads/shdaily.shtml" title="Man in hospital after eating 3 boiled toads"><img src="" width="" height="" alt="Man in hospital after eating 3 boiled toads" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/27 17:36:08</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/metro/Man-in-hospital-after-eating-3-boiled-toads/shdaily.shtml" title="Man in hospital after eating 3 boiled toads" class="title_default">Man in hospital after eating 3 boiled toads</a></h3>
            <p class="item-infor" title="A 70-year-old man in Shanghai was sent to hospital after eating three boiled toads believing it to be a folk remedy for his skin disease.">A 70-year-old man in Shanghai was sent to hospital after eating three boiled toads believing it to be a folk remedy for his skin disease.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.ecns.cn/cns-wire/2015/08-27/178931.shtml" title="China Mobile to play hardball on real-name registration"><img src="http://img01.abroad.imgcdc.com/english/news/business/56/20150827/449079_114453_200x120.jpg" width="200" height="120" alt="China Mobile to play hardball on real-name registration" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/08/27 17:35:12</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://www.ecns.cn/cns-wire/2015/08-27/178931.shtml" title="China Mobile to play hardball on real-name registration" class="title_default">China Mobile to play hardball on real-name registration</a></h3>
            <p class="item-infor" title="From September 1, China Mobile will restrict SIM card functions for customers who fail to comply with a real-name registration requirement as set by Chinese authorities. ">From September 1, China Mobile will restrict SIM card functions for customers who fail to comply with a real-name registration requirement as set by Chinese authorities. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/2015-08/27/content_21717976.htm" title="Suspect in Virginia TV shooting had history of workplace issues"><img src="http://img04.abroad.imgcdc.com/english/news/world/55/20150827/449077_114451.jpg" width="160" height="103" alt="Suspect in Virginia TV shooting had history of workplace issues" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/08/27 17:32:10</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/2015-08/27/content_21717976.htm" title="Suspect in Virginia TV shooting had history of workplace issues" class="title_default">Suspect in Virginia TV shooting had history of workplace issues</a></h3>
            <p class="item-infor" title="Two television journalists, who were both white, were killed during a live broadcast reportedly by a veteran anchorman in an apparent racist attack.">Two television journalists, who were both white, were killed during a live broadcast reportedly by a veteran anchorman in an apparent racist attack.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/27/2201s893409.htm" title="China Launches Yaogan-27 Remote Sensing Satellite"><img src="http://img04.abroad.imgcdc.com/english/news/china/54/20150827/449068_114446_200x120.jpg" width="200" height="120" alt="China Launches Yaogan-27 Remote Sensing Satellite" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/27 17:29:53</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/27/2201s893409.htm" title="China Launches Yaogan-27 Remote Sensing Satellite" class="title_default">China Launches Yaogan-27 Remote Sensing Satellite</a></h3>
            <p class="item-infor" title="China's Yaogan-27 remote sensing satellite was sent into space on Thursday at 10:31 a.m. Beijing Time, from Taiyuan launch site in Shanxi Province, north China. ">China's Yaogan-27 remote sensing satellite was sent into space on Thursday at 10:31 a.m. Beijing Time, from Taiyuan launch site in Shanxi Province, north China. </p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/27/3123s893356.htm" title="Full Line-up of This Year's Shanghai Echo Park Festival Comes out"><img src="" width="" height="" alt="Full Line-up of This Year's Shanghai Echo Park Festival Comes out" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/08/27 10:07:13</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/27/3123s893356.htm" title="Full Line-up of This Year's Shanghai Echo Park Festival Comes out" class="title_default">Full Line-up of This Year's Shanghai Echo Park Festival Comes out</a></h3>
            <p class="item-infor" title="The full line-up of this year's Shanghai Echo Park Festival has been out, with Manchester-based indie rock band Everything Everything and renowned American hip hop duo Black Star added to the bill.">The full line-up of this year's Shanghai Echo Park Festival has been out, with Manchester-based indie rock band Everything Everything and renowned American hip hop duo Black Star added to the bill.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-08/26/c_134558559.htm" title="China to Step up Financial Leasing to Bolster Economy"><img src="" width="" height="" alt="China to Step up Financial Leasing to Bolster Economy" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/08/27 10:05:01</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-08/26/c_134558559.htm" title="China to Step up Financial Leasing to Bolster Economy" class="title_default">China to Step up Financial Leasing to Bolster Economy</a></h3>
            <p class="item-infor" title=" The State Council, China's Cabinet, on Wednesday mapped out measures to accelerate development of financial leasing and make it better serve the real economy"> The State Council, China's Cabinet, on Wednesday mapped out measures to accelerate development of financial leasing and make it better serve the real economy</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/china/2015-08/27/content_21717738.htm" title="Beijing Tops Domestic List for Traffic Congestion"><img src="" width="" height="" alt="Beijing Tops Domestic List for Traffic Congestion" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/27 10:02:40</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/china/2015-08/27/content_21717738.htm" title="Beijing Tops Domestic List for Traffic Congestion" class="title_default">Beijing Tops Domestic List for Traffic Congestion</a></h3>
            <p class="item-infor" title="Beijing topped the traffic congestion list of 45 major domestic cities in the second quarter, and the frequent use of ride-hailing services was cited in a report as a major reason.">Beijing topped the traffic congestion list of 45 major domestic cities in the second quarter, and the frequent use of ride-hailing services was cited in a report as a major reason.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/27/3961s893337.htm" title="China Shuts Down 300 Websites for Online Extortion and Paid Post Deletions"><img src="http://img01.abroad.imgcdc.com/english/news/china/54/20150827/448631_114316.jpg" width="160" height="120" alt="China Shuts Down 300 Websites for Online Extortion and Paid Post Deletions" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/27 09:57:51</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/27/3961s893337.htm" title="China Shuts Down 300 Websites for Online Extortion and Paid Post Deletions" class="title_default">China Shuts Down 300 Websites for Online Extortion and Paid Post Deletions</a></h3>
            <p class="item-infor" title="Chinese authorities are reporting they've shut down nearly 300 websites as part of a 6-month crack-down on online extortion.">Chinese authorities are reporting they've shut down nearly 300 websites as part of a 6-month crack-down on online extortion.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-08/27/c_134558610.htm" title="Chinese Firm Signs 1.48 Bln USD Deal to Build Cement Plants in Africa"><img src="" width="" height="" alt="Chinese Firm Signs 1.48 Bln USD Deal to Build Cement Plants in Africa" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/08/27 09:56:11</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-08/27/c_134558610.htm" title="Chinese Firm Signs 1.48 Bln USD Deal to Build Cement Plants in Africa" class="title_default">Chinese Firm Signs 1.48 Bln USD Deal to Build Cement Plants in Africa</a></h3>
            <p class="item-infor" title="A Chinese firm has signed a deal worth 1.487 billion U.S. dollars with Nigeria-based cement giant Dangote Group to build cement plants in several African countries.">A Chinese firm has signed a deal worth 1.487 billion U.S. dollars with Nigeria-based cement giant Dangote Group to build cement plants in several African countries.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/27/3941s893345.htm" title="Bolt and Gatlin Progress to Men's 200m Final"><img src="http://img03.abroad.imgcdc.com/english/news/sports/57/20150827/448620_114313.jpg" width="200" height="120" alt="Bolt and Gatlin Progress to Men's 200m Final" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/08/27 09:49:46</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/27/3941s893345.htm" title="Bolt and Gatlin Progress to Men's 200m Final" class="title_default">Bolt and Gatlin Progress to Men's 200m Final</a></h3>
            <p class="item-infor" title="Jamaican Usain Bolt has progressed to the men's 200 metres final at 2015 IAAF World Championships in Beijing.">Jamaican Usain Bolt has progressed to the men's 200 metres final at 2015 IAAF World Championships in Beijing.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/26/2201s893324.htm" title="China's "Ox-on-Bear" Sculpture Attracts Prayers for Plunging Stock Market"><img src="http://img02.abroad.imgcdc.com/english/news/china/54/20150826/448469_114289.jpg" width="160" height="120" alt="China's "Ox-on-Bear" Sculpture Attracts Prayers for Plunging Stock Market" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/26 21:40:04</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/26/2201s893324.htm" title="China's "Ox-on-Bear" Sculpture Attracts Prayers for Plunging Stock Market" class="title_default">China's "Ox-on-Bear" Sculpture Attracts Prayers for Plunging Stock Market</a></h3>
            <p class="item-infor" title="A sculpture of an ox sitting on a bear has become an unlikely sightseeing spot, attracting pilgrims who come to pray for the plunging stock market.">A sculpture of an ox sitting on a bear has become an unlikely sightseeing spot, attracting pilgrims who come to pray for the plunging stock market.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/business/2015-08/26/content_21713774.htm" title="Stocks Close Lower after Central Bank Cuts Rates"><img src="http://img01.abroad.imgcdc.com/english/news/business/56/20150826/448468_114288.jpg" width="160" height="103" alt="Stocks Close Lower after Central Bank Cuts Rates" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/08/26 21:38:24</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/business/2015-08/26/content_21713774.htm" title="Stocks Close Lower after Central Bank Cuts Rates" class="title_default">Stocks Close Lower after Central Bank Cuts Rates</a></h3>
            <p class="item-infor" title="Stocks closed lower on Wednesday after swinging between gains and losses, as the central bank's decision sent mixed message to markets.">Stocks closed lower on Wednesday after swinging between gains and losses, as the central bank's decision sent mixed message to markets.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-08/26/c_134557131.htm" title=""Go Away Mr. Tumor" Continues to Lead China's Box Office Sales"><img src="http://img03.abroad.imgcdc.com/english/news/showbiz/58/20150826/448466_114287.jpg" width="200" height="120" alt=""Go Away Mr. Tumor" Continues to Lead China's Box Office Sales" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/08/26 21:36:49</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-08/26/c_134557131.htm" title=""Go Away Mr. Tumor" Continues to Lead China's Box Office Sales" class="title_default">"Go Away Mr. Tumor" Continues to Lead China's Box Office Sales</a></h3>
            <p class="item-infor" title="Tear-jerking romance "Go Away Mr. Tumor" ruled China's box office in the week ending Aug. 23, pulling in 225 million yuan (35 million U.S. dollars).">Tear-jerking romance "Go Away Mr. Tumor" ruled China's box office in the week ending Aug. 23, pulling in 225 million yuan (35 million U.S. dollars).</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/26/2281s893307.htm" title="Palace Museum to Issue Colouring Books"><img src="http://img03.abroad.imgcdc.com/english/news/china/54/20150826/448464_114285.jpg" width="200" height="120" alt="Palace Museum to Issue Colouring Books" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/26 21:33:58</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/26/2281s893307.htm" title="Palace Museum to Issue Colouring Books" class="title_default">Palace Museum to Issue Colouring Books</a></h3>
            <p class="item-infor" title="The Palace Museum in Beijing will issue a series of colouring books to celebrate the 90th anniversary of its establishment this October. ">The Palace Museum in Beijing will issue a series of colouring books to celebrate the 90th anniversary of its establishment this October. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/cn_eu/2015-08/26/content_21706536.htm" title="Navy Completes Joint Beach Drill"><img src="http://img04.abroad.imgcdc.com/english/news/china/54/20150826/447825_114062.jpg" width="200" height="120" alt="Navy Completes Joint Beach Drill" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/26 09:56:10</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/cn_eu/2015-08/26/content_21706536.htm" title="Navy Completes Joint Beach Drill" class="title_default">Navy Completes Joint Beach Drill</a></h3>
            <p class="item-infor" title="The Chinese navy completed its first overseas joint beach landing drill on Tuesday as part of an ongoing marine exercise with Russia.">The Chinese navy completed its first overseas joint beach landing drill on Tuesday as part of an ongoing marine exercise with Russia.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/26/3961s893232.htm" title="Legislators Support Removal of Driving Restriction from Law Amendment"><img src="http://img04.abroad.imgcdc.com/english/news/china/54/20150826/447821_114059.jpg" width="160" height="120" alt="Legislators Support Removal of Driving Restriction from Law Amendment" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/26 09:52:34</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/26/3961s893232.htm" title="Legislators Support Removal of Driving Restriction from Law Amendment" class="title_default">Legislators Support Removal of Driving Restriction from Law Amendment</a></h3>
            <p class="item-infor" title="China's top lawmakers are moving to get rid of a widely-controversial driving ban from the country's Air Pollution Control Laws.">China's top lawmakers are moving to get rid of a widely-controversial driving ban from the country's Air Pollution Control Laws.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/china/tibet50years/2015-08/26/content_21706350.htm" title="National and Ethnic Unity Vital for Region, Xi Says"><img src="http://img03.abroad.imgcdc.com/english/news/china/54/20150826/447819_114058.jpg" width="200" height="120" alt="National and Ethnic Unity Vital for Region, Xi Says" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/26 09:50:50</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/china/tibet50years/2015-08/26/content_21706350.htm" title="National and Ethnic Unity Vital for Region, Xi Says" class="title_default">National and Ethnic Unity Vital for Region, Xi Says</a></h3>
            <p class="item-infor" title="President Xi Jinping has underlined national and ethnic unity as key for Tibet, vowing to focus on long-term, comprehensive stability and an "unswerving" anti-separatism battle.">President Xi Jinping has underlined national and ethnic unity as key for Tibet, vowing to focus on long-term, comprehensive stability and an "unswerving" anti-separatism battle.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/08/26/2982s893225.htm" title="Chinese Bank Offers Loan to Support Wheat Purchase"><img src="http://img02.abroad.imgcdc.com/english/news/china/54/20150826/447818_114057.jpg" width="200" height="120" alt="Chinese Bank Offers Loan to Support Wheat Purchase" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/08/26 09:48:55</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/08/26/2982s893225.htm" title="Chinese Bank Offers Loan to Support Wheat Purchase" class="title_default">Chinese Bank Offers Loan to Support Wheat Purchase</a></h3>
            <p class="item-infor" title="The Agricultural Development Bank, one of China's policy banks, has offered a loan of nearly 81.5 billion yuan to support wheat purchases">The Agricultural Development Bank, one of China's policy banks, has offered a loan of nearly 81.5 billion yuan to support wheat purchases</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/business/2015-08/26/content_21706702.htm" title="Bank Lowers Lending Rate to Ease Debts"><img src="http://img02.abroad.imgcdc.com/english/news/business/56/20150826/447808_114056.jpg" width="200" height="120" alt="Bank Lowers Lending Rate to Ease Debts" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/08/26 09:47:00</em><em class="hide">August 28 2015 08:56:43</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/business/2015-08/26/content_21706702.htm" title="Bank Lowers Lending Rate to Ease Debts" class="title_default">Bank Lowers Lending Rate to Ease Debts</a></h3>
            <p class="item-infor" title="China's central bank announced that it would lower the benchmark deposit and lending rates by 25 basis points beginning on Wednesday, hoping to further ease companies' debt burdens and curb expanding downward risks.">China's central bank announced that it would lower the benchmark deposit and lending rates by 25 basis points beginning on Wednesday, hoping to further ease companies' debt burdens and curb expanding downward risks.</p>
          </div>
        </div>
      </div>
      <div class="page-latest-more">
        <a href="#" id="page-latest-show-more"><em class="page-latest-more-icon">&nbsp;</em></a>
        <a href="/news/index.html" id="page-latest-click-more"><em class="page-latest-more-icon">Show More</em></a>
      </div>
    </div><!-- page-latest End -->
    <div class="page-left-ad"><div id='CH_ENG_TL_00001' class='adclass' pushtype='no'></div></div> 
    <div class="page-video">
      <h2 class="modTit"><strong><a href="/video/index.html">VIDEO</a></strong></h2>
      <div class="page-video-body">
        <div class="page-video-left">
          <script type="text/javascript" src="http://c.wrating.com/v2_pre.js"></script>
<!--noscript-->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="420" height="234">
            <param name="movie" value="http://english.china.com/videoPlayer/video.swf"/>
            <param name="quality" value="high"/>
            <param name="bgcolor" value="#ffffff"/>
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="true"/>
            <param name="wmode" value="Opaque">
            <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/08/0819todayinhistory.mp4&loop=1&autoplay=0"/>
            <!--[if!IE]>
            -->
            <object type="application/x-shockwave-flash" data="http://english.china.com/videoPlayer/video.swf" width="420" height="234">
              <param name="quality" value="high"/>
              <param name="bgcolor" value="#ffffff"/>
              <param name="allowScriptAccess" value="sameDomain"/>
              <param name="allowFullScreen" value="true"/>
              <param name="wmode" value="Opaque">
              <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/08/0819todayinhistory.mp4&loop=1&autoplay=0"/>
              <!--<![endif]-->
              <!--[if gte IE 6]>
              -->
              <p>
                Either scripts and active content are not permitted to run or Adobe Flash Player version 11.4.0 or greater is not installed.
              </p>
              <!--<![endif]-->
              <a href="http://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player"/>
              </a>
              <!--[if!IE]>--></object>
            <!--<![endif]-->
          </object>
          <!--/noscript-->
          <a href="/home/videobig/1299/20150819/441206.html" class="video-tit">Today in History: The War against Japanese Aggression, 1941.8.19</a>
        </div>
        <div class="page-video-right" id="page-video-right">
          <div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20150505/365661.html"><img src="http://img02.english.china.com/home/videosmall/1301/20150505/365674_88751.jpg" width="245" height="125" alt="My Chinese Life: Mark O'Connell--Driving Chinese Golf up to Par" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20150505/365661.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">My Chinese Life: Mark O'Connell--Driving Chinese Golf up to Par</strong></h3>
              <p class="item-infor">Coming from the most famous golf course in South Africa, Mark injects new life into the Chinese game. </p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div><div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20150420/350839.html"><img src="http://img04.english.china.com/home/videosmall/1301/20150421/352084_82774.jpg" width="245" height="125" alt="My Chinese Life: Andile Munyai--Back to School in Beijing" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20150420/350839.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">My Chinese Life: Andile Munyai--Back to School in Beijing</strong></h3>
              <p class="item-infor"></p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div><!-- page-video End -->
    <div class="page-mods">
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/travel/index.html">TRAVEL</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners"><img src="http://img03.english.china.com/travel/listright/mostpopular/1534/20150506/366181_88898.jpg" width="330" height="190" alt="Food Awards 2015: the winners" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners" class="title_default">Food Awards 2015: the winners</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12394/2015/08/11/2743s891245.htm" title="Japanese War Orphan Recounts Past" class="title_default">Japanese War Orphan Recounts Past</a></li><li><a href="http://english.cri.cn/6566/2014/12/25/44s858261.htm" title="Winter Nadam Kicks Off in North China" class="title_default">Winter Nadam Kicks Off in North China</a></li><li><a href="http://english.cri.cn/6566/2014/09/29/44s845986.htm" title="To Experience Authentic Taiwan Folk Art in Beijing" class="title_default">To Experience Authentic Taiwan Folk Art in Beijing</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/chinese/index.html">LEARN CHINESE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love"><img src="http://img04.english.china.com/home/learnpic/1315/20141124/211729_51886.jpg" width="330" height="190" alt="China's First Love" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love">China's First Love</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12514/2014/10/16/2001s848110.htm">Top 10 Popular Chinese TV Dramas Overseas</a></li><li><a href="http://english.cri.cn/12514/2014/10/17/2001s848240.htm">çµç¶ Chinese Pipa</a></li><li><a href="http://english.cri.cn/12514/2014/09/25/2001s845407.htm">Useful Shopping Sentences in Chinese</a></li>
          </ul>
        </div>

      </div>
      
      
    </div><!-- page-mods End -->
    <!--<div class="page-left-ad"><a href="#"><img src="/file/left-ad-2.jpg" /></a></div> page-left-ad End -->
    <!-- page-photos End -->
  </div><!-- page-left End -->
  <div class="page-right">
    
    <div class="page-right-ad noMarginTop">
      <div id='CH_ENG_HZH_00001' class='adclass' pushtype='no'></div>
    </div><!-- page-right-ad End -->
    <!--include virtual="/etc/right_top_ad_index.shtml" -->
      
    <!-- #15734 -->

    <!-- page-right-ad End -->
    
    <div class="page-watched">
      <h2 class="modTit"><strong>Most Watched</strong></h2>
      <div class="page-watched-body" id="rank-video">
      </div>
    </div><!-- page-watched End -->
    <div class="page-popular">
      <h2 class="modTit"><strong>Most Popular</strong></h2>
      <ul class="page-popular-body" id="rank-list"></ul>
    </div><!-- page-popular End -->
    <div class="page-right-ad"><div id='CH_ENG_AN_00001' class='adclass' pushtype='no'></div></div> 
    <div class="page-tochina">
      <h2 class="modTit"><strong>Tune in to China</strong></h2>
      <div class="page-tochina-body">
        <div class="item radio-news">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="title">-</h3>
              <h4 id="playtime">-</h4>
              <a href="http://english.cri.cn/7146/2012/12/03/301s736372.htm" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/am846.wsx" class="item-play"></a>
          </div>
        </div>
        <div class="item radio-ez">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="nexttitle">-</h3>
              <h4 id="nexttime">-</h4>
              <a href="http://english.cri.cn/easyfm/ezplaytime.html" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/fm915.wsx" class="item-play"></a>
          </div>
        </div>
      </div>
    </div><!-- page-tochina End -->

    <!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://www.chinahighlights.com/tour/beijingtour/forbidden-city-heritage-walk.htm"><img src="http://img02.english.china.com/home/imgtj2/11771/20150729/427302_107797.gif" width="300" height="250" alt="ææ¸¸åä½æ¨è" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->
    <div class="page-hotListening">
      <h2 class="page-hotListening-tit"><strong>Hot Listening</strong></h2>
      <div class="page-hotListening-body">
        <div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/pik.htm"><img src="http://img03.english.china.com/audio/right/hot/1691/20131224/4933_1370.jpg" width="120" height="90" alt="People in the Know" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/pik.htm">People in the Know</a><a href="http://english.cri.cn/cribb/plus/pik.htm" class="icon-horn"></a></h3>
            <p class="item-infor">PIK gives you insights to the world through interviews.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/today.htm"><img src="http://img01.english.china.com/audio/right/hot/1691/20131224/4931_1369.jpg" width="120" height="90" alt="Today" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/today.htm">Today</a><a href="http://english.cri.cn/cribb/plus/today.htm" class="icon-horn"></a></h3>
            <p class="item-infor">A news magazine show with in-depth panel discussions.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/easymorning.html"><img src="http://img04.english.china.com/audio/right/hot/1691/20131224/4928_1368.jpg" width="120" height="90" alt="EZ Morning" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/easymorning.html">EZ Morning</a><a href="http://english.cri.cn/easyfm/easymorning.html" class="icon-horn"></a></h3>
            <p class="item-infor">It lights up your mornings with interesting chit-chats.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/ezwheel.html"><img src="http://img01.english.china.com/audio/right/hot/1691/20131224/4927_1367.jpg" width="120" height="90" alt="More to Learn" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/ezwheel.html">More to Learn</a><a href="http://english.cri.cn/easyfm/ezwheel.html" class="icon-horn"></a></h3>
            <p class="item-infor">More to Learn is filled up with English stories and anecdotes.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/hour.html"><img src="http://img01.english.china.com/audio/right/hot/1691/20131224/4923_1366.jpg" width="120" height="90" alt="The Beijing Hour" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/hour.html">The Beijing Hour</a><a href="http://english.cri.cn/easyfm/hour.html" class="icon-horn"></a></h3>
            <p class="item-infor">It opens up the world to you with latest news updates.</p>
          </div>
        </div>
      </div>
    </div><!-- page-hotListening End -->

    <div class="page-mobile">
      <h2 class="modTit"><strong>Mobile</strong></h2>
      <div class="page-mobile-body">
        <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=539404062&mt=8&s=143441" class="iphone" title="Mobile Iphone">Iphone</a>
      </div>
    </div><!-- page-mobile End -->

    <div class="page-cooperation">
      <h2 class="modTit"><strong>Cooperation</strong></h2>
      <div class="page-cooperation-body">
        <a href="http://gc.wrating.com/click.php?a=&c=860099-1000099998&cs=341_285_2559_860010_400000000&ds=354_355_356_357_358&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dmobi.mgeek.TunnyBrowser%26referrer%3Dchannel_id%253Dchinacom%2526utm_source%253Dchinacom"><img src="/file/logo-dolphin-140-90.png" height="90" width="140"></a>
      </div>
    </div><!-- page-cooperation End -->

  </div><!-- page-right End -->
</div><!-- page-main End -->

<script>
window.collectMethod_rank = window.collectMethod_rank || [];
collectMethod_rank.push(function () {
  setRank("rank-video", 3, "http://english.china.com/js/english_43_day.js", "video", function(){
    setRank("rank-list", 5, "http://english.china.com/js/english_22_day.js", "list");
  });
});
</script>

<!-- /etc/channelsitemap.shtml Start -->
<div class="page-map">
  <div class="page-map-body maxWidth">
  <dl class="item">
      <dt><a href="http://english.china.com/news/index.html">News:</a></dt>
      <dd>
        <a href="http://english.china.com/news/china/index.html">China</a>
        <a href="http://english.china.com/news/world/index.html">World</a>
        <a href="http://english.china.com/news/business/index.html">Business</a>
        <a href="http://english.china.com/news/sports/index.html">Sports</a>
        <a href="http://english.china.com/news/showbiz/index.html">Showbiz</a>
		<a href="http://english.china.com/audio/index.html">Audio</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/video/index.html">Video:</a></dt>
      <dd>
        <a href="http://english.china.com/video/c4/index.html">C4</a>
        <a href="http://english.china.com/video/life/index.html">My Chinese Life</a>
        <a href="http://english.china.com/video/thesoundstage/index.html">The Sound Stage</a>
        <a href="http://english.china.com/video/chinarevealed/index.html">China Revealed</a>
        <a href="http://english.china.com/video/showbiz/index.html">Showbiz Video</a>
        <a href="http://english.china.com/video/tour/index.html">Travel Video</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/photos/index.html">Photos:</a></dt>
      <dd>
        <a href="http://english.china.com/photos/china/index.html">China</a>
        <a href="http://english.china.com/photos/world/index.html">World</a>
        <a href="http://english.china.com/photos/fun/index.html">Fun</a>
        <a href="http://english.china.com/photos/travel/index.html">Travel</a>
        <a href="http://english.china.com/photos/entertainment/index.html">Entertainment</a>
        <a href="http://english.china.com/photos/sports/index.html">Sports</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/travel/index.html">Travel:</a></dt>
      <dd>
        <a href="http://english.china.com/travel/beijing/index.html">Beijing</a>
        <a href="http://english.china.com/travel/shanghai/index.html">Shanghai</a>
        <a href="http://english.china.com/travel/guangzhou/index.html">Guangzhou</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/lifestyle/index.html">Lifestyle:</a></dt>
      <dd>        
        <a href="http://english.china.com/lifestyle/livemusic/index.html">Live Music</a>
        <a href="http://english.china.com/lifestyle/opera/index.html">Opera & Classical</a>
        <a href="http://english.china.com/lifestyle/movies/index.html">Movies</a>
        <a href="http://english.china.com/lifestyle/traditionalshows/index.html">Traditional Shows</a>
        <a href="http://english.china.com/lifestyle/exhibitions/index.html">Exhibitions</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/chinese/index.html">Learn Chinese:</a></dt>
      <dd>
        <a href="http://english.china.com/chinese/studio/index.html">Chinese Studio</a>
        <a href="http://english.china.com/chinese/living/index.html">Living Chinese</a>
        <a href="http://english.china.com/chinese/everyday/index.html">Everyday Chinese</a>
        <a href="http://english.china.com/chinese/justforfun/index.html">Just For Fun</a>
        <a href="http://english.china.com/chinese/culture/index.html">Chinese Culture</a>
        <a href="http://english.china.com/chinese/buzzwords/index.html">Buzzwords</a>        
      </dd>
    </dl>      
  </div>
</div><!-- page-map End -->
<!-- /etc/channelsitemap.shtml End -->

<div class="page-link">
  <div class="page-link-body maxWidth">
    <a href="http://english.cri.cn/">CRIENGLISH.com</a>|<a href="http://www.chinadaily.com.cn/">China Daily</a>|<a href="http://www.xinhuanet.com/english/">Xinhua</a>|<a href="http://www.china.org.cn/index.htm">China.org.cn</a>|<a href="http://english.cntv.cn/">CNTV</a>
  </div>
</div><!-- page-link End -->

<!-- /etc/channelcopyright.shtml Start -->
<div class="page-footer">
  <div class="page-foot-link">
    <a href="/about/">About China.com</a>|<a href="/about/gmg.html">About GMG</a>|<a href="/ad/">Ad Services</a>|<a href="/contact/">Contact Information</a>|<a href="/copyright/">Copyright Notice</a>
  </div>
  <p><!--E-mail to:<a href="mailto:english@bj.china.com">english@bj.china.com</a><br />-->Copyright &copy; China.com All Rights Reserved</p>
</div><!-- page-footer End -->

<script src="/js/require.min.js" data-main="/js/main"></script>

<!-- START WRating v1.0 -->
<script type="text/javascript" src="http://c.wrating.com/a1.js">
</script>
<script type="text/javascript">
var vjAcc="860010-0446010000";
var wrUrl="http://c.wrating.com/";
vjTrack("");
</script>
<noscript><img src="http://c.wrating.com/a.gif?a=&c=860010-0446010000" width="1" 
height="1"/></noscript>
<!-- END WRating v1.0 -->

<!-- Start Alexa Certify Javascript #13481-->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S6Upi1awA+00a/", domain:"china.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S6Upi1awA+00a/" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript #13481-->


<!-- Start Google Analytics #16010-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60581848-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics #16010-->

<!-- /etc/channelcopyright.shtml End -->
</body>
</html>