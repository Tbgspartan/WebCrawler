<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-d439dbc.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-d439dbc.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-d439dbc.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-d439dbc.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-d439dbc.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'd439dbc',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-d439dbc.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/adobe%20dreamweaver%20cs6%20crack/" class="tag3">adobe dreamweaver cs6 crack</a>
	<a href="/search/agents%20of%20shield/" class="tag2">agents of shield</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/arrow/" class="tag2">arrow</a>
	<a href="/search/bahubali/" class="tag2">bahubali</a>
	<a href="/search/boruto%20naruto%20the%20movie/" class="tag3">boruto naruto the movie</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag2">fear the walking dead</a>
	<a href="/search/flash/" class="tag2">flash</a>
	<a href="/search/flash%20s02e02/" class="tag2">flash s02e02</a>
	<a href="/search/french/" class="tag2">french</a>
	<a href="/search/gog/" class="tag2">gog</a>
	<a href="/search/gotham/" class="tag2">gotham</a>
	<a href="/search/hindi/" class="tag10">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/hotel%20transylvania%202/" class="tag2">hotel transylvania 2</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/izombie/" class="tag2">izombie</a>
	<a href="/search/jazba/" class="tag2">jazba</a>
	<a href="/search/kat%20ph%20com/" class="tag3">kat ph com</a>
	<a href="/search/limitless/" class="tag3">limitless</a>
	<a href="/search/limitless%20s01e04/" class="tag2">limitless s01e04</a>
	<a href="/search/mac/" class="tag2">mac</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/ncis/" class="tag2">ncis</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/ripsalot/" class="tag3">ripsalot</a>
	<a href="/search/scream%20queens/" class="tag2">scream queens</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20blacklist/" class="tag2">the blacklist</a>
	<a href="/search/the%20flash/" class="tag5">the flash</a>
	<a href="/search/the%20flash%20s02e02/" class="tag6">the flash s02e02</a>
	<a href="/search/the%20flash%20season%202%20is%20safe%201/" class="tag4">the flash season 2 is safe 1</a>
	<a href="/search/the%20martian/" class="tag2">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag6">the walking dead</a>
	<a href="/search/the%20walking%20dead%20s06e01/" class="tag2">the walking dead s06e01</a>
	<a href="/search/walking%20dead/" class="tag2">walking dead</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400692,0" class="icommentjs icon16" href="/the-new-adventures-of-peter-pan-2015-hdrip-xvid-ac3-evo-t11400692.html#comment"> <em class="iconvalue">45</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-new-adventures-of-peter-pan-2015-hdrip-xvid-ac3-evo-t11400692.html" class="cellMainLink">The New Adventures of Peter Pan 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1510355387">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T16:30:04+00:00">12 Oct 2015, 16:30:04</span></td>
			<td class="green center">6762</td>
			<td class="red lasttd center">6112</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11408064,0" class="icommentjs icon16" href="/the-cokeville-miracle-2015-1080p-brrip-x264-yify-t11408064.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-cokeville-miracle-2015-1080p-brrip-x264-yify-t11408064.html" class="cellMainLink">The Cokeville Miracle (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1547389085">1.44 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T22:31:33+00:00">13 Oct 2015, 22:31:33</span></td>
			<td class="green center">6194</td>
			<td class="red lasttd center">5477</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11410602,0" class="icommentjs icon16" href="/vacation-2015-web-dl-1080p-dual-audio-rus-eng-t11410602.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vacation-2015-web-dl-1080p-dual-audio-rus-eng-t11410602.html" class="cellMainLink">Vacation (2015) WEB-DL 1080p | [DUAL AUDIO] | [RUS-ENG]</a></div>
			</td>
			<td class="nobr center" data-sort="3789767647">3.53 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T10:09:18+00:00">14 Oct 2015, 10:09:18</span></td>
			<td class="green center">6493</td>
			<td class="red lasttd center">2121</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11402474,0" class="icommentjs icon16" href="/inside-out-2015-1080p-web-dl-x264-aac-jyk-t11402474.html#comment"> <em class="iconvalue">53</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/inside-out-2015-1080p-web-dl-x264-aac-jyk-t11402474.html" class="cellMainLink">Inside Out 2015 1080p WEB-DL x264 AAC-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2635370902">2.45 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T00:19:06+00:00">13 Oct 2015, 00:19:06</span></td>
			<td class="green center">4593</td>
			<td class="red lasttd center">3586</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400910,0" class="icommentjs icon16" href="/garm-wars-the-last-druid-2014-hdrip-xvid-ac3-evo-t11400910.html#comment"> <em class="iconvalue">65</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/garm-wars-the-last-druid-2014-hdrip-xvid-ac3-evo-t11400910.html" class="cellMainLink">Garm Wars The Last Druid 2014 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1507899718">1.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:18:33+00:00">12 Oct 2015, 17:18:33</span></td>
			<td class="green center">4512</td>
			<td class="red lasttd center">3155</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11411012,0" class="icommentjs icon16" href="/the-pack-2015-hdrip-xvid-etrg-t11411012.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-pack-2015-hdrip-xvid-etrg-t11411012.html" class="cellMainLink">The Pack 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="747158813">712.55 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T11:39:19+00:00">14 Oct 2015, 11:39:19</span></td>
			<td class="green center">3458</td>
			<td class="red lasttd center">3139</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11397819,0" class="icommentjs icon16" href="/painkillers-2015-hdrip-xvid-ac3-evo-t11397819.html#comment"> <em class="iconvalue">35</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/painkillers-2015-hdrip-xvid-ac3-evo-t11397819.html" class="cellMainLink">Painkillers 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1494227624">1.39 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T06:08:20+00:00">12 Oct 2015, 06:08:20</span></td>
			<td class="green center">2980</td>
			<td class="red lasttd center">2524</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11401382,0" class="icommentjs icon16" href="/the-ouija-exorcism-2015-hdrip-xvid-ac3-evo-t11401382.html#comment"> <em class="iconvalue">48</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-ouija-exorcism-2015-hdrip-xvid-ac3-evo-t11401382.html" class="cellMainLink">The Ouija Exorcism 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1517696385">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T18:46:45+00:00">12 Oct 2015, 18:46:45</span></td>
			<td class="green center">2433</td>
			<td class="red lasttd center">1892</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402459,0" class="icommentjs icon16" href="/the-land-before-time-1988-1080p-brrip-x264-yify-t11402459.html#comment"> <em class="iconvalue">53</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-land-before-time-1988-1080p-brrip-x264-yify-t11402459.html" class="cellMainLink">The Land Before Time (1988) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1322775060">1.23 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T00:08:56+00:00">13 Oct 2015, 00:08:56</span></td>
			<td class="green center">2516</td>
			<td class="red lasttd center">1193</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11407960,0" class="icommentjs icon16" href="/disaster-wars-earthquake-vs-tsunami-2013-1080p-brrip-x264-yify-t11407960.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/disaster-wars-earthquake-vs-tsunami-2013-1080p-brrip-x264-yify-t11407960.html" class="cellMainLink">Disaster Wars: Earthquake vs. Tsunami (2013) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1325633768">1.23 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T21:57:12+00:00">13 Oct 2015, 21:57:12</span></td>
			<td class="green center">1491</td>
			<td class="red lasttd center">1643</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11408290,0" class="icommentjs icon16" href="/spl-2-a-time-for-consequences-2015-1080p-bluray-x264-dts-jyk-t11408290.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/spl-2-a-time-for-consequences-2015-1080p-bluray-x264-dts-jyk-t11408290.html" class="cellMainLink">SPL 2 A Time for Consequences 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3245608423">3.02 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T00:07:40+00:00">14 Oct 2015, 00:07:40</span></td>
			<td class="green center">1548</td>
			<td class="red lasttd center">1376</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11408235,0" class="icommentjs icon16" href="/rush-hour-2-2001-1080p-brrip-x264-yify-t11408235.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/rush-hour-2-2001-1080p-brrip-x264-yify-t11408235.html" class="cellMainLink">Rush Hour 2 (2001) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1324961869">1.23 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T23:37:25+00:00">13 Oct 2015, 23:37:25</span></td>
			<td class="green center">1635</td>
			<td class="red lasttd center">1122</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11403391,0" class="icommentjs icon16" href="/suburban-gothic-2014-720p-brrip-x264-yify-t11403391.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/suburban-gothic-2014-720p-brrip-x264-yify-t11403391.html" class="cellMainLink">Suburban Gothic (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="735654595">701.57 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T04:34:59+00:00">13 Oct 2015, 04:34:59</span></td>
			<td class="green center">1737</td>
			<td class="red lasttd center">745</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11413910,0" class="icommentjs icon16" href="/danny-collins-2015-1080p-bluray-x264-dts-jyk-t11413910.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/danny-collins-2015-1080p-bluray-x264-dts-jyk-t11413910.html" class="cellMainLink">Danny Collins 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2899280007">2.7 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T23:13:19+00:00">14 Oct 2015, 23:13:19</span></td>
			<td class="green center">1239</td>
			<td class="red lasttd center">743</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11406575,0" class="icommentjs icon16" href="/5-to-7-2014-1080p-brrip-x264-yify-t11406575.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/5-to-7-2014-1080p-brrip-x264-yify-t11406575.html" class="cellMainLink">5 to 7 (2014) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1550458379">1.44 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T16:52:21+00:00">13 Oct 2015, 16:52:21</span></td>
			<td class="green center">1046</td>
			<td class="red lasttd center">929</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11414109,0" class="icommentjs icon16" href="/arrow-s04e02-hdtv-x264-lol-ettv-t11414109.html#comment"> <em class="iconvalue">129</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrow-s04e02-hdtv-x264-lol-ettv-t11414109.html" class="cellMainLink">Arrow S04E02 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="297646169">283.86 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T01:01:21+00:00">15 Oct 2015, 01:01:21</span></td>
			<td class="green center">27106</td>
			<td class="red lasttd center">12492</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11408457,0" class="icommentjs icon16" href="/the-flash-2014-s02e02-hdtv-x264-lol-ettv-t11408457.html#comment"> <em class="iconvalue">273</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-flash-2014-s02e02-hdtv-x264-lol-ettv-t11408457.html" class="cellMainLink">The Flash 2014 S02E02 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="241247241">230.07 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T01:25:50+00:00">14 Oct 2015, 01:25:50</span></td>
			<td class="green center">27823</td>
			<td class="red lasttd center">4929</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11402293,0" class="icommentjs icon16" href="/the-big-bang-theory-s09e04-hdtv-x264-lol-ettv-t11402293.html#comment"> <em class="iconvalue">201</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-big-bang-theory-s09e04-hdtv-x264-lol-ettv-t11402293.html" class="cellMainLink">The Big Bang Theory S09E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="138541040">132.12 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T23:04:18+00:00">12 Oct 2015, 23:04:18</span></td>
			<td class="green center">17619</td>
			<td class="red lasttd center">1696</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11414569,0" class="icommentjs icon16" href="/american-horror-story-s05e02-proper-hdtv-x264-killers-ettv-t11414569.html#comment"> <em class="iconvalue">47</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/american-horror-story-s05e02-proper-hdtv-x264-killers-ettv-t11414569.html" class="cellMainLink">American Horror Story S05E02 PROPER HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="571118341">544.66 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T03:58:20+00:00">15 Oct 2015, 03:58:20</span></td>
			<td class="green center">12223</td>
			<td class="red lasttd center">5908</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11402549,0" class="icommentjs icon16" href="/gotham-s02e04-hdtv-x264-lol-ettv-t11402549.html#comment"> <em class="iconvalue">185</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gotham-s02e04-hdtv-x264-lol-ettv-t11402549.html" class="cellMainLink">Gotham S02E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="240766052">229.61 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T01:00:19+00:00">13 Oct 2015, 01:00:19</span></td>
			<td class="green center">13525</td>
			<td class="red lasttd center">1787</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11408630,0" class="icommentjs icon16" href="/marvels-agents-of-s-h-i-e-l-d-s03e03-internal-hdtv-x264-killers-ettv-t11408630.html#comment"> <em class="iconvalue">153</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/marvels-agents-of-s-h-i-e-l-d-s03e03-internal-hdtv-x264-killers-ettv-t11408630.html" class="cellMainLink">Marvels Agents of S H I E L D S03E03 INTERNAL HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="285382744">272.16 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T02:11:20+00:00">14 Oct 2015, 02:11:20</span></td>
			<td class="green center">12745</td>
			<td class="red lasttd center">2675</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11414247,0" class="icommentjs icon16" href="/supernatural-s11e02-hdtv-x264-lol-ettv-t11414247.html#comment"> <em class="iconvalue">68</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supernatural-s11e02-hdtv-x264-lol-ettv-t11414247.html" class="cellMainLink">Supernatural S11E02 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="240247870">229.12 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T01:59:26+00:00">15 Oct 2015, 01:59:26</span></td>
			<td class="green center">10894</td>
			<td class="red lasttd center">4032</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11414197,0" class="icommentjs icon16" href="/modern-family-s07e04-hdtv-x264-fleet-rartv-t11414197.html#comment"> <em class="iconvalue">36</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/modern-family-s07e04-hdtv-x264-fleet-rartv-t11414197.html" class="cellMainLink">Modern Family S07E04 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="212281057">202.45 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T01:32:27+00:00">15 Oct 2015, 01:32:27</span></td>
			<td class="green center">9751</td>
			<td class="red lasttd center">3169</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11414205,0" class="icommentjs icon16" href="/empire-2015-s02e04-hdtv-x264-killers-ettv-t11414205.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/empire-2015-s02e04-hdtv-x264-killers-ettv-t11414205.html" class="cellMainLink">Empire 2015 S02E04 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="357785305">341.21 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T01:36:35+00:00">15 Oct 2015, 01:36:35</span></td>
			<td class="green center">7462</td>
			<td class="red lasttd center">4206</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402724,0" class="icommentjs icon16" href="/blindspot-s01e04-hdtv-x264-lol-ettv-t11402724.html#comment"> <em class="iconvalue">131</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blindspot-s01e04-hdtv-x264-lol-ettv-t11402724.html" class="cellMainLink">Blindspot S01E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="323641233">308.65 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T02:02:17+00:00">13 Oct 2015, 02:02:17</span></td>
			<td class="green center">8489</td>
			<td class="red lasttd center">1267</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11397039,0" class="icommentjs icon16" href="/quantico-s01e03-hdtv-x264-lol-ettv-t11397039.html#comment"> <em class="iconvalue">204</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/quantico-s01e03-hdtv-x264-lol-ettv-t11397039.html" class="cellMainLink">Quantico S01E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="302660657">288.64 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T02:03:19+00:00">12 Oct 2015, 02:03:19</span></td>
			<td class="green center">8475</td>
			<td class="red lasttd center">1191</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11397194,0" class="icommentjs icon16" href="/homeland-s05e02-web-dl-x264-fum-ettv-t11397194.html#comment"> <em class="iconvalue">70</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/homeland-s05e02-web-dl-x264-fum-ettv-t11397194.html" class="cellMainLink">Homeland S05E02 WEB-DL x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="278960539">266.04 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T02:42:24+00:00">12 Oct 2015, 02:42:24</span></td>
			<td class="green center">8110</td>
			<td class="red lasttd center">865</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11402913,0" class="icommentjs icon16" href="/fargo-s02e01-hdtv-x264-killers-ettv-t11402913.html#comment"> <em class="iconvalue">115</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fargo-s02e01-hdtv-x264-killers-ettv-t11402913.html" class="cellMainLink">Fargo S02E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="280754123">267.75 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T03:12:23+00:00">13 Oct 2015, 03:12:23</span></td>
			<td class="green center">7283</td>
			<td class="red lasttd center">683</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11414368,0" class="icommentjs icon16" href="/south-park-s19e04-hdtv-x264-killers-ettv-t11414368.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/south-park-s19e04-hdtv-x264-killers-ettv-t11414368.html" class="cellMainLink">South Park S19E04 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="107376898">102.4 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T02:36:47+00:00">15 Oct 2015, 02:36:47</span></td>
			<td class="green center">5978</td>
			<td class="red lasttd center">1288</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11396866,0" class="icommentjs icon16" href="/once-upon-a-time-s05e03-hdtv-x264-killers-ettv-t11396866.html#comment"> <em class="iconvalue">78</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/once-upon-a-time-s05e03-hdtv-x264-killers-ettv-t11396866.html" class="cellMainLink">Once Upon A Time S05E03 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="356212275">339.71 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T01:07:16+00:00">12 Oct 2015, 01:07:16</span></td>
			<td class="green center">5841</td>
			<td class="red lasttd center">571</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400816,0" class="icommentjs icon16" href="/va-fitness-hits-vol-1-2015-mp3-320-kbps-t11400816.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-fitness-hits-vol-1-2015-mp3-320-kbps-t11400816.html" class="cellMainLink">VA - Fitness Hits Vol. 1 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="650968735">620.81 <span>MB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:01:04+00:00">12 Oct 2015, 17:01:04</span></td>
			<td class="green center">864</td>
			<td class="red lasttd center">403</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11415652,0" class="icommentjs icon16" href="/mp3-new-releases-2015-week-41-glodls-t11415652.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-41-glodls-t11415652.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 41 [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="3710504864">3.46 <span>GB</span></td>
			<td class="center">452</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T09:20:35+00:00">15 Oct 2015, 09:20:35</span></td>
			<td class="green center">247</td>
			<td class="red lasttd center">971</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11411143,0" class="icommentjs icon16" href="/va-halloween-party-music-2015-mp3-320kbps-h4ckus-glodls-t11411143.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-halloween-party-music-2015-mp3-320kbps-h4ckus-glodls-t11411143.html" class="cellMainLink">VA - Halloween Party Music [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="504331902">480.97 <span>MB</span></td>
			<td class="center">56</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T12:14:24+00:00">14 Oct 2015, 12:14:24</span></td>
			<td class="green center">523</td>
			<td class="red lasttd center">198</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11403600,0" class="icommentjs icon16" href="/jean-michel-jarre-electronica-1-the-time-machine-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11403600.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jean-michel-jarre-electronica-1-the-time-machine-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11403600.html" class="cellMainLink">Jean-Michel Jarre â Electronica 1. The Time Machine [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="167841479">160.07 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T05:24:36+00:00">13 Oct 2015, 05:24:36</span></td>
			<td class="green center">563</td>
			<td class="red lasttd center">84</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11411085,0" class="icommentjs icon16" href="/va-ibiza-2015-2015-mp3-320kbps-h4ckus-glodls-t11411085.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ibiza-2015-2015-mp3-320kbps-h4ckus-glodls-t11411085.html" class="cellMainLink">VA - IBIZA 2015 [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="830633097">792.15 <span>MB</span></td>
			<td class="center">54</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T11:59:41+00:00">14 Oct 2015, 11:59:41</span></td>
			<td class="green center">292</td>
			<td class="red lasttd center">192</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11400923,0" class="icommentjs icon16" href="/va-happy-hardcore-top-50-best-ever-2015-mp3-320-kbps-t11400923.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-happy-hardcore-top-50-best-ever-2015-mp3-320-kbps-t11400923.html" class="cellMainLink">VA - Happy Hardcore Top 50 Best Ever (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="375624483">358.22 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:21:44+00:00">12 Oct 2015, 17:21:44</span></td>
			<td class="green center">237</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400901,0" class="icommentjs icon16" href="/va-endless-deep-finest-deep-house-grooves-vol-1-2015-mp3-320-kbps-t11400901.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-endless-deep-finest-deep-house-grooves-vol-1-2015-mp3-320-kbps-t11400901.html" class="cellMainLink">VA - Endless Deep: Finest Deep House Grooves Vol. 1 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="238661371">227.61 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:17:38+00:00">12 Oct 2015, 17:17:38</span></td>
			<td class="green center">221</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11399407,0" class="icommentjs icon16" href="/hardwell-feat-jake-reese-mad-world-original-mix-320-kbps-edm-rg-t11399407.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/hardwell-feat-jake-reese-mad-world-original-mix-320-kbps-edm-rg-t11399407.html" class="cellMainLink">Hardwell feat. Jake Reese - Mad World (Original Mix) [320 Kbps] [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="10792280">10.29 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T12:11:35+00:00">12 Oct 2015, 12:11:35</span></td>
			<td class="green center">219</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11407403,0" class="icommentjs icon16" href="/va-privilege-ibiza-worlds-biggest-club-3cd-2015-mp3-320-kbps-t11407403.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-privilege-ibiza-worlds-biggest-club-3cd-2015-mp3-320-kbps-t11407403.html" class="cellMainLink">VA - Privilege Ibiza: Worlds Biggest Club [3CD] (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="406733639">387.89 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T19:13:52+00:00">13 Oct 2015, 19:13:52</span></td>
			<td class="green center">184</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11404784,0" class="icommentjs icon16" href="/beach-house-thank-your-lucky-stars-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11404784.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/beach-house-thank-your-lucky-stars-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11404784.html" class="cellMainLink">Beach House - Thank Your Lucky Stars (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="98993693">94.41 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T10:21:48+00:00">13 Oct 2015, 10:21:48</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11412671,0" class="icommentjs icon16" href="/va-low-frequencies-drum-bass-dubstep-break-beat-grooves-2015-mp3-320-kbps-t11412671.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-low-frequencies-drum-bass-dubstep-break-beat-grooves-2015-mp3-320-kbps-t11412671.html" class="cellMainLink">VA - Low Frequencies (Drum &amp; Bass, Dubstep &amp; Break Beat Grooves) (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="202356123">192.98 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T17:54:56+00:00">14 Oct 2015, 17:54:56</span></td>
			<td class="green center">137</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11399809,0" class="icommentjs icon16" href="/b-a-r-2015-week-40-glodls-t11399809.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/b-a-r-2015-week-40-glodls-t11399809.html" class="cellMainLink">B.A.R. 2015 Week 40 - [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4456946295">4.15 <span>GB</span></td>
			<td class="center">554</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T13:27:17+00:00">12 Oct 2015, 13:27:17</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">117</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400116,0" class="icommentjs icon16" href="/kiss-fm-fresh-top-40-octombrie-2015-romanian-extremlymtorrents-t11400116.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/kiss-fm-fresh-top-40-octombrie-2015-romanian-extremlymtorrents-t11400116.html" class="cellMainLink">Kiss Fm - Fresh Top 40 - Octombrie 2015 Romanian ExtremlymTorrents</a></div>
			</td>
			<td class="nobr center" data-sort="140062208">133.57 <span>MB</span></td>
			<td class="center">40</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T14:20:42+00:00">12 Oct 2015, 14:20:42</span></td>
			<td class="green center">109</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11416799,0" class="icommentjs icon16" href="/demi-lovato-confident-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11416799.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/demi-lovato-confident-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11416799.html" class="cellMainLink">Demi Lovato - Confident [Deluxe Edition] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="132456219">126.32 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T14:22:03+00:00">15 Oct 2015, 14:22:03</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">78</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11409612,0" class="icommentjs icon16" href="/beatport-singles-ade-edition-12-10-2015-edm-electro-house-future-house-hardstyle-trance-progressive-house-t11409612.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/beatport-singles-ade-edition-12-10-2015-edm-electro-house-future-house-hardstyle-trance-progressive-house-t11409612.html" class="cellMainLink">Beatport Singles (ADE Edition) - 12.10.2015 (EDM,Electro House,Future House,Hardstyle,Trance,Progressive House)</a></div>
			</td>
			<td class="nobr center" data-sort="4125643045">3.84 <span>GB</span></td>
			<td class="center">269</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T05:48:58+00:00">14 Oct 2015, 05:48:58</span></td>
			<td class="green center">55</td>
			<td class="red lasttd center">106</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11401982,0" class="icommentjs icon16" href="/the-witcher-3-wild-hunt-hearts-of-stone-gog-t11401982.html#comment"> <em class="iconvalue">195</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-witcher-3-wild-hunt-hearts-of-stone-gog-t11401982.html" class="cellMainLink">The Witcher 3: Wild Hunt - Hearts of Stone (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="4563157730">4.25 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T21:55:50+00:00">12 Oct 2015, 21:55:50</span></td>
			<td class="green center">1547</td>
			<td class="red lasttd center">1077</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11411579,0" class="icommentjs icon16" href="/the-age-of-decadence-codex-t11411579.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-age-of-decadence-codex-t11411579.html" class="cellMainLink">The Age of Decadence-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="1186043479">1.1 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T13:36:16+00:00">14 Oct 2015, 13:36:16</span></td>
			<td class="green center">524</td>
			<td class="red lasttd center">161</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11404187,0" class="icommentjs icon16" href="/wasteland-2-directors-cut-codex-t11404187.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/wasteland-2-directors-cut-codex-t11404187.html" class="cellMainLink">Wasteland 2 Directors Cut-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="12023601002">11.2 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T07:45:06+00:00">13 Oct 2015, 07:45:06</span></td>
			<td class="green center">303</td>
			<td class="red lasttd center">391</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11413813,0" class="icommentjs icon16" href="/minecraft-story-mode-episode-1-gog-t11413813.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/minecraft-story-mode-episode-1-gog-t11413813.html" class="cellMainLink">Minecraft: Story Mode - Episode 1 (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="754790972">719.82 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T22:49:56+00:00">14 Oct 2015, 22:49:56</span></td>
			<td class="green center">397</td>
			<td class="red lasttd center">161</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11401800,0" class="icommentjs icon16" href="/lula-the-sexy-empire-good-old-games-t11401800.html#comment"> <em class="iconvalue">30</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/lula-the-sexy-empire-good-old-games-t11401800.html" class="cellMainLink">Lula The Sexy Empire - Good Old Games</a></div>
			</td>
			<td class="nobr center" data-sort="371223712">354.03 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T20:41:50+00:00">12 Oct 2015, 20:41:50</span></td>
			<td class="green center">353</td>
			<td class="red lasttd center">92</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11413177,0" class="icommentjs icon16" href="/chimeras-2-the-signs-of-prophecy-collector-s-edition-asg-t11413177.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/chimeras-2-the-signs-of-prophecy-collector-s-edition-asg-t11413177.html" class="cellMainLink">Chimeras 2 - The Signs of Prophecy Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="871823894">831.44 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T19:44:17+00:00">14 Oct 2015, 19:44:17</span></td>
			<td class="green center">270</td>
			<td class="red lasttd center">86</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11412570,0" class="icommentjs icon16" href="/dragon-age-inquisition-digital-deluxe-edition-update-10-2014-pc-repack-by-fitgirl-t11412570.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dragon-age-inquisition-digital-deluxe-edition-update-10-2014-pc-repack-by-fitgirl-t11412570.html" class="cellMainLink">Dragon Age: Inquisition - Digital Deluxe Edition [Update 10] (2014) PC | RePack by FitGirl</a></div>
			</td>
			<td class="nobr center" data-sort="21836603862">20.34 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T17:26:05+00:00">14 Oct 2015, 17:26:05</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">219</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11412153,0" class="icommentjs icon16" href="/might-and-magic-heroes-vii-update-v1-2-codex-t11412153.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/might-and-magic-heroes-vii-update-v1-2-codex-t11412153.html" class="cellMainLink">Might and Magic Heroes VII Update v1 2-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="517784635">493.8 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T15:36:25+00:00">14 Oct 2015, 15:36:25</span></td>
			<td class="green center">134</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11408317,0" class="icommentjs icon16" href="/ppsspp-gold-psp-emulator-v1-1-1-0-apk-t11408317.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ppsspp-gold-psp-emulator-v1-1-1-0-apk-t11408317.html" class="cellMainLink">PPSSPP Gold - PSP emulator v1.1.1.0.apk</a></div>
			</td>
			<td class="nobr center" data-sort="19493935">18.59 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T00:17:16+00:00">14 Oct 2015, 00:17:16</span></td>
			<td class="green center">127</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402947,0" class="icommentjs icon16" href="/huniepop-gog-t11402947.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/huniepop-gog-t11402947.html" class="cellMainLink">HuniePop (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="311343396">296.92 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T03:23:50+00:00">13 Oct 2015, 03:23:50</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11407771,0" class="icommentjs icon16" href="/bedlam-plaza-t11407771.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/bedlam-plaza-t11407771.html" class="cellMainLink">Bedlam-PLAZA</a></div>
			</td>
			<td class="nobr center" data-sort="2020696942">1.88 <span>GB</span></td>
			<td class="center">43</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T20:56:58+00:00">13 Oct 2015, 20:56:58</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11407762,0" class="icommentjs icon16" href="/assetto-corsa-update-v1-3-2-incl-dream-pack-2-dlc-bat-t11407762.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/assetto-corsa-update-v1-3-2-incl-dream-pack-2-dlc-bat-t11407762.html" class="cellMainLink">Assetto Corsa Update v1 3 2 Incl Dream Pack 2 DLC-BAT</a></div>
			</td>
			<td class="nobr center" data-sort="5205617924">4.85 <span>GB</span></td>
			<td class="center">55</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T20:54:40+00:00">13 Oct 2015, 20:54:40</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11398768,0" class="icommentjs icon16" href="/vortex-the-gateway-v0-671-t11398768.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/vortex-the-gateway-v0-671-t11398768.html" class="cellMainLink">Vortex: The Gateway v0.671</a></div>
			</td>
			<td class="nobr center" data-sort="893429301">852.04 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T10:57:36+00:00">12 Oct 2015, 10:57:36</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11398745,0" class="icommentjs icon16" href="/darkest-dungeon-build-11015-t11398745.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/darkest-dungeon-build-11015-t11398745.html" class="cellMainLink">Darkest Dungeon Build 11015</a></div>
			</td>
			<td class="nobr center" data-sort="1319025986">1.23 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T10:51:44+00:00">12 Oct 2015, 10:51:44</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11411946,0" class="icommentjs icon16" href="/tales-of-zestiria-ps3-duplex-t11411946.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/tales-of-zestiria-ps3-duplex-t11411946.html" class="cellMainLink">Tales of Zestiria PS3-DUPLEX</a></div>
			</td>
			<td class="nobr center" data-sort="9232092654">8.6 <span>GB</span></td>
			<td class="center">96</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T14:46:33+00:00">14 Oct 2015, 14:46:33</span></td>
			<td class="green center">14</td>
			<td class="red lasttd center">73</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11404778,0" class="icommentjs icon16" href="/eset-nod32-antivirus-smart-security-9-0-318-x86x64-incl-serial-team-os-t11404778.html#comment"> <em class="iconvalue">63</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/eset-nod32-antivirus-smart-security-9-0-318-x86x64-incl-serial-team-os-t11404778.html" class="cellMainLink">ESET NOD32 Antivirus &amp; Smart Security 9.0.318 (x86x64) incl Serial-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="370049602">352.91 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T10:18:44+00:00">13 Oct 2015, 10:18:44</span></td>
			<td class="green center">456</td>
			<td class="red lasttd center">208</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11401314,0" class="icommentjs icon16" href="/malwarebytes-anti-malware-premium-2-2-0-1024-final-multilingual-incl-keygen-team-os-t11401314.html#comment"> <em class="iconvalue">93</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/malwarebytes-anti-malware-premium-2-2-0-1024-final-multilingual-incl-keygen-team-os-t11401314.html" class="cellMainLink">Malwarebytes Anti-Malware Premium 2.2.0.1024 Final Multilingual incl Keygen-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="22993764">21.93 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T18:34:05+00:00">12 Oct 2015, 18:34:05</span></td>
			<td class="green center">469</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11403961,0" class="icommentjs icon16" href="/adobe-acrobat-xi-pro-11-0-13-multilingual-update-incl-patch-team-os-t11403961.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-acrobat-xi-pro-11-0-13-multilingual-update-incl-patch-team-os-t11403961.html" class="cellMainLink">Adobe Acrobat XI Pro 11.0.13 Multilingual+update incl Patch-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="760804786">725.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T06:53:01+00:00">13 Oct 2015, 06:53:01</span></td>
			<td class="green center">350</td>
			<td class="red lasttd center">134</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11412392,0" class="icommentjs icon16" href="/adobe-photoshop-elements-14-0-repack-cracked-diakov-appzdam-t11412392.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-photoshop-elements-14-0-repack-cracked-diakov-appzdam-t11412392.html" class="cellMainLink">Adobe Photoshop Elements 14.0 RePack + Cracked (Diakov) - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="2062309360">1.92 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T16:40:05+00:00">14 Oct 2015, 16:40:05</span></td>
			<td class="green center">281</td>
			<td class="red lasttd center">132</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11405321,0" class="icommentjs icon16" href="/adobe-flash-player-19-0-0-207-final-2015-pc-t11405321.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-flash-player-19-0-0-207-final-2015-pc-t11405321.html" class="cellMainLink">Adobe Flash Player 19.0.0.207 Final (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="57684768">55.01 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T13:02:23+00:00">13 Oct 2015, 13:02:23</span></td>
			<td class="green center">294</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11404924,0" class="icommentjs icon16" href="/netlimiter-4-0-15-0-x86x64-enterprise-edition-4realtorrentz-t11404924.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/netlimiter-4-0-15-0-x86x64-enterprise-edition-4realtorrentz-t11404924.html" class="cellMainLink">NetLimiter 4.0.15.0 (x86x64) Enterprise Edition [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="14938334">14.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T11:07:30+00:00">13 Oct 2015, 11:07:30</span></td>
			<td class="green center">196</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11407218,0" class="icommentjs icon16" href="/filemaker-pro-14-advanced-14-0-3-313-multilingual-win-mac-incl-patch-team-os-t11407218.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/filemaker-pro-14-advanced-14-0-3-313-multilingual-win-mac-incl-patch-team-os-t11407218.html" class="cellMainLink">FileMaker Pro 14 Advanced 14.0.3.313 Multilingual (Win-Mac) incl Patch-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="1356542753">1.26 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T18:31:37+00:00">13 Oct 2015, 18:31:37</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">93</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11408191,0" class="icommentjs icon16" href="/windows-10-enterprise-x64-multilanguage-oct-2015-generation2-t11408191.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-enterprise-x64-multilanguage-oct-2015-generation2-t11408191.html" class="cellMainLink">Windows 10 Enterprise X64 Multilanguage Oct 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="8919646645">8.31 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T23:09:58+00:00">13 Oct 2015, 23:09:58</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">264</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400416,0" class="icommentjs icon16" href="/acdsee-ultimate-9-0-build-565-x64-11-10-2015-2015-pc-repack-by-kpojiuk-t11400416.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/acdsee-ultimate-9-0-build-565-x64-11-10-2015-2015-pc-repack-by-kpojiuk-t11400416.html" class="cellMainLink">ACDSee Ultimate 9.0 Build 565 [x64] (11.10.2015) (2015) PC | RePack by KpoJIuK</a></div>
			</td>
			<td class="nobr center" data-sort="68459602">65.29 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T15:31:46+00:00">12 Oct 2015, 15:31:46</span></td>
			<td class="green center">191</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11411245,0" class="icommentjs icon16" href="/super-hide-ip-3-3-6-2-patch-4realtorrentz-t11411245.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/super-hide-ip-3-3-6-2-patch-4realtorrentz-t11411245.html" class="cellMainLink">Super Hide IP 3.3.6.2 + patch [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="2931917">2.8 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T12:32:59+00:00">14 Oct 2015, 12:32:59</span></td>
			<td class="green center">159</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11411860,0" class="icommentjs icon16" href="/o-o-defrag-professional-19-0-build-99-repack-by-d-akov-t11411860.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/o-o-defrag-professional-19-0-build-99-repack-by-d-akov-t11411860.html" class="cellMainLink">O&amp;O Defrag Professional 19.0 Build 99 RePack by D!akov</a></div>
			</td>
			<td class="nobr center" data-sort="23246182">22.17 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T14:19:41+00:00">14 Oct 2015, 14:19:41</span></td>
			<td class="green center">151</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11405401,0" class="icommentjs icon16" href="/infix-pdf-editor-pro-6-42-final-2015-pc-repack-by-d-akov-t11405401.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/infix-pdf-editor-pro-6-42-final-2015-pc-repack-by-d-akov-t11405401.html" class="cellMainLink">Infix PDF Editor Pro 6.42 Final (2015) PC | RePack by D!akov</a></div>
			</td>
			<td class="nobr center" data-sort="67558960">64.43 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T13:19:16+00:00">13 Oct 2015, 13:19:16</span></td>
			<td class="green center">146</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11397546,0" class="icommentjs icon16" href="/cyberlink-youcam-deluxe-6-0-4601-0-multilingual-team-os-t11397546.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/cyberlink-youcam-deluxe-6-0-4601-0-multilingual-team-os-t11397546.html" class="cellMainLink">CyberLink YouCam Deluxe 6.0.4601.0 Multilingual-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="464396014">442.88 <span>MB</span></td>
			<td class="center">88</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T04:24:09+00:00">12 Oct 2015, 04:24:09</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11416291,0" class="icommentjs icon16" href="/destroy-windows-10-spying-1-5-0-build-500-4realtorrentz-t11416291.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/destroy-windows-10-spying-1-5-0-build-500-4realtorrentz-t11416291.html" class="cellMainLink">Destroy Windows 10 Spying 1.5.0 Build 500 [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="230186">224.79 <span>KB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T12:18:33+00:00">15 Oct 2015, 12:18:33</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11405418,0" class="icommentjs icon16" href="/foxit-reader-7-2-2-929-2015-pc-portable-by-portableappz-t11405418.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/foxit-reader-7-2-2-929-2015-pc-portable-by-portableappz-t11405418.html" class="cellMainLink">Foxit Reader 7.2.2.929 (2015) PC | Portable by PortableAppZ</a></div>
			</td>
			<td class="nobr center" data-sort="145286836">138.56 <span>MB</span></td>
			<td class="center">474</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T13:22:50+00:00">13 Oct 2015, 13:22:50</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">4</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11416026,0" class="icommentjs icon16" href="/horriblesubs-naruto-shippuuden-434-720p-mkv-t11416026.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-naruto-shippuuden-434-720p-mkv-t11416026.html" class="cellMainLink">[HorribleSubs] Naruto Shippuuden - 434 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="330326940">315.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T11:10:02+00:00">15 Oct 2015, 11:10:02</span></td>
			<td class="green center">894</td>
			<td class="red lasttd center">271</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-hidan-no-aria-aa-02-raw-tva-1280x720-x264-aac-mp4-t11407267.html" class="cellMainLink">[Leopard-Raws] Hidan no Aria AA - 02 RAW (TVA 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="355821086">339.34 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T18:45:02+00:00">13 Oct 2015, 18:45:02</span></td>
			<td class="green center">319</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11416386,0" class="icommentjs icon16" href="/naruto-shippuden-shippuuden-434-480p-eng-sub-iorchid-t11416386.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-shippuuden-434-480p-eng-sub-iorchid-t11416386.html" class="cellMainLink">Naruto Shippuden (Shippuuden) - 434 [480p][Eng Sub][iORcHiD]</a></div>
			</td>
			<td class="nobr center" data-sort="95406866">90.99 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T12:53:16+00:00">15 Oct 2015, 12:53:16</span></td>
			<td class="green center">231</td>
			<td class="red lasttd center">179</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-gakusen-toshi-asterisk-02-cc1f0155-mkv-t11415523.html" class="cellMainLink">[FFF] Gakusen Toshi Asterisk - 02 [CC1F0155].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="460603115">439.27 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T08:45:02+00:00">15 Oct 2015, 08:45:02</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11416235,0" class="icommentjs icon16" href="/animerg-naruto-shippuuden-434-english-subbed-480p-kami-t11416235.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-naruto-shippuuden-434-english-subbed-480p-kami-t11416235.html" class="cellMainLink">[AnimeRG] Naruto Shippuuden - 434 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="57060759">54.42 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T11:59:34+00:00">15 Oct 2015, 11:59:34</span></td>
			<td class="green center">148</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-noragami-aragoto-02-cf8a32fe-mkv-t11411747.html" class="cellMainLink">[FFF] Noragami Aragoto - 02 [CF8A32FE].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="387991484">370.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T13:50:03+00:00">14 Oct 2015, 13:50:03</span></td>
			<td class="green center">157</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11398942,0" class="icommentjs icon16" href="/vivid-owari-no-seraph-13-700eba12-mkv-t11398942.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-owari-no-seraph-13-700eba12-mkv-t11398942.html" class="cellMainLink">[Vivid] Owari no Seraph - 13 [700EBA12].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="351619841">335.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T11:45:03+00:00">12 Oct 2015, 11:45:03</span></td>
			<td class="green center">155</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-dd-hokuto-no-ken-2-ichigo-aji-plus-02-raw-tx-1280x720-x264-aac-mp4-t11407498.html" class="cellMainLink">[Leopard-Raws] DD Hokuto no Ken 2 - Ichigo Aji Plus - 02 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="368763967">351.68 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T19:35:02+00:00">13 Oct 2015, 19:35:02</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11402519,0" class="icommentjs icon16" href="/fff-infinite-stratos-2-bd-720p-aac-t11402519.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-infinite-stratos-2-bd-720p-aac-t11402519.html" class="cellMainLink">[FFF] Infinite Stratos 2 [BD][720p-AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="7101149916">6.61 <span>GB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T00:40:04+00:00">13 Oct 2015, 00:40:04</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">71</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-diabolik-lovers-more-blood-04-raw-atx-1280x720-x264-aac-mp4-t11415955.html" class="cellMainLink">[Leopard-Raws] Diabolik Lovers More, Blood - 04 RAW (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="241526581">230.34 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T10:50:01+00:00">15 Oct 2015, 10:50:01</span></td>
			<td class="green center">110</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-kamisama-minarai-himitsu-no-cocotama-03-raw-tx-1280x720-x264-aac-mp4-t11416833.html" class="cellMainLink">[Leopard-Raws] Kamisama Minarai - Himitsu no Cocotama - 03 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="241666580">230.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T14:30:02+00:00">15 Oct 2015, 14:30:02</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-dungeon-ni-deai-wo-motomeru-no-wa-machigatteiru-darou-ka-vol-03-bd-1080p-flac-t11416920.html" class="cellMainLink">[FFF] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka - Vol.03 [BD][1080p-FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2493874094">2.32 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T14:50:02+00:00">15 Oct 2015, 14:50:02</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-k-return-of-kings-02-ea5a0625-mkv-t11415013.html" class="cellMainLink">[Commie] K Return of Kings - 02 [EA5A0625].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="603618171">575.66 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T06:10:02+00:00">15 Oct 2015, 06:10:02</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11410424,0" class="icommentjs icon16" href="/fff-seiken-tsukai-no-world-break-tv-t11410424.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-seiken-tsukai-no-world-break-tv-t11410424.html" class="cellMainLink">[FFF] Seiken Tsukai no World Break [TV]</a></div>
			</td>
			<td class="nobr center" data-sort="5450981087">5.08 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T09:20:02+00:00">14 Oct 2015, 09:20:02</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11414551,0" class="icommentjs icon16" href="/deadfish-magi-sinbad-no-bouken-04-ova-dvd-576p-aac-mp4-t11414551.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-magi-sinbad-no-bouken-04-ova-dvd-576p-aac-mp4-t11414551.html" class="cellMainLink">[DeadFish] Magi: Sinbad no Bouken - 04 - OVA [DVD][576p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="331910243">316.53 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T03:51:28+00:00">15 Oct 2015, 03:51:28</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">22</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11412168,0" class="icommentjs icon16" href="/marvel-week-10-14-2015-nem-t11412168.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-10-14-2015-nem-t11412168.html" class="cellMainLink">Marvel Week+ (10-14-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="694780182">662.59 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T15:39:39+00:00">14 Oct 2015, 15:39:39</span></td>
			<td class="green center">943</td>
			<td class="red lasttd center">571</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11411458,0" class="icommentjs icon16" href="/dc-week-10-14-2015-aka-dc-you-week-20-nem-t11411458.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-10-14-2015-aka-dc-you-week-20-nem-t11411458.html" class="cellMainLink">DC Week+ (10-14-2015) (aka DC YOU Week 20) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="550988245">525.46 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T13:21:49+00:00">14 Oct 2015, 13:21:49</span></td>
			<td class="green center">690</td>
			<td class="red lasttd center">394</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11411499,0" class="icommentjs icon16" href="/the-walking-dead-147-2015-digital-minutemen-acan-cbr-t11411499.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-walking-dead-147-2015-digital-minutemen-acan-cbr-t11411499.html" class="cellMainLink">The Walking Dead 147 (2015) (Digital) (Minutemen-Acan).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="39797306">37.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T13:31:44+00:00">14 Oct 2015, 13:31:44</span></td>
			<td class="green center">572</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11405727,0" class="icommentjs icon16" href="/injustice-gods-among-us-year-four-024-2015-digital-son-of-ultron-empire-cbr-nem-t11405727.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/injustice-gods-among-us-year-four-024-2015-digital-son-of-ultron-empire-cbr-nem-t11405727.html" class="cellMainLink">Injustice - Gods Among Us - Year Four 024 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="21346545">20.36 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T14:30:02+00:00">13 Oct 2015, 14:30:02</span></td>
			<td class="green center">440</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401214,0" class="icommentjs icon16" href="/solar-energy-engineering-processes-and-systems-2nd-edition-2014-pdf-t11401214.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/solar-energy-engineering-processes-and-systems-2nd-edition-2014-pdf-t11401214.html" class="cellMainLink">Solar Energy Engineering: Processes and Systems, 2nd Edition [2014] [PDF]</a></div>
			</td>
			<td class="nobr center" data-sort="18289560">17.44 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T18:16:54+00:00">12 Oct 2015, 18:16:54</span></td>
			<td class="green center">429</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11404915,0" class="icommentjs icon16" href="/make-bicycle-projects-upgrade-accessorize-and-customize-with-electronics-mechanics-and-metalwork-1st-edition-2015-pdf-gooner-t11404915.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/make-bicycle-projects-upgrade-accessorize-and-customize-with-electronics-mechanics-and-metalwork-1st-edition-2015-pdf-gooner-t11404915.html" class="cellMainLink">Make Bicycle Projects - Upgrade, Accessorize and Customize with Electronics, Mechanics and Metalwork - 1st Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="11939940">11.39 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T11:05:21+00:00">13 Oct 2015, 11:05:21</span></td>
			<td class="green center">341</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401690,0" class="icommentjs icon16" href="/machine-learning-in-python-essential-techniques-for-predictive-analysis-1st-edition-2015-pdf-epub-mobi-gooner-t11401690.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/machine-learning-in-python-essential-techniques-for-predictive-analysis-1st-edition-2015-pdf-epub-mobi-gooner-t11401690.html" class="cellMainLink">Machine Learning in Python - Essential Techniques for Predictive Analysis - 1st Edition (2015) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="32046202">30.56 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T20:12:55+00:00">12 Oct 2015, 20:12:55</span></td>
			<td class="green center">292</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11411920,0" class="icommentjs icon16" href="/make-paper-inventions-machines-that-move-drawings-that-light-up-and-wearables-and-structures-you-can-cut-fold-and-roll-1st-ed-2015-epub-gooner-t11411920.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/make-paper-inventions-machines-that-move-drawings-that-light-up-and-wearables-and-structures-you-can-cut-fold-and-roll-1st-ed-2015-epub-gooner-t11411920.html" class="cellMainLink">Make - Paper Inventions - Machines that Move, Drawings that Light Up and Wearables and Structures You Can Cut, Fold and Roll - 1st Ed (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="206960756">197.37 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T14:38:32+00:00">14 Oct 2015, 14:38:32</span></td>
			<td class="green center">276</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401618,0" class="icommentjs icon16" href="/principles-of-solar-engineering-3rd-edition-2015-pdf-t11401618.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/principles-of-solar-engineering-3rd-edition-2015-pdf-t11401618.html" class="cellMainLink">Principles of Solar Engineering, 3rd Edition [2015] [PDF]</a></div>
			</td>
			<td class="nobr center" data-sort="10641813">10.15 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T19:48:00+00:00">12 Oct 2015, 19:48:00</span></td>
			<td class="green center">263</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11404956,0" class="icommentjs icon16" href="/liquid-explosives-2015-pdf-gooner-t11404956.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/liquid-explosives-2015-pdf-gooner-t11404956.html" class="cellMainLink">Liquid Explosives (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="12319588">11.75 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T11:16:29+00:00">13 Oct 2015, 11:16:29</span></td>
			<td class="green center">247</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11413940,0" class="icommentjs icon16" href="/the-new-york-times-bestsellers-october-18-2015-fiction-non-fiction-t11413940.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-new-york-times-bestsellers-october-18-2015-fiction-non-fiction-t11413940.html" class="cellMainLink">The New York Times Bestsellers - October 18, 2015 [Fiction / Non-Fiction]</a></div>
			</td>
			<td class="nobr center" data-sort="116062885">110.69 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T23:31:28+00:00">14 Oct 2015, 23:31:28</span></td>
			<td class="green center">228</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11409652,0" class="icommentjs icon16" href="/0-day-week-of-2015-10-07-t11409652.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-10-07-t11409652.html" class="cellMainLink">0-Day Week of 2015.10.07</a></div>
			</td>
			<td class="nobr center" data-sort="12603441414">11.74 <span>GB</span></td>
			<td class="center">195</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T06:03:58+00:00">14 Oct 2015, 06:03:58</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">334</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401565,0" class="icommentjs icon16" href="/life-s-greatest-secret-the-race-to-crack-the-genetic-code-2015-epub-gooner-t11401565.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/life-s-greatest-secret-the-race-to-crack-the-genetic-code-2015-epub-gooner-t11401565.html" class="cellMainLink">Life&#039;s Greatest Secret - The Race to Crack the Genetic Code (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="20957156">19.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T19:29:51+00:00">12 Oct 2015, 19:29:51</span></td>
			<td class="green center">207</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11411104,0" class="icommentjs icon16" href="/the-uncanny-avengers-001-2015-two-covers-digital-minutemen-wtf-cbr-nem-t11411104.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-uncanny-avengers-001-2015-two-covers-digital-minutemen-wtf-cbr-nem-t11411104.html" class="cellMainLink">The Uncanny Avengers 001 (2015) (two covers) (Digital) (Minutemen-WTF).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="66857309">63.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T12:04:56+00:00">14 Oct 2015, 12:04:56</span></td>
			<td class="green center">196</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11411518,0" class="icommentjs icon16" href="/new-avengers-001-2015-digital-minutemen-midas-cbr-t11411518.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/new-avengers-001-2015-digital-minutemen-midas-cbr-t11411518.html" class="cellMainLink">New Avengers 001 (2015) (digital) (Minutemen-Midas).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="36339593">34.66 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T13:33:58+00:00">14 Oct 2015, 13:33:58</span></td>
			<td class="green center">194</td>
			<td class="red lasttd center">10</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11400737,0" class="icommentjs icon16" href="/top-100-70-s-rock-albums-by-ultimateclassicrock-cd-s-76-100-flac-t11400737.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/top-100-70-s-rock-albums-by-ultimateclassicrock-cd-s-76-100-flac-t11400737.html" class="cellMainLink">Top 100 &#039;70&#039;s Rock Albums by UltimateClassicRock - CD&#039;s 76-100 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="7426728008">6.92 <span>GB</span></td>
			<td class="center">570</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T16:41:05+00:00">12 Oct 2015, 16:41:05</span></td>
			<td class="green center">154</td>
			<td class="red lasttd center">317</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401137,0" class="icommentjs icon16" href="/linda-ronstadt-for-sentimental-reasons-2014-24-96-hd-flac-t11401137.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/linda-ronstadt-for-sentimental-reasons-2014-24-96-hd-flac-t11401137.html" class="cellMainLink">Linda Ronstadt - For Sentimental Reasons (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="898260959">856.65 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:57:09+00:00">12 Oct 2015, 17:57:09</span></td>
			<td class="green center">136</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11411896,0" class="icommentjs icon16" href="/santana-borboletta-2014-24-96-hd-flac-t11411896.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/santana-borboletta-2014-24-96-hd-flac-t11411896.html" class="cellMainLink">Santana - Borboletta (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1089443122">1.01 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T14:30:59+00:00">14 Oct 2015, 14:30:59</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11398990,0" class="icommentjs icon16" href="/miles-davis-amandla-2011-24-192-hd-flac-t11398990.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/miles-davis-amandla-2011-24-192-hd-flac-t11398990.html" class="cellMainLink">Miles Davis - Amandla (2011) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1853835654">1.73 <span>GB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T11:55:24+00:00">12 Oct 2015, 11:55:24</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11399656,0" class="icommentjs icon16" href="/tom-jones-long-lost-suitcase-2015-flac-t11399656.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tom-jones-long-lost-suitcase-2015-flac-t11399656.html" class="cellMainLink">Tom Jones - Long Lost Suitcase (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="266168637">253.84 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T12:52:56+00:00">12 Oct 2015, 12:52:56</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mozart-complete-symphonies-linden-t11408428.html" class="cellMainLink">Mozart - Complete Symphonies - Linden</a></div>
			</td>
			<td class="nobr center" data-sort="3225112548">3 <span>GB</span></td>
			<td class="center">170</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T01:19:45+00:00">14 Oct 2015, 01:19:45</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11404221,0" class="icommentjs icon16" href="/keith-jarrett-creation-2015-24-48-hd-flac-t11404221.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/keith-jarrett-creation-2015-24-48-hd-flac-t11404221.html" class="cellMainLink">Keith Jarrett - Creation (2015) [24-48 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="653769439">623.48 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T07:56:27+00:00">13 Oct 2015, 07:56:27</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11404998,0" class="icommentjs icon16" href="/commodores-machine-gun-2015-24-192-hd-flac-t11404998.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/commodores-machine-gun-2015-24-192-hd-flac-t11404998.html" class="cellMainLink">Commodores - Machine Gun (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1613594070">1.5 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T11:27:48+00:00">13 Oct 2015, 11:27:48</span></td>
			<td class="green center">89</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11415738,0" class="icommentjs icon16" href="/van-morrison-astral-weeks-2013-24-192-hd-flac-t11415738.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/van-morrison-astral-weeks-2013-24-192-hd-flac-t11415738.html" class="cellMainLink">Van Morrison - Astral Weeks (2013) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1826149381">1.7 <span>GB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T09:50:43+00:00">15 Oct 2015, 09:50:43</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/toni-braxton-essential-mixes-2010-2015-extended-mix-flac-pirate-shovon-t11398770.html" class="cellMainLink">Toni Braxton - Essential Mixes [2010] [2015 Extended Mix] [FLAC] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="577008629">550.28 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T10:58:11+00:00">12 Oct 2015, 10:58:11</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11403339,0" class="icommentjs icon16" href="/bruce-springsteen-1984-08-05-brendan-byrne-arena-meadwolands-nj-flac-t11403339.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bruce-springsteen-1984-08-05-brendan-byrne-arena-meadwolands-nj-flac-t11403339.html" class="cellMainLink">Bruce Springsteen 1984-08-05 Brendan Byrne Arena, Meadwolands, NJ [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1412933847">1.32 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T04:21:09+00:00">13 Oct 2015, 04:21:09</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11405656,0" class="icommentjs icon16" href="/v-a-mille-papaveri-rossi-le-canzoni-di-fabrizio-de-andrÃ©-flac-tntvillage-t11405656.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/v-a-mille-papaveri-rossi-le-canzoni-di-fabrizio-de-andrÃ©-flac-tntvillage-t11405656.html" class="cellMainLink">V.A. - Mille Papaveri Rossi Le canzoni di Fabrizio De AndrÃ© [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="819979507">781.99 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T14:12:37+00:00">13 Oct 2015, 14:12:37</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11415358,0" class="icommentjs icon16" href="/kim-simmonds-savoy-brown-the-devil-to-pay-2015-flac-t11415358.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/kim-simmonds-savoy-brown-the-devil-to-pay-2015-flac-t11415358.html" class="cellMainLink">Kim Simmonds &amp; Savoy Brown - The Devil To Pay (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="421490010">401.96 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-15T07:43:51+00:00">15 Oct 2015, 07:43:51</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11412849,0" class="icommentjs icon16" href="/va-pure-country-moods-4-cd-boxed-set-flac-t11412849.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-pure-country-moods-4-cd-boxed-set-flac-t11412849.html" class="cellMainLink">VA - Pure Country Moods 4 CD Boxed Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="887511891">846.4 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T18:40:28+00:00">14 Oct 2015, 18:40:28</span></td>
			<td class="green center">56</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/dee-dee-bridgewater-this-is-dee-dee-bridgewater-retrospective-2015-flac-t11413033.html" class="cellMainLink">Dee Dee Bridgewater - This Is Dee Dee Bridgewater Retrospective (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="738960086">704.73 <span>MB</span></td>
			<td class="center">35</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-14T19:16:28+00:00">14 Oct 2015, 19:16:28</span></td>
			<td class="green center">55</td>
			<td class="red lasttd center">19</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/torrents-need-category-change-post-here-super-users-translat-thread-104709/?unread=17000481">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Torrents That Need a Category Change - Post Here For Super Users &amp; Translators To Move V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/Joel/">Joel</a></span></span> <time class="timeago" datetime="2015-10-16T01:01:00+00:00">16 Oct 2015, 01:01</time></span>
	</li>
		<li>
		<a href="/community/show/search-not-catching-all-episodes/?unread=17000480">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Search not catching all episodes
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/zi69y/">zi69y</a></span></span> <time class="timeago" datetime="2015-10-16T01:00:28+00:00">16 Oct 2015, 01:00</time></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=17000477">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/%5BK%5DasuRusa%5BK%5D/">[K]asuRusa[K]</a></span></span> <time class="timeago" datetime="2015-10-16T00:57:30+00:00">16 Oct 2015, 00:57</time></span>
	</li>
		<li>
		<a href="/community/show/v12-2014-2015-new-webrips-web-dl-hdrip-releases-october-2015/?unread=17000475">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				V12 2014 / 2015 New WebRips/ Web-DL/HDrip Releases October 2015
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/MoonChild/">MoonChild</a></span></span> <time class="timeago" datetime="2015-10-16T00:56:38+00:00">16 Oct 2015, 00:56</time></span>
	</li>
		<li>
		<a href="/community/show/suggest-achievements-official-thread-v2/?unread=17000474">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Suggest Achievements! Official Thread v2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/OptimusPr1me/">OptimusPr1me</a></span></span> <time class="timeago" datetime="2015-10-16T00:56:06+00:00">16 Oct 2015, 00:56</time></span>
	</li>
		<li>
		<a href="/community/show/why-can-t-i-d-l-torrent-files-vpn/?unread=17000472">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Why can&#039;t I d/l torrent files with VPN on?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_4"><a class="plain" href="/user/thLullaby/">thLullaby</a></span></span> <time class="timeago" datetime="2015-10-16T00:55:04+00:00">16 Oct 2015, 00:55</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/Mr.White/post/new-site-rules/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> New site Rules</p></a><span class="explanation">by <a class="plain aclColor_11" href="/user/Mr.White/">Mr.White</a> <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span></li>
	<li><a href="/blog/Dude./post/a-blog-that-is-n0t-a-free-key-giveaway/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> A Blog That Is N0T a Free Key Giveaway...</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Dude./">Dude.</a> <time class="timeago" datetime="2015-10-15T13:10:53+00:00">15 Oct 2015, 13:10</time></span></li>
	<li><a href="/blog/ScribeOfGoD/post/key-giveaways/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Key giveaways!!</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/ScribeOfGoD/">ScribeOfGoD</a> <time class="timeago" datetime="2015-10-14T10:14:56+00:00">14 Oct 2015, 10:14</time></span></li>
	<li><a href="/blog/OptimusPr1me/post/october-is-domestic-violence-awareness-month/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> October Is Domestic Violence Awareness Month</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <time class="timeago" datetime="2015-10-14T07:54:21+00:00">14 Oct 2015, 07:54</time></span></li>
	<li><a href="/blog/DarkUS3R/post/one-of-my-girlfriend-texted-me/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> One of my girlfriend texted me.â¦â¦</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/DarkUS3R/">DarkUS3R</a> <time class="timeago" datetime="2015-10-14T07:48:46+00:00">14 Oct 2015, 07:48</time></span></li>
	<li><a href="/blog/thLullaby/post/dear-anon/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Dear Anon..</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/thLullaby/">thLullaby</a> <time class="timeago" datetime="2015-10-13T11:56:09+00:00">13 Oct 2015, 11:56</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/book%20apk/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Book apk
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/girl%20in%20progress%20nl%20subs/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				girl in progress nl subs
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/brides%20priya%20rai/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				brides priya rai
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/vst/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				vst
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/selma%202015/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Selma 2015
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/scorpion%20s02e04%201080p/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Scorpion S02E04 1080p
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/food%20inc/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				food inc
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/insurgent%20movie/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Insurgent Movie
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20sims%204/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the sims 4
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/reservoir%20dogs/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Reservoir Dogs
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/limitless/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				limitless
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
