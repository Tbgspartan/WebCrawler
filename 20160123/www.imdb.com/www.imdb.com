



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "011-1854853-2420453";
                var ue_id = "0F6NK55KQ41KMQBSXB5B";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="0F6NK55KQ41KMQBSXB5B" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-75fec8c5.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2467623394._CB300617431_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['f'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['444444578591'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4044276354._CB299069504_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"e197329bb22b0bb82750e58fe4604f9f176634a7",
"2016-01-23T18%3A04%3A29GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50131;
generic.days_to_midnight = 0.5802199244499207;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'f']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=444444578591;ord=444444578591?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=444444578591?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=444444578591?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=01-23&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59416254/?ref_=nv_nw_tn_1"
> âThe X-Filesâ: Gillian Anderson Was Offered Half as Much as David Duchovny for Revival Series
</a><br />
                        <span class="time">15 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59416095/?ref_=nv_nw_tn_2"
> Sony Rebooting Jim Hensonâs âLabyrinthâ
</a><br />
                        <span class="time">16 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59416244/?ref_=nv_nw_tn_3"
> âThe Walking Deadâsâ Lauren Cohan to Appear in âBatman v Superman: Dawn of Justiceâ
</a><br />
                        <span class="time">15 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0090605/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjE5MTQxNTE3Nl5BMl5BanBnXkFtZTcwMjM0ODEyMw@@._V1._SY315_CR58,0,410,315_.jpg",
            titleYears : "1986",
            rank : 64,
                    headline : "Aliens"
    },
    nameAd : {
            clickThru : "/name/nm0004266/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjA1NzkyNjAzNV5BMl5BanBnXkFtZTcwNzYyOTUxOQ@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 120,
            headline : "Anne Hathaway"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYiY7ObywPjEQNp4Ussj09xi9w3iRJujfYrN0RWfefMYtwi4PHt-Jhby8CRQaTwgzRlvqDeHFrV%0D%0AUZAaUvk6_5sQ7EroixE3psklc53Dzx2Uo1GXXvVVuGPYbK_52GI9VA5j8nmcwzPh86hFX2sk0mex%0D%0AXzya92Roy2IBr5Zi6u1PTojqa9IZARxgbZyVpauJQq2m222C6qgmLWp5E7eXNVRfgg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYl39N8egfv3eWtXbE_AYICritUjWmhL_EtlRI73M6zhIJT0sDCsCj-J2BgCx1IxmVtbv7SkO66%0D%0AP8u8sqQzf62DJzqZUr2Ngr2gGZjDhWbyPC2CSRWh8L5QRx2oMIbA6j7RERJeuqD7CPrw2QLxdLAq%0D%0Ab8hubeMHk6sLJ6MdwARR0N_BfMY0wIMmzXmsW-0YiYQL%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYsN_g_hyEF30tITrPO8HsXrEBQRF1ZMtNr45axcUY26FziHukhYhawROgLNd_Ku9GeYQg8pjw7%0D%0Anwg2rG8vTbcxq4Jgg5Ngk2m26mXOJFfwOXxqwoT57zWjN3r6Q23iv6fmXnQA31d_QLjubc4YTnWG%0D%0AaMkBooNLL0G2t96a1isiZRN0ZQftqeDosdCcIdu1e_gx%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=444444578591;ord=444444578591?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=444444578591;ord=444444578591?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2156246297?ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395216362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2156246297" data-source="bylist" data-id="ls056131825" data-rid="0F6NK55KQ41KMQBSXB5B" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="eason 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." alt="eason 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." src="http://ia.media-imdb.com/images/M/MV5BMTYwOTEzMDMzMl5BMl5BanBnXkFtZTgwNzExODIzNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwOTEzMDMzMl5BMl5BanBnXkFtZTgwNzExODIzNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="eason 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." title="eason 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="eason 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." title="eason 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt0944947/?ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395216362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Game of Thrones" </a> </div> </div> <div class="secondary ellipsis"> Stark Battle Banner Teaser </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1871033625?ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395216362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1871033625" data-source="bylist" data-id="ls002653141" data-rid="0F6NK55KQ41KMQBSXB5B" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." alt="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." src="http://ia.media-imdb.com/images/M/MV5BMjg3NDMxMDU2N15BMl5BanBnXkFtZTgwNTE3OTUyNzE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjg3NDMxMDU2N15BMl5BanBnXkFtZTgwNTE3OTUyNzE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." title="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." title="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3410834/?ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395216362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > The Divergent Series: Allegiant </a> </div> </div> <div class="secondary ellipsis"> Latest Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2258416921?ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395216362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2258416921" data-source="bylist" data-id="ls056131825" data-rid="0F6NK55KQ41KMQBSXB5B" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Get a first look at &quot;The People v. O.J. Simpson: American Crime Story&quot; with the cast and crew." alt="Get a first look at &quot;The People v. O.J. Simpson: American Crime Story&quot; with the cast and crew." src="http://ia.media-imdb.com/images/M/MV5BMTc4Mzg0MzMwMF5BMl5BanBnXkFtZTgwNzQ3NTY3NzE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc4Mzg0MzMwMF5BMl5BanBnXkFtZTgwNzQ3NTY3NzE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Get a first look at &quot;The People v. O.J. Simpson: American Crime Story&quot; with the cast and crew." title="Get a first look at &quot;The People v. O.J. Simpson: American Crime Story&quot; with the cast and crew." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Get a first look at &quot;The People v. O.J. Simpson: American Crime Story&quot; with the cast and crew." title="Get a first look at &quot;The People v. O.J. Simpson: American Crime Story&quot; with the cast and crew." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2788432/?ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395216362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "American Crime Story" </a> </div> </div> <div class="secondary ellipsis"> The People V. O.J. Simpson </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395216362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/sundance/video/?ref_=hm_sun_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>In the Picture: The IMDb Studio at Sundance</h3> </a> </span> </span> <p class="blurb">From our on-site studio, we bring you the stars of the movies screening at the 2016 Sundance Film Festival. Visit our <a href="/sundance/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk1">special section</a> for photos of <a href="/sundance/premieres-and-parties/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk2">premieres and parties</a>, and more.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi92779801?ref_=hm_sun_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi92779801" data-source="bylist" data-id="ls073917212" data-rid="0F6NK55KQ41KMQBSXB5B" data-type="playlist" class="video-colorbox" data-refsuffix="hm_sun" data-ref="hm_sun_i_1"> <img itemprop="image" class="pri_image" title="Emmy-winning actor Bradley Whitford stopped by The IMDb Studio at Sundance and dropped some hilarious anecdotes about the biggest prankster he's ever worked with. Hint: It was on &quot;The West Wing&quot; set!" alt="Emmy-winning actor Bradley Whitford stopped by The IMDb Studio at Sundance and dropped some hilarious anecdotes about the biggest prankster he's ever worked with. Hint: It was on &quot;The West Wing&quot; set!" src="http://ia.media-imdb.com/images/M/MV5BNzgzMjgwNzE1MV5BMl5BanBnXkFtZTgwMTY4MTk3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzgzMjgwNzE1MV5BMl5BanBnXkFtZTgwMTY4MTk3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> <img alt="Emmy-winning actor Bradley Whitford stopped by The IMDb Studio at Sundance and dropped some hilarious anecdotes about the biggest prankster he's ever worked with. Hint: It was on &quot;The West Wing&quot; set!" title="Emmy-winning actor Bradley Whitford stopped by The IMDb Studio at Sundance and dropped some hilarious anecdotes about the biggest prankster he's ever worked with. Hint: It was on &quot;The West Wing&quot; set!" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Emmy-winning actor Bradley Whitford stopped by The IMDb Studio at Sundance and dropped some hilarious anecdotes about the biggest prankster he's ever worked with. Hint: It was on &quot;The West Wing&quot; set!" title="Emmy-winning actor Bradley Whitford stopped by The IMDb Studio at Sundance and dropped some hilarious anecdotes about the biggest prankster he's ever worked with. Hint: It was on &quot;The West Wing&quot; set!" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/video/?ref_=hm_sun_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > Bradley Whitford Reveals Most Evil Prankster </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-1/?imageid=rm1394009856&ref_=hm_sun_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Greta Gerwig at event of The IMDb Studio (2015)" alt="Greta Gerwig at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUxODIxMzE0M15BMl5BanBnXkFtZTgwOTc0Mjk3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUxODIxMzE0M15BMl5BanBnXkFtZTgwOTc0Mjk3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-1/?ref_=hm_sun_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio - Day 1 Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3682972953?ref_=hm_sun_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3682972953" data-source="bylist" data-id="ls073917212" data-rid="0F6NK55KQ41KMQBSXB5B" data-type="playlist" class="video-colorbox" data-refsuffix="hm_sun" data-ref="hm_sun_i_3"> <img itemprop="image" class="pri_image" title="Nick Jonas, director Andrew Neel, and Ben Schnetzer talk about their new Sundance film 'Goat.' This story brought together a cast of young actors who had a lot of fun behind the scenes! Check out this video to find out which cast member was the biggest practical jokester." alt="Nick Jonas, director Andrew Neel, and Ben Schnetzer talk about their new Sundance film 'Goat.' This story brought together a cast of young actors who had a lot of fun behind the scenes! Check out this video to find out which cast member was the biggest practical jokester." src="http://ia.media-imdb.com/images/M/MV5BNDY1NzYwOTY1MF5BMl5BanBnXkFtZTgwMzcxMjk3NzE@._V1_SY201_CR33,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDY1NzYwOTY1MF5BMl5BanBnXkFtZTgwMzcxMjk3NzE@._V1_SY201_CR33,0,201,201_AL_UY402_UX402_AL_.jpg" /> <img alt="Nick Jonas, director Andrew Neel, and Ben Schnetzer talk about their new Sundance film 'Goat.' This story brought together a cast of young actors who had a lot of fun behind the scenes! Check out this video to find out which cast member was the biggest practical jokester." title="Nick Jonas, director Andrew Neel, and Ben Schnetzer talk about their new Sundance film 'Goat.' This story brought together a cast of young actors who had a lot of fun behind the scenes! Check out this video to find out which cast member was the biggest practical jokester." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Nick Jonas, director Andrew Neel, and Ben Schnetzer talk about their new Sundance film 'Goat.' This story brought together a cast of young actors who had a lot of fun behind the scenes! Check out this video to find out which cast member was the biggest practical jokester." title="Nick Jonas, director Andrew Neel, and Ben Schnetzer talk about their new Sundance film 'Goat.' This story brought together a cast of young actors who had a lot of fun behind the scenes! Check out this video to find out which cast member was the biggest practical jokester." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/video/?ref_=hm_sun_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > Nick Jonas, Cast Talk Fun on <i>Goat</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/sundance/video/?ref_=hm_sun_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355102&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch more video from The IMDb Studio at Sundance</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>"The X-Files" Is Finally Back</h3> </span> </span> <p class="blurb">The truth is out there once again. Get ready for the premiere of "<a href="/title/tt4370492/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760722&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_xf_lk1">The X-Files</a>" miniseries by checking out the top-rated episodes of the <a href="/title/tt0106179/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760722&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_xf_lk2">original series</a>, photos of the show through the years, and the latest posters.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/x-files-top-24-episodes/ls031232115?ref_=hm_tv_xf_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760722&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Laurie Holden in The X-Files (1993)" alt="Still of Laurie Holden in The X-Files (1993)" src="http://ia.media-imdb.com/images/M/MV5BMTQ0NjM0OTQxOV5BMl5BanBnXkFtZTgwNDE1Nzc3MjE@._V1_SY201_CR34,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ0NjM0OTQxOV5BMl5BanBnXkFtZTgwNDE1Nzc3MjE@._V1_SY201_CR34,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/x-files-top-24-episodes/ls031232115?ref_=hm_tv_xf_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760722&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Top-Rated "X-Files" Episodes </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/the-x-files-through-the-years?imageid=rm1045228800&ref_=hm_tv_xf_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760722&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Gillian Anderson and David Duchovny in The X-Files (2015)" alt="Still of Gillian Anderson and David Duchovny in The X-Files (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjI5NzMzMTYwN15BMl5BanBnXkFtZTgwNjQ1MDMwNzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI5NzMzMTYwN15BMl5BanBnXkFtZTgwNjQ1MDMwNzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/the-x-files-through-the-years/?ref_=hm_tv_xf_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760722&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > "The X-Files" Through the Years </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3612796672/rg4124023552?ref_=hm_tv_xf_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760722&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The X-Files (2015)" alt="The X-Files (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTkyNjkzMDI1N15BMl5BanBnXkFtZTgwOTY5NjY3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkyNjkzMDI1N15BMl5BanBnXkFtZTgwOTY5NjY3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/gallery/rg4124023552?ref_=hm_tv_xf_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760722&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > New "X-Files" Posters </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59416254?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQzMTk4MjY3NF5BMl5BanBnXkFtZTcwMzI1ODAxOA@@._V1_SY150_CR10,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59416254?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >âThe X-Filesâ: Gillian Anderson Was Offered Half as Much as David Duchovny for Revival Series</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p> <a href="/name/nm0000096?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Gillian Anderson</a> may have earned Emmy, Golden Globe and SAG Awards for her portrayal of Dana Scully in Foxâs sci-fi hit â<a href="/title/tt0106179?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">The X-Files</a>,â but she recently revealed that she had to fight to be paid the same as male co-star <a href="/name/nm0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">David Duchovny</a> â during the showâs original run and for the hotly-anticipated ...                                        <span class="nobr"><a href="/news/ni59416254?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59416095?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Sony Rebooting Jim Hensonâs âLabyrinthâ</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59416244?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >âThe Walking Deadâsâ Lauren Cohan to Appear in âBatman v Superman: Dawn of Justiceâ</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59416223?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Charlotte Rampling Clarifies Oscar Racism Remarks, Regrets She Was 'Misinterpreted'</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59415900?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Ice Cube on Oscars: âYou Canât Boycott Something You Never Went Toâ</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59416095?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTczNTkyOTIzN15BMl5BanBnXkFtZTgwNjY3MjQxMDE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59416095?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Sony Rebooting Jim Hensonâs âLabyrinthâ</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>Sony is developing a reboot of â<a href="/title/tt0091369?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Labyrinth</a>,â the final movie directed by <a href="/name/nm0001345?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Jim Henson</a>, and has closed a deal with the <a href="/name/nm0001345?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Jim Henson</a> Co. to produce the film with Sonyâs TriStar division.<a href="/name/nm0378229?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk4">Lisa Henson</a> of the Henson Co. will produce the project. â<a href="/title/tt2015381?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk5">Guardians of the Galaxy</a>â co-writer <a href="/name/nm2270979?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk6">Nicole Perlman</a> will write ...                                        <span class="nobr"><a href="/news/ni59416095?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59416244?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >âThe Walking Deadâsâ Lauren Cohan to Appear in âBatman v Superman: Dawn of Justiceâ</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59416223?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Charlotte Rampling Clarifies Oscar Racism Remarks, Regrets She Was 'Misinterpreted'</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59415900?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Ice Cube on Oscars: âYou Canât Boycott Something You Never Went Toâ</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59417429?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Sundance: Sony Pictures Classics Nabs Frank Zappa Documentary (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">35 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59416254?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQzMTk4MjY3NF5BMl5BanBnXkFtZTcwMzI1ODAxOA@@._V1_SY150_CR10,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59416254?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >âThe X-Filesâ: Gillian Anderson Was Offered Half as Much as David Duchovny for Revival Series</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p> <a href="/name/nm0000096?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Gillian Anderson</a> may have earned Emmy, Golden Globe and SAG Awards for her portrayal of Dana Scully in Foxâs sci-fi hit â<a href="/title/tt0106179?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">The X-Files</a>,â but she recently revealed that she had to fight to be paid the same as male co-star <a href="/name/nm0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">David Duchovny</a> â during the showâs original run and for the hotly-anticipated ...                                        <span class="nobr"><a href="/news/ni59416254?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59416014?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Comedies from Amy Poehler, Rashida Jones Among NBC Pilot Orders</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59417373?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Galavant Sneak Peek: Sing Along to Gareth's Less-Than-Flowery Love Song</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59417380?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Is This The Best South Park Cosplay of All-Time?</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000222?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>TVovermind.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59417355?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Ratings: Blue Bloods Eyes Possible 11-Month High, Five-0 Also Rises</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59402496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTA1NTk0MjMyOTFeQTJeQWpwZ15BbWU3MDA4NzEzMTM@._V1_SY150_CR6,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59402496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Kate Hudson: Mom Goldie Hawn and I Bond Over Being the Only Women in the Family</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>Popsugar.com</a></span>
    </div>
                                </div>
<p><a href="/name/nm0005028?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Kate Hudson</a> and <a href="/name/nm0000443?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Goldie Hawn</a>Â have the kind of close-as-can-be relationship most mothers and daughters strive for. When we caught up with Kate to talk about her new movie, <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk3">Kung Fu Panda 3</a>, our conversation aboutÂ the movie's themes of family quickly turned to Kate'sÂ own relatives. When we mentioned ...                                        <span class="nobr"><a href="/news/ni59402496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59416317?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Inside Miley Cyrus & Liam Hemsworth's Engagement: The Real Reason Why the Timing Is Finally Right for Them</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59416409?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Carol Burnett on Her Daughter's Drug Addiction: 'It Was Hell'</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59417428?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Daniel Radcliffe and His Girlfriend Are the Coziest Couple at Sundance</a>
    <div class="infobar">
            <span class="text-muted">24 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59417431?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Caitlyn Jenner Takes First-Ever Selfie Stick for a Twirl Through Her Home</a>
    <div class="infobar">
            <span class="text-muted">34 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg616602368?ref_=hm_snp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395356322&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Photos We Love: Week of January 22</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2992957184/rg616602368?ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395356322&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jennifer Aniston and Justin Theroux" alt="Jennifer Aniston and Justin Theroux" src="http://ia.media-imdb.com/images/M/MV5BMTk5MDM1OTc5NF5BMl5BanBnXkFtZTgwNjE2MjQ3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk5MDM1OTc5NF5BMl5BanBnXkFtZTgwNjE2MjQ3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1813047040/rg616602368?ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395356322&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Morgan Spurlock" alt="Morgan Spurlock" src="http://ia.media-imdb.com/images/M/MV5BMTUwMzgxNzQ4OF5BMl5BanBnXkFtZTgwNTU1MDk3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwMzgxNzQ4OF5BMl5BanBnXkFtZTgwNTU1MDk3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1955456768/rg616602368?ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395356322&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Kate McKinnon and Adam Driver" alt="Kate McKinnon and Adam Driver" src="http://ia.media-imdb.com/images/M/MV5BMjA1MjE5MjE1Nl5BMl5BanBnXkFtZTgwMTY5NDI3NzE@._V1_SY201_CR49,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA1MjE5MjE1Nl5BMl5BanBnXkFtZTgwMTY5NDI3NzE@._V1_SY201_CR49,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg616602368?ref_=hm_snp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395356322&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See the gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=1-23&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm2930503?ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jack Reynor" alt="Jack Reynor" src="http://ia.media-imdb.com/images/M/MV5BMjMzNTk0NDc3OV5BMl5BanBnXkFtZTgwMzE1MDAxMTE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMzNTk0NDc3OV5BMl5BanBnXkFtZTgwMzE1MDAxMTE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm2930503?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Jack Reynor</a> (24) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1247407?ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Julia Jones" alt="Julia Jones" src="http://ia.media-imdb.com/images/M/MV5BMzQyNjY0ODg4NV5BMl5BanBnXkFtZTcwNzQ4ODM3OA@@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzQyNjY0ODg4NV5BMl5BanBnXkFtZTcwNzQ4ODM3OA@@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1247407?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Julia Jones</a> (35) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0002127?ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Mariska Hargitay" alt="Mariska Hargitay" src="http://ia.media-imdb.com/images/M/MV5BMTgyOTEyMTMwMF5BMl5BanBnXkFtZTcwMTA3MTU5MQ@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgyOTEyMTMwMF5BMl5BanBnXkFtZTcwMTA3MTU5MQ@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0002127?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Mariska Hargitay</a> (52) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005485?ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Tiffani Thiessen" alt="Tiffani Thiessen" src="http://ia.media-imdb.com/images/M/MV5BNjI3NTU2MjM5Ml5BMl5BanBnXkFtZTgwODQwNzM0NDE@._V1_SY172_CR7,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjI3NTU2MjM5Ml5BMl5BanBnXkFtZTgwODQwNzM0NDE@._V1_SY172_CR7,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005485?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Tiffani Thiessen</a> (42) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000442?ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Rutger Hauer" alt="Rutger Hauer" src="http://ia.media-imdb.com/images/M/MV5BMTg4NDAyMjYxM15BMl5BanBnXkFtZTcwNDA0MTg1Ng@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg4NDAyMjYxM15BMl5BanBnXkFtZTcwNDA0MTg1Ng@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Rutger Hauer</a> (72) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=1-23&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-6"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/offsite/?page-action=offsite-amazon&token=BCYihmUlY8SnecrxNBBNXbAZvDir5XemcxIkmfLTgj-UWtWjECHVEOWDVI_50YsC_D4zFH1NgdRZ%0D%0AumJpCv5I2eLvbkcBIucqB_x8AOE_MgG79_-E4tMiWi1opOI0yLSxCvd4WWy5Fv6OtuPw41_k15Vt%0D%0AkjC3zd-mbBJHx19ZVqh9YO_JY2ZypI9EaVPHL0K1hEgfvjAXKNt7Nbvh85BNrgyv6SMtaGdqbRkN%0D%0ALKDKwUPjxjwUQH5pOoyVtNhGAWl99l3c%0D%0A&ref_=hm_pop_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394163162&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Live From The IMDb Studio at Sundance</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYnWsKSd7yjH-HdExfhfTEf4mhx10PvFAm8NjuiOAQXiB1XUFw1hL8ZQ2RXFr1fL5Gk8jm45cpq%0D%0ArvJVJQdkuWqo1J4iT94CFJMmom9gufBC41OZFm__84a1USkpsFfbfrqOY7gOnhjUmlQpRO8h7DrB%0D%0AlD1NgJM4A8dHXM-KroDXe8wafdjQf0gBBigXSAeuRkCk22kPCbc-Z8QnZNTpDJ12nbl4ZHAHkFEx%0D%0Ao0wWf2jgSM0%0D%0A&ref_=hm_pop_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394163162&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BOTUyMTQ2MTI2OF5BMl5BanBnXkFtZTgwNjU4OTc3NzE@._UX500_CR20,25,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTUyMTQ2MTI2OF5BMl5BanBnXkFtZTgwNjU4OTc3NzE@._UX500_CR20,25,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><p class="blurb">Join us live from The IMDb Studio at Sundance on Saturday, Jan. 23, at 8 p.m. ET/5 p.m. PT for a live conversation with <a href="/name/nm1754366/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394163162&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk1">Tika Sumpter</a>, star of <i><a href="/title/tt4258698/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394163162&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk2">Southside With You</a></i>. Catch us Sunday at the same time for more live interviews with Sundance stars at <a href="/offsite/?page-action=offsite-amazon&token=BCYlNFgUf6KQTHdh4e64EJkZ0lhGVSpX5QCbHyrRgy4_0v9jw-R5xqXT0J1ltDrswE30BRMh0pwe%0D%0AlMDFVoGG1hKPVtdKwL5c_xHgSKLQ1-7D9o-zri_rBoVinUQUGTusgkhLxP3cQTEArtwPzR83vJHC%0D%0A6MrJIbzqw-ae6aB6La8HcD_fP_74Sb-x0JKCkyDVrDSll0MRb8W2MB_TUKcX80Lj3FRHA9omaVN3%0D%0AdBmT7K0tYaA%0D%0A&ref_=hm_pop_lk3">Amazon.com/IMDbAsks</a>. This livestream is best viewed on laptops, desktops, and tablets.</p> <p class="seemore"><a href="/offsite/?page-action=offsite-amazon&token=BCYjWhHCe4ybCWl9BvjbLGswpyOKHBQZvujAqSKPv25Z36dM_72e_LZPZgQH3592VBFh-kF24X_U%0D%0AwLLlUe47At3qbbCLaehRJGb4GoCWvcl71c6yB_OVUA1A4dW3W4uHfkdlkGuCFQmxr-PU-ht0GjwP%0D%0Aow6hBo7Jn11CR8abjRPNZc_P-L5smWkjQdFxibo5pU6GFfvL3dM1To1n6jLGhm1yimJa1ykrmcpd%0D%0AqFsu4oj3E5qW9SOiVgLDFJUamuE5HsGc%0D%0A&ref_=hm_pop_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394163162&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >Tune in here for the one-on-one interview</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Photos From the 2016 Sundance Film Festival</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-1/?imageid=rm3823821824&ref_=hm_sun_ph_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355122&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="AnnaLynne McCord at event of The IMDb Studio (2015)" alt="AnnaLynne McCord at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg0OTIzNjE5Nl5BMl5BanBnXkFtZTgwMTg0NDk3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0OTIzNjE5Nl5BMl5BanBnXkFtZTgwMTg0NDk3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-1/?ref_=hm_sun_ph_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355122&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio at Sundance -- Day 1 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/premieres-and-parties/?imageid=rm1240327168&ref_=hm_sun_ph_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355122&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Rhys Darby" alt="Rhys Darby" src="http://ia.media-imdb.com/images/M/MV5BMTQ1NzMxOTgwN15BMl5BanBnXkFtZTgwNDQ3NTk3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1NzMxOTgwN15BMl5BanBnXkFtZTgwNDQ3NTk3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/premieres-and-parties/?ref_=hm_sun_ph_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355122&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Premieres and Parties </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/portraits-and-people/?imageid=rm202499840&ref_=hm_sun_ph_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355122&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Paul Dano, Daniel Radcliffe, Daniel Scheinert, Dan Kwan and Mary Elizabeth" alt="Paul Dano, Daniel Radcliffe, Daniel Scheinert, Dan Kwan and Mary Elizabeth" src="http://ia.media-imdb.com/images/M/MV5BMjMyMTkxMzM2MF5BMl5BanBnXkFtZTgwNzIwMTk3NzE@._V1_SY201_CR45,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMyMTkxMzM2MF5BMl5BanBnXkFtZTgwNzIwMTk3NzE@._V1_SY201_CR45,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/portraits-and-people/?ref_=hm_sun_ph_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395355122&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Portraits and People </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/oscars-costume-design-makeup-hairstyling/?ref_=hm_ac_acd_cos_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760302&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Oscars&#174; Style: Costumes, Makeup, and Hairstyling</h3> </a> </span> </span> <p class="blurb">Get a closer look at the work of the <a href="/awards-central/oscars/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760302&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_cos_lk1">artists nominated</a> for Best Achievement in Costume Design and Best Achievement in Makeup and Hairstyling.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-costume-design-makeup-hairstyling/?imageid=rm3644711680&ref_=hm_ac_acd_cos_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760302&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Eddie Redmayne in The Danish Girl (2015)" alt="Still of Eddie Redmayne in The Danish Girl (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUwMjE3OTU1OF5BMl5BanBnXkFtZTgwMjgwMzkyNzE@._V1_SY201_CR31,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwMjE3OTU1OF5BMl5BanBnXkFtZTgwMjgwMzkyNzE@._V1_SY201_CR31,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-costume-design-makeup-hairstyling/?imageid=rm1752758016&ref_=hm_ac_acd_cos_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760302&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Nicholas Hoult in Mad Max: Fury Road (2015)" alt="Still of Nicholas Hoult in Mad Max: Fury Road (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTU5MTQwNDgzMl5BMl5BanBnXkFtZTgwMjE3NTE2NTE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU5MTQwNDgzMl5BMl5BanBnXkFtZTgwMjE3NTE2NTE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-costume-design-makeup-hairstyling/?imageid=rm2659837440&ref_=hm_ac_acd_cos_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760302&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Rooney Mara in Carol (2015)" alt="Still of Rooney Mara in Carol (2015)" src="http://ia.media-imdb.com/images/M/MV5BODA4MzQ2ODQ5OF5BMl5BanBnXkFtZTgwMDE1MTE2NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODA4MzQ2ODQ5OF5BMl5BanBnXkFtZTgwMDE1MTE2NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/oscars-costume-design-makeup-hairstyling/?ref_=hm_ac_acd_cos_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394760302&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >View the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt1355683/trivia?item=tr2110511&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1355683?ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="AlianÃ§a do Crime (2015)" alt="AlianÃ§a do Crime (2015)" src="http://ia.media-imdb.com/images/M/MV5BNzg0ODI3NDQxNF5BMl5BanBnXkFtZTgwMzgzNDA0NjE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzg0ODI3NDQxNF5BMl5BanBnXkFtZTgwMzgzNDA0NjE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt1355683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Black Mass</a></strong> <p class="blurb">The project was shelved after <a href="/name/nm0000136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Johnny Depp</a> dropped out. After Depp agreed to return, <a href="/name/nm0249291?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk2">Joel Edgerton</a> was unavailable. Edgerton returned in the role in the end.</p> <p class="seemore"><a href="/title/tt1355683/trivia?item=tr2110511&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;XTJi_KwcF-c&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Tarantino's Bar & Restaurant</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Uma Thurman and John Travolta in Pulp Fiction (1994)" alt="Still of Uma Thurman and John Travolta in Pulp Fiction (1994)" src="http://ia.media-imdb.com/images/M/MV5BNzU4MDkzNzEwOV5BMl5BanBnXkFtZTcwNDAzNDY3Mw@@._V1_SY207_CR106,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzU4MDkzNzEwOV5BMl5BanBnXkFtZTcwNDAzNDY3Mw@@._V1_SY207_CR106,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Michael Fassbender and Diane Kruger in Inglourious Basterds (2009)" alt="Still of Michael Fassbender and Diane Kruger in Inglourious Basterds (2009)" src="http://ia.media-imdb.com/images/M/MV5BMjIwMTgxOTA5OF5BMl5BanBnXkFtZTcwODE0OTY3Mg@@._V1_SY207_CR85,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIwMTgxOTA5OF5BMl5BanBnXkFtZTcwODE0OTY3Mg@@._V1_SY207_CR85,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Tim Roth and Amanda Plummer in Pulp Fiction (1994)" alt="Still of Tim Roth and Amanda Plummer in Pulp Fiction (1994)" src="http://ia.media-imdb.com/images/M/MV5BMjM3NDUxNzQ3NV5BMl5BanBnXkFtZTgwNDAwMzg5MTE@._V1_SY207_CR84,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM3NDUxNzQ3NV5BMl5BanBnXkFtZTgwNDAwMzg5MTE@._V1_SY207_CR84,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ordell & Louis" alt="Ordell & Louis" src="http://ia.media-imdb.com/images/M/MV5BMjA4NTY0MzI3N15BMl5BanBnXkFtZTYwMjc0Mjg2._V1_SY207_CR68,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA4NTY0MzI3N15BMl5BanBnXkFtZTYwMjc0Mjg2._V1_SY207_CR68,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Steve Buscemi, Quentin Tarantino, Michael Madsen and Edward Bunker in Reservoir Dogs (1992)" alt="Still of Steve Buscemi, Quentin Tarantino, Michael Madsen and Edward Bunker in Reservoir Dogs (1992)" src="http://ia.media-imdb.com/images/M/MV5BODU3MDYwMTMyMF5BMl5BanBnXkFtZTgwMzg3OTEwMjE@._V1_SY207_CR81,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODU3MDYwMTMyMF5BMl5BanBnXkFtZTgwMzg3OTEwMjE@._V1_SY207_CR81,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Which of these restaurants/bars from the mind of Quentin Tarantino would you most like to visit? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/246553417?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=444444578591;ord=444444578591?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=444444578591?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=444444578591?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1860213"></div> <div class="title"> <a href="/title/tt1860213?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Dirty Grandpa </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2304933"></div> <div class="title"> <a href="/title/tt2304933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The 5th Wave </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3882082"></div> <div class="title"> <a href="/title/tt3882082?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> The Boy </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2888046"></div> <div class="title"> <a href="/title/tt2888046?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Ip Man 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2049543"></div> <div class="title"> <a href="/title/tt2049543?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Synchronicity </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3781476"></div> <div class="title"> <a href="/title/tt3781476?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Monster Hunt </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2322517"></div> <div class="title"> <a href="/title/tt2322517?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Mojave </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4374460"></div> <div class="title"> <a href="/title/tt4374460?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Aferim! </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4019560"></div> <div class="title"> <a href="/title/tt4019560?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t8"> Exposed </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390109562&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2869728"></div> <div class="title"> <a href="/title/tt2869728?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Ride Along 2 </a> <span class="secondary-text">$35.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1663202"></div> <div class="title"> <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> The Revenant </a> <span class="secondary-text">$31.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Star Wars: Episode VII - The Force Awakens </a> <span class="secondary-text">$26.3M</span> </div> <div class="action"> <a href="/showtimes/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4172430"></div> <div class="title"> <a href="/title/tt4172430?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> 13 Hours: The Secret Soldiers of Benghazi </a> <span class="secondary-text">$16.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1528854"></div> <div class="title"> <a href="/title/tt1528854?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Daddy's Home </a> <span class="secondary-text">$9.5M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2267968"></div> <div class="title"> <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Kung Fu Panda 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2025690"></div> <div class="title"> <a href="/title/tt2025690?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The Finest Hours </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4667094"></div> <div class="title"> <a href="/title/tt4667094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Fifty Shades of Black </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2140037"></div> <div class="title"> <a href="/title/tt2140037?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Jane Got a Gun </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-2032675409._CB285310264_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/video/?ref_=hm_ac_obn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393684762&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Oscars by the Numbers</h3> </a> </span> </span> <p class="blurb">As the Oscars approach, we took a look all the way back to the first ceremony in 1929. Find out fun facts such as how much it cost to attend the prestigious event back then. Plus, do you know who gave the shortest Oscar speech ever and what she said?</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1150334233?ref_=hm_ac_obn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393684762&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1150334233" data-source="bylist" data-id="ls031574778" data-rid="0F6NK55KQ41KMQBSXB5B" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ac_obn" data-ref="hm_ac_obn_i_1"> <img itemprop="image" class="pri_image" title="IMDb Originals (2015-)" alt="IMDb Originals (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjMzMjQ4NzU1NV5BMl5BanBnXkFtZTgwMDkwODA3NzE@._V1_SY230_CR51,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMzMjQ4NzU1NV5BMl5BanBnXkFtZTgwMDkwODA3NzE@._V1_SY230_CR51,0,307,230_AL_UY460_UX614_AL_.jpg" /> <img alt="IMDb Originals (2015-)" title="IMDb Originals (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb Originals (2015-)" title="IMDb Originals (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/video/?ref_=hm_ac_obn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393684762&pf_rd_r=0F6NK55KQ41KMQBSXB5B&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch more IMDb Originals video</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYiSvH2xSP6MnkeyNuMDzRtItRsVEuOVoeNF--Q9Ao4wlqwEOsaqHhZY2L4TyukgHT9dIDleW5e%0D%0AEF9eTFGpi0T5XOEUWNKk7wcQwr19fQOJUFz4t3WHBenHB4w9MfFP3vmJ9M1xCKoxtIUNdJzu3lWt%0D%0AolpYmF1eENG6epg1aWZ5EKrGpDdvYg7pMsA984GZP_lbdPI2qk0BRSCgRMHRcWuNhD9COSEYDGwi%0D%0ARrbz3vm-F3o%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYmTru-uXi_HRPEH4VvQ7ux3uIvAbZ6mJuQIBChbAHJsvKKm-n6Lg087JDw-1NUzXFK9HY4d7RW%0D%0AikoWGCC3O7hmcu5Y51oU1N3h5J-3f8ladp_F_g0keKcBJ1QvO2I_zjDq3UC6V0ohpvLvyWiilz-o%0D%0A2_9vwB-VqmhQa5suql3jwf0U08K6zoAP2iq9hlYh2u9BwUsmlNZBbCMbcYysxp4F2w%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYio2kIz_1GSHiscW_NxZikzy9zPUCrnlA8B81JD7dhRBNDr25MT8LF_oSo5Vkr0NcFR6s2zG2F%0D%0AVSUhaYjhS2GezMkGTeVRZFZcSSKLF_k088J8kPrWZGXp20-KI9ItejrgaErsv8egrZ3IZEFQL-4l%0D%0AciuW4AefJbthmknQM75L5Eg508oIa5Fh9TJwhxEjmEFhIJb062PuIfenxnQeytSb6wl07E761cLc%0D%0ARcmEufVJ_8b7Iy-1VXGJwMdtJP2UXtzYNawuY4i4TF1hSgnMGQKRLJFvrlMOXeEFQoHNPe2h1otk%0D%0AnPFo3DvrEOHiaodNGine3NW--wkbLRJdGBIV_fs0wAv0aG9pTTF-yOlG6vId-BIRh5uSVZnghFrY%0D%0AZ0p-9GwoRVjeTpe_nXdUvkL9XNd4pDxgJJKtSqZb1FPg5BmHIBk%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYkAaAWItN1jNdunHSY1lR85pEwGW_lrAMRBG4KJioawzWNFn5ZGJUI2C-C5MO-G6BAJozbgZlQ%0D%0AnJRMGAitBjub0ZZgMfgAWJD1Mp_LfYx49S68svouc77FyqQmgUmEVYnpSRSv_ic-A11tQG4cECfr%0D%0ADvipTWkXmYdIzRY8-KA2u4P9O3Uk3ku4JMOEnlBeOJV75f87sD3jHo2J-a4xK1fn2g%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYsNDS8_FtjVA20K_j7kxD0Lx5C22de3r7YS91zHMb47WfG7rYpof_0A2k1X3ZDTqKS2eho_p_h%0D%0AiZySetoMT3YUA6NwTaV8hQ3LEuxF7IDRawAjpR3v3YqaORRBsFbuty_AqSoXaCsiLztxQ2QvfVXU%0D%0Ao4T9GBGXvT8NXsShtgHSB6UKzS3cNgfyjzUtsIA1WuJU%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYhIa15QTYopCqWfP07HYLXfXKTJTh_8mw0tyTew1j99LD193T8rtqOGhEfh6Aeh3HqXNTn441s%0D%0Abl9Xnq2_GIvoqcuTQ-FR48gUboXzR46xHjerC4GbScqcp_JX23DWMe4a9DEQ5n2JLHPbp_KbUMnS%0D%0ALwStgxX95h2kBjqA-iupVf_rbCbjfh1Jf31E4REHAyMe%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYjNHjq_ZjO7vKMfWxMRId2fl4iILzy5bPEd9tEWmyMGWNQIkZAPEO4qlcANdsgWAq7QoGXJCJk%0D%0AWS8s3ifnobWs-hBmyqCPKw_LT8zcPU7E765eVyp7eC0c9Yaxix87Kmr3yZj0sVLb6oFDLu9EoqAS%0D%0A4Yw3z9uZy-DZAR3JrSvnHn3_qone5KNOt_8xqKj2NF3HvUCQpLw5xGg0swfnx5uXlw%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYleJluZWccZcQZDYwjt3v6d9yFQAWE38TSbA6KuKo1MWXYcw061bEBZ8cCHyrP8i5DvoVRRBgI%0D%0ASECJqkXxECHHS-6gl1Gvx9EiBeP1UScOSdN3gr-FAyBgODGzqBO3fTOkeMPg9mQ4lhpm9J5j3l9T%0D%0AbYy18h2ln7V0fKQ4sNY0yfx24JStkqqHr2Ef1G2Xo67Y%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYjI8-avmJcuYtm6nU4_n3qA7cwQCvGSxVFz0ZaNsw0ykAeJkZzgJymI2Y3nCgOO1M4VKfZhnYo%0D%0AofhptRMRMGxQpffZ6zAL3uJM9HVSlBgKWlzOTEc16yKy6Q8RHeO3ie7XQo0tuV7SB3ADNpke43Vc%0D%0Am05wjFppX7iqI77HCHeh-RTHP3iDR8T2nCmc_SSLZlp1j54mhNLSqEhBMMaTtP7QNitMYFo93Lnd%0D%0AJJGpeI__IY1qm8ss7EECrZIn7S8KFGEt%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYrab_JSNGHpuyR7jRlKK__QVlV5_0XIz0hjV2Bc5-FUbcZG7OQH0zZLa-vPTFovKKlBfSOQl3U%0D%0Aben1Vr_VHzxozWh4V59ZMaVYNoAUhi6KC-iHyaTagVvbgKb5uwCqyTkqD7AdBPRuWpkWS5LW0LR-%0D%0ATNOQpoWqJZzW8zGTaSLRxilykEcLgz1ZlH-ICp3RAGX-nytaI-m1qT6xzx5I2sAnhzpOSBFR4lYn%0D%0A7U1Rh8KMrTw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYvSSTmzCUlvDaqk8691O0mvIN7Ag-0Mk-PrKpgqvKCH24SVbueu_JrYvnJC2d09UFNCHb4TzyZ%0D%0ASguAeulRdw-1a-o1_eVafChpqQ5vuKN07U6lwKrIudRegRcX74QEPvJ7VFcpQ63pVeHYrAalUvaf%0D%0Ax6mvSTvoD5igyJBZzEG6-zrZCF-pm1L4dENMeJslQSNtudWVm7JxDjSt-jV3bZWjoD_LgIPypPcp%0D%0AY2KhxMLh_gk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYvjnxhpq49WCHZropXqyfXtcA0iypOakTLhix9jZUv_IszBqTZgVlAUIo_ZMSRle6zUZZTWs9k%0D%0AFqF2MJ8LHXPG0Gct7ALHmx6JHG7a4jAuuhi4y_gK5-nWUnJ2eMgZnxgstjHh70vdigMP_R-HyFQ3%0D%0AOTkHcSFrtq0zgZ_Zb8P27TdLHHwxbWxpQmAqEO5TMPMJ-Fk_45F1dF8iSyyj2s32SzhVzVBMKRmY%0D%0ACA0OhDmKgLg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYuZ2lHnC4zk9Gm2L5DbuRlwBI6dsSQnWQZ6f07g5m9CFpRtFL2UmuIfkubQDSzgOzL4jM6Hm1w%0D%0AFZiWO1LbOxalQU02kHPvWY0V_81ZW2JFfx-xIjfz58iz8ibSVVHsscGQim0NG2LbKtH4ibZZF6t-%0D%0AkK-Ek6CX4YvtwB6CoOoB02YVafFJLNNn0uJiLvnPBHbf_f_0D1BRq77yTltTJny_6853utkdEIPw%0D%0Ag5xE4PCd5lE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYnxttAt5ChkKdSNfgCDynBAJy7_0MUwSlsTelMVVaGX96gYSEVTLDuSeg9N3z6n5yKVaL-ybMW%0D%0AKhJinCppvgR3cg8hbxI_VjQJpc_1wQHAG9iH4MhbztsnufQozFVBwdpJ1RZYg9D62tlC_JRiQPVE%0D%0AC6HATKSNT40M9HVhM7DJ6yV4wKNrmVyJ9_8OQ-pUjft3-6crUQJpQpaRIv4lKSDdWmFrvxFGolGu%0D%0ArThtxHxQ9VA%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYkYKDm0QttAwd7NsKmC1pDrRCBDTHkqn1vm2O9eKBYQ-O9lwHT47UNWQPLmR-qOr5yZ_1sdIHj%0D%0AmQG7wvr7aeqjCRKDFLcdlQYbzU-P0GcuLQhBhHTc3jlfDJXKzosWwNhZbbcoqzWMjrguH74GDRBd%0D%0AVQjYeeoWFwaZduFOVXrUXuotWf9N5GsBlZfZlQirNoX2X20xGDatep6_7DLPm8o2vz5C0l8LAAqC%0D%0AYxOVvVwv-9ITEMhMf-O_XSu1RHsx0s_P%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYmSR5WJeqWPCmXMW0-hujMChDXOOolUmdoVuHiPtGMMLSfqXlZ00xZdGHHhKhTnB5rQKV-kxv-%0D%0A_aFnL1h-mE6YCbF7FOBMzAq3uhl184TYZiDl_5a26ipodMcq_xmY1Re7F5QQNz4oybQUtKaKL3BY%0D%0A0bM5XY29BEvWKkOoBILTjeo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYpTvlnYqgRFMbN8szBb8z9PbBtoQVHmV5pdWjfV3VD-8mzrnk9G8PZL8O55XcoqduJhRe9qn4W%0D%0AWgNEaFSFfL97my7pa3T9vCm5q35kCkV1UoBHn_dGUWIv0hMCYRUj_IHvGqDBot8NubfyFxN2OyGX%0D%0AulKkMWa-k3VQp22xrAJD_9w%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-3617413113._CB300284919_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2635043045._CB299003638_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=010121fa5aa4e1ca89edba2946dd6588bab0e9f8808edda8a53715eff39fbdfe67bf",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=444444578591"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=444444578591&ord=444444578591";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="291"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
