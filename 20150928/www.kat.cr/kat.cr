<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-773a99c.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-773a99c.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-773a99c.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-773a99c.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-773a99c.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '773a99c',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-773a99c.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/2015%20tamil%20movie/" class="tag2">2015 tamil movie</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/doctor%20who/" class="tag3">doctor who</a>
	<a href="/search/everest/" class="tag2">everest</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag2">fear the walking dead</a>
	<a href="/search/fifa%2016/" class="tag2">fifa 16</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/heroes/" class="tag2">heroes</a>
	<a href="/search/heroes%20reborn/" class="tag3">heroes reborn</a>
	<a href="/search/hindi/" class="tag8">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/hotel%20transylvania/" class="tag2">hotel transylvania</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag6">kat ph com</a>
	<a href="/search/katti%20batti/" class="tag2">katti batti</a>
	<a href="/search/kis%20kisko%20pyaar%20karoon/" class="tag3">kis kisko pyaar karoon</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/mad%20max/" class="tag3">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/mission%20impossible/" class="tag2">mission impossible</a>
	<a href="/search/movies/" class="tag2">movies</a>
	<a href="/search/narcos/" class="tag2">narcos</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/paper%20towns/" class="tag2">paper towns</a>
	<a href="/search/pixels/" class="tag2">pixels</a>
	<a href="/search/ripsalot/" class="tag5">ripsalot</a>
	<a href="/search/san%20andreas/" class="tag2">san andreas</a>
	<a href="/search/scandal/" class="tag2">scandal</a>
	<a href="/search/sicario/" class="tag2">sicario</a>
	<a href="/search/tamil/" class="tag5">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag3">telugu 2015</a>
	<a href="/search/terminator%20genisys/" class="tag3">terminator genisys</a>
	<a href="/search/the%20player/" class="tag2">the player</a>
	<a href="/search/the%20visit/" class="tag2">the visit</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/top%20gun%20spanish/" class="tag2">top gun spanish</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11316021,0" class="icommentjs icon16" href="/terminator-genisys-2015-720p-brrip-x264-yify-t11316021.html#comment"> <em class="iconvalue">315</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/terminator-genisys-2015-720p-brrip-x264-yify-t11316021.html" class="cellMainLink">Terminator Genisys (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="916424347">873.97 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="25 Sep 2015, 14:03">2&nbsp;days</span></td>
			<td class="green center">32681</td>
			<td class="red lasttd center">27198</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11312756,0" class="icommentjs icon16" href="/san-andreas-2015-720p-brrip-x264-yify-t11312756.html#comment"> <em class="iconvalue">152</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/san-andreas-2015-720p-brrip-x264-yify-t11312756.html" class="cellMainLink">San Andreas (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="854987861">815.38 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 22:41">3&nbsp;days</span></td>
			<td class="green center">18315</td>
			<td class="red lasttd center">12879</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310157,0" class="icommentjs icon16" href="/mission-impossible-rogue-nation-2015-1080p-hdrip-x264-aac-jyk-t11310157.html#comment"> <em class="iconvalue">43</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/mission-impossible-rogue-nation-2015-1080p-hdrip-x264-aac-jyk-t11310157.html" class="cellMainLink">Mission Impossible Rogue Nation 2015 1080p HDRip x264 AAC-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3738534678">3.48 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="24 Sep 2015, 11:43">3&nbsp;days</span></td>
			<td class="green center">8523</td>
			<td class="red lasttd center">5101</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11314613,0" class="icommentjs icon16" href="/the-visit-2015-v2-hc-hdrip-xvid-ac3-evo-t11314613.html#comment"> <em class="iconvalue">89</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-visit-2015-v2-hc-hdrip-xvid-ac3-evo-t11314613.html" class="cellMainLink">The Visit 2015 V2 HC HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1514114207">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Sep 2015, 08:03">2&nbsp;days</span></td>
			<td class="green center">5109</td>
			<td class="red lasttd center">4421</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11309328,0" class="icommentjs icon16" href="/ted-2-2015-1080p-web-dl-x264-ac3-jyk-t11309328.html#comment"> <em class="iconvalue">48</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ted-2-2015-1080p-web-dl-x264-ac3-jyk-t11309328.html" class="cellMainLink">Ted 2 2015 1080p WEB-DL x264 AC3-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3218854847">3 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 07:41">3&nbsp;days</span></td>
			<td class="green center">3515</td>
			<td class="red lasttd center">1685</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11311487,0" class="icommentjs icon16" href="/11-blocks-2015-hdrip-xvid-ac3-evo-t11311487.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/11-blocks-2015-hdrip-xvid-ac3-evo-t11311487.html" class="cellMainLink">11 Blocks 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1477235533">1.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Sep 2015, 16:05">3&nbsp;days</span></td>
			<td class="green center">1858</td>
			<td class="red lasttd center">1764</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11315758,0" class="icommentjs icon16" href="/jurassic-world-2015-1080p-bluray-x264-sparks-t11315758.html#comment"> <em class="iconvalue">50</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/jurassic-world-2015-1080p-bluray-x264-sparks-t11315758.html" class="cellMainLink">Jurassic World 2015 1080p BluRay x264-SPARKS</a></div>
			</td>
			<td class="nobr center" data-sort="9554672235">8.9 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="25 Sep 2015, 12:41">2&nbsp;days</span></td>
			<td class="green center">766</td>
			<td class="red lasttd center">2333</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11317070,0" class="icommentjs icon16" href="/terremoto-a-falha-de-san-andreas-2015-5-1-ch-dublado-1080p-by-luanharper-t11317070.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/terremoto-a-falha-de-san-andreas-2015-5-1-ch-dublado-1080p-by-luanharper-t11317070.html" class="cellMainLink">Terremoto - A Falha de San Andreas (2015) 5.1 CH Dublado 1080p (By-LuanHarper)</a></div>
			</td>
			<td class="nobr center" data-sort="2260004360">2.1 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 19:30">2&nbsp;days</span></td>
			<td class="green center">1187</td>
			<td class="red lasttd center">1418</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11321006,0" class="icommentjs icon16" href="/kis-kisko-pyaar-karoon-2015-1cd-desiscr-rip-x264-aac-dus-t11321006.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/kis-kisko-pyaar-karoon-2015-1cd-desiscr-rip-x264-aac-dus-t11321006.html" class="cellMainLink">Kis Kisko Pyaar Karoon (2015) 1CD DesiSCR Rip - x264 AAC - DUS</a></div>
			</td>
			<td class="nobr center" data-sort="735810528">701.72 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="26 Sep 2015, 14:52">1&nbsp;day</span></td>
			<td class="green center">485</td>
			<td class="red lasttd center">2178</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11311691,0" class="icommentjs icon16" href="/pixels-2015-720p-webrip-x264-aac2-0-rarbg-t11311691.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/pixels-2015-720p-webrip-x264-aac2-0-rarbg-t11311691.html" class="cellMainLink">Pixels 2015 720p WEBRip x264 AAC2.0-RARBG</a></div>
			</td>
			<td class="nobr center" data-sort="2932018456">2.73 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 17:00">3&nbsp;days</span></td>
			<td class="green center">947</td>
			<td class="red lasttd center">978</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11313025,0" class="icommentjs icon16" href="/spy-2015-extended-1080p-brrip-x264-dts-jyk-t11313025.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/spy-2015-extended-1080p-brrip-x264-dts-jyk-t11313025.html" class="cellMainLink">Spy 2015 EXTENDED 1080p BRRip x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3527876744">3.29 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Sep 2015, 00:09">2&nbsp;days</span></td>
			<td class="green center">1137</td>
			<td class="red lasttd center">465</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11316182,0" class="icommentjs icon16" href="/paper-towns-2015-720p-hdrip-750mb-mkvcage-t11316182.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/paper-towns-2015-720p-hdrip-750mb-mkvcage-t11316182.html" class="cellMainLink">Paper Towns (2015) 720p HDRip 750MB - MkvCage</a></div>
			</td>
			<td class="nobr center" data-sort="787775340">751.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Sep 2015, 14:47">2&nbsp;days</span></td>
			<td class="green center">1004</td>
			<td class="red lasttd center">537</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11315161,0" class="icommentjs icon16" href="/ashby-2015-hdrip-xvid-ac3-evo-t11315161.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ashby-2015-hdrip-xvid-ac3-evo-t11315161.html" class="cellMainLink">Ashby 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1487461968">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Sep 2015, 11:15">2&nbsp;days</span></td>
			<td class="green center">818</td>
			<td class="red lasttd center">722</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313426,0" class="icommentjs icon16" href="/pay-the-ghost-2015-720p-hdrip-omitube-mkv-t11313426.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/pay-the-ghost-2015-720p-hdrip-omitube-mkv-t11313426.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/pay-the-ghost-2015-720p-hdrip-omitube-mkv-t11313426.html" class="cellMainLink">Pay the Ghost 2015 720p HDrip OmiTube.mkv</a></div>
			</td>
			<td class="nobr center" data-sort="761296633">726.03 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Sep 2015, 02:35">2&nbsp;days</span></td>
			<td class="green center">913</td>
			<td class="red lasttd center">212</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11317373,0" class="icommentjs icon16" href="/tomorrowland-il-mondo-di-domani-2015-italian-ac3-dual-bluray-1080p-x264-ig-t11317373.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/tomorrowland-il-mondo-di-domani-2015-italian-ac3-dual-bluray-1080p-x264-ig-t11317373.html" class="cellMainLink">Tomorrowland Il Mondo Di Domani 2015 ITALiAN AC3 DUAL BluRay 1080p x264-iG</a></div>
			</td>
			<td class="nobr center" data-sort="2866914586">2.67 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 20:37">2&nbsp;days</span></td>
			<td class="green center">507</td>
			<td class="red lasttd center">818</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313442,0" class="icommentjs icon16" href="/heroes-reborn-s01e01e02-hdtv-xvid-fum-ettv-t11313442.html#comment"> <em class="iconvalue">172</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/heroes-reborn-s01e01e02-hdtv-xvid-fum-ettv-t11313442.html" class="cellMainLink">Heroes Reborn S01E01E02 HDTV XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="728030871">694.3 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="25 Sep 2015, 02:41">2&nbsp;days</span></td>
			<td class="green center">12249</td>
			<td class="red lasttd center">3227</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308305,0" class="icommentjs icon16" href="/south-park-s19e02-hdtv-x264-killers-ettv-t11308305.html#comment"> <em class="iconvalue">91</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/south-park-s19e02-hdtv-x264-killers-ettv-t11308305.html" class="cellMainLink">South Park S19E02 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="97045201">92.55 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="24 Sep 2015, 02:37">3&nbsp;days</span></td>
			<td class="green center">7576</td>
			<td class="red lasttd center">245</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313491,0" class="icommentjs icon16" href="/how-to-get-away-with-murder-s02e01-hdtv-x264-lol-rartv-t11313491.html#comment"> <em class="iconvalue">91</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/how-to-get-away-with-murder-s02e01-hdtv-x264-lol-rartv-t11313491.html" class="cellMainLink">How to Get Away with Murder S02E01 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="260372990">248.31 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 02:55">2&nbsp;days</span></td>
			<td class="green center">6257</td>
			<td class="red lasttd center">902</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308176,0" class="icommentjs icon16" href="/empire-2015-s02e01-hdtv-x264-killers-ettv-t11308176.html#comment"> <em class="iconvalue">107</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/empire-2015-s02e01-hdtv-x264-killers-ettv-t11308176.html" class="cellMainLink">Empire 2015 S02E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="412569307">393.46 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Sep 2015, 02:02">3&nbsp;days</span></td>
			<td class="green center">5208</td>
			<td class="red lasttd center">1773</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308109,0" class="icommentjs icon16" href="/modern-family-s07e01-hdtv-x264-batv-ettv-t11308109.html#comment"> <em class="iconvalue">60</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/modern-family-s07e01-hdtv-x264-batv-ettv-t11308109.html" class="cellMainLink">Modern Family S07E01 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="233715085">222.89 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Sep 2015, 01:36">3&nbsp;days</span></td>
			<td class="green center">5143</td>
			<td class="red lasttd center">1395</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11313348,0" class="icommentjs icon16" href="/scandal-us-s05e01-hdtv-x264-killers-ettv-t11313348.html#comment"> <em class="iconvalue">45</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/scandal-us-s05e01-hdtv-x264-killers-ettv-t11313348.html" class="cellMainLink">Scandal US S05E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="291573274">278.07 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Sep 2015, 02:08">2&nbsp;days</span></td>
			<td class="green center">3905</td>
			<td class="red lasttd center">1250</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11318489,0" class="icommentjs icon16" href="/z-nation-s02e03-hdtv-x264-killers-ettv-t11318489.html#comment"> <em class="iconvalue">60</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/z-nation-s02e03-hdtv-x264-killers-ettv-t11318489.html" class="cellMainLink">Z Nation S02E03 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="630671913">601.46 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="26 Sep 2015, 03:11">1&nbsp;day</span></td>
			<td class="green center">3478</td>
			<td class="red lasttd center">1915</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11313286,0" class="icommentjs icon16" href="/greys-anatomy-s12e01-hdtv-x264-killers-rartv-t11313286.html#comment"> <em class="iconvalue">39</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/greys-anatomy-s12e01-hdtv-x264-killers-rartv-t11313286.html" class="cellMainLink">Greys Anatomy S12E01 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="315426145">300.81 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 01:55">2&nbsp;days</span></td>
			<td class="green center">3859</td>
			<td class="red lasttd center">380</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313534,0" class="icommentjs icon16" href="/dominion-s02e12-hdtv-x264-killers-ettv-t11313534.html#comment"> <em class="iconvalue">50</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/dominion-s02e12-hdtv-x264-killers-ettv-t11313534.html" class="cellMainLink">Dominion S02E12 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="265750152">253.44 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 03:05">2&nbsp;days</span></td>
			<td class="green center">3072</td>
			<td class="red lasttd center">350</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308063,0" class="icommentjs icon16" href="/rosewood-s01e01-hdtv-x264-killers-ettv-t11308063.html#comment"> <em class="iconvalue">40</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/rosewood-s01e01-hdtv-x264-killers-ettv-t11308063.html" class="cellMainLink">Rosewood S01E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="512688664">488.94 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Sep 2015, 01:11">3&nbsp;days</span></td>
			<td class="green center">2485</td>
			<td class="red lasttd center">987</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11318292,0" class="icommentjs icon16" href="/hawaii-five-0-2010-s06e01-hdtv-x264-lol-ettv-t11318292.html#comment"> <em class="iconvalue">36</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/hawaii-five-0-2010-s06e01-hdtv-x264-lol-ettv-t11318292.html" class="cellMainLink">Hawaii Five-0 2010 S06E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="473933049">451.98 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="26 Sep 2015, 01:58">1&nbsp;day</span></td>
			<td class="green center">2667</td>
			<td class="red lasttd center">495</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308461,0" class="icommentjs icon16" href="/survivor-s31e01-hdtv-x264-uav-ettv-t11308461.html#comment"> <em class="iconvalue">49</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/survivor-s31e01-hdtv-x264-uav-ettv-t11308461.html" class="cellMainLink">Survivor S31E01 HDTV x264-UAV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="903012730">861.18 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 03:24">3&nbsp;days</span></td>
			<td class="green center">2180</td>
			<td class="red lasttd center">295</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11318613,0" class="icommentjs icon16" href="/continuum-s04e04-hdtv-x264-killers-ettv-t11318613.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/continuum-s04e04-hdtv-x264-killers-ettv-t11318613.html" class="cellMainLink">Continuum S04E04 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="331543442">316.18 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="26 Sep 2015, 03:50">1&nbsp;day</span></td>
			<td class="green center">1980</td>
			<td class="red lasttd center">301</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11323474,0" class="icommentjs icon16" href="/doctor-who-2005-s09e02-web-dl-xvid-fum-ettv-t11323474.html#comment"> <em class="iconvalue">37</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/doctor-who-2005-s09e02-web-dl-xvid-fum-ettv-t11323474.html" class="cellMainLink">Doctor Who 2005 S09E02 WEB-DL XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="398413613">379.96 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="27 Sep 2015, 02:29">20&nbsp;hours</span></td>
			<td class="green center">1068</td>
			<td class="red lasttd center">903</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308508,0" class="icommentjs icon16" href="/the-ultimate-fighter-s22-e03-hdtv-x264-jkkk-sparrow-t11308508.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-ultimate-fighter-s22-e03-hdtv-x264-jkkk-sparrow-t11308508.html" class="cellMainLink">The Ultimate Fighter S22 E03 HDTV x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="576952879">550.23 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 03:46">3&nbsp;days</span></td>
			<td class="green center">1319</td>
			<td class="red lasttd center">149</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11314884,0" class="icommentjs icon16" href="/sam-smith-writing-s-on-the-wall-2015-mp3-single-vbuc-t11314884.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/sam-smith-writing-s-on-the-wall-2015-mp3-single-vbuc-t11314884.html" class="cellMainLink">Sam Smith - Writing&#039;s On the Wall 2015 {MP3 Single}~{VBUc}</a></div>
			</td>
			<td class="nobr center" data-sort="11256834">10.74 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Sep 2015, 09:39">2&nbsp;days</span></td>
			<td class="green center">598</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11316270,0" class="icommentjs icon16" href="/va-top-club-40-september-2015-mp3-320-kbps-t11316270.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-top-club-40-september-2015-mp3-320-kbps-t11316270.html" class="cellMainLink">VA - Top Club 40 - September (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="673968262">642.75 <span>MB</span></td>
			<td class="center">61</td>
			<td class="center"><span title="25 Sep 2015, 15:09">2&nbsp;days</span></td>
			<td class="green center">248</td>
			<td class="red lasttd center">122</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11311155,0" class="icommentjs icon16" href="/disclosure-caracal-deluxe-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11311155.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/disclosure-caracal-deluxe-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11311155.html" class="cellMainLink">Disclosure - Caracal (Deluxe) [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="162385103">154.86 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="24 Sep 2015, 15:45">3&nbsp;days</span></td>
			<td class="green center">238</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11312069,0" class="icommentjs icon16" href="/big-boi-phantogram-big-grams-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11312069.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/big-boi-phantogram-big-grams-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11312069.html" class="cellMainLink">Big Boi &amp; Phantogram â Big Grams [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="64877474">61.87 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="24 Sep 2015, 18:54">3&nbsp;days</span></td>
			<td class="green center">254</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310100,0" class="icommentjs icon16" href="/eagles-of-death-metal-zipper-down-2015-mp3-320kpbs-freak37-t11310100.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/eagles-of-death-metal-zipper-down-2015-mp3-320kpbs-freak37-t11310100.html" class="cellMainLink">Eagles Of Death Metal â Zipper Down (2015) [ mp3 320kpbs ] - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="108731114">103.69 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="24 Sep 2015, 11:29">3&nbsp;days</span></td>
			<td class="green center">246</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11311916,0" class="icommentjs icon16" href="/chvrches-every-open-eye-deluxe-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11311916.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/chvrches-every-open-eye-deluxe-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11311916.html" class="cellMainLink">CHVRCHES - Every Open Eye (Deluxe) [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GlODLS]</a></div>
			</td>
			<td class="nobr center" data-sort="133687275">127.49 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="24 Sep 2015, 18:11">3&nbsp;days</span></td>
			<td class="green center">204</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11312178,0" class="icommentjs icon16" href="/kaskade-automatic-2015-album-320-kbps-edm-enigmaticabhi-t11312178.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/kaskade-automatic-2015-album-320-kbps-edm-enigmaticabhi-t11312178.html" class="cellMainLink">Kaskade - Automatic (2015) [Album] [320 KBPS] [EDM] *EnigmaticAbhi*</a></div>
			</td>
			<td class="nobr center" data-sort="131596303">125.5 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="24 Sep 2015, 19:20">3&nbsp;days</span></td>
			<td class="green center">190</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11317643,0" class="icommentjs icon16" href="/rammstein-in-amerika-2015-320ak-t11317643.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/rammstein-in-amerika-2015-320ak-t11317643.html" class="cellMainLink">Rammstein - In Amerika (2015)320ak</a></div>
			</td>
			<td class="nobr center" data-sort="203696143">194.26 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="25 Sep 2015, 22:05">2&nbsp;days</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11319903,0" class="icommentjs icon16" href="/maroon-5-singles-2015-mp3-320kbps-h4ckus-glodls-t11319903.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/maroon-5-singles-2015-mp3-320kbps-h4ckus-glodls-t11319903.html" class="cellMainLink">Maroon 5 - Singles [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="105593729">100.7 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="26 Sep 2015, 09:46">1&nbsp;day</span></td>
			<td class="green center">184</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11312230,0" class="icommentjs icon16" href="/bryson-tiller-trapsoul-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11312230.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/bryson-tiller-trapsoul-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11312230.html" class="cellMainLink">Bryson Tiller - TRAPSOUL [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="108725709">103.69 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="24 Sep 2015, 19:35">3&nbsp;days</span></td>
			<td class="green center">183</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11311867,0" class="icommentjs icon16" href="/sia-alive-single-2015-mp3-320kbps-h4ckus-glodls-t11311867.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/sia-alive-single-2015-mp3-320kbps-h4ckus-glodls-t11311867.html" class="cellMainLink">Sia - Alive [Single] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="10858794">10.36 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="24 Sep 2015, 17:59">3&nbsp;days</span></td>
			<td class="green center">143</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11317413,0" class="icommentjs icon16" href="/trivium-silence-in-the-snow-2015-192ak-t11317413.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/trivium-silence-in-the-snow-2015-192ak-t11317413.html" class="cellMainLink">Trivium - Silence In The Snow (2015) 192ak</a></div>
			</td>
			<td class="nobr center" data-sort="61207271">58.37 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="25 Sep 2015, 20:45">2&nbsp;days</span></td>
			<td class="green center">132</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11317073,0" class="icommentjs icon16" href="/drake-future-what-a-time-to-be-alive-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11317073.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/drake-future-what-a-time-to-be-alive-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11317073.html" class="cellMainLink">Drake &amp; Future - What a Time to Be Alive (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="97657328">93.13 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="25 Sep 2015, 19:30">2&nbsp;days</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/vil-ambu-2015-original-acdrip-320kbps-vbr-mp3-35mb-lranger-t11323524.html" class="cellMainLink">Vil Ambu (2015) - [Original ACDRip - 320Kbps - VBR - Mp3 - 35MB][LRanger]</a></div>
			</td>
			<td class="nobr center" data-sort="38348453">36.57 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="27 Sep 2015, 02:59">20&nbsp;hours</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11314414,0" class="icommentjs icon16" href="/rudimental-we-the-generation-deluxe-2015-freak37-t11314414.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/rudimental-we-the-generation-deluxe-2015-freak37-t11314414.html" class="cellMainLink">Rudimental - We the Generation (Deluxe) (2015).....Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="77241210">73.66 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="25 Sep 2015, 07:07">2&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">8</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313596,0" class="icommentjs icon16" href="/metal-gear-solid-v-the-phantom-pain-v-1-0-0-5-2015-pc-repack-Ð¾Ñ-seyter-t11313596.html#comment"> <em class="iconvalue">159</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/metal-gear-solid-v-the-phantom-pain-v-1-0-0-5-2015-pc-repack-Ð¾Ñ-seyter-t11313596.html" class="cellMainLink">Metal Gear Solid V: The Phantom Pain [v 1.0.0.5] (2015) PC | RePack Ð¾Ñ SEYTER</a></div>
			</td>
			<td class="nobr center" data-sort="12916813534">12.03 <span>GB</span></td>
			<td class="center">62</td>
			<td class="center"><span title="25 Sep 2015, 03:21">2&nbsp;days</span></td>
			<td class="green center">796</td>
			<td class="red lasttd center">850</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11314012,0" class="icommentjs icon16" href="/nba-2k16-codex-t11314012.html#comment"> <em class="iconvalue">61</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/nba-2k16-codex-t11314012.html" class="cellMainLink">NBA.2K16-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="41570637332">38.72 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 05:32">2&nbsp;days</span></td>
			<td class="green center">281</td>
			<td class="red lasttd center">1556</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11316175,0" class="icommentjs icon16" href="/grand-ages-medieval-codex-t11316175.html#comment"> <em class="iconvalue">74</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/grand-ages-medieval-codex-t11316175.html" class="cellMainLink">Grand.Ages.Medieval-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="4372738423">4.07 <span>GB</span></td>
			<td class="center">90</td>
			<td class="center"><span title="25 Sep 2015, 14:45">2&nbsp;days</span></td>
			<td class="green center">321</td>
			<td class="red lasttd center">278</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310832,0" class="icommentjs icon16" href="/cities-skylines-after-dark-codex-t11310832.html#comment"> <em class="iconvalue">68</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/cities-skylines-after-dark-codex-t11310832.html" class="cellMainLink">Cities Skylines After Dark-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="2807761861">2.61 <span>GB</span></td>
			<td class="center">59</td>
			<td class="center"><span title="24 Sep 2015, 14:27">3&nbsp;days</span></td>
			<td class="green center">337</td>
			<td class="red lasttd center">203</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11311587,0" class="icommentjs icon16" href="/final-fantasy-v-reloaded-t11311587.html#comment"> <em class="iconvalue">44</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/final-fantasy-v-reloaded-t11311587.html" class="cellMainLink">FINAL.FANTASY.V-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="1011061208">964.22 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 16:30">3&nbsp;days</span></td>
			<td class="green center">309</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11322703,0" class="icommentjs icon16" href="/mad-max-repack-mulit9-rg-mechanics-t11322703.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/mad-max-repack-mulit9-rg-mechanics-t11322703.html" class="cellMainLink">Mad Max RePack Mulit9-RG Mechanics</a></div>
			</td>
			<td class="nobr center" data-sort="4244323954">3.95 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="26 Sep 2015, 20:59">1&nbsp;day</span></td>
			<td class="green center">239</td>
			<td class="red lasttd center">205</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11315048,0" class="icommentjs icon16" href="/soma-update-4-2015-pc-steam-rip-Ð¾Ñ-r-g-gamblers-t11315048.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/soma-update-4-2015-pc-steam-rip-Ð¾Ñ-r-g-gamblers-t11315048.html" class="cellMainLink">SOMA [Update 4] (2015) PC | Steam-Rip Ð¾Ñ R.G. Gamblers</a></div>
			</td>
			<td class="nobr center" data-sort="11381367753">10.6 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="25 Sep 2015, 10:35">2&nbsp;days</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11312525,0" class="icommentjs icon16" href="/redemption-cemetery-7-clock-of-fate-ce-2015-pc-final-t11312525.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/redemption-cemetery-7-clock-of-fate-ce-2015-pc-final-t11312525.html" class="cellMainLink">Redemption Cemetery 7: Clock of Fate CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="1030440350">982.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 21:12">3&nbsp;days</span></td>
			<td class="green center">161</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11318592,0" class="icommentjs icon16" href="/watchmen-the-end-is-nigh-r-g-mechanics-t11318592.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/watchmen-the-end-is-nigh-r-g-mechanics-t11318592.html" class="cellMainLink">Watchmen: The End is Nigh [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="1730160583">1.61 <span>GB</span></td>
			<td class="center">38</td>
			<td class="center"><span title="26 Sep 2015, 03:44">1&nbsp;day</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11319734,0" class="icommentjs icon16" href="/dark-canvas-3-a-murder-exposed-ce-2015-pc-final-t11319734.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/dark-canvas-3-a-murder-exposed-ce-2015-pc-final-t11319734.html" class="cellMainLink">Dark Canvas 3: A Murder Exposed CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="731461356">697.58 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Sep 2015, 08:54">1&nbsp;day</span></td>
			<td class="green center">105</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11311648,0" class="icommentjs icon16" href="/subway-surfers-1-45-0-apk-kenya-africa-mod-unlimited-keys-coins-unlocked-osmdroid-t11311648.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/subway-surfers-1-45-0-apk-kenya-africa-mod-unlimited-keys-coins-unlocked-osmdroid-t11311648.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/subway-surfers-1-45-0-apk-kenya-africa-mod-unlimited-keys-coins-unlocked-osmdroid-t11311648.html" class="cellMainLink">Subway Surfers 1.45.0 apk Kenya Africa Mod Unlimited Keys Coins Unlocked {OsmDroid}</a></div>
			</td>
			<td class="nobr center" data-sort="97396988">92.89 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 16:48">3&nbsp;days</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308720,0" class="icommentjs icon16" href="/rivals-of-aether-v0-0-2-t11308720.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/rivals-of-aether-v0-0-2-t11308720.html" class="cellMainLink">Rivals of Aether v0.0.2</a></div>
			</td>
			<td class="nobr center" data-sort="35259420">33.63 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 04:59">3&nbsp;days</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11315917,0" class="icommentjs icon16" href="/dying-light-ultimate-edition-v1-6-1-all-dlcs-repack-mr-dj-t11315917.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/dying-light-ultimate-edition-v1-6-1-all-dlcs-repack-mr-dj-t11315917.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/dying-light-ultimate-edition-v1-6-1-all-dlcs-repack-mr-dj-t11315917.html" class="cellMainLink">Dying Light Ultimate Edition v1.6.1 + All DLCs repack Mr DJ</a></div>
			</td>
			<td class="nobr center" data-sort="10716206851">9.98 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Sep 2015, 13:29">2&nbsp;days</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">121</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11320734,0" class="icommentjs icon16" href="/far-cry-4-v1-10-complete-edition-repack-by-corepack-t11320734.html#comment"> <em class="iconvalue">35</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/far-cry-4-v1-10-complete-edition-repack-by-corepack-t11320734.html" class="cellMainLink">Far Cry 4 [v1.10] Complete Edition - RePack by CorePack</a></div>
			</td>
			<td class="nobr center" data-sort="10381960757">9.67 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="26 Sep 2015, 14:09">1&nbsp;day</span></td>
			<td class="green center">21</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313099,0" class="icommentjs icon16" href="/might-and-magic-heroes-vii-steam-preload-3dm-t11313099.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/might-and-magic-heroes-vii-steam-preload-3dm-t11313099.html" class="cellMainLink">Might and Magic Heroes VII Steam Preload-3DM</a></div>
			</td>
			<td class="nobr center" data-sort="11353094092">10.57 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="25 Sep 2015, 00:45">2&nbsp;days</span></td>
			<td class="green center">28</td>
			<td class="red lasttd center">26</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308013,0" class="icommentjs icon16" href="/microsoft-ms-office-2016-pro-plus-rtm-16-0-4266-1003-32-64-bit-ratiborus-3-2-appzdam-t11308013.html#comment"> <em class="iconvalue">58</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-Type"></i>
                    <a href="/microsoft-ms-office-2016-pro-plus-rtm-16-0-4266-1003-32-64-bit-ratiborus-3-2-appzdam-t11308013.html" class="cellMainLink">Microsoft MS Office 2016 Pro Plus RTM 16.0.4266.1003 [32-64 bit] (Ratiborus 3.2) - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="2724702760">2.54 <span>GB</span></td>
			<td class="center">36</td>
			<td class="center"><span title="24 Sep 2015, 00:48">3&nbsp;days</span></td>
			<td class="green center">552</td>
			<td class="red lasttd center">465</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11310751,0" class="icommentjs icon16" href="/kms-office-activator-2016-ultimate-1-1-appzdam-t11310751.html#comment"> <em class="iconvalue">48</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/kms-office-activator-2016-ultimate-1-1-appzdam-t11310751.html" class="cellMainLink">KMS Office Activator 2016 Ultimate 1.1 - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="16798417">16.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 13:55">3&nbsp;days</span></td>
			<td class="green center">535</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11307758,0" class="icommentjs icon16" href="/microsoft-office-pro-plus-2016-v16-0-4266-1003-rtm-iso-x86-x64-deepstatus-t11307758.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/microsoft-office-pro-plus-2016-v16-0-4266-1003-rtm-iso-x86-x64-deepstatus-t11307758.html" class="cellMainLink">Microsoft Office Pro Plus 2016 v16.0.4266.1003 RTM ISO x86 x64 [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="2429184000">2.26 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 23:33">3&nbsp;days</span></td>
			<td class="green center">338</td>
			<td class="red lasttd center">266</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11318185,0" class="icommentjs icon16" href="/nero-2016-platinum-v17-0-02000-multilang-deepstatus-t11318185.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/nero-2016-platinum-v17-0-02000-multilang-deepstatus-t11318185.html" class="cellMainLink">Nero 2016 Platinum v17.0.02000 MultiLang [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="1068894651">1019.38 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="26 Sep 2015, 01:24">1&nbsp;day</span></td>
			<td class="green center">342</td>
			<td class="red lasttd center">175</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11311802,0" class="icommentjs icon16" href="/ccleaner-5-10-5373-crack-inc-pro-business-addition-by-cpul-ctrc-t11311802.html#comment"> <em class="iconvalue">43</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/ccleaner-5-10-5373-crack-inc-pro-business-addition-by-cpul-ctrc-t11311802.html" class="cellMainLink">CCleaner 5.10.5373-Crack Inc.Pro &amp; Business Addition - By~[CPUL] [CTRC]</a></div>
			</td>
			<td class="nobr center" data-sort="7356594">7.02 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 17:43">3&nbsp;days</span></td>
			<td class="green center">291</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313713,0" class="icommentjs icon16" href="/kmsauto-net-2015-1-3-9-portable-windows-office-activator-latest-appzdam-t11313713.html#comment"> <em class="iconvalue">31</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/kmsauto-net-2015-1-3-9-portable-windows-office-activator-latest-appzdam-t11313713.html" class="cellMainLink">KMSAuto Net 2015 1.3.9 Portable Windows + Office Activator [Latest] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="9491433">9.05 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="25 Sep 2015, 04:08">2&nbsp;days</span></td>
			<td class="green center">202</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11313683,0" class="icommentjs icon16" href="/ultraiso-premium-edition-9-6-2-3059-full-inc-key-cpul-ctrc-t11313683.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/ultraiso-premium-edition-9-6-2-3059-full-inc-key-cpul-ctrc-t11313683.html" class="cellMainLink">UltraISO Premium Edition 9.6.2.3059 + Full Inc key.. [CPUL][CTRC]</a></div>
			</td>
			<td class="nobr center" data-sort="4384945">4.18 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 03:58">2&nbsp;days</span></td>
			<td class="green center">153</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11307757,0" class="icommentjs icon16" href="/easeus-partition-master-technician-v10-8-multilang-deepstatus-t11307757.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/easeus-partition-master-technician-v10-8-multilang-deepstatus-t11307757.html" class="cellMainLink">EaseUS Partition Master Technician v10.8 Multilang [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="29750804">28.37 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Sep 2015, 23:33">3&nbsp;days</span></td>
			<td class="green center">134</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11311775,0" class="icommentjs icon16" href="/microsoft-project-office-pro-2016-rtm-16-0-4266-1003-28-languages-iso-32-64-bit-appzdam-t11311775.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/microsoft-project-office-pro-2016-rtm-16-0-4266-1003-28-languages-iso-32-64-bit-appzdam-t11311775.html" class="cellMainLink">Microsoft Project Office Pro 2016 RTM 16.0.4266.1003 + 28 Languages ISO [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="66346795008">61.79 <span>GB</span></td>
			<td class="center">28</td>
			<td class="center"><span title="24 Sep 2015, 17:36">3&nbsp;days</span></td>
			<td class="green center">12</td>
			<td class="red lasttd center">131</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11315837,0" class="icommentjs icon16" href="/internet-download-manager-idm-v-6-23-build-22-patch-4realtorrentz-t11315837.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/internet-download-manager-idm-v-6-23-build-22-patch-4realtorrentz-t11315837.html" class="cellMainLink">Internet Download Manager (IDM) v 6.23 Build 22 + Patch [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="7152985">6.82 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Sep 2015, 13:07">2&nbsp;days</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11320256,0" class="icommentjs icon16" href="/acronis-true-image-2016-19-0-5634-bootcd-multilang-deepstatus-t11320256.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/acronis-true-image-2016-19-0-5634-bootcd-multilang-deepstatus-t11320256.html" class="cellMainLink">Acronis True Image 2016.19.0.5634 + BootCD Multilang [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="946770831">902.91 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="26 Sep 2015, 11:37">1&nbsp;day</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11321978,0" class="icommentjs icon16" href="/daz3d-poser-22771-wasteground-vignette-t11321978.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/daz3d-poser-22771-wasteground-vignette-t11321978.html" class="cellMainLink">Daz3D - Poser - 22771 - Wasteground Vignette</a></div>
			</td>
			<td class="nobr center" data-sort="161715096">154.22 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Sep 2015, 17:20">1&nbsp;day</span></td>
			<td class="green center">55</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310513,0" class="icommentjs icon16" href="/windows-7-8-1-10-pro-x64-esd-en-us-sep-2015-generation2-t11310513.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/windows-7-8-1-10-pro-x64-esd-en-us-sep-2015-generation2-t11310513.html" class="cellMainLink">Windows 7- 8.1- 10 Pro X64 ESD en-US Sep 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="9093303265">8.47 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="24 Sep 2015, 12:57">3&nbsp;days</span></td>
			<td class="green center">24</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11316398,0" class="icommentjs icon16" href="/idm-internet-download-manager-6-23-build-22-crack-clean-appzdam-t11316398.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/idm-internet-download-manager-6-23-build-22-crack-clean-appzdam-t11316398.html" class="cellMainLink">IDM (Internet Download Manager) 6.23 Build 22 + Crack [CLEAN] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="11521543">10.99 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="25 Sep 2015, 15:37">2&nbsp;days</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/poser-daz3d-i13-fantastic-nights-v6-m6-ro-108107-t11322976.html" class="cellMainLink">Poser ~ DAZ3D : i13 Fantastic Nights V6 M6 [RO 108107]</a></div>
			</td>
			<td class="nobr center" data-sort="8218345">7.84 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="26 Sep 2015, 22:27">1&nbsp;day</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">3</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11322389,0" class="icommentjs icon16" href="/horriblesubs-charlotte-13-720p-mkv-t11322389.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/horriblesubs-charlotte-13-720p-mkv-t11322389.html" class="cellMainLink">[HorribleSubs] Charlotte - 13 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="342561693">326.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Sep 2015, 19:25">1&nbsp;day</span></td>
			<td class="green center">2003</td>
			<td class="red lasttd center">432</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11322514,0" class="icommentjs icon16" href="/leopard-raws-charlotte-13-end-bs11-1280x720-x264-aac-mp4-t11322514.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-charlotte-13-end-bs11-1280x720-x264-aac-mp4-t11322514.html" class="cellMainLink">[Leopard-Raws] Charlotte - 13 END (BS11 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="398014649">379.58 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Sep 2015, 19:55">1&nbsp;day</span></td>
			<td class="green center">1568</td>
			<td class="red lasttd center">542</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11312191,0" class="icommentjs icon16" href="/fansub-resistance-naruto-shippuuden-431-french-subbed-1280x720-mp4-t11312191.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/fansub-resistance-naruto-shippuuden-431-french-subbed-1280x720-mp4-t11312191.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 431 [French Subbed] (1280x720).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="240050124">228.93 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 19:22">3&nbsp;days</span></td>
			<td class="green center">1278</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310322,0" class="icommentjs icon16" href="/naruto-shippuden-431-eng-sub-480p-l-mbert-t11310322.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/naruto-shippuden-431-eng-sub-480p-l-mbert-t11310322.html" class="cellMainLink">Naruto Shippuden 431 [EnG SuB] 480p L@mBerT</a></div>
			</td>
			<td class="nobr center" data-sort="59710672">56.94 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="24 Sep 2015, 12:15">3&nbsp;days</span></td>
			<td class="green center">388</td>
			<td class="red lasttd center">100</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/commie-gatchaman-crowds-insight-12-dcce73c5-mkv-t11323231.html" class="cellMainLink">[Commie] Gatchaman Crowds insight - 12 [DCCE73C5].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="318711817">303.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="27 Sep 2015, 01:00">22&nbsp;hours</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/vivid-non-non-biyori-repeat-12-3e3cb332-mkv-t11310794.html" class="cellMainLink">[Vivid] Non Non Biyori Repeat - 12 [3E3CB332].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="329972617">314.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 14:15">3&nbsp;days</span></td>
			<td class="green center">129</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11314850,0" class="icommentjs icon16" href="/fff-highschool-dxd-born-tv-t11314850.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/fff-highschool-dxd-born-tv-t11314850.html" class="cellMainLink">[FFF] Highschool DxD BorN [TV]</a></div>
			</td>
			<td class="nobr center" data-sort="5865064265">5.46 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="25 Sep 2015, 09:26">2&nbsp;days</span></td>
			<td class="green center">92</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11323468,0" class="icommentjs icon16" href="/ipunisher-monster-musume-no-iru-nichijou-vol-1-bd-1280x720-x264-aac-uncensored-t11323468.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ipunisher-monster-musume-no-iru-nichijou-vol-1-bd-1280x720-x264-aac-uncensored-t11323468.html" class="cellMainLink">[iPUNISHER] Monster Musume no Iru Nichijou Vol.1 (BD 1280x720 x264 AAC UNCENSORED)</a></div>
			</td>
			<td class="nobr center" data-sort="872240173">831.83 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="27 Sep 2015, 02:24">20&nbsp;hours</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/commie-ore-monogatari-24-f3620fc6-mkv-t11309254.html" class="cellMainLink">[Commie] Ore Monogatari!! - 24 [F3620FC6].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="206865527">197.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 07:15">3&nbsp;days</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/animerg-one-piece-711-720p-scavvykid-mp4-t11323551.html" class="cellMainLink">[AnimeRG] One Piece - 711 [720p] [ScavvyKiD].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="290205236">276.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="27 Sep 2015, 03:10">20&nbsp;hours</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/fff-ore-monogatari-23-598fc9eb-mkv-t11314837.html" class="cellMainLink">[FFF] Ore Monogatari!! - 23 [598FC9EB].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="287499417">274.18 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Sep 2015, 09:20">2&nbsp;days</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/vivid-gakkou-gurashi-12-44dcde77-mkv-t11315667.html" class="cellMainLink">[Vivid] Gakkou Gurashi! - 12 [44DCDE77].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="403564969">384.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Sep 2015, 12:05">2&nbsp;days</span></td>
			<td class="green center">6</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/commie-cross-ange-tenshi-to-ryuu-no-rondo-volume-3-bd-720p-aac-t11309033.html" class="cellMainLink">[Commie] Cross Ange - Tenshi to Ryuu no Rondo - Volume 3 [BD 720p AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1625312830">1.51 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Sep 2015, 06:10">3&nbsp;days</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310943,0" class="icommentjs icon16" href="/animerg-naruto-shippuuden-431-english-subbed-480p-kami-t11310943.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/animerg-naruto-shippuuden-431-english-subbed-480p-kami-t11310943.html" class="cellMainLink">[AnimeRG] Naruto Shippuuden - 431 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="56884043">54.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 15:06">3&nbsp;days</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11317594,0" class="icommentjs icon16" href="/animerg-prison-school-12-720p-kangoku-gakuen-mp4-kotuwa-t11317594.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/animerg-prison-school-12-720p-kangoku-gakuen-mp4-kotuwa-t11317594.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/animerg-prison-school-12-720p-kangoku-gakuen-mp4-kotuwa-t11317594.html" class="cellMainLink">[AnimeRG] Prison School - 12 (720p) Kangoku Gakuen - MP4 [KoTuWa]</a></div>
			</td>
			<td class="nobr center" data-sort="209289491">199.59 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 21:46">2&nbsp;days</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">4</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11309647,0" class="icommentjs icon16" href="/hydroponics-for-the-home-grower-2015-pdf-gooner-t11309647.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/hydroponics-for-the-home-grower-2015-pdf-gooner-t11309647.html" class="cellMainLink">Hydroponics for the Home Grower (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="35781637">34.12 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 09:18">3&nbsp;days</span></td>
			<td class="green center">464</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11319575,0" class="icommentjs icon16" href="/assorted-magazines-bundle-september-26-2015-true-pdf-t11319575.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/assorted-magazines-bundle-september-26-2015-true-pdf-t11319575.html" class="cellMainLink">Assorted Magazines Bundle - September 26 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="933087026">889.86 <span>MB</span></td>
			<td class="center">50</td>
			<td class="center"><span title="26 Sep 2015, 08:18">1&nbsp;day</span></td>
			<td class="green center">326</td>
			<td class="red lasttd center">283</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308773,0" class="icommentjs icon16" href="/computer-gadget-gamer-mags-sept-24-2015-true-pdf-t11308773.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/computer-gadget-gamer-mags-sept-24-2015-true-pdf-t11308773.html" class="cellMainLink">Computer Gadget &amp; Gamer Mags - Sept 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="160306587">152.88 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="24 Sep 2015, 05:16">3&nbsp;days</span></td>
			<td class="green center">351</td>
			<td class="red lasttd center">113</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308512,0" class="icommentjs icon16" href="/mens-magazines-bundle-september-24-2015-true-pdf-t11308512.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/mens-magazines-bundle-september-24-2015-true-pdf-t11308512.html" class="cellMainLink">Mens Magazines Bundle - September 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="187130146">178.46 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="24 Sep 2015, 03:48">3&nbsp;days</span></td>
			<td class="green center">338</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310116,0" class="icommentjs icon16" href="/i-love-paper-paper-cutting-techniques-and-templates-for-amazing-toys-sculptures-props-and-costumes-2015-pdf-gooner-t11310116.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/i-love-paper-paper-cutting-techniques-and-templates-for-amazing-toys-sculptures-props-and-costumes-2015-pdf-gooner-t11310116.html" class="cellMainLink">I Love Paper - Paper-Cutting Techniques and Templates for Amazing Toys, Sculptures, Props and Costumes (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="71357212">68.05 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 11:36">3&nbsp;days</span></td>
			<td class="green center">296</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308263,0" class="icommentjs icon16" href="/womens-magazines-bundle-sept-24-2015-true-pdf-t11308263.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/womens-magazines-bundle-sept-24-2015-true-pdf-t11308263.html" class="cellMainLink">Womens Magazines Bundle - Sept 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="652070814">621.86 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="24 Sep 2015, 02:22">3&nbsp;days</span></td>
			<td class="green center">186</td>
			<td class="red lasttd center">97</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11315695,0" class="icommentjs icon16" href="/september-2015-new-ebook-releases-t11315695.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/september-2015-new-ebook-releases-t11315695.html" class="cellMainLink">September 2015 New eBook Releases</a></div>
			</td>
			<td class="nobr center" data-sort="112071024">106.88 <span>MB</span></td>
			<td class="center">174</td>
			<td class="center"><span title="25 Sep 2015, 12:13">2&nbsp;days</span></td>
			<td class="green center">186</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11319595,0" class="icommentjs icon16" href="/history-magazines-bundle-september-26-2015-true-pdf-t11319595.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/history-magazines-bundle-september-26-2015-true-pdf-t11319595.html" class="cellMainLink">History Magazines Bundle - September 26 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="158869684">151.51 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="26 Sep 2015, 08:24">1&nbsp;day</span></td>
			<td class="green center">173</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310676,0" class="icommentjs icon16" href="/how-to-get-dressed-a-costume-designer-s-secrets-for-making-your-clothes-look-fit-and-feel-amazing-2015-epub-gooner-t11310676.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/how-to-get-dressed-a-costume-designer-s-secrets-for-making-your-clothes-look-fit-and-feel-amazing-2015-epub-gooner-t11310676.html" class="cellMainLink">How to Get Dressed - A Costume Designer&#039;s Secrets for Making Your Clothes Look, Fit and Feel Amazing (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="11885230">11.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 13:36">3&nbsp;days</span></td>
			<td class="green center">179</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11319586,0" class="icommentjs icon16" href="/automobile-magazines-september-27-2015-true-pdf-t11319586.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/automobile-magazines-september-27-2015-true-pdf-t11319586.html" class="cellMainLink">Automobile Magazines - September 27 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="418928105">399.52 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span title="26 Sep 2015, 08:21">1&nbsp;day</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">93</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11319605,0" class="icommentjs icon16" href="/musician-magazines-bundle-september-26-2015-true-pdf-t11319605.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/musician-magazines-bundle-september-26-2015-true-pdf-t11319605.html" class="cellMainLink">Musician Magazines Bundle - September 26 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="147162463">140.35 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="26 Sep 2015, 08:26">1&nbsp;day</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11309596,0" class="icommentjs icon16" href="/how-to-use-automotive-diagnostic-scanners-2015-pdf-gooner-t11309596.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/how-to-use-automotive-diagnostic-scanners-2015-pdf-gooner-t11309596.html" class="cellMainLink">How To Use Automotive Diagnostic Scanners (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="54737206">52.2 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 09:05">3&nbsp;days</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/home-magazines-september-27-2015-true-pdf-t11323202.html" class="cellMainLink">Home Magazines - September 27 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="162338830">154.82 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="27 Sep 2015, 00:48">22&nbsp;hours</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11320785,0" class="icommentjs icon16" href="/the-hacker-playbook-2-practical-guide-to-penetration-testing-by-peter-kim-pdf-t11320785.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/the-hacker-playbook-2-practical-guide-to-penetration-testing-by-peter-kim-pdf-t11320785.html" class="cellMainLink">The Hacker Playbook 2: Practical Guide To Penetration Testing by Peter Kim.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="24335922">23.21 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Sep 2015, 14:29">1&nbsp;day</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11315152,0" class="icommentjs icon16" href="/programming-for-engineers-a-foundational-approach-to-learning-c-and-matlab-aaron-r-bradley-springer-2011-pdf-t11315152.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/programming-for-engineers-a-foundational-approach-to-learning-c-and-matlab-aaron-r-bradley-springer-2011-pdf-t11315152.html" class="cellMainLink">Programming for Engineers - A Foundational Approach to Learning C and Matlab - Aaron R. Bradley (Springer, 2011).pdf</a></div>
			</td>
			<td class="nobr center" data-sort="3138252">2.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Sep 2015, 11:13">2&nbsp;days</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">4</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11316630,0" class="icommentjs icon16" href="/va-golden-voices-6-cd-set-1999-flac-tfm-t11316630.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-golden-voices-6-cd-set-1999-flac-tfm-t11316630.html" class="cellMainLink">VA - Golden Voices - 6-CD Set - (1999)-[FLAC]-[TFM]</a></div>
			</td>
			<td class="nobr center" data-sort="1760459677">1.64 <span>GB</span></td>
			<td class="center">130</td>
			<td class="center"><span title="25 Sep 2015, 16:51">2&nbsp;days</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11317399,0" class="icommentjs icon16" href="/david-gilmour-studio-discography-1978-2015-bbm-flac-t11317399.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/david-gilmour-studio-discography-1978-2015-bbm-flac-t11317399.html" class="cellMainLink">David Gilmour - Studio Discography (1978 - 2015) BBM-FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="1216393777">1.13 <span>GB</span></td>
			<td class="center">58</td>
			<td class="center"><span title="25 Sep 2015, 20:42">2&nbsp;days</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11316494,0" class="icommentjs icon16" href="/julio-iglesias-mexico-2015-flac-t11316494.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/julio-iglesias-mexico-2015-flac-t11316494.html" class="cellMainLink">Julio Iglesias - Mexico (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="346004419">329.98 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="25 Sep 2015, 15:54">2&nbsp;days</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11312546,0" class="icommentjs icon16" href="/john-mclaughlin-black-light-2015-flac-t11312546.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/john-mclaughlin-black-light-2015-flac-t11312546.html" class="cellMainLink">John McLaughlin - Black Light (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="352581658">336.25 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="24 Sep 2015, 21:20">3&nbsp;days</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11316477,0" class="icommentjs icon16" href="/los-lobos-gates-of-gold-2015-flac-beolab1700-t11316477.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/los-lobos-gates-of-gold-2015-flac-beolab1700-t11316477.html" class="cellMainLink">Los Lobos - Gates of Gold (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="293960257">280.34 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="25 Sep 2015, 15:51">2&nbsp;days</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11320206,0" class="icommentjs icon16" href="/santana-caravanserai-2014-24-96-hd-flac-t11320206.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/santana-caravanserai-2014-24-96-hd-flac-t11320206.html" class="cellMainLink">Santana - Caravanserai (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1205016430">1.12 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span title="26 Sep 2015, 11:24">1&nbsp;day</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11312802,0" class="icommentjs icon16" href="/nina-simone-love-songs-flac-tntvillage-t11312802.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/nina-simone-love-songs-flac-tntvillage-t11312802.html" class="cellMainLink">Nina Simone - Love Songs [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="304237715">290.14 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="24 Sep 2015, 23:01">3&nbsp;days</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11317290,0" class="icommentjs icon16" href="/arcade-fire-reflektor-3cd-deluxe-2015-flac-sn3h1t87-glodls-t11317290.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/arcade-fire-reflektor-3cd-deluxe-2015-flac-sn3h1t87-glodls-t11317290.html" class="cellMainLink">Arcade Fire - Reflektor (3CD Deluxe) [2015] [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="645581670">615.67 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="25 Sep 2015, 20:21">2&nbsp;days</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11319683,0" class="icommentjs icon16" href="/van-morrison-duets-re-working-the-catalogue-2015-24-44-hd-flac-t11319683.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/van-morrison-duets-re-working-the-catalogue-2015-24-44-hd-flac-t11319683.html" class="cellMainLink">Van Morrison - Duets Re-Working The Catalogue (2015) [24-44 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="918965383">876.39 <span>MB</span></td>
			<td class="center">38</td>
			<td class="center"><span title="26 Sep 2015, 08:43">1&nbsp;day</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11317113,0" class="icommentjs icon16" href="/drake-future-what-a-time-to-be-alive-2015-flac-sn3h1t87-glodls-t11317113.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/drake-future-what-a-time-to-be-alive-2015-flac-sn3h1t87-glodls-t11317113.html" class="cellMainLink">Drake &amp; Future - What a Time To Be Alive (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="235312737">224.41 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="25 Sep 2015, 19:39">2&nbsp;days</span></td>
			<td class="green center">45</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11317249,0" class="icommentjs icon16" href="/fetty-wap-fetty-wap-deluxe-edition-2015-flac-sn3h1t87-glodls-t11317249.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/fetty-wap-fetty-wap-deluxe-edition-2015-flac-sn3h1t87-glodls-t11317249.html" class="cellMainLink">Fetty Wap - Fetty Wap (Deluxe Edition) (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="558913205">533.02 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span title="25 Sep 2015, 20:10">2&nbsp;days</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11316795,0" class="icommentjs icon16" href="/cecil-taylor-conquistador-2014-24-192-hd-flac-t11316795.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/cecil-taylor-conquistador-2014-24-192-hd-flac-t11316795.html" class="cellMainLink">Cecil Taylor - Conquistador! (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1501698570">1.4 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="25 Sep 2015, 17:45">2&nbsp;days</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308594,0" class="icommentjs icon16" href="/va-latin-music-flac-mp3-big-papi-japanese-import-victor-tokyo-1991-t11308594.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-latin-music-flac-mp3-big-papi-japanese-import-victor-tokyo-1991-t11308594.html" class="cellMainLink">VA - Latin Music [FLAC+MP3](Big Papi) Japanese Import Victor Tokyo 1991</a></div>
			</td>
			<td class="nobr center" data-sort="349021387">332.85 <span>MB</span></td>
			<td class="center">35</td>
			<td class="center"><span title="24 Sep 2015, 04:19">3&nbsp;days</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/ravel-romantic-classic-le-tombeau-de-couperin-piano-concerto-in-g-la-valse-bolero-nanut-pantelli-adolph-2003-flac-t11309760.html" class="cellMainLink">Ravel - Romantic Classic: Le Tombeau de Couperin, Piano Concerto in G, La Valse, Bolero [Nanut, Pantelli, Adolph] (2003) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="328925524">313.69 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="24 Sep 2015, 09:46">3&nbsp;days</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11322035,0" class="icommentjs icon16" href="/rolling-stones-december-s-children-2005-24-88-hd-flac-t11322035.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/rolling-stones-december-s-children-2005-24-88-hd-flac-t11322035.html" class="cellMainLink">Rolling Stones - December&#039;s Children (2005) [24-88 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="566433989">540.19 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span title="26 Sep 2015, 17:32">1&nbsp;day</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">12</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/quiz-can-you-guess-movie-v14/?unread=16940311">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Quiz; Can you guess this movie? V14
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/OvOvO/">OvOvO</a></span></span> <span title="27 Sep 2015, 23:17">4&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/monthly-music-series/?unread=16940306">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Monthly Music Series
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Snagus/">Snagus</a></span></span> <span title="27 Sep 2015, 23:13">8&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/post-your-favorite-android-games-recommended/?unread=16940302">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				POST your Favorite Android Games [Recommended]
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/GR8FL_LWYR/">GR8FL_LWYR</a></span></span> <span title="27 Sep 2015, 23:09">12&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/50-shows-see-you-die/?unread=16940301">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				50 Shows To See Before You Die
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Snagus/">Snagus</a></span></span> <span title="27 Sep 2015, 23:08">12&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/better-late-never/?unread=16940300">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Better late than never
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/GR8FL_LWYR/">GR8FL_LWYR</a></span></span> <span title="27 Sep 2015, 23:08">13&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/found-my-way-forum-hi-kat/?unread=16940299">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Found my way to the forum. Hi kat!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/GR8FL_LWYR/">GR8FL_LWYR</a></span></span> <span title="27 Sep 2015, 23:07">14&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="01 Sep 2015, 16:13">3&nbsp;weeks&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">5&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">6&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/magicpotions/post/children-by-watertiger/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Children by watertiger</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="27 Sep 2015, 14:23">8&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/Touro73/post/come-in-and-congratulate-the-soopah-doopah-smod-keka-umans-with-his-100k/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Come In And Congratulate The Soopah Doopah Smod Keka_Umans With His 100K!!!</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Touro73/">Touro73</a> <span title="27 Sep 2015, 02:12">21&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/zotobom/post/phiso-under-control-ep/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Phiso - Under Control EP</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/zotobom/">zotobom</a> <span title="26 Sep 2015, 14:31">yesterday</span></span></li>
	<li><a href="/blog/Bayfia/post/knowing-when-to-quit-the-heartache-of-hoarding/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Knowing when to quit - The heartache of &quot;hoarding&quot;</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/Bayfia/">Bayfia</a> <span title="26 Sep 2015, 13:23">yesterday</span></span></li>
	<li><a href="/blog/eclipze_angel/post/the-thing-i-am-not-good-at/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The thing I am not good at..</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/eclipze_angel/">eclipze_angel</a> <span title="26 Sep 2015, 10:08">yesterday</span></span></li>
	<li><a href="/blog/Excalibur1/post/signature-and-avatar-images/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Signature and Avatar Images</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Excalibur1/">Excalibur1</a> <span title="26 Sep 2015, 09:08">yesterday</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/back%20to%20the%20future%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				back to the future 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/adriana%20chechik/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				adriana chechik
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20prophet%20%282014%29/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				The Prophet (2014)
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/loli/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				loli
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/umineko%20no%20naku%20koro%20ni/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				umineko no naku koro ni
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/tiffany%20teen/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				tiffany teen
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/lea%20hart/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				lea hart
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/vegetable%20cock/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				vegetable cock
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ted2/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ted2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/mdtm%20045/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Mdtm 045
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/free%20movie%20download/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				free movie download
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
