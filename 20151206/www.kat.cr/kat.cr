<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-261c451.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-261c451.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-261c451.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-261c451.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-261c451.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '261c451',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-261c451.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<span  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></span>
<span  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></span>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <span  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></span>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag8">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/adele/" class="tag2">adele</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/arrow/" class="tag2">arrow</a>
	<a href="/search/christmas/" class="tag6">christmas</a>
	<a href="/search/creed/" class="tag2">creed</a>
	<a href="/search/creed%202015/" class="tag2">creed 2015</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/doctor%20who/" class="tag2">doctor who</a>
	<a href="/search/dual%20audio%20hindi/" class="tag3">dual audio hindi</a>
	<a href="/search/etrg/" class="tag2">etrg</a>
	<a href="/search/flac/" class="tag2">flac</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/goosebumps/" class="tag2">goosebumps</a>
	<a href="/search/grimm/" class="tag2">grimm</a>
	<a href="/search/grimm%20s05e05/" class="tag1">grimm s05e05</a>
	<a href="/search/hate%20story%203/" class="tag2">hate story 3</a>
	<a href="/search/hindi/" class="tag8">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/hotel%20transylvania%202/" class="tag2">hotel transylvania 2</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/jessica%20jones/" class="tag2">jessica jones</a>
	<a href="/search/krampus/" class="tag2">krampus</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/one%20punch%20man/" class="tag2">one punch man</a>
	<a href="/search/prem%20ratan%20dhan%20payo/" class="tag2">prem ratan dhan payo</a>
	<a href="/search/sicario/" class="tag2">sicario</a>
	<a href="/search/spectre/" class="tag4">spectre</a>
	<a href="/search/spectre%202015/" class="tag4">spectre 2015</a>
	<a href="/search/star%20wars/" class="tag3">star wars</a>
	<a href="/search/tamasha/" class="tag3">tamasha</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag3">telugu 2015</a>
	<a href="/search/the%20blacklist/" class="tag2">the blacklist</a>
	<a href="/search/the%20martian/" class="tag3">the martian</a>
	<a href="/search/the%20walk/" class="tag2">the walk</a>
	<a href="/search/the%20walking%20dead/" class="tag4">the walking dead</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11700076,0" class="icommentjs kaButton smallButton rightButton" href="/goosebumps-2015-hdrip-xvid-ac3-evo-t11700076.html#comment">118 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/goosebumps-2015-hdrip-xvid-ac3-evo-t11700076.html" class="cellMainLink">Goosebumps 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1501435005">1.4 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T21:51:45+00:00">04 Dec 2015, 21:51:45</span></td>
			<td class="green center">17259</td>
			<td class="red lasttd center">14565</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11697829,0" class="icommentjs kaButton smallButton rightButton" href="/earthfall-2015-hdrip-xvid-ac3-evo-t11697829.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/earthfall-2015-hdrip-xvid-ac3-evo-t11697829.html" class="cellMainLink">Earthfall 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1517508666">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T11:50:17+00:00">04 Dec 2015, 11:50:17</span></td>
			<td class="green center">4569</td>
			<td class="red lasttd center">4373</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11701241,0" class="icommentjs kaButton smallButton rightButton" href="/monkey-king-hero-is-back-2015-chinese-1080p-hdrip-x264-aac-jyk-t11701241.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/monkey-king-hero-is-back-2015-chinese-1080p-hdrip-x264-aac-jyk-t11701241.html" class="cellMainLink">Monkey King Hero is Back 2015 CHINESE 1080p HDRip x264 AAC-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="1946215241">1.81 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T04:00:05+00:00">05 Dec 2015, 04:00:05</span></td>
			<td class="green center">4222</td>
			<td class="red lasttd center">3296</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11702387,0" class="icommentjs kaButton smallButton rightButton" href="/hate-story-3-2015-dvdscr-x264-aac-ddr-t11702387.html#comment">35 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hate-story-3-2015-dvdscr-x264-aac-ddr-t11702387.html" class="cellMainLink">Hate Story 3 (2015) DVDSCR - x264 - AAC [DDR]</a></div>
			</td>
			<td class="nobr center" data-sort="1577320224">1.47 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T09:18:11+00:00">05 Dec 2015, 09:18:11</span></td>
			<td class="green center">2319</td>
			<td class="red lasttd center">4901</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698988,0" class="icommentjs kaButton smallButton rightButton" href="/hotel-transylvania-2-2015-webrip-1080p-dual-audio-rus-eng-t11698988.html#comment">28 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hotel-transylvania-2-2015-webrip-1080p-dual-audio-rus-eng-t11698988.html" class="cellMainLink">Hotel Transylvania 2 (2015) WEBRip 1080p | [DUAL AUDIO] | [RUS-ENG]</a></div>
			</td>
			<td class="nobr center" data-sort="4807725609">4.48 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T16:52:39+00:00">04 Dec 2015, 16:52:39</span></td>
			<td class="green center">3377</td>
			<td class="red lasttd center">2048</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11697773,0" class="icommentjs kaButton smallButton rightButton" href="/the-wannabe-2015-hdrip-xvid-etrg-t11697773.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-wannabe-2015-hdrip-xvid-etrg-t11697773.html" class="cellMainLink">The Wannabe 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="742267565">707.88 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T11:30:36+00:00">04 Dec 2015, 11:30:36</span></td>
			<td class="green center">3328</td>
			<td class="red lasttd center">1909</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11691940,0" class="icommentjs kaButton smallButton rightButton" href="/lost-in-the-sun-2015-720p-brrip-x264-aac-etrg-t11691940.html#comment">44 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lost-in-the-sun-2015-720p-brrip-x264-aac-etrg-t11691940.html" class="cellMainLink">Lost in the Sun 2015 720p BRRip x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="747529771">712.9 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T09:38:14+00:00">03 Dec 2015, 09:38:14</span></td>
			<td class="green center">3068</td>
			<td class="red lasttd center">987</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11702440,0" class="icommentjs kaButton smallButton rightButton" href="/bone-tomahawk-2015-brrip-xvid-ac3-evo-t11702440.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bone-tomahawk-2015-brrip-xvid-ac3-evo-t11702440.html" class="cellMainLink">Bone Tomahawk 2015 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1634695641">1.52 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T09:38:06+00:00">05 Dec 2015, 09:38:06</span></td>
			<td class="green center">2002</td>
			<td class="red lasttd center">2663</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11703377,0" class="icommentjs kaButton smallButton rightButton" href="/war-room-2015-french-bdrip-xvid-vivi-avi-t11703377.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/war-room-2015-french-bdrip-xvid-vivi-avi-t11703377.html" class="cellMainLink">War Room 2015 FRENCH BDRip XviD-ViVi avi</a></div>
			</td>
			<td class="nobr center" data-sort="1514645752">1.41 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T13:53:07+00:00">05 Dec 2015, 13:53:07</span></td>
			<td class="green center">2518</td>
			<td class="red lasttd center">1234</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11700516,0" class="icommentjs kaButton smallButton rightButton" href="/trainwreck-2015-unrated-1080p-bluray-x264-dts-jyk-t11700516.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/trainwreck-2015-unrated-1080p-bluray-x264-dts-jyk-t11700516.html" class="cellMainLink">Trainwreck 2015 UNRATED 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3484731990">3.25 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T23:56:19+00:00">04 Dec 2015, 23:56:19</span></td>
			<td class="green center">2145</td>
			<td class="red lasttd center">1795</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11695358,0" class="icommentjs kaButton smallButton rightButton" href="/la-isla-minima-2014-french-bdrip-xvid-avitech-avi-t11695358.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/la-isla-minima-2014-french-bdrip-xvid-avitech-avi-t11695358.html" class="cellMainLink">La Isla Minima 2014 FRENCH BDRiP XViD-AViTECH avi</a></div>
			</td>
			<td class="nobr center" data-sort="734627840">700.6 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T23:31:51+00:00">03 Dec 2015, 23:31:51</span></td>
			<td class="green center">2376</td>
			<td class="red lasttd center">467</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11702295,0" class="icommentjs kaButton smallButton rightButton" href="/the-martian-2015-ts720p-x264-aac-english-chs-mp4ba-t11702295.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-martian-2015-ts720p-x264-aac-english-chs-mp4ba-t11702295.html" class="cellMainLink">The Martian 2015 TS720P X264 AAC English CHS Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="3057647738">2.85 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T08:51:29+00:00">05 Dec 2015, 08:51:29</span></td>
			<td class="green center">494</td>
			<td class="red lasttd center">4202</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/shark-lake-2015-bdrip-xvid-ac3-evo-t11703338.html" class="cellMainLink">Shark Lake 2015 BDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1466795702">1.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T13:45:47+00:00">05 Dec 2015, 13:45:47</span></td>
			<td class="green center">953</td>
			<td class="red lasttd center">1197</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698883,0" class="icommentjs kaButton smallButton rightButton" href="/extinction-2015-bdrip-xvid-ac3-evo-t11698883.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/extinction-2015-bdrip-xvid-ac3-evo-t11698883.html" class="cellMainLink">Extinction 2015 BDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1489615528">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T16:30:23+00:00">04 Dec 2015, 16:30:23</span></td>
			<td class="green center">1065</td>
			<td class="red lasttd center">812</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698058,0" class="icommentjs kaButton smallButton rightButton" href="/emptiness-is-nothing-2015-720p-hdrip-h264-cinefox-movietam-t11698058.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/emptiness-is-nothing-2015-720p-hdrip-h264-cinefox-movietam-t11698058.html" class="cellMainLink">Emptiness is Nothing 2015 720p HDRip H264-CINEFOX [MovietaM]</a></div>
			</td>
			<td class="nobr center" data-sort="2135286406">1.99 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T12:49:11+00:00">04 Dec 2015, 12:49:11</span></td>
			<td class="green center">1330</td>
			<td class="red lasttd center">215</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690134,0" class="icommentjs kaButton smallButton rightButton" href="/arrow-s04e08-hdtv-x264-lol-ettv-t11690134.html#comment">275 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrow-s04e08-hdtv-x264-lol-ettv-t11690134.html" class="cellMainLink">Arrow S04E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="369311698">352.2 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T02:00:30+00:00">03 Dec 2015, 02:00:30</span></td>
			<td class="green center">16269</td>
			<td class="red lasttd center">1811</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11695764,0" class="icommentjs kaButton smallButton rightButton" href="/the-vampire-diaries-s07e08-hdtv-x264-lol-ettv-t11695764.html#comment">76 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-vampire-diaries-s07e08-hdtv-x264-lol-ettv-t11695764.html" class="cellMainLink">The Vampire Diaries S07E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="210752802">200.99 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T02:00:29+00:00">04 Dec 2015, 02:00:29</span></td>
			<td class="green center">9595</td>
			<td class="red lasttd center">1081</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11702048,0" class="icommentjs kaButton smallButton rightButton" href="/ash-vs-evil-dead-s01e06-hdtv-x264-killers-ettv-t11702048.html#comment">73 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ash-vs-evil-dead-s01e06-hdtv-x264-killers-ettv-t11702048.html" class="cellMainLink">Ash vs Evil Dead S01E06 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="230915452">220.22 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T07:43:29+00:00">05 Dec 2015, 07:43:29</span></td>
			<td class="green center">7368</td>
			<td class="red lasttd center">1195</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11696004,0" class="icommentjs kaButton smallButton rightButton" href="/the-originals-s03e08-hdtv-x264-lol-ettv-t11696004.html#comment">75 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-originals-s03e08-hdtv-x264-lol-ettv-t11696004.html" class="cellMainLink">The Originals S03E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="195460462">186.41 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T03:02:25+00:00">04 Dec 2015, 03:02:25</span></td>
			<td class="green center">7421</td>
			<td class="red lasttd center">738</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690233,0" class="icommentjs kaButton smallButton rightButton" href="/modern-family-s07e08-hdtv-x264-killers-ettv-t11690233.html#comment">57 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/modern-family-s07e08-hdtv-x264-killers-ettv-t11690233.html" class="cellMainLink">Modern Family S07E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="189860849">181.07 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T02:35:21+00:00">03 Dec 2015, 02:35:21</span></td>
			<td class="green center">7460</td>
			<td class="red lasttd center">535</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690398,0" class="icommentjs kaButton smallButton rightButton" href="/supernatural-s11e08-hdtv-x264-lol-ettv-t11690398.html#comment">106 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supernatural-s11e08-hdtv-x264-lol-ettv-t11690398.html" class="cellMainLink">Supernatural S11E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="223603141">213.24 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T03:00:24+00:00">03 Dec 2015, 03:00:24</span></td>
			<td class="green center">6767</td>
			<td class="red lasttd center">590</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11706399,0" class="icommentjs kaButton smallButton rightButton" href="/doctor-who-2005-s09e12-hdtv-x264-tastetv-rartv-t11706399.html#comment">31 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/doctor-who-2005-s09e12-hdtv-x264-tastetv-rartv-t11706399.html" class="cellMainLink">Doctor Who 2005 S09E12 HDTV x264-TASTETV[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="377679672">360.18 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T21:48:09+00:00">05 Dec 2015, 21:48:09</span></td>
			<td class="green center">6151</td>
			<td class="red lasttd center">1117</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690450,0" class="icommentjs kaButton smallButton rightButton" href="/empire-2015-s02e10-hdtv-x264-killers-ettv-t11690450.html#comment">45 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/empire-2015-s02e10-hdtv-x264-killers-ettv-t11690450.html" class="cellMainLink">Empire 2015 S02E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="391696413">373.55 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T03:09:21+00:00">03 Dec 2015, 03:09:21</span></td>
			<td class="green center">6174</td>
			<td class="red lasttd center">647</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690575,0" class="icommentjs kaButton smallButton rightButton" href="/south-park-s19e09-hdtv-x264-killers-ettv-t11690575.html#comment">89 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/south-park-s19e09-hdtv-x264-killers-ettv-t11690575.html" class="cellMainLink">South Park S19E09 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="112836548">107.61 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T03:42:23+00:00">03 Dec 2015, 03:42:23</span></td>
			<td class="green center">6332</td>
			<td class="red lasttd center">250</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690671,0" class="icommentjs kaButton smallButton rightButton" href="/american-horror-story-s05e08-hdtv-x264-killers-ettv-t11690671.html#comment">82 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/american-horror-story-s05e08-hdtv-x264-killers-ettv-t11690671.html" class="cellMainLink">American Horror Story S05E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="249598965">238.04 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T04:06:26+00:00">03 Dec 2015, 04:06:26</span></td>
			<td class="green center">5557</td>
			<td class="red lasttd center">450</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11701041,0" class="icommentjs kaButton smallButton rightButton" href="/grimm-s05e05-hdtv-x264-fleet-rartv-t11701041.html#comment">59 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/grimm-s05e05-hdtv-x264-fleet-rartv-t11701041.html" class="cellMainLink">Grimm S05E05 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="328129872">312.93 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T03:13:41+00:00">05 Dec 2015, 03:13:41</span></td>
			<td class="green center">5342</td>
			<td class="red lasttd center">780</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11701271,0" class="icommentjs kaButton smallButton rightButton" href="/z-nation-s02e13-hdtv-x264-killers-ettv-t11701271.html#comment">47 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/z-nation-s02e13-hdtv-x264-killers-ettv-t11701271.html" class="cellMainLink">Z Nation S02E13 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="301014807">287.07 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T04:05:19+00:00">05 Dec 2015, 04:05:19</span></td>
			<td class="green center">3675</td>
			<td class="red lasttd center">528</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11701243,0" class="icommentjs kaButton smallButton rightButton" href="/the-knick-s02e08-internal-hdtv-x264-killers-ettv-t11701243.html#comment">27 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-knick-s02e08-internal-hdtv-x264-killers-ettv-t11701243.html" class="cellMainLink">The Knick S02E08 INTERNAL HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="276842781">264.02 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T04:00:25+00:00">05 Dec 2015, 04:00:25</span></td>
			<td class="green center">1916</td>
			<td class="red lasttd center">202</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11701647,0" class="icommentjs kaButton smallButton rightButton" href="/gold-rush-s06e08-mammoth-channel-hdtv-x264-w4f-ettv-t11701647.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gold-rush-s06e08-mammoth-channel-hdtv-x264-w4f-ettv-t11701647.html" class="cellMainLink">Gold Rush S06E08 Mammoth Channel HDTV x264-W4F[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="377187405">359.71 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T05:56:27+00:00">05 Dec 2015, 05:56:27</span></td>
			<td class="green center">1874</td>
			<td class="red lasttd center">166</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690474,0" class="icommentjs kaButton smallButton rightButton" href="/survivor-s31e12-hdtv-x264-fum-ettv-t11690474.html#comment">32 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/survivor-s31e12-hdtv-x264-fum-ettv-t11690474.html" class="cellMainLink">Survivor S31E12 HDTV x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="386819523">368.9 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T03:16:21+00:00">03 Dec 2015, 03:16:21</span></td>
			<td class="green center">1706</td>
			<td class="red lasttd center">100</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11703137,0" class="icommentjs kaButton smallButton rightButton" href="/jul-my-world-2015-filoumoutonrip-mp3-t11703137.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jul-my-world-2015-filoumoutonrip-mp3-t11703137.html" class="cellMainLink">JUL MY WORLD 2015 - filoumoutonRIP MP3</a></div>
			</td>
			<td class="nobr center" data-sort="84977192">81.04 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T12:39:50+00:00">05 Dec 2015, 12:39:50</span></td>
			<td class="green center">3131</td>
			<td class="red lasttd center">260</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11703147,0" class="icommentjs kaButton smallButton rightButton" href="/booba-nero-nemisis-cd-fr-2015-street-filoumoutonrip-t11703147.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/booba-nero-nemisis-cd-fr-2015-street-filoumoutonrip-t11703147.html" class="cellMainLink">Booba-Nero_Nemisis-CD-FR-2015-STREET filoumoutonRIP</a></div>
			</td>
			<td class="nobr center" data-sort="90390281">86.2 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T12:42:58+00:00">05 Dec 2015, 12:42:58</span></td>
			<td class="green center">2596</td>
			<td class="red lasttd center">357</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11703125,0" class="icommentjs kaButton smallButton rightButton" href="/rohff-le-rohff-game-2015-mp3-192kbps-filoumoutonrip-t11703125.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rohff-le-rohff-game-2015-mp3-192kbps-filoumoutonrip-t11703125.html" class="cellMainLink">rohff-le rohff game-2015-mp3-192kbps-filoumoutonRIP</a></div>
			</td>
			<td class="nobr center" data-sort="118211424">112.74 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T12:36:14+00:00">05 Dec 2015, 12:36:14</span></td>
			<td class="green center">1908</td>
			<td class="red lasttd center">191</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11707021,0" class="icommentjs kaButton smallButton rightButton" href="/gradur-sheguey-vara-2-2015-filoumoutonrip-t11707021.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/gradur-sheguey-vara-2-2015-filoumoutonrip-t11707021.html" class="cellMainLink">Gradur - Sheguey Vara 2 (2015) filoumoutonRIP</a></div>
			</td>
			<td class="nobr center" data-sort="182559121">174.1 <span>MB</span></td>
			<td class="center">35</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T01:44:05+00:00">06 Dec 2015, 01:44:05</span></td>
			<td class="green center">1489</td>
			<td class="red lasttd center">347</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11695256,0" class="icommentjs kaButton smallButton rightButton" href="/rick-ross-black-market-rap-2015-t11695256.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rick-ross-black-market-rap-2015-t11695256.html" class="cellMainLink">Rick Ross - Black Market [Rap] [2015]</a></div>
			</td>
			<td class="nobr center" data-sort="185108464">176.53 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T23:02:42+00:00">03 Dec 2015, 23:02:42</span></td>
			<td class="green center">1485</td>
			<td class="red lasttd center">156</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11702147,0" class="icommentjs kaButton smallButton rightButton" href="/coldplay-a-head-full-of-dreams-japanese-edition-2015-mp3-320-kbps-cbr-sn3h1t87-glodls-t11702147.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/coldplay-a-head-full-of-dreams-japanese-edition-2015-mp3-320-kbps-cbr-sn3h1t87-glodls-t11702147.html" class="cellMainLink">Coldplay - A Head Full Of Dreams (Japanese Edition) [2015] [MP3-320 Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="130698741">124.64 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T08:06:05+00:00">05 Dec 2015, 08:06:05</span></td>
			<td class="green center">995</td>
			<td class="red lasttd center">327</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11693467,0" class="icommentjs kaButton smallButton rightButton" href="/kid-cudi-speedin-bullet-2-heaven-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11693467.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/kid-cudi-speedin-bullet-2-heaven-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11693467.html" class="cellMainLink">Kid Cudi - Speedin Bullet 2 Heaven (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="222971711">212.64 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T15:08:34+00:00">03 Dec 2015, 15:08:34</span></td>
			<td class="green center">1087</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11696416,0" class="icommentjs kaButton smallButton rightButton" href="/jeremih-late-nights-rnb-2015-t11696416.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jeremih-late-nights-rnb-2015-t11696416.html" class="cellMainLink">Jeremih - Late Nights [RNB] [2015]</a></div>
			</td>
			<td class="nobr center" data-sort="141006808">134.47 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T05:10:41+00:00">04 Dec 2015, 05:10:41</span></td>
			<td class="green center">1012</td>
			<td class="red lasttd center">125</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690306,0" class="icommentjs kaButton smallButton rightButton" href="/g-eazy-when-it-s-dark-out-rap-2015-t11690306.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/g-eazy-when-it-s-dark-out-rap-2015-t11690306.html" class="cellMainLink">G-Eazy - When It&#039;s Dark Out [Rap] [2015]</a></div>
			</td>
			<td class="nobr center" data-sort="147433531">140.6 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T02:46:37+00:00">03 Dec 2015, 02:46:37</span></td>
			<td class="green center">892</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11692929,0" class="icommentjs kaButton smallButton rightButton" href="/babyface-return-of-the-tender-lover-2015-mp3-320kbps-h4ckus-glodls-t11692929.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/babyface-return-of-the-tender-lover-2015-mp3-320kbps-h4ckus-glodls-t11692929.html" class="cellMainLink">Babyface - Return Of The Tender Lover [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="102872826">98.11 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T13:15:59+00:00">03 Dec 2015, 13:15:59</span></td>
			<td class="green center">761</td>
			<td class="red lasttd center">119</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11694850,0" class="icommentjs kaButton smallButton rightButton" href="/bruce-springsteen-the-ties-that-bind-the-river-collection-2015-freak37-t11694850.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bruce-springsteen-the-ties-that-bind-the-river-collection-2015-freak37-t11694850.html" class="cellMainLink">Bruce Springsteen-The Ties That Bind The River: Collection 2015...Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="474622898">452.64 <span>MB</span></td>
			<td class="center">56</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T21:21:45+00:00">03 Dec 2015, 21:21:45</span></td>
			<td class="green center">589</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11692395,0" class="icommentjs kaButton smallButton rightButton" href="/troye-sivan-blue-neighbourhood-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11692395.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/troye-sivan-blue-neighbourhood-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11692395.html" class="cellMainLink">Troye Sivan - Blue Neighbourhood [Deluxe Edition] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="141505836">134.95 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T11:38:31+00:00">03 Dec 2015, 11:38:31</span></td>
			<td class="green center">529</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11692603,0" class="icommentjs kaButton smallButton rightButton" href="/pimp-c-long-live-the-pimp-2015-mp3-320kbps-h4ckus-glodls-t11692603.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/pimp-c-long-live-the-pimp-2015-mp3-320kbps-h4ckus-glodls-t11692603.html" class="cellMainLink">Pimp C - Long Live The Pimp [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="128988273">123.01 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T12:33:55+00:00">03 Dec 2015, 12:33:55</span></td>
			<td class="green center">505</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11707874,0" class="icommentjs kaButton smallButton rightButton" href="/mp3-new-releases-2015-week-48-amazeballz-glodls-t11707874.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-48-amazeballz-glodls-t11707874.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 48 - AMAZEBALLZ [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4125960505">3.84 <span>GB</span></td>
			<td class="center">550</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T06:29:27+00:00">06 Dec 2015, 06:29:27</span></td>
			<td class="green center">211</td>
			<td class="red lasttd center">451</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-thats-what-i-call-music-92-2015-mp3-320kbps-h4ckus-glodls-t11704931.html" class="cellMainLink">VA - Now Thats What I Call Music 92 [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="381691318">364.01 <span>MB</span></td>
			<td class="center">53</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T15:48:21+00:00">05 Dec 2015, 15:48:21</span></td>
			<td class="green center">303</td>
			<td class="red lasttd center">152</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11694067,0" class="icommentjs kaButton smallButton rightButton" href="/dragon-quest-heroes-slime-edition-reloaded-t11694067.html#comment">113 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/dragon-quest-heroes-slime-edition-reloaded-t11694067.html" class="cellMainLink">Dragon Quest Heroes Slime Edition-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="21022087930">19.58 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T17:48:17+00:00">03 Dec 2015, 17:48:17</span></td>
			<td class="green center">926</td>
			<td class="red lasttd center">2142</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11697327,0" class="icommentjs kaButton smallButton rightButton" href="/fallout-4-update-3-2015-pc-repack-Ð¾Ñ-r-g-freedom-t11697327.html#comment">34 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/fallout-4-update-3-2015-pc-repack-Ð¾Ñ-r-g-freedom-t11697327.html" class="cellMainLink">Fallout 4 [Update 3] (2015) PC | RePack Ð¾Ñ R.G. Freedom</a></div>
			</td>
			<td class="nobr center" data-sort="20393556805">18.99 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T09:15:43+00:00">04 Dec 2015, 09:15:43</span></td>
			<td class="green center">839</td>
			<td class="red lasttd center">606</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698780,0" class="icommentjs kaButton smallButton rightButton" href="/fifa-15-ultimate-team-edition-update-8-2014-pc-repack-by-r-g-mechanics-t11698780.html#comment">44 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/fifa-15-ultimate-team-edition-update-8-2014-pc-repack-by-r-g-mechanics-t11698780.html" class="cellMainLink">FIFA 15: Ultimate Team Edition [Update 8] (2014) PC | RePack By R.G Mechanics</a></div>
			</td>
			<td class="nobr center" data-sort="7356018441">6.85 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T16:08:30+00:00">04 Dec 2015, 16:08:30</span></td>
			<td class="green center">764</td>
			<td class="red lasttd center">548</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11689485,0" class="icommentjs kaButton smallButton rightButton" href="/just-cause-3-xl-edition-true-multi9-not-cracked-fitgirl-repack-selective-download-from-15-gb-t11689485.html#comment">126 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/just-cause-3-xl-edition-true-multi9-not-cracked-fitgirl-repack-selective-download-from-15-gb-t11689485.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/just-cause-3-xl-edition-true-multi9-not-cracked-fitgirl-repack-selective-download-from-15-gb-t11689485.html" class="cellMainLink">Just Cause 3: XL Edition (True MULTI9, NOT CRACKED) [FitGirl Repack, Selective Download - from 15 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="23988334104">22.34 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T22:12:19+00:00">02 Dec 2015, 22:12:19</span></td>
			<td class="green center">174</td>
			<td class="red lasttd center">958</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11696977,0" class="icommentjs kaButton smallButton rightButton" href="/don-t-starve-v-1-158816-2-dlc-2013-repack-decepticon-t11696977.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/don-t-starve-v-1-158816-2-dlc-2013-repack-decepticon-t11696977.html" class="cellMainLink">Don&#039;t Starve [v 1.158816 + 2 DLC] (2013) RePack [Decepticon]</a></div>
			</td>
			<td class="nobr center" data-sort="344839753">328.86 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T07:35:26+00:00">04 Dec 2015, 07:35:26</span></td>
			<td class="green center">612</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11699325,0" class="icommentjs kaButton smallButton rightButton" href="/helldivers-codex-t11699325.html#comment">34 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/helldivers-codex-t11699325.html" class="cellMainLink">HELLDIVERS-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="5301507880">4.94 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T18:16:19+00:00">04 Dec 2015, 18:16:19</span></td>
			<td class="green center">411</td>
			<td class="red lasttd center">379</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11699017,0" class="icommentjs kaButton smallButton rightButton" href="/tom-clancy-s-splinter-cell-double-agent-2007-repack-samael-t11699017.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tom-clancy-s-splinter-cell-double-agent-2007-repack-samael-t11699017.html" class="cellMainLink">Tom Clancy&#039;s Splinter Cell: Double Agent (2007)RePack [Samael]</a></div>
			</td>
			<td class="nobr center" data-sort="5914301904">5.51 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T16:58:51+00:00">04 Dec 2015, 16:58:51</span></td>
			<td class="green center">395</td>
			<td class="red lasttd center">218</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11699741,0" class="icommentjs kaButton smallButton rightButton" href="/shadow-complex-remastered-2015-en-ru-fitgirl-repack-t11699741.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/shadow-complex-remastered-2015-en-ru-fitgirl-repack-t11699741.html" class="cellMainLink">Shadow Complex Remastered (2015) [En-Ru] [FitGirl Repack]</a></div>
			</td>
			<td class="nobr center" data-sort="714272202">681.18 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T20:17:27+00:00">04 Dec 2015, 20:17:27</span></td>
			<td class="green center">474</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11691173,0" class="icommentjs kaButton smallButton rightButton" href="/christmas-stories-4-puss-in-boots-collector-s-edition-asg-t11691173.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/christmas-stories-4-puss-in-boots-collector-s-edition-asg-t11691173.html" class="cellMainLink">Christmas Stories 4 - Puss in Boots Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="1456349270">1.36 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T06:14:32+00:00">03 Dec 2015, 06:14:32</span></td>
			<td class="green center">324</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11688779,0" class="icommentjs kaButton smallButton rightButton" href="/rocket-league-v-1-10-4-dlc-2015-r-g-mechanics-t11688779.html#comment">13 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/rocket-league-v-1-10-4-dlc-2015-r-g-mechanics-t11688779.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/rocket-league-v-1-10-4-dlc-2015-r-g-mechanics-t11688779.html" class="cellMainLink">Rocket League [v 1.10 + 4 DLC] (2015) [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="1700645024">1.58 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T19:15:29+00:00">02 Dec 2015, 19:15:29</span></td>
			<td class="green center">276</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11701949,0" class="icommentjs kaButton smallButton rightButton" href="/final-cut-6-fade-to-black-collector-s-edition-asg-t11701949.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/final-cut-6-fade-to-black-collector-s-edition-asg-t11701949.html" class="cellMainLink">Final Cut 6 - Fade to Black Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="1046136537">997.67 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T07:17:35+00:00">05 Dec 2015, 07:17:35</span></td>
			<td class="green center">257</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11702721,0" class="icommentjs kaButton smallButton rightButton" href="/outlast-whistleblower-2014-pc-repack-Ð¾Ñ-r-g-ÐÐµchanics-t11702721.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/outlast-whistleblower-2014-pc-repack-Ð¾Ñ-r-g-ÐÐµchanics-t11702721.html" class="cellMainLink">Outlast: Whistleblower (2014) PC | RePack Ð¾Ñ R.G. ÐÐµchanics</a></div>
			</td>
			<td class="nobr center" data-sort="3036229979">2.83 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T10:49:32+00:00">05 Dec 2015, 10:49:32</span></td>
			<td class="green center">208</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/cityconomy-service-for-your-city-2015-repack-r-g-freedom-t11703103.html" class="cellMainLink">Cityconomy: Service for your City (2015) RePack [R.G. Freedom]</a></div>
			</td>
			<td class="nobr center" data-sort="1243840224">1.16 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T12:31:29+00:00">05 Dec 2015, 12:31:29</span></td>
			<td class="green center">212</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11706749,0" class="icommentjs kaButton smallButton rightButton" href="/plague-inc-evolved-v-0-9-0-1-2014-repack-decepticon-t11706749.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/plague-inc-evolved-v-0-9-0-1-2014-repack-decepticon-t11706749.html" class="cellMainLink">Plague Inc: Evolved [v 0.9.0.1] (2014) RePack [Decepticon]</a></div>
			</td>
			<td class="nobr center" data-sort="295179920">281.51 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T00:00:13+00:00">06 Dec 2015, 00:00:13</span></td>
			<td class="green center">206</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11693360,0" class="icommentjs kaButton smallButton rightButton" href="/dreamfall-chapters-book-four-revelations-reloaded-t11693360.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/dreamfall-chapters-book-four-revelations-reloaded-t11693360.html" class="cellMainLink">Dreamfall Chapters Book Four Revelations-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="13943870974">12.99 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T14:43:03+00:00">03 Dec 2015, 14:43:03</span></td>
			<td class="green center">130</td>
			<td class="red lasttd center">169</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11694400,0" class="icommentjs kaButton smallButton rightButton" href="/avast-premier-antivirus-11-1-2245-2016-final-keys-2015-t11694400.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/avast-premier-antivirus-11-1-2245-2016-final-keys-2015-t11694400.html" class="cellMainLink">Avast Premier Antivirus 11.1.2245 [2016] Final + Keys (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="214406486">204.47 <span>MB</span></td>
			<td class="center">64</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T19:16:28+00:00">03 Dec 2015, 19:16:28</span></td>
			<td class="green center">438</td>
			<td class="red lasttd center">123</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11699340,0" class="icommentjs kaButton smallButton rightButton" href="/keys-for-eset-kaspersky-avast-dr-web-avira-december-4-2015-pc-t11699340.html#comment">20 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/keys-for-eset-kaspersky-avast-dr-web-avira-december-4-2015-pc-t11699340.html" class="cellMainLink">Keys for ESET, Kaspersky, Avast, Dr.Web, Avira [December 4] (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="3369388">3.21 <span>MB</span></td>
			<td class="center">138</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T18:19:25+00:00">04 Dec 2015, 18:19:25</span></td>
			<td class="green center">234</td>
			<td class="red lasttd center">246</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11692994,0" class="icommentjs kaButton smallButton rightButton" href="/find-my-android-phone-premium-v10-5-0-2015-android-t11692994.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/find-my-android-phone-premium-v10-5-0-2015-android-t11692994.html" class="cellMainLink">Find My Android Phone! Premium [v10.5.0] (2015) Android</a></div>
			</td>
			<td class="nobr center" data-sort="37428484">35.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T13:30:05+00:00">03 Dec 2015, 13:30:05</span></td>
			<td class="green center">322</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11700535,0" class="icommentjs kaButton smallButton rightButton" href="/windows-10-pro-core-x64-6in1-oem-en-us-dec-2015-generation2-t11700535.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-core-x64-6in1-oem-en-us-dec-2015-generation2-t11700535.html" class="cellMainLink">Windows 10 Pro-Core X64 6in1 OEM en-US Dec 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="4591333838">4.28 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T00:08:13+00:00">05 Dec 2015, 00:08:13</span></td>
			<td class="green center">173</td>
			<td class="red lasttd center">321</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11691168,0" class="icommentjs kaButton smallButton rightButton" href="/pinnacle-studio-ultimate-19-1-0-multilingual-x86x64-incl-keygen-team-os-t11691168.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pinnacle-studio-ultimate-19-1-0-multilingual-x86x64-incl-keygen-team-os-t11691168.html" class="cellMainLink">Pinnacle Studio Ultimate 19.1.0 Multilingual (x86x64) Incl keygen=-TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="6575618135">6.12 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T06:13:16+00:00">03 Dec 2015, 06:13:16</span></td>
			<td class="green center">185</td>
			<td class="red lasttd center">262</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11688832,0" class="icommentjs kaButton smallButton rightButton" href="/adobe-photoshop-cc-2015-1-v20151114-r-301-repack-d-akov-t11688832.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-photoshop-cc-2015-1-v20151114-r-301-repack-d-akov-t11688832.html" class="cellMainLink">Adobe Photoshop CC 2015.1 (v20151114.r.301) RePack [D!akov]</a></div>
			</td>
			<td class="nobr center" data-sort="1534730416">1.43 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T19:30:57+00:00">02 Dec 2015, 19:30:57</span></td>
			<td class="green center">243</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11701052,0" class="icommentjs kaButton smallButton rightButton" href="/corel-videostudio-pro-ultimate-x8-18-6-0-6-multilingual-x86x64-incl-keygen-team-os-t11701052.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/corel-videostudio-pro-ultimate-x8-18-6-0-6-multilingual-x86x64-incl-keygen-team-os-t11701052.html" class="cellMainLink">Corel VideoStudio Pro+Ultimate X8 18.6.0.6 Multilingual (x86x64) Incl Keygen-=TEAM OS</a></div>
			</td>
			<td class="nobr center" data-sort="8415241922">7.84 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T03:16:04+00:00">05 Dec 2015, 03:16:04</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">243</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11689178,0" class="icommentjs kaButton smallButton rightButton" href="/acronis-true-image-2016-19-0-build-6027-final-seven7i-patch-dec2015-seven7i-t11689178.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/acronis-true-image-2016-19-0-build-6027-final-seven7i-patch-dec2015-seven7i-t11689178.html" class="cellMainLink">Acronis True Image 2016 19.0 Build 6027 Final + Seven7i Patch Dec2015 Seven7i</a></div>
			</td>
			<td class="nobr center" data-sort="426358708">406.61 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T20:47:29+00:00">02 Dec 2015, 20:47:29</span></td>
			<td class="green center">190</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11688608,0" class="icommentjs kaButton smallButton rightButton" href="/tubemate-2-2-6-645-apk-modded-adfree-material-design-updated-osmdroid-t11688608.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tubemate-2-2-6-645-apk-modded-adfree-material-design-updated-osmdroid-t11688608.html" class="cellMainLink">TubeMate 2.2.6.645 apk Modded AdFree Material Design (Updated) {OsmDroid}</a></div>
			</td>
			<td class="nobr center" data-sort="2499928">2.38 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T18:41:28+00:00">02 Dec 2015, 18:41:28</span></td>
			<td class="green center">164</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698393,0" class="icommentjs kaButton smallButton rightButton" href="/photoshop-cc-2015-v16-1-0-inc-update-2-crack-32-64-bit-appzdam-t11698393.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/photoshop-cc-2015-v16-1-0-inc-update-2-crack-32-64-bit-appzdam-t11698393.html" class="cellMainLink">Photoshop CC 2015 (v16.1.0) Inc. Update 2 + Crack [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="1723970483">1.61 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T14:25:12+00:00">04 Dec 2015, 14:25:12</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11694692,0" class="icommentjs kaButton smallButton rightButton" href="/auslogics-boostspeed-8-1-2-0-repack-portable-kpojiuk-t11694692.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/auslogics-boostspeed-8-1-2-0-repack-portable-kpojiuk-t11694692.html" class="cellMainLink">AusLogics BoostSpeed 8.1.2.0 RePack &amp; Portable [KpoJIuK]</a></div>
			</td>
			<td class="nobr center" data-sort="14550612">13.88 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T20:27:46+00:00">03 Dec 2015, 20:27:46</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11688813,0" class="icommentjs kaButton smallButton rightButton" href="/dll-suite-9-0-0-2190-repack-by-d-akov-t11688813.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dll-suite-9-0-0-2190-repack-by-d-akov-t11688813.html" class="cellMainLink">DLL Suite 9.0.0.2190 RePack by D!akov</a></div>
			</td>
			<td class="nobr center" data-sort="9343218">8.91 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T19:25:48+00:00">02 Dec 2015, 19:25:48</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698396,0" class="icommentjs kaButton smallButton rightButton" href="/adobe-illustrator-cc-2015-2-19-2-0-crack-macosx-appzdam-t11698396.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-illustrator-cc-2015-2-19-2-0-crack-macosx-appzdam-t11698396.html" class="cellMainLink">Adobe Illustrator CC 2015.2 (19.2.0) + Crack [MacOSX] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="2108224194">1.96 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T14:26:36+00:00">04 Dec 2015, 14:26:36</span></td>
			<td class="green center">101</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11699591,0" class="icommentjs kaButton smallButton rightButton" href="/re-loader-activator-2-0-rc-2-2015-windows-office-activator-t11699591.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/re-loader-activator-2-0-rc-2-2015-windows-office-activator-t11699591.html" class="cellMainLink">Re-Loader Activator 2.0 RC 2 (2015) [Windows &amp; Office Activator]</a></div>
			</td>
			<td class="nobr center" data-sort="2115508">2.02 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T19:27:23+00:00">04 Dec 2015, 19:27:23</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11693230,0" class="icommentjs kaButton smallButton rightButton" href="/bandicam-2-4-2-905-final-incl-keymaker-2015-t11693230.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/bandicam-2-4-2-905-final-incl-keymaker-2015-t11693230.html" class="cellMainLink">Bandicam 2.4.2.905 Final incl. Keymaker(2015)</a></div>
			</td>
			<td class="nobr center" data-sort="15866403">15.13 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T14:22:19+00:00">03 Dec 2015, 14:22:19</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">4</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698019,0" class="icommentjs kaButton smallButton rightButton" href="/fansub-resistance-naruto-shippuuden-440-1280x720-french-subbed-mp4-t11698019.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fansub-resistance-naruto-shippuuden-440-1280x720-french-subbed-mp4-t11698019.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 440 (1280x720) [FRENCH SUBBED].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="207626618">198.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T12:38:10+00:00">04 Dec 2015, 12:38:10</span></td>
			<td class="green center">1057</td>
			<td class="red lasttd center">94</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11705555,0" class="icommentjs kaButton smallButton rightButton" href="/one-punch-man-ova-road-to-hero-720p-english-subbed-arrg-lucifer22-t11705555.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-punch-man-ova-road-to-hero-720p-english-subbed-arrg-lucifer22-t11705555.html" class="cellMainLink">One Punch Man OVA : Road to Hero [720p][English Subbed][ARRG][Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="127877626">121.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T17:40:10+00:00">05 Dec 2015, 17:40:10</span></td>
			<td class="green center">786</td>
			<td class="red lasttd center">236</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11706147,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-haikyuu-s2-10-720p-mkv-t11706147.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-haikyuu-s2-10-720p-mkv-t11706147.html" class="cellMainLink">[HorribleSubs] Haikyuu!! S2 - 10 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="342628634">326.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T20:10:03+00:00">05 Dec 2015, 20:10:03</span></td>
			<td class="green center">705</td>
			<td class="red lasttd center">113</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dmg-gakusen-toshi-asterisk-10-720p-big5-mp4-t11709245.html" class="cellMainLink">[DMG][Gakusen Toshi Asterisk][10][720P][BIG5].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="211195874">201.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T10:34:37+00:00">06 Dec 2015, 10:34:37</span></td>
			<td class="green center">491</td>
			<td class="red lasttd center">92</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11697957,0" class="icommentjs kaButton smallButton rightButton" href="/samurai-7-complete-dual-audio-1080p-hevc-x265-t11697957.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/samurai-7-complete-dual-audio-1080p-hevc-x265-t11697957.html" class="cellMainLink">Samurai 7 (Complete) [DUAL-AUDIO] [1080p] [HEVC] [x265]</a></div>
			</td>
			<td class="nobr center" data-sort="5821976751">5.42 <span>GB</span></td>
			<td class="center">34</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T12:22:27+00:00">04 Dec 2015, 12:22:27</span></td>
			<td class="green center">252</td>
			<td class="red lasttd center">536</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11695181,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-gate-complete-720p-khatake2-t11695181.html#comment">7 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/animerg-gate-complete-720p-khatake2-t11695181.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-gate-complete-720p-khatake2-t11695181.html" class="cellMainLink">[AnimeRG] GATE (Complete) [720p] [khatake2]</a></div>
			</td>
			<td class="nobr center" data-sort="2009587377">1.87 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T22:38:22+00:00">03 Dec 2015, 22:38:22</span></td>
			<td class="green center">220</td>
			<td class="red lasttd center">215</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11709889,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-dragon-ball-super-22-english-subbed-720p-sehjada-t11709889.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-dragon-ball-super-22-english-subbed-720p-sehjada-t11709889.html" class="cellMainLink">[ARRG]Dragon Ball Super - 22 English Subbed [720P] (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="158655593">151.31 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T12:55:40+00:00">06 Dec 2015, 12:55:40</span></td>
			<td class="green center">258</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11699147,0" class="icommentjs kaButton smallButton rightButton" href="/shepardtds-queen-s-blade-dual-audio-uncensored-720p-8-bit-x265-hevc-1-12-ovas-complete-t11699147.html#comment">10 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/shepardtds-queen-s-blade-dual-audio-uncensored-720p-8-bit-x265-hevc-1-12-ovas-complete-t11699147.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/shepardtds-queen-s-blade-dual-audio-uncensored-720p-8-bit-x265-hevc-1-12-ovas-complete-t11699147.html" class="cellMainLink">[ShepardTDS] Queen&#039;s Blade Dual Audio Uncensored [720p 8-bit x265 HEVC] 1-12+OVAs Complete</a></div>
			</td>
			<td class="nobr center" data-sort="2510790960">2.34 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T17:36:27+00:00">04 Dec 2015, 17:36:27</span></td>
			<td class="green center">190</td>
			<td class="red lasttd center">193</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11707179,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-one-piece-721-english-subbed-480p-kami-t11707179.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-piece-721-english-subbed-480p-kami-t11707179.html" class="cellMainLink">[AnimeRG] One Piece - 721 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="72694604">69.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T02:51:17+00:00">06 Dec 2015, 02:51:17</span></td>
			<td class="green center">220</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11695139,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-another-complete-720p-bdrip-dual-khatake2-t11695139.html#comment">10 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/animerg-another-complete-720p-bdrip-dual-khatake2-t11695139.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-another-complete-720p-bdrip-dual-khatake2-t11695139.html" class="cellMainLink">[AnimeRG] Another (Complete) [720p] [BDRip] [Dual] [khatake2]</a></div>
			</td>
			<td class="nobr center" data-sort="2560670906">2.38 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T22:27:43+00:00">03 Dec 2015, 22:27:43</span></td>
			<td class="green center">159</td>
			<td class="red lasttd center">153</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-kindaichi-shounen-no-jikenbo-returns-2015-10-raw-ntv-1280x720-x264-aac-mp4-t11702942.html" class="cellMainLink">[Leopard-Raws] Kindaichi Shounen no Jikenbo Returns (2015) - 10 RAW (NTV 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="286464224">273.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T11:45:02+00:00">05 Dec 2015, 11:45:02</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11710066,0" class="icommentjs kaButton smallButton rightButton" href="/dragon-ball-super-episode-022-english-subbed-720p-arizone-t11710066.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-episode-022-english-subbed-720p-arizone-t11710066.html" class="cellMainLink">DRAGON BALL SUPER Episode - 022 [ENGLISH SUBBED] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="159494440">152.11 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T13:49:07+00:00">06 Dec 2015, 13:49:07</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">101</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11692504,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-naruto-shippuuden-440-english-subbed-480p-kami-t11692504.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-naruto-shippuuden-440-english-subbed-480p-kami-t11692504.html" class="cellMainLink">[AnimeRG] Naruto Shippuuden - 440 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="44069489">42.03 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T12:08:17+00:00">03 Dec 2015, 12:08:17</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-miss-monochrome-the-animation-3-10-raw-mx-1280x720-x264-aac-mp4-t11699048.html" class="cellMainLink">[Leopard-Raws] Miss Monochrome The Animation 3 - 10 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="146590927">139.8 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T17:05:03+00:00">04 Dec 2015, 17:05:03</span></td>
			<td class="green center">80</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-kidou-senshi-gundam-tekketsu-no-orphans-10-tbs-1280x720-x264-aac-mp4-t11709238.html" class="cellMainLink">[Ohys-Raws] Kidou Senshi Gundam - Tekketsu no Orphans - 10 (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="392868102">374.67 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T10:33:39+00:00">06 Dec 2015, 10:33:39</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">19</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11691339,0" class="icommentjs kaButton smallButton rightButton" href="/photoshop-20-photo-editing-techniques-every-photoshop-beginner-should-know-t11691339.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/photoshop-20-photo-editing-techniques-every-photoshop-beginner-should-know-t11691339.html" class="cellMainLink">Photoshop 20 Photo Editing Techniques Every Photoshop Beginner Should Know</a></div>
			</td>
			<td class="nobr center" data-sort="15155209">14.45 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T07:02:54+00:00">03 Dec 2015, 07:02:54</span></td>
			<td class="green center">980</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11707113,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-december-6-2015-true-pdf-t11707113.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-december-6-2015-true-pdf-t11707113.html" class="cellMainLink">Assorted Magazines Bundle - December 6 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="950616387">906.58 <span>MB</span></td>
			<td class="center">66</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T02:19:19+00:00">06 Dec 2015, 02:19:19</span></td>
			<td class="green center">502</td>
			<td class="red lasttd center">756</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11689720,0" class="icommentjs kaButton smallButton rightButton" href="/marvel-week-12-02-2015-nem-t11689720.html#comment">45 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-12-02-2015-nem-t11689720.html" class="cellMainLink">Marvel Week+ (12-02-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="800000439">762.94 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T23:29:32+00:00">02 Dec 2015, 23:29:32</span></td>
			<td class="green center">731</td>
			<td class="red lasttd center">270</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11692990,0" class="icommentjs kaButton smallButton rightButton" href="/1001-recipes-you-ve-always-wanted-to-cook-2015-epub-gooner-t11692990.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/1001-recipes-you-ve-always-wanted-to-cook-2015-epub-gooner-t11692990.html" class="cellMainLink">1001 Recipes You&#039;ve Always Wanted to Cook (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="9720587">9.27 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T13:28:34+00:00">03 Dec 2015, 13:28:34</span></td>
			<td class="green center">595</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11694733,0" class="icommentjs kaButton smallButton rightButton" href="/the-complete-beginners-guide-to-mastering-photoshop-and-creating-amazing-professional-looking-photos-in-24-hours-or-less-epub-t11694733.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-complete-beginners-guide-to-mastering-photoshop-and-creating-amazing-professional-looking-photos-in-24-hours-or-less-epub-t11694733.html" class="cellMainLink">The Complete Beginners Guide To Mastering Photoshop And Creating Amazing, Professional Looking Photos In 24 Hours Or Less! [epub]</a></div>
			</td>
			<td class="nobr center" data-sort="138044">134.81 <span>KB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T20:39:39+00:00">03 Dec 2015, 20:39:39</span></td>
			<td class="green center">592</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11689103,0" class="icommentjs kaButton smallButton rightButton" href="/dc-week-12-02-2015-aka-dc-you-week-27-nem-t11689103.html#comment">28 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-12-02-2015-aka-dc-you-week-27-nem-t11689103.html" class="cellMainLink">DC Week+ (12-02-2015) (aka DC YOU Week 27) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="648950798">618.89 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T20:28:37+00:00">02 Dec 2015, 20:28:37</span></td>
			<td class="green center">467</td>
			<td class="red lasttd center">152</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11697239,0" class="icommentjs kaButton smallButton rightButton" href="/learn-c-in-a-day-the-ultimate-crash-course-to-learning-the-basics-of-c-in-no-time-t11697239.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/learn-c-in-a-day-the-ultimate-crash-course-to-learning-the-basics-of-c-in-no-time-t11697239.html" class="cellMainLink">Learn C++ In A DAY The Ultimate Crash Course to Learning the Basics of C++ In No Time</a></div>
			</td>
			<td class="nobr center" data-sort="942075">920 <span>KB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T08:50:35+00:00">04 Dec 2015, 08:50:35</span></td>
			<td class="green center">521</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11697257,0" class="icommentjs kaButton smallButton rightButton" href="/python-programming-for-beginners-a-step-by-step-guide-to-learning-the-basics-of-computer-programming-t11697257.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/python-programming-for-beginners-a-step-by-step-guide-to-learning-the-basics-of-computer-programming-t11697257.html" class="cellMainLink">Python Programming for Beginners A Step-by-Step Guide to Learning the Basics of Computer Programming</a></div>
			</td>
			<td class="nobr center" data-sort="869611">849.23 <span>KB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T08:54:28+00:00">04 Dec 2015, 08:54:28</span></td>
			<td class="green center">475</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11697606,0" class="icommentjs kaButton smallButton rightButton" href="/learn-python-visually-2015-epub-pdf-mobi-ertb-t11697606.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/learn-python-visually-2015-epub-pdf-mobi-ertb-t11697606.html" class="cellMainLink">Learn Python Visually 2015 [ePUB+PDF+MOBI] {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="6754628">6.44 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T10:38:43+00:00">04 Dec 2015, 10:38:43</span></td>
			<td class="green center">419</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690663,0" class="icommentjs kaButton smallButton rightButton" href="/wood-working-magazines-december-3-2015-true-pdf-t11690663.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/wood-working-magazines-december-3-2015-true-pdf-t11690663.html" class="cellMainLink">WOOD Working Magazines - December 3 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="95956348">91.51 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T04:04:23+00:00">03 Dec 2015, 04:04:23</span></td>
			<td class="green center">383</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11699755,0" class="icommentjs kaButton smallButton rightButton" href="/raspberry-pi-magazine-magpi-01-40-pdf-eng-tntvillage-t11699755.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/raspberry-pi-magazine-magpi-01-40-pdf-eng-tntvillage-t11699755.html" class="cellMainLink">Raspberry Pi Magazine - MagPi 01-40, [Pdf - Eng] [TNTVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="677291192">645.92 <span>MB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T20:21:12+00:00">04 Dec 2015, 20:21:12</span></td>
			<td class="green center">318</td>
			<td class="red lasttd center">134</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11691312,0" class="icommentjs kaButton smallButton rightButton" href="/easy-asian-takeout-delicious-and-healthy-asian-recipes-at-home-t11691312.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/easy-asian-takeout-delicious-and-healthy-asian-recipes-at-home-t11691312.html" class="cellMainLink">Easy Asian Takeout Delicious and Healthy Asian Recipes At Home</a></div>
			</td>
			<td class="nobr center" data-sort="19285028">18.39 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T06:54:25+00:00">03 Dec 2015, 06:54:25</span></td>
			<td class="green center">344</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11690897,0" class="icommentjs kaButton smallButton rightButton" href="/home-garden-magazines-december-3-2015-true-pdf-t11690897.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-garden-magazines-december-3-2015-true-pdf-t11690897.html" class="cellMainLink">Home &amp; Garden Magazines - December 3 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="221204853">210.96 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T05:14:21+00:00">03 Dec 2015, 05:14:21</span></td>
			<td class="green center">277</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11691053,0" class="icommentjs kaButton smallButton rightButton" href="/outdoors-magazines-bundle-december-3-2015-true-pdf-t11691053.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/outdoors-magazines-bundle-december-3-2015-true-pdf-t11691053.html" class="cellMainLink">Outdoors Magazines Bundle - December 3 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="140538464">134.03 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T05:46:42+00:00">03 Dec 2015, 05:46:42</span></td>
			<td class="green center">264</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11688596,0" class="icommentjs kaButton smallButton rightButton" href="/the-totally-awesome-hulk-001-2016-webrip-gg-dcp-cbr-t11688596.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-totally-awesome-hulk-001-2016-webrip-gg-dcp-cbr-t11688596.html" class="cellMainLink">The Totally Awesome Hulk 001 (2016) (webrip) (GG-DCP).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="51351583">48.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T18:38:04+00:00">02 Dec 2015, 18:38:04</span></td>
			<td class="green center">194</td>
			<td class="red lasttd center">10</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11702455,0" class="icommentjs kaButton smallButton rightButton" href="/coldplay-a-head-full-of-dreams-2015-t11702455.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/coldplay-a-head-full-of-dreams-2015-t11702455.html" class="cellMainLink">Coldplay - A Head Full of Dreams (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="311649715">297.21 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T09:42:48+00:00">05 Dec 2015, 09:42:48</span></td>
			<td class="green center">502</td>
			<td class="red lasttd center">152</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11695732,0" class="icommentjs kaButton smallButton rightButton" href="/bruce-springsteen-the-ties-that-bind-the-river-collection-2015-flac-t11695732.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bruce-springsteen-the-ties-that-bind-the-river-collection-2015-flac-t11695732.html" class="cellMainLink">Bruce Springsteen - The Ties That Bind [The River Collection] (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="1348925023">1.26 <span>GB</span></td>
			<td class="center">58</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T01:52:04+00:00">04 Dec 2015, 01:52:04</span></td>
			<td class="green center">248</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11688770,0" class="icommentjs kaButton smallButton rightButton" href="/va-piano-christmas-2015-flac-t11688770.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-piano-christmas-2015-flac-t11688770.html" class="cellMainLink">VA - Piano Christmas (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="119507127">113.97 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T19:13:55+00:00">02 Dec 2015, 19:13:55</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11699443,0" class="icommentjs kaButton smallButton rightButton" href="/va-christmas-2015-flac-t11699443.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-christmas-2015-flac-t11699443.html" class="cellMainLink">VA - Christmas (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="552896664">527.28 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T18:42:27+00:00">04 Dec 2015, 18:42:27</span></td>
			<td class="green center">146</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698781,0" class="icommentjs kaButton smallButton rightButton" href="/nirvana-nevermind-2011-24-96-hd-flac-t11698781.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/nirvana-nevermind-2011-24-96-hd-flac-t11698781.html" class="cellMainLink">Nirvana - Nevermind (2011) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1164133406">1.08 <span>GB</span></td>
			<td class="center">33</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T16:09:25+00:00">04 Dec 2015, 16:09:25</span></td>
			<td class="green center">141</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11698207,0" class="icommentjs kaButton smallButton rightButton" href="/john-lennon-the-alternate-double-fantasy-2005-flac-t11698207.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/john-lennon-the-alternate-double-fantasy-2005-flac-t11698207.html" class="cellMainLink">John Lennon - The Alternate Double Fantasy (2005) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="912823910">870.54 <span>MB</span></td>
			<td class="center">63</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T13:31:49+00:00">04 Dec 2015, 13:31:49</span></td>
			<td class="green center">128</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11703348,0" class="icommentjs kaButton smallButton rightButton" href="/the-allman-brothers-band-idlewild-south-deluxe-edition-2015-flac-beolab1700-t11703348.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-allman-brothers-band-idlewild-south-deluxe-edition-2015-flac-beolab1700-t11703348.html" class="cellMainLink">The Allman Brothers Band - Idlewild South [Deluxe Edition] (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="1070580374">1020.99 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T13:48:44+00:00">05 Dec 2015, 13:48:44</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11691809,0" class="icommentjs kaButton smallButton rightButton" href="/rod-stewart-atlantic-crossing-2013-24-192-hd-flac-t11691809.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rod-stewart-atlantic-crossing-2013-24-192-hd-flac-t11691809.html" class="cellMainLink">Rod Stewart - Atlantic Crossing (2013) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1650414890">1.54 <span>GB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-03T09:08:27+00:00">03 Dec 2015, 09:08:27</span></td>
			<td class="green center">121</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11688885,0" class="icommentjs kaButton smallButton rightButton" href="/jethro-tull-too-old-to-rock-n-roll-too-young-to-die-the-tv-special-edition-2015-flac-beolab1700-t11688885.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jethro-tull-too-old-to-rock-n-roll-too-young-to-die-the-tv-special-edition-2015-flac-beolab1700-t11688885.html" class="cellMainLink">Jethro Tull - Too Old to Rock n Roll Too Young to Die [The TV Special Edition] (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="806282385">768.93 <span>MB</span></td>
			<td class="center">41</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T19:42:05+00:00">02 Dec 2015, 19:42:05</span></td>
			<td class="green center">129</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11688823,0" class="icommentjs kaButton smallButton rightButton" href="/bread-guitar-man-2015-24-192-hd-flac-t11688823.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bread-guitar-man-2015-24-192-hd-flac-t11688823.html" class="cellMainLink">Bread - Guitar Man (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1469044796">1.37 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-02T19:27:36+00:00">02 Dec 2015, 19:27:36</span></td>
			<td class="green center">113</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11695783,0" class="icommentjs kaButton smallButton rightButton" href="/rick-ross-black-market-deluxe-edition-rap-flac-2015-t11695783.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rick-ross-black-market-deluxe-edition-rap-flac-2015-t11695783.html" class="cellMainLink">Rick Ross - Black Market [Deluxe Edition] [Rap] [Flac] [2015]</a></div>
			</td>
			<td class="nobr center" data-sort="516919036">492.97 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T02:06:02+00:00">04 Dec 2015, 02:06:02</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11695487,0" class="icommentjs kaButton smallButton rightButton" href="/the-alan-parsons-project-turn-of-a-friendly-card-2cd-deluxe-35th-anniversary-2015-flac-t11695487.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-alan-parsons-project-turn-of-a-friendly-card-2cd-deluxe-35th-anniversary-2015-flac-t11695487.html" class="cellMainLink">The Alan Parsons Project - Turn of a Friendly Card [2CD Deluxe 35th Anniversary] (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="856174823">816.51 <span>MB</span></td>
			<td class="center">37</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T00:24:08+00:00">04 Dec 2015, 00:24:08</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bette-midler-a-gift-of-love-2015-flac-t11699806.html" class="cellMainLink">Bette Midler - A Gift Of Love (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="475773169">453.73 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-04T20:31:18+00:00">04 Dec 2015, 20:31:18</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/joe-dassin-joe-dassin-chante-avec-les-choeurs-de-l-armee-rouge-2015-flac-t11703695.html" class="cellMainLink">Joe Dassin - Joe Dassin chante avec Les Choeurs de l&#039;Armee Rouge (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="508839459">485.27 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-05T14:05:59+00:00">05 Dec 2015, 14:05:59</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11709457,0" class="icommentjs kaButton smallButton rightButton" href="/chet-baker-paul-bley-diane-flac-tntvillage-t11709457.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/chet-baker-paul-bley-diane-flac-tntvillage-t11709457.html" class="cellMainLink">Chet Baker &amp; Paul Bley - Diane [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="165864221">158.18 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-06T11:32:59+00:00">06 Dec 2015, 11:32:59</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">18</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <span  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></span>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <span  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></span>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-tv-show-are-you-watching-right-now-v4/?unread=17182054">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What TV show are you watching right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Bad.Karma/">Bad.Karma</a></span></span> <time class="timeago" datetime="2015-12-06T18:01:57+00:00">06 Dec 2015, 18:01</time></span>
	</li>
		<li>
		<a href="/community/show/congrats-our-newly-elite-uploaders-verified-uploaders/?unread=17182050">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Congrats to Our Newly Elite Uploaders &amp; Verified Uploaders
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/Dr.Soc/">Dr.Soc</a></span></span> <time class="timeago" datetime="2015-12-06T18:01:23+00:00">06 Dec 2015, 18:01</time></span>
	</li>
		<li>
		<a href="/community/show/shout-out-all-app-lovers-uploaders/?unread=17182046">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Shout Out to ALL App Lovers &amp; Uploaders!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_eliteuploader"><a class="plain" href="/user/KaranPC./">KaranPC.</a></span></span> <time class="timeago" datetime="2015-12-06T18:00:47+00:00">06 Dec 2015, 18:00</time></span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v12/?unread=17182045">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V12
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/kaiserklaude/">kaiserklaude</a></span></span> <time class="timeago" datetime="2015-12-06T18:00:45+00:00">06 Dec 2015, 18:00</time></span>
	</li>
		<li>
		<a href="/community/show/karaoke-korner/?unread=17182041">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Karaoke Korner
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_eliteuploader"><a class="plain" href="/user/BJthe1DJ/">BJthe1DJ</a></span></span> <time class="timeago" datetime="2015-12-06T18:00:03+00:00">06 Dec 2015, 18:00</time></span>
	</li>
		<li>
		<a href="/community/show/kickass-anime-community-v-6/?unread=17182039">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Kickass Anime Community V.6!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Esper-kun/">Esper-kun</a></span></span> <time class="timeago" datetime="2015-12-06T17:59:31+00:00">06 Dec 2015, 17:59</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/ADHDerby/post/help-caught-kat-members-paying-their-fine/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Help caught KAT members paying their fine</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/ADHDerby/">ADHDerby</a> <time class="timeago" datetime="2015-12-06T01:58:01+00:00">06 Dec 2015, 01:58</time></span></li>
	<li><a href="/blog/TheDels/post/ode-to-matthew-patrick/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Ode to Matthew Patrick</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-12-05T14:24:40+00:00">05 Dec 2015, 14:24</time></span></li>
	<li><a href="/blog/little%20D/post/a-special-thanks/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> A special thanks</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/little%20D/">little D</a> <time class="timeago" datetime="2015-12-05T06:58:31+00:00">05 Dec 2015, 06:58</time></span></li>
	<li><a href="/blog/JingleBunnies/post/trackers-trackers-i-have-lost-my-trackers-and-now-nothing-is-seeding-is-it-the-end-of-the-world-as-we-know-it/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Trackers trackers â¦ I have lost my trackers and now nothing is seeding - is it the end of the world as we know it?</p></a><span class="explanation">by <a class="plain aclColor_8" href="/user/JingleBunnies/">JingleBunnies</a> <time class="timeago" datetime="2015-12-05T05:58:45+00:00">05 Dec 2015, 05:58</time></span></li>
	<li><a href="/blog/Satfoun/post/my-second-blog-p/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> My second Blog :p</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/Satfoun/">Satfoun</a> <time class="timeago" datetime="2015-12-04T11:49:59+00:00">04 Dec 2015, 11:49</time></span></li>
	<li><a href="/blog/TheDels/post/ode-to-doug-walker/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Ode to Doug Walker</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-12-04T09:54:48+00:00">04 Dec 2015, 09:54</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/tame%20impala/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				tame impala
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/keylogger/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				keylogger
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/elite%20killer/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				elite killer
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/flash%20s01e12/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				flash s01e12
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/2%20skinny/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				2 skinny
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/quantico%20s01e05/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Quantico S01E05
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/about%20this%20butt/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				about this butt
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/video%20converter%20hd/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				video converter hd
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%2Bforest/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the+forest
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/eavy/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				eavy
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/paw%20patrol/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				paw patrol
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.kat.cr');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <span  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></span>
        <span  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></span>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
