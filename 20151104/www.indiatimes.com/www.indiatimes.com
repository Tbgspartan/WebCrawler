<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.47" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.47" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.47"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.47"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.47"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.47"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.47"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-11-04 13:30:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             13 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/bjp-leader-kailash-vijayvargiya-goes-after-srk-calls-him-anti-national-and-accuses-him-of-conspiring-with-pak-246946.html" class=" tint" title="BJP Leader Kailash Vijayvargiya Goes After SRK, Calls Him Anti-National And Accuses Him Of Conspiring With Pak!">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/srk502_1446577819_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/srk502_1446577819_236x111.jpg"  border="0" alt="BJP Leader Kailash Vijayvargiya Goes After SRK, Calls Him Anti-National And Accuses Him Of Conspiring With Pak!"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/bjp-leader-kailash-vijayvargiya-goes-after-srk-calls-him-anti-national-and-accuses-him-of-conspiring-with-pak-246946.html" title="BJP Leader Kailash Vijayvargiya Goes After SRK, Calls Him Anti-National And Accuses Him Of Conspiring With Pak!">
                            BJP Leader Kailash Vijayvargiya Goes After SRK, Calls Him Anti-National And Accuses Him Of Conspiring With Pak!                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            35 mins ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/satellite-images-show-how-smoke-from-punjab-s-burning-fields-are-choking-north-india-246964.html" title="Satellite Images Show How Smoke From Punjab's Burning Fields Are Choking North  India" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/sat5_1446623146_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/sat5_1446623146_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/satellite-images-show-how-smoke-from-punjab-s-burning-fields-are-choking-north-india-246964.html" title="Satellite Images Show How Smoke From Punjab's Burning Fields Are Choking North  India">
                            Satellite Images Show How Smoke From Punjab's Burning Fields Are Choking North  India                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            1 hour ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/neil-nitin-mukesh-has-bagged-a-role-in-game-of-thrones-it-s-as-bizarre-as-it-sounds-246950.html" title="Neil Nitin Mukesh Has Bagged A Role In Game of Thrones & It's As Bizarre As It Sounds!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/neil502_1446619292_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/neil502_1446619292_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/neil-nitin-mukesh-has-bagged-a-role-in-game-of-thrones-it-s-as-bizarre-as-it-sounds-246950.html" title="Neil Nitin Mukesh Has Bagged A Role In Game of Thrones & It's As Bizarre As It Sounds!">
                            Neil Nitin Mukesh Has Bagged A Role In Game of Thrones & It's As Bizarre As It Sounds!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            1 hour ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/9-iconic-photographs-of-children-suffering-and-what-they-really-should-have-been-in-an-ideal-world-246900.html" title="9 Iconic Photographs Of Children Suffering And What They Really Should Have Been In An Ideal World" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/cp_1446467214_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/cp_1446467214_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/9-iconic-photographs-of-children-suffering-and-what-they-really-should-have-been-in-an-ideal-world-246900.html" title="9 Iconic Photographs Of Children Suffering And What They Really Should Have Been In An Ideal World">
                            9 Iconic Photographs Of Children Suffering And What They Really Should Have Been In An Ideal World                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            1 hour ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/yuvraj-singh-reportedly-set-to-marry-actress-model-hazel-keech-roka-ceremony-planned-soon-246961.html" title="Yuvraj Singh Reportedly Set To Marry Actress Model Hazel Keech. Roka Ceremony Planned Soon" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/yuvrajhazel_1446620038_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/yuvrajhazel_1446620038_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/yuvraj-singh-reportedly-set-to-marry-actress-model-hazel-keech-roka-ceremony-planned-soon-246961.html" title="Yuvraj Singh Reportedly Set To Marry Actress Model Hazel Keech. Roka Ceremony Planned Soon">
                            Yuvraj Singh Reportedly Set To Marry Actress Model Hazel Keech. Roka Ceremony Planned Soon                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/folks-on-tinder-judge-people-just-by-their-looks-so-this-brand-decided-to-do-something-about-it-246891.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/folks-on-tinder-judge-people-just-by-their-looks-so-this-brand-decided-to-do-something-about-it-246891.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Nov/card_1446463264_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card_1446463264_502x234.jpg"  border="0" alt="Folks on Tinder Judge People Just By Their Looks, So This Brand Decided To Do Something About It!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/folks-on-tinder-judge-people-just-by-their-looks-so-this-brand-decided-to-do-something-about-it-246891.html" title="Folks on Tinder Judge People Just By Their Looks, So This Brand Decided To Do Something About It!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/folks-on-tinder-judge-people-just-by-their-looks-so-this-brand-decided-to-do-something-about-it-246891.html');">Folks on Tinder Judge People Just By Their Looks, So This Brand Decided To Do Something About It!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/9-things-you-will-relate-to-if-you-hate-winters-245550.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/9-things-you-will-relate-to-if-you-hate-winters-245550.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Sep/card_1443099189_1443099205_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card_1443099189_1443099205_502x234.jpg"  border="0" alt="9 Things You Will Relate To If You Hate Winters" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/9-things-you-will-relate-to-if-you-hate-winters-245550.html" title="9 Things You Will Relate To If You Hate Winters" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/9-things-you-will-relate-to-if-you-hate-winters-245550.html');">9 Things You Will Relate To If You Hate Winters</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-trailer-of-kung-fu-panda-3-is-out-and-it-ll-take-you-to-a-panda-paradise-246960.html'>video</a>                           
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/the-trailer-of-kung-fu-panda-3-is-out-and-it-ll-take-you-to-a-panda-paradise-246960.html" class="tint" title="The Trailer Of 'Kung Fu Panda 3' Is Out And It'll Take You To A Panda Paradise!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/videocafe/the-trailer-of-kung-fu-panda-3-is-out-and-it-ll-take-you-to-a-panda-paradise-246960.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/kungfupanda3_card_1446620266_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/kungfupanda3_card_1446620266_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/the-trailer-of-kung-fu-panda-3-is-out-and-it-ll-take-you-to-a-panda-paradise-246960.html" title="The Trailer Of 'Kung Fu Panda 3' Is Out And It'll Take You To A Panda Paradise!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/videocafe/the-trailer-of-kung-fu-panda-3-is-out-and-it-ll-take-you-to-a-panda-paradise-246960.html');">
                            The Trailer Of 'Kung Fu Panda 3' Is Out And It'll Take You To A Panda Paradise!                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-why-indian-english-is-completely-different-from-any-other-country-in-the-world-246919.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/this-is-why-indian-english-is-completely-different-from-any-other-country-in-the-world-246919.html" class="tint" title="This Is Why Indian-English Is Completely Different From Any Other Country In The World!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/indianeng_card1_1446538015_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/indianeng_card1_1446538015_218x102.jpg" border="0" alt="This Is Why Indian-English Is Completely Different From Any Other Country In The World!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-is-why-indian-english-is-completely-different-from-any-other-country-in-the-world-246919.html" title="This Is Why Indian-English Is Completely Different From Any Other Country In The World!">
                            This Is Why Indian-English Is Completely Different From Any Other Country In The World!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/12-reasons-why-road-trips-are-the-best-way-to-travel-246881.html" class="tint" title="12 Reasons Why Road Trips Are The Best Way To Travel">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/card_1446449596_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card_1446449596_218x102.jpg" border="0" alt="12 Reasons Why Road Trips Are The Best Way To Travel"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/12-reasons-why-road-trips-are-the-best-way-to-travel-246881.html" title="12 Reasons Why Road Trips Are The Best Way To Travel">
                            12 Reasons Why Road Trips Are The Best Way To Travel                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/healthyliving/15-everyday-indian-veggies-that-you-had-no-idea-were-so-healthy-246782.html" class="tint" title="15 Everyday Indian Veggies That You Had No Idea Were So Healthy!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/card_1446448331_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card_1446448331_218x102.jpg" border="0" alt="15 Everyday Indian Veggies That You Had No Idea Were So Healthy!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/15-everyday-indian-veggies-that-you-had-no-idea-were-so-healthy-246782.html" title="15 Everyday Indian Veggies That You Had No Idea Were So Healthy!">
                            15 Everyday Indian Veggies That You Had No Idea Were So Healthy!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/facebook-s-video-on-king-khan-is-so-adorable-that-it-ll-leave-a-smile-on-face-of-every-srk-fan-246941.html" class="tint" title="Facebook's Video On King Khan Is So Adorable That It'll Leave A Smile On Face Of Every SRK Fan!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/srk5_1446555423_1446555430_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/srk5_1446555423_1446555430_218x102.jpg" border="0" alt="Facebook's Video On King Khan Is So Adorable That It'll Leave A Smile On Face Of Every SRK Fan!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/facebook-s-video-on-king-khan-is-so-adorable-that-it-ll-leave-a-smile-on-face-of-every-srk-fan-246941.html" title="Facebook's Video On King Khan Is So Adorable That It'll Leave A Smile On Face Of Every SRK Fan!">
                            Facebook's Video On King Khan Is So Adorable That It'll Leave A Smile On Face Of Every SRK Fan!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/this-page-from-saeed-jaffrey-s-personal-diary-on-falling-for-his-ex-wife-will-melt-your-heart-246911.html" class="tint" title="This Page From Saeed Jaffrey's Personal Diary On Falling For His Ex-Wife Will Melt Your Heart!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/scard_1446533975_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/scard_1446533975_218x102.jpg" border="0" alt="This Page From Saeed Jaffrey's Personal Diary On Falling For His Ex-Wife Will Melt Your Heart!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/this-page-from-saeed-jaffrey-s-personal-diary-on-falling-for-his-ex-wife-will-melt-your-heart-246911.html" title="This Page From Saeed Jaffrey's Personal Diary On Falling For His Ex-Wife Will Melt Your Heart!">
                            This Page From Saeed Jaffrey's Personal Diary On Falling For His Ex-Wife Will Melt Your Heart!                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            1 hour ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/six-hours-after-making-helmets-compulsory-andhra-pradesh-runs-out-of-helmets-246952.html" title="Six Hours After Making Helmets Compulsory, Andhra Pradesh Runs Out Of Helmets" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/bccl16_1446616927_1446616931_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/six-hours-after-making-helmets-compulsory-andhra-pradesh-runs-out-of-helmets-246952.html" title="Six Hours After Making Helmets Compulsory, Andhra Pradesh Runs Out Of Helmets">
                            Six Hours After Making Helmets Compulsory, Andhra Pradesh Runs Out Of Helmets                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            1 hour ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/jama-at-ud-da-wah-chief-hafiz-saeed-sends-an-open-invitation-to-srk-to-make-pakistan-his-abode-246951.html" title="Jama'at-ud-Da'wah Chief Hafiz Saeed Sends An Open Invitation To SRK To Make Pakistan His Abode!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446617047_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/jama-at-ud-da-wah-chief-hafiz-saeed-sends-an-open-invitation-to-srk-to-make-pakistan-his-abode-246951.html" title="Jama'at-ud-Da'wah Chief Hafiz Saeed Sends An Open Invitation To SRK To Make Pakistan His Abode!">
                            Jama'at-ud-Da'wah Chief Hafiz Saeed Sends An Open Invitation To SRK To Make Pakistan His Abode!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            2 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/mizoram-and-meghalaya-top-india-in-giving-women-equal-opportunity-for-work-246948.html" title="Mizoram And Meghalaya Top India In Giving Women Equal Opportunity To Work" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/navut6_1446615838_1446615841_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/mizoram-and-meghalaya-top-india-in-giving-women-equal-opportunity-for-work-246948.html" title="Mizoram And Meghalaya Top India In Giving Women Equal Opportunity To Work">
                            Mizoram And Meghalaya Top India In Giving Women Equal Opportunity To Work                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            2 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/after-zuckerberg-it-s-microsoft-chief-satya-nadella-who-is-visiting-india-next-246947.html" title="Days After Mark Zuckerberg, Microsoft Chief Satya Nadella Is Also Visiting India #IndiaIsTheFuture" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/satya502_1446614935_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/after-zuckerberg-it-s-microsoft-chief-satya-nadella-who-is-visiting-india-next-246947.html" title="Days After Mark Zuckerberg, Microsoft Chief Satya Nadella Is Also Visiting India #IndiaIsTheFuture">
                            Days After Mark Zuckerberg, Microsoft Chief Satya Nadella Is Also Visiting India #IndiaIsTheFuture                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            15 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/goodbye-stars-twitter-is-replacing-its-favourite-button-with-hearts-so-it-becomes-simpler-to-use-246944.html" title="Goodbye Stars! Twitter Is Replacing Its 'Favourite' Button With Hearts So It Becomes Simpler To Use!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/twitter502_1446568919_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/goodbye-stars-twitter-is-replacing-its-favourite-button-with-hearts-so-it-becomes-simpler-to-use-246944.html" title="Goodbye Stars! Twitter Is Replacing Its 'Favourite' Button With Hearts So It Becomes Simpler To Use!">
                            Goodbye Stars! Twitter Is Replacing Its 'Favourite' Button With Hearts So It Becomes Simpler To Use!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/34-bollywood-celebrities-you-didn-t-know-were-using-a-stage-name-246937.html" class="tint" title="34 Bollywood Celebrities You Didn't Know Were Using A Stage Name">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446554712_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/34-bollywood-celebrities-you-didn-t-know-were-using-a-stage-name-246937.html" title="34 Bollywood Celebrities You Didn't Know Were Using A Stage Name">
                            34 Bollywood Celebrities You Didn't Know Were Using A Stage Name                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/tips-tricks/here-are-6-easy-things-you-can-do-to-protect-your-joints-246883.html" class="tint" title="Here Are 6 Easy Things You Can Do To Protect Your Joints!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1446451406_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/here-are-6-easy-things-you-can-do-to-protect-your-joints-246883.html" title="Here Are 6 Easy Things You Can Do To Protect Your Joints!">
                            Here Are 6 Easy Things You Can Do To Protect Your Joints!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/10-weird-song-lyrics-that-are-so-bad-that-they-are-actually-cool-246929.html" class="tint" title="10 Weird Song Lyrics That Are So Bad That They Are Actually Cool!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card1_1446548435_1446548468_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/10-weird-song-lyrics-that-are-so-bad-that-they-are-actually-cool-246929.html" title="10 Weird Song Lyrics That Are So Bad That They Are Actually Cool!">
                            10 Weird Song Lyrics That Are So Bad That They Are Actually Cool!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/technology/6-must-know-facts-about-google-s-project-loon-which-aims-to-provide-free-internet-to-rural-india-246916.html" class="tint" title="6 Must Know Facts About Google's 'Project Loon' Which Aims To Provide Free Internet To Rural India">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446535793_218x102.jpg" border="0" alt="6 Must Know Facts About Google's 'Project Loon' Which Aims To Provide Free Internet To Rural India"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/6-must-know-facts-about-google-s-project-loon-which-aims-to-provide-free-internet-to-rural-india-246916.html" title="6 Must Know Facts About Google's 'Project Loon' Which Aims To Provide Free Internet To Rural India">
                            6 Must Know Facts About Google's 'Project Loon' Which Aims To Provide Free Internet To Rural India                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-things-you-will-relate-to-if-you-hate-winters-245550.html" class="tint" title="9 Things You Will Relate To If You Hate Winters">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443099189_1443099205_218x102.jpg" border="0" alt="9 Things You Will Relate To If You Hate Winters"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-things-you-will-relate-to-if-you-hate-winters-245550.html" title="9 Things You Will Relate To If You Hate Winters">
                            9 Things You Will Relate To If You Hate Winters                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/bollywood/leaked-pictures-from-the-sets-of-mohenjo-daro-9-reasons-why-it-ll-be-next-year-s-biggest-release-246914.html" class="tint" title="Leaked Pictures From The Sets Of 'Mohenjo Daro' + 9 Reasons Why It'll Be Next Year's Biggest Release">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446536851_218x102.jpg" border="0" alt="Leaked Pictures From The Sets Of 'Mohenjo Daro' + 9 Reasons Why It'll Be Next Year's Biggest Release"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/leaked-pictures-from-the-sets-of-mohenjo-daro-9-reasons-why-it-ll-be-next-year-s-biggest-release-246914.html" title="Leaked Pictures From The Sets Of 'Mohenjo Daro' + 9 Reasons Why It'll Be Next Year's Biggest Release">
                            Leaked Pictures From The Sets Of 'Mohenjo Daro' + 9 Reasons Why It'll Be Next Year's Biggest Release                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/mahira-khan-a-friend-send-a-message-to-shiv-sainiks-protesting-against-her-but-did-she-go-too-far-246932.html" class="tint" title="Mahira Khan & A Friend Send A Message To Shiv Sainiks Protesting Against Her. But Did She Go Too Far?">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/mahira-khan-halloween_0_0_1446549885_1446549895_218x102.jpg" border="0" alt="Mahira Khan & A Friend Send A Message To Shiv Sainiks Protesting Against Her. But Did She Go Too Far?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/mahira-khan-a-friend-send-a-message-to-shiv-sainiks-protesting-against-her-but-did-she-go-too-far-246932.html" title="Mahira Khan & A Friend Send A Message To Shiv Sainiks Protesting Against Her. But Did She Go Too Far?">
                            Mahira Khan & A Friend Send A Message To Shiv Sainiks Protesting Against Her. But Did She Go Too Far?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/22-struggles-every-smartphone-user-knows-all-too-well-246862.html" class="tint" title="22 Struggles Every Smartphone User Knows All Too Well!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-7_1446549793_218x102.jpg" border="0" alt="22 Struggles Every Smartphone User Knows All Too Well!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/22-struggles-every-smartphone-user-knows-all-too-well-246862.html" title="22 Struggles Every Smartphone User Knows All Too Well!">
                            22 Struggles Every Smartphone User Knows All Too Well!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            15 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/kamal-haasan-says-intolerance-will-divide-india-but-he-won-t-return-his-national-awards-246945.html" title="Kamal Haasan Says 'Intolerance' Will Divide India, But He Won't Return His National Awards!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kamal502_1446567366_236x111.jpg" border="0" alt="Kamal Haasan Says 'Intolerance' Will Divide India, But He Won't Return His National Awards!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/kamal-haasan-says-intolerance-will-divide-india-but-he-won-t-return-his-national-awards-246945.html" title="Kamal Haasan Says 'Intolerance' Will Divide India, But He Won't Return His National Awards!">
                            Kamal Haasan Says 'Intolerance' Will Divide India, But He Won't Return His National Awards!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/kerala-college-suspends-a-student-for-questioning-why-girls-and-boys-have-to-sit-separately-wtf-246943.html" title="Kerala College Suspends A Student For Questioning Why Girls And Boys Have To Sit Separately! #WTF" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/farookcollege_1446561536_236x111.jpg" border="0" alt="Kerala College Suspends A Student For Questioning Why Girls And Boys Have To Sit Separately! #WTF"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/kerala-college-suspends-a-student-for-questioning-why-girls-and-boys-have-to-sit-separately-wtf-246943.html" title="Kerala College Suspends A Student For Questioning Why Girls And Boys Have To Sit Separately! #WTF">
                            Kerala College Suspends A Student For Questioning Why Girls And Boys Have To Sit Separately! #WTF                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/jain-monk-shri-hansratna-vijayji-maharaj-saheb-creates-history-by-completing-423-days-of-fasting-penance-246942.html" title="Jain Monk Shri Hansratna Vijayji Maharaj Saheb Creates History By Completing 423 Days Of Fasting Penance" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/jain-monk-502_1446559754_236x111.jpg" border="0" alt="Jain Monk Shri Hansratna Vijayji Maharaj Saheb Creates History By Completing 423 Days Of Fasting Penance"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/jain-monk-shri-hansratna-vijayji-maharaj-saheb-creates-history-by-completing-423-days-of-fasting-penance-246942.html" title="Jain Monk Shri Hansratna Vijayji Maharaj Saheb Creates History By Completing 423 Days Of Fasting Penance">
                            Jain Monk Shri Hansratna Vijayji Maharaj Saheb Creates History By Completing 423 Days Of Fasting Penance                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/the-ice-shelf-in-antarctica-is-expanding-not-contracting-we-tell-you-why-it-s-not-a-good-news-246925.html" title="NASA Says The Antarctica Ice Shelf Is Expanding, Not Contracting. We Tell You Why It's Not A Good News" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/antarctica-502_1446558370_236x111.jpg" border="0" alt="NASA Says The Antarctica Ice Shelf Is Expanding, Not Contracting. We Tell You Why It's Not A Good News"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/the-ice-shelf-in-antarctica-is-expanding-not-contracting-we-tell-you-why-it-s-not-a-good-news-246925.html" title="NASA Says The Antarctica Ice Shelf Is Expanding, Not Contracting. We Tell You Why It's Not A Good News">
                            NASA Says The Antarctica Ice Shelf Is Expanding, Not Contracting. We Tell You Why It's Not A Good News                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            19 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/20-aerial-shots-of-land-art-will-leave-stunned-at-how-imaginative-the-human-mind-is-246894.html" title="20 Aerial Shots Of Land Art Will Leave Stunned At How Imaginative The Human Mind Is" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cp_1446461705_1446461711_236x111.jpg" border="0" alt="20 Aerial Shots Of Land Art Will Leave Stunned At How Imaginative The Human Mind Is"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/20-aerial-shots-of-land-art-will-leave-stunned-at-how-imaginative-the-human-mind-is-246894.html" title="20 Aerial Shots Of Land Art Will Leave Stunned At How Imaginative The Human Mind Is">
                            20 Aerial Shots Of Land Art Will Leave Stunned At How Imaginative The Human Mind Is                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/12-reasons-why-road-trips-are-the-best-way-to-travel-246881.html" class="tint" title="12 Reasons Why Road Trips Are The Best Way To Travel">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446449596_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/self/12-reasons-why-road-trips-are-the-best-way-to-travel-246881.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/12-reasons-why-road-trips-are-the-best-way-to-travel-246881.html" title="12 Reasons Why Road Trips Are The Best Way To Travel">
                            12 Reasons Why Road Trips Are The Best Way To Travel                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/facebook-s-video-on-king-khan-is-so-adorable-that-it-ll-leave-a-smile-on-face-of-every-srk-fan-246941.html" class="tint" title="Facebook's Video On King Khan Is So Adorable That It'll Leave A Smile On Face Of Every SRK Fan!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/srk5_1446555423_1446555430_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/facebook-s-video-on-king-khan-is-so-adorable-that-it-ll-leave-a-smile-on-face-of-every-srk-fan-246941.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/facebook-s-video-on-king-khan-is-so-adorable-that-it-ll-leave-a-smile-on-face-of-every-srk-fan-246941.html" title="Facebook's Video On King Khan Is So Adorable That It'll Leave A Smile On Face Of Every SRK Fan!">
                            Facebook's Video On King Khan Is So Adorable That It'll Leave A Smile On Face Of Every SRK Fan!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/more-trouble-for-bajirao-mastani-after-peshwa-family-raises-objection-to-bajirao-s-depiction-246935.html" class="tint" title="More Trouble For 'Bajirao Mastani' After Peshwa Family Raises Objection To Bajirao's Depiction">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446551972_1446551979_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bollywood/more-trouble-for-bajirao-mastani-after-peshwa-family-raises-objection-to-bajirao-s-depiction-246935.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/more-trouble-for-bajirao-mastani-after-peshwa-family-raises-objection-to-bajirao-s-depiction-246935.html" title="More Trouble For 'Bajirao Mastani' After Peshwa Family Raises Objection To Bajirao's Depiction">
                            More Trouble For 'Bajirao Mastani' After Peshwa Family Raises Objection To Bajirao's Depiction                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-jat-boys-singing-a-haryanvi-rock-fusion-song-is-so-quirky-that-it-ll-make-you-head-bang-246910.html'>video</a>		

                        <a href="http://www.indiatimes.com/videocafe/these-jat-boys-singing-a-haryanvi-rock-fusion-song-is-so-quirky-that-it-ll-make-you-head-bang-246910.html" class="tint" title="These Jat Boys Singing A Haryanvi Rock Fusion Song Is So Quirky That It'll Make You Head-Bang">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/rockini_card_1446531204_218x102.jpg" border="0" alt="These Jat Boys Singing A Haryanvi Rock Fusion Song Is So Quirky That It'll Make You Head-Bang"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/these-jat-boys-singing-a-haryanvi-rock-fusion-song-is-so-quirky-that-it-ll-make-you-head-bang-246910.html" title="These Jat Boys Singing A Haryanvi Rock Fusion Song Is So Quirky That It'll Make You Head-Bang">
                            These Jat Boys Singing A Haryanvi Rock Fusion Song Is So Quirky That It'll Make You Head-Bang                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/karan-arjun-s-sultan-hug-other-highlights-of-srk-s-50th-birthday-which-you-might-have-missed-246907.html" class="tint" title="Karan-Arjun's 'Sultan' Hug + Other Highlights Of SRK's 50th Birthday Which You Might Have Missed!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/srk_1446527623_1446527632_218x102.jpg" border="0" alt="Karan-Arjun's 'Sultan' Hug + Other Highlights Of SRK's 50th Birthday Which You Might Have Missed!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/karan-arjun-s-sultan-hug-other-highlights-of-srk-s-50th-birthday-which-you-might-have-missed-246907.html" title="Karan-Arjun's 'Sultan' Hug + Other Highlights Of SRK's 50th Birthday Which You Might Have Missed!">
                            Karan-Arjun's 'Sultan' Hug + Other Highlights Of SRK's 50th Birthday Which You Might Have Missed!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/more-trouble-for-bajirao-mastani-after-peshwa-family-raises-objection-to-bajirao-s-depiction-246935.html" class="tint" title="More Trouble For 'Bajirao Mastani' After Peshwa Family Raises Objection To Bajirao's Depiction">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446551972_1446551979_218x102.jpg" border="0" alt="More Trouble For 'Bajirao Mastani' After Peshwa Family Raises Objection To Bajirao's Depiction"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/more-trouble-for-bajirao-mastani-after-peshwa-family-raises-objection-to-bajirao-s-depiction-246935.html" title="More Trouble For 'Bajirao Mastani' After Peshwa Family Raises Objection To Bajirao's Depiction">
                            More Trouble For 'Bajirao Mastani' After Peshwa Family Raises Objection To Bajirao's Depiction                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/7-bollywood-celebrities-reveal-their-favourite-travel-destinations-246917.html" class="tint" title="7 Bollywood Celebrities Reveal Their Favourite Travel Destinations">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446536236_1446536241_218x102.jpg" border="0" alt="7 Bollywood Celebrities Reveal Their Favourite Travel Destinations"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/7-bollywood-celebrities-reveal-their-favourite-travel-destinations-246917.html" title="7 Bollywood Celebrities Reveal Their Favourite Travel Destinations">
                            7 Bollywood Celebrities Reveal Their Favourite Travel Destinations                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/12-things-shahrukh-khan-said-at-his-50th-birthday-bash-on-how-to-live-life-king-khan-size-246903.html" class="tint" title="12 Things Shahrukh Khan Said At His 50th Birthday Bash On How To Live Life 'King-Khan-Size'">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cszx0rwueaadcbp_1446473366_1446473373_218x102.jpg" border="0" alt="12 Things Shahrukh Khan Said At His 50th Birthday Bash On How To Live Life 'King-Khan-Size'"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/12-things-shahrukh-khan-said-at-his-50th-birthday-bash-on-how-to-live-life-king-khan-size-246903.html" title="12 Things Shahrukh Khan Said At His 50th Birthday Bash On How To Live Life 'King-Khan-Size'">
                            12 Things Shahrukh Khan Said At His 50th Birthday Bash On How To Live Life 'King-Khan-Size'                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            20 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/wikileaks-tweets-about-wikileaks4india-calls-it-fake-accuses-it-of-creating-communal-tensions-246940.html" title="Wikileaks Tweets About @Wikileaks4India. Calls It Fake, Accuses It Of Creating Communal Tensions" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card1_1446554149_236x111.jpg" border="0" alt="Wikileaks Tweets About @Wikileaks4India. Calls It Fake, Accuses It Of Creating Communal Tensions"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/wikileaks-tweets-about-wikileaks4india-calls-it-fake-accuses-it-of-creating-communal-tensions-246940.html" title="Wikileaks Tweets About @Wikileaks4India. Calls It Fake, Accuses It Of Creating Communal Tensions">
                            Wikileaks Tweets About @Wikileaks4India. Calls It Fake, Accuses It Of Creating Communal Tensions                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            20 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/in-a-bid-to-recover-from-vast-losses-in-past-standard-chartered-will-cut-15-000-jobs-246934.html" title="In A Bid To Recover From Vast Losses In Past, Standard Chartered Will Cut 15,000 Jobs" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/std5_1446551710_236x111.jpg" border="0" alt="In A Bid To Recover From Vast Losses In Past, Standard Chartered Will Cut 15,000 Jobs"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/in-a-bid-to-recover-from-vast-losses-in-past-standard-chartered-will-cut-15-000-jobs-246934.html" title="In A Bid To Recover From Vast Losses In Past, Standard Chartered Will Cut 15,000 Jobs">
                            In A Bid To Recover From Vast Losses In Past, Standard Chartered Will Cut 15,000 Jobs                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            21 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/sadhvi-prachi-strikes-again-calls-shah-rukh-khan-a-pakistani-agent-246928.html" title="Sadhvi Prachi Strikes Again, Calls Shah Rukh Khan A 'Pakistani Agent'" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/srkcard_1446548448_236x111.jpg" border="0" alt="Sadhvi Prachi Strikes Again, Calls Shah Rukh Khan A 'Pakistani Agent'"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/sadhvi-prachi-strikes-again-calls-shah-rukh-khan-a-pakistani-agent-246928.html" title="Sadhvi Prachi Strikes Again, Calls Shah Rukh Khan A 'Pakistani Agent'">
                            Sadhvi Prachi Strikes Again, Calls Shah Rukh Khan A 'Pakistani Agent'                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            21 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/canada-shows-some-love-back-punjabi-becomes-the-third-official-language-of-country-s-parliament-246930.html" title="Canada Shows Some Love Back. Punjabi Becomes The Third Official Language Of Country's Parliament" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/12205101_10153738801644851_695442678_n_1446550205_1446550212_236x111.jpg" border="0" alt="Canada Shows Some Love Back. Punjabi Becomes The Third Official Language Of Country's Parliament"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/canada-shows-some-love-back-punjabi-becomes-the-third-official-language-of-country-s-parliament-246930.html" title="Canada Shows Some Love Back. Punjabi Becomes The Third Official Language Of Country's Parliament">
                            Canada Shows Some Love Back. Punjabi Becomes The Third Official Language Of Country's Parliament                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            21 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/china-ka-maal-over-40-chinese-products-sold-online-last-year-were-fakes-246921.html" title="China Ka Maal! Over 40% Chinese Products Sold Online Last Year Were Fakes!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/china502_1446544136_236x111.jpg" border="0" alt="China Ka Maal! Over 40% Chinese Products Sold Online Last Year Were Fakes!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/china-ka-maal-over-40-chinese-products-sold-online-last-year-were-fakes-246921.html" title="China Ka Maal! Over 40% Chinese Products Sold Online Last Year Were Fakes!">
                            China Ka Maal! Over 40% Chinese Products Sold Online Last Year Were Fakes!                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/wow-rajinikanth-amitabh-bachchan-might-share-screen-space-in-robot-2_-246936.html" class="tint" title="Wow! Rajinikanth & Amitabh Bachchan Might Share Screen Space In Robot 2!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/rajinikanth-bigb_1446551951_1446551956_502x234.jpg" border="0" alt="Wow! Rajinikanth & Amitabh Bachchan Might Share Screen Space In Robot 2!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/wow-rajinikanth-amitabh-bachchan-might-share-screen-space-in-robot-2_-246936.html" title="Wow! Rajinikanth & Amitabh Bachchan Might Share Screen Space In Robot 2!">
                        Wow! Rajinikanth & Amitabh Bachchan Might Share Screen Space In Robot 2!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/mahira-khan-a-friend-send-a-message-to-shiv-sainiks-protesting-against-her-but-did-she-go-too-far-246932.html" class="tint" title="Mahira Khan & A Friend Send A Message To Shiv Sainiks Protesting Against Her. But Did She Go Too Far?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/mahira-khan-halloween_0_0_1446549885_1446549895_502x234.jpg" border="0" alt="Mahira Khan & A Friend Send A Message To Shiv Sainiks Protesting Against Her. But Did She Go Too Far?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/mahira-khan-a-friend-send-a-message-to-shiv-sainiks-protesting-against-her-but-did-she-go-too-far-246932.html" title="Mahira Khan & A Friend Send A Message To Shiv Sainiks Protesting Against Her. But Did She Go Too Far?">
                        Mahira Khan & A Friend Send A Message To Shiv Sainiks Protesting Against Her. But Did She Go Too Far?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/these-20-breathtaking-underwater-portraits-will-make-you-want-to-take-a-dive-right-now-246906.html" class="tint" title="These 20 Breathtaking Underwater Portraits Will Make You Want To Take A Dive Right Now!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage-card-image_1446479743_1446479812_1446479830_502x234.jpg" border="0" alt="These 20 Breathtaking Underwater Portraits Will Make You Want To Take A Dive Right Now!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/these-20-breathtaking-underwater-portraits-will-make-you-want-to-take-a-dive-right-now-246906.html" title="These 20 Breathtaking Underwater Portraits Will Make You Want To Take A Dive Right Now!">
                        These 20 Breathtaking Underwater Portraits Will Make You Want To Take A Dive Right Now!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/leaked-pictures-from-the-sets-of-mohenjo-daro-9-reasons-why-it-ll-be-next-year-s-biggest-release-246914.html" class="tint" title="Leaked Pictures From The Sets Of 'Mohenjo Daro' + 9 Reasons Why It'll Be Next Year's Biggest Release">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446536851_502x234.jpg" border="0" alt="Leaked Pictures From The Sets Of 'Mohenjo Daro' + 9 Reasons Why It'll Be Next Year's Biggest Release" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/leaked-pictures-from-the-sets-of-mohenjo-daro-9-reasons-why-it-ll-be-next-year-s-biggest-release-246914.html" title="Leaked Pictures From The Sets Of 'Mohenjo Daro' + 9 Reasons Why It'll Be Next Year's Biggest Release">
                        Leaked Pictures From The Sets Of 'Mohenjo Daro' + 9 Reasons Why It'll Be Next Year's Biggest Release                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/15-everyday-indian-veggies-that-you-had-no-idea-were-so-healthy-246782.html" class="tint" title="15 Everyday Indian Veggies That You Had No Idea Were So Healthy!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446448331_502x234.jpg" border="0" alt="15 Everyday Indian Veggies That You Had No Idea Were So Healthy!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/15-everyday-indian-veggies-that-you-had-no-idea-were-so-healthy-246782.html" title="15 Everyday Indian Veggies That You Had No Idea Were So Healthy!">
                        15 Everyday Indian Veggies That You Had No Idea Were So Healthy!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/7-bollywood-celebrities-reveal-their-favourite-travel-destinations-246917.html" class="tint" title="7 Bollywood Celebrities Reveal Their Favourite Travel Destinations">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446536236_1446536241_502x234.jpg" border="0" alt="7 Bollywood Celebrities Reveal Their Favourite Travel Destinations" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/7-bollywood-celebrities-reveal-their-favourite-travel-destinations-246917.html" title="7 Bollywood Celebrities Reveal Their Favourite Travel Destinations">
                        7 Bollywood Celebrities Reveal Their Favourite Travel Destinations                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-why-indian-english-is-completely-different-from-any-other-country-in-the-world-246919.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-is-why-indian-english-is-completely-different-from-any-other-country-in-the-world-246919.html" class="tint" title="This Is Why Indian-English Is Completely Different From Any Other Country In The World!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/indianeng_card1_1446538015_502x234.jpg" border="0" alt="This Is Why Indian-English Is Completely Different From Any Other Country In The World!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-is-why-indian-english-is-completely-different-from-any-other-country-in-the-world-246919.html" title="This Is Why Indian-English Is Completely Different From Any Other Country In The World!">
                        This Is Why Indian-English Is Completely Different From Any Other Country In The World!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-things-to-keep-in-mind-when-you-date-someone-who-worries-a-lot-246873.html" class="tint" title="9 Things To Keep In Mind When You Date Someone Who Worries A Lot">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446456661_502x234.jpg" border="0" alt="9 Things To Keep In Mind When You Date Someone Who Worries A Lot" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-things-to-keep-in-mind-when-you-date-someone-who-worries-a-lot-246873.html" title="9 Things To Keep In Mind When You Date Someone Who Worries A Lot">
                        9 Things To Keep In Mind When You Date Someone Who Worries A Lot                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-ways-to-survive-job-loss-it-s-not-the-end-of-the-world-you-know-246700.html" class="tint" title="10 Ways To Survive Job Loss. Itâs Not The End Of The World You Know!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-2_1446033964_502x234.jpg" border="0" alt="10 Ways To Survive Job Loss. Itâs Not The End Of The World You Know!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-ways-to-survive-job-loss-it-s-not-the-end-of-the-world-you-know-246700.html" title="10 Ways To Survive Job Loss. Itâs Not The End Of The World You Know!">
                        10 Ways To Survive Job Loss. Itâs Not The End Of The World You Know!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-drone-s-eye-view-of-apple-s-monstrous-new-campus-is-mesmerising-246913.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-drone-s-eye-view-of-apple-s-monstrous-new-campus-is-mesmerising-246913.html" class="tint" title="This Drone's Eye View Of Apple's Monstrous New Campus Is Mesmerising!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/apple_campus_2_card_1446533977_502x234.jpg" border="0" alt="This Drone's Eye View Of Apple's Monstrous New Campus Is Mesmerising!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-drone-s-eye-view-of-apple-s-monstrous-new-campus-is-mesmerising-246913.html" title="This Drone's Eye View Of Apple's Monstrous New Campus Is Mesmerising!">
                        This Drone's Eye View Of Apple's Monstrous New Campus Is Mesmerising!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-page-from-saeed-jaffrey-s-personal-diary-on-falling-for-his-ex-wife-will-melt-your-heart-246911.html" class="tint" title="This Page From Saeed Jaffrey's Personal Diary On Falling For His Ex-Wife Will Melt Your Heart!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/scard_1446533975_502x234.jpg" border="0" alt="This Page From Saeed Jaffrey's Personal Diary On Falling For His Ex-Wife Will Melt Your Heart!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-page-from-saeed-jaffrey-s-personal-diary-on-falling-for-his-ex-wife-will-melt-your-heart-246911.html" title="This Page From Saeed Jaffrey's Personal Diary On Falling For His Ex-Wife Will Melt Your Heart!">
                        This Page From Saeed Jaffrey's Personal Diary On Falling For His Ex-Wife Will Melt Your Heart!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/6-must-know-facts-about-google-s-project-loon-which-aims-to-provide-free-internet-to-rural-india-246916.html" class="tint" title="6 Must Know Facts About Google's 'Project Loon' Which Aims To Provide Free Internet To Rural India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446535793_502x234.jpg" border="0" alt="6 Must Know Facts About Google's 'Project Loon' Which Aims To Provide Free Internet To Rural India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/6-must-know-facts-about-google-s-project-loon-which-aims-to-provide-free-internet-to-rural-india-246916.html" title="6 Must Know Facts About Google's 'Project Loon' Which Aims To Provide Free Internet To Rural India">
                        6 Must Know Facts About Google's 'Project Loon' Which Aims To Provide Free Internet To Rural India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-jat-boys-singing-a-haryanvi-rock-fusion-song-is-so-quirky-that-it-ll-make-you-head-bang-246910.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/these-jat-boys-singing-a-haryanvi-rock-fusion-song-is-so-quirky-that-it-ll-make-you-head-bang-246910.html" class="tint" title="These Jat Boys Singing A Haryanvi Rock Fusion Song Is So Quirky That It'll Make You Head-Bang">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/rockini_card_1446531204_502x234.jpg" border="0" alt="These Jat Boys Singing A Haryanvi Rock Fusion Song Is So Quirky That It'll Make You Head-Bang" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/these-jat-boys-singing-a-haryanvi-rock-fusion-song-is-so-quirky-that-it-ll-make-you-head-bang-246910.html" title="These Jat Boys Singing A Haryanvi Rock Fusion Song Is So Quirky That It'll Make You Head-Bang">
                        These Jat Boys Singing A Haryanvi Rock Fusion Song Is So Quirky That It'll Make You Head-Bang                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/22-struggles-every-smartphone-user-knows-all-too-well-246862.html" class="tint" title="22 Struggles Every Smartphone User Knows All Too Well!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-7_1446549793_502x234.jpg" border="0" alt="22 Struggles Every Smartphone User Knows All Too Well!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/22-struggles-every-smartphone-user-knows-all-too-well-246862.html" title="22 Struggles Every Smartphone User Knows All Too Well!">
                        22 Struggles Every Smartphone User Knows All Too Well!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/karan-arjun-s-sultan-hug-other-highlights-of-srk-s-50th-birthday-which-you-might-have-missed-246907.html" class="tint" title="Karan-Arjun's 'Sultan' Hug + Other Highlights Of SRK's 50th Birthday Which You Might Have Missed!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/srk_1446527623_1446527632_502x234.jpg" border="0" alt="Karan-Arjun's 'Sultan' Hug + Other Highlights Of SRK's 50th Birthday Which You Might Have Missed!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/karan-arjun-s-sultan-hug-other-highlights-of-srk-s-50th-birthday-which-you-might-have-missed-246907.html" title="Karan-Arjun's 'Sultan' Hug + Other Highlights Of SRK's 50th Birthday Which You Might Have Missed!">
                        Karan-Arjun's 'Sultan' Hug + Other Highlights Of SRK's 50th Birthday Which You Might Have Missed!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/here-s-a-15-minute-pilates-workout-for-newbies-that-promises-to-tone-the-whole-body-246778.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/here-s-a-15-minute-pilates-workout-for-newbies-that-promises-to-tone-the-whole-body-246778.html" class="tint" title="Here's A 15 Minute Pilates Workout For Newbies That Promises To Tone The Whole Body!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/cover_1446122784_502x234.jpg" border="0" alt="Here's A 15 Minute Pilates Workout For Newbies That Promises To Tone The Whole Body!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/here-s-a-15-minute-pilates-workout-for-newbies-that-promises-to-tone-the-whole-body-246778.html" title="Here's A 15 Minute Pilates Workout For Newbies That Promises To Tone The Whole Body!">
                        Here's A 15 Minute Pilates Workout For Newbies That Promises To Tone The Whole Body!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/20-hilariously-honest-illustrations-that-prove-adulthood-is-no-child-s-play-246813.html" class="tint" title="20 Hilariously Honest Illustrations That Prove Adulthood Is No Child's Play!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1446204908_502x234.jpg" border="0" alt="20 Hilariously Honest Illustrations That Prove Adulthood Is No Child's Play!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/20-hilariously-honest-illustrations-that-prove-adulthood-is-no-child-s-play-246813.html" title="20 Hilariously Honest Illustrations That Prove Adulthood Is No Child's Play!">
                        20 Hilariously Honest Illustrations That Prove Adulthood Is No Child's Play!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-times-when-srk-proved-that-he-looks-good-with-an-actress-of-any-age-246876.html" class="tint" title="11 Times When SRK Proved That He Looks Good With An Actress Of Any Age!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collagevvv_1446459137_1446532338_1446532344_502x234.jpg" border="0" alt="11 Times When SRK Proved That He Looks Good With An Actress Of Any Age!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-times-when-srk-proved-that-he-looks-good-with-an-actress-of-any-age-246876.html" title="11 Times When SRK Proved That He Looks Good With An Actress Of Any Age!">
                        11 Times When SRK Proved That He Looks Good With An Actress Of Any Age!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-slow-mo-guys-spin-a-water-filled-foam-ball-produce-some-hypnotic-split-second-galaxies-246896.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-slow-mo-guys-spin-a-water-filled-foam-ball-produce-some-hypnotic-split-second-galaxies-246896.html" class="tint" title="The Slow-Mo Guys Spin A Water-Filled Foam Ball & Produce Some Hypnotic Split-Second Galaxies!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/waterloggedfoamball_card1_1446465045_502x234.jpg" border="0" alt="The Slow-Mo Guys Spin A Water-Filled Foam Ball & Produce Some Hypnotic Split-Second Galaxies!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-slow-mo-guys-spin-a-water-filled-foam-ball-produce-some-hypnotic-split-second-galaxies-246896.html" title="The Slow-Mo Guys Spin A Water-Filled Foam Ball & Produce Some Hypnotic Split-Second Galaxies!">
                        The Slow-Mo Guys Spin A Water-Filled Foam Ball & Produce Some Hypnotic Split-Second Galaxies!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/10-people-who-slept-off-at-the-worst-moment-possible-246819.html" class="tint" title="10 People Who Slept Off At The Worst Moment Possible">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/napoleon_1446210677_1446210698_502x234.jpg" border="0" alt="10 People Who Slept Off At The Worst Moment Possible" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/10-people-who-slept-off-at-the-worst-moment-possible-246819.html" title="10 People Who Slept Off At The Worst Moment Possible">
                        10 People Who Slept Off At The Worst Moment Possible                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/12-things-shahrukh-khan-said-at-his-50th-birthday-bash-on-how-to-live-life-king-khan-size-246903.html" class="tint" title="12 Things Shahrukh Khan Said At His 50th Birthday Bash On How To Live Life 'King-Khan-Size'">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cszx0rwueaadcbp_1446473366_1446473373_502x234.jpg" border="0" alt="12 Things Shahrukh Khan Said At His 50th Birthday Bash On How To Live Life 'King-Khan-Size'" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/12-things-shahrukh-khan-said-at-his-50th-birthday-bash-on-how-to-live-life-king-khan-size-246903.html" title="12 Things Shahrukh Khan Said At His 50th Birthday Bash On How To Live Life 'King-Khan-Size'">
                        12 Things Shahrukh Khan Said At His 50th Birthday Bash On How To Live Life 'King-Khan-Size'                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/this-is-how-porn-is-shaping-popular-culture-and-affecting-young-adults-today-245531.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/this-is-how-porn-is-shaping-popular-culture-and-affecting-young-adults-today-245531.html" class="tint" title="This Is How Porn Is Shaping Popular Culture And Affecting Young Adults Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/card-3_1443074014_502x234.jpg" border="0" alt="This Is How Porn Is Shaping Popular Culture And Affecting Young Adults Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/this-is-how-porn-is-shaping-popular-culture-and-affecting-young-adults-today-245531.html" title="This Is How Porn Is Shaping Popular Culture And Affecting Young Adults Today">
                        This Is How Porn Is Shaping Popular Culture And Affecting Young Adults Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/13-hilarious-but-accurate-definitions-of-everyday-words-246769.html" class="tint" title="13 Hilarious But Accurate Definitions Of Everyday Words">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1446117094_502x234.jpg" border="0" alt="13 Hilarious But Accurate Definitions Of Everyday Words" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/13-hilarious-but-accurate-definitions-of-everyday-words-246769.html" title="13 Hilarious But Accurate Definitions Of Everyday Words">
                        13 Hilarious But Accurate Definitions Of Everyday Words                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/art-entertainment-have-no-boundaries-says-salman-khan-on-ban-of-pakistani-actors-in-maharashtra-246901.html" class="tint" title="Art & Entertainment Have No Boundaries, Says Salman Khan On Ban Of Pakistani Actors In Maharashtra!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-s_1446467719_502x234.jpg" border="0" alt="Art & Entertainment Have No Boundaries, Says Salman Khan On Ban Of Pakistani Actors In Maharashtra!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/art-entertainment-have-no-boundaries-says-salman-khan-on-ban-of-pakistani-actors-in-maharashtra-246901.html" title="Art & Entertainment Have No Boundaries, Says Salman Khan On Ban Of Pakistani Actors In Maharashtra!">
                        Art & Entertainment Have No Boundaries, Says Salman Khan On Ban Of Pakistani Actors In Maharashtra!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/7-of-the-most-intelligent-crimes-ever-committed-by-indians-246808.html" class="tint" title="7 Of The Most Intelligent Crimes Ever Committed By Indians">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1446201536_502x234.jpg" border="0" alt="7 Of The Most Intelligent Crimes Ever Committed By Indians" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/7-of-the-most-intelligent-crimes-ever-committed-by-indians-246808.html" title="7 Of The Most Intelligent Crimes Ever Committed By Indians">
                        7 Of The Most Intelligent Crimes Ever Committed By Indians                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-of-a-crazy-guy-throwing-an-iphone-6s-in-molten-hot-lava-will-break-your-heart-246895.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-a-crazy-guy-throwing-an-iphone-6s-in-molten-hot-lava-will-break-your-heart-246895.html" class="tint" title="This Video Of A Crazy Guy Throwing An iPhone 6s In Molten Hot Lava Will Break Your Heart!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/iphone_lava_card1_1446462712_502x234.jpg" border="0" alt="This Video Of A Crazy Guy Throwing An iPhone 6s In Molten Hot Lava Will Break Your Heart!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-a-crazy-guy-throwing-an-iphone-6s-in-molten-hot-lava-will-break-your-heart-246895.html" title="This Video Of A Crazy Guy Throwing An iPhone 6s In Molten Hot Lava Will Break Your Heart!">
                        This Video Of A Crazy Guy Throwing An iPhone 6s In Molten Hot Lava Will Break Your Heart!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/first-episode-of-fauji-will-tell-you-where-the-srk-magic-began-246893.html" class="tint" title="First Episode Of Fauji Will Tell You Where The SRK Magic Began!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/srk-collage_1446460224_1446460275_502x234.jpg" border="0" alt="First Episode Of Fauji Will Tell You Where The SRK Magic Began!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/first-episode-of-fauji-will-tell-you-where-the-srk-magic-began-246893.html" title="First Episode Of Fauji Will Tell You Where The SRK Magic Began!">
                        First Episode Of Fauji Will Tell You Where The SRK Magic Began!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/10-delicious-high-protein-chicken-recipes-to-slim-you-down-246790.html" class="tint" title="10 Delicious High-Protein Chicken Recipes To Slim You Down">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card2_1446190638_502x234.jpg" border="0" alt="10 Delicious High-Protein Chicken Recipes To Slim You Down" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/10-delicious-high-protein-chicken-recipes-to-slim-you-down-246790.html" title="10 Delicious High-Protein Chicken Recipes To Slim You Down">
                        10 Delicious High-Protein Chicken Recipes To Slim You Down                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/srk-shows-you-how-to-make-any-girl-fall-for-you-in-under-4-mins-246887.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/srk-shows-you-how-to-make-any-girl-fall-for-you-in-under-4-mins-246887.html" class="tint" title="SRK Shows You How To Make Any Girl Fall For You In Under 4 Mins!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/srk_mashup_card_1446453284_502x234.jpg" border="0" alt="SRK Shows You How To Make Any Girl Fall For You In Under 4 Mins!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/srk-shows-you-how-to-make-any-girl-fall-for-you-in-under-4-mins-246887.html" title="SRK Shows You How To Make Any Girl Fall For You In Under 4 Mins!">
                        SRK Shows You How To Make Any Girl Fall For You In Under 4 Mins!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/14-smart-hacks-to-make-space-in-your-tiny-room-246707.html" class="tint" title="14 Smart Hacks To Make Space In Your Tiny Room">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/wakeup_1446017360_1446017372_502x234.jpg" border="0" alt="14 Smart Hacks To Make Space In Your Tiny Room" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/14-smart-hacks-to-make-space-in-your-tiny-room-246707.html" title="14 Smart Hacks To Make Space In Your Tiny Room">
                        14 Smart Hacks To Make Space In Your Tiny Room                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/inhuman-this-madhya-pradesh-minister-kicked-the-head-of-a-teenager-who-begged-for-her-help-246890.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/inhuman-this-madhya-pradesh-minister-kicked-the-head-of-a-teenager-who-begged-for-her-help-246890.html" class="tint" title="Inhuman! This Madhya Pradesh Minister Kicked The Head Of A Teenager Who Begged For Her Help">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/kusum-mehdele-card1_1446457295_502x234.jpg" border="0" alt="Inhuman! This Madhya Pradesh Minister Kicked The Head Of A Teenager Who Begged For Her Help" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/inhuman-this-madhya-pradesh-minister-kicked-the-head-of-a-teenager-who-begged-for-her-help-246890.html" title="Inhuman! This Madhya Pradesh Minister Kicked The Head Of A Teenager Who Begged For Her Help">
                        Inhuman! This Madhya Pradesh Minister Kicked The Head Of A Teenager Who Begged For Her Help                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/b-town-celebs-shower-srk-with-fantastic-birthday-wishes-share-their-golden-moments-with-him-246875.html" class="tint" title="B-Town Celebs Shower SRK With FANtastic Birthday Wishes, Share Their Golden Moments With Him">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card1_1446451119_502x234.jpg" border="0" alt="B-Town Celebs Shower SRK With FANtastic Birthday Wishes, Share Their Golden Moments With Him" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/b-town-celebs-shower-srk-with-fantastic-birthday-wishes-share-their-golden-moments-with-him-246875.html" title="B-Town Celebs Shower SRK With FANtastic Birthday Wishes, Share Their Golden Moments With Him">
                        B-Town Celebs Shower SRK With FANtastic Birthday Wishes, Share Their Golden Moments With Him                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-what-happens-when-two-mighty-cowboys-fight-it-out-in-rajasthani-style-246882.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-is-what-happens-when-two-mighty-cowboys-fight-it-out-in-rajasthani-style-246882.html" class="tint" title="This Is What Happens When Two Mighty Cowboys Fight It Out In Rajasthani Style!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/rajashtanicowboys_card_1446450741_502x234.jpg" border="0" alt="This Is What Happens When Two Mighty Cowboys Fight It Out In Rajasthani Style!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-is-what-happens-when-two-mighty-cowboys-fight-it-out-in-rajasthani-style-246882.html" title="This Is What Happens When Two Mighty Cowboys Fight It Out In Rajasthani Style!">
                        This Is What Happens When Two Mighty Cowboys Fight It Out In Rajasthani Style!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/srk-doesn-t-hold-back-on-his-50th-birthday-says-there-is-extreme-intolerance-in-india-calls-those-returning-their-awards-brave-246884.html" class="tint" title="SRK Says There Is 'Extreme Intolerance' In India, Calls Those Returning Their Awards 'Brave'">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/c1_1446452049_502x234.jpg" border="0" alt="SRK Says There Is 'Extreme Intolerance' In India, Calls Those Returning Their Awards 'Brave'" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/srk-doesn-t-hold-back-on-his-50th-birthday-says-there-is-extreme-intolerance-in-india-calls-those-returning-their-awards-brave-246884.html" title="SRK Says There Is 'Extreme Intolerance' In India, Calls Those Returning Their Awards 'Brave'">
                        SRK Says There Is 'Extreme Intolerance' In India, Calls Those Returning Their Awards 'Brave'                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/this-is-why-living-within-a-10-km-radius-of-an-airport-is-bad-for-your-health-246716.html" class="tint" title="This Is Why Living Within A 10 Km Radius Of An Airport Is Bad For Your Health!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-4_1446026225_502x234.jpg" border="0" alt="This Is Why Living Within A 10 Km Radius Of An Airport Is Bad For Your Health!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/this-is-why-living-within-a-10-km-radius-of-an-airport-is-bad-for-your-health-246716.html" title="This Is Why Living Within A 10 Km Radius Of An Airport Is Bad For Your Health!">
                        This Is Why Living Within A 10 Km Radius Of An Airport Is Bad For Your Health!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/7-extremely-difficult-questions-that-have-been-asked-at-google-interviews-246767.html" class="tint" title="7 Extremely Difficult Questions That Have Been Asked At Google Interviews">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1446116218_502x234.jpg" border="0" alt="7 Extremely Difficult Questions That Have Been Asked At Google Interviews" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/7-extremely-difficult-questions-that-have-been-asked-at-google-interviews-246767.html" title="7 Extremely Difficult Questions That Have Been Asked At Google Interviews">
                        7 Extremely Difficult Questions That Have Been Asked At Google Interviews                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/8-reasons-you-should-never-underestimate-the-power-of-positive-thinking-246659.html" class="tint" title="8 Reasons You Should Never Underestimate The Power Of Positive Thinking">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1445929874_502x234.jpg" border="0" alt="8 Reasons You Should Never Underestimate The Power Of Positive Thinking" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/8-reasons-you-should-never-underestimate-the-power-of-positive-thinking-246659.html" title="8 Reasons You Should Never Underestimate The Power Of Positive Thinking">
                        8 Reasons You Should Never Underestimate The Power Of Positive Thinking                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-ways-in-which-rejection-is-a-blessing-in-disguise-246731.html" class="tint" title="11 Ways In Which Rejection Is A Blessing In Disguise">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/chak-de-card_1446032346_502x234.jpg" border="0" alt="11 Ways In Which Rejection Is A Blessing In Disguise" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-ways-in-which-rejection-is-a-blessing-in-disguise-246731.html" title="11 Ways In Which Rejection Is A Blessing In Disguise">
                        11 Ways In Which Rejection Is A Blessing In Disguise                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/on-his-50th-birthday-srk-gifts-himself-this-phenomenal-teaser-of-fan-mustwatch-for-srk-fans-246871.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/on-his-50th-birthday-srk-gifts-himself-this-phenomenal-teaser-of-fan-mustwatch-for-srk-fans-246871.html" class="tint" title="On His 50th Birthday, SRK Gifts Himself This Phenomenal Teaser Of Fan #MustWatch For SRK Fans!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/fan_1446403827_1446403833_502x234.jpg" border="0" alt="On His 50th Birthday, SRK Gifts Himself This Phenomenal Teaser Of Fan #MustWatch For SRK Fans!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/on-his-50th-birthday-srk-gifts-himself-this-phenomenal-teaser-of-fan-mustwatch-for-srk-fans-246871.html" title="On His 50th Birthday, SRK Gifts Himself This Phenomenal Teaser Of Fan #MustWatch For SRK Fans!">
                        On His 50th Birthday, SRK Gifts Himself This Phenomenal Teaser Of Fan #MustWatch For SRK Fans!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/9-things-you-should-never-do-to-your-eyes-246797.html" class="tint" title="9 Things You Should Never Do To Your Eyes">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-fdg_1446548367_502x234.jpg" border="0" alt="9 Things You Should Never Do To Your Eyes" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/9-things-you-should-never-do-to-your-eyes-246797.html" title="9 Things You Should Never Do To Your Eyes">
                        9 Things You Should Never Do To Your Eyes                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/barkha-dutt-raveena-tandon-sling-mud-at-each-other-on-the-auspicious-occasion-of-karwachauth-246870.html" class="tint" title="Barkha Dutt & Raveena Tandon Sling Mud At Each Other On The Auspicious Occasion Of Karwachauth!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/pl_1446380159_1446380166_502x234.jpg" border="0" alt="Barkha Dutt & Raveena Tandon Sling Mud At Each Other On The Auspicious Occasion Of Karwachauth!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/barkha-dutt-raveena-tandon-sling-mud-at-each-other-on-the-auspicious-occasion-of-karwachauth-246870.html" title="Barkha Dutt & Raveena Tandon Sling Mud At Each Other On The Auspicious Occasion Of Karwachauth!">
                        Barkha Dutt & Raveena Tandon Sling Mud At Each Other On The Auspicious Occasion Of Karwachauth!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/9-foods-you-can-carry-when-you-re-out-for-a-picnic-a-business-meet-or-a-trek-246801.html" class="tint" title="9 Foods You Can Carry When You're Out For A Picnic, A Business Meet Or A Trek">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/sqsq_1446201202_1446201233_502x234.jpg" border="0" alt="9 Foods You Can Carry When You're Out For A Picnic, A Business Meet Or A Trek" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/9-foods-you-can-carry-when-you-re-out-for-a-picnic-a-business-meet-or-a-trek-246801.html" title="9 Foods You Can Carry When You're Out For A Picnic, A Business Meet Or A Trek">
                        9 Foods You Can Carry When You're Out For A Picnic, A Business Meet Or A Trek                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/if-you-re-wondering-where-has-all-the-black-money-gone-then-this-guy-has-all-the-answers-246856.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/if-you-re-wondering-where-has-all-the-black-money-gone-then-this-guy-has-all-the-answers-246856.html" class="tint" title="If You're Wondering Where Has All The Black Money Gone, Then This Guy Has All The Answers!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/blackmoney_card_1446365068_502x234.jpg" border="0" alt="If You're Wondering Where Has All The Black Money Gone, Then This Guy Has All The Answers!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/if-you-re-wondering-where-has-all-the-black-money-gone-then-this-guy-has-all-the-answers-246856.html" title="If You're Wondering Where Has All The Black Money Gone, Then This Guy Has All The Answers!">
                        If You're Wondering Where Has All The Black Money Gone, Then This Guy Has All The Answers!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood-star-leonardo-dicaprio-on-a-secret-visit-to-india-takes-time-out-to-see-the-taj-mahal-246868.html" class="tint" title="Hollywood Star Leonardo DiCaprio On A Secret Visit To India, Takes Time Out To See The Taj Mahal">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/2_1446377499_1446377511_502x234.jpg" border="0" alt="Hollywood Star Leonardo DiCaprio On A Secret Visit To India, Takes Time Out To See The Taj Mahal" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood-star-leonardo-dicaprio-on-a-secret-visit-to-india-takes-time-out-to-see-the-taj-mahal-246868.html" title="Hollywood Star Leonardo DiCaprio On A Secret Visit To India, Takes Time Out To See The Taj Mahal">
                        Hollywood Star Leonardo DiCaprio On A Secret Visit To India, Takes Time Out To See The Taj Mahal                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/7-times-when-shah-rukh-khan-stole-the-thunder-of-lead-actors-with-a-mere-cameo-246751.html" class="tint" title="7 Times When Shah Rukh Khan Stole The Thunder Of Lead Actors With A Mere Cameo!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1446101859_1446101866_502x234.jpg" border="0" alt="7 Times When Shah Rukh Khan Stole The Thunder Of Lead Actors With A Mere Cameo!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/7-times-when-shah-rukh-khan-stole-the-thunder-of-lead-actors-with-a-mere-cameo-246751.html" title="7 Times When Shah Rukh Khan Stole The Thunder Of Lead Actors With A Mere Cameo!">
                        7 Times When Shah Rukh Khan Stole The Thunder Of Lead Actors With A Mere Cameo!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/guess-who-was-the-original-choice-for-the-role-of-mr-india-246866.html" class="tint" title="Guess Who Was The Original Choice For The Role Of Mr. India!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/21_1446375446_1446375452_502x234.jpg" border="0" alt="Guess Who Was The Original Choice For The Role Of Mr. India!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/guess-who-was-the-original-choice-for-the-role-of-mr-india-246866.html" title="Guess Who Was The Original Choice For The Role Of Mr. India!">
                        Guess Who Was The Original Choice For The Role Of Mr. India!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/forget-deepika-padukone-ranbir-kapoor-totally-owns-tamasha-s-song-wat-wat-wat-246863.html" class="tint" title="Forget Deepika Padukone, Ranbir Kapoor Totally Owns Tamasha's Song 'Wat Wat Wat'!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/wat-wat-card_1446371813_1446371818_502x234.jpg" border="0" alt="Forget Deepika Padukone, Ranbir Kapoor Totally Owns Tamasha's Song 'Wat Wat Wat'!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/forget-deepika-padukone-ranbir-kapoor-totally-owns-tamasha-s-song-wat-wat-wat-246863.html" title="Forget Deepika Padukone, Ranbir Kapoor Totally Owns Tamasha's Song 'Wat Wat Wat'!">
                        Forget Deepika Padukone, Ranbir Kapoor Totally Owns Tamasha's Song 'Wat Wat Wat'!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/this-doctor-gave-up-his-practice-and-opened-a-farmacy-to-use-organic-food-to-treat-people-246629.html" class="tint" title="This Doctor Gave Up His Practice And Opened A âFarmacyâ To Use Organic Food To Treat People">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1445854152_502x234.jpg" border="0" alt="This Doctor Gave Up His Practice And Opened A âFarmacyâ To Use Organic Food To Treat People" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/this-doctor-gave-up-his-practice-and-opened-a-farmacy-to-use-organic-food-to-treat-people-246629.html" title="This Doctor Gave Up His Practice And Opened A âFarmacyâ To Use Organic Food To Treat People">
                        This Doctor Gave Up His Practice And Opened A âFarmacyâ To Use Organic Food To Treat People                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/vikas-bhalla-fails-to-strike-a-chord-with-the-audience-gets-axed-from-bigg-boss-9_-246860.html" class="tint" title="Vikas Bhalla Fails To Strike A Chord With The Audience, Gets Axed From Bigg Boss 9!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/vb123_1446370116_1446370121_502x234.jpg" border="0" alt="Vikas Bhalla Fails To Strike A Chord With The Audience, Gets Axed From Bigg Boss 9!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/vikas-bhalla-fails-to-strike-a-chord-with-the-audience-gets-axed-from-bigg-boss-9_-246860.html" title="Vikas Bhalla Fails To Strike A Chord With The Audience, Gets Axed From Bigg Boss 9!">
                        Vikas Bhalla Fails To Strike A Chord With The Audience, Gets Axed From Bigg Boss 9!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/a-55-yo-man-dies-of-shock-after-watching-telugu-horror-film-raju-gari-gadhi-246858.html" class="tint" title="A 55 YO Man Dies Of Shock After Watching Telugu Horror Film 'Raju Gari Gadhi'!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/2_1446367180_1446367189_502x234.jpg" border="0" alt="A 55 YO Man Dies Of Shock After Watching Telugu Horror Film 'Raju Gari Gadhi'!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/a-55-yo-man-dies-of-shock-after-watching-telugu-horror-film-raju-gari-gadhi-246858.html" title="A 55 YO Man Dies Of Shock After Watching Telugu Horror Film 'Raju Gari Gadhi'!">
                        A 55 YO Man Dies Of Shock After Watching Telugu Horror Film 'Raju Gari Gadhi'!                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/these-20-breathtaking-underwater-portraits-will-make-you-want-to-take-a-dive-right-now-246906.html" class="tint" title="These 20 Breathtaking Underwater Portraits Will Make You Want To Take A Dive Right Now!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage-card-image_1446479743_1446479812_1446479830_218x102.jpg" border="0" alt="These 20 Breathtaking Underwater Portraits Will Make You Want To Take A Dive Right Now!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/these-20-breathtaking-underwater-portraits-will-make-you-want-to-take-a-dive-right-now-246906.html" title="These 20 Breathtaking Underwater Portraits Will Make You Want To Take A Dive Right Now!">
                            These 20 Breathtaking Underwater Portraits Will Make You Want To Take A Dive Right Now!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/wow-rajinikanth-amitabh-bachchan-might-share-screen-space-in-robot-2_-246936.html" class="tint" title="Wow! Rajinikanth & Amitabh Bachchan Might Share Screen Space In Robot 2!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/rajinikanth-bigb_1446551951_1446551956_218x102.jpg" border="0" alt="Wow! Rajinikanth & Amitabh Bachchan Might Share Screen Space In Robot 2!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/wow-rajinikanth-amitabh-bachchan-might-share-screen-space-in-robot-2_-246936.html" title="Wow! Rajinikanth & Amitabh Bachchan Might Share Screen Space In Robot 2!">
                            Wow! Rajinikanth & Amitabh Bachchan Might Share Screen Space In Robot 2!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/7-of-the-most-intelligent-crimes-ever-committed-by-indians-246808.html" class="tint" title="7 Of The Most Intelligent Crimes Ever Committed By Indians">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1446201536_218x102.jpg" border="0" alt="7 Of The Most Intelligent Crimes Ever Committed By Indians"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/7-of-the-most-intelligent-crimes-ever-committed-by-indians-246808.html" title="7 Of The Most Intelligent Crimes Ever Committed By Indians">
                            7 Of The Most Intelligent Crimes Ever Committed By Indians                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-times-when-srk-proved-that-he-looks-good-with-an-actress-of-any-age-246876.html" class="tint" title="11 Times When SRK Proved That He Looks Good With An Actress Of Any Age!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collagevvv_1446459137_1446532338_1446532344_218x102.jpg" border="0" alt="11 Times When SRK Proved That He Looks Good With An Actress Of Any Age!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-times-when-srk-proved-that-he-looks-good-with-an-actress-of-any-age-246876.html" title="11 Times When SRK Proved That He Looks Good With An Actress Of Any Age!">
                            11 Times When SRK Proved That He Looks Good With An Actress Of Any Age!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/art-entertainment-have-no-boundaries-says-salman-khan-on-ban-of-pakistani-actors-in-maharashtra-246901.html" class="tint" title="Art & Entertainment Have No Boundaries, Says Salman Khan On Ban Of Pakistani Actors In Maharashtra!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-s_1446467719_218x102.jpg" border="0" alt="Art & Entertainment Have No Boundaries, Says Salman Khan On Ban Of Pakistani Actors In Maharashtra!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/art-entertainment-have-no-boundaries-says-salman-khan-on-ban-of-pakistani-actors-in-maharashtra-246901.html" title="Art & Entertainment Have No Boundaries, Says Salman Khan On Ban Of Pakistani Actors In Maharashtra!">
                            Art & Entertainment Have No Boundaries, Says Salman Khan On Ban Of Pakistani Actors In Maharashtra!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '246891', 'homepage', {'nonInteraction': 1});
			ga('send', 'event', 'OnLoad Partner Stories', '245550', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/bccl16_1446616927_1446616931_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446617047_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/navut6_1446615838_1446615841_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/satya502_1446614935_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/twitter502_1446568919_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446554712_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1446451406_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card1_1446548435_1446548468_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446535793_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443099189_1443099205_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446536851_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/mahira-khan-halloween_0_0_1446549885_1446549895_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-7_1446549793_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/kamal502_1446567366_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/farookcollege_1446561536_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/jain-monk-502_1446559754_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/antarctica-502_1446558370_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/cp_1446461705_1446461711_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446449596_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/srk5_1446555423_1446555430_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446551972_1446551979_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/rockini_card_1446531204_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/srk_1446527623_1446527632_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446551972_1446551979_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446536236_1446536241_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/cszx0rwueaadcbp_1446473366_1446473373_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/card1_1446554149_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/std5_1446551710_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/srkcard_1446548448_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/12205101_10153738801644851_695442678_n_1446550205_1446550212_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/china502_1446544136_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/rajinikanth-bigb_1446551951_1446551956_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/mahira-khan-halloween_0_0_1446549885_1446549895_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage-card-image_1446479743_1446479812_1446479830_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446536851_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446448331_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446536236_1446536241_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/indianeng_card1_1446538015_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446456661_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-2_1446033964_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/apple_campus_2_card_1446533977_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/scard_1446533975_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446535793_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/rockini_card_1446531204_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-7_1446549793_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/srk_1446527623_1446527632_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/cover_1446122784_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1446204908_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collagevvv_1446459137_1446532338_1446532344_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/waterloggedfoamball_card1_1446465045_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/napoleon_1446210677_1446210698_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cszx0rwueaadcbp_1446473366_1446473373_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/card-3_1443074014_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1446117094_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-s_1446467719_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1446201536_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/iphone_lava_card1_1446462712_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/srk-collage_1446460224_1446460275_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card2_1446190638_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/srk_mashup_card_1446453284_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/wakeup_1446017360_1446017372_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/kusum-mehdele-card1_1446457295_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card1_1446451119_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/rajashtanicowboys_card_1446450741_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/c1_1446452049_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-4_1446026225_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1446116218_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1445929874_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/chak-de-card_1446032346_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/fan_1446403827_1446403833_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-fdg_1446548367_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/pl_1446380159_1446380166_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/sqsq_1446201202_1446201233_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/blackmoney_card_1446365068_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/2_1446377499_1446377511_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1446101859_1446101866_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/21_1446375446_1446375452_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/wat-wat-card_1446371813_1446371818_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1445854152_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/vb123_1446370116_1446370121_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/2_1446367180_1446367189_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage-card-image_1446479743_1446479812_1446479830_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/rajinikanth-bigb_1446551951_1446551956_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1446201536_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collagevvv_1446459137_1446532338_1446532344_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-s_1446467719_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,820,770<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>50,403 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.47" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.47"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.47"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.47"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.47"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>