<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-261c451.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-261c451.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-261c451.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-261c451.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-261c451.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '261c451',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-261c451.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<span  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></span>
<span  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></span>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <span  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></span>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/adele/" class="tag2">adele</a>
	<a href="/search/american%20horror%20story/" class="tag2">american horror story</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/arrow/" class="tag3">arrow</a>
	<a href="/search/christmas/" class="tag4">christmas</a>
	<a href="/search/creed/" class="tag2">creed</a>
	<a href="/search/discography/" class="tag2">discography</a>
	<a href="/search/dual%20audio%20hindi/" class="tag3">dual audio hindi</a>
	<a href="/search/empire/" class="tag2">empire</a>
	<a href="/search/fallout%204/" class="tag2">fallout 4</a>
	<a href="/search/flash/" class="tag2">flash</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/hindi/" class="tag9">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/hotel%20transylvania%202/" class="tag2">hotel transylvania 2</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/jessica%20jones/" class="tag4">jessica jones</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/monsters%20seasons%201%203%20complete/" class="tag4">monsters seasons 1 3 complete</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/poser/" class="tag2">poser</a>
	<a href="/search/prem%20ratan%20dhan%20payo/" class="tag2">prem ratan dhan payo</a>
	<a href="/search/quantico/" class="tag2">quantico</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/south%20park/" class="tag2">south park</a>
	<a href="/search/spectre/" class="tag2">spectre</a>
	<a href="/search/star%20wars/" class="tag3">star wars</a>
	<a href="/search/supergirl/" class="tag2">supergirl</a>
	<a href="/search/supernatural/" class="tag2">supernatural</a>
	<a href="/search/survivor/" class="tag2">survivor</a>
	<a href="/search/tamil/" class="tag5">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20flash/" class="tag3">the flash</a>
	<a href="/search/the%20flash%20s02e08/" class="tag2">the flash s02e08</a>
	<a href="/search/the%20man%20in%20the%20high%20castle/" class="tag2">the man in the high castle</a>
	<a href="/search/the%20martian/" class="tag2">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag5">the walking dead</a>
	<a href="/search/walking%20dead/" class="tag2">walking dead</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11653170,0" class="icommentjs kaButton smallButton rightButton" href="/talvar-2015-hindi-bluray-720p-x264-aac-hon3y-t11653170.html#comment">55 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/talvar-2015-hindi-bluray-720p-x264-aac-hon3y-t11653170.html" class="cellMainLink">Talvar 2015 Hindi BluRay 720p x264 AAC - Hon3y</a></div>
			</td>
			<td class="nobr center" data-sort="1099139566">1.02 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T20:39:32+00:00">25 Nov 2015, 20:39:32</span></td>
			<td class="green center">4259</td>
			<td class="red lasttd center">5512</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645939,0" class="icommentjs kaButton smallButton rightButton" href="/santas-little-helper-2015-hdrip-xvid-ac3-evo-t11645939.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/santas-little-helper-2015-hdrip-xvid-ac3-evo-t11645939.html" class="cellMainLink">Santas Little Helper 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1504298166">1.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T13:52:08+00:00">24 Nov 2015, 13:52:08</span></td>
			<td class="green center">3618</td>
			<td class="red lasttd center">2542</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11656078,0" class="icommentjs kaButton smallButton rightButton" href="/poker-night-2014-1080p-bluray-x264-dts-jyk-t11656078.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/poker-night-2014-1080p-bluray-x264-dts-jyk-t11656078.html" class="cellMainLink">Poker Night 2014 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2841218731">2.65 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T11:20:00+00:00">26 Nov 2015, 11:20:00</span></td>
			<td class="green center">2770</td>
			<td class="red lasttd center">2440</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655693,0" class="icommentjs kaButton smallButton rightButton" href="/the-little-prince-2015-720p-bluray-x264-aac-etrg-t11655693.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-little-prince-2015-720p-bluray-x264-aac-etrg-t11655693.html" class="cellMainLink">The Little Prince 2015 720p BluRay x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="838557745">799.71 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T09:35:14+00:00">26 Nov 2015, 09:35:14</span></td>
			<td class="green center">2462</td>
			<td class="red lasttd center">2078</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651132,0" class="icommentjs kaButton smallButton rightButton" href="/unnatural-2015-dvdrip-xvid-evo-t11651132.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/unnatural-2015-dvdrip-xvid-evo-t11651132.html" class="cellMainLink">Unnatural 2015 DVDRip XviD-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="906013106">864.04 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T12:04:29+00:00">25 Nov 2015, 12:04:29</span></td>
			<td class="green center">2256</td>
			<td class="red lasttd center">1545</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11650324,0" class="icommentjs kaButton smallButton rightButton" href="/captain-america-civil-war-trailer-world-premiere-1080p-lucifer22-t11650324.html#comment">39 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/captain-america-civil-war-trailer-world-premiere-1080p-lucifer22-t11650324.html" class="cellMainLink">Captain America Civil War - Trailer World Premiere 1080p [Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="52109655">49.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T08:22:43+00:00">25 Nov 2015, 08:22:43</span></td>
			<td class="green center">2439</td>
			<td class="red lasttd center">326</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645859,0" class="icommentjs kaButton smallButton rightButton" href="/maze-runner-the-scorch-trials-2015-fansub-vostfr-720p-bluray-x264-teamsuw-mkv-t11645859.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/maze-runner-the-scorch-trials-2015-fansub-vostfr-720p-bluray-x264-teamsuw-mkv-t11645859.html" class="cellMainLink">Maze Runner The Scorch Trials 2015 FANSUB VOSTFR 720p BluRay x264-TeamSuW mkv</a></div>
			</td>
			<td class="nobr center" data-sort="7041100227">6.56 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T13:28:21+00:00">24 Nov 2015, 13:28:21</span></td>
			<td class="green center">1925</td>
			<td class="red lasttd center">923</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651125,0" class="icommentjs kaButton smallButton rightButton" href="/l-a-slasher-2015-dvdrip-xvid-evo-t11651125.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/l-a-slasher-2015-dvdrip-xvid-evo-t11651125.html" class="cellMainLink">L A Slasher 2015 DVDRip XviD-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="914343012">871.99 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T12:02:26+00:00">25 Nov 2015, 12:02:26</span></td>
			<td class="green center">1807</td>
			<td class="red lasttd center">1092</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648146,0" class="icommentjs kaButton smallButton rightButton" href="/the-33-2015-1080p-web-dl-x264-ac3-jyk-t11648146.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-33-2015-1080p-web-dl-x264-ac3-jyk-t11648146.html" class="cellMainLink">The 33 2015 1080p WEB-DL x264 AC3-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3758889036">3.5 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T19:33:11+00:00">24 Nov 2015, 19:33:11</span></td>
			<td class="green center">1163</td>
			<td class="red lasttd center">1156</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655860,0" class="icommentjs kaButton smallButton rightButton" href="/talvar-2015-dvdrip-x264-ac3-5-1-esub-ddr-t11655860.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/talvar-2015-dvdrip-x264-ac3-5-1-esub-ddr-t11655860.html" class="cellMainLink">Talvar (2015) DVDRip - x264 - AC3 5.1 - ESub [DDR]</a></div>
			</td>
			<td class="nobr center" data-sort="1581756810">1.47 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T10:12:56+00:00">26 Nov 2015, 10:12:56</span></td>
			<td class="green center">707</td>
			<td class="red lasttd center">1871</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649234,0" class="icommentjs kaButton smallButton rightButton" href="/goodbye-mr-loser-2015-hd720p-x264-aac-mandarin-chs-eng-mp4ba-t11649234.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/goodbye-mr-loser-2015-hd720p-x264-aac-mandarin-chs-eng-mp4ba-t11649234.html" class="cellMainLink">Goodbye Mr Loser 2015 HD720P X264 AAC Mandarin CHS-ENG Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="1525153914">1.42 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T02:59:40+00:00">25 Nov 2015, 02:59:40</span></td>
			<td class="green center">712</td>
			<td class="red lasttd center">1389</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655781,0" class="icommentjs kaButton smallButton rightButton" href="/kaagaz-ke-fools-2015-dvdrip-xvid-ac3-esub-ddr-t11655781.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kaagaz-ke-fools-2015-dvdrip-xvid-ac3-esub-ddr-t11655781.html" class="cellMainLink">Kaagaz Ke Fools (2015) DVDRip - XviD - AC3 - ESub [DDR]</a></div>
			</td>
			<td class="nobr center" data-sort="1577471977">1.47 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T09:58:10+00:00">26 Nov 2015, 09:58:10</span></td>
			<td class="green center">533</td>
			<td class="red lasttd center">1674</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11647747,0" class="icommentjs kaButton smallButton rightButton" href="/attack-on-titan-part-2-2015-720p-web-dl-650mb-mkvcage-t11647747.html#comment">39 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/attack-on-titan-part-2-2015-720p-web-dl-650mb-mkvcage-t11647747.html" class="cellMainLink">Attack on Titan: Part 2 (2015) 720p WEB-DL 650MB - MkvCage</a></div>
			</td>
			<td class="nobr center" data-sort="683579394">651.91 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T18:13:30+00:00">24 Nov 2015, 18:13:30</span></td>
			<td class="green center">525</td>
			<td class="red lasttd center">185</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/misiÃ³n-secreta-extraction-spanish-espaÃ±ol-hdrip-xvid-elitetorrent-t11659838.html" class="cellMainLink">MisiÃ³n secreta: Extraction SPANiSH ESPAÃ±OL HDRip XviD-ELiTETORRENT</a></div>
			</td>
			<td class="nobr center" data-sort="1936484363">1.8 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-27T07:15:17+00:00">27 Nov 2015, 07:15:17</span></td>
			<td class="green center">272</td>
			<td class="red lasttd center">657</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11652069,0" class="icommentjs kaButton smallButton rightButton" href="/hitman-agent-47-2015-bdrip-xvid-etrg-t11652069.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hitman-agent-47-2015-bdrip-xvid-etrg-t11652069.html" class="cellMainLink">Hitman Agent 47 2015 BDRip XviD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="742334946">707.95 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T16:22:29+00:00">25 Nov 2015, 16:22:29</span></td>
			<td class="green center">362</td>
			<td class="red lasttd center">290</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643465,0" class="icommentjs kaButton smallButton rightButton" href="/gotham-s02e10-hdtv-x264-lol-ettv-t11643465.html#comment">190 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gotham-s02e10-hdtv-x264-lol-ettv-t11643465.html" class="cellMainLink">Gotham S02E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="231998941">221.25 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T02:01:18+00:00">24 Nov 2015, 02:01:18</span></td>
			<td class="green center">9467</td>
			<td class="red lasttd center">790</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648928,0" class="icommentjs kaButton smallButton rightButton" href="/limitless-s01e10-hdtv-x264-lol-rartv-t11648928.html#comment">78 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/limitless-s01e10-hdtv-x264-lol-rartv-t11648928.html" class="cellMainLink">Limitless S01E10 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="257294263">245.37 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T00:59:02+00:00">25 Nov 2015, 00:59:02</span></td>
			<td class="green center">9203</td>
			<td class="red lasttd center">868</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11654490,0" class="icommentjs kaButton smallButton rightButton" href="/empire-2015-s02e09-hdtv-x264-killers-rartv-t11654490.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/empire-2015-s02e09-hdtv-x264-killers-rartv-t11654490.html" class="cellMainLink">Empire 2015 S02E09 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="384714603">366.89 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T02:28:13+00:00">26 Nov 2015, 02:28:13</span></td>
			<td class="green center">7794</td>
			<td class="red lasttd center">1676</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643455,0" class="icommentjs kaButton smallButton rightButton" href="/supergirl-s01e04-hdtv-x264-lol-ettv-t11643455.html#comment">177 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supergirl-s01e04-hdtv-x264-lol-ettv-t11643455.html" class="cellMainLink">Supergirl S01E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="358426975">341.82 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T01:59:23+00:00">24 Nov 2015, 01:59:23</span></td>
			<td class="green center">7254</td>
			<td class="red lasttd center">912</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643622,0" class="icommentjs kaButton smallButton rightButton" href="/blindspot-s01e10-hdtv-x264-lol-ettv-t11643622.html#comment">153 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blindspot-s01e10-hdtv-x264-lol-ettv-t11643622.html" class="cellMainLink">Blindspot S01E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="301103349">287.15 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T03:02:19+00:00">24 Nov 2015, 03:02:19</span></td>
			<td class="green center">7161</td>
			<td class="red lasttd center">859</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11659035,0" class="icommentjs kaButton smallButton rightButton" href="/2-broke-girls-s05e03-hdtv-x264-lol-rartv-t11659035.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/2-broke-girls-s05e03-hdtv-x264-lol-rartv-t11659035.html" class="cellMainLink">2 Broke Girls S05E03 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="152596915">145.53 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-27T02:59:03+00:00">27 Nov 2015, 02:59:03</span></td>
			<td class="green center">4351</td>
			<td class="red lasttd center">1394</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11658731,0" class="icommentjs kaButton smallButton rightButton" href="/elementary-s04e04-hdtv-x264-lol-ettv-t11658731.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/elementary-s04e04-hdtv-x264-lol-ettv-t11658731.html" class="cellMainLink">Elementary S04E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="219175468">209.02 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-27T01:01:31+00:00">27 Nov 2015, 01:01:31</span></td>
			<td class="green center">4171</td>
			<td class="red lasttd center">1693</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643904,0" class="icommentjs kaButton smallButton rightButton" href="/fargo-s02e07-hdtv-x264-killers-ettv-t11643904.html#comment">81 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fargo-s02e07-hdtv-x264-killers-ettv-t11643904.html" class="cellMainLink">Fargo S02E07 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="450636574">429.76 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T04:35:19+00:00">24 Nov 2015, 04:35:19</span></td>
			<td class="green center">4300</td>
			<td class="red lasttd center">346</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643603,0" class="icommentjs kaButton smallButton rightButton" href="/scorpion-s02e10-hdtv-x264-lol-ettv-t11643603.html#comment">87 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scorpion-s02e10-hdtv-x264-lol-ettv-t11643603.html" class="cellMainLink">Scorpion S02E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="308872348">294.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T02:59:18+00:00">24 Nov 2015, 02:59:18</span></td>
			<td class="green center">4009</td>
			<td class="red lasttd center">416</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649409,0" class="icommentjs kaButton smallButton rightButton" href="/scream-queens-2015-s01e10-hdtv-x264-killers-ettv-t11649409.html#comment">40 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scream-queens-2015-s01e10-hdtv-x264-killers-ettv-t11649409.html" class="cellMainLink">Scream Queens 2015 S01E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="305809898">291.64 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T04:26:37+00:00">25 Nov 2015, 04:26:37</span></td>
			<td class="green center">2876</td>
			<td class="red lasttd center">441</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643610,0" class="icommentjs kaButton smallButton rightButton" href="/castle-2009-s08e08-hdtv-x264-lol-ettv-t11643610.html#comment">49 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/castle-2009-s08e08-hdtv-x264-lol-ettv-t11643610.html" class="cellMainLink">Castle 2009 S08E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="301466780">287.5 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T03:00:18+00:00">24 Nov 2015, 03:00:18</span></td>
			<td class="green center">2953</td>
			<td class="red lasttd center">274</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11654739,0" class="icommentjs kaButton smallButton rightButton" href="/survivor-s31e10e11-hdtv-x264-fum-ettv-t11654739.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/survivor-s31e10e11-hdtv-x264-fum-ettv-t11654739.html" class="cellMainLink">Survivor S31E10E11 HDTV x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="713816287">680.75 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T04:34:20+00:00">26 Nov 2015, 04:34:20</span></td>
			<td class="green center">2799</td>
			<td class="red lasttd center">566</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649048,0" class="icommentjs kaButton smallButton rightButton" href="/ncis-s13e10-hdtv-x264-lol-rartv-t11649048.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ncis-s13e10-hdtv-x264-lol-rartv-t11649048.html" class="cellMainLink">NCIS S13E10 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="215199613">205.23 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T01:55:43+00:00">25 Nov 2015, 01:55:43</span></td>
			<td class="green center">2112</td>
			<td class="red lasttd center">154</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649065,0" class="icommentjs kaButton smallButton rightButton" href="/chicago-med-s01e02-hdtv-x264-killers-ettv-t11649065.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/chicago-med-s01e02-hdtv-x264-killers-ettv-t11649065.html" class="cellMainLink">Chicago Med S01E02 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="335724778">320.17 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T02:00:19+00:00">25 Nov 2015, 02:00:19</span></td>
			<td class="green center">1726</td>
			<td class="red lasttd center">379</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649362,0" class="icommentjs kaButton smallButton rightButton" href="/chicago-fire-s04e07-hdtv-x264-killers-rartv-t11649362.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/chicago-fire-s04e07-hdtv-x264-killers-rartv-t11649362.html" class="cellMainLink">Chicago Fire S04E07 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="342474749">326.61 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T04:04:27+00:00">25 Nov 2015, 04:04:27</span></td>
			<td class="green center">1428</td>
			<td class="red lasttd center">473</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11657796,0" class="icommentjs kaButton smallButton rightButton" href="/thangamagan-2015-itunes-tamil-mp3-320kbps-t11657796.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/thangamagan-2015-itunes-tamil-mp3-320kbps-t11657796.html" class="cellMainLink">Thangamagan - 2015 - iTunes Tamil Mp3 320Kbps</a></div>
			</td>
			<td class="nobr center" data-sort="33411391">31.86 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T19:37:17+00:00">26 Nov 2015, 19:37:17</span></td>
			<td class="green center">843</td>
			<td class="red lasttd center">204</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641493,0" class="icommentjs kaButton smallButton rightButton" href="/va-natural-deep-house-2015-mp3-320-kbps-t11641493.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-natural-deep-house-2015-mp3-320-kbps-t11641493.html" class="cellMainLink">VA - Natural Deep House (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1383174503">1.29 <span>GB</span></td>
			<td class="center">103</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:40:25+00:00">23 Nov 2015, 18:40:25</span></td>
			<td class="green center">503</td>
			<td class="red lasttd center">219</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651297,0" class="icommentjs kaButton smallButton rightButton" href="/adele-hello-marshmello-remix-mp3-edm-rg-t11651297.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/adele-hello-marshmello-remix-mp3-edm-rg-t11651297.html" class="cellMainLink">Adele - HeLLo (Marshmello Remix).mp3 [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="10111037">9.64 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T12:49:07+00:00">25 Nov 2015, 12:49:07</span></td>
			<td class="green center">561</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11658845,0" class="icommentjs kaButton smallButton rightButton" href="/lil-wayne-no-ceilings-2-320kbps-2015-official-mixjoint-t11658845.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/lil-wayne-no-ceilings-2-320kbps-2015-official-mixjoint-t11658845.html" class="cellMainLink">Lil Wayne - No Ceilings 2 [320Kbps] [2015] [Official] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="220390892">210.18 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-27T01:37:10+00:00">27 Nov 2015, 01:37:10</span></td>
			<td class="green center">454</td>
			<td class="red lasttd center">241</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651778,0" class="icommentjs kaButton smallButton rightButton" href="/frank-sinatra-the-100th-birthday-swing-album-2015-mp3-320kbps-h4ckus-glodls-t11651778.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/frank-sinatra-the-100th-birthday-swing-album-2015-mp3-320kbps-h4ckus-glodls-t11651778.html" class="cellMainLink">Frank Sinatra - The 100th Birthday Swing Album [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="584505318">557.43 <span>MB</span></td>
			<td class="center">94</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T15:11:44+00:00">25 Nov 2015, 15:11:44</span></td>
			<td class="green center">496</td>
			<td class="red lasttd center">125</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655822,0" class="icommentjs kaButton smallButton rightButton" href="/va-beatport-top-100-progressive-house-november-2015-mp3-320-kbps-t11655822.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-beatport-top-100-progressive-house-november-2015-mp3-320-kbps-t11655822.html" class="cellMainLink">VA - Beatport Top 100 Progressive House November (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1359581821">1.27 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T10:04:15+00:00">26 Nov 2015, 10:04:15</span></td>
			<td class="green center">326</td>
			<td class="red lasttd center">267</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11659034,0" class="icommentjs kaButton smallButton rightButton" href="/mp3-new-releases-2015-week-47-amazeballs-glodls-t11659034.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-47-amazeballs-glodls-t11659034.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 47 - AMAZEBALLS [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4503858493">4.19 <span>GB</span></td>
			<td class="center">591</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-27T02:59:03+00:00">27 Nov 2015, 02:59:03</span></td>
			<td class="green center">154</td>
			<td class="red lasttd center">604</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645111,0" class="icommentjs kaButton smallButton rightButton" href="/va-classical-voices-the-musicals-2015-320kpbs-freak37-t11645111.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-classical-voices-the-musicals-2015-320kpbs-freak37-t11645111.html" class="cellMainLink">VA - Classical Voices The Musicals - (2015) [320kpbs ] - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="388639182">370.64 <span>MB</span></td>
			<td class="center">48</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T10:30:36+00:00">24 Nov 2015, 10:30:36</span></td>
			<td class="green center">414</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641417,0" class="icommentjs kaButton smallButton rightButton" href="/va-autumn-festival-100-club-house-2015-mp3-320-kbps-t11641417.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-autumn-festival-100-club-house-2015-mp3-320-kbps-t11641417.html" class="cellMainLink">VA - Autumn Festival: 100 Club House (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1232357030">1.15 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:20:17+00:00">23 Nov 2015, 18:20:17</span></td>
			<td class="green center">351</td>
			<td class="red lasttd center">148</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655874,0" class="icommentjs kaButton smallButton rightButton" href="/va-ibiza-easy-listening-2015-mp3-320-kbps-t11655874.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ibiza-easy-listening-2015-mp3-320-kbps-t11655874.html" class="cellMainLink">Va - Ibiza Easy Listening (2015) [MP3 320 Kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="323316393">308.34 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T10:18:22+00:00">26 Nov 2015, 10:18:22</span></td>
			<td class="green center">228</td>
			<td class="red lasttd center">100</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11652751,0" class="icommentjs kaButton smallButton rightButton" href="/the-drifters-stand-by-me-the-very-best-of-2015-freak37-t11652751.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-drifters-stand-by-me-the-very-best-of-2015-freak37-t11652751.html" class="cellMainLink">The Drifters-Stand By Me The Very Best of-2015...Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="183277001">174.79 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T18:49:20+00:00">25 Nov 2015, 18:49:20</span></td>
			<td class="green center">250</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11657593,0" class="icommentjs kaButton smallButton rightButton" href="/the-corrs-white-light-2015-320kbps-pirate-shovon-t11657593.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-corrs-white-light-2015-320kbps-pirate-shovon-t11657593.html" class="cellMainLink">The Corrs - White Light [2015] [320Kbps] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="116382732">110.99 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T18:35:51+00:00">26 Nov 2015, 18:35:51</span></td>
			<td class="green center">238</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11650574,0" class="icommentjs kaButton smallButton rightButton" href="/va-superbomb-club-music-pack-49-2015-mp3-320-kbps-t11650574.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-superbomb-club-music-pack-49-2015-mp3-320-kbps-t11650574.html" class="cellMainLink">VA - SuperBomb Club Music Pack 49 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="520180687">496.08 <span>MB</span></td>
			<td class="center">42</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T09:30:27+00:00">25 Nov 2015, 09:30:27</span></td>
			<td class="green center">205</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11644468,0" class="icommentjs kaButton smallButton rightButton" href="/various-artists-love-lyric-blues-2015-t11644468.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-love-lyric-blues-2015-t11644468.html" class="cellMainLink">Various Artists - Love Lyric Blues (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="1133988560">1.06 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T07:27:01+00:00">24 Nov 2015, 07:27:01</span></td>
			<td class="green center">181</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641464,0" class="icommentjs kaButton smallButton rightButton" href="/va-bermuda-island-uplifting-vocal-trance-2015-mp3-320-kbps-t11641464.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-bermuda-island-uplifting-vocal-trance-2015-mp3-320-kbps-t11641464.html" class="cellMainLink">VA - Bermuda Island: Uplifting Vocal Trance (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1409842749">1.31 <span>GB</span></td>
			<td class="center">103</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:32:26+00:00">23 Nov 2015, 18:32:26</span></td>
			<td class="green center">168</td>
			<td class="red lasttd center">114</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11642169,0" class="icommentjs kaButton smallButton rightButton" href="/starcraft-ii-legacy-of-the-void-reloaded-t11642169.html#comment">221 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/starcraft-ii-legacy-of-the-void-reloaded-t11642169.html" class="cellMainLink">StarCraft II Legacy of the Void-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="20951470549">19.51 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T21:25:13+00:00">23 Nov 2015, 21:25:13</span></td>
			<td class="green center">910</td>
			<td class="red lasttd center">2403</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11653838,0" class="icommentjs kaButton smallButton rightButton" href="/saint-seiya-soldiers-soul-codex-t11653838.html#comment">51 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/saint-seiya-soldiers-soul-codex-t11653838.html" class="cellMainLink">Saint Seiya Soldiers Soul-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="4112782816">3.83 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T23:12:04+00:00">25 Nov 2015, 23:12:04</span></td>
			<td class="green center">1079</td>
			<td class="red lasttd center">1045</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11644353,0" class="icommentjs kaButton smallButton rightButton" href="/starcraft-2-legacy-of-the-void-2015-battle-rip-reloaded-t11644353.html#comment">57 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/starcraft-2-legacy-of-the-void-2015-battle-rip-reloaded-t11644353.html" class="cellMainLink">StarCraft 2: Legacy of the Void [2015] (Battle-Rip)- RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="25488672600">23.74 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T06:51:31+00:00">24 Nov 2015, 06:51:31</span></td>
			<td class="green center">633</td>
			<td class="red lasttd center">559</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11646297,0" class="icommentjs kaButton smallButton rightButton" href="/fallout-4-update-2-2015-pc-repack-by-r-g-freedom-t11646297.html#comment">44 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/fallout-4-update-2-2015-pc-repack-by-r-g-freedom-t11646297.html" class="cellMainLink">Fallout 4 [Update 2] (2015) PC | RePack by R.G. Freedom</a></div>
			</td>
			<td class="nobr center" data-sort="20392777657">18.99 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T15:40:43+00:00">24 Nov 2015, 15:40:43</span></td>
			<td class="green center">573</td>
			<td class="red lasttd center">352</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11642216,0" class="icommentjs kaButton smallButton rightButton" href="/assassin-s-creed-syndicate-gold-edition-update-1-multi16-repack-r-g-mechanics-t11642216.html#comment">79 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/assassin-s-creed-syndicate-gold-edition-update-1-multi16-repack-r-g-mechanics-t11642216.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assassin-s-creed-syndicate-gold-edition-update-1-multi16-repack-r-g-mechanics-t11642216.html" class="cellMainLink">Assassin&#039;s Creed Syndicate [Gold Edition] [Update 1] [multi16] Repack-R.G.Mechanics</a></div>
			</td>
			<td class="nobr center" data-sort="22364385521">20.83 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T21:36:56+00:00">23 Nov 2015, 21:36:56</span></td>
			<td class="green center">184</td>
			<td class="red lasttd center">601</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11652423,0" class="icommentjs kaButton smallButton rightButton" href="/just-cause-3-steam-preload-mercs213-t11652423.html#comment">65 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/just-cause-3-steam-preload-mercs213-t11652423.html" class="cellMainLink">Just Cause 3 Steam Preload-MERCS213</a></div>
			</td>
			<td class="nobr center" data-sort="41328817862">38.49 <span>GB</span></td>
			<td class="center">88</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T17:33:38+00:00">25 Nov 2015, 17:33:38</span></td>
			<td class="green center">110</td>
			<td class="red lasttd center">685</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645532,0" class="icommentjs kaButton smallButton rightButton" href="/mystery-case-files-13-ravenhearst-unlocked-collector-s-edition-asg-t11645532.html#comment">30 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/mystery-case-files-13-ravenhearst-unlocked-collector-s-edition-asg-t11645532.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mystery-case-files-13-ravenhearst-unlocked-collector-s-edition-asg-t11645532.html" class="cellMainLink">Mystery Case Files - 13 Ravenhearst Unlocked Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="950299434">906.28 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T12:20:36+00:00">24 Nov 2015, 12:20:36</span></td>
			<td class="green center">322</td>
			<td class="red lasttd center">98</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11647721,0" class="icommentjs kaButton smallButton rightButton" href="/minecraft-story-mode-episode-3-codex-t11647721.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/minecraft-story-mode-episode-3-codex-t11647721.html" class="cellMainLink">Minecraft Story Mode Episode 3-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="2091325165">1.95 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T18:08:17+00:00">24 Nov 2015, 18:08:17</span></td>
			<td class="green center">255</td>
			<td class="red lasttd center">111</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649735,0" class="icommentjs kaButton smallButton rightButton" href="/game-of-thrones-a-telltale-games-series-episode-1-6-2014-pc-repack-by-r-g-catalyst-t11649735.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/game-of-thrones-a-telltale-games-series-episode-1-6-2014-pc-repack-by-r-g-catalyst-t11649735.html" class="cellMainLink">Game of Thrones - A Telltale Games Series. Episode 1-6 (2014) PC | RePack by R.G. Catalyst</a></div>
			</td>
			<td class="nobr center" data-sort="5533120075">5.15 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T05:59:23+00:00">25 Nov 2015, 05:59:23</span></td>
			<td class="green center">225</td>
			<td class="red lasttd center">156</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648354,0" class="icommentjs kaButton smallButton rightButton" href="/stronghold-crusader-2-incl-4-dlc-gog-t11648354.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/stronghold-crusader-2-incl-4-dlc-gog-t11648354.html" class="cellMainLink">Stronghold Crusader 2 (Incl 4 DLC) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="3970691676">3.7 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T20:39:45+00:00">24 Nov 2015, 20:39:45</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">116</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649759,0" class="icommentjs kaButton smallButton rightButton" href="/might-and-magic-heroes-vii-deluxe-edition-v-1-50-2015-pc-repack-by-r-g-catalyst-t11649759.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/might-and-magic-heroes-vii-deluxe-edition-v-1-50-2015-pc-repack-by-r-g-catalyst-t11649759.html" class="cellMainLink">Might and Magic Heroes VII: Deluxe Edition [v 1.50] (2015) PC | RePack by R.G. Catalyst</a></div>
			</td>
			<td class="nobr center" data-sort="9604326806">8.94 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T06:04:10+00:00">25 Nov 2015, 06:04:10</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11656610,0" class="icommentjs kaButton smallButton rightButton" href="/call-of-duty-black-ops-iii-update-3-reloaded-t11656610.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/call-of-duty-black-ops-iii-update-3-reloaded-t11656610.html" class="cellMainLink">Call of Duty Black Ops III Update 3-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="557471216">531.65 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T14:14:42+00:00">26 Nov 2015, 14:14:42</span></td>
			<td class="green center">123</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655486,0" class="icommentjs kaButton smallButton rightButton" href="/the-curio-society-eclipse-over-mesina-collector-s-edition-asg-t11655486.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-curio-society-eclipse-over-mesina-collector-s-edition-asg-t11655486.html" class="cellMainLink">The Curio Society - Eclipse Over Mesina Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="1386992797">1.29 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T08:29:11+00:00">26 Nov 2015, 08:29:11</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651808,0" class="icommentjs kaButton smallButton rightButton" href="/payday-2-pc-full-game-repack-v-1431-nosteam-t11651808.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/payday-2-pc-full-game-repack-v-1431-nosteam-t11651808.html" class="cellMainLink">PAYDAY 2 PC full game repack v_1431 ^^nosTEAM^^</a></div>
			</td>
			<td class="nobr center" data-sort="13794224390">12.85 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T15:17:30+00:00">25 Nov 2015, 15:17:30</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">191</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648446,0" class="icommentjs kaButton smallButton rightButton" href="/handball-16-xbox360-complex-t11648446.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/handball-16-xbox360-complex-t11648446.html" class="cellMainLink">Handball.16.XBOX360-COMPLEX</a></div>
			</td>
			<td class="nobr center" data-sort="6055728592">5.64 <span>GB</span></td>
			<td class="center">63</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T21:05:59+00:00">24 Nov 2015, 21:05:59</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">78</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645829,0" class="icommentjs kaButton smallButton rightButton" href="/winrar-5-30-final-incl-crack-techtools-t11645829.html#comment">53 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/winrar-5-30-final-incl-crack-techtools-t11645829.html" class="cellMainLink">WinRAR 5.30 FINAL Incl. Crack [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="5577060">5.32 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T13:21:03+00:00">24 Nov 2015, 13:21:03</span></td>
			<td class="green center">554</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648941,0" class="icommentjs kaButton smallButton rightButton" href="/iobit-advanced-systemcare-pro-9-0-3-1078-key-4realtorrentz-t11648941.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/iobit-advanced-systemcare-pro-9-0-3-1078-key-4realtorrentz-t11648941.html" class="cellMainLink">IObit advanced SystemCare Pro 9.0.3.1078 + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="39547105">37.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T01:06:59+00:00">25 Nov 2015, 01:06:59</span></td>
			<td class="green center">256</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645977,0" class="icommentjs kaButton smallButton rightButton" href="/ccleaner-5-12-5431-2015-pc-portable-t11645977.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ccleaner-5-12-5431-2015-pc-portable-t11645977.html" class="cellMainLink">CCleaner 5.12.5431 (2015) PC | + Portable</a></div>
			</td>
			<td class="nobr center" data-sort="24978001">23.82 <span>MB</span></td>
			<td class="center">60</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T14:11:03+00:00">24 Nov 2015, 14:11:03</span></td>
			<td class="green center">231</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648608,0" class="icommentjs kaButton smallButton rightButton" href="/output-sounds-collection-rev-signal-rev-x-loops-exhale-all-expansions-oddsox-t11648608.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/output-sounds-collection-rev-signal-rev-x-loops-exhale-all-expansions-oddsox-t11648608.html" class="cellMainLink">Output Sounds Collection - REV, Signal, REV X-Loops, Exhale, All Expansions [oddsox]</a></div>
			</td>
			<td class="nobr center" data-sort="36937354489">34.4 <span>GB</span></td>
			<td class="center">58</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T22:18:25+00:00">24 Nov 2015, 22:18:25</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">310</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11654227,0" class="icommentjs kaButton smallButton rightButton" href="/easeus-data-recovery-wizard-9-8-0-technician-x32-x64-multi-crack-t11654227.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/easeus-data-recovery-wizard-9-8-0-technician-x32-x64-multi-crack-t11654227.html" class="cellMainLink">EaseUS Data Recovery Wizard 9.8.0 Technician (x32/x64][Multi][Crack]</a></div>
			</td>
			<td class="nobr center" data-sort="18118344">17.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T00:32:26+00:00">26 Nov 2015, 00:32:26</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11658871,0" class="icommentjs kaButton smallButton rightButton" href="/windows-repair-3-7-0-pro-key-4realtorrentz-t11658871.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-repair-3-7-0-pro-key-4realtorrentz-t11658871.html" class="cellMainLink">Windows Repair 3.7.0 Pro + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="20857517">19.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-27T01:51:18+00:00">27 Nov 2015, 01:51:18</span></td>
			<td class="green center">137</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649367,0" class="icommentjs kaButton smallButton rightButton" href="/daz3d-poser-selina-bundle-t11649367.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-poser-selina-bundle-t11649367.html" class="cellMainLink">DAZ3D - Poser Selina Bundle</a></div>
			</td>
			<td class="nobr center" data-sort="379822311">362.23 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T04:05:18+00:00">25 Nov 2015, 04:05:18</span></td>
			<td class="green center">92</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645041,0" class="icommentjs kaButton smallButton rightButton" href="/drive-snapshot-1-43-0-17827-portable-key-4realtorrentz-t11645041.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/drive-snapshot-1-43-0-17827-portable-key-4realtorrentz-t11645041.html" class="cellMainLink">Drive SnapShot 1.43.0.17827 + Portable + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="4859089">4.63 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T10:19:11+00:00">24 Nov 2015, 10:19:11</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648387,0" class="icommentjs kaButton smallButton rightButton" href="/daz3d-the-hero-and-the-damsel-poses-g2m-g2f-t11648387.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-the-hero-and-the-damsel-poses-g2m-g2f-t11648387.html" class="cellMainLink">Daz3D - The Hero and the Damsel Poses (G2M, G2F)</a></div>
			</td>
			<td class="nobr center" data-sort="2088843">1.99 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T20:47:39+00:00">24 Nov 2015, 20:47:39</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/actual-multiple-monitors-8-6-2-crack-4realtorrentz-t11649021.html" class="cellMainLink">Actual Multiple Monitors 8.6.2 + Crack [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="12768060">12.18 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T01:43:30+00:00">25 Nov 2015, 01:43:30</span></td>
			<td class="green center">49</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-bat-professional-7-0-0-56-2015-pc-repack-by-kpojiuk-t11650554.html" class="cellMainLink">The Bat! Professional 7.0.0.56 (2015) PC | RePack by KpoJIuk</a></div>
			</td>
			<td class="nobr center" data-sort="16134479">15.39 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T09:25:43+00:00">25 Nov 2015, 09:25:43</span></td>
			<td class="green center">49</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11650991,0" class="icommentjs kaButton smallButton rightButton" href="/wondershare-filmora-6-8-0-15-final-serials-b4tman-t11650991.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/wondershare-filmora-6-8-0-15-final-serials-b4tman-t11650991.html" class="cellMainLink">Wondershare Filmora 6.8.0.15 Final + Serials {B4tman}</a></div>
			</td>
			<td class="nobr center" data-sort="112107089">106.91 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T11:19:55+00:00">25 Nov 2015, 11:19:55</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11656943,0" class="icommentjs kaButton smallButton rightButton" href="/adobe-acrobat-pro-dc-2015-009-20079-multilingual-rocky-45-cpul-t11656943.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-acrobat-pro-dc-2015-009-20079-multilingual-rocky-45-cpul-t11656943.html" class="cellMainLink">Adobe Acrobat Pro DC 2015.009.20079 Multilingual - Rocky_45 [CPUL]</a></div>
			</td>
			<td class="nobr center" data-sort="712662324">679.65 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T15:47:06+00:00">26 Nov 2015, 15:47:06</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/xfer-records-serum-1-07b4-vsti-aax-x86-x64-r4e-t11648183.html" class="cellMainLink">Xfer Records Serum 1.07b4 VSTi AAX x86 x64-r4e</a></div>
			</td>
			<td class="nobr center" data-sort="2885909929">2.69 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T19:44:02+00:00">24 Nov 2015, 19:44:02</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11646205,0" class="icommentjs kaButton smallButton rightButton" href="/poser-daz3d-city-unseelie-for-g2f-19063-t11646205.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/poser-daz3d-city-unseelie-for-g2f-19063-t11646205.html" class="cellMainLink">Poser - Daz3D - City Unseelie for G2F 19063</a></div>
			</td>
			<td class="nobr center" data-sort="133955525">127.75 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T15:16:10+00:00">24 Nov 2015, 15:16:10</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">3</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11656051,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-naruto-shippuuden-439-720p-mkv-t11656051.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-naruto-shippuuden-439-720p-mkv-t11656051.html" class="cellMainLink">[HorribleSubs] Naruto Shippuuden - 439 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="330339562">315.04 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T11:10:02+00:00">26 Nov 2015, 11:10:02</span></td>
			<td class="green center">519</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-lupin-sansei-2015-09-raw-ntv-1280x720-x264-aac-mp4-t11659659.html" class="cellMainLink">[Leopard-Raws] Lupin Sansei (2015) - 09 RAW (NTV 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="333549559">318.1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-27T06:15:02+00:00">27 Nov 2015, 06:15:02</span></td>
			<td class="green center">248</td>
			<td class="red lasttd center">221</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11656178,0" class="icommentjs kaButton smallButton rightButton" href="/naruto-shippuden-episode-439-english-subbed-720p-arizone-t11656178.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-episode-439-english-subbed-720p-arizone-t11656178.html" class="cellMainLink">Naruto Shippuden Episode 439 [ENGLISH SUBBED] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="138992835">132.55 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T11:49:04+00:00">26 Nov 2015, 11:49:04</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11646421,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-hagure-yuusha-no-aesthetica-complete-uncensored-dual-audio-bd-1280x720-x264-aac-scavvykid-t11646421.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-hagure-yuusha-no-aesthetica-complete-uncensored-dual-audio-bd-1280x720-x264-aac-scavvykid-t11646421.html" class="cellMainLink">[AnimeRG] Hagure Yuusha no Aesthetica Complete [Uncensored] [Dual Audio] [BD 1280x720 x264 AAC] [ScavvyKiD]</a></div>
			</td>
			<td class="nobr center" data-sort="2668079877">2.48 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T16:13:01+00:00">24 Nov 2015, 16:13:01</span></td>
			<td class="green center">137</td>
			<td class="red lasttd center">194</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11656182,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-naruto-shippuuden-439-english-subbed-480p-kami-t11656182.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-naruto-shippuuden-439-english-subbed-480p-kami-t11656182.html" class="cellMainLink">[AnimeRG] Naruto Shippuuden - 439 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="62978749">60.06 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T11:49:53+00:00">26 Nov 2015, 11:49:53</span></td>
			<td class="green center">121</td>
			<td class="red lasttd center">56</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645490,0" class="icommentjs kaButton smallButton rightButton" href="/shepardtds-k-project-dual-audio-720p-8-bit-x265-hevc-1-13-complete-t11645490.html#comment">7 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/shepardtds-k-project-dual-audio-720p-8-bit-x265-hevc-1-13-complete-t11645490.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/shepardtds-k-project-dual-audio-720p-8-bit-x265-hevc-1-13-complete-t11645490.html" class="cellMainLink">[ShepardTDS] K-Project Dual Audio [720p 8-bit x265 HEVC] 1-13 Complete</a></div>
			</td>
			<td class="nobr center" data-sort="3424230629">3.19 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T12:02:46+00:00">24 Nov 2015, 12:02:46</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-akuma-no-riddle-riddle-story-of-devil-1-12-english-dubbed-720p-sehjada-t11654755.html" class="cellMainLink">[ARRG] Akuma No Riddle (Riddle Story of Devil) - (1-12) English Dubbed 720P (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="1120509624">1.04 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T04:40:10+00:00">26 Nov 2015, 04:40:10</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11646486,0" class="icommentjs kaButton smallButton rightButton" href="/ipunisher-monster-musume-no-iru-nichijou-vol-3-bd-1280x720-h264-10bit-aac-uncensored-t11646486.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-monster-musume-no-iru-nichijou-vol-3-bd-1280x720-h264-10bit-aac-uncensored-t11646486.html" class="cellMainLink">[iPUNISHER] Monster Musume no Iru Nichijou Vol.3 (BD 1280x720 h264 10bit AAC UNCENSORED)</a></div>
			</td>
			<td class="nobr center" data-sort="615898532">587.37 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T16:29:15+00:00">24 Nov 2015, 16:29:15</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11653394,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-dragon-ball-super-020-480p-kami-mkv-t11653394.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-020-480p-kami-mkv-t11653394.html" class="cellMainLink">[AnimeRG] Dragon Ball Super 020 - [480p] [KaMi].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="56723742">54.1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T21:52:43+00:00">25 Nov 2015, 21:52:43</span></td>
			<td class="green center">60</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11650138,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-prison-school-live-action-05-english-subbed-720p-sehjada-t11650138.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-prison-school-live-action-05-english-subbed-720p-sehjada-t11650138.html" class="cellMainLink">[ARRG] Prison School (LIVE ACTION) - 05 English Subbed 720P (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="102619947">97.87 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T07:33:38+00:00">25 Nov 2015, 07:33:38</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655881,0" class="icommentjs kaButton smallButton rightButton" href="/fff-shingeki-no-kyojin-bd-1080p-flac-t11655881.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-shingeki-no-kyojin-bd-1080p-flac-t11655881.html" class="cellMainLink">[FFF] Shingeki no Kyojin [BD][1080p-FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="57829327210">53.86 <span>GB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T10:20:02+00:00">26 Nov 2015, 10:20:02</span></td>
			<td class="green center">19</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-taimadou-gakuen-35-shiken-shoutai-08-720p-aac-mp4-t11653237.html" class="cellMainLink">[BakedFish] Taimadou Gakuen 35 Shiken Shoutai - 08 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="330882773">315.55 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T20:59:04+00:00">25 Nov 2015, 20:59:04</span></td>
			<td class="green center">26</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-naruto-shippuuden-439-720p-aac-mp4-t11656158.html" class="cellMainLink">[DeadFish] Naruto Shippuuden - 439 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="313786475">299.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T11:42:38+00:00">26 Nov 2015, 11:42:38</span></td>
			<td class="green center">24</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-teekyuu-68-6df1c062-mkv-t11641916.html" class="cellMainLink">[Commie] Teekyuu - 68 [6DF1C062].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="48904641">46.64 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T20:25:02+00:00">23 Nov 2015, 20:25:02</span></td>
			<td class="green center">26</td>
			<td class="red lasttd center">0</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-dakara-boku-wa-h-ga-dekinai-bd-720p-aac-t11655553.html" class="cellMainLink">[FFF] Dakara Boku wa, H ga Dekinai [BD][720p-AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="6436947833">5.99 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T08:55:03+00:00">26 Nov 2015, 08:55:03</span></td>
			<td class="green center">9</td>
			<td class="red lasttd center">27</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655340,0" class="icommentjs kaButton smallButton rightButton" href="/marvel-week-11-25-2015-nem-t11655340.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-11-25-2015-nem-t11655340.html" class="cellMainLink">Marvel Week+ (11-25-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="589740058">562.42 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T07:34:06+00:00">26 Nov 2015, 07:34:06</span></td>
			<td class="green center">778</td>
			<td class="red lasttd center">309</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11655209,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-november-26-2015-true-pdf-t11655209.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-november-26-2015-true-pdf-t11655209.html" class="cellMainLink">Assorted Magazines Bundle - November 26 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="640633144">610.96 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T07:07:52+00:00">26 Nov 2015, 07:07:52</span></td>
			<td class="green center">542</td>
			<td class="red lasttd center">463</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11654208,0" class="icommentjs kaButton smallButton rightButton" href="/dc-week-11-25-2015-aka-dc-you-week-26-nem-t11654208.html#comment">32 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-11-25-2015-aka-dc-you-week-26-nem-t11654208.html" class="cellMainLink">DC Week+ (11-25-2015) (aka DC YOU Week 26) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="878878774">838.16 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T00:19:32+00:00">26 Nov 2015, 00:19:32</span></td>
			<td class="green center">621</td>
			<td class="red lasttd center">299</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651567,0" class="icommentjs kaButton smallButton rightButton" href="/dark-knight-iii-the-master-race-001-2016-digital-zone-empire-cbr-nem-t11651567.html#comment">30 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dark-knight-iii-the-master-race-001-2016-digital-zone-empire-cbr-nem-t11651567.html" class="cellMainLink">Dark Knight III - The Master Race 001 (2016) (Digital) (Zone-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="64731515">61.73 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T14:13:53+00:00">25 Nov 2015, 14:13:53</span></td>
			<td class="green center">656</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641281,0" class="icommentjs kaButton smallButton rightButton" href="/the-telecommunications-handbook-engineering-guidelines-for-fixed-mobile-and-satellite-systems-1st-edition-2015-pdf-gooner-t11641281.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-telecommunications-handbook-engineering-guidelines-for-fixed-mobile-and-satellite-systems-1st-edition-2015-pdf-gooner-t11641281.html" class="cellMainLink">The Telecommunications Handbook - Engineering Guidelines for Fixed, Mobile and Satellite Systems - 1st Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="14366502">13.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:02:41+00:00">23 Nov 2015, 18:02:41</span></td>
			<td class="green center">422</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645165,0" class="icommentjs kaButton smallButton rightButton" href="/it-s-not-about-the-shark-how-to-solve-unsolvable-problems-pdf-mobi-t11645165.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/it-s-not-about-the-shark-how-to-solve-unsolvable-problems-pdf-mobi-t11645165.html" class="cellMainLink">It&#039;s Not About the Shark How to Solve Unsolvable Problems (PDF, MOBI)</a></div>
			</td>
			<td class="nobr center" data-sort="1298234">1.24 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T10:41:16+00:00">24 Nov 2015, 10:41:16</span></td>
			<td class="green center">361</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641849,0" class="icommentjs kaButton smallButton rightButton" href="/the-postage-stamp-vegetable-garden-grow-tons-of-organic-vegetables-in-tiny-spaces-and-containers-2015-epub-gooner-t11641849.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-postage-stamp-vegetable-garden-grow-tons-of-organic-vegetables-in-tiny-spaces-and-containers-2015-epub-gooner-t11641849.html" class="cellMainLink">The Postage Stamp Vegetable Garden - Grow Tons of Organic Vegetables in Tiny Spaces and Containers (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="9179987">8.75 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T20:11:04+00:00">23 Nov 2015, 20:11:04</span></td>
			<td class="green center">314</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649746,0" class="icommentjs kaButton smallButton rightButton" href="/microsoft-excel-2016-formulas-and-functions-includes-content-update-program-epub-ertb-t11649746.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/microsoft-excel-2016-formulas-and-functions-includes-content-update-program-epub-ertb-t11649746.html" class="cellMainLink">Microsoft Excel 2016 Formulas and Functions (includes Content Update Program) [ePUB] {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="34687833">33.08 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T06:01:10+00:00">25 Nov 2015, 06:01:10</span></td>
			<td class="green center">277</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651783,0" class="icommentjs kaButton smallButton rightButton" href="/all-new-wolverine-002-2016-digital-empire-cbr-t11651783.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/all-new-wolverine-002-2016-digital-empire-cbr-t11651783.html" class="cellMainLink">All-New Wolverine 002 (2016) (Digital-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="33473303">31.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T15:13:27+00:00">25 Nov 2015, 15:13:27</span></td>
			<td class="green center">218</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651697,0" class="icommentjs kaButton smallButton rightButton" href="/saga-031-2015-digital-minutemen-spaztastic-cbr-nem-t11651697.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/saga-031-2015-digital-minutemen-spaztastic-cbr-nem-t11651697.html" class="cellMainLink">Saga 031 (2015) (digital) (Minutemen-Spaztastic).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="33903977">32.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T14:49:34+00:00">25 Nov 2015, 14:49:34</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11652656,0" class="icommentjs kaButton smallButton rightButton" href="/venom-space-knight-001-2016-digital-zone-empire-cbr-t11652656.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/venom-space-knight-001-2016-digital-zone-empire-cbr-t11652656.html" class="cellMainLink">Venom - Space Knight 001 (2016) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="33448227">31.9 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T18:30:07+00:00">25 Nov 2015, 18:30:07</span></td>
			<td class="green center">200</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651796,0" class="icommentjs kaButton smallButton rightButton" href="/darth-vader-013-2016-digital-blackmanta-empire-cbr-t11651796.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/darth-vader-013-2016-digital-blackmanta-empire-cbr-t11651796.html" class="cellMainLink">Darth Vader 013 (2016) (Digital) (BlackManta-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="35522543">33.88 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T15:15:16+00:00">25 Nov 2015, 15:15:16</span></td>
			<td class="green center">197</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649810,0" class="icommentjs kaButton smallButton rightButton" href="/0-day-week-of-2015-11-18-t11649810.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-11-18-t11649810.html" class="cellMainLink">0-Day Week of 2015.11.18</a></div>
			</td>
			<td class="nobr center" data-sort="7224598430">6.73 <span>GB</span></td>
			<td class="center">149</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T06:15:12+00:00">25 Nov 2015, 06:15:12</span></td>
			<td class="green center">126</td>
			<td class="red lasttd center">117</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11652661,0" class="icommentjs kaButton smallButton rightButton" href="/silk-001-2016-digital-zone-empire-cbr-t11652661.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/silk-001-2016-digital-zone-empire-cbr-t11652661.html" class="cellMainLink">Silk 001 (2016) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="43268033">41.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T18:30:53+00:00">25 Nov 2015, 18:30:53</span></td>
			<td class="green center">161</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11652709,0" class="icommentjs kaButton smallButton rightButton" href="/guardians-of-the-galaxy-002-2016-digital-zone-empire-cbr-t11652709.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/guardians-of-the-galaxy-002-2016-digital-zone-empire-cbr-t11652709.html" class="cellMainLink">Guardians of the Galaxy 002 (2016) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="43987211">41.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T18:40:06+00:00">25 Nov 2015, 18:40:06</span></td>
			<td class="green center">148</td>
			<td class="red lasttd center">9</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641500,0" class="icommentjs kaButton smallButton rightButton" href="/frank-sinatra-a-jolly-christmas-from-frank-sinatra-2015-24-96-hd-flac-t11641500.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/frank-sinatra-a-jolly-christmas-from-frank-sinatra-2015-24-96-hd-flac-t11641500.html" class="cellMainLink">Frank Sinatra - A Jolly Christmas From Frank Sinatra (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="369570344">352.45 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:43:06+00:00">23 Nov 2015, 18:43:06</span></td>
			<td class="green center">256</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11642139,0" class="icommentjs kaButton smallButton rightButton" href="/the-who-tommy-super-deluxe-2014-24-96-hd-flac-t11642139.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-tommy-super-deluxe-2014-24-96-hd-flac-t11642139.html" class="cellMainLink">The Who - Tommy (Super Deluxe 2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="5615599402">5.23 <span>GB</span></td>
			<td class="center">182</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T21:15:27+00:00">23 Nov 2015, 21:15:27</span></td>
			<td class="green center">130</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11656001,0" class="icommentjs kaButton smallButton rightButton" href="/ella-fitzgerald-louis-armstrong-ella-and-louis-again-2014-24-192-hd-flac-t11656001.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ella-fitzgerald-louis-armstrong-ella-and-louis-again-2014-24-192-hd-flac-t11656001.html" class="cellMainLink">Ella Fitzgerald &amp; Louis Armstrong - Ella and Louis Again (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1930167844">1.8 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T10:58:29+00:00">26 Nov 2015, 10:58:29</span></td>
			<td class="green center">129</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11650215,0" class="icommentjs kaButton smallButton rightButton" href="/ray-conniff-christmas-caroling-1985-cd-flac-urbin4hd-t11650215.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ray-conniff-christmas-caroling-1985-cd-flac-urbin4hd-t11650215.html" class="cellMainLink">RAY CONNIFF Christmas Caroling 1985 CD-FLAC URBiN4HD</a></div>
			</td>
			<td class="nobr center" data-sort="313260126">298.75 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T07:52:55+00:00">25 Nov 2015, 07:52:55</span></td>
			<td class="green center">129</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11650989,0" class="icommentjs kaButton smallButton rightButton" href="/bread-baby-i-m-a-want-you-2015-24-192-hd-flac-t11650989.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bread-baby-i-m-a-want-you-2015-24-192-hd-flac-t11650989.html" class="cellMainLink">Bread - Baby I&#039;m-a Want You (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1310411125">1.22 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T11:19:53+00:00">25 Nov 2015, 11:19:53</span></td>
			<td class="green center">110</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643509,0" class="icommentjs kaButton smallButton rightButton" href="/va-classical-voices-the-musicals-3cd-2015-flac-t11643509.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-classical-voices-the-musicals-3cd-2015-flac-t11643509.html" class="cellMainLink">VA - Classical Voices: The Musicals [3CD] (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="901603680">859.84 <span>MB</span></td>
			<td class="center">65</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T02:16:37+00:00">24 Nov 2015, 02:16:37</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11653706,0" class="icommentjs kaButton smallButton rightButton" href="/arlo-guthrie-alice-s-restaurant-1967-flac-t11653706.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/arlo-guthrie-alice-s-restaurant-1967-flac-t11653706.html" class="cellMainLink">Arlo Guthrie - Alice&#039;s Restaurant (1967) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="212596588">202.75 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T22:46:00+00:00">25 Nov 2015, 22:46:00</span></td>
			<td class="green center">102</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11647827,0" class="icommentjs kaButton smallButton rightButton" href="/the-who-live-in-hyde-park-2cd-2015-flac-t11647827.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-live-in-hyde-park-2cd-2015-flac-t11647827.html" class="cellMainLink">The Who - Live in Hyde Park [2CD] (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="865994922">825.88 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T18:30:25+00:00">24 Nov 2015, 18:30:25</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648059,0" class="icommentjs kaButton smallButton rightButton" href="/the-velvet-underground-the-complete-matrix-tapes-2015-flac-beolab1700-t11648059.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-velvet-underground-the-complete-matrix-tapes-2015-flac-beolab1700-t11648059.html" class="cellMainLink">The Velvet Underground - The Complete Matrix Tapes (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="1562194208">1.45 <span>GB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T19:14:38+00:00">24 Nov 2015, 19:14:38</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11652556,0" class="icommentjs kaButton smallButton rightButton" href="/the-who-the-who-sell-out-2015-24-96-hd-flac-t11652556.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-the-who-sell-out-2015-24-96-hd-flac-t11652556.html" class="cellMainLink">The Who - The Who Sell Out (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1947032061">1.81 <span>GB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T18:01:28+00:00">25 Nov 2015, 18:01:28</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643668,0" class="icommentjs kaButton smallButton rightButton" href="/anastacia-ultimate-collection-2015-flac-t11643668.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/anastacia-ultimate-collection-2015-flac-t11643668.html" class="cellMainLink">Anastacia - Ultimate Collection (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="547840058">522.46 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T03:16:50+00:00">24 Nov 2015, 03:16:50</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11658237,0" class="icommentjs kaButton smallButton rightButton" href="/jimi-hendrix-no-more-a-rolling-stone-sbd-2004-flac-t11658237.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jimi-hendrix-no-more-a-rolling-stone-sbd-2004-flac-t11658237.html" class="cellMainLink">Jimi Hendrix - No More A Rolling Stone (SBD 2004) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="488962152">466.31 <span>MB</span></td>
			<td class="center">42</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T22:05:40+00:00">26 Nov 2015, 22:05:40</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11657018,0" class="icommentjs kaButton smallButton rightButton" href="/the-who-my-generation-mono-2014-24-96-hd-flac-t11657018.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-my-generation-mono-2014-24-96-hd-flac-t11657018.html" class="cellMainLink">The Who - My Generation (Mono) (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="990076582">944.21 <span>MB</span></td>
			<td class="center">39</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T16:07:41+00:00">26 Nov 2015, 16:07:41</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11644060,0" class="icommentjs kaButton smallButton rightButton" href="/bobby-vinton-a-very-merry-christmas-1995-cd-flac-urbin4hd-t11644060.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bobby-vinton-a-very-merry-christmas-1995-cd-flac-urbin4hd-t11644060.html" class="cellMainLink">BOBBY VINTON A Very Merry Christmas 1995 CD-FLAC URBiN4HD</a></div>
			</td>
			<td class="nobr center" data-sort="188267808">179.55 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T05:20:23+00:00">24 Nov 2015, 05:20:23</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11658245,0" class="icommentjs kaButton smallButton rightButton" href="/a-very-special-christmas-flac-discography-10-albums-t11658245.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/a-very-special-christmas-flac-discography-10-albums-t11658245.html" class="cellMainLink">A Very Special Christmas FLAC Discography - 10 Albums</a></div>
			</td>
			<td class="nobr center" data-sort="3682992699">3.43 <span>GB</span></td>
			<td class="center">180</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-26T22:08:08+00:00">26 Nov 2015, 22:08:08</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">58</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <span  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></span>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <span  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></span>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-tv-show-are-you-watching-right-now-v4/?unread=17144767">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What TV show are you watching right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/Vampire/">Vampire</a></span></span> <time class="timeago" datetime="2015-11-27T17:57:30+00:00">27 Nov 2015, 17:57</time></span>
	</li>
		<li>
		<a href="/community/show/anime-quiz-thread-118888/?unread=17144766">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Anime Quiz!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/Matrix/">Matrix</a></span></span> <time class="timeago" datetime="2015-11-27T17:57:16+00:00">27 Nov 2015, 17:57</time></span>
	</li>
		<li>
		<a href="/community/show/kat-s-problems/?unread=17144764">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				KAT&#039;s Problems
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_3"><a class="plain" href="/user/Sr.Dr4cuLa/">Sr.Dr4cuLa</a></span></span> <time class="timeago" datetime="2015-11-27T17:56:59+00:00">27 Nov 2015, 17:56</time></span>
	</li>
		<li>
		<a href="/community/show/deep-60-s-music-releases/?unread=17144751">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				deep-60&#039;s Music Releases
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_eliteuploader"><a class="plain" href="/user/deep__60/">deep__60</a></span></span> <time class="timeago" datetime="2015-11-27T17:53:39+00:00">27 Nov 2015, 17:53</time></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v5-thread-116026/?unread=17144747">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V5
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_4"><a class="plain" href="/user/Bartacus/">Bartacus</a></span></span> <time class="timeago" datetime="2015-11-27T17:52:33+00:00">27 Nov 2015, 17:52</time></span>
	</li>
		<li>
		<a href="/community/show/dvd-screeners-your-consideration-and-discussion-2014-2015/?unread=17144739">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				DVD Screeners &quot;For Your Consideration&quot; and Discussion (2014 - 2015)
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Blaker123/">Blaker123</a></span></span> <time class="timeago" datetime="2015-11-27T17:50:33+00:00">27 Nov 2015, 17:50</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/felix56/post/harlem-cycle-1-chester-himes/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Harlem Cycle #1 - Chester Himes</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/felix56/">felix56</a> <time class="timeago" datetime="2015-11-27T09:40:41+00:00">27 Nov 2015, 09:40</time></span></li>
	<li><a href="/blog/dfblast/post/writing-therapy/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Writing Therapy</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/dfblast/">dfblast</a> <time class="timeago" datetime="2015-11-27T08:16:14+00:00">27 Nov 2015, 08:16</time></span></li>
	<li><a href="/blog/olderthangod/post/at-a-certain-age/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> At a Certain Age</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <time class="timeago" datetime="2015-11-26T16:28:19+00:00">26 Nov 2015, 16:28</time></span></li>
	<li><a href="/blog/olderthangod/post/full-english-breakfast/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Full English Breakfast</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <time class="timeago" datetime="2015-11-26T12:45:04+00:00">26 Nov 2015, 12:45</time></span></li>
	<li><a href="/blog/rabinsxp/post/our-incomplete-love-story/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Our Incomplete Love Story</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/rabinsxp/">rabinsxp</a> <time class="timeago" datetime="2015-11-26T12:03:00+00:00">26 Nov 2015, 12:03</time></span></li>
	<li><a href="/blog/TheDels/post/the-life-of-an-indonesian-uploader-mcd-blogs/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The Life of an Indonesian Uploader (MCD Blogs)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-11-26T09:22:47+00:00">26 Nov 2015, 09:22</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/now%20thats/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				now thats
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/mac%20os%20x/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Mac os x
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/mcc/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				mcc
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20quran%20with%20urdu%20translation/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the quran with urdu translation
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/yify%201080p/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				yify 1080p
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/cuck%20old/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				cuck old
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/a%20mother%20rage/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				a mother rage
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/critical%20season/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				critical season
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/idhu%20enna%20maayam/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Idhu Enna Maayam
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/chi%20running/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				chi running
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/one%20last%20dance%202003/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				one last dance 2003
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.kat.cr');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <span  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></span>
        <span  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></span>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
