



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "253-6273827-5932871";
                var ue_id = "0S13QE9GK3BJAYKE2SCG";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="0S13QE9GK3BJAYKE2SCG" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-54f8cee4.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-3715863264._CB300352660_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['443757724749'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4106243895._CB300549911_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"e5654bc29f9a9645c49f7fa10405c2022a294842",
"2016-01-15T18%3A02%3A32GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50248;
generic.days_to_midnight = 0.5815740823745728;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=443757724749;ord=443757724749?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=443757724749?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=443757724749?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=01-15&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59385865/?ref_=nv_nw_tn_1"
> 10 Cloverfield Lane Trailer: Surprise! A Cloverfield Spinoff Is Coming for You
</a><br />
                        <span class="time">12 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59383695/?ref_=nv_nw_tn_2"
> 'Ride Along 2' and 'Revenant' Look to Remove 'Star Wars' from Its Perch
</a><br />
                        <span class="time">14 January 2016 12:00 PM, UTC</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59382424/?ref_=nv_nw_tn_3"
> Oscar nominations: 'The Revenant' leads with 12, followed by 'Mad Max: Fury Road' (10) and 'The Martian' (7)
</a><br />
                        <span class="time">14 January 2016 2:06 PM, UTC</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0082971/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTkwNTAzNjQ4MV5BMl5BanBnXkFtZTcwNzM4MzczMw@@._V1._SY345_CR60,30,410,315_.jpg",
            titleYears : "1981",
            rank : 36,
                    headline : "Raiders of the Lost Ark"
    },
    nameAd : {
            clickThru : "/name/nm0000098/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjA4MDU4MjIyMl5BMl5BanBnXkFtZTcwOTM2OTE0Nw@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 176,
            headline : "Jennifer Aniston"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYttv2CEooPHks8W0TIxPI02a7IavQoYpUo54U1srU9r0jW7BL8QNvGM2ReSn4stCBuRZq71a1o%0D%0AIO1y8SqLBA8cSOOQSOEFNqMDmtG6hWmn8py0vGTWTwJh8hitaaSDmXx3hMu93tNBzMBIRW0PRMhO%0D%0ADprQB-olMFllEJ8at7QIGf5dTCUibpCO9c3gm002i5hv78Xi94lUUwrXAt-TX9XDzw%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYsVWmGCe3BfDvWWETsbLcmLwPq_Ix6NaJiMFHZCE5qydbcq_TSA-aa2HO82onAA66G226AQL9Z%0D%0AO54AZkJg364q98qmvxPi_nwXA3y2hjB0M1rcMccUUZ7kv9swayfi3j1vKn5hlWMWSdVsqVuIjvhl%0D%0ANEiz5DFL1fCT_ChadY3k9UbkZ551kY49RwK6oNVfkV0y%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYiHWboOvAYG43Z94Ex7NS79FoAF-Z7ayKiu-vAEOraGXWibJtV7Zo2LTDc0ZlTStkZUJoG-Lnw%0D%0AV7MBEl7f3xG29dpwdXWUUwwq4WzBuH0-dp5ocS59T_hwLKOyWabrxh_H-thkLWwLA459s70T7CAz%0D%0A-wq2EZl3D5WhD_LNylB9tg_UDg7h61EPC22NTye-KIT9%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=443757724749;ord=443757724749?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=443757724749;ord=443757724749?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4237604121?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi4237604121" data-source="bylist" data-id="ls002309697" data-rid="0S13QE9GK3BJAYKE2SCG" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Waking up from a car accident, a young woman finds herself in the basement of a man who says he's saved her life from a chemical attack that has left the outside uninhabitable." alt="Waking up from a car accident, a young woman finds herself in the basement of a man who says he's saved her life from a chemical attack that has left the outside uninhabitable." src="http://ia.media-imdb.com/images/M/MV5BMjEzMjczOTIxMV5BMl5BanBnXkFtZTgwOTUwMjI3NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzMjczOTIxMV5BMl5BanBnXkFtZTgwOTUwMjI3NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Waking up from a car accident, a young woman finds herself in the basement of a man who says he's saved her life from a chemical attack that has left the outside uninhabitable." title="Waking up from a car accident, a young woman finds herself in the basement of a man who says he's saved her life from a chemical attack that has left the outside uninhabitable." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Waking up from a car accident, a young woman finds herself in the basement of a man who says he's saved her life from a chemical attack that has left the outside uninhabitable." title="Waking up from a car accident, a young woman finds herself in the basement of a man who says he's saved her life from a chemical attack that has left the outside uninhabitable." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1179933/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > 10 Cloverfield Lane </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi212776217?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi212776217" data-source="bylist" data-id="ls056131825" data-rid="0S13QE9GK3BJAYKE2SCG" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Shrewd, savvy U.S. Attorney Chuck Rhoades and the brilliant, ambitious hedge fund king Bobby &quot;Axe&quot; Axelrod are on an explosive collision course, with each using all of his considerable smarts, power and influence to outmaneuver the other." alt="Shrewd, savvy U.S. Attorney Chuck Rhoades and the brilliant, ambitious hedge fund king Bobby &quot;Axe&quot; Axelrod are on an explosive collision course, with each using all of his considerable smarts, power and influence to outmaneuver the other." src="http://ia.media-imdb.com/images/M/MV5BMTY4ODk3OTM4Nl5BMl5BanBnXkFtZTgwMjMwNzg1NzE@._V1_SY298_CR9,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4ODk3OTM4Nl5BMl5BanBnXkFtZTgwMjMwNzg1NzE@._V1_SY298_CR9,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Shrewd, savvy U.S. Attorney Chuck Rhoades and the brilliant, ambitious hedge fund king Bobby &quot;Axe&quot; Axelrod are on an explosive collision course, with each using all of his considerable smarts, power and influence to outmaneuver the other." title="Shrewd, savvy U.S. Attorney Chuck Rhoades and the brilliant, ambitious hedge fund king Bobby &quot;Axe&quot; Axelrod are on an explosive collision course, with each using all of his considerable smarts, power and influence to outmaneuver the other." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Shrewd, savvy U.S. Attorney Chuck Rhoades and the brilliant, ambitious hedge fund king Bobby &quot;Axe&quot; Axelrod are on an explosive collision course, with each using all of his considerable smarts, power and influence to outmaneuver the other." title="Shrewd, savvy U.S. Attorney Chuck Rhoades and the brilliant, ambitious hedge fund king Bobby &quot;Axe&quot; Axelrod are on an explosive collision course, with each using all of his considerable smarts, power and influence to outmaneuver the other." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4270492/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Billions" </a> </div> </div> <div class="secondary ellipsis"> Tune In Sunday at 10 p.m. </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3048191257?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3048191257" data-source="bylist" data-id="ls002397163" data-rid="0S13QE9GK3BJAYKE2SCG" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Lee Gates (George Clooney) is a bombastic TV personality whose popular financial network show has made him the money wiz of Wall Street. But after he hawks a high tech stock that mysteriously crashes, an irate investor (Jack O'Connell) takes Gates, his crew, and his ace producer Patty Fenn (Julia Roberts) hostage live on air. Unfolding in real time, Gates and Fenn must find a way to keep themselves alive while simultaneously uncovering the truth behind a tangle of big money lies." alt="Lee Gates (George Clooney) is a bombastic TV personality whose popular financial network show has made him the money wiz of Wall Street. But after he hawks a high tech stock that mysteriously crashes, an irate investor (Jack O'Connell) takes Gates, his crew, and his ace producer Patty Fenn (Julia Roberts) hostage live on air. Unfolding in real time, Gates and Fenn must find a way to keep themselves alive while simultaneously uncovering the truth behind a tangle of big money lies." src="http://ia.media-imdb.com/images/M/MV5BMzYzNDY5NjIyNF5BMl5BanBnXkFtZTgwMjYzMzA3NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzYzNDY5NjIyNF5BMl5BanBnXkFtZTgwMjYzMzA3NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Lee Gates (George Clooney) is a bombastic TV personality whose popular financial network show has made him the money wiz of Wall Street. But after he hawks a high tech stock that mysteriously crashes, an irate investor (Jack O'Connell) takes Gates, his crew, and his ace producer Patty Fenn (Julia Roberts) hostage live on air. Unfolding in real time, Gates and Fenn must find a way to keep themselves alive while simultaneously uncovering the truth behind a tangle of big money lies." title="Lee Gates (George Clooney) is a bombastic TV personality whose popular financial network show has made him the money wiz of Wall Street. But after he hawks a high tech stock that mysteriously crashes, an irate investor (Jack O'Connell) takes Gates, his crew, and his ace producer Patty Fenn (Julia Roberts) hostage live on air. Unfolding in real time, Gates and Fenn must find a way to keep themselves alive while simultaneously uncovering the truth behind a tangle of big money lies." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Lee Gates (George Clooney) is a bombastic TV personality whose popular financial network show has made him the money wiz of Wall Street. But after he hawks a high tech stock that mysteriously crashes, an irate investor (Jack O'Connell) takes Gates, his crew, and his ace producer Patty Fenn (Julia Roberts) hostage live on air. Unfolding in real time, Gates and Fenn must find a way to keep themselves alive while simultaneously uncovering the truth behind a tangle of big money lies." title="Lee Gates (George Clooney) is a bombastic TV personality whose popular financial network show has made him the money wiz of Wall Street. But after he hawks a high tech stock that mysteriously crashes, an irate investor (Jack O'Connell) takes Gates, his crew, and his ace producer Patty Fenn (Julia Roberts) hostage live on air. Unfolding in real time, Gates and Fenn must find a way to keep themselves alive while simultaneously uncovering the truth behind a tangle of big money lies." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2241351/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Money Monster </a> </div> </div> <div class="secondary ellipsis"> International Trailer </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390393782&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>It's Oscars&#174; Season Once Again</h3> </a> </span> </span> <p class="blurb">We've got awards season covered, including the <a href="/awards-central/oscars?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_lk1">2016 Academy Awards&#174;</a>. Check out nominees lists, galleries, features, original videos, and more.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-nominee-photos-2016?imageid=rm2591484160&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Jennifer Jason Leigh in The Hateful Eight (2015)" alt="Still of Jennifer Jason Leigh in The Hateful Eight (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTk3MDAzNTU1NF5BMl5BanBnXkFtZTgwMjA5MDM1NjE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk3MDAzNTU1NF5BMl5BanBnXkFtZTgwMjA5MDM1NjE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/oscars-nominee-photos-2016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > Oscars Nominees Gallery </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/facts-about-chris-rock/ls015186022?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjIzNzc5NDc3Ml5BMl5BanBnXkFtZTgwMzQ4ODkzMzE@._UX860_CR60,80,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIzNzc5NDc3Ml5BMl5BanBnXkFtZTgwMzQ4ODkzMzE@._UX860_CR60,80,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/facts-about-chris-rock/ls015186022?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > How Well Do You Know Oscars Host Chris Rock? </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389670262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Visit Awards Central on IMDb</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="https://twitter.com/imdb?ref_=hm_twi_hd" > <h3>Twitter Chat With the Directors of 'Anomalisa'</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2401878/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388881922&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_twi_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388881922&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Anomalisa (2015)" alt="Anomalisa (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTkyMzI2MzQ1N15BMl5BanBnXkFtZTgwNDg0MzQxNzE@._V1_SY250_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkyMzI2MzQ1N15BMl5BanBnXkFtZTgwNDg0MzQxNzE@._V1_SY250_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="https://twitter.com/imdb?ref_=hm_twi_i_2" > <img itemprop="image" class="pri_image" title="Anomalisa (2015)" alt="Anomalisa (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjA2NTgwNTg3NF5BMl5BanBnXkFtZTgwMTIwMDE1NzE@._V1_SY250_CR78,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA2NTgwNTg3NF5BMl5BanBnXkFtZTgwMTIwMDE1NzE@._V1_SY250_CR78,0,444,250_AL_UY500_UX888_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Join us Friday, Jan. 15, at 5 p.m. ET/2 p.m. PT for a live Twitter chat with <a href="/name/nm2122478/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388881922&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_twi_lk1">Duke Johnson</a> and <a href="/name/nm0442109/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388881922&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_twi_lk2">Charlie Kaufman</a>, directors of Oscars nominee <i><a href="/title/tt2401878/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388881922&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_twi_lk3">Anomalisa</a></i>. Make sure to follow <a href="https://twitter.com/imdb?ref_=hm_twi_lk4">@IMDb on Twitter</a> and send your questions to #IMDbAskAnomalisa.</p> <p class="seemore"><a href="https://twitter.com/imdb?ref_=hm_twi_sm" class="position_bottom supplemental" >Follow IMDb on Twitter</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59385865?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQ1NzU4NjI5Nl5BMl5BanBnXkFtZTcwNTkyMjc1MQ@@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59385865?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >10 Cloverfield Lane Trailer: Surprise! A Cloverfield Spinoff Is Coming for You</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Vulture</a></span>
    </div>
                                </div>
<p>You might not have seen or heard it coming, but just like that monster in the original movie, anotherÂ <a href="/title/tt1060277?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Cloverfield</a>Â film is coming for you. A mysterious teaser trailer forÂ 10 <a href="/title/tt1060277?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Cloverfield</a> LaneÂ played before screenings of 13 HoursÂ today, prompting fans to speculate that 2008's mysteriously-marketed ...                                        <span class="nobr"><a href="/news/ni59385865?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59383695?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >'Ride Along 2' and 'Revenant' Look to Remove 'Star Wars' from Its Perch</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 1:00 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59382424?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Oscar nominations: 'The Revenant' leads with 12, followed by 'Mad Max: Fury Road' (10) and 'The Martian' (7)</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 3:06 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Gold Derby</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59382413?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Oscar Nomination Snubs: Ridley Scott, Steven Spielberg, Jane Fonda, Aaron Sorkin & Black America</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 3:00 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59383223?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >USA Picks Up Gale Anne Hurdâs âFalling Waterâ</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59383695?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59383695?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >'Ride Along 2' and 'Revenant' Look to Remove 'Star Wars' from Its Perch</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 1:00 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p><a href="/name/nm0455052?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Martin Luther King</a> weekend arrives amid the awards season hustle and bustle. This is a good thing for <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">The Revenant</a> following its monster wide release last weekend, Golden Globe domination and 12 Oscar nominations this morning. However, just as good during the awards season is a strong bit of ...                                        <span class="nobr"><a href="/news/ni59383695?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59382424?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Oscar nominations: 'The Revenant' leads with 12, followed by 'Mad Max: Fury Road' (10) and 'The Martian' (7)</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 3:06 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Gold Derby</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59382413?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Oscar Nomination Snubs: Ridley Scott, Steven Spielberg, Jane Fonda, Aaron Sorkin & Black America</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 3:00 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59383212?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Lack Of Color In Oscar Nominations Frustrates Academy President</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59386755?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Netflix To Crack Down On Proxy Servers As Share Price Continues To Be Volatile</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59383223?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTgwODUzMjExOF5BMl5BanBnXkFtZTcwMTMxMDEyOQ@@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59383223?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >USA Picks Up Gale Anne Hurdâs âFalling Waterâ</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>USA has picked up to series â<a href="/title/tt4520992?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Falling Water</a>,â a new fantasy drama from <a href="/name/nm0005036?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Gale Anne Hurd</a> (â<a href="/title/tt1520211?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">The Walking Dead</a>â), <a href="/name/nm1398578?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">Blake Masters</a> (â<a href="/title/tt0457229?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk5">Brotherhood</a>â) and <a href="/name/nm0111303?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk6">Henry Bromell</a> (â<a href="/title/tt1796960?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk7">Homeland</a>â). The pilot was written and co-created by Masters and Bromell and is from <a href="/company/co0242101?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk8">Universal Cable Productions</a> (Ucp). <a href="/name/nm0294379?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk9">Juan Carlos Fresnadillo</a>...                                        <span class="nobr"><a href="/news/ni59383223?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59383194?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Amy Pascal & Jamie Foxx Team For 1940s TV Crime Drama About Race âDarktownâ</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 7:00 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59383196?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >âMozart In The Jungleâ Available For Free On Amazon This Weekend</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 6:26 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59383427?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Syfy Sets Premiere Dates for â12 Monkeys,â Freshman Drama âHuntersâ</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59383399?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Will Supergirl or DC's Legends Get a 'Visit' From Legion of Super-Heroes?</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59383243?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTE5MzQ5ODAzOF5BMl5BanBnXkFtZTYwMTk0NTg3._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59383243?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Read Daniel Radcliffe's Touching Farewell To Alan Rickman</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>cinemablend.com</a></span>
    </div>
                                </div>
<p>For many, there is one role for which <a href="/name/nm0000614?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Alan Rickman</a> will be remembered among all others. He didn't simply play Severus Snape in the <a href="/title/tt0241527?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Harry Potter</a> film series. He was that character. The actors who were involved in the <a href="/title/tt0241527?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk3">Harry Potter</a> series spent a decade of their lives working together. Needless to say...                                        <span class="nobr"><a href="/news/ni59383243?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59383439?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >David Bowie's Family Plans 'Private Ceremony' Celebrating 'Beloved' Late Legend: Statement</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59383442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Lady Gaga Celebrates Oscar Nomination with Heartfelt Note: 'This Nomination Lends a Voice to Victims and Survivors All Over the World'</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59383026?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Rosie Huntington-Whiteley Shows Off Her $350,000 Engagement Ring From Jason Statham</a>
    <div class="infobar">
            <span class="text-muted">14 January 2016 5:38 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59385111?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >RenÃ© AngÃ©lil's Death 'Was a Shock,' Says Source: 'No One Was Prepared For This'</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm282126080/rg784964352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Shannara Chronicles (2016)" alt="The Shannara Chronicles (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjEzNzYxMjc5OF5BMl5BanBnXkFtZTgwNTc0NDA3NzE@._V1_SY201_CR78,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzNzYxMjc5OF5BMl5BanBnXkFtZTgwNTc0NDA3NzE@._V1_SY201_CR78,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm282126080/rg784964352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Stills </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1637934848/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Deadpool (2016)" alt="Deadpool (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjQyODg5Njc4N15BMl5BanBnXkFtZTgwMzExMjE3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQyODg5Njc4N15BMl5BanBnXkFtZTgwMzExMjE3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1637934848/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/golden-globes-photos-we-love-2016?imageid=rm283830016&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Lady Gaga at event of 73rd Golden Globe Awards (2016)" alt="Lady Gaga at event of 73rd Golden Globe Awards (2016)" src="http://ia.media-imdb.com/images/M/MV5BMzYyODYzMzA1MF5BMl5BanBnXkFtZTgwNzUwMDg2NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzYyODYzMzA1MF5BMl5BanBnXkFtZTgwNzUwMDg2NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/golden-globes-photos-we-love-2016?imageid=rm283830016&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388636222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Photos We Love From the Golden Globes </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=1-15&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm4874651?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Dove Cameron" alt="Dove Cameron" src="http://ia.media-imdb.com/images/M/MV5BMjI5MTQ4MDAwOV5BMl5BanBnXkFtZTgwNzY1MTUwMTE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI5MTQ4MDAwOV5BMl5BanBnXkFtZTgwNzY1MTUwMTE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm4874651?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Dove Cameron</a> (20) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1789336?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jessy Schram" alt="Jessy Schram" src="http://ia.media-imdb.com/images/M/MV5BMTM4MzQ3NjE5N15BMl5BanBnXkFtZTcwNDIxMTM0Nw@@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM4MzQ3NjE5N15BMl5BanBnXkFtZTcwNDIxMTM0Nw@@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1789336?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Jessy Schram</a> (30) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005093?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Regina King" alt="Regina King" src="http://ia.media-imdb.com/images/M/MV5BMTc4MTU4MDc5NF5BMl5BanBnXkFtZTcwNDk5NTE2MQ@@._V1_SY172_CR9,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc4MTU4MDc5NF5BMl5BanBnXkFtZTcwNDk5NTE2MQ@@._V1_SY172_CR9,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005093?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Regina King</a> (45) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0626362?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="James Nesbitt" alt="James Nesbitt" src="http://ia.media-imdb.com/images/M/MV5BMTg2NTk3OTA1Nl5BMl5BanBnXkFtZTcwNjg5NzY4Mw@@._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg2NTk3OTA1Nl5BMl5BanBnXkFtZTcwNjg5NzY4Mw@@._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0626362?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">James Nesbitt</a> (51) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0128655?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Eddie Cahill" alt="Eddie Cahill" src="http://ia.media-imdb.com/images/M/MV5BMTIwMTM3MzA1Nl5BMl5BanBnXkFtZTYwMDU2MzE2._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTIwMTM3MzA1Nl5BMl5BanBnXkFtZTYwMDU2MzE2._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0128655?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Eddie Cahill</a> (38) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=1-15&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg2211420928?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389704362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_acs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389704362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>TV Spotlight: Latest "American Crime Story" Photos</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2664490752/rg2211420928?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389704362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_acs_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389704362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of David Schwimmer in American Crime Story (2016)" alt="Still of David Schwimmer in American Crime Story (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTQ4OTQ4MDcwN15BMl5BanBnXkFtZTgwODc3NDA3NzE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4OTQ4MDcwN15BMl5BanBnXkFtZTgwODc3NDA3NzE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2496718592/rg2211420928?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389704362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_acs_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389704362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Sarah Paulson in American Crime Story (2016)" alt="Still of Sarah Paulson in American Crime Story (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjE0MzQ2Mzk1N15BMl5BanBnXkFtZTgwMzc3NDA3NzE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE0MzQ2Mzk1N15BMl5BanBnXkFtZTgwMzc3NDA3NzE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg2211420928?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389704362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_acs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2389704362&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See photos of "American Crime Story"</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Indie Focus: Go on a 'Monster Hunt'</h3> </span> </span> <p class="blurb">This Chinese fantasy film blends live action and animation to tell the story of a half-human, half-monster child who is threatened by both monster-hating humans and monsters in an ancient world based on medieval China.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3781476/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390189382&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390189382&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Monster Hunt (2015)" alt="Monster Hunt (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUyMjUzNDgxMV5BMl5BanBnXkFtZTgwMDUwNTQ2NzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUyMjUzNDgxMV5BMl5BanBnXkFtZTgwMDUwNTQ2NzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi322613785?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390189382&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390189382&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi322613785" data-rid="0S13QE9GK3BJAYKE2SCG" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Monster Hunt (2015)" alt="Monster Hunt (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUzMzU4MDMzNV5BMl5BanBnXkFtZTgwMDMxODE3NzE@._V1_SY250_CR4,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzMzU4MDMzNV5BMl5BanBnXkFtZTgwMDMxODE3NzE@._V1_SY250_CR4,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Monster Hunt (2015)" title="Monster Hunt (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Monster Hunt (2015)" title="Monster Hunt (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt1340138/trivia?item=tr2468517&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1340138?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Terminator Genisys (2015)" alt="Terminator Genisys (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjM1NTc0NzE4OF5BMl5BanBnXkFtZTgwNDkyNjQ1NTE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM1NTc0NzE4OF5BMl5BanBnXkFtZTgwNDkyNjQ1NTE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt1340138?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Terminator Genisys</a></strong> <p class="blurb">Shot over 90 first unit days.</p> <p class="seemore"><a href="/title/tt1340138/trivia?item=tr2468517&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;lPvypzUe9zU&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/lPvypzUe9zU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Oscars 2016: Best Motion Picture of the Year</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/lPvypzUe9zU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Spotlight (2015)" alt="Spotlight (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjIyOTM5OTIzNV5BMl5BanBnXkFtZTgwMDkzODE2NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIyOTM5OTIzNV5BMl5BanBnXkFtZTgwMDkzODE2NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/lPvypzUe9zU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Revenant (2015)" alt="The Revenant (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/lPvypzUe9zU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Room (2015)" alt="Room (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjE4NzgzNzEwMl5BMl5BanBnXkFtZTgwMTMzMDE0NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE4NzgzNzEwMl5BMl5BanBnXkFtZTgwMTMzMDE0NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/lPvypzUe9zU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Brooklyn (2015)" alt="Brooklyn (2015)" src="http://ia.media-imdb.com/images/M/MV5BMzE4MDk5NzEyOV5BMl5BanBnXkFtZTgwNDM4NDA3NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzE4MDk5NzEyOV5BMl5BanBnXkFtZTgwNDM4NDA3NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/lPvypzUe9zU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Bridge of Spies (2015)" alt="Bridge of Spies (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjIxOTI0MjU5NV5BMl5BanBnXkFtZTgwNzM4OTk4NTE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIxOTI0MjU5NV5BMl5BanBnXkFtZTgwNzM4OTk4NTE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Which film should win the Oscar for Best Motion Picture of the Year at the 2016 Oscars? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/252617200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/lPvypzUe9zU/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388733862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=443757724749;ord=443757724749?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=443757724749?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=443757724749?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2869728"></div> <div class="title"> <a href="/title/tt2869728?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Ride Along 2 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4172430"></div> <div class="title"> <a href="/title/tt4172430?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> 13 Hours: The Secret Soldiers of Benghazi </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1594972"></div> <div class="title"> <a href="/title/tt1594972?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Norm of the North </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4048668"></div> <div class="title"> <a href="/title/tt4048668?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Band of Robbers </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3165630"></div> <div class="title"> <a href="/title/tt3165630?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> The Benefactor </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2718440"></div> <div class="title"> <a href="/title/tt2718440?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Moonwalkers </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3577624"></div> <div class="title"> <a href="/title/tt3577624?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Un jour comme un autre </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104142&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390104282&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Star Wars: The Force Awakens </a> <span class="secondary-text">$42.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1663202"></div> <div class="title"> <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> The Revenant </a> <span class="secondary-text">$39.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1528854"></div> <div class="title"> <a href="/title/tt1528854?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Daddy's Home </a> <span class="secondary-text">$15.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3387542"></div> <div class="title"> <a href="/title/tt3387542?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> The Forest </a> <span class="secondary-text">$12.7M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1850457"></div> <div class="title"> <a href="/title/tt1850457?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Sisters </a> <span class="secondary-text">$7.2M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1860213"></div> <div class="title"> <a href="/title/tt1860213?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Dirty Grandpa </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2304933"></div> <div class="title"> <a href="/title/tt2304933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The 5th Wave </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3882082"></div> <div class="title"> <a href="/title/tt3882082?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> The Boy </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2888046"></div> <div class="title"> <a href="/title/tt2888046?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Ip Man 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2049543"></div> <div class="title"> <a href="/title/tt2049543?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Synchronicity </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-2032675409._CB285310264_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/golden-globes-our-favorite-fashions-2016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2383543262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_rhs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2383543262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Our Favorite Golden Globe Looks</h3> </a> </span> </span> <p class="blurb">The stars shined brightly at the Golden Globes with stunning red-carpet looks, and we've rounded up some of our favorite fashions of the night.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/golden-globes-our-favorite-fashions-2016?imageid=rm1964434944&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2383543262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_rhs_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2383543262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Channing Tatum and Jenna Dewan Tatum at event of 73rd Golden Globe Awards (2016)" alt="Channing Tatum and Jenna Dewan Tatum at event of 73rd Golden Globe Awards (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjA3MTQwNDk1N15BMl5BanBnXkFtZTgwNTE0ODc2NzE@._V1_SY525_CR45,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA3MTQwNDk1N15BMl5BanBnXkFtZTgwNTE0ODc2NzE@._V1_SY525_CR45,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/golden-globes-our-favorite-fashions-2016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2383543262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_rhs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2383543262&pf_rd_r=0S13QE9GK3BJAYKE2SCG&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYjyXyhkaHcgdUHm_Y00xA7ZajsgBrV5edVk-a-1t26kN2s7RAB6uQi_TiXTbbAYdNqLLcZ4MO4%0D%0Ajj-dKB5cBCkdMjB1dHYV7U8sYGzoODlODiHYuj9kLChekuv1NRmg5uV4E37SpEjteThEIKcjmuaT%0D%0AOgwfTfSaVKV6xBAGWkGRVeG1lLHPKDLvNQUVgFLgwF1V_WiDDIHSkR7Mg1icbiWC7PfglyAuUtix%0D%0AZGL3wewPIfc%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYiQRAoODvgA4c8FBi1nLoVksnZfepiYeyYa7tKvH67m3joQRPktD0qSwn5dc_RJsKsOPSA10Vo%0D%0ATQWn3iL33CQSk6MIgjBvQd_Nk984zZJGf9jDqHrwG6FArURk5Mh5YI1pPu1zuvGa9PfDNJ8n4ve4%0D%0AZd1ssl-5y451v1ArieJRi2-TND3G381KJmUbmwDX-VuX9b4VLH8VB9evhwXTGs7buw%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYqPyjQS6_ar1-5b1uXnUdy2sU7a0x5w2k9zdPhljFUHE3GmTj_QLfa5-Tsyh783LIo12-wzb0B%0D%0AGGiKZJO55RGpJaHOeJq_f_ipFlqpkkUhE5HRlzviKtk01D3O_L5npK2mPyELkijXN88eFwWOXMgJ%0D%0AEaoqNl2nwY18G4DwCctzxgTYgIe4YMMSOdB2P4rVroCbS0sfbZWipR6xXwe_R_jZs6akQmLGLA6p%0D%0APlNTtx3WW0rVGAsDBiRlOoFUkinpcAI-_2jVQ07Yop4xEcaBIoappbM4f6-lEycbroFBqt-Uz6Qg%0D%0AP4wTPN6dvGimt8qejFpJgLKbOIdtIDrShcuYs3m5_ZHmTaJVaJN7EqInpvxpomZ--ohjQ3nf7fQw%0D%0A1aJsT7uBmA9kRjtV_6FJsXx5_C4hFA%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYrTvwDsszNaJqxEHMAkJUhUgr1_Ep-mmYpI7N07qBy641mePM-HNLkQlEhuAtXFMX8IzRQzaLQ%0D%0A3wfzaCfnfrc_matwyS2QF2Hd1Utm3yIKs-eghcwc1TARN7vsQlyCe12jGkDesp4gqLN8PhLsqfTn%0D%0AsZBx2N_nwFQDK8wTX3x0BpdZCNHBCypXlM37KqPfC8nuAcZIKo7DEC37dPBBptd62w%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYvx0qgdmUDSkc72V6NXlaxAUWBChEN1Dzy6DkLD3wkdhgqt9YrRaPdlbF7JCcvcXjqqzq7P-GP%0D%0AJ5cx4VNTPdR_VX8dIncEnyNnwjQo0EB9m8zsA1ojYBOKPhar7seIuwk3XXxstySkbiQMpqCTZx6U%0D%0AA9AzGIGbiwGlxoufECQLan_ykp0td2Za_zjn6kvWk4zL%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYo1CVm2IkFgiO5PyRdTN1eXocwO99cpCKlzoszIaI9-ab47lyjqDgUgrs4PDy_jqyun3fWknTp%0D%0A_QmifyTVPAEd1EWJPqKqfZ91Y8jeY4-uMF9gOKaAmQ8dQRDTeYwQi_TrLbuvrFouUor2epw935ad%0D%0AWnQwiA2c20ZL9CDK34Mpha_mC0joF9ylg4sydt8kDogQ%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYljl9i6Mef2riaHAi42GJ_VgmAVLjjM8bCF9kAEZoIqLY21qX88B6ocOICp6TvOgGehTkFuPIU%0D%0AcODHvPN7LF2QH1byOPVDKoKU9DrSP0p6FVPtzd2NKDiGzFuQHb6S9nI3iXtrgCZMyScXNKub2eCM%0D%0Agwv4RBc2ZjQQGg7z5BcSYbLNW383F-BdBcrAdZ0nUVdoV5q0j0ViBv5Mg56K2suNuw%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYuTKKmT-EyD2D8S7SFRZth33awVrus8Dx4eDuVycluFcezGPpljgrWQ0yoC-MhytrfOD8IIsRV%0D%0AvNo50v_j10Guq8LJmluV0KL17FIpOhWUYUSdOoNCxJxuORGYlsQA2ONMY_R4-oQ9AsiWcPYTgDNK%0D%0A__dSdT90FHOLw2Pth4uMpXB4eePtL2-GEyleMRuEFk1I%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYpdMuUu9WgmEcrLF32OAy82RnTjEPyrpmmpXWRsxG6Pqj701FgW5BCLXD5TBCvqIcZrBo-pP2z%0D%0ApPP-26CvOuIkXOBvefGL-ZBOd9LsmJT7dggLOGK9yjP3pbrayrcK9BSR9yymGdY_6YTpQUC00Szu%0D%0AepjhUCFDjdyDi4hCzA2DfQ0_vNoyTJSFomucWSNlJi2D_S40F0J-I6NDpBzLY3DmJJrhNJ2KZekz%0D%0Axz5ao_ktogZGmtIwFbIR5jUbpX_u__LE%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYpI0NrBBxauL4bboXoF2HuViy8K0JcwUevBelqgXJ2zYaQrUxwsNkk8H09E3zqkG4dE1IMGTEA%0D%0A_Sd50-PsWtg0eDfGV3hOSQsC3JPUJWRpNf5BA9oRFRSAJCghuRCr5mZg-M6hFlM0jgcplYJ55zDG%0D%0AuWwgsi4ij0KTyV0f3I4JtXqalS8Fzs9mAysqJou2BKxDyDIIuPLSpMrft2hBj7ZqJxki2UzIr-Iy%0D%0AAOVfivryDPk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYpi2Xb-9DRstFHDBX7rVDQc8YeiUQ6kSy8WLNTAwHlysCuqUIVwt5WEZLBbjX20KnEYBSTZ1r-%0D%0AegKYhH_2mH5Xif6XGGtaMGXnebaZpf3llV21O6FgpsfWq_zYTAocsX1WUvNI3b2qqaiw7h4LQAgD%0D%0AIBkPwjKxH7wDPy61qk_PQWC-Iuh-V9cgcnNqrDps5SQDITKhl2ttwixGrpCuiIrSQqbELiJBeYIP%0D%0AVniP7g5h6fM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYvMUMNaPlVWSPZNE_itLZqnWLQt24AMARmXCVTYqLgxuVLHCYfdlGNYqKXq-0Na9CZRiuWbvwI%0D%0AsA1zxVYmX93wX8MLX0CS2t29b34xqBfxNXvTcrhXpXVLIdG2bwgVdpr9dqNzRpLuPUFYTd91RUtI%0D%0AE_QXFbYnG8q_uQG56PFvpPuUFXdAxIugOyvyCSfia6JpuoiDmq6gAkAO84IEdYKQn5otZMtnDXH7%0D%0A-2dtZGo-EqQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYjJ_PEd-EW5s8RkIZdjhWcC-isnkLZtAGc6PO2WCvD0YpFs9zBUEtUCBynPJxouNKjmU0LsqJ1%0D%0AEAVlb9zaGi2Rto9mwU4CKSk3rIkxzjaanTEyYpZ-diHdTSxLP-pZqjWn89klkx_firgkTZBvwi4d%0D%0Ab_e7MF7XyIS7yvg81nT5d2USt1vJwrfKEDZMKFHRpCcsIwYqYamnXRWe6mRt2Uzbn1DDgy33uY26%0D%0A08eHtkY2Ofk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYrLIrPK9QFNF60Y2YVTuChE9E_oWZVGswv_apUD0edeDrPlfdNnYneI2dXL2v2h6gAGBtjt0rc%0D%0Aksa4czrZtStu_tdfIx4V4Ze8gQT0476IzK2bzdfn6FhB5lTgvaxWCQSajQ-UfEza5iaHrQPNU-JD%0D%0A5FE2auAWfPTWXCNPxB6ip3LSd_fYRD5k5Ng2uPf7KCP42HJ68_rZAWb4MlC5gHTCcy5nmyRESNqY%0D%0AGSXiH9Yx7Ok%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYrWIgRNysAEhXkIQlW7UoAha8LJSHfjvVjJlH5H8iFReVuCO6N-yaUv6TEkaW2sDvOB7H_Ylgf%0D%0A1lTzFtuJLu2MPtsv_mKr5UzLlvaU6wviTxpW_JmZE_Wa4OOER_pEFHBftRj1No6JVN0nc3C7spT5%0D%0ALFDWr7Nd64cBFhaaKRLMmNsDRICG10-LaiS_yaozzmjpXU8ePQ7A8B3h58vxLJJN0tur-_qtkKfY%0D%0A45n-Xqh18NXLhHId3ddoviYOJ5vmqKhm%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYnw7lgxdDm6Ap2Ajz764NBxthQ-Sz2mvpRvoAfs-Q_bwy8WNw967JtsfFk_FxEvYuV2o3RGQUo%0D%0AhYTocs9FIgDsz5-fPL-1DOvjEAJsLLzSHdXKX-plK_xgp452qtCnWXbc6upWWsHa2Skduz-9fTHr%0D%0AuAuvSmap9S_sB1Nrpnmjymg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYjHi2G5xMl__SNbkjh7DkRTwlnAal2w2eHkAvojRMbz83FLa0K731L4GY9Xfxj0Zb1X57Fc-yX%0D%0Aq5hwmZIA908dTdtXZx98nwbn-V-3WICWaICBklH_JGC7LMEXMbSwEsVXvW8jzAG0qrhclNiEmQrR%0D%0Abr_CYLe3NyHcPOlPzHkJBxc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-3617413113._CB300284919_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-4073101944._CB300352925_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101b4def892a8b3e825f7a23cc6ae2243c838b1b691749061b9081951552402da08",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=443757724749"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=443757724749&ord=443757724749";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="280"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
