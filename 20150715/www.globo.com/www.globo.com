



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='14/07/2015 21:59:29' /><meta property='busca:modified' content='14/07/2015 21:59:29' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/21825a611e44.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/96d91591782a.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class=""><div class="v-separator"></div><a target="_top" href="http://m.g1.globo.com" accesskey="n" class="barra-item-g1 link-produto">g1</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://m.globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto">globoesporte</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto">gshow</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto">famosos &amp; etc</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://m.techtudo.com.br" accesskey="b" class="barra-item-tech link-produto">tecnologia</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><SCRIPT LANGUAGE=JavaScript><!--
OAS_url = 'http://ads.globo.com/RealMedia/ads/';OAS_listpos = 'Frame1,Middle,Middle3';OAS_query = '';if(document.cookie){var query=document.cookie.match(/aamoas=(.+?)(;|$)/);if(query){OAS_query=unescape(query[1]);}
var queryUUID=document.cookie.match(/aam_uuid=(.+?)(;|$)/);if(queryUUID){if(OAS_query) {OAS_query+='&';}
OAS_query+='uuid=' + queryUUID[1];}}
OAS_sitepage = 'globo.com/globo.com/home';OAS_version=10;OAS_rn = '001234567890';OAS_rns = '1234567890';OAS_rn=new String (Math.random());OAS_rns=OAS_rn.substring (2,11);function OAS_NORMAL(pos){document.write('<A HREF="' + OAS_url + 'click_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + OAS_query + '" TARGET=_top>');document.write('<IMG SRC="' + OAS_url + 'adstream_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + OAS_query + '" BORDER=0></A>');}
//--></SCRIPT><SCRIPT LANGUAGE="JavaScript"><!--
OAS_version=11;if(navigator.userAgent.indexOf('Mozilla/3')!=-1)
OAS_version=10;if(OAS_version >= 11){if(document.body && document.body.offsetWidth >= 732){document.write('<SCR'+ 'IPT LANGUAGE=JavaScript1.1 SRC="' + OAS_url + 'adstream_mjx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '?' + OAS_query + '"></SC'+'RIPT>');}}
//--></SCRIPT><SCRIPT LANGUAGE="JavaScript"><!--
document.write('')
function OAS_AD(pos){if(OAS_version >= 11)
OAS_RICH(pos);else
OAS_NORMAL(pos);}
//--></SCRIPT><header><div id="base-container-width-element" class="container"><div class="header-mobile-top"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo" href="http://m.globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-nacional/">
                                                <span class="titulo">Jornal Nacional</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/radar-g1/platb/">
                                                    <span class="titulo">TrÃ¢nsito</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                <span class="titulo">Copa do Brasil</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/volei/">
                                                <span class="titulo">VÃ´lei</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-america/">
                                                    <span class="titulo">Copa AmÃ©rica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/superstar/2015">
                                                <span class="titulo">SuperStar</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/esquenta/">
                                                    <span class="titulo">Esquenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/superstar/2015">
                                                    <span class="titulo">SuperStar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/pe-na-cova/index.html">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-07-1421:56:20Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><script>
(function(){OAS_MOBILE=function(){};window.MOBILE_WIDTH_SETTING=731;if(document.body && document.body.offsetWidth <= window.MOBILE_WIDTH_SETTING){OAS_AD=function(){};OAS_MOBILE=function(pos) {document.write("<scr"+"ipt type='text/javascript' src='http://ads.globo.com/RealMedia/ads/adstream_jx.ads/" + OAS_sitepage + "/1" + String(Math.random()).substring (2,11) + "@" + pos + "'><\/scr" + "ipt>");};}else{OAS_MOBILE=function(pos) {document.getElementById(pos).style.display = "none";};}}());</script><div id="x60" class="opec-area opec-x60 grid-12"><div class="opec-internal"><script>OAS_MOBILE('x60');</script><noscript><a href='http://ads.globo.com/RealMedia/ads/click_nx.ads/globo.com/globo.com/home/@x60'><img src='http://ads.globo.com/RealMedia/ads/adstream_nx.ads/globo.com/globo.com/home/@x60'></a></noscript></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"><div class="glb-box-eventos-maker"><div class="glb-container" style="border-color: #FF7400;"><a class="content-box-evento" href="http://globoesporte.globo.com/cartola-fc/"><span class="logo" style="background: url(http://s2.glbimg.com/J7EmQ8qEpKZNWcwDO_oDRzAgYzs=/0x0:100x75/100x75/s.glbimg.com/en/ho/f/original/2015/05/18/sprite.png) 0 0 no-repeat; width: 100px; height: 75px;"></span><span class="title-parent"><span class="logo-title" style="color: #FF7400;">mercado aberto</span><span class="title" data-hover-color="#FF7400">Veja a sua pontuaÃ§Ã£o e escale o time para a prÃ³xima rodada do Cartola FC</span></span><span class="subtitle" style="color: #FF7400;">nÃ£o perca tempo<span class="arrow"> âº</span></span></a></div></div></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/07/collor-diz-que-nova-fase-da-lava-jato-extrapolou-limites-da-legalidade.html" class=" " title="Collor diz que PF extrapolou todos os limites da legalidade"><div class="conteudo"><h2>Collor diz que PF extrapolou todos os limites da legalidade</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Janot cita indÃ­cios de 5 crimes e pede &#39;discriÃ§Ã£o&#39; Ã  PF" href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/07/em-pedido-ao-stf-janot-cita-indicios-de-5-crimes-e-pede-discricao-pf.html">Janot cita indÃ­cios de 5 crimes e pede &#39;discriÃ§Ã£o&#39; Ã  PF</a></div></li></ul></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/07/senado-aprova-aumento-do-tempo-de-internacao-para-menores-infratores.html" class=" " title="Senado aprova internaÃ§Ã£o de atÃ© 
dez anos para menores infratores"><div class="conteudo"><h2>Senado aprova internaÃ§Ã£o de atÃ© <br />dez anos para menores infratores</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/blog/cristiana-lobo/post/em-almoco-no-alvorada-lula-diz-para-dilma-retomar-dialogo-com-o-pais.html" class=" " title="Lula pede volta de diÃ¡logo de Dilma com paÃ­s"><div class="conteudo"><h2>Lula pede volta de diÃ¡logo de Dilma com paÃ­s</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/brasil/delator-da-lava-jato-diz-que-pagou-4-milhoes-jose-dirceu-pedido-de-diretor-da-petrobras-16763725" class=" " title="Delator diz que pagou R$ 4 mi a JosÃ© Dirceu"><div class="conteudo"><h2>Delator diz que pagou R$ 4 mi <br />a JosÃ© Dirceu</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/babilonia/Vem-por-ai/noticia/2015/07/beatriz-faz-surpresa-para-diogo.html" class="foto " title="&#39;BabilÃ´nia&#39;: vilÃ£ surpreende affair  (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/h5owTGqgENoLejhRxXC5NiRrUHA=/filters:quality(10):strip_icc()/s2.glbimg.com/Xn6u1AE0rf3QAMkb2fBIchHeXp8=/0x66:426x232/155x60/s.glbimg.com/et/gs/f/original/2015/07/14/diogo.jpg" alt="&#39;BabilÃ´nia&#39;: vilÃ£ surpreende affair  (TV Globo)" title="&#39;BabilÃ´nia&#39;: vilÃ£ surpreende affair  (TV Globo)"
         data-original-image="s2.glbimg.com/Xn6u1AE0rf3QAMkb2fBIchHeXp8=/0x66:426x232/155x60/s.glbimg.com/et/gs/f/original/2015/07/14/diogo.jpg" data-url-smart_horizontal="Aera6RA0gy-m4a6-uDa_8yAEI2c=/90x56/smart/filters:strip_icc()/" data-url-smart="Aera6RA0gy-m4a6-uDa_8yAEI2c=/90x56/smart/filters:strip_icc()/" data-url-feature="Aera6RA0gy-m4a6-uDa_8yAEI2c=/90x56/smart/filters:strip_icc()/" data-url-tablet="lAOTxs5amis-C3DC5VeuHLzS2Ys=/160xorig/smart/filters:strip_icc()/" data-url-desktop="AUZjMDhSZyOEiLoWu28HvaIqswo=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;BabilÃ´nia&#39;: vilÃ£ surpreende affair </h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Regina posa poderosa" href="http://gshow.globo.com/novelas/babilonia/Vem-por-ai/noticia/2015/07/regina-vira-modelo-veja-fotos.html">Regina posa poderosa</a></div></li><li><div class="mobile-grid-partial"><a title="Ivan Ã© vÃ­tima de racismo" href="http://extra.globo.com/tv-e-lazer/assim-como-aconteceu-com-ator-vinicius-romao-ivan-preso-em-babilonia-apos-ser-vitima-de-preconceito-16752801.html">Ivan Ã© vÃ­tima de racismo</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/verdades-secretas/Vem-por-ai/noticia/2015/07/fanny-flagra-conversa-intima-entre-giovanna-e-anthony.html" class="foto " title="&#39;Verdades&#39;: Gio provoca Fanny (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/42UlcedTKKC6jCr5cQzOhjAX-p8=/filters:quality(10):strip_icc()/s2.glbimg.com/M8jKAvJy5p0KCKnu36p8QLlgg7U=/117x34:536x196/155x60/s.glbimg.com/et/gs/f/original/2015/07/13/cena-24.jpg" alt="&#39;Verdades&#39;: Gio provoca Fanny (TV Globo)" title="&#39;Verdades&#39;: Gio provoca Fanny (TV Globo)"
         data-original-image="s2.glbimg.com/M8jKAvJy5p0KCKnu36p8QLlgg7U=/117x34:536x196/155x60/s.glbimg.com/et/gs/f/original/2015/07/13/cena-24.jpg" data-url-smart_horizontal="sIIlSI_nBR515lEXZ3W-Rw4qzcA=/90x56/smart/filters:strip_icc()/" data-url-smart="sIIlSI_nBR515lEXZ3W-Rw4qzcA=/90x56/smart/filters:strip_icc()/" data-url-feature="sIIlSI_nBR515lEXZ3W-Rw4qzcA=/90x56/smart/filters:strip_icc()/" data-url-tablet="agysNl4JwLt1LhbKnDl20l5gtzs=/160xorig/smart/filters:strip_icc()/" data-url-desktop="B0wcCY5abw2O83unS6G455h54k0=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Verdades&#39;: Gio provoca Fanny</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="MÃ£e desconfia de Angel" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/07/desconfiada-carolina-perguntara-se-angel-nao-gosta-de-alex.html">MÃ£e desconfia de Angel</a></div></li><li><div class="mobile-grid-partial"><a title="Alex presenteia com joia" href="http://extra.globo.com/tv-e-lazer/telinha/verdades-secretas-alex-compra-joia-para-carolina-paga-as-dividas-de-hilda-16747997.html">Alex presenteia com joia</a></div></li></ul></div></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/blog/matheus-leitao/post/policia-federal-apreende-r-367-milhoes-na-16a-fase-da-lava-jato.html" class="foto " title="Apreendidos R$ 3,67 milhÃµes em empresa de SP (ReproduÃ§Ã£o / PF)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/TDDmO1ctsCjaPEA70TJYCABLpl8=/filters:quality(10):strip_icc()/s2.glbimg.com/ddDJCl9HJQ0UBmiPJtbhAcyZi2E=/33x0:609x468/155x126/s.glbimg.com/jo/g1/f/original/2015/07/14/lava_jato_-_dinheiro_apreendido.jpg" alt="Apreendidos R$ 3,67 milhÃµes em empresa de SP (ReproduÃ§Ã£o / PF)" title="Apreendidos R$ 3,67 milhÃµes em empresa de SP (ReproduÃ§Ã£o / PF)"
         data-original-image="s2.glbimg.com/ddDJCl9HJQ0UBmiPJtbhAcyZi2E=/33x0:609x468/155x126/s.glbimg.com/jo/g1/f/original/2015/07/14/lava_jato_-_dinheiro_apreendido.jpg" data-url-smart_horizontal="XL7Kc6OuDq-BrPj6KWO_pkkbkn4=/90x56/smart/filters:strip_icc()/" data-url-smart="XL7Kc6OuDq-BrPj6KWO_pkkbkn4=/90x56/smart/filters:strip_icc()/" data-url-feature="XL7Kc6OuDq-BrPj6KWO_pkkbkn4=/90x56/smart/filters:strip_icc()/" data-url-tablet="dE5f_qRANNy0A478wfxJCeE2z5I=/160xorig/smart/filters:strip_icc()/" data-url-desktop="6pIcQanjc5K2s0Jtfi5Tx4LypEI=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Apreendidos R$ 3,67 milhÃµes em empresa de SP</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/07/pf-apreende-ferrari-e-lamborghini-na-residencia-de-collor-em-brasilia.html" class="foto " title="PF apreende na casa de Collor carros de luxo (Pedro Ladeira/Folhapress)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/8hOkxKuxJFFnEoQQ3REj4MoHTR8=/filters:quality(10):strip_icc()/s2.glbimg.com/5QWYgJLOurrgJ-AikVDaD9_5PKw=/20x0:266x199/155x126/s.glbimg.com/jo/g1/f/original/2015/07/14/ferrari_collor.jpg" alt="PF apreende na casa de Collor carros de luxo (Pedro Ladeira/Folhapress)" title="PF apreende na casa de Collor carros de luxo (Pedro Ladeira/Folhapress)"
         data-original-image="s2.glbimg.com/5QWYgJLOurrgJ-AikVDaD9_5PKw=/20x0:266x199/155x126/s.glbimg.com/jo/g1/f/original/2015/07/14/ferrari_collor.jpg" data-url-smart_horizontal="7d5HEfnQz6icd7Bq_5EnT559ddk=/90x56/smart/filters:strip_icc()/" data-url-smart="7d5HEfnQz6icd7Bq_5EnT559ddk=/90x56/smart/filters:strip_icc()/" data-url-feature="7d5HEfnQz6icd7Bq_5EnT559ddk=/90x56/smart/filters:strip_icc()/" data-url-tablet="3LncX37ke7M2dWPNjMcxf5gsBa8=/160xorig/smart/filters:strip_icc()/" data-url-desktop="NpHNm_0HoMJ57RNtjrlDGIE7HsY=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>PF apreende na casa de Collor carros de luxo</h2></div></a></div></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/rj/futebol/copa-do-brasil/jogo/14-07-2015/botafogo-figueirense/" class=" " title="Siga: Bota pega o Figueira pela Copa do Brasil"><div class="conteudo"><h2>Siga: Bota pega o Figueira pela Copa do Brasil</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/rs/futebol/copa-do-brasil/jogo/14-07-2015/gremio-criciuma/" class=" " title="CriciÃºma abre placar sobre o GrÃªmio; lances"><div class="conteudo"><h2>CriciÃºma abre placar sobre o GrÃªmio; lances</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/jogos-pan-americanos/no-ar/finais-do-judo-jogos-pan-americanos-de-toronto.html" class="foto " title="Siga aqui finais do judÃ´ no Pan (Rafael Burza/CBJ)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/jO6TFy1zyvXQHdYkPyXuUHJcTQI=/filters:quality(10):strip_icc()/s2.glbimg.com/cNOYnz_-fgDUnyc6YIQwnQm-Z4A=/96x303:1089x687/155x60/s.glbimg.com/es/ge/f/original/2015/07/14/luciano_correa.jpg" alt="Siga aqui finais do judÃ´ no Pan (Rafael Burza/CBJ)" title="Siga aqui finais do judÃ´ no Pan (Rafael Burza/CBJ)"
         data-original-image="s2.glbimg.com/cNOYnz_-fgDUnyc6YIQwnQm-Z4A=/96x303:1089x687/155x60/s.glbimg.com/es/ge/f/original/2015/07/14/luciano_correa.jpg" data-url-smart_horizontal="lSxOO2S3OlnAdVQitMaca4og0mo=/90x56/smart/filters:strip_icc()/" data-url-smart="lSxOO2S3OlnAdVQitMaca4og0mo=/90x56/smart/filters:strip_icc()/" data-url-feature="lSxOO2S3OlnAdVQitMaca4og0mo=/90x56/smart/filters:strip_icc()/" data-url-tablet="NuLAQwAzhDZDLrWZN1TnjqRraoI=/160xorig/smart/filters:strip_icc()/" data-url-desktop="LUjblXUB2tNPQPPLLhnMrBtW9xo=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Siga aqui finais do judÃ´ no Pan</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Isaquias ganha 2Âº ouro" href="http://globoesporte.globo.com/jogos-pan-americanos/noticia/2015/07/isaquias-queiroz-vence-c1-200m-e-conquista-segundo-ouro-no-pan-2015.html">Isaquias ganha 2Âº ouro</a></div></li><li><div class="mobile-grid-partial"><a title="Zanetti fica com ouro" href="http://globoesporte.globo.com/jogos-pan-americanos/noticia/2015/07/sutil-e-frio-arthur-zanetti-confirma-favoritismo-e-fica-com-ouro-nas-argolas.html">Zanetti fica com ouro</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/palmeiras/noticia/2015/07/palmeiras-e-recebido-com-festa-de-mais-de-300-torcedores-em-londrina.html" class="foto " title="VerdÃ£o chega ao PR com festa (Fabricio Crepaldi)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/Ud16hp7CzxIpLI5YonjlZ2gMuhc=/filters:quality(10):strip_icc()/s2.glbimg.com/V7DoVYmh7gvUvgqA9Ssm5RJrygg=/0x219:640x467/155x60/s.glbimg.com/es/ge/f/original/2015/07/14/fullsizerender3.jpg" alt="VerdÃ£o chega ao PR com festa (Fabricio Crepaldi)" title="VerdÃ£o chega ao PR com festa (Fabricio Crepaldi)"
         data-original-image="s2.glbimg.com/V7DoVYmh7gvUvgqA9Ssm5RJrygg=/0x219:640x467/155x60/s.glbimg.com/es/ge/f/original/2015/07/14/fullsizerender3.jpg" data-url-smart_horizontal="Ty2kphz_JoqbNDrQ5yyp5ARN-eI=/90x56/smart/filters:strip_icc()/" data-url-smart="Ty2kphz_JoqbNDrQ5yyp5ARN-eI=/90x56/smart/filters:strip_icc()/" data-url-feature="Ty2kphz_JoqbNDrQ5yyp5ARN-eI=/90x56/smart/filters:strip_icc()/" data-url-tablet="wXoU4LYSteDqQcdIn9_ZYWH2io0=/160xorig/smart/filters:strip_icc()/" data-url-desktop="mWgM5_Xbr2sV0CF5YpXk--JkaKU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>VerdÃ£o chega ao PR com festa</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Veja disputa no G-4 e Z-4" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">Veja disputa no G-4 e Z-4</a></div></li><li><div class="mobile-grid-partial"><a title="R10 terÃ¡ festa no Maraca" href="http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/07/de-volta-ao-rio-apos-tres-anos-r10-tera-ainda-mais-exposicao-no-dia-dia.html">R10 terÃ¡ festa no Maraca</a></div></li></ul></div></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div data-position="top1"><script>
OAS_rn=new String(Math.random());OAS_rns=OAS_rn.substring(2,11);if(document.body && document.body.offsetWidth >= 732){document.write("<scr"+"ipt type='text/javascript' src='http://ads.globo.com/RealMedia/ads/adstream_jx.ads/globo.com/globo.com/home/1" + OAS_rns +"@Top1'><\/script>");}
</script><noscript><a href='http://ads.globo.com/RealMedia/ads/click_nx.ads/globo.com/globo.com/home/@Top1'><img src='http://ads.globo.com/RealMedia/ads/adstream_nx.ads/globo.com/globo.com/home/@Top1'></a></noscript></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/">encontro</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/padre-marcelo-rossi-comenta-que-seu-livro-philia-aborda-a-importancia-da-autoestima/4320303/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/padre-marcelo-rossi-comenta-que-seu-livro-philia-aborda-a-importancia-da-autoestima/4320303/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4320303.jpg" alt="Padre Marcelo fala da importÃ¢ncia da autoestima: &#39;Mexe atÃ© com a fÃ©&#39;" /><div class="time">02:47</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">novo livro</span><span class="title">Padre Marcelo fala da importÃ¢ncia da autoestima: &#39;Mexe atÃ© com a fÃ©&#39;</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/ingrid-guimaraes-revela-que-tinha-baixa-autoestima-quando-era-adolescente/4320315/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/ingrid-guimaraes-revela-que-tinha-baixa-autoestima-quando-era-adolescente/4320315/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4320315.jpg" alt="Ingrid GuimarÃ£es conta sobre cirurgia facial feita aos 20 anos" /><div class="time">01:54</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">&#39;sou muito mais bonita hoje&#39;</span><span class="title">Ingrid GuimarÃ£es conta sobre cirurgia facial feita aos 20 anos</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/michelli-esta-com-123-quilos-e-aprendeu-a-ser-feliz-com-o-proprio-corpo/4320339/"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/michelli-esta-com-123-quilos-e-aprendeu-a-ser-feliz-com-o-proprio-corpo/4320339/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4320339.jpg" alt="&#39;Levanta sua cabeÃ§a&#39;, aconselha mulher que nÃ£o liga para crÃ­ticas" /><div class="time">04:07</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">engordou apÃ³s casamento</span><span class="title">&#39;Levanta sua cabeÃ§a&#39;, aconselha mulher que nÃ£o liga para crÃ­ticas</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/padre-marcelo-rossi-comenta-que-seu-livro-philia-aborda-a-importancia-da-autoestima/4320303/"><span class="subtitle">novo livro</span><span class="title">Padre Marcelo fala da importÃ¢ncia da autoestima: &#39;Mexe atÃ© com a fÃ©&#39;</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/ingrid-guimaraes-revela-que-tinha-baixa-autoestima-quando-era-adolescente/4320315/"><span class="subtitle">&#39;sou muito mais bonita hoje&#39;</span><span class="title">Ingrid GuimarÃ£es conta sobre cirurgia facial feita aos 20 anos</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/michelli-esta-com-123-quilos-e-aprendeu-a-ser-feliz-com-o-proprio-corpo/4320339/"><span class="subtitle">engordou apÃ³s casamento</span><span class="title">&#39;Levanta sua cabeÃ§a&#39;, aconselha mulher que nÃ£o liga para crÃ­ticas</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-entretenimento"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/padre-marcelo-rossi-comenta-que-seu-livro-philia-aborda-a-importancia-da-autoestima/4320303/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/ingrid-guimaraes-revela-que-tinha-baixa-autoestima-quando-era-adolescente/4320315/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/rede-globo/encontro-com-fatima-bernardes/t/programa/v/michelli-esta-com-123-quilos-e-aprendeu-a-ser-feliz-com-o-proprio-corpo/4320339/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais<strong> vÃ­deos</strong><span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div data-position="middle"><div><script>OAS_AD("Middle");</script><noscript><a href='http://ads.globo.com/RealMedia/ads/click_nx.ads/globo.com/globo.com/home@Middle'><img src='http://ads.globo.com/RealMedia/ads/adstream_nx.ads/globo.com/globo.com/home@Middle'></a></noscript></div></div></div></div></div></section></div><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div class="opec-internal"><script>OAS_MOBILE('x61');</script><noscript><a href='http://ads.globo.com/RealMedia/ads/click_nx.ads/globo.com/globo.com/home/@x61'><img src='http://ads.globo.com/RealMedia/ads/adstream_nx.ads/globo.com/globo.com/home/@x61'></a></noscript></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/santos-regiao/noticia/2015/07/ex-jogadora-de-futebol-e-presa-por-trafico-e-tenta-subornar-pms-em-sp.html" class="foto" title="Ex-jogadora de futebol Ã© presa com drogas e tenta subornar PMs em SP (Arquivo Pessoal)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/zD48nGw_FZsmCP474egfRyBfkwA=/filters:quality(10):strip_icc()/s2.glbimg.com/sWcBdIfo-hCK9XqtIKoif-V0Bcs=/0x160:930x659/335x180/s.glbimg.com/jo/g1/f/original/2015/07/14/11107723_931908790207335_1172546277669207278_n.jpg" alt="Ex-jogadora de futebol Ã© presa com drogas e tenta subornar PMs em SP (Arquivo Pessoal)" title="Ex-jogadora de futebol Ã© presa com drogas e tenta subornar PMs em SP (Arquivo Pessoal)"
                data-original-image="s2.glbimg.com/sWcBdIfo-hCK9XqtIKoif-V0Bcs=/0x160:930x659/335x180/s.glbimg.com/jo/g1/f/original/2015/07/14/11107723_931908790207335_1172546277669207278_n.jpg" data-url-smart_horizontal="2cPS1qB9k1wa6J7c0e3d5Dl3ZKA=/90x0/smart/filters:strip_icc()/" data-url-smart="2cPS1qB9k1wa6J7c0e3d5Dl3ZKA=/90x0/smart/filters:strip_icc()/" data-url-feature="2cPS1qB9k1wa6J7c0e3d5Dl3ZKA=/90x0/smart/filters:strip_icc()/" data-url-tablet="8NtFpyO-MHZB4PKukFRyEZtzvrA=/220x125/smart/filters:strip_icc()/" data-url-desktop="mTzq1cRVis3hrN169lATXIDk89s=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ex-jogadora de futebol Ã© presa com drogas e tenta subornar PMs em SP</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 21:27:13" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/mma/menina-de-13-anos-nocauteada-em-evento-de-mma-profissional-apoiado-por-upp-16761854.html" class="foto" title="No RJ, menina de 13 anos Ã© nocauteada em evento de MMA organizado por UPP  (reproduÃ§Ã£o )" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/hXrPP3dwq-k9mlUJckRhOnkf0As=/filters:quality(10):strip_icc()/s2.glbimg.com/fwWq6Dx-YuCJhQzbzNwz5fxYJng=/84x0:625x360/120x80/s.glbimg.com/en/ho/f/original/2015/07/14/mma-com-menor.jpg" alt="No RJ, menina de 13 anos Ã© nocauteada em evento de MMA organizado por UPP  (reproduÃ§Ã£o )" title="No RJ, menina de 13 anos Ã© nocauteada em evento de MMA organizado por UPP  (reproduÃ§Ã£o )"
                data-original-image="s2.glbimg.com/fwWq6Dx-YuCJhQzbzNwz5fxYJng=/84x0:625x360/120x80/s.glbimg.com/en/ho/f/original/2015/07/14/mma-com-menor.jpg" data-url-smart_horizontal="EuACwDLgdwBTOrMaZIr65ZW33rQ=/90x0/smart/filters:strip_icc()/" data-url-smart="EuACwDLgdwBTOrMaZIr65ZW33rQ=/90x0/smart/filters:strip_icc()/" data-url-feature="EuACwDLgdwBTOrMaZIr65ZW33rQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="WCtRxwYrOgPbsq7vTHVKJTaF3_c=/70x50/smart/filters:strip_icc()/" data-url-desktop="9MMlPJ3AJi9kdiDfkZox9V-cD7U=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>No RJ, menina de 13 anos Ã© nocauteada em evento de MMA organizado por UPP </p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/noticia/2015/07/justica-suspende-lei-que-proibe-venda-de-foie-gras-em-sao-paulo.html" class="foto" title="JustiÃ§a suspende lei que proÃ­be venda de foie gras em SÃ£o Paulo por &#39;crueldade&#39; (Stephanie Diani/The New York Times/Arquivo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/70pzFs-hEg36x-qh_vWJ5O4tyuA=/filters:quality(10):strip_icc()/s2.glbimg.com/zIHr9UlXjXr_0ACVZfsSh-tMMYw=/0x66:304x269/120x80/s.glbimg.com/jo/g1/f/original/2015/06/26/foiegras.jpg" alt="JustiÃ§a suspende lei que proÃ­be venda de foie gras em SÃ£o Paulo por &#39;crueldade&#39; (Stephanie Diani/The New York Times/Arquivo)" title="JustiÃ§a suspende lei que proÃ­be venda de foie gras em SÃ£o Paulo por &#39;crueldade&#39; (Stephanie Diani/The New York Times/Arquivo)"
                data-original-image="s2.glbimg.com/zIHr9UlXjXr_0ACVZfsSh-tMMYw=/0x66:304x269/120x80/s.glbimg.com/jo/g1/f/original/2015/06/26/foiegras.jpg" data-url-smart_horizontal="LSCudjDUWRxqpy3Px8bL99qgcbc=/90x0/smart/filters:strip_icc()/" data-url-smart="LSCudjDUWRxqpy3Px8bL99qgcbc=/90x0/smart/filters:strip_icc()/" data-url-feature="LSCudjDUWRxqpy3Px8bL99qgcbc=/90x0/smart/filters:strip_icc()/" data-url-tablet="vWGhKy4mtbifXGtV_VWjjJVOHAk=/70x50/smart/filters:strip_icc()/" data-url-desktop="2EYuabkZyhFeTimGMfAQ3Zv2lxA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>JustiÃ§a suspende lei que proÃ­be venda de foie gras em SÃ£o Paulo por &#39;crueldade&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 20:18:03" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/sao-carlos-regiao/noticia/2015/07/acusado-de-transmitir-o-virus-da-aids-de-proposito-para-3-mulheres-e-preso.html" class="" title="FuncionÃ¡rio pÃºblico do DF Ã© preso por passar vÃ­rus da Aids para trÃªs mulheres de propÃ³sito (reproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>FuncionÃ¡rio pÃºblico do DF Ã© preso por passar vÃ­rus da Aids para trÃªs mulheres de propÃ³sito</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/casos-de-policia/personal-trainer-detido-apos-agredir-ex-namorada-dentro-de-academia-16759517.html" class="foto" title="Personal trainer Ã© detido apÃ³s dar golpe que deixou a ex inconsciente; vÃ­deo (Extra)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/rVgLTXs-8lNhLPZCVFDdj3aR5nk=/filters:quality(10):strip_icc()/s2.glbimg.com/UCEAf6Q8z8jFRleSsT6vFDCaSng=/0x58:533x413/120x80/s.glbimg.com/en/ho/f/original/2015/07/14/agress_1.jpg" alt="Personal trainer Ã© detido apÃ³s dar golpe que deixou a ex inconsciente; vÃ­deo (Extra)" title="Personal trainer Ã© detido apÃ³s dar golpe que deixou a ex inconsciente; vÃ­deo (Extra)"
                data-original-image="s2.glbimg.com/UCEAf6Q8z8jFRleSsT6vFDCaSng=/0x58:533x413/120x80/s.glbimg.com/en/ho/f/original/2015/07/14/agress_1.jpg" data-url-smart_horizontal="easT719i0zBtkksW8b4zQccpmb0=/90x0/smart/filters:strip_icc()/" data-url-smart="easT719i0zBtkksW8b4zQccpmb0=/90x0/smart/filters:strip_icc()/" data-url-feature="easT719i0zBtkksW8b4zQccpmb0=/90x0/smart/filters:strip_icc()/" data-url-tablet="D64swUs6IKFK7j_6Nhtz4APTbDI=/70x50/smart/filters:strip_icc()/" data-url-desktop="VO24HG2TnJRp1lXYfGXvwuyrsHM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Personal trainer Ã© detido apÃ³s dar golpe que deixou a ex inconsciente; vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 11:11:19" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/review/tp-link-tl-wa850re.html" class="foto" title="Wi-Fi fraco? Confira aparelho &#39;baratinho&#39; que &#39;turbina&#39; sua Internet e veja se vale a pena (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/qSHmXVHkDMR3SCBP0YHQz0f11gM=/filters:quality(10):strip_icc()/s2.glbimg.com/lfC0qkjWurngl_ujDHvPx43sBUc=/0x4:504x340/120x80/s.glbimg.com/po/tt2/f/original/2015/07/14/repetidor-tp-link3.jpg" alt="Wi-Fi fraco? Confira aparelho &#39;baratinho&#39; que &#39;turbina&#39; sua Internet e veja se vale a pena (DivulgaÃ§Ã£o)" title="Wi-Fi fraco? Confira aparelho &#39;baratinho&#39; que &#39;turbina&#39; sua Internet e veja se vale a pena (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/lfC0qkjWurngl_ujDHvPx43sBUc=/0x4:504x340/120x80/s.glbimg.com/po/tt2/f/original/2015/07/14/repetidor-tp-link3.jpg" data-url-smart_horizontal="zkgqkUT6nZUaW7_hHBWX-KZG7LQ=/90x0/smart/filters:strip_icc()/" data-url-smart="zkgqkUT6nZUaW7_hHBWX-KZG7LQ=/90x0/smart/filters:strip_icc()/" data-url-feature="zkgqkUT6nZUaW7_hHBWX-KZG7LQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="qTRrGUePcGg8hRbU5q8WmBjm9q4=/70x50/smart/filters:strip_icc()/" data-url-desktop="4YCUcvekFRk4uWC1pbNSaz4MzPA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Wi-Fi fraco? Confira aparelho &#39;baratinho&#39; que &#39;turbina&#39; sua Internet e veja se vale a pena</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 21:13:19" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/carros/noticia/2015/07/honda-paga-us-25-milhoes-para-encerrar-caso-de-discriminacao.html" class="" title="Honda paga US$ 25 milhÃµes para encerrar processos de discriminaÃ§Ã£o nos Estados Unidos (Marcelo Brandt/G1)" rel="bookmark"><span class="conteudo"><p>Honda paga US$ 25 milhÃµes para encerrar processos de discriminaÃ§Ã£o nos Estados Unidos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 20:13:24" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/bahia/noticia/2015/07/homem-passa-mais-de-120-mil-trotes-em-7-meses-para-central-da-pm-na-ba.html" class="foto" title="600 por dia: Homem Ã© preso por passar 120 mil trotes para a polÃ­cia na Bahia; vÃ­deo (ReproduÃ§Ã£o/TV Bahia)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Kg9TJljYg_Mb3KeeOiocol0Z27A=/filters:quality(10):strip_icc()/s2.glbimg.com/okWvpLcPPGBZdawJSsjMLoVFJis=/130x59:327x190/120x80/s.glbimg.com/jo/g1/f/original/2015/07/14/sem_titulo_9.jpg" alt="600 por dia: Homem Ã© preso por passar 120 mil trotes para a polÃ­cia na Bahia; vÃ­deo (ReproduÃ§Ã£o/TV Bahia)" title="600 por dia: Homem Ã© preso por passar 120 mil trotes para a polÃ­cia na Bahia; vÃ­deo (ReproduÃ§Ã£o/TV Bahia)"
                data-original-image="s2.glbimg.com/okWvpLcPPGBZdawJSsjMLoVFJis=/130x59:327x190/120x80/s.glbimg.com/jo/g1/f/original/2015/07/14/sem_titulo_9.jpg" data-url-smart_horizontal="WNDIcqw459wqZFqS_zyG8kNXLW4=/90x0/smart/filters:strip_icc()/" data-url-smart="WNDIcqw459wqZFqS_zyG8kNXLW4=/90x0/smart/filters:strip_icc()/" data-url-feature="WNDIcqw459wqZFqS_zyG8kNXLW4=/90x0/smart/filters:strip_icc()/" data-url-tablet="VDwuF7PxUXO5SQhVJt4oCzLsDk4=/70x50/smart/filters:strip_icc()/" data-url-desktop="qMEaxDv_tMPXxk0bJxTPbdbxeuY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>600 por dia: Homem Ã© preso por passar 120 mil trotes para a polÃ­cia na Bahia; vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 20:18:56" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/bauru-marilia/noticia/2015/07/tres-caes-sao-encontrados-amarrados-e-desnutridos-em-casa-de-marilia.html" class="foto" title="Desnutridos, trÃªs cÃ£es sÃ£o resgatados amarrados e expostos a sol e chuva (DivulgaÃ§Ã£o / ONG DPAM)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/X2AWXfSKZCmvzXc3-DDNwXQ6O14=/filters:quality(10):strip_icc()/s2.glbimg.com/PGdB79YsrXgwZp7jD40wqamuchw=/233x104:571x329/120x80/s.glbimg.com/jo/g1/f/original/2015/07/14/cachorro.jpg" alt="Desnutridos, trÃªs cÃ£es sÃ£o resgatados amarrados e expostos a sol e chuva (DivulgaÃ§Ã£o / ONG DPAM)" title="Desnutridos, trÃªs cÃ£es sÃ£o resgatados amarrados e expostos a sol e chuva (DivulgaÃ§Ã£o / ONG DPAM)"
                data-original-image="s2.glbimg.com/PGdB79YsrXgwZp7jD40wqamuchw=/233x104:571x329/120x80/s.glbimg.com/jo/g1/f/original/2015/07/14/cachorro.jpg" data-url-smart_horizontal="ZZiab7kVNw1_k6lI5eNHTAgvd0E=/90x0/smart/filters:strip_icc()/" data-url-smart="ZZiab7kVNw1_k6lI5eNHTAgvd0E=/90x0/smart/filters:strip_icc()/" data-url-feature="ZZiab7kVNw1_k6lI5eNHTAgvd0E=/90x0/smart/filters:strip_icc()/" data-url-tablet="7hhSJdVl-ddpVOYfmqeDGBomB60=/70x50/smart/filters:strip_icc()/" data-url-desktop="ESMDa3CZTgp11he008lBkvMXcwM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Desnutridos, trÃªs cÃ£es sÃ£o resgatados amarrados e expostos a sol e chuva</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/em-versao-retro-de-craques-de-barcelona-e-real-madrid-messi-ganha-bigode.html" class="foto" title="Blog brinca com imagens e mostra como jogadores seriam no passado; veja fotos (ReproduÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/NHUFkDufWAUZbhqXv0IleqeC6bQ=/filters:quality(10):strip_icc()/s2.glbimg.com/iuwRmv3M6XyIU_Uhnf5bU1bdjeA=/32x164:503x417/335x180/s.glbimg.com/es/ge/f/original/2015/07/14/01-messi2.jpg" alt="Blog brinca com imagens e mostra como jogadores seriam no passado; veja fotos (ReproduÃ§Ã£o)" title="Blog brinca com imagens e mostra como jogadores seriam no passado; veja fotos (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/iuwRmv3M6XyIU_Uhnf5bU1bdjeA=/32x164:503x417/335x180/s.glbimg.com/es/ge/f/original/2015/07/14/01-messi2.jpg" data-url-smart_horizontal="Fkukw-t4OVwbeeqfuephuVIQfS8=/90x0/smart/filters:strip_icc()/" data-url-smart="Fkukw-t4OVwbeeqfuephuVIQfS8=/90x0/smart/filters:strip_icc()/" data-url-feature="Fkukw-t4OVwbeeqfuephuVIQfS8=/90x0/smart/filters:strip_icc()/" data-url-tablet="Iq4-eQOkI6kFOstsTyewsRjiSus=/220x125/smart/filters:strip_icc()/" data-url-desktop="B_4_mnv4KAyHkvyCftcG7uDBBoc=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Blog brinca com imagens e mostra como jogadores seriam no passado; veja fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 16:31:50" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/eventos/futebol-de-areia/noticia/2015/07/brasil-elimina-espanha-e-pega-campea-mundial-russia-nas-quartas-de-final.html" class="foto" title="Brasil elimina Espanha na areia e pega campeÃ£ mundial RÃºssia nas quartas de final (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ss1cWSC7WAUKhpnRjofSdxmyoiU=/filters:quality(10):strip_icc()/s2.glbimg.com/O5H_0NfIjePIEe7cJMoNE_APZCs=/118x106:560x400/120x80/s.glbimg.com/es/ge/f/original/2015/07/14/fute_de_areia_1.jpg" alt="Brasil elimina Espanha na areia e pega campeÃ£ mundial RÃºssia nas quartas de final (DivulgaÃ§Ã£o)" title="Brasil elimina Espanha na areia e pega campeÃ£ mundial RÃºssia nas quartas de final (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/O5H_0NfIjePIEe7cJMoNE_APZCs=/118x106:560x400/120x80/s.glbimg.com/es/ge/f/original/2015/07/14/fute_de_areia_1.jpg" data-url-smart_horizontal="6cEfh6t-za4bo8o-FqSxZo3XO8o=/90x0/smart/filters:strip_icc()/" data-url-smart="6cEfh6t-za4bo8o-FqSxZo3XO8o=/90x0/smart/filters:strip_icc()/" data-url-feature="6cEfh6t-za4bo8o-FqSxZo3XO8o=/90x0/smart/filters:strip_icc()/" data-url-tablet="YHsmsJ9jZB8gRV_nPO2PFhbKLqo=/70x50/smart/filters:strip_icc()/" data-url-desktop="QpwA6jWNI11f4GKj0dX2vtU4yf4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Brasil elimina Espanha na areia e pega campeÃ£ mundial <br />RÃºssia nas quartas de final</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 21:39:12" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/07/fla-se-reune-com-pai-de-felipe-melo-mas-valores-afastam-volante-da-gavea.html" class="foto" title="Felipe Melo pede R$ 1 milhÃ£o por mÃªs e valor dificulta ida para o Flamengo (ReproduÃ§Ã£o / Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/v7QkwJmfibHGOXMTNEpngWYVfYs=/filters:quality(10):strip_icc()/s2.glbimg.com/VnP7d9RgM0u3A5Og1ZnD2MPTbac=/133x56:497x298/120x80/s.glbimg.com/es/ge/f/original/2015/07/14/felipe_melo.jpg" alt="Felipe Melo pede R$ 1 milhÃ£o por mÃªs e valor dificulta ida para o Flamengo (ReproduÃ§Ã£o / Instagram)" title="Felipe Melo pede R$ 1 milhÃ£o por mÃªs e valor dificulta ida para o Flamengo (ReproduÃ§Ã£o / Instagram)"
                data-original-image="s2.glbimg.com/VnP7d9RgM0u3A5Og1ZnD2MPTbac=/133x56:497x298/120x80/s.glbimg.com/es/ge/f/original/2015/07/14/felipe_melo.jpg" data-url-smart_horizontal="WNGAvVe_4uXdZK5ytmvaRVGw5Es=/90x0/smart/filters:strip_icc()/" data-url-smart="WNGAvVe_4uXdZK5ytmvaRVGw5Es=/90x0/smart/filters:strip_icc()/" data-url-feature="WNGAvVe_4uXdZK5ytmvaRVGw5Es=/90x0/smart/filters:strip_icc()/" data-url-tablet="A-5fy1VzbfV2PBATwTUFKEkSvvw=/70x50/smart/filters:strip_icc()/" data-url-desktop="coyIpCk4-8oLwiu8H538po3kiIk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Felipe Melo pede R$ 1 milhÃ£o por mÃªs e valor dificulta ida para o Flamengo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 17:52:38" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/bastidores-fc/post/contrario-classico-com-duas-torcidas-vasco-veta-venda-em-sao-januario.html" class="" title="ContrÃ¡rio a clÃ¡ssico com o Flu com 2 torcidas, Vasco veta venda de ingressos em SÃ£o JanuÃ¡rio" rel="bookmark"><span class="conteudo"><p>ContrÃ¡rio a clÃ¡ssico com o Flu com 2 torcidas, Vasco veta venda de ingressos em SÃ£o JanuÃ¡rio</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 21:43:09" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/parana/noticia/2015/07/paranistas-posam-com-funkeiros-em-lanchonete-e-levam-bronca-da-diretoria.html" class="foto" title="ApÃ³s foto com funkeiros, jogadores do ParanÃ¡ levam bronca por sanduÃ­che (ReproduÃ§Ã£o/Facebook)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/s0otSqhOTe4aG-wgeDF2NfzH8wA=/filters:quality(10):strip_icc()/s2.glbimg.com/te-_uYBF4Mo9F5J3CnDRFjz-1-I=/70x0:799x485/120x80/s.glbimg.com/es/ge/f/original/2015/07/14/facebook_parana.jpg" alt="ApÃ³s foto com funkeiros, jogadores do ParanÃ¡ levam bronca por sanduÃ­che (ReproduÃ§Ã£o/Facebook)" title="ApÃ³s foto com funkeiros, jogadores do ParanÃ¡ levam bronca por sanduÃ­che (ReproduÃ§Ã£o/Facebook)"
                data-original-image="s2.glbimg.com/te-_uYBF4Mo9F5J3CnDRFjz-1-I=/70x0:799x485/120x80/s.glbimg.com/es/ge/f/original/2015/07/14/facebook_parana.jpg" data-url-smart_horizontal="R8LlPT5oQQ7cEm_xxiBfg3Ez4Uo=/90x0/smart/filters:strip_icc()/" data-url-smart="R8LlPT5oQQ7cEm_xxiBfg3Ez4Uo=/90x0/smart/filters:strip_icc()/" data-url-feature="R8LlPT5oQQ7cEm_xxiBfg3Ez4Uo=/90x0/smart/filters:strip_icc()/" data-url-tablet="2utQm9NDrbbYVa22p4vRHrAQxD4=/70x50/smart/filters:strip_icc()/" data-url-desktop="HOG4jf7aVi0-yJfCVnnX6G27RPA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ApÃ³s foto com funkeiros, jogadores do ParanÃ¡ levam bronca por sanduÃ­che</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 21:41:47" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/programas/sportv-news/noticia/2015/07/apos-saida-de-leo-moura-fort-lauderdale-acerta-com-lateral-gabriel.html" class="foto" title="ApÃ³s saÃ­da de LÃ©o Moura, Fort Lauderdale fecha com o ex-Fluminense Gabriel (Wesley Santos/Pressdigital)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/V2xp_3FFNR8oO3XrYBQ7tiMEIfE=/filters:quality(10):strip_icc()/s2.glbimg.com/pe-b27mTrOeSAS8Gzll4mIkXyGs=/333x23:566x178/120x80/s.glbimg.com/es/ge/f/original/2012/01/27/carrossel_gabriellateral.jpg" alt="ApÃ³s saÃ­da de LÃ©o Moura, Fort Lauderdale fecha com o ex-Fluminense Gabriel (Wesley Santos/Pressdigital)" title="ApÃ³s saÃ­da de LÃ©o Moura, Fort Lauderdale fecha com o ex-Fluminense Gabriel (Wesley Santos/Pressdigital)"
                data-original-image="s2.glbimg.com/pe-b27mTrOeSAS8Gzll4mIkXyGs=/333x23:566x178/120x80/s.glbimg.com/es/ge/f/original/2012/01/27/carrossel_gabriellateral.jpg" data-url-smart_horizontal="QZgRiFASxGgztPPSqYcknXIjaeY=/90x0/smart/filters:strip_icc()/" data-url-smart="QZgRiFASxGgztPPSqYcknXIjaeY=/90x0/smart/filters:strip_icc()/" data-url-feature="QZgRiFASxGgztPPSqYcknXIjaeY=/90x0/smart/filters:strip_icc()/" data-url-tablet="HWoB5Fgt6b0CI5CgWU21uyNaUW4=/70x50/smart/filters:strip_icc()/" data-url-desktop="QE1aX6ght8mn9qdA9tnxYe8AP70=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ApÃ³s saÃ­da de LÃ©o Moura, Fort Lauderdale fecha com o ex-Fluminense Gabriel</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 17:38:22" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/santos/noticia/2015/07/santos-acumula-dois-meses-de-salarios-atrasados-para-jogadores.html" class="" title="Sem patrocÃ­nio master, Santos nÃ£o paga salÃ¡rios hÃ¡ dois meses e pode atÃ© perder atletas" rel="bookmark"><span class="conteudo"><p>Sem patrocÃ­nio master, Santos nÃ£o paga salÃ¡rios hÃ¡ dois meses e pode atÃ© perder atletas</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 18:48:38" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/07/alimentos-arrecadados-em-festa-por-tevez-sao-saqueados-da-bombonera.html" class="foto" title="Parte da comida arrecada em festa para Tevez Ã© saqueada na Bombonera (AP)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/w0rXx7D8t6nbaSU3UhfrbOCsVlM=/filters:quality(10):strip_icc()/s2.glbimg.com/mMWA5GumQ_JVMf2CZhxIKVeRWlk=/1921x1033:3831x2307/120x80/s.glbimg.com/es/ge/f/original/2015/07/13/carlos_tevez_boca_juniors_4.jpg" alt="Parte da comida arrecada em festa para Tevez Ã© saqueada na Bombonera (AP)" title="Parte da comida arrecada em festa para Tevez Ã© saqueada na Bombonera (AP)"
                data-original-image="s2.glbimg.com/mMWA5GumQ_JVMf2CZhxIKVeRWlk=/1921x1033:3831x2307/120x80/s.glbimg.com/es/ge/f/original/2015/07/13/carlos_tevez_boca_juniors_4.jpg" data-url-smart_horizontal="1uq_A6d6GwYFPbIE2TXmU-YQWDs=/90x0/smart/filters:strip_icc()/" data-url-smart="1uq_A6d6GwYFPbIE2TXmU-YQWDs=/90x0/smart/filters:strip_icc()/" data-url-feature="1uq_A6d6GwYFPbIE2TXmU-YQWDs=/90x0/smart/filters:strip_icc()/" data-url-tablet="D7cDUmMH9tvv0cu9WcmbGWX3Z34=/70x50/smart/filters:strip_icc()/" data-url-desktop="ZOUdR4Jnt-FS_V-nvkhGVvm4uY4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Parte da comida arrecada em festa para Tevez Ã© saqueada na Bombonera</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 21:40:48" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/botafogo/sindicato-de-atletas-estuda-forma-de-ajudar-reduzir-suspensao-de-jobson-junto-fifa-16762635.html" class="foto" title="Sindicato estuda como diminuir puniÃ§Ã£o do atacante Jobson junto a Fifa (ReproduÃ§Ã£o/SporTV)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/NOJWbNqO6g8dxvFA949iLYKZ1S8=/filters:quality(10):strip_icc()/s2.glbimg.com/TTo4w_AgPNqLA9BKnRDOTZqkG7E=/50x0:230x120/120x80/s.glbimg.com/jo/g1/f/original/2015/04/24/botafogo-jobson-suspenso.jpg" alt="Sindicato estuda como diminuir puniÃ§Ã£o do atacante Jobson junto a Fifa (ReproduÃ§Ã£o/SporTV)" title="Sindicato estuda como diminuir puniÃ§Ã£o do atacante Jobson junto a Fifa (ReproduÃ§Ã£o/SporTV)"
                data-original-image="s2.glbimg.com/TTo4w_AgPNqLA9BKnRDOTZqkG7E=/50x0:230x120/120x80/s.glbimg.com/jo/g1/f/original/2015/04/24/botafogo-jobson-suspenso.jpg" data-url-smart_horizontal="zZu0JN9eYq0S9CIrEQJRWgLEWPg=/90x0/smart/filters:strip_icc()/" data-url-smart="zZu0JN9eYq0S9CIrEQJRWgLEWPg=/90x0/smart/filters:strip_icc()/" data-url-feature="zZu0JN9eYq0S9CIrEQJRWgLEWPg=/90x0/smart/filters:strip_icc()/" data-url-tablet="nObLmRs-wsZ7ZSYzc1TKyMP001E=/70x50/smart/filters:strip_icc()/" data-url-desktop="WR0yu3nfJh1_dL_K5SCdWLHDF0Y=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Sindicato estuda como diminuir puniÃ§Ã£o do atacante Jobson junto a Fifa</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/paula-fernandes-exibe-corpao-enquanto-malha.html" class="foto" title="Paula Fernandes posta foto do seu dia de malhaÃ§Ã£o: &#39;Trabalhei braÃ§o, glÃºteos...&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/ypNruPVzvBFXelaZur1mQ4aI5I8=/filters:quality(10):strip_icc()/s2.glbimg.com/YBf_xcXjvbaol6c3MWGsenY1hVQ=/0x48:640x392/335x180/e.glbimg.com/og/ed/f/original/2015/07/14/paula_fernandes.jpg" alt="Paula Fernandes posta foto do seu dia de malhaÃ§Ã£o: &#39;Trabalhei braÃ§o, glÃºteos...&#39; (ReproduÃ§Ã£o)" title="Paula Fernandes posta foto do seu dia de malhaÃ§Ã£o: &#39;Trabalhei braÃ§o, glÃºteos...&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/YBf_xcXjvbaol6c3MWGsenY1hVQ=/0x48:640x392/335x180/e.glbimg.com/og/ed/f/original/2015/07/14/paula_fernandes.jpg" data-url-smart_horizontal="Ri7B0ua4qiwRiDHRF8L8FXlb-qc=/90x0/smart/filters:strip_icc()/" data-url-smart="Ri7B0ua4qiwRiDHRF8L8FXlb-qc=/90x0/smart/filters:strip_icc()/" data-url-feature="Ri7B0ua4qiwRiDHRF8L8FXlb-qc=/90x0/smart/filters:strip_icc()/" data-url-tablet="6tncJKXP5w5blf9DH0NLs7Bg0Co=/220x125/smart/filters:strip_icc()/" data-url-desktop="OKUeidLqUkkA0X4B1wgCGDFHpZc=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Paula Fernandes posta foto do seu dia de malhaÃ§Ã£o: &#39;Trabalhei braÃ§o, glÃºteos...&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 21:40:26" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/alinne-moraes-passeia-com-filho-e-marido-em-shopping.html" class="foto" title="De volta Ã s novelas, Alinne Moraes passeia com o filho e o marido em shopping (AgNews)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/OD8UWo4t53Sfe8Kzn6VfUu6AlaE=/filters:quality(10):strip_icc()/s2.glbimg.com/gUOLuA82t51qXdGLduJ_d9KpnxE=/67x75:527x381/120x80/e.glbimg.com/og/ed/f/original/2015/07/14/alinne_moraes_e_familia_6.jpg" alt="De volta Ã s novelas, Alinne Moraes passeia com o filho e o marido em shopping (AgNews)" title="De volta Ã s novelas, Alinne Moraes passeia com o filho e o marido em shopping (AgNews)"
                data-original-image="s2.glbimg.com/gUOLuA82t51qXdGLduJ_d9KpnxE=/67x75:527x381/120x80/e.glbimg.com/og/ed/f/original/2015/07/14/alinne_moraes_e_familia_6.jpg" data-url-smart_horizontal="Q8ZXubswGhLGiasspq_mdp2I4hE=/90x0/smart/filters:strip_icc()/" data-url-smart="Q8ZXubswGhLGiasspq_mdp2I4hE=/90x0/smart/filters:strip_icc()/" data-url-feature="Q8ZXubswGhLGiasspq_mdp2I4hE=/90x0/smart/filters:strip_icc()/" data-url-tablet="FLMvWdjUIRVCcpg7lrXGbAErKCQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="fn89fjtAFbjsldS34Ctq4VMrWIg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>De volta Ã s novelas, Alinne Moraes passeia com o filho <br />e o marido em shopping</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/gravidez/noticia/2015/07/preta-gil-vai-ser-avo-de-uma-menina.html" class="foto" title="GrÃ¡vida de 5 meses do filho de Preta Gil, modelo de 17 anos espera uma menina (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ZUWW5UyGY_FlwqJbTbTvbL6JAQ8=/filters:quality(10):strip_icc()/s2.glbimg.com/tY0r2_aoOq4hgkm-nWhrLfJgTq4=/0x135:640x562/120x80/s.glbimg.com/jo/eg/f/original/2015/07/14/11018365_1428880770742588_154023299_n.jpg" alt="GrÃ¡vida de 5 meses do filho de Preta Gil, modelo de 17 anos espera uma menina (ReproduÃ§Ã£o/Instagram)" title="GrÃ¡vida de 5 meses do filho de Preta Gil, modelo de 17 anos espera uma menina (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/tY0r2_aoOq4hgkm-nWhrLfJgTq4=/0x135:640x562/120x80/s.glbimg.com/jo/eg/f/original/2015/07/14/11018365_1428880770742588_154023299_n.jpg" data-url-smart_horizontal="p9_GRifSoWbR9oE_fexr2mMBNdg=/90x0/smart/filters:strip_icc()/" data-url-smart="p9_GRifSoWbR9oE_fexr2mMBNdg=/90x0/smart/filters:strip_icc()/" data-url-feature="p9_GRifSoWbR9oE_fexr2mMBNdg=/90x0/smart/filters:strip_icc()/" data-url-tablet="dGJ3rY0e0OUzqWEyAy404mwbTIw=/70x50/smart/filters:strip_icc()/" data-url-desktop="QELLNpMsgFaCCSlXeFNd7hOETrw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>GrÃ¡vida de 5 meses do filho de Preta Gil, modelo de 17 anos espera uma menina</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 16:18:02" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/latino-e-rayanne-morais-se-separam.html" class="" title="Latino divulga carta e confirma separaÃ§Ã£o de Rayanne: &#39;Verdade foi enterrada no culto&#39;" rel="bookmark"><span class="conteudo"><p>Latino divulga carta e confirma separaÃ§Ã£o de Rayanne: &#39;Verdade foi enterrada no culto&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 17:45:21" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/07/mariana-felicio-esta-esperando-seu-segundo-filho-presente-de-deus.html" class="foto" title="MÃ£e de um bebÃª, ex-BBB Mariana FelÃ­cio estÃ¡ grÃ¡vida do 2Âº filho: &#39;Presente de Deus&#39; (Instagram / ReproduÃ§Ã£o -DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/QWWR-DNvaGXyp1iFvlK7yh2a2Fc=/filters:quality(10):strip_icc()/s2.glbimg.com/Wf6Z752KFRLjXiUE08Ke4hFMsiA=/2x0:279x185/120x80/s.glbimg.com/jo/eg/f/original/2015/07/14/felicio_1.jpg" alt="MÃ£e de um bebÃª, ex-BBB Mariana FelÃ­cio estÃ¡ grÃ¡vida do 2Âº filho: &#39;Presente de Deus&#39; (Instagram / ReproduÃ§Ã£o -DivulgaÃ§Ã£o)" title="MÃ£e de um bebÃª, ex-BBB Mariana FelÃ­cio estÃ¡ grÃ¡vida do 2Âº filho: &#39;Presente de Deus&#39; (Instagram / ReproduÃ§Ã£o -DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/Wf6Z752KFRLjXiUE08Ke4hFMsiA=/2x0:279x185/120x80/s.glbimg.com/jo/eg/f/original/2015/07/14/felicio_1.jpg" data-url-smart_horizontal="C7dIyMlmRDsnfGn0dwToNAiRflc=/90x0/smart/filters:strip_icc()/" data-url-smart="C7dIyMlmRDsnfGn0dwToNAiRflc=/90x0/smart/filters:strip_icc()/" data-url-feature="C7dIyMlmRDsnfGn0dwToNAiRflc=/90x0/smart/filters:strip_icc()/" data-url-tablet="Ckw5ufwBRJVuCWxQdooedkXt3lk=/70x50/smart/filters:strip_icc()/" data-url-desktop="pklHwrlXhuxvs0ysXCr0MboxGxQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>MÃ£e de um bebÃª, ex-BBB Mariana FelÃ­cio estÃ¡ grÃ¡vida do 2Âº filho: &#39;Presente de Deus&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/luciana-vendramini-aos-44-anos-posta-foto-de-lingerie-nao-sou-magra-16762038.html" class="foto" title="Aos 44 anos, Luciana Vendramini posta foto e mostra que segue linda (reproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/-x2yj_0EOC_NnN0rmV-jfr3C8yY=/filters:quality(10):strip_icc()/s2.glbimg.com/WPnbMLxeMEPPYrRmpmDuqk2vK9A=/0x0:448x298/120x80/s.glbimg.com/en/ho/f/original/2015/07/14/vendramini.jpg" alt="Aos 44 anos, Luciana Vendramini posta foto e mostra que segue linda (reproduÃ§Ã£o)" title="Aos 44 anos, Luciana Vendramini posta foto e mostra que segue linda (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/WPnbMLxeMEPPYrRmpmDuqk2vK9A=/0x0:448x298/120x80/s.glbimg.com/en/ho/f/original/2015/07/14/vendramini.jpg" data-url-smart_horizontal="uZ6-vn-wTsjohLZrTpGB3aMslLU=/90x0/smart/filters:strip_icc()/" data-url-smart="uZ6-vn-wTsjohLZrTpGB3aMslLU=/90x0/smart/filters:strip_icc()/" data-url-feature="uZ6-vn-wTsjohLZrTpGB3aMslLU=/90x0/smart/filters:strip_icc()/" data-url-tablet="nuFoJVYxVjEg3pJZLK9ddXLNxA4=/70x50/smart/filters:strip_icc()/" data-url-desktop="I4ZlDM7J5fvIK4KHacPOZRewMHU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Aos 44 anos, Luciana Vendramini posta foto e mostra que segue linda</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 20:23:52" class="chamada chamada-principal mobile-grid-full"><a href="http://oglobo.globo.com/cultura/revista-da-tv/christiane-pelajo-fala-sobre-volta-tv-apos-cair-do-cavalo-minha-maior-preocupacao-era-ficar-boa-16762213" class="" title="Christiane Pelajo celebra volta ao &#39;Jornal da Globo&#39; apÃ³s cair de cavalo e passar por cirurgia (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class="conteudo"><p>Christiane Pelajo celebra volta ao &#39;Jornal da Globo&#39; apÃ³s cair de cavalo e passar por cirurgia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://canalviva.globo.com/programas/mais-da-tv/materias/peter-brandao-lembra-o-gente-inocente-e-comenta-babilonia.html" class="foto" title="Lembra dele? Ex-Gente Inocente celebra papel em BabilÃ´nia: &#39;Maravilhoso&#39; (Arquivo pessoal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ahTQmsptZl0RPp6OMy2mnMPRT60=/filters:quality(10):strip_icc()/s2.glbimg.com/xHKY3Op6WfbIf5wJi7NBCBxnd70=/99x67:524x350/120x80/g.glbimg.com/og/gs/gsat2/f/original/2015/07/14/peter.png" alt="Lembra dele? Ex-Gente Inocente celebra papel em BabilÃ´nia: &#39;Maravilhoso&#39; (Arquivo pessoal)" title="Lembra dele? Ex-Gente Inocente celebra papel em BabilÃ´nia: &#39;Maravilhoso&#39; (Arquivo pessoal)"
                data-original-image="s2.glbimg.com/xHKY3Op6WfbIf5wJi7NBCBxnd70=/99x67:524x350/120x80/g.glbimg.com/og/gs/gsat2/f/original/2015/07/14/peter.png" data-url-smart_horizontal="qGF-7LoV6GEue3C119fmKaUBcYU=/90x0/smart/filters:strip_icc()/" data-url-smart="qGF-7LoV6GEue3C119fmKaUBcYU=/90x0/smart/filters:strip_icc()/" data-url-feature="qGF-7LoV6GEue3C119fmKaUBcYU=/90x0/smart/filters:strip_icc()/" data-url-tablet="NZirwTRI62t7VcD934tfztNSXRE=/70x50/smart/filters:strip_icc()/" data-url-desktop="VyBJPCxOfQlom_IJfkITPlyAvS0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Lembra dele? Ex-Gente Inocente celebra papel em BabilÃ´nia: &#39;Maravilhoso&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-14 20:22:10" class="chamada chamada-principal mobile-grid-full"><a href="http://casavogue.globo.com/LazerCultura/Comida-bebida/Receita/noticia/2015/07/ovos-escoceses-para-sair-da-dieta.html" class="foto" title="Bolovo vocÃª sabe fazer, mas e a versÃ£o vinda direto da EscÃ³cia? Veja a receita aqui (divulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Fc4jGtbfGTPY2WTqxyj_hsdARvQ=/filters:quality(10):strip_icc()/s2.glbimg.com/7LZ-_MYiE4fuZp73MJM7eXEfyGs=/78x111:485x382/120x80/s.glbimg.com/en/ho/f/original/2015/07/14/ovo_1.jpg" alt="Bolovo vocÃª sabe fazer, mas e a versÃ£o vinda direto da EscÃ³cia? Veja a receita aqui (divulgaÃ§Ã£o)" title="Bolovo vocÃª sabe fazer, mas e a versÃ£o vinda direto da EscÃ³cia? Veja a receita aqui (divulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/7LZ-_MYiE4fuZp73MJM7eXEfyGs=/78x111:485x382/120x80/s.glbimg.com/en/ho/f/original/2015/07/14/ovo_1.jpg" data-url-smart_horizontal="POdgSFMkpK9inTZHKgzkxTJqtVY=/90x0/smart/filters:strip_icc()/" data-url-smart="POdgSFMkpK9inTZHKgzkxTJqtVY=/90x0/smart/filters:strip_icc()/" data-url-feature="POdgSFMkpK9inTZHKgzkxTJqtVY=/90x0/smart/filters:strip_icc()/" data-url-tablet="nzniyKfSFjetuMTOrLV9YYFI8T8=/70x50/smart/filters:strip_icc()/" data-url-desktop="soiUmxYUMM1AVtT4hVP3bxaH06c=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Bolovo vocÃª sabe fazer, mas e a versÃ£o vinda direto da EscÃ³cia? Veja a receita aqui</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div data-position="middle3"><div><script>OAS_AD("Middle3")</script></div></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2014/index.html", "logo": "http://s2.glbimg.com/kX8ylHaY-BqxGG2GesdMgR7SxQc=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_38x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2014/rss", "logo_tv": "http://s2.glbimg.com/tKm4gbARSp2eHSq6zr2alVV82hc=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2014/index.html", "logo": "http://s2.glbimg.com/kX8ylHaY-BqxGG2GesdMgR7SxQc=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_38x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2014/rss", "logo_tv": "http://s2.glbimg.com/tKm4gbARSp2eHSq6zr2alVV82hc=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior "><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="tecnologia &amp; games"><span class="word word-0">tecnologia</span><span class="word word-1">&</span><span class="word word-2">games</span></a></h2><div id="ad-position-x34" class="opec"><div data-position="x34"><script>OAS_RICH("x34")</script></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/07/conheca-oito-novas-funcoes-do-ios-9-que-nao-existiam-no-ios-8.html" title="iOS 9: veja inovaÃ§Ãµes que podem mudar seu jeito de usar o iPhone"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/kIJgfGoCyxID7j0tDfdL1SZhL2A=/filters:quality(10):strip_icc()/s2.glbimg.com/ymyF80kVDv9Pynosot15yvUvB14=/0x0:546x289/245x130/s.glbimg.com/po/tt2/f/original/2015/06/11/captura_de_tela_2015-06-11_as_11.32.05.png" alt="iOS 9: veja inovaÃ§Ãµes que podem mudar seu jeito de usar o iPhone (TechTudo)" title="iOS 9: veja inovaÃ§Ãµes que podem mudar seu jeito de usar o iPhone (TechTudo)"
             data-original-image="s2.glbimg.com/ymyF80kVDv9Pynosot15yvUvB14=/0x0:546x289/245x130/s.glbimg.com/po/tt2/f/original/2015/06/11/captura_de_tela_2015-06-11_as_11.32.05.png" data-url-smart_horizontal="xfxbtQVsEMK5QjnTWoqs9xmB73I=/90x56/smart/filters:strip_icc()/" data-url-smart="xfxbtQVsEMK5QjnTWoqs9xmB73I=/90x56/smart/filters:strip_icc()/" data-url-feature="xfxbtQVsEMK5QjnTWoqs9xmB73I=/90x56/smart/filters:strip_icc()/" data-url-tablet="eT23KLUyPGj0wYRc1m_I-WmukIM=/160x96/smart/filters:strip_icc()/" data-url-desktop="8ptdxJHLk28yZDfn1FV0tz1T06U=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>iOS 9: veja inovaÃ§Ãµes que podem mudar seu jeito de usar o iPhone</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/07/app-gratis-simula-livros-de-colorir-para-adultos-no-iphone-e-no-ipad.html" title="Febre do momento, livros de colorir chegam grÃ¡tis para iPhone"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/vUVyjVlK4c_k-AuGfFTV3ByfN3I=/filters:quality(10):strip_icc()/s2.glbimg.com/ZYZ16dTEXI0t7YPztLWS2l7s_ds=/174x96:695x372/245x130/s.glbimg.com/po/tt2/f/original/2015/07/14/iphone_5.jpg" alt="Febre do momento, livros de colorir chegam grÃ¡tis para iPhone (Luciana Maline/TechTudo)" title="Febre do momento, livros de colorir chegam grÃ¡tis para iPhone (Luciana Maline/TechTudo)"
             data-original-image="s2.glbimg.com/ZYZ16dTEXI0t7YPztLWS2l7s_ds=/174x96:695x372/245x130/s.glbimg.com/po/tt2/f/original/2015/07/14/iphone_5.jpg" data-url-smart_horizontal="PyQnuz0zhzLvvfYQjSOUDlJMbdk=/90x56/smart/filters:strip_icc()/" data-url-smart="PyQnuz0zhzLvvfYQjSOUDlJMbdk=/90x56/smart/filters:strip_icc()/" data-url-feature="PyQnuz0zhzLvvfYQjSOUDlJMbdk=/90x56/smart/filters:strip_icc()/" data-url-tablet="aSSyUYp1mYkO5TptOXOrgTQ1788=/160x96/smart/filters:strip_icc()/" data-url-desktop="FV4C-KwL77Bld6e0Ti64YuHjxeY=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Febre do momento, livros de <br />colorir chegam grÃ¡tis para iPhone</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/07/impressora-superportatil-para-celulares-funciona-como-bateria-extra-veja.html" title="ConheÃ§a impressora para smarts que ainda carrega a sua bateria"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/TKv-hyoHsTyA3nP2xa7f5SJ_fbg=/filters:quality(10):strip_icc()/s2.glbimg.com/QROXew6v48eqhLKccz8O_zahuOU=/48x30:647x348/245x130/s.glbimg.com/po/tt2/f/original/2015/07/14/4dc0da317026847571674107def4faac_original.jpg" alt="ConheÃ§a impressora para smarts que ainda carrega a sua bateria (DivulgaÃ§Ã£o)" title="ConheÃ§a impressora para smarts que ainda carrega a sua bateria (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/QROXew6v48eqhLKccz8O_zahuOU=/48x30:647x348/245x130/s.glbimg.com/po/tt2/f/original/2015/07/14/4dc0da317026847571674107def4faac_original.jpg" data-url-smart_horizontal="-kCh3Fg8qgtM_C_QcU4WgNbiviA=/90x56/smart/filters:strip_icc()/" data-url-smart="-kCh3Fg8qgtM_C_QcU4WgNbiviA=/90x56/smart/filters:strip_icc()/" data-url-feature="-kCh3Fg8qgtM_C_QcU4WgNbiviA=/90x56/smart/filters:strip_icc()/" data-url-tablet="VK42IeMYn9JO9zoTx7thBHUdBV0=/160x96/smart/filters:strip_icc()/" data-url-desktop="ARFRqxvqB9gItJMMJzi6H6HnNAM=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>ConheÃ§a impressora para smarts que ainda carrega a sua bateria</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/07/multilaser-lanca-concorrente-baratinha-da-gopro-com-wi-fi-e-tela-lcd.html" title="Rival brasileira da GoPro chega &#39;baratinha&#39; com Wi-Fi e tela LCD"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/9pu5Xt14JtPWAP2eswKV-S0O73U=/filters:quality(10):strip_icc()/s2.glbimg.com/PwZr0f03hvTUkLdR4y_4YcRGjAU=/0x0:849x450/245x130/s.glbimg.com/po/tt2/f/original/2015/07/14/fullsport_cam_1_1.jpg" alt="Rival brasileira da GoPro chega &#39;baratinha&#39; com Wi-Fi e tela LCD (DivulgaÃ§Ã£o)" title="Rival brasileira da GoPro chega &#39;baratinha&#39; com Wi-Fi e tela LCD (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/PwZr0f03hvTUkLdR4y_4YcRGjAU=/0x0:849x450/245x130/s.glbimg.com/po/tt2/f/original/2015/07/14/fullsport_cam_1_1.jpg" data-url-smart_horizontal="W_Va-IeMF5e8FWZAspj9DswVOqY=/90x56/smart/filters:strip_icc()/" data-url-smart="W_Va-IeMF5e8FWZAspj9DswVOqY=/90x56/smart/filters:strip_icc()/" data-url-feature="W_Va-IeMF5e8FWZAspj9DswVOqY=/90x56/smart/filters:strip_icc()/" data-url-tablet="XOEYwnqyjl55HWKkT2iEofjcIKI=/160x96/smart/filters:strip_icc()/" data-url-desktop="Kxfm9m3M1ami5LJDkzq5EWulz3M=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Rival brasileira da GoPro chega &#39;baratinha&#39; com Wi-Fi e tela LCD</p></span></a></li></ul><div class="destaque-inferior analytics-area analytics-id-B"><ul><li class="techtudo"><a href="http://www.techtudo.com.br/" title="Teste agora a velocidade da sua internet"><span class="logo"></span><span class="title">Teste agora a velocidade da sua internet &rsaquo;</span></a></li><li class="exty"><a href="http://www.techtudo.com.br/tudo-sobre/extensao-globo-com.html" title="Adicione e veja as notÃ­cias em tempo real"><span class="logo"></span><span class="title">Adicione e veja as notÃ­cias em tempo real &rsaquo;</span></a></li></ul></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://ela.oglobo.globo.com/beleza/as-novas-tecnologias-que-corrigem-todos-os-defeitinhos-da-pele-do-rosto-16747166" alt="&#39;Efeito Photoshop&#39; e mais produtos que corrigem defeitinhos da pele do rosto"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/OVZdjTh93EdMeHWfEOQfO8Nu_dE=/filters:quality(10):strip_icc()/s2.glbimg.com/CrIVOFdCfeEbUX9pwiccSQwzyuw=/0x76:463x375/155x100/s.glbimg.com/en/ho/f/original/2015/07/14/sombra-marrom.jpg" alt="&#39;Efeito Photoshop&#39; e mais produtos que corrigem defeitinhos da pele do rosto (ReproduÃ§Ã£o)" title="&#39;Efeito Photoshop&#39; e mais produtos que corrigem defeitinhos da pele do rosto (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/CrIVOFdCfeEbUX9pwiccSQwzyuw=/0x76:463x375/155x100/s.glbimg.com/en/ho/f/original/2015/07/14/sombra-marrom.jpg" data-url-smart_horizontal="3IT0-p5ajsaGkQyHnZlImee1y58=/90x56/smart/filters:strip_icc()/" data-url-smart="3IT0-p5ajsaGkQyHnZlImee1y58=/90x56/smart/filters:strip_icc()/" data-url-feature="3IT0-p5ajsaGkQyHnZlImee1y58=/90x56/smart/filters:strip_icc()/" data-url-tablet="0cFogly7S07Q2giTHx1_L084r3w=/122x75/smart/filters:strip_icc()/" data-url-desktop="YpCSbkJYIDi3N3QkBVWqq0fOaXc=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>nova geraÃ§Ã£o de cosmÃ©ticos</h3><p>&#39;Efeito Photoshop&#39; e mais produtos que corrigem defeitinhos da pele do rosto</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/bem-estar/materias/desafio-dos-30-dias-conheca-vantagens-e-desvantagens-do-programa-de-exercicios.htm" alt="ConheÃ§a prÃ³s e contras do programa de exercÃ­cios que Ã© moda na web"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/UIsxlIq3Dy3tbxT_s0tYdWCHFLI=/filters:quality(10):strip_icc()/s2.glbimg.com/nnLebEdp-5Hgdb_3LrwTMHeYdio=/0x0:620x400/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/07/13/620_istock_000035655532_xxxlarge.jpg" alt="ConheÃ§a prÃ³s e contras do programa de exercÃ­cios que Ã© moda na web (Getty Images)" title="ConheÃ§a prÃ³s e contras do programa de exercÃ­cios que Ã© moda na web (Getty Images)"
            data-original-image="s2.glbimg.com/nnLebEdp-5Hgdb_3LrwTMHeYdio=/0x0:620x400/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/07/13/620_istock_000035655532_xxxlarge.jpg" data-url-smart_horizontal="e-qO5if4147rYj6-HB-KV9mQwUk=/90x56/smart/filters:strip_icc()/" data-url-smart="e-qO5if4147rYj6-HB-KV9mQwUk=/90x56/smart/filters:strip_icc()/" data-url-feature="e-qO5if4147rYj6-HB-KV9mQwUk=/90x56/smart/filters:strip_icc()/" data-url-tablet="ALZ8ZT8pLKa1zVlu90SXkykoGIw=/122x75/smart/filters:strip_icc()/" data-url-desktop="pxF7-Xpc-Z3Mo1KMUCUqphLsO1k=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>&#39;Desafio dos 30 dias&#39;</h3><p>ConheÃ§a prÃ³s e contras do programa de exercÃ­cios que Ã© moda na web</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/moda-news/noticia/2015/07/maxibrinco-e-tendencia-para-festas-segundo-couture-de-inverno-2016.html" alt="Grifes apostam em maxibrinco para festas na prÃ³xima estaÃ§Ã£o; fotos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/oeS4y7tkKexXQGEW2068dbppgbE=/filters:quality(10):strip_icc()/s2.glbimg.com/Yn-yxFIjb1BeJlY6jkKfWcoDxWg=/0x138:1734x1257/155x100/s.glbimg.com/en/ho/f/original/2015/07/14/maxibrinco.jpg" alt="Grifes apostam em maxibrinco para festas na prÃ³xima estaÃ§Ã£o; fotos (Getty Images)" title="Grifes apostam em maxibrinco para festas na prÃ³xima estaÃ§Ã£o; fotos (Getty Images)"
            data-original-image="s2.glbimg.com/Yn-yxFIjb1BeJlY6jkKfWcoDxWg=/0x138:1734x1257/155x100/s.glbimg.com/en/ho/f/original/2015/07/14/maxibrinco.jpg" data-url-smart_horizontal="gN5aUi71isTZ2fcdaLF5eY_bxNc=/90x56/smart/filters:strip_icc()/" data-url-smart="gN5aUi71isTZ2fcdaLF5eY_bxNc=/90x56/smart/filters:strip_icc()/" data-url-feature="gN5aUi71isTZ2fcdaLF5eY_bxNc=/90x56/smart/filters:strip_icc()/" data-url-tablet="gaZPZ-JbttDUphZ7o8dwxcEA31g=/122x75/smart/filters:strip_icc()/" data-url-desktop="ulaG2egsqbaMQ7COC92p2AkzNXM=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>tem coragem?</h3><p>Grifes apostam em maxibrinco para festas na prÃ³xima estaÃ§Ã£o; fotos</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Jardim/Decoracao/Moveis/Sofa/fotos/2015/07/sofa-em-l-como-e-quando-usar.html" alt="SofÃ¡ em &#39;L&#39; economiza espaÃ§o e Ã© superconfortÃ¡vel; veja formas de usar"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/J5E73OvbTGFP5rgj7I_Q2u6SmC4=/filters:quality(10):strip_icc()/s2.glbimg.com/MPQVw-LD73fwefKPN4kAAmU0A1Y=/0x260:507x587/155x100/e.glbimg.com/og/ed/f/original/2013/07/31/45-cj669d_76.jpg" alt="SofÃ¡ em &#39;L&#39; economiza espaÃ§o e Ã© superconfortÃ¡vel; veja formas de usar (Marcelo Magnani/Casa e Jardim)" title="SofÃ¡ em &#39;L&#39; economiza espaÃ§o e Ã© superconfortÃ¡vel; veja formas de usar (Marcelo Magnani/Casa e Jardim)"
            data-original-image="s2.glbimg.com/MPQVw-LD73fwefKPN4kAAmU0A1Y=/0x260:507x587/155x100/e.glbimg.com/og/ed/f/original/2013/07/31/45-cj669d_76.jpg" data-url-smart_horizontal="VKzu1OgDMzOZHc_kFOssO8K74iE=/90x56/smart/filters:strip_icc()/" data-url-smart="VKzu1OgDMzOZHc_kFOssO8K74iE=/90x56/smart/filters:strip_icc()/" data-url-feature="VKzu1OgDMzOZHc_kFOssO8K74iE=/90x56/smart/filters:strip_icc()/" data-url-tablet="XipdH-MvXSrLgVWMZyikTfLuLPk=/122x75/smart/filters:strip_icc()/" data-url-desktop="JOARJs4doeP2S36iI65MvA0d6IY=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>lugar para todos</h3><p>SofÃ¡ em &#39;L&#39; economiza espaÃ§o e Ã© superconfortÃ¡vel; veja formas de usar</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/apartamentos/noticia/2015/07/texturas-e-cores-no-lar-de-220-m.html" alt="Cores claras predominam em apÃª de 220 mÂ² reformado em SP; ambientes"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/jHjDAXIdvWzzUZRHlvddQaQ5FXM=/filters:quality(10):strip_icc()/s2.glbimg.com/BOF2eanS7WZzwi7fb1i071Axewg=/18x0:618x387/155x100/e.glbimg.com/og/ed/f/original/2015/07/07/apartamento-roberta-banqueri-tatuape-19.jpg" alt="Cores claras predominam em apÃª de 220 mÂ² reformado em SP; ambientes (Gabriel Arantes / divulgaÃ§Ã£o)" title="Cores claras predominam em apÃª de 220 mÂ² reformado em SP; ambientes (Gabriel Arantes / divulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/BOF2eanS7WZzwi7fb1i071Axewg=/18x0:618x387/155x100/e.glbimg.com/og/ed/f/original/2015/07/07/apartamento-roberta-banqueri-tatuape-19.jpg" data-url-smart_horizontal="_unBaS4u2WJ9107jJ9DesQCsZh8=/90x56/smart/filters:strip_icc()/" data-url-smart="_unBaS4u2WJ9107jJ9DesQCsZh8=/90x56/smart/filters:strip_icc()/" data-url-feature="_unBaS4u2WJ9107jJ9DesQCsZh8=/90x56/smart/filters:strip_icc()/" data-url-tablet="vl6S4CvoOzcU54lipCIE3uPfR1s=/122x75/smart/filters:strip_icc()/" data-url-desktop="mO2iwFMMnQDwrxO80DKDOUBF1Hs=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>leveza na metrÃ³pole</h3><p>Cores claras predominam em apÃª de 220 mÂ² reformado em SP; ambientes</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/porta-retrato-diferente-para-suas-fotos/" alt="Porta-retratos feitos com objetos inusitados Ã© opÃ§Ã£o barata e criativa"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/vjAzbXQy9yVO4mwkU6LdburSGuw=/filters:quality(10):strip_icc()/s2.glbimg.com/AvOHwyp9d2UUuMQfUBJ7XLcTUp4=/0x0:558x360/155x100/s.glbimg.com/en/ho/f/original/2015/07/14/foto-roda-de-bicicleta_1.jpg" alt="Porta-retratos feitos com objetos inusitados Ã© opÃ§Ã£o barata e criativa (ReproduÃ§Ã£o/Pinterest)" title="Porta-retratos feitos com objetos inusitados Ã© opÃ§Ã£o barata e criativa (ReproduÃ§Ã£o/Pinterest)"
            data-original-image="s2.glbimg.com/AvOHwyp9d2UUuMQfUBJ7XLcTUp4=/0x0:558x360/155x100/s.glbimg.com/en/ho/f/original/2015/07/14/foto-roda-de-bicicleta_1.jpg" data-url-smart_horizontal="tSqOJs8ZyCoC5Pq8oCxAi6EHVuo=/90x56/smart/filters:strip_icc()/" data-url-smart="tSqOJs8ZyCoC5Pq8oCxAi6EHVuo=/90x56/smart/filters:strip_icc()/" data-url-feature="tSqOJs8ZyCoC5Pq8oCxAi6EHVuo=/90x56/smart/filters:strip_icc()/" data-url-tablet="M9SUCAVgvGu9w5W2pE4u4_zLUis=/122x75/smart/filters:strip_icc()/" data-url-desktop="IQWEnD65EMH_aD_o2P5U3aMzpN8=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>sem dinheiro?</h3><p>Porta-retratos feitos com objetos inusitados Ã© opÃ§Ã£o barata e criativa</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/07/veja-mais-imagens-do-ensaio-de-tati-zaqui-na-playboy.html" title="VÃ­deo mostra novas imagens de Tati Zaqui posando pelada; veja"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/Ki3BhciRbZpv6al4iki6KwTgBoU=/filters:quality(10):strip_icc()/s2.glbimg.com/NJPKtkamubvAsx6xanVk3MoQg3g=/0x0:335x177/245x130/s.glbimg.com/en/ho/f/original/2015/07/14/tati.jpg" alt="VÃ­deo mostra novas imagens de Tati Zaqui posando pelada; veja (divulgaÃ§Ã£o)" title="VÃ­deo mostra novas imagens de Tati Zaqui posando pelada; veja (divulgaÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/NJPKtkamubvAsx6xanVk3MoQg3g=/0x0:335x177/245x130/s.glbimg.com/en/ho/f/original/2015/07/14/tati.jpg" data-url-smart_horizontal="c3WWDJcZPnx4J8WPUDO8uLXAPX0=/90x56/smart/filters:strip_icc()/" data-url-smart="c3WWDJcZPnx4J8WPUDO8uLXAPX0=/90x56/smart/filters:strip_icc()/" data-url-feature="c3WWDJcZPnx4J8WPUDO8uLXAPX0=/90x56/smart/filters:strip_icc()/" data-url-tablet="AyqP7nRMYmCj7t4SQLhRcCwF7rs=/160x95/smart/filters:strip_icc()/" data-url-desktop="GuAoTWBSVOHAgHiVmuSBD7icCxU=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>VÃ­deo mostra novas imagens de Tati Zaqui posando pelada; veja</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Celebridades/noticia/2015/07/luiza-brunet-aparece-nua-em-foto-antiga.html" title="Luiza Brunet posta foto antiga sem roupa em Ibiza; amplie aqui"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/RQWee8QwTWQeCXWRRzPK98v_CgA=/filters:quality(10):strip_icc()/s2.glbimg.com/PXEAGQhnIgY54nFox0Q4KXReDDI=/100x14:432x191/245x130/s.glbimg.com/en/ho/f/original/2015/07/14/brunet.jpg" alt="Luiza Brunet posta foto antiga sem roupa em Ibiza; amplie aqui (reproduÃ§Ã£o)" title="Luiza Brunet posta foto antiga sem roupa em Ibiza; amplie aqui (reproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/PXEAGQhnIgY54nFox0Q4KXReDDI=/100x14:432x191/245x130/s.glbimg.com/en/ho/f/original/2015/07/14/brunet.jpg" data-url-smart_horizontal="z0SEBMETaXNOfKfdBQ-bcPk7jDk=/90x56/smart/filters:strip_icc()/" data-url-smart="z0SEBMETaXNOfKfdBQ-bcPk7jDk=/90x56/smart/filters:strip_icc()/" data-url-feature="z0SEBMETaXNOfKfdBQ-bcPk7jDk=/90x56/smart/filters:strip_icc()/" data-url-tablet="tb8Vz1yzJJ4riNuSNNn-uVtby7k=/160x95/smart/filters:strip_icc()/" data-url-desktop="-5-26mc5HJrhW2dwOYadKpR5hFE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Luiza Brunet posta foto antiga sem roupa em Ibiza; amplie aqui</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://multishow.globo.com/programas/vai-que-cola/materias/alexandre-pato-prestigia-namorada-fiorella-mattheis-em-gravacao-do-vai-que-cola.htm" title="Fiorella grava e recebe a visita de Pato: &#39;Amei que vocÃª veio"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/DpxdTarTsRmO5mvB3TU3AzIph7M=/filters:quality(10):strip_icc()/s2.glbimg.com/h63yugEriSpIy7gi2f45o576dX0=/0x0:335x177/245x130/s.glbimg.com/en/ho/f/original/2015/07/14/pato_1.jpg" alt="Fiorella grava e recebe a visita de Pato: &#39;Amei que vocÃª veio (reproduÃ§Ã£o)" title="Fiorella grava e recebe a visita de Pato: &#39;Amei que vocÃª veio (reproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/h63yugEriSpIy7gi2f45o576dX0=/0x0:335x177/245x130/s.glbimg.com/en/ho/f/original/2015/07/14/pato_1.jpg" data-url-smart_horizontal="JYAzNI38PxSiBmpy60yhLRUzTaU=/90x56/smart/filters:strip_icc()/" data-url-smart="JYAzNI38PxSiBmpy60yhLRUzTaU=/90x56/smart/filters:strip_icc()/" data-url-feature="JYAzNI38PxSiBmpy60yhLRUzTaU=/90x56/smart/filters:strip_icc()/" data-url-tablet="o8zeQHGH2emyVk0smLp2fhSCBEs=/160x95/smart/filters:strip_icc()/" data-url-desktop="kBslFBKGyih2f8Cj5r5XWDUQH5U=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Fiorella grava e recebe a visita de Pato: &#39;Amei que vocÃª veio</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/maira-charken-delegata-de-babilonia-exibe-flexibilidade.html" title="Delegata de &#39;BabilÃ´nia&#39; mostra incrÃ­vel flexibilidade em aula"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/T3kYUeH6Hgndt_OfxFXe4qPZ_VU=/filters:quality(10):strip_icc()/s2.glbimg.com/EX-YoCSWt9kAM72M4pzd4ygmOTw=/0x205:640x545/245x130/e.glbimg.com/og/ed/f/original/2015/07/14/maira_charken.jpg" alt="Delegata de &#39;BabilÃ´nia&#39; mostra incrÃ­vel flexibilidade em aula (ReproduÃ§Ã£o)" title="Delegata de &#39;BabilÃ´nia&#39; mostra incrÃ­vel flexibilidade em aula (ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/EX-YoCSWt9kAM72M4pzd4ygmOTw=/0x205:640x545/245x130/e.glbimg.com/og/ed/f/original/2015/07/14/maira_charken.jpg" data-url-smart_horizontal="Hvv45K_togwG60NjfkIqcRV8LrE=/90x56/smart/filters:strip_icc()/" data-url-smart="Hvv45K_togwG60NjfkIqcRV8LrE=/90x56/smart/filters:strip_icc()/" data-url-feature="Hvv45K_togwG60NjfkIqcRV8LrE=/90x56/smart/filters:strip_icc()/" data-url-tablet="Mv3VcGKQxbIFtoyr55j8RFM6as0=/160x95/smart/filters:strip_icc()/" data-url-desktop="8wb_nch14noxHaex1mcCrtjuBbI=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Delegata de &#39;BabilÃ´nia&#39; mostra incrÃ­vel flexibilidade em aula</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/babilonia/index.html">BABILÃNIA</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2014/index.html">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/07/patricia-questiona-margot-se-gravidez-foi-proposital.html" title="&#39;I Love ParaisÃ³polis&#39;: PatrÃ­cia questiona Margot sobre gravidez"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/d77n7fqvVTYjhx6Ob9fcc0tacmI=/filters:quality(10):strip_icc()/s2.glbimg.com/CusQL83F2R1mlI5i6BStChgUBCU=/0x62:690x428/245x130/s.glbimg.com/et/gs/f/original/2015/07/14/margot-patricia.jpg" alt="&#39;I Love ParaisÃ³polis&#39;: PatrÃ­cia questiona Margot sobre gravidez (ThaÃ­s Dias/Gshow)" title="&#39;I Love ParaisÃ³polis&#39;: PatrÃ­cia questiona Margot sobre gravidez (ThaÃ­s Dias/Gshow)"
                    data-original-image="s2.glbimg.com/CusQL83F2R1mlI5i6BStChgUBCU=/0x62:690x428/245x130/s.glbimg.com/et/gs/f/original/2015/07/14/margot-patricia.jpg" data-url-smart_horizontal="rb_v6Q6uqL8h6GLxXxvaWaMDVDs=/90x56/smart/filters:strip_icc()/" data-url-smart="rb_v6Q6uqL8h6GLxXxvaWaMDVDs=/90x56/smart/filters:strip_icc()/" data-url-feature="rb_v6Q6uqL8h6GLxXxvaWaMDVDs=/90x56/smart/filters:strip_icc()/" data-url-tablet="IMU0Sh76YgsfROvkDYLfJhHSQLY=/160x95/smart/filters:strip_icc()/" data-url-desktop="esk_JGHCIxE62KAUE7PVlV9vvmw=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;I Love ParaisÃ³polis&#39;: PatrÃ­cia questiona Margot sobre gravidez</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2014/Vem-por-ai/noticia/2015/07/casamentocobrade-jade-entra-na-igreja-e-todos-ficam-emocionados.html" title="&#39;MalhaÃ§Ã£o&#39;: Jade entra na igreja e todos ficam emocionados "><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/6L8qeCPY68JN88Ab3Q7QZJTe0-I=/filters:quality(10):strip_icc()/s2.glbimg.com/To8uz8A9hU8hYebFb-J1S-zKZIU=/44x53:637x368/245x130/s.glbimg.com/et/gs/f/original/2015/07/13/jade_entra_na_igreja_acompanhada_por_lucrecia_e_edgard.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Jade entra na igreja e todos ficam emocionados  (Raphael Dias/Gshow)" title="&#39;MalhaÃ§Ã£o&#39;: Jade entra na igreja e todos ficam emocionados  (Raphael Dias/Gshow)"
                    data-original-image="s2.glbimg.com/To8uz8A9hU8hYebFb-J1S-zKZIU=/44x53:637x368/245x130/s.glbimg.com/et/gs/f/original/2015/07/13/jade_entra_na_igreja_acompanhada_por_lucrecia_e_edgard.jpg" data-url-smart_horizontal="CTA6723bMd1086NPuZSsV24DpQE=/90x56/smart/filters:strip_icc()/" data-url-smart="CTA6723bMd1086NPuZSsV24DpQE=/90x56/smart/filters:strip_icc()/" data-url-feature="CTA6723bMd1086NPuZSsV24DpQE=/90x56/smart/filters:strip_icc()/" data-url-tablet="K4cnUflkXpNmcYYqQJEFfR0v3tE=/160x95/smart/filters:strip_icc()/" data-url-desktop="oqO62MMFQL6CompPRkZzApA9oDI=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Jade entra na igreja e todos ficam emocionados </p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/07/vitoria-expulsa-emilia-de-campobello-e-cozinheira-faz-ameaca.html" title="&#39;AlÃ©m do Tempo&#39;: VitÃ³ria expulsa EmÃ­lia de cidade e Ã© ameaÃ§ada"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ZL-6lys2P9h8RPLQdBp60-zNUQg=/filters:quality(10):strip_icc()/s2.glbimg.com/yNmb5l7Afm48X_si_ZOdNgg22Ww=/174x61:690x334/245x130/s.glbimg.com/et/gs/f/original/2015/07/14/emilia-ana-beatriz-nogueira-alem-do-tempo.jpg" alt="&#39;AlÃ©m do Tempo&#39;: VitÃ³ria expulsa EmÃ­lia de cidade e Ã© ameaÃ§ada (TV Globo)" title="&#39;AlÃ©m do Tempo&#39;: VitÃ³ria expulsa EmÃ­lia de cidade e Ã© ameaÃ§ada (TV Globo)"
                    data-original-image="s2.glbimg.com/yNmb5l7Afm48X_si_ZOdNgg22Ww=/174x61:690x334/245x130/s.glbimg.com/et/gs/f/original/2015/07/14/emilia-ana-beatriz-nogueira-alem-do-tempo.jpg" data-url-smart_horizontal="siv7hsExJPquhvSSsm_1uG4Wflg=/90x56/smart/filters:strip_icc()/" data-url-smart="siv7hsExJPquhvSSsm_1uG4Wflg=/90x56/smart/filters:strip_icc()/" data-url-feature="siv7hsExJPquhvSSsm_1uG4Wflg=/90x56/smart/filters:strip_icc()/" data-url-tablet="_I3ZZzMvf-r5dMvWaNJTjfqdKmY=/160x95/smart/filters:strip_icc()/" data-url-desktop="DsdZb1sRSB0Q__NH5hr7yd2IbA0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;AlÃ©m do Tempo&#39;: VitÃ³ria expulsa EmÃ­lia de cidade e Ã© ameaÃ§ada</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/07/barbara-paz-aparece-suja-entre-trapos-e-mendigos-para-gravacao-de-regra-do-jogo.html" title="BÃ¡rbara Paz aparece suja entre mendigos em gravaÃ§Ã£o; imagens"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/uxolzTm7XAaMBo6a3-bNxefgCOA=/filters:quality(10):strip_icc()/s2.glbimg.com/tXdlNuOrs-QQAB8hmC1SZWdLFwA=/0x59:690x425/245x130/s.glbimg.com/et/gs/f/original/2015/07/14/barbara-paz-a-regra-do-jogo_1.jpg" alt="BÃ¡rbara Paz aparece suja entre mendigos em gravaÃ§Ã£o; imagens (Ellen Soares / Gshow)" title="BÃ¡rbara Paz aparece suja entre mendigos em gravaÃ§Ã£o; imagens (Ellen Soares / Gshow)"
                    data-original-image="s2.glbimg.com/tXdlNuOrs-QQAB8hmC1SZWdLFwA=/0x59:690x425/245x130/s.glbimg.com/et/gs/f/original/2015/07/14/barbara-paz-a-regra-do-jogo_1.jpg" data-url-smart_horizontal="yJbUh0m9FzmJyvXqOlcNpx9CSSc=/90x56/smart/filters:strip_icc()/" data-url-smart="yJbUh0m9FzmJyvXqOlcNpx9CSSc=/90x56/smart/filters:strip_icc()/" data-url-feature="yJbUh0m9FzmJyvXqOlcNpx9CSSc=/90x56/smart/filters:strip_icc()/" data-url-tablet="rJVmEOps5O6wZeW7Pf0OkzMcjLU=/160x95/smart/filters:strip_icc()/" data-url-desktop="IjBlgERtDwIxyTltvGRFZJNbh6c=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>BÃ¡rbara Paz aparece suja entre mendigos em gravaÃ§Ã£o; imagens</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://revistamonet.globo.com/Musica/noticia/2015/07/jack-black-e-boy-george-fazem-cover-de-hello-i-love-you-com-guitarrista-dos-doors.html" title="Boy George e Jack Black tocam clÃ¡ssico do The Doors com o guitarrista da banda (reproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/l40k8dASHdzFsQSYIz0qIMUZtHk=/filters:quality(10):strip_icc()/s2.glbimg.com/zpOigN6vTY_d922P9RmGeE94edY=/39x16:282x164/215x130/s.glbimg.com/en/ho/f/original/2015/07/14/boy.jpg"
                data-original-image="s2.glbimg.com/zpOigN6vTY_d922P9RmGeE94edY=/39x16:282x164/215x130/s.glbimg.com/en/ho/f/original/2015/07/14/boy.jpg" data-url-smart_horizontal="t9U1AkPE_P4Tpleb35NGW9UUekc=/90x56/smart/filters:strip_icc()/" data-url-smart="t9U1AkPE_P4Tpleb35NGW9UUekc=/90x56/smart/filters:strip_icc()/" data-url-feature="t9U1AkPE_P4Tpleb35NGW9UUekc=/90x56/smart/filters:strip_icc()/" data-url-tablet="3cx1XfPucIKabz6-_IZD3QeazPo=/120x80/smart/filters:strip_icc()/" data-url-desktop="ttyl1Wk6bPRC11wHIYC3d5MC5JA=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Boy George e Jack Black tocam clÃ¡ssico do The Doors com o guitarrista da banda</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/musica/noticia/2015/07/mc-vilaozin-de-6-anos-e-incluido-em-inquerito-do-mp-sobre-funkeiros-mirins.html" title="MC VilÃ£ozin, de 6 anos, estÃ¡ em inquÃ©rito sobre funkeiros mirins por &#39;Tapa na bunda&#39; (DivulgaÃ§Ã£o / RP Produtora)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/r683dA9vxYJOvnZzyiqJuFaEKFM=/filters:quality(10):strip_icc()/s2.glbimg.com/-X0cyzDQBKgHk9iNSG9KPETrrtE=/115x88:513x328/215x130/s.glbimg.com/jo/g1/f/original/2015/07/13/mc-vilaozin.jpg"
                data-original-image="s2.glbimg.com/-X0cyzDQBKgHk9iNSG9KPETrrtE=/115x88:513x328/215x130/s.glbimg.com/jo/g1/f/original/2015/07/13/mc-vilaozin.jpg" data-url-smart_horizontal="hHdXprCKwZGA4mWZrB8ca9QcN4E=/90x56/smart/filters:strip_icc()/" data-url-smart="hHdXprCKwZGA4mWZrB8ca9QcN4E=/90x56/smart/filters:strip_icc()/" data-url-feature="hHdXprCKwZGA4mWZrB8ca9QcN4E=/90x56/smart/filters:strip_icc()/" data-url-tablet="_8Cdp17ApnGrQRmGmaZa_vLC4-4=/120x80/smart/filters:strip_icc()/" data-url-desktop="IZBn1vwBRh4PIKU0Z4Uxy7vYYug=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">MC VilÃ£ozin, de 6 anos, estÃ¡ em inquÃ©rito sobre funkeiros mirins por &#39;Tapa na bunda&#39;</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Musica/noticia/2015/07/malta-comemora-um-ano-de-sucesso-somos-autossuficientes.html" title="Malta comemora um ano de vitÃ³ria no SuperStar e dÃ¡ seu obrigado aos fÃ£s da banda (Ag. News)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/a9q3LqkWW6EWnXtbKL40W0WaQv8=/filters:quality(10):strip_icc()/s2.glbimg.com/fSjXTBSf5FOaxX8J_rInTrsBNcg=/122x0:1650x924/215x130/s.glbimg.com/jo/eg/f/original/2015/07/13/img_4411.jpg"
                data-original-image="s2.glbimg.com/fSjXTBSf5FOaxX8J_rInTrsBNcg=/122x0:1650x924/215x130/s.glbimg.com/jo/eg/f/original/2015/07/13/img_4411.jpg" data-url-smart_horizontal="KnTiniZRP0OB9DfKxmnnc8c76ZM=/90x56/smart/filters:strip_icc()/" data-url-smart="KnTiniZRP0OB9DfKxmnnc8c76ZM=/90x56/smart/filters:strip_icc()/" data-url-feature="KnTiniZRP0OB9DfKxmnnc8c76ZM=/90x56/smart/filters:strip_icc()/" data-url-tablet="WYEJuf-EW_TKxtYY2IR2hNXkgK0=/120x80/smart/filters:strip_icc()/" data-url-desktop="y2Xg7hqPJyZwmRbL36y0oLjIP3w=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Malta comemora um ano de vitÃ³ria no SuperStar e dÃ¡ seu obrigado aos fÃ£s da banda</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/07/pf-apreende-ferrari-e-lamborghini-na-residencia-de-collor-em-brasilia.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">PolÃ­cia Federal apreende Ferrari, Porsche<br /> e Lamborghini na casa de Collor</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://oglobo.globo.com/blogs/pagenotfound/posts/2015/07/14/homem-acusado-de-matar-namorada-ao-fazer-sexo-dirigindo-carro-569376.asp" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Homem Ã© acusado de matar a namorada <br />ao fazer sexo enquanto dirigia carro</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/noticia/2015/07/carro-248-kmh-e-o-mais-rapido-ja-registrado-por-um-radar-em-sp.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Radar nÃ£o consegue registrar placa nem modelo do carro que fez 248 km/h em SP</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/sorocaba-jundiai/noticia/2015/07/falsa-medica-e-descoberta-apos-abandonar-plantao-em-pronto-socorro.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Falsa mÃ©dica Ã© descoberta apÃ³s abandonar plantÃ£o em pronto-socorro</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/mato-grosso-do-sul/noticia/2015/07/pai-das-quadrigemeas-e-solto-e-nega-abandono-foi-questao-de-segundos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Pai das quadrigÃªmeas Ã© solto e nega abandono: &#39;foi questÃ£o de segundos&#39;</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/jogos-pan-americanos/noticia/2015/07/polemica-choro-amor-italiano-e-prata-ingrid-rouba-cena-no-inicio-do-pan.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">AlvoroÃ§o com corpaÃ§o, salto nota zero e prata: Ingrid Oliveira rouba a cena no Pan</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/jogos-pan-americanos/noticia/2015/07/isaquias-queiroz-vence-c1-200m-e-conquista-segundo-ouro-no-pan-2015.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Embalado pelo vento, Isaquias vence C1 200m e conquista mais um ouro</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/jogos-pan-americanos/noticia/2015/07/atento-treinador-cuida-de-perto-de-fase-adolescente-do-xodo-flavinha.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Atento, treinador cuida de perto de fase adolescente do xodÃ³ Flavinha</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/07/revista-lista-os-50-craques-para-2020-com-5-brasileiros-e-marquinhos-em-2.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Revista lista os 50 craques para 2020 com<br /> 5 brasileiros; Marquinhos Ã© o 2Âº</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/jogos-pan-americanos/noticia/2015/07/canada-tem-ajudinha-e-dupla-do-brasil-divide-medalha-merito-e-nosso.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">CanadÃ¡ tem &#39;ajudinha&#39;, e dupla do Brasil divide medalha: &quot;MÃ©rito Ã© nosso&#39;</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/praia/noticia/2015/07/rita-guedes-entra-no-mar-de-roupa-de-ginastica.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Rita Guedes chama a atenÃ§Ã£o ao entrar no mar de calÃ§a legging e &#39;farol aceso&#39;</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/07/kelly-key-exibe-barriga-sarada-e-recebe-elogio-maravilhosa.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Kelly Key exibe barriga sarada em rede social e recebe elogio: &#39;Maravilhosa&#39;</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/verdades-secretas/Vem-por-ai/noticia/2015/07/angel-fica-aos-prantos-com-revelacao-de-fanny.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Em &#39;Verdades Secretas&#39;, Angel fica aos prantos com revelaÃ§Ã£o de Fanny</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistamarieclaire.globo.com/Celebridades/noticia/2015/07/karina-bacchi-mostra-boa-forma-so-posar-com-shortinho-e-top-em-viagen.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Karina Bacchi mostra boa forma ao posar com shortinho e top em viagem</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/07/mariana-felicio-esta-esperando-seu-segundo-filho-presente-de-deus.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">MÃ£e de um bebÃª, ex-BBB Mariana FelÃ­cio estÃ¡ grÃ¡vida do 2Âº filho: &#39;Presente de Deus&#39;</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://tvg.globo.com/programas/pe-na-cova/index.html">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://tvg.globo.com/programas/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div class="opec-internal"><script>OAS_MOBILE('x62');</script><noscript><a href='http://ads.globo.com/RealMedia/ads/click_nx.ads/globo.com/globo.com/home/@x62'><img src='http://ads.globo.com/RealMedia/ads/adstream_nx.ads/globo.com/globo.com/home/@x62'></a></noscript></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/1c5247b9b573.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><script>OAS_AD("Frame1")</script></div><script type='text/javascript'>
window.glb=window.glb || {};window.glb.analyticsConfig = {scrollEventChance: 50,
setDomainVar: true,
setLoggedVar: true,
setReferrerVar: true,
clicksAsEvents: false,
waitForAnalyticsTime: false};var _gaq=_gaq || [];_gaq.push(
['_setAccount', 'UA-296593-2'],
['_setDomainName','www.globo.com']);if(window.glb.analyticsConfig.setLoggedVar && !!document.cookie.match(/GLBID=.*?(;|$)/)){_gaq.push(['_setCustomVar', 11, 'Visita Logada', 'visita', 2],
['_setCustomVar', 12, 'Visitante Logado', 'visitante', 1]);}if(window.glb.analyticsConfig.waitForAnalyticsTime){(function(){try{var waitTime=localStorage.getItem('analytics-wait-time');if(waitTime){localStorage.removeItem('analytics-wait-time');_gaq.push(['_setCustomVar', 16, 'Tempo Clique', waitTime, 3]);}} catch(e){}}());}if(window.glb.analyticsConfig.setReferrerVar){(function(){var sessionData=document.cookie.match(/__utmb=10109875[.](.*?)[.].*?(;|$)/);if(sessionData){return;}
var referrer=document.referrer, domain;if(!referrer){domain = 'Direto';}else{domain=referrer.replace(/^https?:\/\//, '').split('/')[0];}
_gaq.push(['_setCustomVar', 13, 'Referencia Atual', domain, 2]);}());}
_gaq.push(
['_trackPageview'],
['b._setAccount', 'UA-296593-15'],
['b._setDomainName','.globo.com'],
['b._trackPageview']);(function(){var ga=document.createElement('script');ga.type = 'text/javascript';ga.async=true;ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);})();
</script><div id="mboxScriptContainer" style="display:none;"></div><script>
var PMObjGlobo = {Section : "pÃ¡gina inicial",
Page : "pÃ¡gina inicial"};</script><script src="http://s.glbimg.com/en/ho/static/componentes/tags/js/atm.js"></script><script>
(function(){var tc=new TagContainerLoader();tc.tagContainerDC="d1";
tc.tagContainerNamespace="globo";tc.tagContainerName="Globo";
tc.loadTagContainer();tc.t();}());</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 14/07/2015 21:59:32 -->
