<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.14" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.14" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.14"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.14"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.14"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.14"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><script>(function() {var _fbq = window._fbq || (window._fbq = []);if (!_fbq.loaded) {var fbds = document.createElement('script');fbds.async = true;fbds.src = '//connect.facebook.net/en_US/fbds.js';var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(fbds, s);_fbq.loaded = true;}_fbq.push(['addPixelId', '624029644406124']);})();window._fbq = window._fbq || [];window._fbq.push(['track', 'PixelInitialized', {}]);</script><noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=624029644406124&amp;ev=PixelInitialized" /></noscript>           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.14"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-09-10 06:10:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             1 day ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/culture/travel/i-drove-on-the-indiathailand-highway-and-its-not-all-fun-games-and-breathtaking-landscapes-244975.html" class=" tint" title="I Drove On The India-Thailand Highway And It Was Just As Awesome As You Imagined It!">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/travel-card2_1441687983_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/travel-card2_1441687983_236x111.jpg"  border="0" alt="I Drove On The India-Thailand Highway And It Was Just As Awesome As You Imagined It!"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/i-drove-on-the-indiathailand-highway-and-its-not-all-fun-games-and-breathtaking-landscapes-244975.html" title="I Drove On The India-Thailand Highway And It Was Just As Awesome As You Imagined It!">
                            I Drove On The India-Thailand Highway And It Was Just As Awesome As You Imagined It!                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/archeologists-may-just-have-discovered-the-lost-tomb-of-the-founder-of-bengaluru-245061.html" title="Archeologists May Just Have Discovered The Lost Tomb Of The Founder Of Bengaluru" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441803661_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441803661_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/archeologists-may-just-have-discovered-the-lost-tomb-of-the-founder-of-bengaluru-245061.html" title="Archeologists May Just Have Discovered The Lost Tomb Of The Founder Of Bengaluru">
                            Archeologists May Just Have Discovered The Lost Tomb Of The Founder Of Bengaluru                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/this-woman-walked-out-of-her-marriage-when-her-husband-said-no-to-sharing-the-bed-with-pet-dogs-245060.html" title="This Woman Walked Out Of Her Marriage When Her Husband Said No To Sharing The Bed With Pet Dogs" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/mar5onlife_1441802874_1441802879_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/mar5onlife_1441802874_1441802879_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-woman-walked-out-of-her-marriage-when-her-husband-said-no-to-sharing-the-bed-with-pet-dogs-245060.html" title="This Woman Walked Out Of Her Marriage When Her Husband Said No To Sharing The Bed With Pet Dogs">
                            This Woman Walked Out Of Her Marriage When Her Husband Said No To Sharing The Bed With Pet Dogs                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/this-new-york-born-fashionista-quit-her-job-to-become-a-nun-245059.html" title="This New York Born Fashionista Quit Her Job To Become A Nun!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/nun-502_1441801218_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/nun-502_1441801218_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-new-york-born-fashionista-quit-her-job-to-become-a-nun-245059.html" title="This New York Born Fashionista Quit Her Job To Become A Nun!">
                            This New York Born Fashionista Quit Her Job To Become A Nun!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/weird/doctors-go-crazy-at-homeopathy-conference-after-taking-a-psychedelic-drug-245058.html" title="Doctors Go Crazy At Homeopathy Conference After Taking A Psychedelic Drug" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/iprim-6_1441801207_1441801211_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/iprim-6_1441801207_1441801211_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/doctors-go-crazy-at-homeopathy-conference-after-taking-a-psychedelic-drug-245058.html" title="Doctors Go Crazy At Homeopathy Conference After Taking A Psychedelic Drug">
                            Doctors Go Crazy At Homeopathy Conference After Taking A Psychedelic Drug                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/7-signs-that-prove-indian-culture-celebrates-the-philosophy-of-taking-breaks-more-than-any-other-in-the-world-244777.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/7-signs-that-prove-indian-culture-celebrates-the-philosophy-of-taking-breaks-more-than-any-other-in-the-world-244777.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Sep/card_1441708325_1441708334_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card_1441708325_1441708334_502x234.jpg"  border="0" alt="7 Signs That Prove Indian Culture Celebrates The Philosophy Of Taking Breaks More Than Any Other In The World!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/7-signs-that-prove-indian-culture-celebrates-the-philosophy-of-taking-breaks-more-than-any-other-in-the-world-244777.html" title="7 Signs That Prove Indian Culture Celebrates The Philosophy Of Taking Breaks More Than Any Other In The World!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/7-signs-that-prove-indian-culture-celebrates-the-philosophy-of-taking-breaks-more-than-any-other-in-the-world-244777.html');">7 Signs That Prove Indian Culture Celebrates The Philosophy Of Taking Breaks More Than Any Other In The World!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/technology/these-8-apple-products-just-shook-the-world-of-tech-for-2015-and-beyond-245066.html" class="tint" title="These 8 Apple Products Just Shook The World Of Tech For 2015 And Beyond" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/technology/these-8-apple-products-just-shook-the-world-of-tech-for-2015-and-beyond-245066.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/card 502_1441828257_1441828269_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card 502_1441828257_1441828269_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/these-8-apple-products-just-shook-the-world-of-tech-for-2015-and-beyond-245066.html" title="These 8 Apple Products Just Shook The World Of Tech For 2015 And Beyond" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/technology/these-8-apple-products-just-shook-the-world-of-tech-for-2015-and-beyond-245066.html');">
                            These 8 Apple Products Just Shook The World Of Tech For 2015 And Beyond                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/recipes/9-corn-recipes-we-bet-youve-never-heard-of-245043.html" class="tint" title="9 Corn Recipes We Bet You've Never Heard Of" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/recipes/9-corn-recipes-we-bet-youve-never-heard-of-245043.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/corn_1441787307_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/corn_1441787307_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/recipes/9-corn-recipes-we-bet-youve-never-heard-of-245043.html" title="9 Corn Recipes We Bet You've Never Heard Of" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/recipes/9-corn-recipes-we-bet-youve-never-heard-of-245043.html');">
                            9 Corn Recipes We Bet You've Never Heard Of                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/remembering-captain-vikram-batra-pvc-â-the-bravest-of-them-all-245027.html" class="tint" title="Remembering Captain Vikram Batra, PVC â The Bravest Of Them All">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/vikram-card2_1441779776_1441779835_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/vikram-card2_1441779776_1441779835_218x102.jpg" border="0" alt="Remembering Captain Vikram Batra, PVC â The Bravest Of Them All"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/remembering-captain-vikram-batra-pvc-â-the-bravest-of-them-all-245027.html" title="Remembering Captain Vikram Batra, PVC â The Bravest Of Them All">
                            Remembering Captain Vikram Batra, PVC â The Bravest Of Them All                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/technology/these-8-apple-products-just-shook-the-world-of-tech-for-2015-and-beyond-245066.html" class="tint" title="These 8 Apple Products Just Shook The World Of Tech For 2015 And Beyond">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/card 502_1441828257_1441828269_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card 502_1441828257_1441828269_218x102.jpg" border="0" alt="These 8 Apple Products Just Shook The World Of Tech For 2015 And Beyond"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/these-8-apple-products-just-shook-the-world-of-tech-for-2015-and-beyond-245066.html" title="These 8 Apple Products Just Shook The World Of Tech For 2015 And Beyond">
                            These 8 Apple Products Just Shook The World Of Tech For 2015 And Beyond                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/after-sonam-kapoor-twitter-found-all-its-humor-in-sonakshi-sinhas-tweet-against-meat-ban-245039.html" class="tint" title="After Sonam Kapoor, Twitter Found All Its Humor In Sonakshi Sinha's Tweet Against Meat Ban">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/sonakshi-sinha---bollywoodgo net_1441785637_1441785646_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/sonakshi-sinha---bollywoodgo net_1441785637_1441785646_218x102.jpg" border="0" alt="After Sonam Kapoor, Twitter Found All Its Humor In Sonakshi Sinha's Tweet Against Meat Ban"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/after-sonam-kapoor-twitter-found-all-its-humor-in-sonakshi-sinhas-tweet-against-meat-ban-245039.html" title="After Sonam Kapoor, Twitter Found All Its Humor In Sonakshi Sinha's Tweet Against Meat Ban">
                            After Sonam Kapoor, Twitter Found All Its Humor In Sonakshi Sinha's Tweet Against Meat Ban                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-elderly-couples-sweet-reunion-at-the-airport-proves-that-love-has-no-age-limit-245021.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/this-elderly-couples-sweet-reunion-at-the-airport-proves-that-love-has-no-age-limit-245021.html" class="tint" title="This Elderly Couple's Sweet Reunion At The Airport Proves That Love Is Timeless">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Sep/love_card_1441732550_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Sep/love_card_1441732550_218x102.jpg" border="0" alt="This Elderly Couple's Sweet Reunion At The Airport Proves That Love Is Timeless"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-elderly-couples-sweet-reunion-at-the-airport-proves-that-love-has-no-age-limit-245021.html" title="This Elderly Couple's Sweet Reunion At The Airport Proves That Love Is Timeless">
                            This Elderly Couple's Sweet Reunion At The Airport Proves That Love Is Timeless                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/11-dialogues-from-andaaz-apna-apna-jaane-bhi-do-yaaro-thatll-actually-help-you-in-real-life-244976.html" class="tint" title="11 Dialogues From Andaaz Apna Apna & Jaane Bhi Do Yaaro That'll Actually Help You In Real Life!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/card2_1441781197_1441781204_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card2_1441781197_1441781204_218x102.jpg" border="0" alt="11 Dialogues From Andaaz Apna Apna & Jaane Bhi Do Yaaro That'll Actually Help You In Real Life!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/11-dialogues-from-andaaz-apna-apna-jaane-bhi-do-yaaro-thatll-actually-help-you-in-real-life-244976.html" title="11 Dialogues From Andaaz Apna Apna & Jaane Bhi Do Yaaro That'll Actually Help You In Real Life!">
                            11 Dialogues From Andaaz Apna Apna & Jaane Bhi Do Yaaro That'll Actually Help You In Real Life!                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            12 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/mumbais-new-police-commissioner-takes-re-1-as-salary-+-everything-you-want-to-know-about-him-245052.html" title="Mumbai's New Police Commissioner Takes Re 1 As Salary + Everything You Want To Know About Him!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/javed-502_1441797033_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/mumbais-new-police-commissioner-takes-re-1-as-salary-+-everything-you-want-to-know-about-him-245052.html" title="Mumbai's New Police Commissioner Takes Re 1 As Salary + Everything You Want To Know About Him!">
                            Mumbai's New Police Commissioner Takes Re 1 As Salary + Everything You Want To Know About Him!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            12 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/gujratis-in-vadodara-are-banking-on-lord-hanuman-to-revive-the-rupee-plan-to-decorate-temple-with-currency-worth-7-lakh-245050.html" title="Gujaratis In Vadodara Banking On Lord Hanuman To Revive The Rupee. Plan To Decorate Temple With Currency Worth 7 Lakh" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/mandir-502_1441793353_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/gujratis-in-vadodara-are-banking-on-lord-hanuman-to-revive-the-rupee-plan-to-decorate-temple-with-currency-worth-7-lakh-245050.html" title="Gujaratis In Vadodara Banking On Lord Hanuman To Revive The Rupee. Plan To Decorate Temple With Currency Worth 7 Lakh">
                            Gujaratis In Vadodara Banking On Lord Hanuman To Revive The Rupee. Plan To Decorate Temple With Currency Worth 7 Lakh                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/tired-of-waiting-for-his-ration-card-to-be-issued-old-man-strips-naked-in-front-of-female-officer-245037.html" title="Tired Of Waiting For His Ration Card To Be Issued, Old Man Strips Naked In Front Of Female Officer" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441784579_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/tired-of-waiting-for-his-ration-card-to-be-issued-old-man-strips-naked-in-front-of-female-officer-245037.html" title="Tired Of Waiting For His Ration Card To Be Issued, Old Man Strips Naked In Front Of Female Officer">
                            Tired Of Waiting For His Ration Card To Be Issued, Old Man Strips Naked In Front Of Female Officer                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/as-europe-opens-its-doors-to-syrian-refugees-these-14-pictures-show-there-is-still-hope-for-mankind-245044.html" title="As Europe Opens Its Doors To Syrian Refugees, These 14 Pictures Show There Is Still Hope For Mankind!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cop-cap-christof-stache-afp-getty-images_1441796347_1441796351_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/as-europe-opens-its-doors-to-syrian-refugees-these-14-pictures-show-there-is-still-hope-for-mankind-245044.html" title="As Europe Opens Its Doors To Syrian Refugees, These 14 Pictures Show There Is Still Hope For Mankind!">
                            As Europe Opens Its Doors To Syrian Refugees, These 14 Pictures Show There Is Still Hope For Mankind!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/colorado-is-earning-so-much-revenue-from-pot-they-may-give-residents-their-money-back-245046.html" title="Colorado Is Earning So Much Revenue From Pot, They May Give Residents Their Money Back!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441788410_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/colorado-is-earning-so-much-revenue-from-pot-they-may-give-residents-their-money-back-245046.html" title="Colorado Is Earning So Much Revenue From Pot, They May Give Residents Their Money Back!">
                            Colorado Is Earning So Much Revenue From Pot, They May Give Residents Their Money Back!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/gurmeet-ram-rahim-singh-is-ready-to-enter-the-bigg-boss-house-but-only-on-one-condition-245056.html" class="tint" title="Gurmeet Ram Rahim Singh Is Ready To Enter The Bigg Boss House. But, Only On One Condition!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/msg_1441799442_1441799447_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/gurmeet-ram-rahim-singh-is-ready-to-enter-the-bigg-boss-house-but-only-on-one-condition-245056.html" title="Gurmeet Ram Rahim Singh Is Ready To Enter The Bigg Boss House. But, Only On One Condition!">
                            Gurmeet Ram Rahim Singh Is Ready To Enter The Bigg Boss House. But, Only On One Condition!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/sunny-leone-is-all-set-to-play-superwoman-on-screen-and-that-too-in-her-home-production-245030.html" class="tint" title="Sunny Leone Is All Set To Play Superwoman On Screen And That Too In Her Home Production!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/sunny-leone_1441780122_1441780129_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/sunny-leone-is-all-set-to-play-superwoman-on-screen-and-that-too-in-her-home-production-245030.html" title="Sunny Leone Is All Set To Play Superwoman On Screen And That Too In Her Home Production!">
                            Sunny Leone Is All Set To Play Superwoman On Screen And That Too In Her Home Production!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/after-sonam-kapoor-twitter-found-all-its-humor-in-sonakshi-sinhas-tweet-against-meat-ban-245039.html" class="tint" title="After Sonam Kapoor, Twitter Found All Its Humor In Sonakshi Sinha's Tweet Against Meat Ban">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/sonakshi-sinha---bollywoodgo net_1441785637_1441785646_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/after-sonam-kapoor-twitter-found-all-its-humor-in-sonakshi-sinhas-tweet-against-meat-ban-245039.html" title="After Sonam Kapoor, Twitter Found All Its Humor In Sonakshi Sinha's Tweet Against Meat Ban">
                            After Sonam Kapoor, Twitter Found All Its Humor In Sonakshi Sinha's Tweet Against Meat Ban                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/radhe-maa-and-shiney-ahuja-to-slap-a-legal-notice-on-a-tv-channel-245034.html" class="tint" title="Radhe Maa And Shiney Ahuja To Slap A Legal Notice On A TV Channel">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-2_1441783131_1441783139_218x102.jpg" border="0" alt="Radhe Maa And Shiney Ahuja To Slap A Legal Notice On A TV Channel"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/radhe-maa-and-shiney-ahuja-to-slap-a-legal-notice-on-a-tv-channel-245034.html" title="Radhe Maa And Shiney Ahuja To Slap A Legal Notice On A TV Channel">
                            Radhe Maa And Shiney Ahuja To Slap A Legal Notice On A TV Channel                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/sonu-nigam-shares-his-last-conversation-with-aadesh-shrivastava-and-its-heartwrenching-245022.html" class="tint" title="Sonu Nigam Shares His Last Conversation With Aadesh Shrivastava And It's Heart-Wrenching!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-image_1441774910_1441774918_218x102.jpg" border="0" alt="Sonu Nigam Shares His Last Conversation With Aadesh Shrivastava And It's Heart-Wrenching!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/sonu-nigam-shares-his-last-conversation-with-aadesh-shrivastava-and-its-heartwrenching-245022.html" title="Sonu Nigam Shares His Last Conversation With Aadesh Shrivastava And It's Heart-Wrenching!">
                            Sonu Nigam Shares His Last Conversation With Aadesh Shrivastava And It's Heart-Wrenching!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/bollywood/20-films-that-prove-akshay-kumar-has-more-going-for-him-than-just-action-244989.html" class="tint" title="20 Films That Prove Akshay Kumar Has More Going For Him Than Just Action">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/ffbb_1441700134_1441700142_218x102.jpg" border="0" alt="20 Films That Prove Akshay Kumar Has More Going For Him Than Just Action"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/20-films-that-prove-akshay-kumar-has-more-going-for-him-than-just-action-244989.html" title="20 Films That Prove Akshay Kumar Has More Going For Him Than Just Action">
                            20 Films That Prove Akshay Kumar Has More Going For Him Than Just Action                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/9-conversations-every-parent-needs-to-have-with-their-kids-244894.html" class="tint" title="9 Conversations Every Parent Needs To Have With Their Kids">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441430634_218x102.jpg" border="0" alt="9 Conversations Every Parent Needs To Have With Their Kids"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/9-conversations-every-parent-needs-to-have-with-their-kids-244894.html" title="9 Conversations Every Parent Needs To Have With Their Kids">
                            9 Conversations Every Parent Needs To Have With Their Kids                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/gurmeet-ram-rahim-singh-is-ready-to-enter-the-bigg-boss-house-but-only-on-one-condition-245056.html" class="tint" title="Gurmeet Ram Rahim Singh Is Ready To Enter The Bigg Boss House. But, Only On One Condition!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/msg_1441799442_1441799447_218x102.jpg" border="0" alt="Gurmeet Ram Rahim Singh Is Ready To Enter The Bigg Boss House. But, Only On One Condition!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/gurmeet-ram-rahim-singh-is-ready-to-enter-the-bigg-boss-house-but-only-on-one-condition-245056.html" title="Gurmeet Ram Rahim Singh Is Ready To Enter The Bigg Boss House. But, Only On One Condition!">
                            Gurmeet Ram Rahim Singh Is Ready To Enter The Bigg Boss House. But, Only On One Condition!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                    <a href="http://www.indiatimes.com/news/sports/recap-on-this-day-25-years-ago-pete-sampras-became-the-youngestever-us-open-champion-245045.html" class="wow sticker">&nbsp;</a>
                                                
                        <a href="http://www.indiatimes.com/news/sports/recap-on-this-day-25-years-ago-pete-sampras-became-the-youngestever-us-open-champion-245045.html" title="Recap: On This Day 25 Years Ago, Pete Sampras Became The Youngest-Ever US Open Champion" class=" tint">
                            <a class='video-btn sprite' href='http://www.indiatimes.com/news/sports/recap-on-this-day-25-years-ago-pete-sampras-became-the-youngestever-us-open-champion-245045.html'>video</a>

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/petesamprasusopen_1441787191_236x111.jpg" border="0" alt="Recap: On This Day 25 Years Ago, Pete Sampras Became The Youngest-Ever US Open Champion"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/recap-on-this-day-25-years-ago-pete-sampras-became-the-youngestever-us-open-champion-245045.html" title="Recap: On This Day 25 Years Ago, Pete Sampras Became The Youngest-Ever US Open Champion">
                            Recap: On This Day 25 Years Ago, Pete Sampras Became The Youngest-Ever US Open Champion                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/the-wait-to-meet-your-favourite-disney-characters-might-just-be-over-bengaluru-disneyland-could-soon-be-a-reality-245042.html" title="The Wait To Meet Your Favourite Disney Characters Might Just Be Over. Bengaluru Disneyland Could Soon Be A Reality" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/disney-502_1441786833_236x111.jpg" border="0" alt="The Wait To Meet Your Favourite Disney Characters Might Just Be Over. Bengaluru Disneyland Could Soon Be A Reality"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/the-wait-to-meet-your-favourite-disney-characters-might-just-be-over-bengaluru-disneyland-could-soon-be-a-reality-245042.html" title="The Wait To Meet Your Favourite Disney Characters Might Just Be Over. Bengaluru Disneyland Could Soon Be A Reality">
                            The Wait To Meet Your Favourite Disney Characters Might Just Be Over. Bengaluru Disneyland Could Soon Be A Reality                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/this-engineering-student-got-so-wasted-he-doesnt-remember-squat-about-designing-a-plane-245040.html" title="This Engineering Student Got So Wasted, He Doesn't Remember Squat About Designing A Plane!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/wasted502_1441785497_236x111.jpg" border="0" alt="This Engineering Student Got So Wasted, He Doesn't Remember Squat About Designing A Plane!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/this-engineering-student-got-so-wasted-he-doesnt-remember-squat-about-designing-a-plane-245040.html" title="This Engineering Student Got So Wasted, He Doesn't Remember Squat About Designing A Plane!">
                            This Engineering Student Got So Wasted, He Doesn't Remember Squat About Designing A Plane!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/government-is-planning-to-move-indian-standard-time-ahead-by-30-mins-heres-how-itll-change-your-life-245035.html" title="Government Is Planning To Move Indian Standard Time Ahead By 30 Mins. Here's How It'll Change Your Life!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/clock-502_1441783530_236x111.jpg" border="0" alt="Government Is Planning To Move Indian Standard Time Ahead By 30 Mins. Here's How It'll Change Your Life!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/government-is-planning-to-move-indian-standard-time-ahead-by-30-mins-heres-how-itll-change-your-life-245035.html" title="Government Is Planning To Move Indian Standard Time Ahead By 30 Mins. Here's How It'll Change Your Life!">
                            Government Is Planning To Move Indian Standard Time Ahead By 30 Mins. Here's How It'll Change Your Life!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/sports/virat-kohli-could-be-fastracked-into-odi-captaincy-mahendra-singh-dhoni-left-to-lead-only-in-t20s-sooner-than-you-think-245033.html" title="Virat Kohli Could Be Fastracked Into ODI Captaincy. Mahendra Singh Dhoni Left To Lead Only In T20s Sooner Than You Think" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/kohlidhoni4reuters_1441780195_236x111.jpg" border="0" alt="Virat Kohli Could Be Fastracked Into ODI Captaincy. Mahendra Singh Dhoni Left To Lead Only In T20s Sooner Than You Think"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/virat-kohli-could-be-fastracked-into-odi-captaincy-mahendra-singh-dhoni-left-to-lead-only-in-t20s-sooner-than-you-think-245033.html" title="Virat Kohli Could Be Fastracked Into ODI Captaincy. Mahendra Singh Dhoni Left To Lead Only In T20s Sooner Than You Think">
                            Virat Kohli Could Be Fastracked Into ODI Captaincy. Mahendra Singh Dhoni Left To Lead Only In T20s Sooner Than You Think                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/buzz/youâll-be-shocked-to-know-that-work-stress-is-as-dangerous-for-health-as-secondhand-smoke-245011.html" class="tint" title="Youâll Be Shocked To Know That Work Stress Is As Dangerous For Health As Second-Hand Smoke!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441708840_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/buzz/youâll-be-shocked-to-know-that-work-stress-is-as-dangerous-for-health-as-secondhand-smoke-245011.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/youâll-be-shocked-to-know-that-work-stress-is-as-dangerous-for-health-as-secondhand-smoke-245011.html" title="Youâll Be Shocked To Know That Work Stress Is As Dangerous For Health As Second-Hand Smoke!">
                            Youâll Be Shocked To Know That Work Stress Is As Dangerous For Health As Second-Hand Smoke!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/radhe-maa-and-shiney-ahuja-to-slap-a-legal-notice-on-a-tv-channel-245034.html" class="tint" title="Radhe Maa And Shiney Ahuja To Slap A Legal Notice On A TV Channel">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-2_1441783131_1441783139_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/radhe-maa-and-shiney-ahuja-to-slap-a-legal-notice-on-a-tv-channel-245034.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/radhe-maa-and-shiney-ahuja-to-slap-a-legal-notice-on-a-tv-channel-245034.html" title="Radhe Maa And Shiney Ahuja To Slap A Legal Notice On A TV Channel">
                            Radhe Maa And Shiney Ahuja To Slap A Legal Notice On A TV Channel                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/always-worried-about-your-health-hereâs-what-you-can-do-to-reduce-your-anxiety-245005.html" class="tint" title="Always Worried About Your Health? Hereâs What You Can Do To Reduce Your Anxiety">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441706108_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/healthyliving/always-worried-about-your-health-hereâs-what-you-can-do-to-reduce-your-anxiety-245005.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/always-worried-about-your-health-hereâs-what-you-can-do-to-reduce-your-anxiety-245005.html" title="Always Worried About Your Health? Hereâs What You Can Do To Reduce Your Anxiety">
                            Always Worried About Your Health? Hereâs What You Can Do To Reduce Your Anxiety                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/healthyliving/25-foods-you-should-give-up-right-now-244868.html" class="tint" title="25 Foods You Should Give Up Right Now">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/food-card_1441352601_218x102.jpg" border="0" alt="25 Foods You Should Give Up Right Now"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/25-foods-you-should-give-up-right-now-244868.html" title="25 Foods You Should Give Up Right Now">
                            25 Foods You Should Give Up Right Now                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/sunny-leone-is-all-set-to-play-superwoman-on-screen-and-that-too-in-her-home-production-245030.html" class="tint" title="Sunny Leone Is All Set To Play Superwoman On Screen And That Too In Her Home Production!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/sunny-leone_1441780122_1441780129_218x102.jpg" border="0" alt="Sunny Leone Is All Set To Play Superwoman On Screen And That Too In Her Home Production!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/sunny-leone-is-all-set-to-play-superwoman-on-screen-and-that-too-in-her-home-production-245030.html" title="Sunny Leone Is All Set To Play Superwoman On Screen And That Too In Her Home Production!">
                            Sunny Leone Is All Set To Play Superwoman On Screen And That Too In Her Home Production!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/now-that-theres-going-to-be-a-gangs-of-wasseypur-3-we-look-back-at-15-iconic-dialogues-from-the-first-two-parts-245017.html" class="tint" title="Now That There's Going To Be A Gangs Of Wasseypur 3, We Look Back At 15 Iconic Dialogues From The First Two Parts!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441710154_218x102.jpg" border="0" alt="Now That There's Going To Be A Gangs Of Wasseypur 3, We Look Back At 15 Iconic Dialogues From The First Two Parts!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/now-that-theres-going-to-be-a-gangs-of-wasseypur-3-we-look-back-at-15-iconic-dialogues-from-the-first-two-parts-245017.html" title="Now That There's Going To Be A Gangs Of Wasseypur 3, We Look Back At 15 Iconic Dialogues From The First Two Parts!">
                            Now That There's Going To Be A Gangs Of Wasseypur 3, We Look Back At 15 Iconic Dialogues From The First Two Parts!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/healthyliving/6-minute-killer-workout-to-lose-that-stomach-fat-at-home-244988.html" class="tint" title="6 Minute Killer Workout To Lose That Stomach Fat At Home">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/ab-card_1441695449_218x102.jpg" border="0" alt="6 Minute Killer Workout To Lose That Stomach Fat At Home"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/6-minute-killer-workout-to-lose-that-stomach-fat-at-home-244988.html" title="6 Minute Killer Workout To Lose That Stomach Fat At Home">
                            6 Minute Killer Workout To Lose That Stomach Fat At Home                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/food/17-times-food-tasted-better-on-a-banana-leaf-than-on-a-normal-plate-244892.html" class="tint" title="17 Times Food Tasted Better On A Banana Leaf Than On A Normal Plate">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/bananal_1441374358_1441374370_218x102.jpg" border="0" alt="17 Times Food Tasted Better On A Banana Leaf Than On A Normal Plate"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/food/17-times-food-tasted-better-on-a-banana-leaf-than-on-a-normal-plate-244892.html" title="17 Times Food Tasted Better On A Banana Leaf Than On A Normal Plate">
                            17 Times Food Tasted Better On A Banana Leaf Than On A Normal Plate                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/this-journo-is-the-most-hated-person-in-the-world-after-she-kicked-syrian-migrants-on-camera-245028.html" title="This Hungarian Journo Is The Most Hated Person In The World After She Kicked Syrian Migrants On Camera" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441780454_236x111.jpg" border="0" alt="This Hungarian Journo Is The Most Hated Person In The World After She Kicked Syrian Migrants On Camera"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/this-journo-is-the-most-hated-person-in-the-world-after-she-kicked-syrian-migrants-on-camera-245028.html" title="This Hungarian Journo Is The Most Hated Person In The World After She Kicked Syrian Migrants On Camera">
                            This Hungarian Journo Is The Most Hated Person In The World After She Kicked Syrian Migrants On Camera                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/saudi-diplomat-accused-of-allegedly-raping-two-nepalese-helps-and-confining-them-in-his-flat-245023.html" title="Saudi Diplomat Accused Of Allegedly Raping Two Nepalese Helps And Confining Them In His Flat" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/gilrs-5502_1441777785_236x111.jpg" border="0" alt="Saudi Diplomat Accused Of Allegedly Raping Two Nepalese Helps And Confining Them In His Flat"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/saudi-diplomat-accused-of-allegedly-raping-two-nepalese-helps-and-confining-them-in-his-flat-245023.html" title="Saudi Diplomat Accused Of Allegedly Raping Two Nepalese Helps And Confining Them In His Flat">
                            Saudi Diplomat Accused Of Allegedly Raping Two Nepalese Helps And Confining Them In His Flat                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/indias-fifa-world-cup-dream-shattered-but-it-were-these-times-when-they-made-the-nation-proud-245020.html" title="India's FIFA World Cup Dream Shattered But It Were These Times When They Made The Nation Proud" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/footballindiawc_1441725390_236x111.jpg" border="0" alt="India's FIFA World Cup Dream Shattered But It Were These Times When They Made The Nation Proud"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/indias-fifa-world-cup-dream-shattered-but-it-were-these-times-when-they-made-the-nation-proud-245020.html" title="India's FIFA World Cup Dream Shattered But It Were These Times When They Made The Nation Proud">
                            India's FIFA World Cup Dream Shattered But It Were These Times When They Made The Nation Proud                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/the-worlds-most-famous-electric-car-might-be-coming-to-india-soon-245019.html" title="The World's Most Famous Electric Car Might Be Coming To India Soon" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/wallpaperscraft-6_1441713883_1441713887_236x111.jpg" border="0" alt="The World's Most Famous Electric Car Might Be Coming To India Soon"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/the-worlds-most-famous-electric-car-might-be-coming-to-india-soon-245019.html" title="The World's Most Famous Electric Car Might Be Coming To India Soon">
                            The World's Most Famous Electric Car Might Be Coming To India Soon                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/think-that-you-are-being-underpaid-you-may-be-right-india-pays-the-lowest-starting-salaries-in-asiapacific-245018.html" title="Think That You Are Being Underpaid? You May Be Right! India Pays The Lowest Starting Salaries In Asia-Pacific" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/currency-502_1441713593_236x111.jpg" border="0" alt="Think That You Are Being Underpaid? You May Be Right! India Pays The Lowest Starting Salaries In Asia-Pacific"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/think-that-you-are-being-underpaid-you-may-be-right-india-pays-the-lowest-starting-salaries-in-asiapacific-245018.html" title="Think That You Are Being Underpaid? You May Be Right! India Pays The Lowest Starting Salaries In Asia-Pacific">
                            Think That You Are Being Underpaid? You May Be Right! India Pays The Lowest Starting Salaries In Asia-Pacific                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/11-dialogues-from-andaaz-apna-apna-jaane-bhi-do-yaaro-thatll-actually-help-you-in-real-life-244976.html" class="tint" title="11 Dialogues From Andaaz Apna Apna & Jaane Bhi Do Yaaro That'll Actually Help You In Real Life!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card2_1441781197_1441781204_502x234.jpg" border="0" alt="11 Dialogues From Andaaz Apna Apna & Jaane Bhi Do Yaaro That'll Actually Help You In Real Life!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/11-dialogues-from-andaaz-apna-apna-jaane-bhi-do-yaaro-thatll-actually-help-you-in-real-life-244976.html" title="11 Dialogues From Andaaz Apna Apna & Jaane Bhi Do Yaaro That'll Actually Help You In Real Life!">
                        11 Dialogues From Andaaz Apna Apna & Jaane Bhi Do Yaaro That'll Actually Help You In Real Life!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/steve-jobs-+-9-famous-refugees-who-changed-the-world-245024.html" class="tint" title="Steve Jobs + 9 Famous Refugees Who Changed The World">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441783864_502x234.jpg" border="0" alt="Steve Jobs + 9 Famous Refugees Who Changed The World" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/steve-jobs-+-9-famous-refugees-who-changed-the-world-245024.html" title="Steve Jobs + 9 Famous Refugees Who Changed The World">
                        Steve Jobs + 9 Famous Refugees Who Changed The World                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-elderly-couples-sweet-reunion-at-the-airport-proves-that-love-has-no-age-limit-245021.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-elderly-couples-sweet-reunion-at-the-airport-proves-that-love-has-no-age-limit-245021.html" class="tint" title="This Elderly Couple's Sweet Reunion At The Airport Proves That Love Is Timeless">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/love_card_1441732550_502x234.jpg" border="0" alt="This Elderly Couple's Sweet Reunion At The Airport Proves That Love Is Timeless" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-elderly-couples-sweet-reunion-at-the-airport-proves-that-love-has-no-age-limit-245021.html" title="This Elderly Couple's Sweet Reunion At The Airport Proves That Love Is Timeless">
                        This Elderly Couple's Sweet Reunion At The Airport Proves That Love Is Timeless                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/remembering-captain-vikram-batra-pvc-â-the-bravest-of-them-all-245027.html" class="tint" title="Remembering Captain Vikram Batra, PVC â The Bravest Of Them All">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/vikram-card2_1441779776_1441779835_502x234.jpg" border="0" alt="Remembering Captain Vikram Batra, PVC â The Bravest Of Them All" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/remembering-captain-vikram-batra-pvc-â-the-bravest-of-them-all-245027.html" title="Remembering Captain Vikram Batra, PVC â The Bravest Of Them All">
                        Remembering Captain Vikram Batra, PVC â The Bravest Of Them All                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/what-if-the-next-bond-was-decided-upon-by-putting-the-candidates-in-the-bigg-boss-house-245010.html" class="tint" title="What If The Next Bond Was Decided Upon By Putting The Candidates In The Bigg Boss House?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441708621_502x234.jpg" border="0" alt="What If The Next Bond Was Decided Upon By Putting The Candidates In The Bigg Boss House?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/what-if-the-next-bond-was-decided-upon-by-putting-the-candidates-in-the-bigg-boss-house-245010.html" title="What If The Next Bond Was Decided Upon By Putting The Candidates In The Bigg Boss House?">
                        What If The Next Bond Was Decided Upon By Putting The Candidates In The Bigg Boss House?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/watch-shahid-kapoor-and-alia-bhatt-go-bonkers-during-the-making-of-shaandaar-245013.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/watch-shahid-kapoor-and-alia-bhatt-go-bonkers-during-the-making-of-shaandaar-245013.html" class="tint" title="Watch Shahid Kapoor And Alia Bhatt Go Bonkers During The Making Of 'Shaandaar'">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/screenshot_3_1441709595_1441709606_502x234.jpg" border="0" alt="Watch Shahid Kapoor And Alia Bhatt Go Bonkers During The Making Of 'Shaandaar'" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/watch-shahid-kapoor-and-alia-bhatt-go-bonkers-during-the-making-of-shaandaar-245013.html" title="Watch Shahid Kapoor And Alia Bhatt Go Bonkers During The Making Of 'Shaandaar'">
                        Watch Shahid Kapoor And Alia Bhatt Go Bonkers During The Making Of 'Shaandaar'                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/sonu-nigam-shares-his-last-conversation-with-aadesh-shrivastava-and-its-heartwrenching-245022.html" class="tint" title="Sonu Nigam Shares His Last Conversation With Aadesh Shrivastava And It's Heart-Wrenching!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-image_1441774910_1441774918_502x234.jpg" border="0" alt="Sonu Nigam Shares His Last Conversation With Aadesh Shrivastava And It's Heart-Wrenching!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/sonu-nigam-shares-his-last-conversation-with-aadesh-shrivastava-and-its-heartwrenching-245022.html" title="Sonu Nigam Shares His Last Conversation With Aadesh Shrivastava And It's Heart-Wrenching!">
                        Sonu Nigam Shares His Last Conversation With Aadesh Shrivastava And It's Heart-Wrenching!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/6-minute-killer-workout-to-lose-that-stomach-fat-at-home-244988.html" class="tint" title="6 Minute Killer Workout To Lose That Stomach Fat At Home">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/ab-card_1441695449_502x234.jpg" border="0" alt="6 Minute Killer Workout To Lose That Stomach Fat At Home" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/6-minute-killer-workout-to-lose-that-stomach-fat-at-home-244988.html" title="6 Minute Killer Workout To Lose That Stomach Fat At Home">
                        6 Minute Killer Workout To Lose That Stomach Fat At Home                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-conversations-every-parent-needs-to-have-with-their-kids-244894.html" class="tint" title="9 Conversations Every Parent Needs To Have With Their Kids">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441430634_502x234.jpg" border="0" alt="9 Conversations Every Parent Needs To Have With Their Kids" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-conversations-every-parent-needs-to-have-with-their-kids-244894.html" title="9 Conversations Every Parent Needs To Have With Their Kids">
                        9 Conversations Every Parent Needs To Have With Their Kids                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/20-films-that-prove-akshay-kumar-has-more-going-for-him-than-just-action-244989.html" class="tint" title="20 Films That Prove Akshay Kumar Has More Going For Him Than Just Action">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/ffbb_1441700134_1441700142_502x234.jpg" border="0" alt="20 Films That Prove Akshay Kumar Has More Going For Him Than Just Action" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/20-films-that-prove-akshay-kumar-has-more-going-for-him-than-just-action-244989.html" title="20 Films That Prove Akshay Kumar Has More Going For Him Than Just Action">
                        20 Films That Prove Akshay Kumar Has More Going For Him Than Just Action                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/25-foods-you-should-give-up-right-now-244868.html" class="tint" title="25 Foods You Should Give Up Right Now">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/food-card_1441352601_502x234.jpg" border="0" alt="25 Foods You Should Give Up Right Now" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/25-foods-you-should-give-up-right-now-244868.html" title="25 Foods You Should Give Up Right Now">
                        25 Foods You Should Give Up Right Now                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/these-27-photos-from-ranbir-and-deepikas-films-will-evoke-major-wanderlust-in-you-245001.html" class="tint" title="These 27 Photos From Ranbir And Deepika's Films Will Evoke Major Wanderlust In You!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-06_1441778899_1441778909_502x234.jpg" border="0" alt="These 27 Photos From Ranbir And Deepika's Films Will Evoke Major Wanderlust In You!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/these-27-photos-from-ranbir-and-deepikas-films-will-evoke-major-wanderlust-in-you-245001.html" title="These 27 Photos From Ranbir And Deepika's Films Will Evoke Major Wanderlust In You!">
                        These 27 Photos From Ranbir And Deepika's Films Will Evoke Major Wanderlust In You!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/8-small-things-about-you-that-will-make-people-judge-you-almost-instantly-244982.html" class="tint" title="8 Small Things About You That Will Make People Judge You Almost Instantly">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441689278_1441689490_502x234.jpg" border="0" alt="8 Small Things About You That Will Make People Judge You Almost Instantly" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/8-small-things-about-you-that-will-make-people-judge-you-almost-instantly-244982.html" title="8 Small Things About You That Will Make People Judge You Almost Instantly">
                        8 Small Things About You That Will Make People Judge You Almost Instantly                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/now-that-theres-going-to-be-a-gangs-of-wasseypur-3-we-look-back-at-15-iconic-dialogues-from-the-first-two-parts-245017.html" class="tint" title="Now That There's Going To Be A Gangs Of Wasseypur 3, We Look Back At 15 Iconic Dialogues From The First Two Parts!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441710154_502x234.jpg" border="0" alt="Now That There's Going To Be A Gangs Of Wasseypur 3, We Look Back At 15 Iconic Dialogues From The First Two Parts!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/now-that-theres-going-to-be-a-gangs-of-wasseypur-3-we-look-back-at-15-iconic-dialogues-from-the-first-two-parts-245017.html" title="Now That There's Going To Be A Gangs Of Wasseypur 3, We Look Back At 15 Iconic Dialogues From The First Two Parts!">
                        Now That There's Going To Be A Gangs Of Wasseypur 3, We Look Back At 15 Iconic Dialogues From The First Two Parts!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/a-dummyâs-guide-to-safe-sexting-because-no-one-wants-their-weiner-trending-on-the-web-245016.html" class="tint" title="A Dummyâs Guide To Safe Sexting. Because No One Wants Their Weiner Trending On The Web">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441711424_502x234.jpg" border="0" alt="A Dummyâs Guide To Safe Sexting. Because No One Wants Their Weiner Trending On The Web" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/a-dummyâs-guide-to-safe-sexting-because-no-one-wants-their-weiner-trending-on-the-web-245016.html" title="A Dummyâs Guide To Safe Sexting. Because No One Wants Their Weiner Trending On The Web">
                        A Dummyâs Guide To Safe Sexting. Because No One Wants Their Weiner Trending On The Web                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/get-cast-alongside-a-r-rehman-hrithik-roshan-and-stephen-hawking-for-the-we-the-people-campaign-245014.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/get-cast-alongside-a-r-rehman-hrithik-roshan-and-stephen-hawking-for-the-we-the-people-campaign-245014.html" class="tint" title="Get Cast Alongside A R Rehman, Hrithik Roshan And Stephen Hawking For The 'We The People' Campaign">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/502_1441708822_502x234.jpg" border="0" alt="Get Cast Alongside A R Rehman, Hrithik Roshan And Stephen Hawking For The 'We The People' Campaign" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/get-cast-alongside-a-r-rehman-hrithik-roshan-and-stephen-hawking-for-the-we-the-people-campaign-245014.html" title="Get Cast Alongside A R Rehman, Hrithik Roshan And Stephen Hawking For The 'We The People' Campaign">
                        Get Cast Alongside A R Rehman, Hrithik Roshan And Stephen Hawking For The 'We The People' Campaign                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-similarity-between-lip-to-lip-de-kissiyan-and-coldplays-strawberry-swing-is-too-hard-to-ignore-245009.html" class="tint" title="This Similarity Between 'Lip To Lip De Kissiyan' And Coldplay's 'Strawberry Swing' Is Too Hard To Ignore">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441707816_502x234.jpg" border="0" alt="This Similarity Between 'Lip To Lip De Kissiyan' And Coldplay's 'Strawberry Swing' Is Too Hard To Ignore" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-similarity-between-lip-to-lip-de-kissiyan-and-coldplays-strawberry-swing-is-too-hard-to-ignore-245009.html" title="This Similarity Between 'Lip To Lip De Kissiyan' And Coldplay's 'Strawberry Swing' Is Too Hard To Ignore">
                        This Similarity Between 'Lip To Lip De Kissiyan' And Coldplay's 'Strawberry Swing' Is Too Hard To Ignore                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/sonam-kapoor-calls-meatban-a-misogynistic-move-gets-trolled-ruthlessly-by-judgemental-assholes-245007.html" class="tint" title="Sonam Kapoor Calls #Meatban A 'Misogynistic' Move, Gets Trolled Ruthlessly By 'Judgemental Assholes'!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441706814_502x234.jpg" border="0" alt="Sonam Kapoor Calls #Meatban A 'Misogynistic' Move, Gets Trolled Ruthlessly By 'Judgemental Assholes'!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/sonam-kapoor-calls-meatban-a-misogynistic-move-gets-trolled-ruthlessly-by-judgemental-assholes-245007.html" title="Sonam Kapoor Calls #Meatban A 'Misogynistic' Move, Gets Trolled Ruthlessly By 'Judgemental Assholes'!">
                        Sonam Kapoor Calls #Meatban A 'Misogynistic' Move, Gets Trolled Ruthlessly By 'Judgemental Assholes'!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/7-things-you-can-do-to-keep-your-eyesight-from-worsening-and-improve-your-vision-244968.html" class="tint" title="7 Things You Can Do To Keep Your Eyesight From Worsening And Improve Your Vision">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/vision_1441618721_502x234.jpg" border="0" alt="7 Things You Can Do To Keep Your Eyesight From Worsening And Improve Your Vision" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/7-things-you-can-do-to-keep-your-eyesight-from-worsening-and-improve-your-vision-244968.html" title="7 Things You Can Do To Keep Your Eyesight From Worsening And Improve Your Vision">
                        7 Things You Can Do To Keep Your Eyesight From Worsening And Improve Your Vision                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-mashup-of-sridevi-dancing-on-lean-on-is-so-creepy-itll-make-your-skin-crawl-244996.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-mashup-of-sridevi-dancing-on-lean-on-is-so-creepy-itll-make-your-skin-crawl-244996.html" class="tint" title="This Mashup Of Sridevi Dancing On 'Lean On' Is So Creepy It'll Make Your Skin Crawl!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/nagin_card_1441698763_502x234.jpg" border="0" alt="This Mashup Of Sridevi Dancing On 'Lean On' Is So Creepy It'll Make Your Skin Crawl!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-mashup-of-sridevi-dancing-on-lean-on-is-so-creepy-itll-make-your-skin-crawl-244996.html" title="This Mashup Of Sridevi Dancing On 'Lean On' Is So Creepy It'll Make Your Skin Crawl!">
                        This Mashup Of Sridevi Dancing On 'Lean On' Is So Creepy It'll Make Your Skin Crawl!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/children-suffering-from-cancer-live-their-superhero-dreams-in-this-amazing-fantasy-photo-shoot-244946.html" class="tint" title="Children Suffering From Cancer Live Their Superhero Dreams In This Amazing Fantasy Photo Shoot">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/4card1_1441601248_1441601257_502x234.jpg" border="0" alt="Children Suffering From Cancer Live Their Superhero Dreams In This Amazing Fantasy Photo Shoot" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/children-suffering-from-cancer-live-their-superhero-dreams-in-this-amazing-fantasy-photo-shoot-244946.html" title="Children Suffering From Cancer Live Their Superhero Dreams In This Amazing Fantasy Photo Shoot">
                        Children Suffering From Cancer Live Their Superhero Dreams In This Amazing Fantasy Photo Shoot                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/old-at-heart-research-shows-that-our-hearts-are-much-older-than-they-are-supposed-to-be-244969.html" class="tint" title="Old At Heart. Research Shows That Our Hearts Are Much Older Than They Are Supposed To Be!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cover_1441619655_502x234.jpg" border="0" alt="Old At Heart. Research Shows That Our Hearts Are Much Older Than They Are Supposed To Be!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/old-at-heart-research-shows-that-our-hearts-are-much-older-than-they-are-supposed-to-be-244969.html" title="Old At Heart. Research Shows That Our Hearts Are Much Older Than They Are Supposed To Be!">
                        Old At Heart. Research Shows That Our Hearts Are Much Older Than They Are Supposed To Be!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/the-vanity-vans-of-these-7-bollywood-stars-will-make-you-feel-very-poor-244884.html" class="tint" title="The Vanity Vans Of These 7 Bollywood Stars Will Make You Feel Very Poor!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441363089_1441363097_502x234.jpg" border="0" alt="The Vanity Vans Of These 7 Bollywood Stars Will Make You Feel Very Poor!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/the-vanity-vans-of-these-7-bollywood-stars-will-make-you-feel-very-poor-244884.html" title="The Vanity Vans Of These 7 Bollywood Stars Will Make You Feel Very Poor!">
                        The Vanity Vans Of These 7 Bollywood Stars Will Make You Feel Very Poor!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/aishwarya-rai-casts-a-spell-with-jazbaas-new-song-bandeyaa-244985.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/aishwarya-rai-casts-a-spell-with-jazbaas-new-song-bandeyaa-244985.html" class="tint" title="Aishwarya Rai Casts A Spell With Jazbaa's New Song 'Bandeyaa' !">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/screenshot_1_1441692602_1441692608_502x234.jpg" border="0" alt="Aishwarya Rai Casts A Spell With Jazbaa's New Song 'Bandeyaa' !" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/aishwarya-rai-casts-a-spell-with-jazbaas-new-song-bandeyaa-244985.html" title="Aishwarya Rai Casts A Spell With Jazbaa's New Song 'Bandeyaa' !">
                        Aishwarya Rai Casts A Spell With Jazbaa's New Song 'Bandeyaa' !                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/7-things-you-probably-didnât-know-you-could-do-on-your-mobile-browser-244979.html" class="tint" title="7 Things You Probably Didnât Know You Could Do On Your Mobile Browser">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cover_1441692703_502x234.jpg" border="0" alt="7 Things You Probably Didnât Know You Could Do On Your Mobile Browser" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/7-things-you-probably-didnât-know-you-could-do-on-your-mobile-browser-244979.html" title="7 Things You Probably Didnât Know You Could Do On Your Mobile Browser">
                        7 Things You Probably Didnât Know You Could Do On Your Mobile Browser                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/17-times-food-tasted-better-on-a-banana-leaf-than-on-a-normal-plate-244892.html" class="tint" title="17 Times Food Tasted Better On A Banana Leaf Than On A Normal Plate">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/bananal_1441374358_1441374370_502x234.jpg" border="0" alt="17 Times Food Tasted Better On A Banana Leaf Than On A Normal Plate" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/17-times-food-tasted-better-on-a-banana-leaf-than-on-a-normal-plate-244892.html" title="17 Times Food Tasted Better On A Banana Leaf Than On A Normal Plate">
                        17 Times Food Tasted Better On A Banana Leaf Than On A Normal Plate                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/6-yoga-poses-to-lower-your-cholesterol-levels-and-get-healthy-244956.html" class="tint" title="6 Yoga Poses To Lower Your Cholesterol Levels And Get Healthy">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cholesterol-card_1441612237_502x234.jpg" border="0" alt="6 Yoga Poses To Lower Your Cholesterol Levels And Get Healthy" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/6-yoga-poses-to-lower-your-cholesterol-levels-and-get-healthy-244956.html" title="6 Yoga Poses To Lower Your Cholesterol Levels And Get Healthy">
                        6 Yoga Poses To Lower Your Cholesterol Levels And Get Healthy                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/13-places-in-india-where-you-will-always-find-a-neverending-queue-244887.html" class="tint" title="13 Places In India Where You Will Always Find A Never-Ending Queue">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/wwww_1441365018_1441365030_502x234.jpg" border="0" alt="13 Places In India Where You Will Always Find A Never-Ending Queue" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/13-places-in-india-where-you-will-always-find-a-neverending-queue-244887.html" title="13 Places In India Where You Will Always Find A Never-Ending Queue">
                        13 Places In India Where You Will Always Find A Never-Ending Queue                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/31-gorgeous-photographs-that-look-photoshopped-but-are-totally-real-244883.html" class="tint" title="31 Gorgeous Photographs That Look Photoshopped But Are Totally Real">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441363068_502x234.jpg" border="0" alt="31 Gorgeous Photographs That Look Photoshopped But Are Totally Real" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/31-gorgeous-photographs-that-look-photoshopped-but-are-totally-real-244883.html" title="31 Gorgeous Photographs That Look Photoshopped But Are Totally Real">
                        31 Gorgeous Photographs That Look Photoshopped But Are Totally Real                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/five-components-of-food-that-should-be-a-part-of-your-everyday-diet-244965.html" class="tint" title="Five Components Of Food That Should Be A Part Of Your Everyday Diet">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441615068_502x234.jpg" border="0" alt="Five Components Of Food That Should Be A Part Of Your Everyday Diet" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/five-components-of-food-that-should-be-a-part-of-your-everyday-diet-244965.html" title="Five Components Of Food That Should Be A Part Of Your Everyday Diet">
                        Five Components Of Food That Should Be A Part Of Your Everyday Diet                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-celebrities-who-prove-that-making-films-is-not-just-about-money-244960.html" class="tint" title="12 Celebrities Who Prove That Making Films Is Not Just About Money">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441699608_502x234.jpg" border="0" alt="12 Celebrities Who Prove That Making Films Is Not Just About Money" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-celebrities-who-prove-that-making-films-is-not-just-about-money-244960.html" title="12 Celebrities Who Prove That Making Films Is Not Just About Money">
                        12 Celebrities Who Prove That Making Films Is Not Just About Money                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/chennai-women-auto-drivers-are-giving-the-men-sleepless-lights-244978.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/chennai-women-auto-drivers-are-giving-the-men-sleepless-lights-244978.html" class="tint" title="Chennai Women Auto Drivers Are Giving The Men Sleepless Lights!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/chennaidriver_card_1441629563_502x234.jpg" border="0" alt="Chennai Women Auto Drivers Are Giving The Men Sleepless Lights!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/chennai-women-auto-drivers-are-giving-the-men-sleepless-lights-244978.html" title="Chennai Women Auto Drivers Are Giving The Men Sleepless Lights!">
                        Chennai Women Auto Drivers Are Giving The Men Sleepless Lights!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/11-places-that-became-really-popular-after-their-associations-with-bollywood-and-hollywood-244811.html" class="tint" title="11 Places That Became Really Popular After Their Associations With Bollywood And Hollywood">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441257391_1441257403_502x234.jpg" border="0" alt="11 Places That Became Really Popular After Their Associations With Bollywood And Hollywood" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/11-places-that-became-really-popular-after-their-associations-with-bollywood-and-hollywood-244811.html" title="11 Places That Became Really Popular After Their Associations With Bollywood And Hollywood">
                        11 Places That Became Really Popular After Their Associations With Bollywood And Hollywood                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/iphone-6s-is-almost-here-hereâre-8-rumoured-features-that-apple-addicts-are-raving-about-244977.html" class="tint" title="iPhone 6s Is Almost Here. Hereâre 8 Rumoured Features That Apple Addicts Are Raving About">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/iphone6s-502_1441629189_502x234.jpg" border="0" alt="iPhone 6s Is Almost Here. Hereâre 8 Rumoured Features That Apple Addicts Are Raving About" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/iphone-6s-is-almost-here-hereâre-8-rumoured-features-that-apple-addicts-are-raving-about-244977.html" title="iPhone 6s Is Almost Here. Hereâre 8 Rumoured Features That Apple Addicts Are Raving About">
                        iPhone 6s Is Almost Here. Hereâre 8 Rumoured Features That Apple Addicts Are Raving About                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/decoding-running-shoes-how-to-select-the-right-pair-244961.html" class="tint" title="Decoding Running Shoes - How To Select The Right Pair">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441700208_502x234.jpg" border="0" alt="Decoding Running Shoes - How To Select The Right Pair" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/decoding-running-shoes-how-to-select-the-right-pair-244961.html" title="Decoding Running Shoes - How To Select The Right Pair">
                        Decoding Running Shoes - How To Select The Right Pair                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/9-things-you-didnt-know-about-salmans-prodigy-athiya-shetty-244876.html" class="tint" title="9 Things You Didn't Know About Salman's Prodigy Athiya Shetty">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/as-card_1441356229_502x234.jpg" border="0" alt="9 Things You Didn't Know About Salman's Prodigy Athiya Shetty" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/9-things-you-didnt-know-about-salmans-prodigy-athiya-shetty-244876.html" title="9 Things You Didn't Know About Salman's Prodigy Athiya Shetty">
                        9 Things You Didn't Know About Salman's Prodigy Athiya Shetty                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/despite-a-dull-trailer-heres-why-you-shouldnt-miss-aishwarya-rais-jazbaa-244959.html" class="tint" title="Despite A Dull Trailer, Here's Why You Shouldn't Miss Aishwarya Rai's 'Jazbaa'">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/jazbaa_1441611368_1441611376_502x234.jpg" border="0" alt="Despite A Dull Trailer, Here's Why You Shouldn't Miss Aishwarya Rai's 'Jazbaa'" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/despite-a-dull-trailer-heres-why-you-shouldnt-miss-aishwarya-rais-jazbaa-244959.html" title="Despite A Dull Trailer, Here's Why You Shouldn't Miss Aishwarya Rai's 'Jazbaa'">
                        Despite A Dull Trailer, Here's Why You Shouldn't Miss Aishwarya Rai's 'Jazbaa'                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/17-examples-that-prove-indian-comics-are-the-best-in-the-world-244760.html" class="tint" title="17 Examples That Prove Indian Comics Are The Best In The World">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441097233_502x234.jpg" border="0" alt="17 Examples That Prove Indian Comics Are The Best In The World" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/17-examples-that-prove-indian-comics-are-the-best-in-the-world-244760.html" title="17 Examples That Prove Indian Comics Are The Best In The World">
                        17 Examples That Prove Indian Comics Are The Best In The World                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/robin-williams-daughter-pens-down-a-heartbreaking-note-about-depression-244954.html" class="tint" title="Robin Williams' Daughter Pens Down A Heartbreaking Note About Depression!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/rw_1441609814_1441609820_502x234.jpg" border="0" alt="Robin Williams' Daughter Pens Down A Heartbreaking Note About Depression!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/robin-williams-daughter-pens-down-a-heartbreaking-note-about-depression-244954.html" title="Robin Williams' Daughter Pens Down A Heartbreaking Note About Depression!">
                        Robin Williams' Daughter Pens Down A Heartbreaking Note About Depression!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/donât-get-duped-a-dummyâs-guide-to-avoid-buying-fakes-244948.html" class="tint" title="Donât Get Duped. A Dummyâs Guide To Avoid Buying Fakes">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/fake-card_1441614921_502x234.jpg" border="0" alt="Donât Get Duped. A Dummyâs Guide To Avoid Buying Fakes" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/donât-get-duped-a-dummyâs-guide-to-avoid-buying-fakes-244948.html" title="Donât Get Duped. A Dummyâs Guide To Avoid Buying Fakes">
                        Donât Get Duped. A Dummyâs Guide To Avoid Buying Fakes                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/1-in-5-women-affected-by-pcos-in-india-but-fret-not-we-have-the-solution-244753.html" class="tint" title="1 In 5 Women Affected By PCOS In India! But Fret Not, We Have The Solution">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/pcos-card_1441093311_502x234.gif" border="0" alt="1 In 5 Women Affected By PCOS In India! But Fret Not, We Have The Solution" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/1-in-5-women-affected-by-pcos-in-india-but-fret-not-we-have-the-solution-244753.html" title="1 In 5 Women Affected By PCOS In India! But Fret Not, We Have The Solution">
                        1 In 5 Women Affected By PCOS In India! But Fret Not, We Have The Solution                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-of-a-mall-toy-car-operator-forcibly-kissing-a-child-will-scare-you-even-if-you-are-not-a-parent-244963.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-a-mall-toy-car-operator-forcibly-kissing-a-child-will-scare-you-even-if-you-are-not-a-parent-244963.html" class="tint" title="This Video Of A Mall Toy Car Operator Forcibly Kissing A Child Will Scare You, Even If You Are Not A Parent">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/toykiss_card_1441613229_502x234.jpg" border="0" alt="This Video Of A Mall Toy Car Operator Forcibly Kissing A Child Will Scare You, Even If You Are Not A Parent" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-a-mall-toy-car-operator-forcibly-kissing-a-child-will-scare-you-even-if-you-are-not-a-parent-244963.html" title="This Video Of A Mall Toy Car Operator Forcibly Kissing A Child Will Scare You, Even If You Are Not A Parent">
                        This Video Of A Mall Toy Car Operator Forcibly Kissing A Child Will Scare You, Even If You Are Not A Parent                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-parody-of-salmans-main-hoon-hero-tera-is-so-funny-that-it-will-leave-you-in-splits-244949.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-parody-of-salmans-main-hoon-hero-tera-is-so-funny-that-it-will-leave-you-in-splits-244949.html" class="tint" title="This Parody Of Salman's 'Main Hoon Hero Tera' Is So Funny That It Will Leave You In Splits!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/hero_card_1441607021_502x234.jpg" border="0" alt="This Parody Of Salman's 'Main Hoon Hero Tera' Is So Funny That It Will Leave You In Splits!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-parody-of-salmans-main-hoon-hero-tera-is-so-funny-that-it-will-leave-you-in-splits-244949.html" title="This Parody Of Salman's 'Main Hoon Hero Tera' Is So Funny That It Will Leave You In Splits!">
                        This Parody Of Salman's 'Main Hoon Hero Tera' Is So Funny That It Will Leave You In Splits!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/17-words-and-phrases-you-use-everyday-and-their-smartersounding-alternatives-244781.html" class="tint" title="17 Words And Phrases You Use Everyday And Their Smarter-Sounding Alternatives">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441169885_502x234.jpg" border="0" alt="17 Words And Phrases You Use Everyday And Their Smarter-Sounding Alternatives" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/17-words-and-phrases-you-use-everyday-and-their-smartersounding-alternatives-244781.html" title="17 Words And Phrases You Use Everyday And Their Smarter-Sounding Alternatives">
                        17 Words And Phrases You Use Everyday And Their Smarter-Sounding Alternatives                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/this-doublesided-transparent-television-is-so-very-thin-its-freaking-foldable-244950.html" class="tint" title="This Double-Sided, Transparent Television Is So Very Thin, It's Freaking Foldable!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/tv5_1441606990_502x234.jpg" border="0" alt="This Double-Sided, Transparent Television Is So Very Thin, It's Freaking Foldable!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/this-doublesided-transparent-television-is-so-very-thin-its-freaking-foldable-244950.html" title="This Double-Sided, Transparent Television Is So Very Thin, It's Freaking Foldable!">
                        This Double-Sided, Transparent Television Is So Very Thin, It's Freaking Foldable!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/we-drink-caffeine-for-energy-but-is-that-really-what-it-gives-us-244879.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/we-drink-caffeine-for-energy-but-is-that-really-what-it-gives-us-244879.html" class="tint" title="We Drink Caffeine For Energy. But Is That Really What It Gives Us?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/caffeine-card_1441357721_502x234.jpg" border="0" alt="We Drink Caffeine For Energy. But Is That Really What It Gives Us?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/we-drink-caffeine-for-energy-but-is-that-really-what-it-gives-us-244879.html" title="We Drink Caffeine For Energy. But Is That Really What It Gives Us?">
                        We Drink Caffeine For Energy. But Is That Really What It Gives Us?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-confessions-of-a-notsocool-girl-244584.html" class="tint" title="9 Confessions Of A Not-So-Cool Girl">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/ab_1440590774_1440590781_502x234.jpg" border="0" alt="9 Confessions Of A Not-So-Cool Girl" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-confessions-of-a-notsocool-girl-244584.html" title="9 Confessions Of A Not-So-Cool Girl">
                        9 Confessions Of A Not-So-Cool Girl                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-proposals-from-bollywood-films-that-will-melt-your-heart-244839.html" class="tint" title="11 Proposals From Bollywood Films That Will Melt Your Heart">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/srk_1441268952_1441268956_502x234.jpg" border="0" alt="11 Proposals From Bollywood Films That Will Melt Your Heart" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-proposals-from-bollywood-films-that-will-melt-your-heart-244839.html" title="11 Proposals From Bollywood Films That Will Melt Your Heart">
                        11 Proposals From Bollywood Films That Will Melt Your Heart                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-legit-ways-in-which-alcohol-can-help-you-lose-those-kilos-244838.html" class="tint" title="5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/alcohol_1441267169_502x234.jpg" border="0" alt="5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-legit-ways-in-which-alcohol-can-help-you-lose-those-kilos-244838.html" title="5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos">
                        5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/these-12-people-dont-have-any-right-to-drive-on-roads-heres-why-244932.html" class="tint" title="These 12 People Should Not Be Driving On Roads! Here's Why">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/worstdrivers_card_1441532487_502x234.jpg" border="0" alt="These 12 People Should Not Be Driving On Roads! Here's Why" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/these-12-people-dont-have-any-right-to-drive-on-roads-heres-why-244932.html" title="These 12 People Should Not Be Driving On Roads! Here's Why">
                        These 12 People Should Not Be Driving On Roads! Here's Why                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/the-vanity-vans-of-these-7-bollywood-stars-will-make-you-feel-very-poor-244884.html" class="tint" title="The Vanity Vans Of These 7 Bollywood Stars Will Make You Feel Very Poor!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441363089_1441363097_218x102.jpg" border="0" alt="The Vanity Vans Of These 7 Bollywood Stars Will Make You Feel Very Poor!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/the-vanity-vans-of-these-7-bollywood-stars-will-make-you-feel-very-poor-244884.html" title="The Vanity Vans Of These 7 Bollywood Stars Will Make You Feel Very Poor!">
                            The Vanity Vans Of These 7 Bollywood Stars Will Make You Feel Very Poor!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/travel/i-drove-on-the-indiathailand-highway-and-its-not-all-fun-games-and-breathtaking-landscapes-244975.html" class="tint" title="I Drove On The India-Thailand Highway And It Was Just As Awesome As You Imagined It!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/travel-card2_1441687983_218x102.jpg" border="0" alt="I Drove On The India-Thailand Highway And It Was Just As Awesome As You Imagined It!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/i-drove-on-the-indiathailand-highway-and-its-not-all-fun-games-and-breathtaking-landscapes-244975.html" title="I Drove On The India-Thailand Highway And It Was Just As Awesome As You Imagined It!">
                            I Drove On The India-Thailand Highway And It Was Just As Awesome As You Imagined It!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/watch-shahid-kapoor-and-alia-bhatt-go-bonkers-during-the-making-of-shaandaar-245013.html'>video</a>					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/watch-shahid-kapoor-and-alia-bhatt-go-bonkers-during-the-making-of-shaandaar-245013.html" class="tint" title="Watch Shahid Kapoor And Alia Bhatt Go Bonkers During The Making Of 'Shaandaar'">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/screenshot_3_1441709595_1441709606_218x102.jpg" border="0" alt="Watch Shahid Kapoor And Alia Bhatt Go Bonkers During The Making Of 'Shaandaar'"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/watch-shahid-kapoor-and-alia-bhatt-go-bonkers-during-the-making-of-shaandaar-245013.html" title="Watch Shahid Kapoor And Alia Bhatt Go Bonkers During The Making Of 'Shaandaar'">
                            Watch Shahid Kapoor And Alia Bhatt Go Bonkers During The Making Of 'Shaandaar'                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/sonam-kapoor-calls-meatban-a-misogynistic-move-gets-trolled-ruthlessly-by-judgemental-assholes-245007.html" class="tint" title="Sonam Kapoor Calls #Meatban A 'Misogynistic' Move, Gets Trolled Ruthlessly By 'Judgemental Assholes'!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441706814_218x102.jpg" border="0" alt="Sonam Kapoor Calls #Meatban A 'Misogynistic' Move, Gets Trolled Ruthlessly By 'Judgemental Assholes'!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/sonam-kapoor-calls-meatban-a-misogynistic-move-gets-trolled-ruthlessly-by-judgemental-assholes-245007.html" title="Sonam Kapoor Calls #Meatban A 'Misogynistic' Move, Gets Trolled Ruthlessly By 'Judgemental Assholes'!">
                            Sonam Kapoor Calls #Meatban A 'Misogynistic' Move, Gets Trolled Ruthlessly By 'Judgemental Assholes'!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/self/8-small-things-about-you-that-will-make-people-judge-you-almost-instantly-244982.html" class="tint" title="8 Small Things About You That Will Make People Judge You Almost Instantly">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441689278_1441689490_218x102.jpg" border="0" alt="8 Small Things About You That Will Make People Judge You Almost Instantly"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/8-small-things-about-you-that-will-make-people-judge-you-almost-instantly-244982.html" title="8 Small Things About You That Will Make People Judge You Almost Instantly">
                            8 Small Things About You That Will Make People Judge You Almost Instantly                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#hp_block_2').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       showBigAD2('bigAd2_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#hp_block_3').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      showBigAD3('bigAd3_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#container4').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible

       BADros('badRos_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '244777', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Sep/javed-502_1441797033_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/mandir-502_1441793353_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441784579_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/cop-cap-christof-stache-afp-getty-images_1441796347_1441796351_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441788410_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/msg_1441799442_1441799447_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/sunny-leone_1441780122_1441780129_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/sonakshi-sinha---bollywoodgo net_1441785637_1441785646_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-2_1441783131_1441783139_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-image_1441774910_1441774918_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/ffbb_1441700134_1441700142_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441430634_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/msg_1441799442_1441799447_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/videocafe/2015/Sep/petesamprasusopen_1441787191_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/disney-502_1441786833_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/wasted502_1441785497_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/clock-502_1441783530_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/kohlidhoni4reuters_1441780195_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441708840_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-2_1441783131_1441783139_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441706108_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/food-card_1441352601_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/sunny-leone_1441780122_1441780129_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441710154_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/ab-card_1441695449_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/bananal_1441374358_1441374370_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Sep/card_1441780454_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/gilrs-5502_1441777785_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/footballindiawc_1441725390_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/wallpaperscraft-6_1441713883_1441713887_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/currency-502_1441713593_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/card2_1441781197_1441781204_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441783864_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/love_card_1441732550_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/vikram-card2_1441779776_1441779835_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441708621_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/screenshot_3_1441709595_1441709606_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-image_1441774910_1441774918_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/ab-card_1441695449_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441430634_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/ffbb_1441700134_1441700142_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/food-card_1441352601_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-06_1441778899_1441778909_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441689278_1441689490_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441710154_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441711424_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/502_1441708822_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441707816_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441706814_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/vision_1441618721_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/nagin_card_1441698763_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/4card1_1441601248_1441601257_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cover_1441619655_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441363089_1441363097_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/screenshot_1_1441692602_1441692608_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cover_1441692703_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/bananal_1441374358_1441374370_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cholesterol-card_1441612237_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/wwww_1441365018_1441365030_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441363068_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441615068_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441699608_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/chennaidriver_card_1441629563_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441257391_1441257403_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/iphone6s-502_1441629189_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441700208_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/as-card_1441356229_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/jazbaa_1441611368_1441611376_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441097233_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/rw_1441609814_1441609820_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/fake-card_1441614921_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/pcos-card_1441093311_502x234.gif","http://media.indiatimes.in/media/videocafe/2015/Sep/toykiss_card_1441613229_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/hero_card_1441607021_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441169885_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/tv5_1441606990_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/caffeine-card_1441357721_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/ab_1440590774_1440590781_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/srk_1441268952_1441268956_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/alcohol_1441267169_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/worstdrivers_card_1441532487_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441363089_1441363097_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/travel-card2_1441687983_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/screenshot_3_1441709595_1441709606_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441706814_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441689278_1441689490_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,524,795<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>48,967 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.14" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.14"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.14"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.14"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.14"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>