



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='09/09/2015 21:52:07' /><meta property='busca:modified' content='09/09/2015 21:52:07' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/3c48889f8f35.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/12ba0da8a716.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "globo.com/globo.com/home", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-nacional/">
                                                <span class="titulo">Jornal Nacional</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/tenis/">
                                                <span class="titulo">TÃªnis</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-america/">
                                                    <span class="titulo">Copa AmÃ©rica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                <span class="titulo">Ã de Casa</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/esquenta/">
                                                    <span class="titulo">Esquenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/pe-na-cova/index.html">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-09-0921:54:11Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A primeira-area"><div class="destaque-principal-wide-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/noticia/2015/09/standard-and-poors-tira-grau-de-investimento-do-brasil.html" class=" " title="Brasil perde grau de investimento da S&amp;P"><div class="conteudo"><h2>Brasil perde grau de investimento da S&amp;P</h2></div></a></div></div><div class="destaque-wide-triplo-foto-topo destaque destaque-primeiro-scroll principal triplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><ul class="chamada"><li><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/09/parlamentares-reagem-negativamente-perda-do-grau-de-investimento.html" class=" " title="Congresso reage a rebaixamento de nota"><div class="conteudo"><h2>Congresso reage a rebaixamento de nota</h2></div></a></div></li><li><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/noticia/2015/09/o-que-significa-para-o-brasil-perda-do-grau-de-investimento-entenda.html" class=" " title="Nota Ã© rebaixada de BBB- para BB+; veja "><div class="conteudo"><h2>Nota Ã© rebaixada de BBB- para BB+; veja </h2></div></a></div></li><li><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/blog/thais-heredia/1.html" class=" " title="ThaÃ­s HerÃ©dia cita efeitos na economia"><div class="conteudo"><h2>ThaÃ­s HerÃ©dia cita efeitos na economia</h2></div></a></div></li></ul></div><div class="grid-base narrow pull-left"><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/09/camara-aprova-texto-que-restabelece-doacao-de-empresas-partidos.html" class=" " title="Texto que restabelece doaÃ§Ãµes de empresa a partido passa na CÃ¢mara"><div class="conteudo"><h2>Texto que restabelece doaÃ§Ãµes de empresa a partido passa na CÃ¢mara</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="AO VIVO: CÃ¢mara volta discutir texto da reforma polÃ­tica" href="http://g1.globo.com/politica/ao-vivo/2015/cobertura-ao-vivo.html">AO VIVO: CÃ¢mara volta discutir texto da reforma polÃ­tica</a></div></li></ul></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/09/namorada-de-romero-descobre-farsa-e-da-escandalo.html" class="foto " title="&#39;Regra&#39;: Paty dÃ¡ piti apÃ³s flagra (Isabella Pinheiro / Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/Z3awlhbTbuA5Q62PbBTh27ficbM=/filters:quality(10):strip_icc()/s2.glbimg.com/NwHGL2CThsWji0-da35MdS99tjo=/114x23:661x278/155x72/s.glbimg.com/et/gs/f/original/2015/09/09/paty.jpg" alt="&#39;Regra&#39;: Paty dÃ¡ piti apÃ³s flagra (Isabella Pinheiro / Gshow)" title="&#39;Regra&#39;: Paty dÃ¡ piti apÃ³s flagra (Isabella Pinheiro / Gshow)"
         data-original-image="s2.glbimg.com/NwHGL2CThsWji0-da35MdS99tjo=/114x23:661x278/155x72/s.glbimg.com/et/gs/f/original/2015/09/09/paty.jpg" data-url-smart_horizontal="Ys97_ud8rnbLMYg8wGmB4Nr2D-w=/90x56/smart/filters:strip_icc()/" data-url-smart="Ys97_ud8rnbLMYg8wGmB4Nr2D-w=/90x56/smart/filters:strip_icc()/" data-url-feature="Ys97_ud8rnbLMYg8wGmB4Nr2D-w=/90x56/smart/filters:strip_icc()/" data-url-tablet="ue014Jws75CzCfcaS-wCRocVQCc=/160xorig/smart/filters:strip_icc()/" data-url-desktop="nFjUCzAa2cZpeXsJZVjsO-mBL-A=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra&#39;: Paty dÃ¡ piti apÃ³s flagra</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/verdades-secretas/Vem-por-ai/noticia/2015/09/guilherme-e-giovanna-se-unem-contra-alex.html" class="foto " title="Gui se alia a Gio para flagrar Alex (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/aOQwOgp533nw2vs6j7YnWbyXkSI=/filters:quality(10):strip_icc()/s2.glbimg.com/i8jT4-LHJzIEhXPeWy2iSbGmBDk=/0x87:690x408/155x72/s.glbimg.com/et/gs/f/original/2015/09/08/giovanna.jpg" alt="Gui se alia a Gio para flagrar Alex (TV Globo)" title="Gui se alia a Gio para flagrar Alex (TV Globo)"
         data-original-image="s2.glbimg.com/i8jT4-LHJzIEhXPeWy2iSbGmBDk=/0x87:690x408/155x72/s.glbimg.com/et/gs/f/original/2015/09/08/giovanna.jpg" data-url-smart_horizontal="Sl3IRCRNi_rXxOC5BYbp8KqGfb0=/90x56/smart/filters:strip_icc()/" data-url-smart="Sl3IRCRNi_rXxOC5BYbp8KqGfb0=/90x56/smart/filters:strip_icc()/" data-url-feature="Sl3IRCRNi_rXxOC5BYbp8KqGfb0=/90x56/smart/filters:strip_icc()/" data-url-tablet="1620uUIwIntgUF9Pxo9hWxOAjNo=/160xorig/smart/filters:strip_icc()/" data-url-desktop="VsJbxaCpg2K6xNR_pLbTkax1MNM=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Gui se alia a Gio para flagrar Alex</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/pr/futebol/brasileirao-serie-a/jogo/09-09-2015/coritiba-fluminense/" class="foto " title="Coxa recebe o Flu e tenta fugir do Z-4; siga aqui (Monique Silva)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/hR63uF5ToHmOPcmkYDZPL_P-lDU=/filters:quality(10):strip_icc()/s2.glbimg.com/h8XOHtmY-2HkitK10SPZti_ZEmY=/0x911:2447x2016/155x70/s.glbimg.com/es/ge/f/original/2015/09/09/couto.jpeg" alt="Coxa recebe o Flu e tenta fugir do Z-4; siga aqui (Monique Silva)" title="Coxa recebe o Flu e tenta fugir do Z-4; siga aqui (Monique Silva)"
         data-original-image="s2.glbimg.com/h8XOHtmY-2HkitK10SPZti_ZEmY=/0x911:2447x2016/155x70/s.glbimg.com/es/ge/f/original/2015/09/09/couto.jpeg" data-url-smart_horizontal="XaU9cOi1ohuHEO9YEqSHDUAwEzk=/90x56/smart/filters:strip_icc()/" data-url-smart="XaU9cOi1ohuHEO9YEqSHDUAwEzk=/90x56/smart/filters:strip_icc()/" data-url-feature="XaU9cOi1ohuHEO9YEqSHDUAwEzk=/90x56/smart/filters:strip_icc()/" data-url-tablet="Ai8pgYacXFLBgMKV1zsp0kR4Hvo=/160xorig/smart/filters:strip_icc()/" data-url-desktop="DgTqHuMX89Zy7s_45PhWDXBqTKs=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Coxa recebe o Flu e tenta fugir do Z-4; siga aqui</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Figueirense empata com o AtlÃ©tico-PR; acompanhe aqui" href="http://globoesporte.globo.com/sc/futebol/brasileirao-serie-a/jogo/09-09-2015/figueirense-atletico-pr/">Figueirense empata com o AtlÃ©tico-PR; acompanhe aqui</a></div></li></ul></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/sp/futebol/brasileirao-serie-a/jogo/09-09-2015/corinthians-gremio/" class="foto " title="Lances: lÃ­der Corinthians recebe o GrÃªmio (Marcos Ribolli)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/uAZM_Cfw2HFHrOkIwQi5c19jBJk=/filters:quality(10):strip_icc()/s2.glbimg.com/S_cVxfNPTqAd7PIDSN_IfNWEWDI=/0x0:3264x2191/155x104/s.glbimg.com/es/ge/f/original/2015/09/09/img_3700.jpg" alt="Lances: lÃ­der Corinthians recebe o GrÃªmio (Marcos Ribolli)" title="Lances: lÃ­der Corinthians recebe o GrÃªmio (Marcos Ribolli)"
         data-original-image="s2.glbimg.com/S_cVxfNPTqAd7PIDSN_IfNWEWDI=/0x0:3264x2191/155x104/s.glbimg.com/es/ge/f/original/2015/09/09/img_3700.jpg" data-url-smart_horizontal="aLR0gRapeBa5KlUZa_p7oCvT6BY=/90x56/smart/filters:strip_icc()/" data-url-smart="aLR0gRapeBa5KlUZa_p7oCvT6BY=/90x56/smart/filters:strip_icc()/" data-url-feature="aLR0gRapeBa5KlUZa_p7oCvT6BY=/90x56/smart/filters:strip_icc()/" data-url-tablet="PgGOFhCynt8ZI8O72zDbuukwvQc=/160xorig/smart/filters:strip_icc()/" data-url-desktop="JzxdbQ5fcg_dmZ6Ove9-0bIS4vs=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Lances: lÃ­der Corinthians recebe o GrÃªmio</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/sp/santos-e-regiao/futebol/brasileirao-serie-a/jogo/09-09-2015/santos-sao-paulo/" class="foto " title="Siga tudo do clÃ¡ssico Santos e SÃ£o Paulo (Marcelo Hazan)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/4Pv2nc3Dcuos9U9e1Yh6ovux6_s=/filters:quality(10):strip_icc()/s2.glbimg.com/VWhH2ufYq4P8Dy3e6InwJy3bkeM=/199x387:1470x1240/155x104/s.glbimg.com/es/ge/f/original/2015/09/09/vila_3.jpg" alt="Siga tudo do clÃ¡ssico Santos e SÃ£o Paulo (Marcelo Hazan)" title="Siga tudo do clÃ¡ssico Santos e SÃ£o Paulo (Marcelo Hazan)"
         data-original-image="s2.glbimg.com/VWhH2ufYq4P8Dy3e6InwJy3bkeM=/199x387:1470x1240/155x104/s.glbimg.com/es/ge/f/original/2015/09/09/vila_3.jpg" data-url-smart_horizontal="COvGLL-lQRTC1IlB3Yn6I3FDG-E=/90x56/smart/filters:strip_icc()/" data-url-smart="COvGLL-lQRTC1IlB3Yn6I3FDG-E=/90x56/smart/filters:strip_icc()/" data-url-feature="COvGLL-lQRTC1IlB3Yn6I3FDG-E=/90x56/smart/filters:strip_icc()/" data-url-tablet="msfnW_7wSsiMaFR1eeyDowjeQKc=/160xorig/smart/filters:strip_icc()/" data-url-desktop="eZ46aZeVR1xbee1v1hv8wxj2X4c=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Siga tudo do clÃ¡ssico Santos e SÃ£o Paulo</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/rs/futebol/brasileirao-serie-a/jogo/09-09-2015/internacional-palmeiras/" class="foto " title="Inter faz no inÃ­cio e segura vitÃ³ria contra Palmeiras (Diego Guichard/GloboEsporte.com)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/sY0XrCAOvKjYtn8Ts0gK8MsIFVk=/filters:quality(10):strip_icc()/s2.glbimg.com/o3pATnMxoZDtgjYbxoSry3Cm6I4=/118x57:528x242/155x70/s.glbimg.com/jo/g1/f/original/2015/09/09/gol4.jpg" alt="Inter faz no inÃ­cio e segura vitÃ³ria contra Palmeiras (Diego Guichard/GloboEsporte.com)" title="Inter faz no inÃ­cio e segura vitÃ³ria contra Palmeiras (Diego Guichard/GloboEsporte.com)"
         data-original-image="s2.glbimg.com/o3pATnMxoZDtgjYbxoSry3Cm6I4=/118x57:528x242/155x70/s.glbimg.com/jo/g1/f/original/2015/09/09/gol4.jpg" data-url-smart_horizontal="ayv3v4BJZ-PqrKDqDl7SGgzMnBQ=/90x56/smart/filters:strip_icc()/" data-url-smart="ayv3v4BJZ-PqrKDqDl7SGgzMnBQ=/90x56/smart/filters:strip_icc()/" data-url-feature="ayv3v4BJZ-PqrKDqDl7SGgzMnBQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="i5FVeW2Uuqxv-RG3Evh1s3fxuAE=/160xorig/smart/filters:strip_icc()/" data-url-desktop="DpFDJoWsHl208UUH1UMCGq5xfgM=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Inter faz no inÃ­cio e segura vitÃ³ria contra Palmeiras</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/sp/campinas-e-regiao/futebol/brasileirao-serie-a/jogo/09-09-2015/ponte-preta-vasco/" class="foto " title="Vasco finda 52 dias de jejum ao vencer a Ponte (MARCOS BEZERRA/FUTURA PRESS/ESTADÃO CONTEÃDO)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/RlcZ5CmcR5M6bjwoT1cn2NW4iaI=/filters:quality(10):strip_icc()/s2.glbimg.com/HEiyFoCDXJFqXDokBy0b9Vkw4QI=/0x219:2287x1253/155x70/s.glbimg.com/es/ge/f/original/2015/09/09/fup20150909762.jpg" alt="Vasco finda 52 dias de jejum ao vencer a Ponte (MARCOS BEZERRA/FUTURA PRESS/ESTADÃO CONTEÃDO)" title="Vasco finda 52 dias de jejum ao vencer a Ponte (MARCOS BEZERRA/FUTURA PRESS/ESTADÃO CONTEÃDO)"
         data-original-image="s2.glbimg.com/HEiyFoCDXJFqXDokBy0b9Vkw4QI=/0x219:2287x1253/155x70/s.glbimg.com/es/ge/f/original/2015/09/09/fup20150909762.jpg" data-url-smart_horizontal="loFPObYVC3xZ28JRe9PTnPoR77s=/90x56/smart/filters:strip_icc()/" data-url-smart="loFPObYVC3xZ28JRe9PTnPoR77s=/90x56/smart/filters:strip_icc()/" data-url-feature="loFPObYVC3xZ28JRe9PTnPoR77s=/90x56/smart/filters:strip_icc()/" data-url-tablet="qvLArr6tAxzzSm3xbW0-eBk2GSQ=/160xorig/smart/filters:strip_icc()/" data-url-desktop="Pe8cHHhbQ_i5sTZ1EjoqyJW26qQ=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Vasco finda 52 dias de jejum ao vencer a Ponte</h2></div></a></div></div></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/busca/?q=futebol">futebol</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-vasco-leandrao-arrisca-de-fora-e-encaixa-no-fundo-das-redes-aos-29-do-2o-tempo/4455847/"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-vasco-leandrao-arrisca-de-fora-e-encaixa-no-fundo-das-redes-aos-29-do-2o-tempo/4455847/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4455847.jpg" alt="Atacante LeandrÃ£o arrisca e marca para o Vasco em Campinas" /><div class="time">01:15</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">ponte preta x vasco</span><span class="title">Atacante LeandrÃ£o arrisca e marca para o Vasco em Campinas</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-atletico-mg-luan-marca-de-letra-aos-11-do-1o-tempo/4455443/"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-atletico-mg-luan-marca-de-letra-aos-11-do-1o-tempo/4455443/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4455443.jpg" alt="Que golaÃ§o! Luan marca de letra para o Galo contra o AvaÃ­ em BH" /><div class="time">01:15</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">atlÃ©tico-mg x avaÃ­</span><span class="title">Que golaÃ§o! Luan marca de letra para o Galo contra o AvaÃ­ em BH</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-internacional-dalessandro-cobra-falta-e-nilton-desvia-aos-19-do-1o-tempo/4455543/"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-internacional-dalessandro-cobra-falta-e-nilton-desvia-aos-19-do-1o-tempo/4455543/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4455543.jpg" alt="D&#39;Alessandro cobra falta e Nilton desvia para abri o marcador" /><div class="time">00:47</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">internacional x palmeiras</span><span class="title">D&#39;Alessandro cobra falta e Nilton desvia para abri o marcador</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-vasco-leandrao-arrisca-de-fora-e-encaixa-no-fundo-das-redes-aos-29-do-2o-tempo/4455847/"><span class="subtitle">ponte preta x vasco</span><span class="title">Atacante LeandrÃ£o arrisca e marca para o Vasco em Campinas</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-atletico-mg-luan-marca-de-letra-aos-11-do-1o-tempo/4455443/"><span class="subtitle">atlÃ©tico-mg x avaÃ­</span><span class="title">Que golaÃ§o! Luan marca de letra para o Galo contra o AvaÃ­ em BH</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-internacional-dalessandro-cobra-falta-e-nilton-desvia-aos-19-do-1o-tempo/4455543/"><span class="subtitle">internacional x palmeiras</span><span class="title">D&#39;Alessandro cobra falta e Nilton desvia para abri o marcador</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-esporte"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/globocom/tempo-real/v/gol-do-vasco-leandrao-arrisca-de-fora-e-encaixa-no-fundo-das-redes-aos-29-do-2o-tempo/4455847/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/globocom/tempo-real/v/gol-do-atletico-mg-luan-marca-de-letra-aos-11-do-1o-tempo/4455443/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/globocom/tempo-real/v/gol-do-internacional-dalessandro-cobra-falta-e-nilton-desvia-aos-19-do-1o-tempo/4455543/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais<strong> vÃ­deos</strong><span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/joelma-vai-delegacia-para-denunciar-chimbinha-por-difamacao-acompanhada-de-empregadas-causa-tumulto-17444398.html" class="foto" title="Jornal diz que empregadas de Joelma teriam recebido ameaÃ§as de Chimbinha (JR Avelar)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/JXD0GRUYf7SL4906qwNc-Xdc5i0=/filters:quality(10):strip_icc()/s2.glbimg.com/w7vB2eOqm205frFMq8hV5uIBWM4=/51x5:552x274/335x180/s.glbimg.com/en/ho/f/original/2015/09/09/joelma.jpg" alt="Jornal diz que empregadas de Joelma teriam recebido ameaÃ§as de Chimbinha (JR Avelar)" title="Jornal diz que empregadas de Joelma teriam recebido ameaÃ§as de Chimbinha (JR Avelar)"
                data-original-image="s2.glbimg.com/w7vB2eOqm205frFMq8hV5uIBWM4=/51x5:552x274/335x180/s.glbimg.com/en/ho/f/original/2015/09/09/joelma.jpg" data-url-smart_horizontal="_jghHiO_lNybwJt08SctkC6AVuM=/90x0/smart/filters:strip_icc()/" data-url-smart="_jghHiO_lNybwJt08SctkC6AVuM=/90x0/smart/filters:strip_icc()/" data-url-feature="_jghHiO_lNybwJt08SctkC6AVuM=/90x0/smart/filters:strip_icc()/" data-url-tablet="eB3ScQ39-TAeCIrxxmyzXcUcUTo=/220x125/smart/filters:strip_icc()/" data-url-desktop="kKZN5F7gVxj0YtRKrD-_RwvnHWM=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jornal diz que empregadas de Joelma teriam recebido ameaÃ§as de Chimbinha</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:39:54" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/casos-de-policia/de-salto-alto-policial-paisana-detem-homem-que-tentava-atacar-pedestre-com-estilete-17444835.html" class="foto" title="Inspetora de fÃ©rias saca pistola e prende suspeito no Centro do RJ; veja as fotos (WhatsApp)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/a4QZS_hqmgKK60vjgNDl8kMTdrI=/filters:quality(10):strip_icc()/s2.glbimg.com/zid2KI5T0lE-bGGlChRks4ejONk=/85x70:268x191/120x80/s.glbimg.com/en/ho/f/original/2015/09/09/inspetora.jpg" alt="Inspetora de fÃ©rias saca pistola e prende suspeito no Centro do RJ; veja as fotos (WhatsApp)" title="Inspetora de fÃ©rias saca pistola e prende suspeito no Centro do RJ; veja as fotos (WhatsApp)"
                data-original-image="s2.glbimg.com/zid2KI5T0lE-bGGlChRks4ejONk=/85x70:268x191/120x80/s.glbimg.com/en/ho/f/original/2015/09/09/inspetora.jpg" data-url-smart_horizontal="12Yfa4OkWfDkJEHqGrq1IeK9MbY=/90x0/smart/filters:strip_icc()/" data-url-smart="12Yfa4OkWfDkJEHqGrq1IeK9MbY=/90x0/smart/filters:strip_icc()/" data-url-feature="12Yfa4OkWfDkJEHqGrq1IeK9MbY=/90x0/smart/filters:strip_icc()/" data-url-tablet="NFVrq5dSr0WbiBdubW661C2D9f8=/70x50/smart/filters:strip_icc()/" data-url-desktop="ZyWIJPGdmDrlqpMrHVaf7iUU-2k=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Inspetora de fÃ©rias saca pistola e prende suspeito no Centro do RJ; veja as fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 19:31:56" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/am/amazonas/noticia/2015/09/pensao-semanal-de-r-400-levou-pai-jogar-bebe-em-rio-no-am-diz-policia.html" class="foto" title="PensÃ£o semanal de R$ 400 levou pai a jogar bebÃª em rio, de acordo com a polÃ­cia (DivulgaÃ§Ã£o PolÃ­cia Civil e Luis Henrique/G1 AM)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/qJi9YGeKvIismC63oTyyiHOq5YA=/filters:quality(10):strip_icc()/s2.glbimg.com/2hDdjJBFJsxXQHPvuI8M-eO-IHA=/0x23:620x436/120x80/s.glbimg.com/jo/g1/f/original/2015/09/08/pais_bebe.jpg" alt="PensÃ£o semanal de R$ 400 levou pai a jogar bebÃª em rio, de acordo com a polÃ­cia (DivulgaÃ§Ã£o PolÃ­cia Civil e Luis Henrique/G1 AM)" title="PensÃ£o semanal de R$ 400 levou pai a jogar bebÃª em rio, de acordo com a polÃ­cia (DivulgaÃ§Ã£o PolÃ­cia Civil e Luis Henrique/G1 AM)"
                data-original-image="s2.glbimg.com/2hDdjJBFJsxXQHPvuI8M-eO-IHA=/0x23:620x436/120x80/s.glbimg.com/jo/g1/f/original/2015/09/08/pais_bebe.jpg" data-url-smart_horizontal="Pe9dknOhnLVmA0D88aByqn2ucNc=/90x0/smart/filters:strip_icc()/" data-url-smart="Pe9dknOhnLVmA0D88aByqn2ucNc=/90x0/smart/filters:strip_icc()/" data-url-feature="Pe9dknOhnLVmA0D88aByqn2ucNc=/90x0/smart/filters:strip_icc()/" data-url-tablet="Jgm897iwU7frkCV4-9TnJcJpl80=/70x50/smart/filters:strip_icc()/" data-url-desktop="f4w_YpOwR2LEJXNXosEKBrEv8hU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PensÃ£o semanal de R$ 400 levou pai a jogar bebÃª em rio, de acordo com a polÃ­cia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:22:34" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rs/rio-grande-do-sul/noticia/2015/09/menina-de-13-anos-e-estuprada-por-quatro-adolescentes-no-rs-diz-bm.html" class="" title="Quatro menores de 15 e 16 anos invadem casa e estupram garota de 13 no litoral do RS (Arquivo Pessoal)" rel="bookmark"><span class="conteudo"><p>Quatro menores de 15 e 16 anos invadem casa e estupram garota de 13 no litoral do RS</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 21:16:31" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/09/iphone-6s-lancamento-da-apple-acontece-nos-estados-unidos.html" class="foto" title="iPhone 6S com cÃ¢mera 4K e iPad Pro: conheÃ§a os lanÃ§amentos da Apple (Apple)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/1PZRnsFmZTqUtOn6jYKwJqaKNTI=/filters:quality(10):strip_icc()/s2.glbimg.com/BYbVeUXNE2lZ7d0CUy19FHchUJE=/64x62:531x363/155x100/s.glbimg.com/po/tt2/f/original/2015/09/09/iphonecores.png" alt="iPhone 6S com cÃ¢mera 4K e iPad Pro: conheÃ§a os lanÃ§amentos da Apple (Apple)" title="iPhone 6S com cÃ¢mera 4K e iPad Pro: conheÃ§a os lanÃ§amentos da Apple (Apple)"
                data-original-image="s2.glbimg.com/BYbVeUXNE2lZ7d0CUy19FHchUJE=/64x62:531x363/155x100/s.glbimg.com/po/tt2/f/original/2015/09/09/iphonecores.png" data-url-smart_horizontal="M5iuObXdL-9LN-CuyVNgDBYogpw=/90x0/smart/filters:strip_icc()/" data-url-smart="M5iuObXdL-9LN-CuyVNgDBYogpw=/90x0/smart/filters:strip_icc()/" data-url-feature="M5iuObXdL-9LN-CuyVNgDBYogpw=/90x0/smart/filters:strip_icc()/" data-url-tablet="VAwuM_H2jM3Wiq_PULaIVe6rAa0=/70x50/smart/filters:strip_icc()/" data-url-desktop="YDLbrP6N-waB_7UFwTRdETgYAng=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>iPhone 6S com cÃ¢mera 4K <br />e iPad Pro: conheÃ§a os lanÃ§amentos da Apple</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 14:22:09" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/09/como-usar-o-app-wifi-free-e-encontrar-redes-de-wi-fi.html" class="foto" title="App &#39;mÃ¡gico&#39; encontra redes de Wi-Fi para seu smart em &#39;qualquer lugar&#39;; saiba usar (Pond5)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/j_dwOdLNAy9FFsxWnfMiwJuwWG8=/filters:quality(10):strip_icc()/s2.glbimg.com/r-bkvBlePAAHKdYz4kjgGtmk3lo=/0x106:587x497/120x80/s.glbimg.com/po/tt2/f/original/2015/07/17/037107562-hand-holding-smartphone-wi-fi-.jpeg" alt="App &#39;mÃ¡gico&#39; encontra redes de Wi-Fi para seu smart em &#39;qualquer lugar&#39;; saiba usar (Pond5)" title="App &#39;mÃ¡gico&#39; encontra redes de Wi-Fi para seu smart em &#39;qualquer lugar&#39;; saiba usar (Pond5)"
                data-original-image="s2.glbimg.com/r-bkvBlePAAHKdYz4kjgGtmk3lo=/0x106:587x497/120x80/s.glbimg.com/po/tt2/f/original/2015/07/17/037107562-hand-holding-smartphone-wi-fi-.jpeg" data-url-smart_horizontal="VN-zvBBAXgm34FIqj5bzPSKo0Vs=/90x0/smart/filters:strip_icc()/" data-url-smart="VN-zvBBAXgm34FIqj5bzPSKo0Vs=/90x0/smart/filters:strip_icc()/" data-url-feature="VN-zvBBAXgm34FIqj5bzPSKo0Vs=/90x0/smart/filters:strip_icc()/" data-url-tablet="y2DvQwzANV1WuXDVdy2fZN-H74w=/70x50/smart/filters:strip_icc()/" data-url-desktop="taKE3Nexx08Bqshzzq0Cs_hmswc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>App &#39;mÃ¡gico&#39; encontra redes de Wi-Fi para seu smart em &#39;qualquer lugar&#39;; saiba usar</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:46:34" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/loterias/noticia/2015/09/mega-sena-concurso-1740-resultado.html" class="" title="Mega-Sena acumula e pode pagar R$ 30 milhÃµes; confira as dezenas sorteadas nesta quarta-feira (ReproduÃ§Ã£o / Facebook / Melissa Mackenzie)" rel="bookmark"><span class="conteudo"><p>Mega-Sena acumula e pode pagar R$ 30 milhÃµes; confira as dezenas sorteadas nesta quarta-feira</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:22:34" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/mundo/noticia/2015/09/jovem-cai-da-famosa-pedra-lingua-de-troll-na-noruega-e-morre.html" class="foto" title="Jovem australiana morre apÃ³s cair da famosa pedra &#39;LÃ­ngua de Troll&#39; na Noruega (Caters News/The Grosby Group)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/IiHt5uwIo5YSmst1utve_MM76VQ=/filters:quality(10):strip_icc()/s2.glbimg.com/V2TEIBu4m3pRlvMroECcs_CaF6w=/520x228:945x511/120x80/s.glbimg.com/jo/g1/f/original/2015/08/27/pirueta-precipicio2.jpg" alt="Jovem australiana morre apÃ³s cair da famosa pedra &#39;LÃ­ngua de Troll&#39; na Noruega (Caters News/The Grosby Group)" title="Jovem australiana morre apÃ³s cair da famosa pedra &#39;LÃ­ngua de Troll&#39; na Noruega (Caters News/The Grosby Group)"
                data-original-image="s2.glbimg.com/V2TEIBu4m3pRlvMroECcs_CaF6w=/520x228:945x511/120x80/s.glbimg.com/jo/g1/f/original/2015/08/27/pirueta-precipicio2.jpg" data-url-smart_horizontal="Q5feXS_dre3uCMLQ8XmqphAXhSo=/90x0/smart/filters:strip_icc()/" data-url-smart="Q5feXS_dre3uCMLQ8XmqphAXhSo=/90x0/smart/filters:strip_icc()/" data-url-feature="Q5feXS_dre3uCMLQ8XmqphAXhSo=/90x0/smart/filters:strip_icc()/" data-url-tablet="tLCFxmPR91RQS7x9yp16mZNdg8A=/70x50/smart/filters:strip_icc()/" data-url-desktop="aUwJIpRsXYDuk3BYFan-p5kAcN8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jovem australiana morre apÃ³s cair da famosa pedra &#39;LÃ­ngua de Troll&#39; na Noruega</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 21:14:21" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/noticia/2015/09/camara-aprova-projeto-de-lei-que-proibe-aplicativo-uber-em-sp.html" class="foto" title="CÃ¢mara aprova projeto de lei que proÃ­be aplicativo Uber em SÃ£o Paulo (Nelson Antoine/Frame/EstadÃ£o ConteÃºdo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/V9TpNqAqG2RJQD77lfMXOKz2b1o=/filters:quality(10):strip_icc()/s2.glbimg.com/yAl2cBNMebO7zD9N3VRy-oNltXQ=/0x403:600x673/155x70/s.glbimg.com/jo/g1/f/original/2015/09/09/sp-uber-votacao-taxistas-comemoram.jpg" alt="CÃ¢mara aprova projeto de lei que proÃ­be aplicativo Uber em SÃ£o Paulo (Nelson Antoine/Frame/EstadÃ£o ConteÃºdo)" title="CÃ¢mara aprova projeto de lei que proÃ­be aplicativo Uber em SÃ£o Paulo (Nelson Antoine/Frame/EstadÃ£o ConteÃºdo)"
                data-original-image="s2.glbimg.com/yAl2cBNMebO7zD9N3VRy-oNltXQ=/0x403:600x673/155x70/s.glbimg.com/jo/g1/f/original/2015/09/09/sp-uber-votacao-taxistas-comemoram.jpg" data-url-smart_horizontal="EECKtZe4CGyt3PQz_pDwYXbb2xs=/90x0/smart/filters:strip_icc()/" data-url-smart="EECKtZe4CGyt3PQz_pDwYXbb2xs=/90x0/smart/filters:strip_icc()/" data-url-feature="EECKtZe4CGyt3PQz_pDwYXbb2xs=/90x0/smart/filters:strip_icc()/" data-url-tablet="0xvV6KosZIrCyme7y805xtyHtng=/70x50/smart/filters:strip_icc()/" data-url-desktop="U0N3or1NJDyzHWCK6bocsAQBhhk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>CÃ¢mara aprova projeto <br />de lei que proÃ­be aplicativo Uber em SÃ£o Paulo</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/mg/futebol/brasileirao-serie-a/jogo/09-09-2015/atletico-mg-avai/" class="foto" title="AtlÃ©tico-MG faz grande primeiro tempo, bate AvaÃ­ e segue caÃ§ando o Corinthians (Bruno Cantini/AtlÃ©tico MG)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/s85-hWsEYgSSXZm5RCr3SAAbN-I=/filters:quality(10):strip_icc()/s2.glbimg.com/_714Zs9AzstOAzr20dYryuoN5Ro=/0x90:2000x1165/335x180/s.glbimg.com/es/ge/f/original/2015/09/09/21095589899_6e452f8c35_o_1.jpg" alt="AtlÃ©tico-MG faz grande primeiro tempo, bate AvaÃ­ e segue caÃ§ando o Corinthians (Bruno Cantini/AtlÃ©tico MG)" title="AtlÃ©tico-MG faz grande primeiro tempo, bate AvaÃ­ e segue caÃ§ando o Corinthians (Bruno Cantini/AtlÃ©tico MG)"
                data-original-image="s2.glbimg.com/_714Zs9AzstOAzr20dYryuoN5Ro=/0x90:2000x1165/335x180/s.glbimg.com/es/ge/f/original/2015/09/09/21095589899_6e452f8c35_o_1.jpg" data-url-smart_horizontal="F3hdkrMpAagY5MdDwNiElPpnysI=/90x0/smart/filters:strip_icc()/" data-url-smart="F3hdkrMpAagY5MdDwNiElPpnysI=/90x0/smart/filters:strip_icc()/" data-url-feature="F3hdkrMpAagY5MdDwNiElPpnysI=/90x0/smart/filters:strip_icc()/" data-url-tablet="iNKmD2scFWTPNHVWsvicCaQMHMY=/220x125/smart/filters:strip_icc()/" data-url-desktop="QztEYfC0KAzP2aObvWk5Oyokok4=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>AtlÃ©tico-MG faz grande primeiro tempo, bate AvaÃ­ e segue caÃ§ando o Corinthians</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 19:44:33" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/sp/sorocaba/futebol/noticia/2015/09/arbitro-dispara-apos-reducao-de-pena-de-dudu-por-agressao-um-absurdo.html" class="foto" title="Ãrbitro agredido dispara depois da reduÃ§Ã£o de pena de Dudu: &#39;7 a 1 foi pouco&#39; (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/EdlL1roAfwg2lOEhsHVBnEwinFo=/filters:quality(10):strip_icc()/s2.glbimg.com/vPWonJAlKOEVA8uSwAmNcbuAfHo=/0x1:127x85/120x80/e.glbimg.com/og/ed/f/original/2015/05/03/ceretta-home.jpg" alt="Ãrbitro agredido dispara depois da reduÃ§Ã£o de pena de Dudu: &#39;7 a 1 foi pouco&#39; (DivulgaÃ§Ã£o)" title="Ãrbitro agredido dispara depois da reduÃ§Ã£o de pena de Dudu: &#39;7 a 1 foi pouco&#39; (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/vPWonJAlKOEVA8uSwAmNcbuAfHo=/0x1:127x85/120x80/e.glbimg.com/og/ed/f/original/2015/05/03/ceretta-home.jpg" data-url-smart_horizontal="vR7GYYSTOIvKlUhB0Dox6W-wcnY=/90x0/smart/filters:strip_icc()/" data-url-smart="vR7GYYSTOIvKlUhB0Dox6W-wcnY=/90x0/smart/filters:strip_icc()/" data-url-feature="vR7GYYSTOIvKlUhB0Dox6W-wcnY=/90x0/smart/filters:strip_icc()/" data-url-tablet="CnHCpvaNZFXyTX99JmflYTKZisA=/70x50/smart/filters:strip_icc()/" data-url-desktop="bsBQ26ty06kNLlSXZCXkBlgRNYk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ãrbitro agredido dispara depois da reduÃ§Ã£o de pena <br />de Dudu: &#39;7 a 1 foi pouco&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 17:18:38" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/fluminense/muricy-ramalho-revela-nao-ter-magoas-do-fluminense-diz-que-voltaria-treinar-equipe-17443542.html" class="foto" title="Hoje sem time, Muricy nega ter mÃ¡goa e diz que voltaria ao Flu: &#39;Ã um grande clube&#39; (AE)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Nac9mQtWC_r0bZcQHvkbW5O6iHk=/filters:quality(10):strip_icc()/s2.glbimg.com/yEn57CtahzncekSXu3z8qHgrFcw=/96x10:216x90/120x80/s.glbimg.com/es/ge/f/original/2010/08/16/muricy_fluminense_ae_30.jpg" alt="Hoje sem time, Muricy nega ter mÃ¡goa e diz que voltaria ao Flu: &#39;Ã um grande clube&#39; (AE)" title="Hoje sem time, Muricy nega ter mÃ¡goa e diz que voltaria ao Flu: &#39;Ã um grande clube&#39; (AE)"
                data-original-image="s2.glbimg.com/yEn57CtahzncekSXu3z8qHgrFcw=/96x10:216x90/120x80/s.glbimg.com/es/ge/f/original/2010/08/16/muricy_fluminense_ae_30.jpg" data-url-smart_horizontal="b2IbbSy2r20asCgUzY2Z_j2g4tk=/90x0/smart/filters:strip_icc()/" data-url-smart="b2IbbSy2r20asCgUzY2Z_j2g4tk=/90x0/smart/filters:strip_icc()/" data-url-feature="b2IbbSy2r20asCgUzY2Z_j2g4tk=/90x0/smart/filters:strip_icc()/" data-url-tablet="puuA_R51Z81nqjiUnIxzplO4dZg=/70x50/smart/filters:strip_icc()/" data-url-desktop="krryGABMHOiPhJ3Bq983sjEWUiA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Hoje sem time, Muricy nega ter mÃ¡goa e diz que voltaria ao Flu: &#39;Ã um grande clube&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:25:05" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/eventos/US-Open/no-ar/us-open-de-tenis-2015.html" class="" title="Federer e Gasquet se enfrentam nas quartas do US Open; clique e acompanhe em tempo real (Leo Correa / MoWA Press)" rel="bookmark"><span class="conteudo"><p>Federer e Gasquet se enfrentam nas quartas do US Open; clique e acompanhe em tempo real</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 19:54:42" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/futebol-ingles/noticia/2015/09/meia-ingles-e-absolvido-de-pena-por-uso-de-cocaina-apos-morte-de-filha.html" class="foto" title="Meia inglÃªs Ã© absolvido de pena por uso de cocaÃ­na depois da morte da filha
 (AgÃªncia Reuters)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/7YOJPykq17XBn0ZGVDQ-HzrBJoM=/filters:quality(10):strip_icc()/s2.glbimg.com/nYBpQG4r0lN3yfSD-Z-xmNJCAX0=/106x0:565x305/120x80/s.glbimg.com/es/ge/f/original/2013/08/31/livermore_aguero-reu.jpg" alt="Meia inglÃªs Ã© absolvido de pena por uso de cocaÃ­na depois da morte da filha
 (AgÃªncia Reuters)" title="Meia inglÃªs Ã© absolvido de pena por uso de cocaÃ­na depois da morte da filha
 (AgÃªncia Reuters)"
                data-original-image="s2.glbimg.com/nYBpQG4r0lN3yfSD-Z-xmNJCAX0=/106x0:565x305/120x80/s.glbimg.com/es/ge/f/original/2013/08/31/livermore_aguero-reu.jpg" data-url-smart_horizontal="daAa0o3ARFMNSU7SwzCQ_UwxGPo=/90x0/smart/filters:strip_icc()/" data-url-smart="daAa0o3ARFMNSU7SwzCQ_UwxGPo=/90x0/smart/filters:strip_icc()/" data-url-feature="daAa0o3ARFMNSU7SwzCQ_UwxGPo=/90x0/smart/filters:strip_icc()/" data-url-tablet="k6J0RQeYMvs92WnAJ_XhliGxRy8=/70x50/smart/filters:strip_icc()/" data-url-desktop="3_7DTnSCf1VPJefc1jwVwVIZgAg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Meia inglÃªs Ã© absolvido de pena por uso de cocaÃ­na depois da morte da filha<br /></p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 18:34:36" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/noticia/2015/09/pelo-twitter-juninho-pernambucano-critica-auditores-do-stjd-torcedores.html" class="foto" title="Caso Sheik faz Juninho criticar auditores do STJD em rede social: &#39;Torcedores&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/vlWsKeHRv-YY3dHhXrK0Ua-XgiU=/filters:quality(10):strip_icc()/s2.glbimg.com/DChE9pWG6wx-x_7gmQ5LRsnyIZI=/7x41:214x178/120x80/s.glbimg.com/es/ge/f/original/2015/09/09/juninho_vale.jpg" alt="Caso Sheik faz Juninho criticar auditores do STJD em rede social: &#39;Torcedores&#39; (ReproduÃ§Ã£o)" title="Caso Sheik faz Juninho criticar auditores do STJD em rede social: &#39;Torcedores&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/DChE9pWG6wx-x_7gmQ5LRsnyIZI=/7x41:214x178/120x80/s.glbimg.com/es/ge/f/original/2015/09/09/juninho_vale.jpg" data-url-smart_horizontal="MhtHnaa-re0xeL2ftCv89XD5SZ4=/90x0/smart/filters:strip_icc()/" data-url-smart="MhtHnaa-re0xeL2ftCv89XD5SZ4=/90x0/smart/filters:strip_icc()/" data-url-feature="MhtHnaa-re0xeL2ftCv89XD5SZ4=/90x0/smart/filters:strip_icc()/" data-url-tablet="d7c5LFAN_EGO09I9DLa4XtA7-vE=/70x50/smart/filters:strip_icc()/" data-url-desktop="7e6fdFbhtkGWbdXImlmC_YbA7sw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Caso Sheik faz Juninho criticar auditores do STJD <br />em rede social: &#39;Torcedores&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 19:28:14" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/pombo-sem-asa/post/com-uso-de-drogas-em-campo-jogo-do-carioca-feminino-tem-atraso-de-uma-hora.html#GE-DESTAQUES-user-sel-10,editorial,1218373041" class="" title="Jogo feminino de futebol do RJ atrasa 52 min por ter pessoas consumindo drogas em campo" rel="bookmark"><span class="conteudo"><p>Jogo feminino de futebol do RJ atrasa 52 min por ter pessoas consumindo drogas em campo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/09/empresario-de-cristiano-ronaldo-e-mourinho-quer-levar-gallo-para-europa.html" class="foto" title="EmpresÃ¡rio de Cristiano Ronaldo e Mourinho quer levar Gallo para a Europa (ReproduÃ§Ã£o/SporTV)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/4OYgBsY2JtMWt5T_7B50Ignf67c=/filters:quality(10):strip_icc()/s2.glbimg.com/e8qH7NEIDMMBI3OqkTvXlD4JFDg=/162x45:546x301/120x80/s.glbimg.com/es/ge/f/original/2015/05/21/gallo.png" alt="EmpresÃ¡rio de Cristiano Ronaldo e Mourinho quer levar Gallo para a Europa (ReproduÃ§Ã£o/SporTV)" title="EmpresÃ¡rio de Cristiano Ronaldo e Mourinho quer levar Gallo para a Europa (ReproduÃ§Ã£o/SporTV)"
                data-original-image="s2.glbimg.com/e8qH7NEIDMMBI3OqkTvXlD4JFDg=/162x45:546x301/120x80/s.glbimg.com/es/ge/f/original/2015/05/21/gallo.png" data-url-smart_horizontal="NnXSI7wHTrNr1a2i9wOP5vEx8QE=/90x0/smart/filters:strip_icc()/" data-url-smart="NnXSI7wHTrNr1a2i9wOP5vEx8QE=/90x0/smart/filters:strip_icc()/" data-url-feature="NnXSI7wHTrNr1a2i9wOP5vEx8QE=/90x0/smart/filters:strip_icc()/" data-url-tablet="tmjo2yTiSdeCFqsipDxs67eP1Mk=/70x50/smart/filters:strip_icc()/" data-url-desktop="swAyoOUE9QI0xX-yw8R101G1wnQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>EmpresÃ¡rio de Cristiano Ronaldo e Mourinho quer levar Gallo para a Europa</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 18:25:06" class="chamada chamada-principal mobile-grid-full"><a href="http://blogs.oglobo.globo.com/radicais/post/surfista-quebra-pescoco-em-quatro-partes-apos-pegar-onda-de-sete-metros.html" class="foto" title="Surfista quebra o pescoÃ§o em 4 partes, tem traumatismo em onda de 7 m e sobrevive (reproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/7HwR8NUHhpo_bc4gPSQKw6ZJ3fA=/filters:quality(10):strip_icc()/s2.glbimg.com/JpMbq7LfO0McuUuK4V7ssvkl_vk=/153x131:443x325/120x80/s.glbimg.com/en/ho/f/original/2015/09/09/shawn-dollar.jpg" alt="Surfista quebra o pescoÃ§o em 4 partes, tem traumatismo em onda de 7 m e sobrevive (reproduÃ§Ã£o)" title="Surfista quebra o pescoÃ§o em 4 partes, tem traumatismo em onda de 7 m e sobrevive (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/JpMbq7LfO0McuUuK4V7ssvkl_vk=/153x131:443x325/120x80/s.glbimg.com/en/ho/f/original/2015/09/09/shawn-dollar.jpg" data-url-smart_horizontal="eECFKr0-N472rsRmzqf8h4qBwVU=/90x0/smart/filters:strip_icc()/" data-url-smart="eECFKr0-N472rsRmzqf8h4qBwVU=/90x0/smart/filters:strip_icc()/" data-url-feature="eECFKr0-N472rsRmzqf8h4qBwVU=/90x0/smart/filters:strip_icc()/" data-url-tablet="opjsclvhfgzBJ4e6q8T2072vg7I=/70x50/smart/filters:strip_icc()/" data-url-desktop="DWO6-uy-87vovXVrsYGwZDkkKTo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Surfista quebra o pescoÃ§o em 4 partes, tem traumatismo em onda de 7 m e sobrevive</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/09/bella-falconi-arrasa-de-biquini-poucos-dias-apos-dar-luz.html" class="foto" title="Bella Falconi posa sÃ³ de biquÃ­ni e exibe o corpaÃ§o 19 dias apÃ³s dar Ã  luz: &#39;Amando&#39; (ReproduÃ§Ã£o/ Instagram)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/2C3s7g6f-Z2-WOatBetRdHx_SJs=/filters:quality(10):strip_icc()/s2.glbimg.com/SIkXlVf7hzIPpbZJRq4Uid4frWg=/33x31:1051x577/335x180/s.glbimg.com/jo/eg/f/original/2015/09/09/bella_1.jpg" alt="Bella Falconi posa sÃ³ de biquÃ­ni e exibe o corpaÃ§o 19 dias apÃ³s dar Ã  luz: &#39;Amando&#39; (ReproduÃ§Ã£o/ Instagram)" title="Bella Falconi posa sÃ³ de biquÃ­ni e exibe o corpaÃ§o 19 dias apÃ³s dar Ã  luz: &#39;Amando&#39; (ReproduÃ§Ã£o/ Instagram)"
                data-original-image="s2.glbimg.com/SIkXlVf7hzIPpbZJRq4Uid4frWg=/33x31:1051x577/335x180/s.glbimg.com/jo/eg/f/original/2015/09/09/bella_1.jpg" data-url-smart_horizontal="G4HxbhGzrJ0ddNn7-wR_mrdHfi4=/90x0/smart/filters:strip_icc()/" data-url-smart="G4HxbhGzrJ0ddNn7-wR_mrdHfi4=/90x0/smart/filters:strip_icc()/" data-url-feature="G4HxbhGzrJ0ddNn7-wR_mrdHfi4=/90x0/smart/filters:strip_icc()/" data-url-tablet="jOkY1zWlyoLb7K11yY6eOx8g2pg=/220x125/smart/filters:strip_icc()/" data-url-desktop="MV6_JrLnrwwnBs1Q8hcTMuNyKs8=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Bella Falconi posa sÃ³ de biquÃ­ni e exibe o corpaÃ§o 19 dias apÃ³s dar Ã  luz: &#39;Amando&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:51:44" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/09/kelly-key-mostra-corpo-sarado-em-ensaio-fotografico.html" class="foto" title="Kelly Key posta fotos de ensaio com luvas de boxe e mostra a sua boa forma (Instagram / ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/JClOA9kZHIvREdUkOfL1cpP9YGE=/filters:quality(10):strip_icc()/s2.glbimg.com/4--5GP0e4ncupTCBFkoJ3oXlsbc=/0x0:640x426/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/11910126_940627562665051_2001573353_n_1.jpg" alt="Kelly Key posta fotos de ensaio com luvas de boxe e mostra a sua boa forma (Instagram / ReproduÃ§Ã£o)" title="Kelly Key posta fotos de ensaio com luvas de boxe e mostra a sua boa forma (Instagram / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/4--5GP0e4ncupTCBFkoJ3oXlsbc=/0x0:640x426/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/11910126_940627562665051_2001573353_n_1.jpg" data-url-smart_horizontal="sZ4FDAMUx--FW5xgGdvbfVZqkUA=/90x0/smart/filters:strip_icc()/" data-url-smart="sZ4FDAMUx--FW5xgGdvbfVZqkUA=/90x0/smart/filters:strip_icc()/" data-url-feature="sZ4FDAMUx--FW5xgGdvbfVZqkUA=/90x0/smart/filters:strip_icc()/" data-url-tablet="eRYo-YWfquoH-_EdHCqsLo7-MiI=/70x50/smart/filters:strip_icc()/" data-url-desktop="ydyKby4Dcks1Lh0PLZ9kSf4EPHY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Kelly Key posta fotos de ensaio com luvas de boxe e mostra a sua boa forma</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:22:34" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/09/kelly-gisch-mulher-de-cesar-cielo-mostra-barrigao-de-nove-meses.html" class="foto" title="SÃ³ de lingerie, mulher de Cesar Cielo exibe barrigÃ£o de 9 meses: &#39;Muito especial&#39; (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/t0qBbDhMhDT3d09F8IwI1vZVboE=/filters:quality(10):strip_icc()/s2.glbimg.com/_FulgZOaEF0EiKGpw9AmJ_IxCNo=/0x65:556x436/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/kelly.png" alt="SÃ³ de lingerie, mulher de Cesar Cielo exibe barrigÃ£o de 9 meses: &#39;Muito especial&#39; (ReproduÃ§Ã£o/Instagram)" title="SÃ³ de lingerie, mulher de Cesar Cielo exibe barrigÃ£o de 9 meses: &#39;Muito especial&#39; (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/_FulgZOaEF0EiKGpw9AmJ_IxCNo=/0x65:556x436/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/kelly.png" data-url-smart_horizontal="HTLxo_U4-WC4y90jN8uNd_JQadQ=/90x0/smart/filters:strip_icc()/" data-url-smart="HTLxo_U4-WC4y90jN8uNd_JQadQ=/90x0/smart/filters:strip_icc()/" data-url-feature="HTLxo_U4-WC4y90jN8uNd_JQadQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="-mG5bb9Brqy6oRrdsO09Z-MOEJk=/70x50/smart/filters:strip_icc()/" data-url-desktop="oJj3dRmAmw8aVZfAzm6EedX4GYE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>SÃ³ de lingerie, mulher de Cesar Cielo exibe barrigÃ£o<br /> de 9 meses: &#39;Muito especial&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:46:34" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/biquini/noticia/2015/09/natalia-casassola-posta-foto-de-biquini-e-rapaz-critica-quanta-estria.html" class="" title="Ex-BBB Natalia reage a crÃ­tica por estria em foto: &#39;NÃ£o estÃ£o acostumados com o natural&#39; (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class="conteudo"><p>Ex-BBB Natalia reage a crÃ­tica por estria em foto: &#39;NÃ£o estÃ£o acostumados com o natural&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 20:57:40" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/09/kadu-moliterno-e-cristianne-rodriguez-fazem-abdominal-sexy.html" class="foto" title="Namorada de Kadu Moliterno faz abdominal pendurada no ator; clique e veja as fotos (Instagram / ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/EoQmzT6StP8qglukXdzQPCMGyHE=/filters:quality(10):strip_icc()/s2.glbimg.com/5e5IVP9mnZQTGZvdo6XPsirfMb0=/29x78:528x411/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/02_3.jpg" alt="Namorada de Kadu Moliterno faz abdominal pendurada no ator; clique e veja as fotos (Instagram / ReproduÃ§Ã£o)" title="Namorada de Kadu Moliterno faz abdominal pendurada no ator; clique e veja as fotos (Instagram / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/5e5IVP9mnZQTGZvdo6XPsirfMb0=/29x78:528x411/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/02_3.jpg" data-url-smart_horizontal="s1r0fUOpNtcC0MPebktY0L3c4hY=/90x0/smart/filters:strip_icc()/" data-url-smart="s1r0fUOpNtcC0MPebktY0L3c4hY=/90x0/smart/filters:strip_icc()/" data-url-feature="s1r0fUOpNtcC0MPebktY0L3c4hY=/90x0/smart/filters:strip_icc()/" data-url-tablet="HAmLtBASQO6fIpSHamH1GyWMIeg=/70x50/smart/filters:strip_icc()/" data-url-desktop="ejSSgkDQ8HXcsSi3pBw2-96E2sU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Namorada de Kadu Moliterno faz abdominal pendurada no ator; clique e veja as fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-09 18:20:06" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/09/ticiane-pinheiro-vai-com-rafa-justus-ao-aniversario-da-ex-enteada-em-sp.html" class="foto" title="Ticiane leva Rafinha Ã  festa da irmÃ£; Ana Paula Siebert e Carol Celico tambÃ©m foram (Manuela Scarpa / PhotorioNews)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/5-aXcOS4uh06co2GTStrheKXNHU=/filters:quality(10):strip_icc()/s2.glbimg.com/SEPmEK1v4iHYKeSmQSivH0XM2-k=/367x41:722x278/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/ticiane_pinheiro_com_a_filha_rafaella_justus-6573.jpg" alt="Ticiane leva Rafinha Ã  festa da irmÃ£; Ana Paula Siebert e Carol Celico tambÃ©m foram (Manuela Scarpa / PhotorioNews)" title="Ticiane leva Rafinha Ã  festa da irmÃ£; Ana Paula Siebert e Carol Celico tambÃ©m foram (Manuela Scarpa / PhotorioNews)"
                data-original-image="s2.glbimg.com/SEPmEK1v4iHYKeSmQSivH0XM2-k=/367x41:722x278/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/ticiane_pinheiro_com_a_filha_rafaella_justus-6573.jpg" data-url-smart_horizontal="v21DmecgVm5UoNfal85hy8xc1AA=/90x0/smart/filters:strip_icc()/" data-url-smart="v21DmecgVm5UoNfal85hy8xc1AA=/90x0/smart/filters:strip_icc()/" data-url-feature="v21DmecgVm5UoNfal85hy8xc1AA=/90x0/smart/filters:strip_icc()/" data-url-tablet="9qomogKEO0dP4SfVlDrlC761CeQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="-VnQZfbuTjwgH5_90NT8pQ0vbuI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ticiane leva Rafinha Ã  festa da irmÃ£; Ana Paula Siebert e Carol Celico tambÃ©m foram</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/09/barbara-evans-ostenta-com-carrao-em-miami.html" class="" title="BÃ¡rbara Evans curte muito a vida em Miami de carrÃ£o: &#39;Pena que estÃ¡ acabando&#39;; veja a imagem (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class="conteudo"><p>BÃ¡rbara Evans curte muito a vida em Miami de carrÃ£o: &#39;Pena que estÃ¡ acabando&#39;; veja a imagem</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/beleza/noticia/2015/09/jaque-khury-mostra-bumbum-perfeito-em-foto-ao-lado-do-filho.html" class="foto" title="Bumbum perfeito de Jaque Khury rouba a cena em foto com filho em rede social (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/FgZalje-WcD4WgJgqVyz5A951u4=/filters:quality(10):strip_icc()/s2.glbimg.com/_Lq4XVuai-pBndQcjqxASoDOoLQ=/0x58:640x485/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/jaque_com_o_filho.jpg" alt="Bumbum perfeito de Jaque Khury rouba a cena em foto com filho em rede social (ReproduÃ§Ã£o/Instagram)" title="Bumbum perfeito de Jaque Khury rouba a cena em foto com filho em rede social (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/_Lq4XVuai-pBndQcjqxASoDOoLQ=/0x58:640x485/120x80/s.glbimg.com/jo/eg/f/original/2015/09/09/jaque_com_o_filho.jpg" data-url-smart_horizontal="uOVQI1Lvq8p2qXpqI0OxgC7sWN8=/90x0/smart/filters:strip_icc()/" data-url-smart="uOVQI1Lvq8p2qXpqI0OxgC7sWN8=/90x0/smart/filters:strip_icc()/" data-url-feature="uOVQI1Lvq8p2qXpqI0OxgC7sWN8=/90x0/smart/filters:strip_icc()/" data-url-tablet="Xnw6kvFuIbySph3Qz26kmJ4uVXY=/70x50/smart/filters:strip_icc()/" data-url-desktop="j5u14Bxzv4xFpShyrmF4GyF8VXY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Bumbum perfeito de Jaque Khury rouba a cena em foto com filho em rede social</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry publieditorial"><div data-photo-subtitle="2015-09-09 13:27:13" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/estilo/beleza/videos/?utm_source=home-globocom&amp;utm_medium=fake-banner&amp;utm_terem=15-09-09&amp;utm_content=gshow-beleza&amp;utm_campaign=avon" class="foto" title="Confira o passo a passo dos makes que estÃ£o bombando na TV! (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/f-NpCekx1D85k5Hif0i2vZ1osxc=/filters:quality(10):strip_icc()/s2.glbimg.com/tL7thae75Dx4qQSGvq7FZHnGhIA=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/09/09/fake_banner.jpg" alt="Confira o passo a passo dos makes que estÃ£o bombando na TV! (ReproduÃ§Ã£o)" title="Confira o passo a passo dos makes que estÃ£o bombando na TV! (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/tL7thae75Dx4qQSGvq7FZHnGhIA=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/09/09/fake_banner.jpg" data-url-smart_horizontal="3wH-CaUfgzVYD2RDrW-26Yyn75w=/90x0/smart/filters:strip_icc()/" data-url-smart="3wH-CaUfgzVYD2RDrW-26Yyn75w=/90x0/smart/filters:strip_icc()/" data-url-feature="3wH-CaUfgzVYD2RDrW-26Yyn75w=/90x0/smart/filters:strip_icc()/" data-url-tablet="mfXy3eaFWcihPTlOrTYpeRUQqRY=/70x50/smart/filters:strip_icc()/" data-url-desktop="YQm7sp4xaeHu5eg3CZprsRPSrUY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><span>ESPECIAL PUBLICITÃRIO</span><p>Confira o passo a passo dos makes que estÃ£o bombando na TV!</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/09/whatsapp-web-corrige-falha-grave-que-pode-ter-afetado-200-milhoes.html" title="Falha que &#39;abria&#39; WhatsApp para ser invadido no PC Ã© solucionada"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/Z3QC3POJpykQtPRQwSgceTabqMo=/filters:quality(10):strip_icc()/s2.glbimg.com/IqUswL0pv9Dqm-zOgMBGNkm4fOI=/0x21:695x390/245x130/s.glbimg.com/po/tt2/f/original/2015/04/24/whatsappweb6.jpg" alt="Falha que &#39;abria&#39; WhatsApp para ser invadido no PC Ã© solucionada (TechTudo/Lucas Mendes)" title="Falha que &#39;abria&#39; WhatsApp para ser invadido no PC Ã© solucionada (TechTudo/Lucas Mendes)"
             data-original-image="s2.glbimg.com/IqUswL0pv9Dqm-zOgMBGNkm4fOI=/0x21:695x390/245x130/s.glbimg.com/po/tt2/f/original/2015/04/24/whatsappweb6.jpg" data-url-smart_horizontal="bBsLh6ZwNbBWDd41KI-2eX1cLuQ=/90x56/smart/filters:strip_icc()/" data-url-smart="bBsLh6ZwNbBWDd41KI-2eX1cLuQ=/90x56/smart/filters:strip_icc()/" data-url-feature="bBsLh6ZwNbBWDd41KI-2eX1cLuQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="AWAdNZidE92Jb6GvTInnqYB7Z5Y=/160x96/smart/filters:strip_icc()/" data-url-desktop="-FB1-TuNTxAnWMWaT14t9SW2Wcg=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Falha que &#39;abria&#39; WhatsApp para ser invadido no PC Ã© solucionada</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/09/fifa-16-revela-lista-com-20-melhores-jogadoras-e-inclui-brasileiras.html" title="Fifa 16 revela lista com melhores jogadoras e inclui duas brasileiras"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/MczyAwFWV20JGMhKCLimKCuFblM=/filters:quality(10):strip_icc()/s2.glbimg.com/TVkr-ThGgOutvRbll3eUhlWvZy8=/0x0:1500x795/245x130/s.glbimg.com/po/tt2/f/original/2015/09/09/ctzxbigfphileslovkwy.jpg" alt="Fifa 16 revela lista com melhores jogadoras e inclui duas brasileiras (DivulgaÃ§Ã£o)" title="Fifa 16 revela lista com melhores jogadoras e inclui duas brasileiras (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/TVkr-ThGgOutvRbll3eUhlWvZy8=/0x0:1500x795/245x130/s.glbimg.com/po/tt2/f/original/2015/09/09/ctzxbigfphileslovkwy.jpg" data-url-smart_horizontal="AnsCtD2RxUwrk09xItZAcaze4BA=/90x56/smart/filters:strip_icc()/" data-url-smart="AnsCtD2RxUwrk09xItZAcaze4BA=/90x56/smart/filters:strip_icc()/" data-url-feature="AnsCtD2RxUwrk09xItZAcaze4BA=/90x56/smart/filters:strip_icc()/" data-url-tablet="geI9XG0jrPPiryEKzgt4EMaameo=/160x96/smart/filters:strip_icc()/" data-url-desktop="QKeydNdgL-6emcz_1F1McL1zS8Q=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Fifa 16 revela lista com melhores jogadoras e inclui duas brasileiras</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/09/gopro-lanca-programa-para-interessados-em-super-camera-que-filma-em-8k.html" title="GoPro libera modelo &#39;absurdo&#39; com 16 cÃ¢meras que grava em 8K"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/yBvBU5zX8YAyXaQbcyxcascgyMw=/filters:quality(10):strip_icc()/s2.glbimg.com/1agO0AH7Zzyf0_pBS6jAY0uFf2Y=/0x36:600x355/245x130/s.glbimg.com/po/tt2/f/original/2015/09/09/08122007894095.jpg" alt="GoPro libera modelo &#39;absurdo&#39; com 16 cÃ¢meras que grava em 8K (DivulgaÃ§Ã£o)" title="GoPro libera modelo &#39;absurdo&#39; com 16 cÃ¢meras que grava em 8K (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/1agO0AH7Zzyf0_pBS6jAY0uFf2Y=/0x36:600x355/245x130/s.glbimg.com/po/tt2/f/original/2015/09/09/08122007894095.jpg" data-url-smart_horizontal="Mnh0FracArPWv0xGtHdTT9YTAHU=/90x56/smart/filters:strip_icc()/" data-url-smart="Mnh0FracArPWv0xGtHdTT9YTAHU=/90x56/smart/filters:strip_icc()/" data-url-feature="Mnh0FracArPWv0xGtHdTT9YTAHU=/90x56/smart/filters:strip_icc()/" data-url-tablet="zx2par3afXN-NQ8fruvBpMcKo9M=/160x96/smart/filters:strip_icc()/" data-url-desktop="z-h5eN-8S5EnKX3caL3R_xmYU2w=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>GoPro libera modelo &#39;absurdo&#39; com 16 cÃ¢meras que grava em 8K</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/09/pes-2016-tera-maracana-e-edicao-especial-com-capa-do-flamengo.html" title="PES 2016 terÃ¡ MaracanÃ£ e uma ediÃ§Ã£o com Flamengo na capa"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/UgSvtlTJLYQfp1ZvWxZLwzZ4evY=/filters:quality(10):strip_icc()/s2.glbimg.com/L0BNUbqFt6esTml8LdeZZgLKodg=/0x0:1920x1018/245x130/s.glbimg.com/es/ge/f/original/2015/09/09/pes2016-flamengo-02.jpg" alt="PES 2016 terÃ¡ MaracanÃ£ e uma ediÃ§Ã£o com Flamengo na capa (DivulgaÃ§Ã£o)" title="PES 2016 terÃ¡ MaracanÃ£ e uma ediÃ§Ã£o com Flamengo na capa (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/L0BNUbqFt6esTml8LdeZZgLKodg=/0x0:1920x1018/245x130/s.glbimg.com/es/ge/f/original/2015/09/09/pes2016-flamengo-02.jpg" data-url-smart_horizontal="z3_RgIUOwFios6Q4QykXT0-JYuU=/90x56/smart/filters:strip_icc()/" data-url-smart="z3_RgIUOwFios6Q4QykXT0-JYuU=/90x56/smart/filters:strip_icc()/" data-url-feature="z3_RgIUOwFios6Q4QykXT0-JYuU=/90x56/smart/filters:strip_icc()/" data-url-tablet="d2-zq6DXiXLaOPZNu2ZB520ldGI=/160x96/smart/filters:strip_icc()/" data-url-desktop="mpSN3lygLwl_XeG5UyAUVw4gC9U=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>PES 2016 terÃ¡ MaracanÃ£ e uma ediÃ§Ã£o com Flamengo na capa</p></span></a></li></ul><div class="destaque-inferior analytics-area analytics-id-B"><ul><li class="techtudo"><a href="http://www.techtudo.com.br/" title="Teste agora a velocidade da sua internet"><span class="logo"></span><span class="title">Teste agora a velocidade da sua internet &rsaquo;</span></a></li><li class="exty"><a href="http://www.techtudo.com.br/tudo-sobre/extensao-globo-com.html" title="Adicione e veja as notÃ­cias em tempo real"><span class="logo"></span><span class="title">Adicione e veja as notÃ­cias em tempo real &rsaquo;</span></a></li></ul></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Beleza/noticia/2015/09/9-erros-que-voce-comete-na-hora-de-pintar-o-cabelo.html" alt="ConheÃ§a 9 erros comuns que vocÃª pode cometer ao pintar o cabelo"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/RF9j12sB-1r0Kpmhc4i4GaUH7k4=/filters:quality(10):strip_icc()/s2.glbimg.com/FlhjYrqPWL00HBoSA9jopOsO0v4=/0x0:620x399/155x100/e.glbimg.com/og/ed/f/original/2015/09/08/cabelo-loiro.jpg" alt="ConheÃ§a 9 erros comuns que vocÃª pode cometer ao pintar o cabelo (Thinkstock)" title="ConheÃ§a 9 erros comuns que vocÃª pode cometer ao pintar o cabelo (Thinkstock)"
            data-original-image="s2.glbimg.com/FlhjYrqPWL00HBoSA9jopOsO0v4=/0x0:620x399/155x100/e.glbimg.com/og/ed/f/original/2015/09/08/cabelo-loiro.jpg" data-url-smart_horizontal="0KLdqNJSefZsF3vZTbRZmfbm5h4=/90x56/smart/filters:strip_icc()/" data-url-smart="0KLdqNJSefZsF3vZTbRZmfbm5h4=/90x56/smart/filters:strip_icc()/" data-url-feature="0KLdqNJSefZsF3vZTbRZmfbm5h4=/90x56/smart/filters:strip_icc()/" data-url-tablet="9U7OyduBJ0DoSMsvRHbwfb126S4=/122x75/smart/filters:strip_icc()/" data-url-desktop="lAzzLEMUBf1l3r0t9oM-Dd0qYds=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>em casa ou no salÃ£o</h3><p>ConheÃ§a 9 erros comuns que vocÃª pode cometer ao pintar o cabelo</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/ela/reese-witherspoon-eleita-pela-revista-people-mais-bem-vestida-de-2015-17437925" alt="Mais bem vestida de 2015, Reese conta o que jamais entraria no seu closet"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/5rfE6txEZ57W6wWNthCOE8xe500=/filters:quality(10):strip_icc()/s2.glbimg.com/s0UZrCXM9o5sXhHAd2aDhYBopsg=/33x62:353x269/155x100/s.glbimg.com/en/ho/f/original/2015/09/09/reese.jpg" alt="Mais bem vestida de 2015, Reese conta o que jamais entraria no seu closet (AP)" title="Mais bem vestida de 2015, Reese conta o que jamais entraria no seu closet (AP)"
            data-original-image="s2.glbimg.com/s0UZrCXM9o5sXhHAd2aDhYBopsg=/33x62:353x269/155x100/s.glbimg.com/en/ho/f/original/2015/09/09/reese.jpg" data-url-smart_horizontal="GDgFlDt_dztA_WiMFVQx9DrS0g8=/90x56/smart/filters:strip_icc()/" data-url-smart="GDgFlDt_dztA_WiMFVQx9DrS0g8=/90x56/smart/filters:strip_icc()/" data-url-feature="GDgFlDt_dztA_WiMFVQx9DrS0g8=/90x56/smart/filters:strip_icc()/" data-url-tablet="9NvNAXgGgcFR_S-_M9PkqS0RSTA=/122x75/smart/filters:strip_icc()/" data-url-desktop="H4OBShp-dFDiLj5sMsGwL-8nbY8=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>top cropped, nÃ£o!</h3><p>Mais bem vestida de 2015, Reese conta o que jamais entraria no seu closet</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Beleza/Fitness-e-dieta/noticia/2015/09/alimentos-praticos-que-voce-pre-ci-sa-ter-mao-se-mora-sozinha.html" alt="Lista reÃºne alimentos prÃ¡ticos para ter sempre em casa e nÃ£o sair da linha"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/A6nx0lm5g5hXuTzL0X1yXWFs49s=/filters:quality(10):strip_icc()/s2.glbimg.com/6ODfoIuWAxvTo9x1jSKktXeCohU=/132x3:555x276/155x100/e.glbimg.com/og/ed/f/original/2015/09/08/gl069368.jpg" alt="Lista reÃºne alimentos prÃ¡ticos para ter sempre em casa e nÃ£o sair da linha (Henrique Gendre/Glamour)" title="Lista reÃºne alimentos prÃ¡ticos para ter sempre em casa e nÃ£o sair da linha (Henrique Gendre/Glamour)"
            data-original-image="s2.glbimg.com/6ODfoIuWAxvTo9x1jSKktXeCohU=/132x3:555x276/155x100/e.glbimg.com/og/ed/f/original/2015/09/08/gl069368.jpg" data-url-smart_horizontal="UTNNYmUxPOFwaC7DA1AU_2gxICk=/90x56/smart/filters:strip_icc()/" data-url-smart="UTNNYmUxPOFwaC7DA1AU_2gxICk=/90x56/smart/filters:strip_icc()/" data-url-feature="UTNNYmUxPOFwaC7DA1AU_2gxICk=/90x56/smart/filters:strip_icc()/" data-url-tablet="KyQpdMIy4bqibXnEaWfRyhBH_3I=/122x75/smart/filters:strip_icc()/" data-url-desktop="p5tNYK94o2H-z57tIz38pk5-QR0=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>para facilitar a vida</h3><p>Lista reÃºne alimentos prÃ¡ticos para ter sempre em casa e nÃ£o sair da linha</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/apartamentos/noticia/2015/09/classicos-com-uma-boa-dose-de-arte-e-cor.html" alt="Reforma dÃ¡ vida nova a apÃª antigo ao usar cores vibrantes em alguns pontos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/BIbM0IyIiGb7cLUOaaDTrp4zwY8=/filters:quality(10):strip_icc()/s2.glbimg.com/Y9WqqgZPLZzOMJ5S6GRQsX9DLvo=/0x352:620x751/155x100/e.glbimg.com/og/ed/f/original/2015/09/08/apartamentio-antonio-ferreira_junior-05_1.jpg" alt="Reforma dÃ¡ vida nova a apÃª antigo ao usar cores vibrantes em alguns pontos (Ricardo Labougle )" title="Reforma dÃ¡ vida nova a apÃª antigo ao usar cores vibrantes em alguns pontos (Ricardo Labougle )"
            data-original-image="s2.glbimg.com/Y9WqqgZPLZzOMJ5S6GRQsX9DLvo=/0x352:620x751/155x100/e.glbimg.com/og/ed/f/original/2015/09/08/apartamentio-antonio-ferreira_junior-05_1.jpg" data-url-smart_horizontal="_HDWdp-Half9C9L4nJEgEEIm2vk=/90x56/smart/filters:strip_icc()/" data-url-smart="_HDWdp-Half9C9L4nJEgEEIm2vk=/90x56/smart/filters:strip_icc()/" data-url-feature="_HDWdp-Half9C9L4nJEgEEIm2vk=/90x56/smart/filters:strip_icc()/" data-url-tablet="cAHNQsr43AW08Dv1L4_bfV0WAok=/122x75/smart/filters:strip_icc()/" data-url-desktop="7LCqPkWTVP36FTVUF-LOlegj0JA=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>ar mais leve</h3><p>Reforma dÃ¡ vida nova a apÃª antigo ao usar cores vibrantes em alguns pontos</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Jardim/Dicas/Organizacao/fotos/2015/09/ideias-para-guardar-os-livros-das-criancas.html" alt="Prateleiras, estantes e atÃ© cestos sÃ£o soluÃ§Ãµes para organizar livros infantis"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/OSe3Mi_Up9mrcboc-351IXN9wMw=/filters:quality(10):strip_icc()/s2.glbimg.com/Jy2pQuCBKrAkzzyGfsFRAgoVWpM=/0x232:620x633/155x100/e.glbimg.com/og/ed/f/original/2015/09/08/cj668_qcrianca_119.jpg" alt="Prateleiras, estantes e atÃ© cestos sÃ£o soluÃ§Ãµes para organizar livros infantis (Editora Globo)" title="Prateleiras, estantes e atÃ© cestos sÃ£o soluÃ§Ãµes para organizar livros infantis (Editora Globo)"
            data-original-image="s2.glbimg.com/Jy2pQuCBKrAkzzyGfsFRAgoVWpM=/0x232:620x633/155x100/e.glbimg.com/og/ed/f/original/2015/09/08/cj668_qcrianca_119.jpg" data-url-smart_horizontal="Q-zpaPGhq6KRM2FCd2FDLdLghX0=/90x56/smart/filters:strip_icc()/" data-url-smart="Q-zpaPGhq6KRM2FCd2FDLdLghX0=/90x56/smart/filters:strip_icc()/" data-url-feature="Q-zpaPGhq6KRM2FCd2FDLdLghX0=/90x56/smart/filters:strip_icc()/" data-url-tablet="4LM0WVpVkBHlZBaOZUqa4Gj4wdM=/122x75/smart/filters:strip_icc()/" data-url-desktop="Cea12GY9Gu6cEBpmEvXRsU4kyQs=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>leitura com as crianÃ§as</h3><p>Prateleiras, estantes e atÃ© cestos sÃ£o soluÃ§Ãµes para organizar livros infantis</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/cabeceira-de-cama-diferente-para-o-quarto/" alt="Cabeceiras criativas, como um simples desenho com giz, mudam o quarto"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/P2xtEcOAPgwNOaUge_YdE_BTZOk=/filters:quality(10):strip_icc()/s2.glbimg.com/IPNv0sAsJDVtASmXzPq8_xEX8gU=/5x0:563x360/155x100/s.glbimg.com/en/ho/f/original/2015/09/09/cabeceira-lousa.jpg" alt="Cabeceiras criativas, como um simples desenho com giz, mudam o quarto (ReproduÃ§Ã£o/Pinterest)" title="Cabeceiras criativas, como um simples desenho com giz, mudam o quarto (ReproduÃ§Ã£o/Pinterest)"
            data-original-image="s2.glbimg.com/IPNv0sAsJDVtASmXzPq8_xEX8gU=/5x0:563x360/155x100/s.glbimg.com/en/ho/f/original/2015/09/09/cabeceira-lousa.jpg" data-url-smart_horizontal="RvbX2QkWBvvH4iVV93DmLgsnvYY=/90x56/smart/filters:strip_icc()/" data-url-smart="RvbX2QkWBvvH4iVV93DmLgsnvYY=/90x56/smart/filters:strip_icc()/" data-url-feature="RvbX2QkWBvvH4iVV93DmLgsnvYY=/90x56/smart/filters:strip_icc()/" data-url-tablet="5J7fiRddm3thAZSZS1Anp5gfdNI=/122x75/smart/filters:strip_icc()/" data-url-desktop="BdBU1vYNK0Ii9lLH6OfNoEoBOEQ=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Veja ideias e inspire-se</h3><p>Cabeceiras criativas, como um simples desenho com giz, mudam o quarto</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://vogue.globo.com/beleza/gente/noticia/2015/09/isabeli-fontana-estrela-edicao-de-outubro-da-maxim.html" title="Isabeli Fontana posa seminua: &#39;NÃ£o Ã© fÃ¡cil ser meu namorado&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/3xtpB3VBrQIzvosiQbL-r2vHYt0=/filters:quality(10):strip_icc()/s2.glbimg.com/m4A8KPKd92wY2aiH3L9NifdzvCg=/0x165:558x462/245x130/e.glbimg.com/og/ed/f/original/2015/09/09/207_1015_isabeli_sl11_1.jpg" alt="Isabeli Fontana posa seminua: &#39;NÃ£o Ã© fÃ¡cil ser meu namorado&#39; (ReproduÃ§Ã£o)" title="Isabeli Fontana posa seminua: &#39;NÃ£o Ã© fÃ¡cil ser meu namorado&#39; (ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/m4A8KPKd92wY2aiH3L9NifdzvCg=/0x165:558x462/245x130/e.glbimg.com/og/ed/f/original/2015/09/09/207_1015_isabeli_sl11_1.jpg" data-url-smart_horizontal="ts2rJgu-beu_PEmH0-condYEj38=/90x56/smart/filters:strip_icc()/" data-url-smart="ts2rJgu-beu_PEmH0-condYEj38=/90x56/smart/filters:strip_icc()/" data-url-feature="ts2rJgu-beu_PEmH0-condYEj38=/90x56/smart/filters:strip_icc()/" data-url-tablet="6fvrAvKatjHboTvTRQEGyAe1myQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="ijOg8rv4HkTeG5MO0QCyohj3t3o=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Isabeli Fontana posa seminua: &#39;NÃ£o Ã© fÃ¡cil ser meu namorado&#39;</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/09/tatiele-polyana-abre-album-e-posta-foto-do-fim-de-semana-na-bahia.html" title="Tatiele Polyana posta foto na Bahia e mostra sua boa forma"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/szhtwpRSFzDdxNwl9BKvGfiTOVw=/filters:quality(10):strip_icc()/s2.glbimg.com/WK3odk2DDRYI3_SwyHbtFXkvyMw=/226x360:910x723/245x130/s.glbimg.com/jo/eg/f/original/2015/09/09/11257154_467102486827408_1132207988_n.jpg" alt="Tatiele Polyana posta foto na Bahia e mostra sua boa forma (Instagram / ReproduÃ§Ã£o)" title="Tatiele Polyana posta foto na Bahia e mostra sua boa forma (Instagram / ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/WK3odk2DDRYI3_SwyHbtFXkvyMw=/226x360:910x723/245x130/s.glbimg.com/jo/eg/f/original/2015/09/09/11257154_467102486827408_1132207988_n.jpg" data-url-smart_horizontal="XaKuo9XYn5OwfOiB7EUgoSBojuk=/90x56/smart/filters:strip_icc()/" data-url-smart="XaKuo9XYn5OwfOiB7EUgoSBojuk=/90x56/smart/filters:strip_icc()/" data-url-feature="XaKuo9XYn5OwfOiB7EUgoSBojuk=/90x56/smart/filters:strip_icc()/" data-url-tablet="GazOtWutLqJ-ts_hoyrbVElcJlg=/160x95/smart/filters:strip_icc()/" data-url-desktop="fRvKmJIFe6XVWkMnKylnfhdomLY=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Tatiele Polyana posta foto na Bahia e mostra sua boa forma</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistacrescer.globo.com/Pais-famosos/noticia/2015/09/kelly-key-vai-ser-veterinaria-daqui-alguns-anos-serei-uma.html" title="Estudante de veterinÃ¡ria, Kelly Key comemora o dia: &#39;Serei uma&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/49lr8rpI_w63fzM3G03mvktXuUA=/filters:quality(10):strip_icc()/s2.glbimg.com/JZSHYVtpzdjwJ6XTNf7fnA2PdLA=/0x0:620x328/245x130/e.glbimg.com/og/ed/f/original/2015/09/09/kelly-key-veterinaria.png" alt="Estudante de veterinÃ¡ria, Kelly Key comemora o dia: &#39;Serei uma&#39; (ReproduÃ§Ã£o/ Instagram)" title="Estudante de veterinÃ¡ria, Kelly Key comemora o dia: &#39;Serei uma&#39; (ReproduÃ§Ã£o/ Instagram)"
                    data-original-image="s2.glbimg.com/JZSHYVtpzdjwJ6XTNf7fnA2PdLA=/0x0:620x328/245x130/e.glbimg.com/og/ed/f/original/2015/09/09/kelly-key-veterinaria.png" data-url-smart_horizontal="rlKzq7hkczVLa66WWf0gV76gP6Y=/90x56/smart/filters:strip_icc()/" data-url-smart="rlKzq7hkczVLa66WWf0gV76gP6Y=/90x56/smart/filters:strip_icc()/" data-url-feature="rlKzq7hkczVLa66WWf0gV76gP6Y=/90x56/smart/filters:strip_icc()/" data-url-tablet="zihkmCb6vHoxmRhL_Iqf6byYUM8=/160x95/smart/filters:strip_icc()/" data-url-desktop="cQ_h_kxM0YvPuyq3pGjxhBBuIo8=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Estudante de veterinÃ¡ria, Kelly Key comemora o dia: &#39;Serei uma&#39;</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/QUEM-News/noticia/2015/09/carol-castro-relembra-viagem-noronha-com-foto-de-mergulho.html" title="Carol Castro posta foto de dia de mergulho em Noronha; amplie"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/IZHDRSPFLXqMm7FyfaU4p8k16Ks=/filters:quality(10):strip_icc()/s2.glbimg.com/1X9f_4dshDdGFvedzX5pcSKRTWY=/72x137:554x393/245x130/e.glbimg.com/og/ed/f/original/2015/09/09/11910123_1625127337775999_87194576_n.jpg" alt="Carol Castro posta foto de dia de mergulho em Noronha; amplie (ReproduÃ§Ã£o/Instagram)" title="Carol Castro posta foto de dia de mergulho em Noronha; amplie (ReproduÃ§Ã£o/Instagram)"
                    data-original-image="s2.glbimg.com/1X9f_4dshDdGFvedzX5pcSKRTWY=/72x137:554x393/245x130/e.glbimg.com/og/ed/f/original/2015/09/09/11910123_1625127337775999_87194576_n.jpg" data-url-smart_horizontal="NN3RI2CBfqSPvLj4L3uQV_xHRWw=/90x56/smart/filters:strip_icc()/" data-url-smart="NN3RI2CBfqSPvLj4L3uQV_xHRWw=/90x56/smart/filters:strip_icc()/" data-url-feature="NN3RI2CBfqSPvLj4L3uQV_xHRWw=/90x56/smart/filters:strip_icc()/" data-url-tablet="wjH2ZqACtnWoVV7wlZME2d3yPQw=/160x95/smart/filters:strip_icc()/" data-url-desktop="Y5NlvL6TvDn2qC81f99u43Iy2kM=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Carol Castro posta foto de dia de mergulho em Noronha; amplie</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/09/mari-devolve-presente-dom-peppino-e-o-deixa-tenso.html" title="&#39;I Love&#39;: Mari devolve &#39;presente&#39; a Dom Peppino e o deixa tenso "><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/YGVVxEpvBWcurWiNn5BIjUeFvOg=/filters:quality(10):strip_icc()/s2.glbimg.com/eEEFToVe3ARHuo2fFvnXXN2PqWA=/0x61:690x427/245x130/s.glbimg.com/et/gs/f/original/2015/09/09/mari-com-surpresa.jpg" alt="&#39;I Love&#39;: Mari devolve &#39;presente&#39; a Dom Peppino e o deixa tenso  (TV Globo)" title="&#39;I Love&#39;: Mari devolve &#39;presente&#39; a Dom Peppino e o deixa tenso  (TV Globo)"
                    data-original-image="s2.glbimg.com/eEEFToVe3ARHuo2fFvnXXN2PqWA=/0x61:690x427/245x130/s.glbimg.com/et/gs/f/original/2015/09/09/mari-com-surpresa.jpg" data-url-smart_horizontal="B0UTPpOOOXw6nMbdqdvZhc2cgHA=/90x56/smart/filters:strip_icc()/" data-url-smart="B0UTPpOOOXw6nMbdqdvZhc2cgHA=/90x56/smart/filters:strip_icc()/" data-url-feature="B0UTPpOOOXw6nMbdqdvZhc2cgHA=/90x56/smart/filters:strip_icc()/" data-url-tablet="mfl9cn6Cl8R-5rvzQbz30A6aiK8=/160x95/smart/filters:strip_icc()/" data-url-desktop="A8tXiFfWSxT0jr8L5RRZbDgEV8k=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;I Love&#39;: Mari devolve &#39;presente&#39; a Dom Peppino e o deixa tenso </p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/09/felipe-descobre-que-nunca-foi-traido.html" title="Em &#39;AlÃ©m&#39;, Felipe descobre que na verdade nunca foi traÃ­do"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/MP4aS78TfrM8rBvlVG_oD-kRETE=/filters:quality(10):strip_icc()/s2.glbimg.com/LxCMzrlnDMUSnLoPHorF_KTPGNw=/0x104:690x470/245x130/s.glbimg.com/et/gs/f/original/2015/09/08/rafael-cardoso_felipe.jpg" alt="Em &#39;AlÃ©m&#39;, Felipe descobre que na verdade nunca foi traÃ­do (TV Globo)" title="Em &#39;AlÃ©m&#39;, Felipe descobre que na verdade nunca foi traÃ­do (TV Globo)"
                    data-original-image="s2.glbimg.com/LxCMzrlnDMUSnLoPHorF_KTPGNw=/0x104:690x470/245x130/s.glbimg.com/et/gs/f/original/2015/09/08/rafael-cardoso_felipe.jpg" data-url-smart_horizontal="1iSD_jjg5-p3CKDLcJFlYZ89cuk=/90x56/smart/filters:strip_icc()/" data-url-smart="1iSD_jjg5-p3CKDLcJFlYZ89cuk=/90x56/smart/filters:strip_icc()/" data-url-feature="1iSD_jjg5-p3CKDLcJFlYZ89cuk=/90x56/smart/filters:strip_icc()/" data-url-tablet="7cturXGa5zMxbB2Ti5hDA4Z0Gyo=/160x95/smart/filters:strip_icc()/" data-url-desktop="sjXVFITQAEHkX653gxuA23k45FY=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;AlÃ©m&#39;, Felipe descobre que na verdade nunca foi traÃ­do</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/09/rodrigo-descobre-que-cica-pode-ficar-paraplegica.html" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo descobre que CiÃ§a pode ficar paraplÃ©gica "><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/s7KoGs0VfGetPsy_ZvCWfSOMBqE=/filters:quality(10):strip_icc()/s2.glbimg.com/vfK-7rSrGtVGu74Gz-6yWOcwryI=/0x122:690x488/245x130/s.glbimg.com/et/gs/f/original/2015/09/03/rodrigo_1.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Rodrigo descobre que CiÃ§a pode ficar paraplÃ©gica  (ThaÃ­s Dias/Gshow)" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo descobre que CiÃ§a pode ficar paraplÃ©gica  (ThaÃ­s Dias/Gshow)"
                    data-original-image="s2.glbimg.com/vfK-7rSrGtVGu74Gz-6yWOcwryI=/0x122:690x488/245x130/s.glbimg.com/et/gs/f/original/2015/09/03/rodrigo_1.jpg" data-url-smart_horizontal="-iVzfiRnyLdh4Onet-5ULYNGVL0=/90x56/smart/filters:strip_icc()/" data-url-smart="-iVzfiRnyLdh4Onet-5ULYNGVL0=/90x56/smart/filters:strip_icc()/" data-url-feature="-iVzfiRnyLdh4Onet-5ULYNGVL0=/90x56/smart/filters:strip_icc()/" data-url-tablet="Po_PVIdLBFHmSZwHvT6LKV_v0XI=/160x95/smart/filters:strip_icc()/" data-url-desktop="KslBOsxIYbzirTmuCNOVkrRZ9X0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Rodrigo descobre que CiÃ§a pode ficar paraplÃ©gica </p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/09/viviane-araujo-da-confere-no-tanquinho-em-ensaio-do-danca.html" title="Viviane AraÃºjo confere tanquinho em ensaio do &#39;DanÃ§a&#39;; fotos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/f-gjT8xBUZCga3pX3hid5ixSF1s=/filters:quality(10):strip_icc()/s2.glbimg.com/qVkdmjfaASSSIdLmyZhF-pYTpf8=/69x89:566x353/245x130/s.glbimg.com/et/gs/f/original/2015/09/09/viviane-araujo-tanquinho2.jpg" alt="Viviane AraÃºjo confere tanquinho em ensaio do &#39;DanÃ§a&#39;; fotos (TV Globo)" title="Viviane AraÃºjo confere tanquinho em ensaio do &#39;DanÃ§a&#39;; fotos (TV Globo)"
                    data-original-image="s2.glbimg.com/qVkdmjfaASSSIdLmyZhF-pYTpf8=/69x89:566x353/245x130/s.glbimg.com/et/gs/f/original/2015/09/09/viviane-araujo-tanquinho2.jpg" data-url-smart_horizontal="HSMtwtdkqaPySsAkUcc-qNV5tCQ=/90x56/smart/filters:strip_icc()/" data-url-smart="HSMtwtdkqaPySsAkUcc-qNV5tCQ=/90x56/smart/filters:strip_icc()/" data-url-feature="HSMtwtdkqaPySsAkUcc-qNV5tCQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="KDbxwwtIJruezR09wTCSPGMAbrk=/160x95/smart/filters:strip_icc()/" data-url-desktop="zJVRZagMDxYSBR_rA1aGUhnqyE0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Viviane AraÃºjo confere tanquinho em ensaio do &#39;DanÃ§a&#39;; fotos</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/musica/rock-in-rio/2015/noticia/2015/09/-ha-em-1991-e-eleito-momento-mais-marcante-de-30-anos-do-rock-rio.html" title="A-Ha para 198 mil pessoas Ã© eleito o maior momento do Rock in Rio; veja a lista (DivulgaÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/K2SL6gz2-tzSB_8XLPGWagHPdbU=/filters:quality(10):strip_icc()/s2.glbimg.com/gxawMkxamDHnevwY68Zi25qbIuk=/46x0:500x274/215x130/s.glbimg.com/jo/g1/f/original/2015/07/06/aha_newspics_03262015a.jpg"
                data-original-image="s2.glbimg.com/gxawMkxamDHnevwY68Zi25qbIuk=/46x0:500x274/215x130/s.glbimg.com/jo/g1/f/original/2015/07/06/aha_newspics_03262015a.jpg" data-url-smart_horizontal="jBatxpkFZ6e7AkW4EsC73LbJFtA=/90x56/smart/filters:strip_icc()/" data-url-smart="jBatxpkFZ6e7AkW4EsC73LbJFtA=/90x56/smart/filters:strip_icc()/" data-url-feature="jBatxpkFZ6e7AkW4EsC73LbJFtA=/90x56/smart/filters:strip_icc()/" data-url-tablet="JS93pd7rsVRhkjDnGxe1g5M4JS8=/120x80/smart/filters:strip_icc()/" data-url-desktop="lCp7D4RXOxaSHCLC57JLiog8rYI=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">A-Ha para 198 mil pessoas Ã© eleito o maior momento do Rock in Rio; veja a lista</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://extra.globo.com/famosos/gina-lobrista-que-vende-cds-r-5-em-mercadao-de-belem-cotada-para-cantar-com-chimbinha-17436098.html" title="RevelaÃ§Ã£o do tecnobrega pode fazer parceria com Chimbinha, ex de Joelma (ReproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/tGLYwFKwo22v83dQ3kjlOjSizj8=/filters:quality(10):strip_icc()/s2.glbimg.com/3B28b2t12BrVVoOmiDdZUOe0Pvo=/0x63:448x334/215x130/s.glbimg.com/en/ho/f/original/2015/09/09/gina.jpg"
                data-original-image="s2.glbimg.com/3B28b2t12BrVVoOmiDdZUOe0Pvo=/0x63:448x334/215x130/s.glbimg.com/en/ho/f/original/2015/09/09/gina.jpg" data-url-smart_horizontal="EXudATiUHW8-NuqBILU-04yofgY=/90x56/smart/filters:strip_icc()/" data-url-smart="EXudATiUHW8-NuqBILU-04yofgY=/90x56/smart/filters:strip_icc()/" data-url-feature="EXudATiUHW8-NuqBILU-04yofgY=/90x56/smart/filters:strip_icc()/" data-url-tablet="MHJyOv0paYYIeRM68LYZoXYGw6Q=/120x80/smart/filters:strip_icc()/" data-url-desktop="vGigRG2hGOeQZkjhsjV8Q8sdhIo=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">RevelaÃ§Ã£o do tecnobrega pode fazer parceria com Chimbinha, ex de Joelma</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/09/champignon-faz-aniversario-de-morte-e-claudia-bossle-faz-homenagem.html" title="Mulher de Champignon lanÃ§a clipe da mÃºsica feita pelo casal quando se conheceram (Arquivo pessoal/DivulgaÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/EzSrfWoeH8qBQANIl5sbCEkvA4s=/filters:quality(10):strip_icc()/s2.glbimg.com/ZdiWL3FY9sXIuVKy48jH3HKRmgw=/0x75:465x356/215x130/s.glbimg.com/jo/eg/f/original/2014/09/09/bossle.jpg"
                data-original-image="s2.glbimg.com/ZdiWL3FY9sXIuVKy48jH3HKRmgw=/0x75:465x356/215x130/s.glbimg.com/jo/eg/f/original/2014/09/09/bossle.jpg" data-url-smart_horizontal="QFtcimG79X7KEXYtpvsS9kg0paI=/90x56/smart/filters:strip_icc()/" data-url-smart="QFtcimG79X7KEXYtpvsS9kg0paI=/90x56/smart/filters:strip_icc()/" data-url-feature="QFtcimG79X7KEXYtpvsS9kg0paI=/90x56/smart/filters:strip_icc()/" data-url-tablet="7R1ekUgrNgnzKMh4_tQgaW_ry0k=/120x80/smart/filters:strip_icc()/" data-url-desktop="F23P4eLSqYSO54ylli9Wdne_orY=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Mulher de Champignon lanÃ§a clipe da mÃºsica feita  pelo casal quando se conheceram</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/ceara/noticia/2015/09/mulher-traida-expoe-ex-em-outdoor-no-interior-do-ceara-agora-tenho-outro.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Mulher traÃ­da expÃµe ex com mensagem em outdoor no interior do CearÃ¡: &#39;Tenho outro&#39;</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/espirito-santo/noticia/2015/09/crianca-e-abusada-duas-vezes-ao-sair-para-comprar-pao-na-serra-es.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">CrianÃ§a Ã© abusada duas vezes ao sair <br />para comprar pÃ£o, na Serra, ES</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://oglobo.globo.com/mundo/cinegrafista-hungara-filmada-chutando-crianca-refugiada-pode-pegar-ate-sete-anos-de-prisao-17438157" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Cinegrafista hÃºngara filmada chutando crianÃ§a refugiada pode pegar atÃ© 7 anos</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/pa/para/noticia/2015/09/cantora-joelma-do-calypso-presta-depoimento-em-delegacia-no-para.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Cantora Joelma, do Calypso, presta depoimento em delegacia no ParÃ¡</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/sorocaba-jundiai/noticia/2015/09/espuma-do-rio-tiete-transborda-e-interdita-avenida-em-salto.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Espuma do rio TietÃª transborda e interdita avenida em Salto, em SÃ£o Paulo</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/noticia/2015/09/pelo-twitter-juninho-pernambucano-critica-auditores-do-stjd-torcedores.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Pelo Twitter, Juninho Pernambucano faz crÃ­tica a auditores do STJD: &#39;Torcedores&#39;</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/boxe/noticia/2015/09/perto-de-sua-ultima-luta-mayweather-garante-nao-vou-sentir-falta-de-nada.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Perto de sua Ãºltima luta, Mayweather garante: &#39;NÃ£o vou sentir falta de nada&#39;</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/blogs/especial-blog/numerologos/post/serie-58-dos-clubes-que-cairam-desde-2003-estavam-rebaixados-na-23-rodada.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">SÃ©rie A: 58% dos clubes que caÃ­ram desde 2003 estavam &quot;rebaixados&quot; na 23Âª rodada</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/09/com-numeros-notaveis-neymar-diz-selecao-nao-e-de-um-cara-so.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Com nÃºmeros notÃ¡veis, Neymar afirma: &#39;SeleÃ§Ã£o nÃ£o Ã© de um cara sÃ³&#39;</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/09/flamengo-anuncia-parceira-com-game-fantasy-e-tera-capa-exclusiva.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Flamengo anuncia parceria com PES<br />e MaracanÃ£ e terÃ¡ capa exclusiva</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/tv/noticia/2015/09/bombou-na-web-grazi-e-exaltada-em-cena-que-choca-o-publico.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Grazi Ã© aclamada na web na cena em que <br />Larissa se vendeu para conseguir droga</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/beleza/noticia/2015/09/jaque-khury-mostra-bumbum-perfeito-em-foto-ao-lado-do-filho.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Bumbum perfeito de Jaque Khury rouba a cena  em foto com filho em rede social</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/biquini/noticia/2015/09/natalia-casassola-posta-foto-de-biquini-e-rapaz-critica-quanta-estria.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Natalia Casassola posta foto de biquÃ­ni e seguidor critica: &#39;Quanta estria&#39;</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/09/wesley-safadao-comemora-27-anos-com-festao-em-fortaleza.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">FenÃ´meno do forrÃ³, Wesley SafadÃ£o comemora 27 anos com festÃ£o no CearÃ¡</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/sensual/noticia/2015/09/tati-zaqui-revela-que-transou-pela-1-vez-com-uma-mulher-foi-maravilhoso.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Tati Zaqui revela que transou pela 1Âª vez com uma mulher: &#39;Foi maravilhoso&#39;</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://tvg.globo.com/programas/pe-na-cova/index.html">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://tvg.globo.com/programas/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/b43da6b3453a.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><style> @media (max-width:959px){#banner_slb_fim>div>iframe,#banner_slb_meio>div>iframe{-ms-zoom:.72;-moz-transform:scale(.72);-moz-transform-origin:0 0;-o-transform:scale(.72);-o-transform-origin:0 0;-webkit-transform:scale(.72);-webkit-transform-origin:0 0;vertical-align:middle;display:inline-block}#banner_slb_fim>div,#banner_slb_meio>div{background-color:white;overflow:hidden}#banner_slb_fim,#banner_slb_meio{overflow:visible}}.without-opec{display:block !important}.opec-x60{height:auto}</style><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 10};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 09/09/2015 21:52:09 -->
