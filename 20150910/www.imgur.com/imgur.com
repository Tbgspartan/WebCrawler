<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>    Imgur: The most awesome images on the Internet</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="robots" content="follow,index" />
    <meta name="keywords" content="images, funny, image host, image sharing, reaction gif, viral images, current events, cute, visual storytelling, gif" />
    <meta name="description" content="Imgur is the best place to share and enjoy the most awesome images on the Internet. Every day, millions of people use Imgur to be entertained and inspired by funny, heartwarming and helpful images and stories from all around the world." />
    <meta name="copyright" content="Copyright 2015 Imgur, Inc." />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;" />
    
    <link rel="alternate" type="application/rss+xml" title="Imgur Gallery" href="http://feeds.feedburner.com/ImgurGallery?format=xml" />
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-16x16.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="//s.imgur.com/images/favicon-152.png">
    <meta name="msapplication-TileColor" content="#2cd63c">
    <meta name="msapplication-TileImage" content="//s.imgur.com/images/favicon-144.png">
            <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/global.css?1441834350" />

        
        
    
        <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/gallery.css?1441834350" />

            <!--[if IE 9]><link rel="stylesheet" href="//s.imgur.com/include/css/ie-sucks.css?0" type="text/css" /><![endif]-->
    
            <link rel="canonical" href="http://imgur.com/" />
        <meta property="og:url" content="http://imgur.com/" />
    
    <meta name="p:domain_verify" content="a1e7abe8af908cc6dfaf935dd9a20384"/>
    <meta property="og:site_name" content="Imgur" />
    <meta property="fb:admins" content="12331492,12301369" />
    <meta name="twitter:site" content="@imgur" />
    <meta name="twitter:domain" content="imgur.com" />
    <meta name="twitter:creator" content="@imgur" />
    <meta name="twitter:app:id:googleplay" content="com.imgur.mobile" />
        <meta name="twitter:card" content="summary_large_image" />
    <meta property="og:type" content="website" />
            <meta property="og:title" content="Imgur" />
        <meta name="twitter:title" content="Imgur" />
    
            <meta property="og:description" content="Imgur: The most awesome images on the Internet." />
        <meta name="twitter:description" content="Imgur: The most awesome images on the Internet." />
    
    <meta property="og:image" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image:width" content="1200" />
    <meta name="twitter:image:src" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta name="twitter:image:height" content="630" />
    <meta name="twitter:image:width" content="1200" />
    <!--[if lte IE 8]><script type="text/javascript" src="//s.imgur.com/min/iepoly.js?1441834350"></script>
<![endif]-->
    
        
</head>
<body>
            
            <noscript>
                <iframe src="//www.googletagmanager.com/ns.html?id=GTM-W9LTJC" height="0" width="0" style="display:none;visibility:hidden"></iframe>
            </noscript>
            <script type="text/javascript">
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-W9LTJC');
            </script>
        
    

                
    <div id="topbar">
    <div class="header-center">
        <div id="header-logo">

            <a href="//imgur.com" class="logo " data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@logo@@}}">
                
                <span class="logo-icon"></span>
            </a>
        </div>
        <div id="main-nav">
            <ul>
                <li class="menu-btn">
                    <div class="menu-icon">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                        
                    </div>
                    <div class="link-menu">
                        <div class="test-1868-show">
                            <ul class="topics cf">
                                <li class="item bold" data-value="hot"><a href="/hot" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@hot@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a></li>
                                <li class="item bold" data-value="new"><a href="/new" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@new@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a></li>
                                                                                                            <li class="item" data-value="Funny">
                                            <a href="/topic/Funny" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Awesome">
                                            <a href="/topic/Awesome" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Aww">
                                            <a href="/topic/Aww" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="The More You Know">
                                            <a href="/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Storytime">
                                            <a href="/topic/Storytime" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Current Events">
                                            <a href="/topic/Current_Events" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Design &amp; Art">
                                            <a href="/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Reaction">
                                            <a href="/topic/Reaction" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                                        </li>
                                    
                                                                    
                                
                            </ul>
                            <ul class="dropdown-footer cf">
                                <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">blog</a></li>
                                <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">about</a></li>
                                <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">store</a></li>
                                <li><a href="//imgur.com/jobs"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">jobs</a></li>
                                <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">help</a></li>
                                <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">apps</a></li>
                                <li><a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">terms</a></li>
                                <li><a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">privacy</a></li>
                                <li><a href="//imgur.com/removalrequest" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@deletion@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">request deletion</a></li>
                                <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">api</a></li>
                                <li><a href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">ad choices</a></li>
                            </ul>
                        </div>
                        <ul class="test-1868-hide">
                            <li><a href="//imgur.com/jobs" class="new" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@}}">we're hiring!</a></li>
                                                            <li class="title-w" title="Notify all the things!"><a href="http://imgur.com/blog/2015/08/13/an-update-to-notifications/" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@new@@:true}}">blog<span class="red tiptext">new post!</span></a></li>
                            
                            <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@}}">about imgur</a></li>
                            <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@}}">imgur store</a></li>
                            <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@}}">official apps</a></li>
                            <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@}}">developer api</a></li>
                            <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@}}">need help?</a></li>
                        </ul>
                        <div class="clear test-1868-hide"></div>
                        <div class="dropdown-footer hamburger-menu test-1868-hide">
                            <div class="terms-footer">
                                <a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@}}">terms</a>
                                <a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@}}">privacy</a>
                                <a class="small-margin-top" href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@}}">ad choices</a>
                            </div>
                            <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fimgur&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="display:inline-block; border:none; overflow:hidden; height:20px; width:50px; margin-right:7px;" allowTransparency="true"></iframe>
                            <a href="https://twitter.com/imgur" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false"></a>
                        </div>
                    </div>
                </li>
                <li class="upload-button-container">
                    <a class="upload-button disabled upload-blue" href="javascript:;">
                        <div class="icon icon-upload"></div>
                        <span class="upload-btn-text">upload images</span>
                        <span class="upload-queue-length">23</span>
                        <div id="upload-global-top-bar-mini-progress"></div>
                    </a>
                    <div class="creation-dropdown upload-blue">
                        <div class="selection icon icon-arrow-down"></div>
                        <div class="options">
                            <ul>
                                <li class="item">
                                    <a class="creation-dropdown-upload" href="javascript:;">Upload Images</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/vidgif">Video to GIF</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/memegen" data-jafo="{@@event@@:@@makeAMeme@@,@@meta@@:{@@control@@:@@header@@}}">Make a Meme</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="random-button title-n " title="random mode">
                    <a href="//imgur.com/random" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@random@@}}">
                        <div class="random-icon"></div>
                    </a>
                </li>
            </ul>
        </div>
        <div id="secondary-nav">
            <ul class="user-nav">
                <li id="global-search-container">
                    <form class="search-form" method="get" action="//imgur.com/search">
                        <input id="search-global-input" type="text" name="q" class="search" blur placeholder="Search images"  />
                    </form>

                    <div class="search-icon-container">
                        <div class="search-icon-container-icon"></div>
                    </div>
                </li>
                                    <li class="signin-link"><a class="" href="https://imgur.com/signin" data-jafo="{@@event@@:@@signinButtonClick@@}">sign in</a></li>
                    <li id="register"><a class="" href="https://imgur.com/register" data-jafo="{@@event@@:@@registerButtonClick@@}">sign up</a></li>
                
            </ul>
        </div> <!-- #secondary-nav -->
    </div> <!-- .header-center -->
</div> <!-- #topbar -->
<div id="cta-container-placeholder"></div>

    

    

            
        
        
    

            <a href="javascript:;" id="upload-global-album-tipsy" class="title-nw nodisplay" title="You can't create an album with just one image"></a>

<div id="upload-global" class="popup">
    <form method="post" id="upload-global-form" enctype="multipart/form-data" action="http://imgur.com/upload">
        

        <div id="upload-global-buttons">
            <div id="upload-global-upload-loader" class="textbox">
                Loading...
            </div>

            <div id="upload-global-file-wrapper">
                <span class="computerIcon"></span>
                <span class="green">browse</span> your computer

                <input accept="image/*" type="file" name="Filedata" multiple="multiple" id="upload-global-file">
            </div>

            <div class="left">
                <div id="upload-global-url">
                    <span class="webIcon"></span>
                    <input type="text" id="upload-global-link-input" class="placeholder-onkeydown" title="enter image URLs" placeholder="enter image URLs" />
                </div>
            </div>

            <div class="clear"></div>

            <div id="upload-global-dragdrop">
                <span class="dragdropIcon"></span>
                <span class="green">drag and drop</span> here
            </div>

            <div class="left">
                <div id="upload-global-clipboard">
                    <div style="text-align: center"><span class="clipboardIcons">Ctrl + V</span></div>
                    <span class="green">paste</span> from your clipboard
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="textbox center" id="upload-global-instructions"></div>

        <!-- gallery submit vars -->
        <input type="hidden" name="current_upload" value="0"/>
        <input type="hidden" name="total_uploads" value="0"/>
        <input type="hidden" name="terms" value="0"/>
        <input type="hidden" name="gallery_type" value=""/>


        <div id="upload-global-errors" class="textbox error" style="display: none;"></div>

        <div id="upload-global-queue-description" class="textbox nodisplay"></div>

        <div id="upload-global-start-upload">
            <div class="panel">
                <div>

                    <table id="upload-global-actions">
                        <tbody>
                            <tr>

                                <td width="50.4%">

                                    <div id="upload-global-album-checkbox-div">
                                        <input type="hidden" name="gallery_submit" value="0"/>

                                        <button id="upload-global-gallery-checkbox" type="button">
                                            <span class="gallery-icon"></span>publish to Imgur
                                        </button>

                                        <div id="upload-global-gallery-checkbox-overlay"></div>

                                        <input type="hidden" name="create_album" value="0"/>

                                        <button id="upload-global-album-checkbox" type="button">
                                            <span class="createAlbum-icon"></span>create album
                                        </button>

                                    </div>

                                </td>
                                <td width="49.6%">
                                    <button type="button" id="upload-global-start-button" class="button-big green" type="submit">
                                        <span class="start-btn-icon"></span>
                                        Start Upload
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div id="upload-global-album-options">
                    







<table id="upload-global-album-layout">
    <tr>
        <td id="upload-global-album-td">
            <input name="album_title" type="text" title="Optional Album Title" class="placeholder-onkeydown" id="upload-global-album-title" maxlength="55" />
        </td>

        <td class="center title" title="<table><tr><td style='vertical-align: middle'>&lt;img src=&#039;//s.imgur.com/images/album-layout-blog.png&#039; style=&#039;padding-right: 2px;&#039;/&gt;</td><td>&lt;h1&gt;Blog Layout&lt;/h1&gt;Shows all the images on one page in a blog post style layout. Best for albums with a small to medium amount of images.</td></tr></table>">
            <input id="blog-layout-upload" type="radio" name="layout" value="b" checked="checked"/>
            <label for="blog-layout-upload">Blog</label>
        </td>

        <td class="center title" title="<table><tr><td style='vertical-align: middle'>&lt;img src=&#039;//s.imgur.com/images/album-layout-horizontal.png&#039;  style=&#039;padding-right: 6px;&#039;/&gt;</td><td>&lt;h1&gt;Horizontal Layout&lt;/h1&gt;Shows one image at a time in a traditional style, with thumbnails on top. Best for albums that have high resolution photos.</td></tr></table>">
            <input id="horizontal-layout-upload" type="radio" name="layout" value="h"/>
            <label for="horizontal-layout-upload">Horizontal</label>
        </td>

        <td class="center title" title="<table><tr><td style='vertical-align: middle'>&lt;img src=&#039;//s.imgur.com/images/album-layout-grid.png&#039;  style=&#039;padding-right: 6px;&#039;/&gt;</td><td>&lt;h1&gt;Grid Layout&lt;/h1&gt;Shows all the images on one page as thumbnails that expand when clicked on. Best for albums with lots of images.</td></tr></table>">
            <input id="grid-layout-upload" type="radio" name="layout" value="g"/>
            <label for="grid-layout-upload">Grid</label>
        </td>

        <td width="15"></td>
    </tr>
</table>

                </div>

                <div class="textbox list">
                    <table id="upload-global-file-list-header">
                        <tr class="top-tr">
                            <td width="45" class="edit-td"><h2>Edit</h2></td>
                            <td><h2>Images <span class="upload-queue-length">(0)</span></h2></td>
                            <td width="40"></td>
                        </tr>
                    </table>

                    <table id="upload-global-file-list">
                    </table>
                                            <div class="agreement-notice">By uploading, you agree to our <a href="//imgur.com/tos" target="_blank">terms of service</a></div>
                    
                </div>
            </div>
        </div>

        <div id="upload-global-progress-wrapper">
            <div id="upload-global-progressbar">
                <div class="progressBarBG">
                    <div class="progressBar"></div>
                </div>
            </div>
            <div id="upload-global-upload-queue"></div>

            <div id="upload-global-converting">
                <p>
                    Optimizing your large GIFs...
                </p>
            </div>
        </div>
    </form>
</div>

<div id="upload-global-FF-paste-box"></div>

<div id="hiddenDropZone" class="nodisplay"></div>

<div class="nodisplay">
    <div id="upload-global-image-formats">
         That file type is not supported!

        <div class="textbox supported-formats">
            Supported formats: JPEG, GIF, PNG, APNG, TIFF, BMP, PDF, XCF
        </div>
    </div>
</div>

<div id="upload-global-flash-container" title="upload images from your computer">
    <div id="upload-global-flash-browse"></div>
</div>

    

        <div class="nodisplay">
	<div id="colorbox-confirm" class="popup">
		<h3 id="colorbox-confirm-title"></h3>
		
		<div class="textbox" id="colorbox-confirm-message"></div>

		<div class="margin-top">
			<div class="left">
				<a href="javascript:;" class="colorbox-no-confirm" id="colorbox-confirm-no">No way!</a>
			</div>
			
			<div class="right">
				<div class="small-loader"></div>
				<a href="javascript:;" class="colorbox-confirm" id="colorbox-confirm-yes">I'm sure</a>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</div>


            
    

    
            
    

    
    
    
    

    <div id="fullbleed-bg"></div>
    <div id="content" class="outside main">
        
                    <div class="sentence-sorting">
            <span class="before-text">
    The

</span>
<div id="section" class="combobox front-page-section sorting-text-align" name="section">
    <div class="selection">
                <div data-type="base" data-value="hot">
            <span class="name">Most Viral</span>
        </div>
        
        
    </div>

    <div class="options nano">
        <div class="js-galleries content">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                                            Most Viral
                    
                </div>
            </div>
            <ul>
                <li class="bold item nodisplay" data-type="base" data-value="hot">
                    <a href="/hot/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a>
                </li>
                <li class="bold item" data-type="base" data-value="new">
                                            <a href="/new/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@new@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a>
                    
                </li>
                <li class="combobox-hairline"></li>
                                                            <li class="item" data-value="Funny">
                            <a href="/topic/Funny" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                        </li>
                    
                                                            <li class="item" data-value="Awesome">
                            <a href="/topic/Awesome" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                        </li>
                    
                                                            <li class="item" data-value="Aww">
                            <a href="/topic/Aww" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                        </li>
                    
                                                            <li class="item" data-value="The More You Know">
                            <a href="/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                        </li>
                    
                                                            <li class="item" data-value="Storytime">
                            <a href="/topic/Storytime" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                        </li>
                    
                                                            <li class="item" data-value="Current Events">
                            <a href="/topic/Current_Events" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                        </li>
                    
                                                            <li class="item" data-value="Design &amp; Art">
                            <a href="/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                        </li>
                    
                                                            <li class="item" data-value="Reaction">
                            <a href="/topic/Reaction" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                        </li>
                    
                                    
                
            </ul>
        </div>
        <div id="custom-group" class="item new-group"><a href="/gallery/custom" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@custom@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Custom Gallery</a></div>
    </div>
    <input type="hidden" name="section" value="hot">
</div><span class="middle-text"> <!-- intentionally not closed -->

    images on the Internet, 


        sorted by
    
    </span> <!-- .middle-text -->
    
    
        
            
    

            
    
    <div id="sort" class="combobox">
        <div class="selection">
            popularity
        </div>
        <div class="options sorting-text-align">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                    popularity
                </div>
            </div>
            <ul>
                                    <li class="item nodisplay" value="viral">
                                                    <a href="/hot/viral">popularity</a>
                        
                    </li>
                    <li class="item" value="time">
                                                    <a href="/hot/time">newest first</a>
                        
                    </li>
                                        <li class="item" value="top">
                                                    <a href="/top">highest scoring</a>
                        
                    </li>
                    
                
            </ul>
        </div>
        <input type="hidden" name="sort" value="viral">
    </div>

    

    <div class="sort-options">
                                    <ul>
                                            <li><a href="/gallery/random" id="random-button" class="title-n" title="random mode"><span></span></a></li>
                    
                                            <li><a href="javascript:void(0)" id="past-link" class="title-n" title="past images"><span></span></a></li>
                    
                </ul>
            
        
    </div>
    <div id="user-gallery-message">
        
    </div>
</div>

               
        

                    <div class="panel hidden">
                <div id="imagelist" class="home-gallery">
                                            <script type="text/tag-group-data">[]</script>


<div class="posts br10 first-child">
    

            
        <div class="outside-loader"></div>
        
                    
        

        <div class="cards">
                            <div id="m00tubZ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/m00tubZ" data-page="0">
        <img alt="" src="//i.imgur.com/m00tubZb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="m00tubZ" type="image" data-up="14372">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="m00tubZ" type="image" data-downs="376">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-m00tubZ">13,996</span>
                            <span class="points-text-m00tubZ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Domo arigato.</p>
        
        
        <div class="post-info">
            image &middot; 381,730 views
        </div>
    </div>
    
</div>

                            <div id="hCQ28m4" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/hCQ28m4" data-page="0">
        <img alt="" src="//i.imgur.com/hCQ28m4b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="hCQ28m4" type="image" data-up="16618">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="hCQ28m4" type="image" data-downs="231">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-hCQ28m4">16,387</span>
                            <span class="points-text-hCQ28m4">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Homeless man dies saving a woman taken hostage by gunman.</p>
        
        
        <div class="post-info">
            animated &middot; 883,661 views
        </div>
    </div>
    
</div>

                            <div id="QNqcDwJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/QNqcDwJ" data-page="0">
        <img alt="" src="//i.imgur.com/QNqcDwJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="QNqcDwJ" type="image" data-up="8978">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="QNqcDwJ" type="image" data-downs="152">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-QNqcDwJ">8,826</span>
                            <span class="points-text-QNqcDwJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Millions of years of evolution did nothing to prepare her for life in my house.</p>
        
        
        <div class="post-info">
            animated &middot; 3,055,628 views
        </div>
    </div>
    
</div>

                            <div id="nqcMAk0" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/nqcMAk0" data-page="0">
        <img alt="" src="//i.imgur.com/nqcMAk0b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="nqcMAk0" type="image" data-up="5904">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="nqcMAk0" type="image" data-downs="32">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-nqcMAk0">5,872</span>
                            <span class="points-text-nqcMAk0">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Cat-like reflexes by a Dublin busker</p>
        
        
        <div class="post-info">
            animated &middot; 314,794 views
        </div>
    </div>
    
</div>

                            <div id="cvx1pef" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/cvx1pef" data-page="0">
        <img alt="" src="//i.imgur.com/cvx1pefb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="cvx1pef" type="image" data-up="9847">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="cvx1pef" type="image" data-downs="272">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-cvx1pef">9,575</span>
                            <span class="points-text-cvx1pef">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Adult Floofs with their Baby Floofs.</p>
        
        
        <div class="post-info">
            image &middot; 340,001 views
        </div>
    </div>
    
</div>

                            <div id="Bnro2" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Bnro2" data-page="0">
        <img alt="" src="//i.imgur.com/augtkTeb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Bnro2" type="image" data-up="8505">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Bnro2" type="image" data-downs="78">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Bnro2">8,427</span>
                            <span class="points-text-Bnro2">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>In-Store Ikea Reviews</p>
        
        
        <div class="post-info">
            album &middot; 1,026,700 views
        </div>
    </div>
    
</div>

                            <div id="E4OEJDa" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/E4OEJDa" data-page="0">
        <img alt="" src="//i.imgur.com/E4OEJDab.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="E4OEJDa" type="image" data-up="4507">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="E4OEJDa" type="image" data-downs="235">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-E4OEJDa">4,272</span>
                            <span class="points-text-E4OEJDa">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>She is NO hero</p>
        
        
        <div class="post-info">
            image &middot; 129,158 views
        </div>
    </div>
    
</div>

                            <div id="B2LDz9u" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/B2LDz9u" data-page="0">
        <img alt="" src="//i.imgur.com/B2LDz9ub.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="B2LDz9u" type="image" data-up="8236">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="B2LDz9u" type="image" data-downs="91">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-B2LDz9u">8,145</span>
                            <span class="points-text-B2LDz9u">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Epic jump</p>
        
        
        <div class="post-info">
            animated &middot; 626,166 views
        </div>
    </div>
    
</div>

                            <div id="pzYrh" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/pzYrh" data-page="0">
        <img alt="" src="//i.imgur.com/JPbXoGlb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="pzYrh" type="image" data-up="6306">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="pzYrh" type="image" data-downs="146">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-pzYrh">6,160</span>
                            <span class="points-text-pzYrh">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>This is rather terrifying. especially when the mind wanders to what could be happening now.</p>
        
        
        <div class="post-info">
            album &middot; 129,404 views
        </div>
    </div>
    
</div>

                            <div id="Zz8vZFg" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Zz8vZFg" data-page="0">
        <img alt="" src="//i.imgur.com/Zz8vZFgb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Zz8vZFg" type="image" data-up="5066">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Zz8vZFg" type="image" data-downs="67">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Zz8vZFg">4,999</span>
                            <span class="points-text-Zz8vZFg">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>countdown</p>
        
        
        <div class="post-info">
            animated &middot; 285,855 views
        </div>
    </div>
    
</div>

                            <div id="3FKqA" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/3FKqA" data-page="0">
        <img alt="" src="//i.imgur.com/PdTlW5eb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="3FKqA" type="image" data-up="6357">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="3FKqA" type="image" data-downs="110">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-3FKqA">6,247</span>
                            <span class="points-text-3FKqA">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The Rock is too awesome to possibly be real.</p>
        
        
        <div class="post-info">
            album &middot; 147,195 views
        </div>
    </div>
    
</div>

                            <div id="6PFIGTd" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/6PFIGTd" data-page="0">
        <img alt="" src="//i.imgur.com/6PFIGTdb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="6PFIGTd" type="image" data-up="17493">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="6PFIGTd" type="image" data-downs="862">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-6PFIGTd">16,631</span>
                            <span class="points-text-6PFIGTd">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Anime for adults (no teenagers this time)</p>
        
        
        <div class="post-info">
            image &middot; 534,836 views
        </div>
    </div>
    
</div>

                            <div id="fKRLXf1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/fKRLXf1" data-page="0">
        <img alt="" src="//i.imgur.com/fKRLXf1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="fKRLXf1" type="image" data-up="4736">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="fKRLXf1" type="image" data-downs="90">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-fKRLXf1">4,646</span>
                            <span class="points-text-fKRLXf1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Good Boy waiting for his Human.</p>
        
        
        <div class="post-info">
            animated &middot; 346,825 views
        </div>
    </div>
    
</div>

                            <div id="tIGtu" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/tIGtu" data-page="0">
        <img alt="" src="//i.imgur.com/GkDzOxFb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="tIGtu" type="image" data-up="9746">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="tIGtu" type="image" data-downs="423">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-tIGtu">9,323</span>
                            <span class="points-text-tIGtu">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My 10 favourite electronic artists</p>
        
        
        <div class="post-info">
            album &middot; 211,700 views
        </div>
    </div>
    
</div>

                            <div id="Ic9MdF6" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Ic9MdF6" data-page="0">
        <img alt="" src="//i.imgur.com/Ic9MdF6b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Ic9MdF6" type="image" data-up="4204">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Ic9MdF6" type="image" data-downs="291">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Ic9MdF6">3,913</span>
                            <span class="points-text-Ic9MdF6">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I should have started this show a long time ago...</p>
        
        
        <div class="post-info">
            image &middot; 1,159,271 views
        </div>
    </div>
    
</div>

                            <div id="42mAuUE" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/42mAuUE" data-page="0">
        <img alt="" src="//i.imgur.com/42mAuUEb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="42mAuUE" type="image" data-up="4837">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="42mAuUE" type="image" data-downs="85">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-42mAuUE">4,752</span>
                            <span class="points-text-42mAuUE">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Live Action GTA</p>
        
        
        <div class="post-info">
            image &middot; 2,130,408 views
        </div>
    </div>
    
</div>

                            <div id="yZ5cGUc" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/yZ5cGUc" data-page="0">
        <img alt="" src="//i.imgur.com/yZ5cGUcb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="yZ5cGUc" type="image" data-up="4418">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="yZ5cGUc" type="image" data-downs="100">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-yZ5cGUc">4,318</span>
                            <span class="points-text-yZ5cGUc">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My kind of girl</p>
        
        
        <div class="post-info">
            image &middot; 167,245 views
        </div>
    </div>
    
</div>

                            <div id="qoERO" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/qoERO" data-page="0">
        <img alt="" src="//i.imgur.com/4gjWVhcb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="qoERO" type="image" data-up="6271">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="qoERO" type="image" data-downs="141">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-qoERO">6,130</span>
                            <span class="points-text-qoERO">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>.</p>
        
        
        <div class="post-info">
            album &middot; 163,911 views
        </div>
    </div>
    
</div>

                            <div id="1O4pv0g" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/1O4pv0g" data-page="0">
        <img alt="" src="//i.imgur.com/1O4pv0gb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="1O4pv0g" type="image" data-up="6022">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="1O4pv0g" type="image" data-downs="650">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-1O4pv0g">5,372</span>
                            <span class="points-text-1O4pv0g">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW I post the same photo of Michael Cera (badly photoshopped) every day</p>
        
        
        <div class="post-info">
            image &middot; 258,221 views
        </div>
    </div>
    
</div>

                            <div id="E7PFmob" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/E7PFmob" data-page="0">
        <img alt="" src="//i.imgur.com/E7PFmobb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="E7PFmob" type="image" data-up="5387">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="E7PFmob" type="image" data-downs="171">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-E7PFmob">5,216</span>
                            <span class="points-text-E7PFmob">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Came across this quote from my old yearbook</p>
        
        
        <div class="post-info">
            image &middot; 1,815,791 views
        </div>
    </div>
    
</div>

                            <div id="aXMLUpz" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/aXMLUpz" data-page="0">
        <img alt="" src="//i.imgur.com/aXMLUpzb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="aXMLUpz" type="image" data-up="5940">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="aXMLUpz" type="image" data-downs="82">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-aXMLUpz">5,858</span>
                            <span class="points-text-aXMLUpz">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Thid guy passed me while giving my J3WBACA plates a thumbs up, before pulling in front of me</p>
        
        
        <div class="post-info">
            image &middot; 297,905 views
        </div>
    </div>
    
</div>

                            <div id="oLdUI6E" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/oLdUI6E" data-page="0">
        <img alt="" src="//i.imgur.com/oLdUI6Eb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="oLdUI6E" type="image" data-up="3757">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="oLdUI6E" type="image" data-downs="77">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-oLdUI6E">3,680</span>
                            <span class="points-text-oLdUI6E">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>He keeps kicking himself.</p>
        
        
        <div class="post-info">
            animated &middot; 247,872 views
        </div>
    </div>
    
</div>

                            <div id="VkZ62" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/VkZ62" data-page="0">
        <img alt="" src="//i.imgur.com/gEvpl42b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="VkZ62" type="image" data-up="6279">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="VkZ62" type="image" data-downs="161">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-VkZ62">6,118</span>
                            <span class="points-text-VkZ62">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>In 1974, a young photographer took a series of intimate portraits of two mimes in Central Park. 35 years later, when he dug the film negatives out of an old suitcase, he realized whom he had photographed.</p>
        
        
        <div class="post-info">
            album &middot; 720,817 views
        </div>
    </div>
    
</div>

                            <div id="KtyOmp1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/KtyOmp1" data-page="0">
        <img alt="" src="//i.imgur.com/KtyOmp1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="KtyOmp1" type="image" data-up="7466">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="KtyOmp1" type="image" data-downs="77">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-KtyOmp1">7,389</span>
                            <span class="points-text-KtyOmp1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The car I designed in 3rd grade has finally made it into production!!</p>
        
        
        <div class="post-info">
            image &middot; 2,188,052 views
        </div>
    </div>
    
</div>

                            <div id="vKvlm" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/vKvlm" data-page="0">
        <img alt="" src="//i.imgur.com/7iP0hgUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="vKvlm" type="image" data-up="3548">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="vKvlm" type="image" data-downs="134">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-vKvlm">3,414</span>
                            <span class="points-text-vKvlm">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Hipster Barbieâs Instagram</p>
        
        
        <div class="post-info">
            album &middot; 88,106 views
        </div>
    </div>
    
</div>

                            <div id="TRG43Gx" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/TRG43Gx" data-page="0">
        <img alt="" src="//i.imgur.com/TRG43Gxb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="TRG43Gx" type="image" data-up="4402">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="TRG43Gx" type="image" data-downs="143">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-TRG43Gx">4,259</span>
                            <span class="points-text-TRG43Gx">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Dog reacts appropriately to new baby</p>
        
        
        <div class="post-info">
            animated &middot; 641,682 views
        </div>
    </div>
    
</div>

                            <div id="o3shqMI" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/o3shqMI" data-page="0">
        <img alt="" src="//i.imgur.com/o3shqMIb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="o3shqMI" type="image" data-up="10675">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="o3shqMI" type="image" data-downs="220">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-o3shqMI">10,455</span>
                            <span class="points-text-o3shqMI">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Hitler vs Stalin</p>
        
        
        <div class="post-info">
            image &middot; 1,131,040 views
        </div>
    </div>
    
</div>

                            <div id="AYPg0" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/AYPg0" data-page="0">
        <img alt="" src="//i.imgur.com/5quxQA4b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="AYPg0" type="image" data-up="8886">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="AYPg0" type="image" data-downs="156">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-AYPg0">8,730</span>
                            <span class="points-text-AYPg0">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I would do anything for love......</p>
        
        
        <div class="post-info">
            album &middot; 202,592 views
        </div>
    </div>
    
</div>

                            <div id="sEI81" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/sEI81" data-page="0">
        <img alt="" src="//i.imgur.com/YooB5U9b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="sEI81" type="image" data-up="5762">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="sEI81" type="image" data-downs="71">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-sEI81">5,691</span>
                            <span class="points-text-sEI81">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Jackie chan has some adorable facebook posts</p>
        
        
        <div class="post-info">
            album &middot; 174,662 views
        </div>
    </div>
    
</div>

                            <div id="P8ZSZsF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/P8ZSZsF" data-page="0">
        <img alt="" src="//i.imgur.com/P8ZSZsFb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="P8ZSZsF" type="image" data-up="7050">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="P8ZSZsF" type="image" data-downs="84">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-P8ZSZsF">6,966</span>
                            <span class="points-text-P8ZSZsF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>When skate is life but still you are daddy&#039;s little princess</p>
        
        
        <div class="post-info">
            animated &middot; 441,180 views
        </div>
    </div>
    
</div>

                            <div id="VPuPy4K" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/VPuPy4K" data-page="0">
        <img alt="" src="//i.imgur.com/VPuPy4Kb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="VPuPy4K" type="image" data-up="4594">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="VPuPy4K" type="image" data-downs="177">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-VPuPy4K">4,417</span>
                            <span class="points-text-VPuPy4K">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Yes please</p>
        
        
        <div class="post-info">
            image &middot; 224,031 views
        </div>
    </div>
    
</div>

                            <div id="mqE9FME" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/mqE9FME" data-page="0">
        <img alt="" src="//i.imgur.com/mqE9FMEb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="mqE9FME" type="image" data-up="1840">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="mqE9FME" type="image" data-downs="25">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-mqE9FME">1,815</span>
                            <span class="points-text-mqE9FME">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW I remember that awkward thing i did 7 years ago.</p>
        
        
        <div class="post-info">
            animated &middot; 87,954 views
        </div>
    </div>
    
</div>

                            <div id="eu84R" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/eu84R" data-page="0">
        <img alt="" src="//i.imgur.com/M1qqZy2b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="eu84R" type="image" data-up="25145">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="eu84R" type="image" data-downs="281">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-eu84R">24,864</span>
                            <span class="points-text-eu84R">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Just in case...</p>
        
        
        <div class="post-info">
            album &middot; 380,150 views
        </div>
    </div>
    
</div>

                            <div id="FTBUEXF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FTBUEXF" data-page="0">
        <img alt="" src="//i.imgur.com/FTBUEXFb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FTBUEXF" type="image" data-up="2056">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FTBUEXF" type="image" data-downs="134">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FTBUEXF">1,922</span>
                            <span class="points-text-FTBUEXF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>ApplePencil</p>
        
        
        <div class="post-info">
            image &middot; 1,661,356 views
        </div>
    </div>
    
</div>

                            <div id="fOZ2y" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/fOZ2y" data-page="0">
        <img alt="" src="//i.imgur.com/K8it5NGb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="fOZ2y" type="image" data-up="4975">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="fOZ2y" type="image" data-downs="351">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-fOZ2y">4,624</span>
                            <span class="points-text-fOZ2y">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Got to Go</p>
        
        
        <div class="post-info">
            album &middot; 120,137 views
        </div>
    </div>
    
</div>

                            <div id="fonVoLB" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/fonVoLB" data-page="0">
        <img alt="" src="//i.imgur.com/fonVoLBb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="fonVoLB" type="image" data-up="6640">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="fonVoLB" type="image" data-downs="136">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-fonVoLB">6,504</span>
                            <span class="points-text-fonVoLB">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Give him some pot too and he will design a perpetual motion machine</p>
        
        
        <div class="post-info">
            image &middot; 351,555 views
        </div>
    </div>
    
</div>

                            <div id="ygpZbw7" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ygpZbw7" data-page="0">
        <img alt="" src="//i.imgur.com/ygpZbw7b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ygpZbw7" type="image" data-up="2895">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ygpZbw7" type="image" data-downs="80">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ygpZbw7">2,815</span>
                            <span class="points-text-ygpZbw7">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>word association</p>
        
        
        <div class="post-info">
            image &middot; 142,697 views
        </div>
    </div>
    
</div>

                            <div id="BZD6oNH" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/BZD6oNH" data-page="0">
        <img alt="" src="//i.imgur.com/BZD6oNHb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="BZD6oNH" type="image" data-up="4030">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="BZD6oNH" type="image" data-downs="340">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-BZD6oNH">3,690</span>
                            <span class="points-text-BZD6oNH">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>This will always be my favorite &quot;burrrrrnnnnn&quot;...</p>
        
        
        <div class="post-info">
            image &middot; 216,123 views
        </div>
    </div>
    
</div>

                            <div id="VZbx4" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/VZbx4" data-page="0">
        <img alt="" src="//i.imgur.com/6WOeTgKb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="VZbx4" type="image" data-up="7248">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="VZbx4" type="image" data-downs="386">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-VZbx4">6,862</span>
                            <span class="points-text-VZbx4">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>You know what time it is? It&#039;s CRINGE TIME! Part 3</p>
        
        
        <div class="post-info">
            album &middot; 250,457 views
        </div>
    </div>
    
</div>

                            <div id="K4MwxDI" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/K4MwxDI" data-page="0">
        <img alt="" src="//i.imgur.com/K4MwxDIb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="K4MwxDI" type="image" data-up="1139">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="K4MwxDI" type="image" data-downs="69">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-K4MwxDI">1,070</span>
                            <span class="points-text-K4MwxDI">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Have you seen Forrest Gump?</p>
        
        
        <div class="post-info">
            image &middot; 430,773 views
        </div>
    </div>
    
</div>

                            <div id="lWxG5xJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/lWxG5xJ" data-page="0">
        <img alt="" src="//i.imgur.com/lWxG5xJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="lWxG5xJ" type="image" data-up="3681">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="lWxG5xJ" type="image" data-downs="68">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-lWxG5xJ">3,613</span>
                            <span class="points-text-lWxG5xJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>A disgruntled customer has been staring at my mom through her office window because the bird feeder is empty.</p>
        
        
        <div class="post-info">
            image &middot; 998,753 views
        </div>
    </div>
    
</div>

                            <div id="UVIF1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/UVIF1" data-page="0">
        <img alt="" src="//i.imgur.com/7X6Glzlb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="UVIF1" type="image" data-up="3321">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="UVIF1" type="image" data-downs="148">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-UVIF1">3,173</span>
                            <span class="points-text-UVIF1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>funny</p>
        
        
        <div class="post-info">
            album &middot; 101,887 views
        </div>
    </div>
    
</div>

                            <div id="gf6yRXP" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/gf6yRXP" data-page="0">
        <img alt="" src="//i.imgur.com/gf6yRXPb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="gf6yRXP" type="image" data-up="4236">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="gf6yRXP" type="image" data-downs="255">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-gf6yRXP">3,981</span>
                            <span class="points-text-gf6yRXP">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW someone says GIFs don&#039;t count as OC</p>
        
        
        <div class="post-info">
            animated &middot; 353,371 views
        </div>
    </div>
    
</div>

                            <div id="QzqdxHv" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/QzqdxHv" data-page="0">
        <img alt="" src="//i.imgur.com/QzqdxHvb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="QzqdxHv" type="image" data-up="7812">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="QzqdxHv" type="image" data-downs="350">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-QzqdxHv">7,462</span>
                            <span class="points-text-QzqdxHv">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Why don&#039;t the refugees go back to where they come from? This answers your question.</p>
        
        
        <div class="post-info">
            image &middot; 1,521,125 views
        </div>
    </div>
    
</div>

                            <div id="8heN8" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/8heN8" data-page="0">
        <img alt="" src="//i.imgur.com/l9wAOMGb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="8heN8" type="image" data-up="1583">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="8heN8" type="image" data-downs="33">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-8heN8">1,550</span>
                            <span class="points-text-8heN8">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Advanced stupid.</p>
        
        
        <div class="post-info">
            album &middot; 27,411 views
        </div>
    </div>
    
</div>

                            <div id="B3ZDM2o" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/B3ZDM2o" data-page="0">
        <img alt="" src="//i.imgur.com/B3ZDM2ob.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="B3ZDM2o" type="image" data-up="3424">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="B3ZDM2o" type="image" data-downs="205">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-B3ZDM2o">3,219</span>
                            <span class="points-text-B3ZDM2o">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Why the hell does this ice cream taste like shit.</p>
        
        
        <div class="post-info">
            animated &middot; 363,756 views
        </div>
    </div>
    
</div>

                            <div id="grTEV" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/grTEV" data-page="0">
        <img alt="" src="//i.imgur.com/RihhhWtb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="grTEV" type="image" data-up="5269">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="grTEV" type="image" data-downs="660">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-grTEV">4,609</span>
                            <span class="points-text-grTEV">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>8 facts about cannabis you might not know</p>
        
        
        <div class="post-info">
            album &middot; 159,204 views
        </div>
    </div>
    
</div>

                            <div id="UBQCfID" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/UBQCfID" data-page="0">
        <img alt="" src="//i.imgur.com/UBQCfIDb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="UBQCfID" type="image" data-up="10785">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="UBQCfID" type="image" data-downs="972">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-UBQCfID">9,813</span>
                            <span class="points-text-UBQCfID">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Look at my username</p>
        
        
        <div class="post-info">
            animated &middot; 1,319,516 views
        </div>
    </div>
    
</div>

                            <div id="FRBjiRA" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FRBjiRA" data-page="0">
        <img alt="" src="//i.imgur.com/FRBjiRAb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FRBjiRA" type="image" data-up="1779">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FRBjiRA" type="image" data-downs="138">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FRBjiRA">1,641</span>
                            <span class="points-text-FRBjiRA">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Something else I had to get off my chest.</p>
        
        
        <div class="post-info">
            image &middot; 79,976 views
        </div>
    </div>
    
</div>

                            <div id="gXjVu" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/gXjVu" data-page="0">
        <img alt="" src="//i.imgur.com/ZrTc9rAb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="gXjVu" type="image" data-up="2111">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="gXjVu" type="image" data-downs="38">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-gXjVu">2,073</span>
                            <span class="points-text-gXjVu">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Stop Unnecessary actions today</p>
        
        
        <div class="post-info">
            album &middot; 43,385 views
        </div>
    </div>
    
</div>

                            <div id="Qx6Zx" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Qx6Zx" data-page="0">
        <img alt="" src="//i.imgur.com/yu4t5qYb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Qx6Zx" type="image" data-up="1702">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Qx6Zx" type="image" data-downs="53">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Qx6Zx">1,649</span>
                            <span class="points-text-Qx6Zx">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Ok imgur, lets update some stuff</p>
        
        
        <div class="post-info">
            album &middot; 38,333 views
        </div>
    </div>
    
</div>

                            <div id="howW3Rx" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/howW3Rx" data-page="0">
        <img alt="" src="//i.imgur.com/howW3Rxb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="howW3Rx" type="image" data-up="1128">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="howW3Rx" type="image" data-downs="47">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-howW3Rx">1,081</span>
                            <span class="points-text-howW3Rx">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Nailed it. This was published back in 2012</p>
        
        
        <div class="post-info">
            image &middot; 432,429 views
        </div>
    </div>
    
</div>

                            <div id="vQlcF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/vQlcF" data-page="0">
        <img alt="" src="//i.imgur.com/gqne2hob.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="vQlcF" type="image" data-up="4962">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="vQlcF" type="image" data-downs="280">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-vQlcF">4,682</span>
                            <span class="points-text-vQlcF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>You will never guess where in the world this happened!</p>
        
        
        <div class="post-info">
            album &middot; 122,830 views
        </div>
    </div>
    
</div>

                            <div id="Mf5fb3Q" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Mf5fb3Q" data-page="0">
        <img alt="" src="//i.imgur.com/Mf5fb3Qb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Mf5fb3Q" type="image" data-up="897">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Mf5fb3Q" type="image" data-downs="12">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Mf5fb3Q">885</span>
                            <span class="points-text-Mf5fb3Q">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Navy Seal Airdrop</p>
        
        
        <div class="post-info">
            animated &middot; 490,444 views
        </div>
    </div>
    
</div>

                            <div id="ldOMnZh" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ldOMnZh" data-page="0">
        <img alt="" src="//i.imgur.com/ldOMnZhb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ldOMnZh" type="image" data-up="3091">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ldOMnZh" type="image" data-downs="66">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ldOMnZh">3,025</span>
                            <span class="points-text-ldOMnZh">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Plitvice Lakes, Croatia | Went there with high expectations, wasn&#039;t disappointed! </p>
        
        
        <div class="post-info">
            image &middot; 991,526 views
        </div>
    </div>
    
</div>

                            <div id="p5tJIUc" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/p5tJIUc" data-page="0">
        <img alt="" src="//i.imgur.com/p5tJIUcb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="p5tJIUc" type="image" data-up="1678">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="p5tJIUc" type="image" data-downs="60">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-p5tJIUc">1,618</span>
                            <span class="points-text-p5tJIUc">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Danish Police Officer Has Had Enough of Syrian Girl Who Crossed the Border Illegally</p>
        
        
        <div class="post-info">
            image &middot; 367,968 views
        </div>
    </div>
    
</div>

                            <div id="8ZlODtT" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/8ZlODtT" data-page="0">
        <img alt="" src="//i.imgur.com/8ZlODtTb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="8ZlODtT" type="image" data-up="1043">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="8ZlODtT" type="image" data-downs="57">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-8ZlODtT">986</span>
                            <span class="points-text-8ZlODtT">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>me irl</p>
        
        
        <div class="post-info">
            image &middot; 305,151 views
        </div>
    </div>
    
</div>

                            <div id="wLUge" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/wLUge" data-page="0">
        <img alt="" src="//i.imgur.com/AhjsyAbb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="wLUge" type="image" data-up="4454">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="wLUge" type="image" data-downs="114">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-wLUge">4,340</span>
                            <span class="points-text-wLUge">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>#queenfacts is trending on Twitter</p>
        
        
        <div class="post-info">
            album &middot; 145,844 views
        </div>
    </div>
    
</div>

                            <div id="k3cKYfJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/k3cKYfJ" data-page="0">
        <img alt="" src="//i.imgur.com/k3cKYfJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="k3cKYfJ" type="image" data-up="2795">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="k3cKYfJ" type="image" data-downs="217">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-k3cKYfJ">2,578</span>
                            <span class="points-text-k3cKYfJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>You da real MVPs</p>
        
        
        <div class="post-info">
            image &middot; 126,677 views
        </div>
    </div>
    
</div>

                            <div id="B0vKtRi" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/B0vKtRi" data-page="0">
        <img alt="" src="//i.imgur.com/B0vKtRib.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="B0vKtRi" type="image" data-up="3080">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="B0vKtRi" type="image" data-downs="100">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-B0vKtRi">2,980</span>
                            <span class="points-text-B0vKtRi">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Spiders hate peppermint oil</p>
        
        
        <div class="post-info">
            image &middot; 686,931 views
        </div>
    </div>
    
</div>

            
        </div>

        <div class="clear"></div>

        <div class="imagelist-loader textbox center lineheight small-margin-top">
            <img src="//s.imgur.com/images/loaders/ddddd1_181817/48.gif" />
        </div>
    


    </div>
            <div class="page-divider"><a href="javascript:;" class="scroll-top-text" data-jafo="{@@event@@:@@galleryBackToTop@@,@@meta@@:{@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@pageNum@@:0}}">back to top</a></div>
    


                    
                </div>
            
                                                            <div id="top-comments" class="left-column">
                                                                                    <div id="outside-tagging" class="br10">
                                    <div class="group-add-done nodisplay"></div>
        
                                    <div id="tags-current-list" class="tag-list tag-list-immutable">
                                                                                        <div class="panel">
        <div class="textbox">
            <h2>Today&#039;s Best Comments</h2>
        </div>
    
        <div class="captions best-captions">
                            <div class="caption-container rank-0">
                    <p class="clickability">
                        <a href="/gallery/bJZUOyt/comment/473696565"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I&#039;m sweating bullets" src="//i.imgur.com/bJZUOytb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/RZA98">RZA98</a> 5,395 points
                        </div>
        
                                                    Every guy who got laid at camp imgur just &#9829; bricks
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-1">
                    <p class="clickability">
                        <a href="/gallery/hCQ28m4/comment/473907503"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Homeless man dies saving a woman taken hostage by gunman." src="//i.imgur.com/hCQ28m4b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/bipolax">bipolax</a> 4,400 points
                        </div>
        
                                                    his chest gets red at the end :( ... R.I.P unknown hero :(
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-2">
                    <p class="clickability">
                        <a href="/gallery/zU2FxAl/comment/473726176"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I saw this girl with a pretty face. I got jealous. so I stole it" src="//i.imgur.com/zU2FxAlb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/PaperSquids">PaperSquids</a> 4,385 points
                        </div>
        
                                                    So you agree? You think you&#039;re pretty?
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/YTIx8/comment/473699328"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="The Weird Pig" src="//i.imgur.com/TWJuseOb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/CthulhuforPresident2012">CthulhuforPresident2012</a> 3,956 points
                        </div>
        
                                                    Hey <a href="//imgur.com/user/op">@op</a> can you please do a follow-up post when the results come in?
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/D709Mho/comment/473756568"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="This is what Mount Rushmore was supposed to look like." src="//i.imgur.com/D709Mhob.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/captainobvioustits">captainobvioustits</a> 3,579 points
                        </div>
        
                                                    I GUESS THEY SHOULDN&#039;T HAVE RUSHED IT THEN
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/eu84R/comment/473787499"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Just in case..." src="//i.imgur.com/M1qqZy2b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/Therealmaeunu">Therealmaeunu</a> 3,361 points
                        </div>
        
                                                    It&#039;s easier to just die.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container last-child">
                    <p class="clickability">
                        <a href="/gallery/ZorcBlv/comment/473711001"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Maisie Williams new youtube channel is promising" src="//i.imgur.com/ZorcBlvb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/DoverBoys">DoverBoys</a> 3,326 points
                        </div>
        
                                                    I&#039;m sorry, but that gif is just... bleh... here you go: <a class="imgur-image" data-hash="85BtfgN" href="//i.imgur.com/85BtfgN.gif">http://i.imgur.com/85BtfgN.gif</a>
                        
                    </div>
                </div>

                <div class="clear"></div>
            
    
            
        </div>
    </div>


                                        
                                    </div>
                                </div>
        
                                <div id="outside-tagging-showhide" class="no-select title-e short" title=" Show Sidebar">
                                                                            <div id="comment-tab">
                                            <div id="comment-icon-one"></div>
                                            <div id="comment-icon-two"></div>
                                            <div id="comment-icon-three"></div>
                                        </div>
                                    
                                </div>
                            
                        
                        </div>
                    
                
            </div>
        

        <div class="clear"></div>
    </div>

    <div class="nodisplay">
        <div id="past-wrapper" class="popup">
            <div>
                <div class="textbox left">
                    

                    <input type="text" id="days" />

                                            days ago
                    
                </div>
                
                <div id="past-calendar-wrapper" class="textbox left">
                    <div id="past-calendar"></div>
                </div>

                <div id="slider-wrapper" class="textbox left">
                    <div class="ticker negative">&laquo;</div>
                    <div id="slider"></div>
                    <div class="ticker positive">&raquo;</div>
                </div>

                <div id="button-wrapper" class="textbox left">
                    <input type="button" class="button-medium" value="Go" id="past-submit" />
                </div>

                <div class="clear"></div>
            </div>

            <div id="past-loader"></div>
            <div id="past-preview"></div>
        </div>
    </div>

    <div class="tag-group-selector-container"></div>
            <div id="top-tag-container"></div>
    

    <input id="sid" type="hidden" value="217dfcc96542c645d0ce901be1ef7f21" />
        

    

            
    
    
    

            <script type="text/javascript">
            (function() {
                var roll = Math.random();
                if(roll < 0.05) {
                    
                    (function(_,e,rr,s){
                        _errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                        _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)
                    })(window,document,"script","51d1e360b05ccb10310000d2");
                    
                }
            })();
        </script>

        
<!--[if lte IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery-1.10.2.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if gt IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<!--<![endif]-->



        <script type="text/javascript" src="//s.imgur.com/min/global.js?1441834350"></script>

        <script type="text/javascript">
            Imgur.Environment = {
                isDev:         false,
                isSubdomain:   Imgur.Util.isSubdomain('imgur.com'),
                host:          'imgur.com',
                cdnUrl:        '//i.imgur.com',
                staticUrl:     '//s.imgur.com',
                signed:        false,
                auth:          {},
                recaptcha_key: '6LeZbt4SAAAAAKEsafT3QzEFp5vJ1-Z23uy5mPDz',
                msid:          '217dfcc96542c645d0ce901be1ef7f21',
                beta:          {
                    enabled:   true,
                    inBeta:    false
                },
                
                ctaConfig:     {"anonymous":[{"active":true,"type":"button","trackingName":"anonymous","localStorageName":"cta-anonymous","url":"\/","buttonText":"start exploring","title":"Welcome to Imgur!","description":"Your new home for the Internet's best visual stories.","background":"{STATIC}\/images\/house-cta\/cta-background.jpg","newTab":false,"buttonColor":"#77ac21"}],"user":[{"active":false,"type":"button","trackingName":"campImgur","localStorageName":"cta-camp1418760143","customClass":"u-pl95","url":"\/\/imgur.com\/camp","buttonText":"Get More Details","title":"Camp Imgur, August 2015","description":"We're 50% sold out! Reserve your space now.","background":"{STATIC}\/images\/house-cta\/cta-camp-imgur.jpg","newTab":true,"buttonColor":"#cf5033"}],"gaming":[{"active":true,"type":"button","trackingName":"gaming","localStorageName":"cta-gaming","url":"\/\/imgur.com\/t\/gaming\/top\/week","buttonText":"Check it out!","title":"You are now browsing the Internet on God Mode.","description":"Awesome unlocked! Imgur has gaming content like you wouldn't believe.","background":"{STATIC}\/images\/house-cta\/cta-twitch.jpg","newTab":false,"buttonColor":"#4d3280"}],"facebook":[{"active":true,"type":"custom","jsReactClassName":"Facebook","trackingName":"facebook","localStorageName":"cta-fb141008","title":"Like us on Facebook!","description":"We're bringing funny to your newsfeed. Like Imgur on Facebook!","background":"{STATIC}\/images\/house-cta\/facebook-day.jpg"}],"pinterest":[{"active":true,"type":"custom","jsReactClassName":"Pinterest","trackingName":"pinterest","localStorageName":"cta-pinterest-2015-07-14","title":"Follow us on Pinterest!","background":"{STATIC}\/images\/house-cta\/pinterest.png","customClass":"pinterest"}],"promoted":[{"active":true,"type":"custom","jsReactClassName":"Promoted","trackingName":"promoted","localStorageName":false,"buttonText":"Learn More","title":"What Are Promoted Posts?","description":"And why am I seeing them?","background":"{STATIC}\/images\/house-cta\/cta-promoted.jpg","url":"\/\/imgur.com\/blog\/?p=6101","newTab":true,"customClass":"pp-banner"}],"global":[{"active":false,"trackingName":"inCaseYouMissedIt","localStorageName":"cta-icymi-2015-07-14","type":"button","subtype":"app","background":"{STATIC}\/images\/house-cta\/cta-icymi.png","url":"\/\/imgur.com\/topic\/Hand_Picked\/","buttonText":"Take me there","title":"Catch up on the awesome posts you might have missed!","description":"Now featured in the Hand Picked topic.","newTab":false,"customClass":"u-pl260 icymi","buttonColor":"#252525","frontPageOnly":true},{"active":true,"trackingName":"mobileApps","localStorageName":"apps1433176979","type":"button","background":"\/\/s.imgur.com\/images\/house-cta\/cta-apps.jpg?1433176979","url":"\/\/imgur.com\/apps","buttonText":"Count me in!","title":"Get the Imgur Mobile App!","description":"Fully Native. Totally Awesome.","newTab":true,"customClass":"u-pl260"},{"active":true,"type":"custom","jsReactClassName":"Newsletter","trackingName":"inCaseYouMissedItForm","localStorageName":"apps1440530195","background":"{STATIC}\/images\/house-cta\/cta-icymi-form.png?1440530195","newTab":true,"customClass":"u-pl200"}]},
                experiments:   {"exp1868":{"active":true,"name":"hamburger-topics","inclusionProbability":1,"expirationDate":"2015-09-30T00:00:00.000Z","variations":[{"name":"hamburger-topics","inclusionProbability":0.5}]}},
                isGalleryAdmin:false,
                ppBlog: '//imgur.com/blog/?p=6101'
            };

            var imgur = Imgur.getInstance();
            imgur.init(_.merge(Imgur.Environment, {
                
                
                beta: {"enabled":true,"inBeta":false,"showInvite":false,"ctaInvite":[],"ctaWelcome":[]},
            }));

                            var uploader = Imgur.Upload.Global.getInstance(Imgur.getInstance()._);

                if(uploader._.el.$uploadGlobal.length > 0) {
                    uploader.bindPasteUpload();
                    uploader.generalInit();
                }
            

            imgur.generalInit();

            $(function() {
                
            });

            
            

            var _widgetFactory = new Imgur.Factory.Widget(Imgur.Environment);

            _widgetFactory.mergeConfig('global', Imgur.Environment);

            
        </script>
                                
    
    
                    <script type="text/javascript">
            (function(widgetFactory) { 
                widgetFactory.mergeConfig('analytics', {
                    isAdmin: false,
                    logPixel: '//p.imgur.com/lumbar.gif'
                });
                widgetFactory.mergeConfig('global', {
                    analyticsAccountId: 'UA-6671908-2'
                });
    
                widgetFactory.mergeConfig('search', {
                    type: 'global', 
                    q: ''
                });

                widgetFactory.produceJafo();

                widgetFactory.startExperiments();

                                    widgetFactory.produceCtaBanner("/gallery/" + "m00tubZ");
                
            })(_widgetFactory);
        </script>
    

    <script type="text/javascript">
    (function(widgetFactory) { 
        widgetFactory.mergeConfig('global', {
            sid: '217dfcc96542c645d0ce901be1ef7f21'
        });

        widgetFactory.mergeConfig('gallery', {
            sid         : '217dfcc96542c645d0ce901be1ef7f21',
            signed      : false,
            baseURL     : decodeURIComponent('%2Fgallery'),
            sort        : 'viral',
            window      : 'day',
            page        : 0,
            isPro       : false,
            maxPage     : 1712,
            showPast    : true,
            searchQuery : '',
            inSearch    : false,
            advSearch   : null,
            bestCaps    : true
        });

        widgetFactory.mergeConfig('groups', {
            groups: {
                
            }
        });

        widgetFactory.mergeConfig('search', {
            
        });
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
        (function(widgetFactory) {
            var group_type = 'base';

            var group = {
                id: '[hot]',
                tags: [
                    
                ]
            }

            widgetFactory.mergeConfig('gallery', {
                section            : 'hot',
                showGalleryNewInfo : false,
                gallery_type       : null,
                tag                : null,
                sponsoredTag       : null,
                tagFollower        : false,
                group_type         : group_type,
                group              : group
            });
        })(_widgetFactory);
    </script>


    <script type="text/javascript">
        if (typeof _widgetFactory !== 'undefined') {
            _widgetFactory.initNotifications({"post":{"postUpvote":[10,50,100,500,1000,5000],"postComment":[10,50,100],"postView":[100000,500000,1000000,5000000],"postEmbed":[1],"postViral":[1]},"comment":{"commentUpvote":[10,100,500,1000,5000,10000],"commentReply":[1],"commentTop7":[1],"commentMention":[1]},"trophy":{"userTrophy":[1]},"account":{"userNotoriety":[-1,399,999,1999,3999,7999,19999,9223372036854775807]},"message":{"message-msg":[1]}}, [{"name":"Forever Alone","reputation":-1},{"name":"Neutral","reputation":399},{"name":"Accepted","reputation":999},{"name":"Liked","reputation":1999},{"name":"Trusted","reputation":3999},{"name":"Idolized","reputation":7999},{"name":"Renowned","reputation":19999},{"name":"Glorious","reputation":9223372036854775807}]);
        }
    </script>

        <script type="text/javascript" src="//s.imgur.com/min/index.js?1441834350"></script>

    <script type="text/javascript" src="//s.imgur.com/min/gallery.js?1441834350"></script>

    <script type="text/javascript">
    (function(widgetFactory) {
        var emitter = new Imgur.Emitter();

        widgetFactory.produceLocationCommand(emitter);

        var galleryFactory = widgetFactory.produceOutsideGalleryFactory();
        var galleryGraph = galleryFactory.produceOutsideGallery(emitter);

        var gallery = galleryGraph.gallery,
            history = galleryGraph.history;

        var outsideTaggingWidget = galleryFactory.produceOutsideTaggingWidget(emitter, galleryGraph);

        gallery.outsideInit();

        Imgur.Upload.Index.getInstance().generalInit();
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
    $(function() {
        if(!/^([^:]+:)\/\/([^.]+\.)*imgur(-dev)?\.com(\/.*)?$/.test(document.referrer)) {
            Imgur.Util.jafoLog({ event: 'galleryDisplay', meta: { gallerySort: 'viral', galleryType: 'hot' }});
        }
    });
    </script>
    
    

            <script type="text/javascript">
        if (typeof _widgetFactory !== 'undefined') {
            _widgetFactory.produceSearch();
        }
        </script>
    

        
    

            <script type="text/javascript">
            Imgur.Util.getGoogleTracker();
            __ga('send', 'pageview');
        </script>

        
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        

        

        
    

        <script type='text/javascript'>
        
        (function(){
            try{
                var msg = '\n      _\n     (_)\n      _ _ __ ___   __ _ _   _ _ __\n     | | \'_ ` _ \\ / _` | | | | \'__|\n     | | | | | | | (_| | |_| | |\n     |_|_| |_| |_|\\__, |\\__,_|_|\n                   __/ |\n                  |___/\n';
                msg += '========================================\nYou opened the console! Know some code,\ndo you? Want to work for one of the best\nstartups around? http://imgur.com/jobs\n========================================\n';
                imcon.log(msg);
            }catch(e){}
        })()
        
    </script>
    

        

</body>
</html>
