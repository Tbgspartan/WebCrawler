



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "773-0316211-2813585";
                var ue_id = "1HE0Y39NPRP2GFS545EP";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        

        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1HE0Y39NPRP2GFS545EP" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-b5f08f0b.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2832270481._CB288261039_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['c'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['509527254080'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-1665889842._CB286426722_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"740ab8595231091cb1b175ea145a6fe177c7f2d1",
"2015-12-07T18%3A02%3A39GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50241;
generic.days_to_midnight = 0.581493079662323;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'c']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=509527254080;ord=509527254080?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=509527254080?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=509527254080?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/holiday-streaming/?ref_=nv_tvv_hldy_0"
>Holiday Streaming</a></li>
                        <li><a href="/falltv/?ref_=nv_tvv_fall_1"
>Fall TV</a></li>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_2"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_3"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_4"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_5"
>Most Popular TV Shows</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_6"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_7"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_3"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=12-07&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59267053/?ref_=nv_nw_tn_1"
> 'Krampus' Scores Strong Opening While 'Mockingjay' Takes First for Third Straight Week
</a><br />
                        <span class="time">23 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59268727/?ref_=nv_nw_tn_2"
> Kennedy Center Honors Fetes Legends with Tunes, Tears and Lasers
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59267029/?ref_=nv_nw_tn_3"
> Bill Murray Is Playing a Dog in Noted Dog Hater Wes Anderson's Next Movie About Dogs
</a><br />
                        <span class="time">23 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0021749/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BNTIzODMwODU2NF5BMl5BanBnXkFtZTcwNzY3NzE5Ng@@._V1._SY315_CR0,0,410,315_.jpg",
            titleYears : "1931",
            rank : 34,
                    headline : "City Lights"
    },
    nameAd : {
            clickThru : "/name/nm2225369/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTQ1NzU2NTkxNl5BMl5BanBnXkFtZTcwNzMxOTUxOQ@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 2,
            headline : "Jennifer Lawrence"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYp_FhkySKDYP3tFT5sr_oqmSxVitwJrELIfKAM5s1whOBVyaPHSL-fPJYdc6FL4-XRt0YKYFcc%0D%0AzG1lKR0phn5PgNq83IOYIqt6YR1P2v557f7ExJJsl8mzfbuYOoYbPZzNH_IrhZyiS1w6-dhmaRn1%0D%0APxCPE08JM1SF011no_4vbmzqPqT0fHGIQcP2a0X7FNCh-Ahf0gyDIFL_TVVKVb2nPw%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYrC1_0TNMfy5P-555bn9VoSpOCk3-av15JFi7-cWjNCYVzVZ4LI5ozmhS_MjjJBv9PZbdq6irC%0D%0AVJYdrOXl-QIsHr_wXpr8X2m1tuyywB9jfJG2cwKsYWIm6eaSJGe7qhmlerecwbJ7s3jX8IYb84De%0D%0ASyXQdwfYK3X5g7dW6ZnBKQj8Z-HUx0vW0nNs7_xZXOPa%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYiM3UDqSivmRnITJi8u62XnZdSUI23C1nrDZxye1aAjBYtmhHFgZhdbgy2mi63ZO5AI-AyMSCN%0D%0AVXtdeBoMRxutnBqUQcv9QaxFpY_JSzi3zA_5HyLBypZgELDxUSxwvbyaIeEm7wkWN1RhcESw3Tn9%0D%0ABfJTA8SH2k143IxlVKX0APeVXUsfOpbyXgsCM6OWuYQy%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=509527254080;ord=509527254080?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=509527254080;ord=509527254080?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3100029465?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3100029465" data-source="bylist" data-id="ls002309697" data-rid="1HE0Y39NPRP2GFS545EP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="A story of lost love, young love, a legendary sword and one last opportunity at redemption." alt="A story of lost love, young love, a legendary sword and one last opportunity at redemption." src="http://ia.media-imdb.com/images/M/MV5BMTA2MzM5NTk4NTBeQTJeQWpwZ15BbWU4MDk0NjgyNDcx._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA2MzM5NTk4NTBeQTJeQWpwZ15BbWU4MDk0NjgyNDcx._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A story of lost love, young love, a legendary sword and one last opportunity at redemption." title="A story of lost love, young love, a legendary sword and one last opportunity at redemption." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A story of lost love, young love, a legendary sword and one last opportunity at redemption." title="A story of lost love, young love, a legendary sword and one last opportunity at redemption." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2652118?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Crouching Tiger, Hidden Dragon: The Sword of Destiny </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2214572569?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2214572569" data-source="bylist" data-id="ls031751600" data-rid="1HE0Y39NPRP2GFS545EP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="When our favorite characters die, we take it pretty hard. We're pouring one out in honor of those who lost their fake lives this year. From Jon Snow's shocking stabbing, to Derek Shephard's dramatic departure, we will remember them all!" alt="When our favorite characters die, we take it pretty hard. We're pouring one out in honor of those who lost their fake lives this year. From Jon Snow's shocking stabbing, to Derek Shephard's dramatic departure, we will remember them all!" src="http://ia.media-imdb.com/images/M/MV5BMjIzNTYxNjI3Ml5BMl5BanBnXkFtZTgwMTM4NjQ0MDE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIzNTYxNjI3Ml5BMl5BanBnXkFtZTgwMTM4NjQ0MDE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="When our favorite characters die, we take it pretty hard. We're pouring one out in honor of those who lost their fake lives this year. From Jon Snow's shocking stabbing, to Derek Shephard's dramatic departure, we will remember them all!" title="When our favorite characters die, we take it pretty hard. We're pouring one out in honor of those who lost their fake lives this year. From Jon Snow's shocking stabbing, to Derek Shephard's dramatic departure, we will remember them all!" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="When our favorite characters die, we take it pretty hard. We're pouring one out in honor of those who lost their fake lives this year. From Jon Snow's shocking stabbing, to Derek Shephard's dramatic departure, we will remember them all!" title="When our favorite characters die, we take it pretty hard. We're pouring one out in honor of those who lost their fake lives this year. From Jon Snow's shocking stabbing, to Derek Shephard's dramatic departure, we will remember them all!" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt5106590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Top 10 Biggest TV Character Deaths </a> </div> </div> <div class="secondary ellipsis"> IMDb's Best of 2015 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3183915545?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3183915545" data-source="bylist" data-id="ls002922459" data-rid="1HE0Y39NPRP2GFS545EP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="A woman asks her ex-lover for help in order to save her outlaw husband from a gang out to kill him." alt="A woman asks her ex-lover for help in order to save her outlaw husband from a gang out to kill him." src="http://ia.media-imdb.com/images/M/MV5BMjA3NDUwNjc2N15BMl5BanBnXkFtZTgwMjY5Njc5NjE@._V1_SY298_CR9,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA3NDUwNjc2N15BMl5BanBnXkFtZTgwMjY5Njc5NjE@._V1_SY298_CR9,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A woman asks her ex-lover for help in order to save her outlaw husband from a gang out to kill him." title="A woman asks her ex-lover for help in order to save her outlaw husband from a gang out to kill him." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A woman asks her ex-lover for help in order to save her outlaw husband from a gang out to kill him." title="A woman asks her ex-lover for help in order to save her outlaw husband from a gang out to kill him." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2140037?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Jane Got a Gun </a> </div> </div> <div class="secondary ellipsis"> International Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2322243302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/best-of/best-of-2015-most-viewed-trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318821082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bo15_tr_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318821082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Best of 2015: Top 10 Most-Viewed Trailers</h3> </a> </span> </span> <p class="blurb">Watch the year's most popular trailers on IMDb, including the trailer for <i><a href="/title/tt1386697/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318821082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bo15_tr_lk1">Suicide Squad</a></i>.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3032068889?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318821082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bo15_tr_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318821082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3032068889" data-source="bylist" data-id="ls031083534" data-rid="1HE0Y39NPRP2GFS545EP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_bo15_tr" data-ref="hm_bo15_tr_i_1"> <img itemprop="image" class="pri_image" title="Suicide Squad (2016)" alt="Suicide Squad (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjMzMTM4MzM1OV5BMl5BanBnXkFtZTgwODAwMzE2NTE@._UX624_CR0,90,624,351_SY351_SX624_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMzMTM4MzM1OV5BMl5BanBnXkFtZTgwODAwMzE2NTE@._UX624_CR0,90,624,351_SY351_SX624_AL_UY702_UX1248_AL_.jpg" /> <img alt="Suicide Squad (2016)" title="Suicide Squad (2016)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Suicide Squad (2016)" title="Suicide Squad (2016)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/best-of/best-of-2015-most-viewed-trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318821082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bo15_tr_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318821082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Watch the year's most-viewed trailers </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59267053?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59267053?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >'Krampus' Scores Strong Opening While 'Mockingjay' Takes First for Third Straight Week</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p>Universal's <a href="/title/tt3850590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Krampus</a> had a wonderful opening as the Christmas-themed horror comedy managed a second place finish, just behind <a href="/title/tt1951266?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">The Hunger Games: Mockingjay - Part 2</a>, which took first place for a third weekend in a row. <a href="/title/tt3076658?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">Creed</a> held strong as it finds itself in a neck-and-neck battle for third place ...                                        <span class="nobr"><a href="/news/ni59267053?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59268727?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Kennedy Center Honors Fetes Legends with Tunes, Tears and Lasers</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59267029?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Bill Murray Is Playing a Dog in Noted Dog Hater Wes Anderson's Next Movie About Dogs</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59266704?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Ethan Hawke Joins Luc Besson's ValÃ©rian, Will Chill Out in Space with Rihanna and Cara Delevingne</a>
    <div class="infobar">
            <span class="text-muted">6 December 2015 4:40 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59267261?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Los Angeles Film Critics Vote (Updated)</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59267053?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59267053?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >'Krampus' Scores Strong Opening While 'Mockingjay' Takes First for Third Straight Week</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p>Universal's <a href="/title/tt3850590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Krampus</a> had a wonderful opening as the Christmas-themed horror comedy managed a second place finish, just behind <a href="/title/tt1951266?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">The Hunger Games: Mockingjay - Part 2</a>, which took first place for a third weekend in a row. <a href="/title/tt3076658?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Creed</a> held strong as it finds itself in a neck-and-neck battle for third place ...                                        <span class="nobr"><a href="/news/ni59267053?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59267125?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Arthouse Audit: 'Youth' Beats 'Macbeth' Among New Oscar Contenders; 'Chi-Raq' Is Spotty</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59267261?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Los Angeles Film Critics Vote (Updated)</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59267317?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Boston Critics Name 'Spotlight' Best Picture</a>
    <div class="infobar">
            <span class="text-muted">6 December 2015 6:45 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59267452?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Can Leonardo DiCaprio, Will Smith and Jennifer Lawrence Still Deliver Big Box Office?</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59268727?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTA0Mjc0NzExNzBeQTJeQWpwZ15BbWU3MDEzMzQ3MDI@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59268727?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Kennedy Center Honors Fetes Legends with Tunes, Tears and Lasers</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p>Washington â Unveiling a variety of touches that broke from longstanding tradition, the Kennedy Center toasted five titans of the performing arts on Sunday in the 38th annual presentation of its Kennedy Center Honors. The careers of director <a href="/name/nm0000184?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">George Lucas</a>, actors Cecily Tyson and <a href="/name/nm0001549?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Rita Moreno</a>, ...                                        <span class="nobr"><a href="/news/ni59268727?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59265832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >President Obama Sets Primetime Speech on San Bernardino for Sunday</a>
    <div class="infobar">
            <span class="text-muted">6 December 2015 2:54 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59268190?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Adele: Live in New York City: 9 Things to Know About Her Lush NBC Concert</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59266909?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >âSaturday Night Liveâ Ratings Slip With Host Ryan Gosling</a>
    <div class="infobar">
            <span class="text-muted">6 December 2015 6:00 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59266645?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >âSeinfeldâ Producer Howard West Has Died</a>
    <div class="infobar">
            <span class="text-muted">6 December 2015 4:16 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59269114?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjA5ODI0NzIzNV5BMl5BanBnXkFtZTcwMzM0NjI2Nw@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59269114?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Stars React to Their 2016 Grammy Award Nominations</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p>For most musicians, a Grammy Award nomination marks a major milestone in their career. Today, musical newcomers and golden statue veterans alike relished in the highly anticipated announcement of the 2016 nominees.Â  Industry pros <a href="/name/nm2357847?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Taylor Swift</a> and <a href="/name/nm5302509?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Kendrick Lamar</a> topped the list with the most ...                                        <span class="nobr"><a href="/news/ni59269114?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59267976?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Holly Woodlawn, Transgender Actress & Subject of âWalk on the Wild Side,â Dies at 69</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59266945?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Former President Jimmy Carter Says His Brain Cancer Is Gone</a>
    <div class="infobar">
            <span class="text-muted">6 December 2015 6:30 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59267117?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Morgan Freeman, Morgan Freeman's Voice, Safe After His Plane Forced to Make Emergency Landing</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59267083?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >âBuffy the Vampire Slayerâ Actor Pleads Guilty After Allegedly Choking Girlfriend</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1948508160/rg1624152832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Judd Apatow" alt="Judd Apatow" src="http://ia.media-imdb.com/images/M/MV5BMjI4NDMwNTE2OV5BMl5BanBnXkFtZTgwNTYyNTA0NzE@._V1_SY201_CR41,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4NDMwNTE2OV5BMl5BanBnXkFtZTgwNTYyNTA0NzE@._V1_SY201_CR41,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1948508160/rg1624152832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Photos We Love: Week of December 4 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2648291328/rg1859033856?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Big (1988)" alt="Big (1988)" src="http://ia.media-imdb.com/images/M/MV5BMTA2MjYwNjY4MDNeQTJeQWpwZ15BbWU3MDQ0NDg1ODQ@._V1_SY201_CR43,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA2MjYwNjY4MDNeQTJeQWpwZ15BbWU3MDQ0NDg1ODQ@._V1_SY201_CR43,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2648291328/rg1859033856?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Remembering Robert Loggia: 1930 - 2015 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/imdbpicks/point-break?imageid=rm3058884608&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Point Break - CaÃ§adores de EmoÃ§Ãµes (2015)" alt="Point Break - CaÃ§adores de EmoÃ§Ãµes (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjQ1Mjc2NzAyOV5BMl5BanBnXkFtZTgwODI3NTkzNzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ1Mjc2NzAyOV5BMl5BanBnXkFtZTgwODI3NTkzNzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/imdbpicks/point-break?imageid=rm3058884608&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902662&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <i>Point Break</i> Movie Photos </a> </div> </div> </div> </div> </div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=12-7&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0115161?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Emily Browning" alt="Emily Browning" src="http://ia.media-imdb.com/images/M/MV5BMTI5NzgzMjgwMF5BMl5BanBnXkFtZTcwODYzNDc1Mw@@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5NzgzMjgwMF5BMl5BanBnXkFtZTcwODYzNDc1Mw@@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0115161?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Emily Browning</a> (27) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0396558?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Nicholas Hoult" alt="Nicholas Hoult" src="http://ia.media-imdb.com/images/M/MV5BMTUxMTYxNzMzNF5BMl5BanBnXkFtZTcwMTU0NTcxOQ@@._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUxMTYxNzMzNF5BMl5BanBnXkFtZTcwMTU0NTcxOQ@@._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0396558?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Nicholas Hoult</a> (26) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1358539?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jennifer Carpenter" alt="Jennifer Carpenter" src="http://ia.media-imdb.com/images/M/MV5BMTA3Mzk2NTk0MDReQTJeQWpwZ15BbWU3MDQ1MDQyNzY@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA3Mzk2NTk0MDReQTJeQWpwZ15BbWU3MDQ1MDQyNzY@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1358539?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Jennifer Carpenter</a> (36) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001367?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="C. Thomas Howell" alt="C. Thomas Howell" src="http://ia.media-imdb.com/images/M/MV5BMTUwMzEwMjQ0Nl5BMl5BanBnXkFtZTgwNDY1NjQwNTE@._V1_SY166_CR93,0,116,166_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwMzEwMjQ0Nl5BMl5BanBnXkFtZTgwNDY1NjQwNTE@._V1_SY166_CR93,0,116,166_AL_UY332_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001367?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">C. Thomas Howell</a> (49) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0032375?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Shiri Appleby" alt="Shiri Appleby" src="http://ia.media-imdb.com/images/M/MV5BMjE4OTM1NjgyN15BMl5BanBnXkFtZTgwNTM4OTMwMTE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE4OTM1NjgyN15BMl5BanBnXkFtZTgwNTM4OTMwMTE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0032375?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Shiri Appleby</a> (37) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=12-7&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/holiday-streaming/picks-tv-other-worlds/ls016387332?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318809302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hs_pks_ow_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318809302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Holiday Streaming Picks: TV's Other Worlds</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/holiday-streaming/picks-tv-other-worlds/ls016387332?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318809302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hs_pks_ow_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318809302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage#image1" > <img itemprop="image" class="pri_image" title="&quot;The Man in the High Castle&quot;" alt="&quot;The Man in the High Castle&quot;" src="http://ia.media-imdb.com/images/M/MV5BNTEzMjQ4MzY2M15BMl5BanBnXkFtZTgwODQ4MTMxNzE@._V1_SY307_CR68,0,307,307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTEzMjQ4MzY2M15BMl5BanBnXkFtZTgwODQ4MTMxNzE@._V1_SY307_CR68,0,307,307_AL_UY614_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/holiday-streaming/picks-tv-other-worlds/ls016387332?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318809302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hs_pks_ow_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318809302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage#image2" > <img itemprop="image" class="pri_image" title="&quot;Vikings&quot;" alt="&quot;Vikings&quot;" src="http://ia.media-imdb.com/images/M/MV5BMTY4OTQyNzUwOV5BMl5BanBnXkFtZTgwNzk2NTc4NDE@._V1_SY307_CR77,0,307,307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4OTQyNzUwOV5BMl5BanBnXkFtZTgwNzk2NTc4NDE@._V1_SY307_CR77,0,307,307_AL_UY614_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Unplug and unwind: Step out of the mundane and consider other existences, parallel universes, and landscapes that never were or could ever be.</p> <p class="seemore"> <a href="/holiday-streaming/picks-tv-other-worlds/ls016387332?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318809302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hs_pks_ow_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318809302&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > View our holiday streaming picks </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Indie Focus: Exclusive Clip From 'A Royal Night Out'</h3> </span> </span> <p class="blurb">A young Queen Elizabeth (<a href="/name/nm0300589/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2319117882&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk1">Sarah Gadon</a>) and Princess Margaret (<a href="/name/nm2525790/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2319117882&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk2">Bel Powley</a>) ask permission to celebrate the end of World War II with ordinary British citizens in this exclusive clip from the new comedy, <i><a href="/title/tt1837562?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2319117882&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk3">A Royal Night Out</a></i>.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1837562/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2319117882&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2319117882&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="A Royal Night Out (2015)" alt="A Royal Night Out (2015)" src="http://ia.media-imdb.com/images/M/MV5BNDg4MjE5OTUyM15BMl5BanBnXkFtZTgwMjM4NzkyNzE@._V1._CR28,31,698,1031_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDg4MjE5OTUyM15BMl5BanBnXkFtZTgwMjM4NzkyNzE@._V1._CR28,31,698,1031_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1574220313?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2319117882&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2319117882&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1574220313" data-rid="1HE0Y39NPRP2GFS545EP" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ1Mzk1NzQ2Nl5BMl5BanBnXkFtZTgwMzczNTA0NzE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1Mzk1NzQ2Nl5BMl5BanBnXkFtZTgwMzczNTA0NzE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt1976009/trivia?item=tr2693414&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1976009?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Victor Frankenstein (2015)" alt="Victor Frankenstein (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTc2Mjk0MTM0NF5BMl5BanBnXkFtZTgwNjgyOTg1NjE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc2Mjk0MTM0NF5BMl5BanBnXkFtZTgwNjgyOTg1NjE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt1976009?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Victor Frankenstein</a></strong> <p class="blurb">The last major Frankenstein film was <a href="/title/tt0109836?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Mary Shelley's Frankenstein</a> (1994), which featured <a href="/name/nm0000110?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk2">Kenneth Branagh</a>, <a href="/name/nm0000092?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk3">John Cleese</a>, <a href="/name/nm0362735?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk4">Robert Hardy</a> and <a href="/name/nm0000307?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk5">Helena Bonham Carter</a>, all of whom appeared with <a href="/name/nm0705356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk6">Daniel Radcliffe</a> in the Harry Potter films.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt1976009/trivia?item=tr2693414&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-25"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;zjpX1MC2XH8&quot;}">
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/zjpX1MC2XH8/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: 10 Actors Who Have Auditioned to Play Han Solo</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/zjpX1MC2XH8/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Neighbors (2014)" alt="Neighbors (2014)" src="http://ia.media-imdb.com/images/M/MV5BMjE2MDg3MDU3Ml5BMl5BanBnXkFtZTgwMDExMDA3MTE@._V1_SY207_CR9,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE2MDg3MDU3Ml5BMl5BanBnXkFtZTgwMDExMDA3MTE@._V1_SY207_CR9,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/zjpX1MC2XH8/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Oscars (2015)" alt="The Oscars (2015)" src="http://ia.media-imdb.com/images/M/MV5BODI5NjE3MTcyOF5BMl5BanBnXkFtZTgwNjgyNTg1NDE@._V1_SY207_CR11,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODI5NjE3MTcyOF5BMl5BanBnXkFtZTgwNjgyNTg1NDE@._V1_SY207_CR11,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/zjpX1MC2XH8/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Oscars (2015)" alt="The Oscars (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTc3MTgyMTQ4OV5BMl5BanBnXkFtZTgwMDY4Mjg1NDE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc3MTgyMTQ4OV5BMl5BanBnXkFtZTgwMDY4Mjg1NDE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/zjpX1MC2XH8/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Kings of Summer (2013)" alt="The Kings of Summer (2013)" src="http://ia.media-imdb.com/images/M/MV5BMTg5OTY2NTMzNF5BMl5BanBnXkFtZTcwMTA2MTk1OQ@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg5OTY2NTMzNF5BMl5BanBnXkFtZTcwMTA2MTk1OQ@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/zjpX1MC2XH8/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Chandler Riggs" alt="Chandler Riggs" src="http://ia.media-imdb.com/images/M/MV5BNzYwMjE0MDUxNF5BMl5BanBnXkFtZTgwNDg0NTYzMDE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzYwMjE0MDUxNF5BMl5BanBnXkFtZTgwNDg0NTYzMDE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">So far we know that these ten actors have auditioned to play "young Han Solo" -- which guy would you like to see land the role?</p> <p class="seemore"> <a href="/poll/zjpX1MC2XH8/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321903042&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTUzNTA1NzE0MV5BMl5BanBnXkFtZTgwNjA0NjMzNzE@._SY307_CR40,0,307,307_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzNTA1NzE0MV5BMl5BanBnXkFtZTgwNjA0NjMzNzE@._SY307_CR40,0,307,307_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjE2NzU0MTYwMl5BMl5BanBnXkFtZTgwNjg1NDczNzE@._SX400_CR92,0,307,307_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE2NzU0MTYwMl5BMl5BanBnXkFtZTgwNjg1NDczNzE@._SX400_CR92,0,307,307_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzA5OTc0Mzk2OV5BMl5BanBnXkFtZTgwNjE5NjAzNzE@._SX307_CR0,0,307,307_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzA5OTc0Mzk2OV5BMl5BanBnXkFtZTgwNjE5NjAzNzE@._SX307_CR0,0,307,307_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk4OTA2MzYxNl5BMl5BanBnXkFtZTgwOTM1NDAzNzE@._SX417_CR44,22,307,307_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk4OTA2MzYxNl5BMl5BanBnXkFtZTgwOTM1NDAzNzE@._SX417_CR44,22,307,307_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss our one-on-one interviews with <a href="/name/nm0249046/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_lk1">Lisa Edelstein</a>, <a href="/name/nm0000207/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_lk2">Christina Ricci</a>, <a href="/name/nm1157048/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_lk3">Zachary Levi</a>, or <a href="/name/nm0002546/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_lk4">Mena Suvari</a>? Watch the archived online chats and other interviews with your favorite stars.</p> <p class="seemore"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2318787922&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Watch all our one-on-one interviews </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=509527254080;ord=509527254080?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=509527254080?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=509527254080?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1390411"></div> <div class="title"> <a href="/title/tt1390411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> In the Heart of the Sea </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3569230"></div> <div class="title"> <a href="/title/tt3569230?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Legend </a> <span class="secondary-text"></span> </div> <div class="action"> Nationwide Expansion </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1596363"></div> <div class="title"> <a href="/title/tt1596363?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> The Big Short </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3722070"></div> <div class="title"> <a href="/title/tt3722070?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> The Lady in the Van </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3534282"></div> <div class="title"> <a href="/title/tt3534282?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Don Verdean </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902482&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2321902502&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1951266"></div> <div class="title"> <a href="/title/tt1951266?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Hunger Games: Mockingjay - Part 2 </a> <span class="secondary-text">$18.6M</span> </div> <div class="action"> <a href="/showtimes/title/tt1951266?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3850590"></div> <div class="title"> <a href="/title/tt3850590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Krampus </a> <span class="secondary-text">$16.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3076658"></div> <div class="title"> <a href="/title/tt3076658?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Creed </a> <span class="secondary-text">$15.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1979388"></div> <div class="title"> <a href="/title/tt1979388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> The Good Dinosaur </a> <span class="secondary-text">$15.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt1979388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2379713"></div> <div class="title"> <a href="/title/tt2379713?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Spectre </a> <span class="secondary-text">$5.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2379713?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3808342"></div> <div class="title"> <a href="/title/tt3808342?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Le fils de Saul </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt3808342?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Star Wars: The Force Awakens </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1850457"></div> <div class="title"> <a href="/title/tt1850457?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Sisters </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2974918"></div> <div class="title"> <a href="/title/tt2974918?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Alvin and the Chipmunks: The Road Chip </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4382872"></div> <div class="title"> <a href="/title/tt4382872?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Extraction </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/best-of/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2316782282&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bo15_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2316782282&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb's Best of 2015</h3> </a> </span> </span> <p class="blurb">Find out which stars, movies, and TV shows made our Top 10 lists. Plus, watch original videos, browse photos, and more.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/best-of/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2316782282&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bo15_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2316782282&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNTQyOTQ5NjM4N15BMl5BanBnXkFtZTgwNDM3MzMwNzE@._SX307_CR0,40,307,230_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTQyOTQ5NjM4N15BMl5BanBnXkFtZTgwNDM3MzMwNzE@._SX307_CR0,40,307,230_UY1228_UX1228_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/best-of/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2316782282&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bo15_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2316782282&pf_rd_r=1HE0Y39NPRP2GFS545EP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Visit our Best of 2015 section </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYlNYgxVds98Kf3cFvh0mCJZ3g7S640AkPTfN_MlOsYOzaQOppNScFEZ47tOP4jJ-xXhIB5G0_0%0D%0A8xotZdIMfH8XPEtIXfPykHCkIJZbb8BunC1Nz_wHWVgp_kkG63AgXw7XQ05U4QNXwPLj_3H3BNC9%0D%0ArWtY4QhfEGPuuYS_z656JRiYWlgnwOCFGroZINPI0M8o1786-xH_TtpUUDl8MrTpf2eM0woR-0eN%0D%0AVbJ42gZWHp4%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYvmvqn8Uhn-hhHE1d5Gsl0SOS3RIwdN-GS-2MKkRxyVHTLZ4vD521XGI4kPMMCsg3kocR4oTAL%0D%0A4OJdRwMkgegUOtNDARAvq3nH--gW7zO6GJj3XYFfjvgb6Mh0up533sLj741Der1T9q6bw9yTkKgv%0D%0AUH_2fWGapf-2d_zjxJ4tiN7vuO5XPYAMaYvtwQLtSRmvfvJBRROy3MGIEXLJFZLLbg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYs7IWFvhRjTu6QE6wN8Xz_0-1OaKmAKo24myuHMlRqfJxPe1JbqnLd1z8TywJoeBh2JZZFgvqa%0D%0A1W6p4lg-yHLo67CnTANBNUurSSyWFTHcIMH10QD1AoJeO6TLK_2NLBmt6H2obXBaltgz-l-abS6Q%0D%0AUhSOvgOs9cPFjOsoldkEx2RVqeeznXdy7Lzfe1dXLaAmFPACaLT2C-3regJdCfluG33rom538Qj5%0D%0A9wzycq-ti7GLE3D0I83kmyG5K9iuSkh8JZITdfIdH88QcEp6KziLQJuk9zNcPQAWv38NFKHaC7yM%0D%0AmOW4VxN-4m7bzpL6u_CaaVqIvgfFi8FZkcU1vBh-tqYtwisUvv0UVWJgrf_R8RggHvVEkzot7uzg%0D%0AqV8jqv8rj6sOqD1a9kCzb0dk1zkGIQ%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYkX9VSrYGTkGqDz6sdljBX-wCQQ751UcmVlwEvywkAuhIG9a7VNImNbZI4tAao-ULqZgWvAtt4%0D%0AzN7H7ZQ4hL61w01tNjhIFll0fpCyPExftoy4oM3PMCry4LHm3Z_Gnru91Ewkbx_GDr2TmVZLeNbA%0D%0AGZEqC94Dg8jzWYucS0y9oEcl_XUOMli9vyQuwDxhEg9dQcuTRJxaipVGpxhfZZPSxg%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYrQFUHzygodgyf4uyJNNUnPEKwOu5I7R-FjEDWnvSrOimTLVvvjbyRMxWiO_Wlagk8t0lRNHsS%0D%0AbFpOAeqgRRNdU1_m7Tt72bTFli2j9r0636fRRmB2WXC7ZNvAuD89yFs3tOk86inZo_Pt_VC3bSD9%0D%0A7juLfWvzfcqXC4KHkfByVVF4oNUjyuwEy9sN8D3NQfMO%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYpRnwx8JP6g2pjV_heteeLiuVvQv4BS7VnUgnaNv95XoMXNgqSCxp4mrdPClLuh7g7N2W45XDV%0D%0ALhZmguzDZLliqGCAPKGQhKWdX0pFkmpgYF_C_dN3Xk-RBQIQXFGvJ-0d86WQpMNapvuqDQTCT1oA%0D%0AOvGpIyRMoXBvNzpu0-z0eA3-y6uArENLGX7mncpyspap%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYqsBqq6D7WeWEZU7snhkpvtmmIwT0CkEFoOGy_aqRQ46f9G3E3pZNIkKyAGCBUBvkXCT4KIxHu%0D%0AuUysnp8QaVpF-qRFTQ-hioQH1MvyrwfJQQwN0aa72s4oj73pOQoUsZzGeEq1rPY1LNISz98iYMXr%0D%0AAi-DG60KOeXWxOi29mcXr3tfrmfjMG_SxzXGIEJPTG1oAeIHr-8_F_HQ-g-I95L76w%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYiEsnFGxInF_LKusBsdhu_vs4od1a9qv5ACnOfQaWy-SbrRDkDz6AH-of_6HrG1GMaJ9NbuG-Q%0D%0AMFIa3fftMzPO_BITCDbofdoxy0Nm9Bk9Rc4kj_7gJPbbd8WESvpVBTICy6g1OrK80LmptW3EE8ok%0D%0AacPPRxhypZyTOiV9W-EN4YvL_1eMPrnjXOOqfXuWWkll%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYs36EMTazsbiYMY5JVgYLFd3T59awLQQq579Bc3zc0MvJ-l8gDWYSREhRAUfrz-bj2Pv-qplhG%0D%0ABCJEqtO0WgovTk0cl-GdMm-454appNhm9XFph1MWlV3qgKT9RdxMZqCiBrqfWC0zgIK0eTl4wLem%0D%0AfYnGqL54MUioM-ngcl3xAE1teqkTylrOYAvDJUh-B5Kd3hdxNuiWlY1Dto8ExkkvTIZ-nCnB7m1b%0D%0AsIwxrsdZDDiuuakqQPCoKPTk4vvpO_Ul%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYstjQXWZSCMXmCjKULyNa-I2h0Ui0PV5OLS9xdNREdeqDPJirsr2Sq3YdLffRl1gSeY1I7sTZr%0D%0ALon1CXXXpmYiB8A6oUwwbRfsOVOmaIwuS2ByhU9fkb3h7zzU0XY3hBEoQ9-04ZXRumln5NXXsuvE%0D%0ASwq8lJYNl9EXIkDnDEzcx89lYizUsmsNhx38O-6TuojDKyZGZuYRm2QL3wa3ifE6De73eNTNNB66%0D%0AK8mHm6SHiDY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYmkwWzvdog93GHm7PkTTQHz-49MDzmo1tLQiQg_ZpwI-y5t6fGluifh9wwxO1Sv7xKoN-9rSMY%0D%0AO8KM-8m1ksd9ZHOeKmBNPw1V7iret7sozsKwyea_kakOxTJ6Ln4orlZb6tc992MZ-vrVm35H2gtb%0D%0A6_vX1elS-lY1NnU-Ci0XvzFDhmvjihsr9X3_tulitXmHYwdTyLogjqmMICFb7kBw9UsiwbN-3pcY%0D%0A_2IDFX8rdnA%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYprK7truHhXzp0PdjDPT3PLr5Y7WnMJ0XiyseFQIu_dlymh-xEbxwCaaR6nk0ryY4rJV9kJADK%0D%0ABT7M_GL937kQkjuZlv6hAEwhWDTza7Zo1ZjHnP2eR1SOpqaFPPi-TQVxyQ4sYQHYByPTCb2DIzid%0D%0AYqXq-SOxczFMXli-ppXorHJJqOdHVpEGnF3FRUjwau_v6X-e09Q25UBLqx1XgYulZZ4nEneVX4jf%0D%0A18zK1APzfrY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYt3cp94sYzEbuPhPrKPji6c7hQNeK_jV2W989TeGu2zBn_p34cFe4xYzCcYGoJuIbSUlmdJhXF%0D%0AobVqjAkc07zF-cw_3gKdHrssdWQFe610oIUR4VQG9LLQ7kHPsXx6l4ZqgoU98wolPCwP0UjwfL6_%0D%0A4UscKlZVXGiAaYTH2p1_vQMUtL0fswgYl-uNnsM-ozjghJoeFRkqXr7WPmijNs8zhgqmch3A_7Vt%0D%0ARXMNPW_Li1E%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYr6YS4y-rf7dfLPgF9-WuCaCZyxOSKqnyXkExEuKjEixFHX8h2cPzU7NdZg-dHoTSFVhR93YeS%0D%0A_aLMNBaYxwlncQDheyG4GRJAKLLQvIj_4S1LTVGm1SS41-xgGt-R_QjJN5lKXwLwk6utUX_o1aGD%0D%0AKAcDUoexuSZgJgkPczT8UZzSiSHHARJae2BVbyX6jzz4CE9dyNpKNsRv0GwQjbvI_E_CANcgCNJO%0D%0AtUK3xkGmRco%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYrEPMv5Qs9o15lTAWrX3L7Jbug6CfMMFjHiYtu51mKpguoobVP0g8nh09Ufer3uHQsiDHbOSQJ%0D%0AV8V8Fi1hzFWDPVggcYmIxj7UXJdYsETLDkwdoliyn2Iu2OJ8nzIpZSsfaOm9-YP0n0_0bd7l80WL%0D%0A4jrDgwACKBS8YcvIu_a6TmJCDKE35CGnwJz5UzkHk7LYKXtIUKhHMH0q5imaBj72Zw16w_TYzsY7%0D%0AFdYYMOLvpnBlJb78ubL9D2JIQGmIOv52%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYvtdG2oikecv8RBazInGVxSMOHsesfDHrjCovlWXZG6YyNsy3eL7J_H3a92_yaUr1tuxoKM56Z%0D%0ApNujXe1yQN8uzgokxntDw1AcHanqMGaZL3pI8hg_J2WUfFMHCjPKWbQPgCEeTX2HXpnN4ug95N_W%0D%0AbCso9rLJ9wqFaNP4iDx3gO4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYpEBESFe1mWk3GiE-kCTCIH6Tp-nJGxe4UhjQgo5JHPFigsrihvN2f32YRKHvXOcati7MtmZd1%0D%0AsnO3nhTFATd7Bu5O3h5FDcuIrwA0UtgBA9vsisU-rhLUiIzXA-P7DVMv5cyA0wiOPC392vuceL2J%0D%0ApyBjgIUftq_r9XBjSLnTwok%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-864502379._CB288313243_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2028687744._CB289323184_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101053264783901892f94288afbd87ed30801e92d57b2c3f1c944d730567ee94c40",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=509527254080"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=509527254080&ord=509527254080";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="329"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
