



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "747-5415549-2025782";
                var ue_id = "1YT9009K5PQN6Y0EJ86K";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1YT9009K5PQN6Y0EJ86K" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-e94ac15f.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-903233829._CB298432192_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3796876714._CB298540748_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
!function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c?c:a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";a(2)},{2:2}],2:[function(a,b,c){"use strict";!function(){var a,b,c=function(a){return"[object Array]"===Object.prototype.toString.call(a)},d=function(a,b){for(var c=0;c<a.length;c++)c in a&&b.call(null,a[c],c)},e=[],f=!1,g=!1,h=function(){var a=[],b=[],c={};return d(e,function(e){var f="";d(e.dartsite.split("/"),function(b){""!==b&&(b in c||(c[b]=a.length,a.push(b)),f+="/"+c[b])}),b.push(f)}),{iu_parts:a,enc_prev_ius:b}},i=function(){var a=[];return d(e,function(b){var c=[];d(b.sizes,function(a){c.push(a.join("x"))}),a.push(c.join("|"))}),a},j=function(){var a=[];return d(e,function(b){a.push(k(b.targeting))}),a.join("|")},k=function(a,b){var c,d=[];for(var e in a){c=[];for(var f=0;f<a[e].length;f++)c.push(encodeURIComponent(a[e][f]));b?d.push(e+"="+encodeURIComponent(c.join(","))):d.push(e+"="+c.join(","))}return d.join("&")},l=function(){var a=+new Date;g||document.readyState&&"loaded"!==document.readyState||(g=!0,f&&imdbads.cmd.push(function(){for(var b=0;b<e.length;b++)generic.monitoring.record_metric(e[b].name+".fail",csm.duration(a))}))};window.tinygpt={define_slot:function(a,b,c,d){e.push({dartsite:a.replace(/\/$/,""),sizes:b,name:c,targeting:d})},set_targeting:function(b){a=b},callback:function(a){for(var c,d,f={},g=+new Date,h=0;h<e.length;h++)c=e[h].dartsite,d=e[h].name,a[h][c]?f[d]=a[h][c]:window.console&&console.error&&console.error("Unable to correlate GPT response for "+d);imdbads.cmd.push(function(){for(var a=0;a<e.length;a++)ad_utils.slot_events.trigger(e[a].name,"request",{timestamp:b}),ad_utils.slot_events.trigger(e[a].name,"tagdeliver",{timestamp:g});ad_utils.gpt.handle_response(f)})},send:function(){var d,g,m=[],n=function(a,b){c(b)&&(b=b.join(",")),b&&m.push(a+"="+encodeURIComponent(""+b))};if(0===e.length)return void tinygpt.callback({});n("gdfp_req","1"),n("correlator",Math.floor(4503599627370496*Math.random())),n("output","json_html"),n("callback","tinygpt.callback"),n("impl","fifs"),n("json_a","1");var o=h();n("iu_parts",o.iu_parts),n("enc_prev_ius",o.enc_prev_ius),n("prev_iu_szs",i()),n("prev_scp",j()),n("cust_params",k(a,!0)),d=document.createElement("script"),g=document.getElementsByTagName("script")[0],d.async=!0,d.type="text/javascript",d.src="http://pubads.g.doubleclick.net/gampad/ads?"+m.join("&"),d.id="tinygpt",d.onload=d.onerror=d.onreadystatechange=l,f=!0,g.parentNode.insertBefore(d,g),b=+new Date}}}()},{}]},{},[1]);</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['e'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['109092592345'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4261578659._CB296130991_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"31eb0fa951b613772e14f433784f272c3fa38120",
"2016-04-04T17%3A03%3A28GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50192;
generic.days_to_midnight = 0.5809259414672852;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3230827532._CB299577821_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'e']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=109092592345;ord=109092592345?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=109092592345?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=109092592345?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                        <li><a href="/youtube-originals/?ref_=nv_sf_yto_5"
>YouTube Originals</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=04-04&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_3"
>Sundance</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_6"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_7"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_8"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_9"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_10"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_11"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59688212/?ref_=nv_nw_tn_1"
> âDoctor Whoâ Spinoff Unveils Cast
</a><br />
                        <span class="time">2 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59686611/?ref_=nv_nw_tn_2"
> 'Batman v Superman' Drops Big While Crossing $680 Million Worldwide
</a><br />
                        <span class="time">3 April 2016 4:31 PM, UTC</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59687271/?ref_=nv_nw_tn_3"
> âThe Walking Deadâ Finale: Negan Makes His Mark in Bloody Debut (Spoilers)
</a><br />
                        <span class="time">14 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0167260/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjE1NzI1ODk3NF5BMl5BanBnXkFtZTcwNjQ2MTk2Mw@@._V1._SY315_CR0,0,410,315_.jpg",
            titleYears : "2003",
            rank : 8,
                    headline : "The Lord of the Rings: The Return of the King"
    },
    nameAd : {
            clickThru : "/name/nm0372176/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTQxMzk3MTM1NF5BMl5BanBnXkFtZTcwMDMwNTI3Ng@@._V1._SX750_CR230,10,250,315_.jpg",
            rank : 135,
            headline : "Lena Headey"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYjV6KI-rXHdBhGNoqmrthv-XnXuOw5Gt2y1S-X_peSYBpfUWauBjeLQbszZDY7R2hi0mLUreW3%0D%0AEDW9mJ_up63Kzk8vXSohc2YM-Oi94jLxFuZ98A9MKZj6AXpBWvehojPnRLcyZVjht62s5cXeWJwv%0D%0AWXPshGFKXIzunf4AxktA1tlsLyGm0-0dvT9p2WK7AqKgVCHLQ-3PrPGLviJu3XMsbQ%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYqj0Pr-SMqexaLPxzZd2abDl5Fx1qs_k472UYLN2xWTYqqSUMvq5S3g712OAzFWKje1Yzm8Iy3%0D%0AFDoTdhdwJgt8dUe4k1PF2fCqf0axjVQgn2eJRRCO-6iLXqkqyX94MdpwLENlsiiHHoQaHsEL59Io%0D%0Ap8fYr3x9K3S_o8-qbc9pGJU9J-PFm_2tCE-bFB2IZiBJ%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYgRD8VSJ_Z0sqYEv04PoBTXHugX8dGaSjIQHjGXIj8JGW5mupLv2ktgILz87CYR3O195jClAgR%0D%0AaOFb0VPdEDFimwgNOeXJJ6QZo4dp1tkdAtDpB2SLZmNSbQxHE847vDxjMilq1kvW_OfFCUg3Kz_y%0D%0AIfMSwxBtyXDuEnhGgmAcNC_PdUgIBJxHzy0Rpn73fQrL%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=109092592345;ord=109092592345?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=109092592345;ord=109092592345?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3931551001?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457442742&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1" data-video="vi3931551001" data-source="bylist" data-id="ls036992962" data-rid="1YT9009K5PQN6Y0EJ86K" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="&quot;The Walking Dead&quot; fans have finally gotten a glimpse of Negan, played by veteran actor Jeffrey Dean Morgan. What roles has Jeffrey played in the past that prepared him for this year's most anticipated villain?" alt="&quot;The Walking Dead&quot; fans have finally gotten a glimpse of Negan, played by veteran actor Jeffrey Dean Morgan. What roles has Jeffrey played in the past that prepared him for this year's most anticipated villain?" src="http://ia.media-imdb.com/images/M/MV5BNTkzMzA0MjAwMl5BMl5BanBnXkFtZTgwNjI3MTM1ODE@._V1_SY298_CR123,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTkzMzA0MjAwMl5BMl5BanBnXkFtZTgwNjI3MTM1ODE@._V1_SY298_CR123,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="&quot;The Walking Dead&quot; fans have finally gotten a glimpse of Negan, played by veteran actor Jeffrey Dean Morgan. What roles has Jeffrey played in the past that prepared him for this year's most anticipated villain?" title="&quot;The Walking Dead&quot; fans have finally gotten a glimpse of Negan, played by veteran actor Jeffrey Dean Morgan. What roles has Jeffrey played in the past that prepared him for this year's most anticipated villain?" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="&quot;The Walking Dead&quot; fans have finally gotten a glimpse of Negan, played by veteran actor Jeffrey Dean Morgan. What roles has Jeffrey played in the past that prepared him for this year's most anticipated villain?" title="&quot;The Walking Dead&quot; fans have finally gotten a glimpse of Negan, played by veteran actor Jeffrey Dean Morgan. What roles has Jeffrey played in the past that prepared him for this year's most anticipated villain?" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/name/nm0604742/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457442742&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1" > Jeffrey Dean Morgan </a> </div> </div> <div class="secondary ellipsis"> "No Small Parts" IMDb Exclusive </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1901573401?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457442742&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2" data-video="vi1901573401" data-source="bylist" data-id="ls002309697" data-rid="1YT9009K5PQN6Y0EJ86K" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Hank (Paul Dano) is stranded on a deserted island, having given up all hope of ever making it home again. But one day everything changes when a corpse named Manny (Daniel Radcliffe) washes up on shore; the two become fast friends, and ultimately go on an adventure that will bring Hank back to the woman of his dreams." alt="Hank (Paul Dano) is stranded on a deserted island, having given up all hope of ever making it home again. But one day everything changes when a corpse named Manny (Daniel Radcliffe) washes up on shore; the two become fast friends, and ultimately go on an adventure that will bring Hank back to the woman of his dreams." src="http://ia.media-imdb.com/images/M/MV5BMTgwMzcwNDkyOF5BMl5BanBnXkFtZTgwODgyMDM1ODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgwMzcwNDkyOF5BMl5BanBnXkFtZTgwODgyMDM1ODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Hank (Paul Dano) is stranded on a deserted island, having given up all hope of ever making it home again. But one day everything changes when a corpse named Manny (Daniel Radcliffe) washes up on shore; the two become fast friends, and ultimately go on an adventure that will bring Hank back to the woman of his dreams." title="Hank (Paul Dano) is stranded on a deserted island, having given up all hope of ever making it home again. But one day everything changes when a corpse named Manny (Daniel Radcliffe) washes up on shore; the two become fast friends, and ultimately go on an adventure that will bring Hank back to the woman of his dreams." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Hank (Paul Dano) is stranded on a deserted island, having given up all hope of ever making it home again. But one day everything changes when a corpse named Manny (Daniel Radcliffe) washes up on shore; the two become fast friends, and ultimately go on an adventure that will bring Hank back to the woman of his dreams." title="Hank (Paul Dano) is stranded on a deserted island, having given up all hope of ever making it home again. But one day everything changes when a corpse named Manny (Daniel Radcliffe) washes up on shore; the two become fast friends, and ultimately go on an adventure that will bring Hank back to the woman of his dreams." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4034354/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457442742&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2" > Swiss Army Man </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2522330393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457442742&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3" data-video="vi2522330393" data-source="bylist" data-id="ls002653141" data-rid="1YT9009K5PQN6Y0EJ86K" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="See the latest footage from 'X-Men: Apocalypse.'" alt="See the latest footage from 'X-Men: Apocalypse.'" src="http://ia.media-imdb.com/images/M/MV5BMTY0MDY0NjExN15BMl5BanBnXkFtZTgwOTU3OTYyODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0MDY0NjExN15BMl5BanBnXkFtZTgwOTU3OTYyODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="See the latest footage from 'X-Men: Apocalypse.'" title="See the latest footage from 'X-Men: Apocalypse.'" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="See the latest footage from 'X-Men: Apocalypse.'" title="See the latest footage from 'X-Men: Apocalypse.'" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3385516/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457442742&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3" > X-Men: Apocalypse </a> </div> </div> <div class="secondary ellipsis"> "The Four Horsemen" </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457442742&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','HeroWidget',{wb:1});}
                if(typeof uet === 'function'){uet("cf");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Negan Cliffhanger: What's Next for "The Walking Dead"?</h3> </span> </span> <p class="blurb">We serve up questions raised by the chilling season finale of <a href="/title/tt1520211/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457436122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_lk1">AMC's hit series</a>. <i>You've been warned: Spoilers ahead!</i> Plus, check out photos from Season 6.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/negan-cliffhanger-walking-dead-burning-questions-season-7/ls032955011?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457436122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTUzNTkwOTIyM15BMl5BanBnXkFtZTgwMjI3MTM1ODE@._UX1200_CR180,200,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzNTkwOTIyM15BMl5BanBnXkFtZTgwMjI3MTM1ODE@._UX1200_CR180,200,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/negan-cliffhanger-walking-dead-burning-questions-season-7/ls032955011?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457436122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_cap_pri_1" > Negan Cliffhanger: Burning Questions for Season 7 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/the-walking-dead-photos/rg2227870464?imageid=rm3356630784&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457436122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_i_2" > <img itemprop="image" class="pri_image" title="Still of Michael Cudlitz, Andrew Lincoln and Sonequa Martin-Green in The Walking Dead (2010)" alt="Still of Michael Cudlitz, Andrew Lincoln and Sonequa Martin-Green in The Walking Dead (2010)" src="http://ia.media-imdb.com/images/M/MV5BMTg0NTA0OTkyOV5BMl5BanBnXkFtZTgwNjE3MTM1ODE@._V1_SY230_CR20,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0NTA0OTkyOV5BMl5BanBnXkFtZTgwNjE3MTM1ODE@._V1_SY230_CR20,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/the-walking-dead-photos/rg2227870464?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457436122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_cap_pri_2" > Photos From Season 6 </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
                if(typeof uet === 'function'){uet("af");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/robert-downey-jr-through-the-years/rg3972045568?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2455214982&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_rdj_hd" > <h3>Robert Downey Jr. Through the Years</h3> </a> </span> </span> <p class="blurb">Happy Birthday, Iron Man! Take a look back at <a href="/name/nm0000375/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2455214982&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_rdj_lk1">Robert Downey Jr.</a>'s movie career in photos.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/robert-downey-jr-through-the-years/rg3972045568?imageid=rm2302053888&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2455214982&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_rdj_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ3MTIyMzcwOF5BMl5BanBnXkFtZTcwNjg3OTgwMw@@._UY400_CR350,60,201,201_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3MTIyMzcwOF5BMl5BanBnXkFtZTcwNjg3OTgwMw@@._UY400_CR350,60,201,201_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/robert-downey-jr-through-the-years/rg3972045568?imageid=rm3348404480&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2455214982&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_rdj_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTkxMzQzOTkzOF5BMl5BanBnXkFtZTYwNTAyOTU3._UX275_CR30,30,201,201_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxMzQzOTkzOF5BMl5BanBnXkFtZTYwNTAyOTU3._UX275_CR30,30,201,201_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/robert-downey-jr-through-the-years/rg3972045568?imageid=rm4157872384&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2455214982&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_rdj_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTUzOTA4NjE1MV5BMl5BanBnXkFtZTgwNzI0MDkyODE@._UY201_CR125,0,201,201_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzOTA4NjE1MV5BMl5BanBnXkFtZTgwNzI0MDkyODE@._UY201_CR125,0,201,201_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/imdbpicks/robert-downey-jr-through-the-years/rg3972045568?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2455214982&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_rdj_sm" class="position_bottom supplemental" >See Robert through years</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59688212?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk3MjA5MTA0M15BMl5BanBnXkFtZTgwNTY0MjAxNjE@._V1_SY150_CR19,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59688212?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >âDoctor Whoâ Spinoff Unveils Cast</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p><a href="/company/co0118334?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">BBC America</a> unveiled the stars of theÂ â<a href="/title/tt0056751?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Doctor Who</a>â spin-off series â<a href="/title/tt5079788?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">Class</a>â on Monday. <a href="/name/nm0042393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk4">Greg Austin</a>, <a href="/name/nm4597126?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk5">Fady Elsayed</a>, <a href="/name/nm4560292?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk6">Sophie Hopkins</a> and Vivian Oparah will all star in the new show. They will be joined by <a href="/name/nm1906500?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk7">Katherine Kelly</a>, as she takes the role of a teacher and powerful new presence at Coal Hill School. ...                                        <span class="nobr"><a href="/news/ni59688212?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59686611?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >'Batman v Superman' Drops Big While Crossing $680 Million Worldwide</a>
    <div class="infobar">
            <span class="text-muted">3 April 2016 5:31 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59687271?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >âThe Walking Deadâ Finale: Negan Makes His Mark in Bloody Debut (Spoilers)</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59687307?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Caitlyn Jenner to Appear on âTransparentâ Season 3</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59687186?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Dwayne âThe Rockâ Johnson Makes His Historic Return to the WWE Ring at WrestleMania 32</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59686611?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTU5MzQ5MzI1MF5BMl5BanBnXkFtZTgwNzczODk0NzE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59686611?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >'Batman v Superman' Drops Big While Crossing $680 Million Worldwide</a>
    <div class="infobar">
            <span class="text-muted">3 April 2016 5:31 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p>The steep second weekend drop suffered by <a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Batman v Superman: Dawn of Justice</a> will receive the bulk of attention this week and rightfully so as it is the fifth largest second weekend drop for a film that opened over $100 million. That said, the superhero feature maintained the #1 position and has ...                                        <span class="nobr"><a href="/news/ni59686611?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59686823?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Deadpool Just Broke Yet Another Record</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59686693?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >âVaxxedâ Bows In NYC, âMiles Aheadâ Sprints In Debut & âEverybody Wants Some!!â Gets Some: Specialty Box Office</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59686147?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >2016 GLAAD Media Awards Winners; Lilly Wachowski, âBessieâ & More</a>
    <div class="infobar">
            <span class="text-muted">3 April 2016 6:01 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59686642?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Charlize Theron Reveals That She And Tom Hardy Didn't Get Along On The Set Of Mad Max</a>
    <div class="infobar">
            <span class="text-muted">3 April 2016 5:52 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59688411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjExMjUyMDI5OV5BMl5BanBnXkFtZTgwODcyMDY2NzE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59688411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >'Full Frontal with Samantha Bee' gets 26 more episodes to impress Emmy Awards voters</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Gold Derby</a></span>
    </div>
                                </div>
<p>"<a href="/title/tt5323988?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Full Frontal with Samantha Bee</a>" just got a big boost from TBS, which tripled its order for the first season from 13 to 39 episodes. Reviews for this sassy half hour combo of commentary and sketches, which debuted in early February, have been exceptionally strong. It scores an impressive 84 at ...                                        <span class="nobr"><a href="/news/ni59688411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59687271?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >âThe Walking Deadâ Finale: Negan Makes His Mark in Bloody Debut (Spoilers)</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59688212?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >âDoctor Whoâ Spinoff Unveils Cast</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59687276?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Walking Dead Creator Confirms Negan's Victim is 'Very Beloved to Everyone'</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59687307?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Caitlyn Jenner to Appear on âTransparentâ Season 3</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59687887?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjMxMzg3MDI5NV5BMl5BanBnXkFtZTcwOTAxODc0Ng@@._V1_SY150_CR15,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59687887?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Why BeyoncÃ© and Jay Z's 8th Wedding Anniversary Marks Their Year of "Infinite Power"</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p>One of music's most influential couples is celebrating what may be their most important milestone yet.Â  <a href="/name/nm0461498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">BeyoncÃ©</a> and <a href="/name/nm0419650?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Jay Z</a>Â have reached their eighth wedding anniversary today. While the benchmark holds no traditional significance in the grand history of anniversaries, it is unique to this famous ...                                        <span class="nobr"><a href="/news/ni59687887?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59687129?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Kit Harington and Rose Leslie Make Their Relationship Red Carpet Official</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59687265?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Avril Lavigne Is "Feeling Healthy" While Stepping Out With Chad Kroeger at Juno Awards</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59687951?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Drew Barrymore Speaks Out After Will Kopelman Split</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59687244?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Taylor Swift Gives Calvin Harris a Sweet Shout-Out in Her iHeartRadio Music Awards Speech</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NewsDeskWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3252125696/rg3304495872?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457414362&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Still of Heath Ledger in The Dark Knight (2008)" alt="Still of Heath Ledger in The Dark Knight (2008)" src="http://ia.media-imdb.com/images/M/MV5BMjA5ODU3NTI0Ml5BMl5BanBnXkFtZTcwODczMTk2Mw@@._V1_SY201_CR135,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5ODU3NTI0Ml5BMl5BanBnXkFtZTcwODczMTk2Mw@@._V1_SY201_CR135,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm3252125696/rg3304495872?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457414362&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1" > Remembering Heath Ledger: 1979-2008 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1007820544/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457414362&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="Kill Zone 2 (2015)" alt="Kill Zone 2 (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTkzNzk2MDI0MF5BMl5BanBnXkFtZTgwNTE1MTM1ODE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkzNzk2MDI0MF5BMl5BanBnXkFtZTgwNTE1MTM1ODE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1007820544/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457414362&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm186130176/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457414362&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Kit Harington and Rose Leslie" alt="Kit Harington and Rose Leslie" src="http://ia.media-imdb.com/images/M/MV5BMTcxMjY0MDU3MV5BMl5BanBnXkFtZTgwODcwODI1ODE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcxMjY0MDU3MV5BMl5BanBnXkFtZTgwODcwODI1ODE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm186130176/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457414362&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3" > Photos We Love </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=4-4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0300589?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Sarah Gadon" alt="Sarah Gadon" src="http://ia.media-imdb.com/images/M/MV5BMTg3MDA0MDMwN15BMl5BanBnXkFtZTcwMzg5NzM2Ng@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3MDA0MDMwN15BMl5BanBnXkFtZTcwMzg5NzM2Ng@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0300589?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Sarah Gadon</a> (29) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005132?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Heath Ledger" alt="Heath Ledger" src="http://ia.media-imdb.com/images/M/MV5BMTI2NTY0NzA4MF5BMl5BanBnXkFtZTYwMjE1MDE0._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI2NTY0NzA4MF5BMl5BanBnXkFtZTYwMjE1MDE0._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005132?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Heath Ledger</a> (1979-2008) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000375?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Robert Downey Jr." alt="Robert Downey Jr." src="http://ia.media-imdb.com/images/M/MV5BMTAwNjk2NTUyMzleQTJeQWpwZ15BbWU3MDQ2NzQzMTc@._V1_SY172_CR1,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAwNjk2NTUyMzleQTJeQWpwZ15BbWU3MDQ2NzQzMTc@._V1_SY172_CR1,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000375?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Robert Downey Jr.</a> (51) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0915989?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Hugo Weaving" alt="Hugo Weaving" src="http://ia.media-imdb.com/images/M/MV5BMjAxMzAyNDQyMF5BMl5BanBnXkFtZTcwOTM4ODcxMw@@._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAxMzAyNDQyMF5BMl5BanBnXkFtZTcwOTM4ODcxMw@@._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0915989?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Hugo Weaving</a> (56) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1119462?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Amanda Righetti" alt="Amanda Righetti" src="http://ia.media-imdb.com/images/M/MV5BMjI4MzAzMDE3MV5BMl5BanBnXkFtZTgwOTk4NTM0NTE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4MzAzMDE3MV5BMl5BanBnXkFtZTgwOTk4NTM0NTE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1119462?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Amanda Righetti</a> (33) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=4-4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BornTodayWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg902011648?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454204942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_kr_hd" > <h3>Looking Back at "Knight Rider"</h3> </a> </span> </span> <p class="blurb">Take a trip back to the '80s on the 30th anniversary of the last episode of "<a href="/title/tt0083437/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454204942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_kr_lk1">Knight Rider</a>."</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm493952768/rg902011648?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454204942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_kr_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTU0MTE4NTkxN15BMl5BanBnXkFtZTgwOTUzNjk0ODE@._UX1000_CR260,100,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0MTE4NTkxN15BMl5BanBnXkFtZTgwOTUzNjk0ODE@._UX1000_CR260,100,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3144752896/rg902011648?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454204942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_kr_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNDA0MTk5NTY3M15BMl5BanBnXkFtZTgwMTk0Njk0ODE@._UX800_CR190,60,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDA0MTk5NTY3M15BMl5BanBnXkFtZTgwMTk0Njk0ODE@._UX800_CR190,60,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm309403392/rg902011648?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454204942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_kr_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjU3OTY4Njk2OF5BMl5BanBnXkFtZTgwMzUzNjk0ODE@._UX900_CR300,40,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjU3OTY4Njk2OF5BMl5BanBnXkFtZTgwMzUzNjk0ODE@._UX900_CR300,40,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg902011648?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454204942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_kr_sm" class="position_bottom supplemental" >See the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Indie Focus: 'Meet the Hitlers'</h3> </span> </span> <p class="blurb">This documentary explores the lives of individuals from all over the world who have the misfortune of sharing a last name with Adolph Hitler.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2432918/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454092042&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1" > <img itemprop="image" class="pri_image" title="Meet the Hitlers (2014)" alt="Meet the Hitlers (2014)" src="http://ia.media-imdb.com/images/M/MV5BMTgxMDkyNDA3MV5BMl5BanBnXkFtZTgwMjc4NjU2MzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgxMDkyNDA3MV5BMl5BanBnXkFtZTgwMjc4NjU2MzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2236790041?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454092042&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2" data-video="vi2236790041" data-rid="1YT9009K5PQN6Y0EJ86K" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="The first three minutes of the documentary 'Meet the Hitlers', from executive producer Morgan Spurlock." alt="The first three minutes of the documentary 'Meet the Hitlers', from executive producer Morgan Spurlock." src="http://ia.media-imdb.com/images/M/MV5BNjI1NzUyODg3NV5BMl5BanBnXkFtZTgwNDgwODA1ODE@._V1_SY250_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjI1NzUyODg3NV5BMl5BanBnXkFtZTgwNDgwODA1ODE@._V1_SY250_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="The first three minutes of the documentary 'Meet the Hitlers', from executive producer Morgan Spurlock." title="The first three minutes of the documentary 'Meet the Hitlers', from executive producer Morgan Spurlock." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The first three minutes of the documentary 'Meet the Hitlers', from executive producer Morgan Spurlock." title="The first three minutes of the documentary 'Meet the Hitlers', from executive producer Morgan Spurlock." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-9"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/april-indie-releases/ls036252967?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454214102&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_apr_hd" > <h3>April's Indie, Foreign, and Documentary Release Dates</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/april-indie-releases/ls036252967?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454214102&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_apr_i_1" > <img itemprop="image" class="pri_image" title="The Dark Horse (2014)" alt="The Dark Horse (2014)" src="http://ia.media-imdb.com/images/M/MV5BMjQzNjIxOTI4OF5BMl5BanBnXkFtZTgwMzQwNTg5NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQzNjIxOTI4OF5BMl5BanBnXkFtZTgwMzQwNTg5NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/april-indie-releases/ls036252967?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454214102&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_apr_i_2" > <img itemprop="image" class="pri_image" title="Gabriel Byrne, Isabelle Huppert, Jesse Eisenberg and Devin Druid in Back Home (2015)" alt="Gabriel Byrne, Isabelle Huppert, Jesse Eisenberg and Devin Druid in Back Home (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTc0MTQxMDY2OV5BMl5BanBnXkFtZTgwNTIwNTgxODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc0MTQxMDY2OV5BMl5BanBnXkFtZTgwNTIwNTgxODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/april-indie-releases/ls036252967?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454214102&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_apr_i_3" > <img itemprop="image" class="pri_image" title="Lucy Boynton and Ferdia Walsh-Peelo in Sing Street (2016)" alt="Lucy Boynton and Ferdia Walsh-Peelo in Sing Street (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjEzODA3MDcxMl5BMl5BanBnXkFtZTgwODgxNDk3NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzODA3MDcxMl5BMl5BanBnXkFtZTgwODgxNDk3NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Watch trailers, check release dates, and learn more about the slate of indie, foreign, and documentary movies that are due out this month.</p> <p class="seemore"><a href="/imdbpicks/april-indie-releases/ls036252967?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2454214102&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_apr_sm" class="position_bottom supplemental" >See our list</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-11"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_hd" > <h3>Now Trending on Amazon Video</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYtYYkQQh8zEcWHj-iSVxuFMke3KHeG6kCzll24EmYMQ8vVnWHM-R9tMtLa25XEtgTnF1EonU9N%0D%0APJTEqA60RGurv68jgzxhPwbTm6x-sTdfCLF8AhcFJmzTwljgcmy_-GXtMJSYjM875OA3W0baMA1j%0D%0AlQQD23ArTWZA-GaYKx_UErCqbvIHQjlpjRDLkzY9dZEm9ZiWoCpi7wm7XVTrfQppAbApt2OMVEao%0D%0AJCHezxyOEFYpsnIB7rGU_brsTFMMXlFWRB806u8HEW57GXMKHpgsMQ%0D%0A&ref_=hm_aiv_i_1" > <img itemprop="image" class="pri_image" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" src="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYotTWPuR4xxYfCNE8F6ankp7sW4BL96KEoHgHRgeG9wwRdM8ONnalPsUZj_92Ixqo8oMTtWkZG%0D%0ADHyl6hMyepiJojruPAnHx095mMFIVEBJqGKc8Q48bwRC1xR-Ira-1qB9fYE9XyYXo5kWzb1Cwi3T%0D%0AM563TD8M_zHYVPup0DK7bAKxgnXxK-9MV5BTHVe8yoZzsSapHnOJR838HPa20Xf9X1DVoUbEgcrZ%0D%0ApLMJdNsAA8SKI_nSKpNXwwkZX8PgFXFUf8r-mshMNrNpbMhz4-brcQ%0D%0A&ref_=hm_aiv_cap_pri_1" > <i>Star Wars: The Force Awakens</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYi8W7QPqF0tQ5RBqqpdMlVocnQHlLJVsohw2UZF8OE_mSPiSizmOoxowhlBtUXWmL7MPtgOLdF%0D%0A-NJR8YQkkUEzqZxj_YqKbORnKrJOXSm9c-glf7mxLedbNdjfxLMf07b9oSkTFAGtxchcn0gD5ZTb%0D%0AxvxJ9H-NPUANmWmd1DHgxy_MFx226wL2iKZnTaJ44CW4TNn5kbi_qDt9x14_I-gLZIWUqv14zHHI%0D%0APDGaZrlyVeat39CwIACtQmoyPvIpVzdcYgv8QlXSSsFwIIAQ3Vf4gA%0D%0A&ref_=hm_aiv_i_2" > <img itemprop="image" class="pri_image" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYpGiia_EZunw5ZwtudF9Zz-38iBYflvNNpELhL6zuMFpib_fDB0TYq0hMHFGC1tvYn1_7TWTvQ%0D%0AuTqDrizfB3iBzfYt7xjM-n5fK2i45rpHOl7PXagSr_QgaBdyegDZgl79tWwZa2e2S6Z0-BdwtZsU%0D%0AN5i4_qjq3mIjkUZ8uXdv9gkFDZgtyRVqnL0rF6sgsQaFc9S5HTccozYq7tZ99I-o3V31soCxzncw%0D%0AvFV5t9xL8jlFSo0BazG3ngtX9qbHFCnwFV0XV731dFrjrxDdw6lIwQ%0D%0A&ref_=hm_aiv_cap_pri_2" > <i>The Hunger Games: Mockingjay Part 2</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYhNjDEdvx9NzczAVzLdiuX1YroXXtNBoOlea_MrG7QTZ9T4TJrjTRPbX_UU4WWRNvre6qjR4xb%0D%0ABNlSjJLiJjgY-9NWlsvPBa1EDwuVi-m-BItLjdJ1F4riB2DADiCyLgZdhqntexW7VuV91JMPm-1X%0D%0AU2Te32IxMGZ6aa8R1EL8Ra8rIQ3XOEECGTl1Gin9F5ERunyKOgx7E-PQfmbASgUr7Ok5Z0qgyL8_%0D%0An5-ITFyEHbuQerxL5KGt90UMFK7qddAyyYwCTWON8jihcObarLMUbQ%0D%0A&ref_=hm_aiv_i_3" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010)" alt="The Walking Dead (2010)" src="http://ia.media-imdb.com/images/M/MV5BMTk1MjI1NjI0MV5BMl5BanBnXkFtZTcwODQ5MzA3Mw@@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk1MjI1NjI0MV5BMl5BanBnXkFtZTcwODQ5MzA3Mw@@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="The Walking Dead (2010)" title="The Walking Dead (2010)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="The Walking Dead (2010)" title="The Walking Dead (2010)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYrT1e0qkt_ccj7r3zPhZxslW-a5yHcCX6xrCjMBdzleTF2QDSqCNu3_gHk0zQMk02Rjtmwa27m%0D%0AyFE_-Hrjk3RW_q6jsJbA5yhF7JDD0OVR_13wt_c5uYSFUeAYqgjDRpDS37NpvauDqD-6RP1uybvM%0D%0AdZ_y2OZceHxCRbtxyIe_fb4Iu3B0pRh2X8ySlOzuw3OkpFJSBOoHloSWnL95ptBsDt9ExTnnyi2E%0D%0AmLa5DTyS4tniqyYmU29Z1rVSe1Rm6HypqH0M5iWgrl7UceevimJR6Q%0D%0A&ref_=hm_aiv_cap_pri_3" > "The Walking Dead - Season 6" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYpwKc94yZOgkOiWb5wWS7jwPoa2w2u0K-ZcEwgL0SN-GWt5PNKP6ZAUm8KcP03DgMohKCDcXyS%0D%0A2sGtZSrzbCjCSN1hoJIwc7ihbx-t_UvH8YXxzyRqOqI0-He0O17Q_oEuFBBIbg8kyqcYLPG50U7g%0D%0AqlXn9MGxvm38pwEtyIdjD82IW52_ZaJ_O-MGrpryDZ28Jdc0Ffm8u2BENOniZnW8zoulhGbcIGyY%0D%0A4mLvq5rcDbpObUPOz-NNCjljqlu8hO8lWM1nrsG4ssSY_5POCXtcTg%0D%0A&ref_=hm_aiv_i_4" > <img itemprop="image" class="pri_image" title="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" alt="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ0OTE1MTk4N15BMl5BanBnXkFtZTgwMDM5OTk5NjE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ0OTE1MTk4N15BMl5BanBnXkFtZTgwMDM5OTk5NjE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" title="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" title="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYlNzxVIA7FzZqJUj7HE_t7rbldrdXLNxSJ8ZNsjznQJ_QZ2g68A3Y6xeOCXRHV0Q_hjWUWwa01%0D%0AKZ6hG9jDDmZLcvqmVkfFlWtY64cy45KItGQPBroRSRpsxh42Y9hZYkMjyG4dIArKB-XhEDp8ABXH%0D%0AWcns-OIkjxOcxaH2VJSweLNafqsijAbco3xFQY_vA3a2NMvAezYhjnpCJwVd7vE48YfzTGPoSCRW%0D%0AJp9JpCOk1GD3nAttRJ6S7OtRy8Y3qRbV7IIUvlH_SJGto0gFJqWvLg%0D%0A&ref_=hm_aiv_cap_pri_4" > <i>Daddy's Home</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYtDDcDfMMUzaxNmmKOrSiOx4_zyBaEuEvKqjW8-esEycpvUz7KFD_59sp-ZuGMAcEv_JeRc_kO%0D%0A5txiCKVaTCVmc3pWEmWQJAGsNvt-FRymYGgKSTEjKbPYkN49inF1ul_O1CWgtmFzuuTRTPkiid1i%0D%0AWxqo90PIbYoWtUG_yu_RUVJN0kvvoPPDSU9PJK6LIyDa9MGdIsSyRRUSn_oB5fJeiwmD4BHQLZwp%0D%0Aohs6dGF6YNK4clnH7onDf1bqlnznZg5tLmFKegcdsJjcFDE6kEULYw%0D%0A&ref_=hm_aiv_i_5" > <img itemprop="image" class="pri_image" title="Leonardo DiCaprio in The Revenant (2015)" alt="Leonardo DiCaprio in The Revenant (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Leonardo DiCaprio in The Revenant (2015)" title="Leonardo DiCaprio in The Revenant (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Leonardo DiCaprio in The Revenant (2015)" title="Leonardo DiCaprio in The Revenant (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYo9nIFgauMle8ptCzmJUf9-Zc8LNMg0DngYIosr2-CyTUpUuJsnmwcoM3cPorsc4XT5WmSdRmz%0D%0A-g7AqcB1DUk5Hya6NCe-pyeqL6DC8ozWnHQ9RwLEFpDWexxrqqwC0HC5at6chQuJoWTo6pQ1R5WF%0D%0ASg9ytODCIz1X8IvnVooSgILtLnNLy3KkovutE2g1X0vYdP_RbNj2QMUa47qwhnLDQztV0gMkFbdK%0D%0Ajhv-GRdLHv48WjjTV6PerZ4cj7gSWmwOUVp2F3S2wHQ18JuGiRxeKA%0D%0A&ref_=hm_aiv_cap_pri_5" > <i>The Revenant</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb"><i>Star Wars: Episode VII - The Force Awakens</i>, which includes "the making of" Bonus Features, has been topping the charts at Amazon Video. People are also watching <i>The Revenant</i>, <i>Hunger Games: Mockingjay Part 2</i>, <i>Daddy's Home</i> and other trending movies and series.</p> <p class="seemore"><a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457094942&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_sm" class="position_bottom supplemental" >See which movies and shows are trending on Amazon Video</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','RecsWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt1390411/trivia?item=tr2264512&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1390411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="In the Heart of the Sea (2015)" alt="In the Heart of the Sea (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjA5NzUwODExM15BMl5BanBnXkFtZTgwNjM0MzE4NjE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5NzUwODExM15BMl5BanBnXkFtZTgwNjM0MzE4NjE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt1390411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">In the Heart of the Sea</a></strong> <p class="blurb">According to Ron Howard and the commentary on the film, to prepare for the role of starving sailors, the cast were on a diet of 500-800 calories a day to lose weight.</p> <p class="seemore"><a href="/title/tt1390411/trivia?item=tr2264512&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TriviaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;cVdbx47teYs&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/cVdbx47teYs/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd" > <h3>Poll: Rude Awakenings</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/cVdbx47teYs/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="Still of Sigourney Weaver in Alien (1979)" alt="Still of Sigourney Weaver in Alien (1979)" src="http://ia.media-imdb.com/images/M/MV5BMTQ1NTE2ODQxNV5BMl5BanBnXkFtZTcwOTE2NTUyMw@@._V1_SY207_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1NTE2ODQxNV5BMl5BanBnXkFtZTcwOTE2NTUyMw@@._V1_SY207_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/cVdbx47teYs/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Still of Harrison Ford in Star Wars: Episode IV - A New Hope (1977)" alt="Still of Harrison Ford in Star Wars: Episode IV - A New Hope (1977)" src="http://ia.media-imdb.com/images/M/MV5BMTM2NDg1MzA2M15BMl5BanBnXkFtZTcwNTAyMTIyMw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM2NDg1MzA2M15BMl5BanBnXkFtZTcwNTAyMTIyMw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/cVdbx47teYs/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="Still of Sylvester Stallone in Demolition Man (1993)" alt="Still of Sylvester Stallone in Demolition Man (1993)" src="http://ia.media-imdb.com/images/M/MV5BMjEyNjAzNTEyNF5BMl5BanBnXkFtZTgwODIyOTIwMjE@._V1_SY207_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyNjAzNTEyNF5BMl5BanBnXkFtZTgwODIyOTIwMjE@._V1_SY207_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/cVdbx47teYs/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4" > <img itemprop="image" class="pri_image" title="&quot;Buck Rogers in the 25th Century&quot; Gil Gerard, Twiki the Robot" alt="&quot;Buck Rogers in the 25th Century&quot; Gil Gerard, Twiki the Robot" src="http://ia.media-imdb.com/images/M/MV5BMjA3MTIwODQxN15BMl5BanBnXkFtZTYwNjA5MjU2._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA3MTIwODQxN15BMl5BanBnXkFtZTYwNjA5MjU2._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/cVdbx47teYs/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5" > <img itemprop="image" class="pri_image" title="Still of Chris Evans in Captain America: The First Avenger (2011)" alt="Still of Chris Evans in Captain America: The First Avenger (2011)" src="http://ia.media-imdb.com/images/M/MV5BMTY4NTU1NTEwNV5BMl5BanBnXkFtZTcwMzE1NzAwNg@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4NTU1NTEwNV5BMl5BanBnXkFtZTcwMzE1NzAwNg@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Some of these characters fell into a coma, some went into suspended animation, some just took a nap (accidental or deliberate). Which of these character's "rude awakenings" was your favorite? (WARNING: Possible Spoilers)<br /><br />Discuss <a href="http://www.imdb.com/board/bd0000088/nest/232339796?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/cVdbx47teYs/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2457068262&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','PollWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=109092592345;ord=109092592345?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=109092592345?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=109092592345?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3072482"></div> <div class="title"> <a href="/title/tt3072482?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Hardcore Henry </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2702724"></div> <div class="title"> <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The Boss </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1172049"></div> <div class="title"> <a href="/title/tt1172049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Demolition </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2400463"></div> <div class="title"> <a href="/title/tt2400463?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> The Invitation </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2217859"></div> <div class="title"> <a href="/title/tt2217859?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Back Home </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4794512"></div> <div class="title"> <a href="/title/tt4794512?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Wedding Doll </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2091935"></div> <div class="title"> <a href="/title/tt2091935?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Mr. Right </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt5217256"></div> <div class="title"> <a href="/title/tt5217256?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> The Dying of the Light </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2975590"></div> <div class="title"> <a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Batman v Superman: Dawn of Justice </a> <span class="secondary-text">$52.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2948356"></div> <div class="title"> <a href="/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Zootopia </a> <span class="secondary-text">$20.0M</span> </div> <div class="action"> <a href="/showtimes/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3760922"></div> <div class="title"> <a href="/title/tt3760922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> My Big Fat Greek Wedding 2 </a> <span class="secondary-text">$11.1M</span> </div> <div class="action"> <a href="/showtimes/title/tt3760922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4824308"></div> <div class="title"> <a href="/title/tt4824308?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> God's Not Dead 2 </a> <span class="secondary-text">$8.1M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4257926"></div> <div class="title"> <a href="/title/tt4257926?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Miracles from Heaven </a> <span class="secondary-text">$7.6M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BoxOfficeListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3040964"></div> <div class="title"> <a href="/title/tt3040964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> The Jungle Book </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3628584"></div> <div class="title"> <a href="/title/tt3628584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Barbershop: The Next Cut </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3014866"></div> <div class="title"> <a href="/title/tt3014866?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Criminal </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4062536"></div> <div class="title"> <a href="/title/tt4062536?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Green Room </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3495026"></div> <div class="title"> <a href="/title/tt3495026?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Fan </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src="http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3626940295._CB298540565_.html?config=%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D"></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TwitterWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','FacebookWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2451098282&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_hd" > <h3>IMDb Picks: April</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2451098282&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Still of Emilia Clarke in Game of Thrones (2011)" alt="Still of Emilia Clarke in Game of Thrones (2011)" src="http://ia.media-imdb.com/images/M/MV5BNTE4NzY3MjY2NV5BMl5BanBnXkFtZTgwODY5Njk5NzE@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTE4NzY3MjY2NV5BMl5BanBnXkFtZTgwODY5Njk5NzE@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2451098282&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_cap_pri_1" > "Game of Thrones" </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Of course "Game of Thrones" is on our radar this month. See which other movies and TV shows we're excited about in our IMDb Picks section.</p> <p class="seemore"><a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2451098282&pf_rd_r=1YT9009K5PQN6Y0EJ86K&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_sm" class="position_bottom supplemental" >Visit the IMDb Picks section</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYm_4P_ZZ_IHqpeAXVN0qivfIyw7Rs0yaIgCKLPjHVyLMi6m4tZCxNsML5dWjw2GdfgJ1EAxlyF%0D%0Avy_6BMr9gWhDlDM0GeURVSa2wenr6THlcC4P_k1IJCr3sfQO4bUT54a5D5vaSlR45R2wy_onlIBc%0D%0AYf_g901fSzXR5nO_CjKO1rmDWFpiseQEWpveqHOGJKYFFNaCYkiOpFudvw-b4s36QaUVlFnqbJ9I%0D%0AHEXSEwKZC-M%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYqZkQp5tU8wfKb2m4XwZz9UXuIIwmWMavYSx9yjbj-8gEcv-DntDYmGHWD2A75Tf22ZtA95R54%0D%0ANN5ygL1ULNQZLeGiCmcaURn3LRCH05X7nBxfZfdHkBvPpk1uhsb71LWDmceDTxIkLeKeHMU0vXrN%0D%0AHfT_Jlqh_fXIKVoFIJ9AAROqRrr2GpCne7SbYW4S-10zoMnUG-PtUWs0iYnFK8VOcg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYvOKgkxINf24rpGjXqv4C2-hfBU44db5mLB1WfrJ8qd9KaQau7ZHI4dm4LMK3jPwHtsth3hegr%0D%0A4f4bwJbsFGzjz5_46-lTW3ISEarYeUi01vHY74dE2I_hKYaa94mcIWJKCO0-_K1YDFXHleaU6eYc%0D%0AKveMRnOvlpRbisMTJj2AiczlQvc-HhBmF_dxuyF32uHEzDT-UpnLfIFP4WELuCV7h-Fa2vLnkRKy%0D%0AvD38OxeFalGlS6UQ_tbuxg1YJdgI8HntO-A2bunaXbl6c5Z58hqyEEsqYKSq5EMyzsYNIitLmosL%0D%0AyfUPtVXsmLo0c5wqnRs0-wUNRn-yEhujBZy_47QgHfU1Caai8DPpJV6-Rwb9dc-1INa9frzTFudo%0D%0ABJiKzzZJUrbukdS5SORzfHJZI7NMfk_KfqUqAG5cAbVcyOZ2Wi8%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYmRMqAt4424bB7n0BXmQBtDAVixqGIBz1a7bwkO_S3QLYugbcu6fFFqGNWtE4wUMaA9MWuMNqv%0D%0ALdFJbv6CwQGk4K62RYz1JnkX7JNlg8yz7BEM7qGSREQWZdaP4kSOYrI0U8hp96ARyiJ7W5baEVRA%0D%0AA9uT0PJXBhH7bsc0hMgXA0_yvGEUkfmz9KVzl3eWLAouk1sZir2upAZOszwgO8dtgA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYgFtvGAG-tCwwYmFPANiO-UV5_T5IZwM0_PFL6-9ih6auvAx_gV1_dp_9zjwO5OjAvYa0GcZ0Q%0D%0AyyWIjj89-ru35fuLIkdm81NNSSCMsae1NMYaGU4xPXM5jF5unm12cTffgvSIB7dobY4K4ru_fcDF%0D%0AkuIPYwAWPoD2-TEIJTRpTwWfVRe1Q_qRFpzvTcZ_qFTk%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYs3xBqJ9MSmFJL4bEklUMlg4hL9aLSv8cSa0RbueFYT7sKvlebUJvBCpxHmSAC4ymSvMoVPxHR%0D%0AuTe5sTX0MnQO3_JGsNsxPM7e2-6JX71yTo_y6bOsN7h7gWYy0SWp330C822V1xrDAmDBfjk5yUMR%0D%0A0PDtoXA69NQK_9Fxk0YTtetZVGL43ZPLAuhl-kiVrN1r%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYhEr7OMu3967QTQsKSIzRoHzqKUuMgWL1E53M859YoEX0W-Qtl4BjSLILmlJFrRJY-ieGhYHGt%0D%0Aad6y-MHjWl1553tvZnXe8ua-SXA23benvLzL3H5P3_XB6IvH2-dvfB75F5extPIBE1eCrltoPUOC%0D%0ANtc-enF9sO3W8N72hE4V2Ib_flkKd1M9y8IO5Kpgtb2rZZlhHJ4zPK5sEPZbGx07ow%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYgW0oD3_XJh3eb71QqHwRsVjXobMX3fPUgVXNNHl5k2uGgx0KDM-dNmmd_uSd8fmWrYkP31w9e%0D%0AV0fYUKbXSzvb-i4L2YqyCUNEWzHybPGczItj2cPEjoaBiaBdJgaEXXAvWVNBAh7aBfBA49FDsPgs%0D%0AuTg4W_qF8ORMFF-4XJgOJZgIfmmJHuZ7loRFjdI1EopW%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYhUa9hP03e7cXjA4U5zTjlm1l09a9wvMuymKDqrUxlpxvtuNGH8SPMXf-1IiozOO9RoHEyP84v%0D%0Adb8OUhkKOjrT4nNzNZp8aaJlFmj3hwT_mb7Ky20qeYRH-ur3BPrU3HK2MUHdQo0i_ZphXSxRgSLo%0D%0Ap7_Ads5ehuB6MUJXYK8onCOw159JcI6n9lhVgyG5YlPtCq8-z98TOGfqLQRTBi7o2cnu4F6wFJBz%0D%0APkm9_s99l8_UGYYMilqGGvwNtdckD6FP%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYp9e8ZIFRccFflog5bhWkxxEeJV8MaEmrPow0H_npA280K7vtZ1hOXqFhRTka_pyrHsVslwhgk%0D%0AnQDYOPZIxWIo3TGXJ8q5TgTDiSC6v5LhTLZ-zO0gaiW2FZXflCyuiCA99ySh7hdInQtfKuK7_6M4%0D%0AK-3O24usLPFVnzxZXYUnxu6eErBAL8H_gueJphFumq09Dmsr5JCoSsayNly4A4j-8EngZ9Bj6_aU%0D%0AQb1cgASxQfE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYs-qRLnDYZRb9bGj2RP7oK10DwiQsMrPxsRfh3nVVq_0bHm_jr5Nhryj0rF2qNd9TZj05NGdYV%0D%0AkDxnYrU6g29MyGaubjFa6Q37ZxoKo760ABtR8l2jBv4Bnb-S-70qZ-Y5cCG3gUHaffz0s4SlBmH8%0D%0Aevcm26BT7Il19UrxAj9B0ciD80Vzgw-Kv4JHrMm-H_JUoqdW7P0vdaZw1FjEtxfb48L_hnLdwXJn%0D%0A44Atw6cIpRs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYuibGiWl4FIqfE_MOh51-OTKdYbt91Ud1iTgvpTNvrwDBdTHjgQlCp-0uhGRYCSNLvshj1fFmJ%0D%0Akm1TmTKKfSgy-OLfLY_JkRFd3OVASevKnc4KNYkwqUyKUEcw7xKGFOk7ip_XDHmlCcYT0A2zidqx%0D%0AyHIajiIHfsDloJkXcuAuj5GqozVU68-88yo_Z9S0ACroJ5eluOeTz_ZzsdAfC6PxTBVjE3MXL8BI%0D%0A79_c5O7CCrc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYmQZ3Hu9YLQC_klg9o7eMBRCzWyblHS-g4n-D2eElSQYMQ2dH8lLwieJznVQ3e05wNiXXlpKnB%0D%0AAe28j4wQjrLPSkpFhtSNtQkYHhNZUX3KtM5wSKYXeObVeyHUYEtTtEzEb2CJ6wJsiq_YEvEg2wKH%0D%0A5jc5FsYjO0NSKmqnaGxrh1t7gbSL0sGeCGDzO952GLSw3vLAZ9Z37zzuPlliWjCYBhIUAa_fxh3H%0D%0AIC1kaAH_nzw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYkr-Vutjr1o1UwWKavsuTledG7oDpoysRYhLfslpSmaIxrqjl3-Vnn2pqr-9mOiYJV_NoAOpLj%0D%0AZriLNSByD0nu75YggcBsN324x6QsdhHDsIUH_DsDKxgGlVJjIWLuwIuUz3phJFqNGdQ_WmaQTWpi%0D%0Ax5M2KlBEES_NkX-dghCSsPYZODMhEE96j5qrCR4Sjl8Pw4OmNTy4b2qDlaJUCkkjdF-YmNMvxrRy%0D%0AoRYw4j53G-E%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYqD5Ldx69NDqwgGegcTnIliHWMgK6rFGtrADcs2lh6BqB6NNMl9fc3JICm5UFcWq-EvVG9OZG3%0D%0AawADhNCrL915qIcds35hukdrQ_hqSZq9Tmm7TJveUd_4aLvvka7lBAB_64x2TWhiy-mhk427nVtT%0D%0AvwXU-QW39re5GjOIXIxZ3tn0BD64szJihAxuUO0t2VB4dSENef-UrLRgwW2Z5thbuZGVDn7lqVXA%0D%0AEn-v9WPz0hKvr7fyS-3ishMlX6tGffpn%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYiDRnsMyBPJurLowji6ApIFNIxwtWkU6sY3w2l_tx9poa77_XDgS8tjmKa_bx3PIMXahmA9Raf%0D%0AeQPhSSCXaqzMGi8AUwTK7drgT-_5f8Q-3CWe0fH_p1La0Lqx2UiaZhx1FLYl_fM6x8cMZdyFUwn_%0D%0AWq_m4z-cBEnx_Tbx9EJxqAA%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYjsjDmDVS9IN_D2Fc1vNM9mNo4lsTng4o1xF3lRX8ZYmOT834owyl_QrOCZbdlfQZMdHKh9ZGk%0D%0Ad_ylCC4wf3kVu_gLOxq-G57zw-kd3MQhgfZtVBylYvEEIGAvyOX2Qnw4L6UmAchR0nYPE3ecxgZa%0D%0A2-xyAAtxFxmLOKizYNYcmRA%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-2997851314._CB293837866_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-816966180._CB298601395_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=010130925f9a02b350259cd91a063ee1f753e0e0d685c0febbf361e0ccb1538d4ee1",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=109092592345"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-458185442._CB294884543_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=109092592345&ord=109092592345";
    },"unable to request AAN pixel");
</script>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="460"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
