ï»¿<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>China.com - Your guide on traveling and living in China</title>
<meta name="keywords" content="China,travel,lifestyle,learn Chinese,news,videos,business,films,sports,reports" />
<meta name="description" content="English.china.com is a one-stop shop for everything about China â news, events, culture, people, lifestyle, language. It also provides information about traveling and living in China." />
<meta name="auther" content="F7 13489" />
<!-- /etc/htmlhead.shtml Start -->
<link href="/css/style.css?20141205.1" rel="stylesheet" />
<!--[if lte IE 6]> 
<script src="/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.page-select-language-list em, .page-nav .page-openSearch em, .page-focus-prevnext a, .page-video-right .item-text, .page-watched .play, .page-video-right .item-icon, .page-video-right .item-watch'); 
</script>
<![endif]-->
<!-- /etc/htmlhead.shtml End -->
<!-- /etc/goto3g.shtml Start -->

<!-- /etc/goto3g.shtml End -->
</head>

<body>
<!-- /etc/channelhomehead.shtml Start -->
<div class="page-head">
  <div class="page-top-bg" id="page-top">
    <div class="maxWidth page-top">
      <div class="page-logo"><a href="/index.html"><img src="/img/logo.png" alt="china.com" width="208" height="46" /></a></div>
      <div class="page-language">
        <em>language:English</em>
      </div>
      <div class="page-top-right">
          <div class="page-top-time">Thursday, October 24, 2013</div>
          <div class="page-select-language" id="page-select-language">
              <div class="page-show-language" style="display: block;">Language</div>
              <div class="page-select-language-tit" style="display: none;"><i>Language</i></div>
              <div class="page-select-language-list" style="display: none;">
                <a href="http://www.china.com/index.html" class="langCn"><em>www</em></a>
                <a href="http://english.china.com/index.html" class="langEn"><em>english</em></a>
                <a href="http://german.china.com/index.html" class="langDe"><em>german</em></a>
                <a href="http://italy.china.com/index.html" class="langIt"><em>italy</em></a>
                <a href="http://portuguese.china.com/index.html" class="langPt"><em>portuguese</em></a>
                <a href="http://french.china.com/index.html" class="langFr"><em>french</em></a>
                <a href="http://russian.china.com/index.html" class="langRu"><em>russian</em></a>
                <a href="http://espanol.china.com/index.html" class="langEs"><em>espanol</em></a>
                <a href="http://malay.china.com/index.html" class="langMy"><em>malay</em></a>
                <a href="http://vietnamese.china.com/index.html" class="langVn"><em>vietnamese</em></a>
                <a href="http://laos.china.com/index.html" class="langLa"><em>laos</em></a>
                <a href="http://cambodian.china.com/index.html" class="langKh"><em>cambodian</em></a>
                <a href="http://thai.china.com/index.html" class="langTh"><em>thai</em></a>
                <a href="http://indonesian.china.com/index.html" class="langId"><em>indonesian</em></a>
                <a href="http://filipino.china.com/index.html" class="langPh"><em>filipino</em></a>
                <a href="http://myanmar.china.com/index.html" class="langMm"><em>myanmar</em></a>
                <a href="http://japanese.china.com/index.html" class="langJp"><em>japanese</em></a>
                <a href="http://korean.china.com/index.html" class="langKr"><em>korean</em></a>
                <a href="http://mongol.china.com/index.html" class="langMn"><em>mongol</em></a>
                <a href="http://nepal.china.com/index.html" class="langNp"><em>nepal</em></a>
                <a href="http://hindi.china.com/index.html" class="langIn"><em>hindi</em></a>
                <a href="http://bengali.china.com/index.html" class="langMd"><em>bengali</em></a>
                <a href="http://turkish.china.com/index.html" class="langTr"><em>turkish</em></a>
                <a href="http://persian.china.com/index.html" class="langIr"><em>persian</em></a>
                <a href="http://arabic.china.com/index.html" class="langAe"><em>arabic</em></a>
              </div>
            </div>
            <div class="page-search">
              <form id="web-search" name="web-search" method="get" action="http://www.google.com/search">
                <input type="hidden" name="sitesearch" value="english.china.com" />
                <input type="text" name="q" class="web-search-keyword" placeholder="Search here..." />
                <input type="submit" name="button" class="web-search-but" value="Search Site" />
              </form>
            </div>
      </div>
    </div>
  </div>
  <div class="page-nav-bg" id="page-nav">
    <ul class="page-nav maxWidth">
      <li class="page-openSearch"><a href="#"><em>Open search</em></a></li>
      <li class="small-logo"><a href="/index.html"><img src="/img/small-logo.png" /></a></li>
      <li><a href="/news/index.html">News</a></li>
      <li><a href="/video/index.html">Video</a></li>
      <!--<li><a href="/audio/index.html">Audio</a></li>-->
      <!--<li><a href="/photos/index.html">Photos</a></li>-->
      <li><a href="/travel/index.html">Travel</a></li>
      <!--<li><a href="/lifestyle/index.html">Lifestyle</a></li>-->
      <li><a href="/chinese/index.html">Learn Chinese</a></li>
	  <li><a href="http://english.china.com/cityguide/">City Guide</a></li>
      <li><a href="http://mail.china.com/en/" class="ext" target="_blank">Free Mail</a></li>
    </ul>
  </div>
</div><!-- page-head End -->
<!-- /etc/channelhomehead.shtml End -->

<div class="maxWidth">
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 #17286  Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=206" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 End -->
</div>

<div class="page-main maxWidth">
  <div class="page-left">
    <div class="page-focus" id="page-focus">
      <div class="page-focus-body" id="page-focus-body">
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/02/03/4082s915806_3.htm" title="Pandas Take Part in Field Training in Sichuan"><img src="http://img01.mini.abroad.imgcdc.com/english/home/topphoto/1295/20160204/567589_148312.jpg.680x330.jpg" width="680" height="330" alt="Pandas Take Part in Field Training in Sichuan" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/02/03/4082s915806_3.htm" title="Pandas Take Part in Field Training in Sichuan" class="title_default">Pandas Take Part in Field Training in Sichuan</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/02/03/4201s915836.htm" title="Wall of Kindness Appears in Southwest China"><img src="http://img04.mini.abroad.imgcdc.com/english/home/topphoto/1295/20160204/567588_148311.jpg.680x330.jpg" width="680" height="330" alt="Wall of Kindness Appears in Southwest China" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/02/03/4201s915836.htm" title="Wall of Kindness Appears in Southwest China" class="title_default">Wall of Kindness Appears in Southwest China</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/02/03/4082s915818.htm" title="Manned Flower Vending Machine Uses Real People to Sell Flowers"><img src="http://img03.abroad.imgcdc.com/english/news/topphotos/china/189/20160203/567474_148298_680x330.jpg" width="680" height="330" alt="Manned Flower Vending Machine Uses Real People to Sell Flowers" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/02/03/4082s915818.htm" title="Manned Flower Vending Machine Uses Real People to Sell Flowers" class="title_default">Manned Flower Vending Machine Uses Real People to Sell Flowers</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/2016-02/03/c_135069268.htm" title="Hillary Clinton narrowly wins Democratic Iowa caucuses"><img src="http://img02.abroad.imgcdc.com/english/news/topphotos/world/1208/20160203/567472_148297_680x330.jpg" width="680" height="330" alt="Hillary Clinton narrowly wins Democratic Iowa caucuses" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/2016-02/03/c_135069268.htm" title="Hillary Clinton narrowly wins Democratic Iowa caucuses" class="title_default">Hillary Clinton narrowly wins Democratic Iowa caucuses</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/02/02/2702s915657.htm" title="2016 US Presidential Election Starts Officially in Iowa State"><img src="http://img04.mini.abroad.imgcdc.com/english/news/topphotos/world/1208/20160202/565558_147999.jpg.680x330.jpg" width="680" height="330" alt="2016 US Presidential Election Starts Officially in Iowa State" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/02/02/2702s915657.htm" title="2016 US Presidential Election Starts Officially in Iowa State" class="title_default">2016 US Presidential Election Starts Officially in Iowa State</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
      </div>
      <div id="page-focus-console"></div>
      <div class="page-focus-prevnext">
        <a href="#" id="page-focus-prev">Previous focus</a>
        <a href="#" id="page-focus-next">Next focus</a>
      </div>
    </div><!-- page-focus End -->
    
    <div class="page-latest">
      <h2 class="modTit"><strong><a href="/news/index.html">LATEST NEWS</a></strong></h2>
      <div class="page-latest-body" id="page-latest">
        <div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/sports/2016-02/04/content_23395872.htm" title="Lin scores 24 as Hornets rally to beat Cavaliers"><img src="http://img01.abroad.imgcdc.com/english/news/sports/57/20160204/568297_148528_200x120.jpg" width="200" height="120" alt="Lin scores 24 as Hornets rally to beat Cavaliers" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/04 21:25:55</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/sports/2016-02/04/content_23395872.htm" title="Lin scores 24 as Hornets rally to beat Cavaliers" class="title_default">Lin scores 24 as Hornets rally to beat Cavaliers</a></h3>
            <p class="item-infor" title="Jeremy Lin scored 24 points and the Hornets rallied in the second half for a 106-97 victory over James and the Eastern Conference-leading Cleveland Cavaliers on Wednesday night.">Jeremy Lin scored 24 points and the Hornets rallied in the second half for a 106-97 victory over James and the Eastern Conference-leading Cleveland Cavaliers on Wednesday night.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.globaltimes.cn/content/967412.shtml" title="Author Feng Tang attacks decision to pull book from shelves"><img src="http://img02.abroad.imgcdc.com/english/news/showbiz/58/20160204/568298_148529_200x120.jpeg" width="200" height="120" alt="Author Feng Tang attacks decision to pull book from shelves" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/02/04 21:25:52</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://www.globaltimes.cn/content/967412.shtml" title="Author Feng Tang attacks decision to pull book from shelves" class="title_default">Author Feng Tang attacks decision to pull book from shelves</a></h3>
            <p class="item-infor" title="Chinese poet Feng Tang responded to critics of his translation of Tagore's collection of poems and the decision to remove his translation from bookstores in letters he wrote in Chinese and English to the deceased Indian poet and philosopher.  ">Chinese poet Feng Tang responded to critics of his translation of Tagore's collection of poems and the decision to remove his translation from bookstores in letters he wrote in Chinese and English to the deceased Indian poet and philosopher.  </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/04/4201s915945.htm" title="Ecuador FM: UN Decision on Assange Soon"><img src="http://img04.abroad.imgcdc.com/english/news/world/55/20160204/568296_148527_200x120.jpg" width="200" height="120" alt="Ecuador FM: UN Decision on Assange Soon" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/04 21:23:31</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/04/4201s915945.htm" title="Ecuador FM: UN Decision on Assange Soon" class="title_default">Ecuador FM: UN Decision on Assange Soon</a></h3>
            <p class="item-infor" title="Ecuador's Foreign Minister says a decision by the United Nations on whether Julian Assange might be freed from the Ecuadorian Embassy in London and allowed to travel to Ecuador could be handed down as early as Friday.">Ecuador's Foreign Minister says a decision by the United Nations on whether Julian Assange might be freed from the Ecuadorian Embassy in London and allowed to travel to Ecuador could be handed down as early as Friday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://en.people.cn/n3/2016/0204/c90000-9013984.html" title="South Korea uses WeChat red envelopes to lure Chinese tourists"><img src="http://img03.abroad.imgcdc.com/english/news/business/56/20160204/568294_148526_200x120.jpg" width="200" height="120" alt="South Korea uses WeChat red envelopes to lure Chinese tourists" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/04 21:22:04</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://en.people.cn/n3/2016/0204/c90000-9013984.html" title="South Korea uses WeChat red envelopes to lure Chinese tourists" class="title_default">South Korea uses WeChat red envelopes to lure Chinese tourists</a></h3>
            <p class="item-infor" title="With Spring Festival fast approaching, South Korea's travel agencies have launched a series of events to attract Chinese tourists, including sending red envelopes through WeChat, a popular social media platform in China, Beijing Times reports on Wednesday.">With Spring Festival fast approaching, South Korea's travel agencies have launched a series of events to attract Chinese tourists, including sending red envelopes through WeChat, a popular social media platform in China, Beijing Times reports on Wednesday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/metro/Warmer-temperatures-pleasant-weather-during-upcoming-holiday/shdaily.shtml" title="Warmer temperatures, pleasant weather during upcoming holiday"><img src="http://img02.abroad.imgcdc.com/english/news/china/54/20160204/568293_148525_200x120.jpg" width="200" height="120" alt="Warmer temperatures, pleasant weather during upcoming holiday" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/02/04 21:21:18</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/metro/Warmer-temperatures-pleasant-weather-during-upcoming-holiday/shdaily.shtml" title="Warmer temperatures, pleasant weather during upcoming holiday" class="title_default">Warmer temperatures, pleasant weather during upcoming holiday</a></h3>
            <p class="item-infor" title="Locals could expect to enjoy warmer temperatures during the upcoming Spring Festival holiday with highest temperatures reaching 17 degrees Celsius on Wednesday and Thursday next week, forecasters said.">Locals could expect to enjoy warmer temperatures during the upcoming Spring Festival holiday with highest temperatures reaching 17 degrees Celsius on Wednesday and Thursday next week, forecasters said.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/03/3441s915802.htm" title="Shanghai Disney Resort Start Pre-ticketing Mar 28"><img src="http://img02.mini.abroad.imgcdc.com/english/news/china/54/20160204/567590_148313.jpg.200x120.jpg" width="200" height="120" alt="Shanghai Disney Resort Start Pre-ticketing Mar 28" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/02/04 08:54:28</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/03/3441s915802.htm" title="Shanghai Disney Resort Start Pre-ticketing Mar 28" class="title_default">Shanghai Disney Resort Start Pre-ticketing Mar 28</a></h3>
            <p class="item-infor" title="Shanghai Disney Resort has set its ticket price at 370 yuan, or 56 US dollars for standard days, and 499 yuan, or 75 dollars for peak days, such as holidays and weekends.">Shanghai Disney Resort has set its ticket price at 370 yuan, or 56 US dollars for standard days, and 499 yuan, or 75 dollars for peak days, such as holidays and weekends.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/03/4081s915831.htm" title="Visa-free Entry for Chinese Tourists in Malaysia"><img src="http://img03.mini.abroad.imgcdc.com/english/news/business/56/20160204/567585_148310.jpg.200x120.jpg" width="200" height="120" alt="Visa-free Entry for Chinese Tourists in Malaysia" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/04 08:47:09</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/03/4081s915831.htm" title="Visa-free Entry for Chinese Tourists in Malaysia" class="title_default">Visa-free Entry for Chinese Tourists in Malaysia</a></h3>
            <p class="item-infor" title="Malaysia has scrapped the visa requirement for Chinese tourists starting from March. The measure aims to revive the country's tourist industry and build up the economy.">Malaysia has scrapped the visa requirement for Chinese tourists starting from March. The measure aims to revive the country's tourist industry and build up the economy.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/04/3941s915865.htm" title="Chelsea and Watford Play to Goalless Draw in English Premier League"><img src="http://img02.mini.abroad.imgcdc.com/english/news/sports/57/20160204/567584_148309.jpg.200x120.jpg" width="200" height="120" alt="Chelsea and Watford Play to Goalless Draw in English Premier League" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/04 08:44:10</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/04/3941s915865.htm" title="Chelsea and Watford Play to Goalless Draw in English Premier League" class="title_default">Chelsea and Watford Play to Goalless Draw in English Premier League</a></h3>
            <p class="item-infor" title="Struggling Chelsea has been held to a goalless draw with Watford. The 0-0 draw leaves Watford in 9th in the standings, while Chelsea remains at 13th.">Struggling Chelsea has been held to a goalless draw with Watford. The 0-0 draw leaves Watford in 9th in the standings, while Chelsea remains at 13th.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/04/c_135072648.htm" title="Russia Agrees to Take 200 to 300 Asylum Seekers from Norway"><img src="" width="" height="" alt="Russia Agrees to Take 200 to 300 Asylum Seekers from Norway" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/04 08:43:15</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/04/c_135072648.htm" title="Russia Agrees to Take 200 to 300 Asylum Seekers from Norway" class="title_default">Russia Agrees to Take 200 to 300 Asylum Seekers from Norway</a></h3>
            <p class="item-infor" title="Russia agreed on Wednesday to take back 200 to 300 people whose asylum applications were rejected by Norway and said their returns can only happen by plane to Moscow, local media reported.">Russia agreed on Wednesday to take back 200 to 300 people whose asylum applications were rejected by Norway and said their returns can only happen by plane to Moscow, local media reported.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/04/c_135073046.htm" title="U.S. Panel Endorses Technique to Create "Three-parent Babies""><img src="" width="" height="" alt="U.S. Panel Endorses Technique to Create "Three-parent Babies"" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/04 08:41:46</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/04/c_135073046.htm" title="U.S. Panel Endorses Technique to Create "Three-parent Babies"" class="title_default">U.S. Panel Endorses Technique to Create "Three-parent Babies"</a></h3>
            <p class="item-infor" title="A U.S. panel of scientists and ethicists said Wednesday the U.S. government should approve clinical investigations of mitochondrial replacement, a technique that could prevent certain genetic diseases but would create babies with genetic materials from three parents.">A U.S. panel of scientists and ethicists said Wednesday the U.S. government should approve clinical investigations of mitochondrial replacement, a technique that could prevent certain genetic diseases but would create babies with genetic materials from three parents.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.globaltimes.cn/content/967175.shtml" title="Malaysia wary of Zika virus"><img src="" width="" height="" alt="Malaysia wary of Zika virus" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/03 20:51:59</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://www.globaltimes.cn/content/967175.shtml" title="Malaysia wary of Zika virus" class="title_default">Malaysia wary of Zika virus</a></h3>
            <p class="item-infor" title="Malaysians were advised to take precautious measures and refrain from going to South America on Wednesday as the government has shown concern over the Zika virus. ">Malaysians were advised to take precautious measures and refrain from going to South America on Wednesday as the government has shown concern over the Zika virus. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/sports/2016-02/03/content_23382477.htm" title="China FA goes back to the future with coach Gao"><img src="http://img01.abroad.imgcdc.com/english/news/sports/57/20160203/567470_148296_200x120.jpg" width="200" height="120" alt="China FA goes back to the future with coach Gao" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/03 20:51:17</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/sports/2016-02/03/content_23382477.htm" title="China FA goes back to the future with coach Gao" class="title_default">China FA goes back to the future with coach Gao</a></h3>
            <p class="item-infor" title="The Chinese Football Association has turned back the clock by naming ex-national player Gao Hongbo national team coach for a second time.">The Chinese Football Association has turned back the clock by naming ex-national player Gao Hongbo national team coach for a second time.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://en.people.cn/n3/2016/0203/c98649-9013481.html" title="Per-capita GDP of 10 Chinese provinces exceed $10,000 mark"><img src="http://img03.abroad.imgcdc.com/english/news/business/56/20160203/567468_148294_200x120.jpg" width="200" height="120" alt="Per-capita GDP of 10 Chinese provinces exceed $10,000 mark" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/03 20:50:41</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://en.people.cn/n3/2016/0203/c98649-9013481.html" title="Per-capita GDP of 10 Chinese provinces exceed $10,000 mark" class="title_default">Per-capita GDP of 10 Chinese provinces exceed $10,000 mark</a></h3>
            <p class="item-infor" title="With the per-capita GDP of Shandong, an eastern province in China, exceeding $10,000 in 2015, 10 of Chinaâs 31 provincial-level regions are now listed in the club with a per-capita GDP of over $10,000, China Business News reported Wednesday.">With the per-capita GDP of Shandong, an eastern province in China, exceeding $10,000 in 2015, 10 of Chinaâs 31 provincial-level regions are now listed in the club with a per-capita GDP of over $10,000, China Business News reported Wednesday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/national/14-jailed-for-factory-explosion-killing-146-in-China/shdaily.shtml" title="14 jailed for factory explosion killing 146 in China"><img src="http://img02.abroad.imgcdc.com/english/news/china/54/20160203/567466_148293_200x120.jpg" width="200" height="120" alt="14 jailed for factory explosion killing 146 in China" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/02/03 20:49:28</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/national/14-jailed-for-factory-explosion-killing-146-in-China/shdaily.shtml" title="14 jailed for factory explosion killing 146 in China" class="title_default">14 jailed for factory explosion killing 146 in China</a></h3>
            <p class="item-infor" title="COURTS in east China's Jiangsu Province on Wednesday sentenced 14 people to prison terms for various offenses related to an explosion at a factory in 2014 that left 146 dead.">COURTS in east China's Jiangsu Province on Wednesday sentenced 14 people to prison terms for various offenses related to an explosion at a factory in 2014 that left 146 dead.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/03/3441s915802.htm" title="Shanghai Disney Resort Start Pre-ticketing Mar 28"><img src="http://img01.abroad.imgcdc.com/english/news/showbiz/58/20160203/567465_148292_200x120.jpg" width="200" height="120" alt="Shanghai Disney Resort Start Pre-ticketing Mar 28" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/02/03 20:48:43</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/03/3441s915802.htm" title="Shanghai Disney Resort Start Pre-ticketing Mar 28" class="title_default">Shanghai Disney Resort Start Pre-ticketing Mar 28</a></h3>
            <p class="item-infor" title="Shanghai Disney Resort has set its ticket price at 370 yuan, or 56 US dollars for standard days, and 499 yuan, or 75 dollars for peak days, such as holidays and weekends.">Shanghai Disney Resort has set its ticket price at 370 yuan, or 56 US dollars for standard days, and 499 yuan, or 75 dollars for peak days, such as holidays and weekends.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/03/3941s915702.htm" title="Vardy Keeps Leicester Clear at Top"><img src="http://img01.mini.abroad.imgcdc.com/english/news/sports/57/20160203/565727_148068.jpg.200x120.jpg" width="200" height="120" alt="Vardy Keeps Leicester Clear at Top" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/03 07:07:32</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/03/3941s915702.htm" title="Vardy Keeps Leicester Clear at Top" class="title_default">Vardy Keeps Leicester Clear at Top</a></h3>
            <p class="item-infor" title="Leicester has maintained its spot atop the English Premiership. Jermey Vardy with both goals as Leicester dumped Liverpool 2-0 this Tuesday morning.">Leicester has maintained its spot atop the English Premiership. Jermey Vardy with both goals as Leicester dumped Liverpool 2-0 this Tuesday morning.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/03/3941s915704.htm" title="Broncos Ready for Panthers at Super Bowl"><img src="http://img04.mini.abroad.imgcdc.com/english/news/sports/57/20160203/565726_148067.jpg.200x120.jpg" width="200" height="120" alt="Broncos Ready for Panthers at Super Bowl" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/03 07:05:50</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/03/3941s915704.htm" title="Broncos Ready for Panthers at Super Bowl" class="title_default">Broncos Ready for Panthers at Super Bowl</a></h3>
            <p class="item-infor" title="The Carolina Panthers and the Denver Broncos are getting ready to take to the practice field today ahead of Sunday's Super Bowl.">The Carolina Panthers and the Denver Broncos are getting ready to take to the practice field today ahead of Sunday's Super Bowl.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/03/c_135069407.htm" title="U.S. Stocks Slump amid Mixed Earnings, Diving Oil"><img src="" width="" height="" alt="U.S. Stocks Slump amid Mixed Earnings, Diving Oil" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/03 06:24:18</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/03/c_135069407.htm" title="U.S. Stocks Slump amid Mixed Earnings, Diving Oil" class="title_default">U.S. Stocks Slump amid Mixed Earnings, Diving Oil</a></h3>
            <p class="item-infor" title="U.S. stocks suffered big losses Tuesday, as oil prices retreated further amid mixed earnings reports.">U.S. stocks suffered big losses Tuesday, as oil prices retreated further amid mixed earnings reports.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/03/c_135069387.htm" title="U.S., British Leaders Discuss Syria, Refugee Crisis"><img src="" width="" height="" alt="U.S., British Leaders Discuss Syria, Refugee Crisis" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/03 06:05:16</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/03/c_135069387.htm" title="U.S., British Leaders Discuss Syria, Refugee Crisis" class="title_default">U.S., British Leaders Discuss Syria, Refugee Crisis</a></h3>
            <p class="item-infor" title="U.S. President Barack Obama on Tuesday spoke by phone with British Prime Minister David Cameron to discuss Syria and the upcoming Syria aid conference, the White House said.">U.S. President Barack Obama on Tuesday spoke by phone with British Prime Minister David Cameron to discuss Syria and the upcoming Syria aid conference, the White House said.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/business/economy/Bumpy-new-year-for-Chinas-factories/shdaily.shtml" title="Bumpy new year for Chinaâs factories"><img src="" width="" height="" alt="Bumpy new year for Chinaâs factories" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/02 19:02:24</em><em class="hide">February 05 2016 02:34:06</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/business/economy/Bumpy-new-year-for-Chinas-factories/shdaily.shtml" title="Bumpy new year for Chinaâs factories" class="title_default">Bumpy new year for Chinaâs factories</a></h3>
            <p class="item-infor" title="CHINAâS economy is having a bumpy start to the year with activity in manufacturing and the service sector deteriorating in January, figures released yesterday showed.">CHINAâS economy is having a bumpy start to the year with activity in manufacturing and the service sector deteriorating in January, figures released yesterday showed.</p>
          </div>
        </div>
      </div>
      <div class="page-latest-more">
        <a href="#" id="page-latest-show-more"><em class="page-latest-more-icon">&nbsp;</em></a>
        <a href="/news/index.html" id="page-latest-click-more"><em class="page-latest-more-icon">Show More</em></a>
      </div>
    </div><!-- page-latest End -->
    <div class="page-left-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=207" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 End -->
	</div> 
    <div class="page-video">
      <h2 class="modTit"><strong><a href="/video/index.html">VIDEO</a></strong></h2>
      <div class="page-video-body">
        <div class="page-video-left">
          <script type="text/javascript" src="http://c.wrating.com/v2_pre.js"></script>
<!--noscript-->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="420" height="234">
            <param name="movie" value="http://english.china.com/videoPlayer/video.swf"/>
            <param name="quality" value="high"/>
            <param name="bgcolor" value="#ffffff"/>
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="true"/>
            <param name="wmode" value="Opaque">
            <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/mychineselife/2015/12/1230joshweb.mp4&loop=1&autoplay=0"/>
            <!--[if!IE]>
            -->
            <object type="application/x-shockwave-flash" data="http://english.china.com/videoPlayer/video.swf" width="420" height="234">
              <param name="quality" value="high"/>
              <param name="bgcolor" value="#ffffff"/>
              <param name="allowScriptAccess" value="sameDomain"/>
              <param name="allowFullScreen" value="true"/>
              <param name="wmode" value="Opaque">
              <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/mychineselife/2015/12/1230joshweb.mp4&loop=1&autoplay=0"/>
              <!--<![endif]-->
              <!--[if gte IE 6]>
              -->
              <p>
                Either scripts and active content are not permitted to run or Adobe Flash Player version 11.4.0 or greater is not installed.
              </p>
              <!--<![endif]-->
              <a href="http://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player"/>
              </a>
              <!--[if!IE]>--></object>
            <!--<![endif]-->
          </object>
          <!--/noscript-->
          <a href="/home/videobig/1299/20151231/537959.html" class="video-tit">Along the Silk Road: Josh Summers--Rediscover Xinjiang</a>
        </div>
        <div class="page-video-right" id="page-video-right">
          <div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20160128/559220.html"><img src="http://img02.abroad.imgcdc.com/english/home/videosmall/1301/20160128/559293_147037.jpg" width="245" height="125" alt="Along the Silk Road: Elise Anderson--An American Xinjiang Idol" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20160128/559220.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road: Elise Anderson--An American Xinjiang Idol</strong></h3>
              <p class="item-infor">Elise Anderson became a local celebrity because of her passion for the Uyghur performing arts. </p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div><div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20151231/537864.html"><img src="http://img03.abroad.imgcdc.com/english/home/videosmall/1301/20151231/537873_141302.jpg" width="245" height="125" alt="Along the Silk Road: Joy Bostwick--The Silk Road Artist" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20151231/537864.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road: Joy Bostwick--The Silk Road Artist</strong></h3>
              <p class="item-infor">American artist documents the beauty of Xinjiang with her paintings.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div><!-- page-video End -->
    <div class="page-mods">
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/travel/index.html">TRAVEL</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners"><img src="http://img03.mini.abroad.imgcdc.com/english/travel/listright/mostpopular/1534/20150506/366181_88898.jpg.330x190.jpg" width="330" height="190" alt="Food Awards 2015: the winners" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners" class="title_default">Food Awards 2015: the winners</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12394/2015/08/11/2743s891245.htm" title="Japanese War Orphan Recounts Past" class="title_default">Japanese War Orphan Recounts Past</a></li><li><a href="http://english.cri.cn/6566/2014/12/25/44s858261.htm" title="Winter Nadam Kicks Off in North China" class="title_default">Winter Nadam Kicks Off in North China</a></li><li><a href="http://english.cri.cn/6566/2014/09/29/44s845986.htm" title="To Experience Authentic Taiwan Folk Art in Beijing" class="title_default">To Experience Authentic Taiwan Folk Art in Beijing</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/chinese/index.html">LEARN CHINESE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love"><img src="http://img03.mini.abroad.imgcdc.com/english/home/learnpic/1315/20141124/211729_51886.jpg.330x190.jpg" width="330" height="190" alt="China's First Love" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love" class="title_default">China's First Love</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12514/2014/10/16/2001s848110.htm" title="Top 10 Popular Chinese TV Dramas Overseas" class="title_default">Top 10 Popular Chinese TV Dramas Overseas</a></li><li><a href="http://english.cri.cn/12514/2014/10/17/2001s848240.htm" title="çµç¶ Chinese Pipa" class="title_default">çµç¶ Chinese Pipa</a></li><li><a href="http://english.cri.cn/12514/2014/09/25/2001s845407.htm" title="Useful Shopping Sentences in Chinese" class="title_default">Useful Shopping Sentences in Chinese</a></li>
          </ul>
        </div>

      </div>
      
      
    </div><!-- page-mods End -->
    <!--<div class="page-left-ad"><a href="#"><img src="/file/left-ad-2.jpg" /></a></div> page-left-ad End -->
    <!-- page-photos End -->
  </div><!-- page-left End -->
  <div class="page-right">
    
    <div class="page-right-ad noMarginTop">
      <!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=208" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 End -->
    </div><!-- page-right-ad End -->
    <!--include virtual="/etc/right_top_ad_index.shtml" -->
      
    ï»¿<!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://english.cri.cn/12394/2015/09/21/Zt2821s896890.htm"><img src="http://img04.abroad.imgcdc.com/english/home/imgtj/5829/20151014/482283_124639.jpg" width="293" height="88" alt="20151014" /></a><a href="http://english.cri.cn/12394/2015/09/02/Zt2821s894283.htm"><img src="http://img01.abroad.imgcdc.com/english/home/imgtj/5829/20150908/482287_124640.jpg" width="293" height="88" alt="20150908" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->

    <!-- page-right-ad End -->
    
    <div class="page-watched">
      <h2 class="modTit"><strong>Most Watched</strong></h2>
      <div class="page-watched-body" id="rank-video">
      </div>
    </div><!-- page-watched End -->
    <div class="page-popular">
      <h2 class="modTit"><strong>Most Popular</strong></h2>
      <ul class="page-popular-body" id="rank-list"></ul>
    </div><!-- page-popular End -->
    <div class="page-right-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=209" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 End -->
	</div> 
    <div class="page-tochina">
      <h2 class="modTit"><strong>Tune in to China</strong></h2>
      <div class="page-tochina-body">
        <div class="item radio-news">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="title">-</h3>
              <h4 id="playtime">-</h4>
              <a href="http://english.cri.cn/7146/2012/12/03/301s736372.htm" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/am846.wsx" class="item-play"></a>
          </div>
        </div>
        <div class="item radio-ez">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="nexttitle">-</h3>
              <h4 id="nexttime">-</h4>
              <a href="http://english.cri.cn/easyfm/ezplaytime.html" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/fm915.wsx" class="item-play"></a>
          </div>
        </div>
      </div>
    </div><!-- page-tochina End -->

    
    <div class="page-hotListening">
      <h2 class="page-hotListening-tit"><strong>Hot Listening</strong></h2>
      <div class="page-hotListening-body">
        <div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/pik.htm"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4933_1370.jpg" width="120" height="90" alt="People in the Know" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/pik.htm" class="title_default">People in the Know</a><a href="http://english.cri.cn/cribb/plus/pik.htm" class="icon-horn"></a></h3>
            <p class="item-infor">PIK gives you insights to the world through interviews.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/today.htm"><img src="http://img02.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4931_1369.jpg" width="120" height="90" alt="Today" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/today.htm" class="title_default">Today</a><a href="http://english.cri.cn/cribb/plus/today.htm" class="icon-horn"></a></h3>
            <p class="item-infor">A news magazine show with in-depth panel discussions.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/easymorning.html"><img src="http://img01.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4928_1368.jpg" width="120" height="90" alt="EZ Morning" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/easymorning.html" class="title_default">EZ Morning</a><a href="http://english.cri.cn/easyfm/easymorning.html" class="icon-horn"></a></h3>
            <p class="item-infor">It lights up your mornings with interesting chit-chats.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/ezwheel.html"><img src="http://img04.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4927_1367.jpg" width="120" height="90" alt="More to Learn" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/ezwheel.html" class="title_default">More to Learn</a><a href="http://english.cri.cn/easyfm/ezwheel.html" class="icon-horn"></a></h3>
            <p class="item-infor">More to Learn is filled up with English stories and anecdotes.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/hour.html"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4923_1366.jpg" width="120" height="90" alt="The Beijing Hour" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/hour.html" class="title_default">The Beijing Hour</a><a href="http://english.cri.cn/easyfm/hour.html" class="icon-horn"></a></h3>
            <p class="item-infor">It opens up the world to you with latest news updates.</p>
          </div>
        </div>
      </div>
    </div><!-- page-hotListening End -->

    <div class="page-mobile">
      <h2 class="modTit"><strong>Mobile</strong></h2>
      <div class="page-mobile-body">
        <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=539404062&mt=8&s=143441" class="iphone" title="Mobile Iphone">Iphone</a>
      </div>
    </div><!-- page-mobile End -->

    <div class="page-cooperation">
      <h2 class="modTit"><strong>Cooperation</strong></h2>
      <div class="page-cooperation-body">
        <a href="http://gc.wrating.com/click.php?a=&c=860099-1000099998&cs=341_285_2559_860010_400000000&ds=354_355_356_357_358&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dmobi.mgeek.TunnyBrowser%26referrer%3Dchannel_id%253Dchinacom%2526utm_source%253Dchinacom"><img src="/file/logo-dolphin-140-90.png" height="90" width="140"></a>
      </div>
    </div><!-- page-cooperation End -->

  </div><!-- page-right End -->
</div><!-- page-main End -->

<script>
window.collectMethod_rank = window.collectMethod_rank || [];
collectMethod_rank.push(function () {
  // setRank("rank-video", 3, "http://english.china.com/js/english_43_day.js", "video", function(){
  //   setRank("rank-list", 5, "http://english.china.com/js/english_22_day.js", "list");
  // });

  setRank2015("rank-video", 3, "day_top", "194", "http://rank.china.com/rank/cri/english/day/rank.js", "video", function(){
    setRank2015("rank-list", 5, "day_top", "104", "http://rank.china.com/rank/cri/english/day/rank.js", "list");
  });
});
</script>

<!-- /etc/channelsitemap.shtml Start -->
<div class="page-map">
  <div class="page-map-body maxWidth">
  <dl class="item">
      <dt><a href="http://english.china.com/news/index.html">News:</a></dt>
      <dd>
        <a href="http://english.china.com/news/china/index.html">China</a>
        <a href="http://english.china.com/news/world/index.html">World</a>
        <a href="http://english.china.com/news/business/index.html">Business</a>
        <a href="http://english.china.com/news/sports/index.html">Sports</a>
        <a href="http://english.china.com/news/showbiz/index.html">Showbiz</a>
		<a href="http://english.china.com/audio/index.html">Audio</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/video/index.html">Video:</a></dt>
      <dd>
        <a href="http://english.china.com/video/c4/index.html">C4</a>
        <a href="http://english.china.com/video/life/index.html">My Chinese Life</a>
        <a href="http://english.china.com/video/thesoundstage/index.html">The Sound Stage</a>
        <a href="http://english.china.com/video/chinarevealed/index.html">China Revealed</a>
        <a href="http://english.china.com/video/showbiz/index.html">Showbiz Video</a>
        <a href="http://english.china.com/video/tour/index.html">Travel Video</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/photos/index.html">Photos:</a></dt>
      <dd>
        <a href="http://english.china.com/photos/china/index.html">China</a>
        <a href="http://english.china.com/photos/world/index.html">World</a>
        <a href="http://english.china.com/photos/fun/index.html">Fun</a>
        <a href="http://english.china.com/photos/travel/index.html">Travel</a>
        <a href="http://english.china.com/photos/entertainment/index.html">Entertainment</a>
        <a href="http://english.china.com/photos/sports/index.html">Sports</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/travel/index.html">Travel:</a></dt>
      <dd>
        <a href="http://english.china.com/travel/beijing/index.html">Beijing</a>
        <a href="http://english.china.com/travel/shanghai/index.html">Shanghai</a>
        <a href="http://english.china.com/travel/guangzhou/index.html">Guangzhou</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/lifestyle/index.html">Lifestyle:</a></dt>
      <dd>        
        <a href="http://english.china.com/lifestyle/livemusic/index.html">Live Music</a>
        <a href="http://english.china.com/lifestyle/opera/index.html">Opera & Classical</a>
        <a href="http://english.china.com/lifestyle/movies/index.html">Movies</a>
        <a href="http://english.china.com/lifestyle/traditionalshows/index.html">Traditional Shows</a>
        <a href="http://english.china.com/lifestyle/exhibitions/index.html">Exhibitions</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/chinese/index.html">Learn Chinese:</a></dt>
      <dd>
        <a href="http://english.china.com/chinese/studio/index.html">Chinese Studio</a>
        <a href="http://english.china.com/chinese/living/index.html">Living Chinese</a>
        <a href="http://english.china.com/chinese/everyday/index.html">Everyday Chinese</a>
        <a href="http://english.china.com/chinese/justforfun/index.html">Just For Fun</a>
        <a href="http://english.china.com/chinese/culture/index.html">Chinese Culture</a>
        <a href="http://english.china.com/chinese/buzzwords/index.html">Buzzwords</a>        
      </dd>
    </dl>      
  </div>
</div><!-- page-map End -->
<!-- /etc/channelsitemap.shtml End -->

<div class="page-link">
  <div class="page-link-body maxWidth">
    <a href="http://english.cri.cn/">CRIENGLISH.com</a>|<a href="http://www.chinadaily.com.cn/">China Daily</a>|<a href="http://www.xinhuanet.com/english/">Xinhua</a>|<a href="http://www.china.org.cn/index.htm">China.org.cn</a>|<a href="http://english.cntv.cn/">CNTV</a>
  </div>
</div><!-- page-link End -->

<!-- /etc/channelcopyright.shtml Start -->
<div class="page-footer">
  <div class="page-foot-link">
    <a href="/about/">About China.com</a>|<a href="/about/gmg.html">About GMG</a>|<a href="/ad/">Ad Services</a>|<a href="/contact/">Contact Information</a>|<a href="/copyright/">Copyright Notice</a>
  </div>
  <p><!--E-mail to:<a href="mailto:english@bj.china.com">english@bj.china.com</a><br />-->Copyright &copy; China.com All Rights Reserved</p>
</div><!-- page-footer End -->

<script src="/js/require.min.js" data-main="/js/main"></script>

<!-- START WRating v1.0 -->
<script type="text/javascript" src="http://c.wrating.com/a1.js">
</script>
<script type="text/javascript">
var vjAcc="860010-0446010000";
var wrUrl="http://c.wrating.com/";
vjTrack("");
</script>
<noscript><img src="http://c.wrating.com/a.gif?a=&c=860010-0446010000" width="1" 
height="1"/></noscript>
<!-- END WRating v1.0 -->

<!-- Start Alexa Certify Javascript #13481-->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S6Upi1awA+00a/", domain:"china.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S6Upi1awA+00a/" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript #13481-->


<!-- Start Google Analytics #16010-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60581848-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics #16010-->

<!-- /etc/channelcopyright.shtml End -->
</body>
</html>