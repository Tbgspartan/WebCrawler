<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>    Imgur: The most awesome images on the Internet</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="robots" content="follow,index" />
    <meta name="keywords" content="images, funny, image host, image sharing, reaction gif, viral images, current events, cute, visual storytelling, gif" />
    <meta name="description" content="Imgur is the best place to share and enjoy the most awesome images on the Internet. Every day, millions of people use Imgur to be entertained and inspired by funny, heartwarming and helpful images and stories from all around the world." />
    <meta name="copyright" content="Copyright 2016 Imgur, Inc." />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;" />
    
    <link rel="alternate" type="application/rss+xml" title="Imgur Gallery" href="http://feeds.feedburner.com/ImgurGallery?format=xml" />
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-16x16.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="//s.imgur.com/images/favicon-152.png">
    <meta name="msapplication-TileColor" content="#2cd63c">
    <meta name="msapplication-TileImage" content="//s.imgur.com/images/favicon-144.png">
            <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/global.css?1457077774" />

    
        <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/gallery.css?1457077774" />

            <!--[if IE 9]><link rel="stylesheet" href="//s.imgur.com/include/css/ie-sucks.css?0" type="text/css" /><![endif]-->
    
            <link rel="canonical" href="http://imgur.com/" />
        <meta property="og:url" content="http://imgur.com/" />
    
    <meta name="p:domain_verify" content="a1e7abe8af908cc6dfaf935dd9a20384"/>
    <meta property="og:site_name" content="Imgur" />
    <meta property="fb:admins" content="12331492,12301369" />
    <meta property="fb:app_id" content="127621437303857" />
    <meta name="twitter:site" content="@imgur" />
    <meta name="twitter:domain" content="imgur.com" />
    <meta name="twitter:creator" content="@imgur" />
    <meta name="twitter:app:id:googleplay" content="com.imgur.mobile" />
        <meta name="twitter:card" content="summary_large_image" />
    <meta property="og:type" content="website" />
            <meta property="og:title" content="Imgur" />
        <meta name="twitter:title" content="Imgur" />
    
            <meta property="og:description" content="Imgur: The most awesome images on the Internet." />
        <meta name="twitter:description" content="Imgur: The most awesome images on the Internet." />
    
    <meta property="og:image" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image:width" content="1200" />
    <meta name="twitter:image:src" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta name="twitter:image:height" content="630" />
    <meta name="twitter:image:width" content="1200" />
    <!--[if lte IE 8]><script type="text/javascript" src="//s.imgur.com/min/iepoly.js?1457077774"></script>
<![endif]-->
    
        
</head>
<body class="">
                
    <script type='text/javascript'>
    var googletag = googletag || { };
    googletag.cmd = googletag.cmd || [];

    (function() {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
    })();
</script>



<div id="topbar">
    <div class="header-center">
        <div id="header-logo">

            <a href="//imgur.com" class="logo " data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@logo@@}}">
                
                <span class="logo-icon"></span>
            </a>
        </div>
        <div id="main-nav">
            <ul>
                <li class="menu-btn">
                    <div class="menu-icon">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                        
                    </div>
                    <div class="link-menu">
                        <div class="test-1868-show">
                            <ul class="topics cf">
                                <li class="item bold" data-value="hot"><a href="//imgur.com/hot" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@hot@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a></li>
                                <li class="item bold" data-value="new"><a href="//imgur.com/new" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@new@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a></li>
                                                                                                            <li class="item" data-value="Staff Picks">
                                            <a href="//imgur.com/topic/Staff_Picks" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Staff Picks@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Staff Picks</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Funny">
                                            <a href="//imgur.com/topic/Funny" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Awesome">
                                            <a href="//imgur.com/topic/Awesome" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Aww">
                                            <a href="//imgur.com/topic/Aww" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="The More You Know">
                                            <a href="//imgur.com/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Storytime">
                                            <a href="//imgur.com/topic/Storytime" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Current Events">
                                            <a href="//imgur.com/topic/Current_Events" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Design &amp; Art">
                                            <a href="//imgur.com/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Reaction">
                                            <a href="//imgur.com/topic/Reaction" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                                        </li>
                                    
                                                                    
                                                                                                            <li class="item" data-value="Inspiring">
                                            <a href="//imgur.com/topic/Inspiring" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Inspiring@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Inspiring</a>
                                        </li>
                                    
                                
                            </ul>
                            <ul class="dropdown-footer cf">
                                <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">blog</a></li>
                                <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">about</a></li>
                                <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">store</a></li>
                                <li><a href="//imgur.com/jobs"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">jobs</a></li>
                                <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">help</a></li>
                                <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">apps</a></li>
                                <li><a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">terms</a></li>
                                <li><a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">privacy</a></li>
                                <li><a href="//imgur.com/removalrequest" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@deletion@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">request deletion</a></li>
                                <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">api</a></li>
                                <li><a href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">ad choices</a></li>
                            </ul>
                        </div>
                        <ul class="test-1868-hide">
                            <li><a href="//imgur.com/jobs" class="new" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@}}">we're hiring!</a></li>
                                                            <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@new@@:false}}">blog</a></li>
                            
                            <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@}}">about imgur</a></li>
                            <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@}}">imgur store</a></li>
                            <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@}}">imgur apps</a></li>
                            <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@}}">developer api</a></li>
                            <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@}}">need help?</a></li>
                        </ul>
                        <div class="clear test-1868-hide"></div>
                        <div class="dropdown-footer hamburger-menu test-1868-hide">
                            <div class="terms-footer">
                                <a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@}}">terms</a>
                                <a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@}}">privacy</a>
                                <a class="small-margin-top" href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@}}">ad choices</a>
                            </div>
                             
                                <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fimgur&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="display:inline-block; border:none; overflow:hidden; height:20px; width:50px; margin-right:7px;" allowTransparency="true"></iframe>
                                <a href="https://twitter.com/imgur" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false"></a>
                            
                        </div>
                    </div>
                </li>
                <li class="upload-button-container">
                    <a class="upload-button disabled upload-blue" href="javascript:;">
                        <div class="icon icon-upload"></div>
                        <span class="upload-btn-text">upload images</span>
                        <span class="upload-queue-length">23</span>
                        <div id="upload-global-top-bar-mini-progress"></div>
                    </a>
                    <div class="creation-dropdown upload-blue">
                        <div class="selection icon icon-arrow-down"></div>
                        <div class="options">
                            <ul>
                                <li class="item">
                                    <a class="creation-dropdown-upload" href="javascript:;">Upload Images</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/vidgif">Video to GIF</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/memegen" data-jafo="{@@event@@:@@makeAMeme@@,@@meta@@:{@@control@@:@@header@@}}">Make a Meme</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="random-button title-n " title="random mode">
                    <a href="//imgur.com/random" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@random@@}}">
                        <div class="random-icon"></div>
                    </a>
                </li>
            </ul>
        </div>
        <div id="secondary-nav">
            <ul class="user-nav">
                <li id="global-search-container">
                    <form class="search-form" method="get" action="//imgur.com/search">
                        <input id="search-global-input" type="text" name="q" class="search" blur placeholder="Search images"  />
                    </form>

                    <div class="search-icon-container">
                        <div class="search-icon-container-icon"></div>
                    </div>
                </li>
                                    <li class="signin-link"><a class="" href="https://imgur.com/signin?invokedBy=regularSignIn">sign in</a></li>
                    <li id="register"><a class="" href="https://imgur.com/register?invokedBy=regularSignIn">sign up</a></li>
                
            </ul>
        </div> <!-- #secondary-nav -->
    </div> <!-- .header-center -->
</div> <!-- #topbar -->
<div id="cta-container-placeholder"></div>
<div id="cta-lightbox-placeholder"></div>
    



    

    

            

        
    

            <a href="javascript:;" id="upload-global-album-tipsy" class="title-nw nodisplay" title="You can't create an album with just one image"></a>

<div id="upload-global" class="popup">
    <form method="post" id="upload-global-form" enctype="multipart/form-data" action="http://imgur.com/upload">
        

        <div id="upload-global-buttons">
            <div id="upload-global-upload-loader" class="textbox">
                Loading...
            </div>

            <div id="upload-global-file-wrapper">
                <span class="computerIcon"></span>
                <span class="green">browse</span> your computer

                <input accept="image/*" type="file" name="Filedata" multiple="multiple" id="upload-global-file">
            </div>

            <div class="left">
                <div id="upload-global-url">
                    <span class="webIcon"></span>
                    <input type="text" id="upload-global-link-input" class="placeholder-onkeydown" title="enter image URLs" placeholder="enter image URLs" />
                </div>
            </div>

            <div class="clear"></div>

            <div id="upload-global-dragdrop">
                <span class="dragdropIcon"></span>
                <span class="green">drag and drop</span> here
            </div>

            <div class="left">
                <div id="upload-global-clipboard">
                    <div style="text-align: center"><span class="clipboardIcons">Ctrl + V</span></div>
                    <span class="green">paste</span> from your clipboard
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="textbox center" id="upload-global-instructions"></div>

        <!-- gallery submit vars -->
        <input type="hidden" name="current_upload" value="0"/>
        <input type="hidden" name="total_uploads" value="0"/>
        <input type="hidden" name="terms" value="0"/>
        <input type="hidden" name="gallery_type" value=""/>
        <input type="hidden" name="location" value=""/>


        <div id="upload-global-errors" class="textbox error" style="display: none;"></div>

        <div id="upload-global-queue-description" class="textbox nodisplay"></div>

        <div id="upload-global-start-upload">
            <div class="panel">
                <div>

                    <table id="upload-global-actions">
                        <tbody>
                            <tr>

                                <td width="50.4%">

                                    <div id="upload-global-album-checkbox-div">
                                        <input type="hidden" name="gallery_submit" value="0"/>

                                        <button id="upload-global-gallery-checkbox" type="button">
                                            <span class="gallery-icon"></span>share with community
                                        </button>

                                        <div id="upload-global-gallery-checkbox-overlay"></div>

                                        <input type="hidden" name="create_album" value="0"/>

                                        <button id="upload-global-album-checkbox" type="button">
                                            <span class="createAlbum-icon"></span>create album
                                        </button>

                                    </div>

                                </td>
                                <td width="49.6%">
                                    <button type="button" id="upload-global-start-button" class="button-big green" type="submit">
                                        <span class="start-btn-icon"></span>
                                        Start Upload
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div id="upload-global-album-options">
                    <table id="upload-global-album-layout">
    <tr>
        <td id="upload-global-album-td">
            <input name="album_title" type="text" title="Optional Album Title" class="placeholder-onkeydown" id="upload-global-album-title" maxlength="55" />
        </td>
    </tr>
</table>

                </div>

                <div class="textbox list">
                    <table id="upload-global-file-list-header">
                        <tr class="top-tr">
                            <td width="45" class="edit-td"><h2>Edit</h2></td>
                            <td><h2>Images <span class="upload-queue-length">(0)</span></h2></td>
                            <td width="40"></td>
                        </tr>
                    </table>

                    <table id="upload-global-file-list">
                    </table>
                                            <div class="agreement-notice">By uploading, you agree to our <a href="//imgur.com/tos" target="_blank">terms of service</a></div>
                    
                </div>
            </div>
        </div>

        <div id="upload-global-progress-wrapper">
            <div id="upload-global-progressbar">
                <div class="progressBarBG">
                    <div class="progressBar"></div>
                </div>
            </div>
            <div id="upload-global-upload-queue"></div>

            <div id="upload-global-converting">
                <p>
                    Optimizing your large GIFs...
                </p>
            </div>
        </div>
    </form>
</div>

<div id="upload-global-FF-paste-box"></div>

<div id="hiddenDropZone" class="nodisplay"></div>

<div class="nodisplay">
    <div id="upload-global-image-formats">
         That file type is not supported!

        <div class="textbox supported-formats">
            Supported formats: JPEG, GIF, PNG, APNG, TIFF, BMP, PDF, XCF
        </div>
    </div>
</div>

<div id="upload-global-flash-container" title="upload images from your computer">
    <div id="upload-global-flash-browse"></div>
</div>

    

        <div class="nodisplay">
	<div id="colorbox-confirm" class="popup">
		<h3 id="colorbox-confirm-title"></h3>
		
		<div class="textbox" id="colorbox-confirm-message"></div>

		<div class="margin-top">
			<div class="left">
				<a href="javascript:;" class="colorbox-no-confirm" id="colorbox-confirm-no">No way!</a>
			</div>
			
			<div class="right">
				<div class="small-loader"></div>
				<a href="javascript:;" class="colorbox-confirm" id="colorbox-confirm-yes">I'm sure</a>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</div>


    
            
    

    
    
    
    

    <div id="fullbleed-bg"></div>
    <div id="content" class="outside main">
        
                    <div class="sentence-sorting">
            <span class="before-text">
    The

</span>
<div id="section" class="combobox front-page-section sorting-text-align" name="section">
    <div class="selection">
                <div data-type="base" data-value="hot">
            <span class="name">Most Viral</span>
        </div>
        
        
    </div>

    <div class="options nano">
        <div class="js-galleries content">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                                            Most Viral
                    
                </div>
            </div>
            <ul>
                <li class="bold item nodisplay" data-type="base" data-value="hot">
                    <a href="/hot/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a>
                </li>
                <li class="bold item" data-type="base" data-value="new">
                                            <a href="/new/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@new@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a>
                    
                </li>
                <li class="combobox-hairline"></li>
                                                            <li class="item" data-value="Staff Picks">
                            <a href="/topic/Staff_Picks" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Staff Picks@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Staff Picks</a>
                        </li>
                    
                                                            <li class="item" data-value="Funny">
                            <a href="/topic/Funny" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                        </li>
                    
                                                            <li class="item" data-value="Awesome">
                            <a href="/topic/Awesome" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                        </li>
                    
                                                            <li class="item" data-value="Aww">
                            <a href="/topic/Aww" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                        </li>
                    
                                                            <li class="item" data-value="The More You Know">
                            <a href="/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                        </li>
                    
                                                            <li class="item" data-value="Storytime">
                            <a href="/topic/Storytime" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                        </li>
                    
                                                            <li class="item" data-value="Current Events">
                            <a href="/topic/Current_Events" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                        </li>
                    
                                                            <li class="item" data-value="Design &amp; Art">
                            <a href="/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                        </li>
                    
                                                            <li class="item" data-value="Reaction">
                            <a href="/topic/Reaction" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                        </li>
                    
                                    
                                                            <li class="item" data-value="Inspiring">
                            <a href="/topic/Inspiring" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Inspiring@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Inspiring</a>
                        </li>
                    
                
            </ul>
        </div>
        <div id="custom-group" class="item new-group"><a href="/gallery/custom" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@custom@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Custom Gallery</a></div>
    </div>
    <input type="hidden" name="section" value="hot">
</div><span class="middle-text"> <!-- intentionally not closed -->

    images on the Internet, 


        sorted by
    
    </span> <!-- .middle-text -->
    
    
        
            
    

            
    
    <div id="sort" class="combobox">
        <div class="selection">
            popularity
        </div>
        <div class="options sorting-text-align">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                    popularity
                </div>
            </div>
            <ul>
                                    <li class="item nodisplay" value="viral">
                                                    <a href="/hot/viral">popularity</a>
                        
                    </li>
                    <li class="item" value="time">
                                                    <a href="/hot/time">newest first</a>
                        
                    </li>
                                        <li class="item" value="top">
                                                    <a href="/top">highest scoring</a>
                        
                    </li>
                    
                
            </ul>
        </div>
        <input type="hidden" name="sort" value="viral">
    </div>

    

    <div class="sort-options">
                                    <ul>
                                            <li><a href="/gallery/random" id="random-button" class="title-n" title="random mode"><span></span></a></li>
                    
                                            <li><a href="javascript:void(0)" id="past-link" class="title-n" title="past images"><span></span></a></li>
                    
                </ul>
            
        
    </div>
    <div id="user-gallery-message">
        
    </div>
</div>

               
        

                    <div class="panel hidden">
                <div id="imagelist" class="home-gallery">
                                            <script type="text/tag-group-data">[]</script>


<div class="posts br5 first-child">
    

            
        <div class="outside-loader"></div>
        
                    
        

        <div class="cards">
                            <div id="1TiqbmX" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/1TiqbmX" data-page="0">
        <img alt="" src="//i.imgur.com/1TiqbmXb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="1TiqbmX" type="image" data-up="9871">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="1TiqbmX" type="image" data-downs="744">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-1TiqbmX">9,127</span>
                            <span class="points-text-1TiqbmX">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>After 12 hours of labor, I finally squeezed this guy out. Two miscarriages and I&#039;m finally a mom, guys!</p>
        
        
        <div class="post-info">
            image &middot; 1,714,827 views
        </div>
    </div>
    
</div>

                            <div id="RDtW8Gr" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/RDtW8Gr" data-page="0">
        <img alt="" src="//i.imgur.com/RDtW8Grb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="RDtW8Gr" type="image" data-up="5543">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="RDtW8Gr" type="image" data-downs="39">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-RDtW8Gr">5,504</span>
                            <span class="points-text-RDtW8Gr">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Trombone player pretending to play while eating</p>
        
        
        <div class="post-info">
            animated &middot; 2,677,585 views
        </div>
    </div>
    
</div>

                            <div id="qo4ZnOs" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/qo4ZnOs" data-page="0">
        <img alt="" src="//i.imgur.com/qo4ZnOsb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="qo4ZnOs" type="image" data-up="3982">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="qo4ZnOs" type="image" data-downs="53">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-qo4ZnOs">3,929</span>
                            <span class="points-text-qo4ZnOs">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I AM VENGEANCE!</p>
        
        
        <div class="post-info">
            animated &middot; 294,864 views
        </div>
    </div>
    
</div>

                            <div id="VWjNHHJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/VWjNHHJ" data-page="0">
        <img alt="" src="//i.imgur.com/VWjNHHJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="VWjNHHJ" type="image" data-up="5255">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="VWjNHHJ" type="image" data-downs="155">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-VWjNHHJ">5,100</span>
                            <span class="points-text-VWjNHHJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>7-11, not 911.</p>
        
        
        <div class="post-info">
            image &middot; 349,699 views
        </div>
    </div>
    
</div>

                            <div id="iFRlx" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/iFRlx" data-page="0">
        <img alt="" src="//i.imgur.com/CkyQqQ7b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="iFRlx" type="image" data-up="10738">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="iFRlx" type="image" data-downs="523">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-iFRlx">10,215</span>
                            <span class="points-text-iFRlx">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Voters for Trump</p>
        
        
        <div class="post-info">
            album &middot; 116,590 views
        </div>
    </div>
    
</div>

                            <div id="DPpFrRx" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/DPpFrRx" data-page="0">
        <img alt="" src="//i.imgur.com/DPpFrRxb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="DPpFrRx" type="image" data-up="5380">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="DPpFrRx" type="image" data-downs="185">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-DPpFrRx">5,195</span>
                            <span class="points-text-DPpFrRx">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Japan, Savage as F**k</p>
        
        
        <div class="post-info">
            animated &middot; 397,931 views
        </div>
    </div>
    
</div>

                            <div id="WIgQoCo" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/WIgQoCo" data-page="0">
        <img alt="" src="//i.imgur.com/WIgQoCob.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="WIgQoCo" type="image" data-up="4120">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="WIgQoCo" type="image" data-downs="265">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-WIgQoCo">3,855</span>
                            <span class="points-text-WIgQoCo">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>A mutually beneficial arrangement.</p>
        
        
        <div class="post-info">
            image &middot; 392,078 views
        </div>
    </div>
    
</div>

                            <div id="B6f2D" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/B6f2D" data-page="0">
        <img alt="" src="//i.imgur.com/8oB4B5nb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="B6f2D" type="image" data-up="3370">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="B6f2D" type="image" data-downs="126">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-B6f2D">3,244</span>
                            <span class="points-text-B6f2D">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>228mspd</p>
        
        
        <div class="post-info">
            album &middot; 46,985 views
        </div>
    </div>
    
</div>

                            <div id="FS7iR" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FS7iR" data-page="0">
        <img alt="" src="//i.imgur.com/Mm9jQEtb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FS7iR" type="image" data-up="2962">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FS7iR" type="image" data-downs="70">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FS7iR">2,892</span>
                            <span class="points-text-FS7iR">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Get rid of terrible lower backpain pain!</p>
        
        
        <div class="post-info">
            album &middot; 36,310 views
        </div>
    </div>
    
</div>

                            <div id="jYOVrgM" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/jYOVrgM" data-page="0">
        <img alt="" src="//i.imgur.com/jYOVrgMb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="jYOVrgM" type="image" data-up="3846">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="jYOVrgM" type="image" data-downs="211">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-jYOVrgM">3,635</span>
                            <span class="points-text-jYOVrgM">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>He has a point...</p>
        
        
        <div class="post-info">
            animated &middot; 266,092 views
        </div>
    </div>
    
</div>

                            <div id="vuamDL8" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/vuamDL8" data-page="0">
        <img alt="" src="//i.imgur.com/vuamDL8b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="vuamDL8" type="image" data-up="2856">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="vuamDL8" type="image" data-downs="124">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-vuamDL8">2,732</span>
                            <span class="points-text-vuamDL8">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Three years ago I got an account to upvote this post.</p>
        
        
        <div class="post-info">
            image &middot; 106,219 views
        </div>
    </div>
    
</div>

                            <div id="RSZlUsf" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/RSZlUsf" data-page="0">
        <img alt="" src="//i.imgur.com/RSZlUsfb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="RSZlUsf" type="image" data-up="2929">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="RSZlUsf" type="image" data-downs="216">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-RSZlUsf">2,713</span>
                            <span class="points-text-RSZlUsf">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I&#039;ll tell you all about it when I see you</p>
        
        
        <div class="post-info">
            image &middot; 136,419 views
        </div>
    </div>
    
</div>

                            <div id="krpux" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/krpux" data-page="0">
        <img alt="" src="//i.imgur.com/naAUfYTb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="krpux" type="image" data-up="7519">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="krpux" type="image" data-downs="276">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-krpux">7,243</span>
                            <span class="points-text-krpux">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Guys I Got A Nose Ring!!!</p>
        
        
        <div class="post-info">
            album &middot; 89,786 views
        </div>
    </div>
    
</div>

                            <div id="JO162Hl" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/JO162Hl" data-page="0">
        <img alt="" src="//i.imgur.com/JO162Hlb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="JO162Hl" type="image" data-up="3656">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="JO162Hl" type="image" data-downs="178">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-JO162Hl">3,478</span>
                            <span class="points-text-JO162Hl">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Trying to be Young as an Adult</p>
        
        
        <div class="post-info">
            animated &middot; 306,833 views
        </div>
    </div>
    
</div>

                            <div id="d1T27B0" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/d1T27B0" data-page="0">
        <img alt="" src="//i.imgur.com/d1T27B0b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="d1T27B0" type="image" data-up="3640">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="d1T27B0" type="image" data-downs="126">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-d1T27B0">3,514</span>
                            <span class="points-text-d1T27B0">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>.</p>
        
        
        <div class="post-info">
            image &middot; 240,738 views
        </div>
    </div>
    
</div>

                            <div id="yscBR" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/yscBR" data-page="0">
        <img alt="" src="//i.imgur.com/VENCXKDb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="yscBR" type="image" data-up="2024">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="yscBR" type="image" data-downs="25">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-yscBR">1,999</span>
                            <span class="points-text-yscBR">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Take your protein pills and put your helmet on</p>
        
        
        <div class="post-info">
            album &middot; 26,055 views
        </div>
    </div>
    
</div>

                            <div id="3rb7nZJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/3rb7nZJ" data-page="0">
        <img alt="" src="//i.imgur.com/3rb7nZJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="3rb7nZJ" type="image" data-up="2894">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="3rb7nZJ" type="image" data-downs="271">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-3rb7nZJ">2,623</span>
                            <span class="points-text-3rb7nZJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Shaman cat performs ritual, gives 9 lives to kittens</p>
        
        
        <div class="post-info">
            animated &middot; 269,243 views
        </div>
    </div>
    
</div>

                            <div id="y4qS95S" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/y4qS95S" data-page="0">
        <img alt="" src="//i.imgur.com/y4qS95Sb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="y4qS95S" type="image" data-up="2241">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="y4qS95S" type="image" data-downs="71">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-y4qS95S">2,170</span>
                            <span class="points-text-y4qS95S">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My upstairs neighbors at 3am.</p>
        
        
        <div class="post-info">
            animated &middot; 182,232 views
        </div>
    </div>
    
</div>

                            <div id="mU99KZs" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/mU99KZs" data-page="0">
        <img alt="" src="//i.imgur.com/mU99KZsb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="mU99KZs" type="image" data-up="1793">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="mU99KZs" type="image" data-downs="78">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-mU99KZs">1,715</span>
                            <span class="points-text-mU99KZs">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW I find something super weird in Usersub, but I liked it enough to give it an upvote</p>
        
        
        <div class="post-info">
            animated &middot; 116,901 views
        </div>
    </div>
    
</div>

                            <div id="eFbL6" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/eFbL6" data-page="0">
        <img alt="" src="//i.imgur.com/od4Og1Mb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="eFbL6" type="image" data-up="10051">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="eFbL6" type="image" data-downs="109">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-eFbL6">9,942</span>
                            <span class="points-text-eFbL6">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Something, something drugs are bad...</p>
        
        
        <div class="post-info">
            album &middot; 118,918 views
        </div>
    </div>
    
</div>

                            <div id="dHmHt" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/dHmHt" data-page="0">
        <img alt="" src="//i.imgur.com/eviPL6lb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="dHmHt" type="image" data-up="9581">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="dHmHt" type="image" data-downs="201">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-dHmHt">9,380</span>
                            <span class="points-text-dHmHt">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Ten Prank sites for April Fools</p>
        
        
        <div class="post-info">
            album &middot; 114,049 views
        </div>
    </div>
    
</div>

                            <div id="UuOLneO" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/UuOLneO" data-page="0">
        <img alt="" src="//i.imgur.com/UuOLneOb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="UuOLneO" type="image" data-up="1951">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="UuOLneO" type="image" data-downs="63">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-UuOLneO">1,888</span>
                            <span class="points-text-UuOLneO">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I&#039;m still laughing</p>
        
        
        <div class="post-info">
            animated &middot; 131,817 views
        </div>
    </div>
    
</div>

                            <div id="QCavN" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/QCavN" data-page="0">
        <img alt="" src="//i.imgur.com/TB4q2bzb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="QCavN" type="image" data-up="1598">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="QCavN" type="image" data-downs="83">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-QCavN">1,515</span>
                            <span class="points-text-QCavN">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Look at this beast</p>
        
        
        <div class="post-info">
            album &middot; 11,208 views
        </div>
    </div>
    
</div>

                            <div id="3qtaIGO" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/3qtaIGO" data-page="0">
        <img alt="" src="//i.imgur.com/3qtaIGOb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="3qtaIGO" type="image" data-up="6855">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="3qtaIGO" type="image" data-downs="217">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-3qtaIGO">6,638</span>
                            <span class="points-text-3qtaIGO">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;Here comes the airplane..*nyrrr*..&quot;</p>
        
        
        <div class="post-info">
            animated &middot; 486,373 views
        </div>
    </div>
    
</div>

                            <div id="t2VCkcc" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/t2VCkcc" data-page="0">
        <img alt="" src="//i.imgur.com/t2VCkccb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="t2VCkcc" type="image" data-up="2833">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="t2VCkcc" type="image" data-downs="77">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-t2VCkcc">2,756</span>
                            <span class="points-text-t2VCkcc">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Satisfaction ^.^</p>
        
        
        <div class="post-info">
            animated &middot; 219,184 views
        </div>
    </div>
    
</div>

                            <div id="KvIQd" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/KvIQd" data-page="0">
        <img alt="" src="//i.imgur.com/kPvJGpGb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="KvIQd" type="image" data-up="1613">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="KvIQd" type="image" data-downs="62">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-KvIQd">1,551</span>
                            <span class="points-text-KvIQd">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>A big steaming dump of funnies</p>
        
        
        <div class="post-info">
            album &middot; 17,575 views
        </div>
    </div>
    
</div>

                            <div id="8nX8dIe" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/8nX8dIe" data-page="0">
        <img alt="" src="//i.imgur.com/8nX8dIeb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="8nX8dIe" type="image" data-up="1990">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="8nX8dIe" type="image" data-downs="46">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-8nX8dIe">1,944</span>
                            <span class="points-text-8nX8dIe">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Rakotz Bridge, Germany</p>
        
        
        <div class="post-info">
            image &middot; 111,262 views
        </div>
    </div>
    
</div>

                            <div id="XfUoF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/XfUoF" data-page="0">
        <img alt="" src="//i.imgur.com/wXYqzigb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="XfUoF" type="image" data-up="6664">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="XfUoF" type="image" data-downs="276">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-XfUoF">6,388</span>
                            <span class="points-text-XfUoF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>It&#039;s just a prank bro</p>
        
        
        <div class="post-info">
            album &middot; 89,197 views
        </div>
    </div>
    
</div>

                            <div id="FI6Ay" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FI6Ay" data-page="0">
        <img alt="" src="//i.imgur.com/CHFzDDIb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FI6Ay" type="image" data-up="1290">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FI6Ay" type="image" data-downs="24">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FI6Ay">1,266</span>
                            <span class="points-text-FI6Ay">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Zach Anner. Basically a superhero.</p>
        
        
        <div class="post-info">
            album &middot; 7,744 views
        </div>
    </div>
    
</div>

                            <div id="ZB3lHiv" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ZB3lHiv" data-page="0">
        <img alt="" src="//i.imgur.com/ZB3lHivb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ZB3lHiv" type="image" data-up="10168">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ZB3lHiv" type="image" data-downs="136">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ZB3lHiv">10,032</span>
                            <span class="points-text-ZB3lHiv">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I need these</p>
        
        
        <div class="post-info">
            image &middot; 501,763 views
        </div>
    </div>
    
</div>

                            <div id="NXOOgRe" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/NXOOgRe" data-page="0">
        <img alt="" src="//i.imgur.com/NXOOgReb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="NXOOgRe" type="image" data-up="3696">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="NXOOgRe" type="image" data-downs="77">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-NXOOgRe">3,619</span>
                            <span class="points-text-NXOOgRe">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>School</p>
        
        
        <div class="post-info">
            image &middot; 202,939 views
        </div>
    </div>
    
</div>

                            <div id="BBpa7dv" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/BBpa7dv" data-page="0">
        <img alt="" src="//i.imgur.com/BBpa7dvb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="BBpa7dv" type="image" data-up="1833">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="BBpa7dv" type="image" data-downs="221">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-BBpa7dv">1,612</span>
                            <span class="points-text-BBpa7dv">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Probably not the right meme but I hope this will rub off on at least one of you</p>
        
        
        <div class="post-info">
            image &middot; 96,841 views
        </div>
    </div>
    
</div>

                            <div id="Gmi9UnJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Gmi9UnJ" data-page="0">
        <img alt="" src="//i.imgur.com/Gmi9UnJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Gmi9UnJ" type="image" data-up="2241">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Gmi9UnJ" type="image" data-downs="34">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Gmi9UnJ">2,207</span>
                            <span class="points-text-Gmi9UnJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW my spotter whispers his bro-love is stronger than steel.</p>
        
        
        <div class="post-info">
            animated &middot; 181,495 views
        </div>
    </div>
    
</div>

                            <div id="218vyJh" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/218vyJh" data-page="0">
        <img alt="" src="//i.imgur.com/218vyJhb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="218vyJh" type="image" data-up="3715">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="218vyJh" type="image" data-downs="727">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-218vyJh">2,988</span>
                            <span class="points-text-218vyJh">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>honestly this is my greatest photoshop and you fuckers always downvote me because I&#039;m not the one dude, well now I&#039;m going to cage everything I see that&#039;s cera&#039;d on the fp</p>
        
        
        <div class="post-info">
            image &middot; 178,912 views
        </div>
    </div>
    
</div>

                            <div id="GWB4TqW" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/GWB4TqW" data-page="0">
        <img alt="" src="//i.imgur.com/GWB4TqWb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="GWB4TqW" type="image" data-up="1289">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="GWB4TqW" type="image" data-downs="28">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-GWB4TqW">1,261</span>
                            <span class="points-text-GWB4TqW">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Over, under, and out</p>
        
        
        <div class="post-info">
            animated &middot; 327,438 views
        </div>
    </div>
    
</div>

                            <div id="t4r9bBU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/t4r9bBU" data-page="0">
        <img alt="" src="//i.imgur.com/t4r9bBUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="t4r9bBU" type="image" data-up="1385">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="t4r9bBU" type="image" data-downs="40">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-t4r9bBU">1,345</span>
                            <span class="points-text-t4r9bBU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Don&#039;t misclick.</p>
        
        
        <div class="post-info">
            image &middot; 58,501 views
        </div>
    </div>
    
</div>

                            <div id="LneWQu5" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/LneWQu5" data-page="0">
        <img alt="" src="//i.imgur.com/LneWQu5b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="LneWQu5" type="image" data-up="1249">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="LneWQu5" type="image" data-downs="74">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-LneWQu5">1,175</span>
                            <span class="points-text-LneWQu5">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My favotire &quot;just girly thing&quot;</p>
        
        
        <div class="post-info">
            image &middot; 78,102 views
        </div>
    </div>
    
</div>

                            <div id="buSfI" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/buSfI" data-page="0">
        <img alt="" src="//i.imgur.com/lPofZLdb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="buSfI" type="image" data-up="8704">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="buSfI" type="image" data-downs="104">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-buSfI">8,600</span>
                            <span class="points-text-buSfI">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>so much regret, so little time</p>
        
        
        <div class="post-info">
            album &middot; 117,775 views
        </div>
    </div>
    
</div>

                            <div id="ykyvPTU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ykyvPTU" data-page="0">
        <img alt="" src="//i.imgur.com/ykyvPTUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ykyvPTU" type="image" data-up="2561">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ykyvPTU" type="image" data-downs="315">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ykyvPTU">2,246</span>
                            <span class="points-text-ykyvPTU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>do it</p>
        
        
        <div class="post-info">
            image &middot; 152,509 views
        </div>
    </div>
    
</div>

                            <div id="kQQhkDa" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/kQQhkDa" data-page="0">
        <img alt="" src="//i.imgur.com/kQQhkDab.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="kQQhkDa" type="image" data-up="16400">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="kQQhkDa" type="image" data-downs="271">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-kQQhkDa">16,129</span>
                            <span class="points-text-kQQhkDa">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>A man a plan a canal Panama.</p>
        
        
        <div class="post-info">
            image &middot; 4,304,963 views
        </div>
    </div>
    
</div>

                            <div id="gV57xnF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/gV57xnF" data-page="0">
        <img alt="" src="//i.imgur.com/gV57xnFb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="gV57xnF" type="image" data-up="3964">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="gV57xnF" type="image" data-downs="42">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-gV57xnF">3,922</span>
                            <span class="points-text-gV57xnF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Ray Tomlinson, the creator of email, has died - he was literally someone who changed the world.</p>
        
        
        <div class="post-info">
            image &middot; 186,016 views
        </div>
    </div>
    
</div>

                            <div id="iYpWcBm" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/iYpWcBm" data-page="0">
        <img alt="" src="//i.imgur.com/iYpWcBmb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="iYpWcBm" type="image" data-up="1241">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="iYpWcBm" type="image" data-downs="25">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-iYpWcBm">1,216</span>
                            <span class="points-text-iYpWcBm">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>RHM - Really Hot Magma</p>
        
        
        <div class="post-info">
            animated &middot; 116,575 views
        </div>
    </div>
    
</div>

                            <div id="3LmVZ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/3LmVZ" data-page="0">
        <img alt="" src="//i.imgur.com/QjzM5Exb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="3LmVZ" type="image" data-up="4378">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="3LmVZ" type="image" data-downs="84">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-3LmVZ">4,294</span>
                            <span class="points-text-3LmVZ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Calling all Elderscrolls Fans!</p>
        
        
        <div class="post-info">
            album &middot; 61,519 views
        </div>
    </div>
    
</div>

                            <div id="XpGMK" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/XpGMK" data-page="0">
        <img alt="" src="//i.imgur.com/PyCDLjbb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="XpGMK" type="image" data-up="987">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="XpGMK" type="image" data-downs="25">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-XpGMK">962</span>
                            <span class="points-text-XpGMK">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Evil as fuck.</p>
        
        
        <div class="post-info">
            album &middot; 3,753 views
        </div>
    </div>
    
</div>

                            <div id="tBgCZ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/tBgCZ" data-page="0">
        <img alt="" src="//i.imgur.com/LbWxtMjb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="tBgCZ" type="image" data-up="2271">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="tBgCZ" type="image" data-downs="75">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-tBgCZ">2,196</span>
                            <span class="points-text-tBgCZ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Birds with hands</p>
        
        
        <div class="post-info">
            album &middot; 43,582 views
        </div>
    </div>
    
</div>

                            <div id="jYSp7Oy" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/jYSp7Oy" data-page="0">
        <img alt="" src="//i.imgur.com/jYSp7Oyb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="jYSp7Oy" type="image" data-up="1391">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="jYSp7Oy" type="image" data-downs="153">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-jYSp7Oy">1,238</span>
                            <span class="points-text-jYSp7Oy">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>That look of utter defeat at the end.</p>
        
        
        <div class="post-info">
            animated &middot; 129,165 views
        </div>
    </div>
    
</div>

                            <div id="8bNwIR6" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/8bNwIR6" data-page="0">
        <img alt="" src="//i.imgur.com/8bNwIR6b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="8bNwIR6" type="image" data-up="484">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="8bNwIR6" type="image" data-downs="63">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-8bNwIR6">421</span>
                            <span class="points-text-8bNwIR6">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Y&#039;all need Satan</p>
        
        
        <div class="post-info">
            image &middot; 650,375 views
        </div>
    </div>
    
</div>

                            <div id="CO5h3" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/CO5h3" data-page="0">
        <img alt="" src="//i.imgur.com/XAVhjEqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="CO5h3" type="image" data-up="2023">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="CO5h3" type="image" data-downs="72">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-CO5h3">1,951</span>
                            <span class="points-text-CO5h3">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Graphs about things and things about graphs</p>
        
        
        <div class="post-info">
            album &middot; 40,160 views
        </div>
    </div>
    
</div>

                            <div id="cYIeoNy" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/cYIeoNy" data-page="0">
        <img alt="" src="//i.imgur.com/cYIeoNyb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="cYIeoNy" type="image" data-up="1612">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="cYIeoNy" type="image" data-downs="46">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-cYIeoNy">1,566</span>
                            <span class="points-text-cYIeoNy">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW my GF asks if I&#039;m in the mood.</p>
        
        
        <div class="post-info">
            animated &middot; 147,437 views
        </div>
    </div>
    
</div>

                            <div id="dhy2NBF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/dhy2NBF" data-page="0">
        <img alt="" src="//i.imgur.com/dhy2NBFb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="dhy2NBF" type="image" data-up="588">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="dhy2NBF" type="image" data-downs="9">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-dhy2NBF">579</span>
                            <span class="points-text-dhy2NBF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My dog takes care of me when I&#039;m sick, he&#039;s always watching me and he only sleeps when I&#039;m sleeping. I took this picture right after I woke up</p>
        
        
        <div class="post-info">
            image &middot; 875,000 views
        </div>
    </div>
    
</div>

                            <div id="Ep7UoFm" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Ep7UoFm" data-page="0">
        <img alt="" src="//i.imgur.com/Ep7UoFmb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Ep7UoFm" type="image" data-up="1096">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Ep7UoFm" type="image" data-downs="15">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Ep7UoFm">1,081</span>
                            <span class="points-text-Ep7UoFm">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Happy Monday Everyone!</p>
        
        
        <div class="post-info">
            animated &middot; 142,197 views
        </div>
    </div>
    
</div>

                            <div id="x8leZ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/x8leZ" data-page="0">
        <img alt="" src="//i.imgur.com/0HuWd4Vb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="x8leZ" type="image" data-up="15303">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="x8leZ" type="image" data-downs="215">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-x8leZ">15,088</span>
                            <span class="points-text-x8leZ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Mario dissected</p>
        
        
        <div class="post-info">
            album &middot; 179,808 views
        </div>
    </div>
    
</div>

                            <div id="kPuSPDT" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/kPuSPDT" data-page="0">
        <img alt="" src="//i.imgur.com/kPuSPDTb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="kPuSPDT" type="image" data-up="1138">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="kPuSPDT" type="image" data-downs="16">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-kPuSPDT">1,122</span>
                            <span class="points-text-kPuSPDT">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Hand Painting Stripe on Royal Enfield Motorbike Tank</p>
        
        
        <div class="post-info">
            animated &middot; 140,739 views
        </div>
    </div>
    
</div>

                            <div id="h4Df9" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/h4Df9" data-page="0">
        <img alt="" src="//i.imgur.com/iuyAPJwb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="h4Df9" type="image" data-up="8690">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="h4Df9" type="image" data-downs="65">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-h4Df9">8,625</span>
                            <span class="points-text-h4Df9">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Dad reflex.</p>
        
        
        <div class="post-info">
            album &middot; 125,097 views
        </div>
    </div>
    
</div>

                            <div id="NUIiV" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/NUIiV" data-page="0">
        <img alt="" src="//i.imgur.com/0UYvXG1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="NUIiV" type="image" data-up="920">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="NUIiV" type="image" data-downs="19">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-NUIiV">901</span>
                            <span class="points-text-NUIiV">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>THIS IS VERY IMPORTANT</p>
        
        
        <div class="post-info">
            album &middot; 5,285 views
        </div>
    </div>
    
</div>

                            <div id="uoniqcA" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/uoniqcA" data-page="0">
        <img alt="" src="//i.imgur.com/uoniqcAb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="uoniqcA" type="image" data-up="3163">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="uoniqcA" type="image" data-downs="56">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-uoniqcA">3,107</span>
                            <span class="points-text-uoniqcA">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Scything the lawn.</p>
        
        
        <div class="post-info">
            animated &middot; 306,104 views
        </div>
    </div>
    
</div>

                            <div id="PR5wXpd" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/PR5wXpd" data-page="0">
        <img alt="" src="//i.imgur.com/PR5wXpdb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="PR5wXpd" type="image" data-up="1575">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="PR5wXpd" type="image" data-downs="145">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-PR5wXpd">1,430</span>
                            <span class="points-text-PR5wXpd">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Time to learn about firearm mechanics! Part 3</p>
        
        
        <div class="post-info">
            animated &middot; 189,818 views
        </div>
    </div>
    
</div>

                            <div id="3VDlO" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/3VDlO" data-page="0">
        <img alt="" src="//i.imgur.com/9hFgKYvb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="3VDlO" type="image" data-up="8296">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="3VDlO" type="image" data-downs="169">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-3VDlO">8,127</span>
                            <span class="points-text-3VDlO">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The beast</p>
        
        
        <div class="post-info">
            album &middot; 107,711 views
        </div>
    </div>
    
</div>

                            <div id="D7hriG1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/D7hriG1" data-page="0">
        <img alt="" src="//i.imgur.com/D7hriG1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="D7hriG1" type="image" data-up="1327">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="D7hriG1" type="image" data-downs="300">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-D7hriG1">1,027</span>
                            <span class="points-text-D7hriG1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Does not apply to everyone, obviously</p>
        
        
        <div class="post-info">
            image &middot; 49,185 views
        </div>
    </div>
    
</div>

                            <div id="93nLl73" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/93nLl73" data-page="0">
        <img alt="" src="//i.imgur.com/93nLl73b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="93nLl73" type="image" data-up="1544">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="93nLl73" type="image" data-downs="55">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-93nLl73">1,489</span>
                            <span class="points-text-93nLl73">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Yeees yeeees</p>
        
        
        <div class="post-info">
            animated &middot; 201,251 views
        </div>
    </div>
    
</div>

            
        </div>

        <div class="clear"></div>

        <div class="imagelist-loader textbox center lineheight small-margin-top">
            <img src="//s.imgur.com/images/loaders/ddddd1_181817/48.gif" />
        </div>
    


    </div>
            <div class="page-divider"><a href="javascript:;" class="scroll-top-text" data-jafo="{@@event@@:@@galleryBackToTop@@,@@meta@@:{@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@pageNum@@:0}}">back to top</a></div>
    


                    
                </div>
            
                                                            <div id="top-comments" class="left-column">
                                                                                    <div id="outside-tagging" class="br5">
                                    <div class="group-add-done nodisplay"></div>
        
                                    <div id="tags-current-list" class="tag-list tag-list-immutable">
                                                                                        <div class="panel">
        <div class="textbox">
            <h2>Today&#039;s Best Comments</h2>
        </div>
    
        <div class="captions best-captions">
                            <div class="caption-container rank-0">
                    <p class="clickability">
                        <a href="/gallery/rjBl9P3/comment/601174330"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Can we do this again?" src="//i.imgur.com/rjBl9P3b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/ailuropalescent">ailuropalescent</a> 3,353 points
                        </div>
        
                                                    United States
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-1">
                    <p class="clickability">
                        <a href="/gallery/AvhWB/comment/601295345"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Good ol&#039; Uncle Iroh" src="//i.imgur.com/thmx13nb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/FaultySage">FaultySage</a> 3,097 points
                        </div>
        
                                                    Except Azula, bitch is crazy.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-2">
                    <p class="clickability">
                        <a href="/gallery/mRpYt/comment/601242720"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="sassy af" src="//i.imgur.com/srTGLHcb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/Pengarr">Pengarr</a> 2,551 points
                        </div>
        
                                                    The thing that struck me about this interview is how desperately she wanted him to badmouth Hillary and how steadfastly he refused to.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/cg5AnMC/comment/601312335"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="My camera refocused right before I took a picture of some broken glass, and it focused on the trees reflection." src="//i.imgur.com/cg5AnMCb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/theunforgivensinner">theunforgivensinner</a> 2,263 points
                        </div>
        
                                                    Well that&#039;s real fuckin&#039; neato.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/11Mosbr/comment/601102105"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Karma." src="//i.imgur.com/11Mosbrb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/SS250">SS250</a> 2,006 points
                        </div>
        
                                                    Wow, I don&#039;t normally jump on these hate wagons, but that was some straight bitch &#9829; the loser pulled.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/66R8r/comment/601308320"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="This is so true. This election is making me sick" src="//i.imgur.com/RYdfByFb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/TheFuckingRomans">TheFuckingRomans</a> 1,904 points
                        </div>
        
                                                    3rd graders aren&#039;t racist cuz of Trump, they&#039;re racist cuz of shithole parents.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container last-child">
                    <p class="clickability">
                        <a href="/gallery/kQQhkDa/comment/601407055"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="A man a plan a canal Panama." src="//i.imgur.com/kQQhkDab.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/ThatsPermanent">ThatsPermanent</a> 1,875 points
                        </div>
        
                                                    Andrew Bird has a song called Fake Palindromes, based on the idea that none would take the time to verify a long enough palindrome.
                        
                    </div>
                </div>

                <div class="clear"></div>
            
    
            
        </div>
    </div>


                                        
                                    </div>
                                </div>
        
                                <div id="outside-tagging-showhide" class="no-select title-e short" title=" Show Sidebar">
                                                                            <div id="comment-tab">
                                            <div id="comment-icon-one"></div>
                                            <div id="comment-icon-two"></div>
                                            <div id="comment-icon-three"></div>
                                        </div>
                                    
                                </div>
                            
                        
                        </div>
                    
                
            </div>
        

        <div class="clear"></div>
    </div>

    <div class="nodisplay">
        <div id="past-wrapper" class="popup">
            <div>
                <div class="textbox left">
                    

                    <input type="text" id="days" />

                                            days ago
                    
                </div>
                
                <div id="past-calendar-wrapper" class="textbox left">
                    <div id="past-calendar"></div>
                </div>

                <div id="slider-wrapper" class="textbox left">
                    <div class="ticker negative">&laquo;</div>
                    <div id="slider"></div>
                    <div class="ticker positive">&raquo;</div>
                </div>

                <div id="button-wrapper" class="textbox left">
                    <input type="button" class="button-medium" value="Go" id="past-submit" />
                </div>

                <div class="clear"></div>
            </div>

            <div id="past-loader"></div>
            <div id="past-preview"></div>
        </div>
    </div>

    <div class="tag-group-selector-container"></div>
            <div id="top-tag-container"></div>
    

    <input id="sid" type="hidden" value="06d3d9617a2883877c8f5e774e52c741" />
        

    

            
    

    

            <script type="text/javascript">
            (function() {
                var roll = Math.random();
                if(roll < 0.05) {
                    
                    (function(_,e,rr,s){
                        _errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                        _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)
                    })(window,document,"script","51d1e360b05ccb10310000d2");
                    
                }
            })();
        </script>

        
<!--[if lte IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery-1.10.2.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if gt IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<!--<![endif]-->



        <script type="text/javascript" src="//s.imgur.com/min/global.js?1457077774"></script>

        <script type="text/javascript">
            Imgur.Environment = {
                isDev:         false,
                isSubdomain:   Imgur.Util.isSubdomain('imgur.com'),
                host:          'imgur.com',
                cdnUrl:        '//i.imgur.com',
                staticUrl:     '//s.imgur.com',
                signed:        false,
                auth:          {},
                recaptcha_key: '6LeZbt4SAAAAAKEsafT3QzEFp5vJ1-Z23uy5mPDz',
                msid:          '06d3d9617a2883877c8f5e774e52c741',
                beta:          {
                    enabled:   false,
                    inBeta:    false
                },
                
                ctaConfig:     {"anonymous":[{"active":false,"type":"button","trackingName":"anonymous","localStorageName":"cta-anonymous","url":"\/","buttonText":"start exploring","title":"Welcome to Imgur!","description":"Your new home for the Internet's best visual stories.","background":"{STATIC}\/images\/house-cta\/cta-background.jpg","newTab":false,"buttonColor":"#77ac21"}],"user":[{"active":false,"type":"button","trackingName":"campImgur","localStorageName":"cta-camp1418760143","customClass":"u-pl95","url":"\/\/imgur.com\/camp","buttonText":"Get More Details","title":"Camp Imgur, August 2015","description":"We're 50% sold out! Reserve your space now.","background":"{STATIC}\/images\/house-cta\/cta-camp-imgur.jpg","newTab":true,"buttonColor":"#cf5033"}],"gaming":[{"active":true,"type":"button","trackingName":"gaming","localStorageName":"cta-gaming","url":"\/\/imgur.com\/t\/gaming\/top\/week","buttonText":"Check it out!","title":"You are now browsing the Internet on God Mode.","description":"Awesome unlocked! Imgur has gaming content like you wouldn't believe.","background":"{STATIC}\/images\/house-cta\/cta-twitch.jpg","newTab":false,"buttonColor":"#4d3280"}],"facebook":[{"active":true,"type":"custom","jsReactClassName":"Facebook","trackingName":"facebook","localStorageName":"cta-fb141008","title":"Like us on Facebook!","description":"We're bringing funny to your newsfeed. Like Imgur on Facebook!","background":"{STATIC}\/images\/house-cta\/facebook-day.jpg"}],"twitter":[{"active":true,"type":"custom","jsReactClassName":"Twitter","url":"https:\/\/twitter.com\/imgur","trackingName":"twitter","localStorageName":"cta-tw141008","newTab":true,"title":"Follow us on Twitter!","description":""}],"pinterest":[{"active":true,"type":"custom","jsReactClassName":"Pinterest","trackingName":"pinterest","localStorageName":"cta-pinterest-2015-07-14","title":"Follow us on Pinterest!","background":"{STATIC}\/images\/house-cta\/pinterest.png","customClass":"pinterest"}],"promoted":[{"active":true,"type":"custom","jsReactClassName":"Promoted","trackingName":"promoted","localStorageName":false,"buttonText":"Learn More","title":"What Are Promoted Posts?","description":"And why am I seeing them?","background":"{STATIC}\/images\/house-cta\/cta-promoted.jpg","url":"\/\/imgur.com\/blog\/?p=6101","newTab":true,"customClass":"pp-banner"}],"global":[{"active":true,"trackingName":"cta-survey-2016-03-07","localStorageName":"cta-survey-2016-03-07","type":"button","customClass":"u-pl260 cta-dark-text","url":"https:\/\/www.surveymonkey.com\/r\/ZCVCPQH","title":"Hey Imgur!","description":"Have time to take a quick survey?","newTab":true,"buttonText":"YES","buttonColor":"#000000","background":"{STATIC}\/images\/house-cta\/cta-survey.jpg","backgroundColor":"#ffe9d3"},{"active":false,"trackingName":"inCaseYouMissedIt","localStorageName":"cta-icymi-2015-07-14","type":"button","subtype":"app","background":"{STATIC}\/images\/house-cta\/cta-icymi.png","url":"\/\/imgur.com\/topic\/Hand_Picked\/","buttonText":"Take me there","title":"Catch up on the awesome posts you might have missed!","description":"Now featured in the Hand Picked topic.","newTab":false,"customClass":"u-pl260 icymi","buttonColor":"#252525","frontPageOnly":true},{"active":true,"type":"custom","jsReactClassName":"Newsletter","trackingName":"inCaseYouMissedItForm","localStorageName":"apps1440530195","background":"{STATIC}\/images\/house-cta\/cta-icymi-form.png?1440530195","newTab":true,"customClass":"u-pl200"}],"global-lightbox":[{"active":true,"trackingName":"mobileApps","localStorageName":"apps1442873156","type":"custom","jsReactClassName":"GetTheApp","background":"{STATIC}\/images\/space-med-bg.png?1433176979","newTab":true}],"side-gallery":[{"active":false,"localStorageName":"cta-side-whatisimgur","hash":"2gUGa","is_album":true,"variation":"what-is-imgur","readonly":true}]},
                experiments:   {"exp1868":{"active":false,"name":"hamburger-topics","inclusionProbability":1,"expirationDate":"2015-09-30T00:00:00.000Z","variations":[{"name":"hamburger-topics","inclusionProbability":0.5}]},"exp4053":{"active":false,"name":"recs-reddit-new","inclusionProbability":0.2,"expirationDate":"2015-12-30T00:00:00.000Z","variations":[{"name":"recs-new-users","inclusionProbability":0.5}]},"exp3025":{"active":true,"name":"virality-flavors","inclusionProbability":0.1,"expirationDate":"2015-12-17T00:00:00.000Z","variations":[{"name":"beta","inclusionProbability":0},{"name":"gamma","inclusionProbability":0},{"name":"delta","inclusionProbability":0}]},"exp4595":{"active":true,"name":"implicit-promo-optin","inclusionProbability":1,"startDate":1457373622000,"bucketFromDate":1456165800000,"bucketUntilDate":1456597800000,"expirationDate":1458757800000,"variations":[{"name":"control","inclusionProbability":1},{"name":"explicit","inclusionProbability":0},{"name":"implicit","inclusionProbability":0}]}},
                isGalleryAdmin:false,
                ppBlog: '//imgur.com/blog/?p=6101'
            };

            var imgur = Imgur.getInstance();
            imgur.init(_.merge(Imgur.Environment, {
                
                
                beta: {"enabled":false,"inBeta":false,"showInvite":false,"ctaInvite":[],"ctaWelcome":[]},
            }));

                            var uploader = Imgur.Upload.Global.getInstance(Imgur.getInstance()._);

                if(uploader._.el.$uploadGlobal.length > 0) {
                    uploader.bindPasteUpload();
                    uploader.generalInit();
                }
            

            imgur.generalInit();

            

            var _widgetFactory = new Imgur.Factory.Widget(Imgur.Environment);

            _widgetFactory.mergeConfig('global', Imgur.Environment);

            
        </script>

        <script type="text/javascript">
            var __nspid="1mvmyo";
            var __nsptags=[];

            (function() {
                var roll = Math.random();
                if(roll < 0.1) {
                    
                        // This is the NSONE Pulsar tag
                        (function(w, d) { var x = function() {
                        var j=d.createElement("script");j.type="text/javascript";j.async=true;
                        j.src="http"+("https:"===d.location.protocol?"s://cs":"://c")+".ns1p.net/p.js?a="+__nspid;
                        d.body.appendChild(j); }
                        if(w.addEventListener) { w.addEventListener("load", x, false); }
                        else if(w.attachEvent) { w.attachEvent("onload", x); }
                        else { w.onload = x; }
                        }(window, document));

                    
                }
            })();
        </script>

    

    

                    <script type="text/javascript">
            (function(widgetFactory) {
                widgetFactory.mergeConfig('analytics', {
                    isAdmin: false,
                    logPixel: '//p.imgur.com/lumbar.gif'
                });
                widgetFactory.mergeConfig('global', {
                    analyticsAccountId: 'UA-6671908-2'
                });

                widgetFactory.mergeConfig('search', {
                    type: 'global', 
                    q: ''
                });

                widgetFactory.produceJafo();

                widgetFactory.startExperiments();

                                    widgetFactory.produceCtaBanner('/gallery/1TiqbmX');
                
            })(_widgetFactory);
        </script>
    

    <script type="text/javascript">
    (function(widgetFactory) { 
        widgetFactory.mergeConfig('global', {
            sid: '06d3d9617a2883877c8f5e774e52c741'
        });

        widgetFactory.mergeConfig('gallery', {
            sid         : '06d3d9617a2883877c8f5e774e52c741',
            signed      : false,
            baseURL     : decodeURIComponent('%2Fgallery'),
            section     : 'hot',
            sort        : 'viral',
            window      : 'day',
            page        : 0,
            isPro       : false,
            topic       : null,
            tag         : null,
            subreddit   : null,
            maxPage     : 1892,
            showPast    : true,
            searchQuery : '',
            inSearch    : false,
            advSearch   : null,
            bestCaps    : true
        });

        widgetFactory.mergeConfig('groups', {
            groups: {
                
            }
        });

        widgetFactory.mergeConfig('search', {
            
        });
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
        (function(widgetFactory) {
            var group_type = 'base';

            var group = {
                id: '[hot]',
                tags: [
                    
                ]
            }

            widgetFactory.mergeConfig('gallery', {
                section            : 'hot',
                showGalleryNewInfo : false,
                gallery_type       : null,
                topic              : null,
                tag                : null,
                subreddit          : null,
                sponsoredTag       : null,
                tagFollower        : false,
                group_type         : group_type,
                group              : group
            });
        })(_widgetFactory);
    </script>


            <script type="text/javascript">
            if (typeof _widgetFactory !== 'undefined') {
                _widgetFactory.initNotifications({"post":{"postUpvote":[10,50,100,500,1000,5000],"postComment":[10,50,100],"postView":[100000,500000,1000000,5000000],"postEmbed":[1],"postViral":[1]},"comment":{"commentUpvote":[10,100,500,1000,5000,10000],"commentReply":[1],"commentTop7":[1],"commentMention":[1]},"trophy":{"userTrophy":[1]},"account":{"userNotoriety":[399,999,1999,3999,7999,19999,9223372036854775807]},"message":{"message-msg":[1]}}, [{"name":"Forever Alone","reputation":-1},{"name":"Neutral","reputation":399},{"name":"Accepted","reputation":999},{"name":"Liked","reputation":1999},{"name":"Trusted","reputation":3999},{"name":"Idolized","reputation":7999},{"name":"Renowned","reputation":19999},{"name":"Glorious","reputation":9223372036854775807}]);
            }
        </script>
    

        <script type="text/javascript" src="//s.imgur.com/min/index.js?1457077774"></script>

    <script type="text/javascript" src="//s.imgur.com/min/gallery.js?1457077774"></script>

    <script type="text/javascript">
    (function(widgetFactory) {
        var emitter = new Imgur.Emitter();

        widgetFactory.produceLocationCommand(emitter);

        var galleryFactory = widgetFactory.produceOutsideGalleryFactory();
        var galleryGraph = galleryFactory.produceOutsideGallery(emitter);

        var gallery = galleryGraph.gallery,
            history = galleryGraph.history;

        var outsideTaggingWidget = galleryFactory.produceOutsideTaggingWidget(emitter, galleryGraph);

        gallery.outsideInit();

        

        Imgur.Upload.Index.getInstance().generalInit();
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
    Imgur.Util.jafoLog({ 
        event: 'galleryDisplay', 
        meta: Imgur.Gallery.getInstance().getGalleryMetadata() 
    });
    </script>

    

            <script type="text/javascript">
        if (typeof _widgetFactory !== 'undefined') {
            _widgetFactory.produceSearch();
            _widgetFactory.initIdleMonitor();
        }
        </script>
    

        
    

            <script type="text/javascript">
            Imgur.Util.getGoogleTracker();
            __ga('send', 'pageview');
        </script>

        
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        

        

        

        <!-- Begin comScore Tag -->
        <script type="text/javascript">
            var _comscore = _comscore || [];
            _comscore.push({ c1: "2", c2: "7770950" });
            (function() {
                var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
                s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
                el.parentNode.insertBefore(s, el);
            })();
        </script>
        <noscript>
            <img src="http://b.scorecardresearch.com/p?c1=2&c2=7770950&cv=2.0&cj=1" />
        </noscript>
        <!-- End comScore Tag -->

        <!-- Quantcast Tag -->
        <script type="text/javascript">
        var _qevents = _qevents || [];
        (function() {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();
        _qevents.push({
            qacct:"p-f8oruOqDFlMeI"
        });
        </script>

        <noscript>
            <div style="display:none;"><img src="//pixel.quantserve.com/pixel/p-f8oruOqDFlMeI.gif" border="0" height="1" width="1" alt="Quantcast"/></div>
        </noscript>
        <!-- End Quantcast tag -->

        <!-- Facebook Pixel Code -->
        <script type="text/javascript">
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

            fbq('init', '742377892535530');
            fbq('track', 'PageView');

            if (document.location.search === '?reg') {
                fbq('track', 'CompleteRegistration');
            }
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1076474169030064&ev=PageView&noscript=1"/></noscript>
        <!-- End Facebook Pixel Code -->
    

        <script type="text/javascript">
        
        (function(){
            try{
                var msg = '\n      _\n     (_)\n      _ _ __ ___   __ _ _   _ _ __\n     | | \'_ ` _ \\ / _` | | | | \'__|\n     | | | | | | | (_| | |_| | |\n     |_|_| |_| |_|\\__, |\\__,_|_|\n                   __/ |\n                  |___/\n';
                msg += '========================================\nYou opened the console! Know some code,\ndo you? Want to work for one of the best\nstartups around? http://imgur.com/jobs\n========================================\n';
                imcon.log(msg);
            }catch(e){}
        })()
        
    </script>
    

        

</body>
</html>
