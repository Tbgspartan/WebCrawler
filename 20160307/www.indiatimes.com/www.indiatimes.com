<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=110" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=110" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=110"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=110"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=110"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=110"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');fbq("track","ViewContent");</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=110"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0;
var isNewYear = 1;
</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle">
				</span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    
</div><!--pull-ad end-->
</div>
    <!--testing 16-03-08 00:00:04-->

<section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
    
<div class="big-image">
        	<div class="gradient-b"></div>
            <div class="featureds"><span class="border-left">&nbsp;</span> Featured <span class="border-right">&nbsp;</span></div>
            <div class="bid-card-txt">
                                <a href="http://www.indiatimes.com/news/india/an-open-letter-to-bengalureans-from-bellandur-the-poisonous-lake-that-caught-on-fire-251630.html" class="big-card-small">
                    An Open Letter To Bengalureans From Bellandur, The Poisonous Lake That Caught On Fire!                </a>
              <span class="card-line"></span>  
            </div>
            <a href="http://www.indiatimes.com/news/india/an-open-letter-to-bengalureans-from-bellandur-the-poisonous-lake-that-caught-on-fire-251630.html" class="tint"><img src="http://media.indiatimes.in/media/content/2016/Mar/bellandur1big_1457333131_1024x477.jpg"/></a>
</div>
    
        

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

        <div class="feature-list cf"><!--feature-list start-->	
                            <figure>

                        <a href="http://www.indiatimes.com/news/india/rs-6-300-cr-in-debt-vijay-mallya-now-claims-banks-are-victimising-him-251623.html" class=" tint" title="Rs 6,300 Cr In Debt, Vijay Mallya Now Claims Banks Are Victimising Him!">
                        <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Mar/cp_1457327523_1457327528_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/cp_1457327523_1457327528_236x111.jpg"  border="0" alt="Rs 6,300 Cr In Debt, Vijay Mallya Now Claims Banks Are Victimising Him!"/></a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/rs-6-300-cr-in-debt-vijay-mallya-now-claims-banks-are-victimising-him-251623.html" title="Rs 6,300 Cr In Debt, Vijay Mallya Now Claims Banks Are Victimising Him!">
    Rs 6,300 Cr In Debt, Vijay Mallya Now Claims Banks Are Victimising Him!                    </a>
                </figcaption>
                </div><!--feature-list end-->

                    <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/india/this-mahashivratri-a-200-year-old-shiva-temple-will-honour-a-muslim-man-251671.html" title="This Mahashivratri, A 200-Year-Old Shiva Temple Will Honour A Muslim Man" class=" tint">                       
                        <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Mar/ht_1457357584_1457357587_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/ht_1457357584_1457357587_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/this-mahashivratri-a-200-year-old-shiva-temple-will-honour-a-muslim-man-251671.html" title="This Mahashivratri, A 200-Year-Old Shiva Temple Will Honour A Muslim Man">
    This Mahashivratri, A 200-Year-Old Shiva Temple Will Honour A Muslim Man                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/weird/this-chinese-woman-starved-to-her-death-after-being-trapped-in-an-elevator-for-a-month-251669.html" title="This Chinese Woman Starved To Her Death After Being Trapped In An Elevator For A Month" class=" tint">                       
                        <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Mar/640_1457356466_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/640_1457356466_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/weird/this-chinese-woman-starved-to-her-death-after-being-trapped-in-an-elevator-for-a-month-251669.html" title="This Chinese Woman Starved To Her Death After Being Trapped In An Elevator For A Month">
    This Chinese Woman Starved To Her Death After Being Trapped In An Elevator For A Month                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                <figure>
                        
    <a class='video-btn sprite' href='http://www.indiatimes.com/news/india/barkha-dutt-s-reply-to-anupam-kher-s-telegraph-national-debate-speech-will-make-you-think-about-real-azadi-251668.html'>video</a>                    <a href="http://www.indiatimes.com/news/india/barkha-dutt-s-reply-to-anupam-kher-s-telegraph-national-debate-speech-will-make-you-think-about-real-azadi-251668.html" title="Barkha Dutt's Reply To Anupam Kher's Telegraph National Debate Speech Will Make You Think About 'Real' Azadi" class=" tint">                       
                        <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/videocafe/2016/Mar/brakh_1457355642_1457355652_236x111.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2016/Mar/brakh_1457355642_1457355652_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/barkha-dutt-s-reply-to-anupam-kher-s-telegraph-national-debate-speech-will-make-you-think-about-real-azadi-251668.html" title="Barkha Dutt's Reply To Anupam Kher's Telegraph National Debate Speech Will Make You Think About 'Real' Azadi">
    Barkha Dutt's Reply To Anupam Kher's Telegraph National Debate Speech Will Make You Think About 'Real' Azadi                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
            </div><!--news-panel end-->

    <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>

                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/breathe-easy-with-these-7-simple-ways-to-counter-air-pollution-251652.html" class="tint" title="Breathe Easy With These 7 Simple Ways To Counter Air Pollution" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/health/healthyliving/breathe-easy-with-these-7-simple-ways-to-counter-air-pollution-251652.html');">
                        <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Mar/air-pollutioncard_1457345910_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/air-pollutioncard_1457345910_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/breathe-easy-with-these-7-simple-ways-to-counter-air-pollution-251652.html" title="Breathe Easy With These 7 Simple Ways To Counter Air Pollution" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/health/healthyliving/breathe-easy-with-these-7-simple-ways-to-counter-air-pollution-251652.html');">

    Breathe Easy With These 7 Simple Ways To Counter Air Pollution                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/6-pack-band-had-a-happy-fan-moment-with-srk-it-was-nothing-short-of-perfection-251665.html" class="tint" title="'6 Pack Band' Had A Happy Fan Moment With SRK & It Was Nothing Short Of Perfection!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/celebs/6-pack-band-had-a-happy-fan-moment-with-srk-it-was-nothing-short-of-perfection-251665.html');">
                        <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Mar/srk-6-pack-card_1457354571_1457354578_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/srk-6-pack-card_1457354571_1457354578_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/6-pack-band-had-a-happy-fan-moment-with-srk-it-was-nothing-short-of-perfection-251665.html" title="'6 Pack Band' Had A Happy Fan Moment With SRK & It Was Nothing Short Of Perfection!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/celebs/6-pack-band-had-a-happy-fan-moment-with-srk-it-was-nothing-short-of-perfection-251665.html');">

    '6 Pack Band' Had A Happy Fan Moment With SRK & It Was Nothing Short Of Perfection!                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/a-fan-dedicated-a-trailer-to-srk-ended-up-getting-a-job-offer-from-king-khan-himself-251664.html" class="tint" title="A Fan Dedicated A Trailer To SRK & Ended Up Getting A Job Offer From King Khan Himself!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/celebs/a-fan-dedicated-a-trailer-to-srk-ended-up-getting-a-job-offer-from-king-khan-himself-251664.html');">
                        <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Mar/kk2_1457353366_1457353375_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/kk2_1457353366_1457353375_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/a-fan-dedicated-a-trailer-to-srk-ended-up-getting-a-job-offer-from-king-khan-himself-251664.html" title="A Fan Dedicated A Trailer To SRK & Ended Up Getting A Job Offer From King Khan Himself!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/celebs/a-fan-dedicated-a-trailer-to-srk-ended-up-getting-a-job-offer-from-king-khan-himself-251664.html');">

    A Fan Dedicated A Trailer To SRK & Ended Up Getting A Job Offer From King Khan Himself!                        </a>
                    </div>
                </figcaption>
            </div>
                    
    </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
                    
            <div class="trending-panel-list cf" id="column3_0">
                <figure>
                                <a href="http://www.indiatimes.com/culture/who-we-are/7-things-you-can-do-for-your-boyfriend-to-show-him-that-you-love-him-250889.html" class="tint" title="7 Things You Can Do For Your Boyfriend To Show Him That You Love Him" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/7-things-you-can-do-for-your-boyfriend-to-show-him-that-you-love-him-250889.html');">
                        <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Mar/card1_1457337295_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card1_1457337295_236x111.jpg" border="0" alt="7 Things You Can Do For Your Boyfriend To Show Him That You Love Him"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container partner-cont">
                         <div class="partner"><a href="javascript:void(0);" title="">PARTNER</a></div>
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/7-things-you-can-do-for-your-boyfriend-to-show-him-that-you-love-him-250889.html" title="7 Things You Can Do For Your Boyfriend To Show Him That You Love Him">
    7 Things You Can Do For Your Boyfriend To Show Him That You Love Him                    </a>
                         </div>
                </figcaption>
            </div>        
        
        
                
            <div class="trending-panel-list cf" id="column3_1">
                <figure>
                                <a href="http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html" class="tint" title="These 7 Successful Celebrity Siblings Could Have Been Possible Rivals But Joined Forces Instead." onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html');">
                        <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Mar/card_1457090521_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card_1457090521_236x111.jpg" border="0" alt="These 7 Successful Celebrity Siblings Could Have Been Possible Rivals But Joined Forces Instead."/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container partner-cont">
                         <div class="partner"><a href="javascript:void(0);" title="">PARTNER</a></div>
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html" title="These 7 Successful Celebrity Siblings Could Have Been Possible Rivals But Joined Forces Instead.">
    These 7 Successful Celebrity Siblings Could Have Been Possible Rivals But Joined Forces Instead.                    </a>
                         </div>
                </figcaption>
            </div>        
        
        
                
                        <div class="trending-panel-list cf" id="column3_0">
    <span class="strip skyblue-bg"></span>                <figure>
                        <a href="http://www.indiatimes.com/entertainment/celebs/a-fan-dedicated-a-trailer-to-srk-ended-up-getting-a-job-offer-from-king-khan-himself-251664.html" class="tint" title="A Fan Dedicated A Trailer To SRK & Ended Up Getting A Job Offer From King Khan Himself!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html');">
                        <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Mar/kk2_1457353366_1457353375_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/kk2_1457353366_1457353375_236x111.jpg" border="0" alt="A Fan Dedicated A Trailer To SRK & Ended Up Getting A Job Offer From King Khan Himself!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/a-fan-dedicated-a-trailer-to-srk-ended-up-getting-a-job-offer-from-king-khan-himself-251664.html" title="A Fan Dedicated A Trailer To SRK & Ended Up Getting A Job Offer From King Khan Himself! " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html');">
    A Fan Dedicated A Trailer To SRK & Ended Up Getting A Job Offer From King Khan Himself!                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_1">
                    <figure>
                        <a href="http://www.indiatimes.com/entertainment/celebs/14-behind-the-scenes-pics-of-villains-heroes-chilling-together-will-give-you-squad-goals-251633.html" class="tint" title="14 Behind-The-Scenes Pics Of Villains & Heroes Chilling Together Will Give You Squad Goals!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html');">
                        <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Mar/hp_1457334356_1457334368_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/hp_1457334356_1457334368_236x111.jpg" border="0" alt="14 Behind-The-Scenes Pics Of Villains & Heroes Chilling Together Will Give You Squad Goals!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/14-behind-the-scenes-pics-of-villains-heroes-chilling-together-will-give-you-squad-goals-251633.html" title="14 Behind-The-Scenes Pics Of Villains & Heroes Chilling Together Will Give You Squad Goals! " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html');">
    14 Behind-The-Scenes Pics Of Villains & Heroes Chilling Together Will Give You Squad Goals!                    </a>
                   </div>     
                </figcaption>
            </div>
        </div><!--trending-panel end-->
</section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
    <div id="bigAd1_slot"></div>
    <script>
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

<section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>
                    <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/this-women-s-day-let-s-remember-a-woman-who-died-fighting-against-kerala-s-inhuman-breast-tax-251666.html" title="This Women's Day, Let's Remember A Woman Who Died Fighting Against Kerala's Inhuman 'Breast Tax'" class=" tint">


                        <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/thewire8_1457354893_1457354896_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/this-women-s-day-let-s-remember-a-woman-who-died-fighting-against-kerala-s-inhuman-breast-tax-251666.html" title="This Women's Day, Let's Remember A Woman Who Died Fighting Against Kerala's Inhuman 'Breast Tax'">
    This Women's Day, Let's Remember A Woman Who Died Fighting Against Kerala's Inhuman 'Breast Tax'                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-4"  data-slot="129061" data-position="4" data-section="0" data-cb="adwidgetNew"><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/he-offered-rs-11-lakh-to-anyone-who-would-chop-off-kanhaiya-s-tongue-but-this-leader-only-has-150-rupees-in-his-account-251659.html" title="He Offered Rs 11 Lakh To Anyone Who Would Kill Kanhaiya, But This Leader Only Has 150 Rupees In His Account" class=" tint">


                        <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/jnu64_1457350258_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/he-offered-rs-11-lakh-to-anyone-who-would-chop-off-kanhaiya-s-tongue-but-this-leader-only-has-150-rupees-in-his-account-251659.html" title="He Offered Rs 11 Lakh To Anyone Who Would Kill Kanhaiya, But This Leader Only Has 150 Rupees In His Account">
    He Offered Rs 11 Lakh To Anyone Who Would Kill Kanhaiya, But This Leader Only Has 150 Rupees In His Account                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/world/15-images-that-capture-india-s-most-ordinary-moments-in-an-extraordinary-way-251648.html" title="15 Images That Capture India's Most Ordinary Moments In An Extraordinary Way!" class=" tint">


                        <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/600_1457344082_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/15-images-that-capture-india-s-most-ordinary-moments-in-an-extraordinary-way-251648.html" title="15 Images That Capture India's Most Ordinary Moments In An Extraordinary Way!">
    15 Images That Capture India's Most Ordinary Moments In An Extraordinary Way!                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/victim-of-road-accident-accuses-smriti-irani-of-denying-help-says-her-father-could-have-lived-if-the-minister-had-helped-251658.html" title="Road Accident Victim Accuses Smriti Irani Of Denying Help, Says Her Father Could Have Lived If  The Minister Had Helped" class=" tint">


                        <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/600_1457349520_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/victim-of-road-accident-accuses-smriti-irani-of-denying-help-says-her-father-could-have-lived-if-the-minister-had-helped-251658.html" title="Road Accident Victim Accuses Smriti Irani Of Denying Help, Says Her Father Could Have Lived If  The Minister Had Helped">
    Road Accident Victim Accuses Smriti Irani Of Denying Help, Says Her Father Could Have Lived If  The Minister Had Helped                    </a>
                </figcaption> 
            </div>
                </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
                        
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-of-dead-fish-washing-up-on-the-shores-of-bengaluru-s-lake-will-break-your-heart-251660.html'>video</a>                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-dead-fish-washing-up-on-the-shores-of-bengaluru-s-lake-will-break-your-heart-251660.html" class="tint" title="This Video Of Dead Fish Washing Up On The Shores Of Bengaluru's Lake Will Break Your Heart!">
                        <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/deadfishes_card_1457350165_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/videocafe/this-video-of-dead-fish-washing-up-on-the-shores-of-bengaluru-s-lake-will-break-your-heart-251660.html" title="This Video Of Dead Fish Washing Up On The Shores Of Bengaluru's Lake Will Break Your Heart!">
    This Video Of Dead Fish Washing Up On The Shores Of Bengaluru's Lake Will Break Your Heart!                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-times-your-inner-voice-really-screwed-things-up-for-you-251435.html" class="tint" title="11 Times Your Inner Voice Really Screwed Things Up For You">
                        <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picture_1456912982_1456912994_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/11-times-your-inner-voice-really-screwed-things-up-for-you-251435.html" title="11 Times Your Inner Voice Really Screwed Things Up For You">
    11 Times Your Inner Voice Really Screwed Things Up For You                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/kapil-sharma-s-latest-show-will-have-brand-new-characters-and-a-street-based-theme-251649.html" class="tint" title="Kapil Sharma's Latest Show Will Have Brand New Characters And A Street Based Theme">
                        <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/new-card_1457346831_1457346836_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/kapil-sharma-s-latest-show-will-have-brand-new-characters-and-a-street-based-theme-251649.html" title="Kapil Sharma's Latest Show Will Have Brand New Characters And A Street Based Theme">
    Kapil Sharma's Latest Show Will Have Brand New Characters And A Street Based Theme                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/entertainment/hollywood/even-the-most-die-hard-fans-of-friends-tv-show-didn-t-recognise-these-body-doubles-251627.html" class="tint" title="Even The Most Die-Hard Fans Of FRIENDS TV Show Didn't Recognise These Body Doubles!">

                        <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457330103_236x111.jpg" border="0" alt="Even The Most Die-Hard Fans Of FRIENDS TV Show Didn't Recognise These Body Doubles!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/even-the-most-die-hard-fans-of-friends-tv-show-didn-t-recognise-these-body-doubles-251627.html" title="Even The Most Die-Hard Fans Of FRIENDS TV Show Didn't Recognise These Body Doubles!">
            Even The Most Die-Hard Fans Of FRIENDS TV Show Didn't Recognise These Body Doubles!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf colombia"  id="div-clmb-ctn-129061-2"  data-slot="129061" data-position="2" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/entertainment/celebs/a-fan-asked-srk-about-how-he-copes-up-with-failures-he-came-up-with-the-best-reply-ever-251607.html" class="tint" title="A Fan Asked SRK About How He 'Copes Up With Failures' & He Came Up With The Best Reply Ever!">

                        <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/aa_1457256724_1457256736_236x111.jpg" border="0" alt="A Fan Asked SRK About How He 'Copes Up With Failures' & He Came Up With The Best Reply Ever!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/a-fan-asked-srk-about-how-he-copes-up-with-failures-he-came-up-with-the-best-reply-ever-251607.html" title="A Fan Asked SRK About How He 'Copes Up With Failures' & He Came Up With The Best Reply Ever!">
            A Fan Asked SRK About How He 'Copes Up With Failures' & He Came Up With The Best Reply Ever!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/culture/who-we-are/meet-arunachalam-muruganantham-the-mentruation-man-who-is-one-of-india-s-most-well-known-social-entrepreneurs-251615.html" class="tint" title="Meet Arunachalam Muruganantham, The Menstruation Man Who Is One Of India's Most Well-Known Social Entrepreneurs">

                        <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/z_1457270919_1457270931_236x111.jpg" border="0" alt="Meet Arunachalam Muruganantham, The Menstruation Man Who Is One Of India's Most Well-Known Social Entrepreneurs"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/meet-arunachalam-muruganantham-the-mentruation-man-who-is-one-of-india-s-most-well-known-social-entrepreneurs-251615.html" title="Meet Arunachalam Muruganantham, The Menstruation Man Who Is One Of India's Most Well-Known Social Entrepreneurs">
            Meet Arunachalam Muruganantham, The Menstruation Man Who Is One Of India's Most Well-Known Social Entrepreneurs                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/culture/who-we-are/varun-pruthi-bought-57-books-from-a-68-yo-man-showed-us-that-humanity-has-not-lost-its-way-251606.html" class="tint" title="This Inspiring Story Of A 68-Year-Old Book Seller Will Teach You A Valuable Lesson About Life!">

                        <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/sq_1457256629_1457256641_236x111.jpg" border="0" alt="This Inspiring Story Of A 68-Year-Old Book Seller Will Teach You A Valuable Lesson About Life!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/varun-pruthi-bought-57-books-from-a-68-yo-man-showed-us-that-humanity-has-not-lost-its-way-251606.html" title="This Inspiring Story Of A 68-Year-Old Book Seller Will Teach You A Valuable Lesson About Life!">
            This Inspiring Story Of A 68-Year-Old Book Seller Will Teach You A Valuable Lesson About Life!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div>

</section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
    <div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

<section id="hp_block_3" class="container cf"><!--container start-->

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/sports/8-times-mahendra-singh-dhoni-has-finished-matches-for-india-with-a-six-251655.html" title="9 Times Mahendra Singh Dhoni Won Matches For India With A Celebratory Six" class=" tint">
            

                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dhonisix640_1457346692_236x111.jpg" border="0" alt="9 Times Mahendra Singh Dhoni Won Matches For India With A Celebratory Six"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/8-times-mahendra-singh-dhoni-has-finished-matches-for-india-with-a-six-251655.html" title="9 Times Mahendra Singh Dhoni Won Matches For India With A Celebratory Six">
            9 Times Mahendra Singh Dhoni Won Matches For India With A Celebratory Six                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-8"  data-slot="129061" data-position="8" data-section="0" data-cb="adwidgetNew">

                <figure>

                    <a href="http://www.indiatimes.com/news/india/yet-another-miracle-from-baba-ramdev-patanjali-now-sells-products-imported-from-future-251654.html" title="Yet Another Miracle From Baba Ramdev, Patanjali Now Sells Products Imported From Future!" class=" tint">
            

                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card-ramdev-pantajali-october-2016-amla-murabba1_1457346490_236x111.jpg" border="0" alt="Yet Another Miracle From Baba Ramdev, Patanjali Now Sells Products Imported From Future!"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/yet-another-miracle-from-baba-ramdev-patanjali-now-sells-products-imported-from-future-251654.html" title="Yet Another Miracle From Baba Ramdev, Patanjali Now Sells Products Imported From Future!">
            Yet Another Miracle From Baba Ramdev, Patanjali Now Sells Products Imported From Future!                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/sports/balbir-singh-senior-an-indian-hockey-legend-and-olympic-gold-medalist-is-forgotten-by-his-own-people-251650.html" title="Balbir Singh Senior, Indian Hockey Legend And Three-Time Olympic Gold Medalist, Is Forgotten By His Own People" class=" tint">
            

                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/balbir640_1457345449_236x111.jpg" border="0" alt="Balbir Singh Senior, Indian Hockey Legend And Three-Time Olympic Gold Medalist, Is Forgotten By His Own People"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/balbir-singh-senior-an-indian-hockey-legend-and-olympic-gold-medalist-is-forgotten-by-his-own-people-251650.html" title="Balbir Singh Senior, Indian Hockey Legend And Three-Time Olympic Gold Medalist, Is Forgotten By His Own People">
            Balbir Singh Senior, Indian Hockey Legend And Three-Time Olympic Gold Medalist, Is Forgotten By His Own People                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/india/why-can-t-muslim-women-have-four-husbands-if-their-husbands-are-allowed-four-wives-asks-this-high-court-judge-in-kerala-251647.html" title="Why Can't Muslim Women Have Four Husbands If Their Husbands Are Allowed Four Wives, Asks This High Court Judge" class=" tint">
            

                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/rtx1se5b6yyy_1457342207_1457342209_236x111.jpg" border="0" alt="Why Can't Muslim Women Have Four Husbands If Their Husbands Are Allowed Four Wives, Asks This High Court Judge"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/why-can-t-muslim-women-have-four-husbands-if-their-husbands-are-allowed-four-wives-asks-this-high-court-judge-in-kerala-251647.html" title="Why Can't Muslim Women Have Four Husbands If Their Husbands Are Allowed Four Wives, Asks This High Court Judge">
            Why Can't Muslim Women Have Four Husbands If Their Husbands Are Allowed Four Wives, Asks This High Court Judge                    </a>
                </figcaption> 
            </div>
        </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
         
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/anushka-sharma-s-fitness-tips-will-compel-you-to-renew-your-gym-membership-today-251641.html" class="tint" title="Anushka Sharma's Fitness Tips Will Compel You To Renew Your Gym Membership Today!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/anushka-sharma-sultan_1457338428_1457338434_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/celebs/anushka-sharma-s-fitness-tips-will-compel-you-to-renew-your-gym-membership-today-251641.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/anushka-sharma-s-fitness-tips-will-compel-you-to-renew-your-gym-membership-today-251641.html" title="Anushka Sharma's Fitness Tips Will Compel You To Renew Your Gym Membership Today!">
            Anushka Sharma's Fitness Tips Will Compel You To Renew Your Gym Membership Today!                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-unhealthy-things-people-do-to-keep-themselves-awake-during-exam-time-251642.html" class="tint" title="5 Unhealthy Things People Do To Keep Themselves Awake During Exam Time!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1457343379_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/healthyliving/5-unhealthy-things-people-do-to-keep-themselves-awake-during-exam-time-251642.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/5-unhealthy-things-people-do-to-keep-themselves-awake-during-exam-time-251642.html" title="5 Unhealthy Things People Do To Keep Themselves Awake During Exam Time!">
            5 Unhealthy Things People Do To Keep Themselves Awake During Exam Time!                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-team-rescued-a-wild-dog-from-suffocating-to-death-after-he-got-his-head-stuck-in-a-jar-251620.html'>video</a>                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-team-rescued-a-wild-dog-from-suffocating-to-death-after-he-got-his-head-stuck-in-a-jar-251620.html" class="tint" title="This Team Rescued A Wild Dog From Suffocating To Death After He Got His Head Stuck In A Jar!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/dog_card_1457286254_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/this-team-rescued-a-wild-dog-from-suffocating-to-death-after-he-got-his-head-stuck-in-a-jar-251620.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/videocafe/this-team-rescued-a-wild-dog-from-suffocating-to-death-after-he-got-his-head-stuck-in-a-jar-251620.html" title="This Team Rescued A Wild Dog From Suffocating To Death After He Got His Head Stuck In A Jar!">
            This Team Rescued A Wild Dog From Suffocating To Death After He Got His Head Stuck In A Jar!                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/arjun-kapoor-kareena-kapoor-take-a-dig-at-gender-stereotypes-society-s-hypocrisy-in-this-funny-video-251611.html" class="tint" title="Arjun Kapoor, Kareena Kapoor Take A Dig At Gender Stereotypes & Society's Hypocrisy In This Funny Video!">

                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/kiandka_1457265511_1457265522_236x111.jpg" border="0" alt="Arjun Kapoor, Kareena Kapoor Take A Dig At Gender Stereotypes & Society's Hypocrisy In This Funny Video!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/arjun-kapoor-kareena-kapoor-take-a-dig-at-gender-stereotypes-society-s-hypocrisy-in-this-funny-video-251611.html" title="Arjun Kapoor, Kareena Kapoor Take A Dig At Gender Stereotypes & Society's Hypocrisy In This Funny Video!">
    Arjun Kapoor, Kareena Kapoor Take A Dig At Gender Stereotypes & Society's Hypocrisy In This Funny Video!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-6"  data-slot="129061" data-position="6" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html" class="tint" title="These 7 Successful Celebrity Siblings Could Have Been Possible Rivals But Joined Forces Instead.">

                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457090521_236x111.jpg" border="0" alt="These 7 Successful Celebrity Siblings Could Have Been Possible Rivals But Joined Forces Instead."/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/these-7-successful-celebrity-siblings-could-have-been-possible-rivals-but-joined-forces-instead-251484.html" title="These 7 Successful Celebrity Siblings Could Have Been Possible Rivals But Joined Forces Instead.">
    These 7 Successful Celebrity Siblings Could Have Been Possible Rivals But Joined Forces Instead.                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/lifestyle/self/13-life-lessons-we-forgot-to-learn-from-our-favorite-fictional-characters-251481.html" class="tint" title="13 Life Lessons We Forgot To Learn From Our Favorite Fictional Characters">

                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/batman1_1456993145_236x111.jpg" border="0" alt="13 Life Lessons We Forgot To Learn From Our Favorite Fictional Characters"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/13-life-lessons-we-forgot-to-learn-from-our-favorite-fictional-characters-251481.html" title="13 Life Lessons We Forgot To Learn From Our Favorite Fictional Characters">
    13 Life Lessons We Forgot To Learn From Our Favorite Fictional Characters                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/culture/who-we-are/9-towns-from-our-history-books-we-d-love-to-visit-today-251439.html" class="tint" title="9 Towns From Our History Books Weâd Love To Visit Today">

                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dvarka1_1456916679_236x111.jpg" border="0" alt="9 Towns From Our History Books Weâd Love To Visit Today"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/9-towns-from-our-history-books-we-d-love-to-visit-today-251439.html" title="9 Towns From Our History Books Weâd Love To Visit Today">
    9 Towns From Our History Books Weâd Love To Visit Today                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div><!--trending-panel end-->

</section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
    <div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

<section class="container cf" id="container4"><!--container start-->
    <div class="news-panel cf ">
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/weird/dog-takes-truck-for-a-spin-ends-up-crashing-it-251636.html" title="Dog Takes Truck For A Spin, Ends Up Crashing It!" class=" tint">
                                                <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/56da1b01a8c4d image_1457336520_1457336536_236x111.jpg" border="0" alt="Dog Takes Truck For A Spin, Ends Up Crashing It!"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/weird/dog-takes-truck-for-a-spin-ends-up-crashing-it-251636.html" title="Dog Takes Truck For A Spin, Ends Up Crashing It!">
        Dog Takes Truck For A Spin, Ends Up Crashing It!                    </a>
                </figcaption> 
            </div>
                <div class='container1'>            <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-12"  data-slot="129061" data-position="12" data-section="0" data-cb="adwidgetNew">
                <figure>                      
                    <a href="http://www.indiatimes.com/news/sports/lionel-messi-scores-hat-trick-as-barcelona-set-spanish-record-of-35-unbeaten-matches-251640.html" title="Lionel Messi Scores Hat-Trick As Barcelona Set Spanish Record Of 35 Unbeaten Matches" class=" tint">
                                                <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/messigoal640_1457337884_236x111.jpg" border="0" alt="Lionel Messi Scores Hat-Trick As Barcelona Set Spanish Record Of 35 Unbeaten Matches"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/lionel-messi-scores-hat-trick-as-barcelona-set-spanish-record-of-35-unbeaten-matches-251640.html" title="Lionel Messi Scores Hat-Trick As Barcelona Set Spanish Record Of 35 Unbeaten Matches">
        Lionel Messi Scores Hat-Trick As Barcelona Set Spanish Record Of 35 Unbeaten Matches                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/world/billionaire-who-brought-billions-to-iran-government-by-selling-oil-amidst-sanctions-now-sentenced-to-death-for-corruption-251637.html" title="Billionaire Who Brought Billions To Iran Govt By Selling Oil Amidst Sanctions, Now Sentenced To Death For Corruption" class=" tint">
                                                <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/thetruthseeker_1457336876_1457336881_236x111.jpg" border="0" alt="Billionaire Who Brought Billions To Iran Govt By Selling Oil Amidst Sanctions, Now Sentenced To Death For Corruption"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/billionaire-who-brought-billions-to-iran-government-by-selling-oil-amidst-sanctions-now-sentenced-to-death-for-corruption-251637.html" title="Billionaire Who Brought Billions To Iran Govt By Selling Oil Amidst Sanctions, Now Sentenced To Death For Corruption">
        Billionaire Who Brought Billions To Iran Govt By Selling Oil Amidst Sanctions, Now Sentenced To Death For Corruption                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/world/terrorist-attack-in-yemen-leaves-16-dead-including-four-nuns-from-mother-teresa-s-missionaries-of-charity-251634.html" title="Terrorist Attack In Yemen Leaves 16 Dead Including Four Nuns From Mother Teresa's Missionaries Of Charity" class=" tint">
                                                <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/nuns600_1457335280_236x111.jpg" border="0" alt="Terrorist Attack In Yemen Leaves 16 Dead Including Four Nuns From Mother Teresa's Missionaries Of Charity"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/terrorist-attack-in-yemen-leaves-16-dead-including-four-nuns-from-mother-teresa-s-missionaries-of-charity-251634.html" title="Terrorist Attack In Yemen Leaves 16 Dead Including Four Nuns From Mother Teresa's Missionaries Of Charity">
        Terrorist Attack In Yemen Leaves 16 Dead Including Four Nuns From Mother Teresa's Missionaries Of Charity                    </a>
                </figcaption> 
            </div>
                 
    </div>
</div><!--news-panel end-->

<div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
        
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/after-madadev-mohit-raina-will-now-play-chakravartin-samrat-ashoka-on-indian-television-251639.html" class="tint" title="After Mahadev, Mohit Raina Will Now Play Chakravartin Samrat Ashoka On Indian Television!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/shiva-card_1457337801_1457337809_502x234.jpg" border="0" alt="After Mahadev, Mohit Raina Will Now Play Chakravartin Samrat Ashoka On Indian Television!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/after-madadev-mohit-raina-will-now-play-chakravartin-samrat-ashoka-on-indian-television-251639.html" title="After Mahadev, Mohit Raina Will Now Play Chakravartin Samrat Ashoka On Indian Television!">
    After Mahadev, Mohit Raina Will Now Play Chakravartin Samrat Ashoka On Indian Television!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/mercedes-benz-just-congratulated-bmw-on-completing-100-years-in-the-classiest-way-possible-251644.html" class="tint" title="Mercedes-Benz Just Congratulated BMW On Completing 100 Years In The Classiest Way Possible!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card-1_1457345413_502x234.jpg" border="0" alt="Mercedes-Benz Just Congratulated BMW On Completing 100 Years In The Classiest Way Possible!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/mercedes-benz-just-congratulated-bmw-on-completing-100-years-in-the-classiest-way-possible-251644.html" title="Mercedes-Benz Just Congratulated BMW On Completing 100 Years In The Classiest Way Possible!">
    Mercedes-Benz Just Congratulated BMW On Completing 100 Years In The Classiest Way Possible!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/14-behind-the-scenes-pics-of-villains-heroes-chilling-together-will-give-you-squad-goals-251633.html" class="tint" title="14 Behind-The-Scenes Pics Of Villains & Heroes Chilling Together Will Give You Squad Goals!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/hp_1457334356_1457334368_502x234.jpg" border="0" alt="14 Behind-The-Scenes Pics Of Villains & Heroes Chilling Together Will Give You Squad Goals!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/14-behind-the-scenes-pics-of-villains-heroes-chilling-together-will-give-you-squad-goals-251633.html" title="14 Behind-The-Scenes Pics Of Villains & Heroes Chilling Together Will Give You Squad Goals!">
    14 Behind-The-Scenes Pics Of Villains & Heroes Chilling Together Will Give You Squad Goals!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/delhi-cops-stops-this-haryana-vip-for-breaking-traffic-rules-and-films-his-violent-response-welldone-251653.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/videocafe/delhi-cops-stops-this-haryana-vip-for-breaking-traffic-rules-and-films-his-violent-response-welldone-251653.html" class="tint" title="Delhi Cops Stops This Haryana 'VIP' For Breaking Traffic Rules, And Films His Violent Response #WellDone">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/inld_card_1457346120_502x234.jpg" border="0" alt="Delhi Cops Stops This Haryana 'VIP' For Breaking Traffic Rules, And Films His Violent Response #WellDone" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/videocafe/delhi-cops-stops-this-haryana-vip-for-breaking-traffic-rules-and-films-his-violent-response-welldone-251653.html" title="Delhi Cops Stops This Haryana 'VIP' For Breaking Traffic Rules, And Films His Violent Response #WellDone">
    Delhi Cops Stops This Haryana 'VIP' For Breaking Traffic Rules, And Films His Violent Response #WellDone                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/9-email-hacks-you-should-follow-for-better-clearer-communication-251578.html" class="tint" title="9 Email Hacks You Should Follow For Better, Clearer Communication">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp_1457165424_1457165428_502x234.jpg" border="0" alt="9 Email Hacks You Should Follow For Better, Clearer Communication" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/9-email-hacks-you-should-follow-for-better-clearer-communication-251578.html" title="9 Email Hacks You Should Follow For Better, Clearer Communication">
    9 Email Hacks You Should Follow For Better, Clearer Communication                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/tips-tricks/10-healthy-reasons-to-include-cardamom-in-your-everyday-diet-251540.html" class="tint" title="10 Healthy Reasons To Include Cardamom In Your Everyday Diet">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card-black_and_green_cardamom-wikimedia_1457080572_502x234.jpg" border="0" alt="10 Healthy Reasons To Include Cardamom In Your Everyday Diet" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/tips-tricks/10-healthy-reasons-to-include-cardamom-in-your-everyday-diet-251540.html" title="10 Healthy Reasons To Include Cardamom In Your Everyday Diet">
    10 Healthy Reasons To Include Cardamom In Your Everyday Diet                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/i-lost-weight-to-be-fitter-not-to-look-better-says-parineeti-chopra-251632.html" class="tint" title="I Lost Weight To Be Fitter, Not To Look Better, Says Parineeti Chopra">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457334462_1457334466_502x234.jpg" border="0" alt="I Lost Weight To Be Fitter, Not To Look Better, Says Parineeti Chopra" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/i-lost-weight-to-be-fitter-not-to-look-better-says-parineeti-chopra-251632.html" title="I Lost Weight To Be Fitter, Not To Look Better, Says Parineeti Chopra">
    I Lost Weight To Be Fitter, Not To Look Better, Says Parineeti Chopra                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/mountaineer-david-liano-to-climb-mt-everest-for-the-6th-time-this-one-will-be-for-deepika-padukone-251628.html" class="tint" title="Mountaineer David Liano To Climb Mt. Everest For The 6th Time & This One Will Be For Deepika Padukone!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/aa1_1457331244_1457331252_502x234.jpg" border="0" alt="Mountaineer David Liano To Climb Mt. Everest For The 6th Time & This One Will Be For Deepika Padukone!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/mountaineer-david-liano-to-climb-mt-everest-for-the-6th-time-this-one-will-be-for-deepika-padukone-251628.html" title="Mountaineer David Liano To Climb Mt. Everest For The 6th Time & This One Will Be For Deepika Padukone!">
    Mountaineer David Liano To Climb Mt. Everest For The 6th Time & This One Will Be For Deepika Padukone!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/ever-wondered-what-really-makes-serial-killers-want-to-murder-people-251514.html" class="tint" title="Ever Wondered What Really Makes Serial Killers Want To Murder People?">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457010506_502x234.jpg" border="0" alt="Ever Wondered What Really Makes Serial Killers Want To Murder People?" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/ever-wondered-what-really-makes-serial-killers-want-to-murder-people-251514.html" title="Ever Wondered What Really Makes Serial Killers Want To Murder People?">
    Ever Wondered What Really Makes Serial Killers Want To Murder People?                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/ray-tomlinson-father-of-modern-email-the-greatest-invention-of-20th-century-has-passed-away-251626.html" class="tint" title="Ray Tomlinson, Father Of Modern Email - The Greatest Invention Of 20th Century Has Passed Away">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp1_1457329090_1457329094_502x234.jpg" border="0" alt="Ray Tomlinson, Father Of Modern Email - The Greatest Invention Of 20th Century Has Passed Away" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/ray-tomlinson-father-of-modern-email-the-greatest-invention-of-20th-century-has-passed-away-251626.html" title="Ray Tomlinson, Father Of Modern Email - The Greatest Invention Of 20th Century Has Passed Away">
    Ray Tomlinson, Father Of Modern Email - The Greatest Invention Of 20th Century Has Passed Away                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/even-the-most-die-hard-fans-of-friends-tv-show-didn-t-recognise-these-body-doubles-251627.html" class="tint" title="Even The Most Die-Hard Fans Of FRIENDS TV Show Didn't Recognise These Body Doubles!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457330103_502x234.jpg" border="0" alt="Even The Most Die-Hard Fans Of FRIENDS TV Show Didn't Recognise These Body Doubles!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/even-the-most-die-hard-fans-of-friends-tv-show-didn-t-recognise-these-body-doubles-251627.html" title="Even The Most Die-Hard Fans Of FRIENDS TV Show Didn't Recognise These Body Doubles!">
    Even The Most Die-Hard Fans Of FRIENDS TV Show Didn't Recognise These Body Doubles!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/instead-of-women-s-day-there-should-be-a-good-human-being-day-says-sunny-leone-251624.html" class="tint" title="Instead Of Women's Day There Should Be A 'Good Human Being Day', Says Sunny Leone">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/sunny-leone1_1457328137_1457328149_502x234.jpg" border="0" alt="Instead Of Women's Day There Should Be A 'Good Human Being Day', Says Sunny Leone" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/instead-of-women-s-day-there-should-be-a-good-human-being-day-says-sunny-leone-251624.html" title="Instead Of Women's Day There Should Be A 'Good Human Being Day', Says Sunny Leone">
    Instead Of Women's Day There Should Be A 'Good Human Being Day', Says Sunny Leone                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/12-foods-that-you-didn-t-know-cause-acidity-and-some-useful-home-remedies-251554.html" class="tint" title="12 Foods That You Didn't Know Cause Acidity, And Some Useful Home Remedies">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1457090866_502x234.jpg" border="0" alt="12 Foods That You Didn't Know Cause Acidity, And Some Useful Home Remedies" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/12-foods-that-you-didn-t-know-cause-acidity-and-some-useful-home-remedies-251554.html" title="12 Foods That You Didn't Know Cause Acidity, And Some Useful Home Remedies">
    12 Foods That You Didn't Know Cause Acidity, And Some Useful Home Remedies                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/12-very-real-struggles-of-people-on-their-first-day-at-work-251479.html" class="tint" title="12 Very Real Struggles Of People On Their First Day At Work">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/wus_1456992201_1456992212_502x234.jpg" border="0" alt="12 Very Real Struggles Of People On Their First Day At Work" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/12-very-real-struggles-of-people-on-their-first-day-at-work-251479.html" title="12 Very Real Struggles Of People On Their First Day At Work">
    12 Very Real Struggles Of People On Their First Day At Work                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/meet-arunachalam-muruganantham-the-mentruation-man-who-is-one-of-india-s-most-well-known-social-entrepreneurs-251615.html" class="tint" title="Meet Arunachalam Muruganantham, The Menstruation Man Who Is One Of India's Most Well-Known Social Entrepreneurs">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/z_1457270919_1457270931_502x234.jpg" border="0" alt="Meet Arunachalam Muruganantham, The Menstruation Man Who Is One Of India's Most Well-Known Social Entrepreneurs" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/meet-arunachalam-muruganantham-the-mentruation-man-who-is-one-of-india-s-most-well-known-social-entrepreneurs-251615.html" title="Meet Arunachalam Muruganantham, The Menstruation Man Who Is One Of India's Most Well-Known Social Entrepreneurs">
    Meet Arunachalam Muruganantham, The Menstruation Man Who Is One Of India's Most Well-Known Social Entrepreneurs                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/arjun-kapoor-kareena-kapoor-take-a-dig-at-gender-stereotypes-society-s-hypocrisy-in-this-funny-video-251611.html" class="tint" title="Arjun Kapoor, Kareena Kapoor Take A Dig At Gender Stereotypes & Society's Hypocrisy In This Funny Video!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/kiandka_1457265511_1457265522_502x234.jpg" border="0" alt="Arjun Kapoor, Kareena Kapoor Take A Dig At Gender Stereotypes & Society's Hypocrisy In This Funny Video!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/arjun-kapoor-kareena-kapoor-take-a-dig-at-gender-stereotypes-society-s-hypocrisy-in-this-funny-video-251611.html" title="Arjun Kapoor, Kareena Kapoor Take A Dig At Gender Stereotypes & Society's Hypocrisy In This Funny Video!">
    Arjun Kapoor, Kareena Kapoor Take A Dig At Gender Stereotypes & Society's Hypocrisy In This Funny Video!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/these-two-brothers-have-developed-ticket-jugaad-an-app-to-tackle-train-reservation-woes-251612.html" class="tint" title="These Two Brothers Have Developed 'Ticket Jugaad', An App To Tackle Train Reservation Woes!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/brothers_1457265655_1457265666_502x234.jpg" border="0" alt="These Two Brothers Have Developed 'Ticket Jugaad', An App To Tackle Train Reservation Woes!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/these-two-brothers-have-developed-ticket-jugaad-an-app-to-tackle-train-reservation-woes-251612.html" title="These Two Brothers Have Developed 'Ticket Jugaad', An App To Tackle Train Reservation Woes!">
    These Two Brothers Have Developed 'Ticket Jugaad', An App To Tackle Train Reservation Woes!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/13-life-lessons-we-forgot-to-learn-from-our-favorite-fictional-characters-251481.html" class="tint" title="13 Life Lessons We Forgot To Learn From Our Favorite Fictional Characters">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/batman1_1456993145_502x234.jpg" border="0" alt="13 Life Lessons We Forgot To Learn From Our Favorite Fictional Characters" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/13-life-lessons-we-forgot-to-learn-from-our-favorite-fictional-characters-251481.html" title="13 Life Lessons We Forgot To Learn From Our Favorite Fictional Characters">
    13 Life Lessons We Forgot To Learn From Our Favorite Fictional Characters                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/a-fan-asked-srk-about-how-he-copes-up-with-failures-he-came-up-with-the-best-reply-ever-251607.html" class="tint" title="A Fan Asked SRK About How He 'Copes Up With Failures' & He Came Up With The Best Reply Ever!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/aa_1457256724_1457256736_502x234.jpg" border="0" alt="A Fan Asked SRK About How He 'Copes Up With Failures' & He Came Up With The Best Reply Ever!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/a-fan-asked-srk-about-how-he-copes-up-with-failures-he-came-up-with-the-best-reply-ever-251607.html" title="A Fan Asked SRK About How He 'Copes Up With Failures' & He Came Up With The Best Reply Ever!">
    A Fan Asked SRK About How He 'Copes Up With Failures' & He Came Up With The Best Reply Ever!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/9-towns-from-our-history-books-we-d-love-to-visit-today-251439.html" class="tint" title="9 Towns From Our History Books Weâd Love To Visit Today">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dvarka1_1456916679_502x234.jpg" border="0" alt="9 Towns From Our History Books Weâd Love To Visit Today" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/9-towns-from-our-history-books-we-d-love-to-visit-today-251439.html" title="9 Towns From Our History Books Weâd Love To Visit Today">
    9 Towns From Our History Books Weâd Love To Visit Today                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/this-story-of-a-devadasi-s-transformation-into-a-peer-educator-will-inspire-you-to-no-end-251602.html" class="tint" title="This Story Of A Devadasi's Transformation Into A Peer Educator Will Inspire You To No End!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/as_1457252807_1457252819_502x234.jpg" border="0" alt="This Story Of A Devadasi's Transformation Into A Peer Educator Will Inspire You To No End!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/this-story-of-a-devadasi-s-transformation-into-a-peer-educator-will-inspire-you-to-no-end-251602.html" title="This Story Of A Devadasi's Transformation Into A Peer Educator Will Inspire You To No End!">
    This Story Of A Devadasi's Transformation Into A Peer Educator Will Inspire You To No End!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/varun-pruthi-bought-57-books-from-a-68-yo-man-showed-us-that-humanity-has-not-lost-its-way-251606.html" class="tint" title="This Inspiring Story Of A 68-Year-Old Book Seller Will Teach You A Valuable Lesson About Life!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/sq_1457256629_1457256641_502x234.jpg" border="0" alt="This Inspiring Story Of A 68-Year-Old Book Seller Will Teach You A Valuable Lesson About Life!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/varun-pruthi-bought-57-books-from-a-68-yo-man-showed-us-that-humanity-has-not-lost-its-way-251606.html" title="This Inspiring Story Of A 68-Year-Old Book Seller Will Teach You A Valuable Lesson About Life!">
    This Inspiring Story Of A 68-Year-Old Book Seller Will Teach You A Valuable Lesson About Life!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/after-sweeping-top-honors-at-every-award-ceremony-bajirao-mastani-s-trophy-count-is-phenomenal-251598.html" class="tint" title="After Sweeping Top Honors At Every Award Ceremony, Bajirao Mastani's Trophy Count Is Phenomenal!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/aacard_1457251472_1457251478_502x234.jpg" border="0" alt="After Sweeping Top Honors At Every Award Ceremony, Bajirao Mastani's Trophy Count Is Phenomenal!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/after-sweeping-top-honors-at-every-award-ceremony-bajirao-mastani-s-trophy-count-is-phenomenal-251598.html" title="After Sweeping Top Honors At Every Award Ceremony, Bajirao Mastani's Trophy Count Is Phenomenal!">
    After Sweeping Top Honors At Every Award Ceremony, Bajirao Mastani's Trophy Count Is Phenomenal!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-confesses-how-she-was-once-stuck-in-a-love-triangle-251603.html" class="tint" title="Priyanka Chopra Confesses How She Was Once Stuck In A 'Love Triangle'!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/454689092-jpg_1457253182_1457253194_502x234.jpg" border="0" alt="Priyanka Chopra Confesses How She Was Once Stuck In A 'Love Triangle'!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-confesses-how-she-was-once-stuck-in-a-love-triangle-251603.html" title="Priyanka Chopra Confesses How She Was Once Stuck In A 'Love Triangle'!">
    Priyanka Chopra Confesses How She Was Once Stuck In A 'Love Triangle'!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/preity-zinta-makes-a-goodenough-marriage-announcement-introduces-husband-gene-to-the-world-251596.html" class="tint" title="Preity Zinta Makes A 'Goodenough' Marriage Announcement, Introduces Husband Gene To The World">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/pz-card_1457246122_1457246128_502x234.jpg" border="0" alt="Preity Zinta Makes A 'Goodenough' Marriage Announcement, Introduces Husband Gene To The World" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/preity-zinta-makes-a-goodenough-marriage-announcement-introduces-husband-gene-to-the-world-251596.html" title="Preity Zinta Makes A 'Goodenough' Marriage Announcement, Introduces Husband Gene To The World">
    Preity Zinta Makes A 'Goodenough' Marriage Announcement, Introduces Husband Gene To The World                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/calling-india-very-tolerant-aamir-khan-now-urges-pm-to-restrain-people-from-spreading-hatred-251595.html" class="tint" title="Calling India 'Very Tolerant', Aamir Khan Now Urges PM To Restrain People From Spreading Hatred!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/aamir-khan1_1457243597_1457243601_502x234.jpg" border="0" alt="Calling India 'Very Tolerant', Aamir Khan Now Urges PM To Restrain People From Spreading Hatred!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/calling-india-very-tolerant-aamir-khan-now-urges-pm-to-restrain-people-from-spreading-hatred-251595.html" title="Calling India 'Very Tolerant', Aamir Khan Now Urges PM To Restrain People From Spreading Hatred!">
    Calling India 'Very Tolerant', Aamir Khan Now Urges PM To Restrain People From Spreading Hatred!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/11-ways-you-can-cut-down-on-your-wedding-expenses-251516.html" class="tint" title="11 Ways You Can Cut Down On Your Wedding Expenses">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ww_1457014926_1457014936_502x234.jpg" border="0" alt="11 Ways You Can Cut Down On Your Wedding Expenses" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/11-ways-you-can-cut-down-on-your-wedding-expenses-251516.html" title="11 Ways You Can Cut Down On Your Wedding Expenses">
    11 Ways You Can Cut Down On Your Wedding Expenses                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/after-340-days-in-space-astronaut-scott-kelly-is-having-problems-adjusting-to-earth-251594.html" class="tint" title="Astronaut Scott Kelly Talks About How Difficult It Is To Adjust To Gravity And Chairs After 340 Days In Space">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/scott_1457242979_1457242988_502x234.jpg" border="0" alt="Astronaut Scott Kelly Talks About How Difficult It Is To Adjust To Gravity And Chairs After 340 Days In Space" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/after-340-days-in-space-astronaut-scott-kelly-is-having-problems-adjusting-to-earth-251594.html" title="Astronaut Scott Kelly Talks About How Difficult It Is To Adjust To Gravity And Chairs After 340 Days In Space">
    Astronaut Scott Kelly Talks About How Difficult It Is To Adjust To Gravity And Chairs After 340 Days In Space                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/13-times-you-deserve-to-have-that-time-to-yourself-251371.html" class="tint" title="13 Times You Deserve To Have That Time To Yourself">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/alone1_1456895605_502x234.jpg" border="0" alt="13 Times You Deserve To Have That Time To Yourself" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/13-times-you-deserve-to-have-that-time-to-yourself-251371.html" title="13 Times You Deserve To Have That Time To Yourself">
    13 Times You Deserve To Have That Time To Yourself                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/karan-johar-confirms-there-s-going-to-be-a-sequel-to-student-of-the-year-251585.html" class="tint" title="Karan Johar Confirms There's Going To Be A Sequel To 'Student Of The Year'!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1457179098_1457179105_502x234.jpg" border="0" alt="Karan Johar Confirms There's Going To Be A Sequel To 'Student Of The Year'!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/karan-johar-confirms-there-s-going-to-be-a-sequel-to-student-of-the-year-251585.html" title="Karan Johar Confirms There's Going To Be A Sequel To 'Student Of The Year'!">
    Karan Johar Confirms There's Going To Be A Sequel To 'Student Of The Year'!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/academics-say-sarcasm-increases-psychological-well-being-also-makes-you-more-creative-251587.html" class="tint" title="Academics Say Sarcasm Increases Psychological Well-Being, Also Makes You More Creative">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457180695_502x234.jpg" border="0" alt="Academics Say Sarcasm Increases Psychological Well-Being, Also Makes You More Creative" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/academics-say-sarcasm-increases-psychological-well-being-also-makes-you-more-creative-251587.html" title="Academics Say Sarcasm Increases Psychological Well-Being, Also Makes You More Creative">
    Academics Say Sarcasm Increases Psychological Well-Being, Also Makes You More Creative                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/20-bollywood-actors-who-had-the-most-obsessive-stalkers-251547.html" class="tint" title="20 Bollywood Actors Who Had The Most Obsessive Stalkers">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/pc-card_1457087629_1457087634_502x234.jpg" border="0" alt="20 Bollywood Actors Who Had The Most Obsessive Stalkers" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/20-bollywood-actors-who-had-the-most-obsessive-stalkers-251547.html" title="20 Bollywood Actors Who Had The Most Obsessive Stalkers">
    20 Bollywood Actors Who Had The Most Obsessive Stalkers                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/meet-the-90-year-old-cancer-patient-who-chose-an-epic-road-trip-over-treatment-251528.html" class="tint" title="Meet The 90-Year-Old Cancer Patient Who Chose An Epic Road Trip Over Treatment">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1457072842_502x234.jpg" border="0" alt="Meet The 90-Year-Old Cancer Patient Who Chose An Epic Road Trip Over Treatment" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/meet-the-90-year-old-cancer-patient-who-chose-an-epic-road-trip-over-treatment-251528.html" title="Meet The 90-Year-Old Cancer Patient Who Chose An Epic Road Trip Over Treatment">
    Meet The 90-Year-Old Cancer Patient Who Chose An Epic Road Trip Over Treatment                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/9-bollywood-actresses-who-broke-stereotypes-and-proved-it-s-never-too-late-for-love-251576.html" class="tint" title="9 Bollywood Actresses Who Broke Stereotypes And Proved It's Never Too Late For Love">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1457164743_1457164748_502x234.jpg" border="0" alt="9 Bollywood Actresses Who Broke Stereotypes And Proved It's Never Too Late For Love" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/9-bollywood-actresses-who-broke-stereotypes-and-proved-it-s-never-too-late-for-love-251576.html" title="9 Bollywood Actresses Who Broke Stereotypes And Proved It's Never Too Late For Love">
    9 Bollywood Actresses Who Broke Stereotypes And Proved It's Never Too Late For Love                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/delhi-mobile-company-say-freedom-251-was-bought-from-them-for-rs-3600-per-piece-251583.html" class="tint" title="Delhi Mobile Company Say Freedom 251 Was Bought From Them For Rs 3600 Per Piece!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457174207_502x234.jpg" border="0" alt="Delhi Mobile Company Say Freedom 251 Was Bought From Them For Rs 3600 Per Piece!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/delhi-mobile-company-say-freedom-251-was-bought-from-them-for-rs-3600-per-piece-251583.html" title="Delhi Mobile Company Say Freedom 251 Was Bought From Them For Rs 3600 Per Piece!">
    Delhi Mobile Company Say Freedom 251 Was Bought From Them For Rs 3600 Per Piece!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/ever-wondered-why-apple-displays-the-time-as-9-41-am-in-all-their-ads-251551.html" class="tint" title="Ever Wondered Why Apple Displays The Time As 9:41 AM In All Their Ads?">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457159551_502x234.jpg" border="0" alt="Ever Wondered Why Apple Displays The Time As 9:41 AM In All Their Ads?" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/ever-wondered-why-apple-displays-the-time-as-9-41-am-in-all-their-ads-251551.html" title="Ever Wondered Why Apple Displays The Time As 9:41 AM In All Their Ads?">
    Ever Wondered Why Apple Displays The Time As 9:41 AM In All Their Ads?                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/rocket-successfully-launched-by-spacex-on-fifth-attempt-crash-lands-on-drone-ship-251579.html" class="tint" title="SpaceX Launches Rocket Successfully On Fifth Attempt, Crash Lands On A Drone Ship">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457168249_502x234.jpg" border="0" alt="SpaceX Launches Rocket Successfully On Fifth Attempt, Crash Lands On A Drone Ship" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/rocket-successfully-launched-by-spacex-on-fifth-attempt-crash-lands-on-drone-ship-251579.html" title="SpaceX Launches Rocket Successfully On Fifth Attempt, Crash Lands On A Drone Ship">
    SpaceX Launches Rocket Successfully On Fifth Attempt, Crash Lands On A Drone Ship                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/shama-sikander-says-she-had-to-watch-porn-to-prepare-for-her-nymphomaniac-role-in-sexaholic-251572.html" class="tint" title="Shama Sikander Says She Had To Watch Porn To Prepare For Her Nymphomaniac Role In 'Sexaholic'!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/screenshot_3_1457159560_1457159567_502x234.jpg" border="0" alt="Shama Sikander Says She Had To Watch Porn To Prepare For Her Nymphomaniac Role In 'Sexaholic'!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/shama-sikander-says-she-had-to-watch-porn-to-prepare-for-her-nymphomaniac-role-in-sexaholic-251572.html" title="Shama Sikander Says She Had To Watch Porn To Prepare For Her Nymphomaniac Role In 'Sexaholic'!">
    Shama Sikander Says She Had To Watch Porn To Prepare For Her Nymphomaniac Role In 'Sexaholic'!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/watch-priyanka-chopra-destroys-jimmy-fallon-in-a-hot-wings-eating-contest-on-his-show-251568.html" class="tint" title="WATCH: Priyanka Chopra Destroys Jimmy Fallon In A Hot Wings Eating Contest On His Show!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/screenshot_4_1457072587_1457156243_1457156248_502x234.jpg" border="0" alt="WATCH: Priyanka Chopra Destroys Jimmy Fallon In A Hot Wings Eating Contest On His Show!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/watch-priyanka-chopra-destroys-jimmy-fallon-in-a-hot-wings-eating-contest-on-his-show-251568.html" title="WATCH: Priyanka Chopra Destroys Jimmy Fallon In A Hot Wings Eating Contest On His Show!">
    WATCH: Priyanka Chopra Destroys Jimmy Fallon In A Hot Wings Eating Contest On His Show!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/11-science-backed-facts-about-breakups-that-could-clear-your-head-someday-251398.html" class="tint" title="11 Science-Backed Facts About Breakups That Could Clear Your Head Someday">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/2_1456896049_502x234.jpg" border="0" alt="11 Science-Backed Facts About Breakups That Could Clear Your Head Someday" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/11-science-backed-facts-about-breakups-that-could-clear-your-head-someday-251398.html" title="11 Science-Backed Facts About Breakups That Could Clear Your Head Someday">
    11 Science-Backed Facts About Breakups That Could Clear Your Head Someday                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/manoj-kumar-to-get-dadasaheb-phalke-award-bollywood-erupts-with-congratulatory-messages-251561.html" class="tint" title="Manoj Kumar To Get Dadasaheb Phalke Award, Bollywood Erupts With Congratulatory Messages">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/manoj-card_1457095913_1457095916_502x234.jpg" border="0" alt="Manoj Kumar To Get Dadasaheb Phalke Award, Bollywood Erupts With Congratulatory Messages" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/manoj-kumar-to-get-dadasaheb-phalke-award-bollywood-erupts-with-congratulatory-messages-251561.html" title="Manoj Kumar To Get Dadasaheb Phalke Award, Bollywood Erupts With Congratulatory Messages">
    Manoj Kumar To Get Dadasaheb Phalke Award, Bollywood Erupts With Congratulatory Messages                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/censor-board-chief-pahlaj-nihalani-says-directors-have-tried-to-bribe-him-to-pass-their-films-251559.html" class="tint" title="Censor Board Chief Pahlaj Nihalani Says Directors Have Tried To Bribe Him To Pass Their Films!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/vbk-pahlaj_nihalan_2282723g_1457095383_1457095392_502x234.jpg" border="0" alt="Censor Board Chief Pahlaj Nihalani Says Directors Have Tried To Bribe Him To Pass Their Films!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/censor-board-chief-pahlaj-nihalani-says-directors-have-tried-to-bribe-him-to-pass-their-films-251559.html" title="Censor Board Chief Pahlaj Nihalani Says Directors Have Tried To Bribe Him To Pass Their Films!">
    Censor Board Chief Pahlaj Nihalani Says Directors Have Tried To Bribe Him To Pass Their Films!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/follow-this-routine-before-going-to-bed-if-you-want-to-lose-weight-251512.html" class="tint" title="Follow This Routine Before Going To Bed If You Want To Lose Weight">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card-binge-youtube com_1457009609_502x234.jpg" border="0" alt="Follow This Routine Before Going To Bed If You Want To Lose Weight" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/follow-this-routine-before-going-to-bed-if-you-want-to-lose-weight-251512.html" title="Follow This Routine Before Going To Bed If You Want To Lose Weight">
    Follow This Routine Before Going To Bed If You Want To Lose Weight                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/irrfan-khan-bags-another-international-film-bangladeshi-director-s-no-bed-of-roses-251553.html" class="tint" title="Irrfan Khan Bags Another International Film, Bangladeshi Director's 'No Bed Of Roses'">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/o-irrfan-khan-facebook_1457090658_1457090665_502x234.jpg" border="0" alt="Irrfan Khan Bags Another International Film, Bangladeshi Director's 'No Bed Of Roses'" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/irrfan-khan-bags-another-international-film-bangladeshi-director-s-no-bed-of-roses-251553.html" title="Irrfan Khan Bags Another International Film, Bangladeshi Director's 'No Bed Of Roses'">
    Irrfan Khan Bags Another International Film, Bangladeshi Director's 'No Bed Of Roses'                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/meet-raj-kumar-the-olympic-gold-medallist-battling-a-rare-disease-and-struggling-to-make-ends-meet-250462.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/videocafe/meet-raj-kumar-the-olympic-gold-medallist-battling-a-rare-disease-and-struggling-to-make-ends-meet-250462.html" class="tint" title="Meet Raj Kumar, The Olympic Gold Medallist Battling A Rare Disease And Struggling To Make Ends Meet">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/rajkumartiwari_card_1457110937_502x234.jpg" border="0" alt="Meet Raj Kumar, The Olympic Gold Medallist Battling A Rare Disease And Struggling To Make Ends Meet" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/videocafe/meet-raj-kumar-the-olympic-gold-medallist-battling-a-rare-disease-and-struggling-to-make-ends-meet-250462.html" title="Meet Raj Kumar, The Olympic Gold Medallist Battling A Rare Disease And Struggling To Make Ends Meet">
    Meet Raj Kumar, The Olympic Gold Medallist Battling A Rare Disease And Struggling To Make Ends Meet                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/10-of-the-largest-uprisings-in-history-that-started-out-as-student-protests-251537.html" class="tint" title="10 Of The Largest Uprisings In History That Started Out As Student Protests">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card-reuters_1457078513_502x234.jpg" border="0" alt="10 Of The Largest Uprisings In History That Started Out As Student Protests" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/10-of-the-largest-uprisings-in-history-that-started-out-as-student-protests-251537.html" title="10 Of The Largest Uprisings In History That Started Out As Student Protests">
    10 Of The Largest Uprisings In History That Started Out As Student Protests                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/after-bollywood-this-sand-artist-illustrates-100-years-of-hollywood-in-just-4-minutes-251538.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/videocafe/after-bollywood-this-sand-artist-illustrates-100-years-of-hollywood-in-just-4-minutes-251538.html" class="tint" title="After Bollywood, This Sand Artist Illustrates 100 Years Of Hollywood In Just 4 Minutes!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/hollywood_card1_1457112082_502x234.jpg" border="0" alt="After Bollywood, This Sand Artist Illustrates 100 Years Of Hollywood In Just 4 Minutes!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/videocafe/after-bollywood-this-sand-artist-illustrates-100-years-of-hollywood-in-just-4-minutes-251538.html" title="After Bollywood, This Sand Artist Illustrates 100 Years Of Hollywood In Just 4 Minutes!">
    After Bollywood, This Sand Artist Illustrates 100 Years Of Hollywood In Just 4 Minutes!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/dilwale-and-prem-ratan-dhan-payo-are-frontrunners-at-the-golden-kela-awards-for-worst-film-251548.html" class="tint" title="'Dilwale' And 'Prem Ratan Dhan Payo' Are Frontrunners At The Golden Kela Awards For Worst Film!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1457088080_1457088085_502x234.jpg" border="0" alt="'Dilwale' And 'Prem Ratan Dhan Payo' Are Frontrunners At The Golden Kela Awards For Worst Film!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/dilwale-and-prem-ratan-dhan-payo-are-frontrunners-at-the-golden-kela-awards-for-worst-film-251548.html" title="'Dilwale' And 'Prem Ratan Dhan Payo' Are Frontrunners At The Golden Kela Awards For Worst Film!">
    'Dilwale' And 'Prem Ratan Dhan Payo' Are Frontrunners At The Golden Kela Awards For Worst Film!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/arbaaz-khan-and-malaika-are-very-much-together-at-least-that-s-what-these-pictures-tell-us-251543.html" class="tint" title="Arbaaz Khan And Malaika Are Very Much Together, At Least That's What These Pictures Tell Us">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ak-card_1457089508_1457089515_502x234.jpg" border="0" alt="Arbaaz Khan And Malaika Are Very Much Together, At Least That's What These Pictures Tell Us" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/arbaaz-khan-and-malaika-are-very-much-together-at-least-that-s-what-these-pictures-tell-us-251543.html" title="Arbaaz Khan And Malaika Are Very Much Together, At Least That's What These Pictures Tell Us">
    Arbaaz Khan And Malaika Are Very Much Together, At Least That's What These Pictures Tell Us                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/7-really-annoying-things-skinny-people-say-that-make-you-want-to-shoot-them-251496.html" class="tint" title="7 Really Annoying Things Skinny People Say That Make You Want To Shoot Them!">
                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1457002300_502x234.jpg" border="0" alt="7 Really Annoying Things Skinny People Say That Make You Want To Shoot Them!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/7-really-annoying-things-skinny-people-say-that-make-you-want-to-shoot-them-251496.html" title="7 Really Annoying Things Skinny People Say That Make You Want To Shoot Them!">
    7 Really Annoying Things Skinny People Say That Make You Want To Shoot Them!                    </a>
                </div>    
            </figcaption>
        </div>
    
    <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
        {{#if MoreData}}
        {{#each MoreData}} 
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>


                <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

                            {{#if label_name.length}}
                <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
                                             {{/if}} 
                <!--a class="label-tag spon">Sponsored</a-->
                <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                    <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <a href="{{guid}}" title="{{carousal_headline}}">
                    {{carousal_headline}}
                </a>
            </figcaption>
        </div>  
        {{/each}} 
        {{/if}}

    </script>

</div><!--life-panel end-->

<div class="trending-panel cf "><!--trending-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/lifestyle/self/11-ways-you-can-cut-down-on-your-wedding-expenses-251516.html" class="tint" title="11 Ways You Can Cut Down On Your Wedding Expenses">

                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ww_1457014926_1457014936_236x111.jpg" border="0" alt="11 Ways You Can Cut Down On Your Wedding Expenses"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/11-ways-you-can-cut-down-on-your-wedding-expenses-251516.html" title="11 Ways You Can Cut Down On Your Wedding Expenses">
    11 Ways You Can Cut Down On Your Wedding Expenses                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    <div class='container3'>        <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-10"  data-slot="129061" data-position="10" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/lifestyle/technology/after-340-days-in-space-astronaut-scott-kelly-is-having-problems-adjusting-to-earth-251594.html" class="tint" title="Astronaut Scott Kelly Talks About How Difficult It Is To Adjust To Gravity And Chairs After 340 Days In Space">

                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/scott_1457242979_1457242988_236x111.jpg" border="0" alt="Astronaut Scott Kelly Talks About How Difficult It Is To Adjust To Gravity And Chairs After 340 Days In Space"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/after-340-days-in-space-astronaut-scott-kelly-is-having-problems-adjusting-to-earth-251594.html" title="Astronaut Scott Kelly Talks About How Difficult It Is To Adjust To Gravity And Chairs After 340 Days In Space">
    Astronaut Scott Kelly Talks About How Difficult It Is To Adjust To Gravity And Chairs After 340 Days In Space                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-confesses-how-she-was-once-stuck-in-a-love-triangle-251603.html" class="tint" title="Priyanka Chopra Confesses How She Was Once Stuck In A 'Love Triangle'!">

                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/454689092-jpg_1457253182_1457253194_236x111.jpg" border="0" alt="Priyanka Chopra Confesses How She Was Once Stuck In A 'Love Triangle'!"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-confesses-how-she-was-once-stuck-in-a-love-triangle-251603.html" title="Priyanka Chopra Confesses How She Was Once Stuck In A 'Love Triangle'!">
    Priyanka Chopra Confesses How She Was Once Stuck In A 'Love Triangle'!                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/celebs/preity-zinta-makes-a-goodenough-marriage-announcement-introduces-husband-gene-to-the-world-251596.html" class="tint" title="Preity Zinta Makes A 'Goodenough' Marriage Announcement, Introduces Husband Gene To The World">

                    <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/pz-card_1457246122_1457246128_236x111.jpg" border="0" alt="Preity Zinta Makes A 'Goodenough' Marriage Announcement, Introduces Husband Gene To The World"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/preity-zinta-makes-a-goodenough-marriage-announcement-introduces-husband-gene-to-the-world-251596.html" title="Preity Zinta Makes A 'Goodenough' Marriage Announcement, Introduces Husband Gene To The World">
    Preity Zinta Makes A 'Goodenough' Marriage Announcement, Introduces Husband Gene To The World                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    
</div>

</div><!--trending-panel end-->

</section>
<section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<div class="remove-fixed-home">&nbsp;</div>
<section class="big-ads" id="adfooter">
    <div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">

    $('#bigAd1_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD2('bigAd2_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible     

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd2_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD3('bigAd3_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible

            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible  

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd3_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            BADros('badRos_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible
            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });

    $(document).ready(function () {
        /* spotlight onload tracking homepage */
        //console.log("homepage");
            ga('send', 'event', 'OnLoad Partner Stories', '250889', 'homepage', {'nonInteraction': 1});
            ga('send', 'event', 'OnLoad Partner Stories', '251484', 'homepage', {'nonInteraction': 1});
    });

    $(document).ready(function () {
        var trigger_depth = {b2: 99, b3: 499, b4: 1199};
        var is_trigger_active = {b2: true, b3: true, b4: true};
        var call_on_scroll = true;

        $(window).on("scroll", function () {
            if ((!(is_trigger_active.b2) && !(is_trigger_active.b3) && !(is_trigger_active.b4)) == false)
            {
                callPreload(is_trigger_active, trigger_depth);

            }
        });
    });

    function callPreload(active, depth)
    {
        var scroll_top = $(window).scrollTop();
        if ((active.b2) && (scroll_top > depth.b2))
        {
            $.preload("http://media.indiatimes.in/media/content/2016/Mar/thewire8_1457354893_1457354896_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/jnu64_1457350258_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/600_1457344082_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/600_1457349520_236x111.jpg","http://media.indiatimes.in/media/videocafe/2016/Mar/deadfishes_card_1457350165_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/picture_1456912982_1456912994_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/new-card_1457346831_1457346836_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457330103_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/aa_1457256724_1457256736_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/z_1457270919_1457270931_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/sq_1457256629_1457256641_236x111.jpg");
            active.b2 = false;
        }
        if ((active.b3) && (scroll_top > depth.b3))
        {
            $.preload("http://media.indiatimes.in/media/content/2016/Mar/dhonisix640_1457346692_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/card-ramdev-pantajali-october-2016-amla-murabba1_1457346490_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/balbir640_1457345449_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/rtx1se5b6yyy_1457342207_1457342209_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/anushka-sharma-sultan_1457338428_1457338434_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/cover_1457343379_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Mar/dog_card_1457286254_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/kiandka_1457265511_1457265522_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457090521_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/batman1_1456993145_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/dvarka1_1456916679_236x111.jpg");
            active.b3 = false;
        }

        if ((active.b4) && (scroll_top > depth.b4))
        {
            $.preload("http://media.indiatimes.in/media/content/2016/Mar/56da1b01a8c4d image_1457336520_1457336536_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/messigoal640_1457337884_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/thetruthseeker_1457336876_1457336881_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/nuns600_1457335280_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/shiva-card_1457337801_1457337809_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card-1_1457345413_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/hp_1457334356_1457334368_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Mar/inld_card_1457346120_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/cp_1457165424_1457165428_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card-black_and_green_cardamom-wikimedia_1457080572_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457334462_1457334466_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/aa1_1457331244_1457331252_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457010506_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/cp1_1457329090_1457329094_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457330103_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/sunny-leone1_1457328137_1457328149_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/cover_1457090866_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/wus_1456992201_1456992212_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/z_1457270919_1457270931_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/kiandka_1457265511_1457265522_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/brothers_1457265655_1457265666_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/batman1_1456993145_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/aa_1457256724_1457256736_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/dvarka1_1456916679_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/as_1457252807_1457252819_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/sq_1457256629_1457256641_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/aacard_1457251472_1457251478_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/454689092-jpg_1457253182_1457253194_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/pz-card_1457246122_1457246128_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/aamir-khan1_1457243597_1457243601_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/ww_1457014926_1457014936_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/scott_1457242979_1457242988_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/alone1_1456895605_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1457179098_1457179105_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457180695_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/pc-card_1457087629_1457087634_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1457072842_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1457164743_1457164748_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457174207_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457159551_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card_1457168249_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/screenshot_3_1457159560_1457159567_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/screenshot_4_1457072587_1457156243_1457156248_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/2_1456896049_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/manoj-card_1457095913_1457095916_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/vbk-pahlaj_nihalan_2282723g_1457095383_1457095392_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card-binge-youtube com_1457009609_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/o-irrfan-khan-facebook_1457090658_1457090665_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Mar/rajkumartiwari_card_1457110937_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/card-reuters_1457078513_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Mar/hollywood_card1_1457112082_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1457088080_1457088085_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/ak-card_1457089508_1457089515_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/cover_1457002300_502x234.jpg","http://media.indiatimes.in/media/content/2016/Mar/ww_1457014926_1457014936_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/scott_1457242979_1457242988_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/454689092-jpg_1457253182_1457253194_236x111.jpg","http://media.indiatimes.in/media/content/2016/Mar/pz-card_1457246122_1457246128_236x111.jpg");
            active.b4 = false;
        }
    }
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    4,602,295<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>60,018 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://timesofindia.indiatimes.com/budgetspecial.cms"  target="_blank">Budget 2016</a> 
                        
                            <a href="http://timesofindia.indiatimes.com/budget-2016/rail-budget-2016/indiabudget/50867848.cms"  target="_blank">Rail Budget 2016</a> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2016 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    function socialRHSHide(){}
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=110" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=110"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=110"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=110"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=110"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>