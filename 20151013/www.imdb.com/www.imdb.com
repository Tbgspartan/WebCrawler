



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>


    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "803-8109689-7219447";
                var ue_id = "16H879E2Q1C4RM7CHSWK";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        

        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="16H879E2Q1C4RM7CHSWK" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-0622bbfa.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2130512832._CB292160158_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-1180111305._CB293333875_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-520887519._CB293333839_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['300545732753'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-672141567._CB292180313_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"8297fb330ab89604f0a1e5926fa10784dea1cc44",
"2015-10-13T00%3A18%3A31GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 24089;
generic.days_to_midnight = 0.2788078784942627;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=300545732753;ord=300545732753?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=300545732753?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=300545732753?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top 250 Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/falltv/?ref_=nv_tvv_fall_1"
>Fall TV</a></li>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_2"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_3"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_4"
>Top 250 TV Shows</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_6"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_7"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/family-entertainment-guide/?ref_=nv_sf_feg_1"
>Family Entertainment Guide</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_3"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_4"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=10-13&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59100250/?ref_=nv_nw_tn_1"
> Nick Cassavetes to Write, Direct Ronda Rouseyâs âRoad Houseâ (Exclusive)
</a><br />
                        <span class="time">3 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59099903/?ref_=nv_nw_tn_2"
> M. Night Shyamalan Thriller Starring James McAvoy Adds Four (Exclusive)
</a><br />
                        <span class="time">5 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59098254/?ref_=nv_nw_tn_3"
> Was 'The Walking Dead' premiere trying to top 'Game of Thrones' and 'Hardhome'?
</a><br />
                        <span class="time">21 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0347149/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTkyNTExOTM2OV5BMl5BanBnXkFtZTcwNTk1NzEyNw@@._V1._SY315_CR120,0,410,315_.jpg",
            titleYears : "2004",
            rank : 142,
                    headline : "Howl's Moving Castle"
    },
    nameAd : {
            clickThru : "/name/nm1297015/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjAyOTgyMjUzNV5BMl5BanBnXkFtZTcwODM1MTU0Nw@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 49,
            headline : "Emma Stone"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYlffJDCiaaEeE6mtgc-s6BA3F9xLD-9bFpRB5DO-L9YO89H-Vb1sKPZNLmJt3NZjoHFr85YiR5%0D%0AbKYKniGXr-7ku_VVgc6CaViNRYxkliRC-MaKh_NcRVaAfILgntma237yqXZbkU2Dyu8KmvzsZAuf%0D%0AJfbGbCKFZ_7beee8RpiyewERvrQY00RQNWP91N74M4CEkLxZUKSIBInZeEllGMyOTA%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYseGUVSFc5Bu3bQwZJdfIG1WhxvCEtg6eH9dX9ikzznlPi_alQ1IdyXVhJLF2k2MxEf29YAmCI%0D%0AjU2iLjylM1ob-YIf_T3yyvy-E2ofD5URCqYY7uCbwjIPHXNTYXboOAlPhlAyJDQcWjWeXcxz87AU%0D%0Alfa7cMc--G-ReXxr2gULrEnQrYiDNcOdTpESpM0_hk0I%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYmofiDJDhdugvWrJ7c85KilETV40zmb3gwdxSdNJf2pr_uWgAfRdxSWc5sJ2ib59ijzWu2uS2n%0D%0AKFQhrhXb5iqZCHbQxZwy033p9TI7Ii34neJ9ZjjlFIJkTBeA02vKknIi6IQlx2JlqTDUwktUxSEI%0D%0A14Fd_d2V8upqWiPH1sw137MNsk7e_A74QpZRb_oTL1PpzB_TUAO2ArmgeLfFKm1Jmw%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=300545732753;ord=300545732753?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=300545732753;ord=300545732753?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1281078041?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1281078041" data-source="bylist" data-id="ls056131825" data-rid="16H879E2Q1C4RM7CHSWK" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Watch the New York Comic-Con Sizzle Reel for &quot;The Flash.&quot;" alt="Watch the New York Comic-Con Sizzle Reel for &quot;The Flash.&quot;" src="http://ia.media-imdb.com/images/M/MV5BNjAwNzkxNzAwNF5BMl5BanBnXkFtZTgwODg2NTc2NjE@._V1_SY298_CR18,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjAwNzkxNzAwNF5BMl5BanBnXkFtZTgwODg2NTc2NjE@._V1_SY298_CR18,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Watch the New York Comic-Con Sizzle Reel for &quot;The Flash.&quot;" title="Watch the New York Comic-Con Sizzle Reel for &quot;The Flash.&quot;" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Watch the New York Comic-Con Sizzle Reel for &quot;The Flash.&quot;" title="Watch the New York Comic-Con Sizzle Reel for &quot;The Flash.&quot;" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3107288/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "The Flash" </a> </div> </div> <div class="secondary ellipsis"> Comic-Con Sizzle Reel </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1264300825?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1264300825" data-source="bylist" data-id="ls002309697" data-rid="16H879E2Q1C4RM7CHSWK" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Watch the New York Comic-Con Sizzle Reel for &quot;Arrow.&quot;" alt="Watch the New York Comic-Con Sizzle Reel for &quot;Arrow.&quot;" src="http://ia.media-imdb.com/images/M/MV5BMTg3OTc0NzkyOV5BMl5BanBnXkFtZTgwMDMwMTM3MjE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3OTc0NzkyOV5BMl5BanBnXkFtZTgwMDMwMTM3MjE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Watch the New York Comic-Con Sizzle Reel for &quot;Arrow.&quot;" title="Watch the New York Comic-Con Sizzle Reel for &quot;Arrow.&quot;" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Watch the New York Comic-Con Sizzle Reel for &quot;Arrow.&quot;" title="Watch the New York Comic-Con Sizzle Reel for &quot;Arrow.&quot;" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2193021/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Arrow" </a> </div> </div> <div class="secondary ellipsis"> Comic-Con Sizzle Reel </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1968681753?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1968681753" data-source="bylist" data-id="ls002322762" data-rid="16H879E2Q1C4RM7CHSWK" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." alt="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." src="http://ia.media-imdb.com/images/M/MV5BMTExNTk3MTUzNTdeQTJeQWpwZ15BbWU4MDM5ODY3OTYx._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTExNTk3MTUzNTdeQTJeQWpwZ15BbWU4MDM5ODY3OTYx._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." title="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." title="When four generations of the Cooper clan come together for their annual Christmas Eve celebration, a series of unexpected visitors and unlikely events turn the night upside down, leading them all toward a surprising rediscovery of family bonds and the spirit of the holiday." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2279339/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Love the Coopers </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239484182&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/anniversary/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239603102&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_ia_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239603102&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb Asks: Jennifer Aniston</h3> </a> </span> </span> <p class="blurb">IMDb turns 25 on October 17! Over the years, we asked several celebrities the first movie they ever saw in a theater. Watch our interviews with Jennifer Aniston, Oscar Isaac, Ryan Gosling, Jennifer Lawrence, and more.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1165538073?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239603102&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_ia_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239603102&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1165538073" data-source="bylist" data-id="ls079148381" data-rid="16H879E2Q1C4RM7CHSWK" data-type="playlist" class="video-colorbox" data-refsuffix="hm_anv_ia" data-ref="hm_anv_ia_i_1"> <img itemprop="image" class="pri_image" title="IMDb Asks (2015-)" alt="IMDb Asks (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMzk1MzIyMjUyMl5BMl5BanBnXkFtZTgwNTE3MTIwNzE@._V1_SX624_CR0,0,624,351_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzk1MzIyMjUyMl5BMl5BanBnXkFtZTgwNTE3MTIwNzE@._V1_SX624_CR0,0,624,351_AL_UY702_UX1248_AL_.jpg" /> <img alt="IMDb Asks (2015-)" title="IMDb Asks (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb Asks (2015-)" title="IMDb Asks (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/anniversary/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239603102&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_ia_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239603102&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Watch more IMDb Asks videos </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/6-things-you-need-to-know-about-fargo-season-2?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239589802&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_far_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239589802&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Exclusive: 6 Things to Know About Season 2 of "Fargo"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2802850/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239589802&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_far_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239589802&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Fargo (2014-)" alt="Fargo (2014-)" src="http://ia.media-imdb.com/images/M/MV5BNDEzOTYzMDkzN15BMl5BanBnXkFtZTgwODkzNTAyNjE@._V1_SY250_CR2,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDEzOTYzMDkzN15BMl5BanBnXkFtZTgwODkzNTAyNjE@._V1_SY250_CR2,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/6-things-you-need-to-know-about-fargo-season-2?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239589802&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_far_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239589802&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Fargo (2014-)" alt="Fargo (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMjMxNTIxMDQ4Ml5BMl5BanBnXkFtZTgwNTc5Mjk5NjE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMxNTIxMDQ4Ml5BMl5BanBnXkFtZTgwNTc5Mjk5NjE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">The second season of the Emmy award-winning series "Fargo" premieres Monday, Oct. 12, on FX. A prequel to the first season, this season features a new cast and storyline, but once again focuses on the Solverson family. IMDb visited the set to get the exclusive scoop on Season 2.</p> <p class="seemore"> <a href="/falltv/6-things-you-need-to-know-about-fargo-season-2?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239589802&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_far_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239589802&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Read more </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59100250?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTcxNjA2MjI3Nl5BMl5BanBnXkFtZTYwMjkyNjMz._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59100250?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Nick Cassavetes to Write, Direct Ronda Rouseyâs âRoad Houseâ (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> After nabbing UFC fighter <a href="/name/nm3313925?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Ronda Rousey</a> to star in a reboot of <a href="/name/nm0000664?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Patrick Swayze</a>âs â<a href="/title/tt0098206?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">Road House</a>,â MGM has now found its director. Sources tell Variety that <a href="/name/nm0001024?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk4">Nick Cassavetes</a> has closed a deal to write and direct the re-envisioning of the â80s action classic, with Rousey on board to star and produce. ...                                        <span class="nobr"><a href="/news/ni59100250?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59099903?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >M. Night Shyamalan Thriller Starring James McAvoy Adds Four (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59098254?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Was 'The Walking Dead' premiere trying to top 'Game of Thrones' and 'Hardhome'?</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Hitfix</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59100012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Quentin Tarantino Says He Cut Two Different Versions of âThe Hateful Eightâ (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59099711?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >John Ridley To Helm His âL.A. Riotsâ Script For Broad Green And Imagine</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59100012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTgwMzE0Nzc2Nl5BMl5BanBnXkFtZTgwMjMzNTMyNjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59100012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Quentin Tarantino Says He Cut Two Different Versions of âThe Hateful Eightâ (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>When audiences pay to see the limited roadshow engagement of <a href="/name/nm0000233?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Quentin Tarantino</a>âs â<a href="/title/tt3460252?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">The Hateful Eight</a>â this holiday season, it wonât just be the projection of Ultra Panavision 70mm photography that distinguishes it from multiplex versions released two weeks later. It will be a slightly different â ...                                        <span class="nobr"><a href="/news/ni59100012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59099711?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >John Ridley To Helm His âL.A. Riotsâ Script For Broad Green And Imagine</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59098465?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Mad Max Fury Road sequel won't star Charlize Theron's Imperator Furiosa</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000023?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Digital Spy</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59100250?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Nick Cassavetes to Write, Direct Ronda Rouseyâs âRoad Houseâ (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59098240?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >The Cruella de Vil Movie Just Hired An Unusual Writer</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59098377?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjIwMjYyNjk4Nl5BMl5BanBnXkFtZTcwNzA4NDYwMw@@._V1_SY150_CR6,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59098377?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Paul Reubens Will Play Penguin's Dad in Gotham</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Vulture</a></span>
    </div>
                                </div>
<p>You could tellÂ <a href="/title/tt3749900?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Gotham</a>Â star <a href="/name/nm2088699?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Robin Lord Taylor</a> wasn't putting on airs when he giddily asked scribe <a href="/name/nm1129488?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">John Stephens</a> if he could reveal who'd be playing his TV dad. Taylor, who shines as Oswald "Penguin" Cobblepot, is one ofÂ <a href="/title/tt3749900?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">Gotham</a>'s breakout stars, so it was a pleasure to see him do everything but squee...                                        <span class="nobr"><a href="/news/ni59098377?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59098324?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >5 Things to Expect in Supergirlâs First Season</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59098254?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Was 'The Walking Dead' premiere trying to top 'Game of Thrones' and 'Hardhome'?</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Hitfix</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59098250?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Reza Aslan Addresses All Your Leftoversâ Questions</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59098186?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >On The Knick Set With Steven Soderbergh, Binge Director</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59098159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQ1NjkzOTM2MF5BMl5BanBnXkFtZTgwMDY0NTk2NjE@._V1_SY150_CR2,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59098159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Kanye West Crashes âAmerican Idolâ Auditions</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>Congratulations <a href="/name/nm1577190?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Kanye West</a>. Youâre going to Hollywood! The âStrongerâ rapper secured his spot on Saturday night when he âauditionedâ for â<a href="/title/tt0319931?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">American Idol</a>.âOk, Kanye is way too successful to ever audition for a singing competition, but he did make a surprise appearance at â<a href="/title/tt0319931?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk3">Idol</a>âsâ San Francisco ...                                        <span class="nobr"><a href="/news/ni59098159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59098529?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Vin Diesel Puts "Dad Bod" Chatter to Rest With New Six Pack Photo</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0045136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Us Weekly</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59098427?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Amanda Bynes -- Back To School To Prove She's Not a Fool</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000358?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>TMZ</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59098275?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Raven-SymonÃ© Clarifies Controversial Comments From The View, Swears She ''Never Discriminated Against a Name''</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59063203?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >13 Celebrity Power-Couple Costume Ideas</a>
    <div class="infobar">
            <span class="text-muted">just now</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm395767040/rg2664209152?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Man in the High Castle (2015-)" alt="The Man in the High Castle (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjA0NjUyNjQzOV5BMl5BanBnXkFtZTgwODU5MjEwNzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0NjUyNjQzOV5BMl5BanBnXkFtZTgwODU5MjEwNzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm395767040/rg2664209152?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "The Man in the High Castle" - NYCC Panel </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1201073408/rg2647431936?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Dr. Horrible's Sing-Along Blog (2008-)" alt="Dr. Horrible's Sing-Along Blog (2008-)" src="http://ia.media-imdb.com/images/M/MV5BMTc5MDI2NjUyOV5BMl5BanBnXkFtZTgwMzExMzEwNzE@._V1_SY201_CR21,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc5MDI2NjUyOV5BMl5BanBnXkFtZTgwMzExMzEwNzE@._V1_SY201_CR21,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1201073408/rg2647431936?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "Dr. Horrible's Sing-Along Blog Reunion" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/falltv/galleries/the-walking-dead-rm3313233152?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010-)" alt="The Walking Dead (2010-)" src="http://ia.media-imdb.com/images/M/MV5BMTcxODQyMDY2NF5BMl5BanBnXkFtZTgwMDg1MjIwNzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcxODQyMDY2NF5BMl5BanBnXkFtZTgwMDg1MjIwNzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/falltv/galleries/the-walking-dead-rm3313233152?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239785842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "The Walking Dead" - Season 6 Photos </a> </div> </div> </div> </div> </div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=10-12&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0413168?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Hugh Jackman" alt="Hugh Jackman" src="http://ia.media-imdb.com/images/M/MV5BNDExMzIzNjk3Nl5BMl5BanBnXkFtZTcwOTE4NDU5OA@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDExMzIzNjk3Nl5BMl5BanBnXkFtZTcwOTE4NDU5OA@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0413168?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Hugh Jackman</a> (47) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1242688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Josh Hutcherson" alt="Josh Hutcherson" src="http://ia.media-imdb.com/images/M/MV5BMTI4OTk0MjQ1OV5BMl5BanBnXkFtZTcwNTE3NjM3Mw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI4OTk0MjQ1OV5BMl5BanBnXkFtZTcwNTE3NjM3Mw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1242688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Josh Hutcherson</a> (23) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1668284?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Brian J. Smith" alt="Brian J. Smith" src="http://ia.media-imdb.com/images/M/MV5BMjA0OTA3MDY2NV5BMl5BanBnXkFtZTcwMTE5NDk3Mg@@._V1_SY172_CR71,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0OTA3MDY2NV5BMl5BanBnXkFtZTcwMTE5NDk3Mg@@._V1_SY172_CR71,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1668284?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Brian J. Smith</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005417?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Lin Shaye" alt="Lin Shaye" src="http://ia.media-imdb.com/images/M/MV5BMjIyNjk5ODY1NF5BMl5BanBnXkFtZTcwOTgyMDQ2OA@@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIyNjk5ODY1NF5BMl5BanBnXkFtZTcwOTgyMDQ2OA@@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005417?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Lin Shaye</a> (72) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0286033?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Deborah Foreman" alt="Deborah Foreman" src="http://ia.media-imdb.com/images/M/MV5BMjQzMzU2NDUzMF5BMl5BanBnXkFtZTgwOTQzMzIwNzE@._V1_SY172_CR28,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQzMzU2NDUzMF5BMl5BanBnXkFtZTgwOTQzMzIwNzE@._V1_SY172_CR28,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0286033?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Deborah Foreman</a> (53) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=10-12&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Indie Focus: 'Kilo Two Bravo' - Official Trailer</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3622120/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238122882&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238122882&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Kilo Two Bravo (2014)" alt="Kilo Two Bravo (2014)" src="http://ia.media-imdb.com/images/M/MV5BMTQ5MjQ2MjU5NV5BMl5BanBnXkFtZTgwMzI2Mzg5NjE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ5MjQ2MjU5NV5BMl5BanBnXkFtZTgwMzI2Mzg5NjE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2505093913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238122882&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238122882&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2505093913" data-rid="16H879E2Q1C4RM7CHSWK" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Kilo Two Bravo (2014)" alt="Kilo Two Bravo (2014)" src="http://ia.media-imdb.com/images/M/MV5BMTQ2ODI3ODIzMl5BMl5BanBnXkFtZTgwMDgyNDc1MzE@._V1_SY250_CR10,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2ODI3ODIzMl5BMl5BanBnXkFtZTgwMDgyNDc1MzE@._V1_SY250_CR10,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Kilo Two Bravo (2014)" title="Kilo Two Bravo (2014)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Kilo Two Bravo (2014)" title="Kilo Two Bravo (2014)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">When a company of young British soldiers are sent to the Kajaki region of Afghanistan to disable a Taliban roadblock, they find themselves stranded in an active minefield.</p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt3460252/trivia?item=tr2094097&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3460252?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Hateful Eight (2015)" alt="The Hateful Eight (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTgwMzE0Nzc2Nl5BMl5BanBnXkFtZTgwMjMzNTMyNjE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgwMzE0Nzc2Nl5BMl5BanBnXkFtZTgwMjMzNTMyNjE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt3460252?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">The Hateful Eight</a></strong> <p class="blurb">Was shelved in early 2014 after the script was leaked but production was resumed in early 2015.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt3460252/trivia?item=tr2094097&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-25"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;fPetQRjhV_0&quot;}">
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/fPetQRjhV_0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: IMDb STARmeter Award: Favorite Recipient</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:23.5%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/fPetQRjhV_0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Olivia Wilde" alt="Olivia Wilde" src="http://ia.media-imdb.com/images/M/MV5BMjM0MTk2NTU4MF5BMl5BanBnXkFtZTgwNjA2NDEyMDE@._V1_SY259_CR107,0,175,259_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM0MTk2NTU4MF5BMl5BanBnXkFtZTgwNjA2NDEyMDE@._V1_SY259_CR107,0,175,259_AL_UY518_UX350_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:23.5%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/fPetQRjhV_0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Miles Teller" alt="Miles Teller" src="http://ia.media-imdb.com/images/M/MV5BMjI4MzY0ODY2OF5BMl5BanBnXkFtZTgwOTg2OTQ2MjE@._V1_SX175_CR0,0,175,259_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4MzY0ODY2OF5BMl5BanBnXkFtZTgwOTg2OTQ2MjE@._V1_SX175_CR0,0,175,259_AL_UY518_UX350_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:23.5%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/fPetQRjhV_0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jeffrey Tambor" alt="Jeffrey Tambor" src="http://ia.media-imdb.com/images/M/MV5BMzMyMTU1NjgwNF5BMl5BanBnXkFtZTgwNjQzMjAyNDE@._V1_SY259_CR107,0,175,259_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzMyMTU1NjgwNF5BMl5BanBnXkFtZTgwNjQzMjAyNDE@._V1_SY259_CR107,0,175,259_AL_UY518_UX350_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:23.5%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/fPetQRjhV_0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Brie Larson" alt="Brie Larson" src="http://ia.media-imdb.com/images/M/MV5BMTQ0Mjg4MjQ5OF5BMl5BanBnXkFtZTgwNTcwMDA4NjE@._V1_SX175_CR0,0,175,259_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ0Mjg4MjQ5OF5BMl5BanBnXkFtZTgwNTcwMDA4NjE@._V1_SX175_CR0,0,175,259_AL_UY518_UX350_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Who is your favorite recipient of IMDb STARmeter Award? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/244401371?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"> <a href="/poll/fPetQRjhV_0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239026842&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=300545732753;ord=300545732753?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=300545732753?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=300545732753?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1051904"></div> <div class="title"> <a href="/title/tt1051904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Goosebumps </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2554274"></div> <div class="title"> <a href="/title/tt2554274?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Crimson Peak </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3682448"></div> <div class="title"> <a href="/title/tt3682448?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Bridge of Spies </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3859076"></div> <div class="title"> <a href="/title/tt3859076?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Truth </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3170832"></div> <div class="title"> <a href="/title/tt3170832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Room </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3272570"></div> <div class="title"> <a href="/title/tt3272570?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> All Things Must Pass: The Rise and Fall of Tower Records </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1870548"></div> <div class="title"> <a href="/title/tt1870548?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> This Changes Everything </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4183692"></div> <div class="title"> <a href="/title/tt4183692?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Woodlawn </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3181776"></div> <div class="title"> <a href="/title/tt3181776?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t8"> Momentum </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239023922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3659388"></div> <div class="title"> <a href="/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Martian </a> <span class="secondary-text">$37.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2510894"></div> <div class="title"> <a href="/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Hotel Transylvania 2 </a> <span class="secondary-text">$20.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3332064"></div> <div class="title"> <a href="/title/tt3332064?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Pan </a> <span class="secondary-text">$15.3M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2361509"></div> <div class="title"> <a href="/title/tt2361509?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> The Intern </a> <span class="secondary-text">$8.7M</span> </div> <div class="action"> <a href="/showtimes/title/tt2361509?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3397884"></div> <div class="title"> <a href="/title/tt3397884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Sicario </a> <span class="secondary-text">$7.6M</span> </div> <div class="action"> <a href="/showtimes/title/tt3397884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2080374"></div> <div class="title"> <a href="/title/tt2080374?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Steve Jobs </a> <span class="secondary-text"></span> </div> <div class="action"> Nationwide Expansion </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2473510"></div> <div class="title"> <a href="/title/tt2473510?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Paranormal Activity: The Ghost Dimension </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3614530"></div> <div class="title"> <a href="/title/tt3614530?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Jem and the Holograms </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1618442"></div> <div class="title"> <a href="/title/tt1618442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> The Last Witch Hunter </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3164256"></div> <div class="title"> <a href="/title/tt3164256?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Rock the Kasbah </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/anniversary/most-immersive-worlds/ls079103330?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238013602&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238013602&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>The 25 Most Immersive Worlds in Cinema</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/anniversary/most-immersive-worlds/ls079103330?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238013602&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238013602&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Avatar (2009)" alt="Avatar (2009)" src="http://ia.media-imdb.com/images/M/MV5BMTkxMjkzMzAwNF5BMl5BanBnXkFtZTcwMDc3MTUzNA@@._V1_SY525_CR116,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxMjkzMzAwNF5BMl5BanBnXkFtZTcwMDc3MTUzNA@@._V1_SY525_CR116,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Highly immersive cinematic worlds can carry a movie, and we've rounded up the best of the best.</p> <p class="seemore"> <a href="/anniversary/most-immersive-worlds/ls079103330?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238013602&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_anv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2238013602&pf_rd_r=16H879E2Q1C4RM7CHSWK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See the full list </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYu404SDvGsXNX-H0HZbwQtIHuPzVCw2Z1tmDWA0qcmBZyINgkk0sYy5cUmCGw5cw90CSI2UlSi%0D%0A7XEgXDVyHfwQkucqyWmjNP3hUy1e-ZjwJ9RjyGLNljIEtf4Dhcy9v_yQTNI7-cW09PjYp7Gso1DP%0D%0Aj5fBeZx1LYctld-Ioaa4MZ1fvlaAsMCBVcCFdOIbeNu3Gq7ihhuKOXVU8Xz63A3LWB94jTD2swf9%0D%0AJdWrsAktLdg%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYl6yoY81mK5gDKiptp-wrsz9O8FZQybbNWQGam2ZAPd6qc_Fdc5ElaYYVB8kZrAGOtGSWB1QVL%0D%0AI-qw6a6pJ0YNEDldtfsknv81boDhs-1JN0mYe76dO5q28BLrFXD7dRjTR_O7T6C3NPwk9giOt_rP%0D%0Ab5xhyDXkmROc6lVg0yxgt-9AKUu6Ow8SwOru6w_-cDEZXZG7DQ6b2vX-HX58UqzcLg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYu-ZUSXI4G1cR7FDziiQhvqAjsRnxxngFRgdMCv7-DeprBqrJQXCpgWkGijCLEgQkOcYPR7VhB%0D%0ABjJS6zSyfGkRoGg3Mhkjaks4ELW-YzC6G1RelwyQv9S776vupHfaatZXex8vuGGuchvDA_bbWNbS%0D%0AVNNcFIHL9vKrt7MRKVoUlP5mk_iVQixeEWCanmBwOjDLtdyrc037jIORnpnLZR-5WBhT2VblWx7b%0D%0AaxX5l8e7T1tJO7t-VWstwBKHpvuEjWf-r8UZaJ3kzxfkrxcffJDc3t9mUOe3xNvFLinECNoXUEYU%0D%0AJ44HKNPuIAh2JhRK584RLYA2OXHep8N8BElApiEBqQ4yt3En-xHPHFOUDpYZSAZf99mbhgWBbrGF%0D%0AscDt5GcoWD4lSIs-i841yGn17Wow0g%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYmZKypLKrvsIb_YeyGSIti9dp1psZkLgmbGQctTARkcvqocUW9utvaIi6uOL-PQZg2_NAOcO6v%0D%0AaccpSuu6HX7JaV5fcdx9L6hQLYlSrR4vYEjTvu_Xxz6isHSHFkZkgAp5-7VOT5OwLZUU9eEScVx2%0D%0A-uK_GD-iBRBfxUP-MrL1zX4uMGe-adueTVVviP8FTUlOoI3ZpfdUvsvjJkt34Gn_HQ%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYviOg3PXbT1J_zU2P5IVN1zzSNWZh7tdo9-0Q0mO_stT0-p9D3T0wdNnDKJHNRjTSaYs-o2-d5%0D%0AUk2DtaYQKzO5hta2_hoTlynXUA9wsc9Zgbgz4XbdUZVoZOI8gbp4s1dnh-o3EXEy8Og47slllFJc%0D%0Axx6YhrAwa5b8Bt_aB0QO47Tt_E-1uL3stD_4FHki0gcN%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYvnIJofxuMEztkiZ-mTSf1n1E-_WoLllJXV3sgEgg1TPhY2ZU1pXN8_KHjJ32kuD8AhhFgKJAS%0D%0A28XHx-rcrh_FuC_b4-IQWAMYddDz0CTQKjMd5WGHQGOoaoQhPiTN6GZrwDbS7N154r60DbATOakY%0D%0Aj4GR9ndmoWEDmQhYG3q5dbLVO-o7T85p1vGzlzqktPZnRDhzwasqh2zdrXptuHE34Q%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYvNvXJFsvm_OwOtpMZRIRc2TgQshQB36DhveWOhVjZY8BXugl7t00M1MWUapO1UeQu_LuAl_fa%0D%0AwLTtAoQKCF3CKfBhdMZ5jPrtgctv3c1lGo16cZTwVE-fCGVQ11xrI7B8iSItYyAheiwOPI4Pj657%0D%0AitMqv8eLcRjlGoFEI_IrsV4wRd-wJBPXKQg2KH31-GzUwx1LvP3kAMuhS0TtTI03TNIY-G43sZsS%0D%0ARoO6B2v-N83nCs8DuPYtwFfE7PUqTds0IJCn6VbRMwM4E_woaW8o3w%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYvOnIc1BwmsnGxPrGTk5dmtoVsTmxiRB-Ng7W9n17NKtGV-O8dQp5gAr9Srb59CZxqWwoI8fWx%0D%0AJcdIa09O2YE-8onPfOfwIDTsUF1QWa7_iVpn5tEcSAfGpDOYkm4An9cgW9g9VrgC4a-cAK4V9Rfv%0D%0AmBVRnwk4PTw7Vw_ku9SDIiBBBbazlP6KUMVwqPcgvjIMJwe1aMQscprRUAwra38TQQ%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYrAfvsQw-9XayXcCGDyfXeJ_uZks6Mql3hdkAiElwxWk_ug34x6YbA-joE6DdPJaIs3vufwUMH%0D%0AHEGnkOsSfDAkWF-XDgFGrZRkum9Vq1T0Hlgg4HIGADZNWOsDjjIbzkbZIHX9I972s2BMSZxPnOa4%0D%0AnEWJLT3rJD7h7us-GDW6omqU-DZYG8nTM9xab6hQTHKH%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYsSxTyHnM3FHzy5a_KRxk8aZbBXB8yqvZN1rFrkLFWfB4ahdgo_Xm3QrHT6oXzu5T1CtI0qwYh%0D%0AdClkD_odNoHZngT4YBmAz4pQWl7VTALPzqMVYtqpp3QomPI1wYR1dkNV4UnxEgw3LKsssaJ5BAYD%0D%0AbeX7BcMsPONrs1xvyE9FAVMkFPqd0pYwpBAHhDt7TA-sD2hpHxsmgTBxxJzfCZlViHZVrm5mxDHj%0D%0AZRSAUzfNTibuD_SEi2JEDFm8HLFv2gGR%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYuHZMbhIAYbEkVG6A5Tob7GKEuqD_kAeiIckDM5fUo9Z5LbvAw5oYZ8K9uIWOLJNsAzqiOImU-%0D%0A0z8Qf3Cp_6-dzx4V3hhUw_OQ1zBe1NX-p2sRHVmqPlNksNYMSyGqLirXW2cEJOkR40bQ1OdH6rYk%0D%0AdIixBapYPvMyC4SpZ5l13MYzQcUPOipZtpVAr7ascQO3dCdYk_Gixr5bZQ3hrdxLq8sG8MGeHM9J%0D%0Abia3Rm7ISvY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYkQCNVWIHqxTczWjl0dNs3X8qbqC3KIFLTuj0lPh7aeRNUtcf7ecYCWxsOOFnJ8FSseqGSCNlD%0D%0AKvv10hA82pLxmu5xavfp4X8HV6UwAlwD39wIx5BZyBFWwHMV7Z7uVYXZePeYfb4No1rsaYziYPkC%0D%0AwPSvtNRZH_5nT06Gjxnosg7zD09x45d-MbkIlSkA30PRfeRAreAXQQKW5NL8ULCoidx1Y5qClHau%0D%0ArL74aY-g1Do%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYpRHBCZt5_xoQqy7GSlLxXob7rtY0FuZqetDR_GeMpkeIkkkKkiFZGQwH3iuLd8hR1XYztDO2g%0D%0AzVFP0sxPNjj86zK2rdHjSUmJF5RQNgdI20f3gJ2JSfxWvht3p7GLsIuYp0db9JoDArEGm_vNcLz_%0D%0A7Ie0RLvqrNAaL_N3ozztZRg8hsFH5wGaO1-T6CYoNAwSRUb3R0kng1Rylnk7NeDAi4uAryQD_LrA%0D%0AbzR6enDcUwk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYjPXWkpc9VLPrmMivGbEPB4xwYLQ33IQFkjvL1s6zvb4cC7Y4BGV8izR8FN7ypWJ96Sq4lZRdr%0D%0A1gL7s5kPkr1TWYXkx9QGY9tIryMaWwM0cpuQFkIniuzT1LlAdvaRRwuLU8-N5iHsAMH5RCXFjimC%0D%0A9pdfQG77d5JxMEnyPsxOR3uihVLrW1Vh3wXgQiK3nrfaAeYMxugilZieM27bc8HxMl99KeDuNBPn%0D%0AyIMzUyg2HjM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYnj8oo9tmNgbCJsk5V_-XMr5eqOVRFvoczoZckWBhIUv7Q-lz9BRi-GREJC8-BFw0Zm82d6B9k%0D%0A2fspNWcxkEAmNflyIYcCkqpC-C7sK2N6IWXmTgBifFtKm2ltBCAzyxZtK850qxtRV5PGDgjAJXLU%0D%0A1kvbdLMQBfFEgVdkED-nIxTx4UFPq_BhJOPA9R1fWm5eBPXOChpCxPIQm6Jz0Gn4ke8qlfE4g1bh%0D%0Ae7F9EkCaRmE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYuDXIAbVSxxlBo2XPr1YDxxWWDGxvq0X1i4HGMvrvh9kIEGC-d9dCTL9RYIOxVFtBQ7Rok1Pj-%0D%0AZ5PJZRHypUPrMQm__HYYL32FsUYEzEIWPUIYHNRP-ns6VxcittZUirfndQ189I_umK8bisojA0fg%0D%0A-D3SRsX8y-rEZLUjOwylgC6luk9yFTDJ_miOC3co2amiAQwHujwRImTo-tBHItg1D1sZiaHjK0I4%0D%0ADMrSkTvm6zTtx7Vcar12iKZoJdcsgYQ1%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYqhWEc3lyU0EmWkAJblN-_l9Sruzy1tz_hI7kxsp2GnVrX1HF-qWVyoM59MKqQHWkvc_L_paCT%0D%0AZhvZwarJAbluThbiff_ROuZzeDmmc_libRok_XFeNplR2uTr85j7Cyb_9D-62z0Tkgbsc1GQDxWw%0D%0ATIYx0j7kbI0Jg1s3fBcF69Q%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYifzdRkm3zp9RqI7pu9uVhcOQdo-9zs3_AudDaAT3ydul3yr8zj5zYDbI6x1LjmM6dq0E4I-TP%0D%0AomaywUyz1UzIHifVg3ervxyVlZ3Go1Ek4MvxngvsUwISA3bE0RtCcd4KyzsMH-QBZi09AFw1_e07%0D%0A9jxeMbXrCfXNyCZDPGVtqvo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-2313516719._CB292181657_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-1115875196._CB293428624_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1705459340._CB292800891_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3920146857._CB292796746_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-3229803205._CB292800882_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101a2775003368558af7a8be57fdf2a852272cc329a4905da71e889e5cfd1082c4f",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=300545732753"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=300545732753&ord=300545732753";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="365"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
