<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.28" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.28" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.28"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.28"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.28"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.28"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.28"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-10-13 06:30:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             18 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/arushi-s-grandfather-writes-a-moving-open-letter-in-defence-of-the-talwars-246135.html" class=" tint" title="Arushi's Grandfather Writes A Moving Open Letter In Defence Of The Talwars">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/inside-one-fivefff_1444638278_1444638285_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/inside-one-fivefff_1444638278_1444638285_236x111.jpg"  border="0" alt="Arushi's Grandfather Writes A Moving Open Letter In Defence Of The Talwars"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/arushi-s-grandfather-writes-a-moving-open-letter-in-defence-of-the-talwars-246135.html" title="Arushi's Grandfather Writes A Moving Open Letter In Defence Of The Talwars">
                            Arushi's Grandfather Writes A Moving Open Letter In Defence Of The Talwars                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/here-s-why-sania-mirza-won-t-kill-herself-if-she-doesn-t-win-an-olympic-gold-medal-246163.html" title="Here's Why Sania Mirza 'Won't Kill Herself' If She Doesn't Win An Olympic Gold Medal" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/saniaolympics_1444654922_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/saniaolympics_1444654922_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/here-s-why-sania-mirza-won-t-kill-herself-if-she-doesn-t-win-an-olympic-gold-medal-246163.html" title="Here's Why Sania Mirza 'Won't Kill Herself' If She Doesn't Win An Olympic Gold Medal">
                            Here's Why Sania Mirza 'Won't Kill Herself' If She Doesn't Win An Olympic Gold Medal                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/rs-500-fine-for-faded-mehendi-that-s-what-a-chennai-school-demanded-from-a-class-ii-kid-246162.html" title="Rs 500 Fine For Faded Mehendi? That's What A Chennai School Demanded From A Class II Kid" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/mehendi-five_1444654773_1444654777_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/mehendi-five_1444654773_1444654777_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/rs-500-fine-for-faded-mehendi-that-s-what-a-chennai-school-demanded-from-a-class-ii-kid-246162.html" title="Rs 500 Fine For Faded Mehendi? That's What A Chennai School Demanded From A Class II Kid">
                            Rs 500 Fine For Faded Mehendi? That's What A Chennai School Demanded From A Class II Kid                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/this-auto-driver-might-have-saved-pm-modi-s-life-after-tipping-off-police-about-terrorist-plot-to-kill-him-246158.html" title="This Auto Driver Might Have Saved PM Modi's Life After Tipping Off Police About Terrorist Plot To Kill Him" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/five_1444651439_1444651444_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/five_1444651439_1444651444_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-auto-driver-might-have-saved-pm-modi-s-life-after-tipping-off-police-about-terrorist-plot-to-kill-him-246158.html" title="This Auto Driver Might Have Saved PM Modi's Life After Tipping Off Police About Terrorist Plot To Kill Him">
                            This Auto Driver Might Have Saved PM Modi's Life After Tipping Off Police About Terrorist Plot To Kill Him                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/pele-frenzy-grips-kolkata-festivities-begin-a-week-early-thanks-to-the-football-legend-246152.html" title="Pele Frenzy Grips Kolkata! Festivities Begin A Week Early Thanks To The Football Legend" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Oct/pelepuja_1444646572_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/pelepuja_1444646572_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/pele-frenzy-grips-kolkata-festivities-begin-a-week-early-thanks-to-the-football-legend-246152.html" title="Pele Frenzy Grips Kolkata! Festivities Begin A Week Early Thanks To The Football Legend">
                            Pele Frenzy Grips Kolkata! Festivities Begin A Week Early Thanks To The Football Legend                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

            

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/videos/life-is-easy-why-make-it-so-hard-watch-this-farmer-decode-the-secrets-of-happy-existence-245707.html'>video</a>                           
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/videos/life-is-easy-why-make-it-so-hard-watch-this-farmer-decode-the-secrets-of-happy-existence-245707.html" class="tint" title="Life Is Easy, Why Make It So Hard? Watch This Farmer Decode The Secrets Of Happy Existence" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/videos/life-is-easy-why-make-it-so-hard-watch-this-farmer-decode-the-secrets-of-happy-existence-245707.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Sep/life-card_1443525197_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Sep/life-card_1443525197_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/videos/life-is-easy-why-make-it-so-hard-watch-this-farmer-decode-the-secrets-of-happy-existence-245707.html" title="Life Is Easy, Why Make It So Hard? Watch This Farmer Decode The Secrets Of Happy Existence" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/videos/life-is-easy-why-make-it-so-hard-watch-this-farmer-decode-the-secrets-of-happy-existence-245707.html');">
                            Life Is Easy, Why Make It So Hard? Watch This Farmer Decode The Secrets Of Happy Existence                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-reasons-you-can-love-or-hate-but-never-ignore-a-scorpion-245889.html" class="tint" title="14 Reasons You Can Love Or Hate, But Never Ignore A Scorpion" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/14-reasons-you-can-love-or-hate-but-never-ignore-a-scorpion-245889.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/card_1444040906_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/card_1444040906_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-reasons-you-can-love-or-hate-but-never-ignore-a-scorpion-245889.html" title="14 Reasons You Can Love Or Hate, But Never Ignore A Scorpion" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/14-reasons-you-can-love-or-hate-but-never-ignore-a-scorpion-245889.html');">
                            14 Reasons You Can Love Or Hate, But Never Ignore A Scorpion                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/if-you-re-not-a-software-engineer-you-ve-missed-a-lot-of-fun-in-life-here-s-why-246157.html'>video</a>                           
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/if-you-re-not-a-software-engineer-you-ve-missed-a-lot-of-fun-in-life-here-s-why-246157.html" class="tint" title="If You're Not A Software Engineer You've Missed A Lot Of Fun In Life. Here's Why!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/videocafe/if-you-re-not-a-software-engineer-you-ve-missed-a-lot-of-fun-in-life-here-s-why-246157.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Oct/software_eng_card_1444650953_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Oct/software_eng_card_1444650953_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/if-you-re-not-a-software-engineer-you-ve-missed-a-lot-of-fun-in-life-here-s-why-246157.html" title="If You're Not A Software Engineer You've Missed A Lot Of Fun In Life. Here's Why!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/videocafe/if-you-re-not-a-software-engineer-you-ve-missed-a-lot-of-fun-in-life-here-s-why-246157.html');">
                            If You're Not A Software Engineer You've Missed A Lot Of Fun In Life. Here's Why!                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/14-reasons-you-can-love-or-hate-but-never-ignore-a-scorpion-245889.html" class="tint" title="14 Reasons You Can Love Or Hate, But Never Ignore A Scorpion">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/card_1444040906_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/card_1444040906_218x102.jpg" border="0" alt="14 Reasons You Can Love Or Hate, But Never Ignore A Scorpion"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-reasons-you-can-love-or-hate-but-never-ignore-a-scorpion-245889.html" title="14 Reasons You Can Love Or Hate, But Never Ignore A Scorpion">
                            14 Reasons You Can Love Or Hate, But Never Ignore A Scorpion                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-great-khali-has-finally-met-his-match-you-ll-be-surprised-to-find-out-what-it-is-246145.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/the-great-khali-has-finally-met-his-match-you-ll-be-surprised-to-find-out-what-it-is-246145.html" class="tint" title="The Great Khali Has Finally Met His Match. You'll Be Surprised To Find Out What It Is!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Oct/khali_card_1444638126_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Oct/khali_card_1444638126_218x102.jpg" border="0" alt="The Great Khali Has Finally Met His Match. You'll Be Surprised To Find Out What It Is!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/the-great-khali-has-finally-met-his-match-you-ll-be-surprised-to-find-out-what-it-is-246145.html" title="The Great Khali Has Finally Met His Match. You'll Be Surprised To Find Out What It Is!">
                            The Great Khali Has Finally Met His Match. You'll Be Surprised To Find Out What It Is!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/12-quotes-that-will-help-you-let-go-of-the-past-and-be-a-happier-person-245975.html" class="tint" title="12 Quotes That Will Help You Let Go Of The Past And Be A Happier Person">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/card-3_1444211502_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/card-3_1444211502_218x102.jpg" border="0" alt="12 Quotes That Will Help You Let Go Of The Past And Be A Happier Person"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/12-quotes-that-will-help-you-let-go-of-the-past-and-be-a-happier-person-245975.html" title="12 Quotes That Will Help You Let Go Of The Past And Be A Happier Person">
                            12 Quotes That Will Help You Let Go Of The Past And Be A Happier Person                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/bollywood-s-idea-of-a-hit-movie-kidnap-someone-doesn-talwayswork-246143.html" class="tint" title="Bollywood's Idea Of A Hit Movie - Kidnap Someone! #DoesntAlwaysWork">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/kidnapping_1444637224_1444637235_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/kidnapping_1444637224_1444637235_218x102.jpg" border="0" alt="Bollywood's Idea Of A Hit Movie - Kidnap Someone! #DoesntAlwaysWork"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/bollywood-s-idea-of-a-hit-movie-kidnap-someone-doesn-talwayswork-246143.html" title="Bollywood's Idea Of A Hit Movie - Kidnap Someone! #DoesntAlwaysWork">
                            Bollywood's Idea Of A Hit Movie - Kidnap Someone! #DoesntAlwaysWork                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/9-beautiful-home-decor-products-that-you-can-build-yourself-using-everyday-items-diy-246136.html" class="tint" title="9 Beautiful Home Decor Products That You Can Build Yourself Using Everyday Items #DIY">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Oct/2_1444647141_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/2_1444647141_218x102.jpg" border="0" alt="9 Beautiful Home Decor Products That You Can Build Yourself Using Everyday Items #DIY"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/9-beautiful-home-decor-products-that-you-can-build-yourself-using-everyday-items-diy-246136.html" title="9 Beautiful Home Decor Products That You Can Build Yourself Using Everyday Items #DIY">
                            9 Beautiful Home Decor Products That You Can Build Yourself Using Everyday Items #DIY                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/world-s-first-university-for-porn-opens-up-aspiring-actors-look-forward-to-graduate-cum-laude-246155.html" title="World's First University For Porn Opens Up, Aspiring Actors Look Forward To Graduate Cum Laude" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/mia-khalifa-503_1444654619_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/world-s-first-university-for-porn-opens-up-aspiring-actors-look-forward-to-graduate-cum-laude-246155.html" title="World's First University For Porn Opens Up, Aspiring Actors Look Forward To Graduate Cum Laude">
                            World's First University For Porn Opens Up, Aspiring Actors Look Forward To Graduate Cum Laude                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/nasa-s-recent-snaps-reveal-the-exact-spot-where-matt-damon-walked-the-planet-in-the-martian-246148.html" title="NASA's Recent Snaps Reveal The Exact Spot Where Matt Damon Walked The Planet In 'The Martian'" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/mart502_1444643106_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/nasa-s-recent-snaps-reveal-the-exact-spot-where-matt-damon-walked-the-planet-in-the-martian-246148.html" title="NASA's Recent Snaps Reveal The Exact Spot Where Matt Damon Walked The Planet In 'The Martian'">
                            NASA's Recent Snaps Reveal The Exact Spot Where Matt Damon Walked The Planet In 'The Martian'                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/welcome-to-the-village-of-highway-widows-here-you-cross-the-road-to-meet-your-doom-246160.html" title="Welcome To The 'Village Of Highway Widows', Here You Cross The Road To Meet Your Doom" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/job-502_1444652413_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/welcome-to-the-village-of-highway-widows-here-you-cross-the-road-to-meet-your-doom-246160.html" title="Welcome To The 'Village Of Highway Widows', Here You Cross The Road To Meet Your Doom">
                            Welcome To The 'Village Of Highway Widows', Here You Cross The Road To Meet Your Doom                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/mallya-might-have-hidden-rs-4-000-crores-from-bank-loans-in-tax-havens-across-the-world-246149.html" title="Mallya Has Reportedly Hidden Rs 4,000 Crores From Bank Loans In Tax Havens Across The World" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/mally-_1444645056_1444645059_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/mallya-might-have-hidden-rs-4-000-crores-from-bank-loans-in-tax-havens-across-the-world-246149.html" title="Mallya Has Reportedly Hidden Rs 4,000 Crores From Bank Loans In Tax Havens Across The World">
                            Mallya Has Reportedly Hidden Rs 4,000 Crores From Bank Loans In Tax Havens Across The World                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            16 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/utter-disregard-for-pakistan-diplomat-s-book-launch-as-shiv-sena-smears-black-paint-on-organiser-s-face-246138.html" title="Utter Disregard For Pakistan Diplomat's Book Launch As Shiv Sena Smears Black Paint On Organiser's Face" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/kulkarni5_1444636273_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/utter-disregard-for-pakistan-diplomat-s-book-launch-as-shiv-sena-smears-black-paint-on-organiser-s-face-246138.html" title="Utter Disregard For Pakistan Diplomat's Book Launch As Shiv Sena Smears Black Paint On Organiser's Face">
                            Utter Disregard For Pakistan Diplomat's Book Launch As Shiv Sena Smears Black Paint On Organiser's Face                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/bollywood-s-idea-of-a-hit-movie-kidnap-someone-doesn-talwayswork-246143.html" class="tint" title="Bollywood's Idea Of A Hit Movie - Kidnap Someone! #DoesntAlwaysWork">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/kidnapping_1444637224_1444637235_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/bollywood-s-idea-of-a-hit-movie-kidnap-someone-doesn-talwayswork-246143.html" title="Bollywood's Idea Of A Hit Movie - Kidnap Someone! #DoesntAlwaysWork">
                            Bollywood's Idea Of A Hit Movie - Kidnap Someone! #DoesntAlwaysWork                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/tips-tricks/23-foolproof-ways-to-be-a-happier-person-246075.html" class="tint" title="23 Foolproof Ways To Be A Happier Person!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-4_1444394195_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/23-foolproof-ways-to-be-a-happier-person-246075.html" title="23 Foolproof Ways To Be A Happier Person!">
                            23 Foolproof Ways To Be A Happier Person!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/9-beautiful-home-decor-products-that-you-can-build-yourself-using-everyday-items-diy-246136.html" class="tint" title="9 Beautiful Home Decor Products That You Can Build Yourself Using Everyday Items #DIY">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/2_1444647141_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/9-beautiful-home-decor-products-that-you-can-build-yourself-using-everyday-items-diy-246136.html" title="9 Beautiful Home Decor Products That You Can Build Yourself Using Everyday Items #DIY">
                            9 Beautiful Home Decor Products That You Can Build Yourself Using Everyday Items #DIY                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/from-religious-fanaticism-to-cow-slaughter-here-s-everything-you-ever-wanted-to-know-about-beefban-246107.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/from-religious-fanaticism-to-cow-slaughter-here-s-everything-you-ever-wanted-to-know-about-beefban-246107.html" class="tint" title="From Religious Fanaticism To Cow Slaughter, Here's Everything You Ever Wanted To Know About #BeefBan">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/beefban_card_1444543407_218x102.jpg" border="0" alt="From Religious Fanaticism To Cow Slaughter, Here's Everything You Ever Wanted To Know About #BeefBan"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/from-religious-fanaticism-to-cow-slaughter-here-s-everything-you-ever-wanted-to-know-about-beefban-246107.html" title="From Religious Fanaticism To Cow Slaughter, Here's Everything You Ever Wanted To Know About #BeefBan">
                            From Religious Fanaticism To Cow Slaughter, Here's Everything You Ever Wanted To Know About #BeefBan                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/after-the-grand-premiere-of-bigg-boss-9-here-s-a-rundown-on-all-the-14-housemates-246129.html" class="tint" title="After The Grand Premiere Of Bigg Boss 9, Here's A Rundown On All The 14 Housemates!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card2_1444627398_1444627413_218x102.jpg" border="0" alt="After The Grand Premiere Of Bigg Boss 9, Here's A Rundown On All The 14 Housemates!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/after-the-grand-premiere-of-bigg-boss-9-here-s-a-rundown-on-all-the-14-housemates-246129.html" title="After The Grand Premiere Of Bigg Boss 9, Here's A Rundown On All The 14 Housemates!">
                            After The Grand Premiere Of Bigg Boss 9, Here's A Rundown On All The 14 Housemates!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/tips-tricks/23-foolproof-ways-to-be-a-happier-person-246075.html" class="tint" title="23 Foolproof Ways To Be A Happier Person!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-4_1444394195_218x102.jpg" border="0" alt="23 Foolproof Ways To Be A Happier Person!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/23-foolproof-ways-to-be-a-happier-person-246075.html" title="23 Foolproof Ways To Be A Happier Person!">
                            23 Foolproof Ways To Be A Happier Person!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/healthyliving/8-exercises-to-melt-that-bulging-muffin-top-246054.html" class="tint" title="8 Exercises To Melt That Bulging Muffin Top">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-2_1444384489_218x102.jpg" border="0" alt="8 Exercises To Melt That Bulging Muffin Top"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/8-exercises-to-melt-that-bulging-muffin-top-246054.html" title="8 Exercises To Melt That Bulging Muffin Top">
                            8 Exercises To Melt That Bulging Muffin Top                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/a-dancing-abram-s-joy-ride-with-daddy-shahrukh-is-the-cutest-thing-you-ll-see-today-246142.html" class="tint" title="A Dancing AbRam's Joy Ride With Daddy Shahrukh Is The Cutest Thing You'll See Today!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/srkabramml_1444636782_1444636790_218x102.jpg" border="0" alt="A Dancing AbRam's Joy Ride With Daddy Shahrukh Is The Cutest Thing You'll See Today!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/a-dancing-abram-s-joy-ride-with-daddy-shahrukh-is-the-cutest-thing-you-ll-see-today-246142.html" title="A Dancing AbRam's Joy Ride With Daddy Shahrukh Is The Cutest Thing You'll See Today!">
                            A Dancing AbRam's Joy Ride With Daddy Shahrukh Is The Cutest Thing You'll See Today!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/this-boy-is-forced-to-live-in-a-graveyard-by-his-village-because-his-father-has-aids-246140.html" title="This Boy Is Forced To Live In A Graveyard By His Village Because His Father Has AIDS" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/reuters-five_1444636090_1444636093_236x111.jpg" border="0" alt="This Boy Is Forced To Live In A Graveyard By His Village Because His Father Has AIDS"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-boy-is-forced-to-live-in-a-graveyard-by-his-village-because-his-father-has-aids-246140.html" title="This Boy Is Forced To Live In A Graveyard By His Village Because His Father Has AIDS">
                            This Boy Is Forced To Live In A Graveyard By His Village Because His Father Has AIDS                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/next-time-you-put-on-some-makeup-remember-the-starving-children-who-put-the-sparkle-in-it-246144.html" title="Next Time You Put On Some Makeup, Remember The Starving Children Who Put The Sparkle In It" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/make-up-502_1444637540_236x111.jpg" border="0" alt="Next Time You Put On Some Makeup, Remember The Starving Children Who Put The Sparkle In It"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/next-time-you-put-on-some-makeup-remember-the-starving-children-who-put-the-sparkle-in-it-246144.html" title="Next Time You Put On Some Makeup, Remember The Starving Children Who Put The Sparkle In It">
                            Next Time You Put On Some Makeup, Remember The Starving Children Who Put The Sparkle In It                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/she-was-trapped-in-a-boy-s-body-here-s-how-a-16-year-old-set-herself-free-246132.html" title="She Was Trapped In A Boy's Body. Here's How A 16-Year-Old Set Herself Free" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage-five_1444631783_236x111.jpg" border="0" alt="She Was Trapped In A Boy's Body. Here's How A 16-Year-Old Set Herself Free"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/she-was-trapped-in-a-boy-s-body-here-s-how-a-16-year-old-set-herself-free-246132.html" title="She Was Trapped In A Boy's Body. Here's How A 16-Year-Old Set Herself Free">
                            She Was Trapped In A Boy's Body. Here's How A 16-Year-Old Set Herself Free                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/after-female-fighter-pilots-women-will-finally-get-combat-roles-in-the-armed-forces-246134.html" title="After Female Fighter Pilots, Women Will Finally Get Combat Roles In The Armed Forces Soon" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/toi-502_1444632745_236x111.jpg" border="0" alt="After Female Fighter Pilots, Women Will Finally Get Combat Roles In The Armed Forces Soon"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/after-female-fighter-pilots-women-will-finally-get-combat-roles-in-the-armed-forces-246134.html" title="After Female Fighter Pilots, Women Will Finally Get Combat Roles In The Armed Forces Soon">
                            After Female Fighter Pilots, Women Will Finally Get Combat Roles In The Armed Forces Soon                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/deepika-padukone-jumps-to-farmers-aid-on-maharashtra-cm-s-request-launches-live-love-life-intiative-246131.html" title="Deepika Padukone Jumps To Farmers' Aid On Maharashtra CM's Request, Launches 'Live Love Life' Initiative" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/deep502_1444630012_236x111.jpg" border="0" alt="Deepika Padukone Jumps To Farmers' Aid On Maharashtra CM's Request, Launches 'Live Love Life' Initiative"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/deepika-padukone-jumps-to-farmers-aid-on-maharashtra-cm-s-request-launches-live-love-life-intiative-246131.html" title="Deepika Padukone Jumps To Farmers' Aid On Maharashtra CM's Request, Launches 'Live Love Life' Initiative">
                            Deepika Padukone Jumps To Farmers' Aid On Maharashtra CM's Request, Launches 'Live Love Life' Initiative                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/watch-how-these-guys-rescued-newborn-kittens-trapped-inside-an-exhaust-duct-for-2-days-246137.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/watch-how-these-guys-rescued-newborn-kittens-trapped-inside-an-exhaust-duct-for-2-days-246137.html" class="tint" title="Watch How These Guys Rescued Newborn Kittens Trapped Inside An Exhaust Duct For 2 Days!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/kitten_card_1444634694_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/watch-how-these-guys-rescued-newborn-kittens-trapped-inside-an-exhaust-duct-for-2-days-246137.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/watch-how-these-guys-rescued-newborn-kittens-trapped-inside-an-exhaust-duct-for-2-days-246137.html" title="Watch How These Guys Rescued Newborn Kittens Trapped Inside An Exhaust Duct For 2 Days!">
                            Watch How These Guys Rescued Newborn Kittens Trapped Inside An Exhaust Duct For 2 Days!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/whoa-barack-obama-gives-a-piece-of-his-mind-to-kanye-west-on-his-presidential-plans-246146.html" class="tint" title="Whoa! Barack Obama Gives A Piece Of His Mind To Kanye West On His Presidential Plans!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1444640384_1444640391_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/celebs/whoa-barack-obama-gives-a-piece-of-his-mind-to-kanye-west-on-his-presidential-plans-246146.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/whoa-barack-obama-gives-a-piece-of-his-mind-to-kanye-west-on-his-presidential-plans-246146.html" title="Whoa! Barack Obama Gives A Piece Of His Mind To Kanye West On His Presidential Plans!">
                            Whoa! Barack Obama Gives A Piece Of His Mind To Kanye West On His Presidential Plans!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/dutiful-daughter-sonakshi-sinha-gives-all-her-pay-cheques-to-her-parents-246147.html" class="tint" title="Dutiful Daughter Sonakshi Sinha Hands Over All Her Pay Cheques To Her Parents!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/crd_1444640859_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bollywood/dutiful-daughter-sonakshi-sinha-gives-all-her-pay-cheques-to-her-parents-246147.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/dutiful-daughter-sonakshi-sinha-gives-all-her-pay-cheques-to-her-parents-246147.html" title="Dutiful Daughter Sonakshi Sinha Hands Over All Her Pay Cheques To Her Parents!">
                            Dutiful Daughter Sonakshi Sinha Hands Over All Her Pay Cheques To Her Parents!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/food/7-food-and-drink-festivals-around-india-you-have-to-attend-this-october-on-245904.html" class="tint" title="7 Food-And-Drink Festivals Around India You Have To Attend This October On">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cp_1444110528_218x102.jpg" border="0" alt="7 Food-And-Drink Festivals Around India You Have To Attend This October On"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/food/7-food-and-drink-festivals-around-india-you-have-to-attend-this-october-on-245904.html" title="7 Food-And-Drink Festivals Around India You Have To Attend This October On">
                            7 Food-And-Drink Festivals Around India You Have To Attend This October On                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/people-are-selling-goats-and-used-underwear-online-here-are-14-hilarious-online-classifieds-that-are-selling-crazy-things-229596.html" class="tint" title="15 Hilarious Online Classifieds That Are Selling Crazy Things">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/selling_1444563111_1444563123_218x102.jpg" border="0" alt="15 Hilarious Online Classifieds That Are Selling Crazy Things"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/people-are-selling-goats-and-used-underwear-online-here-are-14-hilarious-online-classifieds-that-are-selling-crazy-things-229596.html" title="15 Hilarious Online Classifieds That Are Selling Crazy Things">
                            15 Hilarious Online Classifieds That Are Selling Crazy Things                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/if-you-re-not-a-software-engineer-you-ve-missed-a-lot-of-fun-in-life-here-s-why-246157.html'>video</a>		

                        <a href="http://www.indiatimes.com/videocafe/if-you-re-not-a-software-engineer-you-ve-missed-a-lot-of-fun-in-life-here-s-why-246157.html" class="tint" title="If You're Not A Software Engineer You've Missed A Lot Of Fun In Life. Here's Why!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/software_eng_card_1444650953_218x102.jpg" border="0" alt="If You're Not A Software Engineer You've Missed A Lot Of Fun In Life. Here's Why!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/if-you-re-not-a-software-engineer-you-ve-missed-a-lot-of-fun-in-life-here-s-why-246157.html" title="If You're Not A Software Engineer You've Missed A Lot Of Fun In Life. Here's Why!">
                            If You're Not A Software Engineer You've Missed A Lot Of Fun In Life. Here's Why!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/dutiful-daughter-sonakshi-sinha-gives-all-her-pay-cheques-to-her-parents-246147.html" class="tint" title="Dutiful Daughter Sonakshi Sinha Hands Over All Her Pay Cheques To Her Parents!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/crd_1444640859_218x102.jpg" border="0" alt="Dutiful Daughter Sonakshi Sinha Hands Over All Her Pay Cheques To Her Parents!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/dutiful-daughter-sonakshi-sinha-gives-all-her-pay-cheques-to-her-parents-246147.html" title="Dutiful Daughter Sonakshi Sinha Hands Over All Her Pay Cheques To Her Parents!">
                            Dutiful Daughter Sonakshi Sinha Hands Over All Her Pay Cheques To Her Parents!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/born-without-legs-she-decided-to-not-let-that-stop-her-from-becoming-a-model-and-an-athlete-246125.html" class="tint" title="Born Without Legs, She Decided To Not Let That Stop Her From Becoming A Model And An Athlete">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/ggfrf_1444623360_1444623375_218x102.jpg" border="0" alt="Born Without Legs, She Decided To Not Let That Stop Her From Becoming A Model And An Athlete"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/born-without-legs-she-decided-to-not-let-that-stop-her-from-becoming-a-model-and-an-athlete-246125.html" title="Born Without Legs, She Decided To Not Let That Stop Her From Becoming A Model And An Athlete">
                            Born Without Legs, She Decided To Not Let That Stop Her From Becoming A Model And An Athlete                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            19 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/thanks-to-skewed-sex-ratio-nearly-5-000-patel-grooms-queued-up-to-marry-42-oriya-brides-246128.html" title="Thanks To Skewed Sex Ratio Nearly 5,000 Patel Grooms Queued Up To Marry 42 Oriya Brides!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/patel---card_1444626976_236x111.jpg" border="0" alt="Thanks To Skewed Sex Ratio Nearly 5,000 Patel Grooms Queued Up To Marry 42 Oriya Brides!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/thanks-to-skewed-sex-ratio-nearly-5-000-patel-grooms-queued-up-to-marry-42-oriya-brides-246128.html" title="Thanks To Skewed Sex Ratio Nearly 5,000 Patel Grooms Queued Up To Marry 42 Oriya Brides!">
                            Thanks To Skewed Sex Ratio Nearly 5,000 Patel Grooms Queued Up To Marry 42 Oriya Brides!                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/from-london-to-los-angeles-the-international-langar-week-was-celebrated-across-11-nations-246127.html" title="From London To Los Angeles, The International Langar Week Was Celebrated Across 11 Nations" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/international-langar-week_1444571052_236x111.jpg" border="0" alt="From London To Los Angeles, The International Langar Week Was Celebrated Across 11 Nations"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/from-london-to-los-angeles-the-international-langar-week-was-celebrated-across-11-nations-246127.html" title="From London To Los Angeles, The International Langar Week Was Celebrated Across 11 Nations">
                            From London To Los Angeles, The International Langar Week Was Celebrated Across 11 Nations                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/weird/these-11-humans-have-real-superpowers-that-even-science-can-t-explain-246123.html" title="These 11 Humans Have Real Superpowers That Even Science Canât Explain!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/pjj_1444566534_1444566548_236x111.jpg" border="0" alt="These 11 Humans Have Real Superpowers That Even Science Canât Explain!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/these-11-humans-have-real-superpowers-that-even-science-can-t-explain-246123.html" title="These 11 Humans Have Real Superpowers That Even Science Canât Explain!">
                            These 11 Humans Have Real Superpowers That Even Science Canât Explain!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/man-handed-a-jail-sentence-because-of-a-bounced-cheque-he-issued-17-years-ago-246124.html" title="Man Handed Jail Sentence Because Of A Bounced Cheque He Issued 17 Years Ago" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/bank-502_1444564819_236x111.jpg" border="0" alt="Man Handed Jail Sentence Because Of A Bounced Cheque He Issued 17 Years Ago"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/man-handed-a-jail-sentence-because-of-a-bounced-cheque-he-issued-17-years-ago-246124.html" title="Man Handed Jail Sentence Because Of A Bounced Cheque He Issued 17 Years Ago">
                            Man Handed Jail Sentence Because Of A Bounced Cheque He Issued 17 Years Ago                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/this-is-the-temple-steve-jobs-told-mark-zuckerberg-to-visit-when-facebook-was-struggling-245782.html" title="Nainital's Kainchi Dham Sees Flood Of Visitors After News Of Facebook CEO Mark Zuckerberg's Visit" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage-555_1443693874_1443693881_236x111.jpg" border="0" alt="Nainital's Kainchi Dham Sees Flood Of Visitors After News Of Facebook CEO Mark Zuckerberg's Visit"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-is-the-temple-steve-jobs-told-mark-zuckerberg-to-visit-when-facebook-was-struggling-245782.html" title="Nainital's Kainchi Dham Sees Flood Of Visitors After News Of Facebook CEO Mark Zuckerberg's Visit">
                            Nainital's Kainchi Dham Sees Flood Of Visitors After News Of Facebook CEO Mark Zuckerberg's Visit                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/12-quotes-that-will-help-you-let-go-of-the-past-and-be-a-happier-person-245975.html" class="tint" title="12 Quotes That Will Help You Let Go Of The Past And Be A Happier Person">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-3_1444211502_502x234.jpg" border="0" alt="12 Quotes That Will Help You Let Go Of The Past And Be A Happier Person" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/12-quotes-that-will-help-you-let-go-of-the-past-and-be-a-happier-person-245975.html" title="12 Quotes That Will Help You Let Go Of The Past And Be A Happier Person">
                        12 Quotes That Will Help You Let Go Of The Past And Be A Happier Person                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/from-religious-fanaticism-to-cow-slaughter-here-s-everything-you-ever-wanted-to-know-about-beefban-246107.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/from-religious-fanaticism-to-cow-slaughter-here-s-everything-you-ever-wanted-to-know-about-beefban-246107.html" class="tint" title="From Religious Fanaticism To Cow Slaughter, Here's Everything You Ever Wanted To Know About #BeefBan">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/beefban_card_1444543407_502x234.jpg" border="0" alt="From Religious Fanaticism To Cow Slaughter, Here's Everything You Ever Wanted To Know About #BeefBan" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/from-religious-fanaticism-to-cow-slaughter-here-s-everything-you-ever-wanted-to-know-about-beefban-246107.html" title="From Religious Fanaticism To Cow Slaughter, Here's Everything You Ever Wanted To Know About #BeefBan">
                        From Religious Fanaticism To Cow Slaughter, Here's Everything You Ever Wanted To Know About #BeefBan                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-great-khali-has-finally-met-his-match-you-ll-be-surprised-to-find-out-what-it-is-246145.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-great-khali-has-finally-met-his-match-you-ll-be-surprised-to-find-out-what-it-is-246145.html" class="tint" title="The Great Khali Has Finally Met His Match. You'll Be Surprised To Find Out What It Is!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/khali_card_1444638126_502x234.jpg" border="0" alt="The Great Khali Has Finally Met His Match. You'll Be Surprised To Find Out What It Is!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-great-khali-has-finally-met-his-match-you-ll-be-surprised-to-find-out-what-it-is-246145.html" title="The Great Khali Has Finally Met His Match. You'll Be Surprised To Find Out What It Is!">
                        The Great Khali Has Finally Met His Match. You'll Be Surprised To Find Out What It Is!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/watch-how-your-immune-system-fights-daily-battles-to-keep-you-alive-245552.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/watch-how-your-immune-system-fights-daily-battles-to-keep-you-alive-245552.html" class="tint" title="Watch How Your Immune System Fights Daily Battles To Keep You Alive!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/card-4_1443088631_502x234.jpg" border="0" alt="Watch How Your Immune System Fights Daily Battles To Keep You Alive!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/watch-how-your-immune-system-fights-daily-battles-to-keep-you-alive-245552.html" title="Watch How Your Immune System Fights Daily Battles To Keep You Alive!">
                        Watch How Your Immune System Fights Daily Battles To Keep You Alive!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/a-dancing-abram-s-joy-ride-with-daddy-shahrukh-is-the-cutest-thing-you-ll-see-today-246142.html" class="tint" title="A Dancing AbRam's Joy Ride With Daddy Shahrukh Is The Cutest Thing You'll See Today!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/srkabramml_1444636782_1444636790_502x234.jpg" border="0" alt="A Dancing AbRam's Joy Ride With Daddy Shahrukh Is The Cutest Thing You'll See Today!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/a-dancing-abram-s-joy-ride-with-daddy-shahrukh-is-the-cutest-thing-you-ll-see-today-246142.html" title="A Dancing AbRam's Joy Ride With Daddy Shahrukh Is The Cutest Thing You'll See Today!">
                        A Dancing AbRam's Joy Ride With Daddy Shahrukh Is The Cutest Thing You'll See Today!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/after-the-grand-premiere-of-bigg-boss-9-here-s-a-rundown-on-all-the-14-housemates-246129.html" class="tint" title="After The Grand Premiere Of Bigg Boss 9, Here's A Rundown On All The 14 Housemates!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card2_1444627398_1444627413_502x234.jpg" border="0" alt="After The Grand Premiere Of Bigg Boss 9, Here's A Rundown On All The 14 Housemates!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/after-the-grand-premiere-of-bigg-boss-9-here-s-a-rundown-on-all-the-14-housemates-246129.html" title="After The Grand Premiere Of Bigg Boss 9, Here's A Rundown On All The 14 Housemates!">
                        After The Grand Premiere Of Bigg Boss 9, Here's A Rundown On All The 14 Housemates!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/7-food-and-drink-festivals-around-india-you-have-to-attend-this-october-on-245904.html" class="tint" title="7 Food-And-Drink Festivals Around India You Have To Attend This October On">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cp_1444110528_502x234.jpg" border="0" alt="7 Food-And-Drink Festivals Around India You Have To Attend This October On" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/7-food-and-drink-festivals-around-india-you-have-to-attend-this-october-on-245904.html" title="7 Food-And-Drink Festivals Around India You Have To Attend This October On">
                        7 Food-And-Drink Festivals Around India You Have To Attend This October On                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/abuses-and-cat-calling-in-the-parliament-are-passe-kosovo-politicians-use-tear-gas-and-whistles-246130.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/abuses-and-cat-calling-in-the-parliament-are-passe-kosovo-politicians-use-tear-gas-and-whistles-246130.html" class="tint" title="Abuses And Cat-Calling In The Parliament Are Passe, Kosovo Politicians Use Tear Gas And Whistles!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/kosovo_parl_card_1444629293_502x234.jpg" border="0" alt="Abuses And Cat-Calling In The Parliament Are Passe, Kosovo Politicians Use Tear Gas And Whistles!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/abuses-and-cat-calling-in-the-parliament-are-passe-kosovo-politicians-use-tear-gas-and-whistles-246130.html" title="Abuses And Cat-Calling In The Parliament Are Passe, Kosovo Politicians Use Tear Gas And Whistles!">
                        Abuses And Cat-Calling In The Parliament Are Passe, Kosovo Politicians Use Tear Gas And Whistles!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/8-exercises-to-melt-that-bulging-muffin-top-246054.html" class="tint" title="8 Exercises To Melt That Bulging Muffin Top">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-2_1444384489_502x234.jpg" border="0" alt="8 Exercises To Melt That Bulging Muffin Top" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/8-exercises-to-melt-that-bulging-muffin-top-246054.html" title="8 Exercises To Melt That Bulging Muffin Top">
                        8 Exercises To Melt That Bulging Muffin Top                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/you-can-live-till-you-re-150-if-only-you-give-up-on-sex-246122.html" class="tint" title="You Can Live Till You're 150, If Only You Give Up On Sex">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/love-502_1444560955_502x234.jpg" border="0" alt="You Can Live Till You're 150, If Only You Give Up On Sex" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/you-can-live-till-you-re-150-if-only-you-give-up-on-sex-246122.html" title="You Can Live Till You're 150, If Only You Give Up On Sex">
                        You Can Live Till You're 150, If Only You Give Up On Sex                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/13-friendship-lessons-straight-from-bollywood-that-will-make-you-a-better-friend-245947.html" class="tint" title="13 Friendship Lessons Straight From Bollywood That Will Make You A Better Friend">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/sholey2_1444135591_1444135595_502x234.jpg" border="0" alt="13 Friendship Lessons Straight From Bollywood That Will Make You A Better Friend" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/13-friendship-lessons-straight-from-bollywood-that-will-make-you-a-better-friend-245947.html" title="13 Friendship Lessons Straight From Bollywood That Will Make You A Better Friend">
                        13 Friendship Lessons Straight From Bollywood That Will Make You A Better Friend                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/13-struggles-of-people-who-just-can-t-stay-up-late-245993.html" class="tint" title="13 Struggles Of People Who Just Canât Stay Up Late">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/deepika123_1444286976_1444286987_502x234.jpg" border="0" alt="13 Struggles Of People Who Just Canât Stay Up Late" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/13-struggles-of-people-who-just-can-t-stay-up-late-245993.html" title="13 Struggles Of People Who Just Canât Stay Up Late">
                        13 Struggles Of People Who Just Canât Stay Up Late                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-times-when-celebrities-got-smacked-in-the-face-and-made-headlines-246119.html" class="tint" title="8 Times When Celebrities Got Smacked In The Face And Made Headlines!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/slap1_1444557268_1444557275_502x234.jpg" border="0" alt="8 Times When Celebrities Got Smacked In The Face And Made Headlines!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-times-when-celebrities-got-smacked-in-the-face-and-made-headlines-246119.html" title="8 Times When Celebrities Got Smacked In The Face And Made Headlines!">
                        8 Times When Celebrities Got Smacked In The Face And Made Headlines!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/born-without-legs-she-decided-to-not-let-that-stop-her-from-becoming-a-model-and-an-athlete-246125.html" class="tint" title="Born Without Legs, She Decided To Not Let That Stop Her From Becoming A Model And An Athlete">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/ggfrf_1444623360_1444623375_502x234.jpg" border="0" alt="Born Without Legs, She Decided To Not Let That Stop Her From Becoming A Model And An Athlete" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/born-without-legs-she-decided-to-not-let-that-stop-her-from-becoming-a-model-and-an-athlete-246125.html" title="Born Without Legs, She Decided To Not Let That Stop Her From Becoming A Model And An Athlete">
                        Born Without Legs, She Decided To Not Let That Stop Her From Becoming A Model And An Athlete                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/people-are-selling-goats-and-used-underwear-online-here-are-14-hilarious-online-classifieds-that-are-selling-crazy-things-229596.html" class="tint" title="15 Hilarious Online Classifieds That Are Selling Crazy Things">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/selling_1444563111_1444563123_502x234.jpg" border="0" alt="15 Hilarious Online Classifieds That Are Selling Crazy Things" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/people-are-selling-goats-and-used-underwear-online-here-are-14-hilarious-online-classifieds-that-are-selling-crazy-things-229596.html" title="15 Hilarious Online Classifieds That Are Selling Crazy Things">
                        15 Hilarious Online Classifieds That Are Selling Crazy Things                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/this-hilarious-footage-of-robin-williams-as-aladdin-s-genie-will-teleport-you-to-your-childhood-246121.html" class="tint" title="This Hilarious Footage Of Robin Williams As Aladdin's Genie Will Teleport You To Your Childhood">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/ad_1444560257_1444560265_502x234.jpg" border="0" alt="This Hilarious Footage Of Robin Williams As Aladdin's Genie Will Teleport You To Your Childhood" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/this-hilarious-footage-of-robin-williams-as-aladdin-s-genie-will-teleport-you-to-your-childhood-246121.html" title="This Hilarious Footage Of Robin Williams As Aladdin's Genie Will Teleport You To Your Childhood">
                        This Hilarious Footage Of Robin Williams As Aladdin's Genie Will Teleport You To Your Childhood                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/after-their-liplock-went-viral-these-actresses-deleted-the-pic-but-gave-a-loud-and-clear-message-to-all-the-haters-246116.html" class="tint" title="After Their 'Liplock' Went Viral, These Actresses Deleted The Pic But Gave A Loud And Clear Message To All The Haters!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card1_1444553990_1444554007_502x234.jpg" border="0" alt="After Their 'Liplock' Went Viral, These Actresses Deleted The Pic But Gave A Loud And Clear Message To All The Haters!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/after-their-liplock-went-viral-these-actresses-deleted-the-pic-but-gave-a-loud-and-clear-message-to-all-the-haters-246116.html" title="After Their 'Liplock' Went Viral, These Actresses Deleted The Pic But Gave A Loud And Clear Message To All The Haters!">
                        After Their 'Liplock' Went Viral, These Actresses Deleted The Pic But Gave A Loud And Clear Message To All The Haters!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/despite-his-absence-at-deepika-s-foundation-launch-ranveer-supported-her-in-the-cutest-way-possible-246114.html" class="tint" title="Despite His Absence At Deepika's Foundation Launch, Ranveer Supported Her In The Cutest Way Possible!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/3_1444551623_1444551629_502x234.jpg" border="0" alt="Despite His Absence At Deepika's Foundation Launch, Ranveer Supported Her In The Cutest Way Possible!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/despite-his-absence-at-deepika-s-foundation-launch-ranveer-supported-her-in-the-cutest-way-possible-246114.html" title="Despite His Absence At Deepika's Foundation Launch, Ranveer Supported Her In The Cutest Way Possible!">
                        Despite His Absence At Deepika's Foundation Launch, Ranveer Supported Her In The Cutest Way Possible!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/nasa-astronauts-playing-with-liquid-colours-onboard-the-international-space-station-is-so-trippy-you-will-think-you-are-in-the-70s_-246120.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/nasa-astronauts-playing-with-liquid-colours-onboard-the-international-space-station-is-so-trippy-you-will-think-you-are-in-the-70s_-246120.html" class="tint" title="NASA Astronauts Playing With Liquid Colours Onboard The ISS Is So Trippy, You Will Think You Are In The 70s">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/nasa_card_1444559433_502x234.jpg" border="0" alt="NASA Astronauts Playing With Liquid Colours Onboard The ISS Is So Trippy, You Will Think You Are In The 70s" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/nasa-astronauts-playing-with-liquid-colours-onboard-the-international-space-station-is-so-trippy-you-will-think-you-are-in-the-70s_-246120.html" title="NASA Astronauts Playing With Liquid Colours Onboard The ISS Is So Trippy, You Will Think You Are In The 70s">
                        NASA Astronauts Playing With Liquid Colours Onboard The ISS Is So Trippy, You Will Think You Are In The 70s                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-spoof-of-dil-chahta-hai-is-so-bad-that-it-s-actually-good-246111.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-spoof-of-dil-chahta-hai-is-so-bad-that-it-s-actually-good-246111.html" class="tint" title="This Spoof Of 'Dil Chahta Hai' Is So Bad That It's Actually Good!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/dilchahtahai_card_1444548473_502x234.jpg" border="0" alt="This Spoof Of 'Dil Chahta Hai' Is So Bad That It's Actually Good!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-spoof-of-dil-chahta-hai-is-so-bad-that-it-s-actually-good-246111.html" title="This Spoof Of 'Dil Chahta Hai' Is So Bad That It's Actually Good!">
                        This Spoof Of 'Dil Chahta Hai' Is So Bad That It's Actually Good!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-movies-that-had-beautiful-endings-which-made-us-smile-cry-at-the-same-time-246042.html" class="tint" title="8 Movies That Had Beautiful Endings Which Made Us Smile & Cry At The Same Time!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-image_1444310044_1444310054_502x234.jpg" border="0" alt="8 Movies That Had Beautiful Endings Which Made Us Smile & Cry At The Same Time!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-movies-that-had-beautiful-endings-which-made-us-smile-cry-at-the-same-time-246042.html" title="8 Movies That Had Beautiful Endings Which Made Us Smile & Cry At The Same Time!">
                        8 Movies That Had Beautiful Endings Which Made Us Smile & Cry At The Same Time!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/14-habits-that-are-damaging-the-skin-on-your-face-246061.html" class="tint" title="14 Habits That Are Damaging The Skin On Your Face">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-1_1444382543_502x234.jpg" border="0" alt="14 Habits That Are Damaging The Skin On Your Face" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/14-habits-that-are-damaging-the-skin-on-your-face-246061.html" title="14 Habits That Are Damaging The Skin On Your Face">
                        14 Habits That Are Damaging The Skin On Your Face                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-are-the-most-popular-cuss-words-in-different-languages-all-across-the-globe-245157.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/these-are-the-most-popular-cuss-words-in-different-languages-all-across-the-globe-245157.html" class="tint" title="These Are The Most Popular Cuss Words In Different Languages All Across The Globe!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/insult_card_1442124756_502x234.jpg" border="0" alt="These Are The Most Popular Cuss Words In Different Languages All Across The Globe!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/these-are-the-most-popular-cuss-words-in-different-languages-all-across-the-globe-245157.html" title="These Are The Most Popular Cuss Words In Different Languages All Across The Globe!">
                        These Are The Most Popular Cuss Words In Different Languages All Across The Globe!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/dhanush-fans-can-now-rest-easy-velai-illa-pattadhaari-sequel-thangamagan-is-on-it-s-way-246110.html" class="tint" title="Dhanush Fans Can Now Rest Easy, 'Velai Illa Pattadhaari' Sequel 'Thangamagan' Is On It's Way">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/dhanush-502_1444546774_502x234.jpg" border="0" alt="Dhanush Fans Can Now Rest Easy, 'Velai Illa Pattadhaari' Sequel 'Thangamagan' Is On It's Way" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/dhanush-fans-can-now-rest-easy-velai-illa-pattadhaari-sequel-thangamagan-is-on-it-s-way-246110.html" title="Dhanush Fans Can Now Rest Easy, 'Velai Illa Pattadhaari' Sequel 'Thangamagan' Is On It's Way">
                        Dhanush Fans Can Now Rest Easy, 'Velai Illa Pattadhaari' Sequel 'Thangamagan' Is On It's Way                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/as-bigg-boss-season-9-is-all-set-to-kick-off-today-here-s-a-sneak-peak-of-the-grand-bb-house-246104.html" class="tint" title="As Bigg Boss Season 9 Is All Set To Kick Off Today, Here's A Sneak Peak Of The Grand BB House">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/bigg-boss-card_1444540860_1444540925_502x234.jpg" border="0" alt="As Bigg Boss Season 9 Is All Set To Kick Off Today, Here's A Sneak Peak Of The Grand BB House" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/as-bigg-boss-season-9-is-all-set-to-kick-off-today-here-s-a-sneak-peak-of-the-grand-bb-house-246104.html" title="As Bigg Boss Season 9 Is All Set To Kick Off Today, Here's A Sneak Peak Of The Grand BB House">
                        As Bigg Boss Season 9 Is All Set To Kick Off Today, Here's A Sneak Peak Of The Grand BB House                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/legendary-tamil-actress-manorama-passes-away-at-78-5-lesser-known-facts-about-the-multi-talented-aachi-246105.html" class="tint" title="Legendary Tamil Actress Manorama Passes Away At 78! + 5 Lesser Known Facts About The Multi-Talented 'Aachi'">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/1_1444541777_1444541786_502x234.jpg" border="0" alt="Legendary Tamil Actress Manorama Passes Away At 78! + 5 Lesser Known Facts About The Multi-Talented 'Aachi'" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/legendary-tamil-actress-manorama-passes-away-at-78-5-lesser-known-facts-about-the-multi-talented-aachi-246105.html" title="Legendary Tamil Actress Manorama Passes Away At 78! + 5 Lesser Known Facts About The Multi-Talented 'Aachi'">
                        Legendary Tamil Actress Manorama Passes Away At 78! + 5 Lesser Known Facts About The Multi-Talented 'Aachi'                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/4-exercises-that-will-get-you-the-hot-bikini-body-that-you-desire-246066.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/4-exercises-that-will-get-you-the-hot-bikini-body-that-you-desire-246066.html" class="tint" title="4 Exercises That Will Get You The Hot Bikini Body That You Desire">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/card_1444388629_502x234.jpg" border="0" alt="4 Exercises That Will Get You The Hot Bikini Body That You Desire" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/4-exercises-that-will-get-you-the-hot-bikini-body-that-you-desire-246066.html" title="4 Exercises That Will Get You The Hot Bikini Body That You Desire">
                        4 Exercises That Will Get You The Hot Bikini Body That You Desire                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/you-re-more-than-tits-butts-other-brutally-honest-things-kangana-said-in-this-interview-246102.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/you-re-more-than-tits-butts-other-brutally-honest-things-kangana-said-in-this-interview-246102.html" class="tint" title="'You're More Than Tits & Butts' + Other Brutally Honest Things Kangana Said In This Interview">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/kangana-video_1444538818_1444538826_502x234.jpg" border="0" alt="'You're More Than Tits & Butts' + Other Brutally Honest Things Kangana Said In This Interview" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/you-re-more-than-tits-butts-other-brutally-honest-things-kangana-said-in-this-interview-246102.html" title="'You're More Than Tits & Butts' + Other Brutally Honest Things Kangana Said In This Interview">
                        'You're More Than Tits & Butts' + Other Brutally Honest Things Kangana Said In This Interview                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/15-reasons-why-amitabh-bachchan-is-still-the-man-to-beat-in-bollywood-227889.html" class="tint" title="15 Reasons Why Amitabh Bachchan Is Still The Man To Beat In Bollywood">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2014/Oct/94161273_1412774335_1412774345_502x234.jpg" border="0" alt="15 Reasons Why Amitabh Bachchan Is Still The Man To Beat In Bollywood" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/15-reasons-why-amitabh-bachchan-is-still-the-man-to-beat-in-bollywood-227889.html" title="15 Reasons Why Amitabh Bachchan Is Still The Man To Beat In Bollywood">
                        15 Reasons Why Amitabh Bachchan Is Still The Man To Beat In Bollywood                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/13-harsh-realities-about-life-that-you-are-better-off-accepting-245800.html" class="tint" title="13 Harsh Realities About Life That You Are Better Off Accepting">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/khnh12_1444048403_1444048408_502x234.jpg" border="0" alt="13 Harsh Realities About Life That You Are Better Off Accepting" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/13-harsh-realities-about-life-that-you-are-better-off-accepting-245800.html" title="13 Harsh Realities About Life That You Are Better Off Accepting">
                        13 Harsh Realities About Life That You Are Better Off Accepting                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/court-issues-notice-to-aamir-khan-on-petition-filed-for-hurting-religious-sentiments-in-pk-246089.html" class="tint" title="Court Issues Notice To Aamir Khan On Petition Filed For Hurting Religious Sentiments In PK!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/pk01-oct23_1444461873_1444461881_502x234.jpg" border="0" alt="Court Issues Notice To Aamir Khan On Petition Filed For Hurting Religious Sentiments In PK!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/court-issues-notice-to-aamir-khan-on-petition-filed-for-hurting-religious-sentiments-in-pk-246089.html" title="Court Issues Notice To Aamir Khan On Petition Filed For Hurting Religious Sentiments In PK!">
                        Court Issues Notice To Aamir Khan On Petition Filed For Hurting Religious Sentiments In PK!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/brazilian-singer-yury-s-version-of-tujh-me-rab-dikhta-hai-has-shah-rukh-really-impressed-246096.html" class="tint" title="Brazilian Singer Yury's Version Of 'Tujh Me Rab Dikhta Hai' Has Shah Rukh Really Impressed!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1444473806_1444473814_502x234.jpg" border="0" alt="Brazilian Singer Yury's Version Of 'Tujh Me Rab Dikhta Hai' Has Shah Rukh Really Impressed!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/brazilian-singer-yury-s-version-of-tujh-me-rab-dikhta-hai-has-shah-rukh-really-impressed-246096.html" title="Brazilian Singer Yury's Version Of 'Tujh Me Rab Dikhta Hai' Has Shah Rukh Really Impressed!">
                        Brazilian Singer Yury's Version Of 'Tujh Me Rab Dikhta Hai' Has Shah Rukh Really Impressed!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/male-engineering-student-writes-about-the-gender-inequality-that-still-haunts-our-society-246099.html" class="tint" title="Male Engineering Student Writes About The Gender Inequality That Still Haunts Our Society">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1444478081_502x234.jpg" border="0" alt="Male Engineering Student Writes About The Gender Inequality That Still Haunts Our Society" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/male-engineering-student-writes-about-the-gender-inequality-that-still-haunts-our-society-246099.html" title="Male Engineering Student Writes About The Gender Inequality That Still Haunts Our Society">
                        Male Engineering Student Writes About The Gender Inequality That Still Haunts Our Society                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/baahubali-creates-another-record-makes-it-to-the-question-paper-of-civil-engineering-exam-246095.html" class="tint" title="Baahubali Creates Another Record, Makes It To The Question Paper Of Civil Engineering Exam!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/baahubali-movie-stills-3_1444475112_1444475122_502x234.jpg" border="0" alt="Baahubali Creates Another Record, Makes It To The Question Paper Of Civil Engineering Exam!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/baahubali-creates-another-record-makes-it-to-the-question-paper-of-civil-engineering-exam-246095.html" title="Baahubali Creates Another Record, Makes It To The Question Paper Of Civil Engineering Exam!">
                        Baahubali Creates Another Record, Makes It To The Question Paper Of Civil Engineering Exam!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/this-mushroom-can-induce-spontaneous-orgasms-in-women-with-just-its-smell-246094.html" class="tint" title="This Mushroom Can Induce Spontaneous Orgasms In Women With Just Its Smell!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1444470599_502x234.jpg" border="0" alt="This Mushroom Can Induce Spontaneous Orgasms In Women With Just Its Smell!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/this-mushroom-can-induce-spontaneous-orgasms-in-women-with-just-its-smell-246094.html" title="This Mushroom Can Induce Spontaneous Orgasms In Women With Just Its Smell!">
                        This Mushroom Can Induce Spontaneous Orgasms In Women With Just Its Smell!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-pakistani-anchor-calls-india-an-elephant-with-the-mind-of-a-mouse-how-cute-246090.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-pakistani-anchor-calls-india-an-elephant-with-the-mind-of-a-mouse-how-cute-246090.html" class="tint" title="This Pakistani Anchor Calls India An Elephant With The Mind Of A Mouse. How Cute!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/pakcard_1444462430_502x234.jpg" border="0" alt="This Pakistani Anchor Calls India An Elephant With The Mind Of A Mouse. How Cute!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-pakistani-anchor-calls-india-an-elephant-with-the-mind-of-a-mouse-how-cute-246090.html" title="This Pakistani Anchor Calls India An Elephant With The Mind Of A Mouse. How Cute!">
                        This Pakistani Anchor Calls India An Elephant With The Mind Of A Mouse. How Cute!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/7-things-you-need-to-do-if-you-have-a-frozen-shoulder-246079.html" class="tint" title="7 Things You Need To Do If You Have A Frozen Shoulder">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cover_1444397916_502x234.jpg" border="0" alt="7 Things You Need To Do If You Have A Frozen Shoulder" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/7-things-you-need-to-do-if-you-have-a-frozen-shoulder-246079.html" title="7 Things You Need To Do If You Have A Frozen Shoulder">
                        7 Things You Need To Do If You Have A Frozen Shoulder                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-brilliant-gum-commercial-is-making-people-around-the-world-cry-their-eyes-out-keep-your-tissues-handy-246091.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-brilliant-gum-commercial-is-making-people-around-the-world-cry-their-eyes-out-keep-your-tissues-handy-246091.html" class="tint" title="This Brilliant Gum Commercial Is Making People Around The World Cry Their Eyes Out. Keep Your Tissues Handy">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/card_1444465622_502x234.jpg" border="0" alt="This Brilliant Gum Commercial Is Making People Around The World Cry Their Eyes Out. Keep Your Tissues Handy" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-brilliant-gum-commercial-is-making-people-around-the-world-cry-their-eyes-out-keep-your-tissues-handy-246091.html" title="This Brilliant Gum Commercial Is Making People Around The World Cry Their Eyes Out. Keep Your Tissues Handy">
                        This Brilliant Gum Commercial Is Making People Around The World Cry Their Eyes Out. Keep Your Tissues Handy                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/12-easy-homemade-masala-recipes-that-will-spice-up-your-dishes-246025.html" class="tint" title="12 Easy Homemade Masala Recipes That Will Spice Up Your Dishes!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cover_1444296576_502x234.jpg" border="0" alt="12 Easy Homemade Masala Recipes That Will Spice Up Your Dishes!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/12-easy-homemade-masala-recipes-that-will-spice-up-your-dishes-246025.html" title="12 Easy Homemade Masala Recipes That Will Spice Up Your Dishes!">
                        12 Easy Homemade Masala Recipes That Will Spice Up Your Dishes!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/tv-producer-slaps-rs-50-cr-suit-against-bajrangi-bhaijaan-for-copyright-infringement-246086.html" class="tint" title="TV Producer Slaps Rs 50 Cr Suit Against Bajrangi Bhaijaan For Copyright Infringement!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/salmankhan-kareena759_1444459027_1444459038_502x234.jpg" border="0" alt="TV Producer Slaps Rs 50 Cr Suit Against Bajrangi Bhaijaan For Copyright Infringement!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/tv-producer-slaps-rs-50-cr-suit-against-bajrangi-bhaijaan-for-copyright-infringement-246086.html" title="TV Producer Slaps Rs 50 Cr Suit Against Bajrangi Bhaijaan For Copyright Infringement!">
                        TV Producer Slaps Rs 50 Cr Suit Against Bajrangi Bhaijaan For Copyright Infringement!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-situations-in-life-where-you-have-no-choice-but-to-say-yes-246049.html" class="tint" title="9 Situations In Life Where You Have No Choice But To Say Yes">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cp_1444371074_502x234.jpg" border="0" alt="9 Situations In Life Where You Have No Choice But To Say Yes" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-situations-in-life-where-you-have-no-choice-but-to-say-yes-246049.html" title="9 Situations In Life Where You Have No Choice But To Say Yes">
                        9 Situations In Life Where You Have No Choice But To Say Yes                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/celebs/alia-bhatt-and-shahid-kapoor-troll-a-senseless-reporter-and-it-s-hilarious-246083.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/alia-bhatt-and-shahid-kapoor-troll-a-senseless-reporter-and-it-s-hilarious-246083.html" class="tint" title="Alia Bhatt And Shahid Kapoor Troll A Senseless Reporter, And It's Hilarious!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Oct/skali_1444457381_1444457391_502x234.jpg" border="0" alt="Alia Bhatt And Shahid Kapoor Troll A Senseless Reporter, And It's Hilarious!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/alia-bhatt-and-shahid-kapoor-troll-a-senseless-reporter-and-it-s-hilarious-246083.html" title="Alia Bhatt And Shahid Kapoor Troll A Senseless Reporter, And It's Hilarious!">
                        Alia Bhatt And Shahid Kapoor Troll A Senseless Reporter, And It's Hilarious!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/3-min-fat-burning-exercise-is-all-that-you-need-for-weight-loss-245529.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/3-min-fat-burning-exercise-is-all-that-you-need-for-weight-loss-245529.html" class="tint" title="3 Min Fat Burning Exercise Is All That You Need For Weight Loss">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/card-2_1443074040_502x234.jpg" border="0" alt="3 Min Fat Burning Exercise Is All That You Need For Weight Loss" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/3-min-fat-burning-exercise-is-all-that-you-need-for-weight-loss-245529.html" title="3 Min Fat Burning Exercise Is All That You Need For Weight Loss">
                        3 Min Fat Burning Exercise Is All That You Need For Weight Loss                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-malgudi-anecdotes-that-prove-swami-and-his-friends-were-just-like-us-246068.html" class="tint" title="9 Malgudi Anecdotes That Prove Swami And His Friends Were Just Like Us">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage2_1444389118_1444389128_502x234.jpg" border="0" alt="9 Malgudi Anecdotes That Prove Swami And His Friends Were Just Like Us" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-malgudi-anecdotes-that-prove-swami-and-his-friends-were-just-like-us-246068.html" title="9 Malgudi Anecdotes That Prove Swami And His Friends Were Just Like Us">
                        9 Malgudi Anecdotes That Prove Swami And His Friends Were Just Like Us                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-ways-you-can-kick-feelings-of-envy-and-jealousy-in-the-behind-245937.html" class="tint" title="7 Ways You Can Kick Those Feelings Of Envy And Jealousy In The Behind">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cp_1444279497_502x234.jpg" border="0" alt="7 Ways You Can Kick Those Feelings Of Envy And Jealousy In The Behind" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-ways-you-can-kick-feelings-of-envy-and-jealousy-in-the-behind-245937.html" title="7 Ways You Can Kick Those Feelings Of Envy And Jealousy In The Behind">
                        7 Ways You Can Kick Those Feelings Of Envy And Jealousy In The Behind                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/8-simple-tips-that-will-help-you-sleep-more-comfortably-245986.html" class="tint" title="8 Simple Tips That Will Help You Sleep More Comfortably">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cover_1444216243_502x234.jpg" border="0" alt="8 Simple Tips That Will Help You Sleep More Comfortably" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/8-simple-tips-that-will-help-you-sleep-more-comfortably-245986.html" title="8 Simple Tips That Will Help You Sleep More Comfortably">
                        8 Simple Tips That Will Help You Sleep More Comfortably                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/salman-and-aamir-begin-rigorous-workouts-for-sultan-and-dangal-6-reasons-why-both-the-films-are-similar-246073.html" class="tint" title="Salman And Aamir Begin Rigorous Workouts For Sultan And Dangal + 6 Reasons Why Both The Films Are Similar">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1444392823_502x234.jpg" border="0" alt="Salman And Aamir Begin Rigorous Workouts For Sultan And Dangal + 6 Reasons Why Both The Films Are Similar" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/salman-and-aamir-begin-rigorous-workouts-for-sultan-and-dangal-6-reasons-why-both-the-films-are-similar-246073.html" title="Salman And Aamir Begin Rigorous Workouts For Sultan And Dangal + 6 Reasons Why Both The Films Are Similar">
                        Salman And Aamir Begin Rigorous Workouts For Sultan And Dangal + 6 Reasons Why Both The Films Are Similar                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-unique-lessons-we-ve-learnt-from-our-favorite-tv-serials-245923.html" class="tint" title="11 Unique Lessons Weâve Learnt From Our Favorite TV Serials">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/yhm2_1444117684_1444117688_502x234.jpg" border="0" alt="11 Unique Lessons Weâve Learnt From Our Favorite TV Serials" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-unique-lessons-we-ve-learnt-from-our-favorite-tv-serials-245923.html" title="11 Unique Lessons Weâve Learnt From Our Favorite TV Serials">
                        11 Unique Lessons Weâve Learnt From Our Favorite TV Serials                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/this-is-why-women-need-to-worry-about-their-eyes-more-than-men-245979.html" class="tint" title="This Is Why Women Need To Worry About Their Eyes More Than Men">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card-1_1444214791_502x234.jpg" border="0" alt="This Is Why Women Need To Worry About Their Eyes More Than Men" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/this-is-why-women-need-to-worry-about-their-eyes-more-than-men-245979.html" title="This Is Why Women Need To Worry About Their Eyes More Than Men">
                        This Is Why Women Need To Worry About Their Eyes More Than Men                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/ranveer-singh-might-be-getting-thrashed-in-his-latest-dubsmash-but-he-still-manages-to-smash-it-out-246060.html" class="tint" title="Ranveer Singh Might Be Getting Thrashed In His Latest Dubsmash. But He Still Manages To Smash It Out">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/crd_1444380936_502x234.jpg" border="0" alt="Ranveer Singh Might Be Getting Thrashed In His Latest Dubsmash. But He Still Manages To Smash It Out" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/ranveer-singh-might-be-getting-thrashed-in-his-latest-dubsmash-but-he-still-manages-to-smash-it-out-246060.html" title="Ranveer Singh Might Be Getting Thrashed In His Latest Dubsmash. But He Still Manages To Smash It Out">
                        Ranveer Singh Might Be Getting Thrashed In His Latest Dubsmash. But He Still Manages To Smash It Out                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/self/13-friendship-lessons-straight-from-bollywood-that-will-make-you-a-better-friend-245947.html" class="tint" title="13 Friendship Lessons Straight From Bollywood That Will Make You A Better Friend">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/sholey2_1444135591_1444135595_218x102.jpg" border="0" alt="13 Friendship Lessons Straight From Bollywood That Will Make You A Better Friend"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/13-friendship-lessons-straight-from-bollywood-that-will-make-you-a-better-friend-245947.html" title="13 Friendship Lessons Straight From Bollywood That Will Make You A Better Friend">
                            13 Friendship Lessons Straight From Bollywood That Will Make You A Better Friend                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/15-reasons-why-amitabh-bachchan-is-still-the-man-to-beat-in-bollywood-227889.html" class="tint" title="15 Reasons Why Amitabh Bachchan Is Still The Man To Beat In Bollywood">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2014/Oct/94161273_1412774335_1412774345_218x102.jpg" border="0" alt="15 Reasons Why Amitabh Bachchan Is Still The Man To Beat In Bollywood"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/15-reasons-why-amitabh-bachchan-is-still-the-man-to-beat-in-bollywood-227889.html" title="15 Reasons Why Amitabh Bachchan Is Still The Man To Beat In Bollywood">
                            15 Reasons Why Amitabh Bachchan Is Still The Man To Beat In Bollywood                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/whoa-barack-obama-gives-a-piece-of-his-mind-to-kanye-west-on-his-presidential-plans-246146.html" class="tint" title="Whoa! Barack Obama Gives A Piece Of His Mind To Kanye West On His Presidential Plans!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1444640384_1444640391_218x102.jpg" border="0" alt="Whoa! Barack Obama Gives A Piece Of His Mind To Kanye West On His Presidential Plans!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/whoa-barack-obama-gives-a-piece-of-his-mind-to-kanye-west-on-his-presidential-plans-246146.html" title="Whoa! Barack Obama Gives A Piece Of His Mind To Kanye West On His Presidential Plans!">
                            Whoa! Barack Obama Gives A Piece Of His Mind To Kanye West On His Presidential Plans!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/despite-his-absence-at-deepika-s-foundation-launch-ranveer-supported-her-in-the-cutest-way-possible-246114.html" class="tint" title="Despite His Absence At Deepika's Foundation Launch, Ranveer Supported Her In The Cutest Way Possible!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/3_1444551623_1444551629_218x102.jpg" border="0" alt="Despite His Absence At Deepika's Foundation Launch, Ranveer Supported Her In The Cutest Way Possible!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/despite-his-absence-at-deepika-s-foundation-launch-ranveer-supported-her-in-the-cutest-way-possible-246114.html" title="Despite His Absence At Deepika's Foundation Launch, Ranveer Supported Her In The Cutest Way Possible!">
                            Despite His Absence At Deepika's Foundation Launch, Ranveer Supported Her In The Cutest Way Possible!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/self/13-struggles-of-people-who-just-can-t-stay-up-late-245993.html" class="tint" title="13 Struggles Of People Who Just Canât Stay Up Late">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/deepika123_1444286976_1444286987_218x102.jpg" border="0" alt="13 Struggles Of People Who Just Canât Stay Up Late"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/13-struggles-of-people-who-just-can-t-stay-up-late-245993.html" title="13 Struggles Of People Who Just Canât Stay Up Late">
                            13 Struggles Of People Who Just Canât Stay Up Late                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Oct/mia-khalifa-503_1444654619_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/mart502_1444643106_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/job-502_1444652413_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/mally-_1444645056_1444645059_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/kulkarni5_1444636273_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/kidnapping_1444637224_1444637235_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-4_1444394195_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/2_1444647141_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/beefban_card_1444543407_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card2_1444627398_1444627413_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-4_1444394195_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-2_1444384489_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/srkabramml_1444636782_1444636790_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Oct/reuters-five_1444636090_1444636093_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/make-up-502_1444637540_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage-five_1444631783_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/toi-502_1444632745_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/deep502_1444630012_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/kitten_card_1444634694_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1444640384_1444640391_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/crd_1444640859_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cp_1444110528_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/selling_1444563111_1444563123_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/software_eng_card_1444650953_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/crd_1444640859_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/ggfrf_1444623360_1444623375_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Oct/patel---card_1444626976_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/international-langar-week_1444571052_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/pjj_1444566534_1444566548_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/bank-502_1444564819_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage-555_1443693874_1443693881_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-3_1444211502_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/beefban_card_1444543407_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/khali_card_1444638126_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/card-4_1443088631_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/srkabramml_1444636782_1444636790_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card2_1444627398_1444627413_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cp_1444110528_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/kosovo_parl_card_1444629293_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-2_1444384489_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/love-502_1444560955_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/sholey2_1444135591_1444135595_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/deepika123_1444286976_1444286987_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/slap1_1444557268_1444557275_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/ggfrf_1444623360_1444623375_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/selling_1444563111_1444563123_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/ad_1444560257_1444560265_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card1_1444553990_1444554007_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/3_1444551623_1444551629_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/nasa_card_1444559433_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/dilchahtahai_card_1444548473_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-image_1444310044_1444310054_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-1_1444382543_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/insult_card_1442124756_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/dhanush-502_1444546774_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/bigg-boss-card_1444540860_1444540925_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/1_1444541777_1444541786_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/card_1444388629_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/kangana-video_1444538818_1444538826_502x234.jpg","http://media.indiatimes.in/media/content/2014/Oct/94161273_1412774335_1412774345_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/khnh12_1444048403_1444048408_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/pk01-oct23_1444461873_1444461881_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1444473806_1444473814_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1444478081_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/baahubali-movie-stills-3_1444475112_1444475122_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1444470599_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/pakcard_1444462430_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cover_1444397916_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/card_1444465622_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cover_1444296576_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/salmankhan-kareena759_1444459027_1444459038_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cp_1444371074_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Oct/skali_1444457381_1444457391_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/card-2_1443074040_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage2_1444389118_1444389128_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cp_1444279497_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cover_1444216243_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1444392823_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/yhm2_1444117684_1444117688_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/card-1_1444214791_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/crd_1444380936_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/sholey2_1444135591_1444135595_218x102.jpg","http://media.indiatimes.in/media/content/2014/Oct/94161273_1412774335_1412774345_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1444640384_1444640391_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/3_1444551623_1444551629_218x102.jpg","http://media.indiatimes.in/media/content/2015/Oct/deepika123_1444286976_1444286987_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,698,920<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>49,995 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.28" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.28"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.28"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.28"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.28"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>