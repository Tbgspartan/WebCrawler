<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>    Imgur: The most awesome images on the Internet</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="robots" content="follow,index" />
    <meta name="keywords" content="images, funny, image host, image sharing, reaction gif, viral images, current events, cute, visual storytelling, gif" />
    <meta name="description" content="Imgur is the best place to share and enjoy the most awesome images on the Internet. Every day, millions of people use Imgur to be entertained and inspired by funny, heartwarming and helpful images and stories from all around the world." />
    <meta name="copyright" content="Copyright 2015 Imgur, Inc." />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;" />
    
    <link rel="alternate" type="application/rss+xml" title="Imgur Gallery" href="http://feeds.feedburner.com/ImgurGallery?format=xml" />
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-16x16.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="//s.imgur.com/images/favicon-152.png">
    <meta name="msapplication-TileColor" content="#2cd63c">
    <meta name="msapplication-TileImage" content="//s.imgur.com/images/favicon-144.png">
            <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/global.css?1444239525" />


        
    
        <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/gallery.css?1444239525" />

            <!--[if IE 9]><link rel="stylesheet" href="//s.imgur.com/include/css/ie-sucks.css?0" type="text/css" /><![endif]-->
    
            <link rel="canonical" href="http://imgur.com/" />
        <meta property="og:url" content="http://imgur.com/" />
    
    <meta name="p:domain_verify" content="a1e7abe8af908cc6dfaf935dd9a20384"/>
    <meta property="og:site_name" content="Imgur" />
    <meta property="fb:admins" content="12331492,12301369" />
    <meta name="twitter:site" content="@imgur" />
    <meta name="twitter:domain" content="imgur.com" />
    <meta name="twitter:creator" content="@imgur" />
    <meta name="twitter:app:id:googleplay" content="com.imgur.mobile" />
        <meta name="twitter:card" content="summary_large_image" />
    <meta property="og:type" content="website" />
            <meta property="og:title" content="Imgur" />
        <meta name="twitter:title" content="Imgur" />
    
            <meta property="og:description" content="Imgur: The most awesome images on the Internet." />
        <meta name="twitter:description" content="Imgur: The most awesome images on the Internet." />
    
    <meta property="og:image" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image:width" content="1200" />
    <meta name="twitter:image:src" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta name="twitter:image:height" content="630" />
    <meta name="twitter:image:width" content="1200" />
    <!--[if lte IE 8]><script type="text/javascript" src="//s.imgur.com/min/iepoly.js?1444239525"></script>
<![endif]-->
    
        
</head>
<body>
                
    <div id="topbar">
    <div class="header-center">
        <div id="header-logo">

            <a href="//imgur.com" class="logo " data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@logo@@}}">
                
                <span class="logo-icon"></span>
            </a>
        </div>
        <div id="main-nav">
            <ul>
                <li class="menu-btn">
                    <div class="menu-icon">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                        
                    </div>
                    <div class="link-menu">
                        <div class="test-1868-show">
                            <ul class="topics cf">
                                <li class="item bold" data-value="hot"><a href="//imgur.com/hot" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@hot@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a></li>
                                <li class="item bold" data-value="new"><a href="//imgur.com/new" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@new@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a></li>
                                                                                                            <li class="item" data-value="Hand Picked">
                                            <a href="//imgur.com/topic/Hand_Picked" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Hand Picked@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Hand Picked</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Funny">
                                            <a href="//imgur.com/topic/Funny" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Awesome">
                                            <a href="//imgur.com/topic/Awesome" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Aww">
                                            <a href="//imgur.com/topic/Aww" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="The More You Know">
                                            <a href="//imgur.com/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Storytime">
                                            <a href="//imgur.com/topic/Storytime" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Current Events">
                                            <a href="//imgur.com/topic/Current_Events" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Design &amp; Art">
                                            <a href="//imgur.com/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Reaction">
                                            <a href="//imgur.com/topic/Reaction" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                                        </li>
                                    
                                                                    
                                
                            </ul>
                            <ul class="dropdown-footer cf">
                                <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">blog</a></li>
                                <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">about</a></li>
                                <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">store</a></li>
                                <li><a href="//imgur.com/jobs"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">jobs</a></li>
                                <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">help</a></li>
                                <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">apps</a></li>
                                <li><a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">terms</a></li>
                                <li><a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">privacy</a></li>
                                <li><a href="//imgur.com/removalrequest" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@deletion@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">request deletion</a></li>
                                <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">api</a></li>
                                <li><a href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">ad choices</a></li>
                            </ul>
                        </div>
                        <ul class="test-1868-hide">
                            <li><a href="//imgur.com/jobs" class="new" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@}}">we're hiring!</a></li>
                                                            <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@new@@:false}}">blog</a></li>
                            
                            <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@}}">about imgur</a></li>
                            <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@}}">imgur store</a></li>
                            <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@}}">official apps</a></li>
                            <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@}}">developer api</a></li>
                            <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@}}">need help?</a></li>
                        </ul>
                        <div class="clear test-1868-hide"></div>
                        <div class="dropdown-footer hamburger-menu test-1868-hide">
                            <div class="terms-footer">
                                <a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@}}">terms</a>
                                <a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@}}">privacy</a>
                                <a class="small-margin-top" href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@}}">ad choices</a>
                            </div>
                            <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fimgur&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="display:inline-block; border:none; overflow:hidden; height:20px; width:50px; margin-right:7px;" allowTransparency="true"></iframe>
                            <a href="https://twitter.com/imgur" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false"></a>
                        </div>
                    </div>
                </li>
                <li class="upload-button-container">
                    <a class="upload-button disabled upload-blue" href="javascript:;">
                        <div class="icon icon-upload"></div>
                        <span class="upload-btn-text">upload images</span>
                        <span class="upload-queue-length">23</span>
                        <div id="upload-global-top-bar-mini-progress"></div>
                    </a>
                    <div class="creation-dropdown upload-blue">
                        <div class="selection icon icon-arrow-down"></div>
                        <div class="options">
                            <ul>
                                <li class="item">
                                    <a class="creation-dropdown-upload" href="javascript:;">Upload Images</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/vidgif">Video to GIF</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/memegen" data-jafo="{@@event@@:@@makeAMeme@@,@@meta@@:{@@control@@:@@header@@}}">Make a Meme</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="random-button title-n " title="random mode">
                    <a href="//imgur.com/random" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@random@@}}">
                        <div class="random-icon"></div>
                    </a>
                </li>
            </ul>
        </div>
        <div id="secondary-nav">
            <ul class="user-nav">
                <li id="global-search-container">
                    <form class="search-form" method="get" action="//imgur.com/search">
                        <input id="search-global-input" type="text" name="q" class="search" blur placeholder="Search images"  />
                    </form>

                    <div class="search-icon-container">
                        <div class="search-icon-container-icon"></div>
                    </div>
                </li>
                                    <li class="signin-link"><a class="" href="https://imgur.com/signin" data-jafo="{@@event@@:@@signinButtonClick@@}">sign in</a></li>
                    <li id="register"><a class="" href="https://imgur.com/register" data-jafo="{@@event@@:@@registerButtonClick@@}">sign up</a></li>
                
            </ul>
        </div> <!-- #secondary-nav -->
    </div> <!-- .header-center -->
</div> <!-- #topbar -->
<div id="cta-container-placeholder"></div>
<div id="cta-lightbox-placeholder"></div>

    

    

            

        
    

            <a href="javascript:;" id="upload-global-album-tipsy" class="title-nw nodisplay" title="You can't create an album with just one image"></a>

<div id="upload-global" class="popup">
    <form method="post" id="upload-global-form" enctype="multipart/form-data" action="http://imgur.com/upload">
        

        <div id="upload-global-buttons">
            <div id="upload-global-upload-loader" class="textbox">
                Loading...
            </div>

            <div id="upload-global-file-wrapper">
                <span class="computerIcon"></span>
                <span class="green">browse</span> your computer

                <input accept="image/*" type="file" name="Filedata" multiple="multiple" id="upload-global-file">
            </div>

            <div class="left">
                <div id="upload-global-url">
                    <span class="webIcon"></span>
                    <input type="text" id="upload-global-link-input" class="placeholder-onkeydown" title="enter image URLs" placeholder="enter image URLs" />
                </div>
            </div>

            <div class="clear"></div>

            <div id="upload-global-dragdrop">
                <span class="dragdropIcon"></span>
                <span class="green">drag and drop</span> here
            </div>

            <div class="left">
                <div id="upload-global-clipboard">
                    <div style="text-align: center"><span class="clipboardIcons">Ctrl + V</span></div>
                    <span class="green">paste</span> from your clipboard
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="textbox center" id="upload-global-instructions"></div>

        <!-- gallery submit vars -->
        <input type="hidden" name="current_upload" value="0"/>
        <input type="hidden" name="total_uploads" value="0"/>
        <input type="hidden" name="terms" value="0"/>
        <input type="hidden" name="gallery_type" value=""/>
        <input type="hidden" name="location" value=""/>


        <div id="upload-global-errors" class="textbox error" style="display: none;"></div>

        <div id="upload-global-queue-description" class="textbox nodisplay"></div>

        <div id="upload-global-start-upload">
            <div class="panel">
                <div>

                    <table id="upload-global-actions">
                        <tbody>
                            <tr>

                                <td width="50.4%">

                                    <div id="upload-global-album-checkbox-div">
                                        <input type="hidden" name="gallery_submit" value="0"/>

                                        <button id="upload-global-gallery-checkbox" type="button">
                                            <span class="gallery-icon"></span>publish to Imgur
                                        </button>

                                        <div id="upload-global-gallery-checkbox-overlay"></div>

                                        <input type="hidden" name="create_album" value="0"/>

                                        <button id="upload-global-album-checkbox" type="button">
                                            <span class="createAlbum-icon"></span>create album
                                        </button>

                                    </div>

                                </td>
                                <td width="49.6%">
                                    <button type="button" id="upload-global-start-button" class="button-big green" type="submit">
                                        <span class="start-btn-icon"></span>
                                        Start Upload
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div id="upload-global-album-options">
                    <table id="upload-global-album-layout">
    <tr>
        <td id="upload-global-album-td">
            <input name="album_title" type="text" title="Optional Album Title" class="placeholder-onkeydown" id="upload-global-album-title" maxlength="55" />
        </td>
    </tr>
</table>

                </div>

                <div class="textbox list">
                    <table id="upload-global-file-list-header">
                        <tr class="top-tr">
                            <td width="45" class="edit-td"><h2>Edit</h2></td>
                            <td><h2>Images <span class="upload-queue-length">(0)</span></h2></td>
                            <td width="40"></td>
                        </tr>
                    </table>

                    <table id="upload-global-file-list">
                    </table>
                                            <div class="agreement-notice">By uploading, you agree to our <a href="//imgur.com/tos" target="_blank">terms of service</a></div>
                    
                </div>
            </div>
        </div>

        <div id="upload-global-progress-wrapper">
            <div id="upload-global-progressbar">
                <div class="progressBarBG">
                    <div class="progressBar"></div>
                </div>
            </div>
            <div id="upload-global-upload-queue"></div>

            <div id="upload-global-converting">
                <p>
                    Optimizing your large GIFs...
                </p>
            </div>
        </div>
    </form>
</div>

<div id="upload-global-FF-paste-box"></div>

<div id="hiddenDropZone" class="nodisplay"></div>

<div class="nodisplay">
    <div id="upload-global-image-formats">
         That file type is not supported!

        <div class="textbox supported-formats">
            Supported formats: JPEG, GIF, PNG, APNG, TIFF, BMP, PDF, XCF
        </div>
    </div>
</div>

<div id="upload-global-flash-container" title="upload images from your computer">
    <div id="upload-global-flash-browse"></div>
</div>

    

        <div class="nodisplay">
	<div id="colorbox-confirm" class="popup">
		<h3 id="colorbox-confirm-title"></h3>
		
		<div class="textbox" id="colorbox-confirm-message"></div>

		<div class="margin-top">
			<div class="left">
				<a href="javascript:;" class="colorbox-no-confirm" id="colorbox-confirm-no">No way!</a>
			</div>
			
			<div class="right">
				<div class="small-loader"></div>
				<a href="javascript:;" class="colorbox-confirm" id="colorbox-confirm-yes">I'm sure</a>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</div>


    
            
    

    
    
    
    

    <div id="fullbleed-bg"></div>
    <div id="content" class="outside main">
        
                    <div class="sentence-sorting">
            <span class="before-text">
    The

</span>
<div id="section" class="combobox front-page-section sorting-text-align" name="section">
    <div class="selection">
                <div data-type="base" data-value="hot">
            <span class="name">Most Viral</span>
        </div>
        
        
    </div>

    <div class="options nano">
        <div class="js-galleries content">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                                            Most Viral
                    
                </div>
            </div>
            <ul>
                <li class="bold item nodisplay" data-type="base" data-value="hot">
                    <a href="/hot/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a>
                </li>
                <li class="bold item" data-type="base" data-value="new">
                                            <a href="/new/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@new@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a>
                    
                </li>
                <li class="combobox-hairline"></li>
                                                            <li class="item" data-value="Hand Picked">
                            <a href="/topic/Hand_Picked" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Hand Picked@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Hand Picked</a>
                        </li>
                    
                                                            <li class="item" data-value="Funny">
                            <a href="/topic/Funny" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                        </li>
                    
                                                            <li class="item" data-value="Awesome">
                            <a href="/topic/Awesome" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                        </li>
                    
                                                            <li class="item" data-value="Aww">
                            <a href="/topic/Aww" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                        </li>
                    
                                                            <li class="item" data-value="The More You Know">
                            <a href="/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                        </li>
                    
                                                            <li class="item" data-value="Storytime">
                            <a href="/topic/Storytime" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                        </li>
                    
                                                            <li class="item" data-value="Current Events">
                            <a href="/topic/Current_Events" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                        </li>
                    
                                                            <li class="item" data-value="Design &amp; Art">
                            <a href="/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                        </li>
                    
                                                            <li class="item" data-value="Reaction">
                            <a href="/topic/Reaction" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                        </li>
                    
                                    
                
            </ul>
        </div>
        <div id="custom-group" class="item new-group"><a href="/gallery/custom" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@custom@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Custom Gallery</a></div>
    </div>
    <input type="hidden" name="section" value="hot">
</div><span class="middle-text"> <!-- intentionally not closed -->

    images on the Internet, 


        sorted by
    
    </span> <!-- .middle-text -->
    
    
        
            
    

            
    
    <div id="sort" class="combobox">
        <div class="selection">
            popularity
        </div>
        <div class="options sorting-text-align">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                    popularity
                </div>
            </div>
            <ul>
                                    <li class="item nodisplay" value="viral">
                                                    <a href="/hot/viral">popularity</a>
                        
                    </li>
                    <li class="item" value="time">
                                                    <a href="/hot/time">newest first</a>
                        
                    </li>
                                        <li class="item" value="top">
                                                    <a href="/top">highest scoring</a>
                        
                    </li>
                    
                
            </ul>
        </div>
        <input type="hidden" name="sort" value="viral">
    </div>

    

    <div class="sort-options">
                                    <ul>
                                            <li><a href="/gallery/random" id="random-button" class="title-n" title="random mode"><span></span></a></li>
                    
                                            <li><a href="javascript:void(0)" id="past-link" class="title-n" title="past images"><span></span></a></li>
                    
                </ul>
            
        
    </div>
    <div id="user-gallery-message">
        
    </div>
</div>

               
        

                    <div class="panel hidden">
                <div id="imagelist" class="home-gallery">
                                            <script type="text/tag-group-data">[]</script>


<div class="posts br5 first-child">
    

            
        <div class="outside-loader"></div>
        
                    
        

        <div class="cards">
                            <div id="aieQg" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/aieQg" data-page="0">
        <img alt="" src="//i.imgur.com/UQ7vtL4b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="aieQg" type="image" data-up="10448">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="aieQg" type="image" data-downs="180">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-aieQg">10,268</span>
                            <span class="points-text-aieQg">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>What a difference a dad makes..</p>
        
        
        <div class="post-info">
            album &middot; 189,375 views
        </div>
    </div>
    
</div>

                            <div id="7NziY" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/7NziY" data-page="0">
        <img alt="" src="//i.imgur.com/MyScXxub.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="7NziY" type="image" data-up="13785">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="7NziY" type="image" data-downs="165">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-7NziY">13,620</span>
                            <span class="points-text-7NziY">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>List of short movies you may like</p>
        
        
        <div class="post-info">
            album &middot; 272,421 views
        </div>
    </div>
    
</div>

                            <div id="EG5FsmS" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/EG5FsmS" data-page="0">
        <img alt="" src="//i.imgur.com/EG5FsmSb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="EG5FsmS" type="image" data-up="5876">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="EG5FsmS" type="image" data-downs="103">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-EG5FsmS">5,773</span>
                            <span class="points-text-EG5FsmS">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Repayment</p>
        
        
        <div class="post-info">
            animated &middot; 453,137 views
        </div>
    </div>
    
</div>

                            <div id="KFUQCAO" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/KFUQCAO" data-page="0">
        <img alt="" src="//i.imgur.com/KFUQCAOb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="KFUQCAO" type="image" data-up="3980">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="KFUQCAO" type="image" data-downs="41">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-KFUQCAO">3,939</span>
                            <span class="points-text-KFUQCAO">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The &#039; I am stronger than you&#039; prank</p>
        
        
        <div class="post-info">
            animated &middot; 244,179 views
        </div>
    </div>
    
</div>

                            <div id="LiLZw4r" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/LiLZw4r" data-page="0">
        <img alt="" src="//i.imgur.com/LiLZw4rb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="LiLZw4r" type="image" data-up="2354">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="LiLZw4r" type="image" data-downs="24">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-LiLZw4r">2,330</span>
                            <span class="points-text-LiLZw4r">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>This statue is a totally different one on the other side</p>
        
        
        <div class="post-info">
            image &middot; 464,578 views
        </div>
    </div>
    
</div>

                            <div id="5sG0Mci" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/5sG0Mci" data-page="0">
        <img alt="" src="//i.imgur.com/5sG0Mcib.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="5sG0Mci" type="image" data-up="3681">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="5sG0Mci" type="image" data-downs="139">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-5sG0Mci">3,542</span>
                            <span class="points-text-5sG0Mci">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>A free cupcake for every upvote</p>
        
        
        <div class="post-info">
            animated &middot; 180,331 views
        </div>
    </div>
    
</div>

                            <div id="CM8hQ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/CM8hQ" data-page="0">
        <img alt="" src="//i.imgur.com/UthG27Rb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="CM8hQ" type="image" data-up="3957">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="CM8hQ" type="image" data-downs="104">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-CM8hQ">3,853</span>
                            <span class="points-text-CM8hQ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Backround Dump</p>
        
        
        <div class="post-info">
            album &middot; 64,086 views
        </div>
    </div>
    
</div>

                            <div id="NVFbt" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/NVFbt" data-page="0">
        <img alt="" src="//i.imgur.com/BexozAPb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="NVFbt" type="image" data-up="8009">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="NVFbt" type="image" data-downs="89">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-NVFbt">7,920</span>
                            <span class="points-text-NVFbt">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Real Stories Of Feral Children</p>
        
        
        <div class="post-info">
            album &middot; 184,769 views
        </div>
    </div>
    
</div>

                            <div id="FLrrxGT" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FLrrxGT" data-page="0">
        <img alt="" src="//i.imgur.com/FLrrxGTb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FLrrxGT" type="image" data-up="9305">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FLrrxGT" type="image" data-downs="80">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FLrrxGT">9,225</span>
                            <span class="points-text-FLrrxGT">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Photographers pose with their most famous photo</p>
        
        
        <div class="post-info">
            image &middot; 420,792 views
        </div>
    </div>
    
</div>

                            <div id="H63pkkq" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/H63pkkq" data-page="0">
        <img alt="" src="//i.imgur.com/H63pkkqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="H63pkkq" type="image" data-up="5991">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="H63pkkq" type="image" data-downs="68">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-H63pkkq">5,923</span>
                            <span class="points-text-H63pkkq">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>How my CEO thinks we engineers work</p>
        
        
        <div class="post-info">
            animated &middot; 463,581 views
        </div>
    </div>
    
</div>

                            <div id="V81sLu1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/V81sLu1" data-page="0">
        <img alt="" src="//i.imgur.com/V81sLu1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="V81sLu1" type="image" data-up="2744">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="V81sLu1" type="image" data-downs="46">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-V81sLu1">2,698</span>
                            <span class="points-text-V81sLu1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My initials are E.D, I&#039;ve been signing with a dick my whole life</p>
        
        
        <div class="post-info">
            image &middot; 1,638,379 views
        </div>
    </div>
    
</div>

                            <div id="LARKv" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/LARKv" data-page="0">
        <img alt="" src="//i.imgur.com/znVl7XWb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="LARKv" type="image" data-up="5163">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="LARKv" type="image" data-downs="155">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-LARKv">5,008</span>
                            <span class="points-text-LARKv">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>dragon bros</p>
        
        
        <div class="post-info">
            album &middot; 126,622 views
        </div>
    </div>
    
</div>

                            <div id="h0BDoTU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/h0BDoTU" data-page="0">
        <img alt="" src="//i.imgur.com/h0BDoTUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="h0BDoTU" type="image" data-up="2876">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="h0BDoTU" type="image" data-downs="72">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-h0BDoTU">2,804</span>
                            <span class="points-text-h0BDoTU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>EA... is that you?</p>
        
        
        <div class="post-info">
            image &middot; 641,296 views
        </div>
    </div>
    
</div>

                            <div id="uLOao3O" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/uLOao3O" data-page="0">
        <img alt="" src="//i.imgur.com/uLOao3Ob.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="uLOao3O" type="image" data-up="5981">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="uLOao3O" type="image" data-downs="75">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-uLOao3O">5,906</span>
                            <span class="points-text-uLOao3O">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Concrete stained and scored to look like hardwood flooring</p>
        
        
        <div class="post-info">
            animated &middot; 899,702 views
        </div>
    </div>
    
</div>

                            <div id="AGkpM45" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/AGkpM45" data-page="0">
        <img alt="" src="//i.imgur.com/AGkpM45b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="AGkpM45" type="image" data-up="5034">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="AGkpM45" type="image" data-downs="200">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-AGkpM45">4,834</span>
                            <span class="points-text-AGkpM45">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>if you haven&#039;t seen it, prepare to love it</p>
        
        
        <div class="post-info">
            animated &middot; 465,014 views
        </div>
    </div>
    
</div>

                            <div id="bjmQF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/bjmQF" data-page="0">
        <img alt="" src="//i.imgur.com/UOKfzbvb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="bjmQF" type="image" data-up="7029">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="bjmQF" type="image" data-downs="105">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-bjmQF">6,924</span>
                            <span class="points-text-bjmQF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Yes it&#039;s buzzfeed but this is cool</p>
        
        
        <div class="post-info">
            album &middot; 184,421 views
        </div>
    </div>
    
</div>

                            <div id="4MGPQGL" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/4MGPQGL" data-page="0">
        <img alt="" src="//i.imgur.com/4MGPQGLb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="4MGPQGL" type="image" data-up="9950">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="4MGPQGL" type="image" data-downs="135">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-4MGPQGL">9,815</span>
                            <span class="points-text-4MGPQGL">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>But I want to come cuddle with you...please...</p>
        
        
        <div class="post-info">
            animated &middot; 875,607 views
        </div>
    </div>
    
</div>

                            <div id="Zmuxu" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Zmuxu" data-page="0">
        <img alt="" src="//i.imgur.com/1XuVaeob.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Zmuxu" type="image" data-up="2778">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Zmuxu" type="image" data-downs="86">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Zmuxu">2,692</span>
                            <span class="points-text-Zmuxu">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>D&amp;D 5E Cheatsheet</p>
        
        
        <div class="post-info">
            album &middot; 53,602 views
        </div>
    </div>
    
</div>

                            <div id="SW0cvfe" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/SW0cvfe" data-page="0">
        <img alt="" src="//i.imgur.com/SW0cvfeb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="SW0cvfe" type="image" data-up="4958">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="SW0cvfe" type="image" data-downs="253">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-SW0cvfe">4,705</span>
                            <span class="points-text-SW0cvfe">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I came home to this last night. I married the right woman.</p>
        
        
        <div class="post-info">
            image &middot; 1,625,377 views
        </div>
    </div>
    
</div>

                            <div id="dTSHCNY" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/dTSHCNY" data-page="0">
        <img alt="" src="//i.imgur.com/dTSHCNYb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="dTSHCNY" type="image" data-up="2599">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="dTSHCNY" type="image" data-downs="33">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-dTSHCNY">2,566</span>
                            <span class="points-text-dTSHCNY">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>How tower cranes erect themselves</p>
        
        
        <div class="post-info">
            animated &middot; 184,553 views
        </div>
    </div>
    
</div>

                            <div id="86Zyf" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/86Zyf" data-page="0">
        <img alt="" src="//i.imgur.com/8LVKPSYb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="86Zyf" type="image" data-up="3576">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="86Zyf" type="image" data-downs="69">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-86Zyf">3,507</span>
                            <span class="points-text-86Zyf">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My friends went to IKEA because they wanted &quot;good Tinder&quot; photos for their profiles...</p>
        
        
        <div class="post-info">
            album &middot; 96,361 views
        </div>
    </div>
    
</div>

                            <div id="f1cEgyM" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/f1cEgyM" data-page="0">
        <img alt="" src="//i.imgur.com/f1cEgyMb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="f1cEgyM" type="image" data-up="5873">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="f1cEgyM" type="image" data-downs="129">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-f1cEgyM">5,744</span>
                            <span class="points-text-f1cEgyM">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>public school</p>
        
        
        <div class="post-info">
            image &middot; 315,785 views
        </div>
    </div>
    
</div>

                            <div id="xoyiTHE" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/xoyiTHE" data-page="0">
        <img alt="" src="//i.imgur.com/xoyiTHEb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="xoyiTHE" type="image" data-up="8842">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="xoyiTHE" type="image" data-downs="106">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-xoyiTHE">8,736</span>
                            <span class="points-text-xoyiTHE">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Does going through backwards make stuff bigger?</p>
        
        
        <div class="post-info">
            animated &middot; 762,078 views
        </div>
    </div>
    
</div>

                            <div id="jYOG4jK" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/jYOG4jK" data-page="0">
        <img alt="" src="//i.imgur.com/jYOG4jKb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="jYOG4jK" type="image" data-up="11316">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="jYOG4jK" type="image" data-downs="581">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-jYOG4jK">10,735</span>
                            <span class="points-text-jYOG4jK">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>This isn&#039;t fun at all .. it&#039;s depressing.</p>
        
        
        <div class="post-info">
            image &middot; 606,025 views
        </div>
    </div>
    
</div>

                            <div id="odMDVC2" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/odMDVC2" data-page="0">
        <img alt="" src="//i.imgur.com/odMDVC2b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="odMDVC2" type="image" data-up="3626">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="odMDVC2" type="image" data-downs="32">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-odMDVC2">3,594</span>
                            <span class="points-text-odMDVC2">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Giant Sunfish Near Portugal</p>
        
        
        <div class="post-info">
            animated &middot; 298,020 views
        </div>
    </div>
    
</div>

                            <div id="c3Uhx" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/c3Uhx" data-page="0">
        <img alt="" src="//i.imgur.com/v9snl2Hb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="c3Uhx" type="image" data-up="7560">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="c3Uhx" type="image" data-downs="134">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-c3Uhx">7,426</span>
                            <span class="points-text-c3Uhx">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Have a nice day.</p>
        
        
        <div class="post-info">
            album &middot; 212,670 views
        </div>
    </div>
    
</div>

                            <div id="YHygG" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/YHygG" data-page="0">
        <img alt="" src="//i.imgur.com/yclUOy8b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="YHygG" type="image" data-up="11200">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="YHygG" type="image" data-downs="196">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-YHygG">11,004</span>
                            <span class="points-text-YHygG">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Remember this poor dog?</p>
        
        
        <div class="post-info">
            album &middot; 341,568 views
        </div>
    </div>
    
</div>

                            <div id="o5XZX" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/o5XZX" data-page="0">
        <img alt="" src="//i.imgur.com/eclMnFKb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="o5XZX" type="image" data-up="4070">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="o5XZX" type="image" data-downs="178">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-o5XZX">3,892</span>
                            <span class="points-text-o5XZX">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Come to this post in next 80 min if u want an ass kicking</p>
        
        
        <div class="post-info">
            album &middot; 114,121 views
        </div>
    </div>
    
</div>

                            <div id="qgLGfB3" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/qgLGfB3" data-page="0">
        <img alt="" src="//i.imgur.com/qgLGfB3b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="qgLGfB3" type="image" data-up="2286">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="qgLGfB3" type="image" data-downs="104">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-qgLGfB3">2,182</span>
                            <span class="points-text-qgLGfB3">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>On safari the wife told the husband, &quot;If the antelope survives this, I&#039;ll give you a blow job every day for the rest of your life..&quot;</p>
        
        
        <div class="post-info">
            animated &middot; 135,911 views
        </div>
    </div>
    
</div>

                            <div id="CfFzSdQ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/CfFzSdQ" data-page="0">
        <img alt="" src="//i.imgur.com/CfFzSdQb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="CfFzSdQ" type="image" data-up="3426">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="CfFzSdQ" type="image" data-downs="180">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-CfFzSdQ">3,246</span>
                            <span class="points-text-CfFzSdQ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The daddy</p>
        
        
        <div class="post-info">
            animated &middot; 275,180 views
        </div>
    </div>
    
</div>

                            <div id="vJ4eU6y" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/vJ4eU6y" data-page="0">
        <img alt="" src="//i.imgur.com/vJ4eU6yb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="vJ4eU6y" type="image" data-up="2123">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="vJ4eU6y" type="image" data-downs="35">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-vJ4eU6y">2,088</span>
                            <span class="points-text-vJ4eU6y">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The Magic of Dogs</p>
        
        
        <div class="post-info">
            animated &middot; 121,654 views
        </div>
    </div>
    
</div>

                            <div id="pkjWFZk" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/pkjWFZk" data-page="0">
        <img alt="" src="//i.imgur.com/pkjWFZkb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="pkjWFZk" type="image" data-up="7710">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="pkjWFZk" type="image" data-downs="119">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-pkjWFZk">7,591</span>
                            <span class="points-text-pkjWFZk">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>8 weeks to 8 months</p>
        
        
        <div class="post-info">
            image &middot; 2,681,140 views
        </div>
    </div>
    
</div>

                            <div id="S3Et2BF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/S3Et2BF" data-page="0">
        <img alt="" src="//i.imgur.com/S3Et2BFb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="S3Et2BF" type="image" data-up="2408">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="S3Et2BF" type="image" data-downs="91">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-S3Et2BF">2,317</span>
                            <span class="points-text-S3Et2BF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>spoopy</p>
        
        
        <div class="post-info">
            animated &middot; 156,563 views
        </div>
    </div>
    
</div>

                            <div id="WITKQlD" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/WITKQlD" data-page="0">
        <img alt="" src="//i.imgur.com/WITKQlDb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="WITKQlD" type="image" data-up="3879">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="WITKQlD" type="image" data-downs="152">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-WITKQlD">3,727</span>
                            <span class="points-text-WITKQlD">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>.</p>
        
        
        <div class="post-info">
            image &middot; 214,848 views
        </div>
    </div>
    
</div>

                            <div id="Dftbvmw" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Dftbvmw" data-page="0">
        <img alt="" src="//i.imgur.com/Dftbvmwb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Dftbvmw" type="image" data-up="4963">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Dftbvmw" type="image" data-downs="70">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Dftbvmw">4,893</span>
                            <span class="points-text-Dftbvmw">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Peek-a-Boo</p>
        
        
        <div class="post-info">
            animated &middot; 438,080 views
        </div>
    </div>
    
</div>

                            <div id="1ijaLWW" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/1ijaLWW" data-page="0">
        <img alt="" src="//i.imgur.com/1ijaLWWb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="1ijaLWW" type="image" data-up="4339">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="1ijaLWW" type="image" data-downs="273">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-1ijaLWW">4,066</span>
                            <span class="points-text-1ijaLWW">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>The awkward kiss cam</p>
        
        
        <div class="post-info">
            animated &middot; 654,126 views
        </div>
    </div>
    
</div>

                            <div id="j8g1IsD" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/j8g1IsD" data-page="0">
        <img alt="" src="//i.imgur.com/j8g1IsDb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="j8g1IsD" type="image" data-up="3438">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="j8g1IsD" type="image" data-downs="191">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-j8g1IsD">3,247</span>
                            <span class="points-text-j8g1IsD">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>How To Cure Depression</p>
        
        
        <div class="post-info">
            image &middot; 772,210 views
        </div>
    </div>
    
</div>

                            <div id="PrdkLN7" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/PrdkLN7" data-page="0">
        <img alt="" src="//i.imgur.com/PrdkLN7b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="PrdkLN7" type="image" data-up="5490">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="PrdkLN7" type="image" data-downs="121">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-PrdkLN7">5,369</span>
                            <span class="points-text-PrdkLN7">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Every time I see someone post one of those &#039;declarations&#039; of privacy on Facebook, all I think of is this.</p>
        
        
        <div class="post-info">
            image &middot; 2,342,639 views
        </div>
    </div>
    
</div>

                            <div id="9wZmx" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/9wZmx" data-page="0">
        <img alt="" src="//i.imgur.com/uwDWt9Zb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="9wZmx" type="image" data-up="2151">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="9wZmx" type="image" data-downs="89">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-9wZmx">2,062</span>
                            <span class="points-text-9wZmx">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Teacher get&#039;s naked in classroom!</p>
        
        
        <div class="post-info">
            album &middot; 45,167 views
        </div>
    </div>
    
</div>

                            <div id="qHn6G5F" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/qHn6G5F" data-page="0">
        <img alt="" src="//i.imgur.com/qHn6G5Fb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="qHn6G5F" type="image" data-up="10875">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="qHn6G5F" type="image" data-downs="316">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-qHn6G5F">10,559</span>
                            <span class="points-text-qHn6G5F">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I just wrote the perfect &#039;a&#039;.</p>
        
        
        <div class="post-info">
            image &middot; 1,671,993 views
        </div>
    </div>
    
</div>

                            <div id="ZQGebOg" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ZQGebOg" data-page="0">
        <img alt="" src="//i.imgur.com/ZQGebOgb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ZQGebOg" type="image" data-up="4511">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ZQGebOg" type="image" data-downs="64">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ZQGebOg">4,447</span>
                            <span class="points-text-ZQGebOg">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Blowing a straw from it&#039;s wrapper. Perfect shot</p>
        
        
        <div class="post-info">
            animated &middot; 700,552 views
        </div>
    </div>
    
</div>

                            <div id="SvsOkaH" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/SvsOkaH" data-page="0">
        <img alt="" src="//i.imgur.com/SvsOkaHb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="SvsOkaH" type="image" data-up="4871">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="SvsOkaH" type="image" data-downs="100">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-SvsOkaH">4,771</span>
                            <span class="points-text-SvsOkaH">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Docking Sequence Initiated</p>
        
        
        <div class="post-info">
            animated &middot; 405,893 views
        </div>
    </div>
    
</div>

                            <div id="MrtWfuD" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/MrtWfuD" data-page="0">
        <img alt="" src="//i.imgur.com/MrtWfuDb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="MrtWfuD" type="image" data-up="6080">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="MrtWfuD" type="image" data-downs="566">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-MrtWfuD">5,514</span>
                            <span class="points-text-MrtWfuD">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Ahem</p>
        
        
        <div class="post-info">
            image &middot; 341,871 views
        </div>
    </div>
    
</div>

                            <div id="da2Pcn2" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/da2Pcn2" data-page="0">
        <img alt="" src="//i.imgur.com/da2Pcn2b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="da2Pcn2" type="image" data-up="9561">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="da2Pcn2" type="image" data-downs="142">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-da2Pcn2">9,419</span>
                            <span class="points-text-da2Pcn2">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Skull Money</p>
        
        
        <div class="post-info">
            image &middot; 1,502,963 views
        </div>
    </div>
    
</div>

                            <div id="4vMyWwD" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/4vMyWwD" data-page="0">
        <img alt="" src="//i.imgur.com/4vMyWwDb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="4vMyWwD" type="image" data-up="5296">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="4vMyWwD" type="image" data-downs="128">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-4vMyWwD">5,168</span>
                            <span class="points-text-4vMyWwD">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>When you want to be a lap dog but you&#039;re a horse...</p>
        
        
        <div class="post-info">
            animated &middot; 488,424 views
        </div>
    </div>
    
</div>

                            <div id="ty2os" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ty2os" data-page="0">
        <img alt="" src="//i.imgur.com/btGKkJ9b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ty2os" type="image" data-up="2242">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ty2os" type="image" data-downs="128">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ty2os">2,114</span>
                            <span class="points-text-ty2os">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Black Twitter with a sprinkle of tumblr Pt. 2</p>
        
        
        <div class="post-info">
            album &middot; 53,430 views
        </div>
    </div>
    
</div>

                            <div id="GTyU9vS" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/GTyU9vS" data-page="0">
        <img alt="" src="//i.imgur.com/GTyU9vSb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="GTyU9vS" type="image" data-up="6928">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="GTyU9vS" type="image" data-downs="225">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-GTyU9vS">6,703</span>
                            <span class="points-text-GTyU9vS">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Trash panda wants to see the world burn.</p>
        
        
        <div class="post-info">
            image &middot; 400,956 views
        </div>
    </div>
    
</div>

                            <div id="dPSFJ5x" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/dPSFJ5x" data-page="0">
        <img alt="" src="//i.imgur.com/dPSFJ5xb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="dPSFJ5x" type="image" data-up="3638">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="dPSFJ5x" type="image" data-downs="330">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-dPSFJ5x">3,308</span>
                            <span class="points-text-dPSFJ5x">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW people ask me if I&#039;ve heard of Imgur.</p>
        
        
        <div class="post-info">
            animated &middot; 288,704 views
        </div>
    </div>
    
</div>

                            <div id="LYjk8" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/LYjk8" data-page="0">
        <img alt="" src="//i.imgur.com/OBZyF1lb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="LYjk8" type="image" data-up="2909">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="LYjk8" type="image" data-downs="97">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-LYjk8">2,812</span>
                            <span class="points-text-LYjk8">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Meet Disney&#039;s Newest Princess, MOANA</p>
        
        
        <div class="post-info">
            album &middot; 78,251 views
        </div>
    </div>
    
</div>

                            <div id="9tErDr1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/9tErDr1" data-page="0">
        <img alt="" src="//i.imgur.com/9tErDr1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="9tErDr1" type="image" data-up="3491">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="9tErDr1" type="image" data-downs="96">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-9tErDr1">3,395</span>
                            <span class="points-text-9tErDr1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Lotus Exige still looking great in matte black</p>
        
        
        <div class="post-info">
            image &middot; 189,690 views
        </div>
    </div>
    
</div>

                            <div id="5eoNTtE" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/5eoNTtE" data-page="0">
        <img alt="" src="//i.imgur.com/5eoNTtEb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="5eoNTtE" type="image" data-up="5432">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="5eoNTtE" type="image" data-downs="132">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-5eoNTtE">5,300</span>
                            <span class="points-text-5eoNTtE">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Adolphin Hitler</p>
        
        
        <div class="post-info">
            animated &middot; 424,510 views
        </div>
    </div>
    
</div>

                            <div id="FfyLQiC" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/FfyLQiC" data-page="0">
        <img alt="" src="//i.imgur.com/FfyLQiCb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="FfyLQiC" type="image" data-up="6533">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="FfyLQiC" type="image" data-downs="300">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-FfyLQiC">6,233</span>
                            <span class="points-text-FfyLQiC">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My manager and I when the new intern showed up for work and she wasn&#039;t wearing a bra</p>
        
        
        <div class="post-info">
            animated &middot; 1,210,027 views
        </div>
    </div>
    
</div>

                            <div id="mo5loDn" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/mo5loDn" data-page="0">
        <img alt="" src="//i.imgur.com/mo5loDnb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="mo5loDn" type="image" data-up="3057">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="mo5loDn" type="image" data-downs="62">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-mo5loDn">2,995</span>
                            <span class="points-text-mo5loDn">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Let&#039;s call in sick, stay home and watch the rain</p>
        
        
        <div class="post-info">
            animated &middot; 199,157 views
        </div>
    </div>
    
</div>

                            <div id="UYQQ8bX" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/UYQQ8bX" data-page="0">
        <img alt="" src="//i.imgur.com/UYQQ8bXb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="UYQQ8bX" type="image" data-up="3754">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="UYQQ8bX" type="image" data-downs="242">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-UYQQ8bX">3,512</span>
                            <span class="points-text-UYQQ8bX">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>*rinkydink* Hey! I didn&#039;t realize I had to actually pay my gas bills! Hey! *rinkydink*</p>
        
        
        <div class="post-info">
            image &middot; 211,796 views
        </div>
    </div>
    
</div>

                            <div id="ladUcA6" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ladUcA6" data-page="0">
        <img alt="" src="//i.imgur.com/ladUcA6b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ladUcA6" type="image" data-up="6073">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ladUcA6" type="image" data-downs="96">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ladUcA6">5,977</span>
                            <span class="points-text-ladUcA6">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Effective and quick way to put out a grease fire.</p>
        
        
        <div class="post-info">
            animated &middot; 579,407 views
        </div>
    </div>
    
</div>

                            <div id="jOOZD" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/jOOZD" data-page="0">
        <img alt="" src="//i.imgur.com/lp80Tcwb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="jOOZD" type="image" data-up="1876">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="jOOZD" type="image" data-downs="31">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-jOOZD">1,845</span>
                            <span class="points-text-jOOZD">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>During WWII, Britain&#039;s MI5 hired a graphic designer named Laurence Fish to draw intercepted German bombs. Here&#039;s some of his work.</p>
        
        
        <div class="post-info">
            album &middot; 40,362 views
        </div>
    </div>
    
</div>

                            <div id="UIMmP" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/UIMmP" data-page="0">
        <img alt="" src="//i.imgur.com/KZsuWPHb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="UIMmP" type="image" data-up="1465">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="UIMmP" type="image" data-downs="51">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-UIMmP">1,414</span>
                            <span class="points-text-UIMmP">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Since Halloween is coming</p>
        
        
        <div class="post-info">
            album &middot; 22,529 views
        </div>
    </div>
    
</div>

                            <div id="yFZw15w" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/yFZw15w" data-page="0">
        <img alt="" src="//i.imgur.com/yFZw15wb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="yFZw15w" type="image" data-up="3142">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="yFZw15w" type="image" data-downs="138">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-yFZw15w">3,004</span>
                            <span class="points-text-yFZw15w">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>dating for a year &amp; a half... still don&#039;t know how to girlfriend</p>
        
        
        <div class="post-info">
            image &middot; 190,837 views
        </div>
    </div>
    
</div>

                            <div id="O7FZPJI" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/O7FZPJI" data-page="0">
        <img alt="" src="//i.imgur.com/O7FZPJIb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="O7FZPJI" type="image" data-up="539">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="O7FZPJI" type="image" data-downs="24">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-O7FZPJI">515</span>
                            <span class="points-text-O7FZPJI">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Told my friend I had the dumbest idea for an image. Two days later he sent me this</p>
        
        
        <div class="post-info">
            image &middot; 517,191 views
        </div>
    </div>
    
</div>

                            <div id="Ai6Ec" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Ai6Ec" data-page="0">
        <img alt="" src="//i.imgur.com/JIJckJWb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Ai6Ec" type="image" data-up="3670">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Ai6Ec" type="image" data-downs="77">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Ai6Ec">3,593</span>
                            <span class="points-text-Ai6Ec">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>everyone needs a hobby</p>
        
        
        <div class="post-info">
            album &middot; 125,630 views
        </div>
    </div>
    
</div>

            
        </div>

        <div class="clear"></div>

        <div class="imagelist-loader textbox center lineheight small-margin-top">
            <img src="//s.imgur.com/images/loaders/ddddd1_181817/48.gif" />
        </div>
    


    </div>
            <div class="page-divider"><a href="javascript:;" class="scroll-top-text" data-jafo="{@@event@@:@@galleryBackToTop@@,@@meta@@:{@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@pageNum@@:0}}">back to top</a></div>
    


                    
                </div>
            
                                                            <div id="top-comments" class="left-column">
                                                                                    <div id="outside-tagging" class="br5">
                                    <div class="group-add-done nodisplay"></div>
        
                                    <div id="tags-current-list" class="tag-list tag-list-immutable">
                                                                                        <div class="panel">
        <div class="textbox">
            <h2>Today&#039;s Best Comments</h2>
        </div>
    
        <div class="captions best-captions">
                            <div class="caption-container rank-0">
                    <p class="clickability">
                        <a href="/gallery/rc7idjR/comment/488377723"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Two can play that game I guess" src="//i.imgur.com/rc7idjRb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/KoujiIsAmazing">KoujiIsAmazing</a> 4,471 points
                        </div>
        
                                                    &quot;I feel like killing myself.&quot; &quot;Ugh, I have a boyfriend.&quot;
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-1">
                    <p class="clickability">
                        <a href="/gallery/OApc53F/comment/488546651"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I created a Dads and Daughters hair class to help other fathers learn about doing hair. We had a small class full of dads that learned basic brushing, ponytails, braids and buns. The turnout was great! I&#039;m so proud of these dads for stepping up and buildi" src="//i.imgur.com/OApc53Fb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/PrincessDiannaOffical">PrincessDiannaOffical</a> 3,877 points
                        </div>
        
                                                    Dad:what you want? Daughter: let me get that basic ponytail. Dad: Say no more fam.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-2">
                    <p class="clickability">
                        <a href="/gallery/qHn6G5F/comment/488602023"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I just wrote the perfect &#039;a&#039;." src="//i.imgur.com/qHn6G5Fb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/sevendogs">sevendogs</a> 3,761 points
                        </div>
        
                                                    On the blue line and everything. &#9829;.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/IQwfxb0/comment/488460626"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="." src="//i.imgur.com/IQwfxb0b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/tomslick42">tomslick42</a> 3,223 points
                        </div>
        
                                                    This is the most uncomfortable thing I&#039;ve ever seen on here. I hate you. +1
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/aieQg/comment/488632950"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="What a difference a dad makes.." src="//i.imgur.com/UQ7vtL4b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/JamesWTate">JamesWTate</a> 3,097 points
                        </div>
        
                                                    Always upvote lightly fried fish fillets.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/0AyQXXS/comment/488502542"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="People walking through heavy wind in Norway" src="//i.imgur.com/0AyQXXSb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/JeremyClarksonDidNothingWrong">JeremyClarksonDidNothingWrong</a> 3,068 points
                        </div>
        
                                                    The number of mimes in Norway is too damn high.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container last-child">
                    <p class="clickability">
                        <a href="/gallery/jEL5rtB/comment/488376903"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="When you see a ghost but remember your training" src="//i.imgur.com/jEL5rtBb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/Frubeling">Frubeling</a> 2,697 points
                        </div>
        
                                                    RKO OUTTA NOWHERE
                        
                    </div>
                </div>

                <div class="clear"></div>
            
    
            
        </div>
    </div>


                                        
                                    </div>
                                </div>
        
                                <div id="outside-tagging-showhide" class="no-select title-e short" title=" Show Sidebar">
                                                                            <div id="comment-tab">
                                            <div id="comment-icon-one"></div>
                                            <div id="comment-icon-two"></div>
                                            <div id="comment-icon-three"></div>
                                        </div>
                                    
                                </div>
                            
                        
                        </div>
                    
                
            </div>
        

        <div class="clear"></div>
    </div>

    <div class="nodisplay">
        <div id="past-wrapper" class="popup">
            <div>
                <div class="textbox left">
                    

                    <input type="text" id="days" />

                                            days ago
                    
                </div>
                
                <div id="past-calendar-wrapper" class="textbox left">
                    <div id="past-calendar"></div>
                </div>

                <div id="slider-wrapper" class="textbox left">
                    <div class="ticker negative">&laquo;</div>
                    <div id="slider"></div>
                    <div class="ticker positive">&raquo;</div>
                </div>

                <div id="button-wrapper" class="textbox left">
                    <input type="button" class="button-medium" value="Go" id="past-submit" />
                </div>

                <div class="clear"></div>
            </div>

            <div id="past-loader"></div>
            <div id="past-preview"></div>
        </div>
    </div>

    <div class="tag-group-selector-container"></div>
            <div id="top-tag-container"></div>
    

    <input id="sid" type="hidden" value="f026395cecb7c4f82dcecbb89b11a12c" />
        

    

            
    

    

            <script type="text/javascript">
            (function() {
                var roll = Math.random();
                if(roll < 0.05) {
                    
                    (function(_,e,rr,s){
                        _errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                        _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)
                    })(window,document,"script","51d1e360b05ccb10310000d2");
                    
                }
            })();
        </script>

        
<!--[if lte IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery-1.10.2.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if gt IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<!--<![endif]-->



        <script type="text/javascript" src="//s.imgur.com/min/global.js?1444239525"></script>

        <script type="text/javascript">
            Imgur.Environment = {
                isDev:         false,
                isSubdomain:   Imgur.Util.isSubdomain('imgur.com'),
                host:          'imgur.com',
                cdnUrl:        '//i.imgur.com',
                staticUrl:     '//s.imgur.com',
                signed:        false,
                auth:          {},
                recaptcha_key: '6LeZbt4SAAAAAKEsafT3QzEFp5vJ1-Z23uy5mPDz',
                msid:          'f026395cecb7c4f82dcecbb89b11a12c',
                beta:          {
                    enabled:   false,
                    inBeta:    false
                },
                
                ctaConfig:     {"anonymous":[{"active":false,"type":"button","trackingName":"anonymous","localStorageName":"cta-anonymous","url":"\/","buttonText":"start exploring","title":"Welcome to Imgur!","description":"Your new home for the Internet's best visual stories.","background":"{STATIC}\/images\/house-cta\/cta-background.jpg","newTab":false,"buttonColor":"#77ac21"}],"user":[{"active":false,"type":"button","trackingName":"campImgur","localStorageName":"cta-camp1418760143","customClass":"u-pl95","url":"\/\/imgur.com\/camp","buttonText":"Get More Details","title":"Camp Imgur, August 2015","description":"We're 50% sold out! Reserve your space now.","background":"{STATIC}\/images\/house-cta\/cta-camp-imgur.jpg","newTab":true,"buttonColor":"#cf5033"}],"gaming":[{"active":true,"type":"button","trackingName":"gaming","localStorageName":"cta-gaming","url":"\/\/imgur.com\/t\/gaming\/top\/week","buttonText":"Check it out!","title":"You are now browsing the Internet on God Mode.","description":"Awesome unlocked! Imgur has gaming content like you wouldn't believe.","background":"{STATIC}\/images\/house-cta\/cta-twitch.jpg","newTab":false,"buttonColor":"#4d3280"}],"facebook":[{"active":true,"type":"custom","jsReactClassName":"Facebook","trackingName":"facebook","localStorageName":"cta-fb141008","title":"Like us on Facebook!","description":"We're bringing funny to your newsfeed. Like Imgur on Facebook!","background":"{STATIC}\/images\/house-cta\/facebook-day.jpg"}],"twitter":[{"active":true,"type":"custom","jsReactClassName":"Twitter","url":"https:\/\/twitter.com\/imgur","trackingName":"twitter","localStorageName":"cta-tw141008","newTab":true,"title":"Follow us on Twitter!","description":""}],"pinterest":[{"active":true,"type":"custom","jsReactClassName":"Pinterest","trackingName":"pinterest","localStorageName":"cta-pinterest-2015-07-14","title":"Follow us on Pinterest!","background":"{STATIC}\/images\/house-cta\/pinterest.png","customClass":"pinterest"}],"promoted":[{"active":true,"type":"custom","jsReactClassName":"Promoted","trackingName":"promoted","localStorageName":false,"buttonText":"Learn More","title":"What Are Promoted Posts?","description":"And why am I seeing them?","background":"{STATIC}\/images\/house-cta\/cta-promoted.jpg","url":"\/\/imgur.com\/blog\/?p=6101","newTab":true,"customClass":"pp-banner"}],"global":[{"active":false,"trackingName":"inCaseYouMissedIt","localStorageName":"cta-icymi-2015-07-14","type":"button","subtype":"app","background":"{STATIC}\/images\/house-cta\/cta-icymi.png","url":"\/\/imgur.com\/topic\/Hand_Picked\/","buttonText":"Take me there","title":"Catch up on the awesome posts you might have missed!","description":"Now featured in the Hand Picked topic.","newTab":false,"customClass":"u-pl260 icymi","buttonColor":"#252525","frontPageOnly":true},{"active":true,"trackingName":"mobileApps","localStorageName":"apps1433176979","type":"button","background":"{STATIC}\/images\/house-cta\/cta-apps.jpg?1444096104","url":"\/\/imgur.com\/apps","buttonText":"Get App","title":"The Imgur Mobile App Has Been Upgraded!","description":"Now with Search, Upload, and Grid View","newTab":true,"customClass":"u-pl310"},{"active":true,"type":"custom","jsReactClassName":"Newsletter","trackingName":"inCaseYouMissedItForm","localStorageName":"apps1440530195","background":"{STATIC}\/images\/house-cta\/cta-icymi-form.png?1440530195","newTab":true,"customClass":"u-pl200"}],"global-lightbox":[{"active":true,"trackingName":"mobileApps","localStorageName":"apps1442873156","type":"custom","jsReactClassName":"GetTheApp","background":"{STATIC}\/images\/space-med-bg.png?1433176979","newTab":true}]},
                experiments:   {"exp1868":{"active":false,"name":"hamburger-topics","inclusionProbability":1,"expirationDate":"2015-09-30T00:00:00.000Z","variations":[{"name":"hamburger-topics","inclusionProbability":0.5}]},"exp3485":{"active":true,"name":"recs-new-users","inclusionProbability":0.2,"expirationDate":"2015-10-30T00:00:00.000Z","variations":[{"name":"recs-new-users","inclusionProbability":0.5}]},"exp3125":{"active":true,"name":"idle-cta-lightbox-mobileapp","inclusionProbability":0.05,"expirationDate":"2015-10-10T00:00:00.000Z","variations":[{"name":"idle-cta-lightbox-mobileapp","inclusionProbability":1}]}},
                isGalleryAdmin:false,
                ppBlog: '//imgur.com/blog/?p=6101'
            };

            var imgur = Imgur.getInstance();
            imgur.init(_.merge(Imgur.Environment, {
                
                
                beta: {"enabled":false,"inBeta":false,"showInvite":false,"ctaInvite":[],"ctaWelcome":[]},
            }));

                            var uploader = Imgur.Upload.Global.getInstance(Imgur.getInstance()._);

                if(uploader._.el.$uploadGlobal.length > 0) {
                    uploader.bindPasteUpload();
                    uploader.generalInit();
                }
            

            imgur.generalInit();

            $(function() {
                
            });


            

            var _widgetFactory = new Imgur.Factory.Widget(Imgur.Environment);

            _widgetFactory.mergeConfig('global', Imgur.Environment);

            
        </script>

        <script type="text/javascript">
            var __nspid="1mvmyo";
            var __nsptags=[];

            (function() {
                var roll = Math.random();
                if(roll < 0.1) {
                    
                        // This is the NSONE Pulsar tag
                        (function(w, d) { var x = function() {
                        var j=d.createElement("script");j.type="text/javascript";j.async=true;
                        j.src="http"+("https:"===d.location.protocol?"s://cs":"://c")+".ns1p.net/p.js?a="+__nspid;
                        d.body.appendChild(j); }
                        if(w.addEventListener) { w.addEventListener("load", x, false); }
                        else if(w.attachEvent) { w.attachEvent("onload", x); }
                        else { w.onload = x; }
                        }(window, document));

                    
                }
            })();
        </script>

    

                    <script type="text/javascript">
            (function(widgetFactory) {
                widgetFactory.mergeConfig('analytics', {
                    isAdmin: false,
                    logPixel: '//p.imgur.com/lumbar.gif'
                });
                widgetFactory.mergeConfig('global', {
                    analyticsAccountId: 'UA-6671908-2'
                });

                widgetFactory.mergeConfig('search', {
                    type: 'global', 
                    q: ''
                });

                widgetFactory.produceJafo();

                widgetFactory.startExperiments();

                                    widgetFactory.produceCtaBanner("/gallery/" + "aieQg");
                    Imgur.Util.Experiments._exp3125 = _.bind(widgetFactory.produceCtaLightbox, widgetFactory, "/gallery/" + "aieQg");
                
            })(_widgetFactory);
        </script>
    

    <script type="text/javascript">
    (function(widgetFactory) { 
        widgetFactory.mergeConfig('global', {
            sid: 'f026395cecb7c4f82dcecbb89b11a12c'
        });

        widgetFactory.mergeConfig('gallery', {
            sid         : 'f026395cecb7c4f82dcecbb89b11a12c',
            signed      : false,
            baseURL     : decodeURIComponent('%2Fgallery'),
            sort        : 'viral',
            window      : 'day',
            page        : 0,
            isPro       : false,
            maxPage     : 1741,
            showPast    : true,
            searchQuery : '',
            inSearch    : false,
            advSearch   : null,
            bestCaps    : true
        });

        widgetFactory.mergeConfig('groups', {
            groups: {
                
            }
        });

        widgetFactory.mergeConfig('search', {
            
        });
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
        (function(widgetFactory) {
            var group_type = 'base';

            var group = {
                id: '[hot]',
                tags: [
                    
                ]
            }

            widgetFactory.mergeConfig('gallery', {
                section            : 'hot',
                showGalleryNewInfo : false,
                gallery_type       : null,
                tag                : null,
                sponsoredTag       : null,
                tagFollower        : false,
                group_type         : group_type,
                group              : group
            });
        })(_widgetFactory);
    </script>


            <script type="text/javascript">
            if (typeof _widgetFactory !== 'undefined') {
                _widgetFactory.initNotifications({"post":{"postUpvote":[10,50,100,500,1000,5000],"postComment":[10,50,100],"postView":[100000,500000,1000000,5000000],"postEmbed":[1],"postViral":[1]},"comment":{"commentUpvote":[10,100,500,1000,5000,10000],"commentReply":[1],"commentTop7":[1],"commentMention":[1]},"trophy":{"userTrophy":[1]},"account":{"userNotoriety":[-1,399,999,1999,3999,7999,19999,9223372036854775807]},"message":{"message-msg":[1]}}, [{"name":"Forever Alone","reputation":-1},{"name":"Neutral","reputation":399},{"name":"Accepted","reputation":999},{"name":"Liked","reputation":1999},{"name":"Trusted","reputation":3999},{"name":"Idolized","reputation":7999},{"name":"Renowned","reputation":19999},{"name":"Glorious","reputation":9223372036854775807}]);
            }
        </script>
    

        <script type="text/javascript" src="//s.imgur.com/min/index.js?1444239525"></script>

    <script type="text/javascript" src="//s.imgur.com/min/gallery.js?1444239525"></script>

    <script type="text/javascript">
    (function(widgetFactory) {
        var emitter = new Imgur.Emitter();

        widgetFactory.produceLocationCommand(emitter);

        var galleryFactory = widgetFactory.produceOutsideGalleryFactory();
        var galleryGraph = galleryFactory.produceOutsideGallery(emitter);

        var gallery = galleryGraph.gallery,
            history = galleryGraph.history;

        var outsideTaggingWidget = galleryFactory.produceOutsideTaggingWidget(emitter, galleryGraph);

        gallery.outsideInit();

        

        Imgur.Upload.Index.getInstance().generalInit();
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
    $(function() {
        if(!/^([^:]+:)\/\/([^.]+\.)*imgur(-dev)?\.com(\/.*)?$/.test(document.referrer)) {
            Imgur.Util.jafoLog({ event: 'galleryDisplay', meta: { gallerySort: 'viral', galleryType: 'hot' }});
        }
    });
    </script>

    

            <script type="text/javascript">
        if (typeof _widgetFactory !== 'undefined') {
            _widgetFactory.produceSearch();
            _widgetFactory.initIdleMonitor();
        }
        </script>
    

        
    

            <script type="text/javascript">
            Imgur.Util.getGoogleTracker();
            __ga('send', 'pageview');
        </script>

        
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        

        

        

        <!-- Begin comScore Tag -->
        <script type="text/javascript">
            var _comscore = _comscore || [];
            _comscore.push({ c1: "2", c2: "7770950" });
            (function() {
                var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
                s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
                el.parentNode.insertBefore(s, el);
            })();
        </script>
        <noscript>
            <img src="http://b.scorecardresearch.com/p?c1=2&c2=7770950&cv=2.0&cj=1" />
        </noscript>
        <!-- End comScore Tag -->

        <!-- Quantcast Tag -->
        <script type="text/javascript">
        var _qevents = _qevents || [];
        (function() {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();
        _qevents.push({
            qacct:"p-f8oruOqDFlMeI"
        });
        </script>

        <noscript>
            <div style="display:none;"><img src="//pixel.quantserve.com/pixel/p-f8oruOqDFlMeI.gif" border="0" height="1" width="1" alt="Quantcast"/></div>
        </noscript>
        <!-- End Quantcast tag -->

        <!-- Facebook Pixel Code -->
        <script type="text/javascript">
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

            fbq('init', '742377892535530');
            fbq('track', 'PageView');

            if (document.location.search === '?reg') {
                fbq('track', 'CompleteRegistration');
            }
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1076474169030064&ev=PageView&noscript=1"/></noscript>
        <!-- End Facebook Pixel Code -->
    

        <script type="text/javascript">
        
        (function(){
            try{
                var msg = '\n      _\n     (_)\n      _ _ __ ___   __ _ _   _ _ __\n     | | \'_ ` _ \\ / _` | | | | \'__|\n     | | | | | | | (_| | |_| | |\n     |_|_| |_| |_|\\__, |\\__,_|_|\n                   __/ |\n                  |___/\n';
                msg += '========================================\nYou opened the console! Know some code,\ndo you? Want to work for one of the best\nstartups around? http://imgur.com/jobs\n========================================\n';
                imcon.log(msg);
            }catch(e){}
        })()
        
    </script>
    

        

</body>
</html>
