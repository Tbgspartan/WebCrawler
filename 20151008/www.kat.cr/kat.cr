<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-d673760.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-d673760.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-d673760.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-d673760.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-d673760.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'd673760',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-d673760.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			<li><a data-nop href="/rules/"><i class="ka ka-DMCA lower"></i><span class="menuItem">rules</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/bahubali/" class="tag2">bahubali</a>
	<a href="/search/big%20bang%20theory/" class="tag2">big bang theory</a>
	<a href="/search/blindspot/" class="tag4">blindspot</a>
	<a href="/search/castle/" class="tag2">castle</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/everest/" class="tag2">everest</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag4">fear the walking dead</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag3">fear the walking dead</a>
	<a href="/search/french/" class="tag2">french</a>
	<a href="/search/gog/" class="tag2">gog</a>
	<a href="/search/gotham/" class="tag5">gotham</a>
	<a href="/search/gotham%20s02e03/" class="tag3">gotham s02e03</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi%202015/" class="tag4">hindi 2015</a>
	<a href="/search/hindi%20movies%202015/" class="tag5">hindi movies 2015</a>
	<a href="/search/hotel%20transylvania/" class="tag2">hotel transylvania</a>
	<a href="/search/kat%20ph%20com/" class="tag7">kat ph com</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/minority%20report/" class="tag2">minority report</a>
	<a href="/search/nezu/" class="tag9">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/rick%20and%20morty/" class="tag2">rick and morty</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/scorpion/" class="tag2">scorpion</a>
	<a href="/search/singh%20is%20bling/" class="tag2">singh is bling</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20big%20bang%20theory/" class="tag3">the big bang theory</a>
	<a href="/search/the%20big%20bang%20theory%20s09e03/" class="tag2">the big bang theory s09e03</a>
	<a href="/search/the%20blacklist/" class="tag2">the blacklist</a>
	<a href="/search/the%20flash/" class="tag2">the flash</a>
	<a href="/search/the%20martian/" class="tag4">the martian</a>
	<a href="/search/the%20martian%202015/" class="tag3">the martian 2015</a>
	<a href="/search/the%20strain/" class="tag3">the strain</a>
	<a href="/search/the%20walking%20dead/" class="tag3">the walking dead</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
	<a href="/search/%D7%A1%D7%A8%D7%98%D7%95%D7%9F%20%D7%90%D7%9C%D7%A0%D7%91%D7%99%2040%20%D7%9C%D7%90%20%D7%9E%D7%A6%D7%95%D7%A0%D7%96%D7%A8/" class="tag2">×¡×¨××× ××× ×× 40 ×× ××¦×× ××¨</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11365372,0" class="icommentjs icon16" href="/turbo-kid-2015-720p-brrip-x264-yify-t11365372.html#comment"> <em class="iconvalue">62</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/turbo-kid-2015-720p-brrip-x264-yify-t11365372.html" class="cellMainLink">Turbo Kid (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="791936858">755.25 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T20:11:07+00:00">05 Oct 2015, 20:11:07</span></td>
			<td class="green center">6563</td>
			<td class="red lasttd center">4489</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366577,0" class="icommentjs icon16" href="/paper-towns-2015-1080p-brrip-x264-yify-t11366577.html#comment"> <em class="iconvalue">38</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/paper-towns-2015-1080p-brrip-x264-yify-t11366577.html" class="cellMainLink">Paper Towns (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1758831743">1.64 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T02:57:07+00:00">06 Oct 2015, 02:57:07</span></td>
			<td class="green center">4180</td>
			<td class="red lasttd center">4527</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11356741,0" class="icommentjs icon16" href="/narcopolis-2015-1080p-brrip-x264-yify-t11356741.html#comment"> <em class="iconvalue">30</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/narcopolis-2015-1080p-brrip-x264-yify-t11356741.html" class="cellMainLink">Narcopolis (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1549135222">1.44 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T00:24:15+00:00">04 Oct 2015, 00:24:15</span></td>
			<td class="green center">4828</td>
			<td class="red lasttd center">2653</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366784,0" class="icommentjs icon16" href="/z-for-zachariah-2015-720p-brrip-x264-yify-t11366784.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/z-for-zachariah-2015-720p-brrip-x264-yify-t11366784.html" class="cellMainLink">Z for Zachariah (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="795037351">758.21 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T04:08:01+00:00">06 Oct 2015, 04:08:01</span></td>
			<td class="green center">4042</td>
			<td class="red lasttd center">2956</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11365637,0" class="icommentjs icon16" href="/operator-2015-720p-brrip-x264-yify-t11365637.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/operator-2015-720p-brrip-x264-yify-t11365637.html" class="cellMainLink">Operator (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="729716943">695.91 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T21:39:18+00:00">05 Oct 2015, 21:39:18</span></td>
			<td class="green center">4140</td>
			<td class="red lasttd center">2345</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11368232,0" class="icommentjs icon16" href="/the-vatican-tapes-2015-720p-brrip-x264-yify-t11368232.html#comment"> <em class="iconvalue">47</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-vatican-tapes-2015-720p-brrip-x264-yify-t11368232.html" class="cellMainLink">The Vatican Tapes (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="738651170">704.43 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T11:45:52+00:00">06 Oct 2015, 11:45:52</span></td>
			<td class="green center">2385</td>
			<td class="red lasttd center">1998</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11358042,0" class="icommentjs icon16" href="/eden-2014-brrip-xvid-ac3-evo-t11358042.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/eden-2014-brrip-xvid-ac3-evo-t11358042.html" class="cellMainLink">Eden 2014 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1463709373">1.36 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T07:20:58+00:00">04 Oct 2015, 07:20:58</span></td>
			<td class="green center">1570</td>
			<td class="red lasttd center">1341</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11359239,0" class="icommentjs icon16" href="/partisan-2015-hdrip-xvid-etrg-t11359239.html#comment"> <em class="iconvalue">30</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/partisan-2015-hdrip-xvid-etrg-t11359239.html" class="cellMainLink">Partisan 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="741983225">707.61 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T13:26:53+00:00">04 Oct 2015, 13:26:53</span></td>
			<td class="green center">1744</td>
			<td class="red lasttd center">950</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11362684,0" class="icommentjs icon16" href="/the-assassin2015-hd1080p-x264-aac-mandarin-chs-mp4ba-t11362684.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-assassin2015-hd1080p-x264-aac-mandarin-chs-mp4ba-t11362684.html" class="cellMainLink">The Assassin2015 HD1080P X264 AAC Mandarin CHS Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="1832910970">1.71 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T07:52:16+00:00">05 Oct 2015, 07:52:16</span></td>
			<td class="green center">1312</td>
			<td class="red lasttd center">1535</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11367918,0" class="icommentjs icon16" href="/berkshire-county-2014-720p-brrip-x264-yify-t11367918.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/berkshire-county-2014-720p-brrip-x264-yify-t11367918.html" class="cellMainLink">Berkshire County (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="725894935">692.27 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T10:03:49+00:00">06 Oct 2015, 10:03:49</span></td>
			<td class="green center">1339</td>
			<td class="red lasttd center">1078</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366582,0" class="icommentjs icon16" href="/kis-kisko-pyaar-karoon-2015-desi-scr-rip-xvid-1cd-m-subs-team-ictv-xclusive-t11366582.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kis-kisko-pyaar-karoon-2015-desi-scr-rip-xvid-1cd-m-subs-team-ictv-xclusive-t11366582.html" class="cellMainLink">Kis Kisko Pyaar Karoon (2015) Desi SCR Rip - XviD - [1CD] - M-Subs Team IcTv Xclusive</a></div>
			</td>
			<td class="nobr center" data-sort="758660204">723.51 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T02:58:39+00:00">06 Oct 2015, 02:58:39</span></td>
			<td class="green center">951</td>
			<td class="red lasttd center">1767</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11369116,0" class="icommentjs icon16" href="/the-martian-2015-cam-x264-aac-gly-t11369116.html#comment"> <em class="iconvalue">39</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-martian-2015-cam-x264-aac-gly-t11369116.html" class="cellMainLink">The Martian 2015 CAM x264 AAC-GLY</a></div>
			</td>
			<td class="nobr center" data-sort="1941922119">1.81 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T15:53:13+00:00">06 Oct 2015, 15:53:13</span></td>
			<td class="green center">844</td>
			<td class="red lasttd center">1324</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11362209,0" class="icommentjs icon16" href="/london-road-2015-hdrip-xvid-etrg-t11362209.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/london-road-2015-hdrip-xvid-etrg-t11362209.html" class="cellMainLink">London Road 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="742945047">708.53 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T05:51:46+00:00">05 Oct 2015, 05:51:46</span></td>
			<td class="green center">1064</td>
			<td class="red lasttd center">826</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11364895,0" class="icommentjs icon16" href="/chariot-2013-720p-brrip-x264-yify-t11364895.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/chariot-2013-720p-brrip-x264-yify-t11364895.html" class="cellMainLink">Chariot (2013) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="787105323">750.64 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T17:56:35+00:00">05 Oct 2015, 17:56:35</span></td>
			<td class="green center">1019</td>
			<td class="red lasttd center">586</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11359717,0" class="icommentjs icon16" href="/singh-is-bling-2015-2cd-desiscr-rip-x264-ac3-5-1-upmix-dus-t11359717.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/singh-is-bling-2015-2cd-desiscr-rip-x264-ac3-5-1-upmix-dus-t11359717.html" class="cellMainLink">Singh Is Bling (2015) 2CD DesiSCR Rip - x264 AC3 5.1(UpMix) - DUS</a></div>
			</td>
			<td class="nobr center" data-sort="1472918063">1.37 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T15:45:05+00:00">04 Oct 2015, 15:45:05</span></td>
			<td class="green center">808</td>
			<td class="red lasttd center">830</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11370984,0" class="icommentjs icon16" href="/the-flash-2014-s02e01-hdtv-x264-lol-ettv-t11370984.html#comment"> <em class="iconvalue">258</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-flash-2014-s02e01-hdtv-x264-lol-ettv-t11370984.html" class="cellMainLink">The Flash 2014 S02E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="269859762">257.36 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T01:02:14+00:00">07 Oct 2015, 01:02:14</span></td>
			<td class="green center">15278</td>
			<td class="red lasttd center">11790</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366248,0" class="icommentjs icon16" href="/gotham-s02e03-hdtv-x264-lol-ettv-t11366248.html#comment"> <em class="iconvalue">135</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gotham-s02e03-hdtv-x264-lol-ettv-t11366248.html" class="cellMainLink">Gotham S02E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="271833846">259.24 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T01:01:11+00:00">06 Oct 2015, 01:01:11</span></td>
			<td class="green center">8872</td>
			<td class="red lasttd center">1697</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11365919,0" class="icommentjs icon16" href="/the-big-bang-theory-s09e03-hdtv-x264-lol-ettv-t11365919.html#comment"> <em class="iconvalue">119</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-big-bang-theory-s09e03-hdtv-x264-lol-ettv-t11365919.html" class="cellMainLink">The Big Bang Theory S09E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="132018171">125.9 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T23:04:04+00:00">05 Oct 2015, 23:04:04</span></td>
			<td class="green center">8864</td>
			<td class="red lasttd center">1164</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11361274,0" class="icommentjs icon16" href="/quantico-s01e02-hdtv-x264-lol-ettv-t11361274.html#comment"> <em class="iconvalue">173</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/quantico-s01e02-hdtv-x264-lol-ettv-t11361274.html" class="cellMainLink">Quantico S01E02 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="356510651">340 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T01:00:29+00:00">05 Oct 2015, 01:00:29</span></td>
			<td class="green center">8724</td>
			<td class="red lasttd center">1441</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11371158,0" class="icommentjs icon16" href="/marvels-agents-of-s-h-i-e-l-d-s03e02-hdtv-x264-killers-ettv-t11371158.html#comment"> <em class="iconvalue">102</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/marvels-agents-of-s-h-i-e-l-d-s03e02-hdtv-x264-killers-ettv-t11371158.html" class="cellMainLink">Marvels Agents of S H I E L D S03E02 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="289793968">276.37 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T02:04:15+00:00">07 Oct 2015, 02:04:15</span></td>
			<td class="green center">6366</td>
			<td class="red lasttd center">5689</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11370821,0" class="icommentjs icon16" href="/limitless-s01e03-hdtv-x264-lol-ettv-t11370821.html#comment"> <em class="iconvalue">100</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/limitless-s01e03-hdtv-x264-lol-ettv-t11370821.html" class="cellMainLink">Limitless S01E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="306373412">292.18 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T00:00:13+00:00">07 Oct 2015, 00:00:13</span></td>
			<td class="green center">5819</td>
			<td class="red lasttd center">5853</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11361528,0" class="icommentjs icon16" href="/homeland-s05e01-internal-hdtv-x264-batv-ettv-t11361528.html#comment"> <em class="iconvalue">108</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/homeland-s05e01-internal-hdtv-x264-batv-ettv-t11361528.html" class="cellMainLink">Homeland S05E01 INTERNAL HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="284063099">270.9 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T02:33:14+00:00">05 Oct 2015, 02:33:14</span></td>
			<td class="green center">6668</td>
			<td class="red lasttd center">617</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366396,0" class="icommentjs icon16" href="/blindspot-s01e03-hdtv-x264-lol-ettv-t11366396.html#comment"> <em class="iconvalue">83</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blindspot-s01e03-hdtv-x264-lol-ettv-t11366396.html" class="cellMainLink">Blindspot S01E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="295582165">281.89 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T02:02:16+00:00">06 Oct 2015, 02:02:16</span></td>
			<td class="green center">5872</td>
			<td class="red lasttd center">1288</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11361647,0" class="icommentjs icon16" href="/the-strain-s02e13-hdtv-x264-killers-ettv-t11361647.html#comment"> <em class="iconvalue">89</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-strain-s02e13-hdtv-x264-killers-ettv-t11361647.html" class="cellMainLink">The Strain S02E13 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="313626003">299.1 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T03:18:14+00:00">05 Oct 2015, 03:18:14</span></td>
			<td class="green center">5145</td>
			<td class="red lasttd center">496</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366565,0" class="icommentjs icon16" href="/minority-report-s01e03-hdtv-x264-fum-ettv-t11366565.html#comment"> <em class="iconvalue">45</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/minority-report-s01e03-hdtv-x264-fum-ettv-t11366565.html" class="cellMainLink">Minority Report S01E03 HDTV x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="340909332">325.12 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T02:54:19+00:00">06 Oct 2015, 02:54:19</span></td>
			<td class="green center">3952</td>
			<td class="red lasttd center">717</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366378,0" class="icommentjs icon16" href="/scorpion-s02e03-hdtv-x264-lol-ettv-t11366378.html#comment"> <em class="iconvalue">55</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scorpion-s02e03-hdtv-x264-lol-ettv-t11366378.html" class="cellMainLink">Scorpion S02E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="304323648">290.23 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T01:58:14+00:00">06 Oct 2015, 01:58:14</span></td>
			<td class="green center">3812</td>
			<td class="red lasttd center">798</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11361271,0" class="icommentjs icon16" href="/brooklyn-nine-nine-s03e02-hdtv-x264-fleet-rartv-t11361271.html#comment"> <em class="iconvalue">38</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/brooklyn-nine-nine-s03e02-hdtv-x264-fleet-rartv-t11361271.html" class="cellMainLink">Brooklyn Nine-Nine S03E02 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="179334346">171.03 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T01:00:16+00:00">05 Oct 2015, 01:00:16</span></td>
			<td class="green center">3922</td>
			<td class="red lasttd center">307</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11357703,0" class="icommentjs icon16" href="/ufc-192-cormier-vs-gustafsson-webrip-x264-jkkk-sparrow-t11357703.html#comment"> <em class="iconvalue">70</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-192-cormier-vs-gustafsson-webrip-x264-jkkk-sparrow-t11357703.html" class="cellMainLink">UFC 192 Cormier vs Gustafsson WEBRip x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="2037610746">1.9 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T05:38:05+00:00">04 Oct 2015, 05:38:05</span></td>
			<td class="green center">3624</td>
			<td class="red lasttd center">662</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11371184,0" class="icommentjs icon16" href="/scream-queens-2015-s01e04-hdtv-x264-killers-ettv-t11371184.html#comment"> <em class="iconvalue">37</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scream-queens-2015-s01e04-hdtv-x264-killers-ettv-t11371184.html" class="cellMainLink">Scream Queens 2015 S01E04 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="354537607">338.11 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T02:07:13+00:00">07 Oct 2015, 02:07:13</span></td>
			<td class="green center">2497</td>
			<td class="red lasttd center">2698</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11361281,0" class="icommentjs icon16" href="/once-upon-a-time-s05e02-hdtv-x264-fleet-rartv-t11361281.html#comment"> <em class="iconvalue">55</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/once-upon-a-time-s05e02-hdtv-x264-fleet-rartv-t11361281.html" class="cellMainLink">Once Upon A Time S05E02 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="355399459">338.94 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T01:02:47+00:00">05 Oct 2015, 01:02:47</span></td>
			<td class="green center">3603</td>
			<td class="red lasttd center">387</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11360301,0" class="icommentjs icon16" href="/va-top-100-deep-house-september-2015-mp3-320-kbps-t11360301.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-top-100-deep-house-september-2015-mp3-320-kbps-t11360301.html" class="cellMainLink">VA - Top 100 Deep House [September] (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1432543401">1.33 <span>GB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T18:23:34+00:00">04 Oct 2015, 18:23:34</span></td>
			<td class="green center">280</td>
			<td class="red lasttd center">187</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11364519,0" class="icommentjs icon16" href="/the-official-uk-top-singles-chart-02nd-october-2015-mp3-320kbps-h4ckus-glodls-t11364519.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-official-uk-top-singles-chart-02nd-october-2015-mp3-320kbps-h4ckus-glodls-t11364519.html" class="cellMainLink">The Official UK Top Singles Chart (02nd October 2015) [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="359743149">343.08 <span>MB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T16:27:45+00:00">05 Oct 2015, 16:27:45</span></td>
			<td class="green center">240</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11360136,0" class="icommentjs icon16" href="/va-the-greatest-singer-songwriter-classics-3cd-box-set-2015-mp3-cdda-cd-rip-t11360136.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-greatest-singer-songwriter-classics-3cd-box-set-2015-mp3-cdda-cd-rip-t11360136.html" class="cellMainLink">VA - The Greatest Singer-Songwriter Classics [3CD Box Set] 2015 MP3 / CDDA CD Rip</a></div>
			</td>
			<td class="nobr center" data-sort="409607319">390.63 <span>MB</span></td>
			<td class="center">65</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T17:40:51+00:00">04 Oct 2015, 17:40:51</span></td>
			<td class="green center">216</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11362447,0" class="icommentjs icon16" href="/va-trance-100-best-of-2015-mp3-320-kbps-t11362447.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-trance-100-best-of-2015-mp3-320-kbps-t11362447.html" class="cellMainLink">VA - Trance 100 â Best Of (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1648599350">1.54 <span>GB</span></td>
			<td class="center">103</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T06:46:30+00:00">05 Oct 2015, 06:46:30</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">155</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366200,0" class="icommentjs icon16" href="/paul-mccartney-pipes-of-peace-deluxe-2015-freak37-t11366200.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/paul-mccartney-pipes-of-peace-deluxe-2015-freak37-t11366200.html" class="cellMainLink">Paul McCartney â Pipes of Peace (Deluxe) ( 2015 ) ...-Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="171310678">163.37 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T00:44:16+00:00">06 Oct 2015, 00:44:16</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11360121,0" class="icommentjs icon16" href="/sweet-action-the-ultimate-story-2cd-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11360121.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/sweet-action-the-ultimate-story-2cd-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11360121.html" class="cellMainLink">Sweet - Action! The Ultimate Story [2CD] (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="391533992">373.4 <span>MB</span></td>
			<td class="center">42</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T17:36:32+00:00">04 Oct 2015, 17:36:32</span></td>
			<td class="green center">143</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11364741,0" class="icommentjs icon16" href="/the-howard-stern-show-and-the-wrap-up-show-2015-10-05-t11364741.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-howard-stern-show-and-the-wrap-up-show-2015-10-05-t11364741.html" class="cellMainLink">The Howard Stern Show and The Wrap Up Show - 2015-10-05</a></div>
			</td>
			<td class="nobr center" data-sort="266020169">253.7 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T17:15:18+00:00">05 Oct 2015, 17:15:18</span></td>
			<td class="green center">125</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ministry-of-sound-mash-up-mix-mixed-by-the-cut-up-boys-2015-mp3-320-kbps-t11367666.html" class="cellMainLink">VA - Ministry Of Sound Mash Up Mix (Mixed by The Cut Up Boys) (2015) Mp3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="731129438">697.26 <span>MB</span></td>
			<td class="center">42</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T08:46:55+00:00">06 Oct 2015, 08:46:55</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366760,0" class="icommentjs icon16" href="/beatport-singles-05-10-2015-edm-electro-house-future-house-hardstyle-trance-progressive-house-t11366760.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/beatport-singles-05-10-2015-edm-electro-house-future-house-hardstyle-trance-progressive-house-t11366760.html" class="cellMainLink">Beatport Singles - 05.10.2015 (EDM.Electro House,Future House,Hardstyle,Trance,Progressive House)</a></div>
			</td>
			<td class="nobr center" data-sort="1894952923">1.76 <span>GB</span></td>
			<td class="center">141</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T03:56:48+00:00">06 Oct 2015, 03:56:48</span></td>
			<td class="green center">54</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11361512,0" class="icommentjs icon16" href="/honeyroll-time-to-rock-2015-320ak-t11361512.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/honeyroll-time-to-rock-2015-320ak-t11361512.html" class="cellMainLink">Honeyroll - Time To Rock (2015) 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="116233100">110.85 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T02:28:21+00:00">05 Oct 2015, 02:28:21</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-deep-house-party-mix-2015-mp3-320-kbps-t11367686.html" class="cellMainLink">VA - Deep House Party Mix (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="783106703">746.83 <span>MB</span></td>
			<td class="center">71</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T08:54:07+00:00">06 Oct 2015, 08:54:07</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11363444,0" class="icommentjs icon16" href="/vinai-techno-extended-mix-320-kbps-edm-rg-t11363444.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/vinai-techno-extended-mix-320-kbps-edm-rg-t11363444.html" class="cellMainLink">VINAI - Techno (Extended Mix) [320 Kbps] [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="8171915">7.79 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T12:02:15+00:00">05 Oct 2015, 12:02:15</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11368945,0" class="icommentjs icon16" href="/deerhunter-fading-frontier-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11368945.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/deerhunter-fading-frontier-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11368945.html" class="cellMainLink">Deerhunter - Fading Frontier (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="87398717">83.35 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T15:07:55+00:00">06 Oct 2015, 15:07:55</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11369227,0" class="icommentjs icon16" href="/va-now-that-s-what-i-call-halloween-2015-compilation-mp3-edm-rg-t11369227.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-that-s-what-i-call-halloween-2015-compilation-mp3-edm-rg-t11369227.html" class="cellMainLink">VA - NOW That&#039;s What I Call Halloween (2015) Compilation @ MP3 [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="164062214">156.46 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T16:34:07+00:00">06 Oct 2015, 16:34:07</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11368748,0" class="icommentjs icon16" href="/disclosure-caracal-limited-deluxe-edition-2015-album-mp3-cdda-cd-rip-edm-rg-t11368748.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/disclosure-caracal-limited-deluxe-edition-2015-album-mp3-cdda-cd-rip-edm-rg-t11368748.html" class="cellMainLink">Disclosure - Caracal [Limited Deluxe Edition] 2015 Album @ MP3 / CDDA CD Rip [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="157853398">150.54 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T14:08:39+00:00">06 Oct 2015, 14:08:39</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">16</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11369383,0" class="icommentjs icon16" href="/transformers-devastation-codex-t11369383.html#comment"> <em class="iconvalue">25</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/transformers-devastation-codex-t11369383.html" class="cellMainLink">Transformers.Devastation-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="6884103764">6.41 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T17:12:18+00:00">06 Oct 2015, 17:12:18</span></td>
			<td class="green center">366</td>
			<td class="red lasttd center">928</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11365821,0" class="icommentjs icon16" href="/dragon-age-inquisition-deluxe-edition-cpy-t11365821.html#comment"> <em class="iconvalue">64</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/dragon-age-inquisition-deluxe-edition-cpy-t11365821.html" class="cellMainLink">Dragon Age Inquisition Deluxe Edition-CPY</a></div>
			</td>
			<td class="nobr center" data-sort="48834164628">45.48 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T22:36:59+00:00">05 Oct 2015, 22:36:59</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">566</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11360337,0" class="icommentjs icon16" href="/game-of-thrones-a-telltale-games-series-episode1-5-2014-repack-by-r-g-mechanics-t11360337.html#comment"> <em class="iconvalue">30</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/game-of-thrones-a-telltale-games-series-episode1-5-2014-repack-by-r-g-mechanics-t11360337.html" class="cellMainLink">Game of Thrones-A Telltale Games Series Episode1-5(2014) RePack By R.G Mechanics</a></div>
			</td>
			<td class="nobr center" data-sort="4676457314">4.36 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T18:33:01+00:00">04 Oct 2015, 18:33:01</span></td>
			<td class="green center">248</td>
			<td class="red lasttd center">171</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11357317,0" class="icommentjs icon16" href="/dark-souls-2-scholar-of-the-first-sin-2015-pc-repack-by-r-g-catalyst-t11357317.html#comment"> <em class="iconvalue">43</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/dark-souls-2-scholar-of-the-first-sin-2015-pc-repack-by-r-g-catalyst-t11357317.html" class="cellMainLink">Dark Souls 2: Scholar of the First Sin (2015) PC | RePack by R.G. Catalyst</a></div>
			</td>
			<td class="nobr center" data-sort="7283453134">6.78 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T04:04:31+00:00">04 Oct 2015, 04:04:31</span></td>
			<td class="green center">201</td>
			<td class="red lasttd center">229</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11360736,0" class="icommentjs icon16" href="/nba-2k16-update-1-multi8-fitgirl-repack-selective-download-27-4-30-2-gb-t11360736.html#comment"> <em class="iconvalue">81</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/nba-2k16-update-1-multi8-fitgirl-repack-selective-download-27-4-30-2-gb-t11360736.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/nba-2k16-update-1-multi8-fitgirl-repack-selective-download-27-4-30-2-gb-t11360736.html" class="cellMainLink">NBA 2K16 (+Update 1, MULTI8) [FitGirl Repack, Selective Download - 27.4/30.2 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="32461289353">30.23 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T20:51:06+00:00">04 Oct 2015, 20:51:06</span></td>
			<td class="green center">29</td>
			<td class="red lasttd center">386</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11369316,0" class="icommentjs icon16" href="/prison-architect-v1-0-t11369316.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/prison-architect-v1-0-t11369316.html" class="cellMainLink">Prison Architect v1.0</a></div>
			</td>
			<td class="nobr center" data-sort="281857194">268.8 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T16:56:47+00:00">06 Oct 2015, 16:56:47</span></td>
			<td class="green center">189</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11358012,0" class="icommentjs icon16" href="/grand-theft-auto-anthology-r-g-mechanics-t11358012.html#comment"> <em class="iconvalue">112</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/grand-theft-auto-anthology-r-g-mechanics-t11358012.html" class="cellMainLink">Grand Theft Auto Anthology [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="59059199217">55 <span>GB</span></td>
			<td class="center">123</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T07:14:04+00:00">04 Oct 2015, 07:14:04</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">349</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11359273,0" class="icommentjs icon16" href="/mortal-kombat-x-premium-edition-all-dlc-maxagent-repack-t11359273.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/mortal-kombat-x-premium-edition-all-dlc-maxagent-repack-t11359273.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mortal-kombat-x-premium-edition-all-dlc-maxagent-repack-t11359273.html" class="cellMainLink">Mortal Kombat X Premium Edition + All DLC - MAXAGENT Repack</a></div>
			</td>
			<td class="nobr center" data-sort="25847701720">24.07 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T13:41:30+00:00">04 Oct 2015, 13:41:30</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">285</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11362918,0" class="icommentjs icon16" href="/ravens-cry-digital-deluxe-edition-repack-by-maxagent-t11362918.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ravens-cry-digital-deluxe-edition-repack-by-maxagent-t11362918.html" class="cellMainLink">Ravens Cry Digital Deluxe Edition RePack By MAXAGENT</a></div>
			</td>
			<td class="nobr center" data-sort="10460488471">9.74 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T08:57:23+00:00">05 Oct 2015, 08:57:23</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11360262,0" class="icommentjs icon16" href="/sniper-elite-3-collector-s-edition-v-1-15a-all-dlc-maxagent-repack-t11360262.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/sniper-elite-3-collector-s-edition-v-1-15a-all-dlc-maxagent-repack-t11360262.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/sniper-elite-3-collector-s-edition-v-1-15a-all-dlc-maxagent-repack-t11360262.html" class="cellMainLink">Sniper Elite 3 Collector&#039;s Edition v.1.15a + All DLC - MAXAGENT Repack</a></div>
			</td>
			<td class="nobr center" data-sort="15037365213">14 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T18:13:43+00:00">04 Oct 2015, 18:13:43</span></td>
			<td class="green center">53</td>
			<td class="red lasttd center">103</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11360041,0" class="icommentjs icon16" href="/might-and-magic-heroes-vii-hotfix-codex-t11360041.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/might-and-magic-heroes-vii-hotfix-codex-t11360041.html" class="cellMainLink">Might and Magic Heroes VII Hotfix-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="325706049">310.62 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T17:15:14+00:00">04 Oct 2015, 17:15:14</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11357219,0" class="icommentjs icon16" href="/cross-of-the-dutchman-r-g-mechanics-t11357219.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/cross-of-the-dutchman-r-g-mechanics-t11357219.html" class="cellMainLink">Cross of the Dutchman [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="946340027">902.5 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T03:42:20+00:00">04 Oct 2015, 03:42:20</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11367436,0" class="icommentjs icon16" href="/assassin-s-creed-anthology-the-complete-edition-repack-by-corepack-t11367436.html#comment"> <em class="iconvalue">81</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/assassin-s-creed-anthology-the-complete-edition-repack-by-corepack-t11367436.html" class="cellMainLink">Assassin&#039;s Creed Anthology - The Complete Edition - RePack by CorePack</a></div>
			</td>
			<td class="nobr center" data-sort="57015842781">53.1 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T07:25:44+00:00">06 Oct 2015, 07:25:44</span></td>
			<td class="green center">5</td>
			<td class="red lasttd center">150</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366684,0" class="icommentjs icon16" href="/3dmgame-dragon-age-inquisition-update-1-10-incl-dlc-and-crack-cpy-t11366684.html#comment"> <em class="iconvalue">41</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/3dmgame-dragon-age-inquisition-update-1-10-incl-dlc-and-crack-cpy-t11366684.html" class="cellMainLink">3DMGAME-Dragon Age Inquisition Update 1-10 Incl DLC and Crack-CPY</a></div>
			</td>
			<td class="nobr center" data-sort="9267570472">8.63 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T03:31:39+00:00">06 Oct 2015, 03:31:39</span></td>
			<td class="green center">29</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11360890,0" class="icommentjs icon16" href="/darkest-dungeon-build-10907-t11360890.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/darkest-dungeon-build-10907-t11360890.html" class="cellMainLink">Darkest Dungeon - Build 10907</a></div>
			</td>
			<td class="nobr center" data-sort="1319020551">1.23 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T21:46:45+00:00">04 Oct 2015, 21:46:45</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">23</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11357877,0" class="icommentjs icon16" href="/hard-disk-sentinel-pro-4-60-10-build-7377-beta-patch-4realtorrentz-t11357877.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/hard-disk-sentinel-pro-4-60-10-build-7377-beta-patch-4realtorrentz-t11357877.html" class="cellMainLink">Hard Disk Sentinel Pro 4.60.10 Build 7377 Beta + Patch [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="21446767">20.45 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T06:31:23+00:00">04 Oct 2015, 06:31:23</span></td>
			<td class="green center">264</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11363162,0" class="icommentjs icon16" href="/hotspot-shield-vpn-elite-5-20-2-multilingual-patch-4realtorrentz-t11363162.html#comment"> <em class="iconvalue">30</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/hotspot-shield-vpn-elite-5-20-2-multilingual-patch-4realtorrentz-t11363162.html" class="cellMainLink">Hotspot Shield VPN Elite 5.20.2 Multilingual + Patch [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="28989485">27.65 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T10:26:37+00:00">05 Oct 2015, 10:26:37</span></td>
			<td class="green center">247</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11357828,0" class="icommentjs icon16" href="/kmspico-10-1-7-final-portable-4realtorrentz-t11357828.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/kmspico-10-1-7-final-portable-4realtorrentz-t11357828.html" class="cellMainLink">KMSpico 10.1.7 Final + Portable [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="7775034">7.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T06:07:54+00:00">04 Oct 2015, 06:07:54</span></td>
			<td class="green center">239</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11357530,0" class="icommentjs icon16" href="/microsoft-toolkit-2-6-beta-2-4realtorrentz-t11357530.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-toolkit-2-6-beta-2-4realtorrentz-t11357530.html" class="cellMainLink">Microsoft Toolkit 2.6 Beta 2 [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="56589543">53.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T04:55:23+00:00">04 Oct 2015, 04:55:23</span></td>
			<td class="green center">202</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11367355,0" class="icommentjs icon16" href="/pinnacle-studio-ultimate-v19-0-1-245-64-bit-32bit-content-pack-team-os-t11367355.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pinnacle-studio-ultimate-v19-0-1-245-64-bit-32bit-content-pack-team-os-t11367355.html" class="cellMainLink">Pinnacle Studio Ultimate v19.0.1.245 64 Bit + 32bit +Content Pack-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="7541308066">7.02 <span>GB</span></td>
			<td class="center">381</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T07:03:39+00:00">06 Oct 2015, 07:03:39</span></td>
			<td class="green center">88</td>
			<td class="red lasttd center">128</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11357847,0" class="icommentjs icon16" href="/kmsauto-net-2015-1-4-0-portable-4realtorrentz-t11357847.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/kmsauto-net-2015-1-4-0-portable-4realtorrentz-t11357847.html" class="cellMainLink">KMSAuto Net 2015 1.4.0 Portable [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="5499223">5.24 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T06:18:07+00:00">04 Oct 2015, 06:18:07</span></td>
			<td class="green center">125</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11368178,0" class="icommentjs icon16" href="/product-key-explorer-v3-8-9-0-portable-crack-4realtorrentz-t11368178.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/product-key-explorer-v3-8-9-0-portable-crack-4realtorrentz-t11368178.html" class="cellMainLink">Product Key Explorer v3.8.9.0 &amp; Portable + Crack [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="4354619">4.15 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T11:22:31+00:00">06 Oct 2015, 11:22:31</span></td>
			<td class="green center">103</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11368092,0" class="icommentjs icon16" href="/driver-toolkit-8-5-keys-4realtorrentz-t11368092.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/driver-toolkit-8-5-keys-4realtorrentz-t11368092.html" class="cellMainLink">Driver Toolkit 8.5 + Keys [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="2435999">2.32 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T10:52:42+00:00">06 Oct 2015, 10:52:42</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11365740,0" class="icommentjs icon16" href="/sample-logic-xosphere-kontakt-t11365740.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/sample-logic-xosphere-kontakt-t11365740.html" class="cellMainLink">Sample Logic Xosphere KONTAKT</a></div>
			</td>
			<td class="nobr center" data-sort="5194961450">4.84 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T22:09:46+00:00">05 Oct 2015, 22:09:46</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11365751,0" class="icommentjs icon16" href="/windows-10-x64-6in1-multi-6-esd-oct-2015-generation2-t11365751.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-x64-6in1-multi-6-esd-oct-2015-generation2-t11365751.html" class="cellMainLink">Windows 10 X64 6in1 MULTi-6 ESD Oct 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="4542090183">4.23 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T22:13:52+00:00">05 Oct 2015, 22:13:52</span></td>
			<td class="green center">40</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11367178,0" class="icommentjs icon16" href="/google-play-store-v5-9-12-mod-xpoz-t11367178.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/google-play-store-v5-9-12-mod-xpoz-t11367178.html" class="cellMainLink">Google Play Store v5.9.12 Mod-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="8053920">7.68 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T06:06:05+00:00">06 Oct 2015, 06:06:05</span></td>
			<td class="green center">60</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11363771,0" class="icommentjs icon16" href="/daz3d-poser-ro108004-i13-hot-mess-pose-collection-for-v4-v6-g2f-zip-t11363771.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-poser-ro108004-i13-hot-mess-pose-collection-for-v4-v6-g2f-zip-t11363771.html" class="cellMainLink">Daz3D Poser RO108004 - i13 Hot Mess Pose Collection for V4 V6 G2F.zip</a></div>
			</td>
			<td class="nobr center" data-sort="20342184">19.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T13:23:14+00:00">05 Oct 2015, 13:23:14</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">1</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11367414,0" class="icommentjs icon16" href="/vso-convertxtodvd-5-3-0-29-final-patch-s0ft4pc-t11367414.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/vso-convertxtodvd-5-3-0-29-final-patch-s0ft4pc-t11367414.html" class="cellMainLink">VSO ConvertXtoDVD 5.3.0.29 Final + Patch [S0ft4PC]</a></div>
			</td>
			<td class="nobr center" data-sort="36823915">35.12 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T07:19:44+00:00">06 Oct 2015, 07:19:44</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11363670,0" class="icommentjs icon16" href="/daz3d-poser-ro108557-z-cheeky-madam-expressions-for-g2f-v6-rar-t11363670.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-poser-ro108557-z-cheeky-madam-expressions-for-g2f-v6-rar-t11363670.html" class="cellMainLink">Daz3D Poser RO108557 - Z Cheeky Madam - Expressions for G2F V6.rar</a></div>
			</td>
			<td class="nobr center" data-sort="1411008">1.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T13:02:58+00:00">05 Oct 2015, 13:02:58</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">0</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11363692,0" class="icommentjs icon16" href="/daz3d-dz22844-i13-50-essential-poses-for-g3f-rar-t11363692.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-dz22844-i13-50-essential-poses-for-g3f-rar-t11363692.html" class="cellMainLink">Daz3D DZ22844 - i13 50 Essential Poses for G3F.rar</a></div>
			</td>
			<td class="nobr center" data-sort="1468081">1.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T13:06:56+00:00">05 Oct 2015, 13:06:56</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">1</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11359609,0" class="icommentjs icon16" href="/animerg-dragon-ball-super-13-720p-mkv-t11359609.html#comment"> <em class="iconvalue">35</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-13-720p-mkv-t11359609.html" class="cellMainLink">[AnimeRG] Dragon Ball Super - 13 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="470823583">449.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T15:12:34+00:00">04 Oct 2015, 15:12:34</span></td>
			<td class="green center">706</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-one-punch-man-01-raw-tx-1280x720-x264-aac-mp4-t11360376.html" class="cellMainLink">[Leopard-Raws] One-Punch Man - 01 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="484938231">462.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T18:40:03+00:00">04 Oct 2015, 18:40:03</span></td>
			<td class="green center">534</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11371817,0" class="icommentjs icon16" href="/horriblesubs-kekkai-sensen-12-720p-mkv-t11371817.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-kekkai-sensen-12-720p-mkv-t11371817.html" class="cellMainLink">[HorribleSubs] Kekkai Sensen - 12 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="1176968549">1.1 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T05:35:02+00:00">07 Oct 2015, 05:35:02</span></td>
			<td class="green center">278</td>
			<td class="red lasttd center">530</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kaerizaki-fansub-one-piece-712-french-subbed-hd-1280x720-mp4-t11358535.html" class="cellMainLink">[Kaerizaki-Fansub] One Piece 712 [French Subbed][HD_1280x720].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="294881285">281.22 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T09:47:38+00:00">04 Oct 2015, 09:47:38</span></td>
			<td class="green center">510</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11358456,0" class="icommentjs icon16" href="/animerg-one-piece-712-english-subbed-480p-kami-t11358456.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-piece-712-english-subbed-480p-kami-t11358456.html" class="cellMainLink">[AnimeRG] One Piece - 712 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="78028053">74.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T09:17:01+00:00">04 Oct 2015, 09:17:01</span></td>
			<td class="green center">122</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11363365,0" class="icommentjs icon16" href="/commie-one-punch-man-01-d103c797-mkv-t11363365.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-one-punch-man-01-d103c797-mkv-t11363365.html" class="cellMainLink">[Commie] One-Punch Man - 01 [D103C797].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="451577569">430.66 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T11:35:03+00:00">05 Oct 2015, 11:35:03</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-concrete-revolutio-01v2-4bae2f00-mkv-t11367766.html" class="cellMainLink">[Commie] Concrete Revolutio - 01v2 [4BAE2F00].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="475843957">453.8 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T09:15:03+00:00">06 Oct 2015, 09:15:03</span></td>
			<td class="green center">78</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11356770,0" class="icommentjs icon16" href="/ipunisher-shingeki-kyojin-chuugakkou-attack-on-titan-junior-high-01-720p-aac-mp4-t11356770.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/ipunisher-shingeki-kyojin-chuugakkou-attack-on-titan-junior-high-01-720p-aac-mp4-t11356770.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-shingeki-kyojin-chuugakkou-attack-on-titan-junior-high-01-720p-aac-mp4-t11356770.html" class="cellMainLink">[iPUNISHER] Shingeki! Kyojin Chuugakkou (Attack on Titan: Junior High) - 01 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="240113540">228.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T00:41:49+00:00">04 Oct 2015, 00:41:49</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11357590,0" class="icommentjs icon16" href="/one-piece-712-480p-engsub-iorchid-t11357590.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-712-480p-engsub-iorchid-t11357590.html" class="cellMainLink">One Piece - 712 [480p][EngSub]_iORcHiD</a></div>
			</td>
			<td class="nobr center" data-sort="106363250">101.44 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T05:16:07+00:00">04 Oct 2015, 05:16:07</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366750,0" class="icommentjs icon16" href="/deadfish-one-punch-man-01-720p-aac-mp4-t11366750.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-one-punch-man-01-720p-aac-mp4-t11366750.html" class="cellMainLink">[DeadFish] One Punch Man - 01 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="433051374">412.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T03:53:51+00:00">06 Oct 2015, 03:53:51</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11364588,0" class="icommentjs icon16" href="/animerg-sword-art-online-ii-01-24-complete-720p-dual-audio-jrr-t11364588.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-sword-art-online-ii-01-24-complete-720p-dual-audio-jrr-t11364588.html" class="cellMainLink">[AnimeRG] Sword Art Online II (01-24 Complete) [720p][Dual Audio][JRR]</a></div>
			</td>
			<td class="nobr center" data-sort="8892238902">8.28 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T16:41:32+00:00">05 Oct 2015, 16:41:32</span></td>
			<td class="green center">20</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-ace-of-the-diamond-second-season-27-f575f4fa-mkv-t11366662.html" class="cellMainLink">[Commie] Ace of the Diamond ~Second Season~ - 27 [F575F4FA].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="286254769">272.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T03:25:02+00:00">06 Oct 2015, 03:25:02</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-013-480p-engsub-iorchid-t11360164.html" class="cellMainLink">Dragon Ball Super - 013 [480p][EngSub]_iORcHiD</a></div>
			</td>
			<td class="nobr center" data-sort="106191304">101.27 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T17:48:04+00:00">04 Oct 2015, 17:48:04</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11363952,0" class="icommentjs icon16" href="/bakedfish-one-punch-man-01-720p-aac-mp4-t11363952.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-one-punch-man-01-720p-aac-mp4-t11363952.html" class="cellMainLink">[BakedFish] One Punch Man - 01 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="450745893">429.86 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T14:06:49+00:00">05 Oct 2015, 14:06:49</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">1</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/noobsubs-cross-ange-720p-blu-ray-8bit-aac-mp4-t11366100.html" class="cellMainLink">[NoobSubs] Cross Ange (720p Blu-ray 8bit AAC MP4)</a></div>
			</td>
			<td class="nobr center" data-sort="20097381020">18.72 <span>GB</span></td>
			<td class="center">112</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T00:05:55+00:00">06 Oct 2015, 00:05:55</span></td>
			<td class="green center">11</td>
			<td class="red lasttd center">26</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11358761,0" class="icommentjs icon16" href="/interpersonal-communication-everyday-encounters-8th-edition-2015-pdf-gooner-t11358761.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/interpersonal-communication-everyday-encounters-8th-edition-2015-pdf-gooner-t11358761.html" class="cellMainLink">Interpersonal Communication - Everyday Encounters - 8th Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="44648403">42.58 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T10:56:46+00:00">04 Oct 2015, 10:56:46</span></td>
			<td class="green center">310</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11368805,0" class="icommentjs icon16" href="/injustice-gods-among-us-year-four-023-2015-digital-son-of-ultron-empire-cbr-nem-t11368805.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/injustice-gods-among-us-year-four-023-2015-digital-son-of-ultron-empire-cbr-nem-t11368805.html" class="cellMainLink">Injustice - Gods Among Us - Year Four 023 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="22943984">21.88 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T14:21:32+00:00">06 Oct 2015, 14:21:32</span></td>
			<td class="green center">284</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11356882,0" class="icommentjs icon16" href="/automobile-magazines-bundle-october-4-2015-true-pdf-t11356882.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-bundle-october-4-2015-true-pdf-t11356882.html" class="cellMainLink">Automobile Magazines Bundle - October 4 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="477935638">455.79 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T01:36:15+00:00">04 Oct 2015, 01:36:15</span></td>
			<td class="green center">240</td>
			<td class="red lasttd center">104</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11357888,0" class="icommentjs icon16" href="/current-medical-diagnosis-and-treatment-2016-55e-truepdf-unitedvrg-t11357888.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/current-medical-diagnosis-and-treatment-2016-55e-truepdf-unitedvrg-t11357888.html" class="cellMainLink">CURRENT Medical Diagnosis and Treatment 2016 - 55E [TruePDF] [UnitedVRG]</a></div>
			</td>
			<td class="nobr center" data-sort="34864627">33.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T06:34:50+00:00">04 Oct 2015, 06:34:50</span></td>
			<td class="green center">250</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11369812,0" class="icommentjs icon16" href="/national-geographic-november-2015-usa-t11369812.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/national-geographic-november-2015-usa-t11369812.html" class="cellMainLink">National Geographic - November 2015 USA</a></div>
			</td>
			<td class="nobr center" data-sort="17674379">16.86 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T18:52:21+00:00">06 Oct 2015, 18:52:21</span></td>
			<td class="green center">172</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11358492,0" class="icommentjs icon16" href="/introduction-to-social-media-investigation-a-hands-on-approach-1st-edition-2015-pdf-gooner-t11358492.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/introduction-to-social-media-investigation-a-hands-on-approach-1st-edition-2015-pdf-gooner-t11358492.html" class="cellMainLink">Introduction to Social Media Investigation - A Hands-on Approach - 1st Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="62183208">59.3 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T09:31:50+00:00">04 Oct 2015, 09:31:50</span></td>
			<td class="green center">167</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11357353,0" class="icommentjs icon16" href="/tabloid-magazines-bundle-october-4-2015-true-pdf-t11357353.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/tabloid-magazines-bundle-october-4-2015-true-pdf-t11357353.html" class="cellMainLink">Tabloid Magazines Bundle - October 4 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="238825772">227.76 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T04:16:18+00:00">04 Oct 2015, 04:16:18</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366570,0" class="icommentjs icon16" href="/planet-of-the-apes-collection-all-publishers-1970-2015-nem-t11366570.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/planet-of-the-apes-collection-all-publishers-1970-2015-nem-t11366570.html" class="cellMainLink">Planet of the Apes Collection - All Publishers (1970-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="8846742544">8.24 <span>GB</span></td>
			<td class="center">295</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T02:55:19+00:00">06 Oct 2015, 02:55:19</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">93</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11370777,0" class="icommentjs icon16" href="/the-flash-tpbs-v01-v05-2012-2015-digital-zone-empire-nem-t11370777.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-flash-tpbs-v01-v05-2012-2015-digital-zone-empire-nem-t11370777.html" class="cellMainLink">The Flash TPBs (v01-v05) (2012-2015)) (Digital) (Zone-Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="1803078542">1.68 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T23:47:18+00:00">06 Oct 2015, 23:47:18</span></td>
			<td class="green center">49</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366590,0" class="icommentjs icon16" href="/amazing-spider-man-1-2015-scanbro-cbz-nem-t11366590.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/amazing-spider-man-1-2015-scanbro-cbz-nem-t11366590.html" class="cellMainLink">Amazing Spider-Man #1 (2015) (Scanbro).cbz (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="48007776">45.78 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T03:01:56+00:00">06 Oct 2015, 03:01:56</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11368060,0" class="icommentjs icon16" href="/asimov-isaac-et-al-tantalizing-locked-room-mysteries-epub-zeke23-t11368060.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/asimov-isaac-et-al-tantalizing-locked-room-mysteries-epub-zeke23-t11368060.html" class="cellMainLink">Asimov, Isaac, et al.-Tantalizing Locked Room Mysteries - epub - zeke23</a></div>
			</td>
			<td class="nobr center" data-sort="594528">580.59 <span>KB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T10:42:13+00:00">06 Oct 2015, 10:42:13</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11357171,0" class="icommentjs icon16" href="/motorcycle-magazines-october-4-2015-true-pdf-t11357171.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/motorcycle-magazines-october-4-2015-true-pdf-t11357171.html" class="cellMainLink">Motorcycle Magazines - October 4 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="213143008">203.27 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T03:23:11+00:00">04 Oct 2015, 03:23:11</span></td>
			<td class="green center">61</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11370722,0" class="icommentjs icon16" href="/flynn-vince-the-survivor-mitch-rapp-14-epub-zeke23-t11370722.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/flynn-vince-the-survivor-mitch-rapp-14-epub-zeke23-t11370722.html" class="cellMainLink">Flynn, Vince-The Survivor-(Mitch Rapp #14) - epub - zeke23</a></div>
			</td>
			<td class="nobr center" data-sort="1339095">1.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T23:31:38+00:00">06 Oct 2015, 23:31:38</span></td>
			<td class="green center">61</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11358585,0" class="icommentjs icon16" href="/intermediate-physics-for-medicine-and-biology-5th-edition-2015-pdf-gooner-t11358585.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/intermediate-physics-for-medicine-and-biology-5th-edition-2015-pdf-gooner-t11358585.html" class="cellMainLink">Intermediate Physics for Medicine and Biology - 5th Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="44901347">42.82 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T10:03:51+00:00">04 Oct 2015, 10:03:51</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366574,0" class="icommentjs icon16" href="/open-source-for-you-a-peek-at-the-latest-programming-languages-october-2015-cpul-t11366574.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/open-source-for-you-a-peek-at-the-latest-programming-languages-october-2015-cpul-t11366574.html" class="cellMainLink">Open Source For You - A Peek At The Latest Programming Languages (October 2015) [CPUL]</a></div>
			</td>
			<td class="nobr center" data-sort="14763533">14.08 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T02:56:33+00:00">06 Oct 2015, 02:56:33</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">1</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11359916,0" class="icommentjs icon16" href="/va-battle-of-the-blues-60-original-blues-classics-2014-flac-t11359916.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-battle-of-the-blues-60-original-blues-classics-2014-flac-t11359916.html" class="cellMainLink">VA - Battle of the Blues - 60 Original Blues Classics (2014) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1074648963">1 <span>GB</span></td>
			<td class="center">86</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T16:37:45+00:00">04 Oct 2015, 16:37:45</span></td>
			<td class="green center">221</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11364033,0" class="icommentjs icon16" href="/top-100-70-s-rock-albums-by-ultimateclassicrock-cd-s-51-75-flac-t11364033.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/top-100-70-s-rock-albums-by-ultimateclassicrock-cd-s-51-75-flac-t11364033.html" class="cellMainLink">Top 100 &#039;70&#039;s Rock Albums by UltimateClassicRock - CD&#039;s 51-75 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="7892043558">7.35 <span>GB</span></td>
			<td class="center">591</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T14:31:39+00:00">05 Oct 2015, 14:31:39</span></td>
			<td class="green center">105</td>
			<td class="red lasttd center">115</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/retro-dance-90-s-best-1990-1999-smg-t11363917.html" class="cellMainLink">Retro Dance 90&#039;s Best (1990-1999) - SMG</a></div>
			</td>
			<td class="nobr center" data-sort="19494091486">18.16 <span>GB</span></td>
			<td class="center">580</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T13:56:20+00:00">05 Oct 2015, 13:56:20</span></td>
			<td class="green center">89</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11368311,0" class="icommentjs icon16" href="/eric-clapton-slowhand-2012-24-96-hd-flac-t11368311.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/eric-clapton-slowhand-2012-24-96-hd-flac-t11368311.html" class="cellMainLink">Eric Clapton - Slowhand (2012) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="834654743">795.99 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T12:13:54+00:00">06 Oct 2015, 12:13:54</span></td>
			<td class="green center">92</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11358915,0" class="icommentjs icon16" href="/miles-davis-tutu-2011-24-192-hd-flac-t11358915.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/miles-davis-tutu-2011-24-192-hd-flac-t11358915.html" class="cellMainLink">Miles Davis - Tutu (2011) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1812109422">1.69 <span>GB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T11:38:56+00:00">04 Oct 2015, 11:38:56</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11360308,0" class="icommentjs icon16" href="/rolling-stones-60-s-uk-ep-collection-2011-24-88-hd-flac-t11360308.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rolling-stones-60-s-uk-ep-collection-2011-24-88-hd-flac-t11360308.html" class="cellMainLink">Rolling Stones - 60&#039;s UK EP Collection (2011) [24-88 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="584963085">557.86 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T18:26:12+00:00">04 Oct 2015, 18:26:12</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11364775,0" class="icommentjs icon16" href="/joe-bonamassa-live-at-radio-city-music-hall-2015-flac-t11364775.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/joe-bonamassa-live-at-radio-city-music-hall-2015-flac-t11364775.html" class="cellMainLink">Joe Bonamassa - Live at Radio City Music Hall (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="508182516">484.64 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T17:26:09+00:00">05 Oct 2015, 17:26:09</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11360758,0" class="icommentjs icon16" href="/sweet-action-the-ultimate-story-2015-flac-t11360758.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/sweet-action-the-ultimate-story-2015-flac-t11360758.html" class="cellMainLink">Sweet - Action The Ultimate Story (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1395306392">1.3 <span>GB</span></td>
			<td class="center">67</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T20:58:31+00:00">04 Oct 2015, 20:58:31</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11362913,0" class="icommentjs icon16" href="/don-henley-cass-county-deluxe-edition-2015-24bit-flac-pirate-shovon-t11362913.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/don-henley-cass-county-deluxe-edition-2015-24bit-flac-pirate-shovon-t11362913.html" class="cellMainLink">Don Henley - Cass County [Deluxe Edition] [2015] [24Bit FLAC] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="1495484495">1.39 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T08:54:49+00:00">05 Oct 2015, 08:54:49</span></td>
			<td class="green center">57</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11365012,0" class="icommentjs icon16" href="/david-bowie-full-studio-discography-1967-2013-bbm-flac-t11365012.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/david-bowie-full-studio-discography-1967-2013-bbm-flac-t11365012.html" class="cellMainLink">David Bowie - Full Studio Discography (1967 - 2013) BBM-FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="13901312404">12.95 <span>GB</span></td>
			<td class="center">333</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T18:19:21+00:00">05 Oct 2015, 18:19:21</span></td>
			<td class="green center">30</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11360043,0" class="icommentjs icon16" href="/queensrÃ¿che-condition-hÃ¼man-2015-flac-sn3h1t87-glodls-t11360043.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/queensrÃ¿che-condition-hÃ¼man-2015-flac-sn3h1t87-glodls-t11360043.html" class="cellMainLink">QueensrÃ¿che - Condition HÃ¼man [2015] [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="400862262">382.29 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T17:16:01+00:00">04 Oct 2015, 17:16:01</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11368786,0" class="icommentjs icon16" href="/texas-horns-blues-gotta-holda-me-2015-flac-t11368786.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/texas-horns-blues-gotta-holda-me-2015-flac-t11368786.html" class="cellMainLink">Texas Horns - Blues Gotta Holda Me (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="365500706">348.57 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T14:17:34+00:00">06 Oct 2015, 14:17:34</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11360970,0" class="icommentjs icon16" href="/mickey-hart-rolling-thunder-1972-flac-t11360970.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mickey-hart-rolling-thunder-1972-flac-t11360970.html" class="cellMainLink">Mickey Hart - Rolling Thunder (1972) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="231827609">221.09 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-04T22:28:44+00:00">04 Oct 2015, 22:28:44</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-q-music-top-500-van-90-s-6cd-2015-flac-t11367901.html" class="cellMainLink">VA - Q-Music Top 500 Van 90&#039;s [6CD] (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3121491991">2.91 <span>GB</span></td>
			<td class="center">147</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T09:56:43+00:00">06 Oct 2015, 09:56:43</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11365171,0" class="icommentjs icon16" href="/tamar-braxton-calling-all-lovers-deluxe-edition-2015-flac-sn3h1t87-glodls-t11365171.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tamar-braxton-calling-all-lovers-deluxe-edition-2015-flac-sn3h1t87-glodls-t11365171.html" class="cellMainLink">Tamar Braxton - Calling All Lovers (Deluxe Edition) [2015] [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="385250364">367.4 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-05T19:15:14+00:00">05 Oct 2015, 19:15:14</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">12</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/how-pirate-safely-and-stick-it-man/?unread=16973838">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				How to Pirate Safely and Stick It to The Man.
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/HighDeff/">HighDeff</a></span></span> <time class="timeago" datetime="2015-10-07T23:58:50+00:00">07 Oct 2015, 23:58</time></span>
	</li>
		<li>
		<a href="/community/show/post-threads-need-be-moved-or-trashed-v2-thread-108123/?unread=16973837">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Post Threads that Need to be Moved or Trashed V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Buffalo_Mercs/">Buffalo_Mercs</a></span></span> <time class="timeago" datetime="2015-10-07T23:58:36+00:00">07 Oct 2015, 23:58</time></span>
	</li>
		<li>
		<a href="/community/show/book-yaang3rs-bya-academic-it-engineering-fiction-non-fictio-thread-103734/?unread=16973836">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				BÃÃK Ð¯ANGÎRS[BÐ¯] : Academic, IT, Engineering, Fiction &amp; Non-Fiction Releases
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_2"><a class="plain" href="/user/ira-1969/">ira-1969</a></span></span> <time class="timeago" datetime="2015-10-07T23:58:27+00:00">07 Oct 2015, 23:58</time></span>
	</li>
		<li>
		<a href="/community/show/what-tv-show-are-you-watching-right-now-v3/?unread=16973834">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What TV show are you watching right now? V3
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/nlspearl/">nlspearl</a></span></span> <time class="timeago" datetime="2015-10-07T23:57:56+00:00">07 Oct 2015, 23:57</time></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=16973832">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/descending/">descending</a></span></span> <time class="timeago" datetime="2015-10-07T23:56:53+00:00">07 Oct 2015, 23:56</time></span>
	</li>
		<li>
		<a href="/community/show/music-requests-new-v2/?unread=16973831">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Music Requests - New (V2)
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/TrenxT/">TrenxT</a></span></span> <time class="timeago" datetime="2015-10-07T23:55:22+00:00">07 Oct 2015, 23:55</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-04-24T10:13:38+00:00">24 Apr 2015, 10:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/OptimusPr1me/post/free-fallin/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Free Fallin&#039;</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <time class="timeago" datetime="2015-10-07T10:10:04+00:00">07 Oct 2015, 10:10</time></span></li>
	<li><a href="/blog/olderthangod/post/kat-staff-on-the-loose/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Kat staff on the loose:</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <time class="timeago" datetime="2015-10-06T22:03:24+00:00">06 Oct 2015, 22:03</time></span></li>
	<li><a href="/blog/Dude./post/on-humor-wit/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> On Humor &amp; Wit.</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Dude./">Dude.</a> <time class="timeago" datetime="2015-10-06T18:25:14+00:00">06 Oct 2015, 18:25</time></span></li>
	<li><a href="/blog/Secludish/post/that-goat/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> That #*$#%@# Goat</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Secludish/">Secludish</a> <time class="timeago" datetime="2015-10-06T03:09:00+00:00">06 Oct 2015, 03:09</time></span></li>
	<li><a href="/blog/olderthangod/post/a-slight-mishap/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> A Slight Mishap:</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <time class="timeago" datetime="2015-10-05T20:30:06+00:00">05 Oct 2015, 20:30</time></span></li>
	<li><a href="/blog/Mr.White/post/look-mama-i-m-popular/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Look, mama, I&#039;m popular!</p></a><span class="explanation">by <a class="plain aclColor_11" href="/user/Mr.White/">Mr.White</a> <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/robocop%2Bfrench/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				robocop+french
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/hindi/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				hindi
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/real%20indian%20sex/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				real indian sex
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/ghostbuster/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ghostbuster
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/zz%20top%20greatest/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				zz top greatest
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/the%20winning%20way/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the winning way
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/incubus/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				incubus
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/bluebeam%20extreme/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				bluebeam extreme
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/the%20way%20back/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				The Way Back
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/company%20of%20heroes/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				company of heroes
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/videohive%203d/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				videohive 3d
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
