



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "611-8493441-5944537";
                var ue_id = "0XTAPCH2P500Q4GDED6J";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="0XTAPCH2P500Q4GDED6J" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-d94cc76f.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2467623394._CB300617431_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3693178366._CB298945358_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['d'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['551622094615'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4044276354._CB299069504_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"dad2b1806d14a36df9ad99953215c262cd2cc872",
"2016-01-24T18%3A02%3A36GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50244;
generic.days_to_midnight = 0.5815277695655823;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'd']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=551622094615;ord=551622094615?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;ord=551622094615?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;ord=551622094615?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=01-24&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59419068/?ref_=nv_nw_tn_1"
> Oscars: With PGA Win, âThe Big Shortâ Is Your New Best Picture Frontrunner
</a><br />
                        <span class="time">10 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59418533/?ref_=nv_nw_tn_2"
> Chris Rock Still Hosting Oscars; Monologue to Tackle #OscarsSoWhite
</a><br />
                        <span class="time">17 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59417292/?ref_=nv_nw_tn_3"
> Winter Storm Jonas Cuts Into Northeastern Box Office; âThe Revenantâ to Top Newcomers
</a><br />
                        <span class="time">23 January 2016 3:54 PM, UTC</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0405094/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTE5MTYzMzg4OV5BMl5BanBnXkFtZTYwNjAyMTg2._V1._SY315_CR40,0,410,315_CT10_.jpg",
            titleYears : "2006",
            rank : 55,
                    headline : "The Lives of Others"
    },
    nameAd : {
            clickThru : "/name/nm1212722/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BODE3Mzk4NDkyM15BMl5BanBnXkFtZTcwNjczMTg1Ng@@._V1._SX350_CR45,10,250,315_.jpg",
            rank : 84,
            headline : "Benedict Cumberbatch"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYhrI01mZ7Aw-NTH9SurPJgN_Kp9YlRnIbeLmT84yybC2M1CHRs9eEZHfoDYDbzdHSY3qCXpPec%0D%0AXMDgxZ2vkKr0gsYxrLjBSqG03LpDrafawGodM_osNKaUNEczoVQ9KueNShE1k-9Ny1S5EapeCSQ8%0D%0AKLgraBUYhEKExKrVcYcFJGSudIXg_j0cFbgSMLqd9gjHolVHC3pEG1NqVYnMuhb4Qg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYswk8X3hlLPSJV5dA7WZDsZ2FEDxBGLgunj8cjSVL0_EqT8xEZWekGYZEihu4OOuRcSlsf70bz%0D%0AhDne2tuPsVqbbNPBunAaQABm1t0HNjU_W3YxpVcdUmlX42JmzUSw7EugpeVaRv0280aPI3xIefpt%0D%0AqKAuctIBIC1tqOqrBJNIp_oK2WMCczPHmjPSI_9INa24%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYiAx1DfR-oLVBVsLlSoEnpmNgoEbwNPa0V9KKT4jI-O1wxw43ANeRflBpjsufVDJ7u2DxneA4z%0D%0AA3KzghKWeZe5vdrR5INj-9zxQ3m4uqvsf7kz_5CjkRDmeIQk5Hyy2PX5Z2-GRXDViys_BeWVnnpp%0D%0Ah6eerLZ07HRHYabWj7Wydx-9JxF5cSPPERvfb1JjuDoh%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=551622094615;ord=551622094615?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=551622094615;ord=551622094615?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2407904537?ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395402342&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2407904537" data-source="bylist" data-id="ls056131825" data-rid="0XTAPCH2P500Q4GDED6J" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." alt="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." src="http://ia.media-imdb.com/images/M/MV5BMTYwOTEzMDMzMl5BMl5BanBnXkFtZTgwNzExODIzNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwOTEzMDMzMl5BMl5BanBnXkFtZTgwNzExODIzNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." title="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." title="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt0944947/?ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395402342&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Game of Thrones" </a> </div> </div> <div class="secondary ellipsis"> Lannister Battle Banner Teaser </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1871033625?ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395402342&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1871033625" data-source="bylist" data-id="ls002653141" data-rid="0XTAPCH2P500Q4GDED6J" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." alt="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." src="http://ia.media-imdb.com/images/M/MV5BMjg3NDMxMDU2N15BMl5BanBnXkFtZTgwNTE3OTUyNzE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjg3NDMxMDU2N15BMl5BanBnXkFtZTgwNTE3OTUyNzE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." title="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." title="Beatrice Prior and Tobias Eaton venture into the world outside of the fence and are taken into protective custody by a mysterious agency known as the Bureau of Genetic Welfare." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3410834/?ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395402342&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > The Divergent Series: Allegiant </a> </div> </div> <div class="secondary ellipsis"> Latest Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1334162713?ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395402342&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1334162713" data-source="bylist" data-id="ls056131825" data-rid="0XTAPCH2P500Q4GDED6J" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Comedienne and writer Chelsea Handler discusses the topics of marriage, racism, Silicon Valley, and drugs." alt="Comedienne and writer Chelsea Handler discusses the topics of marriage, racism, Silicon Valley, and drugs." src="http://ia.media-imdb.com/images/M/MV5BNjQ1Mzc4ODU1Nl5BMl5BanBnXkFtZTgwNzUzMjA1NzE@._V1_SY298_CR123,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ1Mzc4ODU1Nl5BMl5BanBnXkFtZTgwNzUzMjA1NzE@._V1_SY298_CR123,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Comedienne and writer Chelsea Handler discusses the topics of marriage, racism, Silicon Valley, and drugs." title="Comedienne and writer Chelsea Handler discusses the topics of marriage, racism, Silicon Valley, and drugs." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Comedienne and writer Chelsea Handler discusses the topics of marriage, racism, Silicon Valley, and drugs." title="Comedienne and writer Chelsea Handler discusses the topics of marriage, racism, Silicon Valley, and drugs." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt5275894/?ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395402342&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Chelsea Does" </a> </div> </div> <div class="secondary ellipsis"> Latest Teaser </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395402342&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/sundance/video/?ref_=hm_sun_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Plugged In: The IMDb Studio at Sundance</h3> </a> </span> </span> <p class="blurb"><a href="/name/nm0000490/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk1">Spike Lee</a> explains why he's not changing his stance toward his Oscar boycott despite recent Academy changes to its rules. Plus, learn more about the movie <i><a href="/title/tt3068194/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk2">Love & Friendship</a></i>. Visit our <a href="/sundance/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk3">special section</a> for photos, videos, and more.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi361280793?ref_=hm_sun_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi361280793" data-source="bylist" data-id="ls073917212" data-rid="0XTAPCH2P500Q4GDED6J" data-type="playlist" class="video-colorbox" data-refsuffix="hm_sun" data-ref="hm_sun_i_1"> <img itemprop="image" class="pri_image" title="Spike Lee, who is debuting his new film 'Michael Jackson's Journey from Motown to Off the Wall' at the 2016 Sundance Film Festival, spoke with IMDb about the January 22 announcement that the Academy will be changing its rules to include more diversity in its voting process. Watch the video above to hear why Spike isn't changing his stance on boycotting the Oscars but why he commends the recent decision." alt="Spike Lee, who is debuting his new film 'Michael Jackson's Journey from Motown to Off the Wall' at the 2016 Sundance Film Festival, spoke with IMDb about the January 22 announcement that the Academy will be changing its rules to include more diversity in its voting process. Watch the video above to hear why Spike isn't changing his stance on boycotting the Oscars but why he commends the recent decision." src="http://ia.media-imdb.com/images/M/MV5BMjM2NzEzNDMzNl5BMl5BanBnXkFtZTgwODA4MTA4NzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM2NzEzNDMzNl5BMl5BanBnXkFtZTgwODA4MTA4NzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> <img alt="Spike Lee, who is debuting his new film 'Michael Jackson's Journey from Motown to Off the Wall' at the 2016 Sundance Film Festival, spoke with IMDb about the January 22 announcement that the Academy will be changing its rules to include more diversity in its voting process. Watch the video above to hear why Spike isn't changing his stance on boycotting the Oscars but why he commends the recent decision." title="Spike Lee, who is debuting his new film 'Michael Jackson's Journey from Motown to Off the Wall' at the 2016 Sundance Film Festival, spoke with IMDb about the January 22 announcement that the Academy will be changing its rules to include more diversity in its voting process. Watch the video above to hear why Spike isn't changing his stance on boycotting the Oscars but why he commends the recent decision." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Spike Lee, who is debuting his new film 'Michael Jackson's Journey from Motown to Off the Wall' at the 2016 Sundance Film Festival, spoke with IMDb about the January 22 announcement that the Academy will be changing its rules to include more diversity in its voting process. Watch the video above to hear why Spike isn't changing his stance on boycotting the Oscars but why he commends the recent decision." title="Spike Lee, who is debuting his new film 'Michael Jackson's Journey from Motown to Off the Wall' at the 2016 Sundance Film Festival, spoke with IMDb about the January 22 announcement that the Academy will be changing its rules to include more diversity in its voting process. Watch the video above to hear why Spike isn't changing his stance on boycotting the Oscars but why he commends the recent decision." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/video/?ref_=hm_sun_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > Spike Lee on Academy Changing Its Rules </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-2/?imageid=rm686875648&ref_=hm_sun_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Taika Waititi at event of The IMDb Studio (2015)" alt="Taika Waititi at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjQ0NTAzOTgzM15BMl5BanBnXkFtZTgwNjIzMjA4NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQ0NTAzOTgzM15BMl5BanBnXkFtZTgwNjIzMjA4NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-2/?ref_=hm_sun_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio - Day 2 Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi914928921?ref_=hm_sun_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi914928921" data-source="bylist" data-id="ls073917212" data-rid="0XTAPCH2P500Q4GDED6J" data-type="playlist" class="video-colorbox" data-refsuffix="hm_sun" data-ref="hm_sun_i_3"> <img itemprop="image" class="pri_image" title="Kate Beckinsale, Chloe Sevigny, and Director Whit Stillman speak about shooting their film 'Love & Friendship.' They also reveal their favorite behind-the-scenes moments that involved castmate Steven Fry. Find out who was the biggest prankster. Plus, see how they fare in the IMDb Snow Hat game!" alt="Kate Beckinsale, Chloe Sevigny, and Director Whit Stillman speak about shooting their film 'Love & Friendship.' They also reveal their favorite behind-the-scenes moments that involved castmate Steven Fry. Find out who was the biggest prankster. Plus, see how they fare in the IMDb Snow Hat game!" src="http://ia.media-imdb.com/images/M/MV5BMjA5OTA1NzU3MF5BMl5BanBnXkFtZTgwODMyMDA4NzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5OTA1NzU3MF5BMl5BanBnXkFtZTgwODMyMDA4NzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> <img alt="Kate Beckinsale, Chloe Sevigny, and Director Whit Stillman speak about shooting their film 'Love & Friendship.' They also reveal their favorite behind-the-scenes moments that involved castmate Steven Fry. Find out who was the biggest prankster. Plus, see how they fare in the IMDb Snow Hat game!" title="Kate Beckinsale, Chloe Sevigny, and Director Whit Stillman speak about shooting their film 'Love & Friendship.' They also reveal their favorite behind-the-scenes moments that involved castmate Steven Fry. Find out who was the biggest prankster. Plus, see how they fare in the IMDb Snow Hat game!" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Kate Beckinsale, Chloe Sevigny, and Director Whit Stillman speak about shooting their film 'Love & Friendship.' They also reveal their favorite behind-the-scenes moments that involved castmate Steven Fry. Find out who was the biggest prankster. Plus, see how they fare in the IMDb Snow Hat game!" title="Kate Beckinsale, Chloe Sevigny, and Director Whit Stillman speak about shooting their film 'Love & Friendship.' They also reveal their favorite behind-the-scenes moments that involved castmate Steven Fry. Find out who was the biggest prankster. Plus, see how they fare in the IMDb Snow Hat game!" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/video/?ref_=hm_sun_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > Chloe Sevigny, Kate Beckinsale on <i>Love & Friendship</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/sundance/video/?ref_=hm_sun_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395407622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch more video from The IMDb Studio at Sundance</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/producers-guild/?ref_=hm_hm_ac_pga_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>'Big Short,' "Thrones," and "Transparent" Win PGA Awards</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/producers-guild/?ref_=hm_hm_ac_pga_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Steve Carell in The Big Short (2015)" alt="Still of Steve Carell in The Big Short (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjY5NjY0MjQ1MV5BMl5BanBnXkFtZTgwMTU5NzM0NzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjY5NjY0MjQ1MV5BMl5BanBnXkFtZTgwMTU5NzM0NzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/producers-guild/?ref_=hm_hm_ac_pga_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Kit Harington and John Bradley in Game of Thrones (2011)" alt="Still of Kit Harington and John Bradley in Game of Thrones (2011)" src="http://ia.media-imdb.com/images/M/MV5BMTg1NzY3NjQ3Nl5BMl5BanBnXkFtZTgwNzc5ODU5NTE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg1NzY3NjQ3Nl5BMl5BanBnXkFtZTgwNzc5ODU5NTE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/producers-guild/?ref_=hm_hm_ac_pga_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Jeffrey Tambor in Transparent (2014)" alt="Still of Jeffrey Tambor in Transparent (2014)" src="http://ia.media-imdb.com/images/M/MV5BMjAxMDA1NjQ4N15BMl5BanBnXkFtZTgwMjYyOTU0NzE@._V1_SY201_CR33,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAxMDA1NjQ4N15BMl5BanBnXkFtZTgwMjYyOTU0NzE@._V1_SY201_CR33,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">The 2016 Producers Guild Awards were announced Saturday, Jan. 23, with <i><a href="/title/tt1596363/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ac_pga_lk1">The Big Short</a></i> taking the top movie prize. "<a href="/title/tt0944947/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ac_pga_lk2">Game of Thrones</a>" and "<a href="/title/tt3502262/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ac_pga_lk3">Transparent</a>" were Drama and Comedy winners in the television categories. Visit our <a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ac_pga_lk4">Awards Central section</a> for the <a href="/awards-central/producers-guild/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ac_pga_lk5">full list of winners</a> and <a href="/awards-central/producers-guild-awards-photos-2016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ac_pga_lk6">photos of the event</a>.</p> <p class="seemore"><a href="/awards-central/producers-guild/?ref_=hm_hm_ac_pga_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See the full list of winners</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59419068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59419068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Oscars: With PGA Win, âThe Big Shortâ Is Your New Best Picture Frontrunner</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>â<a href="/title/tt1596363?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">The Big Short</a>.âMichael B. Jordan said the words so matter-of-factly, so quickly on the heels of recounting the list of Producers Guild of America nominees for theatrical motion pictures, that no one in the Hyatt Century Plaza ballroom quite knew how to react. The spotlight was floating around the ...                                        <span class="nobr"><a href="/news/ni59419068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59418533?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Chris Rock Still Hosting Oscars; Monologue to Tackle #OscarsSoWhite</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59417292?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Winter Storm Jonas Cuts Into Northeastern Box Office; âThe Revenantâ to Top Newcomers</a>
    <div class="infobar">
            <span class="text-muted">23 January 2016 4:54 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59419465?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >'I'm Full of Spice and I Got a Great Rack': Tina Fey Returns to SNL as Sarah Palin to Explain Why She Belongs in Trump's Cabinet</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59418924?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Larry David to Host Saturday Night Live</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59419068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59419068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Oscars: With PGA Win, âThe Big Shortâ Is Your New Best Picture Frontrunner</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>â<a href="/title/tt1596363?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">The Big Short</a>.âMichael B. Jordan said the words so matter-of-factly, so quickly on the heels of recounting the list of Producers Guild of America nominees for theatrical motion pictures, that no one in the Hyatt Century Plaza ballroom quite knew how to react. The spotlight was floating around the ...                                        <span class="nobr"><a href="/news/ni59419068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59418533?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Chris Rock Still Hosting Oscars; Monologue to Tackle #OscarsSoWhite</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59417292?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Winter Storm Jonas Cuts Into Northeastern Box Office; âThe Revenantâ to Top Newcomers</a>
    <div class="infobar">
            <span class="text-muted">23 January 2016 4:54 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59419771?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Sundance: Matt Damon on Not Starring in âManchester By the Seaâ</a>
    <div class="infobar">
            <span class="text-muted">22 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59419740?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >New âWarcraftâ Trailer Gives Peek At This Summerâs 3-D Epic Battle</a>
    <div class="infobar">
            <span class="text-muted">34 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59419465?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTU3NzMwMDI2MF5BMl5BanBnXkFtZTcwNDk0MzcyNw@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59419465?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >'I'm Full of Spice and I Got a Great Rack': Tina Fey Returns to SNL as Sarah Palin to Explain Why She Belongs in Trump's Cabinet</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p>With <a href="/name/nm3126606?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Sarah Palin</a> back in the news, you had to wonder when <a href="/name/nm0275486?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Tina Fey</a> would return to <a href="/title/tt0072562?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Saturday Night Live</a> and reprise her impression of the former Alaska governor. (Aside from the time she did it when she hosted the show in December.) Well she did it again on this weekend's episode of <a href="/title/tt0072562?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">SNL</a>. And she ...                                        <span class="nobr"><a href="/news/ni59419465?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59418924?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Larry David to Host Saturday Night Live</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59417335?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Morgan Creek Plots 'The Exorcist' TV Series At Fox After Saying They Would "Never Ever Attempt To Remake" The Film</a>
    <div class="infobar">
            <span class="text-muted">23 January 2016 5:31 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Indiewire Television</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59419466?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >SNL Takes on the #OscarsSoWhite Controversy by Honoring 'All the White Guys' in Film</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59419765?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Ten Actresses with Incredibly Solid Athletic Coordination</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000222?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>TVovermind.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59418638?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTUwNDIyMDU2OF5BMl5BanBnXkFtZTYwMzQxMzI1._V1_SY150_CR2,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59418638?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >John Legend Opens Up About Diversity in the Academy: "It's a Bigger Problem Than the Oscars Themselves"</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm1775466?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">John Legend</a> is speaking out in regards to the lack of diversity amongst the 2016 Academy Awards. While appearing at the Sundance Film FestivalÂ this weekend, E! News caught up with the Oscar winner who voiced his approval at the Academy's latest goal ofÂ doubling the number of women and diverse ...                                        <span class="nobr"><a href="/news/ni59418638?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59417428?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Daniel Radcliffe and His Girlfriend Are the Coziest Couple at Sundance</a>
    <div class="infobar">
            <span class="text-muted">23 January 2016 6:40 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59419746?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Kim Kardashian & Kourtney Kardashian Are Twinning in Photo Posted 50 Days After Baby Saint West's Birth</a>
    <div class="infobar">
            <span class="text-muted">49 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59419744?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Matt Damon Calls Lack of Oscar Diversity 'Shameful and Embarrassing,' Commends 'Strong First Step'</a>
    <div class="infobar">
            <span class="text-muted">52 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59419745?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Lady Gaga Opens Up About Her Aunt's Sexual Assault: 'It Tormented Her' Before She Died</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg616602368?ref_=hm_snp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Photos We Love: Week of January 22</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3773490176/rg616602368?ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="DulÃ© Hill at event of The IMDb Studio (2015)" alt="DulÃ© Hill at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg1ODAxMjE0MV5BMl5BanBnXkFtZTgwNTg0NDk3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg1ODAxMjE0MV5BMl5BanBnXkFtZTgwNTg0NDk3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3009668864/rg616602368?ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Eden Sher" alt="Eden Sher" src="http://ia.media-imdb.com/images/M/MV5BMjQxNDUyMTI0MV5BMl5BanBnXkFtZTgwODYwMjQ3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQxNDUyMTI0MV5BMl5BanBnXkFtZTgwODYwMjQ3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm202499840/rg616602368?ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Paul Dano, Daniel Radcliffe, Daniel Scheinert, Dan Kwan and Mary Elizabeth" alt="Paul Dano, Daniel Radcliffe, Daniel Scheinert, Dan Kwan and Mary Elizabeth" src="http://ia.media-imdb.com/images/M/MV5BMjMyMTkxMzM2MF5BMl5BanBnXkFtZTgwNzIwMTk3NzE@._V1_SY201_CR45,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMyMTkxMzM2MF5BMl5BanBnXkFtZTgwNzIwMTk3NzE@._V1_SY201_CR45,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg616602368?ref_=hm_snp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See the gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=1-24&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0059215?ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Mischa Barton" alt="Mischa Barton" src="http://ia.media-imdb.com/images/M/MV5BOTIyNzYyMTA1MF5BMl5BanBnXkFtZTcwMDE4NjgyMQ@@._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTIyNzYyMTA1MF5BMl5BanBnXkFtZTcwMDE4NjgyMQ@@._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0059215?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Mischa Barton</a> (30) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1102891?ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Kristen Schaal" alt="Kristen Schaal" src="http://ia.media-imdb.com/images/M/MV5BMTIxNzAzNzUwM15BMl5BanBnXkFyZXN1bWU@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTIxNzAzNzUwM15BMl5BanBnXkFyZXN1bWU@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1102891?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Kristen Schaal</a> (38) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1682573?ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Justin Baldoni" alt="Justin Baldoni" src="http://ia.media-imdb.com/images/M/MV5BMTYwNDY3ODgyN15BMl5BanBnXkFtZTgwNjU5NjY0NzE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwNDY3ODgyN15BMl5BanBnXkFtZTgwNjU5NjY0NzE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1682573?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Justin Baldoni</a> (32) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000498?ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Matthew Lillard" alt="Matthew Lillard" src="http://ia.media-imdb.com/images/M/MV5BMjA2MzkyODc3MV5BMl5BanBnXkFtZTcwMjA0NTcyNw@@._V1_SY172_CR4,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA2MzkyODc3MV5BMl5BanBnXkFtZTcwMjA0NTcyNw@@._V1_SY172_CR4,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Matthew Lillard</a> (46) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1128340?ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ricardo Medina Jr." alt="Ricardo Medina Jr." src="http://ia.media-imdb.com/images/M/MV5BMjAxMTMwNzA4Ml5BMl5BanBnXkFtZTcwMDczNzQ0MQ@@._V1_SY172_CR95,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAxMTMwNzA4Ml5BMl5BanBnXkFtZTcwMDczNzQ0MQ@@._V1_SY172_CR95,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1128340?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Ricardo Medina Jr.</a> (37) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=1-24&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-6"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/offsite/?page-action=offsite-amazon&token=BCYoRUxiBXw_DrPEWynrCUAHbUO2CZTDbiQb-gv0rUBP6W5ClYOMb0fLh2EXp5Qm3zdxEe-wN-6U%0D%0Ak--6aVv5LeISAZD2aq0J4Lkb6XX2TeaGvXRDuRlsU_fQaH2SWReFWtVpDmpatGfXSzlEucfO2dcc%0D%0A6FTPNySMlkzI60f3xVstlZ2lEU3BXRysqejCsOV63xrdqVYzUIZu9GsM2dNT-uPqBf1BilJFV3p0%0D%0AFZ7Rjig7vwNKdP5cPm3ZwFkl5NQxdcwq%0D%0A&ref_=hm_pop_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Live From The IMDb Studio at Sundance</h3> </a> </span> </span> <p class="blurb">Join us live from The IMDb Studio at Sundance on Sunday, Jan. 24, at 8 p.m. ET/5 p.m. PT for a live conversation with <a href="/name/nm0000542/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk1">Penelope Miller</a>, star of <i><a href="/title/tt4196450/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk2">The Birth of a Nation</a></i>. We' ll also catch up with <a href="/name/nm0600667/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk3">Sheri Moon Zombie</a>, star of <i><a href="/title/tt3835080/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk4">31</a></i>, and director <a href="/name/nm0957772/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk5">Rob Zombie</a>. Tune in to watch, live chat, and even ask a question yourself at <a href="/offsite/?page-action=offsite-amazon&token=BCYrluFd_PQMuxJet3bhFtx507ZBr8inrwaMy9qrkj2kwtwIZzdJXQbuplZHFmzww25V4NfMUZJ7%0D%0A8lY7DfTRNGO_AWUDhp-1ZAUwaRqh0J2lz9kKj8nxOckWXwtds24VEFKYIHyNKcxVtfAIFkMlErJo%0D%0Anp5ogdHbqO7zk1oHzw-LKh1EoYjFQIRqvzc7_vAJKOITAvcSwdRBl2bSRt4Uoh9PPzStxJuk7jcK%0D%0Aw7y4aFJsXVY%0D%0A&ref_=hm_pop_lk6">Amazon.com/IMDbAsks</a>. This livestream is best viewed on laptops, desktops, and tablets.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYqUZZfqYU9c0LEf6g90h1h2dTecC6rtq8GcFLb-Awj6VuZbmrAqIBsDlfZdyepGDAiXVkQOGjN%0D%0Arp6R4Xq-OQRXjS1fFmm_XbgI2JvrAVXSPlKHp-KjtCYP5EBzUSq_G-uGeFCTUBxW_kbIDe07wZtI%0D%0AlcVhBP9YXlUmTQRDQCr_sXgKNEV7a5OYQuxuH01wcHtrXiSpdF3la2OItiTzWsjMLbCsflg9zc9_%0D%0AToNw94FGWLE%0D%0A&ref_=hm_pop_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjEzMDc3MDA4M15BMl5BanBnXkFtZTgwNzYzNDcyMDE@._UX170_CR0,0,170,250_SY250_SX170_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzMDc3MDA4M15BMl5BanBnXkFtZTgwNzYzNDcyMDE@._UX170_CR0,0,170,250_SY250_SX170_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYp_VdWsG-do4Uz-ESt5j07XNJFkIA2KaoaqrbMwy-BNa9frp6BCN6oywM_oLX7Ih0c7RdpN3Y9%0D%0A3EwZKJL1szKQw2-nwz2gNaxsUagc8BgfJkhOZuX8E3tAEGhO9Anpb5f0Z4BK_E4XrF_GCAb5Hg6a%0D%0AecWV1VZoMtTkXJAhL9NV5FlfjDAfX8L7aRKofuetetXJbyZH_5KSdmK6FaVOi5P5dt9FlcS9gYfQ%0D%0AShlAyaNnXtg%0D%0A&ref_=hm_pop_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BODAxMjY1ODk4M15BMl5BanBnXkFtZTgwMzg1Mzk3NzE@._UX444_CR0,60,444,250_SY250_SX444_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODAxMjY1ODk4M15BMl5BanBnXkFtZTgwMzg1Mzk3NzE@._UX444_CR0,60,444,250_SY250_SX444_AL_UY500_UX888_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/offsite/?page-action=offsite-amazon&token=BCYnFNwCaCL6U6KxUp4cEsytrD0849mVBW6myEYGLHK6NRiJ3R8vgfrsN4sJNtL3ARB6FJLQQZzB%0D%0ANwTgxqcBEb85W2cEFcXDMKjN9Up91f0qpVTpYhBBN_CCDc8-2ImIwwjSMTXN0zDGw2tbhE7AoiDy%0D%0AOWWvvXSK_gF5o98nSM8I4DnGK26Vwy8XnyN2JYdze7QXaPpJ1D5HlZ-vwFGDyYoUjqYGUxiFc7Y8%0D%0AJYlty7mTW6GHLqgvtkj9WdGhZ8Zidr5N%0D%0A&ref_=hm_pop_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394943682&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Tune in here for the one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Photos From the 2016 Sundance Film Festival</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-2/?imageid=rm2650727424&ref_=hm_sun_ph_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395391782&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jurnee Smollett-Bell and Jessica De Gouw at event of The IMDb Studio (2015)" alt="Jurnee Smollett-Bell and Jessica De Gouw at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjMwMjExMDU5NV5BMl5BanBnXkFtZTgwNjA3MTA4NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjMwMjExMDU5NV5BMl5BanBnXkFtZTgwNjA3MTA4NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-2/?ref_=hm_sun_ph_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395391782&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio at Sundance -- Day 2 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-1/?imageid=rm3823821824&ref_=hm_sun_ph_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395391782&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="AnnaLynne McCord at event of The IMDb Studio (2015)" alt="AnnaLynne McCord at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg0OTIzNjE5Nl5BMl5BanBnXkFtZTgwMTg0NDk3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0OTIzNjE5Nl5BMl5BanBnXkFtZTgwMTg0NDk3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-1/?ref_=hm_sun_ph_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395391782&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio at Sundance -- Day 1 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/premieres-and-parties/?imageid=rm1660609536&ref_=hm_sun_ph_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395391782&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="ChloÃ« Sevigny and Michael C. Hall" alt="ChloÃ« Sevigny and Michael C. Hall" src="http://ia.media-imdb.com/images/M/MV5BMjI4OTczMDE0OV5BMl5BanBnXkFtZTgwMzc0OTk3NzE@._V1_SY201_CR42,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4OTczMDE0OV5BMl5BanBnXkFtZTgwMzc0OTk3NzE@._V1_SY201_CR42,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/premieres-and-parties/?ref_=hm_sun_ph_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395391782&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Premieres and Parties </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>"The X-Files" Is Finally Back</h3> </span> </span> <p class="blurb">The truth is out there once again. Get ready for the premiere of "<a href="/title/tt4370492/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399042&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_xf_lk1">The X-Files</a>" miniseries by checking out the top-rated episodes of the <a href="/title/tt0106179/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399042&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_xf_lk2">original series</a>, photos of the show through the years, and the latest posters.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/x-files-top-24-episodes/ls031232115?ref_=hm_tv_xf_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399042&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Laurie Holden in The X-Files (1993)" alt="Still of Laurie Holden in The X-Files (1993)" src="http://ia.media-imdb.com/images/M/MV5BMTQ0NjM0OTQxOV5BMl5BanBnXkFtZTgwNDE1Nzc3MjE@._V1_SY201_CR34,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ0NjM0OTQxOV5BMl5BanBnXkFtZTgwNDE1Nzc3MjE@._V1_SY201_CR34,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/x-files-top-24-episodes/ls031232115?ref_=hm_tv_xf_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399042&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > Top-Rated "X-Files" Episodes </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/the-x-files-through-the-years?imageid=rm1045228800&ref_=hm_tv_xf_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399042&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Gillian Anderson and David Duchovny in The X-Files (2015)" alt="Still of Gillian Anderson and David Duchovny in The X-Files (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjI5NzMzMTYwN15BMl5BanBnXkFtZTgwNjQ1MDMwNzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI5NzMzMTYwN15BMl5BanBnXkFtZTgwNjQ1MDMwNzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/the-x-files-through-the-years/?ref_=hm_tv_xf_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399042&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > "The X-Files" Through the Years </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3612796672/rg4124023552?ref_=hm_tv_xf_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399042&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The X-Files (2015)" alt="The X-Files (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTkyNjkzMDI1N15BMl5BanBnXkFtZTgwOTY5NjY3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkyNjkzMDI1N15BMl5BanBnXkFtZTgwOTY5NjY3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/gallery/rg4124023552?ref_=hm_tv_xf_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395399042&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > New "X-Files" Posters </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-9"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/oscars-costume-design-makeup-hairstyling/?ref_=hm_ac_acd_cos_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395385982&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Oscars&#174; Style: Costumes, Makeup, and Hairstyling</h3> </a> </span> </span> <p class="blurb">Get a closer look at the work of the <a href="/awards-central/oscars/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395385982&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_cos_lk1">artists nominated</a> for Best Achievement in Costume Design and Best Achievement in Makeup and Hairstyling.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-costume-design-makeup-hairstyling/?imageid=rm3644711680&ref_=hm_ac_acd_cos_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395385982&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Eddie Redmayne in The Danish Girl (2015)" alt="Still of Eddie Redmayne in The Danish Girl (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUwMjE3OTU1OF5BMl5BanBnXkFtZTgwMjgwMzkyNzE@._V1_SY201_CR31,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwMjE3OTU1OF5BMl5BanBnXkFtZTgwMjgwMzkyNzE@._V1_SY201_CR31,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-costume-design-makeup-hairstyling/?imageid=rm1752758016&ref_=hm_ac_acd_cos_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395385982&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Nicholas Hoult in Mad Max: Fury Road (2015)" alt="Still of Nicholas Hoult in Mad Max: Fury Road (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTU5MTQwNDgzMl5BMl5BanBnXkFtZTgwMjE3NTE2NTE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU5MTQwNDgzMl5BMl5BanBnXkFtZTgwMjE3NTE2NTE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-costume-design-makeup-hairstyling/?imageid=rm2659837440&ref_=hm_ac_acd_cos_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395385982&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Rooney Mara in Carol (2015)" alt="Still of Rooney Mara in Carol (2015)" src="http://ia.media-imdb.com/images/M/MV5BODA4MzQ2ODQ5OF5BMl5BanBnXkFtZTgwMDE1MTE2NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODA4MzQ2ODQ5OF5BMl5BanBnXkFtZTgwMDE1MTE2NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/oscars-costume-design-makeup-hairstyling/?ref_=hm_ac_acd_cos_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395385982&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >View the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt2304933/trivia?item=tr2311891&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2304933?ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The 5th Wave (2016)" alt="The 5th Wave (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjQwOTc0Mzg3Nl5BMl5BanBnXkFtZTgwOTg3NjI2NzE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwOTc0Mzg3Nl5BMl5BanBnXkFtZTgwOTg3NjI2NzE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt2304933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">The 5th Wave</a></strong> <p class="blurb">Author Rick Yancey, who wrote the book on which the film is based, once worked for the Internal Revenue Service.</p> <p class="seemore"><a href="/title/tt2304933/trivia?item=tr2311891&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;XTJi_KwcF-c&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Tarantino's Bar & Restaurant</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Uma Thurman and John Travolta in Pulp Fiction (1994)" alt="Still of Uma Thurman and John Travolta in Pulp Fiction (1994)" src="http://ia.media-imdb.com/images/M/MV5BNzU4MDkzNzEwOV5BMl5BanBnXkFtZTcwNDAzNDY3Mw@@._V1_SY207_CR106,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzU4MDkzNzEwOV5BMl5BanBnXkFtZTcwNDAzNDY3Mw@@._V1_SY207_CR106,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Michael Fassbender and Diane Kruger in Inglourious Basterds (2009)" alt="Still of Michael Fassbender and Diane Kruger in Inglourious Basterds (2009)" src="http://ia.media-imdb.com/images/M/MV5BMjIwMTgxOTA5OF5BMl5BanBnXkFtZTcwODE0OTY3Mg@@._V1_SY207_CR85,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIwMTgxOTA5OF5BMl5BanBnXkFtZTcwODE0OTY3Mg@@._V1_SY207_CR85,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Tim Roth and Amanda Plummer in Pulp Fiction (1994)" alt="Still of Tim Roth and Amanda Plummer in Pulp Fiction (1994)" src="http://ia.media-imdb.com/images/M/MV5BMjM3NDUxNzQ3NV5BMl5BanBnXkFtZTgwNDAwMzg5MTE@._V1_SY207_CR84,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM3NDUxNzQ3NV5BMl5BanBnXkFtZTgwNDAwMzg5MTE@._V1_SY207_CR84,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ordell & Louis" alt="Ordell & Louis" src="http://ia.media-imdb.com/images/M/MV5BMjA4NTY0MzI3N15BMl5BanBnXkFtZTYwMjc0Mjg2._V1_SY207_CR68,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA4NTY0MzI3N15BMl5BanBnXkFtZTYwMjc0Mjg2._V1_SY207_CR68,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Steve Buscemi, Quentin Tarantino, Michael Madsen and Edward Bunker in Reservoir Dogs (1992)" alt="Still of Steve Buscemi, Quentin Tarantino, Michael Madsen and Edward Bunker in Reservoir Dogs (1992)" src="http://ia.media-imdb.com/images/M/MV5BODU3MDYwMTMyMF5BMl5BanBnXkFtZTgwMzg3OTEwMjE@._V1_SY207_CR81,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODU3MDYwMTMyMF5BMl5BanBnXkFtZTgwMzg3OTEwMjE@._V1_SY207_CR81,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Which of these restaurants/bars from the mind of Quentin Tarantino would you most like to visit? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/246553417?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/XTJi_KwcF-c/?ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390614602&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=551622094615;ord=551622094615?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;ord=551622094615?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=d;bpx=1;c=0;s=3075;s=32;ord=551622094615?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1860213"></div> <div class="title"> <a href="/title/tt1860213?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Dirty Grandpa </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2304933"></div> <div class="title"> <a href="/title/tt2304933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The 5th Wave </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3882082"></div> <div class="title"> <a href="/title/tt3882082?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> The Boy </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2888046"></div> <div class="title"> <a href="/title/tt2888046?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Ip Man 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2049543"></div> <div class="title"> <a href="/title/tt2049543?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Synchronicity </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3781476"></div> <div class="title"> <a href="/title/tt3781476?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Monster Hunt </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2322517"></div> <div class="title"> <a href="/title/tt2322517?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Mojave </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4374460"></div> <div class="title"> <a href="/title/tt4374460?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Aferim! </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4019560"></div> <div class="title"> <a href="/title/tt4019560?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t8"> Exposed </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390108482&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390109562&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1663202"></div> <div class="title"> <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Revenant </a> <span class="secondary-text">$16.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Star Wars: Episode VII - The Force Awakens </a> <span class="secondary-text">$14.3M</span> </div> <div class="action"> <a href="/showtimes/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2869728"></div> <div class="title"> <a href="/title/tt2869728?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Ride Along 2 </a> <span class="secondary-text">$13.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1860213"></div> <div class="title"> <a href="/title/tt1860213?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Dirty Grandpa </a> <span class="secondary-text">$11.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3882082"></div> <div class="title"> <a href="/title/tt3882082?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> The Boy </a> <span class="secondary-text">$11.3M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2267968"></div> <div class="title"> <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Kung Fu Panda 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2025690"></div> <div class="title"> <a href="/title/tt2025690?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The Finest Hours </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4667094"></div> <div class="title"> <a href="/title/tt4667094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Fifty Shades of Black </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2140037"></div> <div class="title"> <a href="/title/tt2140037?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Jane Got a Gun </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3396827429._CB298884168_.html#%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/oscars-carpet-all-time?ref_=hm_ac_acd_rcat_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394520622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Best Oscars&#174; Red Carpet Photos</h3> </a> </span> </span> <p class="blurb">Check out our gallery of our favorite Oscars&#174; red carpet photos of all time.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-carpet-all-time?imageid=rm3809054464&ref_=hm_ac_acd_rcat_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394520622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTM1MjMxMjI2MF5BMl5BanBnXkFtZTcwMTEzMTMyMw@@._UX307_CR0,10,307,230_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM1MjMxMjI2MF5BMl5BanBnXkFtZTcwMTEzMTMyMw@@._UX307_CR0,10,307,230_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/oscars-carpet-all-time?ref_=hm_ac_acd_rcat_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2394520622&pf_rd_r=0XTAPCH2P500Q4GDED6J&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYl-CgQUTImKF_uicZZHnK6DD-X7ZX9DC-GD0uIYv7OIG1dq761kOef5bs5BXP5vCgTIss1gjjz%0D%0AZppNXZ14PVG-n0PV2TW91Q43j8ymUkmPE86tJ6GVEA_LifuILELu1zXMh6htDLDpG5Av0tRgBO1s%0D%0Ag6LpZzfVXNZ76K20uIIiI4_rU43Iqdz_3DcHeR8gBPJgMT1nPU4yG-P6-0nsAkr2OvtQIDJLioOz%0D%0ASBcoGdRKQKE%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYrrsq9W62cxN_V6ttgfP-LqpqYNUxeh-gNFiyOYdp30CsfOkZGxDt7QLI_dG5rgZIkUxqp04ma%0D%0AQ8FaUjb1RsGjIAD9HhGEKAFOajmIzEau0IQ-Z-JE4Dd00LvaBvAi88nzrunil4dYt15cwIJna8kY%0D%0Ak3mMyLuIoMMuaH_XBRNOI7-Gswg8pP2gzRLNe0sscINV1AGm1OIhRDQ8w-ouLEAeOw%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYr_Q1mNwlNYsCBQ53IMtKv2JOQq-NcBb3Mm-ckRe9JXRhdjHd7L1epb-hQ_VTaOLpTFLi3Ezrx%0D%0AVMnHhZ2unI-k4we18pE3KAtAA1ZaQWO3hcg1fYmfwAv8QAvvbInfkAd44HtX8W0K3gjUJrOseUzJ%0D%0A4epPYIxT_hU2LVwXaOidOBpKJYAG15FSnGq5RLZtxJbihmJCul4tJnNh4M35zePyBHKntaP7ldQk%0D%0AhDlaRlb6KnQfGZMXmS5aZlAPPu-hCUcuD1RRTl2v1xMABv5Op8nB6rE-M9OgsCSlRgLKi_o9EZS6%0D%0AGjtRJYfe2ZqBATgAHbQ2oFTNQN3PzkrK2IVhybY5uifoM6q579ji5N0DQfrNCEFdfuo1jqFvJC1o%0D%0AHlnWSzX1B-K3t2o4Ystnzd-dP8ffBWzZOb5FewUReckVC93esQk%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYkT-LRqbBtTrXFsaugIcV5C620INeoVhHSjjH2m7Rtm2-hlQXudqkY3ln7PbSrqo8Ng1jYfIPn%0D%0A-yteXRP5eP8r2NeepD4-dnb79fmuI_d6ZKX3xiahLnCu6K0-HzhLKUPm3tOq--hPdje1bCpSFy_n%0D%0AsCXAcusPkfRjeYR88WB8Fw9mgXAqAjMFGvvSwxBmBxfwa8p0tV1jZ4PtwYfbD_MqdA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYpDBobJtbJOEIRec2aBYCXkhs_-1nlEMRuLDEFEsg6WlYCQb_qyArxPZr1zro8Rv2W1Q7b494x%0D%0AaO_1nol6g54cjj83xVAzbj4GfpBAWCw1e0-_VJyV4hY6-Vax5g73OMgPrb-qjuHmbi9C2_K9D5zQ%0D%0A9Jy0RIG3lybi4zSt1ZNavN9Qhz6JK9gU68Sa35v10wxo%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYhwfD_C1PFcF7y96fg-xpC_AQsS-_wJWM_mMvN6_pwEYJFCZiAOcdzzbqvNP9Nfno9dyBcVGhA%0D%0AMmdQIluwr3NChYdrRMEH6EZ-L9yI-hTVgHcv9vxG72sRkcRdLyoZPGWSJQqpkmmfjDMsXmc2WLoi%0D%0AmryKYRgaciYCpUJX2qmtZiABmL2wM7aiUnwl9v13twdK%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYjCczI9IZC8jTmzl13js-0NiW5jZQULZgIHgoJGQwFUic0a9o8t-OoX9_4R17pXuIfPAgALQ0H%0D%0ArL9_1tGc6KZsBmNtOSipf7tOS3VcCVI9N0L9wNvE23WUk6Ea6UzvpwjH5qTvG1xE0t4UBd96wTEd%0D%0AlOBm1Q_VbuTVap_i9RgjWUyJML95AtsuGSnNV3LySbAwk15PmZVCDLls68bxcjjcFQ%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYnmsD1z9osZsNXag6LofDI1Em_MYR4p1lWWZjeFgaUjMpa4-aBfShecjIFY4P8YitfMMQEyEQ6%0D%0ALkWGCqLruZibIufIiQW5j6YpoRobr8PsKAGp8BpbDNbrDOgAu2MuL1pE0i4yCmr9_AouH1v02yXG%0D%0AYsP1nMnL4K4ErM_choKYKjfbiujdhKFSlnh9G0sGLa44%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYmrRPh-g5ylfvUp7D6eBgIZbDvA4DOlYRaftGbOcfHXcoHb8nHbhSXh3HjfHFIVaILBDsuvbBZ%0D%0AGnTceXnHGiwKyB9U7KpqCF171orhgg8odtSvyEpRr1tfnE9Op7ekSD8fGTdyhlk3ewA3weE7AycL%0D%0AySMF4eu1ATurZD0Yaz4NV0eH_0RIRyPi0zgOCXSHmi4Nmv1cCEyAyaDJ8tPXl80oWE6ELZboGjuk%0D%0A1OAnMof69sZ2Yhf3PSasmEauF2dZLxRJ%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYjahI5ECRrnmUC_r-zljal8-vKkQJ_9IPSvOXMc_kr0YZlXkd8l4jcM365KC0cJ5XyAV5jRFyw%0D%0AvVWnwr-Wpq2_Fj2A2Hq-ui9lY8ycQ9u7YmKmqHfA1QKx1yt3NTOAPfGXFEzm148fsoDRbD7VkCsn%0D%0AaliNtGFxlktwiXypNVzssNYSCjgpRZ7I5X9-EF6J3bn9WWGdMo9ZPlcSvqlP2XLt4Q1ZUPk6b2fo%0D%0Ak-wBQ4u5TKY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYmBUzttFiwalxnuv5DKQv69ZFIExSh27PDv-J50OzbD3x13GRpd6r5zuEoj8EtfzjVuWdzha49%0D%0Ao1WMNGDrktDnzgA_1CvcIiewUtm0_x2IUxiBRLJ-6wZHXbu-Ij5TqcB0EoRVag1PNtQ85SW9BQn_%0D%0AfjEn5qK6Qj0A9tFueaVnrsK-FeYh9_pYgmot39BG-0-KKRcAkiYW2c2MLGOSy6u_9H8MsikPQzXi%0D%0AG0SokXELA48%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYuSYgno1h3ogh0tbYbrCo4U1FTREeZETT6fvU4lCuaQKkVkbV-ILDtC2dGGZcAz7yRpw-UaaD1%0D%0A8zFyuXM-AOHXbJvU4g1ET1NwABbd_Pm_c4MzVpsz7cVdgT-5bG1Z18DCyUPbFECfCqEA5b7fc7YE%0D%0AOgr-p98Fq_UWz00HFBkh9QtqDfOKVxGMfKbeNwka1nE9JZhCv_wJP-JKmvjubycFzwrdhvPC7Pa-%0D%0AAlUUd4gwUy4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYvmc59oe7vRKMAWMahpDuXXK1kqNeuCCYs7hWhfK1AJUJ7oGUEamUYCg4suJZWulTVw8eXGVz5%0D%0AzMe3PDT1-r97-89b3aM_UOSznvQtBF8mSYHyhCCcnnYOoROV212IZoEVCtkAmoYwWsdSvydRbofK%0D%0Aj0I9WX3qpAxDhGlyWlwfWuL1eqWpxbvDfznDMTIU07N8rWkdUy5B6atD6v9EcfIypUEz9JdkzByC%0D%0AkJbsXfSj9nM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYhWXK1gdzFkjkzFwqyzc9x4VkBdAF-oL3W2THDbpcUO5BnB9Qjyn2q5IgMga9q6wNopezGkSV6%0D%0AY2E-fN2uoFe9bU0HnvMjYDbhfvmBx2239JLGFV5Uh-wi7AUq3TOf_NK-_iGbsbZ1c_Gmy4UXrfmU%0D%0ATt_zBI859whAKPULYN-29yIVK9HVJt1ap1lLa1cUDe4eTCNmefvRT7tDUlIM3sNaQ6-EBYytWP9k%0D%0AOBAKMOlnHdE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYki4w6l1NCLIAHDZUkCk6UoLMUvq_2CJtshyNaZ9J-4zc9M2m6bXi623pd3B5mUL6yBhJWbA85%0D%0Aq-bsI5Ccthu_kufXP147fL3q5P6ITuyMns3kfAU3u4yR34IpUToX5o3de3fIH-mwVy29Amr-GM3Q%0D%0AlztBsNC3JzeEdZotQAdqqHqHB-0BzPyuY7Go-q5sYxA9WRlCJ9gzehpGJaeCM4JXqUbqx09JssU0%0D%0AtYhspGNk5Dvx4Q7gPLZBNZG6llaEs-6q%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYmcwXHmLg2vXkFmJsup6nOSKNGgHTZlDXtR7SotLtTVord9v2ibe3kNWjieNTQZvzYXha-6yUn%0D%0ACV1FtjlcbTqnrTgu_UPILPhBDatZuRk1VyRztubEFUb5rO8P2L4t_RLJBHTBJcSfN85O6GF8L613%0D%0AFh4vUZBysIw8gyJcWTHHybU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYsayM5UfswFjEvjJsPiHZAjikbkXYiYOoRWiImFabXk-UOpoSjnrxVnnXHMqYN0g_elzP-Gjq3%0D%0A5FlsY-JBSBpSoM3scYRoSGbgFfL254T3RhZfFf5Q7pD-AnIZHQmv_LBp3E53PKlwdKcvyC9JniSr%0D%0AghX-5wuJ1W-4tUFgKUkjoeg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-3617413113._CB300284919_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2635043045._CB299003638_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=01010ab1be50a1aaf620f2735c56a31dc1b886c9720b3a6da0189477dcdd5804251f",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=551622094615"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=551622094615&ord=551622094615";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="485"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
