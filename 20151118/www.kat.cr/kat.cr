<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-e55d263.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-e55d263.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-e55d263.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-e55d263.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-e55d263.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'e55d263',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-e55d263.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/blindspot/" class="tag3">blindspot</a>
	<a href="/search/blindspot%20s01e09/" class="tag2">blindspot s01e09</a>
	<a href="/search/christmas/" class="tag2">christmas</a>
	<a href="/search/czech%20streets/" class="tag3">czech streets</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/dual%20audio%20hindi/" class="tag2">dual audio hindi</a>
	<a href="/search/fallout%204/" class="tag3">fallout 4</a>
	<a href="/search/fargo/" class="tag3">fargo</a>
	<a href="/search/fargo%20s02e06/" class="tag2">fargo s02e06</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/game%20of%20thrones/" class="tag2">game of thrones</a>
	<a href="/search/gotham/" class="tag4">gotham</a>
	<a href="/search/gotham%20s02e09/" class="tag3">gotham s02e09</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/one%20punch%20man/" class="tag2">one punch man</a>
	<a href="/search/prem%20ratan%20dhan%20payo/" class="tag2">prem ratan dhan payo</a>
	<a href="/search/quantico/" class="tag2">quantico</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/scorpion/" class="tag2">scorpion</a>
	<a href="/search/spectre/" class="tag3">spectre</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/supergirl/" class="tag8">supergirl</a>
	<a href="/search/supergirl%20s01e04/" class="tag5">supergirl s01e04</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20flash/" class="tag2">the flash</a>
	<a href="/search/the%20martian/" class="tag2">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag7">the walking dead</a>
	<a href="/search/the%20walking%20dead%20s06/" class="tag2">the walking dead s06</a>
	<a href="/search/the%20walking%20dead%20s06e06/" class="tag2">the walking dead s06e06</a>
	<a href="/search/walking%20dead/" class="tag3">walking dead</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11601819,0" class="icommentjs kaButton smallButton rightButton" href="/maze-runner-the-scorch-trials-2015-dvdrip-xvid-ac3-etrg-t11601819.html#comment">73 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/maze-runner-the-scorch-trials-2015-dvdrip-xvid-ac3-etrg-t11601819.html" class="cellMainLink">Maze Runner The Scorch Trials 2015.DVDRip.XViD.AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1509876763">1.41 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T22:29:16+00:00">16 Nov 2015, 22:29:16</span></td>
			<td class="green center">5245</td>
			<td class="red lasttd center">6311</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11608084,0" class="icommentjs kaButton smallButton rightButton" href="/the-33-2015-hdrip-xvid-ac3-evo-t11608084.html#comment">28 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-33-2015-hdrip-xvid-ac3-evo-t11608084.html" class="cellMainLink">The 33 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1540154965">1.43 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T21:04:51+00:00">17 Nov 2015, 21:04:51</span></td>
			<td class="green center">3649</td>
			<td class="red lasttd center">7332</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590075,0" class="icommentjs kaButton smallButton rightButton" href="/love-2015-720p-webrip-aac2-0-h-264-gaspar-noe-t11590075.html#comment">53 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/love-2015-720p-webrip-aac2-0-h-264-gaspar-noe-t11590075.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/love-2015-720p-webrip-aac2-0-h-264-gaspar-noe-t11590075.html" class="cellMainLink">Love (2015) 720P WEBRip AAC2 0 H 264 (Gaspar Noe)</a></div>
			</td>
			<td class="nobr center" data-sort="1626731834">1.52 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T01:44:33+00:00">15 Nov 2015, 01:44:33</span></td>
			<td class="green center">4613</td>
			<td class="red lasttd center">3881</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603794,0" class="icommentjs kaButton smallButton rightButton" href="/90-minutes-in-heaven-2015-hdrip-xvid-etrg-t11603794.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/90-minutes-in-heaven-2015-hdrip-xvid-etrg-t11603794.html" class="cellMainLink">90 Minutes in Heaven 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="740809059">706.49 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T08:25:37+00:00">17 Nov 2015, 08:25:37</span></td>
			<td class="green center">3611</td>
			<td class="red lasttd center">4216</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605575,0" class="icommentjs kaButton smallButton rightButton" href="/kohinoor-2015-malayalam-dvdrip-x264-1cd-700mb-esubs-t11605575.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kohinoor-2015-malayalam-dvdrip-x264-1cd-700mb-esubs-t11605575.html" class="cellMainLink">Kohinoor [2015] Malayalam DVDRip x264 1CD 700MB ESubs</a></div>
			</td>
			<td class="nobr center" data-sort="731814255">697.91 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T16:18:02+00:00">17 Nov 2015, 16:18:02</span></td>
			<td class="green center">2723</td>
			<td class="red lasttd center">2777</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592908,0" class="icommentjs kaButton smallButton rightButton" href="/hitman-agent-47-2015-dvdrip-xvid-etrg-t11592908.html#comment">65 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hitman-agent-47-2015-dvdrip-xvid-etrg-t11592908.html" class="cellMainLink">Hitman Agent 47 2015 DVDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="739794661">705.52 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T14:55:30+00:00">15 Nov 2015, 14:55:30</span></td>
			<td class="green center">2912</td>
			<td class="red lasttd center">2012</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11609339,0" class="icommentjs kaButton smallButton rightButton" href="/007-spectre-2015-hdts-x264-ac3-cpg-t11609339.html#comment">28 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/007-spectre-2015-hdts-x264-ac3-cpg-t11609339.html" class="cellMainLink">007 Spectre 2015 HDTS X264 AC3-CPG</a></div>
			</td>
			<td class="nobr center" data-sort="1996923649">1.86 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T04:27:11+00:00">18 Nov 2015, 04:27:11</span></td>
			<td class="green center">914</td>
			<td class="red lasttd center">3722</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594344,0" class="icommentjs kaButton smallButton rightButton" href="/landmine-goes-click-2015-hdrip-xvid-etrg-t11594344.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/landmine-goes-click-2015-hdrip-xvid-etrg-t11594344.html" class="cellMainLink">Landmine Goes Click 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="747193401">712.58 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:28:13+00:00">15 Nov 2015, 20:28:13</span></td>
			<td class="green center">1877</td>
			<td class="red lasttd center">1409</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605398,0" class="icommentjs kaButton smallButton rightButton" href="/ant-man-spanish-espaÃ±ol-hdrip-xvid-elitetorrent-t11605398.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ant-man-spanish-espaÃ±ol-hdrip-xvid-elitetorrent-t11605398.html" class="cellMainLink">Ant-Man SPANiSH ESPAÃ±OL HDRip XviD-ELiTETORRENT</a></div>
			</td>
			<td class="nobr center" data-sort="2099193869">1.96 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T15:34:19+00:00">17 Nov 2015, 15:34:19</span></td>
			<td class="green center">1796</td>
			<td class="red lasttd center">1309</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605892,0" class="icommentjs kaButton smallButton rightButton" href="/ted-2-2015-brrip-xvid-etrg-t11605892.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ted-2-2015-brrip-xvid-etrg-t11605892.html" class="cellMainLink">Ted 2 2015 BRRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="742152974">707.77 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T17:31:42+00:00">17 Nov 2015, 17:31:42</span></td>
			<td class="green center">1305</td>
			<td class="red lasttd center">2102</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600856,0" class="icommentjs kaButton smallButton rightButton" href="/rasputin-2015-malayalam-dvdrip-x264-1cd-700mb-esubs-t11600856.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/rasputin-2015-malayalam-dvdrip-x264-1cd-700mb-esubs-t11600856.html" class="cellMainLink">Rasputin [2015] Malayalam DVDRip x264 1CD 700MB ESubs</a></div>
			</td>
			<td class="nobr center" data-sort="732080084">698.17 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T18:00:41+00:00">16 Nov 2015, 18:00:41</span></td>
			<td class="green center">1575</td>
			<td class="red lasttd center">1009</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11604056,0" class="icommentjs kaButton smallButton rightButton" href="/flutter-2015-hdrip-xvid-ac3-evo-t11604056.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/flutter-2015-hdrip-xvid-ac3-evo-t11604056.html" class="cellMainLink">Flutter 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1488509610">1.39 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T09:37:35+00:00">17 Nov 2015, 09:37:35</span></td>
			<td class="green center">949</td>
			<td class="red lasttd center">956</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11588979,0" class="icommentjs kaButton smallButton rightButton" href="/national-lampoons-van-wilder-unrated-2002-1080p-bluray-x264-aac-etrg-t11588979.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/national-lampoons-van-wilder-unrated-2002-1080p-bluray-x264-aac-etrg-t11588979.html" class="cellMainLink">National Lampoons Van Wilder Unrated 2002 1080p BluRay x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1476815332">1.38 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:33:54+00:00">14 Nov 2015, 18:33:54</span></td>
			<td class="green center">1015</td>
			<td class="red lasttd center">683</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11597754,0" class="icommentjs kaButton smallButton rightButton" href="/if-there-be-thorns-2015-hdrip-xvid-ac3-evo-t11597754.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/if-there-be-thorns-2015-hdrip-xvid-ac3-evo-t11597754.html" class="cellMainLink">If There Be Thorns 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1484306695">1.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T10:53:37+00:00">16 Nov 2015, 10:53:37</span></td>
			<td class="green center">787</td>
			<td class="red lasttd center">723</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11609345,0" class="icommentjs kaButton smallButton rightButton" href="/mississippi-grind-2015-brrip-xvid-ac3-evo-t11609345.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mississippi-grind-2015-brrip-xvid-ac3-evo-t11609345.html" class="cellMainLink">Mississippi Grind 2015 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1494264472">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T04:31:11+00:00">18 Nov 2015, 04:31:11</span></td>
			<td class="green center">405</td>
			<td class="red lasttd center">1094</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11608827,0" class="icommentjs kaButton smallButton rightButton" href="/the-flash-2014-s02e07-hdtv-x264-lol-ettv-t11608827.html#comment">218 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-flash-2014-s02e07-hdtv-x264-lol-ettv-t11608827.html" class="cellMainLink">The Flash 2014 S02E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="244337686">233.02 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T02:00:17+00:00">18 Nov 2015, 02:00:17</span></td>
			<td class="green center">26212</td>
			<td class="red lasttd center">12166</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11608642,0" class="icommentjs kaButton smallButton rightButton" href="/limitless-s01e09-hdtv-x264-lol-ettv-t11608642.html#comment">66 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/limitless-s01e09-hdtv-x264-lol-ettv-t11608642.html" class="cellMainLink">Limitless S01E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="347796627">331.68 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T01:01:25+00:00">18 Nov 2015, 01:01:25</span></td>
			<td class="green center">12001</td>
			<td class="red lasttd center">6128</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602387,0" class="icommentjs kaButton smallButton rightButton" href="/gotham-s02e09-hdtv-x264-lol-ettv-t11602387.html#comment">183 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gotham-s02e09-hdtv-x264-lol-ettv-t11602387.html" class="cellMainLink">Gotham S02E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="243121921">231.86 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T01:59:21+00:00">17 Nov 2015, 01:59:21</span></td>
			<td class="green center">11622</td>
			<td class="red lasttd center">1763</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602391,0" class="icommentjs kaButton smallButton rightButton" href="/supergirl-s01e05-hdtv-x264-lol-ettv-t11602391.html#comment">194 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supergirl-s01e05-hdtv-x264-lol-ettv-t11602391.html" class="cellMainLink">Supergirl S01E05 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="306836370">292.62 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T02:00:19+00:00">17 Nov 2015, 02:00:19</span></td>
			<td class="green center">10256</td>
			<td class="red lasttd center">2321</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11609063,0" class="icommentjs kaButton smallButton rightButton" href="/marvels-agents-of-s-h-i-e-l-d-s03e08-hdtv-x264-fleet-rartv-t11609063.html#comment">57 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/marvels-agents-of-s-h-i-e-l-d-s03e08-hdtv-x264-fleet-rartv-t11609063.html" class="cellMainLink">Marvels Agents of S H I E L D S03E08 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="277884905">265.01 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T03:01:33+00:00">18 Nov 2015, 03:01:33</span></td>
			<td class="green center">8070</td>
			<td class="red lasttd center">3752</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602592,0" class="icommentjs kaButton smallButton rightButton" href="/blindspot-s01e09-hdtv-x264-lol-ettv-t11602592.html#comment">116 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blindspot-s01e09-hdtv-x264-lol-ettv-t11602592.html" class="cellMainLink">Blindspot S01E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="296287258">282.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T03:00:15+00:00">17 Nov 2015, 03:00:15</span></td>
			<td class="green center">7409</td>
			<td class="red lasttd center">1453</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596170,0" class="icommentjs kaButton smallButton rightButton" href="/quantico-s01e08-hdtv-x264-lol-ettv-t11596170.html#comment">127 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/quantico-s01e08-hdtv-x264-lol-ettv-t11596170.html" class="cellMainLink">Quantico S01E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="274357200">261.65 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T03:00:13+00:00">16 Nov 2015, 03:00:13</span></td>
			<td class="green center">5618</td>
			<td class="red lasttd center">938</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596057,0" class="icommentjs kaButton smallButton rightButton" href="/homeland-s05e07-web-dl-x264-fum-ettv-t11596057.html#comment">68 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/homeland-s05e07-web-dl-x264-fum-ettv-t11596057.html" class="cellMainLink">Homeland S05E07 WEB-DL x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="302535274">288.52 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:12:18+00:00">16 Nov 2015, 02:12:18</span></td>
			<td class="green center">5517</td>
			<td class="red lasttd center">462</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602570,0" class="icommentjs kaButton smallButton rightButton" href="/scorpion-s02e09-hdtv-x264-lol-ettv-t11602570.html#comment">60 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scorpion-s02e09-hdtv-x264-lol-ettv-t11602570.html" class="cellMainLink">Scorpion S02E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="296675058">282.93 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T02:58:16+00:00">17 Nov 2015, 02:58:16</span></td>
			<td class="green center">4425</td>
			<td class="red lasttd center">837</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596035,0" class="icommentjs kaButton smallButton rightButton" href="/once-upon-a-time-s05e08-hdtv-x264-killers-ettv-t11596035.html#comment">62 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/once-upon-a-time-s05e08-hdtv-x264-killers-ettv-t11596035.html" class="cellMainLink">Once Upon a Time S05E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="325260071">310.19 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:04:15+00:00">16 Nov 2015, 02:04:15</span></td>
			<td class="green center">3649</td>
			<td class="red lasttd center">488</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11609062,0" class="icommentjs kaButton smallButton rightButton" href="/izombie-s02e07-hdtv-x264-lol-ettv-t11609062.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/izombie-s02e07-hdtv-x264-lol-ettv-t11609062.html" class="cellMainLink">iZombie S02E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="249115399">237.57 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T03:01:17+00:00">18 Nov 2015, 03:01:17</span></td>
			<td class="green center">3221</td>
			<td class="red lasttd center">1191</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603033,0" class="icommentjs kaButton smallButton rightButton" href="/fargo-s02e06-internal-hdtv-x264-batv-ettv-t11603033.html#comment">60 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fargo-s02e06-internal-hdtv-x264-batv-ettv-t11603033.html" class="cellMainLink">Fargo S02E06 INTERNAL HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="265359924">253.07 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T05:19:33+00:00">17 Nov 2015, 05:19:33</span></td>
			<td class="green center">3560</td>
			<td class="red lasttd center">506</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602405,0" class="icommentjs kaButton smallButton rightButton" href="/castle-2009-s08e07-hdtv-x264-lol-ettv-t11602405.html#comment">39 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/castle-2009-s08e07-hdtv-x264-lol-ettv-t11602405.html" class="cellMainLink">Castle 2009 S08E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="287924163">274.59 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T02:03:18+00:00">17 Nov 2015, 02:03:18</span></td>
			<td class="green center">3338</td>
			<td class="red lasttd center">588</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590966,0" class="icommentjs kaButton smallButton rightButton" href="/ufc-193-rousey-vs-holm-ppv-webrip-x264-jkkk-sparrow-t11590966.html#comment">125 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-193-rousey-vs-holm-ppv-webrip-x264-jkkk-sparrow-t11590966.html" class="cellMainLink">UFC 193 Rousey vs Holm PPV WEBRip x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1946674772">1.81 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T06:46:41+00:00">15 Nov 2015, 06:46:41</span></td>
			<td class="green center">3121</td>
			<td class="red lasttd center">478</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11608820,0" class="icommentjs kaButton smallButton rightButton" href="/ncis-s13e09-hdtv-x264-lol-ettv-t11608820.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ncis-s13e09-hdtv-x264-lol-ettv-t11608820.html" class="cellMainLink">NCIS S13E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="241755464">230.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T01:58:20+00:00">18 Nov 2015, 01:58:20</span></td>
			<td class="green center">2664</td>
			<td class="red lasttd center">805</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592127,0" class="icommentjs kaButton smallButton rightButton" href="/various-artists-100-beautiful-songs-320kbps-mp3-t11592127.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-100-beautiful-songs-320kbps-mp3-t11592127.html" class="cellMainLink">Various Artists - 100 Beautiful Songs - 320Kbps MP3</a></div>
			</td>
			<td class="nobr center" data-sort="1050514824">1001.85 <span>MB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T11:39:44+00:00">15 Nov 2015, 11:39:44</span></td>
			<td class="green center">818</td>
			<td class="red lasttd center">359</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594292,0" class="icommentjs kaButton smallButton rightButton" href="/va-mega-hits-best-of-2015-2015-mp3-vbr-sn3h1t87-glodls-t11594292.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-mega-hits-best-of-2015-2015-mp3-vbr-sn3h1t87-glodls-t11594292.html" class="cellMainLink">VA - Mega Hits - Best Of 2015 [2015] [MP3-VBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="263708109">251.49 <span>MB</span></td>
			<td class="center">41</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:15:07+00:00">15 Nov 2015, 20:15:07</span></td>
			<td class="green center">392</td>
			<td class="red lasttd center">131</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11597936,0" class="icommentjs kaButton smallButton rightButton" href="/va-need-for-speed-100-hits-320kbps-mp3-t11597936.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-need-for-speed-100-hits-320kbps-mp3-t11597936.html" class="cellMainLink">VA - Need For Speed (100 Hits) - 320Kbps MP3</a></div>
			</td>
			<td class="nobr center" data-sort="1154522656">1.08 <span>GB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T11:42:19+00:00">16 Nov 2015, 11:42:19</span></td>
			<td class="green center">374</td>
			<td class="red lasttd center">139</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603843,0" class="icommentjs kaButton smallButton rightButton" href="/va-now-thats-what-i-call-country-christmas-2015-2cd-mp3-320kbps-sn3h1t87-glodls-t11603843.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-thats-what-i-call-country-christmas-2015-2cd-mp3-320kbps-sn3h1t87-glodls-t11603843.html" class="cellMainLink">VA - Now Thats What I Call Country Christmas [2015] [2CD] [MP3-320Kbps] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="260642540">248.57 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T08:33:18+00:00">17 Nov 2015, 08:33:18</span></td>
			<td class="green center">370</td>
			<td class="red lasttd center">110</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11604439,0" class="icommentjs kaButton smallButton rightButton" href="/adele-when-we-were-young-live-at-the-church-single-luman-t11604439.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/adele-when-we-were-young-live-at-the-church-single-luman-t11604439.html" class="cellMainLink">Adele - When We Were Young (Live at The Church) - Single [luman]</a></div>
			</td>
			<td class="nobr center" data-sort="9783158">9.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T11:10:22+00:00">17 Nov 2015, 11:10:22</span></td>
			<td class="green center">372</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11599026,0" class="icommentjs kaButton smallButton rightButton" href="/va-the-seventies-album-3cd-2015-mp3-320kbps-sn3h1t87-glodls-t11599026.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-seventies-album-3cd-2015-mp3-320kbps-sn3h1t87-glodls-t11599026.html" class="cellMainLink">VA - The Seventies Album (3CD) [2015] [MP3-320Kbps] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="547858677">522.48 <span>MB</span></td>
			<td class="center">68</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T14:13:50+00:00">16 Nov 2015, 14:13:50</span></td>
			<td class="green center">329</td>
			<td class="red lasttd center">85</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11595274,0" class="icommentjs kaButton smallButton rightButton" href="/acoustic-blues-mp3-divxtotal-t11595274.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/acoustic-blues-mp3-divxtotal-t11595274.html" class="cellMainLink">Acoustic Blues MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="357304804">340.75 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T23:55:20+00:00">15 Nov 2015, 23:55:20</span></td>
			<td class="green center">203</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594349,0" class="icommentjs kaButton smallButton rightButton" href="/va-538-hitzone-christmas-2015-2cd-mp3-320kbps-sn3h1t87-glodls-t11594349.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-538-hitzone-christmas-2015-2cd-mp3-320kbps-sn3h1t87-glodls-t11594349.html" class="cellMainLink">VA - 538 Hitzone Christmas [2015] (2CD) [MP3-320Kbps] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="409201438">390.24 <span>MB</span></td>
			<td class="center">49</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:30:15+00:00">15 Nov 2015, 20:30:15</span></td>
			<td class="green center">204</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589064,0" class="icommentjs kaButton smallButton rightButton" href="/neil-young-bluenote-cafe-2015-freak37-t11589064.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/neil-young-bluenote-cafe-2015-freak37-t11589064.html" class="cellMainLink">Neil Young â Bluenote Cafe (2015)...Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="352423848">336.1 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:57:40+00:00">14 Nov 2015, 18:57:40</span></td>
			<td class="green center">212</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600009,0" class="icommentjs kaButton smallButton rightButton" href="/train-christmas-in-tahoe-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11600009.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/train-christmas-in-tahoe-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11600009.html" class="cellMainLink">Train - Christmas in Tahoe [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="126861655">120.98 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T14:56:43+00:00">16 Nov 2015, 14:56:43</span></td>
			<td class="green center">163</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-blues-forever-vol-36-2015-mp3-320-kbps-t11605839.html" class="cellMainLink">VA - Blues Forever, Vol.36 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="193215521">184.26 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T17:13:07+00:00">17 Nov 2015, 17:13:07</span></td>
			<td class="green center">113</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605558,0" class="icommentjs kaButton smallButton rightButton" href="/justin-bieber-purpose-limited-deluxe-edition-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11605558.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/justin-bieber-purpose-limited-deluxe-edition-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11605558.html" class="cellMainLink">Justin Bieber - Purpose (Limited Deluxe Edition) (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="177477505">169.26 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T16:14:00+00:00">17 Nov 2015, 16:14:00</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11606084,0" class="icommentjs kaButton smallButton rightButton" href="/the-howard-stern-show-and-the-wrap-up-show-2015-11-17-t11606084.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-howard-stern-show-and-the-wrap-up-show-2015-11-17-t11606084.html" class="cellMainLink">The Howard Stern Show and The Wrap Up Show - 2015-11-17</a></div>
			</td>
			<td class="nobr center" data-sort="261450235">249.34 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T18:18:22+00:00">17 Nov 2015, 18:18:22</span></td>
			<td class="green center">89</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593400,0" class="icommentjs kaButton smallButton rightButton" href="/va-fetenhits-best-of-80-s-2015-mp3-320-kbps-t11593400.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-fetenhits-best-of-80-s-2015-mp3-320-kbps-t11593400.html" class="cellMainLink">VA - Fetenhits: Best Of 80âs (2015) MP3 [320 Kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="586197851">559.04 <span>MB</span></td>
			<td class="center">61</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T17:03:28+00:00">15 Nov 2015, 17:03:28</span></td>
			<td class="green center">88</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602270,0" class="icommentjs kaButton smallButton rightButton" href="/floorfillers-anthems-2016-mp3-divxtotal-t11602270.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/floorfillers-anthems-2016-mp3-divxtotal-t11602270.html" class="cellMainLink">Floorfillers Anthems 2016 MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="365239929">348.32 <span>MB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T01:19:15+00:00">17 Nov 2015, 01:19:15</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">12</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11610476,0" class="icommentjs kaButton smallButton rightButton" href="/xatab-repacker-presents-assassins-creed-syndicate-repack-dlc-multi2-t11610476.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/xatab-repacker-presents-assassins-creed-syndicate-repack-dlc-multi2-t11610476.html" class="cellMainLink">XaTaB REPACKER PRESENTS Assassins Creed Syndicate RePack DLC MULTi2</a></div>
			</td>
			<td class="nobr center" data-sort="25325482384">23.59 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T08:46:13+00:00">18 Nov 2015, 08:46:13</span></td>
			<td class="green center">951</td>
			<td class="red lasttd center">3221</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605974,0" class="icommentjs kaButton smallButton rightButton" href="/assassins-creed-syndicate-full-unlocked-mercs213-t11605974.html#comment">115 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/assassins-creed-syndicate-full-unlocked-mercs213-t11605974.html" class="cellMainLink">Assassins Creed Syndicate Full Unlocked-MERCS213</a></div>
			</td>
			<td class="nobr center" data-sort="44360937733">41.31 <span>GB</span></td>
			<td class="center">389</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T17:53:02+00:00">17 Nov 2015, 17:53:02</span></td>
			<td class="green center">171</td>
			<td class="red lasttd center">3541</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11606676,0" class="icommentjs kaButton smallButton rightButton" href="/game-of-thrones-episode-6-codex-t11606676.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/game-of-thrones-episode-6-codex-t11606676.html" class="cellMainLink">Game of Thrones Episode 6-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="13577361060">12.64 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T19:20:15+00:00">17 Nov 2015, 19:20:15</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">468</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11609390,0" class="icommentjs kaButton smallButton rightButton" href="/corepack-presents-assassins-creed-syndicate-inc-dlc-s-repack-v1-t11609390.html#comment">127 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/corepack-presents-assassins-creed-syndicate-inc-dlc-s-repack-v1-t11609390.html" class="cellMainLink">CorePack Presents - Assassins Creed: Syndicate Inc. DLC&#039;s - RePack V1</a></div>
			</td>
			<td class="nobr center" data-sort="22014666293">20.5 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T04:48:32+00:00">18 Nov 2015, 04:48:32</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">616</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594004,0" class="icommentjs kaButton smallButton rightButton" href="/darksiders-2-deathinitive-edition-update-1-2015-pc-repack-by-r-g-mechanics-t11594004.html#comment">35 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/darksiders-2-deathinitive-edition-update-1-2015-pc-repack-by-r-g-mechanics-t11594004.html" class="cellMainLink">Darksiders 2: Deathinitive Edition [Update 1] (2015) PC | RePack by R.G. Mechanics</a></div>
			</td>
			<td class="nobr center" data-sort="8494382166">7.91 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T19:05:30+00:00">15 Nov 2015, 19:05:30</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">109</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11604954,0" class="icommentjs kaButton smallButton rightButton" href="/dead-or-alive-5-last-round-v1-04-hotfix-2-inc-13-dlc-pack-updated-20151117-3dm-t11604954.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/dead-or-alive-5-last-round-v1-04-hotfix-2-inc-13-dlc-pack-updated-20151117-3dm-t11604954.html" class="cellMainLink">Dead or Alive 5 Last Round v1.04 Hotfix 2 Inc. 13 DLC Pack Updated 20151117-3DM</a></div>
			</td>
			<td class="nobr center" data-sort="1997155946">1.86 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T13:37:21+00:00">17 Nov 2015, 13:37:21</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">78</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589279,0" class="icommentjs kaButton smallButton rightButton" href="/call-of-duty-black-ops-iii-update-1-reloaded-t11589279.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/call-of-duty-black-ops-iii-update-1-reloaded-t11589279.html" class="cellMainLink">Call of Duty Black Ops III Update 1-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="556941846">531.14 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T20:02:47+00:00">14 Nov 2015, 20:02:47</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591120,0" class="icommentjs kaButton smallButton rightButton" href="/minecraft-pocket-edition-v0-13-build-5-cracked-mod-apks-are-here-latest-by-kat-runner-t11591120.html#comment">4 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/minecraft-pocket-edition-v0-13-build-5-cracked-mod-apks-are-here-latest-by-kat-runner-t11591120.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/minecraft-pocket-edition-v0-13-build-5-cracked-mod-apks-are-here-latest-by-kat-runner-t11591120.html" class="cellMainLink">Minecraft â Pocket Edition v0.13 Build 5 Cracked MOD APKs are Here ! [LATEST] by = KAT- RUNNER</a></div>
			</td>
			<td class="nobr center" data-sort="19087273">18.2 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:31:58+00:00">15 Nov 2015, 07:31:58</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600029,0" class="icommentjs kaButton smallButton rightButton" href="/tropico-5-gog-t11600029.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tropico-5-gog-t11600029.html" class="cellMainLink">Tropico 5 (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="2945638913">2.74 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T15:01:28+00:00">16 Nov 2015, 15:01:28</span></td>
			<td class="green center">45</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589389,0" class="icommentjs kaButton smallButton rightButton" href="/dark-tales-8-edgar-allan-poes-the-tell-tale-heart-ce-2015-pc-final-t11589389.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dark-tales-8-edgar-allan-poes-the-tell-tale-heart-ce-2015-pc-final-t11589389.html" class="cellMainLink">Dark Tales 8: Edgar Allan Poes The Tell-Tale Heart CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="723377711">689.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T20:39:18+00:00">14 Nov 2015, 20:39:18</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11604310,0" class="icommentjs kaButton smallButton rightButton" href="/spintires-build-9-11-15-t11604310.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/spintires-build-9-11-15-t11604310.html" class="cellMainLink">SPINTIRES Build 9.11.15</a></div>
			</td>
			<td class="nobr center" data-sort="574369261">547.76 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T10:35:55+00:00">17 Nov 2015, 10:35:55</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11604243,0" class="icommentjs kaButton smallButton rightButton" href="/conquest-of-elysium-4-t11604243.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/conquest-of-elysium-4-t11604243.html" class="cellMainLink">Conquest of Elysium 4</a></div>
			</td>
			<td class="nobr center" data-sort="426645624">406.88 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T10:21:52+00:00">17 Nov 2015, 10:21:52</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589564,0" class="icommentjs kaButton smallButton rightButton" href="/prison-architect-2-2-0-4-gog-t11589564.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/prison-architect-2-2-0-4-gog-t11589564.html" class="cellMainLink">Prison Architect (2.2.0.4) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="285895793">272.65 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T21:44:49+00:00">14 Nov 2015, 21:44:49</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">1</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591682,0" class="icommentjs kaButton smallButton rightButton" href="/the-binding-of-isaac-afterbirth-update-6-repack-mr-dj-t11591682.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-binding-of-isaac-afterbirth-update-6-repack-mr-dj-t11591682.html" class="cellMainLink">The Binding of Isaac Afterbirth Update 6 repack Mr DJ</a></div>
			</td>
			<td class="nobr center" data-sort="469902652">448.13 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T09:49:12+00:00">15 Nov 2015, 09:49:12</span></td>
			<td class="green center">12</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591168,0" class="icommentjs kaButton smallButton rightButton" href="/naruto-shippuden-ultimate-ninja-storm-4-v2-0-mod-apk-is-here-latest-by-kat-runner-t11591168.html#comment">6 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/naruto-shippuden-ultimate-ninja-storm-4-v2-0-mod-apk-is-here-latest-by-kat-runner-t11591168.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/naruto-shippuden-ultimate-ninja-storm-4-v2-0-mod-apk-is-here-latest-by-kat-runner-t11591168.html" class="cellMainLink">Naruto Shippuden â Ultimate Ninja Storm 4 v2.0 Mod APK Is Here! [LATEST] by = KAT- RUNNER</a></div>
			</td>
			<td class="nobr center" data-sort="105185200">100.31 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:46:08+00:00">15 Nov 2015, 07:46:08</span></td>
			<td class="green center">10</td>
			<td class="red lasttd center">4</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11595895,0" class="icommentjs kaButton smallButton rightButton" href="/malwarebytes-anti-malware-premium-latest-serials-14-11-15-4realtorrentz-t11595895.html#comment">57 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType txtType">
                    <a href="/malwarebytes-anti-malware-premium-latest-serials-14-11-15-4realtorrentz-t11595895.html" class="cellMainLink">Malwarebytes Anti-Malware Premium Latest Serials (14.11.15) [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="735">735 <span>bytes</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T01:08:52+00:00">16 Nov 2015, 01:08:52</span></td>
			<td class="green center">289</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603209,0" class="icommentjs kaButton smallButton rightButton" href="/room-arranger-8-3-0-538-32bit-64bit-multilanguage-serial-at-team-t11603209.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/room-arranger-8-3-0-538-32bit-64bit-multilanguage-serial-at-team-t11603209.html" class="cellMainLink">Room Arranger 8.3.0.538 - 32bit &amp; 64bit [Multilanguage] [Serial] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="48392807">46.15 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T06:09:17+00:00">17 Nov 2015, 06:09:17</span></td>
			<td class="green center">249</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591799,0" class="icommentjs kaButton smallButton rightButton" href="/total-uninstall-6-16-0-320-professional-edition-mltilanguage-cracked-remek002-at-team-t11591799.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/total-uninstall-6-16-0-320-professional-edition-mltilanguage-cracked-remek002-at-team-t11591799.html" class="cellMainLink">Total Uninstall 6.16.0.320 Professional Edition [Mltilanguage] [Cracked remek002] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="35616332">33.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T10:26:22+00:00">15 Nov 2015, 10:26:22</span></td>
			<td class="green center">185</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596062,0" class="icommentjs kaButton smallButton rightButton" href="/iobit-driver-booster-pro-3-0-3-275-multilingual-key-4realtorrentz-t11596062.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/iobit-driver-booster-pro-3-0-3-275-multilingual-key-4realtorrentz-t11596062.html" class="cellMainLink">IObit Driver Booster Pro 3.0.3.275 Multilingual + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="14233401">13.57 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:14:17+00:00">16 Nov 2015, 02:14:17</span></td>
			<td class="green center">177</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591059,0" class="icommentjs kaButton smallButton rightButton" href="/windows-10-pro-rtm-build-10586-x64-multi-6-nov2015-pre-activated-t11591059.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-rtm-build-10586-x64-multi-6-nov2015-pre-activated-t11591059.html" class="cellMainLink">Windows 10 Pro RTM Build 10586 x64 MULTi-6 Nov2015-Pre-Activated~</a></div>
			</td>
			<td class="nobr center" data-sort="3654529610">3.4 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:14:30+00:00">15 Nov 2015, 07:14:30</span></td>
			<td class="green center">105</td>
			<td class="red lasttd center">119</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11607894,0" class="icommentjs kaButton smallButton rightButton" href="/advanced-systemcare-pro-9-0-3-1077-multilingual-incl-patch-team-os-t11607894.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/advanced-systemcare-pro-9-0-3-1077-multilingual-incl-patch-team-os-t11607894.html" class="cellMainLink">Advanced SystemCare Pro 9.0.3.1077 Multilingual Incl Patch-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="40167088">38.31 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T20:07:06+00:00">17 Nov 2015, 20:07:06</span></td>
			<td class="green center">139</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596306,0" class="icommentjs kaButton smallButton rightButton" href="/destroy-windows-10-spying-1-5-0-build-693-4realtorrentz-t11596306.html#comment">30 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/destroy-windows-10-spying-1-5-0-build-693-4realtorrentz-t11596306.html" class="cellMainLink">Destroy Windows 10 Spying 1.5.0 Build 693 [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="134183">131.04 <span>KB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T03:44:54+00:00">16 Nov 2015, 03:44:54</span></td>
			<td class="green center">134</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605430,0" class="icommentjs kaButton smallButton rightButton" href="/adobe-photoshop-lightroom-cc-6-3-multilingual-win-mac-incl-patch-team-os-t11605430.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-photoshop-lightroom-cc-6-3-multilingual-win-mac-incl-patch-team-os-t11605430.html" class="cellMainLink">Adobe Photoshop Lightroom CC 6.3 Multilingual (Win &amp; Mac) Incl Patch-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="2012210527">1.87 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T15:44:54+00:00">17 Nov 2015, 15:44:54</span></td>
			<td class="green center">57</td>
			<td class="red lasttd center">144</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590763,0" class="icommentjs kaButton smallButton rightButton" href="/media-player-classic-home-cinema-1-7-10-stable-2015-Ð Ð¡-portable-t11590763.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/media-player-classic-home-cinema-1-7-10-stable-2015-Ð Ð¡-portable-t11590763.html" class="cellMainLink">Media Player Classic Home Cinema 1.7.10 Stable (2015) Ð Ð¡ | + Portable</a></div>
			</td>
			<td class="nobr center" data-sort="110579180">105.46 <span>MB</span></td>
			<td class="center">176</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T05:57:08+00:00">15 Nov 2015, 05:57:08</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11598077,0" class="icommentjs kaButton smallButton rightButton" href="/collection-of-programs-portableapps-v-12-2-15-11-2015-2015-pc-t11598077.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/collection-of-programs-portableapps-v-12-2-15-11-2015-2015-pc-t11598077.html" class="cellMainLink">Collection of programs - PortableApps v.12.2 [15.11.2015] (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="3682434337">3.43 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T12:14:07+00:00">16 Nov 2015, 12:14:07</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596135,0" class="icommentjs kaButton smallButton rightButton" href="/acdsee-pro-9-1-build-453-lite-by-mkn-pre-activated-4realtorrentz-t11596135.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/acdsee-pro-9-1-build-453-lite-by-mkn-pre-activated-4realtorrentz-t11596135.html" class="cellMainLink">ACDSee Pro 9.1 Build 453 Lite by MKN Pre-Activated [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="48221725">45.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:50:47+00:00">16 Nov 2015, 02:50:47</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589054,0" class="icommentjs kaButton smallButton rightButton" href="/the-keys-for-eset-nod32-kaspersky-avast-dr-web-avira-05-11-15-14-11-2015-2015-pc-t11589054.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType txtType">
                    <a href="/the-keys-for-eset-nod32-kaspersky-avast-dr-web-avira-05-11-15-14-11-2015-2015-pc-t11589054.html" class="cellMainLink">The keys for ESET NOD32, Kaspersky, Avast, Dr.Web, Avira [05.11.15 / 14.11.2015] (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="17942910">17.11 <span>MB</span></td>
			<td class="center">2105</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:54:28+00:00">14 Nov 2015, 18:54:28</span></td>
			<td class="green center">45</td>
			<td class="red lasttd center">112</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605437,0" class="icommentjs kaButton smallButton rightButton" href="/solid-pdf-to-word-9-1-6079-1056-multilangual-incl-serial-team-os-t11605437.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/solid-pdf-to-word-9-1-6079-1056-multilangual-incl-serial-team-os-t11605437.html" class="cellMainLink">Solid PDF to Word 9.1.6079.1056 Multilangual Incl Serial-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="111644974">106.47 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T15:45:23+00:00">17 Nov 2015, 15:45:23</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11588941,0" class="icommentjs kaButton smallButton rightButton" href="/4k-video-downloader-3-6-4-1795-2015-pc-portable-t11588941.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/4k-video-downloader-3-6-4-1795-2015-pc-portable-t11588941.html" class="cellMainLink">4K Video Downloader 3.6.4.1795 (2015) PC | + Portable</a></div>
			</td>
			<td class="nobr center" data-sort="129426202">123.43 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:27:35+00:00">14 Nov 2015, 18:27:35</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11610844,0" class="icommentjs kaButton smallButton rightButton" href="/dfx-audio-enhancer-12-012-crack-s0ft4pc-t11610844.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dfx-audio-enhancer-12-012-crack-s0ft4pc-t11610844.html" class="cellMainLink">DFX Audio Enhancer 12.012 + Crack [S0ft4PC]</a></div>
			</td>
			<td class="nobr center" data-sort="5507164">5.25 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T10:19:09+00:00">18 Nov 2015, 10:19:09</span></td>
			<td class="green center">54</td>
			<td class="red lasttd center">15</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-hidan-no-aria-aa-07-raw-tva-1280x720-x264-aac-mp4-t11606520.html" class="cellMainLink">[Leopard-Raws] Hidan no Aria AA - 07 RAW (TVA 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="331317167">315.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T19:15:01+00:00">17 Nov 2015, 19:15:01</span></td>
			<td class="green center">686</td>
			<td class="red lasttd center">176</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594279,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-one-punch-man-07-720p-mkv-t11594279.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-punch-man-07-720p-mkv-t11594279.html" class="cellMainLink">[AnimeRG] One Punch Man - 07 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="286920430">273.63 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:11:42+00:00">15 Nov 2015, 20:11:42</span></td>
			<td class="green center">573</td>
			<td class="red lasttd center">113</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589311,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-haikyuu-s2-07-720p-mkv-t11589311.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-haikyuu-s2-07-720p-mkv-t11589311.html" class="cellMainLink">[HorribleSubs] Haikyuu!! S2 - 07 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="342465219">326.6 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T20:10:03+00:00">14 Nov 2015, 20:10:03</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">679</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590547,0" class="icommentjs kaButton smallButton rightButton" href="/one-piece-718-480p-engsub-iorchid-t11590547.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-718-480p-engsub-iorchid-t11590547.html" class="cellMainLink">One Piece - 718 [480p][EngSub][iORcHiD]</a></div>
			</td>
			<td class="nobr center" data-sort="100414003">95.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T05:02:33+00:00">15 Nov 2015, 05:02:33</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594206,0" class="icommentjs kaButton smallButton rightButton" href="/one-punch-man-07-480p-engsub-iorchid-mkv-t11594206.html#comment">27 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-punch-man-07-480p-engsub-iorchid-mkv-t11594206.html" class="cellMainLink">One-Punch Man - 07 [480p][EngSub][iORcHiD].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="101255408">96.56 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T19:48:33+00:00">15 Nov 2015, 19:48:33</span></td>
			<td class="green center">160</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603695,0" class="icommentjs kaButton smallButton rightButton" href="/ipunisher-triage-x-bd-1280x720-h264-10bit-aac-uncensored-t11603695.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-triage-x-bd-1280x720-h264-10bit-aac-uncensored-t11603695.html" class="cellMainLink">[iPUNISHER] Triage X (BD 1280x720 h264 10bit AAC UNCENSORED)</a></div>
			</td>
			<td class="nobr center" data-sort="3665752963">3.41 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T08:05:33+00:00">17 Nov 2015, 08:05:33</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592296,0" class="icommentjs kaButton smallButton rightButton" href="/dragon-ball-super-episode-019-english-subbed-720p-arizone-t11592296.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-episode-019-english-subbed-720p-arizone-t11592296.html" class="cellMainLink">DRAGON BALL SUPER Episode - 019 [ENGLISH SUBBED] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="160030107">152.62 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T12:26:55+00:00">15 Nov 2015, 12:26:55</span></td>
			<td class="green center">101</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593553,0" class="icommentjs kaButton smallButton rightButton" href="/ae-one-punch-man-07-720p-mp4-t11593553.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ae-one-punch-man-07-720p-mp4-t11593553.html" class="cellMainLink">[AE] One-Punch Man - 07 [720p].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="206517951">196.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T17:41:34+00:00">15 Nov 2015, 17:41:34</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-rakudai-kishi-no-cavalry-07-raw-atx-1280x720-x264-aac-mp4-t11589998.html" class="cellMainLink">[Leopard-Raws] Rakudai Kishi no Cavalry - 07 RAW (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="382285458">364.58 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T01:05:02+00:00">15 Nov 2015, 01:05:02</span></td>
			<td class="green center">17</td>
			<td class="red lasttd center">135</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592034,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-dragon-ball-super-019-480p-eng-sub-lucifer22-t11592034.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-dragon-ball-super-019-480p-eng-sub-lucifer22-t11592034.html" class="cellMainLink">[ARRG] Dragon Ball Super - 019 [480p][Eng Sub]_lucifer22</a></div>
			</td>
			<td class="nobr center" data-sort="64972686">61.96 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T11:23:52+00:00">15 Nov 2015, 11:23:52</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-teekyuu-67-e2b88e60-mkv-t11601330.html" class="cellMainLink">[Commie] Teekyuu - 67 [E2B88E60].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="44540098">42.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T19:55:02+00:00">16 Nov 2015, 19:55:02</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590771,0" class="icommentjs kaButton smallButton rightButton" href="/ae-valkyrie-drive-mermaid-06-uncen-720p-mp4-t11590771.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ae-valkyrie-drive-mermaid-06-uncen-720p-mp4-t11590771.html" class="cellMainLink">[AE] Valkyrie Drive - Mermaid - 06 [UNCEN] [720p].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="178146536">169.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T05:59:28+00:00">15 Nov 2015, 05:59:28</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593525,0" class="icommentjs kaButton smallButton rightButton" href="/bakedfish-one-punch-man-07-720p-aac-mp4-t11593525.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-one-punch-man-07-720p-aac-mp4-t11593525.html" class="cellMainLink">[BakedFish] One Punch Man - 07 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="506927758">483.44 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T17:34:48+00:00">15 Nov 2015, 17:34:48</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603617,0" class="icommentjs kaButton smallButton rightButton" href="/deadfish-yu-gi-oh-arc-v-82-720p-aac-mp4-t11603617.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-yu-gi-oh-arc-v-82-720p-aac-mp4-t11603617.html" class="cellMainLink">[DeadFish] Yu-Gi-Oh! Arc-V - 82 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="613244995">584.84 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T07:39:12+00:00">17 Nov 2015, 07:39:12</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11598562,0" class="icommentjs kaButton smallButton rightButton" href="/dragon-ball-gt-complete-series-dbgt-dual-audio-480p-hevc-t11598562.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-gt-complete-series-dbgt-dual-audio-480p-hevc-t11598562.html" class="cellMainLink">Dragon Ball GT (Complete Series) [DBGT] [DUAL-AUDIO] [480p] [HEVC]</a></div>
			</td>
			<td class="nobr center" data-sort="7146547557">6.66 <span>GB</span></td>
			<td class="center">75</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T13:55:01+00:00">16 Nov 2015, 13:55:01</span></td>
			<td class="green center">10</td>
			<td class="red lasttd center">43</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11609161,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-november-18-2015-true-pdf-t11609161.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-november-18-2015-true-pdf-t11609161.html" class="cellMainLink">Assorted Magazines Bundle - November 18 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="500961235">477.75 <span>MB</span></td>
			<td class="center">38</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T03:22:50+00:00">18 Nov 2015, 03:22:50</span></td>
			<td class="green center">372</td>
			<td class="red lasttd center">492</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589577,0" class="icommentjs kaButton smallButton rightButton" href="/ultimate-iq-tests-philip-carter-ken-russell-t11589577.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/ultimate-iq-tests-philip-carter-ken-russell-t11589577.html" class="cellMainLink">Ultimate IQ Tests - Philip Carter &amp; Ken Russell</a></div>
			</td>
			<td class="nobr center" data-sort="10088588">9.62 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T21:51:27+00:00">14 Nov 2015, 21:51:27</span></td>
			<td class="green center">496</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590748,0" class="icommentjs kaButton smallButton rightButton" href="/fallout-4-vault-dweller-s-survival-guide-prima-official-game-guide-t11590748.html#comment">60 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/fallout-4-vault-dweller-s-survival-guide-prima-official-game-guide-t11590748.html" class="cellMainLink">Fallout 4 Vault Dweller&#039;s Survival Guide: Prima Official Game Guide</a></div>
			</td>
			<td class="nobr center" data-sort="75282330">71.79 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T05:53:38+00:00">15 Nov 2015, 05:53:38</span></td>
			<td class="green center">428</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589878,0" class="icommentjs kaButton smallButton rightButton" href="/islam-and-terrorism-revised-and-updated-edition-the-truth-about-isis-the-middle-east-and-islamic-jihad-m-gabriel-t11589878.html#comment">29 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/islam-and-terrorism-revised-and-updated-edition-the-truth-about-isis-the-middle-east-and-islamic-jihad-m-gabriel-t11589878.html" class="cellMainLink">Islam and Terrorism (Revised and Updated Edition) : The Truth About ISIS, the Middle East and Islamic Jihad - M Gabriel</a></div>
			</td>
			<td class="nobr center" data-sort="6585711">6.28 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T23:56:29+00:00">14 Nov 2015, 23:56:29</span></td>
			<td class="green center">389</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600045,0" class="icommentjs kaButton smallButton rightButton" href="/the-mission-chinese-food-cookbook-t11600045.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-mission-chinese-food-cookbook-t11600045.html" class="cellMainLink">The Mission Chinese Food Cookbook</a></div>
			</td>
			<td class="nobr center" data-sort="52889967">50.44 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T15:06:43+00:00">16 Nov 2015, 15:06:43</span></td>
			<td class="green center">243</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11608163,0" class="icommentjs kaButton smallButton rightButton" href="/the-four-lenses-of-innovation-a-power-tool-for-creative-thinking-1st-edition-2015-pdf-gooner-t11608163.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-four-lenses-of-innovation-a-power-tool-for-creative-thinking-1st-edition-2015-pdf-gooner-t11608163.html" class="cellMainLink">The Four Lenses of Innovation - A Power Tool for Creative Thinking - 1st Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="95181143">90.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T21:31:07+00:00">17 Nov 2015, 21:31:07</span></td>
			<td class="green center">203</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591071,0" class="icommentjs kaButton smallButton rightButton" href="/womens-magazines-bundle-november-15-2015-true-pdf-t11591071.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-november-15-2015-true-pdf-t11591071.html" class="cellMainLink">Womens Magazines Bundle - November 15 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="359233743">342.59 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:17:07+00:00">15 Nov 2015, 07:17:07</span></td>
			<td class="green center">155</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600055,0" class="icommentjs kaButton smallButton rightButton" href="/the-farmer-s-wife-canning-and-preserving-cookbook-over-250-blue-ribbon-recipes-t11600055.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-farmer-s-wife-canning-and-preserving-cookbook-over-250-blue-ribbon-recipes-t11600055.html" class="cellMainLink">The Farmer&#039;s Wife Canning and Preserving Cookbook Over 250 Blue-Ribbon recipes!</a></div>
			</td>
			<td class="nobr center" data-sort="24274829">23.15 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T15:06:50+00:00">16 Nov 2015, 15:06:50</span></td>
			<td class="green center">162</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600053,0" class="icommentjs kaButton smallButton rightButton" href="/real-cajun-rustic-home-cooking-from-donald-link-s-louisiana-t11600053.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/real-cajun-rustic-home-cooking-from-donald-link-s-louisiana-t11600053.html" class="cellMainLink">Real Cajun Rustic Home Cooking from Donald Link&#039;s Louisiana</a></div>
			</td>
			<td class="nobr center" data-sort="39789901">37.95 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T15:06:49+00:00">16 Nov 2015, 15:06:49</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590114,0" class="icommentjs kaButton smallButton rightButton" href="/batman-v1-001-713-extras-1940-2011-digital-scans-novus-empire-minutemen-nem-t11590114.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/batman-v1-001-713-extras-1940-2011-digital-scans-novus-empire-minutemen-nem-t11590114.html" class="cellMainLink">Batman v1 (001-713+Extras) (1940-2011) (digital+scans) (Novus+Empire+Minutemen) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="24441374901">22.76 <span>GB</span></td>
			<td class="center">877</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T02:04:17+00:00">15 Nov 2015, 02:04:17</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">133</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11604924,0" class="icommentjs kaButton smallButton rightButton" href="/the-gourmet-farmer-goes-fishing-the-fish-to-eat-and-how-to-cook-it-2015-epub-gooner-t11604924.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-gourmet-farmer-goes-fishing-the-fish-to-eat-and-how-to-cook-it-2015-epub-gooner-t11604924.html" class="cellMainLink">The Gourmet Farmer Goes Fishing - The Fish to Eat and How to Cook It (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="92074734">87.81 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T13:23:42+00:00">17 Nov 2015, 13:23:42</span></td>
			<td class="green center">102</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11609823,0" class="icommentjs kaButton smallButton rightButton" href="/0-day-week-of-2015-11-11-t11609823.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-11-11-t11609823.html" class="cellMainLink">0-Day Week of 2015.11.11</a></div>
			</td>
			<td class="nobr center" data-sort="12613210246">11.75 <span>GB</span></td>
			<td class="center">189</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T06:27:53+00:00">18 Nov 2015, 06:27:53</span></td>
			<td class="green center">21</td>
			<td class="red lasttd center">182</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11611034,0" class="icommentjs kaButton smallButton rightButton" href="/the-mighty-thor-001-2016-digital-zone-empire-cbr-nem-t11611034.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-mighty-thor-001-2016-digital-zone-empire-cbr-nem-t11611034.html" class="cellMainLink">The Mighty Thor 001 (2016) (Digital) (Zone-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="63579536">60.63 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T11:01:40+00:00">18 Nov 2015, 11:01:40</span></td>
			<td class="green center">103</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/extraordinary-x-men-002-2016-digital-zone-empire-cbr-nem-t11611014.html" class="cellMainLink">Extraordinary X-Men 002 (2016) (Digital) (Zone-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="45988525">43.86 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T10:56:34+00:00">18 Nov 2015, 10:56:34</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605100,0" class="icommentjs kaButton smallButton rightButton" href="/the-good-gardener-a-hands-on-guide-from-national-trust-experts-2015-epub-gooner-t11605100.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-good-gardener-a-hands-on-guide-from-national-trust-experts-2015-epub-gooner-t11605100.html" class="cellMainLink">The Good Gardener - A Hands-on Guide from National Trust Experts (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="112362102">107.16 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T14:17:46+00:00">17 Nov 2015, 14:17:46</span></td>
			<td class="green center">74</td>
			<td class="red lasttd center">20</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600889,0" class="icommentjs kaButton smallButton rightButton" href="/va-favourite-classics-definitive-classical-collection-flac-t11600889.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-favourite-classics-definitive-classical-collection-flac-t11600889.html" class="cellMainLink">VA - Favourite Classics (Definitive Classical Collection) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="690652289">658.66 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T18:05:58+00:00">16 Nov 2015, 18:05:58</span></td>
			<td class="green center">154</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11608252,0" class="icommentjs kaButton smallButton rightButton" href="/the-who-who-s-next-2014-deluxe-24-96-hd-flac-t11608252.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-who-s-next-2014-deluxe-24-96-hd-flac-t11608252.html" class="cellMainLink">The Who - Who&#039;s Next (2014 Deluxe) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3388895163">3.16 <span>GB</span></td>
			<td class="center">49</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T22:09:10+00:00">17 Nov 2015, 22:09:10</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591176,0" class="icommentjs kaButton smallButton rightButton" href="/neil-young-bluenote-cafe-2015-flac-sn3h1t87-glodls-t11591176.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/neil-young-bluenote-cafe-2015-flac-sn3h1t87-glodls-t11591176.html" class="cellMainLink">Neil Young - Bluenote Cafe (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="1002802681">956.35 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:48:07+00:00">15 Nov 2015, 07:48:07</span></td>
			<td class="green center">137</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592042,0" class="icommentjs kaButton smallButton rightButton" href="/michael-jackson-bad-25-3cd-deluxe-2012-flac-t11592042.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/michael-jackson-bad-25-3cd-deluxe-2012-flac-t11592042.html" class="cellMainLink">Michael Jackson - Bad 25 (3CD Deluxe 2012) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1593642203">1.48 <span>GB</span></td>
			<td class="center">177</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T11:25:34+00:00">15 Nov 2015, 11:25:34</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11601656,0" class="icommentjs kaButton smallButton rightButton" href="/top-100-90-s-rock-albums-by-ultimateclassicrock-cd-s-01-25-flac-t11601656.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/top-100-90-s-rock-albums-by-ultimateclassicrock-cd-s-01-25-flac-t11601656.html" class="cellMainLink">Top 100 &#039;90&#039;s Rock Albums by UltimateClassicRock - CD&#039;s 01-25 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="11154816116">10.39 <span>GB</span></td>
			<td class="center">763</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T21:40:23+00:00">16 Nov 2015, 21:40:23</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">119</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11601187,0" class="icommentjs kaButton smallButton rightButton" href="/big-bands-of-the-swingin-years-flac-tntvillage-t11601187.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/big-bands-of-the-swingin-years-flac-tntvillage-t11601187.html" class="cellMainLink">Big Bands Of The Swingin&#039; Years [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="265538273">253.24 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T19:11:52+00:00">16 Nov 2015, 19:11:52</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ellie-goulding-2015-delirium-target-deluxe-edition-flac-freak37-t11597101.html" class="cellMainLink">Ellie Goulding - 2015 - Delirium [Target Deluxe Edition] - FLAC ... [ Freak37 ]</a></div>
			</td>
			<td class="nobr center" data-sort="672447474">641.3 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T07:47:55+00:00">16 Nov 2015, 07:47:55</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593040,0" class="icommentjs kaButton smallButton rightButton" href="/billie-holiday-solitude-2015-24-192-hd-flac-t11593040.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billie-holiday-solitude-2015-24-192-hd-flac-t11593040.html" class="cellMainLink">Billie Holiday - Solitude (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1690023298">1.57 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T15:33:24+00:00">15 Nov 2015, 15:33:24</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592282,0" class="icommentjs kaButton smallButton rightButton" href="/va-magical-melodies-that-will-live-forever-3-cd-box-set-flac-t11592282.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-magical-melodies-that-will-live-forever-3-cd-box-set-flac-t11592282.html" class="cellMainLink">VA - Magical Melodies That Will Live Forever 3 CD Box Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1107754632">1.03 <span>GB</span></td>
			<td class="center">65</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T12:20:49+00:00">15 Nov 2015, 12:20:49</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/one-direction-made-in-the-a-m-deluxe-edition-2015-flac-sn3h1t87-glodls-t11603083.html" class="cellMainLink">One Direction - Made In The A.M. [Deluxe Edition] (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="430819514">410.86 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T05:33:56+00:00">17 Nov 2015, 05:33:56</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11605308,0" class="icommentjs kaButton smallButton rightButton" href="/mark-knopfler-live-in-vaison-la-romaine-1996-sbd-flac-t11605308.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mark-knopfler-live-in-vaison-la-romaine-1996-sbd-flac-t11605308.html" class="cellMainLink">Mark Knopfler - Live in Vaison la Romaine 1996 (SBD) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1114031987">1.04 <span>GB</span></td>
			<td class="center">63</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T15:18:35+00:00">17 Nov 2015, 15:18:35</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589585,0" class="icommentjs kaButton smallButton rightButton" href="/brahms-piano-trios-julius-katchen-josef-suk-jÃ¡nos-starker-t11589585.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/brahms-piano-trios-julius-katchen-josef-suk-jÃ¡nos-starker-t11589585.html" class="cellMainLink">Brahms - Piano trios - Julius Katchen, Josef Suk, JÃ¡nos Starker</a></div>
			</td>
			<td class="nobr center" data-sort="506836900">483.36 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T21:57:13+00:00">14 Nov 2015, 21:57:13</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594392,0" class="icommentjs kaButton smallButton rightButton" href="/prokofiev-violin-sonatas-shlomo-mintz-yefim-bronfman-t11594392.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/prokofiev-violin-sonatas-shlomo-mintz-yefim-bronfman-t11594392.html" class="cellMainLink">Prokofiev - Violin Sonatas - Shlomo Mintz, Yefim Bronfman</a></div>
			</td>
			<td class="nobr center" data-sort="224823840">214.41 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:40:19+00:00">15 Nov 2015, 20:40:19</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600950,0" class="icommentjs kaButton smallButton rightButton" href="/orff-carmina-burana-chailly-rso-berlin-t11600950.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/orff-carmina-burana-chailly-rso-berlin-t11600950.html" class="cellMainLink">Orff - Carmina burana - Chailly, RSO Berlin</a></div>
			</td>
			<td class="nobr center" data-sort="243448737">232.17 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T18:17:34+00:00">16 Nov 2015, 18:17:34</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/david-bowie-the-singles-1969-to-1993-flac-tntvillage-t11610997.html" class="cellMainLink">David Bowie - The Singles 1969 to 1993 [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="1053320753">1004.52 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T10:53:09+00:00">18 Nov 2015, 10:53:09</span></td>
			<td class="green center">42</td>
			<td class="red lasttd center">47</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-phone-do-you-own/?unread=17115377">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What phone do you own?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/VirusUploader/">VirusUploader</a></span></span> <time class="timeago" datetime="2015-11-18T17:48:19+00:00">18 Nov 2015, 17:48</time></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v5-thread-116026/?unread=17115375">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V5
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Minusman/">Minusman</a></span></span> <time class="timeago" datetime="2015-11-18T17:46:24+00:00">18 Nov 2015, 17:46</time></span>
	</li>
		<li>
		<a href="/community/show/adopt-uploader-program-v12-all-users-help-thread-115043/?unread=17115374">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				**Adopt an uploader Program v12-- For all Users to Help**
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/MisterGaga/">MisterGaga</a></span></span> <time class="timeago" datetime="2015-11-18T17:46:20+00:00">18 Nov 2015, 17:46</time></span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v12/?unread=17115372">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V12
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/UnkaWillbur/">UnkaWillbur</a></span></span> <time class="timeago" datetime="2015-11-18T17:44:10+00:00">18 Nov 2015, 17:44</time></span>
	</li>
		<li>
		<a href="/community/show/kingdom-hearts-iii/">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Kingdom Hearts III
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Patricier/">Patricier</a></span></span> <time class="timeago" datetime="2015-11-18T17:39:24+00:00">18 Nov 2015, 17:39</time></span>
	</li>
		<li>
		<a href="/community/show/what-movies-can-you-watch-over-and-over-again-without-being/?unread=17115362">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What movies can you watch over and over again without being bored of it?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/KILLER.SNIPER/">KILLER.SNIPER</a></span></span> <time class="timeago" datetime="2015-11-18T17:39:13+00:00">18 Nov 2015, 17:39</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/Geo%20G.%20Darren/post/the-dictator-inside-you/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The Dictator Inside You.</p></a><span class="explanation">by <a class="plain aclColor_5" href="/user/Geo%20G.%20Darren/">Geo G. Darren</a> <time class="timeago" datetime="2015-11-18T12:33:54+00:00">18 Nov 2015, 12:33</time></span></li>
	<li><a href="/blog/OptimusPr1me/post/soldiers-of-misfortune/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Soldiers Of Misfortune</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/OptimusPr1me/">OptimusPr1me</a> <time class="timeago" datetime="2015-11-18T08:26:43+00:00">18 Nov 2015, 08:26</time></span></li>
	<li><a href="/blog/Gangsta./post/cancer-awareness-by-ps42015/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Cancer awareness (By PS42015)</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Gangsta./">Gangsta.</a> <time class="timeago" datetime="2015-11-18T02:43:12+00:00">18 Nov 2015, 02:43</time></span></li>
	<li><a href="/blog/TheDels/post/why-youtube-s-copyright-system-is-broken/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Why Youtube&#039;s Copyright System is Broken</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-11-17T10:01:27+00:00">17 Nov 2015, 10:01</time></span></li>
	<li><a href="/blog/floofiness/post/global-opportunities/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Global Opportunities</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/floofiness/">floofiness</a> <time class="timeago" datetime="2015-11-16T23:15:16+00:00">16 Nov 2015, 23:15</time></span></li>
	<li><a href="/blog/Maraya21/post/this-blog-doesn-t-contain-the-words-pray-paris-seriously-read-without-fear/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> This blog doesn&#039;t contain the words &quot;Pray&quot; &amp; &quot;Paris&quot;. Seriously, read without fear..</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/Maraya21/">Maraya21</a> <time class="timeago" datetime="2015-11-16T22:15:20+00:00">16 Nov 2015, 22:15</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/sims%2B3/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				sims+3
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/heman%20and%20the%20masters%20of%20the%20universe/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				heman and the masters of the universe
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/microsoft%20office%20for%20mac/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				microsoft office for mac
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/baby/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				baby
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/incest%20real/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				incest real
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/teen%20sex%202015/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				teen sex 2015
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/pro%20evolution%20soccer%202012/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				pro evolution soccer 2012
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/call%20of%20duty%204%3A%20modern%20warfare/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Call of Duty 4: Modern Warfare
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/audacity/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				audacity
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/24%20season%201/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				24 season 1
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/avast%20internet%20security/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				avast internet security
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
