<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-cfaa23e.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-cfaa23e.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-cfaa23e.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-cfaa23e.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-cfaa23e.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'cfaa23e',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-cfaa23e.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2014/" class="tag3">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/ancient%20aliens%20s02e08/" class="tag2">ancient aliens s02e08</a>
	<a href="/search/android/" class="tag3">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/arrow/" class="tag2">arrow</a>
	<a href="/search/avengers/" class="tag3">avengers</a>
	<a href="/search/avengers%20age%20of%20ultron/" class="tag2">avengers age of ultron</a>
	<a href="/search/batman/" class="tag2">batman</a>
	<a href="/search/brothers/" class="tag2">brothers</a>
	<a href="/search/discography/" class="tag4">discography</a>
	<a href="/search/fast%20and%20furious%207/" class="tag2">fast and furious 7</a>
	<a href="/search/flac/" class="tag3">flac</a>
	<a href="/search/french/" class="tag4">french</a>
	<a href="/search/game%20of%20thrones/" class="tag3">game of thrones</a>
	<a href="/search/hannibal/" class="tag3">hannibal</a>
	<a href="/search/hindi/" class="tag9">hindi</a>
	<a href="/search/hindi%202015/" class="tag3">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/insurgent/" class="tag2">insurgent</a>
	<a href="/search/ita/" class="tag4">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag7">kat ph com</a>
	<a href="/search/mad%20max/" class="tag2">mad max</a>
	<a href="/search/malayalam/" class="tag2">malayalam</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag3">movies</a>
	<a href="/search/mr%20robot/" class="tag2">mr robot</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag3">nl</a>
	<a href="/search/pixels/" class="tag2">pixels</a>
	<a href="/search/power/" class="tag2">power</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/san%20andreas/" class="tag2">san andreas</a>
	<a href="/search/southpaw/" class="tag2">southpaw</a>
	<a href="/search/straight%20outta%20compton/" class="tag2">straight outta compton</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/terminator%20genisys/" class="tag2">terminator genisys</a>
	<a href="/search/the%20flash/" class="tag2">the flash</a>
	<a href="/search/the%20minions/" class="tag2">the minions</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/true%20detective/" class="tag2">true detective</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11152601,0" class="icommentjs icon16" href="/san-andreas-2015-hdrip-xvid-ac3-evo-t11152601.html#comment"> <em class="iconvalue">113</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/san-andreas-2015-hdrip-xvid-ac3-evo-t11152601.html" class="cellMainLink">San Andreas 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1541374302">1.44 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Aug 2015, 22:50">1&nbsp;day</span></td>
			<td class="green center">8117</td>
			<td class="red lasttd center">24441</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11140058,0" class="icommentjs icon16" href="/avengers-age-of-ultron-2015-truefrench-webrip-md-xvid-svr-avi-t11140058.html#comment"> <em class="iconvalue">30</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/avengers-age-of-ultron-2015-truefrench-webrip-md-xvid-svr-avi-t11140058.html" class="cellMainLink">Avengers Age of Ultron 2015 TRUEFRENCH WEBRip MD XviD-SVR avi</a></div>
			</td>
			<td class="nobr center" data-sort="1472826278">1.37 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Aug 2015, 04:41">3&nbsp;days</span></td>
			<td class="green center">14035</td>
			<td class="red lasttd center">3217</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11143450,0" class="icommentjs icon16" href="/mythica-the-darkspore-2015-720p-brrip-x264-yify-t11143450.html#comment"> <em class="iconvalue">38</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mythica-the-darkspore-2015-720p-brrip-x264-yify-t11143450.html" class="cellMainLink">Mythica: The Darkspore (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="853603791">814.06 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Aug 2015, 21:25">3&nbsp;days</span></td>
			<td class="green center">5356</td>
			<td class="red lasttd center">5564</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11152849,0" class="icommentjs icon16" href="/minions-2015-hc-hdrip-xvid-ac3-evo-avi-t11152849.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/minions-2015-hc-hdrip-xvid-ac3-evo-avi-t11152849.html" class="cellMainLink">Minions 2015 HC HDRip XviD AC3-EVO avi</a></div>
			</td>
			<td class="nobr center" data-sort="1495820616">1.39 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Aug 2015, 00:48">23&nbsp;hours</span></td>
			<td class="green center">5407</td>
			<td class="red lasttd center">3270</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11142710,0" class="icommentjs icon16" href="/hitman-agent-47-2015-hq-cam-xvid-hal0-t11142710.html#comment"> <em class="iconvalue">77</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hitman-agent-47-2015-hq-cam-xvid-hal0-t11142710.html" class="cellMainLink">Hitman Agent 47 2015 HQ Cam Xvid HaL0</a></div>
			</td>
			<td class="nobr center" data-sort="1509725246">1.41 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Aug 2015, 17:20">3&nbsp;days</span></td>
			<td class="green center">4477</td>
			<td class="red lasttd center">2434</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11140548,0" class="icommentjs icon16" href="/monkey-king-hero-is-back-2015-tc720p-x264-aac-mandarin-chs-mp4ba-hyprz-t11140548.html#comment"> <em class="iconvalue">72</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/monkey-king-hero-is-back-2015-tc720p-x264-aac-mandarin-chs-mp4ba-hyprz-t11140548.html" class="cellMainLink">Monkey King Hero is Back (2015) TC720P X264 AAC Mandarin CHS Mp4Ba [HyprZ]</a></div>
			</td>
			<td class="nobr center" data-sort="1993343098">1.86 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="23 Aug 2015, 07:16">3&nbsp;days</span></td>
			<td class="green center">1811</td>
			<td class="red lasttd center">3252</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11150724,0" class="icommentjs icon16" href="/tiger-house-2015-hdrip-xvid-ac3-etrg-t11150724.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tiger-house-2015-hdrip-xvid-ac3-etrg-t11150724.html" class="cellMainLink">Tiger House 2015 HDRip XViD AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1436761039">1.34 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="25 Aug 2015, 13:14">1&nbsp;day</span></td>
			<td class="green center">1757</td>
			<td class="red lasttd center">3235</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11156667,0" class="icommentjs icon16" href="/the-eternal-zero-2014-french-bdrip-xvid-alioz-avi-t11156667.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-eternal-zero-2014-french-bdrip-xvid-alioz-avi-t11156667.html" class="cellMainLink">The Eternal Zero 2014 FRENCH BDRip XviD-AlioZ avi</a></div>
			</td>
			<td class="nobr center" data-sort="1105282183">1.03 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Aug 2015, 18:36">5&nbsp;hours</span></td>
			<td class="green center">2768</td>
			<td class="red lasttd center">2000</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11149601,0" class="icommentjs icon16" href="/entourage-2015-hdrip-xvid-ac3-etrg-t11149601.html#comment"> <em class="iconvalue">30</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/entourage-2015-hdrip-xvid-ac3-etrg-t11149601.html" class="cellMainLink">Entourage 2015 HDRip XViD AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1470832416">1.37 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="25 Aug 2015, 06:56">1&nbsp;day</span></td>
			<td class="green center">1872</td>
			<td class="red lasttd center">1645</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11150248,0" class="icommentjs icon16" href="/love-and-mercy-2015-hdrip-xvid-ac3-evo-t11150248.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/love-and-mercy-2015-hdrip-xvid-ac3-evo-t11150248.html" class="cellMainLink">Love And Mercy 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1542178430">1.44 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Aug 2015, 10:43">1&nbsp;day</span></td>
			<td class="green center">1568</td>
			<td class="red lasttd center">1864</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11141484,0" class="icommentjs icon16" href="/now-add-honey-2015-hdrip-xvid-ac3-evo-t11141484.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/now-add-honey-2015-hdrip-xvid-ac3-evo-t11141484.html" class="cellMainLink">Now Add Honey 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1496484166">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="23 Aug 2015, 12:19">3&nbsp;days</span></td>
			<td class="green center">1617</td>
			<td class="red lasttd center">1769</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11156758,0" class="icommentjs icon16" href="/spy-2015-hdrip-xvid-etrg-t11156758.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/spy-2015-hdrip-xvid-etrg-t11156758.html" class="cellMainLink">Spy 2015 HDRip XviD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="738274677">704.07 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="26 Aug 2015, 19:00">5&nbsp;hours</span></td>
			<td class="green center">592</td>
			<td class="red lasttd center">2511</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11151590,0" class="icommentjs icon16" href="/robert-2015-dvdrip-xvid-ac3-etrg-t11151590.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/robert-2015-dvdrip-xvid-ac3-etrg-t11151590.html" class="cellMainLink">Robert 2015 DVDRip XViD AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1299968979">1.21 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="25 Aug 2015, 17:21">1&nbsp;day</span></td>
			<td class="green center">1089</td>
			<td class="red lasttd center">1721</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11152176,0" class="icommentjs icon16" href="/32aam-adhyayam-23aam-vakyam-2015-malayalam-hdrip-x264-ac3-700mb-mkv-t11152176.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/32aam-adhyayam-23aam-vakyam-2015-malayalam-hdrip-x264-ac3-700mb-mkv-t11152176.html" class="cellMainLink">32aam Adhyayam 23aam Vakyam [2015] Malayalam HDRip x264 AC3 700MB.mkv</a></div>
			</td>
			<td class="nobr center" data-sort="726389436">692.74 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Aug 2015, 20:01">1&nbsp;day</span></td>
			<td class="green center">757</td>
			<td class="red lasttd center">1284</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11156145,0" class="icommentjs icon16" href="/spy-2015-unrated-hdrip-xvid-ac3-evo-t11156145.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/spy-2015-unrated-hdrip-xvid-ac3-evo-t11156145.html" class="cellMainLink">Spy 2015 UNRATED HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1534983195">1.43 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="26 Aug 2015, 16:47">7&nbsp;hours</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">1594</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11144067,0" class="icommentjs icon16" href="/the-last-ship-s02e11-hdtv-x264-lol-ettv-t11144067.html#comment"> <em class="iconvalue">106</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-last-ship-s02e11-hdtv-x264-lol-ettv-t11144067.html" class="cellMainLink">The Last Ship S02E11 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="395505697">377.18 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Aug 2015, 02:06">2&nbsp;days</span></td>
			<td class="green center">5082</td>
			<td class="red lasttd center">1729</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11148982,0" class="icommentjs icon16" href="/teen-wolf-s05e10-hdtv-x264-killers-ettv-t11148982.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/teen-wolf-s05e10-hdtv-x264-killers-ettv-t11148982.html" class="cellMainLink">Teen Wolf S05E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="249592952">238.03 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Aug 2015, 03:10">1&nbsp;day</span></td>
			<td class="green center">3829</td>
			<td class="red lasttd center">597</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11144959,0" class="icommentjs icon16" href="/ufc-fight-night-74-hdtv-x264-jkkk-sparrow-t11144959.html#comment"> <em class="iconvalue">47</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-fight-night-74-hdtv-x264-jkkk-sparrow-t11144959.html" class="cellMainLink">UFC Fight Night 74 HDTV x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1535741255">1.43 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Aug 2015, 06:06">2&nbsp;days</span></td>
			<td class="green center">3468</td>
			<td class="red lasttd center">887</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11149207,0" class="icommentjs icon16" href="/wwe-raw-2015-08-24-hdtv-x264-ebi-sparrow-t11149207.html#comment"> <em class="iconvalue">46</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-raw-2015-08-24-hdtv-x264-ebi-sparrow-t11149207.html" class="cellMainLink">WWE RAW 2015 08 24 HDTV x264-Ebi -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1538314764">1.43 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="25 Aug 2015, 04:52">1&nbsp;day</span></td>
			<td class="green center">3149</td>
			<td class="red lasttd center">1155</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11153038,0" class="icommentjs icon16" href="/zoo-s01e09-hdtv-x264-lol-ettv-t11153038.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/zoo-s01e09-hdtv-x264-lol-ettv-t11153038.html" class="cellMainLink">Zoo S01E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="364865851">347.96 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="26 Aug 2015, 01:59">22&nbsp;hours</span></td>
			<td class="green center">2550</td>
			<td class="red lasttd center">1497</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11153352,0" class="icommentjs icon16" href="/tyrant-s02e11-hdtv-x264-killers-ettv-t11153352.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tyrant-s02e11-hdtv-x264-killers-ettv-t11153352.html" class="cellMainLink">Tyrant S02E11 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="398418606">379.96 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="26 Aug 2015, 03:57">20&nbsp;hours</span></td>
			<td class="green center">1777</td>
			<td class="red lasttd center">1738</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11153257,0" class="icommentjs icon16" href="/scream-the-tv-series-s01e09-hdtv-x264-killers-ettv-t11153257.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scream-the-tv-series-s01e09-hdtv-x264-killers-ettv-t11153257.html" class="cellMainLink">Scream The TV Series S01E09 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="272366833">259.75 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="26 Aug 2015, 03:15">20&nbsp;hours</span></td>
			<td class="green center">1928</td>
			<td class="red lasttd center">990</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11144375,0" class="icommentjs icon16" href="/the-strain-s02e07-hdtv-x264-killers-ettv-t11144375.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-strain-s02e07-hdtv-x264-killers-ettv-t11144375.html" class="cellMainLink">The Strain S02E07 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="294277917">280.65 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Aug 2015, 03:11">2&nbsp;days</span></td>
			<td class="green center">2052</td>
			<td class="red lasttd center">580</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11144310,0" class="icommentjs icon16" href="/ballers-2015-s01e10-hdtv-x264-batv-ettv-t11144310.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ballers-2015-s01e10-hdtv-x264-batv-ettv-t11144310.html" class="cellMainLink">Ballers 2015 S01E10 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="330538850">315.23 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Aug 2015, 03:06">2&nbsp;days</span></td>
			<td class="green center">2000</td>
			<td class="red lasttd center">546</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11153245,0" class="icommentjs icon16" href="/public-morals-2015-s01e01-hdtv-x264-lol-ettv-t11153245.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/public-morals-2015-s01e01-hdtv-x264-lol-ettv-t11153245.html" class="cellMainLink">Public Morals 2015 S01E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="212557561">202.71 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="26 Aug 2015, 03:08">20&nbsp;hours</span></td>
			<td class="green center">1320</td>
			<td class="red lasttd center">1034</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11139526,0" class="icommentjs icon16" href="/hell-on-wheels-s05e06-hdtv-x264-killers-ettv-t11139526.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hell-on-wheels-s05e06-hdtv-x264-killers-ettv-t11139526.html" class="cellMainLink">Hell on Wheels S05E06 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="333563562">318.11 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="23 Aug 2015, 02:04">3&nbsp;days</span></td>
			<td class="green center">1660</td>
			<td class="red lasttd center">548</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11153050,0" class="icommentjs icon16" href="/rizzoli-and-isles-s06e11-hdtv-x264-lol-ettv-t11153050.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/rizzoli-and-isles-s06e11-hdtv-x264-lol-ettv-t11153050.html" class="cellMainLink">Rizzoli and Isles S06E11 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="313335718">298.82 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="26 Aug 2015, 02:03">22&nbsp;hours</span></td>
			<td class="green center">1256</td>
			<td class="red lasttd center">587</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11149783,0" class="icommentjs icon16" href="/fear-the-walking-dead-s01e01-720p-5-1ch-web-dl-reenc-deejayahmed-t11149783.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fear-the-walking-dead-s01e01-720p-5-1ch-web-dl-reenc-deejayahmed-t11149783.html" class="cellMainLink">Fear The Walking Dead S01E01 720p 5.1Ch Web-DL ReEnc DeeJayAhmed</a></div>
			</td>
			<td class="nobr center" data-sort="551124443">525.59 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Aug 2015, 08:05">1&nbsp;day</span></td>
			<td class="green center">1210</td>
			<td class="red lasttd center">426</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11155329,0" class="icommentjs icon16" href="/hard-knocks-s10e03-houston-texans-720p-t11155329.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hard-knocks-s10e03-houston-texans-720p-t11155329.html" class="cellMainLink">Hard Knocks S10E03 Houston Texans 720p</a></div>
			</td>
			<td class="nobr center" data-sort="1827857593">1.7 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="26 Aug 2015, 13:22">10&nbsp;hours</span></td>
			<td class="green center">327</td>
			<td class="red lasttd center">926</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11145230,0" class="icommentjs icon16" href="/humans-s01-season-1-complete-720p-hdtv-x265-aac-e-subs-gwc-t11145230.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/humans-s01-season-1-complete-720p-hdtv-x265-aac-e-subs-gwc-t11145230.html" class="cellMainLink">Humans S01 Season 1 Complete 720p HDTV x265 AAC E-Subs [GWC]</a></div>
			</td>
			<td class="nobr center" data-sort="1168085873">1.09 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="24 Aug 2015, 06:59">2&nbsp;days</span></td>
			<td class="green center">537</td>
			<td class="red lasttd center">599</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11144080,0" class="icommentjs icon16" href="/motÃ¶rhead-bad-magic-2015-ak-t11144080.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/motÃ¶rhead-bad-magic-2015-ak-t11144080.html" class="cellMainLink">MotÃ¶rhead - Bad Magic (2015)ak</a></div>
			</td>
			<td class="nobr center" data-sort="110101177">105 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="24 Aug 2015, 02:09">2&nbsp;days</span></td>
			<td class="green center">452</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11151756,0" class="icommentjs icon16" href="/va-metallica-first-5-covered-2015-320ak-t11151756.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-metallica-first-5-covered-2015-320ak-t11151756.html" class="cellMainLink">VA- Metallica First 5: Covered 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="624416423">595.49 <span>MB</span></td>
			<td class="center">53</td>
			<td class="center"><span title="25 Aug 2015, 18:26">1&nbsp;day</span></td>
			<td class="green center">321</td>
			<td class="red lasttd center">190</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11145534,0" class="icommentjs icon16" href="/the-beatles-the-beatles-t11145534.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-beatles-the-beatles-t11145534.html" class="cellMainLink">The Beatles - The Beatles</a></div>
			</td>
			<td class="nobr center" data-sort="218614550">208.49 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="24 Aug 2015, 08:24">2&nbsp;days</span></td>
			<td class="green center">3</td>
			<td class="red lasttd center">405</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11143280,0" class="icommentjs icon16" href="/halsey-badlands-deluxe-edition-2015-cbr-320-kbps-aryan-l33t-t11143280.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/halsey-badlands-deluxe-edition-2015-cbr-320-kbps-aryan-l33t-t11143280.html" class="cellMainLink">Halsey - BADLANDS (Deluxe Edition) (2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center" data-sort="134964015">128.71 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="23 Aug 2015, 20:25">3&nbsp;days</span></td>
			<td class="green center">347</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11151094,0" class="icommentjs icon16" href="/five-finger-death-punch-got-your-six-2015-ak-t11151094.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/five-finger-death-punch-got-your-six-2015-ak-t11151094.html" class="cellMainLink">Five Finger Death Punch - Got Your Six 2015 ak</a></div>
			</td>
			<td class="nobr center" data-sort="94933834">90.54 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="25 Aug 2015, 14:57">1&nbsp;day</span></td>
			<td class="green center">247</td>
			<td class="red lasttd center">71</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11145868,0" class="icommentjs icon16" href="/the-doobie-brothers-the-warner-bros-years-1971-1983-2015-mp3-320kbps-beolab1700-t11145868.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-doobie-brothers-the-warner-bros-years-1971-1983-2015-mp3-320kbps-beolab1700-t11145868.html" class="cellMainLink">The Doobie Brothers - The Warner Bros Years 1971-1983 (2015) MP3@320kbps Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="988676505">942.88 <span>MB</span></td>
			<td class="center">123</td>
			<td class="center"><span title="24 Aug 2015, 10:08">2&nbsp;days</span></td>
			<td class="green center">206</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11143622,0" class="icommentjs icon16" href="/various-artists-classically-chilled-2015-2cd-s-mp3-cd-rip-t11143622.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-classically-chilled-2015-2cd-s-mp3-cd-rip-t11143622.html" class="cellMainLink">Various Artists - Classically Chilled (2015) 2CD&#039;s @ MP3 / CD Rip</a></div>
			</td>
			<td class="nobr center" data-sort="380461433">362.84 <span>MB</span></td>
			<td class="center">49</td>
			<td class="center"><span title="23 Aug 2015, 22:58">3&nbsp;days</span></td>
			<td class="green center">216</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11156104,0" class="icommentjs icon16" href="/billboard-hot-100-singles-chart-05th-september-2015-rz-rg-mp3-320kbps-glodls-t11156104.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-hot-100-singles-chart-05th-september-2015-rz-rg-mp3-320kbps-glodls-t11156104.html" class="cellMainLink">Billboard Hot 100 Singles Chart (05th September 2015) RZ-RG [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="877544115">836.89 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center"><span title="26 Aug 2015, 16:37">7&nbsp;hours</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">187</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11155938,0" class="icommentjs icon16" href="/va-billboard-hot-100-singles-chart-05th-sep-2015-cbr-320-kbps-aryan-l33t-t11155938.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-billboard-hot-100-singles-chart-05th-sep-2015-cbr-320-kbps-aryan-l33t-t11155938.html" class="cellMainLink">VA - Billboard Hot 100 Singles Chart (05th Sep 2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center" data-sort="873908058">833.42 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center"><span title="26 Aug 2015, 16:00">8&nbsp;hours</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">206</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11154229,0" class="icommentjs icon16" href="/va-rock-collection-1980-2015-mp3-320-kb-s-t11154229.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-rock-collection-1980-2015-mp3-320-kb-s-t11154229.html" class="cellMainLink">VA - Rock Collection 1980 (2015) MP3 320 Kb/s</a></div>
			</td>
			<td class="nobr center" data-sort="2898409605">2.7 <span>GB</span></td>
			<td class="center">270</td>
			<td class="center"><span title="26 Aug 2015, 08:43">15&nbsp;hours</span></td>
			<td class="green center">132</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-avalanches-since-i-left-you-t11145364.html" class="cellMainLink">The Avalanches - Since I Left You</a></div>
			</td>
			<td class="nobr center" data-sort="147289313">140.47 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="24 Aug 2015, 07:33">2&nbsp;days</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">182</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/beach-house-teen-dream-t11145447.html" class="cellMainLink">Beach House - Teen Dream</a></div>
			</td>
			<td class="nobr center" data-sort="114234120">108.94 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="24 Aug 2015, 07:55">2&nbsp;days</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">144</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11145075,0" class="icommentjs icon16" href="/automator-a-much-better-tomorrow-t11145075.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/automator-a-much-better-tomorrow-t11145075.html" class="cellMainLink">Automator - A Much Better Tomorrow</a></div>
			</td>
			<td class="nobr center" data-sort="124693170">118.92 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="24 Aug 2015, 06:33">2&nbsp;days</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">108</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11147600,0" class="icommentjs icon16" href="/trance-psy-trance-beatport-trance-pack-24-08-2015-mp3-320-kbps-ukhx-tsp-justify-edm-rg-t11147600.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/trance-psy-trance-beatport-trance-pack-24-08-2015-mp3-320-kbps-ukhx-tsp-justify-edm-rg-t11147600.html" class="cellMainLink">(Trance, Psy Trance) Beatport Trance Pack (24-08-2015) MP3, 320 Kbps (UKHx, TSP, JUSTiFY) [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="4844706299">4.51 <span>GB</span></td>
			<td class="center">549</td>
			<td class="center"><span title="24 Aug 2015, 18:16">2&nbsp;days</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-hollies-changin-times-the-complete-hollies-jan-69-march-73-2015-mp3-320kbps-beolab1700-t11156191.html" class="cellMainLink">The Hollies - Changin Times The Complete Hollies Jan 69-March 73 (2015) MP3@320kbps Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="778634329">742.56 <span>MB</span></td>
			<td class="center">95</td>
			<td class="center"><span title="26 Aug 2015, 17:00">7&nbsp;hours</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">50</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11142784,0" class="icommentjs icon16" href="/subnautica-2083-early-acces-2015-pc-repack-Ð¾Ñ-r-g-freedom-t11142784.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/subnautica-2083-early-acces-2015-pc-repack-Ð¾Ñ-r-g-freedom-t11142784.html" class="cellMainLink">Subnautica [2083 | Early Acces] (2015) PC | RePack Ð¾Ñ R.G. Freedom</a></div>
			</td>
			<td class="nobr center" data-sort="1978874397">1.84 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="23 Aug 2015, 17:37">3&nbsp;days</span></td>
			<td class="green center">349</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11146500,0" class="icommentjs icon16" href="/the-witcher-3-wild-hunt-patch-v1-08-3-gog-t11146500.html#comment"> <em class="iconvalue">36</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-witcher-3-wild-hunt-patch-v1-08-3-gog-t11146500.html" class="cellMainLink">The Witcher 3: Wild Hunt (Patch v1.08.3) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="2194774991">2.04 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="24 Aug 2015, 13:05">2&nbsp;days</span></td>
			<td class="green center">267</td>
			<td class="red lasttd center">146</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11150836,0" class="icommentjs icon16" href="/pillars-of-eternity-the-white-march-part-i-codex-t11150836.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pillars-of-eternity-the-white-march-part-i-codex-t11150836.html" class="cellMainLink">Pillars of Eternity The White March Part I-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="9790272720">9.12 <span>GB</span></td>
			<td class="center">68</td>
			<td class="center"><span title="25 Aug 2015, 13:47">1&nbsp;day</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">183</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11147104,0" class="icommentjs icon16" href="/i-shall-remain-codex-t11147104.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/i-shall-remain-codex-t11147104.html" class="cellMainLink">I.Shall.Remain-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="790894181">754.26 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="24 Aug 2015, 15:58">2&nbsp;days</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11146943,0" class="icommentjs icon16" href="/dragon-ball-xenoverse-bundle-edition-v1-07-dlcs-multi9-fitgirl-repack-100-lossless-t11146943.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/dragon-ball-xenoverse-bundle-edition-v1-07-dlcs-multi9-fitgirl-repack-100-lossless-t11146943.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dragon-ball-xenoverse-bundle-edition-v1-07-dlcs-multi9-fitgirl-repack-100-lossless-t11146943.html" class="cellMainLink">Dragon Ball Xenoverse: Bundle Edition (v1.07 + DLCs, MULTI9) [FitGirl Repack, 100% Lossless]</a></div>
			</td>
			<td class="nobr center" data-sort="7647602796">7.12 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="24 Aug 2015, 15:14">2&nbsp;days</span></td>
			<td class="green center">35</td>
			<td class="red lasttd center">158</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11150754,0" class="icommentjs icon16" href="/alien-rage-unlimited-update-6-2013-Ð Ð¡-repack-Ð¾Ñ-nemos-t11150754.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/alien-rage-unlimited-update-6-2013-Ð Ð¡-repack-Ð¾Ñ-nemos-t11150754.html" class="cellMainLink">Alien Rage - Unlimited [Update 6] (2013) Ð Ð¡ | Repack Ð¾Ñ =nemos=</a></div>
			</td>
			<td class="nobr center" data-sort="1583525884">1.47 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="25 Aug 2015, 13:22">1&nbsp;day</span></td>
			<td class="green center">146</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11146304,0" class="icommentjs icon16" href="/homeworld-remastered-collection-v-1-30-2015-pc-steam-rip-Ð¾Ñ-let-splay-t11146304.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/homeworld-remastered-collection-v-1-30-2015-pc-steam-rip-Ð¾Ñ-let-splay-t11146304.html" class="cellMainLink">Homeworld Remastered Collection [v 1.30] (2015) PC | Steam-Rip Ð¾Ñ Let&#039;sPlay</a></div>
			</td>
			<td class="nobr center" data-sort="8990946098">8.37 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="24 Aug 2015, 12:12">2&nbsp;days</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">71</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11142763,0" class="icommentjs icon16" href="/project-cars-update-6-dlc-s-2015-pc-repack-Ð¾Ñ-xatab-t11142763.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/project-cars-update-6-dlc-s-2015-pc-repack-Ð¾Ñ-xatab-t11142763.html" class="cellMainLink">Project CARS [Update 6 + DLC&#039;s] (2015) PC | RePack Ð¾Ñ xatab</a></div>
			</td>
			<td class="nobr center" data-sort="13668531352">12.73 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="23 Aug 2015, 17:33">3&nbsp;days</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11151710,0" class="icommentjs icon16" href="/crookz-the-big-heist-codex-t11151710.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/crookz-the-big-heist-codex-t11151710.html" class="cellMainLink">Crookz The Big Heist - CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="3521811844">3.28 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="25 Aug 2015, 18:10">1&nbsp;day</span></td>
			<td class="green center">126</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11150690,0" class="icommentjs icon16" href="/need-for-speed-complete-pack-rldgames-t11150690.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/need-for-speed-complete-pack-rldgames-t11150690.html" class="cellMainLink">Need For Speed Complete Pack-RLDGAMES</a></div>
			</td>
			<td class="nobr center" data-sort="76848105295">71.57 <span>GB</span></td>
			<td class="center">117</td>
			<td class="center"><span title="25 Aug 2015, 13:05">1&nbsp;day</span></td>
			<td class="green center">3</td>
			<td class="red lasttd center">92</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11153222,0" class="icommentjs icon16" href="/corpse-of-discovery-reloaded-t11153222.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/corpse-of-discovery-reloaded-t11153222.html" class="cellMainLink">Corpse.of.Discovery-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="2994878999">2.79 <span>GB</span></td>
			<td class="center">62</td>
			<td class="center"><span title="26 Aug 2015, 03:03">21&nbsp;hours</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11151745,0" class="icommentjs icon16" href="/hegemony-iii-clash-of-the-ancients-codex-t11151745.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/hegemony-iii-clash-of-the-ancients-codex-t11151745.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/hegemony-iii-clash-of-the-ancients-codex-t11151745.html" class="cellMainLink">Hegemony III Clash of the Ancients-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="1476564621">1.38 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="25 Aug 2015, 18:23">1&nbsp;day</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11140064,0" class="icommentjs icon16" href="/risen-3-titan-lords-enhanced-edition-black-box-t11140064.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/risen-3-titan-lords-enhanced-edition-black-box-t11140064.html" class="cellMainLink">Risen 3 Titan Lords Enhanced Edition-Black Box</a></div>
			</td>
			<td class="nobr center" data-sort="4432121166">4.13 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="23 Aug 2015, 04:43">3&nbsp;days</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11149291,0" class="icommentjs icon16" href="/grandia-ii-anniversary-edition-proper-tinyiso-t11149291.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/grandia-ii-anniversary-edition-proper-tinyiso-t11149291.html" class="cellMainLink">Grandia II Anniversary Edition PROPER-TiNYiSO</a></div>
			</td>
			<td class="nobr center" data-sort="2005813665">1.87 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Aug 2015, 05:17">1&nbsp;day</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11146860,0" class="icommentjs icon16" href="/assetto-corsa-v1-1-6-dream-pack-1-dlc-multi5-fitgirl-repack-100-lossless-t11146860.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/assetto-corsa-v1-1-6-dream-pack-1-dlc-multi5-fitgirl-repack-100-lossless-t11146860.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assetto-corsa-v1-1-6-dream-pack-1-dlc-multi5-fitgirl-repack-100-lossless-t11146860.html" class="cellMainLink">Assetto Corsa (v1.1.6 + Dream Pack #1 DLC, MULTI5) [FitGirl Repack, 100% Lossless]</a></div>
			</td>
			<td class="nobr center" data-sort="4735258485">4.41 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="24 Aug 2015, 14:52">2&nbsp;days</span></td>
			<td class="green center">10</td>
			<td class="red lasttd center">26</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11145787,0" class="icommentjs icon16" href="/windows-7-ultimate-sp1-x86-x64-en-us-oem-esd-aug2015-pre-activation-team-os-t11145787.html#comment"> <em class="iconvalue">40</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-ultimate-sp1-x86-x64-en-us-oem-esd-aug2015-pre-activation-team-os-t11145787.html" class="cellMainLink">Windows 7 Ultimate Sp1 x86-x64 En-Us OEM ESD Aug2015 Pre-Activation=-{TEAM OS}=</a></div>
			</td>
			<td class="nobr center" data-sort="4169095969">3.88 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="24 Aug 2015, 09:39">2&nbsp;days</span></td>
			<td class="green center">246</td>
			<td class="red lasttd center">425</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11145610,0" class="icommentjs icon16" href="/microsoft-windows-10-aio-6in1-x86-x64-iso-ita-tntvillage-t11145610.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-windows-10-aio-6in1-x86-x64-iso-ita-tntvillage-t11145610.html" class="cellMainLink">Microsoft Windows 10 AIO 6in1 x86/x64, [Iso - Ita] [TNTVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="3985072045">3.71 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Aug 2015, 08:48">2&nbsp;days</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">376</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11153069,0" class="icommentjs icon16" href="/logic-pro-x-10-2-0-os-x-mas-tnt-dada-t11153069.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/logic-pro-x-10-2-0-os-x-mas-tnt-dada-t11153069.html" class="cellMainLink">Logic Pro X 10.2.0 OS X [MAS][TNT][dada]</a></div>
			</td>
			<td class="nobr center" data-sort="1219460231">1.14 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="26 Aug 2015, 02:08">21&nbsp;hours</span></td>
			<td class="green center">238</td>
			<td class="red lasttd center">110</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11141705,0" class="icommentjs icon16" href="/parallels-desktop-business-edition-11-0-0-31193-pre-activated-mac-osx-appzdam-t11141705.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/parallels-desktop-business-edition-11-0-0-31193-pre-activated-mac-osx-appzdam-t11141705.html" class="cellMainLink">Parallels Desktop Business Edition 11.0.0 (31193) Pre-Activated [Mac OSX] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="372128881">354.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Aug 2015, 13:02">3&nbsp;days</span></td>
			<td class="green center">269</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11143067,0" class="icommentjs icon16" href="/windows-10-pro-rtm-x86-office-2013-preactivated-teamos-t11143067.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-rtm-x86-office-2013-preactivated-teamos-t11143067.html" class="cellMainLink">Windows 10 Pro Rtm (x86) Office 2013 Preactivated -=teamos=-</a></div>
			</td>
			<td class="nobr center" data-sort="6093444781">5.67 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="23 Aug 2015, 19:13">3&nbsp;days</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">138</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11151950,0" class="icommentjs icon16" href="/vmware-workstation-pro-12-0-0-build-2985596-deepstatus-t11151950.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/vmware-workstation-pro-12-0-0-build-2985596-deepstatus-t11151950.html" class="cellMainLink">VMware Workstation Pro 12.0.0 Build 2985596 [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="298569025">284.74 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="25 Aug 2015, 19:09">1&nbsp;day</span></td>
			<td class="green center">149</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType txtType">
                    <a href="/keys-for-eset-nod32-kaspersky-avast-dr-web-avira-26-08-15-2015-pc-t11155675.html" class="cellMainLink">Keys For ESET NOD32, Kaspersky, Avast, Dr.Web, Avira [26.08.15 ] (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="13595654">12.97 <span>MB</span></td>
			<td class="center">549</td>
			<td class="center"><span title="26 Aug 2015, 14:58">9&nbsp;hours</span></td>
			<td class="green center">89</td>
			<td class="red lasttd center">119</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11147499,0" class="icommentjs icon16" href="/k-lite-codec-pack-11-4-0-mega-full-basic-standard-2015-pc-t11147499.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/k-lite-codec-pack-11-4-0-mega-full-basic-standard-2015-pc-t11147499.html" class="cellMainLink">K-Lite Codec Pack 11.4.0 Mega/Full/Basic/Standard (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="120087577">114.52 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Aug 2015, 17:51">2&nbsp;days</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11149702,0" class="icommentjs icon16" href="/windows-8-1-pro-vl-update-3-x64-en-us-v-2-oem-esd-aug-2015-pre-activate-team-os-t11149702.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-8-1-pro-vl-update-3-x64-en-us-v-2-oem-esd-aug-2015-pre-activate-team-os-t11149702.html" class="cellMainLink">Windows 8.1 Pro Vl Update 3 x64 En-Us V.2 OEM ESD Aug 2015 Pre-Activate-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="4234107713">3.94 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="25 Aug 2015, 07:41">1&nbsp;day</span></td>
			<td class="green center">43</td>
			<td class="red lasttd center">133</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11151397,0" class="icommentjs icon16" href="/top-paid-android-apps-games-themes-pack-25-august-2015-android-zone-t11151397.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/top-paid-android-apps-games-themes-pack-25-august-2015-android-zone-t11151397.html" class="cellMainLink">Top Paid Android Apps, Games &amp; Themes Pack - 25 August 2015 [ANDROID-ZONE]</a></div>
			</td>
			<td class="nobr center" data-sort="1098668544">1.02 <span>GB</span></td>
			<td class="center">62</td>
			<td class="center"><span title="25 Aug 2015, 16:28">1&nbsp;day</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">104</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11152544,0" class="icommentjs icon16" href="/vmware-fusion-v8-0-0-macosx-incl-keymaker-embrace-deepstatus-t11152544.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/vmware-fusion-v8-0-0-macosx-incl-keymaker-embrace-deepstatus-t11152544.html" class="cellMainLink">VMware Fusion v8.0.0 MacOSX Incl.Keymaker-EMBRACE [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="375252176">357.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Aug 2015, 22:20">1&nbsp;day</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11140759,0" class="icommentjs icon16" href="/magix-video-pro-x7-14-0-0-96-64-bit-content-repack-pre-activated-pooshock-appzdam-t11140759.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/magix-video-pro-x7-14-0-0-96-64-bit-content-repack-pre-activated-pooshock-appzdam-t11140759.html" class="cellMainLink">MAGIX Video Pro X7 14.0.0.96 [64 bit] + Content RePack + Pre-Activated (pooshock) - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="4795321326">4.47 <span>GB</span></td>
			<td class="center">18788</td>
			<td class="center"><span title="23 Aug 2015, 08:17">3&nbsp;days</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11143513,0" class="icommentjs icon16" href="/big-fish-audio-and-dieguis-productions-motion-multiformat-t11143513.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/big-fish-audio-and-dieguis-productions-motion-multiformat-t11143513.html" class="cellMainLink">Big Fish Audio and Dieguis Productions Motion MULTiFORMAT</a></div>
			</td>
			<td class="nobr center" data-sort="12635383301">11.77 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="23 Aug 2015, 22:11">3&nbsp;days</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11156624,0" class="icommentjs icon16" href="/ableton-live-suite-v9-2-2-win-x86x64-incl-patch-io-deepstatus-t11156624.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ableton-live-suite-v9-2-2-win-x86x64-incl-patch-io-deepstatus-t11156624.html" class="cellMainLink">Ableton Live Suite v9.2.2 WiN x86x64 Incl.Patch-iO [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="1462611726">1.36 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="26 Aug 2015, 18:27">5&nbsp;hours</span></td>
			<td class="green center">21</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11153608,0" class="icommentjs icon16" href="/windows-10-aio-20in1-greek-full-updated11-8-2015-by-whitedeath-teamos-t11153608.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-aio-20in1-greek-full-updated11-8-2015-by-whitedeath-teamos-t11153608.html" class="cellMainLink">Windows 10 AIO 20IN1 Greek Full Updated11/8/2015 by:WhiteDeath[TeamOS]</a></div>
			</td>
			<td class="nobr center" data-sort="4421384192">4.12 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Aug 2015, 05:27">18&nbsp;hours</span></td>
			<td class="green center">13</td>
			<td class="red lasttd center">36</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11143073,0" class="icommentjs icon16" href="/dragon-ball-super-007-vostfr-sans-horloge-hd-720p-x264-french-subs-mp4-t11143073.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-007-vostfr-sans-horloge-hd-720p-x264-french-subs-mp4-t11143073.html" class="cellMainLink">Dragon Ball Super 007 VOSTFR - Sans Horloge [HD][720p] x264 FRENCH SUBS.mp4</a></div>
			</td>
			<td class="nobr center" data-sort="278406521">265.51 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Aug 2015, 19:15">3&nbsp;days</span></td>
			<td class="green center">1384</td>
			<td class="red lasttd center">84</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-overlord-08-raw-atx-1280x720-x264-aac-mp4-t11151871.html" class="cellMainLink">[Leopard-Raws] Overlord - 08 RAW (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="360390033">343.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Aug 2015, 18:50">1&nbsp;day</span></td>
			<td class="green center">1225</td>
			<td class="red lasttd center">208</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11156254,0" class="icommentjs icon16" href="/horriblesubs-kuusen-madoushi-kouhosei-no-kyoukan-08-720p-mkv-t11156254.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-kuusen-madoushi-kouhosei-no-kyoukan-08-720p-mkv-t11156254.html" class="cellMainLink">[HorribleSubs] Kuusen Madoushi Kouhosei no Kyoukan - 08 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="397176742">378.78 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Aug 2015, 17:15">6&nbsp;hours</span></td>
			<td class="green center">449</td>
			<td class="red lasttd center">798</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11141481,0" class="icommentjs icon16" href="/animerg-dragon-ball-super-007-1080p-phr0sty-mkv-t11141481.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-007-1080p-phr0sty-mkv-t11141481.html" class="cellMainLink">[AnimeRG] Dragon Ball Super 007 - 1080p [Phr0stY].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="621055428">592.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Aug 2015, 12:17">3&nbsp;days</span></td>
			<td class="green center">772</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/island-one-piece-706-vostfr-v1-8bit-720p-a1c5f3d4-mp4-t11142152.html" class="cellMainLink">[ISLAND]One Piece 706 [VOSTFR] [V1] [8bit] [720p] [A1C5F3D4].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="409015900">390.07 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Aug 2015, 14:56">3&nbsp;days</span></td>
			<td class="green center">563</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11140545,0" class="icommentjs icon16" href="/one-piece-706-eng-sub-720p-l-mbert-t11140545.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-706-eng-sub-720p-l-mbert-t11140545.html" class="cellMainLink">One Piece 706 [EnG SuB] 720p L@mBerT</a></div>
			</td>
			<td class="nobr center" data-sort="113807849">108.54 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="23 Aug 2015, 07:16">3&nbsp;days</span></td>
			<td class="green center">219</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-non-non-biyori-repeat-08-eb85f36a-mkv-t11150331.html" class="cellMainLink">[Vivid] Non Non Biyori Repeat - 08 [EB85F36A].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="333715124">318.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="25 Aug 2015, 11:10">1&nbsp;day</span></td>
			<td class="green center">196</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-ore-monogatari-20-8a3d7836-mkv-t11146640.html" class="cellMainLink">[FFF] Ore Monogatari!! - 20 [8A3D7836].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="259505719">247.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Aug 2015, 13:40">2&nbsp;days</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-psycho-pass-2-bd-720p-aac-t11145676.html" class="cellMainLink">[Commie] Psycho-Pass 2 [BD 720p AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="8019479945">7.47 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="24 Aug 2015, 09:10">2&nbsp;days</span></td>
			<td class="green center">45</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11148537,0" class="icommentjs icon16" href="/animerg-assassination-classroom-ansatsu-kyoushitsu-batch-720p-dual-audio-kami-t11148537.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-assassination-classroom-ansatsu-kyoushitsu-batch-720p-dual-audio-kami-t11148537.html" class="cellMainLink">[AnimeRG] Assassination Classroom - Ansatsu Kyoushitsu - Batch [720p] (Dual Audio) [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="3204979048">2.98 <span>GB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="25 Aug 2015, 01:15">1&nbsp;day</span></td>
			<td class="green center">17</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-working-07-1080p-76a31547-mkv-t11154218.html" class="cellMainLink">[FFF] Working!!! - 07 [1080p][76A31547].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="509674218">486.06 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="26 Aug 2015, 08:40">15&nbsp;hours</span></td>
			<td class="green center">9</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11149860,0" class="icommentjs icon16" href="/the-tale-of-the-princess-kaguya-2013-1080p-jpn-5-1-eng-5-1-blu-ray-studio-ghibli-t11149860.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-tale-of-the-princess-kaguya-2013-1080p-jpn-5-1-eng-5-1-blu-ray-studio-ghibli-t11149860.html" class="cellMainLink">The Tale of the Princess Kaguya (2013) 1080p [Jpn 5.1 &amp; Eng 5.1] Blu-ray (Studio Ghibli)</a></div>
			</td>
			<td class="nobr center" data-sort="3172620783">2.95 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="25 Aug 2015, 08:31">1&nbsp;day</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/reinforce-the-rolling-girls-bdrip-1920x1080-x264-flac-t11143352.html" class="cellMainLink">[ReinForce] The Rolling Girls (BDRip 1920x1080 x264 FLAC)</a></div>
			</td>
			<td class="nobr center" data-sort="22675100004">21.12 <span>GB</span></td>
			<td class="center">39</td>
			<td class="center"><span title="23 Aug 2015, 20:45">3&nbsp;days</span></td>
			<td class="green center">4</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-ore-twintail-ni-narimasu-bd-1080p-aac-t11141306.html" class="cellMainLink">[Commie] Ore, Twintail ni Narimasu. [BD 1080p AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="7807522329">7.27 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="23 Aug 2015, 11:30">3&nbsp;days</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/when-marnie-was-there-2014-1080p-jpn-5-0-subs-blu-ray-studio-ghibli-t11149988.html" class="cellMainLink">When Marnie Was There (2014) 1080p [Jpn 5.0 + Subs] Blu-ray (Studio Ghibli)</a></div>
			</td>
			<td class="nobr center" data-sort="2417512866">2.25 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="25 Aug 2015, 09:18">1&nbsp;day</span></td>
			<td class="green center">5</td>
			<td class="red lasttd center">11</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11143749,0" class="icommentjs icon16" href="/computer-gamer-magazines-august-24-2015-true-pdf-t11143749.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/computer-gamer-magazines-august-24-2015-true-pdf-t11143749.html" class="cellMainLink">Computer &amp; Gamer Magazines - August 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="195895176">186.82 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="24 Aug 2015, 00:10">2&nbsp;days</span></td>
			<td class="green center">661</td>
			<td class="red lasttd center">226</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11144321,0" class="icommentjs icon16" href="/mens-magazines-bundle-august-24-2015-true-pdf-t11144321.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/mens-magazines-bundle-august-24-2015-true-pdf-t11144321.html" class="cellMainLink">Mens Magazines Bundle - August 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="255762430">243.91 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="24 Aug 2015, 03:07">2&nbsp;days</span></td>
			<td class="green center">506</td>
			<td class="red lasttd center">186</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11143945,0" class="icommentjs icon16" href="/assorted-magazines-bundle-august-24-2015-true-pdf-t11143945.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-august-24-2015-true-pdf-t11143945.html" class="cellMainLink">Assorted Magazines Bundle - August 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="373985263">356.66 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span title="24 Aug 2015, 01:42">2&nbsp;days</span></td>
			<td class="green center">423</td>
			<td class="red lasttd center">268</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11141426,0" class="icommentjs icon16" href="/c-programming-from-problem-analysis-to-program-design-7th-edition-a4-t11141426.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/c-programming-from-problem-analysis-to-program-design-7th-edition-a4-t11141426.html" class="cellMainLink">C++ Programming From Problem Analysis to Program Design 7th Edition[A4]</a></div>
			</td>
			<td class="nobr center" data-sort="268443202">256.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Aug 2015, 12:02">3&nbsp;days</span></td>
			<td class="green center">448</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11143442,0" class="icommentjs icon16" href="/automate-the-boring-stuff-with-python-practical-programming-for-total-beginners-1st-edition-2015-pdf-epub-mobi-gooner-t11143442.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automate-the-boring-stuff-with-python-practical-programming-for-total-beginners-1st-edition-2015-pdf-epub-mobi-gooner-t11143442.html" class="cellMainLink">Automate the Boring Stuff with Python - Practical Programming for Total Beginners - 1st Edition (2015) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="34010441">32.43 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="23 Aug 2015, 21:21">3&nbsp;days</span></td>
			<td class="green center">415</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11144658,0" class="icommentjs icon16" href="/science-magazines-bundle-august-24-2015-true-pdf-t11144658.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/science-magazines-bundle-august-24-2015-true-pdf-t11144658.html" class="cellMainLink">Science Magazines Bundle - August 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="156408865">149.16 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="24 Aug 2015, 04:36">2&nbsp;days</span></td>
			<td class="green center">342</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11153996,0" class="icommentjs icon16" href="/automobile-magazines-bundle-august-26-2015-true-pdf-t11153996.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-bundle-august-26-2015-true-pdf-t11153996.html" class="cellMainLink">Automobile Magazines Bundle - August 26 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="352478678">336.15 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="26 Aug 2015, 07:27">16&nbsp;hours</span></td>
			<td class="green center">189</td>
			<td class="red lasttd center">145</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11146855,0" class="icommentjs icon16" href="/atlas-a-z-a-pocket-guide-to-the-world-today-6th-edition-dk-publishing-2015-pdf-gooner-t11146855.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/atlas-a-z-a-pocket-guide-to-the-world-today-6th-edition-dk-publishing-2015-pdf-gooner-t11146855.html" class="cellMainLink">Atlas A-Z - A Pocket Guide to the World Today - 6th Edition (DK Publishing) (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="63622140">60.67 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Aug 2015, 14:49">2&nbsp;days</span></td>
			<td class="green center">258</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11152469,0" class="icommentjs icon16" href="/gun-magazines-bundle-august-26-2015-true-pdf-t11152469.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/gun-magazines-bundle-august-26-2015-true-pdf-t11152469.html" class="cellMainLink">Gun Magazines Bundle - August 26 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="229652871">219.01 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="25 Aug 2015, 21:36">1&nbsp;day</span></td>
			<td class="green center">153</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11153972,0" class="icommentjs icon16" href="/feng-shui-and-vaastu-books-collection-multi-languages-mantesh-t11153972.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/feng-shui-and-vaastu-books-collection-multi-languages-mantesh-t11153972.html" class="cellMainLink">Feng Shui And Vaastu Books collection (Multi Languages) - Mantesh</a></div>
			</td>
			<td class="nobr center" data-sort="496768078">473.75 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span title="26 Aug 2015, 07:18">16&nbsp;hours</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11146807,0" class="icommentjs icon16" href="/goodreads-new-science-fiction-ebook-releases-epub-t11146807.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/goodreads-new-science-fiction-ebook-releases-epub-t11146807.html" class="cellMainLink">Goodreads New Science Fiction eBook Releases (epub)</a></div>
			</td>
			<td class="nobr center" data-sort="23543674">22.45 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="24 Aug 2015, 14:38">2&nbsp;days</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11148805,0" class="icommentjs icon16" href="/womens-magazines-bundle-august-25-2015-true-pdf-t11148805.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-august-25-2015-true-pdf-t11148805.html" class="cellMainLink">Womens Magazines Bundle - August 25 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="427928266">408.1 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="25 Aug 2015, 02:57">1&nbsp;day</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11145723,0" class="icommentjs icon16" href="/linux-voice-2014-full-year-collection-t11145723.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/linux-voice-2014-full-year-collection-t11145723.html" class="cellMainLink">Linux Voice 2014 Full Year Collection</a></div>
			</td>
			<td class="nobr center" data-sort="613212588">584.81 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="24 Aug 2015, 09:18">2&nbsp;days</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/motorcycle-magazines-bundle-august-27-2015-true-pdf-t11156949.html" class="cellMainLink">Motorcycle Magazines Bundle - August 27 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="227732429">217.18 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="26 Aug 2015, 19:50">4&nbsp;hours</span></td>
			<td class="green center">30</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11156748,0" class="icommentjs icon16" href="/marvel-week-08-26-2015-nem-t11156748.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-08-26-2015-nem-t11156748.html" class="cellMainLink">Marvel Week+ (08-26-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="611688823">583.35 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="26 Aug 2015, 18:57">5&nbsp;hours</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">42</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11143318,0" class="icommentjs icon16" href="/eric-clapton-just-one-night-2014-24-192-hd-flac-t11143318.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/eric-clapton-just-one-night-2014-24-192-hd-flac-t11143318.html" class="cellMainLink">Eric Clapton - Just One Night (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3764263575">3.51 <span>GB</span></td>
			<td class="center">34</td>
			<td class="center"><span title="23 Aug 2015, 20:34">3&nbsp;days</span></td>
			<td class="green center">141</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11150499,0" class="icommentjs icon16" href="/chicago-the-studio-albums-1979-2008-10cd-box-2015-flac-t11150499.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/chicago-the-studio-albums-1979-2008-10cd-box-2015-flac-t11150499.html" class="cellMainLink">Chicago - The Studio Albums 1979-2008 (10CD-Box 2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="4179081497">3.89 <span>GB</span></td>
			<td class="center">252</td>
			<td class="center"><span title="25 Aug 2015, 12:18">1&nbsp;day</span></td>
			<td class="green center">92</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11150855,0" class="icommentjs icon16" href="/top-100-60-s-rock-albums-by-ultimateclassicrock-cd-s-01-25-flac-t11150855.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/top-100-60-s-rock-albums-by-ultimateclassicrock-cd-s-01-25-flac-t11150855.html" class="cellMainLink">Top 100 &#039;60&#039;s Rock Albums by UltimateClassicRock - CD&#039;s 01-25 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="6779482961">6.31 <span>GB</span></td>
			<td class="center">655</td>
			<td class="center"><span title="25 Aug 2015, 13:54">1&nbsp;day</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">124</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-linn-super-audio-collection-vol-8-2015-flac-24-bit-t11146769.html" class="cellMainLink">VA - LINN: Super Audio Collection Vol. 8 (2015) FLAC | 24-bit</a></div>
			</td>
			<td class="nobr center" data-sort="2156606879">2.01 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="24 Aug 2015, 14:27">2&nbsp;days</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11142910,0" class="icommentjs icon16" href="/the-blues-band-1980-rock-goes-to-college-2015-flac-t11142910.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-blues-band-1980-rock-goes-to-college-2015-flac-t11142910.html" class="cellMainLink">The Blues Band - 1980 Rock Goes to College (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="325910279">310.81 <span>MB</span></td>
			<td class="center">35</td>
			<td class="center"><span title="23 Aug 2015, 18:21">3&nbsp;days</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11141482,0" class="icommentjs icon16" href="/bob-dylan-2014-playlist-the-very-best-of-bob-dylan-eac-flac-t11141482.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bob-dylan-2014-playlist-the-very-best-of-bob-dylan-eac-flac-t11141482.html" class="cellMainLink">Bob Dylan - 2014 - Playlist: The Very Best Of Bob Dylan [EAC FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="522320837">498.12 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span title="23 Aug 2015, 12:18">3&nbsp;days</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11145743,0" class="icommentjs icon16" href="/rick-james-street-songs-2012-24-96-hd-flac-t11145743.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rick-james-street-songs-2012-24-96-hd-flac-t11145743.html" class="cellMainLink">Rick James - Street Songs (2012) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="827736680">789.39 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="24 Aug 2015, 09:22">2&nbsp;days</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rock-collection-1980-flac-t11156568.html" class="cellMainLink">Rock Collection 1980 (FLAC)</a></div>
			</td>
			<td class="nobr center" data-sort="8357037160">7.78 <span>GB</span></td>
			<td class="center">270</td>
			<td class="center"><span title="26 Aug 2015, 18:16">5&nbsp;hours</span></td>
			<td class="green center">43</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11152026,0" class="icommentjs icon16" href="/va-cafe-cuba-50-original-cuban-classics-double-cd-flac-t11152026.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-cafe-cuba-50-original-cuban-classics-double-cd-flac-t11152026.html" class="cellMainLink">VA - Cafe Cuba - 50 Original Cuban Classics Double CD [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="955854936">911.57 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span title="25 Aug 2015, 19:23">1&nbsp;day</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ultimate-movies-4cd-2015-flac-t11156572.html" class="cellMainLink">VA - Ultimate... Movies [4CD] (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1929675603">1.8 <span>GB</span></td>
			<td class="center">107</td>
			<td class="center"><span title="26 Aug 2015, 18:17">5&nbsp;hours</span></td>
			<td class="green center">45</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/armin-van-buuren-a-state-of-trance-at-ushuaia-ibiza-2015-web-2015-ardi3598-t11156577.html" class="cellMainLink">Armin van Buuren - A State Of Trance at Ushuaia, Ibiza 2015 [WEB] (2015) [ARDI3598]</a></div>
			</td>
			<td class="nobr center" data-sort="2287934303">2.13 <span>GB</span></td>
			<td class="center">48</td>
			<td class="center"><span title="26 Aug 2015, 18:18">5&nbsp;hours</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11150198,0" class="icommentjs icon16" href="/va-the-rough-guide-to-cuban-street-party-flac-t11150198.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-rough-guide-to-cuban-street-party-flac-t11150198.html" class="cellMainLink">VA - The Rough Guide to Cuban Street Party [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="491764868">468.98 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="25 Aug 2015, 10:22">1&nbsp;day</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11152403,0" class="icommentjs icon16" href="/va-the-essential-cuban-anthology-double-cd-flac-t11152403.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-essential-cuban-anthology-double-cd-flac-t11152403.html" class="cellMainLink">VA - The Essential Cuban Anthology Double CD [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="943068922">899.38 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span title="25 Aug 2015, 21:00">1&nbsp;day</span></td>
			<td class="green center">40</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rembrandts-the-l-p-flac-mp3-big-papi-friends-theme-1995-t11142891.html" class="cellMainLink">Rembrandts, The - L.P. [FLAC+MP3](Big Papi) Friends Theme 1995</a></div>
			</td>
			<td class="nobr center" data-sort="575429495">548.77 <span>MB</span></td>
			<td class="center">36</td>
			<td class="center"><span title="23 Aug 2015, 18:09">3&nbsp;days</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-studio-33-the-108th-story-2015-flac-t11156562.html" class="cellMainLink">VA - Studio 33 - The 108th Story (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="472859055">450.95 <span>MB</span></td>
			<td class="center">48</td>
			<td class="center"><span title="26 Aug 2015, 18:15">5&nbsp;hours</span></td>
			<td class="green center">25</td>
			<td class="red lasttd center">17</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/torrent-request-month-august-2015/?unread=16827498">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				***Torrent Request Month - August 2015***
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/John.Reese/">John.Reese</a></span></span> <span title="27 Aug 2015, 00:04">1&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=16827497">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/descending/">descending</a></span></span> <span title="27 Aug 2015, 00:04">1&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/igetbooks-house-ebooks/?unread=16827496">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				iGetBooks House of Ebooks.
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/iGetBooks/">iGetBooks</a></span></span> <span title="27 Aug 2015, 00:03">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/latest-book-uploads-v2/?unread=16827494">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Latest Book Uploads V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/iGetBooks/">iGetBooks</a></span></span> <span title="27 Aug 2015, 00:02">3&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/adopt-uploader-program-v11-all-users-help/?unread=16827490">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				**Adopt an uploader Program v11-- For all Users to Help**
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/OptimusPr1me/">OptimusPr1me</a></span></span> <span title="26 Aug 2015, 23:57">7&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/my-first-torrent-please-post-here-and-help-me/?unread=16827481">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				My First Torrent - Please POST Here! and Help Me..
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/OptimusPr1me/">OptimusPr1me</a></span></span> <span title="26 Aug 2015, 23:53">11&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">4&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">4&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/technical-maintenance-march-17/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Technical maintenance (March, 17)
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="17 Mar 2015, 11:46">5&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/Deevious/post/faq-for-deevious-downloads/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> FAQ for Deevious Downloads</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Deevious/">Deevious</a> <span title="26 Aug 2015, 13:33">10&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/magicpotions/post/reply-to-the-the-ashley-madison-hack-by-watertiger/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Reply to the âThe Ashley Madison Hackâ by watertiger</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="26 Aug 2015, 13:30">10&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/The.Blade/post/now-i-am-a-university-student/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Now I&#039;am a University Student</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/The.Blade/">The.Blade</a> <span title="26 Aug 2015, 09:46">14&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/magicpotions/post/key-dreams-part-6-by-watertiger/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Key Dreams part 6 by watertiger</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="25 Aug 2015, 03:00">yesterday</span></span></li>
	<li><a href="/blog/StitchinWitch/post/i-got-99-problems/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> I got 99 Problems...</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/StitchinWitch/">StitchinWitch</a> <span title="24 Aug 2015, 15:53">2&nbsp;days&nbsp;ago</span></span></li>
	<li><a href="/blog/BBCLover/post/i-love-kat/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> I love KAT!</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/BBCLover/">BBCLover</a> <span title="24 Aug 2015, 05:57">2&nbsp;days&nbsp;ago</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/carl%20cox%20global/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				carl cox global
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/porno%2016%20yas/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				porno 16 yas
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/jurassic%20world/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				JURASSIC WORLD
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/qi.s09/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				qi.s09
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/armin%20van%20buuren%20state%20of%20trance%20384/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				armin van buuren state of trance 384
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/.%200/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				. 0
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/tyrant%20s02e11/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				tyrant s02e11
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/supersex%20pdf/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				supersex pdf
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/city%20and%20colour/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				city and colour
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/owl%20eyes/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				owl eyes
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/styx/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				styx
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
