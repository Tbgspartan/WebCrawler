<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.9" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.9" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.9"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.9"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.9"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.9"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><script>(function() {var _fbq = window._fbq || (window._fbq = []);if (!_fbq.loaded) {var fbds = document.createElement('script');fbds.async = true;fbds.src = '//connect.facebook.net/en_US/fbds.js';var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(fbds, s);_fbq.loaded = true;}_fbq.push(['addPixelId', '624029644406124']);})();window._fbq = window._fbq || [];window._fbq.push(['track', 'PixelInitialized', {}]);</script><noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=624029644406124&amp;ev=PixelInitialized" /></noscript>           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.9"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-08-27 06:20:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             19 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/the-jasleen-kaur-incident-has-just-proved-that-we-need-to-stop-âoutragingâ-over-every-damn-thing-244547.html" class=" tint" title="The Jasleen Kaur Incident Has Just Proved That We Need To Stop âOutragingâ Over Every Damn Thing">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-555_1440509411_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-555_1440509411_236x111.jpg"  border="0" alt="The Jasleen Kaur Incident Has Just Proved That We Need To Stop âOutragingâ Over Every Damn Thing"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/the-jasleen-kaur-incident-has-just-proved-that-we-need-to-stop-âoutragingâ-over-every-damn-thing-244547.html" title="The Jasleen Kaur Incident Has Just Proved That We Need To Stop âOutragingâ Over Every Damn Thing">
                            The Jasleen Kaur Incident Has Just Proved That We Need To Stop âOutragingâ Over Every Damn Thing                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>
                        		
						 	<a class='video-btn sprite' href='http://www.indiatimes.com/news/world/shocking-video-captures-the-moment-two-american-journalists-were-shot-dead-during-a-live-broadcast-244590.html'>video</a>                        <a href="http://www.indiatimes.com/news/world/shocking-video-captures-the-moment-two-american-journalists-were-shot-dead-during-a-live-broadcast-244590.html" title="Shocking Video Captures The Moment Two American Journalists Were Shot Dead During A Live Broadcast!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/videocafe/2015/Aug/virgina502_1440603674_236x111.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Aug/virgina502_1440603674_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/shocking-video-captures-the-moment-two-american-journalists-were-shot-dead-during-a-live-broadcast-244590.html" title="Shocking Video Captures The Moment Two American Journalists Were Shot Dead During A Live Broadcast!">
                            Shocking Video Captures The Moment Two American Journalists Were Shot Dead During A Live Broadcast!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/8yearold-girl-receives-new-3dprinted-hand-from-a-designer-who-himself-is-missing-an-arm-244587.html" title="8-Year-Old Girl Receives New 3D-Printed Hand From A Designer Who Himself Is Missing An Arm" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/arm-502_1440590821_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/arm-502_1440590821_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/8yearold-girl-receives-new-3dprinted-hand-from-a-designer-who-himself-is-missing-an-arm-244587.html" title="8-Year-Old Girl Receives New 3D-Printed Hand From A Designer Who Himself Is Missing An Arm">
                            8-Year-Old Girl Receives New 3D-Printed Hand From A Designer Who Himself Is Missing An Arm                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/sania-mirzas-khel-ratna-award-stayed-after-paralympian-hn-girisha-says-he-deserves-it-more-244579.html" title="Sania Mirza's Khel Ratna Award Stayed After Paralympian HN Girisha Says He Deserves It More" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/saniagirishakhelratna_1440588389_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/saniagirishakhelratna_1440588389_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/sania-mirzas-khel-ratna-award-stayed-after-paralympian-hn-girisha-says-he-deserves-it-more-244579.html" title="Sania Mirza's Khel Ratna Award Stayed After Paralympian HN Girisha Says He Deserves It More">
                            Sania Mirza's Khel Ratna Award Stayed After Paralympian HN Girisha Says He Deserves It More                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/these-guys-went-rabbit-hunting-boasted-about-it-on-fb-till-forest-officials-hunted-them-down-244580.html" title="These Guys Went Rabbit Hunting & Boasted About It On FB Till Forest Officials Hunted Them Down!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/rabbit-hunting502_1440589328_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/rabbit-hunting502_1440589328_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/these-guys-went-rabbit-hunting-boasted-about-it-on-fb-till-forest-officials-hunted-them-down-244580.html" title="These Guys Went Rabbit Hunting & Boasted About It On FB Till Forest Officials Hunted Them Down!">
                            These Guys Went Rabbit Hunting & Boasted About It On FB Till Forest Officials Hunted Them Down!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/lifestyle/technology/abhay-deol-lisa-haydon-and-atul-kasbekar-are-coming-together-for-something-big-and-the-suspense-is-driving-us-nuts-244498.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/technology/abhay-deol-lisa-haydon-and-atul-kasbekar-are-coming-together-for-something-big-and-the-suspense-is-driving-us-nuts-244498.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Aug/card_1440423987_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/card_1440423987_502x234.jpg"  border="0" alt="Abhay Deol, Lisa Haydon And Atul Kasbekar Are Coming Together For Something Big And The Suspense Is Driving Us Nuts!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/lifestyle/technology/abhay-deol-lisa-haydon-and-atul-kasbekar-are-coming-together-for-something-big-and-the-suspense-is-driving-us-nuts-244498.html" title="Abhay Deol, Lisa Haydon And Atul Kasbekar Are Coming Together For Something Big And The Suspense Is Driving Us Nuts!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/technology/abhay-deol-lisa-haydon-and-atul-kasbekar-are-coming-together-for-something-big-and-the-suspense-is-driving-us-nuts-244498.html');">Abhay Deol, Lisa Haydon And Atul Kasbekar Are Coming Together For Something Big And The Suspense Is Driving Us Nuts!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/lifestyle/self/9-signs-you-have-a-love-hate-relationship-with-the-monsoon-244372.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/9-signs-you-have-a-love-hate-relationship-with-the-monsoon-244372.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Aug/card_1440394907_1440394918_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/card_1440394907_1440394918_502x234.jpg"  border="0" alt="9 Signs You Have A Love-Hate Relationship With The Monsoon" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/lifestyle/self/9-signs-you-have-a-love-hate-relationship-with-the-monsoon-244372.html" title="9 Signs You Have A Love-Hate Relationship With The Monsoon" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/9-signs-you-have-a-love-hate-relationship-with-the-monsoon-244372.html');">9 Signs You Have A Love-Hate Relationship With The Monsoon</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/buzz/did-you-know-retirement-can-be-good-for-your-health-244533.html" class="tint" title="Did You Know Retirement Can Be Good For Your Health?" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/buzz/did-you-know-retirement-can-be-good-for-your-health-244533.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/retired_1440500267_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/retired_1440500267_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/did-you-know-retirement-can-be-good-for-your-health-244533.html" title="Did You Know Retirement Can Be Good For Your Health?" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/buzz/did-you-know-retirement-can-be-good-for-your-health-244533.html');">
                            Did You Know Retirement Can Be Good For Your Health?                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/a-credible-eyewitness-comes-forward-in-the-jasleen-kaur-case-and-turns-everything-on-its-head-244565.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/a-credible-eyewitness-comes-forward-in-the-jasleen-kaur-case-and-turns-everything-on-its-head-244565.html" class="tint" title="A Credible Eyewitness Comes Forward In The Jasleen Kaur Case And Turns Everything On Its Head">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Aug/502_1440575902_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Aug/502_1440575902_218x102.jpg" border="0" alt="A Credible Eyewitness Comes Forward In The Jasleen Kaur Case And Turns Everything On Its Head"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/a-credible-eyewitness-comes-forward-in-the-jasleen-kaur-case-and-turns-everything-on-its-head-244565.html" title="A Credible Eyewitness Comes Forward In The Jasleen Kaur Case And Turns Everything On Its Head">
                            A Credible Eyewitness Comes Forward In The Jasleen Kaur Case And Turns Everything On Its Head                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/18-facts-that-prove-ancient-india-was-more-progressive-than-well-ever-be-244526.html" class="tint" title="18 Facts That Prove Ancient India Was More Progressive Than We'll Ever Be">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/card_1440494943_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/card_1440494943_218x102.jpg" border="0" alt="18 Facts That Prove Ancient India Was More Progressive Than We'll Ever Be"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/18-facts-that-prove-ancient-india-was-more-progressive-than-well-ever-be-244526.html" title="18 Facts That Prove Ancient India Was More Progressive Than We'll Ever Be">
                            18 Facts That Prove Ancient India Was More Progressive Than We'll Ever Be                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/technology/stephen-hawkingâs-new-black-hole-theory-punches-a-hole-in-science-as-we-know-it-244585.html" class="tint" title="Stephen Hawkingâs New Black Hole Theory Punches A Hole In Science As We Know It">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/hawking-card-2_1440590839_1440590849_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/hawking-card-2_1440590839_1440590849_218x102.jpg" border="0" alt="Stephen Hawkingâs New Black Hole Theory Punches A Hole In Science As We Know It"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/stephen-hawkingâs-new-black-hole-theory-punches-a-hole-in-science-as-we-know-it-244585.html" title="Stephen Hawkingâs New Black Hole Theory Punches A Hole In Science As We Know It">
                            Stephen Hawkingâs New Black Hole Theory Punches A Hole In Science As We Know It                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/an-srk-fan-just-mashed-up-his-whole-career-into-a-single-script-and-it-has-impressed-king-khan-too-244588.html" class="tint" title="SRK's Superfan Just Mashed Up His Whole Career Into A Single Script, And It's Insanely Hilarious!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/sa_1440592628_1440592643_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/sa_1440592628_1440592643_218x102.jpg" border="0" alt="SRK's Superfan Just Mashed Up His Whole Career Into A Single Script, And It's Insanely Hilarious!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/an-srk-fan-just-mashed-up-his-whole-career-into-a-single-script-and-it-has-impressed-king-khan-too-244588.html" title="SRK's Superfan Just Mashed Up His Whole Career Into A Single Script, And It's Insanely Hilarious!">
                            SRK's Superfan Just Mashed Up His Whole Career Into A Single Script, And It's Insanely Hilarious!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/24-reasons-why-you-are-not-getting-pregnant-243828.html" class="tint" title="24 Reasons Why You Are Not Getting Pregnant">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/not-getting-prgnant-card_1438764353_218x102.gif" data-original23="http://media.indiatimes.in/media/content/2015/Aug/not-getting-prgnant-card_1438764353_218x102.gif" border="0" alt="24 Reasons Why You Are Not Getting Pregnant"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/24-reasons-why-you-are-not-getting-pregnant-243828.html" title="24 Reasons Why You Are Not Getting Pregnant">
                            24 Reasons Why You Are Not Getting Pregnant                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/these-10-startups-are-sure-to-turn-into-billion-dollar-businesses-244577.html" title="These 10 Startups Are Sure To Turn Into Billion Dollar Businesses!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/startups-502_1440586013_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/these-10-startups-are-sure-to-turn-into-billion-dollar-businesses-244577.html" title="These 10 Startups Are Sure To Turn Into Billion Dollar Businesses!">
                            These 10 Startups Are Sure To Turn Into Billion Dollar Businesses!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/these-images-of-isis-blowing-up-a-2000yearold-temple-in-syria-will-reduce-you-to-tears-244573.html" title="These Images Of ISIS Blowing Up A 2000-Year-Old Temple In Syria Will Reduce You To Tears" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/5_1440581916_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/these-images-of-isis-blowing-up-a-2000yearold-temple-in-syria-will-reduce-you-to-tears-244573.html" title="These Images Of ISIS Blowing Up A 2000-Year-Old Temple In Syria Will Reduce You To Tears">
                            These Images Of ISIS Blowing Up A 2000-Year-Old Temple In Syria Will Reduce You To Tears                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            15 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/sports/the-musical-chairs-in-the-indian-cricket-team-chopping-and-changing-seems-to-be-the-new-order-244574.html" title="The Musical Chairs In The Indian Cricket Team. Chopping And Changing Seems To Be The New Order" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/vijaydhawanpujara_1440580658_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/the-musical-chairs-in-the-indian-cricket-team-chopping-and-changing-seems-to-be-the-new-order-244574.html" title="The Musical Chairs In The Indian Cricket Team. Chopping And Changing Seems To Be The New Order">
                            The Musical Chairs In The Indian Cricket Team. Chopping And Changing Seems To Be The New Order                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            15 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/fresh-twist-in-indrani-mukherjea-case-shes-accused-of-killing-her-daughter-for-having-an-affair-with-her-stepson-244571.html" title="Fresh Twist In Indrani Mukherjea Case, She's Accused Of Killing Her Daughter For Having An Affair With Her Stepson!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/indrani502_1440578919_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/fresh-twist-in-indrani-mukherjea-case-shes-accused-of-killing-her-daughter-for-having-an-affair-with-her-stepson-244571.html" title="Fresh Twist In Indrani Mukherjea Case, She's Accused Of Killing Her Daughter For Having An Affair With Her Stepson!">
                            Fresh Twist In Indrani Mukherjea Case, She's Accused Of Killing Her Daughter For Having An Affair With Her Stepson!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            15 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/these-heartbreaking-pictures-of-children-in-the-gaza-strip-prove-that-while-wars-end-sufferings-dont-244557.html" title="These Heartbreaking Pictures Of Children In The Gaza Strip Prove That While Wars End, Sufferings Don't" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card-2_1440577783_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/these-heartbreaking-pictures-of-children-in-the-gaza-strip-prove-that-while-wars-end-sufferings-dont-244557.html" title="These Heartbreaking Pictures Of Children In The Gaza Strip Prove That While Wars End, Sufferings Don't">
                            These Heartbreaking Pictures Of Children In The Gaza Strip Prove That While Wars End, Sufferings Don't                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/kabir-khans-phantom-looks-like-its-shot-in-pakistan-but-its-actually-punjab+-9-times-when-films-faked-locations-244408.html" class="tint" title="Kabir Khan's 'Phantom' Looks Like It's Shot In Pakistan, But It's Actually Punjab+ 9 Times When Films Faked Locations!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1440417736_1440417747_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/kabir-khans-phantom-looks-like-its-shot-in-pakistan-but-its-actually-punjab+-9-times-when-films-faked-locations-244408.html" title="Kabir Khan's 'Phantom' Looks Like It's Shot In Pakistan, But It's Actually Punjab+ 9 Times When Films Faked Locations!">
                            Kabir Khan's 'Phantom' Looks Like It's Shot In Pakistan, But It's Actually Punjab+ 9 Times When Films Faked Locations!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/an-srk-fan-just-mashed-up-his-whole-career-into-a-single-script-and-it-has-impressed-king-khan-too-244588.html" class="tint" title="SRK's Superfan Just Mashed Up His Whole Career Into A Single Script, And It's Insanely Hilarious!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/sa_1440592628_1440592643_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/an-srk-fan-just-mashed-up-his-whole-career-into-a-single-script-and-it-has-impressed-king-khan-too-244588.html" title="SRK's Superfan Just Mashed Up His Whole Career Into A Single Script, And It's Insanely Hilarious!">
                            SRK's Superfan Just Mashed Up His Whole Career Into A Single Script, And It's Insanely Hilarious!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/south-star-chiranjeevis-famous-zero-oil-dosa-to-be-called-chiru-dosa-+-15-random-things-named-after-celebrities-244536.html" class="tint" title="South Star Chiranjeevi's Famous Zero Oil Dosa To Be Called 'Chiru Dosa' + 15 Random Things Named After Celebrities">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440569809_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/south-star-chiranjeevis-famous-zero-oil-dosa-to-be-called-chiru-dosa-+-15-random-things-named-after-celebrities-244536.html" title="South Star Chiranjeevi's Famous Zero Oil Dosa To Be Called 'Chiru Dosa' + 15 Random Things Named After Celebrities">
                            South Star Chiranjeevi's Famous Zero Oil Dosa To Be Called 'Chiru Dosa' + 15 Random Things Named After Celebrities                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/technology/whatsapp-has-now-introduced-coloured-emoji-does-that-make-us-more-racist-244575.html" class="tint" title="WhatsApp Has Now Introduced Coloured Emoji And There's Def Something Racist About Them!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440590807_218x102.jpg" border="0" alt="WhatsApp Has Now Introduced Coloured Emoji And There's Def Something Racist About Them!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/whatsapp-has-now-introduced-coloured-emoji-does-that-make-us-more-racist-244575.html" title="WhatsApp Has Now Introduced Coloured Emoji And There's Def Something Racist About Them!">
                            WhatsApp Has Now Introduced Coloured Emoji And There's Def Something Racist About Them!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/23-emotions-you-might-feel-on-a-daily-basis-but-cant-explain-what-it-really-is-244549.html" class="tint" title="23 Everyday Emotions You Didn't Know There Was A Name For!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440566621_218x102.jpg" border="0" alt="23 Everyday Emotions You Didn't Know There Was A Name For!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/23-emotions-you-might-feel-on-a-daily-basis-but-cant-explain-what-it-really-is-244549.html" title="23 Everyday Emotions You Didn't Know There Was A Name For!">
                            23 Everyday Emotions You Didn't Know There Was A Name For!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/bollywood/everything-you-need-to-know-about-jailtourist-sanjay-dutts-paroles-20132015_-244582.html" class="tint" title="Everything You Need To Know About 'Jail-Tourist' Sanjay Dutt's Paroles, 2013-2015">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/lscard_1440589263_218x102.jpg" border="0" alt="Everything You Need To Know About 'Jail-Tourist' Sanjay Dutt's Paroles, 2013-2015"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/everything-you-need-to-know-about-jailtourist-sanjay-dutts-paroles-20132015_-244582.html" title="Everything You Need To Know About 'Jail-Tourist' Sanjay Dutt's Paroles, 2013-2015">
                            Everything You Need To Know About 'Jail-Tourist' Sanjay Dutt's Paroles, 2013-2015                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/bollywood/nawazuddin-siddiqui-enters-the-100-crore-club-+-11-actors-who-made-a-daring-jump-from-offbeat-to-commercial-films-244567.html" class="tint" title="Nawazuddin Siddiqui Enters The 100 Crore Club + 11 Actors Who Made A Daring Jump From Offbeat To Commercial Films">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card1_1440584152_218x102.jpg" border="0" alt="Nawazuddin Siddiqui Enters The 100 Crore Club + 11 Actors Who Made A Daring Jump From Offbeat To Commercial Films"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/nawazuddin-siddiqui-enters-the-100-crore-club-+-11-actors-who-made-a-daring-jump-from-offbeat-to-commercial-films-244567.html" title="Nawazuddin Siddiqui Enters The 100 Crore Club + 11 Actors Who Made A Daring Jump From Offbeat To Commercial Films">
                            Nawazuddin Siddiqui Enters The 100 Crore Club + 11 Actors Who Made A Daring Jump From Offbeat To Commercial Films                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/south-star-chiranjeevis-famous-zero-oil-dosa-to-be-called-chiru-dosa-+-15-random-things-named-after-celebrities-244536.html" class="tint" title="South Star Chiranjeevi's Famous Zero Oil Dosa To Be Called 'Chiru Dosa' + 15 Random Things Named After Celebrities">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440569809_218x102.jpg" border="0" alt="South Star Chiranjeevi's Famous Zero Oil Dosa To Be Called 'Chiru Dosa' + 15 Random Things Named After Celebrities"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/south-star-chiranjeevis-famous-zero-oil-dosa-to-be-called-chiru-dosa-+-15-random-things-named-after-celebrities-244536.html" title="South Star Chiranjeevi's Famous Zero Oil Dosa To Be Called 'Chiru Dosa' + 15 Random Things Named After Celebrities">
                            South Star Chiranjeevi's Famous Zero Oil Dosa To Be Called 'Chiru Dosa' + 15 Random Things Named After Celebrities                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            15 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/mangaloreincident-hindu-woman-does-a-uturn-alleges-she-was-harassed-by-muslim-colleague-244566.html" title="#MangaloreIncident Hindu Woman Does A U-Turn, Alleges She Was Harassed By Muslim Colleague" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440576465_236x111.jpg" border="0" alt="#MangaloreIncident Hindu Woman Does A U-Turn, Alleges She Was Harassed By Muslim Colleague"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/mangaloreincident-hindu-woman-does-a-uturn-alleges-she-was-harassed-by-muslim-colleague-244566.html" title="#MangaloreIncident Hindu Woman Does A U-Turn, Alleges She Was Harassed By Muslim Colleague">
                            #MangaloreIncident Hindu Woman Does A U-Turn, Alleges She Was Harassed By Muslim Colleague                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/whatsapp-services-blocked-in-parts-of-gujarat-as-patel-agitation-boils-over-244569.html" title="WhatsApp Services Blocked In Parts Of Gujarat As Patel Agitation Boils Over" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440578322_236x111.jpg" border="0" alt="WhatsApp Services Blocked In Parts Of Gujarat As Patel Agitation Boils Over"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/whatsapp-services-blocked-in-parts-of-gujarat-as-patel-agitation-boils-over-244569.html" title="WhatsApp Services Blocked In Parts Of Gujarat As Patel Agitation Boils Over">
                            WhatsApp Services Blocked In Parts Of Gujarat As Patel Agitation Boils Over                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/sports/harbhajan-singh-drives-virat-kohli-stuart-binny-in-a-tuk-tuk-in-colombo-indian-cricket-team-on-a-joyride-244562.html" title="Harbhajan Singh Drives Virat Kohli, Stuart Binny In A 'Tuk Tuk' In Colombo. Indian Cricket Team On A Joyride" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/harbhajankohliautotweet_1440573246_236x111.jpg" border="0" alt="Harbhajan Singh Drives Virat Kohli, Stuart Binny In A 'Tuk Tuk' In Colombo. Indian Cricket Team On A Joyride"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/harbhajan-singh-drives-virat-kohli-stuart-binny-in-a-tuk-tuk-in-colombo-indian-cricket-team-on-a-joyride-244562.html" title="Harbhajan Singh Drives Virat Kohli, Stuart Binny In A 'Tuk Tuk' In Colombo. Indian Cricket Team On A Joyride">
                            Harbhajan Singh Drives Virat Kohli, Stuart Binny In A 'Tuk Tuk' In Colombo. Indian Cricket Team On A Joyride                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/heres-how-the-indian-government-plans-to-bring-the-worlds-most-wanted-terrorist-back-home-244556.html" title="Here's How The Indian Government Plans To Bring The World's Most Wanted Terrorist Back Home" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/dawood-502_1440572931_236x111.jpg" border="0" alt="Here's How The Indian Government Plans To Bring The World's Most Wanted Terrorist Back Home"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/heres-how-the-indian-government-plans-to-bring-the-worlds-most-wanted-terrorist-back-home-244556.html" title="Here's How The Indian Government Plans To Bring The World's Most Wanted Terrorist Back Home">
                            Here's How The Indian Government Plans To Bring The World's Most Wanted Terrorist Back Home                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/presenting-the-boy-who-put-his-fist-through-a-$15-million-painting-and-got-away-with-it-244553.html" title="You Have To Meet The Boy Who Put His Fist Through A $1.5-Million Painting And Got Away With It!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/fisted5_1440568735_236x111.jpg" border="0" alt="You Have To Meet The Boy Who Put His Fist Through A $1.5-Million Painting And Got Away With It!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/presenting-the-boy-who-put-his-fist-through-a-$15-million-painting-and-got-away-with-it-244553.html" title="You Have To Meet The Boy Who Put His Fist Through A $1.5-Million Painting And Got Away With It!">
                            You Have To Meet The Boy Who Put His Fist Through A $1.5-Million Painting And Got Away With It!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/everything-you-need-to-know-about-jailtourist-sanjay-dutts-paroles-20132015_-244582.html" class="tint" title="Everything You Need To Know About 'Jail-Tourist' Sanjay Dutt's Paroles, 2013-2015">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/lscard_1440589263_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bollywood/everything-you-need-to-know-about-jailtourist-sanjay-dutts-paroles-20132015_-244582.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/everything-you-need-to-know-about-jailtourist-sanjay-dutts-paroles-20132015_-244582.html" title="Everything You Need To Know About 'Jail-Tourist' Sanjay Dutt's Paroles, 2013-2015">
                            Everything You Need To Know About 'Jail-Tourist' Sanjay Dutt's Paroles, 2013-2015                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/technology/stephen-hawkingâs-new-black-hole-theory-punches-a-hole-in-science-as-we-know-it-244585.html" class="tint" title="Stephen Hawkingâs New Black Hole Theory Punches A Hole In Science As We Know It">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/hawking-card-2_1440590839_1440590849_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/technology/stephen-hawkingâs-new-black-hole-theory-punches-a-hole-in-science-as-we-know-it-244585.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/stephen-hawkingâs-new-black-hole-theory-punches-a-hole-in-science-as-we-know-it-244585.html" title="Stephen Hawkingâs New Black Hole Theory Punches A Hole In Science As We Know It">
                            Stephen Hawkingâs New Black Hole Theory Punches A Hole In Science As We Know It                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/18-facts-that-prove-ancient-india-was-more-progressive-than-well-ever-be-244526.html" class="tint" title="18 Facts That Prove Ancient India Was More Progressive Than We'll Ever Be">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440494943_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/who-we-are/18-facts-that-prove-ancient-india-was-more-progressive-than-well-ever-be-244526.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/18-facts-that-prove-ancient-india-was-more-progressive-than-well-ever-be-244526.html" title="18 Facts That Prove Ancient India Was More Progressive Than We'll Ever Be">
                            18 Facts That Prove Ancient India Was More Progressive Than We'll Ever Be                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/sanjay-dutt-gets-30day-parole-for-daughter-trishalas-nose-surgery-244552.html" class="tint" title="Sanjay Dutt Gets 30-Day Parole For Daughter Trishala's Nose Job. Will He Ever Run Out Of Excuses?">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/aaa_1440573114_218x102.jpg" border="0" alt="Sanjay Dutt Gets 30-Day Parole For Daughter Trishala's Nose Job. Will He Ever Run Out Of Excuses?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/sanjay-dutt-gets-30day-parole-for-daughter-trishalas-nose-surgery-244552.html" title="Sanjay Dutt Gets 30-Day Parole For Daughter Trishala's Nose Job. Will He Ever Run Out Of Excuses?">
                            Sanjay Dutt Gets 30-Day Parole For Daughter Trishala's Nose Job. Will He Ever Run Out Of Excuses?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/been-meaning-to-learn-yoga-let-shilpa-shetty-be-your-instructor-244281.html'>video</a>		

                        <a href="http://www.indiatimes.com/health/healthyliving/been-meaning-to-learn-yoga-let-shilpa-shetty-be-your-instructor-244281.html" class="tint" title="Been Meaning To Learn Yoga? Let Shilpa Shetty Be Your Instructor">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/shilpashetty_1439881579_218x102.jpg" border="0" alt="Been Meaning To Learn Yoga? Let Shilpa Shetty Be Your Instructor"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/been-meaning-to-learn-yoga-let-shilpa-shetty-be-your-instructor-244281.html" title="Been Meaning To Learn Yoga? Let Shilpa Shetty Be Your Instructor">
                            Been Meaning To Learn Yoga? Let Shilpa Shetty Be Your Instructor                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/kangana-imran-khan-have-done-what-emraan-hashmi-could-not-kissed-for-straight-24-hours-for-a-song-244572.html" class="tint" title="Kangana & Imran Khan Have Done What Emraan Hashmi Could Not! Kissed For Straight 24 Hours For A Song!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/katti-batti_1440581833_1440581840_218x102.jpg" border="0" alt="Kangana & Imran Khan Have Done What Emraan Hashmi Could Not! Kissed For Straight 24 Hours For A Song!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/kangana-imran-khan-have-done-what-emraan-hashmi-could-not-kissed-for-straight-24-hours-for-a-song-244572.html" title="Kangana & Imran Khan Have Done What Emraan Hashmi Could Not! Kissed For Straight 24 Hours For A Song!">
                            Kangana & Imran Khan Have Done What Emraan Hashmi Could Not! Kissed For Straight 24 Hours For A Song!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/you-wont-believe-what-this-girl-is-demanding-from-her-brother-as-her-raksha-bandhan-gift-244517.html'>video</a>		

                        <a href="http://www.indiatimes.com/culture/who-we-are/you-wont-believe-what-this-girl-is-demanding-from-her-brother-as-her-raksha-bandhan-gift-244517.html" class="tint" title="You Won't Believe What This Girl Is Demanding From Her Brother As Her Raksha Bandhan Gift!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/bn_1440488478_1440488484_218x102.jpg" border="0" alt="You Won't Believe What This Girl Is Demanding From Her Brother As Her Raksha Bandhan Gift!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/you-wont-believe-what-this-girl-is-demanding-from-her-brother-as-her-raksha-bandhan-gift-244517.html" title="You Won't Believe What This Girl Is Demanding From Her Brother As Her Raksha Bandhan Gift!">
                            You Won't Believe What This Girl Is Demanding From Her Brother As Her Raksha Bandhan Gift!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/9-crazies-who-didnt-really-get-along-well-with-people-on-social-media-244537.html" class="tint" title="9 Crazies Who Didn't Really Get Along Well With People On Social Media">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440502306_218x102.jpg" border="0" alt="9 Crazies Who Didn't Really Get Along Well With People On Social Media"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-crazies-who-didnt-really-get-along-well-with-people-on-social-media-244537.html" title="9 Crazies Who Didn't Really Get Along Well With People On Social Media">
                            9 Crazies Who Didn't Really Get Along Well With People On Social Media                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/chinas-richest-man-bore-the-brunt-of-the-stock-market-crash-he-lost-10-of-his-total-wealth-in-a-single-day-244555.html" title="China's Richest Man Bore The Brunt Of The Stock Market Crash, He Lost 10% Of His Total Wealth In A Single Day!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440570619_236x111.jpg" border="0" alt="China's Richest Man Bore The Brunt Of The Stock Market Crash, He Lost 10% Of His Total Wealth In A Single Day!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/chinas-richest-man-bore-the-brunt-of-the-stock-market-crash-he-lost-10-of-his-total-wealth-in-a-single-day-244555.html" title="China's Richest Man Bore The Brunt Of The Stock Market Crash, He Lost 10% Of His Total Wealth In A Single Day!">
                            China's Richest Man Bore The Brunt Of The Stock Market Crash, He Lost 10% Of His Total Wealth In A Single Day!                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/census2011-reveals-muslim-population-share-up-by-08-hindus-dip-below-80-for-the-first-time-244551.html" title="#Census2011 Reveals Muslim Population Share Up By 0.8%, Hindus Dip Below 80% For The First Time" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440568489_236x111.jpg" border="0" alt="#Census2011 Reveals Muslim Population Share Up By 0.8%, Hindus Dip Below 80% For The First Time"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/census2011-reveals-muslim-population-share-up-by-08-hindus-dip-below-80-for-the-first-time-244551.html" title="#Census2011 Reveals Muslim Population Share Up By 0.8%, Hindus Dip Below 80% For The First Time">
                            #Census2011 Reveals Muslim Population Share Up By 0.8%, Hindus Dip Below 80% For The First Time                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/this-man-in-jodhpur-is-creating-affordable-trolleys-for-paralysed-dogs-244540.html" title="This Man In Jodhpur Is Creating Affordable Trolleys For Paralysed Dogs" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/dog1-5_1440504069_1440504072_236x111.jpg" border="0" alt="This Man In Jodhpur Is Creating Affordable Trolleys For Paralysed Dogs"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-man-in-jodhpur-is-creating-affordable-trolleys-for-paralysed-dogs-244540.html" title="This Man In Jodhpur Is Creating Affordable Trolleys For Paralysed Dogs">
                            This Man In Jodhpur Is Creating Affordable Trolleys For Paralysed Dogs                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/personal-account-of-business-tycoon-ajay-mafatlal-one-of-the-first-indians-to-undergo-a-sex-change-244546.html" title="Personal Account Of Business Tycoon Ajay Mafatlal, One Of The First Indians To Undergo A Sex Change" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/ayaj-502_1440508374_236x111.jpg" border="0" alt="Personal Account Of Business Tycoon Ajay Mafatlal, One Of The First Indians To Undergo A Sex Change"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/personal-account-of-business-tycoon-ajay-mafatlal-one-of-the-first-indians-to-undergo-a-sex-change-244546.html" title="Personal Account Of Business Tycoon Ajay Mafatlal, One Of The First Indians To Undergo A Sex Change">
                            Personal Account Of Business Tycoon Ajay Mafatlal, One Of The First Indians To Undergo A Sex Change                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/this-man-in-mumbai-made-50-hoax-bomb-threat-calls-before-being-arrested-244544.html" title="This Man In Mumbai Made 50 Hoax Bomb Threat Calls Before Being Arrested" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/oxygen-finance-5_1440505361_1440505364_236x111.jpg" border="0" alt="This Man In Mumbai Made 50 Hoax Bomb Threat Calls Before Being Arrested"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-man-in-mumbai-made-50-hoax-bomb-threat-calls-before-being-arrested-244544.html" title="This Man In Mumbai Made 50 Hoax Bomb Threat Calls Before Being Arrested">
                            This Man In Mumbai Made 50 Hoax Bomb Threat Calls Before Being Arrested                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/whatsapp-has-now-introduced-coloured-emoji-does-that-make-us-more-racist-244575.html" class="tint" title="WhatsApp Has Now Introduced Coloured Emoji And There's Def Something Racist About Them!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440590807_502x234.jpg" border="0" alt="WhatsApp Has Now Introduced Coloured Emoji And There's Def Something Racist About Them!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/whatsapp-has-now-introduced-coloured-emoji-does-that-make-us-more-racist-244575.html" title="WhatsApp Has Now Introduced Coloured Emoji And There's Def Something Racist About Them!">
                        WhatsApp Has Now Introduced Coloured Emoji And There's Def Something Racist About Them!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/nawazuddin-siddiqui-enters-the-100-crore-club-+-11-actors-who-made-a-daring-jump-from-offbeat-to-commercial-films-244567.html" class="tint" title="Nawazuddin Siddiqui Enters The 100 Crore Club + 11 Actors Who Made A Daring Jump From Offbeat To Commercial Films">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card1_1440584152_502x234.jpg" border="0" alt="Nawazuddin Siddiqui Enters The 100 Crore Club + 11 Actors Who Made A Daring Jump From Offbeat To Commercial Films" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/nawazuddin-siddiqui-enters-the-100-crore-club-+-11-actors-who-made-a-daring-jump-from-offbeat-to-commercial-films-244567.html" title="Nawazuddin Siddiqui Enters The 100 Crore Club + 11 Actors Who Made A Daring Jump From Offbeat To Commercial Films">
                        Nawazuddin Siddiqui Enters The 100 Crore Club + 11 Actors Who Made A Daring Jump From Offbeat To Commercial Films                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/just-one-day-after-reports-said-he-met-honey-singh-in-a-rehab-jasbir-jassi-denies-it-all-244583.html" class="tint" title="Just One Day After Reports Said He Met Honey Singh In A Rehab, Jasbir Jassi Denies It All!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1440589578_1440589592_502x234.jpg" border="0" alt="Just One Day After Reports Said He Met Honey Singh In A Rehab, Jasbir Jassi Denies It All!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/just-one-day-after-reports-said-he-met-honey-singh-in-a-rehab-jasbir-jassi-denies-it-all-244583.html" title="Just One Day After Reports Said He Met Honey Singh In A Rehab, Jasbir Jassi Denies It All!">
                        Just One Day After Reports Said He Met Honey Singh In A Rehab, Jasbir Jassi Denies It All!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/kangana-imran-khan-have-done-what-emraan-hashmi-could-not-kissed-for-straight-24-hours-for-a-song-244572.html" class="tint" title="Kangana & Imran Khan Have Done What Emraan Hashmi Could Not! Kissed For Straight 24 Hours For A Song!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/katti-batti_1440581833_1440581840_502x234.jpg" border="0" alt="Kangana & Imran Khan Have Done What Emraan Hashmi Could Not! Kissed For Straight 24 Hours For A Song!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/kangana-imran-khan-have-done-what-emraan-hashmi-could-not-kissed-for-straight-24-hours-for-a-song-244572.html" title="Kangana & Imran Khan Have Done What Emraan Hashmi Could Not! Kissed For Straight 24 Hours For A Song!">
                        Kangana & Imran Khan Have Done What Emraan Hashmi Could Not! Kissed For Straight 24 Hours For A Song!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/raksha-bandhan-the-day-every-guy-fears-here-are-the-reasons-why-girls-brozone-guys-244559.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/raksha-bandhan-the-day-every-guy-fears-here-are-the-reasons-why-girls-brozone-guys-244559.html" class="tint" title="Raksha Bandhan - The Day Every Guy Fears. Here Are The Reasons Why Girls 'Bro-Zone' Guys!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/brozone_card_1440573876_502x234.jpg" border="0" alt="Raksha Bandhan - The Day Every Guy Fears. Here Are The Reasons Why Girls 'Bro-Zone' Guys!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/raksha-bandhan-the-day-every-guy-fears-here-are-the-reasons-why-girls-brozone-guys-244559.html" title="Raksha Bandhan - The Day Every Guy Fears. Here Are The Reasons Why Girls 'Bro-Zone' Guys!">
                        Raksha Bandhan - The Day Every Guy Fears. Here Are The Reasons Why Girls 'Bro-Zone' Guys!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/looking-for-some-peace-and-harmony-in-life-meditation-may-just-be-the-thing-for-you-244539.html" class="tint" title="Looking For Some Peace And Harmony In Life? Meditation May Just Be The Thing For You">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card-1_1440567564_502x234.jpg" border="0" alt="Looking For Some Peace And Harmony In Life? Meditation May Just Be The Thing For You" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/looking-for-some-peace-and-harmony-in-life-meditation-may-just-be-the-thing-for-you-244539.html" title="Looking For Some Peace And Harmony In Life? Meditation May Just Be The Thing For You">
                        Looking For Some Peace And Harmony In Life? Meditation May Just Be The Thing For You                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/24-reasons-why-you-are-not-getting-pregnant-243828.html" class="tint" title="24 Reasons Why You Are Not Getting Pregnant">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/not-getting-prgnant-card_1438764353_502x234.gif" border="0" alt="24 Reasons Why You Are Not Getting Pregnant" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/24-reasons-why-you-are-not-getting-pregnant-243828.html" title="24 Reasons Why You Are Not Getting Pregnant">
                        24 Reasons Why You Are Not Getting Pregnant                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/a-credible-eyewitness-comes-forward-in-the-jasleen-kaur-case-and-turns-everything-on-its-head-244565.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/a-credible-eyewitness-comes-forward-in-the-jasleen-kaur-case-and-turns-everything-on-its-head-244565.html" class="tint" title="A Credible Eyewitness Comes Forward In The Jasleen Kaur Case And Turns Everything On Its Head">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/502_1440575902_502x234.jpg" border="0" alt="A Credible Eyewitness Comes Forward In The Jasleen Kaur Case And Turns Everything On Its Head" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/a-credible-eyewitness-comes-forward-in-the-jasleen-kaur-case-and-turns-everything-on-its-head-244565.html" title="A Credible Eyewitness Comes Forward In The Jasleen Kaur Case And Turns Everything On Its Head">
                        A Credible Eyewitness Comes Forward In The Jasleen Kaur Case And Turns Everything On Its Head                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-man-is-trying-to-make-the-world-a-happier-place-one-fart-at-a-time-244554.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-man-is-trying-to-make-the-world-a-happier-place-one-fart-at-a-time-244554.html" class="tint" title="This Man Is Trying To Make The World A Happier Place - One Fart At A Time!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/fartingprank_card_1440569760_502x234.jpg" border="0" alt="This Man Is Trying To Make The World A Happier Place - One Fart At A Time!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-man-is-trying-to-make-the-world-a-happier-place-one-fart-at-a-time-244554.html" title="This Man Is Trying To Make The World A Happier Place - One Fart At A Time!">
                        This Man Is Trying To Make The World A Happier Place - One Fart At A Time!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/sanjay-dutt-gets-30day-parole-for-daughter-trishalas-nose-surgery-244552.html" class="tint" title="Sanjay Dutt Gets 30-Day Parole For Daughter Trishala's Nose Job. Will He Ever Run Out Of Excuses?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/aaa_1440573114_502x234.jpg" border="0" alt="Sanjay Dutt Gets 30-Day Parole For Daughter Trishala's Nose Job. Will He Ever Run Out Of Excuses?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/sanjay-dutt-gets-30day-parole-for-daughter-trishalas-nose-surgery-244552.html" title="Sanjay Dutt Gets 30-Day Parole For Daughter Trishala's Nose Job. Will He Ever Run Out Of Excuses?">
                        Sanjay Dutt Gets 30-Day Parole For Daughter Trishala's Nose Job. Will He Ever Run Out Of Excuses?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/48-culinary-hacks-that-will-make-you-the-master-of-your-kitchen-244530.html" class="tint" title="48 Culinary Hacks That Will Make You The Master Of Your Kitchen">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/hacks502_1440602477_502x234.jpg" border="0" alt="48 Culinary Hacks That Will Make You The Master Of Your Kitchen" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/48-culinary-hacks-that-will-make-you-the-master-of-your-kitchen-244530.html" title="48 Culinary Hacks That Will Make You The Master Of Your Kitchen">
                        48 Culinary Hacks That Will Make You The Master Of Your Kitchen                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-salimsulaiman-track-reveals-shocking-facts-on-mass-killings-of-children-all-over-the-world-244514.html" class="tint" title="This Salim-Sulaiman Track Reveals Shocking Facts On Mass Killings Of Children All Over The World">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440491518_502x234.jpg" border="0" alt="This Salim-Sulaiman Track Reveals Shocking Facts On Mass Killings Of Children All Over The World" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-salimsulaiman-track-reveals-shocking-facts-on-mass-killings-of-children-all-over-the-world-244514.html" title="This Salim-Sulaiman Track Reveals Shocking Facts On Mass Killings Of Children All Over The World">
                        This Salim-Sulaiman Track Reveals Shocking Facts On Mass Killings Of Children All Over The World                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/been-meaning-to-learn-yoga-let-shilpa-shetty-be-your-instructor-244281.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/been-meaning-to-learn-yoga-let-shilpa-shetty-be-your-instructor-244281.html" class="tint" title="Been Meaning To Learn Yoga? Let Shilpa Shetty Be Your Instructor">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/shilpashetty_1439881579_502x234.jpg" border="0" alt="Been Meaning To Learn Yoga? Let Shilpa Shetty Be Your Instructor" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/been-meaning-to-learn-yoga-let-shilpa-shetty-be-your-instructor-244281.html" title="Been Meaning To Learn Yoga? Let Shilpa Shetty Be Your Instructor">
                        Been Meaning To Learn Yoga? Let Shilpa Shetty Be Your Instructor                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/23-emotions-you-might-feel-on-a-daily-basis-but-cant-explain-what-it-really-is-244549.html" class="tint" title="23 Everyday Emotions You Didn't Know There Was A Name For!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440566621_502x234.jpg" border="0" alt="23 Everyday Emotions You Didn't Know There Was A Name For!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/23-emotions-you-might-feel-on-a-daily-basis-but-cant-explain-what-it-really-is-244549.html" title="23 Everyday Emotions You Didn't Know There Was A Name For!">
                        23 Everyday Emotions You Didn't Know There Was A Name For!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/14-healthy-and-delicious-faaral-recipes-for-fasting-season-244478.html" class="tint" title="14 Healthy And Delicious Faaral Recipes For Fasting Season">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/faaral_1440407203_502x234.jpg" border="0" alt="14 Healthy And Delicious Faaral Recipes For Fasting Season" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/14-healthy-and-delicious-faaral-recipes-for-fasting-season-244478.html" title="14 Healthy And Delicious Faaral Recipes For Fasting Season">
                        14 Healthy And Delicious Faaral Recipes For Fasting Season                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/7-times-btowns-men-risked-comparing-their-female-costars-244520.html" class="tint" title="7 Times Actors Compared Their Co-Stars And Paid For It (Sometimes)">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/split1_1440498319_502x234.jpg" border="0" alt="7 Times Actors Compared Their Co-Stars And Paid For It (Sometimes)" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/7-times-btowns-men-risked-comparing-their-female-costars-244520.html" title="7 Times Actors Compared Their Co-Stars And Paid For It (Sometimes)">
                        7 Times Actors Compared Their Co-Stars And Paid For It (Sometimes)                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/fawad-khan-is-all-set-to-play-a-homosexual-character-in-the-upcoming-kapoor-sons-244538.html" class="tint" title="Fawad Khan Is All Set To Play A Homosexual Character In The Upcoming 'Kapoor & Sons'!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440584251_502x234.jpg" border="0" alt="Fawad Khan Is All Set To Play A Homosexual Character In The Upcoming 'Kapoor & Sons'!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/fawad-khan-is-all-set-to-play-a-homosexual-character-in-the-upcoming-kapoor-sons-244538.html" title="Fawad Khan Is All Set To Play A Homosexual Character In The Upcoming 'Kapoor & Sons'!">
                        Fawad Khan Is All Set To Play A Homosexual Character In The Upcoming 'Kapoor & Sons'!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-crazies-who-didnt-really-get-along-well-with-people-on-social-media-244537.html" class="tint" title="9 Crazies Who Didn't Really Get Along Well With People On Social Media">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440502306_502x234.jpg" border="0" alt="9 Crazies Who Didn't Really Get Along Well With People On Social Media" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-crazies-who-didnt-really-get-along-well-with-people-on-social-media-244537.html" title="9 Crazies Who Didn't Really Get Along Well With People On Social Media">
                        9 Crazies Who Didn't Really Get Along Well With People On Social Media                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/maggi-vs-aishwarya-in-jazbaa-which-comeback-would-you-rather-see-heres-both-244543.html" class="tint" title="Maggi Vs Aishwarya In Jazbaa. Which Comeback Would You Rather See? Here's Both">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/maggi_ash_card_1440506182_502x234.jpg" border="0" alt="Maggi Vs Aishwarya In Jazbaa. Which Comeback Would You Rather See? Here's Both" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/maggi-vs-aishwarya-in-jazbaa-which-comeback-would-you-rather-see-heres-both-244543.html" title="Maggi Vs Aishwarya In Jazbaa. Which Comeback Would You Rather See? Here's Both">
                        Maggi Vs Aishwarya In Jazbaa. Which Comeback Would You Rather See? Here's Both                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/anurag-kashyap-takes-the-media-for-a-ride-and-won-a-bet-along-the-way-244510.html" class="tint" title="Anurag Kashyap Takes The Media For A Ride And Won A Bet Along The Way">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/aaa_1440487292_1440487300_502x234.jpg" border="0" alt="Anurag Kashyap Takes The Media For A Ride And Won A Bet Along The Way" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/anurag-kashyap-takes-the-media-for-a-ride-and-won-a-bet-along-the-way-244510.html" title="Anurag Kashyap Takes The Media For A Ride And Won A Bet Along The Way">
                        Anurag Kashyap Takes The Media For A Ride And Won A Bet Along The Way                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/you-wont-believe-what-this-girl-is-demanding-from-her-brother-as-her-raksha-bandhan-gift-244517.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/you-wont-believe-what-this-girl-is-demanding-from-her-brother-as-her-raksha-bandhan-gift-244517.html" class="tint" title="You Won't Believe What This Girl Is Demanding From Her Brother As Her Raksha Bandhan Gift!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/bn_1440488478_1440488484_502x234.jpg" border="0" alt="You Won't Believe What This Girl Is Demanding From Her Brother As Her Raksha Bandhan Gift!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/you-wont-believe-what-this-girl-is-demanding-from-her-brother-as-her-raksha-bandhan-gift-244517.html" title="You Won't Believe What This Girl Is Demanding From Her Brother As Her Raksha Bandhan Gift!">
                        You Won't Believe What This Girl Is Demanding From Her Brother As Her Raksha Bandhan Gift!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/struggling-with-pcos-here-are-some-tips-to-help-you-lose-weight-244515.html" class="tint" title="Struggling With PCOS? Here Are Some Tips To Help You Lose Weight!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/pcos_1440488735_502x234.jpg" border="0" alt="Struggling With PCOS? Here Are Some Tips To Help You Lose Weight!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/struggling-with-pcos-here-are-some-tips-to-help-you-lose-weight-244515.html" title="Struggling With PCOS? Here Are Some Tips To Help You Lose Weight!">
                        Struggling With PCOS? Here Are Some Tips To Help You Lose Weight!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-blazing-bajirao-the-web-series-based-on-bajirao-mastani-will-stun-you-244527.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-blazing-bajirao-the-web-series-based-on-bajirao-mastani-will-stun-you-244527.html" class="tint" title="The Trailer Of 'Blazing Bajirao', The Web Series Based On Bajirao Mastani Will Stun You!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/screenshot_2_1440496458_1440496465_502x234.jpg" border="0" alt="The Trailer Of 'Blazing Bajirao', The Web Series Based On Bajirao Mastani Will Stun You!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-blazing-bajirao-the-web-series-based-on-bajirao-mastani-will-stun-you-244527.html" title="The Trailer Of 'Blazing Bajirao', The Web Series Based On Bajirao Mastani Will Stun You!">
                        The Trailer Of 'Blazing Bajirao', The Web Series Based On Bajirao Mastani Will Stun You!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-trailer-of-aishwarya-rais-comeback-film-jazbaa-is-out-and-its-so-intense-itll-leave-you-panting-244525.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-trailer-of-aishwarya-rais-comeback-film-jazbaa-is-out-and-its-so-intense-itll-leave-you-panting-244525.html" class="tint" title="The Trailer Of Aishwarya Rai's Comeback Film 'Jazbaa' Is Out And It's So Intense It'll Leave You Panting!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/jazbaa_trailer_1440498922_502x234.jpg" border="0" alt="The Trailer Of Aishwarya Rai's Comeback Film 'Jazbaa' Is Out And It's So Intense It'll Leave You Panting!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-trailer-of-aishwarya-rais-comeback-film-jazbaa-is-out-and-its-so-intense-itll-leave-you-panting-244525.html" title="The Trailer Of Aishwarya Rai's Comeback Film 'Jazbaa' Is Out And It's So Intense It'll Leave You Panting!">
                        The Trailer Of Aishwarya Rai's Comeback Film 'Jazbaa' Is Out And It's So Intense It'll Leave You Panting!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-spoton-pieces-of-advice-from-the-god-of-martial-arts-bruce-lee-244404.html" class="tint" title="11 Spot-On Pieces Of Advice From The God Of Martial Arts - Bruce Lee">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440155658_502x234.jpg" border="0" alt="11 Spot-On Pieces Of Advice From The God Of Martial Arts - Bruce Lee" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-spoton-pieces-of-advice-from-the-god-of-martial-arts-bruce-lee-244404.html" title="11 Spot-On Pieces Of Advice From The God Of Martial Arts - Bruce Lee">
                        11 Spot-On Pieces Of Advice From The God Of Martial Arts - Bruce Lee                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/the-ultimate-7-minute-weight-loss-workout-for-beginners-244479.html" class="tint" title="The Ultimate 7 Minute Weight Loss Workout For Beginners">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/exercise-card_1440407967_502x234.gif" border="0" alt="The Ultimate 7 Minute Weight Loss Workout For Beginners" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/the-ultimate-7-minute-weight-loss-workout-for-beginners-244479.html" title="The Ultimate 7 Minute Weight Loss Workout For Beginners">
                        The Ultimate 7 Minute Weight Loss Workout For Beginners                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/punjabi-singer-jassi-says-steroids-to-be-blamed-for-honey-singhs-mysterious-disappearance-and-rehab-stint-244508.html" class="tint" title="Punjabi Singer Jassi Says Steroids To Be Blamed For Honey Singh's Mysterious Disappearance And Rehab Stint!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/hscard2_1440483915_1440483924_502x234.jpg" border="0" alt="Punjabi Singer Jassi Says Steroids To Be Blamed For Honey Singh's Mysterious Disappearance And Rehab Stint!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/punjabi-singer-jassi-says-steroids-to-be-blamed-for-honey-singhs-mysterious-disappearance-and-rehab-stint-244508.html" title="Punjabi Singer Jassi Says Steroids To Be Blamed For Honey Singh's Mysterious Disappearance And Rehab Stint!">
                        Punjabi Singer Jassi Says Steroids To Be Blamed For Honey Singh's Mysterious Disappearance And Rehab Stint!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/its-time-to-remember-the-brave-souls-who-gave-up-their-lives-for-india-in-the-1965-war-244513.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/its-time-to-remember-the-brave-souls-who-gave-up-their-lives-for-india-in-the-1965-war-244513.html" class="tint" title="It's Time To Remember The Brave Souls Who Gave Up Their Lives For India In The 1965 War">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/1965_card1_1440489275_502x234.jpg" border="0" alt="It's Time To Remember The Brave Souls Who Gave Up Their Lives For India In The 1965 War" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/its-time-to-remember-the-brave-souls-who-gave-up-their-lives-for-india-in-the-1965-war-244513.html" title="It's Time To Remember The Brave Souls Who Gave Up Their Lives For India In The 1965 War">
                        It's Time To Remember The Brave Souls Who Gave Up Their Lives For India In The 1965 War                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-had-promised-this-kashmiri-widow-a-new-house-a-500-crore-hit-later-shes-still-waiting-244506.html" class="tint" title="Salman Had Promised This Kashmiri Widow A New House, A 500 Crore Hit Later She's Still Waiting!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440483357_502x234.jpg" border="0" alt="Salman Had Promised This Kashmiri Widow A New House, A 500 Crore Hit Later She's Still Waiting!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-had-promised-this-kashmiri-widow-a-new-house-a-500-crore-hit-later-shes-still-waiting-244506.html" title="Salman Had Promised This Kashmiri Widow A New House, A 500 Crore Hit Later She's Still Waiting!">
                        Salman Had Promised This Kashmiri Widow A New House, A 500 Crore Hit Later She's Still Waiting!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/5-happy-adoption-stories-to-remind-you-why-we-need-friendicoes-in-our-lives-forever-244505.html" class="tint" title="5 Happy Adoption Stories To Remind You Why We Need Friendicoes In Our Lives. Forever">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/dog5_1440482956_502x234.jpg" border="0" alt="5 Happy Adoption Stories To Remind You Why We Need Friendicoes In Our Lives. Forever" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/5-happy-adoption-stories-to-remind-you-why-we-need-friendicoes-in-our-lives-forever-244505.html" title="5 Happy Adoption Stories To Remind You Why We Need Friendicoes In Our Lives. Forever">
                        5 Happy Adoption Stories To Remind You Why We Need Friendicoes In Our Lives. Forever                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/you-need-to-live-your-life-well-because-its-the-only-life-you-have-244463.html" class="tint" title="You Need To 'Live' Your Life Well Because It's The Only Life You Have">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440395968_502x234.jpg" border="0" alt="You Need To 'Live' Your Life Well Because It's The Only Life You Have" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/you-need-to-live-your-life-well-because-its-the-only-life-you-have-244463.html" title="You Need To 'Live' Your Life Well Because It's The Only Life You Have">
                        You Need To 'Live' Your Life Well Because It's The Only Life You Have                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/sooraj-pancholi-talks-about-exflame-jiah-khans-suicide-and-how-his-life-has-never-been-the-same-244501.html" class="tint" title="Sooraj Pancholi Talks About Ex- Flame Jiah Khan's Suicide And How His Life Has Never Been The Same">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card2_1440479491_1440479502_502x234.jpg" border="0" alt="Sooraj Pancholi Talks About Ex- Flame Jiah Khan's Suicide And How His Life Has Never Been The Same" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/sooraj-pancholi-talks-about-exflame-jiah-khans-suicide-and-how-his-life-has-never-been-the-same-244501.html" title="Sooraj Pancholi Talks About Ex- Flame Jiah Khan's Suicide And How His Life Has Never Been The Same">
                        Sooraj Pancholi Talks About Ex- Flame Jiah Khan's Suicide And How His Life Has Never Been The Same                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/dude-think-you-can-handle-a-little-bit-of-broga-244484.html" class="tint" title="Dude, Think You Can Handle A Little Bit Of Broga?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/broga-card_1440411966_502x234.jpg" border="0" alt="Dude, Think You Can Handle A Little Bit Of Broga?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/dude-think-you-can-handle-a-little-bit-of-broga-244484.html" title="Dude, Think You Can Handle A Little Bit Of Broga?">
                        Dude, Think You Can Handle A Little Bit Of Broga?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/9-things-that-are-preventing-you-from-having-great-sex-244472.html" class="tint" title="9 Things That Are Preventing You From Having Great Sex">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/sex-card_1440481144_502x234.gif" border="0" alt="9 Things That Are Preventing You From Having Great Sex" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/9-things-that-are-preventing-you-from-having-great-sex-244472.html" title="9 Things That Are Preventing You From Having Great Sex">
                        9 Things That Are Preventing You From Having Great Sex                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/another-radhe-maa-dubsmash-thatll-give-item-girls-a-run-for-their-money-244495.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/another-radhe-maa-dubsmash-thatll-give-item-girls-a-run-for-their-money-244495.html" class="tint" title="Another Radhe Maa Dubsmash That'll Give Item Girls A Run For Their Money">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/radhemaa_card_1440420775_502x234.jpg" border="0" alt="Another Radhe Maa Dubsmash That'll Give Item Girls A Run For Their Money" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/another-radhe-maa-dubsmash-thatll-give-item-girls-a-run-for-their-money-244495.html" title="Another Radhe Maa Dubsmash That'll Give Item Girls A Run For Their Money">
                        Another Radhe Maa Dubsmash That'll Give Item Girls A Run For Their Money                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/sonakshi-sinhas-california-vacation-will-teach-you-how-to-mix-business-with-pleasure-244476.html" class="tint" title="Sonakshi Sinha's California Vacation Will Teach You How To Mix Business With Pleasure!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1440405934_1440405940_502x234.jpg" border="0" alt="Sonakshi Sinha's California Vacation Will Teach You How To Mix Business With Pleasure!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/sonakshi-sinhas-california-vacation-will-teach-you-how-to-mix-business-with-pleasure-244476.html" title="Sonakshi Sinha's California Vacation Will Teach You How To Mix Business With Pleasure!">
                        Sonakshi Sinha's California Vacation Will Teach You How To Mix Business With Pleasure!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/14-mad-science-advancements-that-could-change-everything-everything-244387.html" class="tint" title="14 Mad Science Advancements That Could Change Everything. EVERYTHING!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440070978_502x234.jpg" border="0" alt="14 Mad Science Advancements That Could Change Everything. EVERYTHING!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/14-mad-science-advancements-that-could-change-everything-everything-244387.html" title="14 Mad Science Advancements That Could Change Everything. EVERYTHING!">
                        14 Mad Science Advancements That Could Change Everything. EVERYTHING!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-80yearold-man-is-the-worlds-most-viral-and-best-dressed-grandpa-244490.html" class="tint" title="This 80-Year-Old Man Is The World's Most Viral (And Best Dressed) Grandpa!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/aa_1440416358_1440416371_502x234.jpg" border="0" alt="This 80-Year-Old Man Is The World's Most Viral (And Best Dressed) Grandpa!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-80yearold-man-is-the-worlds-most-viral-and-best-dressed-grandpa-244490.html" title="This 80-Year-Old Man Is The World's Most Viral (And Best Dressed) Grandpa!">
                        This 80-Year-Old Man Is The World's Most Viral (And Best Dressed) Grandpa!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/prem-ratan-dhan-payo-+-10-longest-climaxes-in-movie-history-dont-laugh-244480.html" class="tint" title="Prem Ratan Dhan Payo + 10 Longest Climaxes In Movie History (Don't Laugh)">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/salman-khan-prdps_1440408330_1440408494_1440408502_1440408512_502x234.jpg" border="0" alt="Prem Ratan Dhan Payo + 10 Longest Climaxes In Movie History (Don't Laugh)" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/prem-ratan-dhan-payo-+-10-longest-climaxes-in-movie-history-dont-laugh-244480.html" title="Prem Ratan Dhan Payo + 10 Longest Climaxes In Movie History (Don't Laugh)">
                        Prem Ratan Dhan Payo + 10 Longest Climaxes In Movie History (Don't Laugh)                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/after-ftii-fiasco-now-a-gujju-superstar-to-pick-indian-entries-for-the-oscars-244482.html" class="tint" title="After FTII Fiasco, Now A Gujju Superstar To Pick Indian Entries For The Oscars">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card1_1440410356_1440410367_502x234.jpg" border="0" alt="After FTII Fiasco, Now A Gujju Superstar To Pick Indian Entries For The Oscars" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/after-ftii-fiasco-now-a-gujju-superstar-to-pick-indian-entries-for-the-oscars-244482.html" title="After FTII Fiasco, Now A Gujju Superstar To Pick Indian Entries For The Oscars">
                        After FTII Fiasco, Now A Gujju Superstar To Pick Indian Entries For The Oscars                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/do-you-hog-like-a-horse-well-these-eating-tips-will-straighten-you-up-for-better-244376.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/do-you-hog-like-a-horse-well-these-eating-tips-will-straighten-you-up-for-better-244376.html" class="tint" title="Do You Hog Like A Horse? Well, These Eating Tips Will Straighten You Up For Better">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/overeating-acard_1440062914_502x234.gif" border="0" alt="Do You Hog Like A Horse? Well, These Eating Tips Will Straighten You Up For Better" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/do-you-hog-like-a-horse-well-these-eating-tips-will-straighten-you-up-for-better-244376.html" title="Do You Hog Like A Horse? Well, These Eating Tips Will Straighten You Up For Better">
                        Do You Hog Like A Horse? Well, These Eating Tips Will Straighten You Up For Better                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/open-letter-defends-delhis-drunk-cop-asks-us-drinkers-to-back-the-hell-off-244481.html" class="tint" title="Open Letter Defends Delhi's Drunk Cop. Asks Us Drinkers To Back The Hell Off!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440409876_502x234.jpg" border="0" alt="Open Letter Defends Delhi's Drunk Cop. Asks Us Drinkers To Back The Hell Off!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/open-letter-defends-delhis-drunk-cop-asks-us-drinkers-to-back-the-hell-off-244481.html" title="Open Letter Defends Delhi's Drunk Cop. Asks Us Drinkers To Back The Hell Off!">
                        Open Letter Defends Delhi's Drunk Cop. Asks Us Drinkers To Back The Hell Off!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-proves-that-angry-punjabi-dads-are-the-most-entertaining-244475.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-proves-that-angry-punjabi-dads-are-the-most-entertaining-244475.html" class="tint" title="This Video Proves That Angry Punjabi Dads Are The Most Entertaining">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/punjabi_dads_card1_1440413846_502x234.jpg" border="0" alt="This Video Proves That Angry Punjabi Dads Are The Most Entertaining" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-proves-that-angry-punjabi-dads-are-the-most-entertaining-244475.html" title="This Video Proves That Angry Punjabi Dads Are The Most Entertaining">
                        This Video Proves That Angry Punjabi Dads Are The Most Entertaining                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-pictures-of-erstwhile-india-that-show-how-people-made-a-living-back-then-244392.html" class="tint" title="11 Pictures Of Erstwhile India That Show How People Made A Living Back Then">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440421167_502x234.jpg" border="0" alt="11 Pictures Of Erstwhile India That Show How People Made A Living Back Then" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-pictures-of-erstwhile-india-that-show-how-people-made-a-living-back-then-244392.html" title="11 Pictures Of Erstwhile India That Show How People Made A Living Back Then">
                        11 Pictures Of Erstwhile India That Show How People Made A Living Back Then                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/channel-ready-to-double-up-salmans-fee-to-get-him-onboard-for-big-boss-season-9_-244466.html" class="tint" title="Channel Ready To Double Up Salman's Fee To Get Him On-board For Big Boss Season 9!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card2_1440401375_1440401381_502x234.jpg" border="0" alt="Channel Ready To Double Up Salman's Fee To Get Him On-board For Big Boss Season 9!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/channel-ready-to-double-up-salmans-fee-to-get-him-onboard-for-big-boss-season-9_-244466.html" title="Channel Ready To Double Up Salman's Fee To Get Him On-board For Big Boss Season 9!">
                        Channel Ready To Double Up Salman's Fee To Get Him On-board For Big Boss Season 9!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/aamir-khans-excessive-crying-is-keeping-people-busy-and-its-bloody-hilarious-244474.html" class="tint" title="Aamir Khan's Excessive Crying Is Keeping People Busy, And It's Bloody Hilarious!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440401666_502x234.jpg" border="0" alt="Aamir Khan's Excessive Crying Is Keeping People Busy, And It's Bloody Hilarious!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/aamir-khans-excessive-crying-is-keeping-people-busy-and-its-bloody-hilarious-244474.html" title="Aamir Khan's Excessive Crying Is Keeping People Busy, And It's Bloody Hilarious!">
                        Aamir Khan's Excessive Crying Is Keeping People Busy, And It's Bloody Hilarious!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/20-effective-ways-to-keep-yourself-healthy-happy-and-motivated-243748.html" class="tint" title="20 Effective Ways To Keep Yourself Healthy, Happy And Motivated">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/happy-card_1438589839_502x234.gif" border="0" alt="20 Effective Ways To Keep Yourself Healthy, Happy And Motivated" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/20-effective-ways-to-keep-yourself-healthy-happy-and-motivated-243748.html" title="20 Effective Ways To Keep Yourself Healthy, Happy And Motivated">
                        20 Effective Ways To Keep Yourself Healthy, Happy And Motivated                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/8-reasons-why-life-at-the-international-space-station-is-crazy-yet-awesome-231881.html" class="tint" title="11 Reasons Why Life At The International Space Station Is Crazy Yet Awesome">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Apr/555_1429018318_1429018338_502x234.jpg" border="0" alt="11 Reasons Why Life At The International Space Station Is Crazy Yet Awesome" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/8-reasons-why-life-at-the-international-space-station-is-crazy-yet-awesome-231881.html" title="11 Reasons Why Life At The International Space Station Is Crazy Yet Awesome">
                        11 Reasons Why Life At The International Space Station Is Crazy Yet Awesome                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/whoa-shahrukh-wants-to-do-a-film-like-ghostbusters-with-salman-and-aamir-244467.html" class="tint" title="Whoa! Shahrukh Says It's Possible For The Three Khans To Come Onscreen Together!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1440398944_1440398976_502x234.jpg" border="0" alt="Whoa! Shahrukh Says It's Possible For The Three Khans To Come Onscreen Together!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/whoa-shahrukh-wants-to-do-a-film-like-ghostbusters-with-salman-and-aamir-244467.html" title="Whoa! Shahrukh Says It's Possible For The Three Khans To Come Onscreen Together!">
                        Whoa! Shahrukh Says It's Possible For The Three Khans To Come Onscreen Together!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/watch-this-ultimate-trick-shot-dunk-that-will-amaze-you-244465.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/watch-this-ultimate-trick-shot-dunk-that-will-amaze-you-244465.html" class="tint" title="Watch This Ultimate Trick Shot Dunk That Will Amaze You!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/trick-shot-dunk_card_1440397625_502x234.jpg" border="0" alt="Watch This Ultimate Trick Shot Dunk That Will Amaze You!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/watch-this-ultimate-trick-shot-dunk-that-will-amaze-you-244465.html" title="Watch This Ultimate Trick Shot Dunk That Will Amaze You!">
                        Watch This Ultimate Trick Shot Dunk That Will Amaze You!                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/self/9-signs-you-have-a-love-hate-relationship-with-the-monsoon-244372.html" class="tint" title="9 Signs You Have A Love-Hate Relationship With The Monsoon">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440394907_1440394918_218x102.jpg" border="0" alt="9 Signs You Have A Love-Hate Relationship With The Monsoon"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/9-signs-you-have-a-love-hate-relationship-with-the-monsoon-244372.html" title="9 Signs You Have A Love-Hate Relationship With The Monsoon">
                            9 Signs You Have A Love-Hate Relationship With The Monsoon                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/food/48-culinary-hacks-that-will-make-you-the-master-of-your-kitchen-244530.html" class="tint" title="48 Culinary Hacks That Will Make You The Master Of Your Kitchen">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/hacks502_1440602477_218x102.jpg" border="0" alt="48 Culinary Hacks That Will Make You The Master Of Your Kitchen"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/food/48-culinary-hacks-that-will-make-you-the-master-of-your-kitchen-244530.html" title="48 Culinary Hacks That Will Make You The Master Of Your Kitchen">
                            48 Culinary Hacks That Will Make You The Master Of Your Kitchen                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/health/recipes/14-healthy-and-delicious-faaral-recipes-for-fasting-season-244478.html" class="tint" title="14 Healthy And Delicious Faaral Recipes For Fasting Season">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/faaral_1440407203_218x102.jpg" border="0" alt="14 Healthy And Delicious Faaral Recipes For Fasting Season"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/recipes/14-healthy-and-delicious-faaral-recipes-for-fasting-season-244478.html" title="14 Healthy And Delicious Faaral Recipes For Fasting Season">
                            14 Healthy And Delicious Faaral Recipes For Fasting Season                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/health/healthyliving/the-ultimate-7-minute-weight-loss-workout-for-beginners-244479.html" class="tint" title="The Ultimate 7 Minute Weight Loss Workout For Beginners">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/exercise-card_1440407967_218x102.gif" border="0" alt="The Ultimate 7 Minute Weight Loss Workout For Beginners"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/the-ultimate-7-minute-weight-loss-workout-for-beginners-244479.html" title="The Ultimate 7 Minute Weight Loss Workout For Beginners">
                            The Ultimate 7 Minute Weight Loss Workout For Beginners                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-had-promised-this-kashmiri-widow-a-new-house-a-500-crore-hit-later-shes-still-waiting-244506.html" class="tint" title="Salman Had Promised This Kashmiri Widow A New House, A 500 Crore Hit Later She's Still Waiting!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440483357_218x102.jpg" border="0" alt="Salman Had Promised This Kashmiri Widow A New House, A 500 Crore Hit Later She's Still Waiting!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-had-promised-this-kashmiri-widow-a-new-house-a-500-crore-hit-later-shes-still-waiting-244506.html" title="Salman Had Promised This Kashmiri Widow A New House, A 500 Crore Hit Later She's Still Waiting!">
                            Salman Had Promised This Kashmiri Widow A New House, A 500 Crore Hit Later She's Still Waiting!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#hp_block_2').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       showBigAD2('bigAd2_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#hp_block_3').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      showBigAD3('bigAd3_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#container4').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible

       BADros('badRos_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/startups-502_1440586013_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/5_1440581916_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/vijaydhawanpujara_1440580658_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/indrani502_1440578919_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/card-2_1440577783_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1440417736_1440417747_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/sa_1440592628_1440592643_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440569809_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440590807_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1440566621_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/lscard_1440589263_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/card1_1440584152_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440569809_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/card_1440576465_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440578322_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/harbhajankohliautotweet_1440573246_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/dawood-502_1440572931_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/fisted5_1440568735_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/lscard_1440589263_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/hawking-card-2_1440590839_1440590849_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440494943_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/aaa_1440573114_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/shilpashetty_1439881579_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/katti-batti_1440581833_1440581840_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/bn_1440488478_1440488484_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440502306_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/card_1440570619_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440568489_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/dog1-5_1440504069_1440504072_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/ayaj-502_1440508374_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/oxygen-finance-5_1440505361_1440505364_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440590807_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card1_1440584152_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1440589578_1440589592_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/katti-batti_1440581833_1440581840_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/brozone_card_1440573876_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card-1_1440567564_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/not-getting-prgnant-card_1438764353_502x234.gif","http://media.indiatimes.in/media/videocafe/2015/Aug/502_1440575902_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/fartingprank_card_1440569760_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/aaa_1440573114_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/hacks502_1440602477_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440491518_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/shilpashetty_1439881579_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1440566621_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/faaral_1440407203_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/split1_1440498319_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440584251_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440502306_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/maggi_ash_card_1440506182_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/aaa_1440487292_1440487300_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/bn_1440488478_1440488484_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/pcos_1440488735_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/screenshot_2_1440496458_1440496465_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/jazbaa_trailer_1440498922_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440155658_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/exercise-card_1440407967_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/hscard2_1440483915_1440483924_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/1965_card1_1440489275_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440483357_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/dog5_1440482956_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440395968_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card2_1440479491_1440479502_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/broga-card_1440411966_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/sex-card_1440481144_502x234.gif","http://media.indiatimes.in/media/videocafe/2015/Aug/radhemaa_card_1440420775_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1440405934_1440405940_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440070978_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/aa_1440416358_1440416371_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/salman-khan-prdps_1440408330_1440408494_1440408502_1440408512_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card1_1440410356_1440410367_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/overeating-acard_1440062914_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/card_1440409876_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/punjabi_dads_card1_1440413846_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440421167_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card2_1440401375_1440401381_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440401666_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/happy-card_1438589839_502x234.gif","http://media.indiatimes.in/media/content/2015/Apr/555_1429018318_1429018338_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1440398944_1440398976_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/trick-shot-dunk_card_1440397625_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440394907_1440394918_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/hacks502_1440602477_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/faaral_1440407203_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/exercise-card_1440407967_218x102.gif","http://media.indiatimes.in/media/content/2015/Aug/card_1440483357_218x102.jpg");
        active.b4=false;
    }
}
</script>





    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,490,577<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>48,612 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.9" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.9"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.9"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.9"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.9"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>