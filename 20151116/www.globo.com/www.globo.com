



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='16/11/2015 16:39:42' /><meta property='busca:modified' content='16/11/2015 16:39:42' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/9d6d233027f5.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/5db9cc20b961.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "{\"adUnit\":\"tvg_Globo.com.Home\", \"adPositionsDesktop\": [\"banner_slim_topo\",\"banner_slb_meio\",\"banner_slb_fim\",\"banner_selo4\",\"banner_floating\"], \"adPositionsMobile\":[\"banner_mobile_topo\",\"banner_mobile_meio\",\"banner_mobile_fim\"]}", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><header id="header-produto" class="header-navegacao header-home" data-analytics="T"><div id="glbbarrawidget"></div><div class="floating-bar"><div class="header-bar"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-floating" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-floating" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div></div></div></div></div></div><div class="cube-container"><div class="cube"><div class="face front"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-top" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-top" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div><div id="busca-padrao"></div></div></div></div></div></div></div></header><script>
(function(window,document) {try{new CustomEvent("test");} catch(e){var CustomEvent=function(event,params) {var evt;params=params || {bubbles: false,
cancelable: false,
detail: undefined};evt=document.createEvent("CustomEvent");evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);return evt;};
CustomEvent.prototype=window.Event.prototype;window.CustomEvent=CustomEvent;}
document.dispatchEvent(new CustomEvent('glb.headerDom.ready'));})(window,document);</script><style type="text/css">
    /*
       evita que o markup do menu seja exibido sem estilo enquanto
       ele nÃ£o foi carregado
    */
    #menu-container {
        display: none;
    }
    #menu-fonts {
        height: 1px;
        width: 1px;
        color: transparent;
        overflow: hidden;
        position: absolute;
    }
</style>

<nav id="menu-container" class="regua-navegacao-tab menu-no-animation">
    

<script id="menu-carousel-template" type="x-tmpl-mustache">

<div id="menu-carousel-header-sizes-infos" class="menu-carousel-header-sizes-infos">
</div>
<div id="menu-carousel-header" class="menu-carousel menu-carousel-header swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <a href="{{link}}" class="menu-carousel-link" data-index="{{index}}">
                <svg class="brand-icon">
                    <use class="menu-logo" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-{{name}}"></use>
                </svg>
            </a>
            <div class="menu-carousel-link-shadow"></div>
        </li>
        {{/portals}}
    </ul>
</div>
<div id="menu-carousel-body" class="menu-carousel menu-carousel-body swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <div id="menu-rounder" class="menu-rounder">{{{menu}}}</div>
        </li>
        {{/portals}}
    </ul>
</div>

</script>

<script id="menu-rounder-template" type="x-tmpl-mustache">

  <div id="menu-cascade" class="menu-cascade" data-menu-index="{{index}}">
    <ul class="menu-root menu-level menu-level-0">{{#items}}{{>recursive_partial}}{{/items}}</ul>
  </div>
  <div id="menu-addon" class="menu-addon"></div>

</script>

<script id="menu-custom-template" type="x-tmpl-mustache">

    {{#items}}{{>recursive_partial}}{{/items}}

</script>


<script id="menu-dinamico-template" type="x-tmpl-mustache">

<li id="menu-{{deepness}}-{{tituloSlug}}" class="menu-item {{#separador}} menu-item-separator{{/separador}}{{#destacado}} menu-item-highlighted{{/destacado}}{{#agrupador}} menu-item-grouped{{/agrupador}}{{#hasChildren}} is-father{{/hasChildren}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}}">

    {{#shouldBeLink}}<a href="{{link}}" class="menu-item-link">{{/shouldBeLink}}
    {{^shouldBeLink}}<span class="menu-item-link">{{/shouldBeLink}}
        <span class="menu-item-title">{{titulo}}<span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#regua-arrow"></use></svg></span></span>
    {{#shouldBeLink}}</a>{{/shouldBeLink}}
    {{^shouldBeLink}}</span>{{/shouldBeLink}}

    {{#hasChildren}}
        <div class="menu-level menu-submenu menu-submenu-level{{deepness}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}} menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back"><span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#regua-arrow"></use></svg></span><span class="regua-navegacao-menu-item">{{titulo}}</span></a>
            <ul class="menu-submenu-vertical-scroll">
            {{#children}}
                {{>recursive_partial}}
            {{/children}}
            <ul>
        </div>
    {{/hasChildren}}
</li>

</script>


    <div id="menu-rounder" class="menu-rounder">
        <div id="menu-cascade" class="menu-cascade" data-menu-index="0">
            

<ul class="menu-root menu-level menu-level-0">
    
        




<li class="menu-item is-father " id="menu-1-famosos">

    <a class="menu-item-link">
        <span class="menu-item-title">Famosos</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Famosos</a>
            
                

                




<li class="menu-item " id="menu-2-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-paparazzo">

    <a href="http://ego.globo.com/paparazzo/index.html" class="menu-item-link">
        <span class="menu-item-title">Paparazzo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/noticias-da-tv/index.html" class="menu-item-link">
        <span class="menu-item-title">Patricia Kogut</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Quem</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-retratos-da-vida">

    <a href="http://extra.globo.com/famosos/" class="menu-item-link">
        <span class="menu-item-title">Retratos da Vida</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-moda-estilo">

    <a class="menu-item-link">
        <span class="menu-item-title">Moda &amp; Estilo</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Moda &amp; Estilo</a>
            
                

                




<li class="menu-item " id="menu-2-ela-no-o-globo">

    <a href="http://ela.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ela no O Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-estilo-no-gshow">

    <a href="http://gshow.globo.com/Estilo/" class="menu-item-link">
        <span class="menu-item-title">Estilo no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-ego">

    <a href="http://ego.globo.com/moda/index.html" class="menu-item-link">
        <span class="menu-item-title">Moda no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-gnt">

    <a href="http://gnt.globo.com/moda/" class="menu-item-link">
        <span class="menu-item-title">Moda no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-glamour">

    <a href="http://revistaglamour.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Glamour</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-gq-brasil">

    <a href="http://gq.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista GQ Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-marie-claire">

    <a href="http://revistamarieclaire.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Marie Claire</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-vogue">

    <a href="http://vogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Vogue</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-bem-estar-saude">

    <a class="menu-item-link">
        <span class="menu-item-title">Bem-Estar &amp; SaÃºde</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Bem-Estar &amp; SaÃºde</a>
            
                

                




<li class="menu-item " id="menu-2-beleza-no-ego">

    <a href="http://ego.globo.com/beleza/index.html" class="menu-item-link">
        <span class="menu-item-title">Beleza no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gnt">

    <a href="http://gnt.globo.com/beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gshow">

    <a href="http://gshow.globo.com/Estilo/Beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-g1">

    <a href="http://g1.globo.com/bemestar/" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no G1</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-gnt">

    <a href="http://gnt.com.br/bemestar" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-horoscopo-no-ego">

    <a href="http://horoscopo.ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">HorÃ³scopo no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-maes-no-gnt">

    <a href="http://gnt.globo.com/maes-e-filhos/" class="menu-item-link">
        <span class="menu-item-title">MÃ£es no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-meus-5-minutos">

    <a href="http://meus5minutos.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Meus 5 Minutos</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-crescer">

    <a href="http://revistacrescer.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Crescer</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-casa-decoracao">

    <a class="menu-item-link">
        <span class="menu-item-title">Casa &amp; DecoraÃ§Ã£o</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Casa &amp; DecoraÃ§Ã£o</a>
            
                

                




<li class="menu-item " id="menu-2-casa-no-gnt">

    <a href="http://gnt.globo.com/casa-e-decoracao/" class="menu-item-link">
        <span class="menu-item-title">Casa no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-casa-vogue">

    <a href="http://casavogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Casa Vogue</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-casa-e-jardim">

    <a href="http://revistacasaejardim.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Casa e Jardim</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-do-zap">

    <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=revista-imoveis" class="menu-item-link">
        <span class="menu-item-title">Revista do Zap</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-gastronomia">

    <a class="menu-item-link">
        <span class="menu-item-title">Gastronomia</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Gastronomia</a>
            
                

                




<li class="menu-item " id="menu-2-casa-e-comida">

    <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/" class="menu-item-link">
        <span class="menu-item-title">Casa e Comida</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-colheradas">

    <a href="http://meus5minutos.globo.com/blogs/Colheradas/" class="menu-item-link">
        <span class="menu-item-title">Colheradas</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-da-ana-maria">

    <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/" class="menu-item-link">
        <span class="menu-item-title">Receitas da Ana Maria</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitascom">

    <a href="http://gshow.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas.com</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-no-gnt">

    <a href="http://gnt.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas no GNT</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-musica">

    <a class="menu-item-link">
        <span class="menu-item-title">MÃºsica</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">MÃºsica</a>
            
                

                




<li class="menu-item " id="menu-2-altas-horas">

    <a href=" http://gshow.globo.com/programas/altas-horas/" class="menu-item-link">
        <span class="menu-item-title">Altas Horas </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-musica">

    <a href="http://g1.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">G1 MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-gshow-musica">

    <a href="http://gshow.globo.com/Musica/" class="menu-item-link">
        <span class="menu-item-title">Gshow MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musica-no-multishow">

    <a href="http://multishow.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica no Multishow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musicacombr">

    <a href="http://musica.com.br/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica.com.br</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-radiobeat">

    <a href="http://radiobeat.com.br/" class="menu-item-link">
        <span class="menu-item-title">RADIOBEAT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-som-brasil">

    <a href="http://globotv.globo.com/rede-globo/som-brasil/" class="menu-item-link">
        <span class="menu-item-title">Som Brasil</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-cinema-teatro">

    <a class="menu-item-link">
        <span class="menu-item-title">Cinema &amp; Teatro</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Cinema &amp; Teatro</a>
            
                

                




<li class="menu-item " id="menu-2-canal-brasil">

    <a href="http://canalbrasil.globo.com/ " class="menu-item-link">
        <span class="menu-item-title">Canal Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-canal-universal">

    <a href="http://universal.globo.com" class="menu-item-link">
        <span class="menu-item-title">Canal Universal</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-filmes-na-globo">

    <a href="http://redeglobo.globo.com/filmes/index.html" class="menu-item-link">
        <span class="menu-item-title">Filmes na Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-cinema">

    <a href="http://g1.globo.com/pop-arte/cinema/" class="menu-item-link">
        <span class="menu-item-title">G1 Cinema</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-globo-filmes">

    <a href=" http://globofilmes.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Globo Filmes</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-megapix">

    <a href="http://megapix.globo.com/" class="menu-item-link">
        <span class="menu-item-title">MegaPix</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-monet">

    <a href="http://revistamonet.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Monet</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-teatro">

    <a href="http://www.agentesevenoteatro.com.br/" class="menu-item-link">
        <span class="menu-item-title">Teatro</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-telecine">

    <a href="http://telecine.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Telecine</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">ego</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">quem</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">patricia kogut</span>
    </a>

    
</li>

    
</ul>

        </div>
        <div id="menu-addon" class="menu-addon"></div>
    </div>

    
</nav>

<div id="menu-fonts">
    
    
    <span class="open-sans">BESbswy</span>
    <span class="open-sans-bold">BESbswy</span>
    <span class="open-sans-light">BESbswy</span>
    <span class="roboto-slab">BESbswy</span>
    <span class="proximanova-semibold">BESbswy</span>
</div>

<div id="menu-content-overlay"></div>
<script>
(function (window) {window.REGUA_SETTINGS=window.REGUA_SETTINGS || {};window.REGUA_SETTINGS.portalName = "home";window.REGUA_SETTINGS.portalHome = "http://www.globo.com/";window.REGUA_SETTINGS.portalsList = ["home","g1","ge","gshow","famosos","techtudo","globotv","globoplay","globosatplay"];window.REGUA_SETTINGS.portalsLinkList = ["http://www.globo.com/","http://g1.globo.com/index.html","http://globoesporte.globo.com/","http://gshow.globo.com/","http://famosos.globo.com/","http://www.techtudo.com.br/","http://globotv.globo.com/","http://globoplay.globo.com/","http://globosatplay.globo.com/"];window.REGUA_SETTINGS.staticUrl = "http://s.glbimg.com/en/ho/static/";window.REGUA_SETTINGS.svgSpriteName = "sprite-c848575be1.svg";window.REGUA_SETTINGS.suggestUrl = "";})(window);
</script><script>(function(){var e,t,n,o;e=function(){function e(){this.name="Android",this.bridge=window.GloboBridge||{navigated:function(e){},menuAction:function(e){},reguaAction:function(e){}}}return e.prototype.navigateTo=function(e){this.bridge.navigated(JSON.stringify(e))},e.prototype.menuAction=function(e){this.bridge.menuAction(JSON.stringify(e))},e.prototype.reguaAction=function(e){this.bridge.reguaAction(JSON.stringify(e))},e}(),t=function(){function e(){this.name="iOS",this.connectWebViewJavascriptBridge(function(e){return e.init(function(e,t){return window.glb.masterApp.manageMessage(e)})})}return e.prototype.connectWebViewJavascriptBridge=function(e){window.WebViewJavascriptBridge?e(WebViewJavascriptBridge):document.addEventListener("WebViewJavascriptBridgeReady",function(){e(WebViewJavascriptBridge)},!1)},e.prototype.brigdeSendJson=function(e){return this.connectWebViewJavascriptBridge(function(t){t.send(JSON.stringify(e))})},e.prototype.navigateTo=function(e){this.brigdeSendJson(e)},e.prototype.menuAction=function(e){this.brigdeSendJson(e)},e.prototype.reguaAction=function(e){this.brigdeSendJson(e)},e}(),n=function(){function n(e){this.platformApp=e,this.initCalled=!1,null!=this.platformApp&&this.bind()}return n.prototype.init=function(){this.initCalled||(this.initCalled=!0,this.header&&(this.processHeaderInfo(),this.bindAfterInit()))},n.prototype.processHeaderInfo=function(){this.countHeaderTries=0,this.maxHeaderTries=100,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer?this.processHeaderInfoAction():setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)},n.prototype.processHeaderInfoAction=function(){var e;if(this.countHeaderTries++,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer)this.cacheVariables(),e=this.getHeaderData(),this.navigateTo(e);else{if(this.countHeaderTries>this.maxHeaderTries)return void console.log("Nao achou o markup interno do header");setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)}},n.prototype.bind=function(){var e;return this.bindAppEvents(),this.header=document.querySelector("#header-produto"),e=function(e){return function(){return e.header||(e.header=document.querySelector("#header-produto")),e.init()}}(this),this.header?this.init():(document.addEventListener("glb.headerDom.ready",e),document.addEventListener("DOMContentLoaded",e))},n.prototype.cacheVariables=function(){return this.elements={},this.elements.header=this.header,this.elements.headerCubeContainer=this.headerCubeContainer,this.elements.headerFront=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".front"):null,this.elements.headerLogoProduto=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-produto-container"):null,this.elements.headerLogoProdutoLink=this.elements.headerLogoProduto?this.elements.headerLogoProduto.parentNode:null,this.elements.headerLogoArea=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-area .logo"):null,this.elements.headerLogoAreaLink=this.elements.headerLogoProduto?this.elements.headerLogoArea.parentNode:null,this.elements.headerSubeditoria=this.elements.headerFront?this.elements.headerFront.querySelector(".menu-subeditoria"):null,this.elements.headerSubeditoriaLink=this.elements.headerSubeditoria?this.elements.headerSubeditoria.querySelector("a"):null,!0},n.prototype.bindAppEvents=function(){document.addEventListener("glb.menu-carousel.ready",function(e){return function(){return e.platformApp.menuAction({action:"menuReady"})}}(this)),document.addEventListener("glb.menu-carousel.shown.before",function(e){return function(){return e.platformApp.menuAction({action:"menuShownBefore"})}}(this)),document.addEventListener("glb.menu-carousel.shown.after",function(e){return function(){return e.platformApp.menuAction({action:"menuShownAfter"})}}(this)),document.addEventListener("glb.menu-carousel.off.before",function(e){return function(){return e.platformApp.menuAction({action:"menuOffBefore"})}}(this)),document.addEventListener("glb.menu-carousel.off.after",function(e){return function(){return e.platformApp.menuAction({action:"menuOffAfter"})}}(this)),document.addEventListener("glb.regua.exists",function(e){return function(){return e.platformApp.reguaAction({action:"reguaExists"})}}(this))},n.prototype.bindAfterInit=function(){},n.prototype._getText=function(e){return e?(e.innerText||e.textContent).trim()||"":""},n.prototype._getHref=function(e){return e?e.getAttribute("href")||"":""},n.prototype._getCssProp=function(e,t){var n;return e?(n=window.getComputedStyle(e),n.getPropertyValue(t)||""):""},n.prototype._getBackgroundColor=function(e){return this._getCssProp(e,"background-color")},n.prototype.getHeaderData=function(){var e;return e={color:this._getBackgroundColor(this.elements.headerFront),editoria:"",editoriaHref:"",subeditoria:"",subeditoriaHref:"",logo:"",logoHref:""},this.elements.headerLogoProduto?(e.logo=this._getText(this.elements.headerLogoProduto),e.logoHref=this._getHref(this.elements.headerLogoProdutoLink),e.editoria=this._getText(this.elements.headerLogoArea),e.editoriaHref=this._getHref(this.elements.headerLogoArea),this.elements.headerSubeditoria&&(e.subeditoria=this._getText(this.elements.headerSubeditoria),e.subeditoriaHref=this._getHref(this.elements.headerSubeditoriaLink))):(e.logo=this._getText(this.elements.headerLogoArea),e.logoHref=this._getHref(this.elements.headerLogoAreaLink)),e},n.prototype.navigateTo=function(e){e.action="navigateTo",e.url=window.location.href,this.platformApp?this.platformApp.navigateTo(e):console.log("Sem Plataforma")},n.prototype.manageMessage=function(e){var t,n;return e&&(t=JSON.parse(e),t.action="regua-change")?(n=t.rulerItem||"home",null!=window.$?$(document).trigger("glb.regua.change",[n]):console.log("Sem jQuery")):void 0},n.prototype.getUserAgent=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n.prototype.getPlatformApp=function(){var n;return n=this.getUserAgent(),n.match(/(iPad|iPhone|iPod)/g)?new t:n.match(/(Android)/g)?new e:void 0},n}(),window.glb=window.glb||{},window.glb.MasterApp=n,window.glb.AndroidApp=e,window.glb.IosApp=t,window.initMasterApp=function(){var e;if(!window.glb.masterApp)return e=n.prototype.getPlatformApp(),window.glb.masterApp=new n(e)},o=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,o||window.initMasterApp()}).call(this);</script><script>(function(){var n,o,e,t;e=function(){return/MobApp_Android|MobApp_iOS|MobApp_WP/},o=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n=function(){var n,t,a,i,r;i=o(),r=i.split(" ");for(a in r)if(n=r[a],e().test(n))return t=n.split("/"),{name:t[0].replace("_"," "),version:t[1],frameworkVersion:t[2]};return null},window.addGlbOnAppClass=function(){var o;window.glb=window.glb||{},window.glb.extractAppFromUserAgent||(window.glb.extractAppFromUserAgent=n,window.glb.nativeAppInfo=n(),null!==window.glb.nativeAppInfo&&(window.glb.nativeAppInfo.frameworkVersion?(o=parseInt(window.glb.nativeAppInfo.frameworkVersion,10),o>=1&&(document.documentElement.className+=" glb-on-app")):document.documentElement.className+=" glb-on-app-comnio"))},t=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,t||window.addGlbOnAppClass()}).call(this);</script><script>(function(){var n,o,e,i,w,t,d,a,r,s,u,l,c;try{new CustomEvent("test")}catch(i){e=i,n=function(n,o){var e;return e=void 0,o=o||{bubbles:!1,cancelable:!1,detail:void 0},e=document.createEvent("CustomEvent"),e.initCustomEvent(n,o.bubbles,o.cancelable,o.detail),e},n.prototype=window.Event.prototype,window.CustomEvent=n}s=function(n,o){var e;return null==n||null==o?!1:(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)"),!!n.className.match(e))},o=function(n,o){s(n,o)||(n.className+=" "+o)},l=function(n,o){var e;s(n,o)&&(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)","g"),n.className=n.className.replace(e,""))},t=function(){return window.myInnerWidth||window.innerWidth},w=function(){return window.myInnerHeight||window.innerHeight},d=function(){var n;return null!=window.isAndroidBrowser?window.isAndroidBrowser:(n=navigator.userAgent,window.isAndroidBrowser=n.indexOf("Mozilla/5.0")>-1&&n.indexOf("Android ")>-1&&n.indexOf("AppleWebKit")>-1&&-1===n.indexOf("Chrome"),window.isAndroidBrowser)},a=function(){return null!=window.isPortrait?window.isPortrait:(window.isPortrait=t()<=w(),window.isPortrait)},r=function(){return null!=window.isTouchable?window.isTouchable:(window.isTouchable="ontouchstart"in window||navigator.msMaxTouchPoints||window.DocumentTouch&&document instanceof window.DocumentTouch,window.isTouchable)},u=function(){var n,o;return window.REGUAMAXWIDTHPORTRAIT=window.REGUAMAXWIDTHPORTRAIT||640,window.REGUAMAXWIDTHLANDSCAPE=window.REGUAMAXWIDTHLANDSCAPE||767,window.isPortrait=a(),window.isTouchable=r(),window.isAndroidBrowser=d(),n=t(),o=-1!==window.location.host.indexOf("g1.globo.com"),(window.isPortrait&&n<=window.REGUAMAXWIDTHPORTRAIT||!window.isPortrait&&n<=window.REGUAMAXWIDTHLANDSCAPE)&&!window.isAndroidBrowser&&document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")&&(null==window.glb.nativeAppInfo||window.glb.nativeAppInfo.frameworkVersion)&&!o},window.glb=window.glb||{},window.glb.hasClass=s,window.glb.addClass=o,window.glb.removeClass=l,window.addHasReguaClass=function(){window.glb.reguaShouldStart||(window.glb.reguaShouldStart=u,window.glb.reguaShouldStart()?(window.glb.addClass(document.documentElement,"has-regua"),window.glb.addClass(document.documentElement,"svg-support"),window.glb.removeClass(document.documentElement,"has-not-regua"),document.dispatchEvent(new CustomEvent("glb.regua.exists"))):(window.glb.removeClass(document.documentElement,"has-regua"),window.glb.removeClass(document.documentElement,"svg-support"),window.glb.addClass(document.documentElement,"has-not-regua")))},c=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,c||window.addHasReguaClass()}).call(this);</script><nav id="regua-navegacao" class="regua-navegacao "><div id="regua-svg-container" class="regua-svg-container"><svg xmlns="http://www.w3.org/2000/svg"><symbol id="eixo-icone-busca" viewBox="0 0 24 24"><path d="M21.67 20.27l-5.72-5.73c.98-1.26 1.55-2.83 1.55-4.54 0-4.14-3.36-7.5-7.5-7.5-4.15 0-7.5 3.36-7.5 7.5 0 4.14 3.35 7.5 7.5 7.5 1.7 0 3.27-.57 4.53-1.54l5.73 5.73c.42.41 1.07.43 1.46.04.39-.39.37-1.04-.05-1.46zM10 15.5c-3.04 0-5.5-2.46-5.5-5.5S6.96 4.5 10 4.5c3.03 0 5.5 2.46 5.5 5.5s-2.47 5.5-5.5 5.5z"/></symbol><symbol id="eixo-icone-feed" viewBox="0 0 24 24"><path d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path fill="#FFF" d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path d="M19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1zM17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1z"/><path d="M4 3v4H0v11c0 1.66 1.34 3 3 3h19c1.1 0 2-.9 2-2V3H4zm0 15.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2v9.09zM22 19H6V5h16v14z"/><path fill="#FFF" d="M4 9v9.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2zM6 5v14h16V5H6zm10 10h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1s-.45 1-1 1zm2-4h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1z"/></symbol><symbol id="eixo-icone-menu" viewBox="0 0 24 24"><path d="M22 6c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 12c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 18c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1z"/></symbol><symbol id="eixo-icone-usuario" viewBox="0 0 24 24"><path d="M12 2C8.4 2 5.5 4.9 5.5 8.5c0 3.61 2.9 6.5 6.5 6.5s6.5-2.89 6.5-6.5C18.5 4.9 15.6 2 12 2zm0 11.07c-2.49 0-4.5-2.02-4.5-4.5s2.01-4.5 4.5-4.5 4.5 2.02 4.5 4.5-2.01 4.5-4.5 4.5zM22.893 23c-.733 0-1.074-.455-1.074-.86C21.817 20.26 18.22 19 12 19s-9.818 1.136-9.818 3.14c0 .406-.41.86-1.092.86C.358 23 0 22.513 0 22.108 0 18.958 4.598 17 12 17s12 1.958 12 5.108c0 .405-.398.892-1.107.892z"/></symbol><symbol id="regua-arrow" viewBox="0 0 5 14"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.902 6.64L1.422.386C1.217.017.762-.11.402.104.05.317-.074.79.133 1.16l3.25 5.843L.1 12.847c-.208.368-.085.838.274 1.05.357.212.814.086 1.02-.28l3.412-6.074c.21-.243.26-.606.096-.904z"/></symbol></svg></div><div class="regua-navegacao-container"><ul class="regua-lista regua-lista-home"><li id="regua-navegacao-item-home" class="regua-navegacao-item active"><span data-href="http://www.globo.com/"
                                            data-target="regua-tab-home" class="home-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-feed"/></svg></span></li><li id="regua-navegacao-item-menu" class="regua-navegacao-item "><span data-target="regua-tab-menu" class="menu-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-menu"/></svg></span></li><li id="regua-navegacao-item-busca" class="regua-navegacao-item "><span data-target="regua-tab-busca" class="busca-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-busca"/></svg></span></li><li id="regua-navegacao-item-usuario" class="regua-navegacao-item "><span data-target="regua-tab-usuario" class="usuario-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-usuario"/></svg></span></li></ul></div></nav><nav id="regua-user-container" class="regua-user-container regua-user-home regua-navegacao-tab"><div id="regua-user-controls" class="regua-user-controls"></div><div id="regua-user-tabs" class="regua-tabs regua-user-tab-2"><div id="tab-labels" class="tab-labels"><span class="tab-label tab-label-1" data-index="1">notificaÃ§Ãµes</span><span class="tab-label tab-label-2" data-index="2">minha conta</span></div><div class="regua-tab regua-tab-1" data-index="1"><div class="regua-content"><ul id="regua-user-profile-notifications" class="regua-user-row regua-user-profile-notifications"></ul></div></div><div class="regua-tab regua-tab-2" data-index="2"><div class="regua-content"><ul id="regua-user-profile-list" class="regua-user-row regua-user-profile-list"></ul><div id="regua-user-logout" class="regua-user-logout"></div></div></div></div></nav><div id="regua-tab-busca" class="regua-navegacao-tab regua-tab-busca regua-busca-home"><div class="regua-search-header"><div class="regua-search-box"><form class="regua-search-form" action="http://globo.com/busca/" method="GET"><div class="regua-search-form-container regua-table"><div class="regua-search-input-box regua-table-cell"><input class="regua-search-input" name="q" type="search" placeholder="o que vocÃª procura?" autocorrect="off" autocapitalize="off" autocomplete="off"/></div><div class="regua-search-buttons-container regua-table-cell"><div class="regua-search-clear-container"><a href="#" class="regua-search-clear-button">clear</a></div><div class="search-button-icon"><input type="submit" value="go" class="regua-search-submit"/><div class="regua-icon-block"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></div><a href="#" class="regua-icon-block search-button-go"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></a></div></div></div></form></div></div><div class="regua-container-search-body"><ul class="regua-pre-suggest"></ul><div class="regua-container-suggest"><ul class="regua-suggest"></ul></div><div class="regua-container-results"></div></div></div>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://fantastico.globo.com/">
                                                <span class="titulo">FantÃ¡stico</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://especiais.g1.globo.com/educacao/app-g1-enem/">
                                                    <span class="titulo">Aplicativo G1 Enem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/tenis/">
                                                <span class="titulo">TÃªnis</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/nba/">
                                                    <span class="titulo">NBA</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-sul-americana/">
                                                    <span class="titulo">Copa Sul-Americana</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="series-originais">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries originais</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                <span class="titulo">The Voice</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                <span class="titulo">Estrelas</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                    <span class="titulo">The Voice</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/ligacoes-perigosas/">
                                                    <span class="titulo">LigaÃ§Ãµes Perigosas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/mister-brau/2015/">
                                                    <span class="titulo">Mister Brau</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/pe-na-cova/2015/">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series-originais">
                                        <div class="submenu-title">sÃ©ries originais</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/laboratorio-do-som/no-ar.html">
                                                    <span class="titulo">LaboratÃ³rio do som</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/lembrancas-do-iraja/no-ar.html">
                                                    <span class="titulo">LembranÃ§as do IrajÃ¡ - PÃ© na cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/maes-a-obra/no-ar.html">
                                                    <span class="titulo">MÃ£es Ã  obra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">O incrÃ­vel SuperÃ´nix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/os-desatinados-malhacao-seu-lugar-no-mundo/no-ar.html">
                                                    <span class="titulo">Os desatinados</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/verdades-secretasdoc/no-ar.html">
                                                    <span class="titulo">Verdades Secretas.doc</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/pop-arte/cinema/">
                                                <span class="titulo">G1 Cinema</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globoplay.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globoplay.globo.com/" data-menu-id="globo-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo play</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-11-1616:34:12Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area hide-mobile"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/11/estado-islamico-ameaca-eua-e-outros-paises-de-ataques-terroristas.html" class="foto " title="Terroristas ameaÃ§am EUA e outros paÃ­ses (GloboNews)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/DgFH4HzYxJnXfsko53Ya23ePL5s=/filters:quality(10):strip_icc()/s2.glbimg.com/0-AnCHw51VaG5PypvVIP7ngyqfo=/70x131:1741x679/335x110/s.glbimg.com/jo/g1/f/original/2015/11/16/ei.jpg" alt="Terroristas ameaÃ§am EUA e outros paÃ­ses (GloboNews)" title="Terroristas ameaÃ§am EUA e outros paÃ­ses (GloboNews)"
         data-original-image="s2.glbimg.com/0-AnCHw51VaG5PypvVIP7ngyqfo=/70x131:1741x679/335x110/s.glbimg.com/jo/g1/f/original/2015/11/16/ei.jpg" data-url-smart_horizontal="VcgitXDPTUi1BKKxK-NIys7P1GM=/400xorig/smart/filters:strip_icc()/" data-url-smart="VcgitXDPTUi1BKKxK-NIys7P1GM=/400xorig/smart/filters:strip_icc()/" data-url-feature="VcgitXDPTUi1BKKxK-NIys7P1GM=/400xorig/smart/filters:strip_icc()/" data-url-tablet="iJIEQG-eJOG_nLTWMLlQV_aAtaA=/340xorig/smart/filters:strip_icc()/" data-url-desktop="3gOR7cRrKoR8Xe0lYApx_0F-I0g=/335xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Terroristas ameaÃ§am EUA e outros paÃ­ses</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="CoalizÃ£o liderada pelos EUA destrÃ³i 116 caminhÃµes" href="http://g1.globo.com/mundo/ao-vivo/2015/explosoes-e-tiroteio-em-paris.html#/glb-feed-post/5649bee2c22d62235dea88f7">CoalizÃ£o liderada pelos EUA destrÃ³i 116 caminhÃµes</a></div></li></ul></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/ao-vivo/2015/explosoes-e-tiroteio-em-paris.html" class=" " title="Presidente promete intensificar ataques: &#39;A FranÃ§a estÃ¡ em guerra&#39;"><div class="conteudo"><h2>Presidente promete intensificar ataques: &#39;A FranÃ§a estÃ¡ em guerra&#39;</h2></div></a></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/economia/2015/11/16/2270-na-turquia-dilma-diz-que-levy-fica-onde-esta" class="foto " title="Dilma diz que Joaquim Levy 
&#39;fica onde estÃ¡&#39; (Volkan Furuncu / AFP ANTÃLIA)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/j4jICejdsksaUFOdcb-P_kL0E3U=/filters:quality(10):strip_icc()/s2.glbimg.com/Nikh5nACEHNZxf110bAHRdsPWSM=/216x237:447x341/155x70/s.glbimg.com/en/ho/f/original/2015/11/16/dilma_turquia.jpg" alt="Dilma diz que Joaquim Levy 
&#39;fica onde estÃ¡&#39; (Volkan Furuncu / AFP ANTÃLIA)" title="Dilma diz que Joaquim Levy 
&#39;fica onde estÃ¡&#39; (Volkan Furuncu / AFP ANTÃLIA)"
         data-original-image="s2.glbimg.com/Nikh5nACEHNZxf110bAHRdsPWSM=/216x237:447x341/155x70/s.glbimg.com/en/ho/f/original/2015/11/16/dilma_turquia.jpg" data-url-smart_horizontal="7veCGOCaWcUIyOEaRs6gglZ527w=/90x56/smart/filters:strip_icc()/" data-url-smart="7veCGOCaWcUIyOEaRs6gglZ527w=/90x56/smart/filters:strip_icc()/" data-url-feature="7veCGOCaWcUIyOEaRs6gglZ527w=/90x56/smart/filters:strip_icc()/" data-url-tablet="2ORHjMdqxNncYqWJvV-GN7Uf3ng=/160xorig/smart/filters:strip_icc()/" data-url-desktop="AFzhEUeBMAXH4CYP46daFcm2WMw=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Dilma diz que Joaquim Levy <br />&#39;fica onde estÃ¡&#39;</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/11/pinato-entrega-ao-conselho-de-etica-relatorio-preliminar-do-caso-cunha.html" class=" " title="Relator pede continuidade de aÃ§Ã£o contra Cunha no Conselho de Ãtica"><div class="conteudo"><h2>Relator pede continuidade de aÃ§Ã£o contra Cunha no Conselho de Ãtica</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://sportv.globo.com/site/combate/noticia/2015/11/cris-cyborg-ja-sabia-que-ronda-nao-aguenta-soco-de-verdade-na-cara.html" class=" " title="Cyborg: &#39;Ronda nÃ£o aguenta soco&#39; (globoesporte.com)"><div class="conteudo"><h2>Cyborg: &#39;Ronda nÃ£o aguenta soco&#39;</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Ex-agente se sente vingado: &#39;Criei um monstro&#39;" href="http://sportv.globo.com/site/combate/noticia/2015/11/ex-agente-celebra-derrota-de-ronda-rousey-e-detona-criei-um-monstro.html">Ex-agente se sente vingado: &#39;Criei um monstro&#39;</a></div></li></ul></div></div></div><div class="grid-base narrow ultimo hide-mobile"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://extra.globo.com/noticias/mundo/espanhol-dado-como-morto-no-bataclan-esta-vivo-soube-de-confusao-pelo-facebook-18062865.html#ixzz3rfolNi70" class="foto " title="Apontado como
morto posta no Face: &#39;Estou vivo&#39; (ReproduÃ§Ã£o do Facebook)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/OGdTA-AAWrGUqrQLXJYanCRI_7M=/filters:quality(10):strip_icc()/s2.glbimg.com/Mz-Eo1gqE7UGpGC8XWcpWW46SgA=/72x48:515x351/155x106/s.glbimg.com/en/ho/f/original/2015/11/16/espanhol-alberto-pardo.jpg" alt="Apontado como
morto posta no Face: &#39;Estou vivo&#39; (ReproduÃ§Ã£o do Facebook)" title="Apontado como
morto posta no Face: &#39;Estou vivo&#39; (ReproduÃ§Ã£o do Facebook)"
         data-original-image="s2.glbimg.com/Mz-Eo1gqE7UGpGC8XWcpWW46SgA=/72x48:515x351/155x106/s.glbimg.com/en/ho/f/original/2015/11/16/espanhol-alberto-pardo.jpg" data-url-smart_horizontal="xCnWjZ5p60IBdRzY2UWh8TP9ZGk=/90x56/smart/filters:strip_icc()/" data-url-smart="xCnWjZ5p60IBdRzY2UWh8TP9ZGk=/90x56/smart/filters:strip_icc()/" data-url-feature="xCnWjZ5p60IBdRzY2UWh8TP9ZGk=/90x56/smart/filters:strip_icc()/" data-url-tablet="tSUmirytW4KRCfnhaUeRpPN1ryk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="yZ8In-E5pKm91pvMtLpAu128Gbw=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Apontado como<br />morto posta no Face: &#39;Estou vivo&#39;</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="AÃ§Ã£o nÃ£o acha suspeito" href="http://g1.globo.com/mundo/noticia/2015/11/operacao-em-bruxelas-termina-sem-localizar-suspeito-de-ataques-em-paris1.html">AÃ§Ã£o nÃ£o acha suspeito</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/globo-news/estudio-i/videos/t/todos-os-videos/v/imagens-mostram-membro-do-estado-islamico-arrastando-corpo/4612145/" class="foto " title="Suspeito circula
levando corpos em vÃ­deo antigo (ReproduÃ§Ã£o / Globo News)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/wqfuCKsgVA_AynXWPa3IXgcKCoc=/filters:quality(10):strip_icc()/s2.glbimg.com/vPvBeI4HYEBapDFKNSNXfLaLSls=/45x57:344x262/155x106/s.glbimg.com/en/ho/f/original/2015/11/16/mentor.jpg" alt="Suspeito circula
levando corpos em vÃ­deo antigo (ReproduÃ§Ã£o / Globo News)" title="Suspeito circula
levando corpos em vÃ­deo antigo (ReproduÃ§Ã£o / Globo News)"
         data-original-image="s2.glbimg.com/vPvBeI4HYEBapDFKNSNXfLaLSls=/45x57:344x262/155x106/s.glbimg.com/en/ho/f/original/2015/11/16/mentor.jpg" data-url-smart_horizontal="1VxcP6YHWc5Nzr6hXUip2twjQRM=/90x56/smart/filters:strip_icc()/" data-url-smart="1VxcP6YHWc5Nzr6hXUip2twjQRM=/90x56/smart/filters:strip_icc()/" data-url-feature="1VxcP6YHWc5Nzr6hXUip2twjQRM=/90x56/smart/filters:strip_icc()/" data-url-tablet="EYFZV1uqiWF03Xhe78ygTBRYjow=/160xorig/smart/filters:strip_icc()/" data-url-desktop="WwtuIQtsKga6A_e_iqK-tMr1lJE=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Suspeito circula<br />levando corpos em vÃ­deo antigo</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Hackers ameaÃ§am EI" href="http://extra.globo.com/noticias/mundo/hackers-do-grupo-anonymous-ameacam-estado-islamico-18060115.html#ixzz3rebAczXr">Hackers ameaÃ§am EI</a></div></li></ul></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/11/universidade-de-harvard-nos-eua-esvazia-edificios.html" class=" " title="Universidade Harvard esvazia edifÃ­cios por ameaÃ§a de bomba"><div class="conteudo"><h2>Universidade Harvard esvazia edifÃ­cios por ameaÃ§a de bomba</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/11/dante-da-beijo-em-lara-e-pede-deixa-eu-cuidar-de-voce.html" class="foto " title="&#39;Regra do Jogo&#39;: Dante beija Lara (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/KUgMxEfRoDPUZSEKeTeZNzas2w0=/filters:quality(10):strip_icc()/s2.glbimg.com/3_T9ymyYF_-AJr4A_2NZjEB_-LY=/0x236:690x503/155x60/s.glbimg.com/et/gs/f/original/2015/11/13/beijo-de-lara-e-dantwe.jpg" alt="&#39;Regra do Jogo&#39;: Dante beija Lara (TV Globo)" title="&#39;Regra do Jogo&#39;: Dante beija Lara (TV Globo)"
         data-original-image="s2.glbimg.com/3_T9ymyYF_-AJr4A_2NZjEB_-LY=/0x236:690x503/155x60/s.glbimg.com/et/gs/f/original/2015/11/13/beijo-de-lara-e-dantwe.jpg" data-url-smart_horizontal="pdJ4x97tAagqkRxWkGVQfYmpC74=/90x56/smart/filters:strip_icc()/" data-url-smart="pdJ4x97tAagqkRxWkGVQfYmpC74=/90x56/smart/filters:strip_icc()/" data-url-feature="pdJ4x97tAagqkRxWkGVQfYmpC74=/90x56/smart/filters:strip_icc()/" data-url-tablet="TsXbKL8e6S2S0_uOFyj9iIY--Mg=/160xorig/smart/filters:strip_icc()/" data-url-desktop="HgYmAtmMA1WIzISDFXZ38cNYKsg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra do Jogo&#39;: Dante beija Lara</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="TÃ³ia diz sim a Romero" href="http://extra.globo.com/tv-e-lazer/telinha/a-regra-do-jogo-toia-aceita-namorar-romero-18045611.html">TÃ³ia diz sim a Romero</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/11/louco-de-ciume-pedro-arma-para-descobrir-se-livia-tem-outro.html" class="foto " title="&#39;AlÃ©m&#39;: Pedro arma por ciÃºme (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/jmuqUCmb5nuEiN6rMKO2UXW7suQ=/filters:quality(10):strip_icc()/s2.glbimg.com/bbTPLHmUrIaZp9kK12xQTnYtncU=/0x144:690x411/155x60/s.glbimg.com/et/gs/f/original/2015/11/16/emilio-dantas.jpg" alt="&#39;AlÃ©m&#39;: Pedro arma por ciÃºme (TV Globo)" title="&#39;AlÃ©m&#39;: Pedro arma por ciÃºme (TV Globo)"
         data-original-image="s2.glbimg.com/bbTPLHmUrIaZp9kK12xQTnYtncU=/0x144:690x411/155x60/s.glbimg.com/et/gs/f/original/2015/11/16/emilio-dantas.jpg" data-url-smart_horizontal="cJ6KPzAIDKtQMa-XrDY0nKtpxu4=/90x56/smart/filters:strip_icc()/" data-url-smart="cJ6KPzAIDKtQMa-XrDY0nKtpxu4=/90x56/smart/filters:strip_icc()/" data-url-feature="cJ6KPzAIDKtQMa-XrDY0nKtpxu4=/90x56/smart/filters:strip_icc()/" data-url-tablet="8dUC6E9UO3gzmHpMFV_3dEV4XFU=/160xorig/smart/filters:strip_icc()/" data-url-desktop="2OGguxtvQjyHAb-_WgRkrrrMHzg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;AlÃ©m&#39;: Pedro arma por ciÃºme</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Melissa estÃ¡ gravida" href="http://extra.globo.com/tv-e-lazer/telinha/alem-do-tempo-melissa-descobre-que-esta-gravida-felipe-sai-de-casa-18043889.html">Melissa estÃ¡ gravida</a></div></li></ul></div></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://sportv.globo.com/site/programas/Extra-ordinarios/noticia/2015/11/maite-diz-que-ira-jogo-do-bota-e-faz-misterio-sobre-nudez-sendo-armado.html" class=" " title="MaitÃª sobre nu: &#39;Sendo armado&#39;"><div class="conteudo"><h2>MaitÃª sobre nu: &#39;Sendo armado&#39;</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Volta de Jefferson Ã© &#39;lado positivo&#39; do vexame em casa" href="http://globoesporte.globo.com/futebol/times/botafogo/noticia/2015/11/fora-da-festa-do-acesso-jefferson-pode-erguer-taca-em-brasilia.html">Volta de Jefferson Ã© &#39;lado positivo&#39; do vexame em casa</a></div></li></ul></div></div></div><div class="grid-base wide analytics-area analytics-id-A mobile-grid-base"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/11/estado-islamico-ameaca-eua-e-outros-paises-de-ataques-terroristas.html" class="foto " title="Terroristas ameaÃ§am EUA e outros paÃ­ses (GloboNews)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/DgFH4HzYxJnXfsko53Ya23ePL5s=/filters:quality(10):strip_icc()/s2.glbimg.com/0-AnCHw51VaG5PypvVIP7ngyqfo=/70x131:1741x679/335x110/s.glbimg.com/jo/g1/f/original/2015/11/16/ei.jpg" alt="Terroristas ameaÃ§am EUA e outros paÃ­ses (GloboNews)" title="Terroristas ameaÃ§am EUA e outros paÃ­ses (GloboNews)"
         data-original-image="s2.glbimg.com/0-AnCHw51VaG5PypvVIP7ngyqfo=/70x131:1741x679/335x110/s.glbimg.com/jo/g1/f/original/2015/11/16/ei.jpg" data-url-smart_horizontal="VcgitXDPTUi1BKKxK-NIys7P1GM=/400xorig/smart/filters:strip_icc()/" data-url-smart="VcgitXDPTUi1BKKxK-NIys7P1GM=/400xorig/smart/filters:strip_icc()/" data-url-feature="VcgitXDPTUi1BKKxK-NIys7P1GM=/400xorig/smart/filters:strip_icc()/" data-url-tablet="iJIEQG-eJOG_nLTWMLlQV_aAtaA=/340xorig/smart/filters:strip_icc()/" data-url-desktop="3gOR7cRrKoR8Xe0lYApx_0F-I0g=/335xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Terroristas ameaÃ§am EUA e outros paÃ­ses</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="CoalizÃ£o liderada pelos EUA destrÃ³i 116 caminhÃµes" href="http://g1.globo.com/mundo/ao-vivo/2015/explosoes-e-tiroteio-em-paris.html#/glb-feed-post/5649bee2c22d62235dea88f7">CoalizÃ£o liderada pelos EUA destrÃ³i 116 caminhÃµes</a></div></li></ul></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/ao-vivo/2015/explosoes-e-tiroteio-em-paris.html" class=" " title="Presidente promete intensificar ataques: &#39;A FranÃ§a estÃ¡ em guerra&#39;"><div class="conteudo"><h2>Presidente promete intensificar ataques: &#39;A FranÃ§a estÃ¡ em guerra&#39;</h2></div></a></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/economia/2015/11/16/2270-na-turquia-dilma-diz-que-levy-fica-onde-esta" class="foto " title="Dilma diz que Joaquim Levy 
&#39;fica onde estÃ¡&#39; (Volkan Furuncu / AFP ANTÃLIA)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/j4jICejdsksaUFOdcb-P_kL0E3U=/filters:quality(10):strip_icc()/s2.glbimg.com/Nikh5nACEHNZxf110bAHRdsPWSM=/216x237:447x341/155x70/s.glbimg.com/en/ho/f/original/2015/11/16/dilma_turquia.jpg" alt="Dilma diz que Joaquim Levy 
&#39;fica onde estÃ¡&#39; (Volkan Furuncu / AFP ANTÃLIA)" title="Dilma diz que Joaquim Levy 
&#39;fica onde estÃ¡&#39; (Volkan Furuncu / AFP ANTÃLIA)"
         data-original-image="s2.glbimg.com/Nikh5nACEHNZxf110bAHRdsPWSM=/216x237:447x341/155x70/s.glbimg.com/en/ho/f/original/2015/11/16/dilma_turquia.jpg" data-url-smart_horizontal="7veCGOCaWcUIyOEaRs6gglZ527w=/90x56/smart/filters:strip_icc()/" data-url-smart="7veCGOCaWcUIyOEaRs6gglZ527w=/90x56/smart/filters:strip_icc()/" data-url-feature="7veCGOCaWcUIyOEaRs6gglZ527w=/90x56/smart/filters:strip_icc()/" data-url-tablet="2ORHjMdqxNncYqWJvV-GN7Uf3ng=/160xorig/smart/filters:strip_icc()/" data-url-desktop="AFzhEUeBMAXH4CYP46daFcm2WMw=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Dilma diz que Joaquim Levy <br />&#39;fica onde estÃ¡&#39;</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/11/pinato-entrega-ao-conselho-de-etica-relatorio-preliminar-do-caso-cunha.html" class=" " title="Relator pede continuidade de aÃ§Ã£o contra Cunha no Conselho de Ãtica"><div class="conteudo"><h2>Relator pede continuidade de aÃ§Ã£o contra Cunha no Conselho de Ãtica</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://extra.globo.com/noticias/mundo/espanhol-dado-como-morto-no-bataclan-esta-vivo-soube-de-confusao-pelo-facebook-18062865.html#ixzz3rfolNi70" class="foto " title="Apontado como
morto posta no Face: &#39;Estou vivo&#39; (ReproduÃ§Ã£o do Facebook)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/OGdTA-AAWrGUqrQLXJYanCRI_7M=/filters:quality(10):strip_icc()/s2.glbimg.com/Mz-Eo1gqE7UGpGC8XWcpWW46SgA=/72x48:515x351/155x106/s.glbimg.com/en/ho/f/original/2015/11/16/espanhol-alberto-pardo.jpg" alt="Apontado como
morto posta no Face: &#39;Estou vivo&#39; (ReproduÃ§Ã£o do Facebook)" title="Apontado como
morto posta no Face: &#39;Estou vivo&#39; (ReproduÃ§Ã£o do Facebook)"
         data-original-image="s2.glbimg.com/Mz-Eo1gqE7UGpGC8XWcpWW46SgA=/72x48:515x351/155x106/s.glbimg.com/en/ho/f/original/2015/11/16/espanhol-alberto-pardo.jpg" data-url-smart_horizontal="xCnWjZ5p60IBdRzY2UWh8TP9ZGk=/90x56/smart/filters:strip_icc()/" data-url-smart="xCnWjZ5p60IBdRzY2UWh8TP9ZGk=/90x56/smart/filters:strip_icc()/" data-url-feature="xCnWjZ5p60IBdRzY2UWh8TP9ZGk=/90x56/smart/filters:strip_icc()/" data-url-tablet="tSUmirytW4KRCfnhaUeRpPN1ryk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="yZ8In-E5pKm91pvMtLpAu128Gbw=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Apontado como<br />morto posta no Face: &#39;Estou vivo&#39;</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="AÃ§Ã£o nÃ£o acha suspeito" href="http://g1.globo.com/mundo/noticia/2015/11/operacao-em-bruxelas-termina-sem-localizar-suspeito-de-ataques-em-paris1.html">AÃ§Ã£o nÃ£o acha suspeito</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/globo-news/estudio-i/videos/t/todos-os-videos/v/imagens-mostram-membro-do-estado-islamico-arrastando-corpo/4612145/" class="foto " title="Suspeito circula
levando corpos em vÃ­deo antigo (ReproduÃ§Ã£o / Globo News)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/wqfuCKsgVA_AynXWPa3IXgcKCoc=/filters:quality(10):strip_icc()/s2.glbimg.com/vPvBeI4HYEBapDFKNSNXfLaLSls=/45x57:344x262/155x106/s.glbimg.com/en/ho/f/original/2015/11/16/mentor.jpg" alt="Suspeito circula
levando corpos em vÃ­deo antigo (ReproduÃ§Ã£o / Globo News)" title="Suspeito circula
levando corpos em vÃ­deo antigo (ReproduÃ§Ã£o / Globo News)"
         data-original-image="s2.glbimg.com/vPvBeI4HYEBapDFKNSNXfLaLSls=/45x57:344x262/155x106/s.glbimg.com/en/ho/f/original/2015/11/16/mentor.jpg" data-url-smart_horizontal="1VxcP6YHWc5Nzr6hXUip2twjQRM=/90x56/smart/filters:strip_icc()/" data-url-smart="1VxcP6YHWc5Nzr6hXUip2twjQRM=/90x56/smart/filters:strip_icc()/" data-url-feature="1VxcP6YHWc5Nzr6hXUip2twjQRM=/90x56/smart/filters:strip_icc()/" data-url-tablet="EYFZV1uqiWF03Xhe78ygTBRYjow=/160xorig/smart/filters:strip_icc()/" data-url-desktop="WwtuIQtsKga6A_e_iqK-tMr1lJE=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Suspeito circula<br />levando corpos em vÃ­deo antigo</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Hackers ameaÃ§am EI" href="http://extra.globo.com/noticias/mundo/hackers-do-grupo-anonymous-ameacam-estado-islamico-18060115.html#ixzz3rebAczXr">Hackers ameaÃ§am EI</a></div></li></ul></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/11/universidade-de-harvard-nos-eua-esvazia-edificios.html" class=" " title="Universidade Harvard esvazia edifÃ­cios por ameaÃ§a de bomba"><div class="conteudo"><h2>Universidade Harvard esvazia edifÃ­cios por ameaÃ§a de bomba</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://sportv.globo.com/site/combate/noticia/2015/11/cris-cyborg-ja-sabia-que-ronda-nao-aguenta-soco-de-verdade-na-cara.html" class=" " title="Cyborg: &#39;Ronda nÃ£o aguenta soco&#39; (globoesporte.com)"><div class="conteudo"><h2>Cyborg: &#39;Ronda nÃ£o aguenta soco&#39;</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Ex-agente se sente vingado: &#39;Criei um monstro&#39;" href="http://sportv.globo.com/site/combate/noticia/2015/11/ex-agente-celebra-derrota-de-ronda-rousey-e-detona-criei-um-monstro.html">Ex-agente se sente vingado: &#39;Criei um monstro&#39;</a></div></li></ul></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://sportv.globo.com/site/programas/Extra-ordinarios/noticia/2015/11/maite-diz-que-ira-jogo-do-bota-e-faz-misterio-sobre-nudez-sendo-armado.html" class=" " title="MaitÃª sobre nu: &#39;Sendo armado&#39;"><div class="conteudo"><h2>MaitÃª sobre nu: &#39;Sendo armado&#39;</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Volta de Jefferson Ã© &#39;lado positivo&#39; do vexame em casa" href="http://globoesporte.globo.com/futebol/times/botafogo/noticia/2015/11/fora-da-festa-do-acesso-jefferson-pode-erguer-taca-em-brasilia.html">Volta de Jefferson Ã© &#39;lado positivo&#39; do vexame em casa</a></div></li></ul></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/11/dante-da-beijo-em-lara-e-pede-deixa-eu-cuidar-de-voce.html" class="foto " title="&#39;Regra do Jogo&#39;: Dante beija Lara (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/KUgMxEfRoDPUZSEKeTeZNzas2w0=/filters:quality(10):strip_icc()/s2.glbimg.com/3_T9ymyYF_-AJr4A_2NZjEB_-LY=/0x236:690x503/155x60/s.glbimg.com/et/gs/f/original/2015/11/13/beijo-de-lara-e-dantwe.jpg" alt="&#39;Regra do Jogo&#39;: Dante beija Lara (TV Globo)" title="&#39;Regra do Jogo&#39;: Dante beija Lara (TV Globo)"
         data-original-image="s2.glbimg.com/3_T9ymyYF_-AJr4A_2NZjEB_-LY=/0x236:690x503/155x60/s.glbimg.com/et/gs/f/original/2015/11/13/beijo-de-lara-e-dantwe.jpg" data-url-smart_horizontal="pdJ4x97tAagqkRxWkGVQfYmpC74=/90x56/smart/filters:strip_icc()/" data-url-smart="pdJ4x97tAagqkRxWkGVQfYmpC74=/90x56/smart/filters:strip_icc()/" data-url-feature="pdJ4x97tAagqkRxWkGVQfYmpC74=/90x56/smart/filters:strip_icc()/" data-url-tablet="TsXbKL8e6S2S0_uOFyj9iIY--Mg=/160xorig/smart/filters:strip_icc()/" data-url-desktop="HgYmAtmMA1WIzISDFXZ38cNYKsg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra do Jogo&#39;: Dante beija Lara</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="TÃ³ia diz sim a Romero" href="http://extra.globo.com/tv-e-lazer/telinha/a-regra-do-jogo-toia-aceita-namorar-romero-18045611.html">TÃ³ia diz sim a Romero</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/11/louco-de-ciume-pedro-arma-para-descobrir-se-livia-tem-outro.html" class="foto " title="&#39;AlÃ©m&#39;: Pedro arma por ciÃºme (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/jmuqUCmb5nuEiN6rMKO2UXW7suQ=/filters:quality(10):strip_icc()/s2.glbimg.com/bbTPLHmUrIaZp9kK12xQTnYtncU=/0x144:690x411/155x60/s.glbimg.com/et/gs/f/original/2015/11/16/emilio-dantas.jpg" alt="&#39;AlÃ©m&#39;: Pedro arma por ciÃºme (TV Globo)" title="&#39;AlÃ©m&#39;: Pedro arma por ciÃºme (TV Globo)"
         data-original-image="s2.glbimg.com/bbTPLHmUrIaZp9kK12xQTnYtncU=/0x144:690x411/155x60/s.glbimg.com/et/gs/f/original/2015/11/16/emilio-dantas.jpg" data-url-smart_horizontal="cJ6KPzAIDKtQMa-XrDY0nKtpxu4=/90x56/smart/filters:strip_icc()/" data-url-smart="cJ6KPzAIDKtQMa-XrDY0nKtpxu4=/90x56/smart/filters:strip_icc()/" data-url-feature="cJ6KPzAIDKtQMa-XrDY0nKtpxu4=/90x56/smart/filters:strip_icc()/" data-url-tablet="8dUC6E9UO3gzmHpMFV_3dEV4XFU=/160xorig/smart/filters:strip_icc()/" data-url-desktop="2OGguxtvQjyHAb-_WgRkrrrMHzg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;AlÃ©m&#39;: Pedro arma por ciÃºme</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Melissa estÃ¡ gravida" href="http://extra.globo.com/tv-e-lazer/telinha/alem-do-tempo-melissa-descobre-que-esta-gravida-felipe-sai-de-casa-18043889.html">Melissa estÃ¡ gravida</a></div></li></ul></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globoplay.globo.com/jornal-hoje/p/818/">Jornal hoje</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-noticia" data-href="http://globoplay.globo.com/v/4611809/"><a href="http://globoplay.globo.com/v/4611809/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4611809.jpg" alt="&#39;Corri sem parar atÃ© ficar longe&#39;, diz brasileiro ferido em atentados" /><div class="time">03:21</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">ferido na cabeÃ§a</span><span class="title">&#39;Corri sem parar atÃ© ficar longe&#39;, diz brasileiro ferido em atentados</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-noticia" data-href="http://globoplay.globo.com/v/4611854/"><a href="http://globoplay.globo.com/v/4611854/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4611854.jpg" alt="Suposto mentor dos ataques terroristas em Paris estaria na SÃ­ria" /><div class="time">05:00</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">InvestigaÃ§Ãµes avanÃ§am</span><span class="title">Suposto mentor dos ataques terroristas em Paris estaria na SÃ­ria</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-noticia" data-href="http://globoplay.globo.com/v/4611953/"><a href="http://globoplay.globo.com/v/4611953/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4611953.jpg" alt="Atentados foram planejados na SÃ­ria e organizados na BÃ©lgica" /><div class="time">01:01</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">Presidente da FranÃ§a informa</span><span class="title">Atentados foram planejados na SÃ­ria e organizados na BÃ©lgica</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-noticia"><a href="http://globoplay.globo.com/v/4611809/"><span class="subtitle">ferido na cabeÃ§a</span><span class="title">&#39;Corri sem parar atÃ© ficar longe&#39;, diz brasileiro ferido em atentados</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-noticia"><a href="http://globoplay.globo.com/v/4611854/"><span class="subtitle">InvestigaÃ§Ãµes avanÃ§am</span><span class="title">Suposto mentor dos ataques terroristas em Paris estaria na SÃ­ria</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-noticia"><a href="http://globoplay.globo.com/v/4611953/"><span class="subtitle">Presidente da FranÃ§a informa</span><span class="title">Atentados foram planejados na SÃ­ria e organizados na BÃ©lgica</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-noticia"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globoplay.globo.com/v/4611809/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globoplay.globo.com/v/4611854/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globoplay.globo.com/v/4611953/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globoplay.globo.com/">mais vÃ­deos <span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/noticias/mundo/pela-primeira-vez-boeing-737-pilotado-apenas-por-mulheres-18062326.html#ixzz3rfUbmVyU" class="foto" title="Empresa tem 2 mulheres no comando de Boeing 737 pela 1Âª vez na histÃ³ria (reproduÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/NMdQ_wfXZ0-Mv35E-GzBLNbTYfU=/filters:quality(10):strip_icc()/s2.glbimg.com/94gvPPRPSDjxuCM7bs4CEw3It7Q=/0x16:640x360/335x180/s.glbimg.com/en/ho/f/original/2015/11/16/chipo.jpg" alt="Empresa tem 2 mulheres no comando de Boeing 737 pela 1Âª vez na histÃ³ria (reproduÃ§Ã£o)" title="Empresa tem 2 mulheres no comando de Boeing 737 pela 1Âª vez na histÃ³ria (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/94gvPPRPSDjxuCM7bs4CEw3It7Q=/0x16:640x360/335x180/s.glbimg.com/en/ho/f/original/2015/11/16/chipo.jpg" data-url-smart_horizontal="buqzd4m2SKWIs68BYTyS7oSylLw=/90x0/smart/filters:strip_icc()/" data-url-smart="buqzd4m2SKWIs68BYTyS7oSylLw=/90x0/smart/filters:strip_icc()/" data-url-feature="buqzd4m2SKWIs68BYTyS7oSylLw=/90x0/smart/filters:strip_icc()/" data-url-tablet="uCl_XfOy13PxQsLMWtx-Wlrj57w=/220x125/smart/filters:strip_icc()/" data-url-desktop="3bCzTtrp6johjvI4lxSqaD6dtHU=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Empresa tem 2 mulheres no comando de Boeing 737 pela 1Âª vez na histÃ³ria</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 15:33:41" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/santos-regiao/noticia/2015/11/suspeito-e-preso-em-sp-apos-anunciar-trabalho-na-web-e-estuprar-candidata.html" class="foto" title="Suspeito Ã© preso em SP por estuprar mulheres atraÃ­das por vagas postadas na web (G1)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Vtljq5F9u75hvrsq4jwnhPnOwFc=/filters:quality(10):strip_icc()/s2.glbimg.com/KEBVmTmGATUc874SFNknsdpuzG4=/0x2:300x201/120x80/s.glbimg.com/jo/g1/f/original/2015/11/16/20151116061603.jpg" alt="Suspeito Ã© preso em SP por estuprar mulheres atraÃ­das por vagas postadas na web (G1)" title="Suspeito Ã© preso em SP por estuprar mulheres atraÃ­das por vagas postadas na web (G1)"
                data-original-image="s2.glbimg.com/KEBVmTmGATUc874SFNknsdpuzG4=/0x2:300x201/120x80/s.glbimg.com/jo/g1/f/original/2015/11/16/20151116061603.jpg" data-url-smart_horizontal="MGITgM7YOyeXRwoxb3xwglFeBCo=/90x0/smart/filters:strip_icc()/" data-url-smart="MGITgM7YOyeXRwoxb3xwglFeBCo=/90x0/smart/filters:strip_icc()/" data-url-feature="MGITgM7YOyeXRwoxb3xwglFeBCo=/90x0/smart/filters:strip_icc()/" data-url-tablet="9z1VSI-6ep_Tl1VFsnTftzaU3_g=/70x50/smart/filters:strip_icc()/" data-url-desktop="3KOMdxV_NOq7wl7smuTnyizxnIk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Suspeito Ã© preso em SP por estuprar mulheres atraÃ­das por vagas postadas na web</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 13:56:33" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/noticias/economia/caixa-indenizara-cliente-que-pensou-ter-ganhado-premio-de-100-mil-na-loteria-18060911.html" class="foto" title="Caixa indenizarÃ¡ cliente que pensou ter ganhado R$ 100 mil na loteria (ReproduÃ§Ã£o/TV Globo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/_V9Xm5URMvsDjNgPN0FW0fMwEAM=/filters:quality(10):strip_icc()/s2.glbimg.com/YBlB3EBX8-IKwkQQPvIGrn7dg2o=/120x0:300x120/120x80/s.glbimg.com/jo/g1/f/original/2015/03/04/mega-sena-300-120.jpg" alt="Caixa indenizarÃ¡ cliente que pensou ter ganhado R$ 100 mil na loteria (ReproduÃ§Ã£o/TV Globo)" title="Caixa indenizarÃ¡ cliente que pensou ter ganhado R$ 100 mil na loteria (ReproduÃ§Ã£o/TV Globo)"
                data-original-image="s2.glbimg.com/YBlB3EBX8-IKwkQQPvIGrn7dg2o=/120x0:300x120/120x80/s.glbimg.com/jo/g1/f/original/2015/03/04/mega-sena-300-120.jpg" data-url-smart_horizontal="HylmzlHngNIwKD7cfD7nzM7Fq8E=/90x0/smart/filters:strip_icc()/" data-url-smart="HylmzlHngNIwKD7cfD7nzM7Fq8E=/90x0/smart/filters:strip_icc()/" data-url-feature="HylmzlHngNIwKD7cfD7nzM7Fq8E=/90x0/smart/filters:strip_icc()/" data-url-tablet="d4XGRzScF-nQPNVjJa2Xa6T5PIo=/70x50/smart/filters:strip_icc()/" data-url-desktop="YNOBqa5-0Na90VkaGA_aTmU4YqI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Caixa indenizarÃ¡ cliente que pensou ter ganhado<br /> R$ 100 mil na loteria</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 15:25:30" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/noticia/2015/11/mae-de-menino-morto-por-escorpiao-tem-alta-em-hospital-de-rio-preto.html" class="" title="MÃ£e que tomou veneno ao saber da morte de filho por escorpiÃ£o tem alta; marido morreu (Pedro Danthas/Volkswagen)" rel="bookmark"><span class="conteudo"><p>MÃ£e que tomou veneno ao saber da morte de filho por escorpiÃ£o tem alta; marido morreu</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 16:32:48" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/noticias/mundo/bombeiro-passa-pelo-maior-transplante-de-rosto-ja-realizado-18063125.html" class="foto" title="Bombeiro passa pelo mais extenso transplante de rosto jÃ¡ realizado; fotos (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/eoycRPrOmCxWZW8apyLhziyNQOI=/filters:quality(10):strip_icc()/s2.glbimg.com/NKzwpag4vjewkvUILuYD0dAry_o=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/11/16/bombeiro.jpg" alt="Bombeiro passa pelo mais extenso transplante de rosto jÃ¡ realizado; fotos (ReproduÃ§Ã£o)" title="Bombeiro passa pelo mais extenso transplante de rosto jÃ¡ realizado; fotos (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/NKzwpag4vjewkvUILuYD0dAry_o=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/11/16/bombeiro.jpg" data-url-smart_horizontal="bNbSfEbwCWQJuhTYvb6kuVaAlbg=/90x0/smart/filters:strip_icc()/" data-url-smart="bNbSfEbwCWQJuhTYvb6kuVaAlbg=/90x0/smart/filters:strip_icc()/" data-url-feature="bNbSfEbwCWQJuhTYvb6kuVaAlbg=/90x0/smart/filters:strip_icc()/" data-url-tablet="B6f_w4r4BofBlBdJBfT0vozFgPk=/70x50/smart/filters:strip_icc()/" data-url-desktop="niA-OGUY8ndrghmz6avGvTnGAes=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Bombeiro passa pelo mais extenso transplante de rosto jÃ¡ realizado; fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 10:21:37" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/11/atualizacao-do-whatsapp-no-iphone-permite-burlar-check-azul-com-3d-touch.html" class="foto" title="AtualizaÃ§Ã£o do WhatsApp
permite &#39;burlar&#39; check azul
no iPhone; saiba usar (Anna Kellen Bull/TechTudo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/gCgpThklhvRsA9bp3UKpzggQvi8=/filters:quality(10):strip_icc()/s2.glbimg.com/M2jYavaPicqVGzBWEa1vL1RQ26s=/264x243:1789x1259/120x80/s.glbimg.com/po/tt2/f/original/2015/11/16/whatsapp-iphone-6s_1.jpg" alt="AtualizaÃ§Ã£o do WhatsApp
permite &#39;burlar&#39; check azul
no iPhone; saiba usar (Anna Kellen Bull/TechTudo)" title="AtualizaÃ§Ã£o do WhatsApp
permite &#39;burlar&#39; check azul
no iPhone; saiba usar (Anna Kellen Bull/TechTudo)"
                data-original-image="s2.glbimg.com/M2jYavaPicqVGzBWEa1vL1RQ26s=/264x243:1789x1259/120x80/s.glbimg.com/po/tt2/f/original/2015/11/16/whatsapp-iphone-6s_1.jpg" data-url-smart_horizontal="9SIHiJNukPnQj5mj1NZe3yi1UGk=/90x0/smart/filters:strip_icc()/" data-url-smart="9SIHiJNukPnQj5mj1NZe3yi1UGk=/90x0/smart/filters:strip_icc()/" data-url-feature="9SIHiJNukPnQj5mj1NZe3yi1UGk=/90x0/smart/filters:strip_icc()/" data-url-tablet="sJJ60HXr_O7HIgPCqyjMdEk1MWU=/70x50/smart/filters:strip_icc()/" data-url-desktop="N4syyOI5y1h-IyaElw_Cg4VHbrU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>AtualizaÃ§Ã£o do WhatsApp<br />permite &#39;burlar&#39; check azul<br />no iPhone; saiba usar</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/am/amazonas/noticia/2015/11/tenente-da-marinha-ferido-em-treino-na-selva-sai-do-coma-apos-43-dias.html" class="" title="Tenente sai do coma apÃ³s 43 dias ferido em treino na selva; &#39;Ele abriu os olhos&#39;, diz o pai (ReproduÃ§Ã£o/Facebook)" rel="bookmark"><span class="conteudo"><p>Tenente sai do coma apÃ³s 43 dias ferido em treino na selva; &#39;Ele abriu os olhos&#39;, diz o pai</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 14:51:26" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pa/para/noticia/2015/11/imagens-mostram-morte-de-mulheres-apos-tentativa-de-fuga-no-pa.html" class="foto" title="Imagens mostram morte de japonesas em rodovia do ParÃ¡ durante tiroteio (ReproduÃ§Ã£o / TV Liberal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/gxtzk8PB8w1ZeciiW_ehH6D6CnQ=/filters:quality(10):strip_icc()/s2.glbimg.com/41X6fDYJcBVpxOmwaFyJtNM-n0E=/194x97:489x294/120x80/s.glbimg.com/jo/g1/f/original/2015/11/16/jl1_1611_mulheres.jpg" alt="Imagens mostram morte de japonesas em rodovia do ParÃ¡ durante tiroteio (ReproduÃ§Ã£o / TV Liberal)" title="Imagens mostram morte de japonesas em rodovia do ParÃ¡ durante tiroteio (ReproduÃ§Ã£o / TV Liberal)"
                data-original-image="s2.glbimg.com/41X6fDYJcBVpxOmwaFyJtNM-n0E=/194x97:489x294/120x80/s.glbimg.com/jo/g1/f/original/2015/11/16/jl1_1611_mulheres.jpg" data-url-smart_horizontal="_8_eUxHJzIsaYgprUygZEHMMyPU=/90x0/smart/filters:strip_icc()/" data-url-smart="_8_eUxHJzIsaYgprUygZEHMMyPU=/90x0/smart/filters:strip_icc()/" data-url-feature="_8_eUxHJzIsaYgprUygZEHMMyPU=/90x0/smart/filters:strip_icc()/" data-url-tablet="_13uq7h-VILtJBnIZj6aKHgd4Fg=/70x50/smart/filters:strip_icc()/" data-url-desktop="TvaaOCuL-4wV36fBY14q7EjNtjI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Imagens mostram morte de japonesas em rodovia do ParÃ¡ durante tiroteio</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 15:40:49" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/goias/noticia/2015/11/pai-e-suspeito-de-espancar-os-4-filhos-e-planejar-envenenar-familia-em-go.html" class="foto" title="Em GO, laudo mostra que 4 irmÃ£os de 4 a 10 anos apanharam atÃ© de martelo (ReproduÃ§Ã£o/TV Anhanguera)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/3A3lH-XLV4quJhKwE4AuyHC8hh4=/filters:quality(10):strip_icc()/s2.glbimg.com/aR-MIYc8I0Zw4NVhNQMs-v66O-A=/115x27:563x325/120x80/s.glbimg.com/jo/g1/f/original/2015/11/16/criancas.jpg" alt="Em GO, laudo mostra que 4 irmÃ£os de 4 a 10 anos apanharam atÃ© de martelo (ReproduÃ§Ã£o/TV Anhanguera)" title="Em GO, laudo mostra que 4 irmÃ£os de 4 a 10 anos apanharam atÃ© de martelo (ReproduÃ§Ã£o/TV Anhanguera)"
                data-original-image="s2.glbimg.com/aR-MIYc8I0Zw4NVhNQMs-v66O-A=/115x27:563x325/120x80/s.glbimg.com/jo/g1/f/original/2015/11/16/criancas.jpg" data-url-smart_horizontal="7gRPynjGB-OieLndt8iyfNy1kgI=/90x0/smart/filters:strip_icc()/" data-url-smart="7gRPynjGB-OieLndt8iyfNy1kgI=/90x0/smart/filters:strip_icc()/" data-url-feature="7gRPynjGB-OieLndt8iyfNy1kgI=/90x0/smart/filters:strip_icc()/" data-url-tablet="KX2Eq-GvsUpo7PDryEEPKwFZHGc=/70x50/smart/filters:strip_icc()/" data-url-desktop="ajVlTgGyWkslTO0dX-CTKq79XMM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Em GO, laudo mostra que 4 irmÃ£os de 4 a 10 anos apanharam atÃ© de martelo</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/noticia/2015/11/empresario-de-holm-revela-aposta-em-nocaute-e-diz-que-faturou-seis-digitos.html" class="foto" title="Agente de Holm agradece Vegas e diz ter faturado seis dÃ­gitos com &#39;azarÃ£o&#39; (Getty Images)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/Wous8XXE800RgTvg1u5GKUkDF8k=/filters:quality(10):strip_icc()/s2.glbimg.com/wncm_nffwxI7fTmy69X9Z35Rqw4=/293x444:2154x1444/335x180/s.glbimg.com/es/ge/f/original/2015/11/15/gettyimages-497206248.jpg" alt="Agente de Holm agradece Vegas e diz ter faturado seis dÃ­gitos com &#39;azarÃ£o&#39; (Getty Images)" title="Agente de Holm agradece Vegas e diz ter faturado seis dÃ­gitos com &#39;azarÃ£o&#39; (Getty Images)"
                data-original-image="s2.glbimg.com/wncm_nffwxI7fTmy69X9Z35Rqw4=/293x444:2154x1444/335x180/s.glbimg.com/es/ge/f/original/2015/11/15/gettyimages-497206248.jpg" data-url-smart_horizontal="2kQHERVWSAe7tqlxpTM4NGuupqU=/90x0/smart/filters:strip_icc()/" data-url-smart="2kQHERVWSAe7tqlxpTM4NGuupqU=/90x0/smart/filters:strip_icc()/" data-url-feature="2kQHERVWSAe7tqlxpTM4NGuupqU=/90x0/smart/filters:strip_icc()/" data-url-tablet="fkEwulrAaqQjrzlGOg2c0I0JTcM=/220x125/smart/filters:strip_icc()/" data-url-desktop="du4xicE5N8fUCVT_D5akxat0SBQ=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Agente de Holm agradece Vegas e diz <br />ter faturado seis dÃ­gitos com &#39;azarÃ£o&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 14:35:49" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/internacional/noticia/2015/11/valdivia-tem-lesao-no-ligamento-e-passa-por-cirurgia-nesta-segunda.html" class="foto" title="ValdÃ­via farÃ¡ cirurgia no joelho esquerdo e desfalca o Inter por seis meses (Felipe Schmidt/GloboEsporte.com)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/krPSK561f1eSCDFCRv1m9PuwlP8=/filters:quality(10):strip_icc()/s2.glbimg.com/pAz03lymhSWcocp6bR47ZofZC0c=/83x42:203x122/120x80/s.glbimg.com/jo/g1/f/original/2015/11/15/valdivia.jpg" alt="ValdÃ­via farÃ¡ cirurgia no joelho esquerdo e desfalca o Inter por seis meses (Felipe Schmidt/GloboEsporte.com)" title="ValdÃ­via farÃ¡ cirurgia no joelho esquerdo e desfalca o Inter por seis meses (Felipe Schmidt/GloboEsporte.com)"
                data-original-image="s2.glbimg.com/pAz03lymhSWcocp6bR47ZofZC0c=/83x42:203x122/120x80/s.glbimg.com/jo/g1/f/original/2015/11/15/valdivia.jpg" data-url-smart_horizontal="KXHNurG7jSh0U4Tor0UQUJmJRTc=/90x0/smart/filters:strip_icc()/" data-url-smart="KXHNurG7jSh0U4Tor0UQUJmJRTc=/90x0/smart/filters:strip_icc()/" data-url-feature="KXHNurG7jSh0U4Tor0UQUJmJRTc=/90x0/smart/filters:strip_icc()/" data-url-tablet="V50iZ7pl4AsiOqeLked_2iJ8-zA=/70x50/smart/filters:strip_icc()/" data-url-desktop="sKeX_fYj8wzqc7d3isC1umc3xrU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ValdÃ­via farÃ¡ cirurgia no joelho esquerdo e desfalca o Inter por seis meses</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 14:52:51" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/programas/troca-de-passes/noticia/2015/11/lino-dispara-contra-atitude-de-atletas-do-fla-em-amistoso-constrangedor.html" class="foto" title="Flamengo teve atuaÃ§Ã£o &#39;constrangedora&#39; em amistoso, diz comentarista (AndrÃ© DurÃ£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/SuL1-JDdI2edZBS9XaOouzgI-8Y=/filters:quality(10):strip_icc()/s2.glbimg.com/-qW760G5HsnBEDeDw4UyFxTLg4o=/1015x276:2461x1240/120x80/s.glbimg.com/es/ge/f/original/2015/11/15/1_1.jpg" alt="Flamengo teve atuaÃ§Ã£o &#39;constrangedora&#39; em amistoso, diz comentarista (AndrÃ© DurÃ£o)" title="Flamengo teve atuaÃ§Ã£o &#39;constrangedora&#39; em amistoso, diz comentarista (AndrÃ© DurÃ£o)"
                data-original-image="s2.glbimg.com/-qW760G5HsnBEDeDw4UyFxTLg4o=/1015x276:2461x1240/120x80/s.glbimg.com/es/ge/f/original/2015/11/15/1_1.jpg" data-url-smart_horizontal="uEByoJ2tPX56N221Pt5SB9Cd5LI=/90x0/smart/filters:strip_icc()/" data-url-smart="uEByoJ2tPX56N221Pt5SB9Cd5LI=/90x0/smart/filters:strip_icc()/" data-url-feature="uEByoJ2tPX56N221Pt5SB9Cd5LI=/90x0/smart/filters:strip_icc()/" data-url-tablet="kjGWU24xIShX4djgRUmPmauY8O8=/70x50/smart/filters:strip_icc()/" data-url-desktop="ScHpnCaTKIQjbpT4lnKioul39_g=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Flamengo teve atuaÃ§Ã£o &#39;constrangedora&#39; em amistoso, diz comentarista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 16:18:21" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/bastidores-fc/post/por-divida-justica-busca-luxemburgo-na-sede-do-corinthians-e-nao-encontra.html" class="" title="Luxemburgo Ã© procurado pela JustiÃ§a no Corinthians 14 anos apÃ³s deixar o clube (Daniel Augusto Jr/Ag. Corinthians)" rel="bookmark"><span class="conteudo"><p>Luxemburgo Ã© procurado pela JustiÃ§a no Corinthians 14 anos apÃ³s deixar o clube</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/sp/santos-e-regiao/futebol/noticia/2015/11/procura-de-um-zagueiro-santos-mira-gum-do-fluminense-para-2016.html" class="foto" title="Santos acredita que pode conseguir a liberaÃ§Ã£o de graÃ§a do tricolor Gum (Nelson Perez / Fluminense FC)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/cRXwXIDCxjxixpGv9OsMbCAtnzs=/filters:quality(10):strip_icc()/s2.glbimg.com/zb3W8vG0ED3e6suvMhqN6yJ31Zs=/626x297:1186x671/120x80/s.glbimg.com/es/ge/f/original/2015/04/27/treino_flu_1.jpg" alt="Santos acredita que pode conseguir a liberaÃ§Ã£o de graÃ§a do tricolor Gum (Nelson Perez / Fluminense FC)" title="Santos acredita que pode conseguir a liberaÃ§Ã£o de graÃ§a do tricolor Gum (Nelson Perez / Fluminense FC)"
                data-original-image="s2.glbimg.com/zb3W8vG0ED3e6suvMhqN6yJ31Zs=/626x297:1186x671/120x80/s.glbimg.com/es/ge/f/original/2015/04/27/treino_flu_1.jpg" data-url-smart_horizontal="XiRdxSgi12vd6Vf5nAhJtQPpQCk=/90x0/smart/filters:strip_icc()/" data-url-smart="XiRdxSgi12vd6Vf5nAhJtQPpQCk=/90x0/smart/filters:strip_icc()/" data-url-feature="XiRdxSgi12vd6Vf5nAhJtQPpQCk=/90x0/smart/filters:strip_icc()/" data-url-tablet="1AS3YjVdqJrPV6o6woBr1qO3L6o=/70x50/smart/filters:strip_icc()/" data-url-desktop="dZlFcM4CpW8fqXpDZs9y_kBMKrQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Santos acredita que pode conseguir  a liberaÃ§Ã£o de graÃ§a do tricolor Gum</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 16:27:22" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/eventos/torneio-dos-campeoes-da-atp/noticia/2015/11/murray-fracassa-ao-imitar-r10-mas-vence-ferrer-na-estreia-em-londres.html" class="foto" title="Murray vence apÃ³s &#39;jogada como R10&#39; na qual se deu mal; assista ao vÃ­deo aqui (Reuters)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/UIKgkTweQvbGPr8rullLHoiVAPI=/filters:quality(10):strip_icc()/s2.glbimg.com/XenlgfPnynHcPl5X-0mB6DSMVXg=/297x48:1646x948/120x80/s.glbimg.com/es/ge/f/original/2015/11/16/2015-11-16t155539z_90690939_mt1aci14173734_rtrmadp_3_ten_1.jpg" alt="Murray vence apÃ³s &#39;jogada como R10&#39; na qual se deu mal; assista ao vÃ­deo aqui (Reuters)" title="Murray vence apÃ³s &#39;jogada como R10&#39; na qual se deu mal; assista ao vÃ­deo aqui (Reuters)"
                data-original-image="s2.glbimg.com/XenlgfPnynHcPl5X-0mB6DSMVXg=/297x48:1646x948/120x80/s.glbimg.com/es/ge/f/original/2015/11/16/2015-11-16t155539z_90690939_mt1aci14173734_rtrmadp_3_ten_1.jpg" data-url-smart_horizontal="sXJK87VYKn1Bi_sjtykCKAAf5W0=/90x0/smart/filters:strip_icc()/" data-url-smart="sXJK87VYKn1Bi_sjtykCKAAf5W0=/90x0/smart/filters:strip_icc()/" data-url-feature="sXJK87VYKn1Bi_sjtykCKAAf5W0=/90x0/smart/filters:strip_icc()/" data-url-tablet="7y_lTsZrFz568pqCQunz4ezzalM=/70x50/smart/filters:strip_icc()/" data-url-desktop="0kgDGbLTdHsp1oZIVrMSm1pNbPY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Murray vence apÃ³s &#39;jogada como R10&#39; na qual se deu mal; assista ao vÃ­deo aqui</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/11/messi-bota-preco-para-trocar-o-barca-inglaterra-r-35-milhoes-por-semana.html" class="" title="Jornal: Messi pede R$ 3,5 milhÃµes para ir para Inglaterra; Arsenal teria preferÃªncia (Getty Images)" rel="bookmark"><span class="conteudo"><p>Jornal: Messi pede R$ 3,5 milhÃµes para ir <br />para Inglaterra; Arsenal teria preferÃªncia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/motor/formula-1/noticia/2015/11/animado-com-festa-brasileira-vettel-sente-falta-de-heroi-local-no-podio.html" class="foto" title="Vettel Ã© sÃ³ elogios para SP e torce para novo &#39;herÃ³i local&#39; brigar por vitÃ³rias (Getty Images)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/bq403if-kWfFpHNmRp3y4nJAJy8=/filters:quality(10):strip_icc()/s2.glbimg.com/dqnZhaKuldfNcqpbSioA3xz49lU=/605x643:1434x1197/120x80/s.glbimg.com/es/ge/f/original/2015/11/15/gettyimages-497268488_1.jpg" alt="Vettel Ã© sÃ³ elogios para SP e torce para novo &#39;herÃ³i local&#39; brigar por vitÃ³rias (Getty Images)" title="Vettel Ã© sÃ³ elogios para SP e torce para novo &#39;herÃ³i local&#39; brigar por vitÃ³rias (Getty Images)"
                data-original-image="s2.glbimg.com/dqnZhaKuldfNcqpbSioA3xz49lU=/605x643:1434x1197/120x80/s.glbimg.com/es/ge/f/original/2015/11/15/gettyimages-497268488_1.jpg" data-url-smart_horizontal="Cl-YCKJynnO0bOArXNJjsuy8k0o=/90x0/smart/filters:strip_icc()/" data-url-smart="Cl-YCKJynnO0bOArXNJjsuy8k0o=/90x0/smart/filters:strip_icc()/" data-url-feature="Cl-YCKJynnO0bOArXNJjsuy8k0o=/90x0/smart/filters:strip_icc()/" data-url-tablet="VrrVqTwM0CInVXAAUhAG8n6edQQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="Rp6Ca_W_WiOKhk6m218Bi_E2Csw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Vettel Ã© sÃ³ elogios para SP e torce para novo &#39;herÃ³i local&#39; brigar por vitÃ³rias</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 15:09:54" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/listas/noticia/2015/11/lista-reune-os-bugs-mais-bizarros-e-engracados-de-pes-2016.html" class="foto" title="PES 2016 tem erros muito bizarros e hilÃ¡rios; confira os melhores em vÃ­deo
 (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/G5uezgmeT2K9FYuWWuoH8EFqp1g=/filters:quality(10):strip_icc()/s2.glbimg.com/ZR-otH2Vh-ZC0UXG6_Qs6OMrSzc=/55x16:511x321/120x80/s.glbimg.com/po/tt2/f/original/2015/11/16/guerrero-flamengo-pes-2016.jpg" alt="PES 2016 tem erros muito bizarros e hilÃ¡rios; confira os melhores em vÃ­deo
 (DivulgaÃ§Ã£o)" title="PES 2016 tem erros muito bizarros e hilÃ¡rios; confira os melhores em vÃ­deo
 (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/ZR-otH2Vh-ZC0UXG6_Qs6OMrSzc=/55x16:511x321/120x80/s.glbimg.com/po/tt2/f/original/2015/11/16/guerrero-flamengo-pes-2016.jpg" data-url-smart_horizontal="zBqIvmviN-2Fqh8Cw5N6Rxq55e8=/90x0/smart/filters:strip_icc()/" data-url-smart="zBqIvmviN-2Fqh8Cw5N6Rxq55e8=/90x0/smart/filters:strip_icc()/" data-url-feature="zBqIvmviN-2Fqh8Cw5N6Rxq55e8=/90x0/smart/filters:strip_icc()/" data-url-tablet="b4wKktwWwve8w7lbuA1lAF612nw=/70x50/smart/filters:strip_icc()/" data-url-desktop="O8ovc3kyj9tyyq1z_BjzP3xZ4JU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PES 2016 tem erros muito bizarros e hilÃ¡rios; confira os melhores em vÃ­deo<br /></p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/sensual/noticia/2015/11/graciella-carvalho-exibe-curvas-apos-dama-com-hidrogel-sou-vencedora.html" class="foto" title="ApÃ³s drama com hidrogel, Graciella Carvalho posa e declara: &#39;Vencedora&#39; (Studio Woody / Ag Fio Condutor)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/XohK9xAlDrraVo4uvOxObGxRMDY=/filters:quality(10):strip_icc()/s2.glbimg.com/zIacKMMDO4v5tYUOh7WGPGHYcvQ=/0x128:1600x988/335x180/s.glbimg.com/jo/eg/f/original/2015/11/16/20151116081849.jpg" alt="ApÃ³s drama com hidrogel, Graciella Carvalho posa e declara: &#39;Vencedora&#39; (Studio Woody / Ag Fio Condutor)" title="ApÃ³s drama com hidrogel, Graciella Carvalho posa e declara: &#39;Vencedora&#39; (Studio Woody / Ag Fio Condutor)"
                data-original-image="s2.glbimg.com/zIacKMMDO4v5tYUOh7WGPGHYcvQ=/0x128:1600x988/335x180/s.glbimg.com/jo/eg/f/original/2015/11/16/20151116081849.jpg" data-url-smart_horizontal="Ivz1a2vNcf9LcYkWf6eeY8avedw=/90x0/smart/filters:strip_icc()/" data-url-smart="Ivz1a2vNcf9LcYkWf6eeY8avedw=/90x0/smart/filters:strip_icc()/" data-url-feature="Ivz1a2vNcf9LcYkWf6eeY8avedw=/90x0/smart/filters:strip_icc()/" data-url-tablet="WS6UptHWLaSrJDLjHIreVk9hf7s=/220x125/smart/filters:strip_icc()/" data-url-desktop="FUDtTHfj-J4V0JKjis-As3hHvQc=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ApÃ³s drama com hidrogel, Graciella Carvalho posa e declara: &#39;Vencedora&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 15:18:12" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/moda/noticia/2015/11/rafaella-santos-diz-que-neymar-e-o-pai-mimam-fazem-tudo-que-quero.html" class="foto" title="Rafaella Santos posa e diz gostar do corpo: &#39;Se Deus me fez assim...&#39;; fotos (MaurÃ­cio Nahas / Revista Joyce Pascowitch)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/quG_Cc4hexPINJgr0yWVwqr0Puw=/filters:quality(10):strip_icc()/s2.glbimg.com/0ypV6i0v4BlmSIImSbCTFLZWuzM=/548x149:1501x785/120x80/s.glbimg.com/jo/eg/f/original/2015/11/16/rafaella_santos.jpg" alt="Rafaella Santos posa e diz gostar do corpo: &#39;Se Deus me fez assim...&#39;; fotos (MaurÃ­cio Nahas / Revista Joyce Pascowitch)" title="Rafaella Santos posa e diz gostar do corpo: &#39;Se Deus me fez assim...&#39;; fotos (MaurÃ­cio Nahas / Revista Joyce Pascowitch)"
                data-original-image="s2.glbimg.com/0ypV6i0v4BlmSIImSbCTFLZWuzM=/548x149:1501x785/120x80/s.glbimg.com/jo/eg/f/original/2015/11/16/rafaella_santos.jpg" data-url-smart_horizontal="JUCIDV-nedrpW7q2l-JCMb-403U=/90x0/smart/filters:strip_icc()/" data-url-smart="JUCIDV-nedrpW7q2l-JCMb-403U=/90x0/smart/filters:strip_icc()/" data-url-feature="JUCIDV-nedrpW7q2l-JCMb-403U=/90x0/smart/filters:strip_icc()/" data-url-tablet="Crc_b843ewFKUyQu_SMiQg1hMmE=/70x50/smart/filters:strip_icc()/" data-url-desktop="yuQKKEcS9itM1U5XwDgJV-0SKW0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Rafaella Santos posa e diz gostar do corpo: &#39;Se Deus me fez assim...&#39;; fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 16:23:01" class="chamada chamada-principal mobile-grid-full"><a href="http://kogut.oglobo.globo.com/noticias-da-tv/noticia/2015/11/ellen-rocche-fala-do-sucesso-da-irma-na-internet-e-diz-que-aos-36-tem-mais-dificuldade-para-emagrecer.html" class="foto" title="Ellen Roche comenta sucesso da irmÃ£ na web: &#39;Ela Ã© tÃ­mida. Levei bronca&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/e6cVzog7skpv-zZbOaa2JYwWYKQ=/filters:quality(10):strip_icc()/s2.glbimg.com/uaEIDZ05hJFA0nB16j8jsC0v_cA=/0x0:1200x799/120x80/i.glbimg.com/og/ig/infoglobo/f/original/2015/11/16/montagem_ellen.jpg" alt="Ellen Roche comenta sucesso da irmÃ£ na web: &#39;Ela Ã© tÃ­mida. Levei bronca&#39; (ReproduÃ§Ã£o)" title="Ellen Roche comenta sucesso da irmÃ£ na web: &#39;Ela Ã© tÃ­mida. Levei bronca&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/uaEIDZ05hJFA0nB16j8jsC0v_cA=/0x0:1200x799/120x80/i.glbimg.com/og/ig/infoglobo/f/original/2015/11/16/montagem_ellen.jpg" data-url-smart_horizontal="VdsnGPDX2Z9jppalVWDSTgktNfE=/90x0/smart/filters:strip_icc()/" data-url-smart="VdsnGPDX2Z9jppalVWDSTgktNfE=/90x0/smart/filters:strip_icc()/" data-url-feature="VdsnGPDX2Z9jppalVWDSTgktNfE=/90x0/smart/filters:strip_icc()/" data-url-tablet="sk_ECMpIDs6HQjXlhj6HMhA4scg=/70x50/smart/filters:strip_icc()/" data-url-desktop="Zm4Tz58k_LAZ1ENTDr-90o48l9g=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ellen Roche comenta sucesso da irmÃ£ na web: &#39;Ela Ã© tÃ­mida. Levei bronca&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 09:28:10" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/cameron-diaz-aparece-nua-em-raro-ensaio-divulgado-por-revista.html" class="" title="Cameron Diaz aparece completamente nua em ensaio antigo divulgado por revista (Manuela Scarpa/Brazil News)" rel="bookmark"><span class="conteudo"><p>Cameron Diaz aparece completamente nua em ensaio antigo divulgado por revista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 15:48:33" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/flavia-alessandra-brinca-com-filha-cacula-em-shopping.html" class="foto" title="LoirÃ­ssima, F. Alessandra brinca com a filha caÃ§ula em shopping do Rio; fotos (AGNEWS)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/6734gdSMDQ_c-n94APGutosRkqU=/filters:quality(10):strip_icc()/s2.glbimg.com/BOVXuWDVXCVVR7Zo1cEfzibACNo=/152x83:486x305/120x80/e.glbimg.com/og/ed/f/original/2015/11/16/dsc_8931.jpg" alt="LoirÃ­ssima, F. Alessandra brinca com a filha caÃ§ula em shopping do Rio; fotos (AGNEWS)" title="LoirÃ­ssima, F. Alessandra brinca com a filha caÃ§ula em shopping do Rio; fotos (AGNEWS)"
                data-original-image="s2.glbimg.com/BOVXuWDVXCVVR7Zo1cEfzibACNo=/152x83:486x305/120x80/e.glbimg.com/og/ed/f/original/2015/11/16/dsc_8931.jpg" data-url-smart_horizontal="yLTo-C7lxrmWOANQmzYpl5nixHw=/90x0/smart/filters:strip_icc()/" data-url-smart="yLTo-C7lxrmWOANQmzYpl5nixHw=/90x0/smart/filters:strip_icc()/" data-url-feature="yLTo-C7lxrmWOANQmzYpl5nixHw=/90x0/smart/filters:strip_icc()/" data-url-tablet="3LIlAKM1Bej08v9zloxzy-b2vY4=/70x50/smart/filters:strip_icc()/" data-url-desktop="8WV_OKX98JPYFuCjYxXiwR5-0KU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>LoirÃ­ssima, F. Alessandra brinca com a filha caÃ§ula em shopping do Rio; fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 15:33:41" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/alheia-rumores-bruna-marquezine-aproveita-ferias-no-nordeste.html" class="foto" title="Com o fim de &#39;I love&#39;, Bruna Marquezine curte fÃ©rias em praias alagoanas; fotos (ReproduÃ§Ã£o / Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/NZ_0yXWmsGTlth3NjTyQPfTPNqE=/filters:quality(10):strip_icc()/s2.glbimg.com/nh6sRQ4OvQU3czbAgtjKTbPlFbM=/0x74:690x534/120x80/s.glbimg.com/et/gs/f/original/2015/11/16/bruna_690.jpg" alt="Com o fim de &#39;I love&#39;, Bruna Marquezine curte fÃ©rias em praias alagoanas; fotos (ReproduÃ§Ã£o / Instagram)" title="Com o fim de &#39;I love&#39;, Bruna Marquezine curte fÃ©rias em praias alagoanas; fotos (ReproduÃ§Ã£o / Instagram)"
                data-original-image="s2.glbimg.com/nh6sRQ4OvQU3czbAgtjKTbPlFbM=/0x74:690x534/120x80/s.glbimg.com/et/gs/f/original/2015/11/16/bruna_690.jpg" data-url-smart_horizontal="QQEmgAc6Nc42HNcbWe3wvEvwb30=/90x0/smart/filters:strip_icc()/" data-url-smart="QQEmgAc6Nc42HNcbWe3wvEvwb30=/90x0/smart/filters:strip_icc()/" data-url-feature="QQEmgAc6Nc42HNcbWe3wvEvwb30=/90x0/smart/filters:strip_icc()/" data-url-tablet="2l3TLLyh-n4vtws5o6fsf0cpFGw=/70x50/smart/filters:strip_icc()/" data-url-desktop="CKxu5LEZh-SMNlEnPlv-gYg8tnQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Com o fim de &#39;I love&#39;, Bruna Marquezine curte fÃ©rias em praias alagoanas; fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 10:32:10" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/shannen-doherty-faz-primeira-aparicao-publica-apos-revelar-cancer.html" class="" title="Ex-&#39;Barrados no Baile&#39; faz primeira apariÃ§Ã£o pÃºblica depois de revelar cÃ¢ncer (Frederick M. Brown/Getty Images)" rel="bookmark"><span class="conteudo"><p>Ex-&#39;Barrados no Baile&#39; faz primeira apariÃ§Ã£o pÃºblica depois de revelar cÃ¢ncer</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-16 15:40:49" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/jaque-khury-posa-com-o-filho-em-nova-york.html" class="foto" title="Jaque Khury posta foto de casacÃ£o com o filho sorrindo na neve em NY (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/mGezVaBP3xtS22Owdx-VUvrlLKk=/filters:quality(10):strip_icc()/s2.glbimg.com/FDZ8qoocQwRAp0eIvBYIEZ_46iA=/177x334:516x559/120x80/e.glbimg.com/og/ed/f/original/2015/11/16/12231085_1520170721632572_243596109_n.jpg" alt="Jaque Khury posta foto de casacÃ£o com o filho sorrindo na neve em NY (ReproduÃ§Ã£o)" title="Jaque Khury posta foto de casacÃ£o com o filho sorrindo na neve em NY (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/FDZ8qoocQwRAp0eIvBYIEZ_46iA=/177x334:516x559/120x80/e.glbimg.com/og/ed/f/original/2015/11/16/12231085_1520170721632572_243596109_n.jpg" data-url-smart_horizontal="BajMlyDw1hyNc34frNR0RCoJQSM=/90x0/smart/filters:strip_icc()/" data-url-smart="BajMlyDw1hyNc34frNR0RCoJQSM=/90x0/smart/filters:strip_icc()/" data-url-feature="BajMlyDw1hyNc34frNR0RCoJQSM=/90x0/smart/filters:strip_icc()/" data-url-tablet="FGIM_a_oabGS216ALYe5h1BpFUQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="bmKOpVCkstcV_nF7xGgWwMRxxPk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jaque Khury posta foto <br />de casacÃ£o com o filho sorrindo na neve em NY</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry publieditorial"><div data-photo-subtitle="2015-11-16 11:11:33" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/ep/muito-mais-para-o-seu-bebe/?utm_source=home-globocom&amp;utm_medium=fake-banner&amp;utm_term=15-11-16&amp;utm_content=muito-mais-para-o-seu-bebe&amp;utm_campaign=j-e-j" class="foto" title="Seu bebÃª merece muito carinho! Confira nossas dicas para cuidar do pequeno (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/JU-DmEv9A3n122vSlpLf2ZQ4bLM=/filters:quality(10):strip_icc()/s2.glbimg.com/H3CmQ4OwWYeP0q_3xt0S2JretDI=/84x95:673x488/120x80/s.glbimg.com/et/gs/f/original/2015/11/12/fraldasssss.jpg" alt="Seu bebÃª merece muito carinho! Confira nossas dicas para cuidar do pequeno (DivulgaÃ§Ã£o)" title="Seu bebÃª merece muito carinho! Confira nossas dicas para cuidar do pequeno (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/H3CmQ4OwWYeP0q_3xt0S2JretDI=/84x95:673x488/120x80/s.glbimg.com/et/gs/f/original/2015/11/12/fraldasssss.jpg" data-url-smart_horizontal="xCunFVkEwaKv8-SOhMTjw_njAhM=/90x0/smart/filters:strip_icc()/" data-url-smart="xCunFVkEwaKv8-SOhMTjw_njAhM=/90x0/smart/filters:strip_icc()/" data-url-feature="xCunFVkEwaKv8-SOhMTjw_njAhM=/90x0/smart/filters:strip_icc()/" data-url-tablet="S705z0BwNYpv5fF3Tkhc8JH9gMA=/70x50/smart/filters:strip_icc()/" data-url-desktop="AaZ37Ltrr7_9XeMx3TnPF-Won6U=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><span>ESPECIAL PUBLICITÃRIO</span><p>Seu bebÃª merece muito carinho! Confira nossas dicas para cuidar do pequeno</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "Totalmente Demais", "url": "http://gshow.globo.com/novelas/totalmente-demais/", "logo": "http://s2.glbimg.com/R8T_N-bSmq23FCuZwf27cTTmjzg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/11/06/logo_36x20.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/totalmente-demais/rss/", "logo_tv": "http://s2.glbimg.com/DOKGjWF1k80-ujyw_Q-OT1IX65M=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/11/06/logo_45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "Totalmente Demais", "url": "http://gshow.globo.com/novelas/totalmente-demais/", "logo": "http://s2.glbimg.com/R8T_N-bSmq23FCuZwf27cTTmjzg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/11/06/logo_36x20.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/totalmente-demais/rss/", "logo_tv": "http://s2.glbimg.com/DOKGjWF1k80-ujyw_Q-OT1IX65M=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/11/06/logo_45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"></section><section class="area sports-column"></section><section class="area etc-column analytics-area"></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/11/xperia-z5-ou-galaxy-s6-qual-smartohone-top-de-linha-e-o-melhor.html" title="Galaxy S6 &#39;derruba&#39; top da Sony?
Veja qual Ã© o smart do momento"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/7uE-28qqAJAo4hHZF4hmzpLuGyc=/filters:quality(10):strip_icc()/s2.glbimg.com/PI3ujYGswmdblHV2Rvl4dUNB_4o=/0x20:695x389/245x130/s.glbimg.com/po/tt2/f/original/2015/04/16/galaxy-s6-41.jpg" alt="Galaxy S6 &#39;derruba&#39; top da Sony?
Veja qual Ã© o smart do momento (Galaxy S6 e S6 Edge entram em prÃ©-venda no Brasil (Foto: FabrÃ­cio Vitorino/TechTudo))" title="Galaxy S6 &#39;derruba&#39; top da Sony?
Veja qual Ã© o smart do momento (Galaxy S6 e S6 Edge entram em prÃ©-venda no Brasil (Foto: FabrÃ­cio Vitorino/TechTudo))"
             data-original-image="s2.glbimg.com/PI3ujYGswmdblHV2Rvl4dUNB_4o=/0x20:695x389/245x130/s.glbimg.com/po/tt2/f/original/2015/04/16/galaxy-s6-41.jpg" data-url-smart_horizontal="tz_dCps8wzqk-3oI7dwrJrzjIjc=/90x56/smart/filters:strip_icc()/" data-url-smart="tz_dCps8wzqk-3oI7dwrJrzjIjc=/90x56/smart/filters:strip_icc()/" data-url-feature="tz_dCps8wzqk-3oI7dwrJrzjIjc=/90x56/smart/filters:strip_icc()/" data-url-tablet="F_mmso4ugCQhsbC0uQbqY2A3KKA=/160x96/smart/filters:strip_icc()/" data-url-desktop="ZxhTVTo7ol15PRoJIGLEqGVvKYM=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Galaxy S6 &#39;derruba&#39; top da Sony?<br />Veja qual Ã© o smart do momento</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/11/como-usar-o-pixlr-para-editar-fotos-pelo-celular.html" title="Aplicativo &#39;mÃ¡gico&#39; deixa as suas fotos perfeitas; aprenda a usar"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/-8BrMuTXEXzFI4jatoA4B2CwvX4=/filters:quality(10):strip_icc()/s2.glbimg.com/SUejY5CLtLUPwe8sFVNjCepDb0Y=/0x43:960x553/245x130/s.glbimg.com/po/tt2/f/original/2013/11/22/029387093-smiling-girl-taking-photo-hers.jpeg" alt="Aplicativo &#39;mÃ¡gico&#39; deixa as suas fotos perfeitas; aprenda a usar (Pond5)" title="Aplicativo &#39;mÃ¡gico&#39; deixa as suas fotos perfeitas; aprenda a usar (Pond5)"
             data-original-image="s2.glbimg.com/SUejY5CLtLUPwe8sFVNjCepDb0Y=/0x43:960x553/245x130/s.glbimg.com/po/tt2/f/original/2013/11/22/029387093-smiling-girl-taking-photo-hers.jpeg" data-url-smart_horizontal="Q4Pi4JXQ73ajqAsfWAlhbhagCyA=/90x56/smart/filters:strip_icc()/" data-url-smart="Q4Pi4JXQ73ajqAsfWAlhbhagCyA=/90x56/smart/filters:strip_icc()/" data-url-feature="Q4Pi4JXQ73ajqAsfWAlhbhagCyA=/90x56/smart/filters:strip_icc()/" data-url-tablet="6gvWIPUjds7AdRL49k2uHv4axVQ=/160x96/smart/filters:strip_icc()/" data-url-desktop="rht2L-omTKt8Ksytc6ihzU8vdT8=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Aplicativo &#39;mÃ¡gico&#39; deixa as suas <br />fotos perfeitas; aprenda a usar</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/11/windows-10-testamos-o-update-1-veja-o-que-muda-e-se-vale-atualizar.html" title="AtualizaÃ§Ã£o do Windows 10: teste revela o que muda e se vale fazer"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ij6aJ21LmQRugZcLyyUd5EJbw_8=/filters:quality(10):strip_icc()/s2.glbimg.com/6Paex93gSPs1WwzeqDVQ8_XJXW4=/36x0:691x348/245x130/s.glbimg.com/po/tt2/f/original/2015/11/16/img_4264.jpg" alt="AtualizaÃ§Ã£o do Windows 10: teste revela o que muda e se vale fazer (ZÃ­ngara Lofrano/TechTudo)" title="AtualizaÃ§Ã£o do Windows 10: teste revela o que muda e se vale fazer (ZÃ­ngara Lofrano/TechTudo)"
             data-original-image="s2.glbimg.com/6Paex93gSPs1WwzeqDVQ8_XJXW4=/36x0:691x348/245x130/s.glbimg.com/po/tt2/f/original/2015/11/16/img_4264.jpg" data-url-smart_horizontal="0vq3CwtgVip0XkAkquveCSviDSs=/90x56/smart/filters:strip_icc()/" data-url-smart="0vq3CwtgVip0XkAkquveCSviDSs=/90x56/smart/filters:strip_icc()/" data-url-feature="0vq3CwtgVip0XkAkquveCSviDSs=/90x56/smart/filters:strip_icc()/" data-url-tablet="PtFWbExHcy3AjN1_yEohyyg6mLs=/160x96/smart/filters:strip_icc()/" data-url-desktop="P0E2XojG82xE2K_D175uAVZjFl8=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>AtualizaÃ§Ã£o do Windows 10: teste <br />revela o que muda e se vale fazer</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/tudo-sobre/reduza.html" title="Site brasileiro te dÃ¡ descontos 
&#39;incrÃ­veis&#39; em lojas online; veja"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/RyYaIyTDc30aJB89u7WLzbLgeMo=/filters:quality(10):strip_icc()/s2.glbimg.com/j0IfJLxOIftEtwLC-uVFGahn_u4=/0x15:695x384/245x130/s.glbimg.com/po/tt2/f/original/2015/08/14/coins-2489177.jpg" alt="Site brasileiro te dÃ¡ descontos 
&#39;incrÃ­veis&#39; em lojas online; veja (Pond5)" title="Site brasileiro te dÃ¡ descontos 
&#39;incrÃ­veis&#39; em lojas online; veja (Pond5)"
             data-original-image="s2.glbimg.com/j0IfJLxOIftEtwLC-uVFGahn_u4=/0x15:695x384/245x130/s.glbimg.com/po/tt2/f/original/2015/08/14/coins-2489177.jpg" data-url-smart_horizontal="G5quoGkVujLq-bCqDBUK7yCZXhQ=/90x56/smart/filters:strip_icc()/" data-url-smart="G5quoGkVujLq-bCqDBUK7yCZXhQ=/90x56/smart/filters:strip_icc()/" data-url-feature="G5quoGkVujLq-bCqDBUK7yCZXhQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="igiC-Y-tyMLS41sz1bufYho5j8w=/160x96/smart/filters:strip_icc()/" data-url-desktop="YL00u1jNW20de49ecyy2g5Ab3Q4=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Site brasileiro te dÃ¡ descontos <br />&#39;incrÃ­veis&#39; em lojas online; veja</p></span></a></li></ul></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Beleza/noticia/2015/11/academia-na-agua-conheca-os-esportes-aquaticos-que-vao-ajuda-la-perder-peso-e-definir-o-corpo.html" alt="ConheÃ§a esportes aquÃ¡ticos que ajudam a emagrecer e a firmar"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/xjZKS2O5Pl4Jw9xgbfPKl2jQeQw=/filters:quality(10):strip_icc()/s2.glbimg.com/MCdq4YhxfdiE72e9bNT1tjJbR9k=/193x6:545x233/155x100/s.glbimg.com/en/ho/f/original/2015/11/16/academia-na-agua.jpg" alt="ConheÃ§a esportes aquÃ¡ticos que ajudam a emagrecer e a firmar (Jorg Badura/Trunk Archive)" title="ConheÃ§a esportes aquÃ¡ticos que ajudam a emagrecer e a firmar (Jorg Badura/Trunk Archive)"
            data-original-image="s2.glbimg.com/MCdq4YhxfdiE72e9bNT1tjJbR9k=/193x6:545x233/155x100/s.glbimg.com/en/ho/f/original/2015/11/16/academia-na-agua.jpg" data-url-smart_horizontal="caS9PxWQlWiSOgmK008Axrn3DSE=/90x56/smart/filters:strip_icc()/" data-url-smart="caS9PxWQlWiSOgmK008Axrn3DSE=/90x56/smart/filters:strip_icc()/" data-url-feature="caS9PxWQlWiSOgmK008Axrn3DSE=/90x56/smart/filters:strip_icc()/" data-url-tablet="vTa6wOu7ihxC3qq2-FVqzNKVwqY=/122x75/smart/filters:strip_icc()/" data-url-desktop="u_vQ8-9gE9SM1H82OHyITnuZ7u4=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Academia na Ã¡gua</h3><p>ConheÃ§a esportes aquÃ¡ticos que ajudam a emagrecer e a firmar</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/moda/votacoes/sandalias-de-plastico-deixam-o-look-mais-descontraido-vote-na-sua-favorita1.htm" alt="SandÃ¡lias de plÃ¡stico deixam o look descontraÃ­do: diga qual vocÃª usaria"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/RLpTE1qJaJQqbE_Ahzpu0GisJOg=/filters:quality(10):strip_icc()/s2.glbimg.com/NxycKwccZDg3xlpfcAStt6VGqKE=/107x458:262x558/155x100/s.glbimg.com/en/ho/f/original/2015/11/16/sandalia_gnt.jpg" alt="SandÃ¡lias de plÃ¡stico deixam o look descontraÃ­do: diga qual vocÃª usaria (Juliana Rocha)" title="SandÃ¡lias de plÃ¡stico deixam o look descontraÃ­do: diga qual vocÃª usaria (Juliana Rocha)"
            data-original-image="s2.glbimg.com/NxycKwccZDg3xlpfcAStt6VGqKE=/107x458:262x558/155x100/s.glbimg.com/en/ho/f/original/2015/11/16/sandalia_gnt.jpg" data-url-smart_horizontal="6RmOYP9MnVkyOkyYuzaF5rCndag=/90x56/smart/filters:strip_icc()/" data-url-smart="6RmOYP9MnVkyOkyYuzaF5rCndag=/90x56/smart/filters:strip_icc()/" data-url-feature="6RmOYP9MnVkyOkyYuzaF5rCndag=/90x56/smart/filters:strip_icc()/" data-url-tablet="uJwu-IPjTgL11zjvsJ-7SQBUERk=/122x75/smart/filters:strip_icc()/" data-url-desktop="lg391ETkCCzVIEuZKQ3I3ia3NUI=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>vote na sua favorita</h3><p>SandÃ¡lias de plÃ¡stico deixam o look descontraÃ­do: diga qual vocÃª usaria</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Beleza/noticia/2015/11/pele-morena-e-negra-aprenda-cuidar.html" alt="Pele morena e negra: aprenda cuidados para mantÃª-la bela"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/S0doXkAFMRMa8aloI6pymU8GW68=/filters:quality(10):strip_icc()/s2.glbimg.com/8tzToLVjVpLuLNpv5Qb0FdkU1iY=/233x84:388x185/155x100/e.glbimg.com/og/ed/f/original/2015/09/03/beleza-afro-sheron-7.jpg" alt="Pele morena e negra: aprenda cuidados para mantÃª-la bela (Paulo Vainer)" title="Pele morena e negra: aprenda cuidados para mantÃª-la bela (Paulo Vainer)"
            data-original-image="s2.glbimg.com/8tzToLVjVpLuLNpv5Qb0FdkU1iY=/233x84:388x185/155x100/e.glbimg.com/og/ed/f/original/2015/09/03/beleza-afro-sheron-7.jpg" data-url-smart_horizontal="s44cmR37aalQKN-OvP0Fjk8Yy6Y=/90x56/smart/filters:strip_icc()/" data-url-smart="s44cmR37aalQKN-OvP0Fjk8Yy6Y=/90x56/smart/filters:strip_icc()/" data-url-feature="s44cmR37aalQKN-OvP0Fjk8Yy6Y=/90x56/smart/filters:strip_icc()/" data-url-tablet="lDuPTRqG_-14TZ6aVNtW0v2eZLU=/122x75/smart/filters:strip_icc()/" data-url-desktop="BoFD5MKzAQyyIid5ftsNTWtFWdQ=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>dermatologista dÃ¡ dicas</h3><p>Pele morena e negra: aprenda cuidados para mantÃª-la bela</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/apartamentos/noticia/2015/10/sofa-versatil-e-marcenaria-transformam-sala-de-35-m.html" alt="Reforma dÃ¡ mais espaÃ§o a 
Ã¡rea social de apartamento"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/kigxM0O7kS8AD4_HDAaI2Wtpo04=/filters:quality(10):strip_icc()/s2.glbimg.com/-PuBwUGLYaK31Rd0O1vegEVY_mo=/32x144:591x505/155x100/e.glbimg.com/og/ed/f/original/2015/10/22/apartamento-abc-ambidestro_4.jpg" alt="Reforma dÃ¡ mais espaÃ§o a 
Ã¡rea social de apartamento (Marcelo Donadussi/ divulgaÃ§Ã£o )" title="Reforma dÃ¡ mais espaÃ§o a 
Ã¡rea social de apartamento (Marcelo Donadussi/ divulgaÃ§Ã£o )"
            data-original-image="s2.glbimg.com/-PuBwUGLYaK31Rd0O1vegEVY_mo=/32x144:591x505/155x100/e.glbimg.com/og/ed/f/original/2015/10/22/apartamento-abc-ambidestro_4.jpg" data-url-smart_horizontal="m62RIuKCRJe3xzoh_3yzmjq7ua4=/90x56/smart/filters:strip_icc()/" data-url-smart="m62RIuKCRJe3xzoh_3yzmjq7ua4=/90x56/smart/filters:strip_icc()/" data-url-feature="m62RIuKCRJe3xzoh_3yzmjq7ua4=/90x56/smart/filters:strip_icc()/" data-url-tablet="c7RhYLCROWSNaWdNT0teILVbIJ8=/122x75/smart/filters:strip_icc()/" data-url-desktop="aU70QQPm5cpTBZGYCFZ-Af1AKLA=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Projeto amplia e dÃ¡ charme</h3><p>Reforma dÃ¡ mais espaÃ§o a <br />Ã¡rea social de apartamento</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/veja-o-que-fazer-para-renovar-o-piso-de-taco/" alt="Restaure seu piso de taco com pintura e gaste pouco na reforma"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/8n135rdGSfLvtp4_kpBFOkfImlg=/filters:quality(10):strip_icc()/s2.glbimg.com/8CMMPA0D8IwIXDGBNvBRpOwyIzA=/8x23:476x326/155x100/s.glbimg.com/en/ho/f/original/2015/11/16/piso-taco.jpg" alt="Restaure seu piso de taco com pintura e gaste pouco na reforma (Shutterstock)" title="Restaure seu piso de taco com pintura e gaste pouco na reforma (Shutterstock)"
            data-original-image="s2.glbimg.com/8CMMPA0D8IwIXDGBNvBRpOwyIzA=/8x23:476x326/155x100/s.glbimg.com/en/ho/f/original/2015/11/16/piso-taco.jpg" data-url-smart_horizontal="8Sqa5jAQhvV4_uP9W-2Gru1fvYo=/90x56/smart/filters:strip_icc()/" data-url-smart="8Sqa5jAQhvV4_uP9W-2Gru1fvYo=/90x56/smart/filters:strip_icc()/" data-url-feature="8Sqa5jAQhvV4_uP9W-2Gru1fvYo=/90x56/smart/filters:strip_icc()/" data-url-tablet="phI2v6F7k5ikF9IIqk7G4BwJzNY=/122x75/smart/filters:strip_icc()/" data-url-desktop="9Fb8M8Dtj-0e8AuGnUs2Xey3FZ0=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>como se fosse novo</h3><p>Restaure seu piso de taco com pintura e gaste pouco na reforma</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Comida/Videos/noticia/2015/10/drops-14-como-descascar-uma-batata-cozida.html" alt="VÃ­deo ensina a descascar batatas de forma fÃ¡cil e sem segredos; veja"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/Fqfq-LUQp1K58_WP4ufsx9tpLPM=/filters:quality(10):strip_icc()/s2.glbimg.com/COZkqwRbX_7OrZ-mRdTlHYPgKSU=/399x98:1417x754/155x100/e.glbimg.com/og/ed/f/original/2015/10/29/batata.jpg" alt="VÃ­deo ensina a descascar batatas de forma fÃ¡cil e sem segredos; veja (ReproduÃ§Ã£o)" title="VÃ­deo ensina a descascar batatas de forma fÃ¡cil e sem segredos; veja (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/COZkqwRbX_7OrZ-mRdTlHYPgKSU=/399x98:1417x754/155x100/e.glbimg.com/og/ed/f/original/2015/10/29/batata.jpg" data-url-smart_horizontal="nmjdU9exURA7wWKFwab9SywvOn8=/90x56/smart/filters:strip_icc()/" data-url-smart="nmjdU9exURA7wWKFwab9SywvOn8=/90x56/smart/filters:strip_icc()/" data-url-feature="nmjdU9exURA7wWKFwab9SywvOn8=/90x56/smart/filters:strip_icc()/" data-url-tablet="9JoiTPMSf3psYLMMz3x4sQeZoXs=/122x75/smart/filters:strip_icc()/" data-url-desktop="j3pAFEbbJnmNTW76fukeOVCIML4=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>tÃ©cnica facilita a vida</h3><p>VÃ­deo ensina a descascar batatas <br />de forma fÃ¡cil e sem segredos; veja</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://epoca.globo.com/colunas-e-blogs/bruno-astuto/noticia/2015/11/daniela-mercury-vai-aparecer-nua-na-capa-do-novo-cd.html" title="Daniela Mercury vai aparecer nua na capa de novo Ã¡lbum"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/yJPtSc0kR57lBDbz9iZOV3h9Tyw=/filters:quality(10):strip_icc()/s2.glbimg.com/qfzPRj7XUO9T5HYKvUqXRiWUVDw=/0x65:466x313/245x130/s.glbimg.com/en/ho/f/original/2015/11/16/daniela_mercury.jpg" alt="Daniela Mercury vai aparecer nua na capa de novo Ã¡lbum (ReproduÃ§Ã£o)" title="Daniela Mercury vai aparecer nua na capa de novo Ã¡lbum (ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/qfzPRj7XUO9T5HYKvUqXRiWUVDw=/0x65:466x313/245x130/s.glbimg.com/en/ho/f/original/2015/11/16/daniela_mercury.jpg" data-url-smart_horizontal="2ZTEGd2zblrJND0T4s33wQz1pg4=/90x56/smart/filters:strip_icc()/" data-url-smart="2ZTEGd2zblrJND0T4s33wQz1pg4=/90x56/smart/filters:strip_icc()/" data-url-feature="2ZTEGd2zblrJND0T4s33wQz1pg4=/90x56/smart/filters:strip_icc()/" data-url-tablet="yziRNVvGwyYOsE5PVjWeiYVaQ4U=/160x95/smart/filters:strip_icc()/" data-url-desktop="Nj6AiUs4b6EEjYVXw2nIrAb1nEA=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Daniela Mercury vai aparecer nua na capa de novo Ã¡lbum</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/ela/gente/agatha-moreira-quer-fantasia-de-carnaval-comportada-18058546" title="Agatha Moreira ri do bumbum e quer sair discreta no carnaval"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/LFVj2Vc8e7MWhoDsaLFEryzlLKY=/filters:quality(10):strip_icc()/s2.glbimg.com/8aZQkQXgR-tPwq3ohhf7qwRWCok=/0x18:459x261/245x130/s.glbimg.com/en/ho/f/original/2015/11/16/agatha-moreira_foto-miguel-sa-2_1.jpg" alt="Agatha Moreira ri do bumbum e quer sair discreta no carnaval (Miguel SÃ¡/ DivulgaÃ§Ã£o)" title="Agatha Moreira ri do bumbum e quer sair discreta no carnaval (Miguel SÃ¡/ DivulgaÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/8aZQkQXgR-tPwq3ohhf7qwRWCok=/0x18:459x261/245x130/s.glbimg.com/en/ho/f/original/2015/11/16/agatha-moreira_foto-miguel-sa-2_1.jpg" data-url-smart_horizontal="01M6YjQOtFVjnb3FDPn-EKoL0u8=/90x56/smart/filters:strip_icc()/" data-url-smart="01M6YjQOtFVjnb3FDPn-EKoL0u8=/90x56/smart/filters:strip_icc()/" data-url-feature="01M6YjQOtFVjnb3FDPn-EKoL0u8=/90x56/smart/filters:strip_icc()/" data-url-tablet="GrYlZ8J9wFGdz2AWiPtvPBa5Gkg=/160x95/smart/filters:strip_icc()/" data-url-desktop="PmXb6M-tYSFo06WdHM5myqz8MEY=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Agatha Moreira ri do bumbum <br />e quer sair discreta no carnaval</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistamonet.globo.com/Listas/noticia/2015/11/celebs-internacionais-que-foram-acusadas-de-abandonar-e-nao-ajudar-seus-irmaos.html" title="Veja celebs que foram acusadas de &#39;abandonar&#39; seus irmÃ£os"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/SPw0Au2qeXepT8tYpF98dPK3Ehk=/filters:quality(10):strip_icc()/s2.glbimg.com/fKzZJ3ju_48hbJciLErazLB4yBw=/0x84:300x243/245x130/s.glbimg.com/jo/g1/f/original/2015/11/13/2015-11-12t044908z_1688658106_gf20000055802_rtrmadp_3_usa-entertainment.jpg" alt="Veja celebs que foram acusadas de &#39;abandonar&#39; seus irmÃ£os (REUTERS/Mario Anzuoni)" title="Veja celebs que foram acusadas de &#39;abandonar&#39; seus irmÃ£os (REUTERS/Mario Anzuoni)"
                    data-original-image="s2.glbimg.com/fKzZJ3ju_48hbJciLErazLB4yBw=/0x84:300x243/245x130/s.glbimg.com/jo/g1/f/original/2015/11/13/2015-11-12t044908z_1688658106_gf20000055802_rtrmadp_3_usa-entertainment.jpg" data-url-smart_horizontal="03LmIucdUrjxr_csxGplLz6sdtk=/90x56/smart/filters:strip_icc()/" data-url-smart="03LmIucdUrjxr_csxGplLz6sdtk=/90x56/smart/filters:strip_icc()/" data-url-feature="03LmIucdUrjxr_csxGplLz6sdtk=/90x56/smart/filters:strip_icc()/" data-url-tablet="D2H704DJ_T0km2jHoOsvoxeJ6M4=/160x95/smart/filters:strip_icc()/" data-url-desktop="57kYzSLpurNp3Gw4oFMHVpdWuFE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Veja celebs que foram acusadas de &#39;abandonar&#39; seus irmÃ£os</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://epoca.globo.com/colunas-e-blogs/bruno-astuto/noticia/2015/11/antonia-morais-comenta-foto-polemica-com-folha-de-maconha-nao-existe-falso-moralismo-na-minha-casa.html" title="Antonia Morais sobre folha de maconha: &#39;Sem falso moralismo&#39;
"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/KE9zFmzqVob3zOunxi8Ti_Ho2Fc=/filters:quality(10):strip_icc()/s2.glbimg.com/m9WwiZ-2A2418yaPzPmn1mbztgY=/112x196:1066x701/245x130/e.glbimg.com/og/ed/f/original/2015/11/13/antonia_1.jpg" alt="Antonia Morais sobre folha de maconha: &#39;Sem falso moralismo&#39;
 (DivulgaÃ§Ã£o)" title="Antonia Morais sobre folha de maconha: &#39;Sem falso moralismo&#39;
 (DivulgaÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/m9WwiZ-2A2418yaPzPmn1mbztgY=/112x196:1066x701/245x130/e.glbimg.com/og/ed/f/original/2015/11/13/antonia_1.jpg" data-url-smart_horizontal="ZoCCmXV6Yf9ysS1V5GocN85MHr4=/90x56/smart/filters:strip_icc()/" data-url-smart="ZoCCmXV6Yf9ysS1V5GocN85MHr4=/90x56/smart/filters:strip_icc()/" data-url-feature="ZoCCmXV6Yf9ysS1V5GocN85MHr4=/90x56/smart/filters:strip_icc()/" data-url-tablet="koREYBlLJ7pIAzuWFm5qDx26a28=/160x95/smart/filters:strip_icc()/" data-url-desktop="oSM4RjrXCKgNQOu9NajHkb5FmEA=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Antonia Morais sobre folha de maconha: &#39;Sem falso moralismo&#39;<br /></p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/totalmente-demais/">TOTALMENTE DEMAIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/11/eliza-ve-arthur-no-mercado-de-flores.html" title="Em &#39;Totalmente Demais&#39;, Eliza vÃª Arthur no mercado de flores"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/dMFa5ekLkD1TTnfpeyZ7YaT0ing=/filters:quality(10):strip_icc()/s2.glbimg.com/vbsKuP4wChY-t-n7Q_gxr9hi3B4=/104x75:645x362/245x130/s.glbimg.com/et/gs/f/original/2015/11/13/eliza.jpg" alt="Em &#39;Totalmente Demais&#39;, Eliza vÃª Arthur no mercado de flores (Artur Meninea / Gshow)" title="Em &#39;Totalmente Demais&#39;, Eliza vÃª Arthur no mercado de flores (Artur Meninea / Gshow)"
                    data-original-image="s2.glbimg.com/vbsKuP4wChY-t-n7Q_gxr9hi3B4=/104x75:645x362/245x130/s.glbimg.com/et/gs/f/original/2015/11/13/eliza.jpg" data-url-smart_horizontal="IgaNDKMI8UpPU3NaghZasy1d4bw=/90x56/smart/filters:strip_icc()/" data-url-smart="IgaNDKMI8UpPU3NaghZasy1d4bw=/90x56/smart/filters:strip_icc()/" data-url-feature="IgaNDKMI8UpPU3NaghZasy1d4bw=/90x56/smart/filters:strip_icc()/" data-url-tablet="cBkWPcuy87KTRZr94q0z6wwcduQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="JXO6uOzTa_phIE7g07Ec1E7n0L0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;Totalmente Demais&#39;, Eliza <br />vÃª Arthur no mercado de flores</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/11/uodson-coloca-roger-para-dancar-com-luciana-e-rola-climao.html" title="Em &#39;MalhaÃ§Ã£o&#39;, Uodson coloca Roger para danÃ§ar com Luciana
"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/GznkJ3u-wNJM0SmoRZg3vb2Awfo=/filters:quality(10):strip_icc()/s2.glbimg.com/Go2HnjtPvmLzct3y5cTWGVsPDzQ=/0x68:690x434/245x130/s.glbimg.com/et/gs/f/original/2015/11/12/roger-luciana_malhacao.jpg" alt="Em &#39;MalhaÃ§Ã£o&#39;, Uodson coloca Roger para danÃ§ar com Luciana
 (Evellyn Pacheco/Gshow)" title="Em &#39;MalhaÃ§Ã£o&#39;, Uodson coloca Roger para danÃ§ar com Luciana
 (Evellyn Pacheco/Gshow)"
                    data-original-image="s2.glbimg.com/Go2HnjtPvmLzct3y5cTWGVsPDzQ=/0x68:690x434/245x130/s.glbimg.com/et/gs/f/original/2015/11/12/roger-luciana_malhacao.jpg" data-url-smart_horizontal="GI7JWkjrEkyYKcJFmI6L1vio5eg=/90x56/smart/filters:strip_icc()/" data-url-smart="GI7JWkjrEkyYKcJFmI6L1vio5eg=/90x56/smart/filters:strip_icc()/" data-url-feature="GI7JWkjrEkyYKcJFmI6L1vio5eg=/90x56/smart/filters:strip_icc()/" data-url-tablet="qpVOa9qn2yM1KP22oqpd4w2AiFI=/160x95/smart/filters:strip_icc()/" data-url-desktop="-jjTJzF2VMDX-R2pjAyFhX9ExK0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;MalhaÃ§Ã£o&#39;, Uodson coloca Roger para danÃ§ar com Luciana<br /></p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/11/julia-rabello-planeja-engravidar-o-quanto-antes-e-aposta-em-marcos-veras-melhor-pai-do-mundo.html" title="JÃºlia Rabello planeja engravidar o quanto antes de Marcos Veras
"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/_7K6xUdYp6Z5_YiIgDdwfkV4iHw=/filters:quality(10):strip_icc()/s2.glbimg.com/WgmFGVHXzt4yRwt2z2gegexEgxw=/104x20:479x220/245x130/s.glbimg.com/et/gs/f/original/2015/11/12/marcosveras-juliarabello.jpg" alt="JÃºlia Rabello planeja engravidar o quanto antes de Marcos Veras
 (Jorge Bispo/ DivulgaÃ§Ã£o)" title="JÃºlia Rabello planeja engravidar o quanto antes de Marcos Veras
 (Jorge Bispo/ DivulgaÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/WgmFGVHXzt4yRwt2z2gegexEgxw=/104x20:479x220/245x130/s.glbimg.com/et/gs/f/original/2015/11/12/marcosveras-juliarabello.jpg" data-url-smart_horizontal="hDjiFUoZF9ysNDpYEHt8pclPA24=/90x56/smart/filters:strip_icc()/" data-url-smart="hDjiFUoZF9ysNDpYEHt8pclPA24=/90x56/smart/filters:strip_icc()/" data-url-feature="hDjiFUoZF9ysNDpYEHt8pclPA24=/90x56/smart/filters:strip_icc()/" data-url-tablet="CnrWQCMoYcZPFW32J-Xn-tbJGac=/160x95/smart/filters:strip_icc()/" data-url-desktop="ONTEyy8R2Y1iThUMyT7pXegtbdw=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>JÃºlia Rabello planeja engravidar o quanto antes de Marcos Veras<br /></p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/11/em-mae-de-dois-dani-monteiro-mostra-o-desafio-de-dar-frutinhas-para-o-filho-ele-cospe-tudo.html" title="Dani Monteiro mostra o desafio de dar frutinhas para o filho"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/CRE2XHpBYjYhDj5-Zo31i_9o3p8=/filters:quality(10):strip_icc()/s2.glbimg.com/WTfFGk3RUHjPP_1i_y-W4B8gwsA=/0x44:690x411/245x130/s.glbimg.com/et/gs/f/original/2015/11/13/dani.jpg" alt="Dani Monteiro mostra o desafio de dar frutinhas para o filho (InÃ¡cio Moraes/Gshow)" title="Dani Monteiro mostra o desafio de dar frutinhas para o filho (InÃ¡cio Moraes/Gshow)"
                    data-original-image="s2.glbimg.com/WTfFGk3RUHjPP_1i_y-W4B8gwsA=/0x44:690x411/245x130/s.glbimg.com/et/gs/f/original/2015/11/13/dani.jpg" data-url-smart_horizontal="5X9DGgS56HOf5mGu_vkwzNAc6k0=/90x56/smart/filters:strip_icc()/" data-url-smart="5X9DGgS56HOf5mGu_vkwzNAc6k0=/90x56/smart/filters:strip_icc()/" data-url-feature="5X9DGgS56HOf5mGu_vkwzNAc6k0=/90x56/smart/filters:strip_icc()/" data-url-tablet="Budgg39GIw_5BNpsHjdSzJhID2A=/160x95/smart/filters:strip_icc()/" data-url-desktop="RHZ25DtZz6wIoMvwfojqXqwq8Cw=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Dani Monteiro mostra o desafio de dar frutinhas para o filho</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/cultura/musica/a-solidariedade-em-torno-do-eagles-of-death-metal-18060147" title="FÃ£s do grupo que fazia show no Bataclan ajudam famÃ­lia de membro da equipe morto
 (Kevin Winter/Getty Images/AFP)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/QhJTXLXJt2WudsxQwt1cw_yHWCk=/filters:quality(10):strip_icc()/s2.glbimg.com/9lzv1eEUcV-NiW3xz6xDIPI5PxM=/52x36:533x327/215x130/s.glbimg.com/jo/g1/f/original/2015/11/14/eagles-death-metal.jpg"
                data-original-image="s2.glbimg.com/9lzv1eEUcV-NiW3xz6xDIPI5PxM=/52x36:533x327/215x130/s.glbimg.com/jo/g1/f/original/2015/11/14/eagles-death-metal.jpg" data-url-smart_horizontal="ueLlT5AQADmaWKajSPyVEBZ5940=/90x56/smart/filters:strip_icc()/" data-url-smart="ueLlT5AQADmaWKajSPyVEBZ5940=/90x56/smart/filters:strip_icc()/" data-url-feature="ueLlT5AQADmaWKajSPyVEBZ5940=/90x56/smart/filters:strip_icc()/" data-url-tablet="vYYu4f6KlT5yxXtifecDuMFIRN0=/120x80/smart/filters:strip_icc()/" data-url-desktop="eFZrw063OvnUi2j2kzbQxIsKo5o=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">FÃ£s do grupo que fazia show no Bataclan ajudam famÃ­lia de membro da equipe morto<br /></div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4574164/" title="Especial Roberto Carlos de 2010 teve Paula Fernandes; relembre show na Ã­ntegra (TV Globo)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/J8YsUPjCCvltT_1VegXZNM_eHlQ=/filters:quality(10):strip_icc()/s2.glbimg.com/Pcs0LQ4yS9UXJ-EwGzd_wxOnfy0=/0x0:766x464/215x130/s.glbimg.com/en/ho/f/original/2015/11/16/paulafernandeseroberto.jpg"
                data-original-image="s2.glbimg.com/Pcs0LQ4yS9UXJ-EwGzd_wxOnfy0=/0x0:766x464/215x130/s.glbimg.com/en/ho/f/original/2015/11/16/paulafernandeseroberto.jpg" data-url-smart_horizontal="bAHmT4YfBhcy1h-1bzCLwUyjiFc=/90x56/smart/filters:strip_icc()/" data-url-smart="bAHmT4YfBhcy1h-1bzCLwUyjiFc=/90x56/smart/filters:strip_icc()/" data-url-feature="bAHmT4YfBhcy1h-1bzCLwUyjiFc=/90x56/smart/filters:strip_icc()/" data-url-tablet="iQhZUNPSeyAHkCCJlv7Qh7RU6Pk=/120x80/smart/filters:strip_icc()/" data-url-desktop="r1JYIXp-sixShhyIEh39hFTpo7A=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Especial Roberto Carlos de 2010 teve Paula Fernandes; relembre show na Ã­ntegra</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/Popquem/noticia/2015/11/joe-jonas-exibe-mechas-azuis-nos-cabelos-antes-de-apresentacao-com-novo-grupo.html" title="Em NY, Joe Jonas exibe visual repaginado junto com seu o novo grupo, o Dnce; fotos (Getty Images)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/aQNwUNhayhXC4JTHhVNYuGotggo=/filters:quality(10):strip_icc()/s2.glbimg.com/69iF5tIcEByQ4h5A_Bz5NMZAiik=/52x25:483x285/215x130/s.glbimg.com/en/ho/f/original/2015/11/16/joe3.jpg"
                data-original-image="s2.glbimg.com/69iF5tIcEByQ4h5A_Bz5NMZAiik=/52x25:483x285/215x130/s.glbimg.com/en/ho/f/original/2015/11/16/joe3.jpg" data-url-smart_horizontal="sV4VcMFJXxHNqu7WxXfB3eJta1U=/90x56/smart/filters:strip_icc()/" data-url-smart="sV4VcMFJXxHNqu7WxXfB3eJta1U=/90x56/smart/filters:strip_icc()/" data-url-feature="sV4VcMFJXxHNqu7WxXfB3eJta1U=/90x56/smart/filters:strip_icc()/" data-url-tablet="NroCV5Si5VPIPgxy_XZsV1PJGmU=/120x80/smart/filters:strip_icc()/" data-url-desktop="vvbKkmGmJLs7VxAmbj5bSH7UJic=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Em NY, Joe Jonas exibe visual repaginado junto com seu o novo grupo, o Dnce; fotos</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/noticia/2015/11/rapaz-montou-salao-para-namorada-antes-de-mata-la-no-abc-diz-familia.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">IrmÃ£o de jovens mortas por ex pediu para morrer ao ver tiros no rosto</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/11/video-mostra-vendedor-sendo-espancado-ate-morte-em-ipanema.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">VÃ­deo mostra vendedor sendo espancado atÃ© a morte em Ipanema</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://oglobo.globo.com/brasil/recifes-de-abrolhos-ameacados-pela-lama-de-mariana-18058373" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Lama de Minas Gerais ameaÃ§a os recifes de corais de Abrolhos, na BA<br /></span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/mundo/noticia/2015/11/filho-de-5-anos-de-chilena-que-morreu-em-paris-conseguiu-fugir.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Filho de 5 anos de chilena que morreu em atentado em Paris conseguiu fugir</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/noticias/mundo/hackers-do-grupo-anonymous-ameacam-estado-islamico-18060115.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Hackers do grupo Anonymous ameaÃ§am o Estado IslÃ¢mico em vÃ­deo</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/ronda-agradece-apoio-de-fas-se-diz-bem-e-jura-vou-dar-um-tempo-mas-vou-voltar.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ronda agradece apoio de fÃ£s, se diz bem e jura: &#39;Vou dar um tempo, mas vou voltar&#39;</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/noticia/2015/11/apos-derrota-para-holly-ronda-tera-que-passar-por-plastica-nos-labios.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">ApÃ³s derrota para Holly, Ronda terÃ¡ que passar por plÃ¡stica nos lÃ¡bios</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/cruzeiro/noticia/2015/11/marcelo-moreno-assiste-jogo-no-mineirao-no-meio-da-torcida.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Marcelo Moreno vai ao MineirÃ£o e vÃª Cruzeiro x Sport no meio da torcida</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/11/messi-bota-preco-para-trocar-o-barca-inglaterra-r-35-milhoes-por-semana.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Messi bota preÃ§o para trocar o BarÃ§a pela Inglaterra: R$ 3,5 mi por semana</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/ronda-previu-como-seria-derrota-para-holm-em-programa-de-tv.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ronda previu como seria derrota para Holm em programa de TV</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/anitta-aposta-em-look-com-transparencia-e-exibe-pernocas-para-show.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Anitta aposta em look com transparÃªncia e exibe pernocas em show</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/famosos/com-maio-engana-mamae-giovanna-antonelli-aproveita-dia-de-praia-ao-lado-do-marido-das-filhas-gemeas-18058823.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Com maiÃ´ engana-mamÃ£e, Antonelli curte praia com marido e as gÃªmeas</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/cuidado-kardashians-modelo-faz-curvas-explodirem-em-vestido-justissimo.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Cuidado, Kardashians! Modelo faz curvas &#39;explodirem&#39; em macacÃ£o</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/e-namoro-larissa-manoela-passeia-de-maos-dadas-com-joao-guilherme-filho-do-cantor-leonardo.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Larissa Manoela passeia de mÃ£os dadas com JoÃ£o Guilherme, filho do cantor Leonardo</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/biquini/noticia/2015/11/ex-bbb-amanda-posa-de-biquini-e-posta-foto-em-rede-social.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ex-BBB Amanda posa de biquÃ­ni e posta foto em rede social: &#39;Que corpo!&#39;, elogia fÃ£</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="A Regra do Jogo" href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A Regra do Jogo</a></li><li class="diretorio-second-level"><a title="AlÃ©m do Tempo" href="http://gshow.globo.com/novelas/alem-do-tempo/">AlÃ©m do Tempo</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="Bastidores" href="http://gshow.globo.com/Bastidores/">Bastidores</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Caminho das Ãndias" href="http://gshow.globo.com/novelas/caminho-das-indias/">Caminho das Ãndias</a></li><li class="diretorio-second-level"><a title="Como Fazer" href="http://gshow.globo.com/como-fazer/">Como Fazer</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Ã de Casa" href="http://gshow.globo.com/programas/e-de-casa/">Ã de Casa</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Escolinha do Professor Raimundo" href="http://gshow.globo.com/programas/escolinha-do-professor-raimundo/">Escolinha do Professor Raimundo</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estilo" href="http://gshow.globo.com/Estilo/">Estilo</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="LigaÃ§Ãµes Perigosas" href="http://gshow.globo.com/series/ligacoes-perigosas/">LigaÃ§Ãµes Perigosas</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Mister Brau" href="http://gshow.globo.com/series/mister-brau/2015/">Mister Brau</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://gshow.globo.com/Musica/">MÃºsica</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://gshow.globo.com/series/pe-na-cova/2015/">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas da Ana Maria" href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">Receitas da Ana Maria</a></li><li class="diretorio-second-level"><a title="SÃ©ries Originais" href="http://gshow.globo.com/webseries/">SÃ©ries Originais</a></li><li class="diretorio-second-level"><a title="SessÃ£o ComÃ©dia" href="http://gshow.globo.com/sessao-comedia/videos/">SessÃ£o ComÃ©dia</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://gshow.globo.com/realities/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Totalmente Demais" href="http://gshow.globo.com/novelas/totalmente-demais/">Totalmente Demais</a></li><li class="diretorio-second-level"><a title="Tomara que Caia" href="http://gshow.globo.com/programas/tomara-que-caia/">Tomara que Caia</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="Zorra" href="http://gshow.globo.com/programas/zorra/">Zorra</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-play diretorio-sem-quebra "><a title="globo play" href="http://globoplay.globo.com/">globo play</a></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade e seguranÃ§a</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/c9570221d3da.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 6000);}
</script><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 0};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 16/11/2015 16:39:45 -->
