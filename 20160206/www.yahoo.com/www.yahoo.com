<!DOCTYPE html>
<html id="atomic" lang="en-US" class="atomic my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr desktop Desktop bkt201">
<head>
    <title>Yahoo</title><meta http-equiv="x-dns-prefetch-control" content="on"><link rel="dns-prefetch" href="//s.yimg.com"><link rel="preconnect" href="//s.yimg.com"><link rel="dns-prefetch" href="//y.analytics.yahoo.com"><link rel="preconnect" href="//y.analytics.yahoo.com"><link rel="dns-prefetch" href="//geo.query.yahoo.com"><link rel="preconnect" href="//geo.query.yahoo.com"><link rel="dns-prefetch" href="//csc.beap.bc.yahoo.com"><link rel="preconnect" href="//csc.beap.bc.yahoo.com"><link rel="dns-prefetch" href="//geo.yahoo.com"><link rel="preconnect" href="//geo.yahoo.com"><link rel="dns-prefetch" href="//comet.yahoo.com"><link rel="preconnect" href="//comet.yahoo.com">    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="description" content="News, email and search are just the beginning. Discover more every day. Find your yodel.">
    <meta name="keywords" content="yahoo, yahoo home page, yahoo homepage, yahoo search, yahoo mail, yahoo messenger, yahoo games, news, finance, sport, entertainment">
    <meta property="og:title" content="Yahoo" />
    <meta property="og:type" content='website' />
    <meta property="og:url" content="http://www.yahoo.com" />
    <meta property="og:description" content="News, email and search are just the beginning. Discover more every day. Find your yodel."/>
    <meta property="og:image" content="https://s.yimg.com/dh/ap/default/130909/y_200_a.png"/>
    <meta property="og:site_name" content="Yahoo" />
    <meta property="fb:app_id" content="90376669494" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="icon" sizes="any" mask href="/sy/os/mit/media/p/common/images/favicon_new-7483e38.svg">
<meta name="theme-color" content="#400090">
    <link rel="shortcut icon" href="/sy/rz/l/favicon.ico" />
    <link rel="canonical" href="https://www.yahoo.com/" />        <link href="/sy/os/fp/atomic-css.d1bbf0c4.css" rel="stylesheet" type="text/css">
            
    
    
    
    <!-- streaming unlocked -->

    <!-- MapleTop -->
    
    
<link rel="stylesheet" type="text/css" href="/sy/zz/combo?nn/lib/metro/g/myy/advance_base_rc4_0.0.31.css&nn/lib/metro/g/myy/font_rc4_spdy_0.0.31.css&nn/lib/metro/g/myy/yahoo20_grid_0.0.96.css&nn/lib/metro/g/myy/video_styles_0.0.23.css&nn/lib/metro/g/myy/advance_color_0.0.7.css&nn/lib/metro/g/theme/viewer_modal_0.0.29.css&nn/lib/metro/g/theme/yglyphs-legacy_0.0.5.css&nn/lib/metro/g/sda/fp_sda_0.0.4.css&nn/lib/metro/g/sda/sda_advance_0.0.8.css&nn/lib/metro/g/fpfooter/advance_0.0.4.css&/os/stencil/3.1.0/styles-ltr.css&/os/yc/css/bundle.c60a6d54.css" />
    <link rel="search" type="application/opensearchdescription+xml" href="https://search.yahoo.com/opensearch.xml" title="Yahoo Search" />
    
    <script>
    var myYahoostartTime = new Date(),
        afPerfHeadStart=new Date().getTime(),
        ie;

    
    document.documentElement.className += ' JsEnabled jsenabled';</script>
    

<style>.breakingnews.gradient-1 {
    background: #ff7617; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #ff7617 0%, #ff9b00 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#ff7617), color-stop(65%,#ff9b00)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* IE10+ */
    background: linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7617', endColorstr='#ff9b00',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}

.breakingnews.gradient-2 {
    background: #e91857; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #e91857 0%, #ff353c 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#e91857), color-stop(65%,#ff353c)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* IE10+ */
    background: linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e91857', endColorstr='#ff353c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}</style>
<style>.js-stream-adfdb-reason {
    display: none;
}
.js-stream-adfdb-options .js-stream-adfdb-other-selected + .js-stream-adfdb-reason {
    display: block;
}

.stream-collapse {
    max-height: 0;
    -webkit-transition: max-height 0.3s ease;
    -moz-transition: max-height 0.3s ease;
    -o-transition: max-height 0.3s ease;
    transition: max-height 0.3s ease;
}

.RevealNested-on .ActionDislike {
    display: none;
}

.streamv2 .stream-share-open .js-stream-share-panel {
    height: auto !important;
    opacity: 1 !important;
}

/**
* Tooltips
*/
.js-stream-actions a:hover .ActionTooltip.hide,
.js-stream-actions .ActionTooltip,
.js-stream-actions a:active .ActionTooltip,
.js-stream-actions a:focus .ActionTooltip {
    clip: rect(1px 1px 1px 1px);
    clip: rect(1px,1px,1px,1px);
    height: 1px;
    width: 1px;
    overflow: hidden;
    line-height: 1.4;
    white-space: nowrap
}

.js-stream-actions a:hover .ActionTooltip {
    clip: rect(auto auto auto auto);
    clip: auto;
    height: auto;
    width: auto;
    overflow: visible;
    -webkit-transform: translateX(-50%) !important;
    -ms-transform: translateX(-50%) !important;
    transform: translateX(-50%) !important;
    *min-width: 80px;
    *margin-right: -48px;
    width: 136px;
    text-align: center;
}

.streamv2 .js-stream-dense .strm-left {
    max-width: 190px;
    width: 29%;
}

.streamv2 .js-stream-sparse .strm-left {
    width: 33%;
    max-width: 230px;
}
.streamv2 .js-stream-sparse .strm-right {
    width: 57%;
}
.streamv2 .js-stream-sparse .strm-full {
    width: 90%;
}

.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-left {
    width: 62.825%;
    max-width: 441px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-left {
    width: 72%;
    max-width: 508px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-right,
.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-right {
    width: auto;
    max-width: none;
}


.streamv2 .strm-right-menu-roundup .strm-right .js-stream-content-link:before {
    content: '';
    vertical-align: middle;
    height: 100%;
    display: inline-block;
}

.streamv2 .strm-chevron {
    display: none;
}

.streamv2 .strm-right-menu-roundup .js-stream-content-link.selected .strm-chevron,
.streamv2 .strm-right-menu-roundup .js-stream-content-link:hover .strm-chevron {
    display: block;
}

.streamv2 .rounded-img {
    border-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-main-img .rounded-img {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.streamv2 .js-stream-sparse .storyline-img-0 {
    border-bottom-left-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-img-1 {
    border-bottom-right-radius: 3px;
}

.ua-ff#atomic .strm-headline-label {
    margin-bottom: 4px;
}
/* cluster image gradient transparent to dark overlay */
.streamv2 .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 32%, rgba(0,0,0,0.65) 97%, rgba(0,0,0,0.65) 98%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(32%,rgba(0,0,0,0)), color-stop(97%,rgba(0,0,0,0.65)), color-stop(98%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

/* cluster image gradient transparent to dark overlay */
.streamv2 .js-stream-roundup .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 52%, rgba(0,0,0,0.85) 107%, rgba(0,0,0,0.85) 78%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(52%,rgba(0,0,0,0)), color-stop(107%,rgba(0,0,0,0.85)), color-stop(78%,rgba(0,0,0,0.85))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

.ua-ie10 .streamv2 .js-stream-related-content ul,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip > ul {
    margin-right: -7px !important;
}

.ua-ie10 .streamv2 .js-stream-related-content li,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip .W\(25\%\) {
    width: 24.5% !important;
}

.streamv2 .js-stream-side-buttons li:nth-child(1) .ActionTooltip {
    top: -1px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(1) .ActionTooltip {
    top: 0px;
}

.streamv2 .js-stream-side-buttons li:nth-child(2) .ActionTooltip {
    top: 22px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(2) .ActionTooltip {
    top: 40px;
}
.streamv2 .js-stream-side-buttons li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons li:nth-child(3) .js-stream-share-panel {
    top: 45px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .js-stream-share-panel {
    top: 64px;
}
.streamv2 .js-stream-side-buttons li:nth-child(4) .js-stream-share-panel {
    top: 86px;
}

.streamv2 .js-stream-side-buttons .ActionComments {
    margin-top: 6px;
}
.streamv2 .js-stream-side-buttons.has-comments .ActionComments {
    margin-top: 0px;
}
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\$c_icon\),
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\#96989f\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\$c_icon\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\#96989f\) {
    color: inherit !important;
}
.streamv2 .js-stream-side-buttons .ActionComments .ActionTooltip {
    margin-top: 15px;
}

.js-stream-comment-counter-update {
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
}

/* Only set opacity to 0 for animation when js is enabled and css3 supported */
.JsEnabled .streamv2 .js-stream-comment-hidden:nth-of-type(1n) {
    opacity: 0;
}

.JsEnabled .streamv2 .animated {
    -webkit-animation-duration: 1.5s;
    animation-duration: 1.5s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
}

@-webkit-keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

@keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

.JsEnabled .streamv2 .fadeOut {
    -webkit-animation-name: fadeOut;
    animation-name: fadeOut;
}

@-webkit-keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@-webkit-keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

@keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

.JsEnabled .streamv2 .fadeIn {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
}


.JsEnabled .streamv2 .js-stream-roundup-filmstrip .fadeIn {
    -webkit-animation-name: fadeInNtk;
    animation-name: fadeInNtk;
}

.streamv2 .js-stream-comment .js-stream-cmnt-up:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-up.selected {
    color: #1AC567 !important;
}
.streamv2 .js-stream-comment .js-stream-cmnt-down:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-down.selected,
.streamv2 .js-stream-comment .js-stream-cmnt-flag:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-flag.selected {
    color: #F0162F !important;
}

/* to show the dislike ad tooltip */
#atomic .Collapse-opened .js-stream-related-item-ad {
    overflow: visible;
}
</style>
 

<style>.js-activitylist-item .orb {
    background-position: 0 0;
    width: 80px;
    height: 80px;
}

.js-activitylist-item .hexagon {
    background-position: -81px 0;
    width: 80px;
    height: 90px;
}

.js-activitylist-item .ribbon {
    background-position: -162px 0;
    width: 91px;
    height: 31px;
}
</style>
 

<style>#uh-search .yui3-aclist {
    width: inherit !important;
}
#uh-search .yui3-aclist-content {
    background-color: #fff;
    text-align: left;
    border: 1px solid #ccc;
    margin-top: 1px;
    border-top: 0;
}
#uh-search .yui3-aclist-list {
    margin: 0;
    line-height: 1.1;
}
#uh-search .yui3-aclist-item {
    padding: 5px 0 6px 0;
    font-size: 18px;
    font-weight: bold;
}
#uh-search .yui3-aclist-item:hover,
#uh-search .yui3-aclist-item-active {
    background-color: #c6d7ff;
}
#uh-search .yui3-aclist-item {
    padding-left: 10px;
    padding-right: 10px;
}
#uh-search .yui3-highlight {
    font-weight: 200;
}
#uh-search-box::-ms-clear {
    display: none;
}


/* Instant Search Styles */

#uh-search .yui3-aclist-item {
    line-height: 0.9;
}
#InstantSearchMask {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s linear 0.15s, opacity 0.15s linear;
}
.iSearch-display .Col1,
.iSearch-display .Col2,
.iSearch-display .Col3 {
    height: 0;
    overflow: hidden;
}
.iSearch-display #InstantSearch {
    visibility: visible;
}
.iSearch-mask #InstantSearchMask {
    visibility: visible;
    opacity: 1;
    transition: visibility 0s linear, opacity 0.15s linear;
}
.iSearch-loading.iSearch-mask #InstantSearchMask,
.iSearch-loading #InstantSearchMask {
    visibility: visible;
    opacity: .7;
}


/* Firefox 28 and below fix */
#uh-search-box,
#uh-ghost-box,
.instant-filters,
.instant-results {
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
/* ----------------------- */
</style>
 

<style>@charset "UTF-8";.Lb-Close-Icon,.icon-share-close,.icons-arrow,.icons-slideshow-icon{background-color:transparent;background-image:url(/sy/os/publish-images/news/2014-04-23/65fcff60-cb23-11e3-bead-55c0602d5659_icons-sb930b067ee.png);background-repeat:no-repeat}#Share .icon-share-close{width:30px;height:25px;background-size:cover;background-position:8px -72px}.Hl-Viewer{background:#fff}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay{position:absolute;color:#fff;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodâ¦EiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDAwMDAiIHN0b3Atb3BhY2l0eT0iMC45NSIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==);background:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,rgba(0,0,0,0)),color-stop(100%,rgba(0,0,0,.95)));background:-moz-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:-webkit-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95))}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline{text-shadow:0 1px 0 #000}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline-Box{position:absolute}.Hl-Viewer .Content-Body{color:#000;font-weight:300;letter-spacing:.3px;word-wrap:break-word;word-break:break-word}.Hl-Viewer .Content-Body>p{margin-bottom:1.45em;margin-top:1.45em}.Hl-Viewer .Content-Body h1,.Hl-Viewer .Content-Body h2,.Hl-Viewer .Content-Body h3,.Hl-Viewer .Content-Body h4,.Hl-Viewer .Content-Body h5,.Hl-Viewer .Content-Body h6{font-weight:400}.Hl-Viewer .Content-Body h1{font-size:18px;font-size:1.8rem}.Hl-Viewer .Content-Body h2{font-size:17px;font-size:1.7rem}.Hl-Viewer .Content-Body h3{font-size:16px;font-size:1.6rem}.Hl-Viewer .Content-Body h4{font-size:15px;font-size:1.5rem}.Hl-Viewer .Content-Body h5{font-size:14px;font-size:1.4rem}.Hl-Viewer .Content-Body h6{font-size:13px;font-size:1.3rem}.Hl-Viewer .icons-slideshow-icon{left:5%;top:5%;height:46px;width:47px;background-size:cover}.Hl-Viewer blockquote{padding:5px 10px;quotes:"\201C" "\201C" "\201C" "\201C";line-height:18px}.Hl-Viewer blockquote:before{color:#ccc;content:open-quote;font-size:3em;line-height:.1em;vertical-align:-.4em}.Hl-Viewer blockquote p{display:inline;color:#747474;font-weight:400}.Hl-Viewer blockquote p:after{content:"\A";white-space:pre}.Hl-Viewer .Credit,.Hl-Viewer .Provider{letter-spacing:.5px}.JsEnabled .ImageLoader-Delayed,.JsEnabled .ImageLoader-Loaded{-moz-transition-duration:.2s;-o-transition-duration:.2s;-webkit-transition-duration:.2s;transition-duration:.2s;-moz-transition-property:opacity;-o-transition-property:opacity;-webkit-transition-property:opacity;transition-property:opacity;-moz-transition-timing-function:ease-out;-o-transition-timing-function:ease-out;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}#Stencil .Bdrs-100{border-top-left-radius:100px;border-top-right-radius:100px;border-bottom-left-radius:100px;border-bottom-right-radius:100px}.Slideshow-Lightbox .lb-meta-content{padding-bottom:30px;background:#fff}.Slideshow-Lightbox .lb-meta-content .lb-meta-txt-container-full,.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-short{display:none}.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-full{display:block}.Slideshow-Lightbox .hide-meta .lb-meta-content{display:none}.Slideshow-Lightbox .Lb-Close-Icon{background-size:cover;width:47px;height:47px;z-index:202;background-position:0 -46px}.Reader-open .BrandBar,.Reader-open .rmp-TDYDBM{display:none}.Content-Body .Editorial-Left{float:left;margin-right:30px}.Content-Body .Editorial-Right{float:right;margin-left:30px}.Content-Body .Editorial-Left,.Content-Body .Editorial-Right{margin-top:0}#hl-viewer .Content-Col,.MagOn .Content-Col{margin-left:300px;margin-right:300px;min-height:100px}.HideCentralColumn .Content-Col{margin-left:150px;margin-right:150px}#hl-viewer{min-height:500px}.ShareBtns a:hover{opacity:.3;filter:alpha(opacity=33)}.modal-actions{bottom:3px}.modal-actions .ShareBtns a:hover{opacity:1;filter:alpha(opacity=100)}#hl-viewer:focus{outline:0}#hl-viewer .Timestamp{color:#abaeb7}#hl-viewer .Viewer-Close-Btn{position:fixed;top:10px;width:28px;height:28px;color:#fff;background-color:#2D1152;font-size:16px;margin-left:11px;z-index:5}#hl-viewer .ShareBtns .Share-Btn{border-radius:50px;width:17px;height:17px;line-height:1.3}#hl-viewer .ShareBtns .Share-Btn:hover{text-decoration:none}#hl-viewer .slideshow-carousel{background:#212124;padding-bottom:66%}#hl-viewer .lb-meta-content{border-bottom:1px solid #e8e8e8}#hl-viewer .lb-meta-caption-container{-webkit-font-smoothing:antialiased}#hl-viewer .lb-meta-txt-container.caption-scroll{max-height:4.3em;overflow-y:auto}#hl-viewer .viewer-wrapper{background:#fff;margin:0 0 30px 58px;min-width:980px}#hl-viewer .viewer-wrapper.mega-modal{min-height:750px}#hl-viewer .content-modal,#hl-viewer .js-viewer-slot-readMore{display:inline-block;width:640px;margin-right:29px;vertical-align:top}#hl-viewer .js-slider-item.first .content-modal{padding-top:0}#hl-viewer .Content-Body{padding-bottom:50px;border-bottom:1px solid #c8c8c8}#hl-viewer .Content-Body i{font-style:italic}#hl-viewer .js-slider-item.last .Content-Body{padding-bottom:0;border-bottom:none}#hl-viewer .Content-Body p:last-child{margin-bottom:0!important}#hl-viewer .js-slider-item.multirightrail .Content-Body.multirightrail{border-bottom:none}#hl-viewer .js-slider-item.multirightrail .content-modal{padding-top:0}#hl-viewer .js-slider-item.multirightrail{border-top:1px solid #B2B2B2;padding-top:50px}#hl-viewer .js-slider-item.multirightrail.first{padding-top:0;border-top:none}#hl-viewer .modal-aside.single{position:absolute;right:0;float:none;width:300px}#hl-viewer .modal-aside{display:inline-block;width:300px;position:relative;vertical-align:top;z-index:1}#hl-viewer .slideshow-carousel .main-col{padding-top:15px}#hl-viewer .index-count{color:#878c91;position:absolute;left:12px;bottom:7px;-webkit-font-smoothing:antialiased}#hl-viewer .js-lb-next,#hl-viewer .js-lb-prev{top:50%;z-index:3;width:28px;color:#878c91;font-size:30px;margin-top:-17px}#hl-viewer .js-lb-next:hover,#hl-viewer .js-lb-prev:hover{color:#fff}#hl-viewer .js-lb-next.Disabled,#hl-viewer .js-lb-prev.Disabled{display:none}#hl-viewer .js-lb-next:focus,#hl-viewer .js-lb-prev:focus{outline:0}#hl-viewer .slideshowv2 .slv2-carousel{padding-bottom:78%}#hl-viewer .slideshowv2 .slv2-carousel .main-col{padding-top:20px;padding-bottom:60px}#hl-viewer .slideshowv2 .slv2-carousel .js-thm-sl-icon{color:#fff;font-size:25px;top:12px;display:none}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-slider-ct{background-color:rgba(33,33,35,.5);border-radius:10px}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-car-mask{width:300px;opacity:1}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .js-thm-sl-icon{display:inline-block}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-pic{width:52px;height:52px}#hl-viewer .slideshowv2 .js-lb-next,#hl-viewer .slideshowv2 .js-lb-prev{opacity:.8;background:#444;color:#fff;width:45px;height:45px;padding:0;border-radius:3px}#hl-viewer .slideshowv2 .js-lb-next:hover,#hl-viewer .slideshowv2 .js-lb-prev:hover{background:#333;opacity:1}#hl-viewer .slideshowv2 .index-count{bottom:inherit;left:inherit;color:#000;position:relative;font-size:19px;font-size:1.9rem;font-weight:500;margin-bottom:8px}#hl-viewer .slideshowv2 .slide-title{font-weight:700;margin-bottom:4px}#hl-viewer .slideshowv2 .lb-meta-txt-container a:hover,#hl-viewer .slideshowv2 .lb-meta-txt-container a:link,#hl-viewer .slideshowv2 .lb-meta-txt-container a:visited{color:#188fff}#hl-viewer .slideshowv2 .sl-thumbnails{bottom:10px}#hl-viewer .slideshowv2 .sl-slider-ct{width:350px}#hl-viewer .slideshowv2 .sl-car-mask{width:150px;opacity:.5;transition:height .2s}#hl-viewer .slideshowv2 .sl-carousel .Selected .sl-pic{border:2px solid #fff}#hl-viewer .slideshowv2 .sl-carousel .sl-pic{width:22px;height:22px;border:2px solid transparent}#hl-viewer .Headline{line-height:1.2;margin-bottom:12px;font-size:30px;font-size:3rem}#hl-viewer .Headline-Box{margin-bottom:10px}#hl-viewer .Off-Network{border:1px solid #188FFF;padding:8px 20px;color:#188FFF!important;font-weight:700;text-decoration:none}#hl-viewer .Off-Network:hover{color:#fff!important;background:#0078ff;border-color:#0078ff}#hl-viewer .Content-Body p,#hl-viewer .remaining-body p{margin-bottom:1.1em;margin-top:0}#hl-viewer .Content-Body p p,#hl-viewer .remaining-body p p{margin:0}#hl-viewer img.Mb-20+p{margin-top:0}#hl-viewer .Content-Body,#hl-viewer .remaining-body{font-weight:400;font-size:15px;font-size:1.55rem;line-height:1.5;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .Content-Body #photo_copyright,#hl-viewer .Content-Body .caption,#hl-viewer .Content-Body .credit,#hl-viewer .Content-Body .source,#hl-viewer .remaining-body #photo_copyright,#hl-viewer .remaining-body .caption,#hl-viewer .remaining-body .credit,#hl-viewer .remaining-body .source{font-size:11px;font-size:1.1rem;color:#b2b2b2}#hl-viewer h2,#hl-viewer h3{font-size:22px;font-size:2.2rem;margin-bottom:.6em}#hl-viewer h4,#hl-viewer h5,#hl-viewer h6{font-size:19px;font-size:1.9rem;margin-bottom:.6em}#hl-viewer .Embed-Img{margin-bottom:20px}#hl-viewer .image{margin-bottom:20px;display:block;line-height:1}#hl-viewer .image .Embed-Img{margin-bottom:0}#hl-viewer .sidekick.fp h4{font-size:13px;font-size:1.3rem;margin-bottom:0}#hl-viewer .twitter-tweet-rendered{margin:10px auto}#hl-viewer #photo_copyright{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .alignright{float:right;margin:0 0 10px 10px}#hl-viewer .alignleft{float:left;margin:0 10px 10px 0}#hl-viewer .pmc-related-type{margin-right:10px}#hl-viewer blockquote{line-height:1.6}#hl-viewer p:empty{display:none}#hl-viewer .Ad-Viewer blockquote p,#hl-viewer .Hl-Viewer blockquote p{display:block}#hl-viewer .hl-ad-LREC{height:250px}#hl-viewer .source{font-size:14px;font-size:1.4rem}#hl-viewer .attribution{font-size:12px}#hl-viewer .first .Fixed-Header{top:-100px}#hl-viewer .Fixed-Header{transition:opacity .8s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .8s ease-in-out,top .6s ease-in-out;position:fixed;top:0;width:600px;padding:14px 0 0}#hl-viewer .Fixed-Header .source{max-width:540px}#hl-viewer .Fixed-Header .modal-actions{top:11px;left:540px;width:100px!important}#hl-viewer .Fixed-Header-Wrap{position:fixed;top:-100px;width:100%;z-index:4;height:48px;background-color:#fff;margin-left:-57px;border-bottom:1px solid #e5e5e5;transition:opacity .6s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .6s ease-in-out,top .6s ease-in-out;opacity:0;box-shadow:0 2px 4px -1px rgba(0,9,30,.1)}#hl-viewer .fadein{opacity:1}#hl-viewer .backfill-video-ads{width:300px;height:169px}#hl-viewer .backfill-video-ads h3{font-size:16px;font-size:1.6rem}#hl-viewer .backfill-video-ads h4{font-size:15px;font-size:1.5rem}#hl-viewer .backfill-video-ads .yvp-setting-btn{display:none}#hl-viewer .SidekickTV .list-view-item{display:inline-block;zoom:1;letter-spacing:normal;word-spacing:normal;text-rendering:auto;vertical-align:top}#hl-viewer .SidekickTV .SidekickTVArrow{border-bottom:40px solid transparent;border-left:40px solid}#hl-viewer .SidekickTV .ad-sponsored{color:#959595;font-size:11px;margin-right:0;padding-right:0}#hl-viewer .continue_reading .arraw,#hl-viewer .js-viewer-view-article .arraw{border-left:5px solid transparent;border-right:5px solid transparent;border-top:6px solid #000;vertical-align:middle}#hl-viewer .continue_reading:hover{color:#188FFF}#hl-viewer .continue_reading:hover .arraw{border-top-color:#188FFF}#hl-viewer .Mt-neg-40{margin-top:-40px}#hl-viewer .Mt-neg-30{margin-top:-30px}#hl-viewer .modal-sidekick-following .sidekick.fp{margin-top:-20px}#hl-viewer figure{margin:0}#hl-viewer iframe{border:none}#hl-viewer #viewer-end{height:1px;width:1px;display:block}#hl-viewer #viewer-end:focus{outline:0}.ShareBtns .Share-Btn{padding:5px;border:1px solid #abaeb7}.ShareBtns .Share-Btn:hover{opacity:1;filter:alpha(opacity=100)}.ShareBtns .Tumblr-Btn{color:#35506d}.ShareBtns .Tumblr-Btn:hover{color:#35506d;background-color:#3593d3}.ShareBtns .Facebook-Btn{color:#3b5998}.ShareBtns .Facebook-Btn:hover{color:#3b5998;background-color:#4e91f2}.ShareBtns .Twitter-Btn{color:#00aced}.ShareBtns .Twitter-Btn:hover{color:#00aced;background-color:#94e8ff}.ShareBtns .Mail-Btn{background-position:-1px 161px;color:#0a80e3}.ShareBtns .Mail-Btn:hover{color:#0a80e3;background-color:#94e8ff}.ShareBtns .StickyPholder{display:inline-block}.ShareBtns .Viewer-Close-Btn{background-position:0 185px;padding:5px;margin-left:40px}.ShareBtns .Viewer-Close-Btn.Sticky{margin-left:-19px}#ModalSticker{max-width:1180px}#ModalSticker.Sticky{display:block!important;opacity:1;filter:alpha(opacity=100)}#ModalSticker .content-modal{border-bottom-width:3px;border-color:#000}#ModalSticker .ShareBtns{position:relative;float:right;margin-top:5px}.Modal-Credit{max-width:200px}.video-stage{margin-right:240px}.video-side-stage{width:240px}.Nav-Start{display:none}.First-Item .Nav-Start{display:block}.First-Item .Nav-Next,.First-Item .Nav-Prev{display:none}.slide-nav-arrow{background-color:#009bfb}.slide-nav-arrow:hover{opacity:1;filter:alpha(opacity=100)}.modal-sidekick,.modal-sidekick-following{display:inline-block}.js-sidekick-container{border-top:1px solid #f1f1f5}.modal-tooltip:hover .modal-tooltip\:h_H\(a\){height:auto!important}.modal-tooltip:hover .modal-tooltip\:h_Op\(1\){opacity:1!important}.c-modal-red{color:#ff156f}.lrec-before-loading{width:300px;height:250px;border:1px solid #e0e4e9;-webkit-transition:height 1.5s;-moz-transition:height 1.5s;-ms-transition:height 1.5s;-o-transition:height 1.5s;transition:height 1.5s}.js-header-searchbox{display:none;position:absolute;height:30px;left:730px;top:9px;border-radius:2px}.js-header-searchbox .srch-input{width:268px;height:100%;line-height:inherit;vertical-align:top;outline:0;box-shadow:none;border:none;border:1px solid #b0b0b0;border-radius:2px 0 0 2px;padding:0 10px}.js-header-searchbox .srch-input:focus{border-color:#0179ff}.js-header-searchbox button{width:32px;height:100%;background-color:#0179ff;font-size:16px;font-weight:700;color:#fff;border-radius:0 2px 2px 0;margin-left:-4px}.Z-6{z-index:6}.Z-7{z-index:7}.Z-8{z-index:8}.Z-9{z-index:9}.fixZindex{z-index:5!important}.viewer-right .js-slider-item.multirightrail.first{margin-top:25px!important}.viewer-right .Viewer-Close-Btn{border-radius:20px!important;font-size:17px!important;height:37px!important;margin-left:-18px!important;top:87px!important;width:37px!important}.viewer-right .viewer-wrapper{border-radius:6px;margin:1px!important;padding:0 0 30px 40px!important}.viewer-right.stream-sparse .viewer-wrapper{top:-12px}@media screen and (min-width:1100px){.js-header-searchbox{display:inline-block}}.Pt-35{padding-top:35px}.Pt-7{padding-top:7px}.JsEnabled .ImageLoader-Delayed{background:#f5f5f5}</style>
 

<style>#applet_p_63794 .Bd-0{border:0}#applet_p_63794 .Bd-1{border-width:1px}#applet_p_63794 .Bdx-1{border-right-width:1px;border-left-width:1px}#applet_p_63794 .Bdy-1{border-top-width:1px;border-bottom-width:1px}#applet_p_63794 .Bd-t{border-top-width:1px}#applet_p_63794 .Bd-end{border-right-width:1px}#applet_p_63794 .Bd-b{border-bottom-width:1px}#applet_p_63794 .Bd-start{border-left-width:1px}#applet_p_63794 .Bdt-0{border-top:0}#applet_p_63794 .Bdend-0{border-right:0}#applet_p_63794 .Bdb-0{border-bottom:0}#applet_p_63794 .Bdstart-0{border-left:0}#applet_p_63794 .Bdrs-0{border-radius:0}#applet_p_63794 .Bdtrrs-0{border-top-right-radius:0}#applet_p_63794 .Bdtlrs-0{border-top-left-radius:0}#applet_p_63794 .Bdbrrs-0{border-bottom-right-radius:0}#applet_p_63794 .Bdblrs-0{border-bottom-left-radius:0}#applet_p_63794 .Bdrs-100{border-radius:100px}#applet_p_63794 .Bdtrrs-100{border-top-right-radius:100px}#applet_p_63794 .Bdtlrs-100{border-top-left-radius:100px}#applet_p_63794 .Bdbrrs-100{border-bottom-right-radius:100px}#applet_p_63794 .Bdblrs-100{border-bottom-left-radius:100px}#applet_p_63794 .Bdrs-300{border-radius:300px}#applet_p_63794 .Bdtrrs-300{border-top-right-radius:300px}#applet_p_63794 .Bdtlrs-300{border-top-left-radius:300px}#applet_p_63794 .Bdbrrs-300{border-bottom-right-radius:300px}#applet_p_63794 .Bdblrs-300{border-bottom-left-radius:300px}#applet_p_63794 .Bdrs{border-radius:3px}#applet_p_63794 .Bdtrrs{border-top-right-radius:3px}#applet_p_63794 .Bdtlrs{border-top-left-radius:3px}#applet_p_63794 .Bdbrrs{border-bottom-right-radius:3px}#applet_p_63794 .Bdblrs{border-bottom-left-radius:3px}#applet_p_63794 .Ff{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-a{font-family:Georgia,"Times New Roman",serif}#applet_p_63794 .Ff-b{font-family:Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-c{font-family:"Monotype Corsiva","Comic Sans MS",cursive}#applet_p_63794 .Ff-d{font-family:Capitals,Impact,fantasy}#applet_p_63794 .Ff-e{font-family:Monaco,"Courier New",monospace}#applet_p_63794 .Fz-0{font-size:0}#applet_p_63794 .Fz-3xs{font-size:7px}#applet_p_63794 .Fz-2xs{font-size:9px}#applet_p_63794 .Fz-xs{font-size:11px;}#applet_p_63794 .Fz-s{font-size:13px;}#applet_p_63794 .Fz-m{font-size:15px;}#applet_p_63794 .Bgc-t{background-color:transparent}#applet_p_63794 .Bgi-n{background-image:none}#applet_p_63794 .Bg-n{background:0 0}#applet_p_63794 .Bgcp-bb{background-clip:border-box}#applet_p_63794 .Bgcp-pb{background-clip:padding-box}#applet_p_63794 .Bgcp-cb{background-clip:content-box}#applet_p_63794 .Bgo-pb{background-origin:padding-box}#applet_p_63794 .Bgo-bb{background-origin:border-box}#applet_p_63794 .Bgo-cb{background-origin:content-box}#applet_p_63794 .Bgz-a{background-size:auto}#applet_p_63794 .Bgz-ct{background-size:contain}#applet_p_63794 .Bgz-cv{background-size:cover;background-position:50% 15%}#applet_p_63794 .Bdcl-c{border-collapse:collapse}#applet_p_63794 .Bdcl-s{border-collapse:separate}#applet_p_63794 .Bxz-cb{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}#applet_p_63794 .Bxz-bb{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#applet_p_63794 .Bxsh-n{box-shadow:none}#applet_p_63794 .Cl-n{clear:none}#applet_p_63794 .Cl-b{clear:both}#applet_p_63794 .Cl-start{clear:left}#applet_p_63794 .Cl-end{clear:right}#applet_p_63794 .Cur-d{cursor:default}#applet_p_63794 .Cur-he{cursor:help}#applet_p_63794 .Cur-m{cursor:move}#applet_p_63794 .Cur-na{cursor:not-allowed}#applet_p_63794 .Cur-nsr{cursor:ns-resize}#applet_p_63794 .Cur-p{cursor:pointer}#applet_p_63794 .Cur-cr{cursor:col-resize}#applet_p_63794 .Cur-w{cursor:wait}#applet_p_63794 .Cur-a{cursor:auto!important}#applet_p_63794 .D-n{display:none}#applet_p_63794 .D-b{display:block}#applet_p_63794 .D-i{display:inline}#applet_p_63794 .D-ib{display:inline-block;*display:inline;zoom:1}#applet_p_63794 .D-tb{display:table}#applet_p_63794 .D-tbr{display:table-row}#applet_p_63794 .D-tbc{display:table-cell}#applet_p_63794 .D-li{display:list-item}#applet_p_63794 .D-ri{display:run-in}#applet_p_63794 .D-cp{display:compact}#applet_p_63794 .D-itb{display:inline-table}#applet_p_63794 .D-tbcl{display:table-column}#applet_p_63794 .D-tbclg{display:table-column-group}#applet_p_63794 .D-tbhg{display:table-header-group}#applet_p_63794 .D-tbfg{display:table-footer-group}#applet_p_63794 .D-tbrg{display:table-row-group}#applet_p_63794 .Fl-n{float:none}#applet_p_63794 .Fl-start{float:left;_display:inline}#applet_p_63794 .Fl-end{float:right;_display:inline}#applet_p_63794 .Fw-n{font-weight:400}#applet_p_63794 .Fw-b{font-weight:700;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}#applet_p_63794 .Fw-100{font-weight:100;*font-weight:normal}#applet_p_63794 .Fw-200{font-weight:200;*font-weight:normal}#applet_p_63794 .Fw-400{font-weight:400;*font-weight:normal}#applet_p_63794 .Fw-br{font-weight:bolder}#applet_p_63794 .Fw-lr{font-weight:lighter}#applet_p_63794 .Fs-n{font-style:normal}#applet_p_63794 .Fs-i{font-style:italic}#applet_p_63794 .Fv-sc{font-variant:small-caps}#applet_p_63794 .Fv-n{font-variant:normal}#applet_p_63794 .H-0{height:0}#applet_p_63794 .H-50{height:50%}#applet_p_63794 .H-100{height:100%}#applet_p_63794 .H-a{height:auto}#applet_p_63794 .H-n,#applet_p_63794 .h-n{-webkit-hyphens:none;-moz-hyphens:none;-ms-hyphens:none;hyphens:none}#applet_p_63794 .List-n{list-style-type:none}#applet_p_63794 .List-d{list-style-type:disc}#applet_p_63794 .List-c{list-style-type:circle}#applet_p_63794 .List-s{list-style-type:square}#applet_p_63794 .List-dc{list-style-type:decimal}#applet_p_63794 .List-dclz{list-style-type:decimal-leading-zero}#applet_p_63794 .List-lr{list-style-type:lower-roman}#applet_p_63794 .List-ur{list-style-type:upper-roman}#applet_p_63794 .Lisi-n{list-style-image:none}#applet_p_63794 .Lh-0{line-height:0}#applet_p_63794 .Lh-01{line-height:.1}#applet_p_63794 .Lh-02{line-height:.2}#applet_p_63794 .Lh-03{line-height:.3}#applet_p_63794 .Lh-04{line-height:.4}#applet_p_63794 .Lh-05{line-height:.5}#applet_p_63794 .Lh-06{line-height:.6}#applet_p_63794 .Lh-07{line-height:.7}#applet_p_63794 .Lh-08{line-height:.8}#applet_p_63794 .Lh-09{line-height:.9}#applet_p_63794 .Lh-1{line-height:1}#applet_p_63794 .Lh-11{line-height:1.1}#applet_p_63794 .Lh-12{line-height:1.2}#applet_p_63794 .Lh-125{line-height:1.25}#applet_p_63794 .Lh-13{line-height:1.3}#applet_p_63794 .Lh-14{line-height:1.4}#applet_p_63794 .Lh-15{line-height:1.5}#applet_p_63794 .Lh-16{line-height:1.6}#applet_p_63794 .Lh-17{line-height:1.7}#applet_p_63794 .Lh-18{line-height:1.8}#applet_p_63794 .Lh-19{line-height:1.9}#applet_p_63794 .Lh-2{line-height:2}#applet_p_63794 .Lh-21{line-height:2.1}#applet_p_63794 .Lh-22{line-height:2.2}#applet_p_63794 .Lh-23{line-height:2.3}#applet_p_63794 .Lh-24{line-height:2.4}#applet_p_63794 .Lh-25{line-height:2.5}#applet_p_63794 .Lh-3{line-height:3}#applet_p_63794 .Lh-reset{line-height:normal}#applet_p_63794 .M-0{margin:0}#applet_p_63794 .M-2{margin:2px}#applet_p_63794 .M-4{margin:4px}#applet_p_63794 .M-6{margin:6px}#applet_p_63794 .M-8{margin:8px}#applet_p_63794 .M-10{margin:10px}#applet_p_63794 .M-12{margin:12px}#applet_p_63794 .M-14{margin:14px}#applet_p_63794 .M-16{margin:16px}#applet_p_63794 .M-18{margin:18px}#applet_p_63794 .M-20{margin:20px}#applet_p_63794 .Mx-1{margin-right:1px;margin-left:1px}#applet_p_63794 .Mx-2{margin-right:2px;margin-left:2px}#applet_p_63794 .Mx-4{margin-right:4px;margin-left:4px}#applet_p_63794 .Mx-6{margin-right:6px;margin-left:6px}#applet_p_63794 .Mx-8{margin-right:8px;margin-left:8px}#applet_p_63794 .Mx-10{margin-right:10px;margin-left:10px}#applet_p_63794 .Mx-12{margin-right:12px;margin-left:12px}#applet_p_63794 .Mx-14{margin-right:14px;margin-left:14px}#applet_p_63794 .Mx-16{margin-right:16px;margin-left:16px}#applet_p_63794 .Mx-18{margin-right:18px;margin-left:18px}#applet_p_63794 .Mx-20{margin-right:20px;margin-left:20px}#applet_p_63794 .My-1{margin-top:1px;margin-bottom:1px}#applet_p_63794 .My-2{margin-top:2px;margin-bottom:2px}#applet_p_63794 .My-4{margin-top:4px;margin-bottom:4px}#applet_p_63794 .My-6{margin-top:6px;margin-bottom:6px}#applet_p_63794 .My-8{margin-top:8px;margin-bottom:8px}#applet_p_63794 .My-10{margin-top:10px;margin-bottom:10px}#applet_p_63794 .My-12{margin-top:12px;margin-bottom:12px}#applet_p_63794 .My-14{margin-top:14px;margin-bottom:14px}#applet_p_63794 .My-16{margin-top:16px;margin-bottom:16px}#applet_p_63794 .My-18{margin-top:18px;margin-bottom:18px}#applet_p_63794 .My-20{margin-top:20px;margin-bottom:20px}#applet_p_63794 .Mt-1{margin-top:1px}#applet_p_63794 .Mb-1{margin-bottom:1px}#applet_p_63794 .Mstart-1{margin-left:1px}#applet_p_63794 .Mend-1{margin-right:1px}#applet_p_63794 .Mt-2{margin-top:2px}#applet_p_63794 .Mb-2{margin-bottom:2px}#applet_p_63794 .Mstart-2{margin-left:2px}#applet_p_63794 .Mend-2{margin-right:2px}#applet_p_63794 .Mt-4{margin-top:4px}#applet_p_63794 .Mb-4{margin-bottom:4px}#applet_p_63794 .Mstart-4{margin-left:4px}#applet_p_63794 .Mend-4{margin-right:4px}#applet_p_63794 .Mt-6{margin-top:6px}#applet_p_63794 .Mb-6{margin-bottom:6px}#applet_p_63794 .Mstart-6{margin-left:6px}#applet_p_63794 .Mend-6{margin-right:6px}#applet_p_63794 .Mt-8{margin-top:8px}#applet_p_63794 .Mb-8{margin-bottom:8px}#applet_p_63794 .Mstart-8{margin-left:8px}#applet_p_63794 .Mend-8{margin-right:8px}#applet_p_63794 .Mt-10{margin-top:10px}#applet_p_63794 .Mb-10{margin-bottom:10px}#applet_p_63794 .Mstart-10{margin-left:10px}#applet_p_63794 .Mend-10{margin-right:10px}#applet_p_63794 .Mt-12{margin-top:12px}#applet_p_63794 .Mb-12{margin-bottom:12px}#applet_p_63794 .Mstart-12{margin-left:12px}#applet_p_63794 .Mend-12{margin-right:12px}#applet_p_63794 .Mt-14{margin-top:14px}#applet_p_63794 .Mb-14{margin-bottom:14px}#applet_p_63794 .Mstart-14{margin-left:14px}#applet_p_63794 .Mend-14{margin-right:14px}#applet_p_63794 .Mt-16{margin-top:16px}#applet_p_63794 .Mb-16{margin-bottom:16px}#applet_p_63794 .Mstart-16{margin-left:16px}#applet_p_63794 .Mend-16{margin-right:16px}#applet_p_63794 .Mt-18{margin-top:18px}#applet_p_63794 .Mb-18{margin-bottom:18px}#applet_p_63794 .Mstart-18{margin-left:18px}#applet_p_63794 .Mend-18{margin-right:18px}#applet_p_63794 .Mt-20{margin-top:20px}#applet_p_63794 .Mb-20{margin-bottom:20px}#applet_p_63794 .Mstart-20{margin-left:20px}#applet_p_63794 .Mend-20{margin-right:20px}#applet_p_63794 .Mt-30{margin-top:30px}#applet_p_63794 .Mb-30{margin-bottom:30px}#applet_p_63794 .Mstart-30{margin-left:30px}#applet_p_63794 .Mend-30{margin-right:30px}#applet_p_63794 .Mt-40{margin-top:40px}#applet_p_63794 .Mb-40{margin-bottom:40px}#applet_p_63794 .Mstart-40{margin-left:40px}#applet_p_63794 .Mend-40{margin-right:40px}#applet_p_63794 .Mt-50{margin-top:50px}#applet_p_63794 .Mb-50{margin-bottom:50px}#applet_p_63794 .Mstart-50{margin-left:50px}#applet_p_63794 .Mend-50{margin-right:50px}#applet_p_63794 .Mt-60{margin-top:60px}#applet_p_63794 .Mb-60{margin-bottom:60px}#applet_p_63794 .Mstart-60{margin-left:60px}#applet_p_63794 .Mend-60{margin-right:60px}#applet_p_63794 .Mt-70{margin-top:70px}#applet_p_63794 .Mb-70{margin-bottom:70px}#applet_p_63794 .Mstart-70{margin-left:70px}#applet_p_63794 .Mend-70{margin-right:70px}#applet_p_63794 .Mt-neg-1{margin-top:-1px}#applet_p_63794 .Mb-neg-1{margin-bottom:-1px}#applet_p_63794 .Mstart-neg-1{margin-left:-1px}#applet_p_63794 .Mend-neg-1{margin-right:-1px}#applet_p_63794 .Mt-neg-4{margin-top:-4px}#applet_p_63794 .Mb-neg-4{margin-bottom:-4px}#applet_p_63794 .Mstart-neg-4{margin-left:-4px}#applet_p_63794 .Mend-neg-4{margin-right:-4px}#applet_p_63794 .Mt-neg-6{margin-top:-6px}#applet_p_63794 .Mb-neg-6{margin-bottom:-6px}#applet_p_63794 .Mstart-neg-6{margin-left:-6px}#applet_p_63794 .Mend-neg-6{margin-right:-6px}#applet_p_63794 .Mt-neg-8{margin-top:-8px}#applet_p_63794 .Mb-neg-8{margin-bottom:-8px}#applet_p_63794 .Mstart-neg-8{margin-left:-8px}#applet_p_63794 .Mend-neg-8{margin-right:-8px}#applet_p_63794 .Mt-neg-10{margin-top:-10px}#applet_p_63794 .Mb-neg-10{margin-bottom:-10px}#applet_p_63794 .Mstart-neg-10{margin-left:-10px}#applet_p_63794 .Mend-neg-10{margin-right:-10px}#applet_p_63794 .Mt-neg-12{margin-top:-12px}#applet_p_63794 .Mb-neg-12{margin-bottom:-12px}#applet_p_63794 .Mstart-neg-12{margin-left:-12px}#applet_p_63794 .Mend-neg-12{margin-right:-12px}#applet_p_63794 .Mt-neg-14{margin-top:-14px}#applet_p_63794 .Mb-neg-14{margin-bottom:-14px}#applet_p_63794 .Mstart-neg-14{margin-left:-14px}#applet_p_63794 .Mend-neg-14{margin-right:-14px}#applet_p_63794 .Mt-neg-16{margin-top:-16px}#applet_p_63794 .Mb-neg-16{margin-bottom:-16px}#applet_p_63794 .Mstart-neg-16{margin-left:-16px}#applet_p_63794 .Mend-neg-16{margin-right:-16px}#applet_p_63794 .Mt-neg-18{margin-top:-18px}#applet_p_63794 .Mb-neg-18{margin-bottom:-18px}#applet_p_63794 .Mstart-neg-18{margin-left:-18px}#applet_p_63794 .Mend-neg-18{margin-right:-18px}#applet_p_63794 .Mt-neg-20{margin-top:-20px}#applet_p_63794 .Mb-neg-20{margin-bottom:-20px}#applet_p_63794 .Mstart-neg-20{margin-left:-20px}#applet_p_63794 .Mend-neg-20{margin-right:-20px}#applet_p_63794 .Mstart-50\%{margin-left:50%}#applet_p_63794 .M-a{margin:auto}#applet_p_63794 .Mx-a{margin-right:auto;margin-left:auto}#applet_p_63794 .\!Mx-a{margin-right:auto!important;margin-left:auto!important}#applet_p_63794 .Mstart-a{margin-left:auto}#applet_p_63794 .Mend-a{margin-right:auto}#applet_p_63794 .Mt-0,#applet_p_63794 .My-0{margin-top:0}#applet_p_63794 .Mb-0,#applet_p_63794 .My-0{margin-bottom:0}#applet_p_63794 .Mstart-0,#applet_p_63794 .Mx-0{margin-left:0}#applet_p_63794 .Mend-0,#applet_p_63794 .Mx-0{margin-right:0}#applet_p_63794 .Miw-0{min-width:0}#applet_p_63794 .Miw-10{min-width:10%}#applet_p_63794 .Miw-15{min-width:15%}#applet_p_63794 .Miw-20{min-width:20%}#applet_p_63794 .Miw-25{min-width:25%}#applet_p_63794 .Miw-30{min-width:30%}#applet_p_63794 .Miw-35{min-width:35%}#applet_p_63794 .Miw-40{min-width:40%}#applet_p_63794 .Miw-45{min-width:45%}#applet_p_63794 .Miw-50{min-width:50%}#applet_p_63794 .Miw-60{min-width:60%}#applet_p_63794 .Miw-70{min-width:70%}#applet_p_63794 .Miw-80{min-width:80%}#applet_p_63794 .Miw-90{min-width:90%}#applet_p_63794 .Miw-100{min-width:100%}#applet_p_63794 .Maw-n{max-width:none}#applet_p_63794 .Maw-10{max-width:10%}#applet_p_63794 .Maw-15{max-width:15%}#applet_p_63794 .Maw-20{max-width:20%}#applet_p_63794 .Maw-25{max-width:25%}#applet_p_63794 .Maw-30{max-width:30%}#applet_p_63794 .Maw-35{max-width:35%}#applet_p_63794 .Maw-40{max-width:40%}#applet_p_63794 .Maw-45{max-width:45%}#applet_p_63794 .Maw-50{max-width:50%}#applet_p_63794 .Maw-60{max-width:60%}#applet_p_63794 .Maw-70{max-width:70%}#applet_p_63794 .Maw-80{max-width:80%}#applet_p_63794 .Maw-90{max-width:90%}#applet_p_63794 .Maw-99{max-width:99%}#applet_p_63794 .Maw-100{max-width:100%}#applet_p_63794 .Mih-0{min-height:0}#applet_p_63794 .Mih-50{min-height:50%;_height:50%}#applet_p_63794 .Mih-60{min-height:60%;_height:60%}#applet_p_63794 .Mih-100{min-height:100%;_height:100%}#applet_p_63794 .Mah-n{max-height:none}#applet_p_63794 .Mah-0{max-height:0}#applet_p_63794 .Mah-50{max-height:50%}#applet_p_63794 .Mah-60{max-height:60%}#applet_p_63794 .Mah-100{max-height:100%}#applet_p_63794 .O-0{outline:0}#applet_p_63794 .T-0{top:0}#applet_p_63794 .B-0{bottom:0}#applet_p_63794 .Start-0{left:0}#applet_p_63794 .End-0{right:0}#applet_p_63794 .T-10{top:10%}#applet_p_63794 .B-10{bottom:10%}#applet_p_63794 .Start-10{left:10%}#applet_p_63794 .End-10{right:10%}#applet_p_63794 .T-25{top:25%}#applet_p_63794 .B-25{bottom:25%}#applet_p_63794 .Start-25{left:25%}#applet_p_63794 .End-25{right:25%}#applet_p_63794 .T-50{top:50%}#applet_p_63794 .B-50{bottom:50%}#applet_p_63794 .Start-50{left:50%}#applet_p_63794 .End-50{right:50%}#applet_p_63794 .T-75{top:75%}#applet_p_63794 .B-75{bottom:75%}#applet_p_63794 .Start-75{left:75%}#applet_p_63794 .End-75{right:75%}#applet_p_63794 .T-100{top:100%}#applet_p_63794 .B-100{bottom:100%}#applet_p_63794 .Start-100{left:100%}#applet_p_63794 .End-100{right:100%}#applet_p_63794 .T-a{top:auto!important}#applet_p_63794 .B-a{bottom:auto!important}#applet_p_63794 .Start-a{left:auto!important}#applet_p_63794 .End-a{right:auto!important}#applet_p_63794 .Op-0{opacity:0;filter:alpha(opacity=0)}#applet_p_63794 .Op-33{opacity:.33;filter:alpha(opacity=33)}#applet_p_63794 .Op-50{opacity:.5;filter:alpha(opacity=50)}#applet_p_63794 .Op-66{opacity:.66;filter:alpha(opacity=66)}#applet_p_63794 .Op-100{opacity:1;filter:alpha(opacity=100)}#applet_p_63794 .Ov-h{overflow:hidden;zoom:1}#applet_p_63794 .Ov-v{overflow:visible}#applet_p_63794 .Ov-s{overflow:scroll}#applet_p_63794 .Ov-a{overflow:auto}#applet_p_63794 .Ovs-t{-webkit-overflow-scrolling:touch}#applet_p_63794 .Ovx-v{overflow-x:visible}#applet_p_63794 .Ovx-h{overflow-x:hidden}#applet_p_63794 .Ovx-s{overflow-x:scroll}#applet_p_63794 .Ovx-a{overflow-x:auto}#applet_p_63794 .Ovy-v{overflow-y:visible}#applet_p_63794 .Ovy-h{overflow-y:hidden}#applet_p_63794 .Ovy-s{overflow-y:scroll}#applet_p_63794 .Ovy-a{overflow-y:auto}#applet_p_63794 .P-0{padding:0}#applet_p_63794 .P-1{padding:1px}#applet_p_63794 .P-2{padding:2px}#applet_p_63794 .P-4{padding:4px}#applet_p_63794 .P-6{padding:6px}#applet_p_63794 .P-8{padding:8px}#applet_p_63794 .P-10{padding:10px}#applet_p_63794 .P-12{padding:12px}#applet_p_63794 .P-14{padding:14px}#applet_p_63794 .P-16{padding:16px}#applet_p_63794 .P-18{padding:18px}#applet_p_63794 .P-20{padding:20px}#applet_p_63794 .P-30{padding:30px}#applet_p_63794 .Px-1{padding-right:1px;padding-left:1px}#applet_p_63794 .Px-2{padding-right:2px;padding-left:2px}#applet_p_63794 .Px-4{padding-right:4px;padding-left:4px}#applet_p_63794 .Px-6{padding-right:6px;padding-left:6px}#applet_p_63794 .Px-8{padding-right:8px;padding-left:8px}#applet_p_63794 .Px-10{padding-right:10px;padding-left:10px}#applet_p_63794 .Px-12{padding-right:12px;padding-left:12px}#applet_p_63794 .Px-14{padding-right:14px;padding-left:14px}#applet_p_63794 .Px-16{padding-right:16px;padding-left:16px}#applet_p_63794 .Px-18{padding-right:18px;padding-left:18px}#applet_p_63794 .Px-20{padding-right:20px;padding-left:20px}#applet_p_63794 .Px-30{padding-right:30px;padding-left:30px}#applet_p_63794 .Py-1{padding-top:1px;padding-bottom:1px}#applet_p_63794 .Py-2{padding-top:2px;padding-bottom:2px}#applet_p_63794 .Py-4{padding-top:4px;padding-bottom:4px}#applet_p_63794 .Py-6{padding-top:6px;padding-bottom:6px}#applet_p_63794 .Py-8{padding-top:8px;padding-bottom:8px}#applet_p_63794 .Py-10{padding-top:10px;padding-bottom:10px}#applet_p_63794 .Py-12{padding-top:12px;padding-bottom:12px}#applet_p_63794 .Py-14{padding-top:14px;padding-bottom:14px}#applet_p_63794 .Py-16{padding-top:16px;padding-bottom:16px}#applet_p_63794 .Py-18{padding-top:18px;padding-bottom:18px}#applet_p_63794 .Py-20{padding-top:20px;padding-bottom:20px}#applet_p_63794 .Py-30{padding-top:30px;padding-bottom:30px}#applet_p_63794 .Pt-1{padding-top:1px}#applet_p_63794 .Pb-1{padding-bottom:1px}#applet_p_63794 .Pstart-1{padding-left:1px}#applet_p_63794 .Pend-1{padding-right:1px}#applet_p_63794 .Pt-2{padding-top:2px}#applet_p_63794 .Pb-2{padding-bottom:2px}#applet_p_63794 .Pstart-2{padding-left:2px}#applet_p_63794 .Pend-2{padding-right:2px}#applet_p_63794 .Pt-4{padding-top:4px}#applet_p_63794 .Pb-4{padding-bottom:4px}#applet_p_63794 .Pstart-4{padding-left:4px}#applet_p_63794 .Pend-4{padding-right:4px}#applet_p_63794 .Pt-6{padding-top:6px}#applet_p_63794 .Pb-6{padding-bottom:6px}#applet_p_63794 .Pstart-6{padding-left:6px}#applet_p_63794 .Pend-6{padding-right:6px}#applet_p_63794 .Pt-8{padding-top:8px}#applet_p_63794 .Pb-8{padding-bottom:8px}#applet_p_63794 .Pstart-8{padding-left:8px}#applet_p_63794 .Pend-8{padding-right:8px}#applet_p_63794 .Pt-10{padding-top:10px}#applet_p_63794 .Pb-10{padding-bottom:10px}#applet_p_63794 .Pstart-10{padding-left:10px}#applet_p_63794 .Pend-10{padding-right:10px}#applet_p_63794 .Pt-12{padding-top:12px}#applet_p_63794 .Pb-12{padding-bottom:12px}#applet_p_63794 .Pstart-12{padding-left:12px}#applet_p_63794 .Pend-12{padding-right:12px}#applet_p_63794 .Pt-14{padding-top:14px}#applet_p_63794 .Pb-14{padding-bottom:14px}#applet_p_63794 .Pstart-14{padding-left:14px}#applet_p_63794 .Pend-14{padding-right:14px}#applet_p_63794 .Pt-16{padding-top:16px}#applet_p_63794 .Pb-16{padding-bottom:16px}#applet_p_63794 .Pstart-16{padding-left:16px}#applet_p_63794 .Pend-16{padding-right:16px}#applet_p_63794 .Pt-18{padding-top:18px}#applet_p_63794 .Pb-18{padding-bottom:18px}#applet_p_63794 .Pstart-18{padding-left:18px}#applet_p_63794 .Pend-18{padding-right:18px}#applet_p_63794 .Pt-20{padding-top:20px}#applet_p_63794 .Pend-20{padding-right:20px}#applet_p_63794 .Pb-20{padding-bottom:20px}#applet_p_63794 .Pstart-20{padding-left:20px}#applet_p_63794 .Pt-30{padding-top:30px}#applet_p_63794 .Pend-30{padding-right:30px}#applet_p_63794 .Pb-30{padding-bottom:30px}#applet_p_63794 .Pstart-30{padding-left:30px}#applet_p_63794 .Pt-0,#applet_p_63794 .Py-0{padding-top:0}#applet_p_63794 .Pb-0,#applet_p_63794 .Py-0{padding-bottom:0}#applet_p_63794 .Pstart-0,#applet_p_63794 .Px-0{padding-left:0}#applet_p_63794 .Pend-0,#applet_p_63794 .Px-0{padding-right:0}#applet_p_63794 .Pe-n{pointer-events:none}#applet_p_63794 .Pe-a{pointer-events:auto}#applet_p_63794 .Pos-s{position:static}#applet_p_63794 .Pos-a{position:absolute}#applet_p_63794 .Pos-r{position:relative}#applet_p_63794 .Pos-f{position:fixed}#applet_p_63794 .Ws-n::-webkit-scrollbar{-webkit-appearance:none}#applet_p_63794 .Tbl-f{table-layout:fixed}#applet_p_63794 .Tbl-a{table-layout:auto}#applet_p_63794 .Ta-c{text-align:center}#applet_p_63794 .Ta-j{text-align:justify}#applet_p_63794 .Ta-start{text-align:left}#applet_p_63794 .Ta-end{text-align:right}#applet_p_63794 .Td-n{text-decoration:none!important}#applet_p_63794 .Td-u,#applet_p_63794 .Td-u\:h:hover{text-decoration:underline}#applet_p_63794 .Tr-a{-webkit-text-rendering:auto;-moz-text-rendering:auto;-ms-text-rendering:auto;-o-text-rendering:auto;text-rendering:auto}#applet_p_63794 .Tr-os{-webkit-text-rendering:optimizeSpeed;-moz-text-rendering:optimizeSpeed;-ms-text-rendering:optimizeSpeed;-o-text-rendering:optimizeSpeed;text-rendering:optimizeSpeed}#applet_p_63794 .Tr-ol{-webkit-text-rendering:optimizeLegibility;-moz-text-rendering:optimizeLegibility;-ms-text-rendering:optimizeLegibility;-o-text-rendering:optimizeLegibility;text-rendering:optimizeLegibility}#applet_p_63794 .Tr-gp{-webkit-text-rendering:geometricPrecision;-moz-text-rendering:geometricPrecision;-ms-text-rendering:geometricPrecision;-o-text-rendering:geometricPrecision;text-rendering:geometricPrecision}#applet_p_63794 .Tr-i{-webkit-text-rendering:inherit;-moz-text-rendering:inherit;-ms-text-rendering:inherit;-o-text-rendering:inherit;text-rendering:inherit}#applet_p_63794 .Tt-n{text-transform:none}#applet_p_63794 .Tt-u{text-transform:uppercase}#applet_p_63794 .Tt-c{text-transform:capitalize}#applet_p_63794 .Tt-l{text-transform:lowercase}#applet_p_63794 .Tsh{text-shadow:0 1px 1px #000}#applet_p_63794 .Tsh-n{text-shadow:none}#applet_p_63794 .Us-n{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}#applet_p_63794 .Va-sup{vertical-align:super}#applet_p_63794 .Va-t{vertical-align:top}#applet_p_63794 .Va-tt{vertical-align:text-top}#applet_p_63794 .Va-m{vertical-align:middle}#applet_p_63794 .Va-bl{vertical-align:baseline}#applet_p_63794 .Va-b{vertical-align:bottom}#applet_p_63794 .Va-tb{vertical-align:text-bottom}#applet_p_63794 .Va-sub{vertical-align:sub}#applet_p_63794 .V-v{visibility:visible}#applet_p_63794 .V-h{visibility:hidden}#applet_p_63794 .V-c{visibility:collapse}#applet_p_63794 .Whs-nw{white-space:nowrap}#applet_p_63794 .Whs-n{white-space:normal}#applet_p_63794 .W-a{width:auto}#applet_p_63794 .W-0{width:0}#applet_p_63794 .W-1{width:1%}#applet_p_63794 .W-10{width:10%;*width:9.6%}#applet_p_63794 .W-15{width:15%;*width:14.8%}#applet_p_63794 .W-20{width:20%;*width:19.5%}#applet_p_63794 .W-25{width:25%;*width:24.5%}#applet_p_63794 .W-30{width:30%;*width:29.6%}#applet_p_63794 .W-33{width:33.33%;*width:33%}#applet_p_63794 .W-35{width:35%;*width:34.9%}#applet_p_63794 .W-40{width:40%;*width:39.5%}#applet_p_63794 .W-45{width:45%;*width:44.9%}#applet_p_63794 .W-50{width:50%;*width:49.5%}#applet_p_63794 .W-55{width:55%;*width:54.8%}#applet_p_63794 .W-60{width:60%}#applet_p_63794 .W-66{width:66.66%}#applet_p_63794 .W-70{width:70%}#applet_p_63794 .W-75{width:75%}#applet_p_63794 .W-80{width:80%}#applet_p_63794 .W-90{width:90%}#applet_p_63794 .W-100{width:100%}#applet_p_63794 .Wpx-1{width:1px}#applet_p_63794 .Wpx-2{width:2px}#applet_p_63794 .Wpx-4{width:4px}#applet_p_63794 .Wpx-6{width:6px}#applet_p_63794 .Wpx-8{width:8px}#applet_p_63794 .Wpx-10{width:10px}#applet_p_63794 .Wpx-12{width:12px}#applet_p_63794 .Wpx-14{width:14px}#applet_p_63794 .Wpx-16{width:16px}#applet_p_63794 .Wpx-18{width:18px}#applet_p_63794 .Wpx-20{width:20px}#applet_p_63794 .Wpx-24{width:24px}#applet_p_63794 .Wpx-26{width:26px}#applet_p_63794 .Wpx-28{width:28px}#applet_p_63794 .Wpx-30{width:30px}#applet_p_63794 .Wpx-32{width:32px}#applet_p_63794 .Wob-ba{word-break:break-all}#applet_p_63794 .Wob-n{word-break:normal!important}#applet_p_63794 .Wow-bw{word-wrap:break-word}#applet_p_63794 .Wow-n{word-wrap:normal!important}#applet_p_63794 .Z-0{z-index:0}#applet_p_63794 .Z-1{z-index:1}#applet_p_63794 .Z-3{z-index:3}#applet_p_63794 .Z-5{z-index:5}#applet_p_63794 .Z-7{z-index:7}#applet_p_63794 .Z-10{z-index:10}#applet_p_63794 .Z-a{z-index:auto!important}#applet_p_63794 .Zoom-1{zoom:1}

#applet_p_63794 .StencilRoot input {
    font-size: 13px;    
}

#applet_p_63794 .StencilRoot button {
    font-size: 14px;
    line-height: normal;
}
</style>

    
    </head>
    <body class="my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr    stream-dense" dir="ltr">
        
        
        
                    <div id="darla-assets-top">
            <script type='text/javascript' src='/sy/rq/darla/2-9-4/js/g-r-min.js'></script>
            
            </div>
                <script type="text/javascript">
        var rapidPageConfig = {
            rapidEarlyConfig : {},
            rapidConfig: {"compr_type":"deflate","tracked_mods":[],"spaceid":2023538075,"ywa":{"project_id":"10001806365479","host":"y.analytics.yahoo.com"},"click_timeout":300,"track_right_click":true,"apv":true,"apv_time":0,"yql_host":"","test_id":"201","client_only":0,"pageview_on_init":true,"perf_navigationtime":2,"addmodules_timeout":500,"keys":{"_rid":"5qnosq1bbcbsb","mrkt":"us","pt":1,"ver":"megastrm","uh_vw":0,"colo":"slw29.fp.bf1.yahoo.com","navtype":"server","nob":1},"viewability":true},
            rapidSingleInstance: 0,
            ywaCF: {"mrkt":12,"pt":13,"test":14,"sec":18,"slk":19,"cpos":21,"pkgt":22,"ct":23,"bpos":24,"cat":25,"dcl":26,"f":40,"prfm1":41,"site":42,"prfm2":43,"prfm3":44,"prfm4":45,"pct":48,"ver":49,"ft":51,"sca":53,"elm":56,"elmt":57,"ad":58,"tsrc":69,"err":72,"bkpt":73,"bw":98,"bh":99,"olncust":100,"noct":101,"uh_vw":102,"rspns":107,"grpt":109,"itc":111,"enr":112,"tar":113,"sp":115,"pos":117,"cposx":118,"cw":119,"ch":120,"t1":121,"t2":122,"t3":123,"t4":124,"t5":125,"refcnt":135,"tar_qa":145,"navtype":146},
            ywaActionMap: {"swp":103,"click":12,"secvw":18,"hvr":115,"error":99},
            ywaOutcomeMap: {"fetch":30,"end":31,"dclent":101,"dclitm":102,"op":105,"cl":106,"nav":108,"svct":109,"unsvct":110,"rmsvct":117,"slct":121,"imprt":123,"lgn":125,"lgo":126,"flagitm":129,"unflagitm":130,"flatcat":131,"unflagcat":132,"slctfltr":133} 
        };
        if (rapidPageConfig.rapidEarlyConfig.keys) {
            rapidPageConfig.rapidEarlyConfig.keys.bw = document.body.offsetWidth;
            rapidPageConfig.rapidEarlyConfig.keys.bh = document.body.offsetHeight;
        }
        rapidPageConfig.rapidConfig.keys.bw = document.body.offsetWidth;
        rapidPageConfig.rapidConfig.keys.bh = document.body.offsetHeight;
        </script>
        
                            
                    
                    
                    
                    
                    
                    

    <div id="UH">
        <div id="UH-ColWrap">
            <div id="applet_p_30345894" class=" M-0 js-applet header Zoom-1  Mb-0 " data-applet-guid="p_30345894" data-applet-type="header" data-applet-params="_suid:30345894" data-i13n="auto:true;sec:hd" data-i13n-sec="hd" data-ylk="rspns:nav;t1:a1;t2:hd;itc:0;"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-uh-wrapper" class="Bds(n) Bdc($headerBdr) Scrolling_Bdc($headerBdrScroll) has-scrolled_Bdc($headerBdrScroll) Bdbw(1px) ua-ie7_Bdbs(s)! ua-ie8_Bdbs(s)! Scrolling_Bxsh($headerShadow) has-scrolled_Bxsh($headerShadow) Bgc(#fff) T(0) Start(0) End(0) Pos(f) Z(3) Zoom">
    
    <div id="mega-topbar" class="Pos(r) H(22px) mini-header_Mt(-19px) Reader-open_Mt(-19px) Bg($topbarBgc) Bxsh($topbarShadow) Z(7)"   data-ylk="rspns:nav;t1:a1;t2:hd;t3:tb;sec:hd;itc:0;elm:itm;elmt:pty;">
    <ul class="Pos(r) Miw(1000px) Pstart(9px) Lh(1.7) Reader-open_Op(0) mini-header_Op(0)" role="navigation">
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:home;t5:home;cpos:1;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) Icon-Fp2 IconHome"></i>Home</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mail.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mail;t5:mail;cpos:2;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mail</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.flickr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:flickr;t5:flickr;cpos:3;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Flickr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.tumblr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:tumblr;t5:tumblr;cpos:4;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Tumblr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://answers.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:answers;t5:answers;cpos:5;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Answers</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://groups.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:groups;t5:groups;cpos:6;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Groups</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mobile.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mobile;t5:mobile;cpos:7;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mobile</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(10px) navigation-menu">
            <a href="https://everything.yahoo.com/" class="navigation-menu-title Pos(r) C(#fff) Pstart(12px) Pend(24px) Py(4px) Fz(13px) menu-open_C($topbarMenu) menu-open_Bgc(#fff) menu-open_Z(8) rapidnofollow"   data-ylk="rspns:op;t5:more;slk:more;itc:1;elmt:mu;cpos:8;" tabindex="1">More<i class="Pos(a) Pt(2px) Pstart(6px) Fw(b) Icon-Fp2 IconDownCaret"></i></a>
            <div class="Pos(a) Bgc(#fff) Bxsh($topbarMenuShadow) Z(7) V(h) Op(0) menu-open_V(v) menu-open_Op(1)">
                <ul class="Px(12px) Py(5px)">
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/politics/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:politics;t5:politics;cpos:9;" tabindex="1">Politics</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/celebrity/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:celebrity;t5:celebrity;cpos:10;" tabindex="1">Celebrity</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/movies/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:movies;t5:movies;cpos:11;" tabindex="1">Movies</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/music/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:music;t5:music;cpos:12;" tabindex="1">Music</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tv/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tv;t5:tv;cpos:13;" tabindex="1">TV</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/health/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:health;t5:health;cpos:14;" tabindex="1">Health</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/style/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:style;t5:style;cpos:15;" tabindex="1">Style</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/beauty/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:beauty;t5:beauty;cpos:16;" tabindex="1">Beauty</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/food/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:food;t5:food;cpos:17;" tabindex="1">Food</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/parenting/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:parenting;t5:parenting;cpos:18;" tabindex="1">Parenting</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/makers/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:makers;t5:makers;cpos:19;" tabindex="1">Makers</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tech/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tech;t5:tech;cpos:20;" tabindex="1">Tech</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://shopping.yahoo.com/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:shopping;t5:shopping;cpos:21;" tabindex="1">Shopping</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/travel/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:travel;t5:travel;cpos:22;" tabindex="1">Travel</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/autos/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:autos;t5:autos;cpos:23;" tabindex="1">Autos</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/realestate/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:homes;t5:homes;cpos:24;" tabindex="1">Real Estate</a>
                    </li>
                
                </ul>
            </div>
        </li>
    
    
    
    </ul>
</div>

    
    <div id="mega-uh" class="M(a) Maw(1256px) Miw(1000px) Pos(r) Pt(22px) Pb(24px) Reader-open_Pt(13px) mini-header_Pt(13px) Reader-open_Pb(14px) mini-header_Pb(14px) Z(6)">
        <h1 class="Fz(0) Pos(a) Pstart(15px) Reader-open_Pt(0px) mini-header_Pt(0px) search-mini-header_Pstart(10px) ua-ie7_Mt(4px)">
    <a id="uh-logo" href="https://www.yahoo.com/" class="D(ib) H(47px) W($bigLogoWidth) Bgi($logoImage) Bgr(nr) Bgz($bigLogoWidth) Reader-open_Bgz($mediumLogoWidth) mini-header_Bgz($mediumLogoWidth) search-mini-header_Bgz($searchLogoWidth) Bgz($smallLogoWidth)!--sm1024 Bgp($bigLogoPos) Reader-open_Bgp($mediumLogoPos) mini-header_Bgp($mediumLogoPos) search-mini-header_Bgp($searchLogoPos) Bgp($searchLogoPos)!--sm1024 ua-ie7_Bgi($logoImageIe) ua-ie7_Mstart(-170px) ua-ie8_Bgi($logoImageIe) "   data-ylk="rspns:nav;t1:a1;t2:hd;sec:hd;itc:0;slk:logo;elm:img;elmt:logo;" tabindex="1">
        <b class="Hidden">Yahoo</b>
    </a>
</h1>

        <ul class="Pos(a) End(15px) List(n) Mt(3px) Reader-open_Mt(1px) mini-header_Mt(1px)" role="menubar">
            
    <li class="Pos(r) Fl(start) Mend(26px)">
        <a id="uh-signin" class="Bdc($signInBtn) Bdrs(5px) Bds(s) Bdw(2px) Bgc($signInBtn):h C($signInBtn) C(#fff):h D(ib) Ell Fz(14px) Fw(b) Py(2px) Mt(5px) Ta(c) Td(n):h Miw(78px) H(18px)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;" tabindex="4"><div class="Miw(78px) Ta(c) Pos(a) T(0) Lh($userNavTextLh)">Sign in</div></a>
    </li>


            
    <li id="uh-notifications" class="uh-menu uh-notifications Fl(start) D(ib) Mend(13px) Cur(p) ua-ie7_D(n) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <button class="uh-menu-btn Pos(r) Bd(0) Px(8px) Pt(1px) Lh(1.3)" aria-label="Notifications" aria-haspopup="true" aria-expanded="true" tabindex="5" title="Notifications">
            <i class="Lh($userNotifIconLh) C($mailBtn) Fz(28px) Grid-U Icon-Fp2 IconBell uh-notifications-icon"></i>
            <span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) End(-4px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-notification-count W(22px)"></span>
        </button>
        <div class="uh-notification-panel uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Ovy(a) Trs($menuTransition) Cur(d) V(h) Op(0) Mah(0) uh-notifications:h_V(v) uh-notifications:h_Op(1) uh-notifications:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" aria-label="Notifications" role="menu" tabindex="5">
            <div class="uh-notifications-loading Py(16px) Px(24px) Ta(c)">
                <div class="W(30px) H(30px) Mx(a) My(12px) Bgz(30px) Bgi($animatedSpinner) ua-ie8_D(n)"></div>
                <span class="C($mailTstamp) D(n) ua-ie8_D(b)">Loading Updates</span>
            </div>
        </div>
    </li>


            
    <li id="uh-mail" class="uh-menu uh-mail D(ib) Mstart(14px) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <a id="uh-mail-link" href="https://mail.yahoo.com/" class="Pos(r) D(ib) Ta(s) Td(n):h uh-menu-btn" aria-label="Mail" aria-haspopup="true" role="button" aria-expanded="true" tabindex="6"><i class="Lh($userNavIconLh) W(28px) Mend(8px) C($mailBtn) Fz(30px) Grid-U Icon-Fp2 IconMail uh-mail-icon"></i><span class="Lh($userNavTextLh) D(ib) C($mailBtn) Fz(14px) Fw(b) Va(t)">Mail</span><span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) Start(16px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-mail-count W(22px)"></span>
        </a>
        
            <div class="uh-mail-preview uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Trs($menuTransition) V(h) Op(0) Mah(0) uh-mail:h_V(v) uh-mail:h_Op(1) uh-mail:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" role="menu" aria-label="Mail" tabindex="6">
                
                    <div class="Px(24px) Py(20px) Ta(c)">
                        <a class="C($menuLink) Fw(b) Td(n)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;">Sign in</a>  to view your mail
                    </div>
                
            </div>
        
    </li>


        </ul>
        <div id="uh-search" class="Pos(r) Mend(375px) Mstart(206px) search-mini-header_Mstart(112px) Mstart(120px)--sm1024 Maw(595px) H(40px) Reader-open_H(40px) mini-header_H(40px) search-mini-header_Maw(685px) Maw(745px)--sm1024 Va(t)">
    <form name="input" class="glowing glow" action="https://search.yahoo.com/search;_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-" method="get" data-assist-action="https://search.yahoo.com/search;_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-">
        <table class="Bdcl(s)" role="presentation">
            <tbody>
                <tr>
                    <td class="Pos(r) P(0) W(100%) H(40px) Reader-open_H(40px) mini-header_H(40px) D(tb) ua-safari_D(tbc) Tbl(f)">
                        <input id="uh-search-box" type="text" name="p" class="Pos(a) T(0) Start(0) Bd Bdc($searchBdrFoc):f Bdc($searchBdr) glow_Bdc($searchBtnGlow) Bgc(t) Bxsh(n) Fz(18px) M(0) O(0) Px(10px) W(100%) H(inh) Z(2) Bxz(bb) Bdrs(2px) ua-ie7_H(36px) ua-ie7_Lh(36px) ua-ie8_Lh(36px) ua-ie7_W(92%)" autofocus autocomplete="off" autocapitalize="off" aria-label="Search" tabindex="2">
                        
                        
                            <input type="hidden" data-fr="yfp-t-201" name="fr" value="yfp-t-201" />
                        
                    </td>
                    <td class="Miw(5px)"></td>
                    <td>
                        <button id="uh-search-button" type="submit" class="rapid-noclick-resp C(#fff) Fz(16px) H(40px) W(140px) Reader-open_H(40px) mini-header_H(40px) Px(26px) Py(0) Whs(nw) Bdrs(3px) Bdw(0) Bgc($searchBtn) glow_Bgc($searchBtnGlow) Bxsh($searchBtnShadow) glow_Bxsh($searchBtnShadowGlow) Lh(1)"   data-ylk="rspns:nav;t1:a1;t2:srch;sec:srch;slk:srchweb;elm:btn;elmt:srch;tar:search.yahoo.com;tar_uri:/search;itc:0;" tabindex="3">
                        <span class="search-default-label Fw(500) ">Search Web</span>
                        <span class="search-short-label Fw(500) D(n) ">Search</span>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    
    <ul id="Skip-links" class="Pos(a)">
        <li><a href="#Navigation" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Navigation</a></li>
        <li><a href="#Main" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Main content</a></li>
        <li><a href="#Aside" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Right rail</a></li>
    </ul>
    
</div>

    <script>
!function(){var e,t,a,n,c=window,o=document,r=o.getElementById("uh-search-box"),g=function(c){try{a=c.keyCode,n=c.target?c.target:c.srcElement,e=r.value.length,n===r&&0===e&&a>=32&&40>=a?r.blur():"EMBED"===n.tagName||"OBJECT"===n.tagName||"INPUT"===n.tagName||"TEXTAREA"===n.tagName||c.altKey||c.metaKey||c.ctrlKey||!(32>a||a>40)||9==a||13==a||16==a||27==a||(e&&(r.setSelectionRange?r.setSelectionRange(e,e):r.createTextRange&&(t=r.createTextRange(),t.moveStart("character",e),t.select())),r.focus())}catch(c){}};r&&(c.addEventListener?c.addEventListener("keydown",g,!1):c.attachEvent&&o.attachEvent("onkeydown",g),setTimeout(function(){r.focus()},500))}();
</script>



    </div>
</div>

 </div> </div> </div>            <!-- App close -->
            </div>
        </div>
    </div><!-- Header -->
    <div id="Stencil">
    <div id="Reader" class="js-viewer-viewerwrapper">
        <div class="ReaderWrap">
            <div class="InnerWrap">
                <div id="applet_p_50000101" class=" App_v2  M-0 js-applet viewer Zoom-1  App-Chromeless " data-applet-guid="p_50000101" data-applet-type="viewer" data-applet-params="_suid:50000101" data-i13n="auto:true;sec:app-view" data-i13n-sec="app-view" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div id="hl-viewer" class="js-slider Hl-Viewer Ov-h desktop" role="document" aria-labelledby="modal-header" aria-live="polite" aria-hidden="true" tabindex="-1">
    
    
        <button id="closebtn" class="Viewer-Close-Btn Bdr-0 D-b Fw(b) js-close-content-viewer rapid-noclick-resp Z-7">
            <i class="Icon-Fp2 IconActionCross"></i>
            <b class="Td(n) Hidden">Close this content, you can also use the Escape key at anytime</b>
        </button>
        <div class="viewer-wrapper Ov-h mega-modal">
            <div class="">
                
                    <div class="content-container"></div>
                
                
                
                
                <div class="footer-ads">
                    <div id="hl-ad-FSRVY-0" class="D-ib">
                        <div id="hl-ad-FSRVY-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT9-0" class="D-ib">
                        <div id="hl-ad-FOOT9-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT-0" class="D-ib">
                        <div id="hl-ad-FOOT-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                </div>
                <span id="viewer-end" tabindex="0"></span>
            </div>
        </div>
    

    


</div>

 </div> </div> </div>            <!-- App close -->
            </div>
            </div>
        </div>
        <div id="Overlay"></div>
        <div class="Reader-bg"></div>
    </div>
</div>
    
    <div id="Masterwrap" class="slider js-viewer-pagewrapper">
        <div class="page">
            <div id="Billboard-ad">
                                                    <div id="my-adsMAST" class="D-n">
                    <div id="my-adsMAST-iframe">
                        
                        <!-- MAST has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136sagpdq(gid%24BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st%241454780299612233,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125vh2sdv,aid%24GmQlIGKLDz4-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=MAST)"></noscript>
                    </div>
                </div>
                                <div id="ad-north-base" class=""><div id="ad-north" class="BillboardAd"></div></div>
            </div>
            
            <div class="Col1" role="navigation" id="Navigation" tabindex="-1">
                <div class="Col1-stack" data-plugin="sticker" data-sticker-top="75px">
                                <div id="applet_p_50000195" class=" M-0 js-applet navrailv2 Zoom-1  Mb-20 " data-applet-guid="p_50000195" data-applet-type="navrailv2" data-applet-params="_suid:50000195" data-i13n="auto:true;sec:nav" data-i13n-sec="nav" data-ylk="rspns:nav;itc:0;t1:a2;t2:nav;elm:itm;"> <!-- App open -->
        <!-- src=mdbm type=popular mod=td-applet-navlinks-atomic ins=p_50000195 ts=1454780065.4034 --><div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

    

    

    <ul class="navlinks-list D(tbc) W(134px) Mstart(6px)">
    
        <li class="navlink  Py(5px)">
            <a href=https://mail.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMail C($MailNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Mail</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://news.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconNews C($NewsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">News</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://sports.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconSports C($SportsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Sports</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://finance.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFinance C($FinanceNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Finance</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/autos/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconAutos C($AutosNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Autos</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/celebrity/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconCelebrity C($CelebrityNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Celebrity</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://shopping.yahoo.com class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconShopping C($ShoppingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Shopping</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/movies/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMovies C($MoviesNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Movies</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/politics/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconPolitics C($PoliticsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Politics</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/beauty/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconBeauty C($BeautyNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Beauty</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Style</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Tech</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/travel/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTravel C($TravelNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Travel</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh770 Py(5px)">
            <a href=https://www.yahoo.com/food/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFood C($FoodNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Food</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh770 Py(5px)">
            <a href=https://www.yahoo.com/health/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconHealth C($HealthNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Health</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh860 Py(5px)">
            <a href=https://www.yahoo.com/makers/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMakers C($MakersNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Makers</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh860 Py(5px)">
            <a href=https://www.yahoo.com/parenting/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconParenting C($ParentingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Parenting</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">TV</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://www.yahoo.com/realestate/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconRealEstate C($RealEstateNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Real Estate</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">More on Yahoo</span>
            </a>
        </li>
    
        <li class="D(b)--maxh900 Py(5px) js-navlinks-seemore Pos(r) D(n) O(0)"  role="button" tabindex="0" aria-haspopup="true" aria-label="More Navlinks">
            <i class="Icon-Fp2 IconStreamShare Va(m) Pend(6px) Fz(22px) D(tbc) Miw(29px) Ta(c) C($MoreNav)"></i>
            <span class="Va(m) D(tbc) Fz(14px) Fw(b) V(h)--sm1024 C($navlink) Col1-stack:h_V(v) C($navlinkHover):h Cur(p)">More</span>

            <div class="js-navlinks-seemore-menu D(n) js-navlinks-menu-open_D(ib) Va(b) Pos(a) B(-14px) Start(60px) Pstart(60px)">
                <ul class="Bxsh($menuShadow) Bd($submenuBdr) Bdrs(3px) Bgc(#fff) Cur(d) Py(13px) Px(20px) Miw(140px)">
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Style</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Tech</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/travel/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTravel C($TravelNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Travel</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh770">
                        <a href=https://www.yahoo.com/food/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconFood C($FoodNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Food</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh770">
                        <a href=https://www.yahoo.com/health/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconHealth C($HealthNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Health</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh860">
                        <a href=https://www.yahoo.com/makers/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconMakers C($MakersNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Makers</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh860">
                        <a href=https://www.yahoo.com/parenting/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconParenting C($ParentingNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Parenting</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">TV</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://www.yahoo.com/realestate/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconRealEstate C($RealEstateNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Real Estate</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b) js-navlinks-lastitem">
                            <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">More on Yahoo</span>
                        </a>
                    </li>
                
                </ul>
            </div>
        </li>
    </ul>


 </div> </div> </div> <!-- yrid: oS62VgAAAADpPgAA8prKCQ.. | edgepipe: 0 | authed: 0 | ynet: 1 | ssl: 1 | spdy: 0 | ytee: 0 | bucket: 201 | colo: bf1 | device: desktop | bot: 0 | environment: prod | lang: en-US | partner: default | site: fp | region: US | intl: us | tz: America/Los_Angeles | mode: fallback -->
<!-- tdserver-my.fp.production.manhattan.bf1.yahoo.com (pprd3-node5059-lh1.manhattan.bf1.yahoo.com) | Served by ynodejs | Sat Feb 06 2016 17:34:25 GMT+0000 (UTC) -->            <!-- App close -->
            </div>                           <div id="my-adsTXTL" class="Mt-10 Mb-20 Pt-20 Pos-r ad-txtl D-n">
                    <div class="Mx-a" id="my-adsTXTL-iframe">
                        <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<style>
	#fc_align{
		position: static !important;
		width: 120px !important;
		margin: auto !important;
	}
</style>
<!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136sagpdq(gid%24BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st%241454780299612233,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413agn4lc3,aid%242BEmIGKLDz4-,bi%242275188051,agp%243476502551,cr%244451009051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=TXTL)"></noscript>
                    </div>
                </div>
                </div>
            </div>
            <div class="Col2">
                <div id="Main" class="Col2-stack" role="content" tabindex="-1">
                    <div id="Banner">
                        
                    </div><!-- Banner -->
                                <div id="applet_p_50000173" class=" M-0 js-applet streamv2 Zoom-1  Mb-20 " data-applet-guid="p_50000173" data-applet-type="streamv2" data-applet-params="_suid:50000173" data-i13n="auto:true;sec:strm;useViewability:true" data-i13n-sec="strm" data-ylk="t1:a3;t2:strm;t3:ct;cat:default;rspns:nav;itc:0;" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div data-uuid-list="p_50000173">
    

    
    <ul class=" js-stream-tmpl-items js-stream-dense  js-stream-hover-enable My(0) Mb(0) Wow(bw)" id="Stream">
    
        
    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Tainan|WIKIID:Residential_area|YCT:001000780|YCT:001000798|YCT:001000655"  data-uuid="2de31d18-3dfd-3673-88b9-80a1424abe40" data-cauuid="2de31d18-3dfd-3673-88b9-80a1424abe40" data-type="article" data-cluster="2de31d18-3dfd-3673-88b9-80a1424abe40"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Pt(12px) Pb(17px)"><div class="js-stream-roundup js-stream-roundup-filmstrip Pos(r) Wow(bw) Cf">
    <div class="strm-headline Pos(r)">
        <a href="http://news.yahoo.com/5-dead-hundreds-rescued-injured-quake-rattles-taiwan-050238778.html" class="Pos(r) D(ib) Z(1) js-stream-content-link js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:2de31d18-3dfd-3673-88b9-80a1424abe40;refcnt:4;subsec:306;imgt:ss;g:2de31d18-3dfd-3673-88b9-80a1424abe40;aid:id-3593644;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:img;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
        
        <img src="/sy/uu/api/res/1.2/4MMUpwIfPbOHmKYS_vWb_Q--/Zmk9c3RyaW07aD0yNzQ7cHlvZmY9MDtxPTk1O3c9NzAyO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/020616/images/smush/Quake2ipad_ipad_1454757119.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
        
        <div class="Pos(a) T(0) Start(0) W(100%) H(100%) Ov(h)">
            <div class="Pos(a) B(0) M(15px) Mb(11px) Mend(120px) C(#fff) Z(1)">
                <h2 class="js-stream-item-title Fz(21px) Fw(b) Mb(3px) Lh(24px) Td(u):h">Death toll rises in powerful Taiwan quake</h2>
                <span class="stream-summary Fz(13px) C(#d9d9db) Mend(5px)">A 6.4-magnitude earthquake kills at least 14 people and leaves more than 100 missing.</span><span class="Fw(b) D(ib) Va(b) Lh(18px) Td(u):h">477 injured Â»</span></div>
            <div class="strm-img-gradient W(100%) H(100%) rounded-img"></div>
        </div>
        </a>
        <div class="Pos(a) End(13px) T(10px) W(30px) Mend(2px) Ta(end) Z(2)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) C(white)" data-cmntnum="306"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow) js-stream-comment-hidden">306</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C(white) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:2de31d18-3dfd-3673-88b9-80a1424abe40;refcnt:4;subsec:306;imgt:ss;g:2de31d18-3dfd-3673-88b9-80a1424abe40;aid:id-3593644;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) Tsh($comment_shadow) ActionComments:h_C($signin_blue)"></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C(white) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) Tsh($comment_shadow)"></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C(white) P(14px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:2de31d18-3dfd-3673-88b9-80a1424abe40;refcnt:4;subsec:306;imgt:ss;g:2de31d18-3dfd-3673-88b9-80a1424abe40;aid:id-3593644;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) Tsh($comment_shadow)"></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C(white) Tsh($comment_shadow) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
    </div>
    <ul class="P(0) Mt(7px) Mstart(-2px) Mend(7px) Fz(12px) Whs(nw) Lts(-.31em)">
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="http://news.yahoo.com/sex-abuse-survivor-takes-leave-absence-vatican-panel-123703164.html?nf=1" data-uuid="fd1977e5-4383-382d-b6b6-781fd0318c97" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:2;bpos:1;pos:2;ss_cid:2de31d18-3dfd-3673-88b9-80a1424abe40;refcnt:4;subsec:306;imgt:ss;g:fd1977e5-4383-382d-b6b6-781fd0318c97;aid:id-3593705;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/m2e4LmUrHuR5fMJjRbYDtA--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/020616/images/smush/saunderspope2_ipad_1454777799.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Victim leaves Pope Francis's sex abuse panel</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/food/super-bowl-food-how-to-enjoy-game-day-without-204035670.html" data-uuid="ee12d0e4-70d9-3172-955f-58d8f583699e" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:3;bpos:1;pos:3;ss_cid:2de31d18-3dfd-3673-88b9-80a1424abe40;refcnt:4;subsec:306;imgt:ss;g:ee12d0e4-70d9-3172-955f-58d8f583699e;aid:id-3593707;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/3lYEC0uuWqrJvssYP9b1JQ--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/020616/images/smush/guacamole_ipad_1454777896.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Enjoy game day without killing your diet</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://gma.yahoo.com/veteran-wins-super-bowl-surprise-lifetime-150051195.html" data-uuid="f11847e5-2436-3ab4-b035-9cfe313d9a61" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:4;bpos:1;pos:4;ss_cid:2de31d18-3dfd-3673-88b9-80a1424abe40;refcnt:4;subsec:306;imgt:ss;g:f11847e5-2436-3ab4-b035-9cfe313d9a61;aid:id-3593699;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/gsV1rN.9vjA9PTkTl7YbpA--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/020616/images/smush/Reagan3ipad_ipad_1454757489.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Marine vet wins major Super Bowl surprise</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://finance.yahoo.com/news/drug-pricing-161315534.html" data-uuid="e0b2349e-2c54-303b-8590-c78349b5c0ce" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:5;bpos:1;pos:5;ss_cid:2de31d18-3dfd-3673-88b9-80a1424abe40;refcnt:4;subsec:306;imgt:ss;g:e0b2349e-2c54-303b-8590-c78349b5c0ce;aid:id-3593701;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/YeLmChc5tbBrfuLQHVUecg--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/020616/images/smush/Martin-Shkreli2ipad_ipad_1454762763.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Martin Shkreli's greed hides biotech's bigger problem</h3>
            </a>
        </li>
        
        
    </ul>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Angela_Stanford|YCT:001000742"  data-uuid="def0ed47-4101-3f88-a0c9-47836eb68b51" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://finance.yahoo.com/news/stanford-professor-says-eliminating-2-153500252.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;subsec:398;imgt:ss;g:def0ed47-4101-3f88-a0c9-47836eb68b51;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000026450S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/uu/api/res/1.2/9DHJXh8UtWdkaO0mhJZUyQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/A_Stanford_professor_says_eliminating-73c45c995a493b086e5e5ed869f9a655" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_us) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://finance.yahoo.com/news/stanford-professor-says-eliminating-2-153500252.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;subsec:398;imgt:ss;g:def0ed47-4101-3f88-a0c9-47836eb68b51;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000026450S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>A Stanford professor says eliminating 2 phrases from your vocabulary can make you more successful</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">(Francisco Osorio/Flickr) Your language shapes the way you approach your goals. 
The way you speak not only affects how others perceive you; it also has the potential to shape your behavior.
Swapping one word for another could make all the difference</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Business Insider</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="398"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">398</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:2;cposy:6;bpos:1;pos:1;subsec:398;imgt:ss;g:def0ed47-4101-3f88-a0c9-47836eb68b51;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000026450S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:2;cposy:6;bpos:1;pos:1;subsec:398;imgt:ss;g:def0ed47-4101-3f88-a0c9-47836eb68b51;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000026450S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad js-video-content Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=Lsp7dOkGIS9uO.VSRnNwM8lQnAWo5a93Aah.dYEAZCC8dnzTbOe4zY8UBS5NNEUlgaD9dEDvm.70JrsB2dshcVL6yRMv_OsdQx45ao9XwIgvUouPxhlfsShZZNG9J.8FCQZImS1pyZyherC_27Q1IvQLT9ZlPYyBqoLfrAVKR7PIZN0Uqu8ANKWjJV4ZtqFwTX1W0i9lzQxre9fAC1Zc3FX_olHcm1qUv0DDoNh4VqZ0QhHH5G4vukHLkY0gtj8ljFSOAT6Rhqab5o9cjWeetwQ7U7vGBpZ.hRxiNi.a0igSysJYoSV0xzwt0nWJ0L.JsniLYyTcXtAaE.PNEEJpNxxYszW7E6GbS75VdLqxRn9ZA9sdBpmeKKZU7HH4t5I-&amp;ap=3" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15mmpnbbb(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299691000,li$0,cr$31649542148,dmn$samsung.com,srv$3,exp$1454787499691000,ct$27,v$1.0,adv$1022324,pbid$1,seid$4250754))&amp;r=1454780299691&amp;al=$(AD_FEEDBACK)" data-uuid="31649542148">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=jYb2XCwGIS.LfGdqWOVE5lKbOnJSfTXC4kECT.I599.SPVlzziGmx8KL9XnG4NpyXPoJxYu2mVDjABIvAUAn1LYFlntWqU_8ObdfJJKUVJnGI5PKCEJ1qMTg.4_kC8PRrgTn4pqAXBA1mD1aBxTSRPaDVK5lXRtwUJZXJxyzeJ1RKO15TC5nBmZNeTfZ.sksCpama_CDnaAK0OqOyMCBK1oefMhngXdCDZzhH8s59aF5vaLlbKLqili0Rqf.IZM7nT1eIghOFiLhEKna811ToxksTeyVKjKj03zXTdX6wseRgjCGGZKLDTi58ZGu2T1.WDe_kvh3mzhOHUo0lzdScq4LVtw7TkdIe7FJNf7AmH4K2ttWeFg8_.b7LRMFMJtqmqAP3bmEW2CmFUtmBgCM5ZBLwmnPFLbQg4etz4DC6Hd0l_1yV0RH3VT6HpW3Z3iryVH4P93uy3P3k2IUg94G8mYtzkkN8UbrajDgzA--%26lp=http%3A%2F%2Fwww.samsung.com%2Fus%2Fexplore%2Flatest-galaxy-smartphones%2F%3Fcid%3Dppc-" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i) js-video-target js-video-image" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:ct;g:31649542148;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/uu/api/res/1.2/_O6GOJCUrh8ogAnyJkodbA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1446563568573-3214.jpg.cf.jpg" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img" alt="">
                </a>
            </div>
            
            <div class="strm-right Pos(r) Mstart(29%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:sp;g:31649542148;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:info;g:31649542148;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=jYb2XCwGIS.LfGdqWOVE5lKbOnJSfTXC4kECT.I599.SPVlzziGmx8KL9XnG4NpyXPoJxYu2mVDjABIvAUAn1LYFlntWqU_8ObdfJJKUVJnGI5PKCEJ1qMTg.4_kC8PRrgTn4pqAXBA1mD1aBxTSRPaDVK5lXRtwUJZXJxyzeJ1RKO15TC5nBmZNeTfZ.sksCpama_CDnaAK0OqOyMCBK1oefMhngXdCDZzhH8s59aF5vaLlbKLqili0Rqf.IZM7nT1eIghOFiLhEKna811ToxksTeyVKjKj03zXTdX6wseRgjCGGZKLDTi58ZGu2T1.WDe_kvh3mzhOHUo0lzdScq4LVtw7TkdIe7FJNf7AmH4K2ttWeFg8_.b7LRMFMJtqmqAP3bmEW2CmFUtmBgCM5ZBLwmnPFLbQg4etz4DC6Hd0l_1yV0RH3VT6HpW3Z3iryVH4P93uy3P3k2IUg94G8mYtzkkN8UbrajDgzA--%26lp=http%3A%2F%2Fwww.samsung.com%2Fus%2Fexplore%2Flatest-galaxy-smartphones%2F%3Fcid%3Dppc-" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:ad;g:31649542148;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class="Pos(r) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>It's Not a Phone, It's a Galaxy: Dual-Edge Display</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u>
                    </a>
                </h3>
                <div>
                    <p class="Pos(r) stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">With the dual-edge display on the Galaxy S6 edge and edge+, beauty has no bounds. It's not a phone, it's a Galaxy.</p>
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=jYb2XCwGIS.LfGdqWOVE5lKbOnJSfTXC4kECT.I599.SPVlzziGmx8KL9XnG4NpyXPoJxYu2mVDjABIvAUAn1LYFlntWqU_8ObdfJJKUVJnGI5PKCEJ1qMTg.4_kC8PRrgTn4pqAXBA1mD1aBxTSRPaDVK5lXRtwUJZXJxyzeJ1RKO15TC5nBmZNeTfZ.sksCpama_CDnaAK0OqOyMCBK1oefMhngXdCDZzhH8s59aF5vaLlbKLqili0Rqf.IZM7nT1eIghOFiLhEKna811ToxksTeyVKjKj03zXTdX6wseRgjCGGZKLDTi58ZGu2T1.WDe_kvh3mzhOHUo0lzdScq4LVtw7TkdIe7FJNf7AmH4K2ttWeFg8_.b7LRMFMJtqmqAP3bmEW2CmFUtmBgCM5ZBLwmnPFLbQg4etz4DC6Hd0l_1yV0RH3VT6HpW3Z3iryVH4P93uy3P3k2IUg94G8mYtzkkN8UbrajDgzA--%26lp=http%3A%2F%2Fwww.samsung.com%2Fus%2Fexplore%2Flatest-galaxy-smartphones%2F%3Fcid%3Dppc-" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:ad;g:31649542148;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Samsung</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:op;g:31649542148;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000780|YCT:001000667" data-offnet="1" data-uuid="e29fa585-240d-3798-8041-e7af7a00e06e" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://www.cbsnews.com/news/11-year-old-boy-convicted-of-killing-8-year-old-girl/" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:4;cposy:8;bpos:1;pos:1;imgt:ss;g:e29fa585-240d-3798-8041-e7af7a00e06e;ct:1;pkgt:orphan_img;grpt:topNewsStoryCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000026480S00009;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/1hSIGwvKkMBl0im5JnVxSg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://markvii.zenfs.com/mcifeed/mcifeed_2f2023f1-a177-4411-9ee3-73212464cdc1--1122289780.jpeg" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_us) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://www.cbsnews.com/news/11-year-old-boy-convicted-of-killing-8-year-old-girl/" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:4;cposy:8;bpos:1;pos:1;imgt:ss;g:e29fa585-240d-3798-8041-e7af7a00e06e;ct:1;pkgt:orphan_img;grpt:topNewsStoryCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000026480S00009;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>â11-year-old boy convicted of killing 8-year-old girl</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">WHITE PINE, Tenn. -- An 11-year-old boy in Tennessee has been found guilty of murdering an eight-year-old girl after she and her sister refused to let him see their puppies, a judge said. Benny Tiller&#39;s grandmother told CBS affiliate WVLT he was found guilty of first degree murder in the October 4, 2015 death of his neighbor, McKayla Dyer. Jefferson County Juvenile Court judge Dennis &quot;Will&quot; Roach II sentenced the boy to state custody until he turns 19. In his order, Roach said the state should use all reasonable resources to determine why Benny shot the girl, and he should be treated and rehabilitated so this never happens again. &quot;A child who commits first-degree murder cannot be willy-nilly</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">CBS News</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:4;cposy:8;bpos:1;pos:1;imgt:ss;g:e29fa585-240d-3798-8041-e7af7a00e06e;ct:1;pkgt:orphan_img;grpt:topNewsStoryCluster;cnt_tpc:U.S.;r:4000026480S00009;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Salma_Hayek|YCT:001000069|YCT:001000031|YCT:001000395|YCT:001000076|YCT:001000427"  data-uuid="a319dcfd-3515-397c-b83e-5778688e9034" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="https://www.yahoo.com/style/salma-hayek-didnt-plan-wearing-133023197.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:5;cposy:9;bpos:1;pos:1;subsec:213;imgt:ss;g:a319dcfd-3515-397c-b83e-5778688e9034;ct:1;pkgt:orphan_img;grpt:topNewsStoryCluster;cnt_tpc:Celebrity;elm:img;elmt:ct;r:4000026600S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/uu/api/res/1.2/Dke3GBXI7FlJoWwspf0ulA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/cd/resizer/2.0/FIT_TO_WIDTH-w650/155aa19f0052dd292131cc09e8f62837b43a11f1.jpg.cf.jpg" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_celebrity) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="celebrity">Celebrity</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="https://www.yahoo.com/style/salma-hayek-didnt-plan-wearing-133023197.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:5;cposy:9;bpos:1;pos:1;subsec:213;imgt:ss;g:a319dcfd-3515-397c-b83e-5778688e9034;ct:1;pkgt:orphan_img;grpt:topNewsStoryCluster;cnt_tpc:Celebrity;elm:hdln;elmt:ct;r:4000026600S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Salma Hayek Didn&#39;t Plan On Wearing This Embarrassing And Suggestive Shirt To The ER</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Salma Hayek learned the meaning of the word âembarrassingâ when she endured an on-set injury.  Hayek never revealed the movie sheâs filming, but sheâs still sort of working on a film called Sausage Party.  Nahh, that movie is actually an animated childrenâs film starring Seth Rogen and James Franco.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">UPROXX</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="213"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">213</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:5;cposy:9;bpos:1;pos:1;subsec:213;imgt:ss;g:a319dcfd-3515-397c-b83e-5778688e9034;ct:1;pkgt:orphan_img;grpt:topNewsStoryCluster;cnt_tpc:Celebrity;r:4000026600S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:5;cposy:9;bpos:1;pos:1;subsec:213;imgt:ss;g:a319dcfd-3515-397c-b83e-5778688e9034;ct:1;pkgt:orphan_img;grpt:topNewsStoryCluster;cnt_tpc:Celebrity;r:4000026600S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Donald_Trump|WIKIID:Sanctions_against_Iran|YCT:001000671" data-offnet="1" data-uuid="3b3c3cd4-7242-31de-98b9-024e4b0baf2b" data-type="article" data-cluster="3b255110-3506-4045-9002-dff258d44655"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://www.salon.com/2016/02/05/donald_trumps_iran_idiocy_the_interview_that_should_have_ended_his_candidacy_once_and_for_all/" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:6;cposy:10;bpos:1;pos:1;ss_cid:3b255110-3506-4045-9002-dff258d44655;refcnt:2;imgt:ss;g:3b3c3cd4-7242-31de-98b9-024e4b0baf2b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000024190S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/h.a2NxBVBdWaeubmjhKRkA--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/3149c32afee604ddfe4d1109c977758f" height="190" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.salon.com/2016/02/05/donald_trumps_iran_idiocy_the_interview_that_should_have_ended_his_candidacy_once_and_for_all/" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:6;cposy:10;bpos:1;pos:1;ss_cid:3b255110-3506-4045-9002-dff258d44655;refcnt:2;imgt:ss;g:3b3c3cd4-7242-31de-98b9-024e4b0baf2b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000024190S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Donald Trumpâs Iran idiocy: The interview that should have ended his candidacy once and for all</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Donald Trump gave an interview this week allÂ of his potential supporters should watch. In his own words, Trump lays bare the very reasons why he would be such a disastrous choice for president. The interview with Anderson Cooper on Thursday, which can be seen here, Â took place in New Hampshire where Trump is campaigning for the upcoming Republican primary in New Hampshire, and the setting included a small group of ordinary New Hampshire voters. The topic turns to President Obamaâs recent nuclear agreement with Iran.Â  Trump unwittingly displays for all to see that he simply does not understand the most basic elements of the agreement. Trump proclaims his familiar boast that he is the best deal-maker</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Salon.com</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://finance.yahoo.com/news/voter-confronts-donald-trump-could-020439538.html" data-uuid="da379cce-b44e-3565-8646-9f8ae4ab6994" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:6;cposy:11;bpos:1;pos:2;ss_cid:3b255110-3506-4045-9002-dff258d44655;refcnt:2;imgt:ss;g:da379cce-b44e-3565-8646-9f8ae4ab6994;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000024190S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/J9Z7vhDbmNU_quHXk2RNyg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/Voter_confronts_Donald_Trump_Could-146ec38231307cefa311fd346ee03085" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Voter confronts Donald Trump: Could my daughters 'look up to a President Trump as a role model'?</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Business Insider</span></div></a>
            
            
            
                <a href="http://news.yahoo.com/video/donald-trump-fights-backs-hampshire-062910598.html" data-uuid="fa48c4b9-7390-37ad-88f0-ccfd8f71411a" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:6;cposy:12;bpos:1;pos:3;ss_cid:3b255110-3506-4045-9002-dff258d44655;refcnt:2;imgt:ss;g:fa48c4b9-7390-37ad-88f0-ccfd8f71411a;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000024190S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/7ORoxFbPh5OGZCsodGL9IA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/video/video.abcnewsplus.com/214e08e7c5eb532491099fe005a48fec" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Donald Trump Fights Backs in New Hampshire</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">ABC News Videos</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:10;bpos:1;pos:1;ss_cid:3b255110-3506-4045-9002-dff258d44655;refcnt:2;imgt:ss;g:3b3c3cd4-7242-31de-98b9-024e4b0baf2b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;r:4000024190S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Peyton_Manning|YCT:001000395" data-offnet="1" data-uuid="d85bc4f2-9fa5-3245-b47a-0624fa7ad902" data-type="article" data-cluster="34037eec-548a-41ac-8c14-efd403fd2a5d"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://www.yardbarker.com/nfl/articles/peyton_manning_wife_ashley_did_receive_drugs_from_guyer_institute/s1_127_20293228" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:7;cposy:13;bpos:1;pos:1;ss_cid:34037eec-548a-41ac-8c14-efd403fd2a5d;refcnt:2;imgt:ss;g:d85bc4f2-9fa5-3245-b47a-0624fa7ad902;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:img;elmt:ct;r:4000024300S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/_jqcdyiwhp.PUFodILsrSw--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/2ffab0248577ecb951f3867f3479b762')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_sports) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="sports">Sports</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.yardbarker.com/nfl/articles/peyton_manning_wife_ashley_did_receive_drugs_from_guyer_institute/s1_127_20293228" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:7;cposy:13;bpos:1;pos:1;ss_cid:34037eec-548a-41ac-8c14-efd403fd2a5d;refcnt:2;imgt:ss;g:d85bc4f2-9fa5-3245-b47a-0624fa7ad902;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:hdln;elmt:ct;r:4000024300S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><span>Manning wife Ashley did receive drugs from Guyer Institute</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">A representative for Peyton Manning has confirmed that Manningâs wife Ashley received medication from the Guyer Institute in Indianapolis. Charlie Sly, the source of the human growth human allegations against Manning in Al Jazeeraâs bombshell report, said while he was being secretly recorded that Ashley Manning received shipments of HGH. Ari Fleischer, a crisis management expert who has been hired by Manning, confirmed to the Washington Post that Ashley received shipments of medication from the Guyer Institute.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Yardbarker</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.cbssports.com/nfl/eye-on-football/25474417" data-uuid="11e30329-b126-3e0f-b20a-e4c8bb3b6be8" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:7;cposy:14;bpos:1;pos:2;ss_cid:34037eec-548a-41ac-8c14-efd403fd2a5d;refcnt:2;imgt:ss;g:11e30329-b126-3e0f-b20a-e4c8bb3b6be8;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:rhdln;elmt:ct;r:4000024300S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/F6kGd2URqGyO0UaKmBUfAA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/aa391d6932ef33eed31e058681904904')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Report: Manning hired investigators to probe Al Jazeera documentary</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">CBS Sports</span></div></a>
            
            
            
                <a href="http://sports.yahoo.com/news/mannings-legal-team-looked-documentary-021927763--nfl.html" data-uuid="be1c13eb-8517-3d65-aff0-fef84b98dea0" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:7;cposy:15;bpos:1;pos:3;ss_cid:34037eec-548a-41ac-8c14-efd403fd2a5d;refcnt:2;imgt:ss;g:be1c13eb-8517-3d65-aff0-fef84b98dea0;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:rhdln;elmt:ct;r:4000024300S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/II5_Aib9rqNkxtnHI7yqKg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_us/Sports/ap/201602041116405685008')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Manning&#39;s legal team looked into documentary</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">The Associated Press</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:13;bpos:1;pos:1;ss_cid:34037eec-548a-41ac-8c14-efd403fd2a5d;refcnt:2;imgt:ss;g:d85bc4f2-9fa5-3245-b47a-0624fa7ad902;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;r:4000024300S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad  Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=.04VlkoGIS8fx7jCtAjQkNZ93biMIhjzEBWPf78NY_0zIfUfXZmkkLPFjhG3jYsbAioOOvvoumauSlSegdKbLBAQ9RTx24P2qfURa3tGmjrD6jm7bhePhA1ecKM5RbQL_mRw2NdzFg4f.rk5ybae8CXIxJrd9I4FgZqTwWYjgdy8fbkPdr6962ppxkI01Mlpu66SIBqF.MYZzp0gddEIyEBL7lCHpXLayOQauSu3FKl.AZvaJYpp.CLGlFaBzEVHbsQeHsX43C0hSOJXUFLPVYFneMwULzWdcJZSRcCitH0Bum6E9xKmxsnQ2jLPZJkbLSlaVuk.CR1tQNGwgAX28ewkgY3hJp9iSezH1BHAiiITiq8t0ITSBB9L6MqRdCN1vz1JGMHEbw--&amp;ap=8" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15opqhs2v(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299691000,li$0,cr$31637560506,dmn$startupf5.net,srv$3,exp$1454787499691000,ct$27,v$1.0,adv$1167625,pbid$1,seid$4250754))&amp;r=1454780299691&amp;al=$(AD_FEEDBACK)" data-uuid="31637560506">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=Q4e6X1kGIS.F2bw5Fl5zONZK09xGvgrKN06hoMTx391qaxN3PkbNEFEe2ZHsFvhiYxGJ1LAoCzVZsiYzkujHMVTzN_S4pmG4YEnFE.Z_1mfM7p5Y1MGGdgrjw7QI_Ul078QCpc8jF6L8SHm4m5Gp8eMoC94Whz1QzM_Q4KhVSqzMPlpdfhDJnHe_K9Stjl4CUeyiTY8DDHtPeDNHyLtqc2n7Evh3j25G6Mye0Q0Py5MPPRToF2r9bbVSce91selFgn9u5OJ652tGV1gSy5Xs5QX4DE1xZc0i.SAODe3ak9fh60aoVJyqR3KokYJmVELXBboU20o0LXP4tBFRzZjavwm_W9wKu8m1yNrTN2ydB9ku4DQbNNI8j7w9rljCG7Mn1no.tueMz9z6i4nYQaV9QbP6JwW6p9r6Xq4sEtBkRMZCGFPCC_hRkP0taNNqUecfIeHCFUBo_yxvgCpgVKV7TOumkYeT7dmVKM4Fxs5kqT2RD238W_x76Lw8DTj.G.ze8lg-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ffb65f9dd-93eb-4fc8-bba1-45bfb168335e%3Fad%3D1" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i)" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ct;g:31637560506;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/PFOxcvSyD_U_iM8HJh0vZw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1451049609080-5981.jpg.cf.jpg')" alt="">
                </a>
            </div>
            
            <div class="strm-right Pos(r) Mstart(29%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:sp;g:31637560506;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:info;g:31637560506;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=Q4e6X1kGIS.F2bw5Fl5zONZK09xGvgrKN06hoMTx391qaxN3PkbNEFEe2ZHsFvhiYxGJ1LAoCzVZsiYzkujHMVTzN_S4pmG4YEnFE.Z_1mfM7p5Y1MGGdgrjw7QI_Ul078QCpc8jF6L8SHm4m5Gp8eMoC94Whz1QzM_Q4KhVSqzMPlpdfhDJnHe_K9Stjl4CUeyiTY8DDHtPeDNHyLtqc2n7Evh3j25G6Mye0Q0Py5MPPRToF2r9bbVSce91selFgn9u5OJ652tGV1gSy5Xs5QX4DE1xZc0i.SAODe3ak9fh60aoVJyqR3KokYJmVELXBboU20o0LXP4tBFRzZjavwm_W9wKu8m1yNrTN2ydB9ku4DQbNNI8j7w9rljCG7Mn1no.tueMz9z6i4nYQaV9QbP6JwW6p9r6Xq4sEtBkRMZCGFPCC_hRkP0taNNqUecfIeHCFUBo_yxvgCpgVKV7TOumkYeT7dmVKM4Fxs5kqT2RD238W_x76Lw8DTj.G.ze8lg-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ffb65f9dd-93eb-4fc8-bba1-45bfb168335e%3Fad%3D1" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ad;g:31637560506;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class="Pos(r) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>Flash info : la France envahie par les algorithmes</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u>
                    </a>
                </h3>
                <div>
                    <p class="Pos(r) stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">AprÃ¨s les Ãtats-Unis, la France est maintenant envahie par de nouveaux algorithmes qui peuvent vous rendre trÃ¨s riches.</p>
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=Q4e6X1kGIS.F2bw5Fl5zONZK09xGvgrKN06hoMTx391qaxN3PkbNEFEe2ZHsFvhiYxGJ1LAoCzVZsiYzkujHMVTzN_S4pmG4YEnFE.Z_1mfM7p5Y1MGGdgrjw7QI_Ul078QCpc8jF6L8SHm4m5Gp8eMoC94Whz1QzM_Q4KhVSqzMPlpdfhDJnHe_K9Stjl4CUeyiTY8DDHtPeDNHyLtqc2n7Evh3j25G6Mye0Q0Py5MPPRToF2r9bbVSce91selFgn9u5OJ652tGV1gSy5Xs5QX4DE1xZc0i.SAODe3ak9fh60aoVJyqR3KokYJmVELXBboU20o0LXP4tBFRzZjavwm_W9wKu8m1yNrTN2ydB9ku4DQbNNI8j7w9rljCG7Mn1no.tueMz9z6i4nYQaV9QbP6JwW6p9r6Xq4sEtBkRMZCGFPCC_hRkP0taNNqUecfIeHCFUBo_yxvgCpgVKV7TOumkYeT7dmVKM4Fxs5kqT2RD238W_x76Lw8DTj.G.ze8lg-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ffb65f9dd-93eb-4fc8-bba1-45bfb168335e%3Fad%3D1" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ad;g:31637560506;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Preditrend</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:op;g:31637560506;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Hillary_Clinton|WIKIID:Bernie_Sanders|WIKIID:Iowa_Democratic_Party|YCT:001000661" data-offnet="1" data-uuid="714384e3-16a3-3c2b-a9ab-ac4f43bf7d78" data-type="article" data-cluster="3cbe3e6a-89a7-473f-a4d4-a7e1f365b7aa"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://www.usatoday.com/story/news/politics/onpolitics/2016/02/05/iowa-margin-between-clinton-sanders-shifts-errors-found/79889298/" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:9;cposy:17;bpos:1;pos:1;ss_cid:3cbe3e6a-89a7-473f-a4d4-a7e1f365b7aa;refcnt:2;imgt:ss;g:714384e3-16a3-3c2b-a9ab-ac4f43bf7d78;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000025730S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/LDU0VZT21zRKoFLI.heOYQ--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/4af44c0254e1db7c84897a076d982f70')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.usatoday.com/story/news/politics/onpolitics/2016/02/05/iowa-margin-between-clinton-sanders-shifts-errors-found/79889298/" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:9;cposy:17;bpos:1;pos:1;ss_cid:3cbe3e6a-89a7-473f-a4d4-a7e1f365b7aa;refcnt:2;imgt:ss;g:714384e3-16a3-3c2b-a9ab-ac4f43bf7d78;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000025730S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Iowa margin between Clinton, Sanders shifts as errors found</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Iowa Democratic Party officials are reviewing results from the Iowa caucuses and making updates where discrepancies have been found. Party Chairwoman Andy McGuire the day after Monday&#39;s caucuses said no review would be conductedÂ and that Democratic presidential candidate Hillary Clintonâs narrow victory over Bernie Sanders is final. &quot;Both the Sanders and Clinton campaigns have flagged a very small number of concerns for us, and we are looking at them all on a case-by-case basis,&quot;Â Iowa Democratic Party spokesman Sam Lau told the Register. The Register, too,Â has received numerous reports that the results announced in its precinct Monday night don&#39;t match what theÂ Iowa Democratic Party has posted on its official results websites.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">USA Today</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.cbsnews.com/news/hillary-clinton-calls-out-bernie-sanders-artful-smear-in-democratic-debate/?ftag=YHF4eb9d17" data-uuid="e0c07d9a-1165-39d2-937b-ebf9ab0813c3" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:9;cposy:18;bpos:1;pos:2;ss_cid:3cbe3e6a-89a7-473f-a4d4-a7e1f365b7aa;refcnt:2;imgt:ss;g:e0c07d9a-1165-39d2-937b-ebf9ab0813c3;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000025730S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/hMAKZxu17qzHWxYME3hrsw--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-us/homerun/cbsnews.cbs.com/b02da092139c7a40f1c00c20a609b76c')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Clinton to Sanders: Stop &quot;artful smear&quot; campaign</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">CBS News</span></div></a>
            
            
            
                <a href="https://www.yahoo.com/movies/live-blog-hillary-clinton-bernie-sanders-face-off-020429843.html" data-uuid="b869b586-7bab-3def-9cfa-49665bbb42c7" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:9;cposy:19;bpos:1;pos:3;ss_cid:3cbe3e6a-89a7-473f-a4d4-a7e1f365b7aa;refcnt:2;imgt:ss;g:b869b586-7bab-3def-9cfa-49665bbb42c7;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000025730S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/8qaqU.BBIOVoW2QptpFCdQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_US/Entertainment/Variety/bernie-sanders-hillary-clinton-democratic-debate.jpg.cf.jpg')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Live Blog: Hillary Clinton, Bernie Sanders Face Off in New Hampshire Debate</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Variety</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:17;bpos:1;pos:1;ss_cid:3cbe3e6a-89a7-473f-a4d4-a7e1f365b7aa;refcnt:2;imgt:ss;g:714384e3-16a3-3c2b-a9ab-ac4f43bf7d78;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;r:4000025730S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:United_States_Marine_Corps|WIKIID:1st_Marine_Division_%28United_States%29|YCT:001000705" data-offnet="1" data-uuid="18656f10-4bca-3732-b7cf-8ccd10757c81" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://www.military.com/daily-news/2016/02/05/japanese-soldiers-break-contact-engage-us-marines.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:10;cposy:20;bpos:1;pos:1;imgt:ss;g:18656f10-4bca-3732-b7cf-8ccd10757c81;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:img;elmt:ct;r:4000025410S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/qMuhDl.O.nVAc5_fioi4Ew--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/28fb46e048bbc169ab5dd47298e0fb53')" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_politics) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://www.military.com/daily-news/2016/02/05/japanese-soldiers-break-contact-engage-us-marines.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:10;cposy:20;bpos:1;pos:1;imgt:ss;g:18656f10-4bca-3732-b7cf-8ccd10757c81;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000025410S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Japanese Soldiers Break Contact, Engage US Marines</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">MARINE CORPS BASE CAMP PENDELTON, Calif - Over the roar of the wind, a voice yelled âcontact front!â Without hesitation, Sgt. First Class Yusuke Irie of the Japan Ground Self-Defense Forceâs Western Army Infantry Regiment Scout Sniper program sprinted to get on line and provide firepower. Surrounded by the sounds of gun fire and yelling, the soldiers alternate seamlessly between providing cover fire and bounding backward. Once out of sight and safe from enemy fire, United States Marine Sgt. Mason Wilhelmy, a pre-scout sniper instructor, 1st Marine Division Schoolâs Pre-Scout Sniper Course, called out âcease fire, cease fireâ and signals the soldiers to gather around him. The soldiers of the JGSDF</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Military.com</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:10;cposy:20;bpos:1;pos:1;imgt:ss;g:18656f10-4bca-3732-b7cf-8ccd10757c81;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;r:4000025410S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Oprah_Winfrey|YCT:001000123" data-offnet="1" data-uuid="2a43b26a-8a85-357c-965b-ff1fe5921c4a" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://www.housebeautiful.com/design-inspiration/celebrity-homes/a5190/oprah-colorado-house/" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:11;cposy:21;bpos:1;pos:1;imgt:ss;g:2a43b26a-8a85-357c-965b-ff1fe5921c4a;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Business;elm:img;elmt:ct;r:4000022490S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="homes"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/Gnu_NFimb1awzLkOmArvgw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/d4f39ca8b02a6c5d815377419d121763')" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_business) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="business">Business</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://www.housebeautiful.com/design-inspiration/celebrity-homes/a5190/oprah-colorado-house/" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:11;cposy:21;bpos:1;pos:1;imgt:ss;g:2a43b26a-8a85-357c-965b-ff1fe5921c4a;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Business;elm:hdln;elmt:ct;r:4000022490S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="homes"><span>Oprah&#39;s New Home Has Amenities We Could Barely Dream Up</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Looks like Oprah snuck in oneÂ more addition to her &quot;Favorite Things&quot; list before the end of 2015Â -Â a $14 million estateÂ located in Telluride, Colorado, which isÂ a major destination for ski aficionados.Â  The five-bedroom, six-and-a-half-bathroom mansion was built by a tech executiveÂ in 2001 and features a seven-person hot tub, a private path to a ski trail,Â and a 35-footÂ outdoor tower containing aÂ fire pit with 360-degree views of the San Sophia Mountain Range. The cherry on top, though, is the 56-foot &quot;wine mine&quot; accessible by mine cart with storageÂ for 1,600 bottles of wine. Take a tour of the home below and tryÂ not to think about how you&#39;ll be spending the coming winter months:Â </p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">House Beautiful</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:11;cposy:21;bpos:1;pos:1;imgt:ss;g:2a43b26a-8a85-357c-965b-ff1fe5921c4a;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Business;r:4000022490S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Johnny_Manziel|WIKIID:Deion_Sanders|YCT:001000290" data-offnet="1" data-uuid="e7a1961c-dd8d-3484-908f-661154b7abd4" data-type="article" data-cluster="bb6d8561-a956-498d-b511-4c77c115e779"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://ftw.usatoday.com/2016/02/deion-sanders-johnny-manziel-domestic-violence" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:12;cposy:22;bpos:1;pos:1;ss_cid:bb6d8561-a956-498d-b511-4c77c115e779;refcnt:2;imgt:ss;g:e7a1961c-dd8d-3484-908f-661154b7abd4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:img;elmt:ct;r:4000017750S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/CtxNkDGgXC5v62Wr6LRDpA--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/f6568e662b9e21ee0db3c4b726826709')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_sports) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="sports">Sports</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://ftw.usatoday.com/2016/02/deion-sanders-johnny-manziel-domestic-violence" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:12;cposy:22;bpos:1;pos:1;ss_cid:bb6d8561-a956-498d-b511-4c77c115e779;refcnt:2;imgt:ss;g:e7a1961c-dd8d-3484-908f-661154b7abd4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:hdln;elmt:ct;r:4000017750S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><span>Deion Sanders had the worst take on what&#39;s going on with Johnny Manziel</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">In response to news reports that Johnny Manziel was accused of assaulting his former girlfriend, Deion Sanders made a comment to Cleveland.com that seemed to miss the roots of domestic violence (hint: itâs not a display of intense love) and Manzielâs issues (also, likely not due to love.) âJohnnyâs in love,â Sanders told cleveland.com at an NFL Network media availability Thursday. âAnd Johnnyâs in love with something thatâs crippling him right now. I understand it. And it upsets me that grownups donât understand it. Because he feels as though this game donât love him, the people in this game donât love him, so the only thing that he associates with love is that thing thatâs really inflicting</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">For The Win</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://finance.yahoo.com/news/johnny-manziel-destroyed-value-nfl-143337910.html" data-uuid="b0d8c0cc-9956-3038-9505-0eded1e01a65" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:12;cposy:23;bpos:1;pos:2;ss_cid:bb6d8561-a956-498d-b511-4c77c115e779;refcnt:2;imgt:ss;g:b0d8c0cc-9956-3038-9505-0eded1e01a65;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:rhdln;elmt:ct;r:4000017750S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/dVyIEWhcIFmv0NZOu.1msg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/Johnny_Manziel_has_destroyed_his-f25c322296df22d2fe101324ccc2379b')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Johnny Manziel has 'destroyed his value' to NFL teams and put his future in jeopardy after latest incident</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Business Insider</span></div></a>
            
            
            
                <a href="http://finance.yahoo.com/news/agent-cuts-ties-johnny-manziel-153319339.html" data-uuid="41319d63-cc41-3427-b645-ca0ea0667327" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:12;cposy:24;bpos:1;pos:3;ss_cid:bb6d8561-a956-498d-b511-4c77c115e779;refcnt:2;imgt:ss;g:41319d63-cc41-3427-b645-ca0ea0667327;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:rhdln;elmt:ct;r:4000017750S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/.tCcBs7zqnT_W1jwV7ADuw--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/Agent_cuts_ties_with_Johnny-3571fe263d1e804fc1306996a54aeb62')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Agent cuts ties with Johnny Manziel and releases a sad statement about the quarterback's future</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Business Insider</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:12;cposy:22;bpos:1;pos:1;ss_cid:bb6d8561-a956-498d-b511-4c77c115e779;refcnt:2;imgt:ss;g:e7a1961c-dd8d-3484-908f-661154b7abd4;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;r:4000017750S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    


    
    </ul>
</div>

<img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="16" height="16" class="D(b) Mx(a) My(12px) js-stream-load-more ImageLoader" style="background-image:url('https://s.yimg.com/zz/nn/lib/metro/g/my/anim_loading_sm.gif')" alt="">


 </div> </div> </div>  <div class="App-Ft Row"> </div>            <!-- App close -->
            </div>
                </div>
            </div>
            <div class="Col3" id="Aside" role="complementary" tabindex="-1">
                <div class="Col3-stack" data-sticker-top="75px" >
                                <div id="applet_p_32209491" class=" M-0 js-applet trending Zoom-1  Mb(30px) " data-applet-guid="p_32209491" data-applet-type="trending" data-applet-params="_suid:32209491" data-i13n="auto:true;sec:tc-ts" data-i13n-sec="tc-ts"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-trending" class="slingstone-selected"><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) gifts-selected_C($disabledHeading) Trs($trendTrs)" data-category="slingstone">Trending Now</h2><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) Pos(r) slingstone-selected_C($disabledHeading) Trs($trendTrs)" data-category="gifts">
                <i class="Icon-Fp2 IconTumblrHeart gifts-selected_C($giftsIcon) Fz(17px) Fw(100) Pos(a) Start(-19px) T(-2px)"></i>Valentine's Day</h2><ul class="Pos(r) Mt(10px)">
        <li class="trending-list" data-category="slingstone">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) gifts-selected_Op(0) gifts-selected_V(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Anthony+Davis&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Anthony Davis"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:1;bpos:1;ccode:rdcab5;g:adb35d57-f6be-38a6-ae45-f243c1b68855;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Anthony Davis</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Heart+disease&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Heart disease"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:2;bpos:1;ccode:rdcab5;g:fe924488-8751-3c5e-9215-1926ae549397;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Heart disease</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Marion+County&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Marion County"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:3;bpos:1;ccode:rdcab5;g:8803c83d-fdb3-3f57-ad69-950d8d1fe794;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Marion County</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Audi+Q3&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Audi Q3"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:4;bpos:1;ccode:rdcab5;g:8e1b5ec2-104c-3e59-b5cd-edb69896c6f3;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Audi Q3</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=The+Middle&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="The Middle"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:5;bpos:1;ccode:rdcab5;g:7acef721-053d-3251-85a6-8368ebf923af;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> The Middle</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Super+Bowl+recipes&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Super Bowl recipes"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:6;bpos:1;ccode:rdcab5;g:bec71ecf-c6e2-3862-ac43-bcb8b4786a64;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Super Bowl recipes</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Santa+Maria&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Santa Maria"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:7;bpos:1;ccode:rdcab5;g:cd9e764e-22db-318e-a0c7-7e455393a0e6;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Santa Maria</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Acura+NSX+sale&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Acura NSX sale"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:8;bpos:1;ccode:rdcab5;g:d0b9842b-53b2-3c15-b0cd-6ae0ce3ac192;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Acura NSX sale</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Friends+Day&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Friends Day"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:9;bpos:1;ccode:rdcab5;g:bc6fa5e4-5698-3777-b884-f11fc68d0789;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Friends Day</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Samsung+Galaxy+phones&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Samsung Galaxy phones"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:10;bpos:1;ccode:rdcab5;g:0df7c3f7-5a21-387e-bcbf-3e142f177d14;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Samsung Galaxy phones</span>
                    </a>
                </li></ul>
        </li><li class="trending-list Pos(a) T(0) W(100%)" data-category="gifts">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) slingstone-selected_Op(0) slingstone-selected_V(h) slingstone-selected_H(0) Ov(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Diamond+earrings&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Diamond earrings"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:1;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Diamond earrings</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Women%27s+scarves&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Women's scarves"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:2;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Women's scarves</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Personalized+balloons&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Personalized balloons"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:3;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Personalized balloons</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Bathrobes&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Bathrobes"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:4;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Bathrobes</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Beer-of-the-month+club&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Beer-of-the-month club"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:5;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Beer-of-the-month club</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Chocolate+gift+baskets&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Chocolate gift baskets"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:6;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Chocolate gift baskets</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Tunic+tops&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Tunic tops"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:7;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Tunic tops</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=LED+projector&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="LED projector"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:8;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> LED projector</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Edible+bouquets&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Edible bouquets"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:9;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Edible bouquets</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Photo+blankets&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Photo blankets"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:10;bpos:2;ccode:rdcab5;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Photo blankets</span>
                        </a>
                    </li></ul>
        </li></ul>
</div>
 </div> </div> </div>            <!-- App close -->
            </div>                            <div id="my-adsFPAD-base">
                    <div id="my-adsFPAD" class="D-n sda-DAPF">
                        <script>
                            rtAdStart = window.performance && window.performance.now && window.performance.now();
                        </script>
                        <div class="Mx-a" id="my-adsFPAD-iframe">
                            <!-- FPAD has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136sagpdq(gid%24BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st%241454780299612233,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125ormv7m,aid%24nggkIGKLDz4-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=FPAD)"></noscript>
                        </div>
                        <script>
                            rtAdDone = window.performance && window.performance.now && window.performance.now();
                        </script>
                    </div>
                </div>                           <div id="my-adsLREC-base" class="lrec-bgcolor">
                   <div id="my-adsLREC" class="Ta-c Pos-r Z-1 Ht-250 Mb-20">
                        <div id="my-adsLREC-iframe">
                            <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<!--QYZ 2264184051,4467049051,;;LREC;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136sagpdq(gid%24BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st%241454780299612233,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413ak0g73f,aid%24XLYkIGKLDz4-,bi%242264184051,agp%243459460551,cr%244467049051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=LREC)"></noscript>
                        </div>
                    </div>
                </div>            <div id="applet_p_63794" class=" App_v2  M-0 js-applet weather Zoom-1  Mb(30px) " data-applet-guid="p_63794" data-applet-type="weather" data-applet-params="_suid:63794" data-i13n="auto:true;sec:app-wea" data-i13n-sec="app-wea"> <!-- App open -->
        <div class="App-Hd" data-region="header"> <div class="js-applet-view-container-header"> <div class="StencilRoot">
    
        <a href="https://weather.yahoo.com" class="C(#000) C($m_blue):h Td(n)">
            <h2 class="Fz(15px) Fw(b) Mb(18px) D(ib) Grid-U App-Title js-show-panel">
                <span class="js-city-name">
                    
                        
                            Yvrac, 33
                        
                    
                </span>
            </h2>
        </a>
    
    
    <div class="Grid-U">
        <div class="LocationPanel Pos(r) Fl(end) Z(3)"></div>
    </div>
    
</div>
 </div> </div>  <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div class="D(ib)">
    
    <ul class="P(0) Mt(0) Mend(0) Cl(b) Td(n) Mb(10px) Mstart(-2px) forecasts forecasts-12597132">
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Today</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Fair" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">59&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">46&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Sun</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Fair" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">53&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">48&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Mon</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Fair" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">55&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">51&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) ">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Tue</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Fair" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">53&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">44&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
    </ul>
    
</div>
 </div> </div> </div>            <!-- App close -->
            </div>            <div id="applet_p_50000171" class=" M-0 js-applet activitylist Zoom-1  Mb-20 " data-applet-guid="p_50000171" data-applet-type="activitylist" data-applet-params="_suid:50000171" data-i13n="auto:true;sec:ltst" data-i13n-sec="ltst"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  
<ul class="Mb(0) P(0) js-activitylist-list">

<li class="js-activitylist-item My(20px)" data-id="138777746034">
    
    <a href="https://www.yahoo.com/food/super-bowl-recipes-nosh-panthers-141744794.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/hhH3vYY9YVCia3UL93ObJg--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160205/wings.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Food</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Super Bowl snacks: Classic game-day fare and unexpected treats</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item My(20px)" data-id="138777733689">
    
    <a href="http://sports.yahoo.com/blogs/nfl-shutdown-corner/greg-cosell-s-sb-50-preview--keys-for-peyton-manning-vs--panthers-041651613.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/BVIhc0eQQ111B51vfYnlKg--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/151214/peyton-manning414.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        <div class="orb-sprite C($description) Fz(11px) Pos(a) B(-5px) End(24px) D(tb) orb Bdrs(34px)">
            
        </div>
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Sports</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Super Bowl preview: Keys for Peyton Manning vs. Panthers</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item Mt(20px)" data-id="138777562209">
    
    <a href="https://www.yahoo.com/style/stars-naacp-image-awards-red-021055963.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/eS5DdvxUWmMAcgsvuh.aDA--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160205/will.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Style</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Red carpet photos: Stars arrive at the NAACP Image Awards</span>
    </span>
    </a>
    
</li>

</ul>

 </div> </div> </div>            <!-- App close -->
            </div>                           <div id="sticky-lrec2-footer" data-plugin="sticker" data-sticker-top="110px" data-sticker-forceposition="top">
               <div class="lrec-bgcolor">
               <div id="my-adsLREC2" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 darla" data-autorotate="1" data-autoeventrt="15000" data-config={"pos":"LREC2","id":"LREC2","clean":"my-adsLREC2","dest":"my-adsLREC2-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}} data-lrec3replace="1" >
                   <div class='Mx-a Ta-c' id="my-adsLREC2-iframe">
                        
                   </div>
               </div>
                               <div id="my-adsLREC3" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 darla D-n" data-config={"pos":"LREC3","id":"LREC3","clean":"my-adsLREC3","dest":"my-adsLREC3-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}}>
                    <div class='Mx-a Ta-c' id="my-adsLREC3-iframe">
                    </div>
               </div>
               <div id="my-adsLREC2-fallback" class="D-n">
                    <a href="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}" style="background:url(https://www.yahoo.com/sy/dh/ap/default/160201/sports-dailyfantasy-v2-a966da5__1_.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;"></a>
               </div>
               </div>
                    <div id="Footer" class="Row Pstart-20 Pend-10 Pos-r" role="contentinfo">
                         <ul class="Bdrs(3px) Px(20px) Py(14px) Ta(c)" role="contentinfo" id="fp-footer"><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/legal/us/yahoo/utos/terms/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Terms</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Privacy</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://advertising.yahoo.com/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Advertise</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/relevantads.html" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">About our Ads</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://careers.yahoo.com/us" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Careers</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://help.yahoo.com/l/us/yahoo/helpcentral/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Help</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://yahoo.uservoice.com/forums/341361-yahoo-home" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Feedback</a></li></ul></div>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>





        
                        <div id="darla-assets-bottom">
                    <script type="text/x-safeframe" id="fc" _ver="2-9-4">{ "positions": [ { "html": "<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->", "id": "FPAD", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['nggkIGKLDz4-']='(as$125ormv7m,aid$nggkIGKLDz4-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125ormv7m,aid$nggkIGKLDz4-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125ormv7m,aid$nggkIGKLDz4-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1.4547802996122E+15", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "0", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": "{\"pt\":\"0\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"pvid\":\"BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=146750051&megamodal=${MEGAMODAL}&bucket=201&asz=300x250&u=https://www.yahoo.com&gdAdId=XLYkIGKLDz4-&gdUuid=BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20&gdSt=1454780299612233&publisher_blob=${RS}|BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20|2023538075|LREC|1454780299.366159|2-9-4:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aXU3Ym82YihnaWQkQmNZWlZ6Y3lMak00UzJoZUxuVVJKUm9JTWpBd01WYTJMNHNRbEMyMCxzdCQxNDU0NzgwMjk5NjEyMjMzLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkODM0UjRyV0JmSHVvNFVEY19Cbl9iZyxsbmckZW4tdXMsY3IkNDQ2NzA0OTA1MSx2JDIuMCxhaWQkWExZa0lHS0xEejQtLGJpJDIyNjQxODQwNTEsbW1lJDk1NDk0MzI2NTI4NjAyMjMxMDksciQwLHlvbyQxLGFncCQzNDU5NDYwNTUxLGFwJExSRUMpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2264184051,4467049051,;;LREC;2023538075;1-->", "id": "LREC", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['XLYkIGKLDz4-']='(as$13ak0g73f,aid$XLYkIGKLDz4-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13ak0g73f,aid$XLYkIGKLDz4-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13ak0g73f,aid$XLYkIGKLDz4-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)", "impID": "XLYkIGKLDz4-", "supp_ugc": "0", "placementID": "3459460551", "creativeID": "4467049051", "serveTime": "1454780299612233", "behavior": "non_exp", "adID": "9549432652860223109", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "300x250", "bookID": "2264184051", "serveType": "-1", "slotID": "1", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(1602chcmo(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,srv$1,si$4452051,ct$25,exp$1454787499612233,adv$26513753608,li$3458215051,cr$4467049051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1454787499612\", \"fdb_intl\": \"en-US\" }", "slotData": "{\"pt\":\"8\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"adjf\":\"1.000000\",\"alpha\":\"-1.000000\",\"ffrac\":\"0.999951\",\"pcpm\":\"-1.000000\",\"fc\":\"false\",\"sdate\":\"1442339887\",\"edate\":\"1498881599\",\"bimpr\":399999991808.000000,\"pimpr\":0.000000,\"spltp\":0.000000,\"frp\":\"false\",\"pvid\":\"BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "1", "userProvidedData": {} } } },{ "html": "<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->", "id": "MAST", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['GmQlIGKLDz4-']='(as$125vh2sdv,aid$GmQlIGKLDz4-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125vh2sdv,aid$GmQlIGKLDz4-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125vh2sdv,aid$GmQlIGKLDz4-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1454780299612233", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "2", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": "{\"pt\":\"0\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"pvid\":\"BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<style>\n	#fc_align{\n		position: static !important;\n		width: 120px !important;\n		margin: auto !important;\n	}\n</style>\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=251157193&asz=120x60&u=https://www.yahoo.com&gdAdId=2BEmIGKLDz4-&gdUuid=BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20&gdSt=1454780299612233&publisher_blob=${RS}|BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20|2023538075|TXTL|1454780299.366291|2-9-4:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aWZhOWdlZyhnaWQkQmNZWlZ6Y3lMak00UzJoZUxuVVJKUm9JTWpBd01WYTJMNHNRbEMyMCxzdCQxNDU0NzgwMjk5NjEyMjMzLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkODM0UjRyV0JmSHVvNFVEY19Cbl9iZyxsbmckZW4tdXMsY3IkNDQ1MTAwOTA1MSx2JDIuMCxhaWQkMkJFbUlHS0xEejQtLGJpJDIyNzUxODgwNTEsbW1lJDk1OTMzMjA3NzYxNzQzNDk0MDgsciQwLHlvbyQxLGFncCQzNDc2NTAyNTUxLGFwJFRYVEwpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->", "id": "TXTL", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['2BEmIGKLDz4-']='(as$13agn4lc3,aid$2BEmIGKLDz4-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13agn4lc3,aid$2BEmIGKLDz4-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13agn4lc3,aid$2BEmIGKLDz4-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)", "impID": "2BEmIGKLDz4-", "supp_ugc": "0", "placementID": "3476502551", "creativeID": "4451009051", "serveTime": "1454780299612233", "behavior": "non_exp", "adID": "9593320776174349408", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "120x45", "bookID": "2275188051", "serveType": "-1", "slotID": "3", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(1604u1b0e(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,srv$1,si$4452051,ct$25,exp$1454787499612233,adv$26679945979,li$3474892551,cr$4451009051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1454787499612\", \"fdb_intl\": \"en-US\" }", "slotData": "{\"pt\":\"3\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"adjf\":\"1.000000\",\"alpha\":\"1.000000\",\"ffrac\":\"1.000000\",\"pcpm\":\"-1.000000\",\"fc\":\"false\",\"ecpm\":0.000000,\"sdate\":\"1446744408\",\"edate\":\"1514782799\",\"bimpr\":0.000000,\"pimpr\":-361395808.000000,\"spltp\":100.000000,\"frp\":\"false\",\"pvid\":\"BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } } ], "meta": { "y": { "pageEndHTML": "<scr"+"ipt language=javascr"+"ipt>\n(function(){window.xzq_p=function(R){M=R};window.xzq_svr=function(R){J=R};function F(S){var T=document;if(T.xzq_i==null){T.xzq_i=new Array();T.xzq_i.c=0}var R=T.xzq_i;R[++R.c]=new Image();R[R.c].src=S}window.xzq_sr=function(){var S=window;var Y=S.xzq_d;if(Y==null){return }if(J==null){return }var T=J+M;if(T.length>P){C();return }var X=\"\";var U=0;var W=Math.random();var V=(Y.hasOwnProperty!=null);var R;for(R in Y){if(typeof Y[R]==\"string\"){if(V&&!Y.hasOwnProperty(R)){continue}if(T.length+X.length+Y[R].length<=P){X+=Y[R]}else{if(T.length+Y[R].length>P){}else{U++;N(T,X,U,W);X=Y[R]}}}}if(U){U++}N(T,X,U,W);C()};function N(R,U,S,T){if(U.length>0){R+=\"&al=\"}F(R+U+\"&s=\"+S+\"&r=\"+T)}function C(){window.xzq_d=null;M=null;J=null}function K(R){xzq_sr()}function B(R){xzq_sr()}function L(U,V,W){if(W){var R=W.toString();var T=U;var Y=R.match(new RegExp(\"\\\\\\\\(([^\\\\\\\\)]*)\\\\\\\\)\"));Y=(Y[1].length>0?Y[1]:\"e\");T=T.replace(new RegExp(\"\\\\\\\\([^\\\\\\\\)]*\\\\\\\\)\",\"g\"),\"(\"+Y+\")\");if(R.indexOf(T)<0){var X=R.indexOf(\"{\");if(X>0){R=R.substring(X,R.length)}else{return W}R=R.replace(new RegExp(\"([^a-zA-Z0-9$_])this([^a-zA-Z0-9$_])\",\"g\"),\"$1xzq_this$2\");var Z=T+\";var rv = f( \"+Y+\",this);\";var S=\"{var a0 = '\"+Y+\"';var ofb = '\"+escape(R)+\"' ;var f = new Function( a0, 'xzq_this', unescape(ofb));\"+Z+\"return rv;}\";return new Function(Y,S)}else{return W}}return V}window.xzq_eh=function(){if(E||I){this.onload=L(\"xzq_onload(e)\",K,this.onload,0);if(E&&typeof (this.onbeforeunload)!=O){this.onbeforeunload=L(\"xzq_dobeforeunload(e)\",B,this.onbeforeunload,0)}}};window.xzq_s=function(){setTimeout(\"xzq_sr()\",1)};var J=null;var M=null;var Q=navigator.appName;var H=navigator.appVersion;var G=navigator.userAgent;var A=parseInt(H);var D=Q.indexOf(\"Microsoft\");var E=D!=-1&&A>=4;var I=(Q.indexOf(\"Netscape\")!=-1||Q.indexOf(\"Opera\")!=-1)&&A>=4;var O=\"undefined\";var P=2000})();\n</scr"+"ipt><scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_svr)xzq_svr('https://csc.beap.bc.yahoo.com/');\nif(window.xzq_p)xzq_p('yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3');\nif(window.xzq_s)xzq_s();\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136sagpdq(gid$BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20,st$1454780299612233,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3\"></noscr"+"ipt>", "pos_list": [ "FPAD","LREC","MAST","TXTL" ], "filtered": [  ], "spaceID": "2023538075", "host": "www.yahoo.com", "lookupTime": "230", "k2_uri": "", "fac_rt": "-1", "serveTime":"1454780299612233", "pvid": "BcYZVzcyLjM4S2heLnURJRoIMjAwMVa2L4sQlC20", "tID": "darla_prefetch_1454780299613_1936477499_1", "npv": "0", "ep": "{\"site-attribute\":\"fdn=1 Y-BUCKET=\\\"201\\\"\",\"secure\":true,\"lang\":\"en-US\",\"ref\":\"https:\\/\\/www.yahoo.com\",\"filter\":\"no_expandable;\",\"darlaID\":\"darla_instance_1454780299611_977206680_0\"}", "pe": "CWZ1bmN0aW9uIGRwZWQoKSB7IAoJaWYod2luZG93Lnh6cV9kPT1udWxsKXdpbmRvdy54enFfZD1uZXcgT2JqZWN0KCk7CndpbmRvdy54enFfZFsnbmdna0lHS0xEejQtJ109JyhhcyQxMjVvcm12N20sYWlkJG5nZ2tJR0tMRHo0LSxjciQtMSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1GUEFEKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydYTFlrSUdLTER6NC0nXT0nKGFzJDEzYWswZzczZixhaWQkWExZa0lHS0xEejQtLGJpJDIyNjQxODQwNTEsYWdwJDM0NTk0NjA1NTEsY3IkNDQ2NzA0OTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1MUkVDKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydHbVFsSUdLTER6NC0nXT0nKGFzJDEyNXZoMnNkdixhaWQkR21RbElHS0xEejQtLGNyJC0xLGN0JDI1LGF0JEgsZW9iJGdkMV9tYXRjaF9pZD0tMTp5cG9zPU1BU1QpJztpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWycyQkVtSUdLTER6NC0nXT0nKGFzJDEzYWduNGxjMyxhaWQkMkJFbUlHS0xEejQtLGJpJDIyNzUxODgwNTEsYWdwJDM0NzY1MDI1NTEsY3IkNDQ1MTAwOTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1UWFRMKSc7CgkJIH07CmRwZWQudHJhbnNJRCA9ICJkYXJsYV9wcmVmZXRjaF8xNDU0NzgwMjk5NjEzXzE5MzY0Nzc0OTlfMSI7CgoJZnVuY3Rpb24gZHBlcigpIHsgCgkKaWYod2luZG93Lnh6cV9zdnIpeHpxX3N2cignaHR0cHM6Ly9jc2MuYmVhcC5iYy55YWhvby5jb20vJyk7CmlmKHdpbmRvdy54enFfcCl4enFfcCgneWk/YnY9MS4wLjAmYnM9KDEzNnNhZ3BkcShnaWQkQmNZWlZ6Y3lMak00UzJoZUxuVVJKUm9JTWpBd01WYTJMNHNRbEMyMCxzdCQxNDU0NzgwMjk5NjEyMjMzLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxwdiQxLHYkMi4wKSkmdD1KXzMtRF8zJyk7CmlmKHdpbmRvdy54enFfcyl4enFfcygpOwoKCgkKIH07CmRwZXIudHJhbnNJRCA9ImRhcmxhX3ByZWZldGNoXzE0NTQ3ODAyOTk2MTNfMTkzNjQ3NzQ5OV8xIjsKCg==", "pym": "" } } } </script>    <script type="text/javascript">
        var pageloadValidAds = ["TXTL","LREC"];
        var bucketSAEnabled = true;
        var w = window, D = w.DARLA,
            C = {"useYAC":0,"usePE":0,"servicePath":"https:\/\/www.yahoo.com\/sdarla\/php\/fc.php","xservicePath":"","beaconPath":"https:\/\/www.yahoo.com\/sdarla\/php\/b.php","renderPath":"","allowFiF":false,"srenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","renderFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","sfbrenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","msgPath":"https:\/\/www.yahoo.com\/sdarla\/2-9-4\/html\/msg.html","cscPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-csc.html","root":"sdarla","edgeRoot":"http:\/\/l.yimg.com\/rq\/darla\/2-9-4","sedgeRoot":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4","version":"2-9-4","tpbURI":"","hostFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/js\/g-r-min.js","beaconsDisabled":true,"rotationTimingDisabled":true,"fdb_locale":"What don't you like about this ad?|It's offensive|Something else|Thank you for helping us improve your Yahoo experience|It's not relevant|It's distracting|I don't like this ad|Send|Done|Why do I see ads?|Learn more about your feedback.","positions":{"DEFAULT":{"supports":false},"FPAD":[],"LREC":{"w":300,"h":250},"MAST":[],"TXTL":{"w":120,"h":45}},"lang":"en-US"},
            _adPerfBeaconData,
            _pendingAds = [],
            _adLT = [];
        var safeframeOptinPositions = {"FPAD":true};
                window._navStart = function _navStart () {
            if (window.performance) {
                if (window.performance.clearMeasures) {
                    try {
                        window.performance.clearMeasures();
                    } catch (ex) {
                    }
                }
                if (window.performance.clearMarks) {
                    try {
                        window.performance.clearMarks();
                    } catch (ex) {
                    }
                }
                _perfMark('MODAL_OPEN');
            }
            window._pendingAds = [];
            window._perfBackfillAdBeacon = false;
            window._adPerfBeaconData = {positions:{}};
        };
        window._adCallStart = function _adCallStart () {
            window._perfMark('MODAL_AD_EVENT_START');
        };
        window._isModalOpen = function _isModalOpen () {
            return (document.getElementsByTagName('html')[0].className.match(/Reader-open/) && !document.getElementsByTagName('html')[0].className.match(/Reader-closed/));
        };
        window._perfMark = function _perfMark (name) {
            if (window.performance && window.performance.mark) {
                window.performance.mark(name);
            }
        };
        window._perfBackfillAd = function _perfBackfillAd() {
                for (var i in window._adPerfBeaconData.positions) {
                    if (window._adPerfBeaconData.positions.hasOwnProperty(i) && i.match(/LREC-/)) {
                        window._perfMark('PSTMSG_' + i);
                        window._adPerfBeaconData.positions[i].backfill = true;
                    } else {
                        window._adPerfBeaconData.positions[i].backfill = false;
                    }
                }
        };
        window._backfillVideoStart = function _backfillVideoStart() {
            window._perfMark('PSTMSG_VIDSTART');
        };
        window._perfModalFetchStart = function _perfModalFetchStart() {
            window._perfMark('MODAL_FETCH_START');
        };
        window._perfModalFetchEnd = function _perfModalFetchEnd() {
            window._perfMark('MODAL_FETCH_END');
        };
        window._backfillVideoPlaybackStart = function _backfillVideoPlaybackStart(isAd) {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfBackfillAdBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfBackfillAdBeacon = true;
                window._perfMark('PSTMSG_PBSTART');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                videoInit = Math.round(window.performance.getEntriesByName('PSTMSG_VIDSTART')[0].startTime - modalStart);
                playbackStart = Math.round(window.performance.getEntriesByName('PSTMSG_PBSTART')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'brightroll_backfill_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            videoInit: videoInit,
                            videoAd: isAd,
                            playbackStart: playbackStart
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._adRenderComplete = function _adRenderComplete() {
            if (window.performance && window.performance.getEntriesByName) {
                var modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0];
                var modalFetchStart = window.performance.getEntriesByName('MODAL_FETCH_START');
                var modalFetchEnd = window.performance.getEntriesByName('MODAL_FETCH_END');
                var rapidCustomData = [];
                var darlaFetchPerf= {};
                var rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                var modalStartTime = modalStart.startTime;

                darlaFetchPerf['name'] = 'darla_info';
                darlaFetchPerf['spaceid'] = window._adPerfBeaconData.spaceId;
                darlaFetchPerf['pvid'] = window._adPerfBeaconData.pvid;
                darlaFetchPerf['site'] = window._adPerfBeaconData.property || 'fp';
                darlaFetchPerf['lang'] = window.Af.context.lang;
                darlaFetchPerf['region'] =  window.Af.context.region;
                darlaFetchPerf['device'] =  window.Af.context.device;
                darlaFetchPerf['rid'] =  window.Af.context.rid;
                darlaFetchPerf['bucket'] = '201';

                if (modalFetchStart && modalFetchStart.length > 0) {
                    darlaFetchPerf['modalFetchStart'] =  Math.round(modalFetchStart[0].startTime -modalStartTime);
                    darlaFetchPerf['modalFetchEnd'] =  Math.round(modalFetchEnd[0].startTime -modalStartTime);
                }
                darlaFetchPerf['eventStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_EVENT_START')[0].startTime -modalStartTime);
                darlaFetchPerf['wsStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_START')[0].startTime - modalStartTime);
                darlaFetchPerf['wsEnd'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_END')[0].startTime - modalStartTime);
                rapidCustomData.push(darlaFetchPerf);

                for (var i in window._adPerfBeaconData.positions) {
                    var darlaPositionPerf = {};
                    var posInfo = window._adPerfBeaconData.positions.hasOwnProperty(i) && window._adPerfBeaconData.positions[i];
                    if(posInfo) {
                        darlaPositionPerf['name'] = i + '_info';
                        darlaPositionPerf['valid'] = posInfo.valid ? 1 : 0;
                        if (posInfo.valid) {
                            darlaPositionPerf['renderStart'] =  Math.round(window.performance.getEntriesByName('ADSTART_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['renderEnd'] =  Math.round(window.performance.getEntriesByName('ADEND_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['bookId'] = posInfo.bookId;
                            darlaPositionPerf['creativeId'] = posInfo.creativeId;
                            darlaPositionPerf['placementId'] = posInfo.placementId;
                            darlaPositionPerf['size'] = posInfo.size;
                            darlaPositionPerf['backfill'] = posInfo.backfill ? 1 : 0;
                            if (posInfo.backfill) {
                                darlaPositionPerf['pstmsg'] = Math.round(window.performance.getEntriesByName('PSTMSG_' + i)[0].startTime - modalStartTime);
                            }
                        }
                        rapidCustomData.push(darlaPositionPerf);
                    }
                }

                if (rapidInstance) {
                    var customPerfData = {'utm': rapidCustomData};
                    var rapidPerfData = {perf_usertime: customPerfData};
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        if (D && C) {
            C.positions = {"FPAD":{"clean":"my-adsFPAD","dest":"my-adsFPAD-iframe","metaSize":1,"fdb":"true,","supports":{"resize-to":1,"exp-ovr":1,"exp-push":1,"lyr":1}},"LREC":{"pos":"LREC","clean":"my-adsLREC","dest":"my-adsLREC-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":0,"lyr":0}},"MAST":{"pos":"MAST","clean":"my-adsMAST","dest":"my-adsMAST-iframe","fr":"expIfr_exp","rmxp":0,"metaSize":true,"w":970,"h":250,"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"closeBtn":{"mode":2,"useShow":1}},"TXTL":{"pos":"TXTL","id":"TXTL","clean":"my-adsTXTL","dest":"my-adsTXTL-iframe","w":120,"h":170,"fr":"expIfr_exp","rmxp":0,"css":".ad-tl2b {overflow:hidden; text-align:left;} p {margin:0px;} a {color:#020e65;} a:hover {color:#0078ff;} .y-fp-pg-controls {margin-top:5px; margin-bottom:5px;} #tl1_slug {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px; color:#999;} #fc_align a {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px;} a:link {text-decoration:none;}"}};
            C.k2={"res":{"rate":100,"pos":["LREC","MAST","FPAD","LREC2","LREC3","LREC-0","LREC2-0","LREC3-0","MAST-0","LDRB-0","SPL2-0","MON-0"]}};
            C.k2E2ERate=100;
C.k2Rate=100;
C.so=1;

            C.events = null;
            
                    C.onStartRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_START');
            }
        };
                    C.onFinishRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_END');
            }
        };
                        C.onFinishParse = function(eventName, result) {
                var ps = result.ps(),
                    modalOpen = false,
                    position, posItem, curAd, curEvt,
                    validPositions = {},
                    isMONFetch = false;
                            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('AD_PARSE');
                modalOpen = true;
                window._adPerfBeaconData.spaceId = result.spaceID;
                window._adPerfBeaconData.pvid = result.pvid;
                curEvt = DARLA.evtSettings(eventName);
                if (curEvt.property) {
                    window._adPerfBeaconData.property = curEvt.property;
                }
            }
                        function fireBeacon(position, size, pageMode) {
            if (position && size && pageMode) {
                try {
                    var img = new Image();
                    img.src = '/p.gif?beaconType=darla&pos=' + position + '&size=' + size + '&mode=' +pageMode + '&bucket=' + 201 + '&rid=' + "5qnosq1bbcbsb";
                } catch (e) {
                    if (console) {
                        console.log('failed to send darla beacon :', e);
                    }
                }
            }
        }

                if (ps && ps.length) {
                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
            for (i = 0, l = ps.length; i < l; i++) {
                position = ps[i];
                posItem = result.item(position);
                if (posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    validPositions[position] = false;
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = false;
                    }
                } else {
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = true;
                    }
                    validPositions[position] = true;
                }
            }
            if (YMedia && YMedia.Af && YMedia.Af.Event && YMedia.Af.Event.fire) {
                YMedia.Af.Event.fire('sidekickrefresh', validPositions);
            }
        }

                    for (i = 0, l = ps.length; i < l; i++) {
                        position = ps[i];
                        posItem = result.item(position);
                                    if (modalOpen) {
                if (posItem.err || posItem.hasErr) {
                    window._adPerfBeaconData.positions[position] = {error: true};
                } else if (posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    window._adPerfBeaconData.positions[position] = {size: '1x1'};
                } else {
                    window._adPerfBeaconData.positions[position] = {valid: true};
                    if (posItem.meta && posItem.meta.y) {
                        window._adPerfBeaconData.positions[position].size = posItem.meta.y.size;
                        window._adPerfBeaconData.positions[position].bookId = posItem.meta.y.bookID;
                        window._adPerfBeaconData.positions[position].creativeId = posItem.meta.y.creativeID;
                        window._adPerfBeaconData.positions[position].placementId = posItem.meta.y.placementID;
                    }
                    window._pendingAds.push(position);
                }
            }
                        if (posItem && posItem.conf && posItem.conf.clean) {
                            curAd = document.getElementById(posItem.conf.clean);
                            if (curAd) {
                                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                var posName = position.split('-')[0];
                if ((posName === "LDRB" || posName === "MAST" || posName === "SPL" || posName === "SPL2") &&
                   (!posItem.hasErr && posItem.size + '' !== '1x1')) {
                    curAd.className = curAd.className.replace('D-n', 'D-ib');
                    if (posName !== "SPL" && posName !== "SPL2") {
                        curAd.className = curAd.className + " Bdb-Grey-1";
                    }
                }
            }
            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                if (position.match(/^SPL/)) {
                    if (posItem.hasErr || posItem.size + '' === '1x1') {
                        if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                            YMedia.one(curAd).setStyle('padding-top', '0px');
                        }
                    } else if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                        var splashWrap = YMedia.one('#' + posItem.conf.clean + '-wrap');
                        splashWrap.setStyle('padding-top', (5/12*100)+'%');
                        splashWrap.setStyle('width', '100%');
                        splashWrap.setStyle('height', '0px');
                        splashWrap.addClass('Mt-20 Mb-50');
                    }
                }
            }
            if ((eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') && position.indexOf("LREC") > -1) {
              if (posItem && posItem.conf && posItem.conf.clean) {
                var fallbackDiv = document.getElementById(posItem.conf.clean + "-fallback");
                }
                if ((posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1'))  && !isMONFetch) {
                    var lrecBackfillString = "<a href=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}\" style=\"background:url(https:\/\/www.yahoo.com\/sy\/dh\/ap\/default\/160201\/sports-dailyfantasy-v2-a966da5__1_.jpg) 0 0 no-repeat;height:height:250px;width:300px;display:block;\"><\/a>";
                    if (fallbackDiv) {
                       fallbackDiv.innerHTML = lrecBackfillString;
                       fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                    }
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    curAd.className = curAd.className.replace(/hl-ad-LREC/, '');
                } else {
                  if (fallbackDiv && fallbackDiv.className.indexOf("D-n") < 0) {
                      fallbackDiv.className += ' D-n';
                    }
                    if (isMONFetch && position === 'LREC-0') {
                        curAd.parentElement.style.height = '600px';
                        var curAd = document.getElementById(posItem.conf.clean);
                        if (curAd.className.indexOf("D-n") < 0  && curAd.className.indexOf("D-ib") >= 0) {
                          curAd.className = curAd.className.replace(/D-ib/, 'D-n');
                        }
                    }
                }
            }
            if (eventName === 'prefetch' && position.indexOf("LREC") > -1) {
                curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
            }
            if ((eventName === 'fetch_selective_ad_lrec2' && position === 'LREC2') || (eventName === 'fetch_selective_ad_lrec3' && position === 'LREC3')) {
                var fallbackDiv = document.getElementById("my-adsLREC2-fallback");
                if (posItem.hasErr || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1')) {
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                    curAd.className = curAd.className.replace('Ht-250', '');
                    // hide div if ad fetch failed or is 1x1
                    if (curAd.className.indexOf("D-n") < 0) {
                        curAd.className += ' D-n';
                    }
                    fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                } else {
                    if (fallbackDiv.className.indexOf("D-n") < 0) {
                        fallbackDiv.className += ' D-n';
                    }
                    curAd.className = curAd.className.replace('D-n', '');
                }

                if (position === 'LREC3') {
                    var lrec2Div = document.getElementById("my-adsLREC2");
                    if (lrec2Div && lrec2Div.className.indexOf("D-n") < 0) {
                        lrec2Div.className += ' D-n';
                    }
                }
            }


                            }
                        }
                    }
                }
                            if (modalOpen && window._pendingAds.length === 0) {
                window._adRenderComplete();
            }
            };

                        C.onStartPosRender = function(posItem) {
                if (window.performance  && window.performance.now) {
                    var ltime = window.performance.now(),
                        posId = posItem && posItem.pos;
                    _adLT.push(['ADSTART_'+posId, Math.round(ltime)]);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADSTART_' + posId);
                }
            };

                        C.onBeforeStartPosRender = function(posItem) {
                if (posItem && safeframeOptinPositions && safeframeOptinPositions[posItem.pos]) {
                    if (posItem.html && posItem.html.match(/<!--[^>]*sfoptout[^>]*-->/)) {
                        return true;
                    }
                }
            };

                        C.onFinishPosRender = function(posId, reqList, posItem) {
                var curAd = document.getElementById("my-ads"+posId),
                    adIndex,
                    posSize = (posItem && posItem.meta && posItem.meta.value("size", "y"));

                // Get clean div for ad position in case defined
                if (posItem && posItem.conf && posItem.conf.clean) {
                    curAd = document.getElementById(posItem.conf.clean);
                }
                if (curAd) {
                    // Let ad take its original size, remove default height given to ad div
                    curAd.className = curAd.className.replace('Ht-250', '');
                    curAd.className = curAd.className.replace('Ht-MAST', '');

                    if(posSize && posSize != "1x1") {
                        curAd.className = curAd.className.replace('D-n', '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    }
                }

                if (window.performance !== undefined && window.performance.now !== undefined) {
                    var whiteListedAds = {"LREC":"my-adsLREC-base","MAST":"my-adsMAST","TL1":"my-adsTL1","TXTL":"my-adsTXTL","LREC-0":"hl-ad-LREC-0","MON-0":"hl-ad-MON-0","MAST-0":"hl-ad-MAST-0","LDRB-0":"hl-ad-LDRB-0","SPL2-0":"hl-ad-SPL2-0","SPL-0":"hl-ad-SPL-0"},
                      ltime = window.performance.now();
                     _adLT.push(['ADEND_'+posId, Math.round(ltime)]);
                    setTimeout(function () {
                        if (window.YAFT !== undefined && window.YAFT.isInitialized() && whiteListedAds[posId]) {
                            // Trigger custom timing for LREC ad position
                            window.YAFT.triggerCustomTiming(whiteListedAds[posId], '', ltime);
                        }
                    },300);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADEND_' + posId);
                    adIndex = window._pendingAds.indexOf(posId);
                    if (adIndex >= 0) {
                        window._pendingAds.splice(adIndex, 1);
                        if (window._pendingAds.length === 0) {
                            window._adRenderComplete();
                        }
                    }
                }
            };

            C.onBeforePosMsg = function(msg_name, position) {
        // Make these configurable for INTLS
        var maxWidth = 970,
            maxHeight = 600,
            newWidth,
            newHeight,
            origWidth,
            origHeight,
            pos;

        if('MAST' !== position) {
            return;
        }

        if (msg_name === 'resize-to') {
            newWidth = arguments[2];
            newHeight = arguments[3];
        }
        else if (msg_name === 'exp-push' || msg_name === 'exp-ovr') {
            pos = $sf.host.get('MAST'),
            origWidth = pos.conf.w;
            origHeight = pos.conf.h;
            //"exp-ovr" or "exp-push", position id, delta X, delta Y, push (true /false), top increase, left increase, right increase, bottom increase
            newWidth = origWidth + arguments[6] + arguments[7];
            newHeight = origHeight + arguments[5] + arguments[8];
        }
        if(newWidth > maxWidth || newHeight > maxHeight) {
            return true;
        }
    };
                        //call back when the ad is expanded or collapsed
            C.onPosMsg = function (msg_name, data, msg_data)  {
                var visible;
                if(msg_name == "collapse" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdExpanded', '');
                    bodyTag.className += " " +  "mastAdCollapsed";
                }
                if(msg_name == "exp-push" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdCollapsed', '');
                    bodyTag.className += " " +  "mastAdExpanded";
                }

                /* generic ad expansion logic */
                if(msg_name == "collapse") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace(data + "-ad-expanded", '');
                }

                if(msg_name == "exp-ovr") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className += " " + data + "-ad-expanded";
                }

                if (msg_name === 'geom-update') {
                    visible = D.render.RenderMgr.get(data).viewedAt > 0;
                    // geom-update event will always be available when Y is available
                    if (YMedia && visible) {
                        YMedia.Global.fire('ads:beacon', {id: data});
                    }
                }
                                if (msg_name === 'cmsg') {
                    var splashConf = DARLA.posSettings(data)
                    if (YMedia && splashConf && splashConf.clean) {
                        var splashNode = document.getElementById(splashConf.clean + '-wrap');
                        if (splashNode) {
                            if (msg_data === 'splash-expand') {
                                YMedia.one(splashNode).setStyle('padding-top', (9/16*100)+'%');
                                if (typeof splashNode.scrollIntoView === 'function') {
                                    splashNode.scrollIntoView();
                                }
                            } else if (msg_data === 'splash-collapse') {
                                YMedia.one(splashNode).setStyle('padding-top', (5/12*100)+'%');
                            }
                        }
                    }
                }
            };

            

            


            if ("OK" == D.config(C)) {
                setTimeout(function() {
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_RSTART', Math.round(ltime)]);
                    }
                    var w = window,
                        d = document,
                        e = d.documentElement,
                        g = d.getElementsByTagName('body')[0],
                        winWidth = w.innerWidth || e.clientWidth || g.clientWidth;
                    if (true && winWidth < 1024 && window.pageloadValidAds && (window.pageloadValidAds.indexOf('TXTL') >= 0)) {
                        var prefetched = D.prefetched();
                        var txtlindex = prefetched.indexOf('TXTL');
                        if (txtlindex >= 0) {
                            delete(prefetched[txtlindex]);
                            for (var i=0; i < prefetched.length; i++) {
                                if (prefetched.hasOwnProperty(i)) {
                                    D.render(prefetched[i]);
                                }
                            }
                        } else {
                            D.render();
                        }
                    } else {
                        D.render();
                    }
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_REND', Math.round(ltime)]);
                    }
                }, 2);
            }
        }
    </script>
                </div>
        
        

        
        
        <input type="hidden" id="afhistorystate">
    <!-- bottom -->
    
                    <script type="text/javascript" src="/sy/zz/combo?yui:/3.18.0/yui/yui-min.js&/ss/rapid-3.29.1.js&/os/mit/td/aperollup-min-163d3e68_desktop_advance.js"></script>
                    <script type="text/javascript" src="/sy/zz/combo?&&/os/mit/td/td-applet-stream-atomic-2.0.439/r-min.js&/os/mit/td/td-applet-mega-header-1.0.193/r-min.js&/os/mit/td/td-applet-viewer-0.1.2102/r-min.js&/os/mit/td/td-applet-navlinks-atomic-0.0.54/r-min.js" async defer></script>                                <script type="text/javascript">                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                        YUI.Env.core.push.apply(YUI.Env.core,["loader-angus","loader-ape-af","loader-ape-applet","loader-ape-location","loader-ape-pipe","loader-ape-social","loader-applet-server","loader-assembler","loader-dust-helpers","loader-finance-streamer","loader-highlander-client","loader-live-event-data","loader-mjata","loader-stencil","loader-td-api","loader-td-dev-info","loader-td-lib-entertainment","loader-td-lib-hovercards","loader-td-lib-livevideo","loader-td-lib-social","loader-td-applet-stream-atomic","loader-td-applet-mega-header","loader-td-applet-viewer","loader-td-applet-navlinks-atomic","loader-td-applet-trending-atomic","loader-td-applet-sidekick","loader-td-applet-related-content","loader-td-applet-mega-comments","loader-td-applet-weather-atomic"]);
YUI.add("loader-ape-af",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-beacon":{group:"ape-af",requires:["json-stringify","querystring-stringify-simple","io-base"]},"af-bootstrap":{group:"ape-af",requires:["af-utils"]},"af-cache":{group:"ape-af",requires:["af-beacon","json","gallery-storage-lite"]},"af-compositeview":{group:"ape-af",requires:["parallel","af-utils"]},"af-comscore":{group:"ape-af",requires:["af-beacon","af-config","querystring-stringify-simple"]},"af-config":{group:"ape-af",requires:["af-utils","cookie"]},"af-content":{group:"ape-af",requires:["af-config","af-dom","af-utils","json-parse","mjata-rest-http","node-pluginhost","querystring-stringify"]},"af-cookie":{group:"ape-af",requires:["cookie","json"]},"af-dom":{group:"ape-af",requires:["node-base","node-core"]},"af-dwelltime":{group:"ape-af",requires:["af-rapid","json-stringify"]},"af-eu-tracking":{group:"ape-af",requires:["af-beacon","media-agof-tracking"]},"af-event":{group:"ape-af",requires:["event-custom"]},"af-history":{group:"ape-af",requires:["json"]},"af-message":{group:"ape-af",langBundles:["strings"],requires:["ape-af-templates-message","af-utils","node-base","selector-css3"]},"af-pageviz":{group:"ape-af",requires:["event-custom"]},"af-poll":{group:"ape-af",requires:["af-pageviz"]},"af-rapid":{group:"ape-af",requires:["af-bootstrap","af-utils","media-rapid-tracking","node-core"]},"af-sync":{group:"ape-af",requires:["af-transport","af-utils"]},"af-trans":{group:"ape-af",requires:["node-base","transition","af-utils"]},"af-transport":{group:"ape-af",requires:["af-beacon","af-config","af-utils","json-stringify","tdapi-remotestore"]},"af-utils":{group:"ape-af",requires:["cookie","intl","oop","querystring-parse-simple"]},"af-viewport-loader":{group:"ape-af",requires:["node","event-base"]},"ape-af-lang-strings":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_de-de":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_el-gr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ae":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-au":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-gb":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ie":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-in":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-my":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-nz":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ph":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-sg":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-za":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-cl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-co":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-es":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-mx":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-pe":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ve":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-fr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_id-id":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_it-it":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-nl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_pt-br":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ro-ro":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_sv-se":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_vi-vn":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-hk":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-tw":{group:"ape-af",requires:["intl"]},"ape-af-templates-message":{group:"ape-af",requires:["template-base","dust"]},"gallery-storage-lite":{group:"ape-af",requires:["event-base","event-custom","event-custom-complex","json","node-base"]},"media-agof-tracking":{group:"ape-af"},"media-rapid-tracking":{group:"ape-af",requires:["event-custom","base","node"]},"media-rmp":{group:"ape-af",requires:["node","io-base","async-queue","get"]},"tdapi-remotestore":{group:"ape-af",requires:["mjata-store","mjata-rest-http","json"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-applet",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-applet":{group:"ape-applet",requires:["af-applet-dom","af-applet-model","af-applet-headerview","af-bootstrap","af-compositeview","af-config","af-utils","array-extras","event-custom","node","stencil-tooltip"]},"af-applet-action":{group:"ape-applet",requires:["af-applet-dom","af-utils","node-event-delegate","node-base"]},"af-applet-dom":{group:"ape-applet",requires:["node-core","af-trans"]},"af-applet-editview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-moreinfo","ape-applet-templates-remove"]},"af-applet-headerview":{group:"ape-applet",requires:["af-applet-view","stencil-selectbox"]},"af-applet-init":{group:"ape-applet",requires:["af-applet","af-beacon","af-bootstrap","af-config","af-utils","event-touch","oop"]},"af-applet-load":{group:"ape-applet",requires:["af-applet-dom","af-message","af-transport","ape-applet-templates-reload"]},"af-applet-mgr":{group:"ape-applet",requires:["af-applet-dom","af-beacon","af-content","af-transport","af-utils"]},"af-applet-model":{group:"ape-applet",langBundles:["strings"],requires:["af-bootstrap","af-config","af-sync","base-build","mjata-json","model"]},"af-applet-savesettings":{group:"ape-applet",requires:["af-dom","af-message"]},"af-applet-settingsview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-settingswrap"]},"af-applet-view":{group:"ape-applet",requires:["af-bootstrap","af-utils","base-build","dust","view"]},"af-applet-viewmgr":{group:"ape-applet",requires:["af-applet-dom"]},"af-applets":{group:"ape-applet",requires:["af-applet-action","af-applet-init","af-applet-load","af-applet-mgr","af-applet-savesettings","af-applet-viewmgr","af-dom","af-utils"]},"ape-applet-lang-strings":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_de-de":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_el-gr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ae":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-au":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-gb":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ie":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-in":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-my":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-nz":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ph":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-sg":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-za":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-cl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-co":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-es":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-mx":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-pe":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ve":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-fr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_id-id":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_it-it":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-nl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_pt-br":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ro-ro":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_sv-se":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_vi-vn":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-hk":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-tw":{group:"ape-applet",requires:["intl"]},"ape-applet-templates-moreinfo":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-reload":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-remove":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-settingswrap":{group:"ape-applet",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-location",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-location-detection":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-detection"]},"af-location-panel":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-list","ape-location-templates-location-panel","stencil-toggle","stencil-tooltip"]},"af-locations":{group:"ape-location",requires:["af-sync","af-utils","mjata-model-base","mjata-model","mjata-lazy-modellist","mjata-model-store"]},"ape-location-lang-strings":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_de-de":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_el-gr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ae":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-au":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-gb":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ie":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-in":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-my":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-nz":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ph":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-sg":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-za":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-cl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-co":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-es":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-mx":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-pe":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ve":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-fr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_id-id":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_it-it":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-nl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_pt-br":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ro-ro":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_sv-se":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_vi-vn":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-hk":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-tw":{group:"ape-location",requires:["intl"]},"ape-location-templates-location-detection":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-list":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-panel":{group:"ape-location",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-pipe",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-comet":{group:"ape-pipe",requires:["af-beacon","af-config","comet","event-custom-base","json-parse"]},"af-pipe":{group:"ape-pipe",requires:["af-beacon","af-comet","af-config","af-utils","event-custom-base"]},comet:{group:"ape-pipe"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-social":{group:"ape-social",requires:[]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-dust-helpers",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{dust:{group:"dust-helpers",requires:["stencil-imageloader","moment","intl-helper"]},"dust-helper-intl":{es:!0,group:"dust-helpers",requires:["intl-messageformat"]},"intl-helper":{group:"dust-helpers",requires:["dust-helper-intl"]},"intl-messageformat":{es:!0,group:"dust-helpers"},moment:{group:"dust-helpers"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-mjata",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mjata-bind-model2dom":{group:"mjata",requires:["mjata-model-store","mjata-template","mjata-util","node-base","node-core"]},"mjata-binder":{group:"mjata",requires:["mjata-bind-model2dom"]},"mjata-json":{group:"mjata"},"mjata-lazy-modellist":{group:"mjata",requires:["lazy-model-list","mjata-model-store"]},"mjata-model":{group:"mjata",requires:["model","mjata-json","mjata-model-store"]},"mjata-model-base":{group:"mjata",requires:["base","mjata-model","mjata-modellist","mjata-model-store"]},"mjata-model-store":{group:"mjata",requires:["event-custom","promise"]},"mjata-modellist":{group:"mjata",requires:["model-list","mjata-model-store"]},"mjata-queue":{group:"mjata"},"mjata-rest-http":{group:"mjata",requires:["json-stringify","io-base","io-xdr"]},"mjata-store":{group:"mjata",requires:["mjata-queue","mjata-util"]},"mjata-template":{group:"mjata",requires:["dust"]},"mjata-util":{group:"mjata"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-stencil",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{hammer:{group:"stencil"},stencil:{group:"stencil"},"stencil-base":{group:"stencil",requires:["stencil","yui-base","event-base","event-delegate","event-mouseenter","json-parse","dom-base","node-base","node-pluginhost","selector"]},"stencil-bquery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-style","node-base","node-pluginhost"]},"stencil-carousel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-hover"]},"stencil-fx":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","pluginhost","event-custom","selector-css3"]},"stencil-fx-collapse":{group:"stencil",requires:["stencil-fx","node-style"]},"stencil-fx-fade":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-fx-flip":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-gallery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","transition"]},"stencil-imageloader":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-screen","node-style","node-pluginhost","array-extras"]},"stencil-lightbox":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","shim-plugin"]},"stencil-scrollview":{group:"stencil",requires:["node-base","node-style","hammer"]},"stencil-selectbox":{group:"stencil",requires:["stencil-base"]},"stencil-slider":{group:"stencil",requires:["stencil"]},"stencil-source":{group:"stencil",requires:["node","oop","pluginhost","mjata-util"]},"stencil-source-af":{group:"stencil",requires:["node","stencil-source","mjata-model-store","mjata-bind-model2dom","af-applets"]},"stencil-source-dom":{group:"stencil",requires:["stencil-source"]},"stencil-source-rmp":{group:"stencil",requires:["stencil-source","stencil-source-url","media-rmp"]},"stencil-source-simple":{group:"stencil",requires:["stencil-source"]},"stencil-source-url":{group:"stencil",requires:["stencil-source","io-base","io-xdr"]},"stencil-sticker":{group:"stencil",requires:["node-base","node-style","node-screen"]},"stencil-tabpanel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-pluginhost"]},"stencil-toggle":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-focus","event-mouseenter","json-parse"]},"stencil-tooltip":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","node-style","stencil-source"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-comments",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-mega-comments-mainview":{group:"td-applet-mega-comments",requires:["af-applet-view","af-transport","af-utils","af-rapid","stencil-tooltip","td-applet-mega-comments-templates-styles"]},"td-applet-mega-comments-templates-avatar":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body"]},"td-applet-mega-comments-templates-comments-body":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-comments-editor-tools"]},"td-applet-mega-comments-templates-comments-editor-tools":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-abuse":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-comment":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-delete":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-reply":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-main":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-form-comment","td-applet-mega-comments-templates-form-reply","td-applet-mega-comments-templates-form-abuse","td-applet-mega-comments-templates-form-delete"]},"td-applet-mega-comments-templates-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-replies-body":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-styles":{group:"td-applet-mega-comments",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-header",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mega-header-plugin-account-switch":{group:"td-applet-mega-header",requires:["node","event-custom","event-move","event-mouseenter","td-applet-mega-header-constants","get","anim","json-parse","escape"]},"mega-header-plugin-autocomplete":{group:"td-applet-mega-header",requires:["node","autocomplete","autocomplete-highlighters","autocomplete-list","td-applet-mega-header-constants"]},"mega-header-plugin-avatar":{group:"td-applet-mega-header",requires:["jsonp","td-applet-mega-header-constants"]},"mega-header-plugin-instant":{group:"td-applet-mega-header",requires:["node","event","af-event","json","jsonp","querystring","td-applet-mega-header-constants"]},"mega-header-plugin-mailpreview":{group:"td-applet-mega-header",requires:["jsonp","af-event","td-applet-mega-header-constants"]},"mega-header-plugin-notifications":{group:"td-applet-mega-header",requires:["af-event","af-cache","td-applet-mega-header-constants"]},"mega-header-plugin-username":{group:"td-applet-mega-header",requires:["jsonp"]},"td-applet-mega-header-constants":{group:"td-applet-mega-header"},"td-applet-mega-header-mainview":{group:"td-applet-mega-header",requires:["af-applet-view","node","td-applet-mega-header-constants","mega-header-plugin-autocomplete","mega-header-plugin-avatar","mega-header-plugin-username","mega-header-plugin-mailpreview","mega-header-plugin-notifications","mega-header-plugin-instant","mega-header-plugin-account-switch"]},"td-applet-mega-header-templates-accountSwitchPanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-autofocus_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-firefoxPromo_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-instant_filters":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-logo":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mail":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mailpreview":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-main":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-topbar","td-applet-mega-header-templates-logo","td-applet-mega-header-templates-profile","td-applet-mega-header-templates-notifications","td-applet-mega-header-templates-mail","td-applet-mega-header-templates-search","td-applet-mega-header-templates-instant_filters"]},"td-applet-mega-header-templates-notificationpanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-notifications":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-profile":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-accountSwitchPanel","td-applet-mega-header-templates-profilePanel"]},"td-applet-mega-header-templates-profilePanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-search":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-autofocus_script"]},"td-applet-mega-header-templates-topbar":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-firefoxPromo_script"]},"td-mega-header-model":{group:"td-applet-mega-header",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-navlinks-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-navlinks-atomic-templates-main":{group:"td-applet-navlinks-atomic",requires:["template-base","dust"]},"td-applet-navlinks-mainview":{group:"td-applet-navlinks-atomic",requires:["af-applet-view"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-related-content",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-related-content-model":{group:"td-applet-related-content",requires:["model","af-event"]},"td-applet-related-content-templates-main":{group:"td-applet-related-content",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-sidekick",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-sidekick-templates-item":{group:"td-applet-sidekick",requires:["template-base","dust"]},"td-applet-sidekick-templates-main":{group:"td-applet-sidekick",requires:["template-base","dust","td-applet-sidekick-templates-item"]},"td-sidekick-mainview":{group:"td-applet-sidekick",requires:["af-applet-view","af-config","af-event","stencil"]},"td-sidekick-model":{group:"td-applet-sidekick",requires:["af-sync","base-build","model"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-stream-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"stream-actiondrawer-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-cache","af-event","af-utils","event-outside","stencil-fx","stencil-fx-collapse"]},"stream-needtoknow-anim":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-pageviz"]},"stream-onboard-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","stencil-fx","stencil-fx-collapse"]},"td-applet-comments-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-interest-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-stream-appletmodel-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-cookie","af-event","af-utils","af-applet-model","af-beacon","af-poll","td-applet-stream-model-v2","td-applet-stream-items-model-v2"]},"td-applet-stream-atomic-templates-breaking_news":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-debug":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_action":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_desktop":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_flyout":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_share":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-errormsg":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-ad_dislike":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_dislike_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-default":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_clusters":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-featured_ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-featured_ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-featured_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-filmstrip":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-followable":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-gs_tile":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-needtoknow_actions":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-play_icon":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-right_menu":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-right_menu_featured"]},"td-applet-stream-atomic-templates-item-right_menu_featured":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-storyline":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_images":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-items":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-debug"]},"td-applet-stream-atomic-templates-main":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-breaking_news","td-applet-stream-atomic-templates-errormsg"]},"td-applet-stream-atomic-templates-related":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-removeditem":{group:"td-applet-stream-atomic",requires:["template-base"
,"dust"]},"td-applet-stream-baseview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","node-scroll-info","af-beacon"]},"td-applet-stream-headerview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view"]},"td-applet-stream-items-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-mainview-v2":{group:"td-applet-stream-atomic",requires:["af-beacon","af-cookie","af-cache","af-event","af-pipe","stencil-imageloader","td-applet-stream-baseview-v2"]},"td-applet-stream-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-onboarding-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-payoff-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-related-model-atomic":{group:"td-applet-stream-atomic",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-trending-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-trending-atomic-mainview":{group:"td-applet-trending-atomic",requires:["af-applet-view","node"]},"td-applet-trending-atomic-templates-main":{group:"td-applet-trending-atomic",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-viewer",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-ads-model":{group:"td-applet-viewer",requires:["af-sync","base-build","model","af-beacon"]},"td-applet-viewer-templates-ad_fdb_thank_you":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-ads_story":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-cards":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-debug","td-applet-viewer-templates-fallback","td-applet-viewer-templates-footer"]},"td-applet-viewer-templates-carousel":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-content_body":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow","td-applet-viewer-templates-ads_story"]},"td-applet-viewer-templates-content_body_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow"]},"td-applet-viewer-templates-credit":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-debug":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-drawer_feedback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-fallback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-footer":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-header":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-share_btns_mega"]},"td-applet-viewer-templates-header_ads":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-lightbox":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel"]},"td-applet-viewer-templates-lightbox_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel","td-applet-viewer-templates-thumbnail_items"]},"td-applet-viewer-templates-mag_slideshow":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-main":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-cards","td-applet-viewer-templates-lightbox"]},"td-applet-viewer-templates-modal_aside_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-modal_lrecs_mega"]},"td-applet-viewer-templates-modal_lrecs_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-reblog":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-sidekicktv":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-slideshow":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-lightbox_mega","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story_cover":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-video","td-applet-viewer-templates-header"]},"td-applet-viewer-templates-thumbnail_items":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-video":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-content_body"]},"td-viewer-ads":{group:"td-applet-viewer",requires:["af-beacon","af-config","af-event"]},"td-viewer-mainview":{group:"td-applet-viewer",requires:["af-applet-view","af-beacon","af-cache","af-config","af-event","stencil","angus-slider","event-tap","td-viewer-ads","td-viewer-slideshow"]},"td-viewer-model":{group:"td-applet-viewer",requires:["af-cache","af-sync","base-build","model","af-beacon"]},"td-viewer-slideshow":{group:"td-applet-viewer",requires:["stencil","angus-slider","event-tap"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-weather-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-weather-atomic-appletmodel":{group:"td-applet-weather-atomic",requires:["mjata-model-base","af-applet-model","mjata-model-store"]},"td-applet-weather-atomic-headerview":{group:"td-applet-weather-atomic",requires:["af-applet-view","mjata-model-store"]},"td-applet-weather-atomic-liteview":{group:"td-applet-weather-atomic",requires:["td-applet-weather-atomic-mainview"]},"td-applet-weather-atomic-mainview":{group:"td-applet-weather-atomic",requires:["af-applet-view","af-message","stencil-fx","stencil-fx-collapse"]},"td-applet-weather-atomic-model":{group:"td-applet-weather-atomic",requires:["model","af-sync"]},"td-applet-weather-atomic-templates-header":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-list-lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.hovercard":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust","td-applet-weather-atomic-templates-list-lite"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-dev-info",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-dev-info":{group:"td-dev-info",requires:["overlay","node-core","td-dev-info-templates-perf"]},"td-dev-info-templates-init":{group:"td-dev-info",requires:["template-base","dust"]},"td-dev-info-templates-perf":{group:"td-dev-info",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-lib-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"social-sharing-lib":{group:"td-lib-social",requires:["base","node-base","event-base","event-custom","stencil-lightbox","stencil-tooltip"]},"td-lib-social-templates-mtf":{group:"td-lib-social",requires:["template-base","dust","td-lib-social-templates-mtf_styles"]},"td-lib-social-templates-mtf_styles":{group:"td-lib-social",requires:["template-base","dust"]},"td-social-email-autocomplete":{group:"td-lib-social",requires:["autocomplete","autocomplete-highlighters","io","json","lang","node-base","node-core","yql"]},"td-social-mtf-popup":{group:"td-lib-social",requires:["base","event-base","io","autosuggest-standalone-loader","autosuggest-compose-utils","td-social-email-autocomplete","node-base","gallery-node-tokeninput"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.applyConfig({"groups":{"ape-af":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-af-0.0.326/","root":"os/mit/td/ape-af-0.0.326/"}}});
YUI.applyConfig({"groups":{"ape-applet":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-applet-0.0.207/","root":"os/mit/td/ape-applet-0.0.207/"}}});
YUI.applyConfig({"groups":{"ape-location":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-location-1.0.9/","root":"os/mit/td/ape-location-1.0.9/"}}});
YUI.applyConfig({"groups":{"ape-pipe":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-pipe-0.0.64/","root":"os/mit/td/ape-pipe-0.0.64/"}}});
YUI.applyConfig({"groups":{"ape-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-social-0.0.5/","root":"os/mit/td/ape-social-0.0.5/"}}});
YUI.applyConfig({"groups":{"dust-helpers":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/dust-helpers-0.0.144/","root":"os/mit/td/dust-helpers-0.0.144/"}}});
YUI.applyConfig({"groups":{"mjata":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/mjata-0.4.35/","root":"os/mit/td/mjata-0.4.35/"}}});
YUI.applyConfig({"groups":{"stencil":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/stencil-3.1.0/","root":"os/mit/td/stencil-3.1.0/"}}});
YUI.applyConfig({"groups":{"td-dev-info":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-dev-info-0.0.30/","root":"os/mit/td/td-dev-info-0.0.30/"}}});
YUI.applyConfig({"groups":{"td-lib-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-lib-social-0.1.207/","root":"os/mit/td/td-lib-social-0.1.207/"}}});
YUI.applyConfig({"modules":{"IntlPolyfill":{"fullpath":"https:\u002F\u002Fs.yimg.com\u002Fzz\u002Fcombo?yui:platform\u002Fintl\u002F0.1.4\u002FIntl.min.js&yui:platform\u002Fintl\u002F0.1.4\u002Flocale-data\u002Fjsonp\u002F{lang}.js","condition":{"name":"IntlPolyfill","trigger":"intl-messageformat","test":function (Y) {
                        return !Y.config.global.Intl;
                    },"when":"before"},"configFn":function (mod) {
                    var lang = 'en-US';
                    if (window.YUI_config && window.YUI_config.lang && window.IntlAvailableLangs && window.IntlAvailableLangs[window.YUI_config.lang]) {
                        lang = window.YUI_config.lang;
                    }
                    mod.fullpath = mod.fullpath.replace('{lang}', lang);
                    return true;
                }}}});
                    });
                });                </script>

<script type="text/javascript">
                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                                        
                        (function(root) {
            root.YUI_config = root.YUI_config || {};
            root.YUI_config.lang = 'en-US';
        }(this));
            var YMedia = YUI({
                
                bootstrap: true,
                lang: 'en-US',
                comboBase: 'https://s.yimg.com/zz/combo?',
                comboSep: '&',
                root: 'yui:' + YUI.version + '/',
                filter: 'min',
                combine: true,
                maxURLLength: 2000,
                groups: {
                    arcade : {
                        base: 'https://s.yimg.com/nn/',
                        combine: true,
                        comboSep: '&',
                        comboBase: 'https://s.yimg.com/zz/combo?',
                        root: '',
                        modules: {
                                                'type_appscontainer_smartphone': {
                        'requires': ['node','node-event-delegate','anim','transition','event-move','stencil','stencil-scrollview'],
                        'path': '/nn/lib/metro/g/appscontainer/appscontainer_smartphone_0.0.18.js'
                    },
                    'type_customizedbutton': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/customizedbutton/customizedbutton_0.0.9.js'
                    },
                    'type_events_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/events/events_0.0.3.js'
                    },
                    'type_geminiads': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/geminiads/geminiads_0.0.4.js'
                    },
                    'type_myy': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid','af-eu-tracking'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_myy_stub_rapid': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app'],
                        'path': '/nn/lib/metro/g/myy/myy_stub_rapid_0.0.4.js'
                    },
                    'type_myy_mobile': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/myy_mobile_0.0.9.js'
                    },
                    'type_myy_viewer': {
                        'requires': ['highlander-client'],
                        'path': '/nn/lib/metro/g/myy/myy_viewer_0.0.10.js'
                    },
                    'type_advance': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/advance_0.0.4.js'
                    },
                    'type_video_manager': {
                        'requires': ['af-content','af-event','base','event-synthetic','node-core','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/video_manager_0.0.126.js'
                    },
                    'type_video_stage': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/video_stage_0.0.8.js'
                    },
                    'type_yahoodotcom_client': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/yahoodotcom_client_0.0.15.js'
                    },
                    'type_myy_scroller': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/myy_scroller_0.0.8.js'
                    },
                    'type_rapidworker_1_1': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_1_0.0.4.js'
                    },
                    'type_rapidworker_1_2': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_2_0.0.3.js'
                    },
                    'type_featurecue': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/featurecue_0.0.14.js'
                    },
                    'type_fp': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_addtomy': {
                        'requires': ['node','event','io','panel','io-base','transition','json-stringify'],
                        'path': '/nn/lib/metro/g/myy/addtomy_0.0.21.js'
                    },
                    'af-applet-basemodel': {
                        'requires': ['af-config','af-sync','af-utils','model'],
                        'path': '/nn/lib/metro/g/myy/af_applet_basemodel_0.0.3.js'
                    },
                    'af-applet-contentmodel': {
                        'requires': ['af-applet-basemodel'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentmodel_0.0.3.js'
                    },
                    'af-applet-baseview': {
                        'requires': ['af-dom','view'],
                        'path': '/nn/lib/metro/g/myy/af_applet_baseview_0.0.3.js'
                    },
                    'af-applet-contentview': {
                        'requires': ['af-applet-baseview','af-trans'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentview_0.0.3.js'
                    },
                    'af-applet-contentsettingsview': {
                        'requires': ['af-applet-contentview','af-utils','ape-applet-templates-settingswrap'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentsettingsview_0.0.3.js'
                    },
                    'af-applet-dd': {
                        'requires': ['af-applet-dom','af-message','event-custom-base'],
                        'path': '/nn/lib/metro/g/myy/af_applet_dd_0.0.4.js'
                    },
                    'type_abu': {
                        'requires': ['af-event'],
                        'path': '/nn/lib/metro/g/myy/abu_0.0.16.js'
                    },
                    'type_abu_event': {
                        'requires': ['event-synthetic','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/abu_event_0.0.2.js'
                    },
                    'type_abu_video': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_0.0.6.js'
                    },
                    'type_abu_video_manager': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','type_abu_video','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_manager_0.0.20.js'
                    },
                    'type_advance_desktop': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop_0.0.8.js'
                    },
                    'type_advance_desktop_viewer': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid','highlander-client'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop-viewer_0.0.3.js'
                    },
                    'type_app_declarations': {
                        'requires': ['af-cookie'],
                        'path': '/nn/lib/metro/g/myy/app_declarations_0.0.6.js'
                    },
                    'type_partner_att_enus_foreseealive': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-alive_0.0.3.js'
                    },
                    'type_partner_att_enus_foreseetrigger': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-trigger_0.0.3.js'
                    },
                    'pure_client_darla': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/pure_client_darla_0.0.2.js'
                    },
                    'type_idletimer': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/idletimer_0.0.1.js'
                    },
                    'type_myycontentdb': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myycontentdb/myycontentdb_0.0.54.js'
                    },
                    'type_uh_init': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myyheader/uh_init_0.0.13.js'
                    },
                    'type_myymail_mainview': {
                        'requires': ['af-applet-contentsettingsview','stencil-selectbox','stencil-toggle'],
                        'path': '/nn/lib/metro/g/myymail/myymail-mainview_0.0.23.js'
                    },
                    'type_myymail_appletmodel': {
                        'requires': ['mjata-model-base','af-applet-contentmodel'],
                        'path': '/nn/lib/metro/g/myymail/myymail-appletmodel_0.0.16.js'
                    },
                    'type_myyrss_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/myyrss/myyrss_0.0.13.js'
                    },
                    'type_nux': {
                        'requires': ['node-base','event-base','panel','io-base','json-stringify','af-transport'],
                        'path': '/nn/lib/metro/g/nux/nux_0.0.44.js'
                    },
                    'type_optin': {
                        'requires': ['node','event','io','panel','io-base'],
                        'path': '/nn/lib/metro/g/optin/optin_0.0.33.js'
                    },
                    'type_changelayout': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/pagenav/changelayout_0.0.40.js'
                    },
                    'type_changetheme': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/changetheme_0.0.50.js'
                    },
                    'type_addmodule': {
                        'requires': ['node','event','io','json-parse','json-stringify'],
                        'path': '/nn/lib/metro/g/pagenav/addmodule_0.0.16.js'
                    },
                    'type_addpage': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/addpage_0.0.27.js'
                    },
                    'type_pagenav': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/pagenav_0.0.35.js'
                    },
                    'type_reco': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/reco/reco_0.0.10.js'
                    },
                    'type_sda': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sda_0.0.26.js'
                    },
                    'type_sdarotate': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sdarotate_0.0.16.js'
                    },
                    'type_signoutcta': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/signoutcta/signoutcta_0.0.9.js'
                    },
                    'type_windowshade_js': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/windowshade/windowshade_0.0.4.js'
                    }
                        }
                    }
                }
            });

            if (!YMedia.config.patches || !YMedia.config.patches.length) {
                YMedia.config.patches = [
                    function patchLangBundlesRequires(Y, loader) {
                        var getRequires = loader.getRequires;
                        loader.getRequires = function (mod) {
                            var i, j, m, name, mods, loadDefaultBundle,
                                locales = Y.config.lang || [],
                                r = getRequires.apply(this, arguments);
                            // expanding requirements with optional requires
                            if (mod.langBundles && !mod.langBundlesExpanded) {
                                mod.langBundlesExpanded = [];
                                locales = typeof locales === 'string' ? [locales] : locales.concat();
                                for (i = 0; i < mod.langBundles.length; i += 1) {
                                    mods = [];
                                    loadDefaultBundle = false;
                                    name = mod.group + '-lang-' + mod.langBundles[i];
                                    for (j = 0; j < locales.length; j += 1) {
                                        m = this.getModule(name + '_' + locales[j].toLowerCase());
                                        if (m) {
                                            mods.push(m);
                                        } else {
                                            // if one of the requested locales is missing,
                                            // the default lang should be fetched
                                            loadDefaultBundle = true;
                                        }
                                    }
                                    if (!mods.length || loadDefaultBundle) {
                                        // falling back to the default lang bundle when needed
                                        m = this.getModule(name);
                                        if (m) {
                                            mods.push(m);
                                        }
                                    }
                                    // adding requirements for each lang bundle
                                    // (duplications are not a problem since they will be deduped)
                                    for (j = 0; j < mods.length; j += 1) {
                                        mod.langBundlesExpanded = mod.langBundlesExpanded.concat(this.getRequires(mods[j]), [mods[j].name]);
                                    }
                                }
                            }
                            return mod.langBundlesExpanded && mod.langBundlesExpanded.length ?
                                    [].concat(mod.langBundlesExpanded, r) : r;
                        };
                    }
            ];
        }
        for (var i = 0; i < YMedia.config.patches.length; i += 1) {YMedia.config.patches[i](YMedia, YMedia.Env._loader);}

        
                        YUI().use('node-base', function(Y) {
                    Y.Global.fire('ymediaReady', {e: YMedia});
                });

                    });
                });        YUI().use('node-base', function(Y) {
        Y.Global.on('ymediaReady', function(data) {
            YMedia = data.e;
    YMedia.applyConfig({"groups":{"td-applet-mega-header":{"base":"/sy/os/mit/td/td-applet-mega-header-1.0.193/","root":"os/mit/td/td-applet-mega-header-1.0.193/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_30345894"] = {"applet_type":"td-applet-mega-header","views":{"main":{"yui_module":"td-applet-mega-header-mainview","yui_class":"TD.Applet.MegaHeaderMainView","config":{"alphatar":{"enabled":true,"urlPath":"https://s.yimg.com/rz/uh/alphatars/","imgType":".png"},"avatar":{"serverCall":false,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"format":"json","_maxage":900}}},"useAvatar":true,"bucket":"201","followable":{"enabled":true,"uri":"/_td_api"},"hasMailPreview":true,"hasMail":true,"hasNotifications":true,"hasProfile":true,"iSearch":{"isEnabled":false,"instantTrending":false,"frcode":"yfp-t-201","frcodeTrending":"fp-tts-201","spaceid":97162737,"cacheMaxAge":30000,"pageViewDelay":1000,"comscoreDelay":3000,"timeout":2500,"highConfidence":true,"autocomplete":{"additionalParams":{"appid":"is","nresults":4},"max_results":4},"api":{"protocol":"https://","host":"search.yahoo.com","path":"/search?","tmpl":"ISRCHA:A0124"},"filterUrls":{"web":"https://search.yahoo.com/search?p=","images":"https://images.search.yahoo.com/search/images?p=","video":"https://video.search.yahoo.com/search/video?p=","news":"https://news.search.yahoo.com/search?p=","local":"https://search.yahoo.com/local/s?p=","answers":"https://answers.search.yahoo.com/search/search_result?p=","shopping":"https://shopping.search.yahoo.com/search?p="}},"isFallback":false,"loginUrl":"https://login.yahoo.com/config/login?.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","logoutUrl":"https://login.yahoo.com/config/login?logout=1&.direct=2&.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","mail":{"compose":{"url":"https://mrd.mail.yahoo.com/compose"},"count":{"api":{"protocol":"https","host":"mg.mail.yahoo.com","path":"/mailservices/v1/newmailcount","query":{"appid":"UnivHeader"}},"refreshInterval":120,"maxCountDisplay":99},"preview":{"prefetch":true,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"q":"select messageInfo.receivedDate, messageInfo.mid, messageInfo.flags.isRead, messageInfo.from.name, messageInfo.subject from ymail.messages where numMid=\"3\" limit 6","format":"json"}},"urls":{"message":"https://mrd.mail.yahoo.com/msg?fid=Inbox&src=hp&mid="}},"url":"https://mail.yahoo.com/"},"miniheader":true,"miniheaderYPos":0,"notifications":{"inlineReader":true,"maxDisplay":0,"maxUpsellsDisplay":10,"count":{"api":{"uri":"/_td_api","requestType":"clustersUpdatesCount"},"refreshInterval":120,"maxCountDisplay":99},"list":{"api":{"uri":"/_td_api","requestType":"clustersList","image_tag":"pc:size=square,pc:size=small_fixed"}}},"search":{"action":"https://search.yahoo.com/search","assistYlc":";_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-","ylc":";_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-","queries":[{"name":"fr","value":"yfp-t-201"}],"autocomplete":{"ghostEnabled":false,"host":"https://search.yahoo.com/sugg/gossip/gossip-us-ura/","crumbKey":"gossip","theme":{"highlight":{"color":"black","background":"#c6d7ff"}},"plugin":{"minQueryLength":3,"resultHighlighter":"phraseMatch"}}},"searchFormGlow":true,"searchMiniHeader":false,"accountSwitchData":{"enabled":""}}}},"templates":{"main":{"yui_module":"td-applet-mega-header-templates-main","template_name":"td-applet-mega-header-templates-main"},"mailpreview":{"yui_module":"td-applet-mega-header-templates-mailpreview","template_name":"td-applet-mega-header-templates-mailpreview"},"accountSwitchPanel":{"yui_module":"td-applet-mega-header-templates-accountSwitchPanel","template_name":"td-applet-mega-header-templates-accountSwitchPanel"},"notificationpanel":{"yui_module":"td-applet-mega-header-templates-notificationpanel","template_name":"td-applet-mega-header-templates-notificationpanel"},"topbar":{"yui_module":"td-applet-mega-header-templates-topbar","template_name":"td-applet-mega-header-templates-topbar"}},"i18n":{"ACCOUNT_INFO":"Account Info","ACCOUNT_SWITCH_WELCOME_1":"Easily switch between multiple Yahoo accounts using the new","ACCOUNT_SWITCH_WELCOME_2":"Click \"Add account\" below to get started!","ACCOUNT_SWITCH_COOKIE_ERR":"It looks like you switched accounts. Refresh the browser to view your personalized page.","ACCOUNT_SWITCH_CRUMB_ERR":"Your account data may be out of sync.<br>Refresh the page to see your accounts.","ACCOUNT_SWITCH_ACCOUNT_MANAGER":"Account Manager","ADD_ACCOUNT":"Add account","ADD_MANAGE_ACCOUNT":"Add or manage accounts","ANSWERS":"Answers","CLEAR_SEARCH":"Clear search query","COMPOSE":"Compose","COMPOSE_CAPS":"COMPOSE","CORPMAIL":"Corp Mail","DEVELOPING_NOW":"Developing Now","ENTER_KEY":"Enter","FIREFOX_PROMO_TEXT":"Install the new Firefox","FOLLOW":"Follow","FOLLOWING":"Following","FROM":"From","GO_TO_MAIL":"Go To Mail","GO_TO_MAIL_CAPS":"GO TO MAIL","HAVE_NO_NEW_MESSAGES":"You have no new messages.","HAVE_NO_UPDATES":"Check back later for updates on stories you are following.","HOME":"Home","IMAGES":"Images","LOADING_MAIL":"Loading Mail Preview","LOADING_UPDATES":"Loading Updates","LOCAL":"Local","MAIL":"Mail","MAIL_CAPS":"MAIL","MENU":"Menu","NEW":"New","NEWS":"News","NEW_MESSAGE":"new message.","NEW_MESSAGES":"new messages.","NOT_FOLLOWING":"Follow a story and get updates here.","NOTIFICATIONS":"Notifications","PRESS":"Press ","PROFILE":"Profile","REL_DAYS":"{0}d","REL_DAYS_AGO":"{0} days ago","REL_HOURS":"{0}h","REL_HOURS_AGO":"{0} hours ago","REL_MINS":"{0}m","REL_MINS_AGO":"{0} minutes ago","REL_MONTHS":"{0}mo","REL_MONTHS_AGO":"{0} months ago","REL_SECS":"{0}s","REL_SECS_AGO":"{0} seconds ago","REL_WEEKS":"{0}wk","REL_WEEKS_AGO":"{0} weeks ago","REL_YEARS":"{0}yr","REL_YEARS_AGO":"{0} years ago","SEARCH":"Search","SEARCH_WEB":"Search Web","SETTINGS":"Settings","SETTINGS_CAPS":"SETTINGS","SHOPPING":"Shopping","SIGNIN":"Sign in","SIGNIN_CAPS":"SIGN IN","SIGNOUT":"Sign Out","SIGNOUT_ALL":"Sign out all","SIGNOUT_CAPS":"SIGN OUT","START_TYPING":"Start typing...","SUBJECT":"Subject","TO_SAVE_FOLLOWS":" to save and get updates.","TO_SEARCH":" to search.","TO_VIEW_NOTIF":" to view your notifications","TO_VIEW_MAIL":" to view your mail","UNABLE_TO_PREVIEW_MAIL":"We are unable to preview your mail.","UNABLE_TO_FETCH_UPDATES":"Check back later for updates on stories you are following.","UNFOLLOW":"Unfollow","VIDEO":"Video","WEB":"Web","WELCOME_BACK":"Welcome back","YOU_HAVE":"You have","SKIP_ASIDE":"Skip to Right rail","SKIP_MAIN":"Skip to Main content","SKIP_NAV":"Skip to Navigation"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-viewer":{"base":"https://s.yimg.com/os/mit/td/td-applet-viewer-0.1.2102/","root":"os/mit/td/td-applet-viewer-0.1.2102/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000101"] = {"applet_type":"td-applet-viewer","models":{"viewer":{"yui_module":"td-viewer-model","yui_class":"TD.Viewer.Model","config":{"dataFetch":{"NUM_PREFETCH_ITEMS":20,"NUM_RENDER_ITEMS":1,"NUM_BATCH_PHOTOS":25,"STALE_DATA_FETCH":1800},"cache":{"max-age":{"SLIDESHOW":300,"STORY":600,"VIDEO":3600},"stale-while-revalidate":{"SLIDESHOW":43200,"STORY":43200,"VIDEO":43200}},"enableCategories":true,"enableEntities":true,"useUuidPrefix":true,"optimisticPrefetch":false}},"ads":{"yui_module":"td-ads-model","yui_class":"TD.Ads.Model","config":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"}},"applet_model":{"models":["viewer","ads"]}},"views":{"main":{"yui_module":"td-viewer-mainview","yui_class":"TD.Viewer.MainView","config":{"ads":{"enableAdsFeedback":0,"enableVideoSpaceid":1,"curveball":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"},"displayAds":{"config":{"LDRP-9":{"w":"768","h":"1"},"LDRB-9":{"w":"728","h":"90","supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LREC-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"WPS-9":{"w":"320","h":"50"},"WP-9":{"w":"320","h":"50"},"sa":"LREC='300x250;1x1' LREC2='300x250;1x1' LREC3='300x250;1x1' MON='300x600;1x1' megamodal=true","LREC2-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"LREC3-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"MAST-9":{"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"fdb":false,"closeBtn":{"mode":2,"useShow":1},"metaSize":true,"z":11},"MON-9":{"w":"300","h":"600","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"SPL-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Freight Big Pro', Georgia, Times, serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: Georgia, Times, serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"SPL2-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Freight Big Pro', Georgia, Times, serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: Georgia, Times, serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"FSRVY-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1,"bg":1,"lyr":1},"z":11},"FOOT9-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11},"FOOT-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11}},"enabled":1,"enabledAuto":false,"enabledDeferRenderAds":false,"forced_modalspaceid":"","enabledMultiAds":true,"positions":{"aboveFold":["LREC-9"],"belowFold":[],"custom":["MAST-9","LDRB-9","SPL-9","SPL2-9"]},"modalposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FSRVY-9,FOOT9-9,MON-9","lrecTimeout":"","enableK2BeaconConf":true,"enableBucketSiteAttribute":true,"offnetSpaceid":"1197762606","offnetposition":"LREC-9","magazineposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FOOT-9,MON-9"},"YVAP":{"accountId":"904","playContext":"default","spaceId":null},"displayAdsFP":{"enabled":0,"pos":"FPAD,LREC","config":[{"id":"FPAD","dest":"my-adsFPAD","pos":"FPAD","clean":"my-adsFPAD-base","h":"250","w":"300"},{"id":"LREC","dest":"my-adsLREC","pos":"LREC","clean":"my-adsLREC-base","h":"250","w":"300"}]},"modalYVAP":{"news":"145","finance":"193","sports":"82","gma":"145","default":"904"},"modalBackfillYVAP":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalPlayContext":{"news":"default","finance":"default","sports":"default","gma":"gma","autos":"bmprpreover","beauty":"bmprpreover","celebrity":"bmprpreover","food":"bmprpreover","health":"bmprpreover","makers":"bmprpreover","movies":"bmprpreover","music":"bmprpreover","parenting":"bmprpreover","politics":"bmprpreover","style":"bmprpreover","tech":"bmprpreover","travel":"bmprpreover","tv":"bmprpreover"},"modalBackfillExpName":"sidekickTVlrecbackfillHTML5","modalBackfillExpType":"right-rail-autoplay","modalBackfillAdTitlePrefix":"UP NEXT: ","modalBackfillYVAPHTML5":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalBackfillExpNameHTML5":"sidekickTVlrecbackfillHTML5"},"category":"","hlView":true,"js":{"twitter":"https://platform.twitter.com/widgets.js","videoplayer":"https://yep.video.yahoo.com/js/3/videoplayer-min.js?r=&ypv=","embed":["click-capture-min","utils"]},"redirectNoContent":true,"rendered":false,"search":{"action":"https://search.yahoo.com/search","frcode":"yfp-t-201-m"},"spaceid":0,"ui":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false}},"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":false,"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalLRECBackfill":true,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"}}}},"templates":{"main":{"yui_module":"td-applet-viewer-templates-main","template_name":"td-applet-viewer-templates-main"},"cards":{"yui_module":"td-applet-viewer-templates-cards","template_name":"td-applet-viewer-templates-cards"},"carousel":{"yui_module":"td-applet-viewer-templates-carousel","template_name":"td-applet-viewer-templates-carousel"},"story":{"yui_module":"td-applet-viewer-templates-story","template_name":"td-applet-viewer-templates-story"},"reblog":{"yui_module":"td-applet-viewer-templates-reblog","template_name":"td-applet-viewer-templates-reblog"},"video":{"yui_module":"td-applet-viewer-templates-video","template_name":"td-applet-viewer-templates-video"},"slideshow":{"yui_module":"td-applet-viewer-templates-slideshow","template_name":"td-applet-viewer-templates-slideshow"},"header":{"yui_module":"td-applet-viewer-templates-header","template_name":"td-applet-viewer-templates-header"},"content_body":{"yui_module":"td-applet-viewer-templates-content_body","template_name":"td-applet-viewer-templates-content_body"},"content_body_mega":{"yui_module":"td-applet-viewer-templates-content_body_mega","template_name":"td-applet-viewer-templates-content_body_mega"},"story_cover":{"yui_module":"td-applet-viewer-templates-story_cover","template_name":"td-applet-viewer-templates-story_cover"},"lightbox":{"yui_module":"td-applet-viewer-templates-lightbox","template_name":"td-applet-viewer-templates-lightbox"},"lightbox_mega":{"yui_module":"td-applet-viewer-templates-lightbox_mega","template_name":"td-applet-viewer-templates-lightbox_mega"},"fallback":{"yui_module":"td-applet-viewer-templates-fallback","template_name":"td-applet-viewer-templates-fallback"},"ads_story":{"yui_module":"td-applet-viewer-templates-ads_story","template_name":"td-applet-viewer-templates-ads_story"},"header_ads":{"yui_module":"td-applet-viewer-templates-header_ads","template_name":"td-applet-viewer-templates-header_ads"},"footer":{"yui_module":"td-applet-viewer-templates-footer","template_name":"td-applet-viewer-templates-footer"},"share_btns":{"yui_module":"td-applet-viewer-templates-share_btns","template_name":"td-applet-viewer-templates-share_btns"},"share_btns_mega":{"yui_module":"td-applet-viewer-templates-share_btns_mega","template_name":"td-applet-viewer-templates-share_btns_mega"},"mag_slideshow":{"yui_module":"td-applet-viewer-templates-mag_slideshow","template_name":"td-applet-viewer-templates-mag_slideshow"},"drawer":{"yui_module":"td-applet-viewer-templates-drawer_feedback","template_name":"td-applet-viewer-templates-drawer_feedback"},"thank_you":{"yui_module":"td-applet-viewer-templates-ad_fdb_thank_you","template_name":"td-applet-viewer-templates-ad_fdb_thank_you"},"sidekicktv":{"yui_module":"td-applet-viewer-templates-sidekicktv","template_name":"td-applet-viewer-templates-sidekicktv"}},"i18n":{"PREVIOUS":"Previous","Next":"Next","MORE":"...","LESS":"less","NOCONTENT_FUNNY":"Feeding the engineers.  Will be back soon...","NOCONTENT_READMORE":"Read more on \"{0}\"","OFFNETWORK":"Read more on {0}","ELLIPSIS":"{0} ...","VIDEO_DURATION":"Duration {0}","SPONSORED":"Sponsored","SWIPE_FOR_NEXT":"Swipe for next article","INSTALL_NOW":"Install now","FULL_ARTICLE":"Read Full Article","ARTICLE_SUMMARY":"Read Summary","READ_MORE":"Read More","AD":"Advertisement","SHARE_EMAIL":"Share to Mail","CLOSE":"Close","RELATED_NEWS":"Related news","START_SLIDESHOW":"Start Slideshow","CONTINUE_READING":"Continue Reading","CLICK_FOR_VIDEOS":"Click here to watch the video","CLICK_FOR_PHOTOS":"Click here to view photos","AD_FDB1":"It's offensive to me","AD_FDB2":"I keep seeing this","AD_FDB3":"It's not relevant to me","AD_FDB4":"Something else","AD_FDB_HEADING":"Why don't you like this ad ?","AD_REVIEW":"We'll review and make changes needed.","AD_THANKYOU":"Thank you for your feedback","AD_SUBMIT":"Submit","AD_FDB_ERORR":"Please select option. To help us making your experience better.","AD_FDB_SOMETHING_ELSE":"Tell us, why you don't like this ad?","DONE":"Done","READ_ARTICLE":"Read more","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","REBLOG":"Reblog","REBLOG_TUMBLR":"Reblog on Tumblr","UNDO":"Undo","SIGN_IN_TO_LIKE":"Sign in to like","LIKABLE_ARTICLE_SIGN_IN":"Sign in to see more stories you like.","SIGN_IN_2":"Sign in","READ_FULL_ARTICLE":"Read full article","COMMENTS":"Comments","FACEBOOK":"Share","TWITTER":"Tweet","EMAIL":"Email","CLOSE_VIEWER":"Close this content, you can also use the Escape key at anytime"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};window.ViewerClickCapture||(window.ViewerClickCapture=function(){function a(b,c,d){return b?b.tagName===c&&b.className&&b.className.indexOf(d)>=0?b:a(b.parentNode,c,d):!1}function b(b){var c=a(b.target,"A","js-content-viewer");c&&(d=b,b.preventDefault&&b.preventDefault())}function c(){!e&&f.removeEventListener&&f.removeEventListener("click",b),e=!0}var d,e,f=document.documentElement;return f.addEventListener&&(f.addEventListener("click",b),window.setTimeout(function(){c()},4e3)),{clear:function(){d=null},last:function(){return d},disable:c}}());window.ViewerUtils||!function(){function a(){function a(a){return a&&"object"==typeof a&&"number"==typeof a.length&&g.call(a)===h||!1}function b(a){return"string"==typeof a||a&&"object"==typeof a&&g.call(a)===i||!1}function c(a){return"undefined"==typeof a}function d(a){return null===a}function e(a){return c(a)||d(a)}var f=Object.prototype,g=f.toString,h="[object Array]",i="[object String]";return{isArray:Array.isArray||a,isNull:d,isString:b,isUndefined:c,isVoid:e}}function b(b,c,d){var e=a();if(!b)return d;if(!c)return b;!e.isArray(c)&&e.isString(c)&&(c=c.split("."));for(var f=0,g=c.length;b&&g>f;f++)b=b[c[f]];return e.isVoid(b)?d:b}window.ViewerUtils={getObjValue:b}}();
YMedia.applyConfig({"groups":{"td-applet-navlinks-atomic":{"base":"https://s.yimg.com/os/mit/td/td-applet-navlinks-atomic-0.0.54/","root":"os/mit/td/td-applet-navlinks-atomic-0.0.54/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000195"] = {"applet_type":"td-applet-navlinks-atomic","views":{"main":{"yui_module":"td-applet-navlinks-mainview","yui_class":"TD.Applet.NavlinksMainView"}},"i18n":{"TITLE":"navlinks-atomic","TOPICS":"Topics"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-stream-atomic":{"base":"/sy/os/mit/td/td-applet-stream-atomic-2.0.439/","root":"os/mit/td/td-applet-stream-atomic-2.0.439/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000173"] = {"applet_type":"td-applet-stream-atomic","models":{"stream":{"yui_module":"td-applet-stream-model-v2","yui_class":"TD.Applet.StreamModel2","data":{"ccode":"mega_global_ranking_hlv2_up_based","more_items":[{"type":"ad","id":"31355634988","title":"Retraite insuffisante? Placez votre argent","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=pmW0nQIGIS.N0wRuYJhinayilO1ceKlXzl6uRCQ8qeTfwl9QpA2R9rdiINZXXnPuWhRptfG4ASA0jN2LW9AuBUdXqQvh35uuy_RgGAE1Khf2VjqHNoI_DXeC5xRnsxb.k9nPlucIMNqhNx0ngEIJB5YBve_2hWd_d0PBonXbl6UFJReNGSoKsOAqOQyUqpEtSxZbEdZP8Yvhf6bLX5dCbDbjSbkqrkzP7eZLMjascTgBYXcZzqp22jbqdumcudLrJVQQGoqu6uxGZQ6RvWPQ.jtGsxK73XTVjB4TJcQw9FsHvCYIyozrst3rcnKAQehBKAc4XavKDRTlcKigtjFMVpEIAyudkuyptejQzNcoygZZ1pSodKlceubYsg5fJyBhYegG68rVKxuhN5vtFBhzFX0_74FOiL1DXcvcNIvM5XwfBRp.Rm7QzUKEbESFZJsDA0_g0bPIUML0DwmLyAGMkUevKfYZ8AfDajv2ScegnGB0.2IeHlvw91jzG8EwdVznRtjMqvVbHTWjLfZ5n3BRQpiZ_iquDjd6RAQxg7cx_39gObVJk05vDJ1s7utpYTImP9E.iIjelaYms0BfLsiZJ4M_PlzVFTK2gSaG0w6jPe8KVZO76_weaeBYmNMfhJmZdPL5IxHTp5DQWw--%26lp=https%3A%2F%2Fserving.plexop.net%2Fpserving%2Fbridge_002.htm%3Fa%3D4%26t%3Dhttp%253a%252f%252fpreg.TradeLG.org%252faserving%252f4%252f1%252f1004%252f2_fr_2822.htm%253fSerialId%253d1115580%26adv%3D1%26f%3D75185","publisher":"TradeLG","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=5PUtfqEGIS87O8mQxVLijor0GwVBc1L37lUeXu6Y9LXO.zmoLjTTCfrQPIl_aY.V35RKklEJoHdQrjYt2E7Dlq95.TZnGvrE07Wfzju_qaI3J3lsTNxBOQ093t15W3LsPl1ToOO6UaDzxOckUGVvAq4EoXIn_Ef3nPI9pdx5g0y3I7XKsr1_luH7ayDJaCiwLuaX7eRsUNtZLGQxefPl6Vzfx_hNXN.SoW7KEwoDc1C61IpKTKNJsGF.VMyehV3eWumYso0zHlMGwFDOA.SHUPJ7ZK5qYCjndyaz203.uACq4WPVQHiCskasBZClApNItqbdU8jHsXxi85RJvk9pJlktKR6Eox3eJQr1TbVTLfn.GiwvceWktg3EGEyWyTw-&ap=13","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15t478bk6(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299691000,li$0,cr$31355634988,dmn$serving.plexop.net,srv$3,exp$1454787499691000,ct$27,v$1.0,adv$1135676,pbid$1,seid$4250754))&r=1454780299691&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/ioOxG20cc7.LRQz8HocIkA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1447685626349-6650.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Toutes nos astuces pour investir seulement â¬300 en bourse et multiplier son placement. Offre spÃ©ciale dÃ©butants","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31355634988","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":0},{"type":"unknown","id":"7314a95c-f234-3168-85bd-0ee6f5c23f6d","instrument":"4000020130S00008","is_eligible":"false"},{"type":"unknown","id":"76e1356b-0bf0-36a8-b916-b6a530ea76ee","instrument":"4000012670S00001","is_eligible":"true"},{"type":"unknown","id":"067d8d1b-7c0e-3763-8203-e4a2807e3d9c","instrument":"4000024230S00008","is_eligible":"false"},{"type":"unknown","id":"8bd28b4f-fabe-34cb-8860-e888eedc3d66","instrument":"4000016100S00008","is_eligible":"false"},{"type":"ad","id":"30956615759","title":"CrÃ©dit sans votre banque de 3000 Ã  40 000â¬ ?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=3fp7ufEGIS9J1jyYYVrA4qwH1IhsTBILAxwjMBrGdOvt6xSVh.k.HVvhLkTCpj5SE6XGXeDTtHSYka.xNir04fhRe4APGVFdfC_WrKfoc4SZJvii55jw8urZwZQDQqW5eStXRZc35iqvj987CvtsIumGK5QmGTUJI3LKI1QKGDXioTKbb_2T4U63IGtPB_639a3w3OZW2UeGV8RSjojug7SyaAZTdO5FzFaSuM8N6YoSjHAAsYMLSNnuHVEI15gFMJWgnajlWNFjyAx.t8A_PnhaGVD2Bwfj0jopLIAZxb1ekkE5J1wrjoPTn0dz7d1b1P.iVmFglX0ywUqh2yyy4zC0rbqKab6XuVlqa3Rys60KOr775mC.6LM6_Z6..OnuilfNIiCn0u9oQEBqih8C_Pgk9YDhTmDUTipbZKD4zo9z0tOR0RQWMq2xox0KVBNuYZh82W2W6zXxCdLkdnbgZJluG63vd.DBCDSyP1fPVqMySuRDCoJRz0WB.Ouwix.8cP6ij5fgiS1NtfhOSByeXVqSLnft2DrH%26lp=http%3A%2F%2Feulerian.pret-dunion.fr%2Fclick%2Fpret-dunion%2F8lL.QlYVeQ7BL6AqQORYZxF9DLsCl0v4M9rF8SZrlA--%2F","publisher":"PrÃªt d'Union","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=eMQbsCkGIS80AYblhvotwBs7zG8yWitsfApg4qY9btleFudgNB2wkC6A8eZZDwHGHyhodtLokkHZN1k8jshJc3P71fn2ekypFDzpGjdzv9_uBBScjW4LovPowhZWAayulTqJSmHChAds2tSkgJMfHeHe_iITLHKkdfVPQuqGgImvM__LuSd59qHksQnNfAsAZ9GxREitC.m_m1Qz.tLDpsmNBnJaYU3Y4VpqO1MUgopsipb.P1W5YkMlgn9wANz7KkZDW_KYfCqupBkNiecoiqXxTp9K9u8KZQXQg8XC7FOaCofvSxJme.LgiihwvujL87vMDOazrDPG9RA5D3RxbhL9R8yoSwfd45IyZVqxkjS6WMKEaWy1YgBvv.H8d0oOSnAYpSHw&ap=18","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15ojb4rpt(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299691000,li$0,cr$30956615759,dmn$pret-dunion.fr,srv$3,exp$1454787499691000,ct$27,v$1.0,adv$936665,pbid$1,seid$4250754))&r=1454780299691&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/N6mu6NleoxbGeoZ8pC9ejg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1450461698230-3205.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ¨s 48,29â¬/mois, rÃ©alisez vos projets moins chers sans les organismes de crÃ©dit traditionnels et plus rapides qu'en passant par votre banque!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":2,"cposy":2,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"30956615759","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":1},{"type":"unknown","id":"d35885df-09ec-35ad-b429-ec98f29e5d0f","instrument":"4000013590S00001","is_eligible":"true"},{"type":"unknown","id":"57ac6398-7de4-3444-8d09-58ca016e023e","instrument":"4000016730S00008","is_eligible":"false"},{"type":"unknown","id":"66518e63-f6d1-34e6-a9d9-aacb52a07962","instrument":"4000015390S00002","is_eligible":"true"},{"type":"unknown","id":"b7cb240d-365c-3bd1-a89f-4f37b8c66dd9","instrument":"4000014000S00008","is_eligible":"false"},{"type":"ad","id":"31700964751","title":"Les joueurs du monde entier ont attendu ce jeu!","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=eRD0P1oGIS9Xh4klZLoXk2aVk2o52E0xULMSYuKyHsUggURy8NqaNRqv_9vtlmbB5ibboOxZhKh5FLb_9PURoE72wSrdIv3HMBjf1lkF4QpBCsJ8fSjH4qqaxht7tw47GUyMNQ2jOj6TCuSJrInIhcZkeprXl0wP2o2rnjgNrcY7WRh8_N5t1O33GAkbirpHljHjnSXwSPFEy_I_mSkpebd7yRPk4m1zUHYByWokjabAih.mQ0dvGyEHjIoRSUUGTnhwe6l71b8TAKbNzotNqdjKJS09.3Fq8nQFz4BU2vewhbCm4yYHSv2sieFR5e3FhKr3uhSnwbvIsiEPlnJOBqq4.F107dIGh_bCexjh3bry.9FyJwcxYy5NLrdKCxgPvXE64QYrMWIQrdVcPfsuwYS1GCWC8td6Ubs92WZYqROXh_Aoug6KF3T6sbChDOSv2L36wqak_XVopIwVXzk59C1LOJyrNb_2Mr9W.vvLiLEbuW6wjN7zt_0hFYs5eRwfYtIExfEXhX9YmRUP55a3yhj4qZ1ojuEwfs_bJRgKF2u6WRj.IiMFQKtT6qV09FGOY_dCm_NvzmP90y4pmK8Qx4Oj3c24uOuE8hDEDzCDCoXiMKVrrS.OZweem4rnsmdQfg--%26lp=http%3A%2F%2Fwww.mnbasd77.com%2Faff_c%3Foffer_id%3D1384%26aff_id%3D1065%26url_id%3D4561%26source%3Dysa%26aff_sub%3DFR%26aff_sub2%3Dn244%26aff_sub3%3D31700964751%26aff_sub4%3DBoundary%26aff_sub5%3DB","publisher":"Plarium","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=6IDFIqQGIS8OSATI8qkpVjpGExRKwWXUQZcv4gHycK9Rk41KDBdGvaUDmJtp5v6IM3wgj2Pi0Rc.9dO2rcpMe6IsmzVypId_MMuRR0RVIDPOESYOhovyTzLoriuf3F1rX8eLYn7fbXnzlhfrEniPZC1V2MgDkVKpEOBU5_BkITh01pckotztInbCspNG7B29uf5ZD11TahtTsy6U3BOZ6yZ8uwLQsVNdJPtlWP7hUpFUQzJfKiCv_Ij961RoZIoH4hJDLIlGogrRD3M5YdN9rFRAeMr9broThRwvbLiC4S1AAwCFB.UzGpkSZ9tpYcyepzHHtjj.VsJ5wOfLMP4gMB0A5pRorOFEB5eKx56S2OZ.HKIu0bHUejFA4d8fXu4JqTM-&ap=23","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15jq97op2(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299691000,li$0,cr$31700964751,dmn$plarium.com,srv$3,exp$1454787499691000,ct$27,v$1.0,adv$5584,pbid$1,seid$4250754))&r=1454780299691&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/rPf_QSlPktpSpX99HLQG6A--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454616775564-7441.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Pourquoi il te faut jouer Ã  ce jeu MMO de stratÃ©gie gratuit et addictif? Le MMORPG le plus excitant auquel tu as jamais jouÃ© ! Ne passe pas Ã  cÃ´tÃ©!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":3,"cposy":3,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31700964751","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":2},{"type":"unknown","id":"7395ee87-5cfe-34ea-8448-140e8375dd2d","instrument":"4000000410S00004","is_eligible":"true"},{"type":"unknown","id":"bd8ef7d5-6e34-3344-931d-14ffd216d01e","instrument":"4000016660S00008","is_eligible":"false"},{"type":"unknown","id":"d841755a-824c-3585-bee1-81398b105624","instrument":"4000015170S00008","is_eligible":"false"},{"type":"unknown","id":"395c14f1-585c-353a-aea5-46ec8311647a","instrument":"4000006770S00001","is_eligible":"true"},{"type":"ad","id":"31590019210","title":"Plonge dans la plus grosse bataille de ta vie !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=84A75qgGIS8EPHiD2ZpjY4R.gvM.P6nJKw6aRMZRVMYPmRGgxTk7jS3k0nVmFmKsxjU82uV8NEwfL2cntueeOAYXiewC8vbcDRde4zmVtJuL.2QjjqdqqEDcAF7qVRvorZnl9H32nCgXKJ6g2pJ01gydkGtzmP0StEtUU.54ErhyFt468IElo5sqkYln7GS2I4yYxM02R5AeGpxmE6ZccTy_6fgTEqeNUKfARXkkoxijWK34ZNgM6j6vmnjkCVOT_Btf0xyUNZ2UmfxSekAMsldqGpDSStD5GkXtfTXdRIrdzPJf3SefGbHeSS5.h1IJFzknuIawBf44vHStJWhCW7ntdWh91pYLufR2qlnCMyKNOGztlHJXPZf1x4h38OmMA.VnmCfuBN1Yccd6eRPMeiG3Hgw0yW3n_N.5hL.IInDMWxssVsecQ2cNwypFtP5uCKDJB6yygPSUtzHSlq_vXooqfQyJ_29D1pd9lW3jQiSx4KfSuksuTSM1gozTJXGpp4qNhHxzV9xl.gank3Zbu9UNbHDkvDCHw.48FK3LHNMljXYitQVyM0lCV.r46FMAZ0w-%26lp=http%3A%2F%2Fplarium.com%2Ffr%2Fjeux-de-strategie%2Fsparta-war-of-empires%2F%3Fplid%3D82731%26pxl%3Dyahoo%21%26publisherID%3D%23sid%23_%23creativeID%23","publisher":"Sparta : War of Empire","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=pq77pOMGIS9tAPqS1CbIx1UKtKtAM5WdSTs6wf5ZyTELmkR8_Q.E2pd1avp9qvhz.OnojD7m6khJNjYZsQfedEh65N.GKAZk9k7qWapdZ_Km3r.NYYlVvMcOG1HZVmjk.vckqQgFztQH9z1TryBYjWUeLpawRjH1NrRFR7Vkv_ZOKc13q7XCyrpZ1XzzXAgn2gFlh6pJ6AMQX2kuqMmKUHqtlaYPPXazu6VEBv333EqoOPOswVejp.eyRJxDK6YkZ2grGuER9wLi2BdxwVeFByew7BQVAevnW.B3VEeMCRQQjXAS4fvlssl0lAxYKS8haJXbSXKhDMDMD3FyNjSn1WdNE22zWMMi0V6Qj.vpuygwN2LLBW7xGkrxm8rDbQtliuw-&ap=28","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15n0u1nra(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299691000,li$0,cr$31590019210,dmn$c31590019210,srv$3,exp$1454787499691000,ct$27,v$1.0,adv$1280969,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/pnD7UE9ORpKoXSP2Ef3TKQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1450170379340-315.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Le succÃ¨s qui fait fureur dans le monde des MMO. Sois de la partie !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":4,"cposy":4,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31590019210","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":3},{"type":"unknown","id":"1aac4e11-2cdc-3c06-8da7-06d2842c2420","instrument":"4000007030S00001","is_eligible":"true"},{"type":"unknown","id":"125a36ed-2822-34dd-ad69-ff22ee2dac76","instrument":"4000009500S00001","is_eligible":"true"},{"type":"unknown","id":"640a6a8f-5101-3f54-a344-174371fbbefd","instrument":"4000009930S00001","is_eligible":"true"},{"type":"unknown","id":"c-f7c3a38a-2b51-40bc-bc7e-582755f9e3ae","instrument":"4000001860S00001","is_eligible":"false"},{"type":"ad","id":"31638139234","title":"20 min/jour peuvent changer votre vie","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=ouFc3HoGIS9mFzKhQKKmB5KvRsi98bKLDPBi2Ix2zalsjT4E64WigxS51poaOSxTx7OB0XfBcw10.wWf2ZIWpZOEqf2Pf3DanCe4K9T3s6txwVYGQfa5DUPk1Yg3lAglNqYfBPjzwuq8LRTDbWSC5v2Bv1uTcri9lWIbeXHQ6wjR9JUiRQC6XqFre9HqJcfC5BRxPCb5xZQVvjJwNI2Bh5Z_9zLTNmGAquqvMbSInhjrV05nNzMWF0bFeHINLjrmr4Vy05L99Wxfw856CcMg6dt2KaXDHFDeC36zLaLONyPpQeypLKkiEcIvsPNvvBaDAIhMJJ4lOzcKPvuQGHzkGYkZkyEXF0C6mdCaB7.IlhB2iIoAT1NtuXlG1hPmb3ckC_z470AmyWwxDRp1rT_CtTWI_nnfn3qYwNMRFQQssYC2xwG4vpHXgNZOTyYGtV7SoaxMvgptungDS2mnZoTU0VH3Fu87wLiwsCxZGtHz_MQE8FY-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F6c6a3f4d-247f-4856-a332-31084b82308d%3Fad%3DCHANGEME","publisher":"Preditrend","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=TlnMIloGIS.39udw2xtdIXUuTOP3EVSMHAlpwMvFGTJRKxY3iNar6QtwEbAwJwm76DIEMIzE8BgtdR67IM61T4Arsfs6qqinX_xDdsu4_ReqbvLc8IlAhpOKKCG1Ztbcz0RHlnJJ6tDqiUBJwd2fd_j0DWCjsobAk.bwW4ca3OmBdCQtvYhcclSuBJFo7_1tJ6bELZfr2fLbSzaK6MP8bkKNm7vjGIaGhRcSfAYINOGJMiUXjIUaSeNElROVG_dX1Yi2AN0GU4Bc05U410VFZl4P0UyA8vxjMpPkxmtm1WSoqh1hGkn1KbtSEHO5kfjhwYUr9SRX14Yjw8cRvX9IXJokl_cbwufVVD0HH5rs8AqA3g8_iXRopvyldBFRVusGkrpN.1cEFgIokXxxigKm9jb.813q&ap=33","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15nqu9aif(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31638139234,dmn$c31638139234,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$1167625,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/CewoJTO0Iquc_XhABGLsGA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1450529481275-2933.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Les nouveaux marchÃ©s boursiers permettent Ã  des FranÃ§ais ordinaires de gagner de lâargent en tradant des actions comme les professionnels.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":5,"cposy":5,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31638139234","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":4},{"type":"unknown","id":"24d5be08-9e7f-3eb4-b8b2-fc802716e42d","instrument":"4000007150S00001","is_eligible":"true"},{"type":"unknown","id":"c0401c6e-2d55-3710-a177-801f97aced27","instrument":"4000006670S00001","is_eligible":"true"},{"type":"unknown","id":"ca146bf2-281d-3c29-9d98-8ca7926e9105","instrument":"4000007370S00001","is_eligible":"true"},{"type":"unknown","id":"dd137f88-7682-398d-8d2e-70d61ea6fdf6","instrument":"4000020020S00008","is_eligible":"false"},{"type":"ad","id":"31686653990","title":"Wall Street Furious Over New Trading Algorithm","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=AduYFTwGIS8w3Vc2GKaGoH1D9CEIuZTqlDMJ0NYbRcXhJIfl2FX8xsGMg0ev18aEKzw.DRg8T6tlaf1v7DuIWZXNVjKsNBq80dSsQ4YLQh0LfCWy34GqAK8DT2AoyDYhwmAT0._yXlTcydBHDZ5ZfNObbKYe4aj5XZkJ1n0dqGs8zMchG0XcjKD5utATq8qnAlf6BFm2w8iFo9o1LplyppdAMZuKNdsBaPMMflrsm_NrV8q3Oz9rF2NGn4nsVqA8j8BWosAkM9GqgmeRniKf7ATDXCuQjYOx.LkhrdxnVen2INjpAjPxuNb9ectdWwFrqVK7mrKO8Xfdsu4s7Bwrlp_RJjKu9LQqGw9p5rHEqblT3DFAU3JTZtzJnrijS3kN12Kx9phY2MT0zRlAyWdGXluzlkiAu75jvpfQGisAIGv33mfZCQRJgEdP0DGx4S.wyQQdVG2vzmCQbfbk9YEBDHkre5BdjDzVkENQRJDb8dRfI_pgQfG_gJebLqCth2Atev4tgIuiLTK2tQmMFTkd8MO3xUfCNuYkwz3J19194PVPCBqRknGTjAcGN9k_u5sOAWoZ%26lp=http%3A%2F%2Fwww.mnbasd77.com%2Faff_c%3Foffer_id%3D1777%26aff_id%3D1065%26url_id%3D4475%26source%3Dysa_FR%26aff_sub%3DFR%26aff_sub2%3Dn355%26aff_sub3%3D31686653990","publisher":"Ultimate4Trading","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=00.xZlwGIS.80T4izorjJ.TNDsq4MRwjPhc2wkWKTCHOikKqBWd1_KRPm58G_nSCdWmUSauq__CJpfnufMxJj6vd2CM.UcqaLeZATgqZDaiPA7TNgBCmadzVL.Y8z_KjMU2KdXUFDl9PwEJ4wyVtOzu6zN1RX5Z_M.PIbocid4mgR.c7rXO7WQotU5qs__WClchUZxdXCfx37oNoTA2Xo5mMdayLdHsfPg2mbNGxw1nWWmqDcxdr0nYOz9YheybPn6Mtrx9gYFlTiSm2kNdjZd9uNM5dHaeXbGHqfvS1NAlj8kCl5DrNrzGMTKDKxLJyoVQ77VrOFVQpu9hnpnT9ym1Q3NLITENRK3289pFellrWCjOshjwWcfH8&ap=38","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15n3nsutr(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31686653990,dmn$start-up365.net,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$5584,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/6eyiXH0RyOsxvsXUssg..w--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1453893807697-8606.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"This couple has created an algorithm that let's every day people compete with top players on Wall Street and it's taking France by storm!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":6,"cposy":6,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31686653990","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":5},{"type":"unknown","id":"3afa0937-6b95-37b7-89e6-7dbd566b7bbb","instrument":"4000015420S00008","is_eligible":"false"},{"type":"unknown","id":"8e8dfb66-19d4-3cf1-a287-f8a4b465a70b","instrument":"4000003580S00004","is_eligible":"true"},{"type":"unknown","id":"e8459931-0c9c-37ef-8468-9d795e693c55","instrument":"4000007540S00001","is_eligible":"true"},{"type":"unknown","id":"63f70efc-edc3-3a27-b265-eca9d21833d2","instrument":"4000008590S00001","is_eligible":"true"},{"type":"ad","id":"31104577720","title":"Are you ready to learn a language?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=IQAf.mYGIS8KEH4jNd.eg_qIJ.Lz9ilMNEQPdwZAwvnf3NvDnRBYg.ZV5KeSyepfzz5dhrhXt0ISD4YgObY3Zfsl5wNgmmjNvwMWcDEWW7bY.GJT_qkSJJDDF3vHYwyMB2vkvLhdTAr7XgAvtpKNalYoDhVV_96IZr8rbDy1BW6sBObs8LkGT6qE7dardPu4qWzAnUFYp2XC.uEjdteGALpR.8WttNXycLjnTFnGcGdP_U2RAAe.9m41hcpNt4J697fktN3eBu0.CXpHhpBV2GCIIFRnCu8FlmRzQYJMsThppnKJqXEWsnr0d5jkFyDVY_g.gOoFkuebP68F1.n5lgpqoZZeRAwiMm3ZqqJS0jWVSHN36POahOQs8V6JvnjsgHQ87eP1vNmKq0x3ov2CPlICG_odY3YYN.knTIBeRWpKTNaGyUWl2Lv2RRGe_4xChVfIhD4hVK9LAPQzpoItc23L1LcUWIk-%26lp=https%3A%2F%2Fadfarm.mediaplex.com%2Fad%2Fck%2F19766-154450-6097-13","publisher":"Babbel","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=I8IjaCAGIS.3znZde_6hEGXrcgpgPsgGlBwPTGNm8ZC4ExW1WMV1dZFsYmuRAXziohufhCxUwhvUHZQVvZfY1OI.2xRdl6wGOtNj6M5SUJg8oeh0_Y0DSlKY9TXsjEtQNLCsfbz9qT3MokAAZJY98JfJIS7.wBOMnO602adowk30UUNRXYbZAuOuiLH.T.H1EjEEIrIqZbkca_Bq2UdDNGF7xwejAimCXGeuJ1PR7qnjr5_2mwi78o4t4Lamt5bN2oYblPBcOKFpkepIWmqPkjyb67NY1hwc3APpn2_BOe_dLAjdFnH3LFXjl0VhuJXeMFI65SSkvPv4vCNY.yFNqQH2r6Y0_Jp6w2eTMdbkQfAuMT6HTt3MPOOOKe1mZnszTg--&ap=43","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15ne7c74d(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31104577720,dmn$lp.babbel.com,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$959887,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"ad","images":{},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Babbel gives you everything you need to speak, write, and understand a new language. Set your own pace - learn what you want, when you want.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":7,"cposy":7,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31104577720","ct":1,"pkgt":"sponsored_noimg","grpt":"singlestory"},"actionsEnabled":true,"index":6},{"type":"unknown","id":"7d0c45b5-a93c-3af5-9512-a93c576412fc","instrument":"4000008480S00002","is_eligible":"true"},{"type":"unknown","id":"ea654e1b-79ec-38b4-8954-ed1494471032","instrument":"4000007200S00001","is_eligible":"true"},{"type":"unknown","id":"3ee846e9-49bc-37ca-80da-391d2e6057bb","instrument":"4000007160S00001","is_eligible":"true"},{"type":"unknown","id":"4d7e1917-65ad-341b-9cca-fa9b936c3bd0","instrument":"4000006390S00001","is_eligible":"true"},{"type":"ad","id":"31703688673","title":"DÃ©couvrez en exclusivitÃ© la web-sÃ©rie Lesieur !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=wUUDZGsGIS9ohO_TKeaoqSZFZUBxRuLfiK3_Gb1.IQJB_quMKTxGUqT_fLUWI39KwNSlrBoKVPOp8TBTfCuzbSxb4xpICpUWE9t0kTHXq4VsNuFb9dXTMnF5trIAG94KKnrNF1rFAnDGfcdODQPsRjwQpbhi7d_zW7KAatZAbGH1zNa9dKPU0qUaSqil2RygpHLWBFujpspZCM6bAscaJWctXvNHzQbrWbycM5W3TGHY95PR1BrevU1LuK6stLYe9LIM0kdcGWII.RDYuZCx1tmu5SwuiIQlOQK5rVtSukHIZLrSWXtl1o2v6t42_ha9VjNoNbuZpeRm7svBVUAytRj2cCJK2aIPOL3Ii_oD02uq_8S5zWgSZnQJHD_dx9Qn79kJWPBPlfIRiY1CiEzy7iKbbRvIR_lsf22lQrdZYcQRVEK9bA8bfWXnnF9FMmptVhZizTD4i1meulzCvwaE0FLlpqRi9ECfQmT01j2ljAQbYUAcAKwntPH2HkuM_3jQL1sN_RFUo8N0zLZhf.aG7Tv_1zfUJePyTojXLuMaJ19m4HUZ19P81hKl4MeZrApHHWuOc5Z.RAcMtJQfgGNswTvZ1euqSLawt_FuaxFS3Bl81Mof5lD_Gpx28a0zgw--%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN3021.1940901RADIUMONE%2FB9347836.127388044%3Bdc_trk_aid%3D300492169%3Bdc_trk_cid%3D68214826%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D","publisher":"Lesieur","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=Cs1x.XkGIS8H7DpK_uuBO5K3fvi2ynnytXuSjex.HLWdrHxDsZphN37pITR.ksvTGgcc7pTtjijaJbPf9Ga5GDI1MLph57hCUaLikS58FYRfmyjITuYb3E4x_4girGWa8GdktWjftBFktPyEt7im.c5e33PvKxSBopUY1YW1e7icps3QS4KJE4XBzihFGxHfEM9pW3eBLU615XespD7SB7PbjYpFkzkilJfVZLc0whvBuTt8DOKRxzyktXj5V3wT4LN3tYYN_avOQc7D4u_GT.z1Cp_Kt9Abd7Etx_tbnIxEe5_TMWSiYIzz8n5OWZzal7L5gEFrXvT.O4X2lvUTnEY0_nL3tb7wSq_5HBA.Sn.xMrbDBcQFkjnUtwQKfsfCxjsqDLoXz52xlb3yqZbKs9LPz5dXtXMYpw--&ap=48","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15lv9n74d(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31703688673,dmn$lesieur.fr,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$1340700,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/LdZPdL2JEqNo7z9e4kfOZw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454692557335-5766.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Fred Chesneau part Ã  la rencontre de ceux pour qui cuisiner au quotidien n'est pas facile. Des plats simples, savoureux et pour de petits budgets.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":8,"cposy":8,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31703688673","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":7},{"type":"unknown","id":"c-ad746bd4-a200-410d-b4ec-52e10f6fbaea","instrument":"4000002360S00001","is_eligible":"false"},{"type":"unknown","id":"aefc99b4-f740-39e7-87d6-11d7504d91e0","instrument":"4000014980S00008","is_eligible":"false"},{"type":"unknown","id":"d53e2217-edaa-361a-a0f9-b0f5e7adcb35","instrument":"4000013220S00001","is_eligible":"true"},{"type":"unknown","id":"deedb0d0-0565-3f3b-a472-917439742b09","instrument":"4000001610S00008","is_eligible":"false"},{"type":"ad","id":"31687619319","title":"Votre assurance santÃ© Ã  partir de 8,10â¬ par mois","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=sG1vnIIGIS9lLG4UNvFw5yLgkiClqIMazhLJyGkHibCJnytDBJNYrS_c0a.OV5Ab20CjtBURXGoXIbfHYiAm4aClNRRJh_LRjHNaTri1lLr5o0aggDCgFjslEwkBmkv_nCvMBcrsRcCzDwxKsnl2DZbAdqbH9o2.rVgK0oAgdGV4Bgmuffj35ysfjgupeIEVsS.UssKmdO9AF05no5YKTFj1sOaqkTGRxyTzBJZtClhRXFA4q4l8AYCYm4_4SXbRKG3cUJJz6sDDtkS1DYWpnR2xoQ9.WAlPjZ78EJMjO0_xjg1V.uJopttUyn4XdN.fUkcqD3vMShrf2SaYZipn5x5NQrcUrhSTaydke5NuGNideE9nDd.0GBXGwp3DFqeoikbGD.4mBPZKct2y30FKXX0ydXtx5i4XcXsfbZVUr_q4hNyYwbeTww354Ghbe3tn11sgO2K7EssIjiqpf9uqct8CQA--%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F299229982%3B126242518%3Bs","publisher":"Axa","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=KDNsUzYGIS9WIz4xCuVSwpi8l5CEbT_MA1v..O01C6qu7te8C0BPk6BGs07n._G_F3Jbwk15Ml04GX6D418m8eBVtlILqskSbgRXp7djVTsbLeE2iJ0_xpx7TFJTEbK9tWPYpkoMiDcYXGe6Y9NufEI1V8kesJFAhpfxSRGPRob6_Ggn3BweCzupHfVKN8yuvsDovprT9wKca2r.nXirJaRh1PML8pVk7BUa5qUu571uo05wJA9ZuMnF.4Q6rdvhKTJwPIG9glrBHhje5cQ2QrmZ5mQqELEq4j7N1jXKX7lBC0hnXGIocFgG4QoYAH1YY6MQob2f7kDKe4JB2WwKV_LX.gLM.Yr4mF8GcrAcF.wPvV36HWNETcbMcEEyO3yzBXVBLYrA5i_Eo_as3g.si0m7AY7NvLSAvQY-&ap=53","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15glpevit(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31687619319,dmn$axa.fr,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$984869,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/w040UOMiXkyE7e08WhAkDQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452592709525-7501.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Choisissez la couverture santÃ© qui s'adapte Ã  vos besoins et Ã  votre budget. Obtenez votre devis sur mesure en 5 min seulement !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":9,"cposy":9,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31687619319","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":8},{"type":"unknown","id":"c-981b3249-9f9b-4b48-a37d-264f971c9f27","instrument":"4000002330S00001","is_eligible":"false"},{"type":"unknown","id":"c76df03e-f6f6-34b9-81e2-1ad30e8770c3","instrument":"4000012940S00008","is_eligible":"false"},{"type":"unknown","id":"c-fa1097fb-568a-4098-9d61-6dafeb236915","instrument":"4000000620S00001","is_eligible":"false"},{"type":"unknown","id":"9eafa6a1-7e4a-32b1-8514-4b6616f7d410","instrument":"4000007690S00001","is_eligible":"true"},{"type":"ad","id":"31666937663","title":"Rachat de CrÃ©dit JusquâÃ  -60% sur vos mensualitÃ©s","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=wQIGLR8GIS8rpD3h5WzzP1jzM2Qo5ligzPiCw_rEvEuIpLYpRoZHIa0r4QbaVJVFQ.gtUp8dlkm8qrYOzYci0Yjm30gcGjzPXioHo2ZkC502PEmsFVHnOtsN8f3hz4pCtzKN09WJ8wqUr7dS3itRiai2QGp7kFlc50uI8fKj_P62IiVMYJP.w.S_HkDNvWnJkliKzFP62oFWmJusnmPtaOH6Suwun0fdu1sCoSliWoqf3ZeWzxl9ANMis.IXaiKnxG8.cLrQeihkcx0_fF18A5HgwGhnWMb8S2yLSczcSmw4nJrIV.SsTkdtq07jF1NifT_GQkMS_o9secB1hasgdQ3bzCsXFMWoa1NBSW6kPs5WIwTiT5nieCOB40nxD1xxnJcfcQ4bbuEGyYXdFeemYOqKHdcnvfNS8YXJhujxALf6gFO6.vZ96oYb.CAyb9WCMd1FteyaLKuvxBpWTC3iFwGQ.PimdYPLtaCFEjZtLrIBV8JGkoVSa5ab4Gf0CTJ2ybwL1k_E%26lp=http%3A%2F%2Fwww.partners-finances.com%2Fformulaires%2F%3Fprov%3Dlienpromo%3A%3Apartners-yahoo-ads-pf-oct15","publisher":"Partners Finances","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=wS8oBqcGIS_j2ntj23BdBhVawwlvwS00e0M_clMi.GIy9a0.pGN0v6QUNSX3Pjn7sJJ.PutwZmF2aCHUgL0KsPszw0ZUIDfY8tXB3ctv4Up4PM9Rrc9_U8T4GaVEdeG1BN0s1NXeTlH2GNag87N3_RaahkhTaPn8Q.O8cAiCwNGWwPbSTS65KCaUgKu3pch54pVOklyn6mHCyamtFoZcixKSH8k8xoPYVtrVCj6g7ubSa7uiB6gVEUp6YBDJwS7KtHXR0irowot0cmx21u1m1GQktT4fLJLL98oK2nrqXN4exb5mtcaL6GWOF_90vdBnRoS9pwGW.bkwbxBqRO13HYC0O3Urwe6p5XkH.SZ8xt.kBCOF3UXNowqzgGs9Yyd.2JrHia3NOUHBtAcS7PxCLkLPQZw6a91z0NA-&ap=58","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15muq0cqt(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31666937663,dmn$c31666937663,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$933642,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/S83lTd6lYbm_.mwthCe88Q--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452851469101-9594.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Simulation Gratuite en 3 mn ! Tous vos crÃ©dits en 1 seul sans changer de banque !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":10,"cposy":10,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31666937663","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":9},{"type":"unknown","id":"a3c9cfff-4bd7-3ae1-98ff-8a036bccc0cc","instrument":"4000004520S00002","is_eligible":"true"},{"type":"unknown","id":"3eeba24f-f04c-3620-a149-edb89ab8b23a","instrument":"4000001870S00004","is_eligible":"true"},{"type":"unknown","id":"77838b44-5627-3806-b63a-a5a8c9770d0e","instrument":"4000012440S00008","is_eligible":"false"},{"type":"unknown","id":"c-30c0eb9e-f406-439e-86d2-5bb46a79e01b","instrument":"4000001110S00001","is_eligible":"false"},{"type":"ad","id":"31352200043","title":"Mutuelle MMA : Ã  partir de 17â¬/mois !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=PfDcLksGIS8EUbIj4rr9xw2_e9anQMtaQ0huUX5_EM7v8nHAdQJxnjyTvSLfgfvlfzY7IEgy6SD_eXCK5mncKkXRNiZeEwQpOcUQBO6TFhUG0t0hhehlEF5UVqU4eearBOXXtZ2jzlai7YfxgZm5.gMcoIGe3NxkIJsHoUuSPGeit0tvTOHZAD2Yj6yEqKGQLA3ZCSh0zYAeAgMoUnaDurcWRTmWED4z.GMDjswciwtvng.URSXreUvs5KUvSkGe47PDby7Uekv4xc0AUzlgo5hXGtsLQj6.j2PO1drLyhBoRz3inMBgj3a11ZQ9jk7GuCuqKq3Z54fxu4za1z5zR2.JXMx3DMiFlcFbgyPUqbx9BZDdKZr3nzjwpcoo.8J8KsqxkTcnSi0TofdNUugvidiDxaK3cOaC6SvI6wxliwP_9Jvqp5Te_BIhWGoVdkq7iQ4Z1Ez3v96mKqbUXjxXIwboXA38mK6QqX6pmcdBO0GJW_712UmSR_tQfEwDd8dHkS9VNtAz6a71P_wvq3sDG9r8ZcTG_weV1CWIRJTCfKKSg7hp59msMdiAkSlNF30AiNcJA.Woa5DfcIGN3vjObn1vcW0knEwZyu7SBm7LOcV4TUnZOVu.heJU57IhfxB34VP.Sj0PR7xgDmk9LBctCJJMeWz8KhjSSyWHsz8Z3yUJFwTc4_lNDhAGqJ32ecZIwKvNx_XcfX3GtzG3A5AXy1hWtf0-%26lp=https%3A%2F%2Fwww.mma.fr%2Fmutuelle-complementaire-sante%2Ftarif-devis%2Fsante.html%3Futm_source%3Dyahoo%26utm_medium%3Dstreamads%26utm_campaign%3Dcampagne_sante%26utm_term%3Dfrvitale1%26xts%3D154037%26xtor%3DCS6-32-%5Bstreamads%5D-%5Byahoo%5D-%5Bauto%5D-%5Bfrvitale1%5D","publisher":"MMA","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=lbJ2rSQGIS_KQPDyaSf_Ccrn8dkl7WZP8AGbo.5DD0iBDwEUgh_HW3uIPdhOpU4Dsdf.Jwj9wIOKWy9hKEVC_2ZyAEKT6NRJMNRIEih.zC0DZFfeq_t8zCiY0WNETflyYxFyj6bWMitZGbng_VADZdEZfsUSXgnw5QsPQV6g8y.V9DUSpEo7G2YzBPnHVwtAvzsa0EYLwElJ2RPHhthj96w9uxqUa9QuDHIu21DlG8TI7KeAZMXZPnQsW.INaOJwlDCaT4dBSrWeeUWfOru6FI1JkoTHopQRtQGqzsdrGRvvgz0qmMTvvVw8c8HZqfUYNSdC3UUkOr8ZWSIkAfIJ8kK8zbyRn3.fY8EKgY9dryTInWXBoyT9Tcw1ee52atk9pxpjv7_7TSE-&ap=63","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15hheodql(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31352200043,dmn$mma.fr,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$1013623,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/KGUzQSsomVNLZyZeFKng.g--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1442592402533-1393.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ©couvrez la nouvelle offre SantÃ© Vitale MMA. C'est Ã  partir de 17â¬/mois !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":11,"cposy":11,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31352200043","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":10},{"type":"unknown","id":"2944f95f-83d0-34d7-84c8-6b4886586a9b","instrument":"4000010350S00001","is_eligible":"true"},{"type":"unknown","id":"75af774b-c2e3-391f-87a6-026f637bc522","instrument":"4000010980S00001","is_eligible":"true"},{"type":"unknown","id":"c-2f8760ce-5410-4c2e-869d-fd51476d772e","instrument":"4000006050S00001","is_eligible":"false"},{"type":"unknown","id":"b52f8016-c308-35af-9656-d09b8b4eef30","instrument":"4000006030S00001","is_eligible":"true"},{"type":"ad","id":"31648266188","title":"Comment retrouver sa silhouette en 2016 ?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=tFJAQRkGIS_D0GmoiqqImYZHtVGZtrgmppFVfrOWrtH2I26ODqmeDSwJlYhponeJd0gyqDbrspzvNnFOHOrx2SG65kxjMX_YEOYFqk85CIhYr.Dtg.VDlCgJ6BXKxMPWkl1c3SDT3Firk2WGKuny9DYrR0z2tyEg8wW1K17ouMawzVw.tSWdNnJ2mklQXlH4dBNEW.htwHJ_17C6_inU_4FrQoyxFWIo5yAFwfCCUVPIO3WuAiRZp2hAQqet63PfmakTZIT2UCflD3C8WvQn4rZxGLBUfrq.2LBSL7QX6K4PYN_gYkiXPb.LVhalHfhmoWNjF9NtEiOrG0Rl0QuLUBeQqs4facjAreH5dBZjCsF5KpI3yA.bz4YCxxXAuDQXq_JtDYIsQJ8gBuO37_va3Dvy6L.fRPv3SMEXW2G97NCVYN3J3O0XuQtM1EihTfPbNFD7W18abCnqxkJd..STJwAJypj9dDPG2IUrJihC8GgN2HDVXxsmM56_q74YML0AnnP91BBQ48.dXGAsXpX.cu4stZA9fg--%26lp=http%3A%2F%2Fww299.smartadserver.com%2Fcall%2Fcliccommand%2F15076436%2F82257%2F%5Btimestamp%5D%2F%5Breference%5D%3F","publisher":"Savoir Maigrir","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=o4M1100GIS.VAFYGK8LuJL3NAvqOOCZEgSHw6xwDyjPMBF9z9DBkH6X033NQUE92O0xz0PAx1EpmdyZeatyrn.Y2ArkErKsKL6FGDiNlxGBQzdeTyP2Dw39zx1k2__On80uHBAV1DWmISEo9rPvfHB.tpR7_nr8Yh0YDi9Z_Wex7tHSd4vAhGT7E4tjyxFzuXctAEJHnYYaPsU6ZjMJZExC4_LEqJ.GMn4bM0JvgA6yAjgA0xmnOWGXMaOV4qS1cNvPM.DA_qezksHkyg1WxYOIHJbjPvn6kmZGprKEuZqm8Widcgj4rEO6fHJVp0ajp1wy37Ilq9Ag6VMOtbUL78GrlCYD8kNj9r4g916OA5_hMYt9W91g767OAJ.55fq6NT8U9VIR0nvg-&ap=68","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(167khtnrs(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31648266188,dmn$savoir-maigrir.aujourdhui.com,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$973526,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/TI7JGeWaG7MpXLfFIVFggQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452165407048-292.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Prenez soin de votre santÃ© et perdez vos kilos en trop sans vous priver ! Passez votre bilan minceur gratuit et obtenez rapidement vos rÃ©sultats !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":12,"cposy":12,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31648266188","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":11},{"type":"unknown","id":"b9f20c84-884b-322b-8b8e-d8e55232b0c5","instrument":"4000013570S00001","is_eligible":"true"},{"type":"unknown","id":"c55da377-b76a-3cbb-8aeb-431820338dc5","instrument":"4000007250S00001","is_eligible":"true"},{"type":"unknown","id":"9f70f6b0-620d-38be-bfb9-4420338e0540","instrument":"4000001520S00004","is_eligible":"true"},{"type":"unknown","id":"58bc19f8-8587-3429-a45f-992f6fd6000b","instrument":"4000021690S00001","is_eligible":"true"},{"type":"ad","id":"31637562972","title":"Regarder en direct: 120â¬ en 60 secondes!","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=XcCrFuIGIS976vpuy_P19P8Coo43VimxJvuPGP1_.BJaIfosQhlS8ruoMJ4mfW_lZLfI6wY84nz7OKK1OWsnS3M_uEIBbtxZ7mi9VTxc46SASAsfBdxvYnlDc6NybbmdZ3aXVENKo2W9JMkxvitPAMFvkGXLbvfqf_woz6O2IEWtJA2FBXS.8c5n2sp3cFq8O5VMqHEnhUY1fhtuWDAi2Shhzt2QTwm9Mtu2u0aJ.5Xlq6KAcynMXHgecjwpnwR6qlFpeI45H2R_l2yF.XdcNeI9o1eG5_MwwNgtH4YNF0B8E7f0Rk5AWbRN2RZfQE.aTQ0xI5KgWHLdd5jhl9ZnSEbByrSJ6ns3UW9wAbUZTAmQYp6LzP_CW2gP8Mc4VxKS1ZaKxSdPZggFsOxtjsfJCsY.CWcwOQ.W4xWWEMOvjOVgFoqZ5GLydArbXS5ejSTztjiN0P_7jnjihZ5rgxr0g4jIjYg88xyIBbN1T8THVcdwCAPVM5ow_.gNL_DJIIYDDsU3.g7d4FKoKydz5Z4ymXrVKTfLF488xNXq%26lp=http%3A%2F%2Fwww.startupf5.com%2FPages%2FTop10%2FPreditrend%2F2.php%3FAffiliateID%3D7679%26SubAffiliateID%3Dspf5%26zone%3DFR","publisher":"Preditrend","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=KZICdWcGIS9vKiCBb6XPLFqd6Oeicq_PDzz7dYHvLjrnwvfbvlkrboagi7GzmpGo_kolKZkeFXFgYLJ5l8eb63.ousu32gvFgEVyRtOpBv17Kdzk0Jizwxg3ByJI.Q8ABOOU3lqucILZhG9mSYaOqUAzNIddaJFDgfj_glcgm2C7xSh1nj4iLgQOPLTJ4vgIPYwXbXxFKszs7psVuHSyBPdBS21ktWfaDInb.tPuq0JbfITjxnhQrIaQYJlfYDq6CSlQ4SNGhSlR1T8Qc..94oCH3OgPKWt5JQe2k1aJRFv6h7aQ4wmSfmBs9EvVuH9uDh1EAK8ftft5KY4mwDMLlAt8Ry36k7hqdlpGkhxA3_lVf_FnPlQAwp_5GXU_&ap=73","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15oalk3p2(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31637562972,dmn$startupf5.com,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$1008395,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/rwpN0bpCacuCbSU8.Ue5VA--/YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/av/curveball/ads/pr/RESIZE/82x82/b43f9ce73ad27b6e6bc7850e3cffdf09.jpeg","width":"82","height":"82"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"L'algorithme de trading de Startup est prouvÃ© et testÃ© dans l'Ã©mission en direct!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":13,"cposy":13,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31637562972","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":12},{"type":"unknown","id":"c80e1937-5690-3694-acc5-72a8e4b91715","instrument":"4000008230S00002","is_eligible":"true"},{"type":"unknown","id":"094844d0-2ce9-31fe-8236-6c5e5b933c22","instrument":"4000013370S00008","is_eligible":"false"},{"type":"unknown","id":"de648220-2924-356e-8acb-4e126a7367a9","instrument":"4000011960S00008","is_eligible":"false"},{"type":"unknown","id":"6b53d5b2-11bc-34b1-bbcc-eb16e37daf76","instrument":"4000008230S00001","is_eligible":"true"},{"type":"ad","id":"31599412193","title":"Lits adultes dÃ¨s 159.99â¬ - Vente-unique.com","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=QZekB8YGIS9gvdxsOJwLwt9.AivbuJSnvjWbcvwy0wfbzFxNdhkKcdrIGwMWl2VUkbDjLwxoW8w2Vmpf0SUbLXsGE5YLpHKRjcWiXX56IetowGRGGWb31Y.hgL1rEmO8.hya_G08Ra.ABVf00mE_Xy9wNqCuqeNPSxcKnCPMF4fIoqQFx0wiOGD05IgB4GGpW.M8ToZwWiVAQWL8m14nR3sbcBV6_bPHBE34xzKWXH8KHWepufugpw9uj0S61IXIP0rsr3HrLZSrUzeS3ZZskhPc5huSExha8udiOtxFhUzjSgsugw2LqvRo48x35hb2pgAvIa7FL41L9KktGKkdj21uodQ.pxKdpBz6PUQZTIRSoxhpqcpRgIanlq1545KRBgJ34OASNo60q9AGDhdEo2mMEM26brA9XwDH105dVLgXWbZvQyVHzQBQ4I78v9FaNpJKhZ0T6ayEDTz888eWECpMgmxKx.6EM9._XHvwFzDgozObo0uCzl.cwQRBx4JH4R2yZDjFxoeojxFDssWot5PeQtODfSCRITzVBIvMidEZaI7WwMUoobpHuVS1xuCmszNof5iIYp6YM6Y1gRQgHEm7wZz0FV3aZ0xiL7woEYsNshy25Yh0TQnUuic1NXSxNPsml0kfZPNpQl0bmA7Dkg2cqTMmXp74F3mgAPBAESipCMEXHx50q4UWrrknh907eqDR%26lp=http%3A%2F%2Fea.vente-unique.com%2Fdynclick%2Fvente-unique-fr%2F%3Fead-publisher%3DYahoo_Stream_Ads%26ead-name%3DLit_adulte_maison_CPM%26ead-location%3DMaison%26ead-creative%3DZALARIS-PA-159%26ead-creativetype%3Dtexte%26eurl%3Dwww.vente-unique.com%2Fc%2Flit%2Fadulte","publisher":"Vente-unique.com","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=vVAuQ6MGIS_hBRj8jTlwqTarJCr5LlRWhfUCt0j9sZJiV5ThAsOYmqbQ9j8vwyS3pV6nd_qdigFHs2yLzDU1HIAnhi9p3qjrV.TgXmplzF9TCgtW54h4iK.EWDi6_jW9nLtVJ_E05foqVCKTUIP7Rsq42yFP2tpiaFh0jJ54WB.CEerL9HqkZU_5XH0tBQQoMBWdisxyyPnjfGkMHEhg1p9DIR05cym06fy58oYoggAqcb5TMD6EmSz2KMwMpPTNemmjFhnVhYQWYbMgHB8IOm0.Mk.eoxDW4ExJO1k9aJezXF_ROTK5Mo_K__7agvaRDzHxp.vY9Wq01cEq6.uJwUK2Gn.8NsEkw3wMv2M6irOfaF.23TOnq.VvmlWMVdNmt31FzFNIZFe2tjQP8YpZn5Hy9ZwqiI8jW0Cb&ap=78","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15rmqaif2(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31599412193,dmn$vente-unique.com,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$1060113,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/meBincGw1G5A7h2Kc9dk0g--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1450712698447-1207.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ©couvrez plus de 80 lits adultes aussi sublimes que pratiques ! En cuir, en bois ou en tissu, trouvez vite le style qui vous conviendra le mieux !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":14,"cposy":14,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31599412193","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":13},{"type":"unknown","id":"4b1eca5a-2d85-363e-b3d9-54ff7f9458b0","instrument":"4000010390S00001","is_eligible":"true"},{"type":"unknown","id":"ca6534eb-0428-3298-9f95-481c937d2957","instrument":"4000015879S00008","is_eligible":"false"},{"type":"unknown","id":"aa55ca98-fe1e-3298-b333-e243e4865c47","instrument":"4000006830S00001","is_eligible":"true"},{"type":"unknown","id":"57266dd0-608b-3ac2-a64c-093961ffa775","instrument":"4000009930S00001","is_eligible":"true"},{"type":"ad","id":"31561105965","title":"Assurez rapidement votre voiture sans vous ruiner","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=KuJMxBcGIS9wvL9KVpc_zslmLXRmlQ8jjRcJBokCsCfScvIhamyI3J37YIgyRUNI.uFPAIbs6mU7lQWLlfyYoT1Xg1aAtt0KPK_3cMZ0PkfLlOD2zfiV6I39QW7.BhbmQZ.5Oh6aC_CqScGDswo81d5974xDiNNSPUVbFNipVgdS.VoOByIWuqv3roRD3fZkR9vxK.AgnkqdqkwYTz9O8O5ceYMR9bI.TPD5MiAXBD8LvT9ZIrrOvGDv.a60SHtHqMx3eH9iUqLEuaXRmuhRrrfGGmRlocdEDd7wJar9Ry.GkpMAbKW9ZM5MW1df01r0R8ezPSod1SO_Ze3sLGxh8yWEdvlukFVKburjALNlwqjFwZMkigF8Ao8.KF._.qxc1Fh2JrUZDIFQZjubZ2yG6rWvqFHwX4gL9dy65q4Y1LHlPvNYzFRC6D5YW0ey4Bkou61azDyPFUvRdIexu02HXP1C%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F299501707%3B126380668%3Bp","publisher":"Axa","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=1h2Lxe4GIS9gKZgf.zwUH69_CnYwXiYg1jBf_xoRc8ameBGBqBAkLBrYJmxhJ_rLGl8tZkdF47lh12Ewdhx_.01Mly5UdXk2BTnC6InLWR_YG.KQ7JDpVKeURlN7pnJ.UIS2fDLY4jRY.VDCc7yeBrxGSghzpAv8Sx51XWXEKTfLjLjGBZFmTT77TuIdzBbUpX5Bt3uF.R9PTCfEenWKQcs31_GA00oX2hgiHk19JBZJHqukjUJjwsIFj9HWElP_eVgSxybTMgvwIG7Mcud9L5upvXSvRthaBB2ZFnyZWE1qtdkKFgDCbvM7322n9PuDPOnjGD9GD1ewpW2MJRW_lqNE3dT8LdUDAOzw3X587_8oA.r3IEX0I2Uv1zpEDdfp7PsWxE9r4v_ASlI9f0BeVcO_8aFqc0.pzfM-&ap=83","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15genhlej(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31561105965,dmn$axa.fr,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$984886,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/_iShrGNYniqin..KN2Ff.A--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1453192794672-7182.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Une Ã©conomie jusqu'Ã  -40%  sur votre assurance auto ? DÃ¨s maintenant avec AXA, trouvez la formule adaptÃ©e Ã  vos besoins !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":15,"cposy":15,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31561105965","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":14},{"type":"unknown","id":"bc14d5ce-531f-3b86-8109-1a4f210bc16d","instrument":"4000014760S00001","is_eligible":"true"},{"type":"unknown","id":"86808a6a-bd60-3172-9c19-7c05d468a59c","instrument":"4000012560S00001","is_eligible":"true"},{"type":"unknown","id":"5870ebce-cbb5-3ccc-806d-2cd822e66b78","instrument":"4000014070S00008","is_eligible":"false"},{"type":"unknown","id":"113f3061-1474-3f57-95d7-9650d61bfcac","instrument":"4000007300S00001","is_eligible":"true"},{"type":"ad","id":"31678710308","title":"Savoir Maigrir, la mÃ©thode de Jean Michel Cohen","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=Mb0q7NYGIS_rZbLg_MNu8jZBQ3RBxJgotsgYFFkZEEz8kSZhQxmMxhVbOJwcKA5HAxx1iEA4MnfdyznbN.6jSILn4ou8OLiQwgaZxjfk1paEE5qH51J.0hm2biWyMGNwIPic8Q2KtG0KJHI3fMQbL.WJ_BsO1XD3t5uvobQ_XZii.VMUbxREL_l76SFz_HJG6WGmGLiS5DUS9Lo8c_hyLLpxatvWlCMa6klshAIQuHRechEyo4JqjU9EqP_lRiQe_4p2NF30hjyOOHhtvYoC2e03kdRRyP8XjvLl.EwUcrU04hT.YYZYVVtpn2Tw7nLp3RybGzw.nBrrmg6Okcabz4PUHTXck4ysqJukDDn6U8UJ0YzKHbNgrqKnqm2RxEh9Wev1NcbywOONbMtmrU6gzQPrlwnRO_VN8ZvPmqBChmyWL8oc.TRcFjBQQWY46DgcT_yZZvlCQP3z.hXzbsDxXX5MNo2PtqgNftkLR527w0e120yoOz5ZO342Qcr0gxs-%26lp=http%3A%2F%2Fww299.smartadserver.com%2Fcall%2Fcliccommand%2F15195832%2F82257%2F%5Btimestamp%5D%2F%5Breference%5D%3F","publisher":"Savoir Maigrir","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=GYhyDDQGIS8ve6AcvHxDfGcJsiMXoEQD6a1oT7BUeQnKfPjTGbGC9Hqry.4eHYivFQ0egEs0i5_iJbhjdwbJ6QihJECafdb8a6pwuXEdETbtQ2DoCqhPUU7ZegEg5i.XHgjMBJWZLaFSfDtNo.r85INyybqTomnG3m.LGf49VKrhIIf3XERmCVZFXH2PJk5ekyZ4FG69TwbmD6n2tfNxFikyfsRbEJpdMPsv9FBlSeSic0UAFwgbDv1MWGaioh1NmFrsiQH.ZuJSlUAdws5yUucydN01hYZgRsdBsDRoHfB5EYhg.eQgasnSjDPrr2Pr99_RP30AY0NSH000mrjm3y9fRpfIVbXAVN1UXrAQPUN7IiiwxiF82Kad.g_ILDO5ZOlFYOlLmJMIfrozqPkK2IRkcZvkBF8c3_k-&ap=88","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(167sgmbuj(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31678710308,dmn$savoir-maigrir.aujourdhui.com,srv$3,exp$1454787499692000,ct$27,v$1.0,adv$973526,pbid$1,seid$4250754))&r=1454780299692&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/NedOfpO.oF4OQHWBCOi8_g--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1453459599219-2804.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"La mÃ©thode Savoir Maigrir vous apprend Ã  manger et Ã  maintenir votre poids idÃ©al. Sans interdictions, une diÃ¨te gourmande qui respecte votre santÃ© !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":16,"cposy":16,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31678710308","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":15},{"type":"unknown","id":"bb8b3a77-80ab-35ed-8ddb-19c198496c86","instrument":"4000007380S00002","is_eligible":"true"},{"type":"unknown","id":"26ebd888-4322-3846-aa65-2d1c45a99010","instrument":"4000026540S00008","is_eligible":"false"},{"type":"unknown","id":"9a8c6942-e1fd-348a-8314-93067fbd442c","instrument":"4000004600S00008","is_eligible":"false"},{"type":"unknown","id":"68cd4991-570b-37d4-a26a-2f42b3a69d3b","instrument":"4000012770S00008","is_eligible":"false"},{"type":"ad","id":"31618806524","title":"Assurance de prÃªt : Ãconomisez jusquâÃ  15 000â¬","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=ngRwXW0GIS.coXFQhgPoO9.qvnD.F8OpHJRgrzFnGDnqzDz2gDYlk0dw.iMzkQ65pScYI8vx1yd4qqDV0Zsu8JN.HHJPb2Iu41f1vCp1pHtP3LCYyZJB8Lj.DsP0OlQwrN78PvyU1_hsoPzQiGBP1B.IKGbUkBFbinUSsPKWgKCbJP00gyJC3EX1U8SerLvdbADo5EahrnE9KXXE71fDyDAFOCRC0HcdXJgSXIHey_02q1gOyjEKpV9ct_CJj6hOvfp.wroYjTS4hgbBGF5OIwdwdEP.hzWFk.Tm_k03rY8g7QBEto54yoGAVHPoD.BmqHngW3kZCawrUnJ5V9dt7mgimtzsTijyUKoNXQuj_gMtDVFVnYDOzIVucoOUsLVM4t6ApjD4jg_pA24w4CgKhMTX8ew_TUwIys4YRlacUELcF1O_0FX1QpRjlAODKhAIA98uOP8B3CpzUg--%26lp=http%3A%2F%2Fwww.magnolia.fr%2Fassurance-de-pret%2F%3Fj%3Dyahoo","publisher":"Magnolia","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=3bKqMB0GIS_OjDmgM11jBJOjZLIJIjNliH3vb5vK9H2A1EXVyZfntahjNFTL5YhI1nGtBby4vfY8XjYiepD1O8oggCzVNwZXXdbonozsNlflyM9PGX6F7RES0.S3dusrJqAYzpZcanod.3GvSVUqqb28kXgXctY1t_OPgqgYG4N9fvhmxDb3rh18oP4BjaSeXJHOIv3AjJCFjYfJgQl8RvWilEbGMJzYn4C_rWYiYa1cmS6a_PbK5a78MXM3yqwHcXl51eQqvCqrtSnR4wELnSy0iNjaqERmJ3C5pDJDmjaTpT4Z99cDDCKe0PErhtM.ERPblX_0OAj2piOBG5NLXxlpGwM0e7DjIhwalwT285TCrFATdZBShkKM1jUkpl8dEZ_eH_Z9XeOqTYddV2pgNDDnttq4IDni43Bp&ap=93","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15mtj6kqd(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299692000,li$0,cr$31618806524,dmn$magnolia.fr,srv$3,exp$1454787499693000,ct$27,v$1.0,adv$1320857,pbid$1,seid$4250754))&r=1454780299693&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/EhR56CZLbxpOpx3tuPpg5Q--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1451489443436-1467.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Comparez gratuitement plus de 15 compagnies et  Ã©conomisez sur votre assurance crÃ©dit.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":17,"cposy":17,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31618806524","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":16},{"type":"unknown","id":"6c1fc150-b6f0-31db-aa2d-b20d6ef9c821","instrument":"4000007180S00001","is_eligible":"true"},{"type":"unknown","id":"3e6aa208-fdae-3366-8fbc-fea0cf896c4d","instrument":"4000006970S00001","is_eligible":"true"},{"type":"unknown","id":"ad149ec3-bfda-3f77-9d11-a0e5b66e92e9","instrument":"4000007320S00001","is_eligible":"true"},{"type":"unknown","id":"5c1d92f5-2ea4-3fc8-a384-a3ae75f391d4","instrument":"4000005960S00001","is_eligible":"true"},{"type":"ad","id":"29546594745","title":"CroisiÃ¨re aux Seychelles jusqu'Ã  -70%","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=eg.mYdAGIS_3G2Gj0JfkBMBe85fq9qRUwu6Ek8xwEM.dMmh.MNu0NQXWBpGRzCZ1S_cd4zGe.BJqFWsAPQhP0WKnCA3Pli_jDb.PaASHVbGgsH3nKVV9aZ6DjdHyeTIM3dIH_0H4KMDbV7xtXK7.dyHcHXsDN2e2T5.tfSs57pO8SHdeJ5mFKpyeDWzEyhMKdRJcRjiqf1IyufUWCmZBiKhUhoIk.xYsI90ggL8fzIXs_wTTRQXMW415INpRBo5OW_utbH.hfSDUB1vaXn.LXEpvd.6UhpA_BeSkTahxKpVy83e3GWU08bNUxpvzyKp5yo4M0hhP3lWmx5YGz3Psv0Ylo_NXW3MZcy0jwwJoHFLVYgQeOesX25Ej_ojF.dCv1ARySe5WQNM0Kk4DzUCfNws4E9XO3F8nDZhpnyDUHcvZZtAlEWsL.AchBh1q3gVFBpmQE2t3.wsMdmOFfMGQO__dB3ScIC7hnIKeBExc94HJNWXUAT3G1wfx5AmoZm3W4j7tTWmclqvSz.mU1.5fEgED0Mhdqm26Ag8_vtCqoC6Sj_9nyv87OJ1GyX9gHV9KlBTtjr_iihdVwF.Ald9w%26lp=http%3A%2F%2Fvoyageprive.solution.weborama.fr%2Ffcgi-bin%2Fdispatch.fcgi%3Fa.A%3Dcl%26a.si%3D2035%26a.te%3D76%26a.ra%3D%5BRANDOM%5D%26g.lu%3D%22+target%3D%22_blank","publisher":"Voyage PrivÃ©","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=86alm8YGIS8HMsj1nD0PfWwfwsy9TTWKzRbzs__ivYGsh._FjhSdA_X_cH_eH0iA3xMf5zL63Xygx_VefIRstDPDsnjG9FVz.6PMQr8g.ZaoLlt.C7V93WCMo041cPwLoRqCeJa236xViQtKbYluBYKJGMGxhN5qcWWcQ8goyJ2LuEjfdYWpL4JEN5LTfoXGa5satDvDoKf73r.BDzeRVxjFp_xByxfDZK0omOxSQ9o2VbK9wi.t1bUaE_HU._QmMW7QpQdmdljTd9Bf0.06NrafHby3N.enZFdLU0.7Remfx4_6QNB6vzWcaeu7kc7dJb.5ynkyYDCtEmmpXqVB4Idl_HiEEKPAYTLH84OiJGSr21SscJMq50eymzPgXXY0Xr_XcNE-&ap=98","imprTrackingUrl":"https://voyageprive.solution.weborama.fr/fcgi-bin/dispatch.fcgi?a.A=im&a.si=2035&a.te=76&a.he=1&a.wi=1&a.hr=p&a.ra=696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(1627t6m1s(gid$696209ce-ccf8-11e5-b67f-9bcd37a7676b-7f11f0f07700,st$1454780299693000,li$0,cr$29546594745,dmn$membres.voyage-prive.com,srv$3,exp$1454787499693000,ct$27,v$1.0,adv$947007,pbid$1,seid$4250754))&r=1454780299693&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/VHRfX2g_0FsEN1TrcxaphQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1420037895799-2000.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"N'hÃ©sitez plus, vivez une odyssÃ©e de rÃªve en catamaran aux Seychelles. Profitez-en vite !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":18,"cposy":18,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"29546594745","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":17},{"type":"unknown","id":"98630a41-4341-399f-a7ec-3178511614a9","instrument":"4000001880S00004","is_eligible":"true"},{"type":"unknown","id":"242a34bf-a39e-3ea1-8cd4-98518a54fff4","instrument":"4000017240S00008","is_eligible":"false"},{"type":"unknown","id":"068cfa53-7c72-3cd6-b6ae-16003e52857a","instrument":"4000018770S00008","is_eligible":"false"},{"type":"unknown","id":"a349c008-99c8-3b71-8f90-9d370d66a22c","instrument":"4000014290S00008","is_eligible":"false"},{"type":"unknown","id":"a2b1b057-7fad-3361-a517-adeec6b389b9","instrument":"4000012130S00001","is_eligible":"true"},{"type":"unknown","id":"bc352e57-abe0-38d3-a9c2-a1c04c1d4e91","instrument":"4000007180S00001","is_eligible":"true"},{"type":"unknown","id":"c-aad79cd0-9745-4209-94a5-3602f6081e22","instrument":"4000011000S00001","is_eligible":"false"},{"type":"unknown","id":"d89380de-d159-3c3e-a949-cc84123245d6","instrument":"4000012080S00008","is_eligible":"false"},{"type":"unknown","id":"17b0c99c-cad4-3a20-9746-bf53e704a678","instrument":"4000013620S00008","is_eligible":"false"},{"type":"unknown","id":"a875ced2-7b89-34f8-a1ff-4f0a320fca10","instrument":"4000008540S00002","is_eligible":"true"},{"type":"unknown","id":"a16b7b2b-dde5-3701-be6c-dc951e5ff7e3","instrument":"4000013520S00008","is_eligible":"false"},{"type":"unknown","id":"c-e9a67d1c-0563-4390-b29a-748d5a57679f","instrument":"4000005270S00001","is_eligible":"false"},{"type":"unknown","id":"46e8ba31-46da-359b-a01b-6dc03afe4deb","instrument":"4000012520S00008","is_eligible":"false"},{"type":"unknown","id":"07dc4a93-ba5d-35e7-8a90-3103da015ae2","instrument":"4000015500S00008","is_eligible":"false"},{"type":"unknown","id":"8719549f-9954-38dc-ab53-446616287270","instrument":"4000007570S00001","is_eligible":"true"},{"type":"unknown","id":"25f545e8-0436-3084-a417-07be4d0a7ae4","instrument":"4000007320S00001","is_eligible":"true"},{"type":"unknown","id":"b6cc55a0-cb8d-11e5-bdbf-3f53600e4f90","instrument":"4000001690S00004","is_eligible":"true"},{"type":"unknown","id":"ca2fd054-3523-3d0c-80c3-4c9c1913e96e","instrument":"4000009630S00001","is_eligible":"true"},{"type":"unknown","id":"d69fa9e3-dcad-3e6c-9bf8-926ac5a6ba73","instrument":"4000008780S00001","is_eligible":"true"},{"type":"unknown","id":"223c89de-c699-3d55-b1d6-d5ef42c8fb54","instrument":"4000009590S00001","is_eligible":"true"},{"type":"unknown","id":"e1c25d4d-3682-383b-a980-d0544681066a","instrument":"4000019530S00008","is_eligible":"false"},{"type":"unknown","id":"878f37f8-d1a7-34ad-bc02-c07ffa48ab6a","instrument":"4000012990S00008","is_eligible":"false"},{"type":"unknown","id":"556595ea-2745-3e4e-8fda-3de4f7658e73","instrument":"4000012130S00008","is_eligible":"false"},{"type":"unknown","id":"5c5a0672-b1d5-3a92-b88b-af28e8f8977f","instrument":"4000012360S00001","is_eligible":"true"},{"type":"unknown","id":"c-1dae8517-767b-4ea0-8883-37ea3fc448ec","instrument":"4000002440S00001","is_eligible":"false"},{"type":"unknown","id":"3cb4e76a-504f-348d-9805-7eb10b2b5967","instrument":"4000014810S00008","is_eligible":"false"},{"type":"unknown","id":"dcaece67-e45d-3a3f-94a1-97d05b051eaa","instrument":"4000016170S00008","is_eligible":"false"},{"type":"unknown","id":"d0d8b33e-f796-339d-b3a2-0e68b389a3bf","instrument":"4000014600S00008","is_eligible":"false"},{"type":"unknown","id":"1234f515-0717-3f9e-8763-9dd308be31a6","instrument":"4000013830S00008","is_eligible":"false"},{"type":"unknown","id":"36632598-abd2-3f71-b5c4-f3df2134dee3","instrument":"4000018050S00008","is_eligible":"false"},{"type":"unknown","id":"17906de6-219b-3e6b-b2ca-75962f0a8673","instrument":"4000012600S00008","is_eligible":"false"},{"type":"unknown","id":"c82cec6d-c9af-3a25-bad1-fd3157931d08","instrument":"4000008680S00001","is_eligible":"true"},{"type":"unknown","id":"c-9715a9be-1a32-49d5-aaf3-4ec0d64476a1","instrument":"4000000750S00001","is_eligible":"false"},{"type":"unknown","id":"a43c12d5-689c-3df4-8d5a-2e35e49b4ce5","instrument":"4000014620S00008","is_eligible":"false"},{"type":"unknown","id":"c-72eb83b2-7ca2-471c-aab8-034fde9f68dd","instrument":"4000005270S00001","is_eligible":"false"},{"type":"unknown","id":"c-cca43f4c-6911-4834-b3ea-ff4db23651ef","instrument":"4000007070S00001","is_eligible":"false"},{"type":"unknown","id":"91fbf37d-3bc0-3bf3-8d85-4387e9ed6c58","instrument":"4000005380S00002","is_eligible":"true"},{"type":"unknown","id":"4c9301c2-6f66-3a4f-a624-c9a8f6727c9e","instrument":"4000003460S00004","is_eligible":"true"},{"type":"unknown","id":"481d8740-f99b-3e48-96cf-06e691a014e0","instrument":"4000011110S00001","is_eligible":"true"},{"type":"unknown","id":"61b17502-c263-3452-9edf-9cab45584638","instrument":"4000006890S00001","is_eligible":"true"},{"type":"unknown","id":"8866670d-41b0-327b-8845-a438dce5824d","instrument":"4000009800S00001","is_eligible":"true"},{"type":"unknown","id":"c-a0e3a627-41b4-463a-a8fe-380f00ad84d9","instrument":"4000001440S00001","is_eligible":"false"},{"type":"unknown","id":"ce0dcb14-6b7b-308f-b3b1-4ac96e66b34d","instrument":"4000012700S00001","is_eligible":"true"},{"type":"unknown","id":"c-10e424b0-d527-460d-b75a-2d0bf641fe9a","instrument":"4000001840S00001","is_eligible":"false"},{"type":"unknown","id":"c4d3414d-c001-3627-b619-7f77a48e49e8","instrument":"4000016039S00008","is_eligible":"false"},{"type":"unknown","id":"c7e528f7-6c30-3087-b969-d4c73d59edf7","instrument":"4000014220S00008","is_eligible":"false"},{"type":"unknown","id":"1742b6f7-2676-30d1-962b-02595af1cd92","instrument":"4000008930S00001","is_eligible":"true"},{"type":"unknown","id":"a25ca539-e252-3a66-af38-f2734924e2d0","instrument":"4000011540S00001","is_eligible":"true"},{"type":"unknown","id":"81baa864-1e48-3724-bd64-fd2427cd674c","instrument":"4000012170S00001","is_eligible":"true"},{"type":"unknown","id":"f2fd9b11-ff66-32ef-a7a7-50ed932f1e88","instrument":"4000009160S00001","is_eligible":"true"},{"type":"unknown","id":"7d4876ab-91c1-3c99-ae93-1e468e6f2118","instrument":"4000001140S00004","is_eligible":"true"},{"type":"unknown","id":"3b885de1-1420-3158-9169-4691ef2909a2","instrument":"4000011160S00001","is_eligible":"true"},{"type":"unknown","id":"55ebc12e-0318-31bd-8a78-b45f7c380e90","instrument":"4000004510S00002","is_eligible":"true"},{"type":"unknown","id":"c-49953d0d-4d11-4195-918e-0facd10c3620","instrument":"4000003870S00001","is_eligible":"false"},{"type":"unknown","id":"3314ca88-4f02-35f4-a67a-6b06a45e8e12","instrument":"4000000500S00004","is_eligible":"true"},{"type":"unknown","id":"45656b97-59e5-340d-9a62-48b31c5200d9","instrument":"4000006420S00002","is_eligible":"true"},{"type":"unknown","id":"a0db3d4c-b3cb-3759-8e66-4d9550305ae6","instrument":"4000009650S00008","is_eligible":"false"},{"type":"unknown","id":"c9b37ad9-d317-3319-af59-7ea9169d17ce","instrument":"4000008250S00001","is_eligible":"true"},{"type":"unknown","id":"c-10849654-bd7e-41ea-9d30-d1528d631b0a","instrument":"4000002640S00001","is_eligible":"false"},{"type":"unknown","id":"6d8763b1-f660-3364-a923-016af8a5d479","instrument":"4000009920S00001","is_eligible":"true"},{"type":"unknown","id":"c-5f1a7c5c-5abd-4178-9215-57bd9c306238","instrument":"4000005290S00001","is_eligible":"false"},{"type":"unknown","id":"c1832306-89c9-3f6d-9885-990d1ddaf572","instrument":"4000013490S00008","is_eligible":"false"},{"type":"unknown","id":"c-ade25c3e-7008-47f5-b860-7b35889f321e","instrument":"4000012100S00001","is_eligible":"false"},{"type":"unknown","id":"c-a064dbfb-0895-4505-a4a6-d99d913ad587","instrument":"4000001470S00001","is_eligible":"false"},{"type":"unknown","id":"d6ed8787-8aeb-39bd-b61d-a6712939ad8c","instrument":"4000010980S00008","is_eligible":"false"},{"type":"unknown","id":"efa31fe9-1b42-3a13-9c92-5f205aac352d","instrument":"4000008120S00001","is_eligible":"true"},{"type":"unknown","id":"1d7be2fb-288e-363d-9ff2-d618ac725f24","instrument":"4000011630S00001","is_eligible":"true"},{"type":"unknown","id":"4ab8f425-b029-32e7-b762-f0da77803eef","instrument":"4000011560S00001","is_eligible":"true"},{"type":"unknown","id":"eca7b918-a32a-39cd-8cc4-fec313461c97","instrument":"4000009480S00001","is_eligible":"true"},{"type":"unknown","id":"c-afd7bef9-cc25-4ef8-9322-db788ade2fda","instrument":"4000002230S00001","is_eligible":"false"},{"type":"unknown","id":"9c869132-2d17-3b63-acf4-011f529f2e01","instrument":"4000006650S00002","is_eligible":"true"},{"type":"unknown","id":"c-6be2116a-eb38-44fe-b8d5-0a51f07fdcfb","instrument":"4000004220S00001","is_eligible":"false"},{"type":"unknown","id":"d8bc1637-001a-3b22-8f6f-2bc2c281a207","instrument":"4000007760S00001","is_eligible":"true"},{"type":"unknown","id":"44685a00-3eb3-3133-97e6-e624cf19f333","instrument":"4000012390S00008","is_eligible":"false"},{"type":"unknown","id":"5b2b05ca-e8b8-3a86-9782-d4386ce76eac","instrument":"4000014600S00008","is_eligible":"false"},{"type":"unknown","id":"c-fd1eb0ea-26d3-42cf-9c92-a41c58de5346","instrument":"4000002700S00001","is_eligible":"false"},{"type":"unknown","id":"0eb17c76-0eb1-3c4a-9aeb-a0ad7a8de914","instrument":"4000012950S00001","is_eligible":"true"},{"type":"unknown","id":"b7d22df5-56d7-35d2-9908-735e4bd35c1a","instrument":"4000007300S00001","is_eligible":"true"},{"type":"unknown","id":"c-451bda5d-99e5-41e5-8050-3d686d4d2a8a","instrument":"4000007960S00001","is_eligible":"false"},{"type":"unknown","id":"269547d7-fc69-388b-82e6-4c4d4385d30d","instrument":"4000010040S00001","is_eligible":"true"},{"type":"unknown","id":"c-8a1b5dbe-63c1-4f7a-a480-7dd5d9898c21","instrument":"4000002070S00001","is_eligible":"false"},{"type":"unknown","id":"b4a33e91-9ae7-3f26-8510-5f6ae235660f","instrument":"4000011620S00001","is_eligible":"true"},{"type":"unknown","id":"2a87e57b-a66f-36e6-9d1c-9f10090f7539","instrument":"4000006390S00001","is_eligible":"true"},{"type":"unknown","id":"f083eac4-b075-3445-b93d-af02e7610bfe","instrument":"4000000280S00004","is_eligible":"true"},{"type":"unknown","id":"c-2dfa968e-69e5-4bba-8719-d82aefd36393","instrument":"4000008870S00001","is_eligible":"false"},{"type":"unknown","id":"5670939e-71d5-3cc3-89f4-7fadb3d70377","instrument":"4000011930S00008","is_eligible":"false"},{"type":"unknown","id":"a5b94cc5-813e-389d-a809-1fdabd76f512","instrument":"4000008500S00001","is_eligible":"true"},{"type":"unknown","id":"e8234a18-9d98-3277-9516-d1edb1c08506","instrument":"4000010140S00002","is_eligible":"true"},{"type":"unknown","id":"36113039-9a81-331a-85d5-719664b2e73b","instrument":"4000008930S00001","is_eligible":"true"},{"type":"unknown","id":"c71a6d71-15a8-3133-a764-f997666987eb","instrument":"4000001560S00004","is_eligible":"true"},{"type":"unknown","id":"f13f8c41-5b5b-3093-9855-57f2f5adaf0b","instrument":"4000009860S00001","is_eligible":"true"},{"type":"unknown","id":"943275d0-ce08-3517-98e2-6401984db7ec","instrument":"4000010990S00008","is_eligible":"false"},{"type":"unknown","id":"a49422e7-158f-3582-bc45-3d7979d974d0","instrument":"4000013830S00008","is_eligible":"false"}],"continuation":"","more":179,"enrichment_ids":[],"cposy":24,"stream_items":[{"id":"id-3593644","cauuid":"2de31d18-3dfd-3673-88b9-80a1424abe40","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ss_cid":"2de31d18-3dfd-3673-88b9-80a1424abe40","refcnt":4,"subsec":306,"imgt":"ss","g":"2de31d18-3dfd-3673-88b9-80a1424abe40","aid":"id-3593644","ct":1,"pkgt":"need_to_know","grpt":"roundup","cnt_tpc":"Need To Know"},"link":"http://news.yahoo.com/5-dead-hundreds-rescued-injured-quake-rattles-taiwan-050238778.html","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"id-3593705","cauuid":"fd1977e5-4383-382d-b6b6-781fd0318c97","link":"http://news.yahoo.com/sex-abuse-survivor-takes-leave-absence-vatican-panel-123703164.html?nf=1","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3593707","cauuid":"ee12d0e4-70d9-3172-955f-58d8f583699e","link":"https://www.yahoo.com/food/super-bowl-food-how-to-enjoy-game-day-without-204035670.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3593699","cauuid":"f11847e5-2436-3ab4-b035-9cfe313d9a61","link":"https://gma.yahoo.com/veteran-wins-super-bowl-surprise-lifetime-150051195.html","type":"video","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3593701","cauuid":"e0b2349e-2c54-303b-8590-c78349b5c0ce","link":"https://finance.yahoo.com/news/drug-pricing-161315534.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"def0ed47-4101-3f88-a0c9-47836eb68b51","i13n":{"cpos":2,"cposy":6,"bpos":1,"pos":1,"subsec":398,"imgt":"ss","g":"def0ed47-4101-3f88-a0c9-47836eb68b51","ct":1,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"U.S."},"link":"http://finance.yahoo.com/news/stanford-professor-says-eliminating-2-153500252.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"31649542148","i13n":{"cpos":3,"cposy":7,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31649542148","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=jYb2XCwGIS.LfGdqWOVE5lKbOnJSfTXC4kECT.I599.SPVlzziGmx8KL9XnG4NpyXPoJxYu2mVDjABIvAUAn1LYFlntWqU_8ObdfJJKUVJnGI5PKCEJ1qMTg.4_kC8PRrgTn4pqAXBA1mD1aBxTSRPaDVK5lXRtwUJZXJxyzeJ1RKO15TC5nBmZNeTfZ.sksCpama_CDnaAK0OqOyMCBK1oefMhngXdCDZzhH8s59aF5vaLlbKLqili0Rqf.IZM7nT1eIghOFiLhEKna811ToxksTeyVKjKj03zXTdX6wseRgjCGGZKLDTi58ZGu2T1.WDe_kvh3mzhOHUo0lzdScq4LVtw7TkdIe7FJNf7AmH4K2ttWeFg8_.b7LRMFMJtqmqAP3bmEW2CmFUtmBgCM5ZBLwmnPFLbQg4etz4DC6Hd0l_1yV0RH3VT6HpW3Z3iryVH4P93uy3P3k2IUg94G8mYtzkkN8UbrajDgzA--%26lp=http%3A%2F%2Fwww.samsung.com%2Fus%2Fexplore%2Flatest-galaxy-smartphones%2F%3Fcid%3Dppc-","type":"ad","video_ad_beacons":{"VIDEO_30_SEC":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=47muh_wGIS9rxkCRWm4PwBYH.pShzwiayfbFEOaUSAhiAy27cRMJe9iBTBzjtJ246VF_vyUW1wVURr7ibtyRi9tF7vliUHTUW2oBqiuTnKSoLNvplY6EZmaqQg37zwlY36rl77crGKVJhjQh9CNhkPDqWQIkXk6BgZjwelwnnOBevYintFRlTDIyddChy4c3GTdlMqJNyTrrGiLreJdNQLuyHNUVwIvc2k2bWu1XW6NupgUxBp0CSlfDBA5j85hqWTDBUPPRBPs4H2WFEU54dD_BlE.pYnEASZlaydRD_FUimA--","VIDEO_CLOSE":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=CGKcZaIGIS.TNABXFIO0vsmbo1UbeiJYw7NmZUn1DD18M5OI.Enc64FX1TS00_V1Gig8pL7d0EYyL1p0JW7j371mFXWMAlulMT8xFUyYx6.QMuhEJNY6qWrYR56pPnFN2OzHdbYwgWuO6pDFqrbmR5shnNol5LoWZoLTthMBy_cPRLMYVmsm3xookYpawWUojjBS8DHaYN9aey8acz1GSs6dZydf9dUmDVxdAAyWxrK5vPJJiucQirGkUn6ditr9roGWN7WwsWcRXlZVur42pbDXT0jZQ_VYZcB6gouZq3pNZQ--","VIDEO_QUARTILE_100":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=DU2FLMsGIS9nEl9gjCe8aTDCCmqoUGTIoFufz1SQiGbPy0Tla_f3h7EDQ5ngqhBW3bboH_ZpdgEdcGOSDwhR1sidHOYZA366049IeOh.0Y2LtmiErevvYUBpUsht3P3Ar6VQoANzUheG01v86WPKxrv91gozlbVY_MGzuaDqWl_14pNGJRyMocU.VPswp_E.vZIdJ5VW0dLbVumGkxl11d_UWxUUY.XNcwAzs12xKhTyJcq_AU_OMcuJ.CbLr_JobIZxvnihg65pfgLhlB0OgWcKLALuczgCNzHnSbtJDQVPT5tH9TuxQxQoGdlmdGL4.g--","VIDEO_QUARTILE_25":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=xgbZpfYGIS.6K0_GT2drS0OXWSxjr34qDcwxQaw57TUm9ICInt9wlulv7kAq253vWUFYFPB0um6ZbOU_RrtX4z4vrskuNRI7NqombosHxcoG6tMMH3QvWCoTxfxqhXZHi7irx6vA5O0NUJLKX71KjPmG2mKtpMKxV5atO2KSRmVGDgf.BVQMHVKqEixikSCRi7a1UIkiOB2jIHEXZaRcj0XAkS2r9UuznbCYEsVRew14TuPo3St7vEzj2vwC6hdv5MznmEOqoVOjitpO9qfyV1gkPjX0EwAlaQ0DbiA64vHN","VIDEO_QUARTILE_50":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=P_YiR1YGIS.Eyl1SZ2pHlEf9zy0oZ8WFuLKm5Tej5roVWkaNorGHo1FFgLu9C7dRRafoR5Fo6ubkbvlzgDiZ2cGD8a1zVXc39uUZtKHpRZf88ud5rxod1sGi2Xflt6If5WNb8.pEk1KPJ88jUP1eLKMFeI3bCIKyc0.dY9qSg9mICd9JFhQO_VOs3pexkIGCqCZWthqbTdDdXCXpVoj8rIp0WU9YpLXduBgq9IFjjL6ZXNgHrLqQEPhztCnTVCk9xdKW86Vj__fnYKd0rcmfcWru5L8QHXaQX9VO4D6Q63ur","VIDEO_QUARTILE_75":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=dlAu4FsGIS_uBbAQ46fyONFqhiVH4OB01Al8OjZtHwXKYROwB38Pn2YJKtG9cKA..g687j0RVJce.YbaYgG78V4ziTwHBXzGjrnMqjNzIJbNq_ncYxJ1t.xCJrWdyw2xYjH07aT2yagliJwEXnfFgUuvoXx0rXhiC2FKnqm_9aeZ75e3ym4WthJDmJYSf75P27FKKpY45Kj0qnf1xEO9p6yT0Et591W0hCq6fFFN9M7p_MP5s8GY3mvlxvtwZL3GqD9xr80BVaQz6M_RdsWegz5ouckactelyStWtDQzcc2i","VIDEO_SKIP":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=E48e9d8GIS9SpzALD69OVqB6AlOfzElQXurQqobPAgD2LVrqPajXvij0adVfixA5QuhxkFsuE1Hl1twrZyj8icjH0Bxc3e6zwgXy_hPlXUS8K.mbsLlWeNqRMhx_ShpxLRE3ZXUHmtbCRTWGGSoXFkZeAGCTn8ob1KyyhgXjyDEMTJgA_7sz5LPKvUq6OoLa6obhU6yXrc7pxLzw4ikm5tQj1_ZOWi1Fb0cwqg7AJAUOl1VLI5LFiRSom1ZZXexn.9fSkt_GF_HuqMD.ar0rEZ1YkDc1gnI8R_uBNrRh2vRd7g--","VIDEO_START":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=GCGrbxMGIS_ICed7CTSPfqkduMzqlL0ZMb4kso2YBRZELW0G1xcRphmURSNrUpdd2HxFwunncMU_91hKth4wNoiYIFm2Vhlg01y09SrznLSMWjdiTjKYwnvk1Ut605Pu6e.asabUeW_KAChBU2gfqjpmMEq8_Vxt1xi074nkVYy267BOaPCiq622LmiDUP8i7zrGCiJOPPCC7ZATPIf2tArPtuEXN3a7HCfqApLCOdv6F1uqoI8263BfIToKe3mw9rLSCaZtOGGZMNjJlMYnmlsOgkRXIXXxLihXc8oH2tJX&vsa=$(V_SKIP_AVAIL)&va=$(V_AUTOPLAYED)&vph=$(V_PLAYER_HEIGHT)&vpw=$(V_PLAYER_WIDTH)&vm=$(V_MUTED)","VIDEO_VIEW":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=_Jm9vBoGIS.lHKzSDR4zXMUMOx1Pb8xkLHXpqWzKFWqu.IhHsW3bSn7sd4W2DFambwn8AtQ.S3NL98EY66gA29qz14PQXtIvLkRI1i0R50LnCUrdRZ0pm4rECX2.J0pOEn5jn3AfRMzCttz2rqMITHG8AxMQBKe8vh2R97VcdzF9ABfGgNIm56B2AqnKf0uPY7qDmXmafY7gTf5eFq4NmvrmYYqty1ZVP84oZdtoIYI1H.i0zNLKQdL3QiwPZ8HLI0gBzL2amEv.9jb7jP_CavtLxglz2XJqc6z9FyJHFW3x5Vh4C.uGlG70yEHLChWh0haFmxpC"},"video_ad_assets":{"usageType":"VIDEO_PRIMARY","mediaInfo":{"url":"https://c-03f97224c18e2d20996cd978acd07a16.http.atlas.cdn.yimg.com/gemini/pr/video_pXCr0glzWcdEwmEqFXBwR7YebAlKLxe6JK5bbQ3KEg60cEuKWScogoWD-stUFAi-DchFYOS-O1M-_5.mp4?a=gemini&mr=0&s=50f9fa49de00f69665bd1eaa1f00fd99","contentType":"video/mp4","height":"360","width":"640","bitrate":"437","length":"32"}}},{"id":"e29fa585-240d-3798-8041-e7af7a00e06e","i13n":{"cpos":4,"cposy":8,"bpos":1,"pos":1,"imgt":"ss","g":"e29fa585-240d-3798-8041-e7af7a00e06e","ct":1,"pkgt":"orphan_img","grpt":"topNewsStoryCluster","cnt_tpc":"U.S."},"link":"http://www.cbsnews.com/news/11-year-old-boy-convicted-of-killing-8-year-old-girl/","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"a319dcfd-3515-397c-b83e-5778688e9034","i13n":{"cpos":5,"cposy":9,"bpos":1,"pos":1,"subsec":213,"imgt":"ss","g":"a319dcfd-3515-397c-b83e-5778688e9034","ct":1,"pkgt":"orphan_img","grpt":"topNewsStoryCluster","cnt_tpc":"Celebrity"},"link":"https://www.yahoo.com/style/salma-hayek-didnt-plan-wearing-133023197.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"3b3c3cd4-7242-31de-98b9-024e4b0baf2b","i13n":{"cpos":6,"cposy":10,"bpos":1,"pos":1,"ss_cid":"3b255110-3506-4045-9002-dff258d44655","refcnt":2,"imgt":"ss","g":"3b3c3cd4-7242-31de-98b9-024e4b0baf2b","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"http://www.salon.com/2016/02/05/donald_trumps_iran_idiocy_the_interview_that_should_have_ended_his_candidacy_once_and_for_all/","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"da379cce-b44e-3565-8646-9f8ae4ab6994","link":"http://finance.yahoo.com/news/voter-confronts-donald-trump-could-020439538.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"fa48c4b9-7390-37ad-88f0-ccfd8f71411a","link":"http://news.yahoo.com/video/donald-trump-fights-backs-hampshire-062910598.html","type":"unknown","viewer_eligible":true,"viewer_fetch":true}]},{"id":"d85bc4f2-9fa5-3245-b47a-0624fa7ad902","i13n":{"cpos":7,"cposy":13,"bpos":1,"pos":1,"ss_cid":"34037eec-548a-41ac-8c14-efd403fd2a5d","refcnt":2,"imgt":"ss","g":"d85bc4f2-9fa5-3245-b47a-0624fa7ad902","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Sports"},"link":"http://www.yardbarker.com/nfl/articles/peyton_manning_wife_ashley_did_receive_drugs_from_guyer_institute/s1_127_20293228","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"11e30329-b126-3e0f-b20a-e4c8bb3b6be8","link":"http://www.cbssports.com/nfl/eye-on-football/25474417","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"be1c13eb-8517-3d65-aff0-fef84b98dea0","link":"http://sports.yahoo.com/news/mannings-legal-team-looked-documentary-021927763--nfl.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"31637560506","i13n":{"cpos":8,"cposy":16,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31637560506","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=Q4e6X1kGIS.F2bw5Fl5zONZK09xGvgrKN06hoMTx391qaxN3PkbNEFEe2ZHsFvhiYxGJ1LAoCzVZsiYzkujHMVTzN_S4pmG4YEnFE.Z_1mfM7p5Y1MGGdgrjw7QI_Ul078QCpc8jF6L8SHm4m5Gp8eMoC94Whz1QzM_Q4KhVSqzMPlpdfhDJnHe_K9Stjl4CUeyiTY8DDHtPeDNHyLtqc2n7Evh3j25G6Mye0Q0Py5MPPRToF2r9bbVSce91selFgn9u5OJ652tGV1gSy5Xs5QX4DE1xZc0i.SAODe3ak9fh60aoVJyqR3KokYJmVELXBboU20o0LXP4tBFRzZjavwm_W9wKu8m1yNrTN2ydB9ku4DQbNNI8j7w9rljCG7Mn1no.tueMz9z6i4nYQaV9QbP6JwW6p9r6Xq4sEtBkRMZCGFPCC_hRkP0taNNqUecfIeHCFUBo_yxvgCpgVKV7TOumkYeT7dmVKM4Fxs5kqT2RD238W_x76Lw8DTj.G.ze8lg-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ffb65f9dd-93eb-4fc8-bba1-45bfb168335e%3Fad%3D1","type":"ad"},{"id":"714384e3-16a3-3c2b-a9ab-ac4f43bf7d78","i13n":{"cpos":9,"cposy":17,"bpos":1,"pos":1,"ss_cid":"3cbe3e6a-89a7-473f-a4d4-a7e1f365b7aa","refcnt":2,"imgt":"ss","g":"714384e3-16a3-3c2b-a9ab-ac4f43bf7d78","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"http://www.usatoday.com/story/news/politics/onpolitics/2016/02/05/iowa-margin-between-clinton-sanders-shifts-errors-found/79889298/","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"e0c07d9a-1165-39d2-937b-ebf9ab0813c3","link":"http://www.cbsnews.com/news/hillary-clinton-calls-out-bernie-sanders-artful-smear-in-democratic-debate/?ftag=YHF4eb9d17","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"b869b586-7bab-3def-9cfa-49665bbb42c7","link":"https://www.yahoo.com/movies/live-blog-hillary-clinton-bernie-sanders-face-off-020429843.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"18656f10-4bca-3732-b7cf-8ccd10757c81","i13n":{"cpos":10,"cposy":20,"bpos":1,"pos":1,"imgt":"ss","g":"18656f10-4bca-3732-b7cf-8ccd10757c81","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Politics"},"link":"http://www.military.com/daily-news/2016/02/05/japanese-soldiers-break-contact-engage-us-marines.html","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"2a43b26a-8a85-357c-965b-ff1fe5921c4a","i13n":{"cpos":11,"cposy":21,"bpos":1,"pos":1,"imgt":"ss","g":"2a43b26a-8a85-357c-965b-ff1fe5921c4a","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Business"},"link":"http://www.housebeautiful.com/design-inspiration/celebrity-homes/a5190/oprah-colorado-house/","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"e7a1961c-dd8d-3484-908f-661154b7abd4","i13n":{"cpos":12,"cposy":22,"bpos":1,"pos":1,"ss_cid":"bb6d8561-a956-498d-b511-4c77c115e779","refcnt":2,"imgt":"ss","g":"e7a1961c-dd8d-3484-908f-661154b7abd4","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Sports"},"link":"http://ftw.usatoday.com/2016/02/deion-sanders-johnny-manziel-domestic-violence","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"b0d8c0cc-9956-3038-9505-0eded1e01a65","link":"http://finance.yahoo.com/news/johnny-manziel-destroyed-value-nfl-143337910.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"41319d63-cc41-3427-b645-ca0ea0667327","link":"http://finance.yahoo.com/news/agent-cuts-ties-johnny-manziel-153319339.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]}],"fetched":12,"interest_data":{"WIKIID:Tainan":{"name":"Taiwan"},"WIKIID:Residential_area":{"name":"residential building"},"YCT:001000780":{"name":"Society & Culture"},"YCT:001000798":{"name":"Disasters & Accidents"},"YCT:001000655":{"name":"Natural Phenomena"},"WIKIID:Angela_Stanford":{"name":"Angela Stanford"},"YCT:001000742":{"name":"Science, Social Science, & Humanities"},"YCT:001000667":{"name":"Crime & Justice"},"WIKIID:Salma_Hayek":{"name":"Salma Hayek"},"YCT:001000069":{"name":"Celebrities"},"YCT:001000031":{"name":"Arts & Entertainment"},"YCT:001000395":{"name":"Health"},"YCT:001000076":{"name":"Movies"},"YCT:001000427":{"name":"Disease & Medical Conditions"},"WIKIID:Donald_Trump":{"name":"Donald Trump"},"WIKIID:Sanctions_against_Iran":{"name":"Sanctions against Iran"},"YCT:001000671":{"name":"Elections"},"WIKIID:Peyton_Manning":{"name":"Peyton Manning"},"WIKIID:Hillary_Clinton":{"name":"Hillary Clinton"},"WIKIID:Bernie_Sanders":{"name":"Bernie Sanders"},"WIKIID:Iowa_Democratic_Party":{"name":"Iowa Democratic Party"},"YCT:001000661":{"name":"Politics"},"WIKIID:United_States_Marine_Corps":{"name":"United States Marine Corps"},"WIKIID:1st_Marine_Division_%28United_States%29":{"name":"1st Marine Division (United States)"},"YCT:001000705":{"name":"Military"},"WIKIID:Oprah_Winfrey":{"name":"Oprah Winfrey"},"YCT:001000123":{"name":"Business"},"WIKIID:Johnny_Manziel":{"name":"Johnny Manziel"},"WIKIID:Deion_Sanders":{"name":"Deion Sanders"},"YCT:001000290":{"name":"Dating"}}}},"items":{"yui_module":"td-applet-stream-items-model-v2","yui_class":"TD.Applet.StreamItemsModel2"},"applet_model":{"yui_module":"td-applet-stream-appletmodel-v2","yui_class":"TD.Applet.StreamAppletModel2","config":{"ads":{"adchoices_text":true,"adchoices_url":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","ad_polices":true,"count":25,"contentType":"","fallback":0,"feedback":true,"frequency":4,"inline_video":true,"pu":"www.yahoo.com","se":4250754,"related_ct_se":"5454650","related_ct_single_ad":true,"related_dedupe_ads":true,"spaceid":"2023538075","sponsored_url":"http://help.yahoo.com/kb/index?page=content&y=PROD_FRONT&locale=en_US&id=SLN14553","start_index":1,"timeout":0,"type":"STRM,STRM_CONTENT,STRM_VIDEO","version":2,"related_start_index":3},"category":"","js":{"attribution_pos":"bottom","click_context":false,"content_events":true,"filters":false,"full_content_event":false,"pluck_item_fields":"id,cauuid,payoffId,events,i13n,link,type,videoUuid,viewer_eligible,viewer_fetch,video_ad_beacons,video_ad_assets","pluck_today_fields":"image,title,summary,publisher,more_link_text","poll_interval":60000,"related_collapse":true,"related_content":true,"related_count":4,"restore_filter":false,"restore_state":false,"restore_state_storage":"history","show_read":true,"sticker":false,"sticker_toptarget":"","summary_pos":"left","xhr_ss_timeout":500,"stories_like_this":0,"login_url":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US&.src=fpctx&.done="},"linkSupplement":null,"onboarding":{"count":3,"debug":false,"pos":-1,"result":3,"topic":false},"pagination":true,"remoteConfig":{"endpoint":"/_td_remote","params":{"disableAppClass":true,"footer":false,"i13n":{"auto":false},"noAbbreviation":true,"rendermode":"hovercard"},"passLocdropCrumb":true,"type":"td-applet-weather"},"request_xhtrace":0,"signed_in":false,"ss_endpoint":"ga-hr.slingstone.yahoo.com@score/v9/homerun/en-US/unified/mega","ss_timeout":300,"template":{"main":"td-applet-stream-atomic:main","drawer":"td-applet-stream-atomic:drawer_desktop","drawer_action":"td-applet-stream-atomic:drawer_action","drawer_share":"td-applet-stream-atomic:drawer_share"},"timeout":800,"total":170,"ui":{"backfill_property_region":false,"big_click_target":true,"bleed":false,"bnb_enabled":true,"cat_label_enabled":true,"comments":true,"comments_inline":true,"comments_inline_ranking":"highestRated","comments_offnet":false,"comments_editorial_suppression":true,"comments_request_count":5,"comments_update":false,"comments_update_maxidle":300000,"comments_update_monitoring":true,"comments_update_throttle":1750,"comments_writes_enabled":true,"followable":true,"followable_signedout":true,"dislike":false,"dollar_sign":true,"editorial_edition":{"label":"EDITION_DEFAULT","top":true,"shouldUpdateLVtimeInCookie":true},"enable_featured_ad_video_gap":true,"enable_hovercolor":true,"enable_stream_notify":false,"enrich_tweet":0,"enrichment":{"types":"","unique":false},"entity_max":9,"exclude_types":"","exclusive_type":"","fauxdal":true,"featured_align":"left","featured_delayed_defer":false,"featured_percent_width":45,"featured_width":460,"featured_height":258,"featured_width_portrait":200,"featured_height_portrait":200,"first_featured_treatment":true,"fixed_layout":false,"fptoday_thumbs_sparse":false,"headline_wrapper":true,"hq_ad_template":"featured_ad","i13n_key_tar_qa":false,"image_quality_override":true,"incremental_count":0,"incremental_delay":300,"incremental_history":100,"inline_filters":false,"inline_filters_article_min":10,"inline_filters_max":6,"inline_video":true,"item_template":"items","like":true,"limit_summary_height":true,"link_to_finance":false,"sticker_top":"94px","mms":false,"needtoknow_actions":true,"needtoknow_template":"filmstrip","needtoknow_storyline_min":4,"off_network_tab":false,"open_in_tab":false,"defaultDrawerAction":{"title":"ACTION_DEFAULT","description":"Content preferences","url":"https://settings.yahoo.com/interests"},"payoffDrawerActions":{"local":{"title":"ACTION_LOCAL","description":"More local news","urlPrefix":"https://news.yahoo.com/local/"},"finance":{"title":"ACTION_FINANCE","description":"Manage portfolios","url":"https://finance.yahoo.com/portfolios"},"sports":{"actionsEnabled":false,"attributionEnabled":false,"title":"ACTION_SPORTS","description":"Edit my teams","url":"https://sports.yahoo.com/myteams"}},"payoffs":"","payoffExpTime":24,"payoffFinanceArticleCount":2,"payoffInterval":6,"payoffInvalidTime":0.5,"payoffSportsLocalTeams":false,"payoffLocalArticleCount":3,"payoffLocalHeadliner":true,"payoffStartIndex":1,"pcsExclusions":false,"preview_enabled":false,"preview_lcp":true,"pubtime_format":"ONE_UNIT_ABBREVIATED","pubtime_maxage":3600,"ribbon_headers":false,"sfl":false,"sfl_get_started_string":"SFL_LINK","scrollbuffer":900,"scroll_node_selector":"body","show_tooltips":true,"smart_crop":true,"stateful_subtle_hint":true,"storyline_cap_image":2,"storyline_cap_noimage":3,"storyline_count":2,"storyline_elapsed_time":true,"storyline_elapsed_time_threshold":720,"storyline_enabled":true,"storyline_items_to_show":2,"storyline_multi_image":true,"storyline_multi_image_roundup":true,"storyline_newsy_disabled":false,"stream_actions":true,"stream_actions_minimal":true,"stream_actions_reblog":true,"stream_actions_share":false,"stream_actions_share_panel":true,"stream_actions_likable":true,"stream_actions_tumblr":true,"stream_debug":false,"stream_payoff_actions":false,"stream_item_maxwidth":"none","stream_item_image_fallback":"https://s.yimg.com/dh/ap/default/151015/stream-gradient-230x130.png","stream_item_large_image_fallback":"https://s.yimg.com/dh/ap/default/150902/stream-gradient.png","stream_single_item_image_fallback":"https://s.yimg.com/dh/ap/default/150903/stream-gradient-single-3.png","summary":true,"summary_length":160,"template_label":"js-stream-dense","template_suffix":"","today_count":5,"today_snippet_count":5,"fc_related_minCount":2,"today_featured_image_tag":"pc:size=medium","tap_for_summary":false,"templates":{"all":{"batch_max":20,"gap":0,"max":170,"start":1,"batch":2,"total":2,"last":7},"featured":{"batch_max":20,"gap":0,"max":170,"portrait":false,"start":0,"batch":0,"total":0},"featured_ad":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":2,"total":2,"last":7},"inline_video":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":0,"total":0}},"thumbnail":true,"thumbnail_align":"right","thumbnail_hover":true,"thumbnail_size":160,"thumbnail_specs":{"featured":"img:190x107|2|80","featured_square":"img:190x190|2|80","inline_video":"img:190x107|2|80","items":"img:190x107|2|80","items_large":"img:190x190|2|80","storyline_square":"img:70x70|2|90","related":"img:165x120|2|80","featured_roundup":"img:702x274|1|95","roundup_wide":"img:170x80|2|80"},"toJpg":true,"truncate_summary":false,"tweet_action":true,"uis_inferred_finance":false,"ult_count":10,"ult_host":"hsrd.yahoo.com","ult_links":false,"use_ss_roundup_slot":true,"view_article_button":false,"viewer":true,"viewer_include_all":true,"viewer_off_network":false,"viewer_reblog":false,"viewer_action":true,"visit_state_threshold":1800000,"preview":0,"related_host":"","stream_payoff_v2":1,"stream_payoff_vizify":0,"stream_rc4":0,"show_jump_to_historical_items":0,"item_templates":["items"]},"weather":false,"woeid":12597132,"tz":"Europe/Paris","xcc":"fr"},"settings":{"size":9,"woeid":12597132},"models":["stream","items","related"]},"interest":{"yui_module":"td-applet-interest-model-v2","yui_class":"TD.Applet.InterestModel2"},"comments":{"yui_module":"td-applet-comments-model-v2","yui_class":"TD.Applet.CommentsModel2"},"related":{"yui_module":"td-applet-stream-related-model-atomic","yui_class":"TD.Applet.StreamRelatedModelAtomic"}},"views":{"main":{"yui_module":"td-applet-stream-mainview-v2","yui_class":"TD.Applet.StreamMainView2"},"drawer":{"yui_module":"stream-actiondrawer-v2"}},"templates":{"main":{"yui_module":"td-applet-stream-atomic-templates-main","template_name":"td-applet-stream-atomic-templates-main"},"related":{"yui_module":"td-applet-stream-atomic-templates-related","template_name":"td-applet-stream-atomic-templates-related"},"drawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_desktop","template_name":"td-applet-stream-atomic-templates-drawer_desktop"},"removedItem":{"yui_module":"td-applet-stream-atomic-templates-removeditem","template_name":"td-applet-stream-atomic-templates-removeditem"},"drawerAction":{"yui_module":"td-applet-stream-atomic-templates-drawer_action","template_name":"td-applet-stream-atomic-templates-drawer_action"},"drawerSharePanel":{"yui_module":"td-applet-stream-atomic-templates-drawer_share","template_name":"td-applet-stream-atomic-templates-drawer_share"},"breakingNews":{"yui_module":"td-applet-stream-atomic-templates-breaking_news","template_name":"td-applet-stream-atomic-templates-breaking_news"},"flyoutDrawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_flyout","template_name":"td-applet-stream-atomic-templates-drawer_flyout"},"items":{"yui_module":"td-applet-stream-atomic-templates-items","template_name":"td-applet-stream-atomic-templates-items"},"item_default":{"yui_module":"td-applet-stream-atomic-templates-item-default","template_name":"td-applet-stream-atomic-templates-item-default"},"item_default_clusters":{"yui_module":"td-applet-stream-atomic-templates-item-default_clusters","template_name":"td-applet-stream-atomic-templates-item-default_clusters"},"item_ad":{"yui_module":"td-applet-stream-atomic-templates-item-ad","template_name":"td-applet-stream-atomic-templates-item-ad"},"item_ad_dislike":{"yui_module":"td-applet-stream-atomic-templates-item-ad_dislike","template_name":"td-applet-stream-atomic-templates-item-ad_dislike"},"item_featured_ad":{"yui_module":"td-applet-stream-atomic-templates-item-featured_ad","template_name":"td-applet-stream-atomic-templates-item-featured_ad"},"item_featured":{"yui_module":"td-applet-stream-atomic-templates-item-featured","template_name":"td-applet-stream-atomic-templates-item-featured"},"item_inline_video":{"yui_module":"td-applet-stream-atomic-templates-item-inline_video","template_name":"td-applet-stream-atomic-templates-item-inline_video"}},"i18n":{"AD_FDB_HEADING":"Why don't you like this ad?","AD_FDB_TELL_US":"Tell us why","AD_FDB_THANKYOU":"Thank you for your feedback. We will remove this and make the changes needed.","AD_FDB1":"It is offensive to me","AD_FDB2":"It is not relevant to me","AD_FDB3":"I keep seeing this","AD_FDB4":"Something else","ABCLOGO":"ABC","ACTION_MORE_LIKE_THIS":"Got it! Tell us more about what you like:","ACTION_FEWER_LIKE_THIS":"Got it! Tell us more about what you dislike:","ACTION_DEFAULT":"Content preferences Â»","ACTION_SPORTS":"Edit my teams Â»","ACTION_FINANCE":"Manage portfolios Â»","ACTION_LOCAL":"More local news Â»","ACTION_STORY_REMOVED":"Story removed","ACTION_SEE_LESS":"Okay. You'll see fewer like this.","ACTION_SEE_MORE":"Great! You'll see more like this.","ACTION_SEE_NO_MORE":"This item has been removed from your stream.","ACTION_SIGN_IN_TO_DISLIKE":"{linkstart}Sign-in{linkend} and we'll show you less like this in the future.","ACTION_SIGN_IN_TO_DISLIKE_TUMBLR":"{linkstart}Sign-in{linkend} to dislike","ACTION_SIGN_IN_TO_LIKE":"{linkstart}Sign-in{linkend} and we'll show you more like this in the future.","ACTION_SIGN_IN_TO_LIKE_SIMPLE":"Sign in to like","ACTION_SIGN_IN_TO_LIKE_TUMBLR":"{linkstart}Sign-in{linkend} to like","ACTION_SIGN_IN_TO_PERSONALIZE":"{linkstart}Sign-in{linkend} to personalize!","ACTION_SIGN_IN_TO_SAVE":"{linkstart}Sign-in{linkend} to save this story to read later.","ACTION_SIGN_IN_TO_SAVE_SHORT":"Sign in to save","ACTION_TELL_LIKE":"Tell us more about what you like:","ACTION_TELL_DISLIKE":"Tell us more about what you dislike:","ACTION_MORE_LESS":"Show more or less of","ACTION_OPTIONS":"Options","ACTION_REMOVE":"Remove from my stream","ACTION_STORIES_LIKE_THIS":"Stories like this","ACTION_OPEN":"Open","ACTION_CLOSE":"Close","ADCHOICES":"AdChoices","ALL_ARTICLES":"All Articles","ALL_CELEBRITY":"All Celebrity News","ALL_FINANCE":"All Finance","ALL_MOVIES":"All Movies News","ALL_MUSIC":"All Music News","ALL_NEWS":"All News","ALL_SPORTS":"All Sports","ALL_STORIES":"All Stories","ALL_TRAVEL":"All Travel Ideas","ALL_TV":"All TV News","AROUND_THE_WEB":"Around The Web","ALSO_LIKE":"You may also like","ASTRO_GO_TO":"Go to Horoscopes Â»","ASTRO_NAME_AQUARIUS":"Aquarius","ASTRO_NAME_ARIES":"Aries","ASTRO_NAME_CANCER":"Cancer","ASTRO_NAME_CAPRICORN":"Capricorn","ASTRO_NAME_GEMINI":"Gemini","ASTRO_NAME_LEO":"Leo","ASTRO_NAME_LIBRA":"Libra","ASTRO_NAME_PISCES":"Pisces","ASTRO_NAME_SAGITTARIUS":"Sagittarius","ASTRO_NAME_SCORPIO":"Scorpio","ASTRO_NAME_TAURUS":"Taurus","ASTRO_NAME_VIRGO":"Virgo","ASTRO_TITLE":"Today's overview","AUCTION":"Action","AUTHOR_AT_PUBLISHER":"{author} at {publisher}","BREAKING_NEWS":"BREAKING NEWS","CNBCLOGO":"","COMMENT_ARTICLE_SIGN_IN":"Sign in to perform this action.","COMMENTS":"Comments","COMMENTS_ZERO":"Start the conversation","CONTENT_PREF":"Content preferences","DISLIKE":"Dislike","DONE":"Done","DYNAMIC_FILTERS":"You Might Like","EDITION_DEFAULT":"Need To Know","FEATURED_MOVIE":"Featured Movie","FLAGGED_COMMENT":"Flagged as inappropriate. Thanks for your feedback.","FOLLOW":"Follow","FOLLOW_GAME":"Follow Game","FOLLOWING":"Following {name}","FOLLOWING_STORY":"Following","FOLLOW_UPDATES":"Follow to receive updates on","FULL_HOROSCOPE":"Full Horoscope","GAME_PERIOD:baseball":"Innings","GAME_PERIOD:football":"Quarters","GAME_PERIOD_1":"1st","GAME_PERIOD_2":"2nd","GAME_PERIOD_3":"3rd","GAME_PERIOD_4":"4th","GAME_COVERAGE":"See Full Coverage","GAME_FINAL":"Final","GAME_RECAP":"Game Recap","GAME_TITLE":"{away_team} vs. {home_team}","HALFTIME":"Halftime","HOROSCOPE":"Horoscope","IN_THEATERS_NOW":"In Theaters Now","IN_THEATERS_TODAY":"In Theaters Today","INSTALL_APP":"Install now","IN_WATCHLIST":"In watchlist","IS_IN_FAVORITES":"is in your favorites","JUMP_TO_HISTORICAL_ITEMS":"Go to where you left off","LATEST_NEWS":"Latest News","LESS":"Less","LIKABLE_ARTICLE_SIGN_IN":"Sign in to save your preferences.","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","LIVE":"Live","LIVE_GAME":"Live","LOAD_MORE":"Load more stories","LOCAL":"Local","LOCAL_BLOWING_SNOW":"Blowing Snow","LOCAL_BLUSTERY":"Blustery","LOCAL_CELSIUS":"Â°C","LOCAL_CLEAR":"Clear","LOCAL_CLOUDY":"Cloudy","LOCAL_COLD":"Cold","LOCAL_DRIZZLE":"Drizzle","LOCAL_DUST":"Dust","LOCAL_FAIR":"Fair","LOCAL_FOGGY":"Foggy","LOCAL_FAHRENHEIT":"Â°F","LOCAL_FREEZING_DRIZZLE":"Freezing Drizzle","LOCAL_FREEZING_RAIN":"Freezing Rain","LOCAL_GO_TO":"Go to Weather Â»","LOCAL_HAIL":"Hail","LOCAL_HAZE":"Haze","LOCAL_HEAVY_SNOW":"Heavy Snow","LOCAL_HOT":"Hot","LOCAL_HURRICANE":"Hurricane","LOCAL_ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","LOCAL_ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LOCAL_LIGHT_SNOW_SHOWERS":"Light Snow Showers","LOCAL_MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","LOCAL_MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","LOCAL_MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","LOCAL_MOSTLY_CLOUDY":"Mostly Cloudy","LOCAL_NEWS":"Local news","LOCAL_NO_STORIES":"Sorry, there are no news articles related to this location. Try setting your location to the nearest large city for more stories.","LOCAL_PARTLY_CLOUDY":"Partly Cloudy","LOCAL_SCATTERED_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_THUNDERSTORMS":"Scattered Thunderstorms","LOCAL_SEVERE_THUNDERSTORMS":"Severe Thunderstorms","LOCAL_SHOWERS":"Showers","LOCAL_SLEET":"Sleet","LOCAL_SMOKY":"Smoky","LOCAL_SNOW":"Snow","LOCAL_SNOW_FLURRIES":"Snow Flurries","LOCAL_SNOW_SHOWERS":"Snow Showers","LOCAL_SUNNY":"Sunny","LOCAL_THUNDERSHOWERS":"Thundershowers","LOCAL_THUNDERSTORMS":"Thunderstorms","LOCAL_TITLE":"{city} News","LOCAL_TORNADO":"Tornado","LOCAL_TROPICAL_STORM":"Tropical Storm","LOCAL_UNKNOWN":"Unknown Conditions","LOCAL_WINDY":"Windy","MIDFIELD":"midfield","MLB_BALLS":"Balls","MLB_ERRORS":"Errors","MLB_HITS":"Hits","MLB_OUTS":"Outs","MLB_RUNS":"Runs","MLB_STRIKES":"Strikes","MORE":"More","MORE_FROM_ROUNDUP":"More from this Roundup","MORE_FROM_TOPIC":"More from {topic} news","MYQUOTES_ADD_MORE":"Add more investments to your {0}Portfolio{1} to stay up to date whenever you visit Yahoo Finance.","MYQUOTES_LOGIN":"{0}Sign-in{1} to see the latest news from the investments in your portfolio.","MYTEAMS_ADD_TEAMS":"{0}Add your favorite teams{1} to start getting news about them today.","MYTEAMS_LOGIN":"{0}Sign-in{1} to get news for your favorite teams.","MYTEAMS_NO_CONTENT":"We can't find recent news for your teams. {0}Edit your teams{1} or visit the {2}All Sports news{3}","MYSAVES":"My Saves","NEW_SINCE_YOUR_LAST_VISIT":"New since your last visit","NEW_STORIES":"New for you","NEW_STORIES_COUNT":"View {new_item_count} new {new_item_count, plural, one {story} other {stories}}","NEXT":"Next","NFL_PROGRESS":"on {field_side} {yard_line}","NO_STORIES_HEADER":"We couldn't find any new stories for you.","NO_STORIES_BODY":"Please check back later or {0}try again{1}","OFF":"OFF","OFFNETWORK":"Read this on {0}","ONBOARDING_CONFIRMATION_INITIAL":"Got it! Almost there","ONBOARDING_CONFIRMATION_ONE":"Got it! One more time","ONBOARDING_CONFIRMATION_ZERO":"Got it! Youâre done","ONBOARDING_TITLE1_FINAL":"Here are some stories based on your preferences","ONBOARDING_TITLE1_INITIAL":"Which would you rather read?","ONBOARDING_TITLE1_ONE":"Now which do you prefer?","ONBOARDING_TITLE1_ZERO":"Awesome! What is your final pick?","OPENS_TODAY":"Opens Today","PORTFOLIO":"Go to your portfolio","PORTFOLIO_GAINERS":"Gainers","PORTFOLIO_LOSERS":"Losers","PORTFOLIO_MARKET_CLOSED":"Market close","PLAY":"Play","PLAY_VIDEO":"Play Video","PLAYING":"playing","PLAYING_NEAR_YOU":"Playing near you","PREVIEW_GAME":"Preview Game","PREVIOUS":"Previous","PREVIOUSLY_VIEWED":"From your last visit","PROGRESS":"{possession_team} {down} & {to_go} at {field_side} {yard_line}","READ_MORE":"Read More","REBLOG":"Reblog on Tumblr","REMOVE":"Remove","SAVE":"Save","SCORE":"Score!","SCORE_ATTRIBUTION":"Click here to track the {display_name} &rarr;","SEE_ALL_STORIES":"See all stories Â»","SEE_DETAILS":"See details Â»","SEE_MORE_STORIES":"See more stories Â»","SEE_FULL_BOXSCORE":"See full boxscore Â»","SFL_HEADER":"Hi {0}, you have no saves. Here's how to get started:","SFL_LINK":"Get started now on the {0}Yahoo Homepage{1}.","SFL_LINK_ATT":"Get started now on the {0}att.net Homepage{1}.","SFL_LINK_FRONTIER":"Get started now on the {0}Frontier Yahoo Homepage{1}.","SFL_LINK_ROGERS":"Get started now on the {0}Rogers Yahoo Homepage{1}.","SFL_LINK_VERIZON":"Get started now on the {0}Verizon Yahoo Homepage{1}.","SFL_STEP_ONE":"Step 1","SFL_STEP_TWO":"Step 2","SFL_TITLE_ONE":"Click on {0} in the stream and on articles across Yahoo to save stories for later.","SFL_TITLE_TWO":"Access your saves from the profile menu {0} on desktop & tablet or menu {1} on your smartphone.","SHARE_THIS":"SHARE THIS","SHOPPING":"Shopping","SIGN_IN":"Sign in!","SIGN_IN_2":"Sign in","SIGN_IN_FOR_MORE":"Looking for past stories?","SIGN_IN_TO_SEE_MORE_TEAMS":" to save your preferences. You'll see more of this team next time you visit.","SLIDESHOW_COUNT":"({0} photos)","SLIDESHOW_PREVIEW_COUNT":"1 of {0}","SPONSORED":"Sponsored","STATUS":"{time} {period}","STORE":"Store","STORIES":"Stories:","STORIES_ABOUT":"Stories about:","SYNOPSIS":"Synopsis","TIME_AGO":"ago","TIME_DAY":"day","TIME_H":"h","TIME_M":"m","TITLE":"Recommended for You","TITLE_LOCAL":"{city} News","TO":"to","TWITTER_REPLY":"Reply","TWITTER_RETWEET":"Retweet","TWITTER_FAVORITE":"Favorite","UNFOLLOW":"Unfollow","UNDO":"Undo","UPCOMING_GAME":"Today at {game_time}","UP_OVER":"Up over","DOWN_OVER":"Down over","VIEW":"View","VIEW_ALL":"View All","VIEW_SLIDESHOW":"View Slideshow","VIEW_FULL_ARTICLE":"VIEW FULL ARTICLE","VIEW_FULL_QUOTE":"View full quote Â»","YAHOO_MOVIES":"Yahoo Movies","YAHOO_ORIGINALS":"Yahoo Originals","YCT:001000931":"Technology","YCUSTOM:COMMERCE":"Commerce","YCUSTOM:MYQUOTES":"My Quotes","YCUSTOM:MYTEAMS":"My Teams","YLABEL:autos":"Autos","YLABEL:business":"Business","YLABEL:beauty":"Beauty","YLABEL:celebrity":"Celebrity","YLABEL:diy":"DIY","YLABEL:entertainment":"entertainment","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:lifestyle":"Lifestyle","YLABEL:makers":"Projects","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:politics":"Politics","YLABEL:science":"science","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:technology":"Technology","YLABEL:travel":"Travel","YLABEL:trending":"Trending","YLABEL:tv":"TV","YLABEL:us":"US","YLABEL:world":"World","YPROP:FINANCE":"Business","YPROP:TOPSTORIES":"All Stories","YPROP:LOCAL":"Local","YPROP:NEWS":"News","YPROP:OMG":"Entertainment","YPROP:SCIENCE":"Science","YPROP:SPORTS":"Sports","YPROV:ABCNEWS":"News","YPROV:ap.org":"AP","YPROV:CNBC":"CNBC","YPROV:NBCSPORT":"NBC Sports","YPROV:reuters.com":"Reuters","YPROV:ROLLINGSTONES":"Rolling Stone","YPROV:us.sports.yahoo.com":"Experts","YTYPE:VIDEO":"Video","YPROP:STYLE":"Style","YMAG:food":"Food","YMAG:tech":"Tech","YMAG:travel":"Travel"},"i13n":{"pv":2,"pp":{"ccode_st":"mega_global_ranking_hlv2_up_based"}},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-trending-atomic":{"base":"/sy/os/mit/td/td-applet-trending-atomic-0.0.45/","root":"os/mit/td/td-applet-trending-atomic-0.0.45/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_32209491"] = {"applet_type":"td-applet-trending-atomic","views":{"main":{"yui_module":"td-applet-trending-atomic-mainview","yui_class":"TD.Applet.TrendingAtomicMainView","config":{"clickableTitles":true,"rotationEnabled":true,"rotationInterval":12000,"giftsEnabled":true}}},"templates":{"main":{"yui_module":"td-applet-trending-atomic-templates-main","template_name":"td-applet-trending-atomic-templates-main"}},"i18n":{"GIFTS_TITLE":"Valentine's Day","TITLE":"Trending","TRENDING_NEWS":"Trending News","TRENDING_NOW":"Trending Now"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-weather-atomic":{"base":"/sy/os/mit/td/td-applet-weather-atomic-0.0.23/","root":"os/mit/td/td-applet-weather-atomic-0.0.23/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_63794"] = {"applet_type":"td-applet-weather-atomic","models":{"applet_model":{"yui_module":"td-applet-weather-atomic-appletmodel","yui_class":"TD.Applet.WeatherAppletModel","models":["weather"],"data":{"i13n":{"sec":"app-wea","bucket":"201"},"current":12597132,"favorite":null,"useplus":false},"user_settings":{"unit":"f","curloc":"12597132"},"config":{"moreinfo":{"link":"http://www.weather.com/?par=yahoonews","img":"http://l.yimg.com/dh/ap/default/130915/wcl1.gif"},"enableFav":false,"enableWeatherYQLP":true,"baseLink":"https://weather.yahoo.com","showWidgetLink":true,"urlGeneratorRules":{"enabled":false,"url_format_local":"","url_format_international":"","url_format_local_ajax":"","local_country_code":[]},"daysLimit":4,"fixed_layout":false}},"weather":{"yui_module":"td-applet-weather-atomic-model","yui_class":"TD.Applet.WeatherModel","data":{"weatherDataList":[{"location":{"woeid":"12597132","city":"Yvrac","state":"33","country":"FR"},"link":{"href":"https://weather.yahoo.com/fr/33/yvrac-12597132/"},"timezone":"CET","flickr":{"attribution":null,"image_assets":[{"width":"2048","height":"2048","type":"main","url":"https://s1.yimg.com/nn/lib/metro/clear_n.jpg"}]},"current":{"sunrise":"08:16:22","sunset":"18:16:24","condition":{"description":"Fair","code":"34","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/fair_day.png"}}},"temp":{"unit":"f","now":"61"},"atmosphere":{"humidity":"52","wind_speed":"15","wind_chill":"61"}},"forecast":{"day":[{"label":"Today","day_of_week":"6","condition":{"description":"Showers","code":"12","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"59","low":"46","unit":"f"}},{"label":"Sun","day_of_week":"0","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"53","low":"48","unit":"f"}},{"label":"Mon","day_of_week":"1","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"55","low":"51","unit":"f"}},{"label":"Tue","day_of_week":"2","condition":{"description":"Showers","code":"12","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"53","low":"44","unit":"f"}}]},"woeid":"12597132","accordianState":"e"}],"currentLoc":{"woeid":"12597132","city":"Yvrac","state":"33","country":"FR"}}}},"views":{"main":{"yui_module":"td-applet-weather-atomic-mainview","yui_class":"TD.Applet.WeatherMainView"},"header":{"yui_module":"td-applet-weather-atomic-headerview","yui_class":"TD.Applet.WeatherHeaderView","config":{}}},"templates":{"main":{"yui_module":"td-applet-weather-atomic-templates-main","template_name":"td-applet-weather-atomic-templates-main"}},"i18n":{"CURRENT_LOCATION":"Current Location","WEATHER":"Weather","HIGH":"High","LOW":"Low","TODAY":"Today","TOMORROW":"Tomorrow","SUNDAY":"Sunday","MONDAY":"Monday","TUESDAY":"Tuesday","WEDNESDAY":"Wednesday","THURSDAY":"Thursday","FRIDAY":"Friday","SATURDAY":"Saturday","ABBR_SUNDAY":"Sun","ABBR_MONDAY":"Mon","ABBR_TUESDAY":"Tue","ABBR_WEDNESDAY":"Wed","ABBR_THURSDAY":"Thu","ABBR_FRIDAY":"Fri","ABBR_SATURDAY":"Sat","BLOWING_SNOW":"Blowing Snow","BLUSTERY":"Blustery","CLEAR":"Clear","CLOUDY":"Cloudy","COLD":"Cold","DRIZZLE":"Drizzle","DUST":"Dust","FAIR":"Fair","FREEZING_DRIZZLE":"Freezing Drizzle","FREEZING_RAIN":"Freezing Rain","FOGGY":"Foggy","FORECAST":"Forecast","FULL_FORECAST":"Full forecast","HAIL":"Hail","HAZE":"Haze","HEAVY_SNOW":"Heavy Snow","HOT":"Hot","HURRICANE":"Hurricane","ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LIGHT_SNOW_SHOWERS":"Light Snow Showers","MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","MIXED_RAIN_AND_SLEET":"Mixed Rain and Sleet","MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","MOSTLY_CLOUDY":"Mostly Cloudy","PARTLY_CLOUDY":"Partly Cloudy","SCATTERED_SHOWERS":"Scattered Showers","SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","SCATTERED_THUNDERSTORMS":" Scattered Thunderstorms","SEARCH_CITY":"Kindly search your city","SEVERE_THUNDERSTORMS":"Severe Thunderstorms","SHOWERS":"Showers","SLEET":"Sleet","SMOKY":"Smoky","SNOW":"Snow","SNOW_FLURRIES":"Snow Flurries","SNOW_SHOWERS":"Snow Showers","SUNNY":"Sunny","THUNDERSHOWERS":"Thundershowers","THUNDERSTORMS":"Thunderstorms","TORNADO":"Tornado","TROPICAL_STORM":"Tropical Storm","WINDY":"Windy","WEATHER_CHICLET_ERROR":"Visit {linkstart}Yahoo Weather{linkend} for a detailed forecast.","GOTO_WEATHER":"See more Â»","NO_WEATHER_MESSAGE":"Visit {linkstart}Yahoo Weather{linkend} for the latest forecast or {detectstart}click here{detectend} to select your precise location.","TEN_DAY":"10 day"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-activitylist-atomic":{"base":"/sy/os/mit/td/td-applet-activitylist-atomic-0.0.60/","root":"os/mit/td/td-applet-activitylist-atomic-0.0.60/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000171"] = {"applet_type":"td-applet-activitylist-atomic","templates":{"list":{"yui_module":"td-applet-activitylist-atomic-templates-list.tumblr","template_name":"td-applet-activitylist-atomic-templates-list.tumblr"}},"i18n":{"ACTION_POST":"Posted","FAVORITE":"favorite","FIND_OUT_WHY":"Find out why Â»","LIVE":"Live","REASON:BREAKING":"Breaking","REASON:COMMENTS":"{count, number, integer} Comments","REASON:EVERGREEN":"","REASON:EXCLUSIVE":"Exclusive","REASON:NEW":"New","REASON:NEW_ON":"New on {site}","REASON:ONLYONYAHOO":"Only on Yahoo","REASON:POPULARONSEARCH":"Popular on Search","REASON:READINGNOW":"{count, number, integer} Reading Now","REASON:SEARCHVISITS":"{count, number, integer} Search Visits","REASON:SHARES":"{count, number, integer} Shares","REASON:SOCIALVISITS":"{count, number, integer} Social Visits","REASON:VIDEOPLAYS":"{count, number, integer} Video Plays","REASON:VIEWS":"{count, number, integer} Views","REASON:VISITS":"{count, number, integer} Visits","REASON:WATCHINGNOW":"{count, number, integer} Watching Now","REASON:WATCHLIVE":"Watch Live","REPLY":"reply","RETWEET":"retweet","TIME_FORMAT":"h:mm A","TIME_FORMAT_SAME_DAY":"[Today]","TIME_FORMAT_NEXT_DAY":"[Tomorrow]","TIME_FORMAT_NEXT_WEEK":"dddd","TIME_FORMAT_ELSE":"MMM D","TITLE":"Only from Yahoo","TWEET_NAVIGATE":"navigate to tweet","VIA_TWITTER":"via Twitter","YLABEL:autos":"Autos","YLABEL:beauty":"Beauty","YLABEL:diy":"DIY","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:parenting":"Parenting","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:travel":"Travel","YLABEL:tv":"TV","YPROP:autos":"Yahoo Autos","YPROP:beauty":"Yahoo Beauty","YPROP:diy":"Yahoo DIY","YPROP:finance":"Yahoo Finance","YPROP:food":"Yahoo Food","YPROP:games":"Yahoo Games","YPROP:gma":"Yahoo News","YPROP:health":"Yahoo Health","YPROP:homes":"Yahoo Homes","YPROP:ivy":"Yahoo Screen","YPROP:makers":"Yahoo Makers","YPROP:movies":"Yahoo Movies","YPROP:music":"Yahoo Music","YPROP:news":"Yahoo News","YPROP:omg":"Yahoo Celebrity","YPROP:parenting":"Yahoo Parenting","YPROP:politics":"Yahoo Politics","YPROP:shopping":"Yahoo Shopping","YPROP:sports":"Yahoo Sports","YPROP:style":"Yahoo Style","YPROP:tech":"Yahoo Tech","YPROP:travel":"Yahoo Travel","YPROP:tv":"Yahoo TV","YPROP:yahoo":"Yahoo"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
    window.Af = window.Af || {};
    window.Af.config = window.Af.config || {};
    window.Af.config.spaceid = "2023538075";
    window.Af.context= {
        crumb : 'ZSDxLnSI75q',
        guid : '',
        mcCrumb: 'd8GMl9M69I4',
        ucsCrumb: '2LBS.LAp3YG',
        device: 'desktop',
        rid : '5qnosq1bbcbsb',
        default_page : 'p1',
        _p : 'p1',
        site : 'fp',
        lang : 'en-US',
        region : 'US',
        authed: 0,
        enable_dd : '',
        default_appletinit : 'viewport',
        locdrop_crumb: '', woeid: '12597132',
        ssl: 1,
        
        bucket: '201'
        
        
    };

    window.Af.config.transport = {
        crumbForGET: true,
        xhr: '/fpjs',
        consolidate: true,
        timeout: 6000
    };

    window.Af.config.onepush = {
        subscribeTimeout : 5000,
        subscribeMaxTries : 1,
        trackInitComet : true,
        trackLatency : true,
        latencySampleSize : 50,
        publicCometHost: 'https://comet.yahoo.com/comet',
        shutdown: false,
        trackShutdown: false
    };

    window.Af.config.beacon = {
        beaconUncaughtErr: true,
        sampleSizeUncaughtErr: 1,
        maxUncaughtErrCount: 5,
        beaconPageLoadPerf: false,
        sampleSize : 1,
        pathPrefix : '/_td_api/beacon',
        batch: false,
        batchInterval: 3
    };

    window.Af.config.pipe = {
        msg_throttle: 2000,
        err_maxstreak: 2
    };

    window.Af.config.td = {
        remote: '/_td_remote',
        xhr: '/_td_api'
    };

    
    YUI.namespace('Env.My.settings').context = {
        uh_js_file : '',
                videoAsyncEnabled: 0,
        videoplayerScriptElementId: 'videoplayerJs',
        videoplayerUrl: 'https://www.yahoo.com/sy/rx/builds/6.155.0.1453355318/en-us/videoplayer-nextgen-flash-min.js',
        videoAutoplay: 1,
        videoLooping: 0,
        videoForcedError: 0,
        videoFullscreen: 1,
        videoHtml5: 0,
        videoMinControls: 0,
        videoCmsEnv: 'prod',
        videoMustWatch: 1,
        videoMWSticky: 0,
        videoMute: 1,
        videoQosRate: 1,
        videoBuffering: 0,
        videoRelated: 0,
        videoYwaRate: 0,
        videoMaxLoops: 3,
        videoModeEnabled: 0,
        videoTiltbackModeEnabled: 0,
        videoSSButton: 0,
        videoPausescreen: 0,
        videoSingleVideo: 0,
        videoMaxInstances: 12,
        videoInitEvent: 'domready',
        videoExpName: 'advance',
        videoComscoreC4: 'US fp',
        videoplayerScrollThrottle: '200',
        
        themeCssEnabled : false,
        themeBgImageEnabled: false,
        transparentPixelImage: 'https://s.yimg.com/os/mit/media/m/base/images/transparent-95031.png',
        pageMessage: {
            type: '',
            msg: ''
        },
        servingBeaconUrl: 'https://bs.serving-sys.com/BurstingPipe/ActivityServer.bs',
        enablePerfBeacon: '0',
        test_id: '201',
        customizedEnabled: false,
        trendingNowOffScreen: 0
    };

    YUI.namespace('Env.Af.Perf').secondYUIUseStart = (new Date()).getTime();

    var yuiPreloadModules = ["type_advance_desktop_viewer","type_video_manager","type_sdarotate","type_sda"];

    yuiPreloadModules = yuiPreloadModules.concat((function (appletTypes) {
        if (!appletTypes || appletTypes.length === 0) {
            return [];
        }
        var yui_modules = [],
            types = YMedia.Array.hash(appletTypes);
        YMedia.Object.each(window.Af.bootstrap, function (bootstrap, guid) {
            if (!types[bootstrap.applet_type]) {
                 return;
            }
            YMedia.Array.each(['models', 'views', 'templates'], function (type) {
                YMedia.Object.each(bootstrap[type], function (config, name) {
                    if (config.yui_module) {
                         yui_modules.push(config.yui_module);
                    }
                });
            });
        });
        return yui_modules;
    })([]));

    YMedia.use(yuiPreloadModules, function (Y, NAME) {
        YUI.namespace('Env.Af.Perf').secondYUIUseStop = (new Date()).getTime();
        var pageMessage = YUI.Env.My.settings.context.pageMessage,
        enablePerfBeacon = YUI.Env.My.settings.context.enablePerfBeacon,
        test_id = YUI.Env.My.settings.context.test_id,
        pausePerfBeacon = false,
        perfBeacon = {}
                ,
        rapidConfig = rapidPageConfig.rapidConfig,
        YWA_CF_MAP = rapidPageConfig.ywaCF,
        YWA_ACTION_MAP = rapidPageConfig.ywaActionMap,
        YWA_OUTCOME_MAP = rapidPageConfig.ywaOutcomeMap;
        if(YAHOO.i13n) {
            YAHOO.i13n.WEBWORKER_FILE = '/lib/metro/g/myy/rapidworker_1_2_0.0.3.js';
            YAHOO.i13n.SPACEID = '2023538075';
            YAHOO.i13n.TEST_ID = '201';
        }
        if (pageMessage.msg != '') {
            Y.Af.Message.show('body', {level: pageMessage.type, content: pageMessage.msg});
        }

        if (1 === 1) {
            Y.Lang.later(45000, null, function() {
                var failedModules  = Y.all("#myColumns .js-applet .App-loading");
                failedModules.each(function (module) {
                    module.setContent('Sorry! We are temporarily unable to load the content. Please refresh or try again later.');
                    module.removeClass('App-loading');
                    module.addClass('App-failed');
                });
            });
        }
        
        
        
        

        YUI.namespace('Env.Af.Perf').YMyAppCreateStart = (new Date()).getTime();
        YMedia.My.App = new Y.My.App(
            {                i13nConfig: {
                    rapid: rapidConfig,
                    rapidInstance: rapidPageConfig.rapidSingleInstance && YAHOO && YAHOO.i13n && YAHOO.i13n.rapidInstance || null,
                    ywaMaps: {
                        YWA_OUTCOME_MAP: YWA_OUTCOME_MAP,
                        YWA_CF_MAP: YWA_CF_MAP,
                        YWA_ACTION_MAP: YWA_ACTION_MAP
                    }
                },
                inlineViewer: 0,
                summaryView: 0,
                stickerTarget: "#SearchBar-Wrapper-Mini",
                magazineViewerEnabled: 1,
                viewerBlacklistDisable: 1,
                viewer: {
                    viewerAnimation: "slide",
                    pageAnimation: "zoom",
                    scrollTopAmount: 0,
                    highlanderMode: "mega-modal"
                }}
        );
        

        var firePerfBeacon = function(e) {
            var key, value, beaconNode, queryString = [];

            if (Y.Object.size(perfBeacon) < 1 || pausePerfBeacon) {
                return;
            }
            pausePerfBeacon = true;
            for (key in perfBeacon) {
                if (perfBeacon.hasOwnProperty(key)) {
                    queryString.push(key+'='+perfBeacon[key]);
                }
            }
            queryString.push('test='+test_id);
            beaconNode = Y.Node.create('<img src="myperf.php?'+queryString.join('&')+'" style="width:1px;height:1px;position:absolute;left:-900px;">');
            Y.one('body').append(beaconNode);
        };

        if (enablePerfBeacon == '1') {
            Y.one('window').once('scroll', firePerfBeacon);
            Y.Lang.later(10000, null, firePerfBeacon);

            YMedia.My.App.after('appletsinit', function (e) {
                if (pausePerfBeacon == true) {
                    return;
                }
                perfBeacon[e.applet.type] = (new Date()) - myYahoostartTime;
            });
        }
        YMedia.on('appletsinit', function (e) {
            
        });
        
    });
            });
     });
</script>
    <!-- bottom -->

    <!-- Comscore -->
            <!-- Begin comScore Tag -->
		<script>
		  var _comscore = _comscore || [];
		  _comscore.push({
		    c1: "2",
		    c2: "7241469",
		    c5: "2023538075",
		    c7: "https://www.yahoo.com/"
		  });
		  (function() {
		    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		    s.src = "/sy/lq/lib/3pm/cs_0.2.js";
		    el.parentNode.insertBefore(s, el);
		  })();
		</script>
		<noscript>
		  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=7241469&c7=https%3A%2F%2Fwww.yahoo.com%2F&c5=2023538075&cv=2.0&cj=1" />
		</noscript>
		<!-- End comScore Tag -->

    <!-- Nielsen -->
    

    <!-- Lighthouse  -->
    

    <!-- yaft -->
            <script type="text/javascript" src="/sy/zz/combo?/os/mit/media/p/content/content-aft-min-a202e73.js&/os/mit/media/p/content/yaft2-aftnoad-min-e5cc02b.js&os/mit/media/p/content/yaft2-harbeacon-min-dabe12d.js"> </script>        
        <script type="text/javascript">
            if (window.YAFT !== undefined) {
                var __yaftConfig = {
                    modules: ["UH","mega-uh","mega-topbar","applet_p_","ad-north-base","fea-","my-adsFPAD-base","my-adsLREC-base","my-adsMAST","my-adsTXTL","content-modal-","hl-ad-LREC-","modal-sidekick-","hl-ad-LREC-0","hl-ad-MON-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","hl-ad-SPL-0"], 
                    modulesExclude: ["UH-Search","UH-ColWrap","mega-uh-wrapper"],
                    canShowVisualReport: false,
                    useNormalizeCoverage: true,
                    generateHAR: false,
                    includeOnlyAft2: true,
                    useNativeStartRender : true,
                    modulesAft2Container: ["hl-viewer"],
                    maxWaitTime: 6000
                };
                __yaftConfig.plugins = [];                __yaftConfig.plugins.push({
                     name: 'aftnoad',
                     isPre: true,
                     config: {
                         useNormalizeCoverage: true,
                         adModules:["ad-north-base","my-adsFPAD-base","my-adsLREC-base","my-adsTL1","my-adsMAST","hl-ad-LREC-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","my-adsTXTL","hl-ad-SPL-0"]
                     }
                });
                            __yaftConfig.plugins.push({
                name: 'har',
                config: {
                    sec: 'har',
                    rapid: function() { if(YMedia && YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) {return YMedia.My.App.getRapidTracker(); } else {return null;}},
                    aftThreshold: 10000,
                    onlySlowest: false
                }
            });
            __yaftConfig.generateHAR = true;
                        if (window.LH && window.LH.isInitialized) {
            window.LH.tag('b',{val: '201'});               
            window.LH.tag("l", {val: false});
        }
                        window.aft2CB = function(data, error) {
        if (!error) {
            var aft2 = Math.round(data.aft);
            var vic2 = data.visuallyComplete;
            var srt2 = Math.round(data.startRender);

            if (window.LH && window.LH.isInitialized) {
                var lhRecord = function lhRecord(name, type, startTime, duration) {
                    window.LH.record(name, {
                        name: name, type: type, startTime: startTime, duration: duration
                    });
                };

                lhRecord('AFT', 'mark', aft2, 0);
                lhRecord('AFT2', 'mark', aft2, 0);
                lhRecord('VIC2', 'mark', vic2, 0);
                lhRecord('STR2', 'mark', srt2, 0);
                window.LH.beacon({
                    clearMarks:false,
                    clearMeasures: false,
                    clearCustomEntries: true,
                    clearTags: false
                });
            }

            var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
            if (rapidInstance) {
                var afterPageLoad = {
                    AFT: aft2,
                    AFT2: aft2,
                    STR: srt2,
                    VIC: vic2
                };
                var perfData = {
                    perf_commontime: {afterPageLoad: afterPageLoad}
                };
                rapidInstance.beaconPerformanceData(perfData);
            }
        }
    };
                window.YAFT.init(__yaftConfig, function(data, error) {
                    if (!error) {
                        try {
                                    if (window.LH && window.LH.isInitialized) {
            var results = [0], module = '', index = '', lhRecord = '';
            for (module in data.modulesReport) {
                for (index in data.modulesReport[module].resources) {
                    results.push(Math.round(data.modulesReport[module].resources[index].durationFromNStart));
                }
            }
            lhRecord = function (ma, va, custom) {
                var res = '';                                
                if (undefined === custom) {
                    if (va && !isNaN(va)) {
                        res = Math.round(va);
                    } else {
                        res = 0;
                    }
                } else {
                    res = va;
                }
                    window.LH.record(ma, { name: ma, type: 'mark', startTime: res, duration: 0 });
            };
                            
            lhRecord('AFT', data.aft);
            lhRecord('AFT1', data.aft);
            if (data.aftNoAd) {
                lhRecord('AFTNOAD', data.aftNoAd);
            }
            lhRecord('X_FB1', 40);
            lhRecord('X_FBN', 66);
            lhRecord('PLT_CACHED_REQs', data.httpRequests.onloadCached);
            lhRecord('COSTLY_RESOURCE', Math.max.apply(null, results));
            lhRecord('StartRender', data.startRender);
            if (screen) {
                lhRecord('SCR_H', screen.height);
                lhRecord('SCR_W', screen.width);
            }
            if (window.FPAD_rendered) {
                lhRecord('xAFT', data.aft);
                lhRecord('xPLT', data.pageLoadTime);
                if (window.rtAdStart && window.rtAdDone) {
                    lhRecord('ADSTART_FPAD', Math.round(rtAdStart));
                    lhRecord('ADEND_FPAD', Math.round(rtAdDone));
                }
            }

            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    lhRecord(_adLT[i][0], _adLT[i][1]);
                }
            }
            window.LH.beacon({ clearMarks: false, clearMeasures: false, clearCustomEntries: true, clearTags: false });   
        }
                                    var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
        if(rapidInstance) {
            var initialPageLoad = {
                AFT: Math.round(data.aft),
                AFT1: Math.round(data.aft),
                STR: data.startRender,
                VIC: data.visuallyComplete,
                PLT: data.pageLoadTime,
                DOMC: data.domElementsCount,
                HTTPC: data.httpRequests.count,
                CP: data.totalCoveragePercentage,
                NCP: data.normTotalCoveragePercentage
            };

            if(data.aftNoAd) {
                initialPageLoad.AFTNOAD = Math.round(data.aftNoAd);
            }              

            var customPerfData = {},
                pagePerfData = {},
                results = [];
            
            var yaftResults = [0], yaftModule = '', yaftIndex = '';
            // Find costly resource time
            for (yaftModule in data.modulesReport) {
                for (yaftIndex in data.modulesReport[yaftModule].resources) {
                    yaftResults.push(Math.round(data.modulesReport[yaftModule].resources[yaftIndex].durationFromNStart));
                }
            }
            pagePerfData['COSTLY_RESOURCE'] = Math.max.apply(null, yaftResults);
            pagePerfData['X_FB1'] = 40;
            pagePerfData['X_FBN'] = 66; 
   
            // Log ad perf data to rapid perf metric
            if (window.FPAD_rendered) {
                pagePerfData['xAFT'] = data.aft;
                pagePerfData['xPLT'] = data.pageLoadTime;
                if (window.rtAdStart && window.rtAdDone) {
                    pagePerfData['ADSTART_FPAD'] = Math.round(rtAdStart);
                    pagePerfData['ADEND_FPAD'] = Math.round(rtAdDone);
                }
            }
            
            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    pagePerfData[_adLT[i][0]]  = _adLT[i][1];
                }
            }
            
            customPerfData['utm'] = pagePerfData;
            var perfData = {
                perf_commontime: {initialPageLoad: initialPageLoad},
                perf_usertime: customPerfData
            };
            rapidInstance.beaconPerformanceData(perfData);
        }
                        } catch (e) {}                                                        
                    }
               });
            }
      </script>

    <!-- Via https/1.1 ir15.fp.ir2.yahoo.com[D992B341] (YahooTrafficServer), http/1.1 usproxy2.fp.bf1.yahoo.com[448EE3A1] (YahooTrafficServer) -->
    <!-- sid=2023538075 -->
    

    </body>
</html>
<!-- myproperty:myservice-us:0:Success -->
<!-- slw29.fp.bf1.yahoo.com compressed/chunked Sat Feb  6 17:38:19 UTC 2016 -->
