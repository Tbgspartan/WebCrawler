<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=117.3" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=117.3" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=117.3"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=117.3"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=117.3"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=117.3"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');fbq("track","ViewContent");</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=117.3"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0;
var isNewYear = 1;
</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                <a href="http://www.indiatimes.com/news/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/play/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Slog Overs');">Slog Overs</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  class="#fffff" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Trending');">Trending</a> 
                                                </div>
                </div>
                    <div class='onScrolled'> 
                        <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                        <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                        <div class="has-tag">
                                                    </div>
                    </div>

            
            </nav>
            
            <div id="sticker" style="display:none;"><span class="readTitle">
                                    </span></div>
                <div class="socls fr share">
                </div>


            <div class="social fr">
                <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
            </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if (this.value == 'Search') {
                        this.value = ''
                    }" onblur="if (this.value == '') {
                                this.value = 'Search'
                            }
                            ;" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data();" id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
					                </dt>
                                    <dt  data-color="yellow-bg" ><a href="http://www.indiatimes.com/play/">Slog Overs</a>
					                </dt>
                                <dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
            </dt>
            <dt class="pink-bg">
                <div class="follow">Follow indiatimes </div>
                <div class="follow_cont"> 
                    <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                    <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
                </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
                            <div id="OOP_Inter" style="display: none;">
                        <script type='text/javascript'>
                            googletag.cmd.push(function () {
                                googletag.display('OOP_Inter'); });
                        </script>
                    </div>
                    <div id="PPD" style="display: none;">
                        <script type='text/javascript'>
                            googletag.cmd.push(function () {
                                googletag.display('PPD');
                            });
                        </script>
                    </div>
            
    </div><!--pull-ad end-->
</div>
    <!--testing 16-03-25 23:50:02-->

<section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
    
<div class="big-image">
        	<div class="gradient-b"></div>
		<a href="http://www.indiatimes.com/news/world/online-rumors-suggest-isis-may-crucify-an-indian-priest-kidnapped-in-yemen-on-good-friday-252512.html" class="gradient-patten"></a>
            <div class="featureds"><span class="border-left">&nbsp;</span> Featured <span class="border-right">&nbsp;</span></div>
            <div class="bid-card-txt">
                                <a href="http://www.indiatimes.com/news/world/online-rumors-suggest-isis-may-crucify-an-indian-priest-kidnapped-in-yemen-on-good-friday-252512.html" class="big-card-small">
                    Online Rumors Suggest ISIS May Crucify An Indian Priest Kidnapped In Yemen, On Good Friday                </a>
              <span class="card-line"></span>  
            </div>
            <a href="http://www.indiatimes.com/news/world/online-rumors-suggest-isis-may-crucify-an-indian-priest-kidnapped-in-yemen-on-good-friday-252512.html" class="tint"><img src="http://media.indiatimes.in/media/content/2016/Mar/rtr3wbpt_1458887756_1024x477.jpg"/></a>
</div>
    
        

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

        <div class="feature-list cf"><!--feature-list start-->	
                            <figure>

                        <a href="http://www.indiatimes.com/news/india/disaster-strikes-indian-army-partol-hit-by-avalanche-in-leh-one-jawan-missing-252546.html" class=" tint" title="Disaster Strikes Indian Army, Partol Hit By Avalanche In Leh, One Jawan Missing">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458909292_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card_1458909292_236x111.jpg"  border="0" alt="Disaster Strikes Indian Army, Partol Hit By Avalanche In Leh, One Jawan Missing"/></a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/disaster-strikes-indian-army-partol-hit-by-avalanche-in-leh-one-jawan-missing-252546.html" title="Disaster Strikes Indian Army, Partol Hit By Avalanche In Leh, One Jawan Missing">
    Disaster Strikes Indian Army, Partol Hit By Avalanche In Leh, One Jawan Missing                    </a>
                </figcaption>
                </div><!--feature-list end-->

                    <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/india/after-being-raped-and-attacked-11-year-old-faces-boycott-from-villagers-252544.html" title="After Being Raped And Attacked, 11-Year-Old Faces Boycott From Villagers" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/girl6_1458908859_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/girl6_1458908859_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/after-being-raped-and-attacked-11-year-old-faces-boycott-from-villagers-252544.html" title="After Being Raped And Attacked, 11-Year-Old Faces Boycott From Villagers">
    After Being Raped And Attacked, 11-Year-Old Faces Boycott From Villagers                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/world/rare-sumatran-rhino-once-thought-extinct-has-been-found-for-the-first-time-in-40-years-252540.html" title="Rare Sumatran Rhino, Once Thought Extinct, Has Been Found For The First Time In 40 Years" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/rhino6_1458906873_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/rhino6_1458906873_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/rare-sumatran-rhino-once-thought-extinct-has-been-found-for-the-first-time-in-40-years-252540.html" title="Rare Sumatran Rhino, Once Thought Extinct, Has Been Found For The First Time In 40 Years">
    Rare Sumatran Rhino, Once Thought Extinct, Has Been Found For The First Time In 40 Years                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/india/shaktiman-s-misery-not-over-policemen-in-dehradun-forcefully-play-holi-with-the-poor-horse-252541.html" title="Shaktiman's Misery Not Over, Policemen In Dehradun Forcefully Play Holi With The Poor Horse" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/shak-fin_1458907401_1458907413_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/shak-fin_1458907401_1458907413_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/shaktiman-s-misery-not-over-policemen-in-dehradun-forcefully-play-holi-with-the-poor-horse-252541.html" title="Shaktiman's Misery Not Over, Policemen In Dehradun Forcefully Play Holi With The Poor Horse">
    Shaktiman's Misery Not Over, Policemen In Dehradun Forcefully Play Holi With The Poor Horse                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
            </div><!--news-panel end-->

    <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>

                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/another-international-celebrity-wears-designs-by-indian-duo-abu-jani-sandeep-khosla-252549.html" class="tint" title="Another International Celebrity Wears Designs By Indian Duo Abu Jani & Sandeep Khosla">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/abu-crd_1458916446_1458916450_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/abu-crd_1458916446_1458916450_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/another-international-celebrity-wears-designs-by-indian-duo-abu-jani-sandeep-khosla-252549.html" title="Another International Celebrity Wears Designs By Indian Duo Abu Jani & Sandeep Khosla" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/celebs/another-international-celebrity-wears-designs-by-indian-duo-abu-jani-sandeep-khosla-252549.html');">

    Another International Celebrity Wears Designs By Indian Duo Abu Jani & Sandeep Khosla                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/play/slog-overs/harsha-bhogle-responds-to-big-b-s-biased-commentator-tweet-says-one-has-to-be-objective-252548.html" class="tint" title="Harsha Bhogle Responds To Big B's âBiased Commentatorâ Tweet, Says One Has To Be Objective">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/hb640_1458912491_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/hb640_1458912491_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/harsha-bhogle-responds-to-big-b-s-biased-commentator-tweet-says-one-has-to-be-objective-252548.html" title="Harsha Bhogle Responds To Big B's âBiased Commentatorâ Tweet, Says One Has To Be Objective" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/play/slog-overs/harsha-bhogle-responds-to-big-b-s-biased-commentator-tweet-says-one-has-to-be-objective-252548.html');">

    Harsha Bhogle Responds To Big B's âBiased Commentatorâ Tweet, Says One Has To Be Objective                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/when-bejoy-nambiar-compared-lex-luthor-of-batman-vs-superman-to-srk-and-left-king-khan-curious-252547.html" class="tint" title="When Bejoy Nambiar Compared Lex Luthor Of 'Batman Vs Superman' To SRK And Left King Khan Curious!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/srk-car_1458912168_1458912174_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/srk-car_1458912168_1458912174_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/when-bejoy-nambiar-compared-lex-luthor-of-batman-vs-superman-to-srk-and-left-king-khan-curious-252547.html" title="When Bejoy Nambiar Compared Lex Luthor Of 'Batman Vs Superman' To SRK And Left King Khan Curious!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/bollywood/when-bejoy-nambiar-compared-lex-luthor-of-batman-vs-superman-to-srk-and-left-king-khan-curious-252547.html');">

    When Bejoy Nambiar Compared Lex Luthor Of 'Batman Vs Superman' To SRK And Left King Khan Curious!                        </a>
                    </div>
                </figcaption>
            </div>
                    
    </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
                    
                        <div class="trending-panel-list cf" id="column3_0">
    <span class="strip skyblue-bg"></span>                <figure>
                        <a href="http://www.indiatimes.com/entertainment/celebs/sunny-leone-slaps-reporter-who-asked-her-how-much-she-charges-for-night-programmes-252517.html" class="tint" title="Sunny Leone Slaps Reporter Who Asked Her How Much She Charges For 'Night Programmes'">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458890839_1458890842_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card_1458890839_1458890842_236x111.jpg" border="0" alt="Sunny Leone Slaps Reporter Who Asked Her How Much She Charges For 'Night Programmes'"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/sunny-leone-slaps-reporter-who-asked-her-how-much-she-charges-for-night-programmes-252517.html" title="Sunny Leone Slaps Reporter Who Asked Her How Much She Charges For 'Night Programmes' " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    Sunny Leone Slaps Reporter Who Asked Her How Much She Charges For 'Night Programmes'                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_1">
                    <figure>
                        <a href="http://www.indiatimes.com/play/slog-overs/poonam-pandey-back-with-small-gift-for-team-india-after-thrilling-win-over-bangladesh-252535.html" class="tint" title="Poonam Pandey Back With 'Small Gift' For Team India After Thrilling Win Over Bangladesh!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/indiapoonam640_1458900900_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/indiapoonam640_1458900900_236x111.jpg" border="0" alt="Poonam Pandey Back With 'Small Gift' For Team India After Thrilling Win Over Bangladesh!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/poonam-pandey-back-with-small-gift-for-team-india-after-thrilling-win-over-bangladesh-252535.html" title="Poonam Pandey Back With 'Small Gift' For Team India After Thrilling Win Over Bangladesh! " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    Poonam Pandey Back With 'Small Gift' For Team India After Thrilling Win Over Bangladesh!                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_2">
                    <figure>
                        <a href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-displays-his-sporting-spirit-consoles-mahmudullah-after-1-run-win-over-bangladesh-252537.html" class="tint" title="MS Dhoni Displays His Sporting Spirit, Consoles Mahmudullah After 1-Run Win Over Bangladesh">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/dm640_1458902155_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/dm640_1458902155_236x111.jpg" border="0" alt="MS Dhoni Displays His Sporting Spirit, Consoles Mahmudullah After 1-Run Win Over Bangladesh"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-displays-his-sporting-spirit-consoles-mahmudullah-after-1-run-win-over-bangladesh-252537.html" title="MS Dhoni Displays His Sporting Spirit, Consoles Mahmudullah After 1-Run Win Over Bangladesh " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    MS Dhoni Displays His Sporting Spirit, Consoles Mahmudullah After 1-Run Win Over Bangladesh                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_3">
                    <figure>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/watch-sad-ben-affleck-s-reaction-to-batman-vs-superman-reviews-will-make-you-love-him-even-more-252524.html" class="tint" title="Watch: Sad Ben Affleck's Reaction To 'Batman Vs Superman' Reviews Will Make You Love Him Even More!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/ben-card_1458888132_1458888137_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/ben-card_1458888132_1458888137_236x111.jpg" border="0" alt="Watch: Sad Ben Affleck's Reaction To 'Batman Vs Superman' Reviews Will Make You Love Him Even More!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/watch-sad-ben-affleck-s-reaction-to-batman-vs-superman-reviews-will-make-you-love-him-even-more-252524.html" title="Watch: Sad Ben Affleck's Reaction To 'Batman Vs Superman' Reviews Will Make You Love Him Even More! " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    Watch: Sad Ben Affleck's Reaction To 'Batman Vs Superman' Reviews Will Make You Love Him Even More!                    </a>
                   </div>     
                </figcaption>
            </div>
        </div><!--trending-panel end-->
</section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
    <div id="bigAd1_slot"></div>
    <script>
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

<section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>
                    <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/world/nasa-not-planning-permanent-mars-colony-wants-to-set-up-research-base-by-2030_-252538.html" title="NASA Not Planning Permanent Mars Colony, Wants To Set Up Research Base By 2030" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/mars600_1458905477_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/mars600_1458905477_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/nasa-not-planning-permanent-mars-colony-wants-to-set-up-research-base-by-2030_-252538.html" title="NASA Not Planning Permanent Mars Colony, Wants To Set Up Research Base By 2030">
    NASA Not Planning Permanent Mars Colony, Wants To Set Up Research Base By 2030                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-4"  data-slot="129061" data-position="4" data-section="0" data-cb="adwidgetNew"><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/search-sedition-and-anti-national-on-google-maps-and-you-will-be-shown-the-road-to-jnu-252536.html" title="Search 'Sedition' And 'Anti-National' On Google Maps And You Will Be Shown The Road To JNU" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/jnu6_1458900915_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/jnu6_1458900915_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/search-sedition-and-anti-national-on-google-maps-and-you-will-be-shown-the-road-to-jnu-252536.html" title="Search 'Sedition' And 'Anti-National' On Google Maps And You Will Be Shown The Road To JNU">
    Search 'Sedition' And 'Anti-National' On Google Maps And You Will Be Shown The Road To JNU                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/thanks-to-odd-even-arvind-kejriwal-only-indian-in-fortune-s-list-of-world-s-50-greatest-leaders-252531.html" title="Thanks To Odd Even Arvind Kejriwal Only Indian In Fortune's List Of World's 50 Greatest Leaders" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/kj-cr_1458896234_1458896240_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/kj-cr_1458896234_1458896240_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/thanks-to-odd-even-arvind-kejriwal-only-indian-in-fortune-s-list-of-world-s-50-greatest-leaders-252531.html" title="Thanks To Odd Even Arvind Kejriwal Only Indian In Fortune's List Of World's 50 Greatest Leaders">
    Thanks To Odd Even Arvind Kejriwal Only Indian In Fortune's List Of World's 50 Greatest Leaders                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/world/man-invents-algae-water-bottles-as-a-greener-alternative-to-plastic-to-save-the-environment-252528.html" title="Man Invents Algae Water Bottles As A Greener Alternative To Plastic To Save The Environment!" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/600_1458892808_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/600_1458892808_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/man-invents-algae-water-bottles-as-a-greener-alternative-to-plastic-to-save-the-environment-252528.html" title="Man Invents Algae Water Bottles As A Greener Alternative To Plastic To Save The Environment!">
    Man Invents Algae Water Bottles As A Greener Alternative To Plastic To Save The Environment!                    </a>
                </figcaption> 
            </div>
                </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
                        
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/a-missed-call-an-unread-fb-message-how-sayani-gupta-landed-a-role-opposite-srk-in-fan-252543.html" class="tint" title="A Missed Call & An Unread FB Message. How Sayani Gupta Landed A Role Opposite SRK In 'Fan'">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/fancr_1458909230_1458909234_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/fancr_1458909230_1458909234_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/a-missed-call-an-unread-fb-message-how-sayani-gupta-landed-a-role-opposite-srk-in-fan-252543.html" title="A Missed Call & An Unread FB Message. How Sayani Gupta Landed A Role Opposite SRK In 'Fan'">
    A Missed Call & An Unread FB Message. How Sayani Gupta Landed A Role Opposite SRK In 'Fan'                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/play/slog-overs/sorry-girls-hardik-pandya-india-s-hero-against-bangladesh-is-already-taken-252542.html" class="tint" title="Sorry Girls, Hardik Pandya, India's Hero Against Bangladesh Is Already Taken">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/hardikmodel640_1458907849_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/hardikmodel640_1458907849_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/sorry-girls-hardik-pandya-india-s-hero-against-bangladesh-is-already-taken-252542.html" title="Sorry Girls, Hardik Pandya, India's Hero Against Bangladesh Is Already Taken">
    Sorry Girls, Hardik Pandya, India's Hero Against Bangladesh Is Already Taken                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/play/slog-overs/pakistan-crash-out-of-world-t20-india-now-have-to-beat-australia-to-enter-semis-252545.html" class="tint" title="Pakistan Crash Out Of World T20, India Now Have To Beat Australia To Enter Semis">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/india640_1458909493_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/india640_1458909493_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/pakistan-crash-out-of-world-t20-india-now-have-to-beat-australia-to-enter-semis-252545.html" title="Pakistan Crash Out Of World T20, India Now Have To Beat Australia To Enter Semis">
    Pakistan Crash Out Of World T20, India Now Have To Beat Australia To Enter Semis                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/entertainment/celebs/another-couple-calls-it-quits-sushant-singh-rajput-splits-with-long-time-girlfriend-ankita-lokhande-252525.html" class="tint" title="Another Couple Calls It Quits! Sushant Singh Rajput Splits With Long-Time Girlfriend Ankita Lokhande">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/sus-cr_1458893282_1458893286_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/sus-cr_1458893282_1458893286_236x111.jpg" border="0" alt="Another Couple Calls It Quits! Sushant Singh Rajput Splits With Long-Time Girlfriend Ankita Lokhande"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/another-couple-calls-it-quits-sushant-singh-rajput-splits-with-long-time-girlfriend-ankita-lokhande-252525.html" title="Another Couple Calls It Quits! Sushant Singh Rajput Splits With Long-Time Girlfriend Ankita Lokhande">
            Another Couple Calls It Quits! Sushant Singh Rajput Splits With Long-Time Girlfriend Ankita Lokhande                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf colombia"  id="div-clmb-ctn-129061-2"  data-slot="129061" data-position="2" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/play/slog-overs/this-video-of-virat-kohli-asking-fans-to-chant-india-instead-of-his-name-is-total-class-252496.html" class="tint" title="This Video Of Virat Kohli Asking Fans To Chant 'India' Instead Of His Name Is Total Class!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458808081_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458808081_236x111.jpg" border="0" alt="This Video Of Virat Kohli Asking Fans To Chant 'India' Instead Of His Name Is Total Class!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/this-video-of-virat-kohli-asking-fans-to-chant-india-instead-of-his-name-is-total-class-252496.html" title="This Video Of Virat Kohli Asking Fans To Chant 'India' Instead Of His Name Is Total Class!">
            This Video Of Virat Kohli Asking Fans To Chant 'India' Instead Of His Name Is Total Class!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-calmly-shuts-down-a-journalist-for-trying-to-belittle-india-s-win-against-bangladesh-252487.html" class="tint" title="MS Dhoni Calmly Shuts Down A Journalist For Trying To Belittle India's Win Against Bangladesh!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/dhoni_1458795990_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dhoni_1458795990_236x111.jpg" border="0" alt="MS Dhoni Calmly Shuts Down A Journalist For Trying To Belittle India's Win Against Bangladesh!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-calmly-shuts-down-a-journalist-for-trying-to-belittle-india-s-win-against-bangladesh-252487.html" title="MS Dhoni Calmly Shuts Down A Journalist For Trying To Belittle India's Win Against Bangladesh!">
            MS Dhoni Calmly Shuts Down A Journalist For Trying To Belittle India's Win Against Bangladesh!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/lifestyle/self/15-of-the-oldest-trees-in-the-world-captured-in-all-their-timeless-beauty-252238.html" class="tint" title="15 Of The Oldest Trees In The World, Captured In All Their Timeless Beauty">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/1card_1458304588_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/1card_1458304588_236x111.jpg" border="0" alt="15 Of The Oldest Trees In The World, Captured In All Their Timeless Beauty"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/15-of-the-oldest-trees-in-the-world-captured-in-all-their-timeless-beauty-252238.html" title="15 Of The Oldest Trees In The World, Captured In All Their Timeless Beauty">
            15 Of The Oldest Trees In The World, Captured In All Their Timeless Beauty                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div>

</section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
    <div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

<section id="hp_block_3" class="container cf"><!--container start-->

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/world/disaster-averted-man-arrested-in-advanced-stages-of-plotting-another-paris-attack-252534.html" title="Disaster Averted, Man Arrested In 'Advanced Stages' Of Plotting Another Paris Attack" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458899601_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458899601_236x111.jpg" border="0" alt="Disaster Averted, Man Arrested In 'Advanced Stages' Of Plotting Another Paris Attack"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/disaster-averted-man-arrested-in-advanced-stages-of-plotting-another-paris-attack-252534.html" title="Disaster Averted, Man Arrested In 'Advanced Stages' Of Plotting Another Paris Attack">
            Disaster Averted, Man Arrested In 'Advanced Stages' Of Plotting Another Paris Attack                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-8"  data-slot="129061" data-position="8" data-section="0" data-cb="adwidgetNew">

                <figure>

                    <a href="http://www.indiatimes.com/news/world/even-as-the-world-fights-in-name-of-religion-pope-francis-kisses-feet-of-muslims-refugees-252529.html" title="Even As The World Fights In Name Of Religion, Pope Francis Kisses Feet Of Muslims Refugees" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1458893971_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1458893971_236x111.jpg" border="0" alt="Even As The World Fights In Name Of Religion, Pope Francis Kisses Feet Of Muslims Refugees"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/even-as-the-world-fights-in-name-of-religion-pope-francis-kisses-feet-of-muslims-refugees-252529.html" title="Even As The World Fights In Name Of Religion, Pope Francis Kisses Feet Of Muslims Refugees">
            Even As The World Fights In Name Of Religion, Pope Francis Kisses Feet Of Muslims Refugees                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/american-parents-offended-by-yoga-get-namaste-banned-in-primary-school-252527.html" title="American Parents Offended By Yoga! Get 'Namaste' Banned In Primary School" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1458892359_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1458892359_236x111.jpg" border="0" alt="American Parents Offended By Yoga! Get 'Namaste' Banned In Primary School"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/american-parents-offended-by-yoga-get-namaste-banned-in-primary-school-252527.html" title="American Parents Offended By Yoga! Get 'Namaste' Banned In Primary School">
            American Parents Offended By Yoga! Get 'Namaste' Banned In Primary School                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/world/after-paris-and-brussels-isis-has-reportedly-trained-400-fighters-to-attack-europe-252530.html" title="After Paris And Brussels, ISIS Has Reportedly Trained 400 Fighters To Attack Europe" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458894768_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458894768_236x111.jpg" border="0" alt="After Paris And Brussels, ISIS Has Reportedly Trained 400 Fighters To Attack Europe"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/after-paris-and-brussels-isis-has-reportedly-trained-400-fighters-to-attack-europe-252530.html" title="After Paris And Brussels, ISIS Has Reportedly Trained 400 Fighters To Attack Europe">
            After Paris And Brussels, ISIS Has Reportedly Trained 400 Fighters To Attack Europe                    </a>
                </figcaption> 
            </div>
        </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
         
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/9-historical-prisons-that-have-the-most-chilling-stories-behind-them-252101.html" class="tint" title="9 Historical Prisons That Have The Most Chilling Stories Behind Them">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/2_1458124513_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/2_1458124513_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/travel/9-historical-prisons-that-have-the-most-chilling-stories-behind-them-252101.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/9-historical-prisons-that-have-the-most-chilling-stories-behind-them-252101.html" title="9 Historical Prisons That Have The Most Chilling Stories Behind Them">
            9 Historical Prisons That Have The Most Chilling Stories Behind Them                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-in-victoria-s-secret-what-is-sexy-list-gets-the-sexiest-eyes-tag-252539.html" class="tint" title="Priyanka Chopra In Victoria's Secret 'What Is Sexy' List, Gets The Sexiest Eyes Tag">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/7519_1458905647_1458905652_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/7519_1458905647_1458905652_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-in-victoria-s-secret-what-is-sexy-list-gets-the-sexiest-eyes-tag-252539.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-in-victoria-s-secret-what-is-sexy-list-gets-the-sexiest-eyes-tag-252539.html" title="Priyanka Chopra In Victoria's Secret 'What Is Sexy' List, Gets The Sexiest Eyes Tag">
            Priyanka Chopra In Victoria's Secret 'What Is Sexy' List, Gets The Sexiest Eyes Tag                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-displays-his-sporting-spirit-consoles-mahmudullah-after-1-run-win-over-bangladesh-252537.html" class="tint" title="MS Dhoni Displays His Sporting Spirit, Consoles Mahmudullah After 1-Run Win Over Bangladesh">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/dm640_1458902155_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dm640_1458902155_502x234.jpg" border="0" alt="http://www.indiatimes.com/play/slog-overs/ms-dhoni-displays-his-sporting-spirit-consoles-mahmudullah-after-1-run-win-over-bangladesh-252537.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-displays-his-sporting-spirit-consoles-mahmudullah-after-1-run-win-over-bangladesh-252537.html" title="MS Dhoni Displays His Sporting Spirit, Consoles Mahmudullah After 1-Run Win Over Bangladesh">
            MS Dhoni Displays His Sporting Spirit, Consoles Mahmudullah After 1-Run Win Over Bangladesh                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/play/slog-overs/redditors-turn-cricket-banter-into-an-obsession-about-dhoni-s-wicket-taking-hands-and-it-is-hilarious-252510.html" class="tint" title="Redditors Turn Cricket Banter Into An Obsession About Dhoni's Wicket-Taking Hands And It Is Hilarious">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458829706_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458829706_236x111.jpg" border="0" alt="Redditors Turn Cricket Banter Into An Obsession About Dhoni's Wicket-Taking Hands And It Is Hilarious"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/redditors-turn-cricket-banter-into-an-obsession-about-dhoni-s-wicket-taking-hands-and-it-is-hilarious-252510.html" title="Redditors Turn Cricket Banter Into An Obsession About Dhoni's Wicket-Taking Hands And It Is Hilarious">
    Redditors Turn Cricket Banter Into An Obsession About Dhoni's Wicket-Taking Hands And It Is Hilarious                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-6"  data-slot="129061" data-position="6" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/play/slog-overs/8-things-players-said-during-hardik-pandya-s-match-winning-final-over-vs-bangladesh-252497.html" class="tint" title="All The Advice Hardik Pandya Got And Other Things Players Said In The Final Over Vs Bangladesh">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/dhonipandyabanafp_1458807656_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dhonipandyabanafp_1458807656_236x111.jpg" border="0" alt="All The Advice Hardik Pandya Got And Other Things Players Said In The Final Over Vs Bangladesh"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/8-things-players-said-during-hardik-pandya-s-match-winning-final-over-vs-bangladesh-252497.html" title="All The Advice Hardik Pandya Got And Other Things Players Said In The Final Over Vs Bangladesh">
    All The Advice Hardik Pandya Got And Other Things Players Said In The Final Over Vs Bangladesh                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/play/slog-overs/harsha-bhogle-s-post-after-the-india-bangladesh-thriller-sums-up-what-a-true-cricket-fan-felt-252506.html" class="tint" title="Harsha Bhogle's Post After The India-Bangladesh Thriller Sums Up What A True Cricket Fan Felt">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/harshapost_1458820748_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/harshapost_1458820748_236x111.jpg" border="0" alt="Harsha Bhogle's Post After The India-Bangladesh Thriller Sums Up What A True Cricket Fan Felt"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/harsha-bhogle-s-post-after-the-india-bangladesh-thriller-sums-up-what-a-true-cricket-fan-felt-252506.html" title="Harsha Bhogle's Post After The India-Bangladesh Thriller Sums Up What A True Cricket Fan Felt">
    Harsha Bhogle's Post After The India-Bangladesh Thriller Sums Up What A True Cricket Fan Felt                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/play/slog-overs/here-s-how-cricketers-celebrated-the-festival-of-colours-all-over-india-252509.html" class="tint" title="Here's How Cricketers Celebrated The Festival Of Colours All Over India">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/holicricket_1458826042_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/holicricket_1458826042_236x111.jpg" border="0" alt="Here's How Cricketers Celebrated The Festival Of Colours All Over India"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/here-s-how-cricketers-celebrated-the-festival-of-colours-all-over-india-252509.html" title="Here's How Cricketers Celebrated The Festival Of Colours All Over India">
    Here's How Cricketers Celebrated The Festival Of Colours All Over India                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div><!--trending-panel end-->

</section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
    <div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

<section class="container cf" id="container4"><!--container start-->
    <div class="news-panel cf ">
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/world/here-s-the-reason-why-time-in-watch-advertisements-is-always-set-to-10-10_-252523.html" title="Here's The Reason Why Time In Watch Advertisements Is Always Set To 10:10!" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/clock6_1458887929_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/clock6_1458887929_236x111.jpg" border="0" alt="Here's The Reason Why Time In Watch Advertisements Is Always Set To 10:10!"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/here-s-the-reason-why-time-in-watch-advertisements-is-always-set-to-10-10_-252523.html" title="Here's The Reason Why Time In Watch Advertisements Is Always Set To 10:10!">
        Here's The Reason Why Time In Watch Advertisements Is Always Set To 10:10!                    </a>
                </figcaption> 
            </div>
                <div class='container1'>            <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-12"  data-slot="129061" data-position="12" data-section="0" data-cb="adwidgetNew">
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/known-for-its-perfect-design-taj-mahal-is-not-really-symmetric-say-bengaluru-researchers-252526.html" title="Known For Its Perfect Design, Taj Mahal Is Not Really Symmetric Say Bengaluru Researchers" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458890725_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458890725_236x111.jpg" border="0" alt="Known For Its Perfect Design, Taj Mahal Is Not Really Symmetric Say Bengaluru Researchers"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/known-for-its-perfect-design-taj-mahal-is-not-really-symmetric-say-bengaluru-researchers-252526.html" title="Known For Its Perfect Design, Taj Mahal Is Not Really Symmetric Say Bengaluru Researchers">
        Known For Its Perfect Design, Taj Mahal Is Not Really Symmetric Say Bengaluru Researchers                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/students-pay-the-price-for-saying-jai-pakistan-on-whatsapp-get-pulled-up-by-police-252519.html" title="Students Pay The Price For Saying 'Jai Pakistan' On WhatsApp, Get Pulled Up By Police!" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/wf640_1458887770_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/wf640_1458887770_236x111.jpg" border="0" alt="Students Pay The Price For Saying 'Jai Pakistan' On WhatsApp, Get Pulled Up By Police!"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/students-pay-the-price-for-saying-jai-pakistan-on-whatsapp-get-pulled-up-by-police-252519.html" title="Students Pay The Price For Saying 'Jai Pakistan' On WhatsApp, Get Pulled Up By Police!">
        Students Pay The Price For Saying 'Jai Pakistan' On WhatsApp, Get Pulled Up By Police!                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/superpower-india-has-the-slowest-internet-speed-in-asia-stands-at-115th-in-the-global-list-252522.html" title="Superpower India Has The Slowest Internet Speed In Asia, Stands At 115th In The Global List!" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/snailonlaptop_1458887013_1458887026_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/snailonlaptop_1458887013_1458887026_236x111.jpg" border="0" alt="Superpower India Has The Slowest Internet Speed In Asia, Stands At 115th In The Global List!"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/superpower-india-has-the-slowest-internet-speed-in-asia-stands-at-115th-in-the-global-list-252522.html" title="Superpower India Has The Slowest Internet Speed In Asia, Stands At 115th In The Global List!">
        Superpower India Has The Slowest Internet Speed In Asia, Stands At 115th In The Global List!                    </a>
                </figcaption> 
            </div>
                 
    </div>
</div><!--news-panel end-->

<div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
        
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/poonam-pandey-back-with-small-gift-for-team-india-after-thrilling-win-over-bangladesh-252535.html" class="tint" title="Poonam Pandey Back With 'Small Gift' For Team India After Thrilling Win Over Bangladesh!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/indiapoonam640_1458900900_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/indiapoonam640_1458900900_502x234.jpg" border="0" alt="Poonam Pandey Back With 'Small Gift' For Team India After Thrilling Win Over Bangladesh!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/poonam-pandey-back-with-small-gift-for-team-india-after-thrilling-win-over-bangladesh-252535.html" title="Poonam Pandey Back With 'Small Gift' For Team India After Thrilling Win Over Bangladesh!">
    Poonam Pandey Back With 'Small Gift' For Team India After Thrilling Win Over Bangladesh!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/ranbir-kapoor-reportedly-visited-ex-flame-deepika-padukone-on-the-sets-of-xxx-252532.html" class="tint" title="Ranbir Kapoor Reportedly Visited Ex-Flame Deepika Padukone On The Sets Of 'xXx'">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cr-d_1458898272_1458898276_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cr-d_1458898272_1458898276_502x234.jpg" border="0" alt="Ranbir Kapoor Reportedly Visited Ex-Flame Deepika Padukone On The Sets Of 'xXx'" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/ranbir-kapoor-reportedly-visited-ex-flame-deepika-padukone-on-the-sets-of-xxx-252532.html" title="Ranbir Kapoor Reportedly Visited Ex-Flame Deepika Padukone On The Sets Of 'xXx'">
    Ranbir Kapoor Reportedly Visited Ex-Flame Deepika Padukone On The Sets Of 'xXx'                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/this-guy-made-3-500-birdhouses-from-scrap-wood-just-so-that-birds-would-have-a-place-to-live-252175.html" class="tint" title="This Guy Made 3,500 Birdhouses From Scrap Wood Just So That Birds Would Have A Place To Live">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage-2_1458216463_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage-2_1458216463_502x234.jpg" border="0" alt="This Guy Made 3,500 Birdhouses From Scrap Wood Just So That Birds Would Have A Place To Live" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/this-guy-made-3-500-birdhouses-from-scrap-wood-just-so-that-birds-would-have-a-place-to-live-252175.html" title="This Guy Made 3,500 Birdhouses From Scrap Wood Just So That Birds Would Have A Place To Live">
    This Guy Made 3,500 Birdhouses From Scrap Wood Just So That Birds Would Have A Place To Live                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/another-couple-calls-it-quits-sushant-singh-rajput-splits-with-long-time-girlfriend-ankita-lokhande-252525.html" class="tint" title="Another Couple Calls It Quits! Sushant Singh Rajput Splits With Long-Time Girlfriend Ankita Lokhande">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/sus-cr_1458893282_1458893286_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/sus-cr_1458893282_1458893286_502x234.jpg" border="0" alt="Another Couple Calls It Quits! Sushant Singh Rajput Splits With Long-Time Girlfriend Ankita Lokhande" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/another-couple-calls-it-quits-sushant-singh-rajput-splits-with-long-time-girlfriend-ankita-lokhande-252525.html" title="Another Couple Calls It Quits! Sushant Singh Rajput Splits With Long-Time Girlfriend Ankita Lokhande">
    Another Couple Calls It Quits! Sushant Singh Rajput Splits With Long-Time Girlfriend Ankita Lokhande                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/shahid-afridi-pakistan-s-never-ageing-maverick-all-rounder-who-always-courted-controversy-252521.html" class="tint" title="Shahid Afridi, Pakistan's 'Never-Ageing' Maverick All-Rounder Who Always Courted Controversy">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/shahid1640_1458886667_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/shahid1640_1458886667_502x234.jpg" border="0" alt="Shahid Afridi, Pakistan's 'Never-Ageing' Maverick All-Rounder Who Always Courted Controversy" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/shahid-afridi-pakistan-s-never-ageing-maverick-all-rounder-who-always-courted-controversy-252521.html" title="Shahid Afridi, Pakistan's 'Never-Ageing' Maverick All-Rounder Who Always Courted Controversy">
    Shahid Afridi, Pakistan's 'Never-Ageing' Maverick All-Rounder Who Always Courted Controversy                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/sunny-leone-slaps-reporter-who-asked-her-how-much-she-charges-for-night-programmes-252517.html" class="tint" title="Sunny Leone Slaps Reporter Who Asked Her How Much She Charges For 'Night Programmes'">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458890839_1458890842_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458890839_1458890842_502x234.jpg" border="0" alt="Sunny Leone Slaps Reporter Who Asked Her How Much She Charges For 'Night Programmes'" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/sunny-leone-slaps-reporter-who-asked-her-how-much-she-charges-for-night-programmes-252517.html" title="Sunny Leone Slaps Reporter Who Asked Her How Much She Charges For 'Night Programmes'">
    Sunny Leone Slaps Reporter Who Asked Her How Much She Charges For 'Night Programmes'                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/watch-sad-ben-affleck-s-reaction-to-batman-vs-superman-reviews-will-make-you-love-him-even-more-252524.html" class="tint" title="Watch: Sad Ben Affleck's Reaction To 'Batman Vs Superman' Reviews Will Make You Love Him Even More!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/ben-card_1458888132_1458888137_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ben-card_1458888132_1458888137_502x234.jpg" border="0" alt="Watch: Sad Ben Affleck's Reaction To 'Batman Vs Superman' Reviews Will Make You Love Him Even More!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/watch-sad-ben-affleck-s-reaction-to-batman-vs-superman-reviews-will-make-you-love-him-even-more-252524.html" title="Watch: Sad Ben Affleck's Reaction To 'Batman Vs Superman' Reviews Will Make You Love Him Even More!">
    Watch: Sad Ben Affleck's Reaction To 'Batman Vs Superman' Reviews Will Make You Love Him Even More!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/gin-tonic-was-introduced-by-the-british-in-india-to-prevent-malaria-yes-really-252188.html" class="tint" title="Gin & Tonic Was Introduced By The British In India To Prevent Malaria. Yes, Really">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458222245_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458222245_502x234.jpg" border="0" alt="Gin & Tonic Was Introduced By The British In India To Prevent Malaria. Yes, Really" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/gin-tonic-was-introduced-by-the-british-in-india-to-prevent-malaria-yes-really-252188.html" title="Gin & Tonic Was Introduced By The British In India To Prevent Malaria. Yes, Really">
    Gin & Tonic Was Introduced By The British In India To Prevent Malaria. Yes, Really                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/kick-off/uefa-to-investigate-illicit-chants-on-the-1958-munich-air-crash-during-manchester-united-liverpool-europa-league-encounter-252514.html" class="tint" title="UEFA To Investigate Illicit Chants On The 1958 Munich Air Crash During Manchester United-Liverpool Encounter">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/lm640_1458883414_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/lm640_1458883414_502x234.jpg" border="0" alt="UEFA To Investigate Illicit Chants On The 1958 Munich Air Crash During Manchester United-Liverpool Encounter" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/kick-off/uefa-to-investigate-illicit-chants-on-the-1958-munich-air-crash-during-manchester-united-liverpool-europa-league-encounter-252514.html" title="UEFA To Investigate Illicit Chants On The 1958 Munich Air Crash During Manchester United-Liverpool Encounter">
    UEFA To Investigate Illicit Chants On The 1958 Munich Air Crash During Manchester United-Liverpool Encounter                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/former-openers-sunil-gavaskar-and-virender-sehwag-hail-virat-kohli-as-the-new-sachin-tendulkar-252513.html" class="tint" title="Former Openers Sunil Gavaskar And Virender Sehwag Hail Virat Kohli As The New Sachin Tendulkar">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/sachnivirat640_1458881223_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/sachnivirat640_1458881223_502x234.jpg" border="0" alt="Former Openers Sunil Gavaskar And Virender Sehwag Hail Virat Kohli As The New Sachin Tendulkar" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/former-openers-sunil-gavaskar-and-virender-sehwag-hail-virat-kohli-as-the-new-sachin-tendulkar-252513.html" title="Former Openers Sunil Gavaskar And Virender Sehwag Hail Virat Kohli As The New Sachin Tendulkar">
    Former Openers Sunil Gavaskar And Virender Sehwag Hail Virat Kohli As The New Sachin Tendulkar                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/20-reminders-that-prove-it-s-okay-to-be-directionless-in-your-20s_-252088.html" class="tint" title="20 Reminders That Prove It's Okay To Be Directionless In Your 20s">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/photo-1453825517242-1a1527bf0a39_1458117789_1458117796_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/photo-1453825517242-1a1527bf0a39_1458117789_1458117796_502x234.jpg" border="0" alt="20 Reminders That Prove It's Okay To Be Directionless In Your 20s" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/20-reminders-that-prove-it-s-okay-to-be-directionless-in-your-20s_-252088.html" title="20 Reminders That Prove It's Okay To Be Directionless In Your 20s">
    20 Reminders That Prove It's Okay To Be Directionless In Your 20s                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/barack-obama-and-michelle-obama-broke-into-the-tango-today-in-argentina-and-it-was-almost-as-good-as-dirty-dancing-252511.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/videocafe/barack-obama-and-michelle-obama-broke-into-the-tango-today-in-argentina-and-it-was-almost-as-good-as-dirty-dancing-252511.html" class="tint" title="Barack Obama And Michelle Obama Broke Into The Tango Today In Argentina And It Was Almost As Good As Dirty Dancing">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/obams_1458838956_1458838965_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/obams_1458838956_1458838965_502x234.jpg" border="0" alt="Barack Obama And Michelle Obama Broke Into The Tango Today In Argentina And It Was Almost As Good As Dirty Dancing" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/videocafe/barack-obama-and-michelle-obama-broke-into-the-tango-today-in-argentina-and-it-was-almost-as-good-as-dirty-dancing-252511.html" title="Barack Obama And Michelle Obama Broke Into The Tango Today In Argentina And It Was Almost As Good As Dirty Dancing">
    Barack Obama And Michelle Obama Broke Into The Tango Today In Argentina And It Was Almost As Good As Dirty Dancing                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/redditors-turn-cricket-banter-into-an-obsession-about-dhoni-s-wicket-taking-hands-and-it-is-hilarious-252510.html" class="tint" title="Redditors Turn Cricket Banter Into An Obsession About Dhoni's Wicket-Taking Hands And It Is Hilarious">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458829706_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458829706_502x234.jpg" border="0" alt="Redditors Turn Cricket Banter Into An Obsession About Dhoni's Wicket-Taking Hands And It Is Hilarious" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/redditors-turn-cricket-banter-into-an-obsession-about-dhoni-s-wicket-taking-hands-and-it-is-hilarious-252510.html" title="Redditors Turn Cricket Banter Into An Obsession About Dhoni's Wicket-Taking Hands And It Is Hilarious">
    Redditors Turn Cricket Banter Into An Obsession About Dhoni's Wicket-Taking Hands And It Is Hilarious                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/meet-norman-borlaug-india-s-annadaata-who-saved-the-lives-of-billions-in-famine-and-starvation-252484.html" class="tint" title="Meet Norman Borlaug - Indiaâs Annadaata Who Saved The Lives Of Billions In Famine And Starvation">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/nb_1458742734_1458742744_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/nb_1458742734_1458742744_502x234.jpg" border="0" alt="Meet Norman Borlaug - Indiaâs Annadaata Who Saved The Lives Of Billions In Famine And Starvation" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/meet-norman-borlaug-india-s-annadaata-who-saved-the-lives-of-billions-in-famine-and-starvation-252484.html" title="Meet Norman Borlaug - Indiaâs Annadaata Who Saved The Lives Of Billions In Famine And Starvation">
    Meet Norman Borlaug - Indiaâs Annadaata Who Saved The Lives Of Billions In Famine And Starvation                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/8-hard-hitting-facts-that-prove-air-pollution-is-slowly-killing-us-all-252230.html" class="tint" title="8 Hard-Hitting Facts That Prove Air Pollution Is Slowly Killing Us All!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover_1458302871_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1458302871_502x234.jpg" border="0" alt="8 Hard-Hitting Facts That Prove Air Pollution Is Slowly Killing Us All!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/8-hard-hitting-facts-that-prove-air-pollution-is-slowly-killing-us-all-252230.html" title="8 Hard-Hitting Facts That Prove Air Pollution Is Slowly Killing Us All!">
    8 Hard-Hitting Facts That Prove Air Pollution Is Slowly Killing Us All!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/21-ways-you-can-kill-your-hangover-depending-on-where-in-the-world-you-are-252282.html" class="tint" title="21 Ways You Can Kill Your Hangover, Depending On Where In The World You Are">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458395647_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458395647_502x234.jpg" border="0" alt="21 Ways You Can Kill Your Hangover, Depending On Where In The World You Are" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/21-ways-you-can-kill-your-hangover-depending-on-where-in-the-world-you-are-252282.html" title="21 Ways You Can Kill Your Hangover, Depending On Where In The World You Are">
    21 Ways You Can Kill Your Hangover, Depending On Where In The World You Are                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/sanjay-dutt-seems-to-be-enjoying-his-freedom-takes-a-late-night-ride-in-an-auto-rickshaw-252505.html" class="tint" title="Sanjay Dutt Seems To Be Enjoying His Freedom, Takes A Late Night Ride In An Auto Rickshaw!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/690358_1458818156_1458818168_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/690358_1458818156_1458818168_502x234.jpg" border="0" alt="Sanjay Dutt Seems To Be Enjoying His Freedom, Takes A Late Night Ride In An Auto Rickshaw!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/sanjay-dutt-seems-to-be-enjoying-his-freedom-takes-a-late-night-ride-in-an-auto-rickshaw-252505.html" title="Sanjay Dutt Seems To Be Enjoying His Freedom, Takes A Late Night Ride In An Auto Rickshaw!">
    Sanjay Dutt Seems To Be Enjoying His Freedom, Takes A Late Night Ride In An Auto Rickshaw!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/harsha-bhogle-s-post-after-the-india-bangladesh-thriller-sums-up-what-a-true-cricket-fan-felt-252506.html" class="tint" title="Harsha Bhogle's Post After The India-Bangladesh Thriller Sums Up What A True Cricket Fan Felt">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/harshapost_1458820748_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/harshapost_1458820748_502x234.jpg" border="0" alt="Harsha Bhogle's Post After The India-Bangladesh Thriller Sums Up What A True Cricket Fan Felt" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/harsha-bhogle-s-post-after-the-india-bangladesh-thriller-sums-up-what-a-true-cricket-fan-felt-252506.html" title="Harsha Bhogle's Post After The India-Bangladesh Thriller Sums Up What A True Cricket Fan Felt">
    Harsha Bhogle's Post After The India-Bangladesh Thriller Sums Up What A True Cricket Fan Felt                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-hilarious-parody-of-tujh-mein-rab-dikhta-hai-is-dedicated-to-all-self-obsessed-people-251353.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/videocafe/this-hilarious-parody-of-tujh-mein-rab-dikhta-hai-is-dedicated-to-all-self-obsessed-people-251353.html" class="tint" title="This Hilarious Parody Of 'Tujh Mein Rab Dikhta Hai' Is Dedicated To All Self Obsessed People!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/tujhmerinrabdikhtahai_prody_card_1456816882_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/tujhmerinrabdikhtahai_prody_card_1456816882_502x234.jpg" border="0" alt="This Hilarious Parody Of 'Tujh Mein Rab Dikhta Hai' Is Dedicated To All Self Obsessed People!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/videocafe/this-hilarious-parody-of-tujh-mein-rab-dikhta-hai-is-dedicated-to-all-self-obsessed-people-251353.html" title="This Hilarious Parody Of 'Tujh Mein Rab Dikhta Hai' Is Dedicated To All Self Obsessed People!">
    This Hilarious Parody Of 'Tujh Mein Rab Dikhta Hai' Is Dedicated To All Self Obsessed People!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-fans-of-sanjay-dutt-will-tell-you-why-we-should-celebrate-his-release-and-it-s-hilarious-251222.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/videocafe/these-fans-of-sanjay-dutt-will-tell-you-why-we-should-celebrate-his-release-and-it-s-hilarious-251222.html" class="tint" title="These Fans Of Sanjay Dutt Will Tell You Why We Should Celebrate His Release And It's Hilarious!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Feb/sanjubaba_card_1456476151_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Feb/sanjubaba_card_1456476151_502x234.jpg" border="0" alt="These Fans Of Sanjay Dutt Will Tell You Why We Should Celebrate His Release And It's Hilarious!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/videocafe/these-fans-of-sanjay-dutt-will-tell-you-why-we-should-celebrate-his-release-and-it-s-hilarious-251222.html" title="These Fans Of Sanjay Dutt Will Tell You Why We Should Celebrate His Release And It's Hilarious!">
    These Fans Of Sanjay Dutt Will Tell You Why We Should Celebrate His Release And It's Hilarious!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/here-s-how-cricketers-celebrated-the-festival-of-colours-all-over-india-252509.html" class="tint" title="Here's How Cricketers Celebrated The Festival Of Colours All Over India">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/holicricket_1458826042_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/holicricket_1458826042_502x234.jpg" border="0" alt="Here's How Cricketers Celebrated The Festival Of Colours All Over India" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/here-s-how-cricketers-celebrated-the-festival-of-colours-all-over-india-252509.html" title="Here's How Cricketers Celebrated The Festival Of Colours All Over India">
    Here's How Cricketers Celebrated The Festival Of Colours All Over India                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-among-time-probables-for-100-most-influential-people-along-with-pm-modi-252502.html" class="tint" title="Priyanka Chopra Among Time Probables For 100 Most Influential People, Along With PM Modi">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/beautiful-priyanka-chopra-2015-hd-wallpaper_1458815440_1458815446_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/beautiful-priyanka-chopra-2015-hd-wallpaper_1458815440_1458815446_502x234.jpg" border="0" alt="Priyanka Chopra Among Time Probables For 100 Most Influential People, Along With PM Modi" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-among-time-probables-for-100-most-influential-people-along-with-pm-modi-252502.html" title="Priyanka Chopra Among Time Probables For 100 Most Influential People, Along With PM Modi">
    Priyanka Chopra Among Time Probables For 100 Most Influential People, Along With PM Modi                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/15-of-the-oldest-trees-in-the-world-captured-in-all-their-timeless-beauty-252238.html" class="tint" title="15 Of The Oldest Trees In The World, Captured In All Their Timeless Beauty">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/1card_1458304588_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/1card_1458304588_502x234.jpg" border="0" alt="15 Of The Oldest Trees In The World, Captured In All Their Timeless Beauty" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/15-of-the-oldest-trees-in-the-world-captured-in-all-their-timeless-beauty-252238.html" title="15 Of The Oldest Trees In The World, Captured In All Their Timeless Beauty">
    15 Of The Oldest Trees In The World, Captured In All Their Timeless Beauty                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/sonakshi-sinha-s-latest-dubsmash-is-the-cutest-things-you-ll-see-today-252498.html" class="tint" title="Sonakshi Sinha's Latest Dubsmash Is The Cutest Thing You'll See Today!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458809205_1458809211_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458809205_1458809211_502x234.jpg" border="0" alt="Sonakshi Sinha's Latest Dubsmash Is The Cutest Thing You'll See Today!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/sonakshi-sinha-s-latest-dubsmash-is-the-cutest-things-you-ll-see-today-252498.html" title="Sonakshi Sinha's Latest Dubsmash Is The Cutest Thing You'll See Today!">
    Sonakshi Sinha's Latest Dubsmash Is The Cutest Thing You'll See Today!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/here-s-proof-that-ranveer-singh-is-in-sri-lanka-by-deepika-padukone-s-side-252495.html" class="tint" title="Here's Proof That Ranveer Singh Is In Sri Lanka, By Deepika Padukone's Side!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cepla7pviaam9hj_1458807491_1458807496_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cepla7pviaam9hj_1458807491_1458807496_502x234.jpg" border="0" alt="Here's Proof That Ranveer Singh Is In Sri Lanka, By Deepika Padukone's Side!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/here-s-proof-that-ranveer-singh-is-in-sri-lanka-by-deepika-padukone-s-side-252495.html" title="Here's Proof That Ranveer Singh Is In Sri Lanka, By Deepika Padukone's Side!">
    Here's Proof That Ranveer Singh Is In Sri Lanka, By Deepika Padukone's Side!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/this-video-of-virat-kohli-asking-fans-to-chant-india-instead-of-his-name-is-total-class-252496.html" class="tint" title="This Video Of Virat Kohli Asking Fans To Chant 'India' Instead Of His Name Is Total Class!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458808081_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458808081_502x234.jpg" border="0" alt="This Video Of Virat Kohli Asking Fans To Chant 'India' Instead Of His Name Is Total Class!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/this-video-of-virat-kohli-asking-fans-to-chant-india-instead-of-his-name-is-total-class-252496.html" title="This Video Of Virat Kohli Asking Fans To Chant 'India' Instead Of His Name Is Total Class!">
    This Video Of Virat Kohli Asking Fans To Chant 'India' Instead Of His Name Is Total Class!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/america-has-developed-a-dengue-vaccine-that-is-100-effective-so-far-252162.html" class="tint" title="America Has Developed A Dengue Vaccine That Is 100% Effective So Far!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover-1_1458214414_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover-1_1458214414_502x234.jpg" border="0" alt="America Has Developed A Dengue Vaccine That Is 100% Effective So Far!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/america-has-developed-a-dengue-vaccine-that-is-100-effective-so-far-252162.html" title="America Has Developed A Dengue Vaccine That Is 100% Effective So Far!">
    America Has Developed A Dengue Vaccine That Is 100% Effective So Far!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/8-things-players-said-during-hardik-pandya-s-match-winning-final-over-vs-bangladesh-252497.html" class="tint" title="All The Advice Hardik Pandya Got And Other Things Players Said In The Final Over Vs Bangladesh">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/dhonipandyabanafp_1458807656_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dhonipandyabanafp_1458807656_502x234.jpg" border="0" alt="All The Advice Hardik Pandya Got And Other Things Players Said In The Final Over Vs Bangladesh" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/8-things-players-said-during-hardik-pandya-s-match-winning-final-over-vs-bangladesh-252497.html" title="All The Advice Hardik Pandya Got And Other Things Players Said In The Final Over Vs Bangladesh">
    All The Advice Hardik Pandya Got And Other Things Players Said In The Final Over Vs Bangladesh                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/from-salman-khan-to-amitabh-bachchan-here-s-how-b-town-is-celebrating-holi-252490.html" class="tint" title="From Salman Khan To Amitabh Bachchan, Here's How B-Town Is Celebrating Holi!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458801059_1458801068_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458801059_1458801068_502x234.jpg" border="0" alt="From Salman Khan To Amitabh Bachchan, Here's How B-Town Is Celebrating Holi!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/from-salman-khan-to-amitabh-bachchan-here-s-how-b-town-is-celebrating-holi-252490.html" title="From Salman Khan To Amitabh Bachchan, Here's How B-Town Is Celebrating Holi!">
    From Salman Khan To Amitabh Bachchan, Here's How B-Town Is Celebrating Holi!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/7-legends-and-beliefs-you-did-not-know-were-associated-with-holi-252415.html" class="tint" title="7 Beliefs Associated With Holi That Will Give You An Insight Into Its Origin">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/rk2_1458648592_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/rk2_1458648592_502x234.jpg" border="0" alt="7 Beliefs Associated With Holi That Will Give You An Insight Into Its Origin" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/7-legends-and-beliefs-you-did-not-know-were-associated-with-holi-252415.html" title="7 Beliefs Associated With Holi That Will Give You An Insight Into Its Origin">
    7 Beliefs Associated With Holi That Will Give You An Insight Into Its Origin                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/here-s-what-would-happen-to-you-if-you-stopped-eating-251832.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/here-s-what-would-happen-to-you-if-you-stopped-eating-251832.html" class="tint" title="Hereâs What Would Happen To You If You Stopped Eating!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1457617303_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1457617303_502x234.jpg" border="0" alt="Hereâs What Would Happen To You If You Stopped Eating!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/here-s-what-would-happen-to-you-if-you-stopped-eating-251832.html" title="Hereâs What Would Happen To You If You Stopped Eating!">
    Hereâs What Would Happen To You If You Stopped Eating!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-calmly-shuts-down-a-journalist-for-trying-to-belittle-india-s-win-against-bangladesh-252487.html" class="tint" title="MS Dhoni Calmly Shuts Down A Journalist For Trying To Belittle India's Win Against Bangladesh!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/dhoni_1458795990_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dhoni_1458795990_502x234.jpg" border="0" alt="MS Dhoni Calmly Shuts Down A Journalist For Trying To Belittle India's Win Against Bangladesh!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-calmly-shuts-down-a-journalist-for-trying-to-belittle-india-s-win-against-bangladesh-252487.html" title="MS Dhoni Calmly Shuts Down A Journalist For Trying To Belittle India's Win Against Bangladesh!">
    MS Dhoni Calmly Shuts Down A Journalist For Trying To Belittle India's Win Against Bangladesh!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-proves-yet-again-he-creates-his-own-luck-gives-india-one-more-reason-to-celebrate-holi-252486.html" class="tint" title="MS Dhoni Proves Yet Again He Creates His Own Luck, Gives India One More Reason To Celebrate Holi">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/dhonicelebratesreuters_1458758731_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dhonicelebratesreuters_1458758731_502x234.jpg" border="0" alt="MS Dhoni Proves Yet Again He Creates His Own Luck, Gives India One More Reason To Celebrate Holi" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-proves-yet-again-he-creates-his-own-luck-gives-india-one-more-reason-to-celebrate-holi-252486.html" title="MS Dhoni Proves Yet Again He Creates His Own Luck, Gives India One More Reason To Celebrate Holi">
    MS Dhoni Proves Yet Again He Creates His Own Luck, Gives India One More Reason To Celebrate Holi                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/7-things-we-worry-about-that-prevents-us-from-enjoying-holi-252438.html" class="tint" title="7 Things We Worry About That Prevents Us From Enjoying Holi">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458746619_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458746619_502x234.jpg" border="0" alt="7 Things We Worry About That Prevents Us From Enjoying Holi" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/7-things-we-worry-about-that-prevents-us-from-enjoying-holi-252438.html" title="7 Things We Worry About That Prevents Us From Enjoying Holi">
    7 Things We Worry About That Prevents Us From Enjoying Holi                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/shahrukh-khan-got-into-the-commentary-box-and-couldn-t-stop-raving-about-virat-kohli-and-ms-dhoni-252485.html" class="tint" title="Shahrukh Khan Got Into The Commentary Box And Couldn't Stop Raving About Virat Kohli And MS Dhoni!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/srkkohli_1458745241_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/srkkohli_1458745241_502x234.jpg" border="0" alt="Shahrukh Khan Got Into The Commentary Box And Couldn't Stop Raving About Virat Kohli And MS Dhoni!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/shahrukh-khan-got-into-the-commentary-box-and-couldn-t-stop-raving-about-virat-kohli-and-ms-dhoni-252485.html" title="Shahrukh Khan Got Into The Commentary Box And Couldn't Stop Raving About Virat Kohli And MS Dhoni!">
    Shahrukh Khan Got Into The Commentary Box And Couldn't Stop Raving About Virat Kohli And MS Dhoni!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/tips-tricks/bhang-is-known-to-fight-depression-6-more-things-you-should-know-252234.html" class="tint" title="'Bhang' Is Known To Fight Depression + 6 More Things You Should Know">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458303166_1458303182_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458303166_1458303182_502x234.jpg" border="0" alt="'Bhang' Is Known To Fight Depression + 6 More Things You Should Know" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/tips-tricks/bhang-is-known-to-fight-depression-6-more-things-you-should-know-252234.html" title="'Bhang' Is Known To Fight Depression + 6 More Things You Should Know">
    'Bhang' Is Known To Fight Depression + 6 More Things You Should Know                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/adele-pays-a-tribute-to-the-brussels-attacks-victims-with-an-emotional-performance-252479.html" class="tint" title="Adele Pays A Tribute To The Brussels Attacks Victims With An Emotional Performance">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/adele-card_1458737274_1458737283_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/adele-card_1458737274_1458737283_502x234.jpg" border="0" alt="Adele Pays A Tribute To The Brussels Attacks Victims With An Emotional Performance" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/adele-pays-a-tribute-to-the-brussels-attacks-victims-with-an-emotional-performance-252479.html" title="Adele Pays A Tribute To The Brussels Attacks Victims With An Emotional Performance">
    Adele Pays A Tribute To The Brussels Attacks Victims With An Emotional Performance                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/i-don-t-play-holi-because-i-have-ocd-about-cleanliness-says-ranveer-singh-252467.html" class="tint" title="I Don't Play Holi Because I Have OCD About Cleanliness, Says Ranveer Singh!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/ranveer-singh-latest-wallpaper-hd_1458730552_1458730557_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ranveer-singh-latest-wallpaper-hd_1458730552_1458730557_502x234.jpg" border="0" alt="I Don't Play Holi Because I Have OCD About Cleanliness, Says Ranveer Singh!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/i-don-t-play-holi-because-i-have-ocd-about-cleanliness-says-ranveer-singh-252467.html" title="I Don't Play Holi Because I Have OCD About Cleanliness, Says Ranveer Singh!">
    I Don't Play Holi Because I Have OCD About Cleanliness, Says Ranveer Singh!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/shahid-kapoor-s-rugged-look-from-rangoon-will-make-all-the-ladies-go-weak-in-their-knees-252463.html" class="tint" title="Shahid Kapoor's Rugged Look From Rangoon Will Make All The Ladies Go Weak In Their Knees!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458729127_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458729127_502x234.jpg" border="0" alt="Shahid Kapoor's Rugged Look From Rangoon Will Make All The Ladies Go Weak In Their Knees!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/shahid-kapoor-s-rugged-look-from-rangoon-will-make-all-the-ladies-go-weak-in-their-knees-252463.html" title="Shahid Kapoor's Rugged Look From Rangoon Will Make All The Ladies Go Weak In Their Knees!">
    Shahid Kapoor's Rugged Look From Rangoon Will Make All The Ladies Go Weak In Their Knees!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/11-different-forms-of-holi-that-are-celebrated-around-india-252340.html" class="tint" title="11 Different Forms Of Holi That Are Celebrated Around India">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/holi2_1458549287_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/holi2_1458549287_502x234.jpg" border="0" alt="11 Different Forms Of Holi That Are Celebrated Around India" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/11-different-forms-of-holi-that-are-celebrated-around-india-252340.html" title="11 Different Forms Of Holi That Are Celebrated Around India">
    11 Different Forms Of Holi That Are Celebrated Around India                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/13-reasons-you-should-really-avoid-being-alone-on-holi-252376.html" class="tint" title="13 Reasons You Should Really Avoid Being Alone On Holi">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/deepika-holi2_1458629609_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/deepika-holi2_1458629609_502x234.jpg" border="0" alt="13 Reasons You Should Really Avoid Being Alone On Holi" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/13-reasons-you-should-really-avoid-being-alone-on-holi-252376.html" title="13 Reasons You Should Really Avoid Being Alone On Holi">
    13 Reasons You Should Really Avoid Being Alone On Holi                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/batman-vs-superman-is-the-best-action-film-ever-well-that-s-what-twitter-says-252469.html" class="tint" title="'Batman Vs Superman' Is The Best Action Film Ever. Well, That's What Twitter Says!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/2-card_1458732860_1458732862_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/2-card_1458732860_1458732862_502x234.jpg" border="0" alt="'Batman Vs Superman' Is The Best Action Film Ever. Well, That's What Twitter Says!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/batman-vs-superman-is-the-best-action-film-ever-well-that-s-what-twitter-says-252469.html" title="'Batman Vs Superman' Is The Best Action Film Ever. Well, That's What Twitter Says!">
    'Batman Vs Superman' Is The Best Action Film Ever. Well, That's What Twitter Says!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/ranveer-singh-makes-a-surprise-visit-to-sri-lanka-attends-a-wedding-with-ladylove-deepika-252462.html" class="tint" title="Ranveer Singh Makes A Surprise Visit To Sri Lanka, Attends A Wedding With Ladylove Deepika">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/tamasha-success-party-00501122015-gossipticket_1458727494_1458727499_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/tamasha-success-party-00501122015-gossipticket_1458727494_1458727499_502x234.jpg" border="0" alt="Ranveer Singh Makes A Surprise Visit To Sri Lanka, Attends A Wedding With Ladylove Deepika" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/ranveer-singh-makes-a-surprise-visit-to-sri-lanka-attends-a-wedding-with-ladylove-deepika-252462.html" title="Ranveer Singh Makes A Surprise Visit To Sri Lanka, Attends A Wedding With Ladylove Deepika">
    Ranveer Singh Makes A Surprise Visit To Sri Lanka, Attends A Wedding With Ladylove Deepika                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-signs-bigg-boss-yet-again-but-only-after-a-30-fee-hike-252455.html" class="tint" title="Salman Khan Signs Bigg Boss Yet Again But Only After A 30% Fee Hike!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/bg-cr_1458721773_1458721777_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/bg-cr_1458721773_1458721777_502x234.jpg" border="0" alt="Salman Khan Signs Bigg Boss Yet Again But Only After A 30% Fee Hike!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/salman-khan-signs-bigg-boss-yet-again-but-only-after-a-30-fee-hike-252455.html" title="Salman Khan Signs Bigg Boss Yet Again But Only After A 30% Fee Hike!">
    Salman Khan Signs Bigg Boss Yet Again But Only After A 30% Fee Hike!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/travel/this-kenyan-woman-s-badly-photoshopped-holiday-pictures-have-made-her-famous-on-the-internet-252456.html" class="tint" title="This Kenyan Woman's Badly Photoshopped 'Holiday Pictures' Have Made Her Famous On The Internet">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp2_1458721899_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp2_1458721899_502x234.jpg" border="0" alt="This Kenyan Woman's Badly Photoshopped 'Holiday Pictures' Have Made Her Famous On The Internet" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/this-kenyan-woman-s-badly-photoshopped-holiday-pictures-have-made-her-famous-on-the-internet-252456.html" title="This Kenyan Woman's Badly Photoshopped 'Holiday Pictures' Have Made Her Famous On The Internet">
    This Kenyan Woman's Badly Photoshopped 'Holiday Pictures' Have Made Her Famous On The Internet                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/after-being-stalked-harassed-this-actress-turned-a-mumbai-beach-into-kung-fu-classroom-for-girls-252437.html" class="tint" title="After Being Stalked & Harassed, This Actress Turned A Mumbai Beach Into Kung Fu Classroom For Girls!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/ishita_1458714756_1458714762_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ishita_1458714756_1458714762_502x234.jpg" border="0" alt="After Being Stalked & Harassed, This Actress Turned A Mumbai Beach Into Kung Fu Classroom For Girls!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/after-being-stalked-harassed-this-actress-turned-a-mumbai-beach-into-kung-fu-classroom-for-girls-252437.html" title="After Being Stalked & Harassed, This Actress Turned A Mumbai Beach Into Kung Fu Classroom For Girls!">
    After Being Stalked & Harassed, This Actress Turned A Mumbai Beach Into Kung Fu Classroom For Girls!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/health/buzz/if-you-re-tired-of-gobi-aloo-try-this-roasted-cauliflower-recipe-instead-252235.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/if-you-re-tired-of-gobi-aloo-try-this-roasted-cauliflower-recipe-instead-252235.html" class="tint" title="If Youâre Tired Of Gobi Aloo, Try This Roasted Cauliflower Recipe Instead!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1458304852_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1458304852_502x234.jpg" border="0" alt="If Youâre Tired Of Gobi Aloo, Try This Roasted Cauliflower Recipe Instead!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/if-you-re-tired-of-gobi-aloo-try-this-roasted-cauliflower-recipe-instead-252235.html" title="If Youâre Tired Of Gobi Aloo, Try This Roasted Cauliflower Recipe Instead!">
    If Youâre Tired Of Gobi Aloo, Try This Roasted Cauliflower Recipe Instead!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/who-knew-life-lessons-could-be-learnt-with-the-help-of-pepsi-mini-cans-this-is-amazing-252382.html" class="tint" title="Who Knew Life Lessons Could Be Learnt With The Help Of Pepsi Mini Cans! This Is Amazing!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458720618_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458720618_502x234.jpg" border="0" alt="Who Knew Life Lessons Could Be Learnt With The Help Of Pepsi Mini Cans! This Is Amazing!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/who-knew-life-lessons-could-be-learnt-with-the-help-of-pepsi-mini-cans-this-is-amazing-252382.html" title="Who Knew Life Lessons Could Be Learnt With The Help Of Pepsi Mini Cans! This Is Amazing!">
    Who Knew Life Lessons Could Be Learnt With The Help Of Pepsi Mini Cans! This Is Amazing!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/this-short-film-will-take-you-back-in-time-make-you-miss-your-school-crush-real-bad-252451.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/this-short-film-will-take-you-back-in-time-make-you-miss-your-school-crush-real-bad-252451.html" class="tint" title="This Short Film Will Take You Back In Time & Make You Miss Your School Crush Real Bad!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/school-card_1458719916_1458719921_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/school-card_1458719916_1458719921_502x234.jpg" border="0" alt="This Short Film Will Take You Back In Time & Make You Miss Your School Crush Real Bad!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/this-short-film-will-take-you-back-in-time-make-you-miss-your-school-crush-real-bad-252451.html" title="This Short Film Will Take You Back In Time & Make You Miss Your School Crush Real Bad!">
    This Short Film Will Take You Back In Time & Make You Miss Your School Crush Real Bad!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/arjun-rampal-has-a-perfect-message-for-haters-and-it-s-all-you-need-to-feel-motivated-today-252440.html" class="tint" title="Arjun Rampal Has A Perfect Message For Haters And It's All You Need To Feel Motivated Today!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/dabboo-ratnani_arjun-rampal_001220140228120230360_1458715571_1458715575_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dabboo-ratnani_arjun-rampal_001220140228120230360_1458715571_1458715575_502x234.jpg" border="0" alt="Arjun Rampal Has A Perfect Message For Haters And It's All You Need To Feel Motivated Today!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/arjun-rampal-has-a-perfect-message-for-haters-and-it-s-all-you-need-to-feel-motivated-today-252440.html" title="Arjun Rampal Has A Perfect Message For Haters And It's All You Need To Feel Motivated Today!">
    Arjun Rampal Has A Perfect Message For Haters And It's All You Need To Feel Motivated Today!                    </a>
                </div>    
            </figcaption>
        </div>
    
</div><!--life-panel end-->

<div class="trending-panel cf "><!--trending-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/celebs/from-salman-khan-to-amitabh-bachchan-here-s-how-b-town-is-celebrating-holi-252490.html" class="tint" title="From Salman Khan To Amitabh Bachchan, Here's How B-Town Is Celebrating Holi!">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458801059_1458801068_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458801059_1458801068_236x111.jpg" border="0" alt="From Salman Khan To Amitabh Bachchan, Here's How B-Town Is Celebrating Holi!"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/from-salman-khan-to-amitabh-bachchan-here-s-how-b-town-is-celebrating-holi-252490.html" title="From Salman Khan To Amitabh Bachchan, Here's How B-Town Is Celebrating Holi!">
    From Salman Khan To Amitabh Bachchan, Here's How B-Town Is Celebrating Holi!                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    <div class='container3'>        <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-10"  data-slot="129061" data-position="10" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-proves-yet-again-he-creates-his-own-luck-gives-india-one-more-reason-to-celebrate-holi-252486.html" class="tint" title="MS Dhoni Proves Yet Again He Creates His Own Luck, Gives India One More Reason To Celebrate Holi">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/dhonicelebratesreuters_1458758731_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dhonicelebratesreuters_1458758731_236x111.jpg" border="0" alt="MS Dhoni Proves Yet Again He Creates His Own Luck, Gives India One More Reason To Celebrate Holi"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ms-dhoni-proves-yet-again-he-creates-his-own-luck-gives-india-one-more-reason-to-celebrate-holi-252486.html" title="MS Dhoni Proves Yet Again He Creates His Own Luck, Gives India One More Reason To Celebrate Holi">
    MS Dhoni Proves Yet Again He Creates His Own Luck, Gives India One More Reason To Celebrate Holi                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/culture/who-we-are/11-different-forms-of-holi-that-are-celebrated-around-india-252340.html" class="tint" title="11 Different Forms Of Holi That Are Celebrated Around India">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/holi2_1458549287_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/holi2_1458549287_236x111.jpg" border="0" alt="11 Different Forms Of Holi That Are Celebrated Around India"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/11-different-forms-of-holi-that-are-celebrated-around-india-252340.html" title="11 Different Forms Of Holi That Are Celebrated Around India">
    11 Different Forms Of Holi That Are Celebrated Around India                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/culture/travel/this-kenyan-woman-s-badly-photoshopped-holiday-pictures-have-made-her-famous-on-the-internet-252456.html" class="tint" title="This Kenyan Woman's Badly Photoshopped 'Holiday Pictures' Have Made Her Famous On The Internet">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp2_1458721899_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp2_1458721899_236x111.jpg" border="0" alt="This Kenyan Woman's Badly Photoshopped 'Holiday Pictures' Have Made Her Famous On The Internet"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/this-kenyan-woman-s-badly-photoshopped-holiday-pictures-have-made-her-famous-on-the-internet-252456.html" title="This Kenyan Woman's Badly Photoshopped 'Holiday Pictures' Have Made Her Famous On The Internet">
    This Kenyan Woman's Badly Photoshopped 'Holiday Pictures' Have Made Her Famous On The Internet                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    
</div>

</div><!--trending-panel end-->

</section>
<section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<div class="remove-fixed-home">&nbsp;</div>
<section class="big-ads" id="adfooter">
    <div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">

    $('#bigAd1_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD2('bigAd2_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible     

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd2_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD3('bigAd3_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible

            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible  

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd3_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            BADros('badRos_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible
            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });

    $(document).ready(function () {
        /* spotlight onload tracking homepage */
        //console.log("homepage");
    });

</script>    <div class="clr"></div>

	
    <script>
                showFooterCode();
        </script>
<style>
    .bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    4,695,690<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>60,302 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>

        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                    <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                            <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                            <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                            <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                            <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                            <a href='http://www.indiatimes.com/play/' class="yellow size">Slog Overs</a> 
                                                <a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                    						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/news/india/'>india</a>
                         
                                        <a href='http://www.indiatimes.com/news/world/'>world</a>
                         
                                        <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                         
                                        <a href='http://www.indiatimes.com/news/weird/'>weird</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                         
                                        <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                         
                                        <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                         
                                        <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                         
                                        <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                         
                                        <a href='http://www.indiatimes.com/culture/food/'>food</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                         
                                        <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                         
                                        <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                         
                                        <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                         
                                        <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                         
                                        <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                         
                                        <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
            
                            </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 

                            <a href="http://timesofindia.indiatimes.com/budgetspecial.cms"  target="_blank">Budget 2016</a> 
                        
                            <a href="http://timesofindia.indiatimes.com/budget-2016/rail-budget-2016/indiabudget/50867848.cms"  target="_blank">Rail Budget 2016</a> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                            <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                            <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                            <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                            <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                            <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                            <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                    </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2016 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $(document).ready(function () {
        $("#subscribers_id").click(function () {
            $("#UserEmail").focus();
        });
    });
    function socialRHSHide() {}
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=117.3" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=117.3"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=117.3"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=117.3"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=117.3"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>