<!DOCTYPE html>
<html id="atomic" lang="en-US" class="atomic my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr desktop Desktop bkt201">
<head>
    <title>Yahoo</title><meta http-equiv="x-dns-prefetch-control" content="on"><link rel="dns-prefetch" href="//s.yimg.com"><link rel="preconnect" href="//s.yimg.com"><link rel="dns-prefetch" href="//y.analytics.yahoo.com"><link rel="preconnect" href="//y.analytics.yahoo.com"><link rel="dns-prefetch" href="//geo.query.yahoo.com"><link rel="preconnect" href="//geo.query.yahoo.com"><link rel="dns-prefetch" href="//csc.beap.bc.yahoo.com"><link rel="preconnect" href="//csc.beap.bc.yahoo.com"><link rel="dns-prefetch" href="//geo.yahoo.com"><link rel="preconnect" href="//geo.yahoo.com"><link rel="dns-prefetch" href="//comet.yahoo.com"><link rel="preconnect" href="//comet.yahoo.com">    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="description" content="News, email and search are just the beginning. Discover more every day. Find your yodel.">
    <meta name="keywords" content="yahoo, yahoo home page, yahoo homepage, yahoo search, yahoo mail, yahoo messenger, yahoo games, news, finance, sport, entertainment">
    <meta property="og:title" content="Yahoo" />
    <meta property="og:type" content='website' />
    <meta property="og:url" content="http://www.yahoo.com" />
    <meta property="og:description" content="News, email and search are just the beginning. Discover more every day. Find your yodel."/>
    <meta property="og:image" content="https://s.yimg.com/dh/ap/default/130909/y_200_a.png"/>
    <meta property="og:site_name" content="Yahoo" />
    <meta property="fb:app_id" content="90376669494" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="icon" sizes="any" mask href="/sy/os/mit/media/p/common/images/favicon_new-7483e38.svg">
<meta name="theme-color" content="#400090">
    <link rel="shortcut icon" href="/sy/rz/l/favicon.ico" />
    <link rel="canonical" href="https://www.yahoo.com/" />        <link href="/sy/os/fp/atomic-css.271ed811.css" rel="stylesheet" type="text/css">
            
    
    
    
    <!-- streaming unlocked -->

    <!-- MapleTop -->
    
    
<link rel="stylesheet" type="text/css" href="/sy/zz/combo?nn/lib/metro/g/myy/advance_base_rc4_0.0.36.css&nn/lib/metro/g/myy/font_rc4_spdy_0.0.35.css&nn/lib/metro/g/myy/yahoo20_grid_0.0.101.css&nn/lib/metro/g/myy/video_styles_0.0.23.css&nn/lib/metro/g/myy/advance_color_0.0.7.css&nn/lib/metro/g/theme/viewer_modal_center_0.0.11.css&nn/lib/metro/g/theme/yglyphs-legacy_0.0.5.css&nn/lib/metro/g/sda/fp_sda_0.0.4.css&nn/lib/metro/g/sda/sda_advance_0.0.8.css&nn/lib/metro/g/fpfooter/advance_0.0.4.css&/os/stencil/3.1.0/styles-ltr.css&/os/yc/css/bundle.c60a6d54.css" />
    <link rel="search" type="application/opensearchdescription+xml" href="https://search.yahoo.com/opensearch.xml" title="Yahoo Search" />
    
    <script>
    var myYahoostartTime = new Date(),
        afPerfHeadStart=new Date().getTime(),
        ie;

    
    document.documentElement.className += ' JsEnabled jsenabled';</script>
    

<style>.breakingnews.gradient-1 {
    background: #ff7617; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #ff7617 0%, #ff9b00 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#ff7617), color-stop(65%,#ff9b00)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* IE10+ */
    background: linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7617', endColorstr='#ff9b00',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}

.breakingnews.gradient-2 {
    background: #e91857; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #e91857 0%, #ff353c 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#e91857), color-stop(65%,#ff353c)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* IE10+ */
    background: linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e91857', endColorstr='#ff353c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}</style>
<style>.js-stream-adfdb-reason {
    display: none;
}
.js-stream-adfdb-options .js-stream-adfdb-other-selected + .js-stream-adfdb-reason {
    display: block;
}

.stream-collapse {
    max-height: 0;
    -webkit-transition: max-height 0.3s ease;
    -moz-transition: max-height 0.3s ease;
    -o-transition: max-height 0.3s ease;
    transition: max-height 0.3s ease;
}

.RevealNested-on .ActionDislike {
    display: none;
}

.streamv2 .stream-share-open .js-stream-share-panel {
    height: auto !important;
    opacity: 1 !important;
}

/**
* Tooltips
*/
.js-stream-actions a:hover .ActionTooltip.hide,
.js-stream-actions .ActionTooltip,
.js-stream-actions a:active .ActionTooltip,
.js-stream-actions a:focus .ActionTooltip {
    clip: rect(1px 1px 1px 1px);
    clip: rect(1px,1px,1px,1px);
    height: 1px;
    width: 1px;
    overflow: hidden;
    line-height: 1.4;
    white-space: nowrap
}

.js-stream-actions a:hover .ActionTooltip {
    clip: rect(auto auto auto auto);
    clip: auto;
    height: auto;
    width: auto;
    overflow: visible;
    -webkit-transform: translateX(-50%) !important;
    -ms-transform: translateX(-50%) !important;
    transform: translateX(-50%) !important;
    *min-width: 80px;
    *margin-right: -48px;
    width: 136px;
    text-align: center;
}

.streamv2 .js-stream-dense .strm-left {
    max-width: 190px;
    width: 29%;
}

.streamv2 .js-stream-sparse .strm-left {
    width: 33%;
    max-width: 230px;
}
.streamv2 .js-stream-sparse .strm-right {
    width: 57%;
}
.streamv2 .js-stream-sparse .strm-full {
    width: 90%;
}

.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-left {
    width: 62.825%;
    max-width: 441px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-left {
    width: 72%;
    max-width: 508px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-right,
.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-right {
    width: auto;
    max-width: none;
}


.streamv2 .strm-right-menu-roundup .strm-right .js-stream-content-link:before {
    content: '';
    vertical-align: middle;
    height: 100%;
    display: inline-block;
}

.streamv2 .strm-chevron {
    display: none;
}

.streamv2 .strm-right-menu-roundup .js-stream-content-link.selected .strm-chevron,
.streamv2 .strm-right-menu-roundup .js-stream-content-link:hover .strm-chevron {
    display: block;
}

.streamv2 .rounded-img {
    border-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-main-img .rounded-img {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.streamv2 .js-stream-sparse .storyline-img-0 {
    border-bottom-left-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-img-1 {
    border-bottom-right-radius: 3px;
}

.ua-ff#atomic .strm-headline-label {
    margin-bottom: 4px;
}
/* cluster image gradient transparent to dark overlay */
.streamv2 .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 32%, rgba(0,0,0,0.65) 97%, rgba(0,0,0,0.65) 98%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(32%,rgba(0,0,0,0)), color-stop(97%,rgba(0,0,0,0.65)), color-stop(98%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

/* cluster image gradient transparent to dark overlay */
.streamv2 .js-stream-roundup .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 52%, rgba(0,0,0,0.85) 107%, rgba(0,0,0,0.85) 78%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(52%,rgba(0,0,0,0)), color-stop(107%,rgba(0,0,0,0.85)), color-stop(78%,rgba(0,0,0,0.85))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

.streamv2 .strm-upsell-gradient-right {
    background: -moz-linear-gradient(right,  rgba(255,255,255,0.005) 0%, rgba(255,255,255,0.90) 85%, rgba(255,255,255,0.99) 98%); /* FF3.6+ */
    background: -webkit-linear-gradient(right,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(right,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(right,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* IE10+ */
    background: linear-gradient(to right, rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* W3C */
}
.streamv2 .strm-upsell-gradient-left {
    background: -moz-linear-gradient(left,  rgba(255,255,255,0.005) 0%, rgba(255,255,255,0.90) 85%, rgba(255,255,255,0.99) 98%); /* FF3.6+ */
    background: -webkit-linear-gradient(left,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(left,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(left,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* IE10+ */
    background: linear-gradient(to left, rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* W3C */
}

.ua-ie10 .streamv2 .js-stream-related-content ul,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip > ul {
    margin-right: -7px !important;
}

.ua-ie10 .streamv2 .js-stream-related-content li,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip .W\(25\%\) {
    width: 24.5% !important;
}

.streamv2 .js-stream-side-buttons li:nth-child(1) .ActionTooltip {
    top: -1px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(1) .ActionTooltip {
    top: 0px;
}

.streamv2 .js-stream-side-buttons li:nth-child(2) .ActionTooltip {
    top: 22px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(2) .ActionTooltip {
    top: 40px;
}
.streamv2 .js-stream-side-buttons li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons li:nth-child(3) .js-stream-share-panel {
    top: 45px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .js-stream-share-panel {
    top: 64px;
}
.streamv2 .js-stream-side-buttons li:nth-child(4) .js-stream-share-panel {
    top: 86px;
}

.streamv2 .js-stream-side-buttons .ActionComments {
    margin-top: 6px;
}
.streamv2 .js-stream-side-buttons.has-comments .ActionComments {
    margin-top: 0px;
}
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\$c_icon\),
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\#96989f\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\$c_icon\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\#96989f\) {
    color: inherit !important;
}
.streamv2 .js-stream-side-buttons .ActionComments .ActionTooltip {
    margin-top: 15px;
}

.js-stream-comment-counter-update {
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
}

/* Only set opacity to 0 for animation when js is enabled and css3 supported */
.JsEnabled .streamv2 .js-stream-comment-hidden:nth-of-type(1n) {
    opacity: 0;
}

.JsEnabled .streamv2 .animated {
    -webkit-animation-duration: 1.5s;
    animation-duration: 1.5s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
}

@-webkit-keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

@keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

.JsEnabled .streamv2 .fadeOut {
    -webkit-animation-name: fadeOut;
    animation-name: fadeOut;
}

@-webkit-keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@-webkit-keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

@keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

.JsEnabled .streamv2 .fadeIn {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
}


.JsEnabled .streamv2 .js-stream-roundup-filmstrip .fadeIn {
    -webkit-animation-name: fadeInNtk;
    animation-name: fadeInNtk;
}

.streamv2 .js-stream-comment .js-stream-cmnt-up:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-up.selected {
    color: #1AC567 !important;
}
.streamv2 .js-stream-comment .js-stream-cmnt-down:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-down.selected,
.streamv2 .js-stream-comment .js-stream-cmnt-flag:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-flag.selected {
    color: #F0162F !important;
}

/* to show the dislike ad tooltip */
#atomic .Collapse-opened .js-stream-related-item-ad {
    overflow: visible;
}

.js-stream-upsell-carousel .js-stream-upsell-left.Disabled,
.js-stream-upsell-carousel .js-stream-upsell-right.Disabled,
.js-stream-upsell-carousel .strm-upsell-gradient-left.Disabled,
.js-stream-upsell-carousel .strm-upsell-gradient-right.Disabled {
    display: none;
}
.js-stream-upsell-carousel .js-stream-upsell-left,
.js-stream-upsell-carousel .js-stream-upsell-right {
    opacity: 0;
}
.js-stream-upsell-carousel:hover .js-stream-upsell-left,
.js-stream-upsell-carousel:hover .js-stream-upsell-right {
    opacity: 0.7;
}
.js-stream-upsell-carousel .js-stream-upsell-left:hover,
.js-stream-upsell-carousel .js-stream-upsell-right:hover {
    opacity: 1;
}

.ua-ie .streamv2 .strm-stretch {
    background-color: #fff;
    opacity: 0;
    filter: alpha(opacity=1);
}
</style>
 

<style>.js-activitylist-item .orb {
    background-position: 0 0;
    width: 80px;
    height: 80px;
}

.js-activitylist-item .hexagon {
    background-position: -81px 0;
    width: 80px;
    height: 90px;
}

.js-activitylist-item .ribbon {
    background-position: -162px 0;
    width: 91px;
    height: 31px;
}
</style>
 

<style>#uh-search .yui3-aclist {
    width: inherit !important;
}
#uh-search .yui3-aclist-content {
    background-color: #fff;
    text-align: left;
    border: 1px solid #ccc;
    margin-top: 1px;
    border-top: 0;
}
#uh-search .yui3-aclist-list {
    margin: 0;
    line-height: 1.1;
}
#uh-search .yui3-aclist-item {
    padding: 5px 0 6px 0;
    font-size: 18px;
    font-weight: bold;
}
#uh-search .yui3-aclist-item:hover,
#uh-search .yui3-aclist-item-active {
    background-color: #c6d7ff;
}
#uh-search .yui3-aclist-item {
    padding-left: 10px;
    padding-right: 10px;
}
#uh-search .yui3-highlight {
    font-weight: 200;
}
#uh-search-box::-ms-clear {
    display: none;
}


/* Instant Search Styles */

#uh-search .yui3-aclist-item {
    line-height: 0.9;
}
#InstantSearchMask {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s linear 0.15s, opacity 0.15s linear;
}
.iSearch-display .Col1,
.iSearch-display .Col2,
.iSearch-display .Col3 {
    height: 0;
    overflow: hidden;
}
.iSearch-display #InstantSearch {
    visibility: visible;
}
.iSearch-mask #InstantSearchMask {
    visibility: visible;
    opacity: 1;
    transition: visibility 0s linear, opacity 0.15s linear;
}
.iSearch-loading.iSearch-mask #InstantSearchMask,
.iSearch-loading #InstantSearchMask {
    visibility: visible;
    opacity: .7;
}


/* Firefox 28 and below fix */
#uh-search-box,
#uh-ghost-box,
.instant-filters,
.instant-results {
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
/* ----------------------- */
</style>
 

<style>@charset "UTF-8";.Lb-Close-Icon,.icon-share-close,.icons-arrow,.icons-slideshow-icon{background-color:transparent;background-image:url(/sy/os/publish-images/news/2014-04-23/65fcff60-cb23-11e3-bead-55c0602d5659_icons-sb930b067ee.png);background-repeat:no-repeat}#Share .icon-share-close{width:30px;height:25px;background-size:cover;background-position:8px -72px}.Hl-Viewer{background:#fff}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay{position:absolute;color:#fff;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodâ¦EiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDAwMDAiIHN0b3Atb3BhY2l0eT0iMC45NSIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==);background:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,rgba(0,0,0,0)),color-stop(100%,rgba(0,0,0,.95)));background:-moz-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:-webkit-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95))}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline{text-shadow:0 1px 0 #000}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline-Box{position:absolute}.Hl-Viewer .Content-Body{color:#000;font-weight:300;letter-spacing:.3px;word-wrap:break-word;word-break:break-word}.Hl-Viewer .Content-Body>p{margin-bottom:1.45em;margin-top:1.45em}.Hl-Viewer .Content-Body h1,.Hl-Viewer .Content-Body h2,.Hl-Viewer .Content-Body h3,.Hl-Viewer .Content-Body h4,.Hl-Viewer .Content-Body h5,.Hl-Viewer .Content-Body h6{font-weight:400}.Hl-Viewer .Content-Body h1{font-size:18px;font-size:1.8rem}.Hl-Viewer .Content-Body h2{font-size:17px;font-size:1.7rem}.Hl-Viewer .Content-Body h3{font-size:16px;font-size:1.6rem}.Hl-Viewer .Content-Body h4{font-size:15px;font-size:1.5rem}.Hl-Viewer .Content-Body h5{font-size:14px;font-size:1.4rem}.Hl-Viewer .Content-Body h6{font-size:13px;font-size:1.3rem}.Hl-Viewer .icons-slideshow-icon{left:5%;top:5%;height:46px;width:47px;background-size:cover}.Hl-Viewer blockquote{padding:5px 10px;quotes:"\201C" "\201C" "\201C" "\201C";line-height:18px}.Hl-Viewer blockquote:before{color:#ccc;content:open-quote;font-size:3em;line-height:.1em;vertical-align:-.4em}.Hl-Viewer blockquote p{display:inline;color:#747474;font-weight:400}.Hl-Viewer blockquote p:after{content:"\A";white-space:pre}.Hl-Viewer .Credit,.Hl-Viewer .Provider{letter-spacing:.5px}.JsEnabled .ImageLoader-Delayed,.JsEnabled .ImageLoader-Loaded{-moz-transition-duration:.2s;-o-transition-duration:.2s;-webkit-transition-duration:.2s;transition-duration:.2s;-moz-transition-property:opacity;-o-transition-property:opacity;-webkit-transition-property:opacity;transition-property:opacity;-moz-transition-timing-function:ease-out;-o-transition-timing-function:ease-out;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}#Stencil .Bdrs-100{border-top-left-radius:100px;border-top-right-radius:100px;border-bottom-left-radius:100px;border-bottom-right-radius:100px}.Slideshow-Lightbox .lb-meta-content{padding-bottom:30px;background:#fff}.Slideshow-Lightbox .lb-meta-content .lb-meta-txt-container-full,.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-short{display:none}.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-full{display:block}.Slideshow-Lightbox .hide-meta .lb-meta-content{display:none}.Slideshow-Lightbox .Lb-Close-Icon{background-size:cover;width:47px;height:47px;z-index:202;background-position:0 -46px}.Reader-open .BrandBar,.Reader-open .rmp-TDYDBM{display:none}.Content-Body .Editorial-Left{float:left;margin-right:30px}.Content-Body .Editorial-Right{float:right;margin-left:30px}.Content-Body .Editorial-Left,.Content-Body .Editorial-Right{margin-top:0}#hl-viewer .Content-Col,.MagOn .Content-Col{margin-left:300px;margin-right:300px;min-height:100px}.HideCentralColumn .Content-Col{margin-left:150px;margin-right:150px}#hl-viewer{min-height:500px}.ShareBtns a:hover{opacity:.3;filter:alpha(opacity=33)}.modal-actions{bottom:3px}.modal-actions .ShareBtns a:hover{opacity:1;filter:alpha(opacity=100)}#hl-viewer:focus{outline:0}#hl-viewer .Timestamp{color:#abaeb7}#hl-viewer .Viewer-Close-Btn{position:fixed;top:10px;width:28px;height:28px;color:#fff;background-color:#2D1152;font-size:16px;margin-left:11px;z-index:5}#hl-viewer .ShareBtns .Share-Btn{border-radius:50px;width:17px;height:17px;line-height:1.3}#hl-viewer .ShareBtns .Share-Btn:hover{text-decoration:none}#hl-viewer .slideshow-carousel{background:#212124;padding-bottom:66%}#hl-viewer .lb-meta-content{border-bottom:1px solid #e8e8e8}#hl-viewer .lb-meta-caption-container{-webkit-font-smoothing:antialiased}#hl-viewer .lb-meta-txt-container.caption-scroll{max-height:4.3em;overflow-y:auto}#hl-viewer .viewer-wrapper{background:#fff;margin:0 0 30px 58px;min-width:980px}#hl-viewer .viewer-wrapper.mega-modal{min-height:750px}#hl-viewer .content-modal,#hl-viewer .js-viewer-slot-readMore{display:inline-block;width:640px;margin-right:29px;vertical-align:top}#hl-viewer .js-slider-item.first .content-modal{padding-top:0}#hl-viewer .Content-Body{padding-bottom:50px;border-bottom:1px solid #c8c8c8}#hl-viewer .Content-Body i{font-style:italic}#hl-viewer .js-slider-item.last .Content-Body{padding-bottom:0;border-bottom:none}#hl-viewer .Content-Body p:last-child{margin-bottom:0!important}#hl-viewer .js-slider-item.multirightrail .Content-Body.multirightrail{border-bottom:none}#hl-viewer .js-slider-item.multirightrail .content-modal{padding-top:0}#hl-viewer .js-slider-item.multirightrail{border-top:1px solid #B2B2B2;padding-top:50px}#hl-viewer .js-slider-item.multirightrail.first{padding-top:0;border-top:none}#hl-viewer .modal-aside.single{position:absolute;right:0;float:none;width:300px}#hl-viewer .modal-aside{display:inline-block;width:300px;position:relative;vertical-align:top;z-index:1}#hl-viewer .slideshow-carousel .main-col{padding-top:15px}#hl-viewer .index-count{color:#878c91;position:absolute;left:12px;bottom:7px;-webkit-font-smoothing:antialiased}#hl-viewer .js-lb-next,#hl-viewer .js-lb-prev{top:50%;z-index:3;width:28px;color:#878c91;font-size:30px;margin-top:-17px}#hl-viewer .js-lb-next:hover,#hl-viewer .js-lb-prev:hover{color:#fff}#hl-viewer .js-lb-next.Disabled,#hl-viewer .js-lb-prev.Disabled{display:none}#hl-viewer .js-lb-next:focus,#hl-viewer .js-lb-prev:focus{outline:0}#hl-viewer .slideshowv2 .slv2-carousel{padding-bottom:78%}#hl-viewer .slideshowv2 .slv2-carousel .main-col{padding-top:20px;padding-bottom:60px}#hl-viewer .slideshowv2 .slv2-carousel .js-thm-sl-icon{color:#fff;font-size:25px;top:12px;display:none}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-slider-ct{background-color:rgba(33,33,35,.5);border-radius:10px}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-car-mask{width:300px;opacity:1}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .js-thm-sl-icon{display:inline-block}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-pic{width:52px;height:52px}#hl-viewer .slideshowv2 .js-lb-next,#hl-viewer .slideshowv2 .js-lb-prev{opacity:.8;background:#444;color:#fff;width:45px;height:45px;padding:0;border-radius:3px}#hl-viewer .slideshowv2 .js-lb-next:hover,#hl-viewer .slideshowv2 .js-lb-prev:hover{background:#333;opacity:1}#hl-viewer .slideshowv2 .index-count{bottom:inherit;left:inherit;color:#000;position:relative;font-size:19px;font-size:1.9rem;font-weight:500;margin-bottom:8px}#hl-viewer .slideshowv2 .slide-title{font-weight:700;margin-bottom:4px}#hl-viewer .slideshowv2 .lb-meta-txt-container a:hover,#hl-viewer .slideshowv2 .lb-meta-txt-container a:link,#hl-viewer .slideshowv2 .lb-meta-txt-container a:visited{color:#188fff}#hl-viewer .slideshowv2 .sl-thumbnails{bottom:10px}#hl-viewer .slideshowv2 .sl-slider-ct{width:350px}#hl-viewer .slideshowv2 .sl-car-mask{width:150px;opacity:.5;transition:height .2s}#hl-viewer .slideshowv2 .sl-carousel .Selected .sl-pic{border:2px solid #fff}#hl-viewer .slideshowv2 .sl-carousel .sl-pic{width:22px;height:22px;border:2px solid transparent}#hl-viewer .Headline{line-height:1.2;margin-bottom:12px;font-size:30px;font-size:3rem}#hl-viewer .Headline-Box{margin-bottom:10px}#hl-viewer .Off-Network{border:1px solid #188FFF;padding:8px 20px;color:#188FFF!important;font-weight:700;text-decoration:none}#hl-viewer .Off-Network:hover{color:#fff!important;background:#0078ff;border-color:#0078ff}#hl-viewer .Content-Body p,#hl-viewer .remaining-body p{margin-bottom:1.1em;margin-top:0}#hl-viewer .Content-Body p p,#hl-viewer .remaining-body p p{margin:0}#hl-viewer img.Mb-20+p{margin-top:0}#hl-viewer .Content-Body,#hl-viewer .remaining-body{font-weight:400;font-size:15px;font-size:1.55rem;line-height:1.5;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .Content-Body #photo_copyright,#hl-viewer .Content-Body .caption,#hl-viewer .Content-Body .credit,#hl-viewer .Content-Body .source,#hl-viewer .remaining-body #photo_copyright,#hl-viewer .remaining-body .caption,#hl-viewer .remaining-body .credit,#hl-viewer .remaining-body .source{font-size:11px;font-size:1.1rem;color:#b2b2b2}#hl-viewer h2,#hl-viewer h3{font-size:22px;font-size:2.2rem;margin-bottom:.6em}#hl-viewer h4,#hl-viewer h5,#hl-viewer h6{font-size:19px;font-size:1.9rem;margin-bottom:.6em}#hl-viewer .Embed-Img{margin-bottom:20px}#hl-viewer .image{margin-bottom:20px;display:block;line-height:1}#hl-viewer .image .Embed-Img{margin-bottom:0}#hl-viewer .sidekick.fp h4{font-size:13px;font-size:1.3rem;margin-bottom:0}#hl-viewer .twitter-tweet-rendered{margin:10px auto}#hl-viewer #photo_copyright{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .alignright{float:right;margin:0 0 10px 10px}#hl-viewer .alignleft{float:left;margin:0 10px 10px 0}#hl-viewer .pmc-related-type{margin-right:10px}#hl-viewer blockquote{line-height:1.6}#hl-viewer p:empty{display:none}#hl-viewer .Ad-Viewer blockquote p,#hl-viewer .Hl-Viewer blockquote p{display:block}#hl-viewer .hl-ad-LREC{height:250px}#hl-viewer .source{font-size:14px;font-size:1.4rem}#hl-viewer .attribution{font-size:12px}#hl-viewer .first .Fixed-Header{top:-100px}#hl-viewer .Fixed-Header{transition:opacity .8s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .8s ease-in-out,top .6s ease-in-out;position:fixed;top:0;width:600px;padding:14px 0 0}#hl-viewer .Fixed-Header .source{max-width:540px}#hl-viewer .Fixed-Header .modal-actions{top:11px;left:540px;width:100px!important}#hl-viewer .Fixed-Header-Wrap{position:fixed;top:-100px;width:100%;z-index:4;height:48px;background-color:#fff;margin-left:-57px;border-bottom:1px solid #e5e5e5;transition:opacity .6s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .6s ease-in-out,top .6s ease-in-out;opacity:0;box-shadow:0 2px 4px -1px rgba(0,9,30,.1)}#hl-viewer .fadein{opacity:1}#hl-viewer .backfill-video-ads{width:300px;height:169px}#hl-viewer .backfill-video-ads h3{font-size:16px;font-size:1.6rem}#hl-viewer .backfill-video-ads h4{font-size:15px;font-size:1.5rem}#hl-viewer .backfill-video-ads .yvp-setting-btn{display:none}#hl-viewer .SidekickTV .list-view-item{display:inline-block;zoom:1;letter-spacing:normal;word-spacing:normal;text-rendering:auto;vertical-align:top}#hl-viewer .SidekickTV .SidekickTVArrow{border-bottom:40px solid transparent;border-left:40px solid}#hl-viewer .SidekickTV .ad-sponsored{color:#959595;font-size:11px;margin-right:0;padding-right:0}#hl-viewer .continue_reading .arraw,#hl-viewer .js-viewer-view-article .arraw{border-left:5px solid transparent;border-right:5px solid transparent;border-top:6px solid #000;vertical-align:middle}#hl-viewer .continue_reading:hover{color:#188FFF}#hl-viewer .continue_reading:hover .arraw{border-top-color:#188FFF}#hl-viewer .Mt-neg-40{margin-top:-40px}#hl-viewer .Mt-neg-30{margin-top:-30px}#hl-viewer .modal-sidekick-following .sidekick.fp{margin-top:-20px}#hl-viewer figure{margin:0}#hl-viewer iframe{border:none}#hl-viewer #viewer-end{height:1px;width:1px;display:block}#hl-viewer #viewer-end:focus{outline:0}#hl-viewer iframe.modal-embed-iframe{width:100%;max-width:640px;height:0;transition:height .2s ease-in-out}.ShareBtns .Share-Btn{padding:5px;border:1px solid #abaeb7}.ShareBtns .Share-Btn:hover{opacity:1;filter:alpha(opacity=100)}.ShareBtns .Tumblr-Btn{color:#35506d}.ShareBtns .Tumblr-Btn:hover{color:#35506d;background-color:#3593d3}.ShareBtns .Facebook-Btn{color:#3b5998}.ShareBtns .Facebook-Btn:hover{color:#3b5998;background-color:#4e91f2}.ShareBtns .Twitter-Btn{color:#00aced}.ShareBtns .Twitter-Btn:hover{color:#00aced;background-color:#94e8ff}.ShareBtns .Mail-Btn{background-position:-1px 161px;color:#0a80e3}.ShareBtns .Mail-Btn:hover{color:#0a80e3;background-color:#94e8ff}.ShareBtns .StickyPholder{display:inline-block}.ShareBtns .Viewer-Close-Btn{background-position:0 185px;padding:5px;margin-left:40px}.ShareBtns .Viewer-Close-Btn.Sticky{margin-left:-19px}#ModalSticker{max-width:1180px}#ModalSticker.Sticky{display:block!important;opacity:1;filter:alpha(opacity=100)}#ModalSticker .content-modal{border-bottom-width:3px;border-color:#000}#ModalSticker .ShareBtns{position:relative;float:right;margin-top:5px}.Modal-Credit{max-width:200px}.video-stage{margin-right:240px}.video-side-stage{width:240px}.Nav-Start{display:none}.First-Item .Nav-Start{display:block}.First-Item .Nav-Next,.First-Item .Nav-Prev{display:none}.slide-nav-arrow{background-color:#009bfb}.slide-nav-arrow:hover{opacity:1;filter:alpha(opacity=100)}.modal-sidekick,.modal-sidekick-following{display:inline-block}.js-sidekick-container{border-top:1px solid #f1f1f5}.modal-tooltip:hover .modal-tooltip\:h_H\(a\){height:auto!important}.modal-tooltip:hover .modal-tooltip\:h_Op\(1\){opacity:1!important}.c-modal-red{color:#ff156f}.lrec-before-loading{width:300px;height:250px;border:1px solid #e0e4e9;-webkit-transition:height 1.5s;-moz-transition:height 1.5s;-ms-transition:height 1.5s;-o-transition:height 1.5s;transition:height 1.5s}.js-header-searchbox{display:none;position:absolute;height:30px;left:730px;top:9px;border-radius:2px}.js-header-searchbox .srch-input{width:268px;height:100%;line-height:inherit;vertical-align:top;outline:0;box-shadow:none;border:none;border:1px solid #b0b0b0;border-radius:2px 0 0 2px;padding:0 10px}.js-header-searchbox .srch-input:focus{border-color:#0179ff}.js-header-searchbox button{width:32px;height:100%;background-color:#0179ff;font-size:16px;font-weight:700;color:#fff;border-radius:0 2px 2px 0;margin-left:-4px}.Z-6{z-index:6}.Z-7{z-index:7}.Z-8{z-index:8}.Z-9{z-index:9}.fixZindex{z-index:5!important}.viewer-right .js-slider-item.multirightrail.first{margin-top:25px!important}.viewer-right .Viewer-Close-Btn{border-radius:20px!important;font-size:17px!important;height:37px!important;margin-left:-18px!important;top:87px!important;width:37px!important}.viewer-right .viewer-wrapper{border-radius:6px;margin:1px!important;padding:0 0 30px 40px!important}.viewer-right.stream-sparse .viewer-wrapper{top:-12px}@media screen and (min-width:1100px){.js-header-searchbox{display:inline-block}}.Pt-35{padding-top:35px}.Pt-7{padding-top:7px}.JsEnabled .ImageLoader-Delayed{background:#f5f5f5}</style>
 

<style>#applet_p_63794 .Bd-0{border:0}#applet_p_63794 .Bd-1{border-width:1px}#applet_p_63794 .Bdx-1{border-right-width:1px;border-left-width:1px}#applet_p_63794 .Bdy-1{border-top-width:1px;border-bottom-width:1px}#applet_p_63794 .Bd-t{border-top-width:1px}#applet_p_63794 .Bd-end{border-right-width:1px}#applet_p_63794 .Bd-b{border-bottom-width:1px}#applet_p_63794 .Bd-start{border-left-width:1px}#applet_p_63794 .Bdt-0{border-top:0}#applet_p_63794 .Bdend-0{border-right:0}#applet_p_63794 .Bdb-0{border-bottom:0}#applet_p_63794 .Bdstart-0{border-left:0}#applet_p_63794 .Bdrs-0{border-radius:0}#applet_p_63794 .Bdtrrs-0{border-top-right-radius:0}#applet_p_63794 .Bdtlrs-0{border-top-left-radius:0}#applet_p_63794 .Bdbrrs-0{border-bottom-right-radius:0}#applet_p_63794 .Bdblrs-0{border-bottom-left-radius:0}#applet_p_63794 .Bdrs-100{border-radius:100px}#applet_p_63794 .Bdtrrs-100{border-top-right-radius:100px}#applet_p_63794 .Bdtlrs-100{border-top-left-radius:100px}#applet_p_63794 .Bdbrrs-100{border-bottom-right-radius:100px}#applet_p_63794 .Bdblrs-100{border-bottom-left-radius:100px}#applet_p_63794 .Bdrs-300{border-radius:300px}#applet_p_63794 .Bdtrrs-300{border-top-right-radius:300px}#applet_p_63794 .Bdtlrs-300{border-top-left-radius:300px}#applet_p_63794 .Bdbrrs-300{border-bottom-right-radius:300px}#applet_p_63794 .Bdblrs-300{border-bottom-left-radius:300px}#applet_p_63794 .Bdrs{border-radius:3px}#applet_p_63794 .Bdtrrs{border-top-right-radius:3px}#applet_p_63794 .Bdtlrs{border-top-left-radius:3px}#applet_p_63794 .Bdbrrs{border-bottom-right-radius:3px}#applet_p_63794 .Bdblrs{border-bottom-left-radius:3px}#applet_p_63794 .Ff{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-a{font-family:Georgia,"Times New Roman",serif}#applet_p_63794 .Ff-b{font-family:Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-c{font-family:"Monotype Corsiva","Comic Sans MS",cursive}#applet_p_63794 .Ff-d{font-family:Capitals,Impact,fantasy}#applet_p_63794 .Ff-e{font-family:Monaco,"Courier New",monospace}#applet_p_63794 .Fz-0{font-size:0}#applet_p_63794 .Fz-3xs{font-size:7px}#applet_p_63794 .Fz-2xs{font-size:9px}#applet_p_63794 .Fz-xs{font-size:11px;}#applet_p_63794 .Fz-s{font-size:13px;}#applet_p_63794 .Fz-m{font-size:15px;}#applet_p_63794 .Bgc-t{background-color:transparent}#applet_p_63794 .Bgi-n{background-image:none}#applet_p_63794 .Bg-n{background:0 0}#applet_p_63794 .Bgcp-bb{background-clip:border-box}#applet_p_63794 .Bgcp-pb{background-clip:padding-box}#applet_p_63794 .Bgcp-cb{background-clip:content-box}#applet_p_63794 .Bgo-pb{background-origin:padding-box}#applet_p_63794 .Bgo-bb{background-origin:border-box}#applet_p_63794 .Bgo-cb{background-origin:content-box}#applet_p_63794 .Bgz-a{background-size:auto}#applet_p_63794 .Bgz-ct{background-size:contain}#applet_p_63794 .Bgz-cv{background-size:cover;background-position:50% 15%}#applet_p_63794 .Bdcl-c{border-collapse:collapse}#applet_p_63794 .Bdcl-s{border-collapse:separate}#applet_p_63794 .Bxz-cb{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}#applet_p_63794 .Bxz-bb{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#applet_p_63794 .Bxsh-n{box-shadow:none}#applet_p_63794 .Cl-n{clear:none}#applet_p_63794 .Cl-b{clear:both}#applet_p_63794 .Cl-start{clear:left}#applet_p_63794 .Cl-end{clear:right}#applet_p_63794 .Cur-d{cursor:default}#applet_p_63794 .Cur-he{cursor:help}#applet_p_63794 .Cur-m{cursor:move}#applet_p_63794 .Cur-na{cursor:not-allowed}#applet_p_63794 .Cur-nsr{cursor:ns-resize}#applet_p_63794 .Cur-p{cursor:pointer}#applet_p_63794 .Cur-cr{cursor:col-resize}#applet_p_63794 .Cur-w{cursor:wait}#applet_p_63794 .Cur-a{cursor:auto!important}#applet_p_63794 .D-n{display:none}#applet_p_63794 .D-b{display:block}#applet_p_63794 .D-i{display:inline}#applet_p_63794 .D-ib{display:inline-block;*display:inline;zoom:1}#applet_p_63794 .D-tb{display:table}#applet_p_63794 .D-tbr{display:table-row}#applet_p_63794 .D-tbc{display:table-cell}#applet_p_63794 .D-li{display:list-item}#applet_p_63794 .D-ri{display:run-in}#applet_p_63794 .D-cp{display:compact}#applet_p_63794 .D-itb{display:inline-table}#applet_p_63794 .D-tbcl{display:table-column}#applet_p_63794 .D-tbclg{display:table-column-group}#applet_p_63794 .D-tbhg{display:table-header-group}#applet_p_63794 .D-tbfg{display:table-footer-group}#applet_p_63794 .D-tbrg{display:table-row-group}#applet_p_63794 .Fl-n{float:none}#applet_p_63794 .Fl-start{float:left;_display:inline}#applet_p_63794 .Fl-end{float:right;_display:inline}#applet_p_63794 .Fw-n{font-weight:400}#applet_p_63794 .Fw-b{font-weight:700;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}#applet_p_63794 .Fw-100{font-weight:100;*font-weight:normal}#applet_p_63794 .Fw-200{font-weight:200;*font-weight:normal}#applet_p_63794 .Fw-400{font-weight:400;*font-weight:normal}#applet_p_63794 .Fw-br{font-weight:bolder}#applet_p_63794 .Fw-lr{font-weight:lighter}#applet_p_63794 .Fs-n{font-style:normal}#applet_p_63794 .Fs-i{font-style:italic}#applet_p_63794 .Fv-sc{font-variant:small-caps}#applet_p_63794 .Fv-n{font-variant:normal}#applet_p_63794 .H-0{height:0}#applet_p_63794 .H-50{height:50%}#applet_p_63794 .H-100{height:100%}#applet_p_63794 .H-a{height:auto}#applet_p_63794 .H-n,#applet_p_63794 .h-n{-webkit-hyphens:none;-moz-hyphens:none;-ms-hyphens:none;hyphens:none}#applet_p_63794 .List-n{list-style-type:none}#applet_p_63794 .List-d{list-style-type:disc}#applet_p_63794 .List-c{list-style-type:circle}#applet_p_63794 .List-s{list-style-type:square}#applet_p_63794 .List-dc{list-style-type:decimal}#applet_p_63794 .List-dclz{list-style-type:decimal-leading-zero}#applet_p_63794 .List-lr{list-style-type:lower-roman}#applet_p_63794 .List-ur{list-style-type:upper-roman}#applet_p_63794 .Lisi-n{list-style-image:none}#applet_p_63794 .Lh-0{line-height:0}#applet_p_63794 .Lh-01{line-height:.1}#applet_p_63794 .Lh-02{line-height:.2}#applet_p_63794 .Lh-03{line-height:.3}#applet_p_63794 .Lh-04{line-height:.4}#applet_p_63794 .Lh-05{line-height:.5}#applet_p_63794 .Lh-06{line-height:.6}#applet_p_63794 .Lh-07{line-height:.7}#applet_p_63794 .Lh-08{line-height:.8}#applet_p_63794 .Lh-09{line-height:.9}#applet_p_63794 .Lh-1{line-height:1}#applet_p_63794 .Lh-11{line-height:1.1}#applet_p_63794 .Lh-12{line-height:1.2}#applet_p_63794 .Lh-125{line-height:1.25}#applet_p_63794 .Lh-13{line-height:1.3}#applet_p_63794 .Lh-14{line-height:1.4}#applet_p_63794 .Lh-15{line-height:1.5}#applet_p_63794 .Lh-16{line-height:1.6}#applet_p_63794 .Lh-17{line-height:1.7}#applet_p_63794 .Lh-18{line-height:1.8}#applet_p_63794 .Lh-19{line-height:1.9}#applet_p_63794 .Lh-2{line-height:2}#applet_p_63794 .Lh-21{line-height:2.1}#applet_p_63794 .Lh-22{line-height:2.2}#applet_p_63794 .Lh-23{line-height:2.3}#applet_p_63794 .Lh-24{line-height:2.4}#applet_p_63794 .Lh-25{line-height:2.5}#applet_p_63794 .Lh-3{line-height:3}#applet_p_63794 .Lh-reset{line-height:normal}#applet_p_63794 .M-0{margin:0}#applet_p_63794 .M-2{margin:2px}#applet_p_63794 .M-4{margin:4px}#applet_p_63794 .M-6{margin:6px}#applet_p_63794 .M-8{margin:8px}#applet_p_63794 .M-10{margin:10px}#applet_p_63794 .M-12{margin:12px}#applet_p_63794 .M-14{margin:14px}#applet_p_63794 .M-16{margin:16px}#applet_p_63794 .M-18{margin:18px}#applet_p_63794 .M-20{margin:20px}#applet_p_63794 .Mx-1{margin-right:1px;margin-left:1px}#applet_p_63794 .Mx-2{margin-right:2px;margin-left:2px}#applet_p_63794 .Mx-4{margin-right:4px;margin-left:4px}#applet_p_63794 .Mx-6{margin-right:6px;margin-left:6px}#applet_p_63794 .Mx-8{margin-right:8px;margin-left:8px}#applet_p_63794 .Mx-10{margin-right:10px;margin-left:10px}#applet_p_63794 .Mx-12{margin-right:12px;margin-left:12px}#applet_p_63794 .Mx-14{margin-right:14px;margin-left:14px}#applet_p_63794 .Mx-16{margin-right:16px;margin-left:16px}#applet_p_63794 .Mx-18{margin-right:18px;margin-left:18px}#applet_p_63794 .Mx-20{margin-right:20px;margin-left:20px}#applet_p_63794 .My-1{margin-top:1px;margin-bottom:1px}#applet_p_63794 .My-2{margin-top:2px;margin-bottom:2px}#applet_p_63794 .My-4{margin-top:4px;margin-bottom:4px}#applet_p_63794 .My-6{margin-top:6px;margin-bottom:6px}#applet_p_63794 .My-8{margin-top:8px;margin-bottom:8px}#applet_p_63794 .My-10{margin-top:10px;margin-bottom:10px}#applet_p_63794 .My-12{margin-top:12px;margin-bottom:12px}#applet_p_63794 .My-14{margin-top:14px;margin-bottom:14px}#applet_p_63794 .My-16{margin-top:16px;margin-bottom:16px}#applet_p_63794 .My-18{margin-top:18px;margin-bottom:18px}#applet_p_63794 .My-20{margin-top:20px;margin-bottom:20px}#applet_p_63794 .Mt-1{margin-top:1px}#applet_p_63794 .Mb-1{margin-bottom:1px}#applet_p_63794 .Mstart-1{margin-left:1px}#applet_p_63794 .Mend-1{margin-right:1px}#applet_p_63794 .Mt-2{margin-top:2px}#applet_p_63794 .Mb-2{margin-bottom:2px}#applet_p_63794 .Mstart-2{margin-left:2px}#applet_p_63794 .Mend-2{margin-right:2px}#applet_p_63794 .Mt-4{margin-top:4px}#applet_p_63794 .Mb-4{margin-bottom:4px}#applet_p_63794 .Mstart-4{margin-left:4px}#applet_p_63794 .Mend-4{margin-right:4px}#applet_p_63794 .Mt-6{margin-top:6px}#applet_p_63794 .Mb-6{margin-bottom:6px}#applet_p_63794 .Mstart-6{margin-left:6px}#applet_p_63794 .Mend-6{margin-right:6px}#applet_p_63794 .Mt-8{margin-top:8px}#applet_p_63794 .Mb-8{margin-bottom:8px}#applet_p_63794 .Mstart-8{margin-left:8px}#applet_p_63794 .Mend-8{margin-right:8px}#applet_p_63794 .Mt-10{margin-top:10px}#applet_p_63794 .Mb-10{margin-bottom:10px}#applet_p_63794 .Mstart-10{margin-left:10px}#applet_p_63794 .Mend-10{margin-right:10px}#applet_p_63794 .Mt-12{margin-top:12px}#applet_p_63794 .Mb-12{margin-bottom:12px}#applet_p_63794 .Mstart-12{margin-left:12px}#applet_p_63794 .Mend-12{margin-right:12px}#applet_p_63794 .Mt-14{margin-top:14px}#applet_p_63794 .Mb-14{margin-bottom:14px}#applet_p_63794 .Mstart-14{margin-left:14px}#applet_p_63794 .Mend-14{margin-right:14px}#applet_p_63794 .Mt-16{margin-top:16px}#applet_p_63794 .Mb-16{margin-bottom:16px}#applet_p_63794 .Mstart-16{margin-left:16px}#applet_p_63794 .Mend-16{margin-right:16px}#applet_p_63794 .Mt-18{margin-top:18px}#applet_p_63794 .Mb-18{margin-bottom:18px}#applet_p_63794 .Mstart-18{margin-left:18px}#applet_p_63794 .Mend-18{margin-right:18px}#applet_p_63794 .Mt-20{margin-top:20px}#applet_p_63794 .Mb-20{margin-bottom:20px}#applet_p_63794 .Mstart-20{margin-left:20px}#applet_p_63794 .Mend-20{margin-right:20px}#applet_p_63794 .Mt-30{margin-top:30px}#applet_p_63794 .Mb-30{margin-bottom:30px}#applet_p_63794 .Mstart-30{margin-left:30px}#applet_p_63794 .Mend-30{margin-right:30px}#applet_p_63794 .Mt-40{margin-top:40px}#applet_p_63794 .Mb-40{margin-bottom:40px}#applet_p_63794 .Mstart-40{margin-left:40px}#applet_p_63794 .Mend-40{margin-right:40px}#applet_p_63794 .Mt-50{margin-top:50px}#applet_p_63794 .Mb-50{margin-bottom:50px}#applet_p_63794 .Mstart-50{margin-left:50px}#applet_p_63794 .Mend-50{margin-right:50px}#applet_p_63794 .Mt-60{margin-top:60px}#applet_p_63794 .Mb-60{margin-bottom:60px}#applet_p_63794 .Mstart-60{margin-left:60px}#applet_p_63794 .Mend-60{margin-right:60px}#applet_p_63794 .Mt-70{margin-top:70px}#applet_p_63794 .Mb-70{margin-bottom:70px}#applet_p_63794 .Mstart-70{margin-left:70px}#applet_p_63794 .Mend-70{margin-right:70px}#applet_p_63794 .Mt-neg-1{margin-top:-1px}#applet_p_63794 .Mb-neg-1{margin-bottom:-1px}#applet_p_63794 .Mstart-neg-1{margin-left:-1px}#applet_p_63794 .Mend-neg-1{margin-right:-1px}#applet_p_63794 .Mt-neg-4{margin-top:-4px}#applet_p_63794 .Mb-neg-4{margin-bottom:-4px}#applet_p_63794 .Mstart-neg-4{margin-left:-4px}#applet_p_63794 .Mend-neg-4{margin-right:-4px}#applet_p_63794 .Mt-neg-6{margin-top:-6px}#applet_p_63794 .Mb-neg-6{margin-bottom:-6px}#applet_p_63794 .Mstart-neg-6{margin-left:-6px}#applet_p_63794 .Mend-neg-6{margin-right:-6px}#applet_p_63794 .Mt-neg-8{margin-top:-8px}#applet_p_63794 .Mb-neg-8{margin-bottom:-8px}#applet_p_63794 .Mstart-neg-8{margin-left:-8px}#applet_p_63794 .Mend-neg-8{margin-right:-8px}#applet_p_63794 .Mt-neg-10{margin-top:-10px}#applet_p_63794 .Mb-neg-10{margin-bottom:-10px}#applet_p_63794 .Mstart-neg-10{margin-left:-10px}#applet_p_63794 .Mend-neg-10{margin-right:-10px}#applet_p_63794 .Mt-neg-12{margin-top:-12px}#applet_p_63794 .Mb-neg-12{margin-bottom:-12px}#applet_p_63794 .Mstart-neg-12{margin-left:-12px}#applet_p_63794 .Mend-neg-12{margin-right:-12px}#applet_p_63794 .Mt-neg-14{margin-top:-14px}#applet_p_63794 .Mb-neg-14{margin-bottom:-14px}#applet_p_63794 .Mstart-neg-14{margin-left:-14px}#applet_p_63794 .Mend-neg-14{margin-right:-14px}#applet_p_63794 .Mt-neg-16{margin-top:-16px}#applet_p_63794 .Mb-neg-16{margin-bottom:-16px}#applet_p_63794 .Mstart-neg-16{margin-left:-16px}#applet_p_63794 .Mend-neg-16{margin-right:-16px}#applet_p_63794 .Mt-neg-18{margin-top:-18px}#applet_p_63794 .Mb-neg-18{margin-bottom:-18px}#applet_p_63794 .Mstart-neg-18{margin-left:-18px}#applet_p_63794 .Mend-neg-18{margin-right:-18px}#applet_p_63794 .Mt-neg-20{margin-top:-20px}#applet_p_63794 .Mb-neg-20{margin-bottom:-20px}#applet_p_63794 .Mstart-neg-20{margin-left:-20px}#applet_p_63794 .Mend-neg-20{margin-right:-20px}#applet_p_63794 .Mstart-50\%{margin-left:50%}#applet_p_63794 .M-a{margin:auto}#applet_p_63794 .Mx-a{margin-right:auto;margin-left:auto}#applet_p_63794 .\!Mx-a{margin-right:auto!important;margin-left:auto!important}#applet_p_63794 .Mstart-a{margin-left:auto}#applet_p_63794 .Mend-a{margin-right:auto}#applet_p_63794 .Mt-0,#applet_p_63794 .My-0{margin-top:0}#applet_p_63794 .Mb-0,#applet_p_63794 .My-0{margin-bottom:0}#applet_p_63794 .Mstart-0,#applet_p_63794 .Mx-0{margin-left:0}#applet_p_63794 .Mend-0,#applet_p_63794 .Mx-0{margin-right:0}#applet_p_63794 .Miw-0{min-width:0}#applet_p_63794 .Miw-10{min-width:10%}#applet_p_63794 .Miw-15{min-width:15%}#applet_p_63794 .Miw-20{min-width:20%}#applet_p_63794 .Miw-25{min-width:25%}#applet_p_63794 .Miw-30{min-width:30%}#applet_p_63794 .Miw-35{min-width:35%}#applet_p_63794 .Miw-40{min-width:40%}#applet_p_63794 .Miw-45{min-width:45%}#applet_p_63794 .Miw-50{min-width:50%}#applet_p_63794 .Miw-60{min-width:60%}#applet_p_63794 .Miw-70{min-width:70%}#applet_p_63794 .Miw-80{min-width:80%}#applet_p_63794 .Miw-90{min-width:90%}#applet_p_63794 .Miw-100{min-width:100%}#applet_p_63794 .Maw-n{max-width:none}#applet_p_63794 .Maw-10{max-width:10%}#applet_p_63794 .Maw-15{max-width:15%}#applet_p_63794 .Maw-20{max-width:20%}#applet_p_63794 .Maw-25{max-width:25%}#applet_p_63794 .Maw-30{max-width:30%}#applet_p_63794 .Maw-35{max-width:35%}#applet_p_63794 .Maw-40{max-width:40%}#applet_p_63794 .Maw-45{max-width:45%}#applet_p_63794 .Maw-50{max-width:50%}#applet_p_63794 .Maw-60{max-width:60%}#applet_p_63794 .Maw-70{max-width:70%}#applet_p_63794 .Maw-80{max-width:80%}#applet_p_63794 .Maw-90{max-width:90%}#applet_p_63794 .Maw-99{max-width:99%}#applet_p_63794 .Maw-100{max-width:100%}#applet_p_63794 .Mih-0{min-height:0}#applet_p_63794 .Mih-50{min-height:50%;_height:50%}#applet_p_63794 .Mih-60{min-height:60%;_height:60%}#applet_p_63794 .Mih-100{min-height:100%;_height:100%}#applet_p_63794 .Mah-n{max-height:none}#applet_p_63794 .Mah-0{max-height:0}#applet_p_63794 .Mah-50{max-height:50%}#applet_p_63794 .Mah-60{max-height:60%}#applet_p_63794 .Mah-100{max-height:100%}#applet_p_63794 .O-0{outline:0}#applet_p_63794 .T-0{top:0}#applet_p_63794 .B-0{bottom:0}#applet_p_63794 .Start-0{left:0}#applet_p_63794 .End-0{right:0}#applet_p_63794 .T-10{top:10%}#applet_p_63794 .B-10{bottom:10%}#applet_p_63794 .Start-10{left:10%}#applet_p_63794 .End-10{right:10%}#applet_p_63794 .T-25{top:25%}#applet_p_63794 .B-25{bottom:25%}#applet_p_63794 .Start-25{left:25%}#applet_p_63794 .End-25{right:25%}#applet_p_63794 .T-50{top:50%}#applet_p_63794 .B-50{bottom:50%}#applet_p_63794 .Start-50{left:50%}#applet_p_63794 .End-50{right:50%}#applet_p_63794 .T-75{top:75%}#applet_p_63794 .B-75{bottom:75%}#applet_p_63794 .Start-75{left:75%}#applet_p_63794 .End-75{right:75%}#applet_p_63794 .T-100{top:100%}#applet_p_63794 .B-100{bottom:100%}#applet_p_63794 .Start-100{left:100%}#applet_p_63794 .End-100{right:100%}#applet_p_63794 .T-a{top:auto!important}#applet_p_63794 .B-a{bottom:auto!important}#applet_p_63794 .Start-a{left:auto!important}#applet_p_63794 .End-a{right:auto!important}#applet_p_63794 .Op-0{opacity:0;filter:alpha(opacity=0)}#applet_p_63794 .Op-33{opacity:.33;filter:alpha(opacity=33)}#applet_p_63794 .Op-50{opacity:.5;filter:alpha(opacity=50)}#applet_p_63794 .Op-66{opacity:.66;filter:alpha(opacity=66)}#applet_p_63794 .Op-100{opacity:1;filter:alpha(opacity=100)}#applet_p_63794 .Ov-h{overflow:hidden;zoom:1}#applet_p_63794 .Ov-v{overflow:visible}#applet_p_63794 .Ov-s{overflow:scroll}#applet_p_63794 .Ov-a{overflow:auto}#applet_p_63794 .Ovs-t{-webkit-overflow-scrolling:touch}#applet_p_63794 .Ovx-v{overflow-x:visible}#applet_p_63794 .Ovx-h{overflow-x:hidden}#applet_p_63794 .Ovx-s{overflow-x:scroll}#applet_p_63794 .Ovx-a{overflow-x:auto}#applet_p_63794 .Ovy-v{overflow-y:visible}#applet_p_63794 .Ovy-h{overflow-y:hidden}#applet_p_63794 .Ovy-s{overflow-y:scroll}#applet_p_63794 .Ovy-a{overflow-y:auto}#applet_p_63794 .P-0{padding:0}#applet_p_63794 .P-1{padding:1px}#applet_p_63794 .P-2{padding:2px}#applet_p_63794 .P-4{padding:4px}#applet_p_63794 .P-6{padding:6px}#applet_p_63794 .P-8{padding:8px}#applet_p_63794 .P-10{padding:10px}#applet_p_63794 .P-12{padding:12px}#applet_p_63794 .P-14{padding:14px}#applet_p_63794 .P-16{padding:16px}#applet_p_63794 .P-18{padding:18px}#applet_p_63794 .P-20{padding:20px}#applet_p_63794 .P-30{padding:30px}#applet_p_63794 .Px-1{padding-right:1px;padding-left:1px}#applet_p_63794 .Px-2{padding-right:2px;padding-left:2px}#applet_p_63794 .Px-4{padding-right:4px;padding-left:4px}#applet_p_63794 .Px-6{padding-right:6px;padding-left:6px}#applet_p_63794 .Px-8{padding-right:8px;padding-left:8px}#applet_p_63794 .Px-10{padding-right:10px;padding-left:10px}#applet_p_63794 .Px-12{padding-right:12px;padding-left:12px}#applet_p_63794 .Px-14{padding-right:14px;padding-left:14px}#applet_p_63794 .Px-16{padding-right:16px;padding-left:16px}#applet_p_63794 .Px-18{padding-right:18px;padding-left:18px}#applet_p_63794 .Px-20{padding-right:20px;padding-left:20px}#applet_p_63794 .Px-30{padding-right:30px;padding-left:30px}#applet_p_63794 .Py-1{padding-top:1px;padding-bottom:1px}#applet_p_63794 .Py-2{padding-top:2px;padding-bottom:2px}#applet_p_63794 .Py-4{padding-top:4px;padding-bottom:4px}#applet_p_63794 .Py-6{padding-top:6px;padding-bottom:6px}#applet_p_63794 .Py-8{padding-top:8px;padding-bottom:8px}#applet_p_63794 .Py-10{padding-top:10px;padding-bottom:10px}#applet_p_63794 .Py-12{padding-top:12px;padding-bottom:12px}#applet_p_63794 .Py-14{padding-top:14px;padding-bottom:14px}#applet_p_63794 .Py-16{padding-top:16px;padding-bottom:16px}#applet_p_63794 .Py-18{padding-top:18px;padding-bottom:18px}#applet_p_63794 .Py-20{padding-top:20px;padding-bottom:20px}#applet_p_63794 .Py-30{padding-top:30px;padding-bottom:30px}#applet_p_63794 .Pt-1{padding-top:1px}#applet_p_63794 .Pb-1{padding-bottom:1px}#applet_p_63794 .Pstart-1{padding-left:1px}#applet_p_63794 .Pend-1{padding-right:1px}#applet_p_63794 .Pt-2{padding-top:2px}#applet_p_63794 .Pb-2{padding-bottom:2px}#applet_p_63794 .Pstart-2{padding-left:2px}#applet_p_63794 .Pend-2{padding-right:2px}#applet_p_63794 .Pt-4{padding-top:4px}#applet_p_63794 .Pb-4{padding-bottom:4px}#applet_p_63794 .Pstart-4{padding-left:4px}#applet_p_63794 .Pend-4{padding-right:4px}#applet_p_63794 .Pt-6{padding-top:6px}#applet_p_63794 .Pb-6{padding-bottom:6px}#applet_p_63794 .Pstart-6{padding-left:6px}#applet_p_63794 .Pend-6{padding-right:6px}#applet_p_63794 .Pt-8{padding-top:8px}#applet_p_63794 .Pb-8{padding-bottom:8px}#applet_p_63794 .Pstart-8{padding-left:8px}#applet_p_63794 .Pend-8{padding-right:8px}#applet_p_63794 .Pt-10{padding-top:10px}#applet_p_63794 .Pb-10{padding-bottom:10px}#applet_p_63794 .Pstart-10{padding-left:10px}#applet_p_63794 .Pend-10{padding-right:10px}#applet_p_63794 .Pt-12{padding-top:12px}#applet_p_63794 .Pb-12{padding-bottom:12px}#applet_p_63794 .Pstart-12{padding-left:12px}#applet_p_63794 .Pend-12{padding-right:12px}#applet_p_63794 .Pt-14{padding-top:14px}#applet_p_63794 .Pb-14{padding-bottom:14px}#applet_p_63794 .Pstart-14{padding-left:14px}#applet_p_63794 .Pend-14{padding-right:14px}#applet_p_63794 .Pt-16{padding-top:16px}#applet_p_63794 .Pb-16{padding-bottom:16px}#applet_p_63794 .Pstart-16{padding-left:16px}#applet_p_63794 .Pend-16{padding-right:16px}#applet_p_63794 .Pt-18{padding-top:18px}#applet_p_63794 .Pb-18{padding-bottom:18px}#applet_p_63794 .Pstart-18{padding-left:18px}#applet_p_63794 .Pend-18{padding-right:18px}#applet_p_63794 .Pt-20{padding-top:20px}#applet_p_63794 .Pend-20{padding-right:20px}#applet_p_63794 .Pb-20{padding-bottom:20px}#applet_p_63794 .Pstart-20{padding-left:20px}#applet_p_63794 .Pt-30{padding-top:30px}#applet_p_63794 .Pend-30{padding-right:30px}#applet_p_63794 .Pb-30{padding-bottom:30px}#applet_p_63794 .Pstart-30{padding-left:30px}#applet_p_63794 .Pt-0,#applet_p_63794 .Py-0{padding-top:0}#applet_p_63794 .Pb-0,#applet_p_63794 .Py-0{padding-bottom:0}#applet_p_63794 .Pstart-0,#applet_p_63794 .Px-0{padding-left:0}#applet_p_63794 .Pend-0,#applet_p_63794 .Px-0{padding-right:0}#applet_p_63794 .Pe-n{pointer-events:none}#applet_p_63794 .Pe-a{pointer-events:auto}#applet_p_63794 .Pos-s{position:static}#applet_p_63794 .Pos-a{position:absolute}#applet_p_63794 .Pos-r{position:relative}#applet_p_63794 .Pos-f{position:fixed}#applet_p_63794 .Ws-n::-webkit-scrollbar{-webkit-appearance:none}#applet_p_63794 .Tbl-f{table-layout:fixed}#applet_p_63794 .Tbl-a{table-layout:auto}#applet_p_63794 .Ta-c{text-align:center}#applet_p_63794 .Ta-j{text-align:justify}#applet_p_63794 .Ta-start{text-align:left}#applet_p_63794 .Ta-end{text-align:right}#applet_p_63794 .Td-n{text-decoration:none!important}#applet_p_63794 .Td-u,#applet_p_63794 .Td-u\:h:hover{text-decoration:underline}#applet_p_63794 .Tr-a{-webkit-text-rendering:auto;-moz-text-rendering:auto;-ms-text-rendering:auto;-o-text-rendering:auto;text-rendering:auto}#applet_p_63794 .Tr-os{-webkit-text-rendering:optimizeSpeed;-moz-text-rendering:optimizeSpeed;-ms-text-rendering:optimizeSpeed;-o-text-rendering:optimizeSpeed;text-rendering:optimizeSpeed}#applet_p_63794 .Tr-ol{-webkit-text-rendering:optimizeLegibility;-moz-text-rendering:optimizeLegibility;-ms-text-rendering:optimizeLegibility;-o-text-rendering:optimizeLegibility;text-rendering:optimizeLegibility}#applet_p_63794 .Tr-gp{-webkit-text-rendering:geometricPrecision;-moz-text-rendering:geometricPrecision;-ms-text-rendering:geometricPrecision;-o-text-rendering:geometricPrecision;text-rendering:geometricPrecision}#applet_p_63794 .Tr-i{-webkit-text-rendering:inherit;-moz-text-rendering:inherit;-ms-text-rendering:inherit;-o-text-rendering:inherit;text-rendering:inherit}#applet_p_63794 .Tt-n{text-transform:none}#applet_p_63794 .Tt-u{text-transform:uppercase}#applet_p_63794 .Tt-c{text-transform:capitalize}#applet_p_63794 .Tt-l{text-transform:lowercase}#applet_p_63794 .Tsh{text-shadow:0 1px 1px #000}#applet_p_63794 .Tsh-n{text-shadow:none}#applet_p_63794 .Us-n{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}#applet_p_63794 .Va-sup{vertical-align:super}#applet_p_63794 .Va-t{vertical-align:top}#applet_p_63794 .Va-tt{vertical-align:text-top}#applet_p_63794 .Va-m{vertical-align:middle}#applet_p_63794 .Va-bl{vertical-align:baseline}#applet_p_63794 .Va-b{vertical-align:bottom}#applet_p_63794 .Va-tb{vertical-align:text-bottom}#applet_p_63794 .Va-sub{vertical-align:sub}#applet_p_63794 .V-v{visibility:visible}#applet_p_63794 .V-h{visibility:hidden}#applet_p_63794 .V-c{visibility:collapse}#applet_p_63794 .Whs-nw{white-space:nowrap}#applet_p_63794 .Whs-n{white-space:normal}#applet_p_63794 .W-a{width:auto}#applet_p_63794 .W-0{width:0}#applet_p_63794 .W-1{width:1%}#applet_p_63794 .W-10{width:10%;*width:9.6%}#applet_p_63794 .W-15{width:15%;*width:14.8%}#applet_p_63794 .W-20{width:20%;*width:19.5%}#applet_p_63794 .W-25{width:25%;*width:24.5%}#applet_p_63794 .W-30{width:30%;*width:29.6%}#applet_p_63794 .W-33{width:33.33%;*width:33%}#applet_p_63794 .W-35{width:35%;*width:34.9%}#applet_p_63794 .W-40{width:40%;*width:39.5%}#applet_p_63794 .W-45{width:45%;*width:44.9%}#applet_p_63794 .W-50{width:50%;*width:49.5%}#applet_p_63794 .W-55{width:55%;*width:54.8%}#applet_p_63794 .W-60{width:60%}#applet_p_63794 .W-66{width:66.66%}#applet_p_63794 .W-70{width:70%}#applet_p_63794 .W-75{width:75%}#applet_p_63794 .W-80{width:80%}#applet_p_63794 .W-90{width:90%}#applet_p_63794 .W-100{width:100%}#applet_p_63794 .Wpx-1{width:1px}#applet_p_63794 .Wpx-2{width:2px}#applet_p_63794 .Wpx-4{width:4px}#applet_p_63794 .Wpx-6{width:6px}#applet_p_63794 .Wpx-8{width:8px}#applet_p_63794 .Wpx-10{width:10px}#applet_p_63794 .Wpx-12{width:12px}#applet_p_63794 .Wpx-14{width:14px}#applet_p_63794 .Wpx-16{width:16px}#applet_p_63794 .Wpx-18{width:18px}#applet_p_63794 .Wpx-20{width:20px}#applet_p_63794 .Wpx-24{width:24px}#applet_p_63794 .Wpx-26{width:26px}#applet_p_63794 .Wpx-28{width:28px}#applet_p_63794 .Wpx-30{width:30px}#applet_p_63794 .Wpx-32{width:32px}#applet_p_63794 .Wob-ba{word-break:break-all}#applet_p_63794 .Wob-n{word-break:normal!important}#applet_p_63794 .Wow-bw{word-wrap:break-word}#applet_p_63794 .Wow-n{word-wrap:normal!important}#applet_p_63794 .Z-0{z-index:0}#applet_p_63794 .Z-1{z-index:1}#applet_p_63794 .Z-3{z-index:3}#applet_p_63794 .Z-5{z-index:5}#applet_p_63794 .Z-7{z-index:7}#applet_p_63794 .Z-10{z-index:10}#applet_p_63794 .Z-a{z-index:auto!important}#applet_p_63794 .Zoom-1{zoom:1}

#applet_p_63794 .StencilRoot input {
    font-size: 13px;    
}

#applet_p_63794 .StencilRoot button {
    font-size: 14px;
    line-height: normal;
}
</style>

    
    
    </head>
    <body class="my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr    stream-dense" dir="ltr">
        
        
        
                    <div id="darla-assets-top">
            <script type='text/javascript' src='/sy/rq/darla/2-9-9/js/g-r-min.js'></script>
            <script>
    var resourceTimingAssets = null;
    if (window.performance && window.performance.mark && window.performance.getEntriesByName) {
        resourceTimingAssets = {'darlaJsLoaded' : 'https://www.yahoo.com/sy/rq/darla/2-9-9/js/g-r-min.js'};
        window.performance.mark('darlaJsLoaded');
    }
</script>
            </div>
                <script type="text/javascript">
        var rapidPageConfig = {
            rapidEarlyConfig : {},
            rapidConfig: {"compr_type":"deflate","tracked_mods":[],"spaceid":2023538075,"click_timeout":300,"track_right_click":true,"apv":true,"apv_time":0,"yql_host":"","test_id":"201","client_only":0,"pageview_on_init":true,"perf_navigationtime":2,"addmodules_timeout":500,"keys":{"_rid":"2r783utbfats3","mrkt":"us","pt":1,"ver":"megastrm","uh_vw":0,"colo":"slw88.fp.ne1.yahoo.com","navtype":"server"},"viewability":true},
            rapidSingleInstance: 0,
            ywaCF: "",
            ywaActionMap: "",
            ywaOutcomeMap: "" 
        };

        if (rapidPageConfig.rapidEarlyConfig.keys) {
            rapidPageConfig.rapidEarlyConfig.keys.bw = document.body.offsetWidth;
            rapidPageConfig.rapidEarlyConfig.keys.bh = document.body.offsetHeight;
        }
        rapidPageConfig.rapidConfig.keys.bw = document.body.offsetWidth;
        rapidPageConfig.rapidConfig.keys.bh = document.body.offsetHeight;
        </script>
        
                            
                    
                    
                    
                    
                    
                    

    <div id="UH">
        <div id="UH-ColWrap">
            <div id="applet_p_30345894" class=" M-0 js-applet header Zoom-1  Mb-0 " data-applet-guid="p_30345894" data-applet-type="header" data-applet-params="_suid:30345894" data-i13n="auto:true;sec:hd" data-i13n-sec="hd" data-ylk="rspns:nav;t1:a1;t2:hd;itc:0;"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-uh-wrapper" class="Bds(n) Bdc($headerBdr) Scrolling_Bdc($headerBdrScroll) has-scrolled_Bdc($headerBdrScroll) Bdbw(1px) ua-ie7_Bdbs(s)! ua-ie8_Bdbs(s)! Scrolling_Bxsh($headerShadow) has-scrolled_Bxsh($headerShadow) Bgc(#fff) T(0) Start(0) End(0) Pos(f) Z(3) Zoom">
    
    <div id="mega-topbar" class="Pos(r) H(22px) mini-header_Mt(-19px) Reader-open_Mt(-19px) Bg($topbarBgc) Bxsh($topbarShadow) Z(7)"   data-ylk="rspns:nav;t1:a1;t2:hd;t3:tb;sec:hd;itc:0;elm:itm;elmt:pty;">
    <ul class="Pos(r) Miw(1000px) Pstart(9px) Lh(1.7) Reader-open_Op(0) mini-header_Op(0)" role="navigation">
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:home;t5:home;cpos:1;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) Icon-Fp2 IconHome"></i>Home</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mail.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mail;t5:mail;cpos:2;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mail</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.flickr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:flickr;t5:flickr;cpos:3;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Flickr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.tumblr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:tumblr;t5:tumblr;cpos:4;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Tumblr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://answers.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:answers;t5:answers;cpos:5;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Answers</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://groups.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:groups;t5:groups;cpos:6;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Groups</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mobile.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mobile;t5:mobile;cpos:7;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mobile</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(10px) navigation-menu">
            <a href="https://everything.yahoo.com/" class="navigation-menu-title Pos(r) C(#fff) Pstart(12px) Pend(24px) Py(4px) Fz(13px) menu-open_C($topbarMenu) menu-open_Bgc(#fff) menu-open_Z(8) rapidnofollow"   data-ylk="rspns:op;t5:more;slk:more;itc:1;elmt:mu;cpos:8;" tabindex="1">More<i class="Pos(a) Pt(2px) Pstart(6px) Fw(b) Icon-Fp2 IconDownCaret"></i></a>
            <div class="Pos(a) Bgc(#fff) Bxsh($topbarMenuShadow) Z(7) V(h) Op(0) menu-open_V(v) menu-open_Op(1)">
                <ul class="Px(12px) Py(5px)">
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/politics/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:politics;t5:politics;cpos:9;" tabindex="1">Politics</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/celebrity/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:celebrity;t5:celebrity;cpos:10;" tabindex="1">Celebrity</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/movies/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:movies;t5:movies;cpos:11;" tabindex="1">Movies</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/music/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:music;t5:music;cpos:12;" tabindex="1">Music</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tv/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tv;t5:tv;cpos:13;" tabindex="1">TV</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/style/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:style;t5:style;cpos:14;" tabindex="1">Style</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/beauty/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:beauty;t5:beauty;cpos:15;" tabindex="1">Beauty</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tech/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tech;t5:tech;cpos:16;" tabindex="1">Tech</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://shopping.yahoo.com/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:shopping;t5:shopping;cpos:17;" tabindex="1">Shopping</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/autos/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:autos;t5:autos;cpos:18;" tabindex="1">Autos</a>
                    </li>
                
                </ul>
            </div>
        </li>
    
    
    
    
        <li id="mega-top-ff-promo" class="Pos(a) End(0) D(ib) Mend(18px) Pstart(14px) D(n)!">
            <a class="C(#fff) Pstart(4px) Td(n)! Td(u)!:h Fz(13px)" href="https://www.mozilla.org/firefox/new/?utm_source=yahoo&amp;utm_medium=referral&amp;utm_campaign=y-uh&amp;utm_content=y-install-new-firefox" target="_blank" tabindex="1"   data-ylk="t5:ff-promo;slk:ff-promo;t4:pty-mu;">
                <img id="yucs-ff-img" class="Pend(4px) Va(m) ua-ie7_D(n)" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAADAFBMVEVMaXEAAAAAAAAAAAAAAAAAAADCOicAAAAAAAAAAACjICQAAADNXCvISyxtx+5wyvGbICONHiAAAACxNCLlfS3keCjhcSlhxO5swuqwLiRkxesAAAAAAABWst/fgi3pvhX73he6PiqzKCWSHSDBRinhbyfJWTLefC9QDxHneynZaC/dbCuuMCnshirukDVkxe1gx+5gxO1XptVYrNpPvejXWiVaERM3CwyLIR3YXSaN1/e5Xi5rGRUuDAmV1OvrkSKLq7LuoBLimiP3zE3e2Z361EL0ziTUoCX0uR7eXCXhYiXORSbdWCbfYCXbVCbXTyXiaibjbiYMXp/ZUSbJPifkdCbkcCXcVybGOybLQSYFU5QVbKzBNSffXybgZibQSSbjZyXneCXVTCYDotjmeyYGTYzogiTokCIKl9DsoR/ohyMFSYfnbCW6LCa9MiYWKlwRL2NEuekeeLgWhcHvrxz/4gKwISfwhyMYHUvcXycKq99hwu5PuurqmCETZ6gLOXE2tuc+tud4zfL+9sP/8JUYfrz4zwwfc7PzvB34+OP+50b95V//74PXWSX//fIVdLQRjMfTSibmgCZrPjwMNWv+6HMLR4Aos+Rpxe/OYyYHQXsPkMr1xh0qptr/+dZIvevfaCb2mCOqYTYisuRTvusXJVYdIVABqN3ndCWOU0K0WjMHndQYr+LrhSatIiXfcCW4KSYtWHLqonDPUSbVkDBkw+6INjgsKVB5ZFDXdiYmksQknr+5n0TitSYLWJgfqt711XROqtk3pNdJns7QlTD676z/7GH/7WujuLjufSNDMUZHTWt/clbjjSRgSDpMMD5Jiavos4myc1qaeGPQfTaNfze9Wy0/Z28hLV7ZrybheUcpR20qs+X71gcuPVr62UerQy/BcSx0n3k4mc6+vFXooB9VsN/RaiSOoWvCnUL4zTuckUaOy9HF5N7tlCB4gFcals53in9Vn8bS6+2MxeJ+sMTz+PuTuMl2lpuGs9SyxK8qcJzk7fTy0UFagZb83juJ9+V4AAAASXRSTlMADhgiCC/+AVME/Dv+/vb8/vJHNG3duPLt+rwTYn8t9/6he3eA8vPmZOZCue7pnuNpzWIgPrSjjd/nEPPTl/6H8mTjtP7C0/yLJ0Hq1wAAAxVJREFUKM9jYIADEyl9cT5+cT5DAwZMICvRxD9jazUQzGjil7JClTSWaNpa01hYU7NkSU1h45u3drIIOV09I74ZBw/N3T+9oXD+/MLGhrkHOz87OEJlRconNT17vGvXi71P7jQAJafvn3voU3Pnd2eItGbYgisv9+w5krl395rF0xevqcr4cPjL15ZOF1ewtGpvb+CKlPTap4+Sl1dVVS1PzphdfPjb0ZZ5XXIgaZ64nLDQlKzanat2r04GgtVA6daffVOPHnMDSaflJASCpGduDtyyMiMjY/aE4tai9j/NU48rAGXVTyaApNPSn/Pe2nR5AlCuv7+oPfpEc09PKVB6bY5XXEBY4qT09MpNFy8AJaOj6+rqfv9rmdpc6gH0llesVwDY9MpL7UVFQI1BQfuifp1o6e5TkGcQTfT09I0LAJmeWRcN1AmSTM3+0Tdvat//IwyigT6esXEBAWxlK88EgUBUamr2tFkfO5t7mrvdGcR8g32A2vPKIk6nRoFAdvasWfX173uOHW/528EgVukPlPcqi/RcNC07O3Xj9dRp9VOmFLzu6u7qmlPCoF2ZlOTnE8sWHhe6c8eOLZklNzdOKci/d+BAd2lpiRODFo93fLCfZ7xnHDDoJmWlrX94u75g6dX7r0rb2t7ZMzCoRUZODA73A7o/IDDAN37bjYKC/Ar5trttHSWTgMGi4h3iDTTe07d3M++Dbdvz8/OXVpybM6ejJDPLGhTmvDFQ+bhrk7cvq6hYdmrR2Y7MzPSsmZYgacUIsHy4Z6xXHM/k85NXhSYuXDhpUuh6HlNwhLPlgeSDQQYAgx8Y/uXliYGBC3z9IclFOC8XKD/R3y8cqMDXKzBsRVhcnJdnvBxEml0pNzcmIhJkAFBBrJeXr6+vp3+IMCdEmpNRuWxDHlA+PskfqAIEvNl0WJlY2KHSzBpcZUADQrzj4yf6+yclsUVIcsCl2VmYWDnMuNZtAKmIjIyMyJO0kOFmZoQaDpSXZubmMBcUEODiEhAUtLGVEWJlgsuCFTAyMbNyC3EAgRA3K7M0CyfYZABvvj+RWMmoDQAAAABJRU5ErkJggg==" width="15" height="15" alt="Firefox">Install the new Firefox<span>&nbsp;Â»</span>
            </a>
        </li>
        <script>!function(){var a,b,c,d="; "+document.cookie,e=d.split("; DSS="),f=new RegExp("sdts=(\\d+)");e&&2===e.length&&(a=f.exec(e[1]),a&&2===a.length&&(b=a[1])),(!b||b&&parseInt((new Date).getTime())/1e3-b>604800)&&(c=document.getElementById("mega-top-ff-promo"),c.className=c.className.replace(/D\(n\)/g,""))}();</script>
    
    </ul>
</div>

    
    <div id="mega-uh" class="M(a) Maw(1256px) Miw(1000px) Pos(r) Pt(22px) Pb(24px) Reader-open_Pt(13px) mini-header_Pt(13px) Reader-open_Pb(14px) mini-header_Pb(14px) Z(6)">
        <h1 class="Fz(0) Pos(a) Pstart(15px) Reader-open_Pt(0px) mini-header_Pt(0px) search-mini-header_Pstart(10px) ua-ie7_Mt(4px)">
    <a id="uh-logo" href="https://www.yahoo.com/" class="D(ib) H(47px) W($bigLogoWidth) Bgi($logoImage) Bgr(nr) Bgz($bigLogoWidth) Reader-open_Bgz($mediumLogoWidth) mini-header_Bgz($mediumLogoWidth) search-mini-header_Bgz($searchLogoWidth) Bgz($smallLogoWidth)!--sm1024 Bgp($bigLogoPos) Reader-open_Bgp($mediumLogoPos) mini-header_Bgp($mediumLogoPos) search-mini-header_Bgp($searchLogoPos) Bgp($searchLogoPos)!--sm1024 ua-ie7_Bgi($logoImageIe) ua-ie7_Mstart(-170px) ua-ie8_Bgi($logoImageIe) "   data-ylk="rspns:nav;t1:a1;t2:hd;sec:hd;itc:0;slk:logo;elm:img;elmt:logo;" tabindex="1">
        <b class="Hidden">Yahoo</b>
    </a>
</h1>

        <ul class="Pos(a) End(15px) List(n) Mt(3px) Reader-open_Mt(1px) mini-header_Mt(1px)" role="menubar">
            
    <li class="Pos(r) Fl(start) Mend(26px)">
        <a id="uh-signin" class="Bdc($signInBtn) Bdrs(5px) Bds(s) Bdw(2px) Bgc($signInBtn):h C($signInBtn) C(#fff):h D(ib) Ell Fz(14px) Fw(b) Py(2px) Mt(5px) Ta(c) Td(n):h Miw(78px) H(18px)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;" tabindex="4"><div class="Miw(78px) Ta(c) Pos(a) T(0) Lh($userNavTextLh)">Sign in</div></a>
    </li>


            
    <li id="uh-notifications" class="uh-menu uh-notifications Fl(start) D(ib) Mend(13px) Cur(p) ua-ie7_D(n) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <button class="uh-menu-btn Pos(r) Bd(0) Px(8px) Pt(1px) Lh(1.3)" aria-label="Notifications" aria-haspopup="true" aria-expanded="true" tabindex="5" title="Notifications">
            <i class="Lh($userNotifIconLh) C($mailBtn) Fz(28px) Grid-U Icon-Fp2 IconBell uh-notifications-icon"></i>
            <span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) End(-4px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-notification-count W(22px)"></span>
        </button>
        <div class="uh-notification-panel uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Ovy(a) Trs($menuTransition) Cur(d) V(h) Op(0) Mah(0) uh-notifications:h_V(v) uh-notifications:h_Op(1) uh-notifications:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" aria-label="Notifications" role="menu" tabindex="5">
            <div class="uh-notifications-loading Py(16px) Px(24px) Ta(c)">
                <div class="W(30px) H(30px) Mx(a) My(12px) Bgz(30px) Bgi($animatedSpinner) ua-ie8_D(n)"></div>
                <span class="C($mailTstamp) D(n) ua-ie8_D(b)">Loading Updates</span>
            </div>
        </div>
    </li>


            
    <li id="uh-mail" class="uh-menu uh-mail D(ib) Mstart(14px) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <a id="uh-mail-link" href="https://mail.yahoo.com/" class="Pos(r) D(ib) Ta(s) Td(n):h uh-menu-btn" aria-label="Mail" aria-haspopup="true" role="button" aria-expanded="true" tabindex="6"><i class="Lh($userNavIconLh) W(28px) Mend(8px) C($mailBtn) Fz(30px) Grid-U Icon-Fp2 IconMail uh-mail-icon"></i><span class="Lh($userNavTextLh) D(ib) C($mailBtn) Fz(14px) Fw(b) Va(t)">Mail</span><span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) Start(16px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-mail-count W(22px)"></span>
        </a>
        
            <div class="uh-mail-preview uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Trs($menuTransition) V(h) Op(0) Mah(0) uh-mail:h_V(v) uh-mail:h_Op(1) uh-mail:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" role="menu" aria-label="Mail" tabindex="6">
                
                    <div class="Px(24px) Py(20px) Ta(c)">
                        <a class="C($menuLink) Fw(b) Td(n)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;">Sign in</a>  to view your mail
                    </div>
                
            </div>
        
    </li>


        </ul>
        <div id="uh-search" class="Pos(r) Mend(375px) Mstart(206px) search-mini-header_Mstart(112px) Mstart(120px)--sm1024 Maw(595px) H(40px) Reader-open_H(40px) mini-header_H(40px) search-mini-header_Maw(685px) Maw(745px)--sm1024 Va(t)">
    <form name="input" class="glowing glow" action="https://search.yahoo.com/search;_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-" method="get" data-assist-action="https://search.yahoo.com/search;_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-">
        <table class="Bdcl(s)" role="presentation">
            <tbody>
                <tr>
                    <td class="Pos(r) P(0) W(100%) H(40px) Reader-open_H(40px) mini-header_H(40px) D(tb) ua-safari_D(tbc) Tbl(f)">
                        <input id="uh-search-box" type="text" name="p" class="Pos(a) T(0) Start(0) Bd Bdc($searchBdrFoc):f Bdc($searchBdr) glow_Bdc($searchBtnGlow) Bgc(t) Bxsh(n) Fz(18px) M(0) O(0) Px(10px) W(100%) H(inh) Z(2) Bxz(bb) Bdrs(2px) ua-ie7_H(36px) ua-ie7_Lh(36px) ua-ie8_Lh(36px) ua-ie7_W(92%)" autofocus autocomplete="off" autocapitalize="off" aria-label="Search" tabindex="2">
                        
                        
                            <input type="hidden" data-fr="yfp-t-201" name="fr" value="yfp-t-201" />
                        
                            <input type="hidden" data-fp="1" name="fp" value="1" />
                        
                            <input type="hidden" data-toggle="1" name="toggle" value="1" />
                        
                            <input type="hidden" data-cop="mss" name="cop" value="mss" />
                        
                            <input type="hidden" data-ei="UTF-8" name="ei" value="UTF-8" />
                        
                    </td>
                    <td class="Miw(5px)"></td>
                    <td>
                        <button id="uh-search-button" type="submit" class="rapid-noclick-resp C(#fff) Fz(16px) H(40px) W(140px) Reader-open_H(40px) mini-header_H(40px) Px(26px) Py(0) Whs(nw) Bdrs(3px) Bdw(0) Bgc($searchBtn) glow_Bgc($searchBtnGlow) Bxsh($searchBtnShadow) glow_Bxsh($searchBtnShadowGlow) Lh(1)"   data-ylk="rspns:nav;t1:a1;t2:srch;sec:srch;slk:srchweb;elm:btn;elmt:srch;tar:search.yahoo.com;tar_uri:/search;itc:0;" tabindex="3">
                        <span class="search-default-label Fw(500) ">Search Web</span>
                        <span class="search-short-label Fw(500) D(n) ">Search</span>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    
    <ul id="Skip-links" class="Pos(a)">
        <li><a href="#Navigation" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f" tabindex="3">Skip to Navigation</a></li>
        <li><a href="#Main" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f" tabindex="3">Skip to Main content</a></li>
        <li><a href="#Aside" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f" tabindex="3">Skip to Related content</a></li>
    </ul>
    
</div>

    <script>
!function(){var e,t,a,n,c=window,o=document,r=o.getElementById("uh-search-box"),g=function(c){try{a=c.keyCode,n=c.target?c.target:c.srcElement,e=r.value.length,n===r&&0===e&&a>=32&&40>=a?r.blur():"EMBED"===n.tagName||"OBJECT"===n.tagName||"INPUT"===n.tagName||"TEXTAREA"===n.tagName||c.altKey||c.metaKey||c.ctrlKey||!(32>a||a>40)||9==a||13==a||16==a||27==a||(e&&(r.setSelectionRange?r.setSelectionRange(e,e):r.createTextRange&&(t=r.createTextRange(),t.moveStart("character",e),t.select())),r.focus())}catch(c){}};r&&(c.addEventListener?c.addEventListener("keydown",g,!1):c.attachEvent&&o.attachEvent("onkeydown",g),setTimeout(function(){r.focus()},500))}();
</script>



    </div>
</div>

 </div> </div> </div>            <!-- App close -->
            </div>
        </div>
    </div><!-- Header -->
    <div id="Stencil">
    <div id="Reader" class="js-viewer-viewerwrapper">
        <div class="ReaderWrap">
            <div class="InnerWrap">
                <div id="applet_p_50000101" class=" App_v2  M-0 js-applet viewer Zoom-1  App-Chromeless " data-applet-guid="p_50000101" data-applet-type="viewer" data-applet-params="_suid:50000101" data-i13n="auto:true;sec:app-view" data-i13n-sec="app-view" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div id="hl-viewer" class="js-slider Hl-Viewer Ov-h desktop" role="document" aria-labelledby="modal-header" aria-live="polite" aria-hidden="true" tabindex="-1">
    
    
        <button id="closebtn" class="Viewer-Close-Btn Bdr-0 D-b Fw(b) js-close-content-viewer rapid-noclick-resp Z-7">
            <i class="Icon-Fp2 IconActionCross"></i>
            <b class="Td(n) Hidden">Close this content, you can also use the Escape key at anytime</b>
        </button>
        <div class="viewer-wrapper Ov-h mega-modal">
            <div class="">
                
                    <div class="content-container"></div>
                
                
                
                
                <div class="footer-ads">
                    <div id="hl-ad-FSRVY-0" class="D-ib">
                        <div id="hl-ad-FSRVY-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT9-0" class="D-ib">
                        <div id="hl-ad-FOOT9-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT-0" class="D-ib">
                        <div id="hl-ad-FOOT-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                </div>
                <span id="viewer-end" tabindex="0"></span>
            </div>
        </div>
    

    


</div>

 </div> </div> </div>            <!-- App close -->
            </div>
            </div>
        </div>
        <div id="Overlay"></div>
        <div class="Reader-bg"></div>
    </div>
</div>
    
    <div id="Masterwrap" class="slider js-viewer-pagewrapper">
        <div class="page">
            <div id="Billboard-ad">
                                                    <div id="my-adsMAST" class="D-n">
                    <div id="my-adsMAST-iframe">
                        
                        <!-- MAST has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(1366cu6rd(gid%24km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st%241458927491953684,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125uieu5q,aid%249oJFWmKKanM-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=MAST)"></noscript>
                    </div>
                </div>
                                <div id="ad-north-base" class=""><div id="ad-north" class="BillboardAd"></div></div>
            </div>
            
            <div class="Col1" role="navigation" id="Navigation" tabindex="-1">
                <div class="Col1-stack" data-plugin="sticker" data-sticker-top="75px">
                                <div id="applet_p_50000195" class=" M-0 js-applet navrailv2 Zoom-1  Mb-20 " data-applet-guid="p_50000195" data-applet-type="navrailv2" data-applet-params="_suid:50000195" data-i13n="auto:true;sec:nav" data-i13n-sec="nav" data-ylk="rspns:nav;itc:0;t1:a2;t2:nav;elm:itm;"> <!-- App open -->
        <!-- src=mdbm type=popular mod=td-applet-navlinks-atomic ins=p_50000195 ts=1458927159.0872 --><div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

    

    

    <ul class="navlinks-list D(tbc) W(134px) Mstart(6px)">
    
        <li class="navlink  Py(5px)">
            <a href=https://mail.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMail C($MailNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Mail</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://news.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconNews C($NewsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">News</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://sports.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconSports C($SportsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Sports</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://finance.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFinance C($FinanceNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Finance</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/autos/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconAutos C($AutosNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Autos</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/celebrity/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconCelebrity C($CelebrityNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Celebrity</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://shopping.yahoo.com class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconShopping C($ShoppingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Shopping</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/movies/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMovies C($MoviesNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Movies</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/politics/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconPolitics C($PoliticsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Politics</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/beauty/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconBeauty C($BeautyNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Beauty</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Style</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Tech</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">TV</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">More on Yahoo</span>
            </a>
        </li>
    
        <li class="D(b)--maxh700 Py(5px) js-navlinks-seemore Pos(r) D(n) O(0)"  role="button" tabindex="0" aria-haspopup="true" aria-label="More Navlinks">
            <i class="Icon-Fp2 IconStreamShare Va(m) Pend(6px) Fz(22px) D(tbc) Miw(29px) Ta(c) C($MoreNav)"></i>
            <span class="Va(m) D(tbc) Fz(14px) Fw(b) V(h)--sm1024 C($navlink) Col1-stack:h_V(v) C($navlinkHover):h Cur(p)">More</span>

            <div class="js-navlinks-seemore-menu D(n) js-navlinks-menu-open_D(ib) Va(b) Pos(a) B(-14px) Start(60px) Pstart(60px)">
                <ul class="Bxsh($menuShadow) Bd($submenuBdr) Bdrs(3px) Bgc(#fff) Cur(d) Py(13px) Px(20px) Miw(140px)">
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Style</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Tech</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">TV</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">More on Yahoo</span>
                        </a>
                    </li>
                
                </ul>
            </div>
        </li>
    </ul>


 </div> </div> </div> <!-- yrid: Nnb1VgAAAADNAgAADALsVA.. | edgepipe: 0 | authed: 0 | ynet: 1 | ssl: 1 | spdy: 0 | ytee: 0 | mode: fallback | bucket: 201 | colo: ne1 | device: desktop | bot: 0 | environment: prod | lang: en-US | partner: default | site: fp | region: US | intl: us | tz: America/Los_Angeles -->
<!-- tdserver-my.fp.production.manhattan.ne1.yahoo.com (pprd3-node5018-lh1.manhattan.ne1.yahoo.com) | Served by ynodejs | Fri Mar 25 2016 17:32:39 GMT+0000 (UTC) -->            <!-- App close -->
            </div>                           <div id="my-adsTXTL" class="Mt-10 Mb-20 Pt-20 Pos-r ad-txtl D-n">
                    <div class="Mx-a" id="my-adsTXTL-iframe">
                        <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<style>
	#fc_align{
		position: static !important;
		width: 120px !important;
		margin: auto !important;
	}
</style>
<!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(1366cu6rd(gid%24km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st%241458927491953684,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413afd2cqv,aid%24aIRFWmKKanM-,bi%242275188051,agp%243476502551,cr%244451009051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=TXTL)"></noscript>
                    </div>
                </div>
                </div>
            </div>
            <div class="Col2">
                <div id="Main" class="Col2-stack" role="content" tabindex="-1">
                    <div id="Banner">
                        
                    </div><!-- Banner -->
                                <div id="applet_p_50000173" class=" M-0 js-applet streamv2 Zoom-1  Mb-20 " data-applet-guid="p_50000173" data-applet-type="streamv2" data-applet-params="_suid:50000173" data-i13n="auto:true;sec:strm;useViewability:true" data-i13n-sec="strm" data-ylk="t1:a3;t2:strm;t3:ct;cat:default;rspns:nav;itc:0;" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div data-uuid-list="p_50000173">
    

    
    <ul class=" js-stream-tmpl-items js-stream-dense  js-stream-hover-enable My(0) Mb(0) Wow(bw)" id="Stream">
    
        
    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Islamic_state|WIKIID:Ash_Carter|WIKIID:Iraq|WIKIID:The_Pentagon|WIKIID:Joseph_Dunford|WIKIID:Syria|WIKIID:Barack_Obama|YCT:001000661|YCT:001000713"  data-uuid="b2ef655a-f7f1-3008-9c4d-9de92e6b6f93" data-cauuid="b2ef655a-f7f1-3008-9c4d-9de92e6b6f93" data-type="article" data-cluster="b2ef655a-f7f1-3008-9c4d-9de92e6b6f93"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Pt(12px) Pb(17px)"><div class="js-stream-roundup js-stream-roundup-filmstrip Pos(r) Wow(bw) Cf">
    <div class="strm-headline Pos(r)">
        <a href="http://news.yahoo.com/officials-us-killed-senior-islamic-state-leader-141746901--politics.html?nf=1" class="Pos(r) D(ib) Z(1) js-stream-content-link js-content-title js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;refcnt:4;subsec:needtoknow;imgt:ss;g:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;aid:id-3599436;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:ISIS second in command killed in raid;elm:img;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
        
        <img src="/sy/uu/api/res/1.2/iAIaMxAG.rAR5OKvYg5jmQ--/Zmk9c3RyaW07aD0yNzQ7cHlvZmY9MDtxPTk1O3c9NzAyO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032516/images/smush/isis2_ipad_1458923192.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
        
        <div class="Pos(a) T(0) Start(0) W(100%) H(100%) Ov(h)" title="The U.S. killed Abd al-Rahman Mustafa al-Qaduli in a Syrian raid, Pentagon chief Ashton Carter confirmed. (AP)" >
            <div class="Pos(a) B(0) M(15px) Mb(11px) Mend(120px) C(#fff) Z(1)">
                <h2 class="js-stream-item-title Fz(21px) Fw(b) Mb(3px) Lh(24px) Td(u):h">ISIS second in command killed in raid</h2>
                <span class="stream-summary Fz(13px) C(#d9d9db) Mend(5px)">The U.S. killed Abd al-Rahman Mustafa al-Qaduli in a Syrian raid, Pentagon chief Ashton Carter confirmed.</span><span class="Fw(b) D(ib) Va(b) Lh(18px) Td(u):h">Accelerating the campaign Â»</span></div>
            <div class="strm-img-gradient W(100%) H(100%) rounded-img"></div>
        </div>
        </a>
        <div class="Pos(a) End(13px) T(10px) W(30px) Mend(2px) Ta(end) Z(2)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) C(white)" data-cmntnum="1851"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow) js-stream-comment-hidden">1851</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C(white) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;refcnt:4;subsec:1851;imgt:ss;g:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;aid:id-3599436;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) Tsh($comment_shadow) ActionComments:h_C($signin_blue)"></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C(white) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) Tsh($comment_shadow)"></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C(white) P(14px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;refcnt:4;subsec:1851;imgt:ss;g:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;aid:id-3599436;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) Tsh($comment_shadow)"></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C(white) Tsh($comment_shadow) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
    </div>
    <ul class="P(0) Mt(7px) Mstart(-2px) Mend(7px) Fz(12px) Whs(nw) Lts(-.31em)">
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/tech/microsoft-launches-ai-chatbot-on-twitter-and-it-132424697.html" data-uuid="da3c7ee3-16fa-302f-8073-9d09541f4826" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:2;bpos:1;pos:2;ss_cid:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;refcnt:4;subsec:needtoknow;imgt:ss;g:da3c7ee3-16fa-302f-8073-9d09541f4826;aid:id-3599387;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:Microsoft axes chat robot after it goes rogue;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/FaS3ajYh_Wd6MHpBJiu6NA--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032516/images/smush/chatbot1_ipad_1458873170.jpg.cf.jpg" class="W(100%) rounded-img" title="Microsoft launches AI chatbot on Twitter and it turns racist within hours. (Yahoo Tech)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Microsoft axes chat robot after it goes rogue</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://finance.yahoo.com/news/young-broke-scared-irs-millennial-152014893.html" data-uuid="73f9eb08-591d-3a13-8130-acd5678758b0" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:3;bpos:1;pos:3;ss_cid:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;refcnt:4;subsec:needtoknow;imgt:ss;g:73f9eb08-591d-3a13-8130-acd5678758b0;aid:id-3599399;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:Why millennials are scared of the IRS;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/qGmptgl6E_4QhSFlERW4Ug--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032516/images/smush/tax-fear_ipad_1458880491.jpg.cf.jpg" class="W(100%) rounded-img" title="Why millennials are scared of the IRS. (Reuters)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Why millennials are scared of the IRS</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="http://sports.yahoo.com/blogs/nba-ball-dont-lie/nba-threatens-lost-charlotte-all-star-game-over-nc-anti-trans-law-235244562.html" data-uuid="748b248f-4c9c-3be5-a420-f511c95de13b" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:4;bpos:1;pos:4;ss_cid:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;refcnt:4;subsec:needtoknow;imgt:ss;g:748b248f-4c9c-3be5-a420-f511c95de13b;aid:id-3599380;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:NBA may move All-Star Game over new N.C. law;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/sa9cvIS7HrEAlnzZOmALtA--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032516/images/smush/silver_ipad_1458865624.jpg.cf.jpg" class="W(100%) rounded-img" title="NBA commissioner Adam Silver. (AP)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">NBA may move All-Star Game over new N.C. law</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/katiecouric/how-motor-city-is-shifting-gears-192341852.html" data-uuid="4a67bf10-c821-39c3-b0ad-3934c423c254" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:5;bpos:1;pos:5;ss_cid:b2ef655a-f7f1-3008-9c4d-9de92e6b6f93;refcnt:4;subsec:needtoknow;imgt:ss;g:4a67bf10-c821-39c3-b0ad-3934c423c254;aid:id-3599366;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:How the Motor City is shifting gears;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/0DdtEAHVBTT8ozaj3gYTqg--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032416/images/smush/detroit_ipad_1458861133.jpg.cf.jpg" class="W(100%) rounded-img" title="How the Motor City is shifting gears. (Yahoo News)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">How the Motor City is shifting gears</h3>
            </a>
        </li>
        
        
    </ul>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Hillary_Clinton|WIKIID:Clinton_Foundation|WIKIID:Allen_Stanford|YCT:001000661|YCT:001000681|YCT:001000667|YCT:001000780" data-offnet="1" data-uuid="b4578bd7-ca70-3f7d-8b65-e3a701c3ac29" data-type="article" data-cluster="60dfcfe7-6edf-438d-9790-9da1cea32277"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://observer.com/2016/03/bombshell-clinton-foundation-donors-flight-from-justice-aided-by-hillary-allies/" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:60dfcfe7-6edf-438d-9790-9da1cea32277;refcnt:2;imgt:ss;g:b4578bd7-ca70-3f7d-8b65-e3a701c3ac29;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000026540S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/.rj0pjleA32KGxsZPwaXHw--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/36bae20446168b969ba53c0bd269e709" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px) Pos(r)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://observer.com/2016/03/bombshell-clinton-foundation-donors-flight-from-justice-aided-by-hillary-allies/" class="O(n):f C($m_blue):f D(b)  js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:60dfcfe7-6edf-438d-9790-9da1cea32277;refcnt:2;imgt:ss;g:b4578bd7-ca70-3f7d-8b65-e3a701c3ac29;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000026540S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Bombshell: Clinton Foundation Donorâs Flight from Justice Aided by Hillary Allies</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">RecentÂ news reportsÂ indicate that the FBI is investigating former Secretary of State Hillary Clinton for granting favors to her familyâs foundation donors and for its systematic accounting fraud. In January, the Sunday Times of London cited former Judge Andrew Napolitano, a conservative libertarian and frequent Fox News guest, as saying that the FBI was taking evidence âseriouslyâ and that Hillary âcould hear about that soon from the Department of Justice.â Itâs hard to believe that the Obama administration and its hideously politicized Justice Department would ever indict Hillary, given that President Obama picked her for the secretary of state job and its clear favoritism toward her in the</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">The New York Observer</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://finance.yahoo.com/news/rudy-giuliani-says-hillary-clinton-184920233.html" data-uuid="92822301-5d90-3fe9-937e-4ee7e4f24c87" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:2;cposy:7;bpos:1;pos:2;ss_cid:60dfcfe7-6edf-438d-9790-9da1cea32277;refcnt:2;imgt:ss;g:92822301-5d90-3fe9-937e-4ee7e4f24c87;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000026540S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/GfBAN66Tr_TBT9z6ZfRw2Q--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/fortune_175/94613ceb35b955f17276921c7f27a39f" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Rudy Giuliani Says Hillary Clinton Could Be Considered a âFounding Member of ISISâ</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Fortune</span></div></a>
            
            
            
                <a href="http://www.upi.com/Top_News/US/2016/03/24/Rudy-Guiliani-calls-Hillary-Clinton-founding-member-of-Islamic-State/9691458836387/" data-uuid="f1170531-6bde-33e8-bfc4-2fcf77834448" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:2;cposy:8;bpos:1;pos:3;ss_cid:60dfcfe7-6edf-438d-9790-9da1cea32277;refcnt:2;imgt:ss;g:f1170531-6bde-33e8-bfc4-2fcf77834448;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000026540S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/qv1j1dWITvqx5ewgs39a6w--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/24af713d05f651733606f8604be31f73" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Rudy Guiliani calls Hillary Clinton 'founding member' of Islamic State</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">UPI</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:60dfcfe7-6edf-438d-9790-9da1cea32277;refcnt:2;imgt:ss;g:b4578bd7-ca70-3f7d-8b65-e3a701c3ac29;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;r:4000026540S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad  Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=pcIvzAIGIS.3BkGd9Tvt9RSjXbNB52eLfo0OTykoJoPQAujOZVqcRTRKaGtiHi2Bh55fcd_nadH8QxaqkYZH5QPfisP3zLX8w_g26QIu79fTZnjMEo1Z9XMWVTTnzlCs9p4KQDC2Zv_qrWLbC6c3NgeGRE5ONzJq2gC3K1B19w8JJxhKhM5CBfboRskfKyWOCdDoN4ToTeceFJceC3g7wS1ZvTd5AhLdjD815nH7mh0Gd2ShzwDWrXS7JleqyOBsyev7xzpnrY17tD5xoy0tt8nGGKdHppXfHBu1UFuQ2ixZsGVIDS7n.FPphX6XgLUIE89z8d.HZgOB7o6oeenwA2unUv_HVfrMPQjHufeSsBlaVI9kLRFR0q9FkmcgawKJ_uIN0WtOTQ--&amp;ap=3" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15pb7th9n(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31815148360,dmn$start-upf5.com,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1167625,pbid$1,seid$4250754))&amp;r=1458927491998&amp;al=$(AD_FEEDBACK)" data-uuid="31815148360">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=R9LIKCUGIS_7IHZOlDGJft7ds66AOPpbHr6VDp1I3AkI5ZKKNzdU1fFd.CdGxGjvPUFlkQNYRHHzaCA1iuovU2PSU64dl5dSom3Q5DNAGwBveCu20PSx72Y_I0RSPygSRgxxKHUL.5VqGMLOF5j30LLW8qZifASKd6X6EaY6FWLpK1eIDOaek8lCK5eS2Vor0otCdaUeGF87MgCwBTmMb8I9MPo0piW_6_6JmWdPVmYJ29OwRZUk3iluA4Sescfb5cSPu.CYnUmS0zsmN8YoffPyMwr3fprEruvAQYLIhd6jaAz1F89d1sNc5FNT4lFDf2hzTyENs0gD8TmhqlvQ7pQvAxNyIbOMN2DPYm3drAZcEP7T_z6FLTLCkBup9WJeINi.loCBHRF74btK7ZH4fcdW4.V30vU7HlW9fkngqgxRGFIuHDoPM3jcglt1fNSzR5K3Fmaczqw9f1.UrqDEI8Xq2Sm8T19oZTjXXIkGpjSkCTnqbB5ClTUideBguq5lNS8-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ff237962d-3d60-4fc4-9627-5445dad3618f%3Fad%3D1" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i)" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ct;g:31815148360;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/uu/api/res/1.2/YCi1jiAaLq_B419Tlf6oZg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1458303694388-538.jpg.cf.jpg" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img" alt="">
                </a>
            </div>
            
            <div class="strm-right Fl(start) W(66%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:sp;g:31815148360;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:info;g:31815148360;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <div class="Pos(r)">
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=R9LIKCUGIS_7IHZOlDGJft7ds66AOPpbHr6VDp1I3AkI5ZKKNzdU1fFd.CdGxGjvPUFlkQNYRHHzaCA1iuovU2PSU64dl5dSom3Q5DNAGwBveCu20PSx72Y_I0RSPygSRgxxKHUL.5VqGMLOF5j30LLW8qZifASKd6X6EaY6FWLpK1eIDOaek8lCK5eS2Vor0otCdaUeGF87MgCwBTmMb8I9MPo0piW_6_6JmWdPVmYJ29OwRZUk3iluA4Sescfb5cSPu.CYnUmS0zsmN8YoffPyMwr3fprEruvAQYLIhd6jaAz1F89d1sNc5FNT4lFDf2hzTyENs0gD8TmhqlvQ7pQvAxNyIbOMN2DPYm3drAZcEP7T_z6FLTLCkBup9WJeINi.loCBHRF74btK7ZH4fcdW4.V30vU7HlW9fkngqgxRGFIuHDoPM3jcglt1fNSzR5K3Fmaczqw9f1.UrqDEI8Xq2Sm8T19oZTjXXIkGpjSkCTnqbB5ClTUideBguq5lNS8-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ff237962d-3d60-4fc4-9627-5445dad3618f%3Fad%3D1" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ad;g:31815148360;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class=" D(b) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>N'achetez AUCUNE action avant d'avoir vu ceci !</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u>
                    </a>
                </h3>
                <div>
                    <p class=" stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">Ce nouvel algorithme de trading vient d'Ãªtre rÃ©vÃ©lÃ© au public et est en train de secouer toute la France.</p>
                </div><div class="Pos(r)">
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=R9LIKCUGIS_7IHZOlDGJft7ds66AOPpbHr6VDp1I3AkI5ZKKNzdU1fFd.CdGxGjvPUFlkQNYRHHzaCA1iuovU2PSU64dl5dSom3Q5DNAGwBveCu20PSx72Y_I0RSPygSRgxxKHUL.5VqGMLOF5j30LLW8qZifASKd6X6EaY6FWLpK1eIDOaek8lCK5eS2Vor0otCdaUeGF87MgCwBTmMb8I9MPo0piW_6_6JmWdPVmYJ29OwRZUk3iluA4Sescfb5cSPu.CYnUmS0zsmN8YoffPyMwr3fprEruvAQYLIhd6jaAz1F89d1sNc5FNT4lFDf2hzTyENs0gD8TmhqlvQ7pQvAxNyIbOMN2DPYm3drAZcEP7T_z6FLTLCkBup9WJeINi.loCBHRF74btK7ZH4fcdW4.V30vU7HlW9fkngqgxRGFIuHDoPM3jcglt1fNSzR5K3Fmaczqw9f1.UrqDEI8Xq2Sm8T19oZTjXXIkGpjSkCTnqbB5ClTUideBguq5lNS8-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ff237962d-3d60-4fc4-9627-5445dad3618f%3Fad%3D1" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ad;g:31815148360;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Preditrend</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:op;g:31815148360;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Donald_Trump|WIKIID:Hillary_Clinton|YCT:001000671" data-offnet="1" data-uuid="cfce9891-804e-3e00-b0fb-f73ff3fb3ea6" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://mashable.com/2016/03/25/trump-bad-plls/" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:4;cposy:10;bpos:1;pos:1;imgt:ss;g:cfce9891-804e-3e00-b0fb-f73ff3fb3ea6;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:img;elmt:ct;r:4000024940S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="tech"><img src="/sy/uu/api/res/1.2/jgYSpHd2nYK33J.Ey6XMtA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/8f91773cfe8123367e9a5cac52c8b712" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_politics) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://mashable.com/2016/03/25/trump-bad-plls/" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:4;cposy:10;bpos:1;pos:1;imgt:ss;g:cfce9891-804e-3e00-b0fb-f73ff3fb3ea6;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000024940S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="tech"><span>Donald Trump hits all-time low in the polls that really matter</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">WASHINGTON - Â Donald Trump&#39;s poll numbers have plunged to new lows after a month of escalating violence at his rallies. After heavy media coverage of fights at his events and his repeated moves to egg on his backers as they get rough with protestors, Trump is now trailing Hillary Clinton by a gap thatÂ would be the largest in decades come election day. Clinton is clobbering Trump by double digits in five of the six national polls released this week, up from narrow leads she held for most of the campaign. Trump&#39;s slide is being driven by women as the percentage of Americans with an unfavorable opinion of him continues to rise. And pollsters believe the violence at his rallies - not to mention his</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Mashable</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:4;cposy:10;bpos:1;pos:1;imgt:ss;g:cfce9891-804e-3e00-b0fb-f73ff3fb3ea6;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;r:4000024940S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Indiana_State_Police|YCT:001000288|YCT:001000667|YCT:001000780" data-offnet="1" data-uuid="3c099d53-6258-3e0c-9057-f50cbe9daf63" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://www.usatoday.com/story/news/nation-now/2016/03/24/indiana-woman-missing-decades-found-living-texas/82238858/" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;imgt:ss;g:3c099d53-6258-3e0c-9057-f50cbe9daf63;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000026660S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/kj0Od72Ht36M6eQLHEb_6Q--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/fc760bb2d9a2f75e30aa661f21c2c894" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_us) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://www.usatoday.com/story/news/nation-now/2016/03/24/indiana-woman-missing-decades-found-living-texas/82238858/" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;imgt:ss;g:3c099d53-6258-3e0c-9057-f50cbe9daf63;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000026660S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Indiana woman missing for decades found living in Texas</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">INDIANAPOLIS - An Indiana woman missing for more than 40 years was found living in a small south Texas town under a different name, police said. Lula Ann Gillespie-Miller, then 28, left her Laurel, Ind., home in 1974 after giving birth to her third child, a release from the Indiana State Police said. She felt she was too young to be a mother and gave her parents custody of her children. The last time her family heard from her was the following year, when they received a letter from their daughter postmarked in Richmond, Ind. In 2014, State Police Detective Sgt. Scott Jarvis took on the missing persons case after learning about it from a website that assists families with those types of investigations,</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">USA Today</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:5;cposy:11;bpos:1;pos:1;imgt:ss;g:3c099d53-6258-3e0c-9057-f50cbe9daf63;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000026660S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Donald_Trump|WIKIID:Twitter|WIKIID:Ted_Cruz|YCT:001000031|YCT:001000075|YCT:001000085"  data-uuid="2a15c483-d121-3568-b78b-bcd85ae3dbc0" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://news.yahoo.com/alleged-ted-cruz-sex-scandal-133900795.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:6;cposy:12;bpos:1;pos:1;subsec:1742;imgt:ss;g:2a15c483-d121-3568-b78b-bcd85ae3dbc0;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Entertainment;elm:img;elmt:ct;r:4000026570S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/uu/api/res/1.2/.RCtr9lpq0X.FD4ywcBkfw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/mic_26/84933750e40e759ee012413bc1ce797c" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_entertainment) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="entertainment">Entertainment</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://news.yahoo.com/alleged-ted-cruz-sex-scandal-133900795.html" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:6;cposy:12;bpos:1;pos:1;subsec:1742;imgt:ss;g:2a15c483-d121-3568-b78b-bcd85ae3dbc0;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Entertainment;elm:hdln;elmt:ct;r:4000026570S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Alleged Ted Cruz Sex Scandal Had the Internet&#39;s Cruz Detractors Buzzing</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Republican presidential candidate Ted Cruz is battling an entanglement of rumors that allege the Texas senator is &quot;hiding five different mistresses,&quot; according to the tabloid the National Enquirer. According to its source, identified as a &quot;Washington insider,&quot; &quot;private detectives are digging into at least five affairs Ted Cruz supposedly had,&quot; and &quot;the leaked details are an attempt to destroy what&#39;s left of his White House campaign.&quot; The supposed affairs are detailed in the Enquirer&#39;s most recent print issue.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Mic</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="1742"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">1742</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:12;bpos:1;pos:1;subsec:1742;imgt:ss;g:2a15c483-d121-3568-b78b-bcd85ae3dbc0;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Entertainment;r:4000026570S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:12;bpos:1;pos:1;subsec:1742;imgt:ss;g:2a15c483-d121-3568-b78b-bcd85ae3dbc0;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Entertainment;r:4000026570S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000001" data-offnet="1" data-uuid="9eece9ff-bbde-3c0c-a47e-b8a7d58106a5" data-type="article" data-cluster="8a2d2196-5f8c-4671-8d1f-c85818ea49f2"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://www.sbnation.com/lookit/2016/3/25/11303122/kansas-garbage-time-score-maryland-frank-mason" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:7;cposy:13;bpos:1;pos:1;ss_cid:8a2d2196-5f8c-4671-8d1f-c85818ea49f2;refcnt:2;imgt:ss;g:9eece9ff-bbde-3c0c-a47e-b8a7d58106a5;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:img;elmt:ct;r:4000026180S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/gD1A6EumyWfpCPcISs9Ltg--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/0f38bf97f130206dae57e1c53dfab67a')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px) Pos(r)">
            <div class="strm-headline-label js-content-label C(c_sports) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="sports">Sports</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.sbnation.com/lookit/2016/3/25/11303122/kansas-garbage-time-score-maryland-frank-mason" class="O(n):f C($m_blue):f D(b)  js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:7;cposy:13;bpos:1;pos:1;ss_cid:8a2d2196-5f8c-4671-8d1f-c85818ea49f2;refcnt:2;imgt:ss;g:9eece9ff-bbde-3c0c-a47e-b8a7d58106a5;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:hdln;elmt:ct;r:4000026180S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><span>Kansas player scores ultimate garbage time basket while everybody shakes hands</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">The game between Kansas and Maryland ended with a little bit of drama, but not because of another NCAA Tournament buzzer-beater. Well, not a game-winning buzzer beater. The Jayhawks were leading by 14, 77-63 with the final seconds ticking off. Frank Mason III had the ball and was dribbling out the clock. He appeared to be oblivious that there was even time left on the clock, casually dribbling (traveling) around the basket before laying it up and in. Harmless enough, except that shot clearly came with time left. So referees had to go to the monitors and review the play. Eventually they concluded that the shot did count and the final score was 79-63. It likely didn&#39;t impact the gambling line,</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">SB Nation</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.testudotimes.com/maryland-terps-basketball/2016/3/23/11290058/kansas-ncaa-tournament-game-2016-predictions" data-uuid="cf4925a2-cb00-322f-a633-caf982c37828" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:7;cposy:14;bpos:1;pos:2;ss_cid:8a2d2196-5f8c-4671-8d1f-c85818ea49f2;refcnt:2;imgt:ss;g:cf4925a2-cb00-322f-a633-caf982c37828;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:rhdln;elmt:ct;r:4000026180S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/qne.5KYpkrYPbKmjmZMynA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/209b88231ad959692058f133c9e1b004')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">How Maryland can beat Kansas</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Testudo Times</span></div></a>
            
            
            
                <a href="http://sports.yahoo.com/news/top-seeded-kansas-tops-no-5-maryland-79-040639582--ncaab.html" data-uuid="8bcc0aef-8123-3844-92e4-66b7257d2aa3" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:7;cposy:15;bpos:1;pos:3;ss_cid:8a2d2196-5f8c-4671-8d1f-c85818ea49f2;refcnt:2;imgt:ss;g:8bcc0aef-8123-3844-92e4-66b7257d2aa3;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:rhdln;elmt:ct;r:4000026180S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/X4USUQ5cbRMCugmXlc8ViQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_us/Sports/ap/201603242102757230663')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Top-seeded Kansas tops No. 5 Maryland 79-63 in South Region</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">The Associated Press</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:13;bpos:1;pos:1;ss_cid:8a2d2196-5f8c-4671-8d1f-c85818ea49f2;refcnt:2;imgt:ss;g:9eece9ff-bbde-3c0c-a47e-b8a7d58106a5;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;r:4000026180S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad  Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=a9FWu4MGIS_ucBAREXzYOl5UrIbhjVmouAVwdlfQmnkDO9FOx1u5BWd4LeEoy5fxC7LSK22HTXVUOY4Xu9X4FSjpZ21Hkk4uVLW9zQPqF6JyHQZzsqQF8rDoOfUulsSOhTYsJ5jNSMYeENmYLKHIxG8ZjowIZj4QNF8bBOOiSDoCUNuEt81Jfx0vOlwDKEKyoklXWyDx0NKeTzAHq.IVTaIQBqUSGJWqZDgqKSs9BwttPMhbSZeOHcxTm9ei2Z5u4CsNTToQsb0zazRu8uyi47PDJ7jDu9W5KPR0wU.Vr7h8pe_Pg.Wz4acs8XIntzYglutNnCCWJzB_9Dijqe0aARZLpaR2qV3Y.iRDGP9OSqKnhPIxI2eE8OlXrxQIQJ2S86YFJdYvuQ--&amp;ap=8" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15lntm50a(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31745180003,dmn$winamax.fr,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1127521,pbid$1,seid$4250754))&amp;r=1458927491998&amp;al=$(AD_FEEDBACK)" data-uuid="31745180003">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=85AriqcGIS8MZ79iHkje30.iY.jkqebt9LQXEc27M8ppPJAEdm9sBNFStHL2mYd5AQBSlJ_w.Qb_YnVCglus4Tii7dgHNgi2UU6jEPPlQTGHwFK8g0rKIk63IGwu1xjRPE8bkmV06lVlo.c8Q_iffroMDTIg2A5ISxwoF2xTXJYzrahVfml5z2dvKFeyb.M0bcFxW9x5ZC2kDPv0gMxOk1.7xc7.zo.v9sf5oD6UPPfnt986LuFvWC66wNuGqcwrKhiWVSGBw30.k5o09xhlnNDCXKq92ggdGRdJ.0yjQkXvDpBvC3lmBIz2WRyYqhTXKSt5n8YGQWKGU2MxAPa_az6KfXj7UnOcdZoEMpmy3vEzpIETM02fbGImi65.BtzgOuqbnfmAWKRk8s0A1SfW.GV3rlexuNoaM1dz5UD4LCnsIzxtRb.3mHKeiqjJbf1KlqsgM8uq5uY.qc._r0hw_KarbcPTJBtAk9U7Jv_EX13iKcrsf5B8dvS.ArlzgWO74qhD.7XlYqLc_5.n5E_IOV_.G_YBmatDJbG9NU4Jm2ey0t4tdarSiIAAzllWm81nUshXbUUknlOBZUEF3VkSidlvaQbh.whF6s.DX95m4GEXnkGSxNu4Gu33Tpw-%26lp=https%3A%2F%2Fwww.winamax.fr%2Flanding%2Flanding_leads.php%3Fldg%3Dgrilles%26banid%3D29181%26utm_source%3DYAHOO%26utm_medium%3DCPC%26utm_campaign%3DNATIVE_filrouge_grilles_vectoriel" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i)" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ct;g:31745180003;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/cqRDFObjBFGS7Qv_c7W.3A--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1455880088682-93.jpg.cf.jpg')" alt="">
                </a>
            </div>
            
            <div class="strm-right Fl(start) W(66%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:sp;g:31745180003;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:info;g:31745180003;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <div class="Pos(r)">
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=85AriqcGIS8MZ79iHkje30.iY.jkqebt9LQXEc27M8ppPJAEdm9sBNFStHL2mYd5AQBSlJ_w.Qb_YnVCglus4Tii7dgHNgi2UU6jEPPlQTGHwFK8g0rKIk63IGwu1xjRPE8bkmV06lVlo.c8Q_iffroMDTIg2A5ISxwoF2xTXJYzrahVfml5z2dvKFeyb.M0bcFxW9x5ZC2kDPv0gMxOk1.7xc7.zo.v9sf5oD6UPPfnt986LuFvWC66wNuGqcwrKhiWVSGBw30.k5o09xhlnNDCXKq92ggdGRdJ.0yjQkXvDpBvC3lmBIz2WRyYqhTXKSt5n8YGQWKGU2MxAPa_az6KfXj7UnOcdZoEMpmy3vEzpIETM02fbGImi65.BtzgOuqbnfmAWKRk8s0A1SfW.GV3rlexuNoaM1dz5UD4LCnsIzxtRb.3mHKeiqjJbf1KlqsgM8uq5uY.qc._r0hw_KarbcPTJBtAk9U7Jv_EX13iKcrsf5B8dvS.ArlzgWO74qhD.7XlYqLc_5.n5E_IOV_.G_YBmatDJbG9NU4Jm2ey0t4tdarSiIAAzllWm81nUshXbUUknlOBZUEF3VkSidlvaQbh.whF6s.DX95m4GEXnkGSxNu4Gu33Tpw-%26lp=https%3A%2F%2Fwww.winamax.fr%2Flanding%2Flanding_leads.php%3Fldg%3Dgrilles%26banid%3D29181%26utm_source%3DYAHOO%26utm_medium%3DCPC%26utm_campaign%3DNATIVE_filrouge_grilles_vectoriel" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ad;g:31745180003;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class=" D(b) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>Pronostiquez 7 matchs, il y a 10 000 â¬ Ã  gagner !</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u>
                    </a>
                </h3>
                <div>
                    <p class=" stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">Pronostiquez une grille de 7 ou 12 matchs en solo ou ou en Ã©quipe, et remportez une partie des 10 000â¬ ou 50 000â¬ garantis !</p>
                </div><div class="Pos(r)">
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=85AriqcGIS8MZ79iHkje30.iY.jkqebt9LQXEc27M8ppPJAEdm9sBNFStHL2mYd5AQBSlJ_w.Qb_YnVCglus4Tii7dgHNgi2UU6jEPPlQTGHwFK8g0rKIk63IGwu1xjRPE8bkmV06lVlo.c8Q_iffroMDTIg2A5ISxwoF2xTXJYzrahVfml5z2dvKFeyb.M0bcFxW9x5ZC2kDPv0gMxOk1.7xc7.zo.v9sf5oD6UPPfnt986LuFvWC66wNuGqcwrKhiWVSGBw30.k5o09xhlnNDCXKq92ggdGRdJ.0yjQkXvDpBvC3lmBIz2WRyYqhTXKSt5n8YGQWKGU2MxAPa_az6KfXj7UnOcdZoEMpmy3vEzpIETM02fbGImi65.BtzgOuqbnfmAWKRk8s0A1SfW.GV3rlexuNoaM1dz5UD4LCnsIzxtRb.3mHKeiqjJbf1KlqsgM8uq5uY.qc._r0hw_KarbcPTJBtAk9U7Jv_EX13iKcrsf5B8dvS.ArlzgWO74qhD.7XlYqLc_5.n5E_IOV_.G_YBmatDJbG9NU4Jm2ey0t4tdarSiIAAzllWm81nUshXbUUknlOBZUEF3VkSidlvaQbh.whF6s.DX95m4GEXnkGSxNu4Gu33Tpw-%26lp=https%3A%2F%2Fwww.winamax.fr%2Flanding%2Flanding_leads.php%3Fldg%3Dgrilles%26banid%3D29181%26utm_source%3DYAHOO%26utm_medium%3DCPC%26utm_campaign%3DNATIVE_filrouge_grilles_vectoriel" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ad;g:31745180003;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">WINAMAX</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:op;g:31745180003;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Borough_president|YCT:001000667|YCT:001000780" data-offnet="1" data-uuid="44e687f8-1cf4-3493-999b-034c5d09f25d" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://www.nytimes.com/2016/03/27/nyregion/glen-grays-the-mailman-cuffed-in-brooklyn.html?_r=0" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:9;cposy:17;bpos:1;pos:1;imgt:ss;g:44e687f8-1cf4-3493-999b-034c5d09f25d;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:News;elm:img;elmt:ct;r:4000024200S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/KyP7GKeNjN751MZguHuVmg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/0b1f397bf934b9f9ac64b0be78037a5f')" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_news) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="news">News</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://www.nytimes.com/2016/03/27/nyregion/glen-grays-the-mailman-cuffed-in-brooklyn.html?_r=0" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:9;cposy:17;bpos:1;pos:1;imgt:ss;g:44e687f8-1cf4-3493-999b-034c5d09f25d;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:News;elm:hdln;elmt:ct;r:4000024200S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>A Mailman, Handcuffed, in Brooklyn</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Late in the afternoon on St. Patrickâs Day, Glen Grays, a 27-year-old African-American mail carrier, was making his rounds in Crown Heights, in Brooklyn, about to leave a package at 999 President Street. Mr. Grays prides himself on getting to know the community he serves, he told me on Wednesday. He figures out who is sick, or old, or enfeebled, and makes sure that their parcels, especially if they contain medication - âI can shake a box and usually figure that out,â he said - land directly at the doors of the people waiting for them, even if they live in fourth- or fifth-floor apartments, in walk-up buildings. On this afternoon, Mr. Grays was descending the steps of his mail truck backward,</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">New York Times</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:17;bpos:1;pos:1;imgt:ss;g:44e687f8-1cf4-3493-999b-034c5d09f25d;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:News;r:4000024200S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Oregon|WIKIID:Grayson_Allen|YCT:001000069" data-offnet="1" data-uuid="510b3264-c0ab-3e9c-834a-c3584bb0f9e1" data-type="article" data-cluster="19ec3c70-0714-4193-b7be-1eac25be006a"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://www.yardbarker.com/college_basketball/articles/grayson_allen_brushes_aside_hug_from_dillon_brooks/s1_127_20549869" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:10;cposy:18;bpos:1;pos:1;ss_cid:19ec3c70-0714-4193-b7be-1eac25be006a;refcnt:2;imgt:ss;g:510b3264-c0ab-3e9c-834a-c3584bb0f9e1;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:img;elmt:ct;r:4000021270S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/bGNt20ooCXqLpTOq7GesPg--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/8c8f090db80d871d85345f2e94df97fa')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px) Pos(r)">
            <div class="strm-headline-label js-content-label C(c_sports) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="sports">Sports</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.yardbarker.com/college_basketball/articles/grayson_allen_brushes_aside_hug_from_dillon_brooks/s1_127_20549869" class="O(n):f C($m_blue):f D(b)  js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:10;cposy:18;bpos:1;pos:1;ss_cid:19ec3c70-0714-4193-b7be-1eac25be006a;refcnt:2;imgt:ss;g:510b3264-c0ab-3e9c-834a-c3584bb0f9e1;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:hdln;elmt:ct;r:4000021270S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="sports"><span>WATCH: Grayson Allen brushes aside hug from Oregon star</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Grayson Allen was in no mood for postgame hugs from his opponent after losing to Oregon in the Sweet 16 of the NCAA Tournament on Thursday. Allen dribbled out the clock as his Duke Blue Devils fell to the Ducks 82-68 in Anaheim, Calif. After the buzzer sounded, Dillon Brooks went to give Allen a hug. The Duke standout wasnât having any of it. It&#39;s easy to understand where Allen was coming from. Who would want to give Brooks a hug to congratulate him on the 14-point win? And who would want to give him a hug after he hoisted a 3-pointer on the previous possession that went in? You could also argue that Allen only learned the move from his head coach.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Yardbarker</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.charlotteobserver.com/sports/college/article67895707.html" data-uuid="b0c5651d-b549-3254-b4e1-5381fa4098c7" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:10;cposy:19;bpos:1;pos:2;ss_cid:19ec3c70-0714-4193-b7be-1eac25be006a;refcnt:2;imgt:ss;g:b0c5651d-b549-3254-b4e1-5381fa4098c7;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:rhdln;elmt:ct;r:4000021270S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/lHJzni2IgHT_Xmw43fpkdQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/f8c4a40f2ad7560c7cb352527e4d9caa')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Ducksâ Chris Boucher, Dillon Brooks could be challenge for Duke</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">The Charlotte Observer</span></div></a>
            
            
            
                <a href="http://sports.yahoo.com/news/coach-k-lectured-dillon-brooks-010000952.html" data-uuid="684a89d7-1d27-3936-80b1-3abb3a933593" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:10;cposy:20;bpos:1;pos:3;ss_cid:19ec3c70-0714-4193-b7be-1eac25be006a;refcnt:2;imgt:ss;g:684a89d7-1d27-3936-80b1-3abb3a933593;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;elm:rhdln;elmt:ct;r:4000021270S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="sports"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/FuYq6gZUYma43c794oQztw--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-GB/homerun/uk.sporting.news/bba84a7bd4077cd67a1b6ea24b7a01f8')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Coach K lectured Dillon Brooks about sportsmanship in the handshake line</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Sporting News</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:10;cposy:18;bpos:1;pos:1;ss_cid:19ec3c70-0714-4193-b7be-1eac25be006a;refcnt:2;imgt:ss;g:510b3264-c0ab-3e9c-834a-c3584bb0f9e1;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Sports;r:4000021270S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Hyundai|WIKIID:Concept_car|WIKIID:BMW|YCT:001000992|YCT:001000993"  data-uuid="f2f21ddf-3ec1-3ee1-a2fb-99b8d4f99cf5" data-type="article" data-cluster="77c1e3ee-c972-454c-9d04-9b9b226d701f"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://finance.yahoo.com/news/car-everyone-raving-york-auto-122000719.html" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:11;cposy:21;bpos:1;pos:1;ss_cid:77c1e3ee-c972-454c-9d04-9b9b226d701f;refcnt:2;subsec:19;imgt:ss;g:f2f21ddf-3ec1-3ee1-a2fb-99b8d4f99cf5;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Business;elm:img;elmt:ct;r:4000011330S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/ERJdoHG9UcItpA_s4wqlPg--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/This_is_the_car_everyone-9223bd76d2584cbfa479d51a3395dc83')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px) Pos(r)">
            <div class="strm-headline-label js-content-label C(c_business) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="business">Business</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://finance.yahoo.com/news/car-everyone-raving-york-auto-122000719.html" class="O(n):f C($m_blue):f D(b)  js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:11;cposy:21;bpos:1;pos:1;ss_cid:77c1e3ee-c972-454c-9d04-9b9b226d701f;refcnt:2;subsec:19;imgt:ss;g:f2f21ddf-3ec1-3ee1-a2fb-99b8d4f99cf5;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Business;elm:hdln;elmt:ct;r:4000011330S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>This is the car everyone is raving about at the New York Auto Show</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">The 2016 New York Auto Show featured a series of high-profile debuts including the Nissan GT-R, the Audi R8 Spyder, and the Mazda MX-5RF. The stylish hybrid concept car is the latest offering from Hyundai&#39;s newly launched Genesis luxury brand. &quot;The New York Concept is a progressive concept car that showcases the design quality of the brand,&quot; Genesis boss Manfred Fitzgerald said in a statement ahead of the unveiling.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Business Insider</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.usatoday.com/videos/money/cars/2016/03/24/82219158/" data-uuid="ba2b71ee-652e-3e49-9a01-d4004d30938b" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:11;cposy:22;bpos:1;pos:2;ss_cid:77c1e3ee-c972-454c-9d04-9b9b226d701f;refcnt:2;subsec:19;imgt:ss;g:ba2b71ee-652e-3e49-9a01-d4004d30938b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Business;elm:rhdln;elmt:ct;r:4000011330S00001;ccode:mega_global_ranking_hlv2_up_based;t4:rel;"  data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/wjYPRbaGSyX9RHFjqzr8Ow--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/69cad9ecefb183b6564942317f4d4174')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Million dollar cars at the NY Auto Show</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">USA Today</span></div></a>
            
            
            
                <a href="https://www.yahoo.com/autos/2017-chevrolet-camaro-zl1-convertible-214500021.html" data-uuid="f54e1b76-350b-39f4-a77f-d268ac32300e" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:11;cposy:23;bpos:1;pos:3;ss_cid:77c1e3ee-c972-454c-9d04-9b9b226d701f;refcnt:2;subsec:19;imgt:ss;g:f54e1b76-350b-39f4-a77f-d268ac32300e;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Business;elm:rhdln;elmt:ct;r:4000011330S00001;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/VVUju2n.fm4rhmQbfUl7BQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/the_drive_165/4bf95a8b5bd23e50dee756f196aaf008')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">2017 Chevrolet Camaro ZL1 Convertible Debuts Quietly at New York Auto Show</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">The Drive</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="19"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">19</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:11;cposy:21;bpos:1;pos:1;ss_cid:77c1e3ee-c972-454c-9d04-9b9b226d701f;refcnt:2;subsec:19;imgt:ss;g:f2f21ddf-3ec1-3ee1-a2fb-99b8d4f99cf5;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Business;r:4000011330S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:11;cposy:21;bpos:1;pos:1;ss_cid:77c1e3ee-c972-454c-9d04-9b9b226d701f;refcnt:2;subsec:19;imgt:ss;g:f2f21ddf-3ec1-3ee1-a2fb-99b8d4f99cf5;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Business;r:4000011330S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Justice_League|YCT:001000031|YCT:001000075|YCT:001000076" data-offnet="1" data-uuid="cc55924e-8763-304e-8368-b0fe11718aac" data-type="article" data-cluster="64c93bda-32bb-40b4-8383-4a3cd79fc328"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://www.popsugar.com/entertainment/Justice-League-Set-Pictures-40670997" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:12;cposy:24;bpos:1;pos:1;ss_cid:64c93bda-32bb-40b4-8383-4a3cd79fc328;refcnt:2;imgt:ss;g:cc55924e-8763-304e-8368-b0fe11718aac;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;elm:img;elmt:ct;r:4000019550S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="celebrity"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/TU8ZYFew99uLKcOto5pX1A--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/132dab8c357d549933ddeee7cbee72ec')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px) Pos(r)">
            <div class="strm-headline-label js-content-label C(c_entertainment) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="entertainment">Entertainment</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.popsugar.com/entertainment/Justice-League-Set-Pictures-40670997" class="O(n):f C($m_blue):f D(b)  js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:12;cposy:24;bpos:1;pos:1;ss_cid:64c93bda-32bb-40b4-8383-4a3cd79fc328;refcnt:2;imgt:ss;g:cc55924e-8763-304e-8368-b0fe11718aac;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;elm:hdln;elmt:ct;r:4000019550S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="celebrity"><span>You Might Need to Sit Down For Jason Momoa&#39;s Pictures From the Justice League Set</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">If you&#39;ve been waiting with bated breath to catch Batman, Superman, Wonder Woman, Aquaman, The Flash, and Cyborg all on one screen together, the time is drawing near. Filming for Justice League: Part One has officially begun, and according to Instagram snaps from the members of the cast - and even the director, Zack Snyder - things on set are already heating up. Aquaman actor and certified hottie Jason Momoa even shared a pic while getting his chest shaved for the role! (On a related note, does anybody know how to get a job shaving Jason Momoa&#39;s chest?) Check out all the pics from the set so far, behold all the times Momoa practically busted out of his shirt, and get caight up on all the DC movies</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">PopSugar</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="https://www.yahoo.com/tv/batman-v-superman-est-160m-211319900.html" data-uuid="743ba47d-e391-32bd-8730-1c104f62c22a" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:12;cposy:25;bpos:1;pos:2;ss_cid:64c93bda-32bb-40b4-8383-4a3cd79fc328;refcnt:2;imgt:ss;g:743ba47d-e391-32bd-8730-1c104f62c22a;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;elm:rhdln;elmt:ct;r:4000019550S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="celebrity"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/mt415_9hY3g80HkEOMOe_A--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-us/homerun/deadline.com/efe78ccf754b4c3085d9172d8283f450')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">âBatman V Supermanâ: Estimated $160M+ U.S. Opening Super-Powered By Social Media, Billion-Plus Viewed Ads & Gal Gadot</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Deadline</span></div></a>
            
            
            
                <a href="https://www.yahoo.com/movies/batman-v-superman-cast-director-respond-negative-reviews-172058692.html" data-uuid="01f60368-ef17-387f-967f-3e0d33c180c2" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:12;cposy:26;bpos:1;pos:3;ss_cid:64c93bda-32bb-40b4-8383-4a3cd79fc328;refcnt:2;imgt:ss;g:01f60368-ef17-387f-967f-3e0d33c180c2;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;elm:rhdln;elmt:ct;r:4000019550S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="celebrity"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/ZlcOo.v8x2lqfrW_aBIjLg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_US/Entertainment/Variety/batman-v-superman-dawn-of-justice-16.jpg.cf.jpg')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">âBatman v Supermanâ Cast, Director Respond to Negative Reviews</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Variety</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:12;cposy:24;bpos:1;pos:1;ss_cid:64c93bda-32bb-40b4-8383-4a3cd79fc328;refcnt:2;imgt:ss;g:cc55924e-8763-304e-8368-b0fe11718aac;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;r:4000019550S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    


    
    </ul>
</div>

<img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="16" height="16" class="D(b) Mx(a) My(12px) js-stream-load-more ImageLoader" style="background-image:url('https://s.yimg.com/zz/nn/lib/metro/g/my/anim_loading_sm.gif')" alt="">


 </div> </div> </div>  <div class="App-Ft Row"> </div>            <!-- App close -->
            </div>
                </div>
            </div>
            <div class="Col3" id="Aside" role="complementary" tabindex="-1">
                <div class="Col3-stack" data-sticker-top="75px" >
                                <div id="applet_p_32209491" class=" M-0 js-applet trending Zoom-1  Mb(30px) " data-applet-guid="p_32209491" data-applet-type="trending" data-applet-params="_suid:32209491" data-i13n="auto:true;sec:tc-ts" data-i13n-sec="tc-ts"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-trending" class="slingstone-selected"><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) gifts-selected_C($disabledHeading) Trs($trendTrs)" data-category="slingstone">Trending Now</h2><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) Pos(r) slingstone-selected_C($disabledHeading) Trs($trendTrs)" data-category="gifts">
                <i class="Icon-Fp2 IconTrendingEaster gifts-selected_C($giftsIcon) Fz(20px) Fw(100) Pos(a) Start(-19px) T(-4px)"></i>Easter Searches</h2><ul class="Pos(r) Mt(10px)">
        <li class="trending-list" data-category="slingstone">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) gifts-selected_Op(0) gifts-selected_V(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Garry+Shandling&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Garry Shandling"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:1;bpos:1;ccode:trending_search;g:5415d8e4-e396-3bb9-bfa9-fe4b99d45fb4;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Garry Shandling</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Jian+Ghomeshi&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Jian Ghomeshi"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:2;bpos:1;ccode:trending_search;g:09110e19-8bc7-3d90-9cb7-603503cf12a3;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Jian Ghomeshi</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Google+Maps&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Google Maps"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:3;bpos:1;ccode:trending_search;g:7608b551-7f2c-392e-8ee1-972b9b9d68bf;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Google Maps</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=ACM+Awards&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="ACM Awards"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:4;bpos:1;ccode:trending_search;g:4576f9b3-0bc2-3374-89d2-4355c5916fb5;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> ACM Awards</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Mariah+Carey&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Mariah Carey"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:5;bpos:1;ccode:trending_search;g:45915227-c1f3-33b3-8109-0663f248f2f1;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Mariah Carey</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Allergy+symptoms&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Allergy symptoms"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:6;bpos:1;ccode:trending_search;g:38e642ee-afdd-33e7-8044-7de910eabef7;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Allergy symptoms</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Hannah+Montana&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Hannah Montana"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:7;bpos:1;ccode:trending_search;g:98753dd9-c920-3d91-8b98-0f0f66882d56;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Hannah Montana</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Electric+cars&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Electric cars"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:8;bpos:1;ccode:trending_search;g:d7009a55-158b-3a5b-ad03-d1172c191284;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Electric cars</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=George+Clooney&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="George Clooney"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:9;bpos:1;ccode:trending_search;g:ebd29c9a-726b-3517-9818-779ffd863536;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> George Clooney</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Apple+iPad+Pro&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Apple iPad Pro"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:10;bpos:1;ccode:trending_search;g:c53a7ffb-50b3-39da-813b-2644805aa66d;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Apple iPad Pro</span>
                    </a>
                </li></ul>
        </li><li class="trending-list Pos(a) T(0) W(100%)" data-category="gifts">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) slingstone-selected_Op(0) slingstone-selected_V(h) slingstone-selected_H(0) Ov(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Easter+basket+delivery&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Easter basket delivery"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:1;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Easter basket delivery</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Polo+T-shirts&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Polo T-shirts"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:2;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Polo T-shirts</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Mixers&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Mixers"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:3;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Mixers</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Maxi+dresses&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Maxi dresses"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:4;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Maxi dresses</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Cute+Easter+desserts&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Cute Easter desserts"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:5;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Cute Easter desserts</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=E-gift+cards&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="E-gift cards"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:6;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> E-gift cards</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Jumbo+Easter+eggs&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Jumbo Easter eggs"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:7;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Jumbo Easter eggs</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Boys%27+bow+ties&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Boys' bow ties"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:8;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Boys' bow ties</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Chocolate+truffles&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Chocolate truffles"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:9;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Chocolate truffles</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Folding+table+and+chairs&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Folding table and chairs"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:10;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Folding table and chairs</span>
                        </a>
                    </li></ul>
        </li></ul>
</div>
 </div> </div> </div>            <!-- App close -->
            </div>                            <div id="my-adsFPAD-base">
                    <div id="my-adsFPAD" class="D-n sda-DAPF">
                        <script>
                            rtAdStart = window.performance && window.performance.now && window.performance.now();
                        </script>
                        <div class="Mx-a" id="my-adsFPAD-iframe">
                            <!-- FPAD has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(1366cu6rd(gid%24km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st%241458927491953684,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125obcbhf,aid%24EoBFWmKKanM-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=FPAD)"></noscript>
                        </div>
                        <script>
                            rtAdDone = window.performance && window.performance.now && window.performance.now();
                        </script>
                    </div>
                </div>                           <div id="my-adsLREC-base" class="lrec-bgcolor">
                   <div id="my-adsLREC" class="Ta-c Pos-r Z-1 Ht-250 Mb-20">
                        <div id="my-adsLREC-iframe">
                            <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<!--QYZ 2264184051,4467049051,;;LREC;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(1366cu6rd(gid%24km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st%241458927491953684,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413a7bvhp5,aid%24hIFFWmKKanM-,bi%242264184051,agp%243459460551,cr%244467049051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=LREC)"></noscript>
                        </div>
                    </div>
                </div>            <div id="applet_p_63794" class=" App_v2  M-0 js-applet weather Zoom-1  Mb(30px) " data-applet-guid="p_63794" data-applet-type="weather" data-applet-params="_suid:63794" data-i13n="auto:true;sec:app-wea" data-i13n-sec="app-wea"> <!-- App open -->
        <div class="App-Hd" data-region="header"> <div class="js-applet-view-container-header"> <div class="StencilRoot">
    
        <a href="https://weather.yahoo.com" class="C(#000) C($m_blue):h Td(n)">
            <h2 class="Fz(15px) Fw(b) Mb(18px) D(ib) Grid-U App-Title js-show-panel">
                <span class="js-city-name">
                    
                        
                            Poitou-Charentes
                        
                    
                </span>
            </h2>
        </a>
    
    
    <div class="Grid-U">
        <div class="LocationPanel Pos(r) Fl(end) Z(3)"></div>
    </div>
    
</div>
 </div> </div>  <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div class="D(ib)">
    
    <ul class="P(0) Mt(0) Mend(0) Cl(b) Td(n) Mb(10px) Mstart(-2px) forecasts forecasts-7153327">
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/state/poitou-charentes-7153327/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Today</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png" width="43" height="43" alt="Cloudy" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">56&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">45&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/state/poitou-charentes-7153327/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Sat</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png" width="43" height="43" alt="Cloudy" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">65&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">44&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/state/poitou-charentes-7153327/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Sun</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png" width="43" height="43" alt="Cloudy" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">53&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">44&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) ">
            <a href="https://weather.yahoo.com/fr/state/poitou-charentes-7153327/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Mon</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png" width="43" height="43" alt="Cloudy" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">53&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">46&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
    </ul>
    
</div>
 </div> </div> </div>            <!-- App close -->
            </div>            <div id="applet_p_63796" class=" App_v2  M-0 js-applet scores Zoom-1  Mb-20 " data-applet-guid="p_63796" data-applet-type="scores" data-applet-params="_suid:63796" data-i13n="auto:true;sec:app-scor" data-i13n-sec="app-scor"> <!-- App open -->
        <div class="App-Hd" data-region="header"> <div class="js-applet-view-container-header"> <div class='Mb(10px)'>
    <h2 class="D(ib) Va(t) Fz(15px) App-Title">Scoreboard</h2>
    <form class="D(ib) Va(t) Miw(150px) SelectBox ysp-league Pos(r) Fl(end) ua-ie7_D(b)! ua-ie7_Fl(n)!" action="" autocomplete="off">
        <!-- the character here will be plugged before the string in the 'dropdown' -->
        
            <div class='SelectBox-Pick Bd(0) P(0) Ta(end)'><b class='SelectBox-Text Fl(n)! Fw(500)'>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
                NCAAB
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            </b> <i class='Icon Fl(n)! Va(m)'>&#xe002;</i></div>
            <!-- '.Start(0)' needs to be set on this select box so things work in a LTR context -->
            <select name="league" class='Start(0) js-applet-action' data-plugin='selectbox' data-applet-action="savesettings" data-applet-actioncfg="src:form.ysp-league;showview:main" data-setting="league" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:itm;elmt:cat;itc:1;cat:ncaab">
                
                    
                        <option value="" >Trending</option>
                    
                
                    
                        <option value="my" >My Teams</option>
                    
                
                    
                        <option value="nfl" >NFL</option>
                    
                
                    
                        <option value="mlb" >MLB</option>
                    
                
                    
                        <option value="nba" >NBA</option>
                    
                
                    
                        <option value="nhl" >NHL</option>
                    
                
                    
                        <option value="ncaaf" >NCAAF</option>
                    
                
                    
                        <option value="ncaab" selected>NCAAB</option>
                    
                
                    
                        
                    
                
                    
                        <option value="soccer.l.mls" >MLS</option>
                    
                
                    
                        <option value="soccer.l.fbgb" >Premier League</option>
                    
                
                    
                        <option value="soccer.l.fbchampions" >Champions League</option>
                    
                
                    
                        <option value="soccer.l.fbes" >La Liga</option>
                    
                
                    
                        <option value="soccer.l.fbde" >Bundesliga</option>
                    
                
                    
                        <option value="soccer.l.fbit" >Serie A</option>
                    
                
                    
                        <option value="soccer.l.fbfr" >Ligue 1</option>
                    
                
            </select>
        
    </form>

    
</div>
 </div> </div>  <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  
<style>
  .LiveUpdate {background-color: #fbdb56 !important;}
</style>
<div class="Pos(r) T(24px) Bdb Bdbw(4px) Bdbs(s) Bdc(#f1f1f5) Top(20px) W(100%)"></div>
<ul class="Pos(r) Fz(13px) SimpleTabs Td(n) Mb(10px)">
    <li class="D(ib) Va(t) ">
        <a class="Mend(20px) Pb(4px) Cur(p) C($dimmed) tab-selected_C(#000)! Fw(500) Td(n) tab-selected_Bdb($selectedTabBd) rapidnofollow" data-range="prev" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:tab;elmt:day;itc:1;cat:ncaab">
            
            
            
            Yesterday
            
        </a>
    </li>
    <li class="D(ib) Va(t)  tab-selected">
        <a class="Mend(20px) Pb(4px) Cur(p) C($dimmed) Fw(500) Td(n) tab-selected_C(#000)! tab-selected_Bdb($selectedTabBd) rapidnofollow" data-range="curr" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:tab;elmt:day;itc:1;cat:ncaab">
            
            
            
            Today
            
        </a>
    </li>
    <li class="D(ib) Va(t) ">
        <a class="Mend(20px) Pb(4px) Cur(p) C($dimmed) tab-selected_C(#000)! Fw(500) Td(n) tab-selected_Bdb($selectedTabBd) rapidnofollow" data-range="next" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:tab;elmt:day;itc:1;cat:ncaab">
            
            
            
            Tomorrow
            
        </a>
    </li>
</ul>

<div class="Mb(8px)">

    
    <div data-model="TDScoresModel@ncaab.g.201603250618" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/ncaab/iowa-state-cyclones-virginia-cavaliers-201603250618/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:ncaab">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="https://s.yimg.com/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Iowa St." class="ImageLoader" style="background-image:url('https://s.yimg.com/dh/ap/default/151102/IowaState_70x70.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    (4) Iowa St.
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="https://s.yimg.com/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Virginia" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/vaf.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    (1) Virginia
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            7:10 pm ET
                        </span>
                        <span class="D(ib)">
                            
                                
                                    CBS
                                
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>


    
    <div data-model="TDScoresModel@ncaab.g.201603250423" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/ncaab/wisconsin-badgers-notre-dame-fighting-irish-201603250423/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:ncaab">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="https://s.yimg.com/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Wisconsin" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/wbg.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    (7) Wisconsin
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="https://s.yimg.com/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Notre Dame" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/nbf.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    (6) Notre Dame
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            7:27 pm ET
                        </span>
                        <span class="D(ib)">
                            
                                
                                    TBS
                                
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>


    
    <div data-model="TDScoresModel@ncaab.g.201603250553" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/ncaab/gonzaga-bulldogs-syracuse-orange-201603250553/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:ncaab">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="https://s.yimg.com/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Gonzaga" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/gaj.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    (11) Gonzaga
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="https://s.yimg.com/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Syracuse" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/sci.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    (10) Syracuse
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            9:40 pm ET
                        </span>
                        <span class="D(ib)">
                            
                                
                                    CBS
                                
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>


    
    <div data-model="TDScoresModel@ncaab.g.201603250413" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/ncaab/indiana-hoosiers-north-carolina-tar-heels-201603250413/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:ncaab">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="https://s.yimg.com/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Indiana" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/iai.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    (5) Indiana
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="https://s.yimg.com/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="N. Carolina" class="ImageLoader" style="background-image:url('https://sp.yimg.com/j/assets/ipt/north-carolina_70.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    (1) N. Carolina
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            9:57 pm ET
                        </span>
                        <span class="D(ib)">
                            
                                
                                    TBS
                                
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>



</div>

 </div> </div> </div>  <div class="App-Ft Row">  <div data-region="footer" class="Fl-start Pos-r Z-1"> <div class="js-applet-view-container-footer"> <a class="D(ib) Mb(28px) Cur(p) C($link) C($m_blue):h C($m_blue):f Fw(500) Td(n)" id="ysp-more-scores" href="" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:itm;elmt:mr;itc:0;cat:ncaab;">More scores &raquo;</a>
 </div> </div> </div>            <!-- App close -->
            </div>            <div id="applet_p_50000171" class=" M-0 js-applet activitylist Zoom-1  Mb-20 " data-applet-guid="p_50000171" data-applet-type="activitylist" data-applet-params="_suid:50000171" data-i13n="auto:true;sec:ltst" data-i13n-sec="ltst"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  
<ul class="Mb(0) P(0) js-activitylist-list">

<li class="js-activitylist-item My(20px)" data-id="141665420229">
    
    <a href="https://www.yahoo.com/movies/how-my-big-fat-greek-wedding-went-from-indie-190302147.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:ee35d3c0-3ed3-3bc9-bebd-0f46b61ba60c;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/O9EVpittsg3lgG2NR8gLZQ--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160324/greekwedding2002.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Movies</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">How âGreek Weddingâ went from indie darling to blockbuster hit</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item Mt(20px)" data-id="141665406119">
    
    <a href="https://www.yahoo.com/celebrity/celebrity-spring-breakers-2016-010339774.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:eb819bab-603c-36ce-8c43-9a17aeea79bd;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/sXhQPrLHungfdvNJ8mMapg--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160324/albahawaii.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Celebrity</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Photos: Celebrity spring breakers</span>
    </span>
    </a>
    
</li>

</ul>

 </div> </div> </div>            <!-- App close -->
            </div>                           <div id="sticky-lrec2-footer" data-plugin="sticker" data-sticker-top="80px" data-sticker-forceposition="top">
               <div class="lrec-bgcolor">
               <div id="my-adsLREC2" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250" data-autorotate="1" data-autoeventrt="10000" data-config={"pos":"LREC2","id":"LREC2","clean":"my-adsLREC2","dest":"my-adsLREC2-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}} data-lrec3replace="1" data-lrec4enabled="1">
                   <div class='Mx-a Ta-c' id="my-adsLREC2-iframe">
                        
                   </div>
               </div>
                               <div id="my-adsLREC3" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 D-n" data-config={"pos":"LREC3","id":"LREC3","clean":"my-adsLREC3","dest":"my-adsLREC3-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}}>
                    <div class='Mx-a Ta-c' id="my-adsLREC3-iframe">
                    </div>
               </div>
               <div id="my-adsLREC2-fallback" class="D-n">
                    <a href="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16563095&PluID=0&ord=${CACHEBUSTER}" style="background:url(https://www.yahoo.com/sy/nn/lib/metro/300_250_Human_Touch_mail.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;margin:auto;"></a><img width="1" height="1" alt="" src="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=16563095&PluID=0&ord=${CACHEBUSTER}&rtu=-1" style="display:none">
               </div>
               
               </div>
                               <div class="lrec-bgcolor">
                <div id="my-adsLREC4" class="Ta-c Mt(20px) Mb-20 Pos-r Ht-250" data-config={"pos":"LREC4","id":"LREC4","clean":"my-adsLREC4","dest":"my-adsLREC4-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}}>
                    <div class='Mx-a Ta-c' id="my-adsLREC4-iframe">
                    </div>
               </div>
                <div id="my-adsLREC4-fallback" class="Mt(20px) D-n">
                    <a href="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16991760&PluID=0&ord=${CACHEBUSTER}" style="background:url(https://s.yimg.com/nn/lib/metro/DailyFantasy_BN_Baseball_300x250-min.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;margin:auto;"></a><img width="1" height="1" alt="" src="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=16991760&PluID=0&ord=${CACHEBUSTER}&rtu=-1" style="display:none">
               </div>
               </div>
               
                    <div id="Footer" class="Row Pstart-20 Pend-10 Pos-r" role="contentinfo">
                         <ul class="Bdrs(3px) Px(20px) Py(14px) Ta(c)" role="contentinfo" id="fp-footer"><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/legal/us/yahoo/utos/terms/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Terms</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Privacy</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://advertising.yahoo.com/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Advertise</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/relevantads.html" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">About our Ads</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://careers.yahoo.com/us" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Careers</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://help.yahoo.com/l/us/yahoo/helpcentral/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Help</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://yahoo.uservoice.com/forums/341361-yahoo-home" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Feedback</a></li></ul></div>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>





        
                        <div id="darla-assets-bottom">
                    <script type="text/x-safeframe" id="fc" _ver="2-9-9">{ "positions": [ { "html": "<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->", "id": "FPAD", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['EoBFWmKKanM-']='(as$125obcbhf,aid$EoBFWmKKanM-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125obcbhf,aid$EoBFWmKKanM-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125obcbhf,aid$EoBFWmKKanM-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1.4589274919537E+15", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "0", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": {"pt":"0","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","pvid":"km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=146750051&megamodal=${MEGAMODAL}&bucket=201&asz=300x250&u=https://www.yahoo.com&gdAdId=hIFFWmKKanM-&gdUuid=km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8&gdSt=1458927491953684&publisher_blob=${RS}|km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8|2023538075|LREC|1458927491.150054|2-9-9:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aWJpdGNpaShnaWQka205aFZEazRMakdGcVUwNlZ2VjNneHh1TWpBd01WYjFkNE1SQjRzOCxzdCQxNDU4OTI3NDkxOTUzNjg0LHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5Yngkekhhbk9Gb2F2SEZrMVFvX0JaVjcydyxsbmckZW4tdXMsY3IkNDQ2NzA0OTA1MSx2JDIuMCxhaWQkaElGRldtS0thbk0tLGJpJDIyNjQxODQwNTEsbW1lJDk1NDk0MzI2NTI4NjAyMjMxMDksciQwLHlvbyQxLGFncCQzNDU5NDYwNTUxLGFwJExSRUMpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2264184051,4467049051,;;LREC;2023538075;1-->", "id": "LREC", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['hIFFWmKKanM-']='(as$13a7bvhp5,aid$hIFFWmKKanM-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13a7bvhp5,aid$hIFFWmKKanM-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13a7bvhp5,aid$hIFFWmKKanM-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)", "impID": "hIFFWmKKanM-", "supp_ugc": "0", "placementID": "3459460551", "creativeID": "4467049051", "serveTime": "1458927491953684", "behavior": "non_exp", "adID": "9549432652860223109", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "300x250", "bookID": "2264184051", "serveType": "-1", "slotID": "1", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(160dbrbh3(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,srv$1,si$4452051,ct$25,exp$1458934691953684,adv$26513753608,li$3458215051,cr$4467049051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1458934691953\", \"fdb_intl\": \"en-US\" }", "slotData": {"pt":"8","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","adjf":"1.000000","alpha":"-1.000000","ffrac":"0.999965","pcpm":"-1.000000","fc":"false","sdate":"1442339887","edate":"1498881599","bimpr":399999991808.000000,"pimpr":0.000000,"spltp":0.000000,"frp":"false","pvid":"km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "1", "userProvidedData": {} } } },{ "html": "<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->", "id": "MAST", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['9oJFWmKKanM-']='(as$125uieu5q,aid$9oJFWmKKanM-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125uieu5q,aid$9oJFWmKKanM-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125uieu5q,aid$9oJFWmKKanM-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1458927491953684", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "2", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": {"pt":"0","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","pvid":"km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<style>\n	#fc_align{\n		position: static !important;\n		width: 120px !important;\n		margin: auto !important;\n	}\n</style>\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=251157193&asz=120x60&u=https://www.yahoo.com&gdAdId=aIRFWmKKanM-&gdUuid=km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8&gdSt=1458927491953684&publisher_blob=${RS}|km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8|2023538075|TXTL|1458927491.150178|2-9-9:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aXR0Z2kzYyhnaWQka205aFZEazRMakdGcVUwNlZ2VjNneHh1TWpBd01WYjFkNE1SQjRzOCxzdCQxNDU4OTI3NDkxOTUzNjg0LHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5Yngkekhhbk9Gb2F2SEZrMVFvX0JaVjcydyxsbmckZW4tdXMsY3IkNDQ1MTAwOTA1MSx2JDIuMCxhaWQkYUlSRldtS0thbk0tLGJpJDIyNzUxODgwNTEsbW1lJDk1OTMzMjA3NzYxNzQzNDk0MDgsciQwLHlvbyQxLGFncCQzNDc2NTAyNTUxLGFwJFRYVEwpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->", "id": "TXTL", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['aIRFWmKKanM-']='(as$13afd2cqv,aid$aIRFWmKKanM-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13afd2cqv,aid$aIRFWmKKanM-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13afd2cqv,aid$aIRFWmKKanM-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)", "impID": "aIRFWmKKanM-", "supp_ugc": "0", "placementID": "3476502551", "creativeID": "4451009051", "serveTime": "1458927491953684", "behavior": "non_exp", "adID": "9593320776174349408", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "120x45", "bookID": "2275188051", "serveType": "-1", "slotID": "3", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(160hsspq5(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,srv$1,si$4452051,ct$25,exp$1458934691953684,adv$26679945979,li$3474892551,cr$4451009051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1458934691953\", \"fdb_intl\": \"en-US\" }", "slotData": {"pt":"3","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","adjf":"1.000000","alpha":"1.000000","ffrac":"1.000000","pcpm":"-1.000000","fc":"false","ecpm":0.000000,"sdate":"1446744408","edate":"1514782799","bimpr":0.000000,"pimpr":-538267200.000000,"spltp":100.000000,"frp":"false","pvid":"km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } } ], "meta": { "y": { "pageEndHTML": "<scr"+"ipt language=javascr"+"ipt>\n(function(){window.xzq_p=function(R){M=R};window.xzq_svr=function(R){J=R};function F(S){var T=document;if(T.xzq_i==null){T.xzq_i=new Array();T.xzq_i.c=0}var R=T.xzq_i;R[++R.c]=new Image();R[R.c].src=S}window.xzq_sr=function(){var S=window;var Y=S.xzq_d;if(Y==null){return }if(J==null){return }var T=J+M;if(T.length>P){C();return }var X=\"\";var U=0;var W=Math.random();var V=(Y.hasOwnProperty!=null);var R;for(R in Y){if(typeof Y[R]==\"string\"){if(V&&!Y.hasOwnProperty(R)){continue}if(T.length+X.length+Y[R].length<=P){X+=Y[R]}else{if(T.length+Y[R].length>P){}else{U++;N(T,X,U,W);X=Y[R]}}}}if(U){U++}N(T,X,U,W);C()};function N(R,U,S,T){if(U.length>0){R+=\"&al=\"}F(R+U+\"&s=\"+S+\"&r=\"+T)}function C(){window.xzq_d=null;M=null;J=null}function K(R){xzq_sr()}function B(R){xzq_sr()}function L(U,V,W){if(W){var R=W.toString();var T=U;var Y=R.match(new RegExp(\"\\\\\\\\(([^\\\\\\\\)]*)\\\\\\\\)\"));Y=(Y[1].length>0?Y[1]:\"e\");T=T.replace(new RegExp(\"\\\\\\\\([^\\\\\\\\)]*\\\\\\\\)\",\"g\"),\"(\"+Y+\")\");if(R.indexOf(T)<0){var X=R.indexOf(\"{\");if(X>0){R=R.substring(X,R.length)}else{return W}R=R.replace(new RegExp(\"([^a-zA-Z0-9$_])this([^a-zA-Z0-9$_])\",\"g\"),\"$1xzq_this$2\");var Z=T+\";var rv = f( \"+Y+\",this);\";var S=\"{var a0 = '\"+Y+\"';var ofb = '\"+escape(R)+\"' ;var f = new Function( a0, 'xzq_this', unescape(ofb));\"+Z+\"return rv;}\";return new Function(Y,S)}else{return W}}return V}window.xzq_eh=function(){if(E||I){this.onload=L(\"xzq_onload(e)\",K,this.onload,0);if(E&&typeof (this.onbeforeunload)!=O){this.onbeforeunload=L(\"xzq_dobeforeunload(e)\",B,this.onbeforeunload,0)}}};window.xzq_s=function(){setTimeout(\"xzq_sr()\",1)};var J=null;var M=null;var Q=navigator.appName;var H=navigator.appVersion;var G=navigator.userAgent;var A=parseInt(H);var D=Q.indexOf(\"Microsoft\");var E=D!=-1&&A>=4;var I=(Q.indexOf(\"Netscape\")!=-1||Q.indexOf(\"Opera\")!=-1)&&A>=4;var O=\"undefined\";var P=2000})();\n</scr"+"ipt><scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_svr)xzq_svr('https://csc.beap.bc.yahoo.com/');\nif(window.xzq_p)xzq_p('yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3');\nif(window.xzq_s)xzq_s();\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1366cu6rd(gid$km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8,st$1458927491953684,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3\"></noscr"+"ipt><scr"+"ipt>(function(c){var d=\"https://\",a=c&&c.JSON,e=\"ypcdb\",g=document,b;function j(n,q,p,o){var m,r;try{m=new Date();m.setTime(m.getTime()+o*1000);g.cookie=[n,\"=\",encodeURIComponent(q),\"; domain=\",p,\"; path=/; max-age=\",o,\"; expires=\",m.toUTCString()].join(\"\")}catch(r){}}function k(m){return function(){i(m)}}function i(n){var m,o;try{m=new Image();m.onerror=m.onload=function(){m.onerror=m.onload=null;m=null};m.src=n}catch(o){}}function f(o){var p=\"\",n,s,r,q;if(o){try{n=o.match(/^https?:\\/\\/([^\\/\\?]*)(yahoo\\.com|yimg\\.com|flickr\\.com|yahoo\\.net|rivals\\.com)(:\\d+)?([\\/\\?]|$)/);if(n&&n[2]){p=n[2]}n=(n&&n[1])||null;s=n?n.length-1:-1;r=n&&s>=0?n[s]:null;if(r&&r!=\".\"&&r!=\"/\"){p=\"\"}}catch(q){p=\"\"}}return p}function l(B,n,q,m,p){var u,s,t,A,r,F,z,E,C,y,o,D,x,v=1000,w=v;try{b=location}catch(z){b=null}try{if(a){C=a.parse(p)}else{y=new Function(\"return \"+p);C=y()}}catch(z){C=null}if(y){y=null}try{s=b.hostname;t=b.protocol;if(t){t+=\"//\"}}catch(z){s=t=\"\"}if(!s){try{A=g.URL||b.href||\"\";r=A.match(/^((http[s]?)\\:[\\/]+)?([^:\\/\\s]+|[\\:\\dabcdef\\.]+)/i);if(r&&r[1]&&r[3]){t=r[1]||\"\";s=r[3]||\"\"}}catch(z){t=s=\"\"}}if(!s||!C||!t||!q){return}A=g.URL||b.href||\"\";E=f(A);if(!E||g.cookie.indexOf(\"ypcdb=\"+n)>-1){return}if(t===d){q=m}u=0;while(F=q[u++]){o=F.lastIndexOf(\"=\");if(o!=-1){D=F.substr(1+o);x=C[D];if(x){setTimeout(k(t+F+x),w);w+=v}}}u=0;while(F=B[u++]){setTimeout(k(t+F),w);w+=v}setTimeout(function(){j(e,n,E,86400)},w)}function h(){l(['ads.yahoo.com/get-user-id?ver=2&s=800000001&type=redirect&ts=1458927491&sig=189fd73e04856760','ads.yahoo.com/get-user-id?ver=2&s=800000004&type=redirect&ts=1458927491&sig=37684b039361ecff','ads.yahoo.com/get-user-id?ver=2&s=800000003&type=redirect&ts=1458927491&sig=a9acb3e1a241fe0e','ads.yahoo.com/get-user-id?ver=2&s=800000008&type=redirect&ts=1458927491&sig=df15efdb167de014'],'bc978578055c4d81af2f9f38edfc77e2',['csync.flickr.com/csync?ver=2.1','csync.yahooapis.com/csync?ver=2.1'],['csync.flickr.com/csync?ver=2.1','csync.yahooapis.com/csync?ver=2.1'],'{\"2.1\":\"&id=23351&value=8onnq79osngf3%26o%3d3%26f%3d8v&optout=&timeout=1458927491&sig=11gc4d56s\"}')}if(c.addEventListener){c.addEventListener(\"load\",h,false)}else{if(c.attachEvent){c.attachEvent(\"onload\",h)}else{c.onload=h}}})(window);\n</scr"+"ipt>", "pos_list": [ "FPAD","LREC","MAST","TXTL" ], "filtered": [  ], "spaceID": "2023538075", "host": "www.yahoo.com", "lookupTime": "83", "k2_uri": "", "fac_rt": "-1", "serveTime":"1458927491953684", "pvid": "km9hVDk4LjGFqU06VvV3gxxuMjAwMVb1d4MRB4s8", "tID": "darla_prefetch_1458927491953_598511490_1", "npv": "0", "ep": "{\"site-attribute\":\"fdn=1 Y-BUCKET=\\\"201\\\" ctout=320\",\"secure\":true,\"lang\":\"en-US\",\"ref\":\"https:\\/\\/www.yahoo.com\",\"filter\":\"no_expandable;\",\"darlaID\":\"darla_instance_1458927491951_157733509_0\"}", "pe": "CWZ1bmN0aW9uIGRwZWQoKSB7IAoJaWYod2luZG93Lnh6cV9kPT1udWxsKXdpbmRvdy54enFfZD1uZXcgT2JqZWN0KCk7CndpbmRvdy54enFfZFsnRW9CRldtS0thbk0tJ109JyhhcyQxMjVvYmNiaGYsYWlkJEVvQkZXbUtLYW5NLSxjciQtMSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1GUEFEKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydoSUZGV21LS2FuTS0nXT0nKGFzJDEzYTdidmhwNSxhaWQkaElGRldtS0thbk0tLGJpJDIyNjQxODQwNTEsYWdwJDM0NTk0NjA1NTEsY3IkNDQ2NzA0OTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1MUkVDKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWyc5b0pGV21LS2FuTS0nXT0nKGFzJDEyNXVpZXU1cSxhaWQkOW9KRldtS0thbk0tLGNyJC0xLGN0JDI1LGF0JEgsZW9iJGdkMV9tYXRjaF9pZD0tMTp5cG9zPU1BU1QpJztpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydhSVJGV21LS2FuTS0nXT0nKGFzJDEzYWZkMmNxdixhaWQkYUlSRldtS0thbk0tLGJpJDIyNzUxODgwNTEsYWdwJDM0NzY1MDI1NTEsY3IkNDQ1MTAwOTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1UWFRMKSc7CgkJIH07CmRwZWQudHJhbnNJRCA9ICJkYXJsYV9wcmVmZXRjaF8xNDU4OTI3NDkxOTUzXzU5ODUxMTQ5MF8xIjsKCglmdW5jdGlvbiBkcGVyKCkgeyAKCQppZih3aW5kb3cueHpxX3N2cil4enFfc3ZyKCdodHRwczovL2NzYy5iZWFwLmJjLnlhaG9vLmNvbS8nKTsKaWYod2luZG93Lnh6cV9wKXh6cV9wKCd5aT9idj0xLjAuMCZicz0oMTM2NmN1NnJkKGdpZCRrbTloVkRrNExqR0ZxVTA2VnZWM2d4eHVNakF3TVZiMWQ0TVJCNHM4LHN0JDE0NTg5Mjc0OTE5NTM2ODQsc2kkNDQ1MjA1MSxzcCQyMDIzNTM4MDc1LHB2JDEsdiQyLjApKSZ0PUpfMy1EXzMnKTsKaWYod2luZG93Lnh6cV9zKXh6cV9zKCk7CgoKCShmdW5jdGlvbihjKXt2YXIgZD0iaHR0cHM6Ly8iLGE9YyYmYy5KU09OLGU9InlwY2RiIixnPWRvY3VtZW50LGI7ZnVuY3Rpb24gaihuLHEscCxvKXt2YXIgbSxyO3RyeXttPW5ldyBEYXRlKCk7bS5zZXRUaW1lKG0uZ2V0VGltZSgpK28qMTAwMCk7Zy5jb29raWU9W24sIj0iLGVuY29kZVVSSUNvbXBvbmVudChxKSwiOyBkb21haW49IixwLCI7IHBhdGg9LzsgbWF4LWFnZT0iLG8sIjsgZXhwaXJlcz0iLG0udG9VVENTdHJpbmcoKV0uam9pbigiIil9Y2F0Y2gocil7fX1mdW5jdGlvbiBrKG0pe3JldHVybiBmdW5jdGlvbigpe2kobSl9fWZ1bmN0aW9uIGkobil7dmFyIG0sbzt0cnl7bT1uZXcgSW1hZ2UoKTttLm9uZXJyb3I9bS5vbmxvYWQ9ZnVuY3Rpb24oKXttLm9uZXJyb3I9bS5vbmxvYWQ9bnVsbDttPW51bGx9O20uc3JjPW59Y2F0Y2gobyl7fX1mdW5jdGlvbiBmKG8pe3ZhciBwPSIiLG4scyxyLHE7aWYobyl7dHJ5e249by5tYXRjaCgvXmh0dHBzPzpcL1wvKFteXC9cP10qKSh5YWhvb1wuY29tfHlpbWdcLmNvbXxmbGlja3JcLmNvbXx5YWhvb1wubmV0fHJpdmFsc1wuY29tKSg6XGQrKT8oW1wvXD9dfCQpLyk7aWYobiYmblsyXSl7cD1uWzJdfW49KG4mJm5bMV0pfHxudWxsO3M9bj9uLmxlbmd0aC0xOi0xO3I9biYmcz49MD9uW3NdOm51bGw7aWYociYmciE9Ii4iJiZyIT0iLyIpe3A9IiJ9fWNhdGNoKHEpe3A9IiJ9fXJldHVybiBwfWZ1bmN0aW9uIGwoQixuLHEsbSxwKXt2YXIgdSxzLHQsQSxyLEYseixFLEMseSxvLEQseCx2PTEwMDAsdz12O3RyeXtiPWxvY2F0aW9ufWNhdGNoKHope2I9bnVsbH10cnl7aWYoYSl7Qz1hLnBhcnNlKHApfWVsc2V7eT1uZXcgRnVuY3Rpb24oInJldHVybiAiK3ApO0M9eSgpfX1jYXRjaCh6KXtDPW51bGx9aWYoeSl7eT1udWxsfXRyeXtzPWIuaG9zdG5hbWU7dD1iLnByb3RvY29sO2lmKHQpe3QrPSIvLyJ9fWNhdGNoKHope3M9dD0iIn1pZighcyl7dHJ5e0E9Zy5VUkx8fGIuaHJlZnx8IiI7cj1BLm1hdGNoKC9eKChodHRwW3NdPylcOltcL10rKT8oW146XC9cc10rfFtcOlxkYWJjZGVmXC5dKykvaSk7aWYociYmclsxXSYmclszXSl7dD1yWzFdfHwiIjtzPXJbM118fCIifX1jYXRjaCh6KXt0PXM9IiJ9fWlmKCFzfHwhQ3x8IXR8fCFxKXtyZXR1cm59QT1nLlVSTHx8Yi5ocmVmfHwiIjtFPWYoQSk7aWYoIUV8fGcuY29va2llLmluZGV4T2YoInlwY2RiPSIrbik+LTEpe3JldHVybn1pZih0PT09ZCl7cT1tfXU9MDt3aGlsZShGPXFbdSsrXSl7bz1GLmxhc3RJbmRleE9mKCI9Iik7aWYobyE9LTEpe0Q9Ri5zdWJzdHIoMStvKTt4PUNbRF07aWYoeCl7c2V0VGltZW91dChrKHQrRit4KSx3KTt3Kz12fX19dT0wO3doaWxlKEY9Qlt1KytdKXtzZXRUaW1lb3V0KGsodCtGKSx3KTt3Kz12fXNldFRpbWVvdXQoZnVuY3Rpb24oKXtqKGUsbixFLDg2NDAwKX0sdyl9ZnVuY3Rpb24gaCgpe2woWydhZHMueWFob28uY29tL2dldC11c2VyLWlkP3Zlcj0yJnM9ODAwMDAwMDAxJnR5cGU9cmVkaXJlY3QmdHM9MTQ1ODkyNzQ5MSZzaWc9MTg5ZmQ3M2UwNDg1Njc2MCcsJ2Fkcy55YWhvby5jb20vZ2V0LXVzZXItaWQ/dmVyPTImcz04MDAwMDAwMDQmdHlwZT1yZWRpcmVjdCZ0cz0xNDU4OTI3NDkxJnNpZz0zNzY4NGIwMzkzNjFlY2ZmJywnYWRzLnlhaG9vLmNvbS9nZXQtdXNlci1pZD92ZXI9MiZzPTgwMDAwMDAwMyZ0eXBlPXJlZGlyZWN0JnRzPTE0NTg5Mjc0OTEmc2lnPWE5YWNiM2UxYTI0MWZlMGUnLCdhZHMueWFob28uY29tL2dldC11c2VyLWlkP3Zlcj0yJnM9ODAwMDAwMDA4JnR5cGU9cmVkaXJlY3QmdHM9MTQ1ODkyNzQ5MSZzaWc9ZGYxNWVmZGIxNjdkZTAxNCddLCdiYzk3ODU3ODA1NWM0ZDgxYWYyZjlmMzhlZGZjNzdlMicsWydjc3luYy5mbGlja3IuY29tL2NzeW5jP3Zlcj0yLjEnLCdjc3luYy55YWhvb2FwaXMuY29tL2NzeW5jP3Zlcj0yLjEnXSxbJ2NzeW5jLmZsaWNrci5jb20vY3N5bmM/dmVyPTIuMScsJ2NzeW5jLnlhaG9vYXBpcy5jb20vY3N5bmM/dmVyPTIuMSddLCd7IjIuMSI6IiZpZD0yMzM1MSZ2YWx1ZT04b25ucTc5b3NuZ2YzJTI2byUzZDMlMjZmJTNkOHYmb3B0b3V0PSZ0aW1lb3V0PTE0NTg5Mjc0OTEmc2lnPTExZ2M0ZDU2cyJ9Jyl9aWYoYy5hZGRFdmVudExpc3RlbmVyKXtjLmFkZEV2ZW50TGlzdGVuZXIoImxvYWQiLGgsZmFsc2UpfWVsc2V7aWYoYy5hdHRhY2hFdmVudCl7Yy5hdHRhY2hFdmVudCgib25sb2FkIixoKX1lbHNle2Mub25sb2FkPWh9fX0pKHdpbmRvdyk7CgoKCQogfTsKZHBlci50cmFuc0lEID0iZGFybGFfcHJlZmV0Y2hfMTQ1ODkyNzQ5MTk1M181OTg1MTE0OTBfMSI7Cgo=", "pym": "" } } } </script>    <script type="text/javascript">
        var pageloadValidAds = ["TXTL","LREC"];
        var mastRotationEnabled = 0;
        var bucketSAEnabled = true;
        var facCustomTimout = 380;
        var w = window, D = w.DARLA,
            C = {"useYAC":0,"usePE":0,"servicePath":"https:\/\/www.yahoo.com\/sdarla\/php\/fc.php","xservicePath":"","beaconPath":"https:\/\/www.yahoo.com\/sdarla\/php\/b.php","renderPath":"","allowFiF":false,"srenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/html\/r-sf.html","renderFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/html\/r-sf.html","sfbrenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/html\/r-sf.html","msgPath":"https:\/\/www.yahoo.com\/sdarla\/2-9-9\/html\/msg.html","cscPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/html\/r-csc.html","root":"sdarla","edgeRoot":"http:\/\/l.yimg.com\/rq\/darla\/2-9-9","sedgeRoot":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9","version":"2-9-9","tpbURI":"","hostFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/js\/g-r-min.js","beaconsDisabled":true,"rotationTimingDisabled":true,"fdb_locale":"What don't you like about this ad?|It's offensive|Something else|Thank you for helping us improve your Yahoo experience|It's not relevant|It's distracting|I don't like this ad|Send|Done|Why do I see ads?|Learn more about your feedback.","positions":{"DEFAULT":{"supports":false},"FPAD":[],"LREC":{"w":300,"h":250},"MAST":[],"TXTL":{"w":120,"h":45}},"lang":"en-US"},
            _adPerfBeaconData,
            _pendingAds = [],
            _adLT = [];
        var safeframeOptinPositions = {"FPAD":true};
                window._navStart = function _navStart () {
            if (window.performance) {
                if (window.performance.clearMeasures) {
                    try {
                        window.performance.clearMeasures();
                    } catch (ex) {
                    }
                }
                if (window.performance.clearMarks) {
                    try {
                        window.performance.clearMarks();
                    } catch (ex) {
                    }
                }
                _perfMark('MODAL_OPEN');
            }
            window._pendingAds = [];
            window._perfBackfillAdBeacon = false;
            window._perfVideoInitBeacon = false;
            window._adPerfBeaconData = {positions:{}};
        };
        window._adCallStart = function _adCallStart () {
            window._perfMark('MODAL_AD_EVENT_START');
        };
        window._isModalOpen = function _isModalOpen () {
            return (document.getElementsByTagName('html')[0].className.match(/Reader-open/) && !document.getElementsByTagName('html')[0].className.match(/Reader-closed/));
        };
        window._perfMark = function _perfMark (name) {
            if (window.performance && window.performance.mark) {
                window.performance.mark(name);
            }
        };
        window._perfBackfillAd = function _perfBackfillAd() {
                for (var i in window._adPerfBeaconData.positions) {
                    if (window._adPerfBeaconData.positions.hasOwnProperty(i) && i.match(/LREC-/)) {
                        window._perfMark('PSTMSG_' + i);
                        window._adPerfBeaconData.positions[i].backfill = true;
                    } else {
                        window._adPerfBeaconData.positions[i].backfill = false;
                    }
                }
        };
        window._backfillVideoStart = function _backfillVideoStart() {
            window._perfMark('PSTMSG_VIDSTART');
        };
        window._perfModalFetchStart = function _perfModalFetchStart() {
            window._perfMark('MODAL_FETCH_START');
        };
        window._perfModalFetchEnd = function _perfModalFetchEnd() {
            window._perfMark('MODAL_FETCH_END');
        };
        window._perfModalVideoInit = function _perfModalVideoInit() {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfVideoInitBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfVideoInitBeacon = true;
                window._perfMark('MODAL_VIDEO_INIT');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                modalVideoInit = Math.round(window.performance.getEntriesByName('MODAL_VIDEO_INIT')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'modal_video_playback_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            modalVideoInit: modalVideoInit
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._backfillVideoPlaybackStart = function _backfillVideoPlaybackStart(isAd) {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfBackfillAdBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfBackfillAdBeacon = true;
                window._perfMark('PSTMSG_PBSTART');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                videoInit = Math.round(window.performance.getEntriesByName('PSTMSG_VIDSTART')[0].startTime - modalStart);
                playbackStart = Math.round(window.performance.getEntriesByName('PSTMSG_PBSTART')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'brightroll_backfill_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            videoInit: videoInit,
                            videoAd: isAd,
                            playbackStart: playbackStart
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._adRenderComplete = function _adRenderComplete() {
            if (window.performance && window.performance.getEntriesByName) {
                var modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0];
                var modalFetchStart = window.performance.getEntriesByName('MODAL_FETCH_START');
                var modalFetchEnd = window.performance.getEntriesByName('MODAL_FETCH_END');
                var rapidCustomData = [];
                var darlaFetchPerf= {};
                var rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                var modalStartTime = modalStart.startTime;

                darlaFetchPerf['name'] = 'darla_info';
                darlaFetchPerf['spaceid'] = window._adPerfBeaconData.spaceId;
                darlaFetchPerf['pvid'] = window._adPerfBeaconData.pvid;
                darlaFetchPerf['site'] = window._adPerfBeaconData.property || 'fp';
                darlaFetchPerf['lang'] = window.Af.context.lang;
                darlaFetchPerf['region'] =  window.Af.context.region;
                darlaFetchPerf['device'] =  window.Af.context.device;
                darlaFetchPerf['rid'] =  window.Af.context.rid;
                darlaFetchPerf['bucket'] = '201';

                if (modalFetchStart && modalFetchStart.length > 0) {
                    darlaFetchPerf['modalFetchStart'] =  Math.round(modalFetchStart[0].startTime -modalStartTime);
                    darlaFetchPerf['modalFetchEnd'] =  Math.round(modalFetchEnd[0].startTime -modalStartTime);
                }
                darlaFetchPerf['eventStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_EVENT_START')[0].startTime -modalStartTime);
                darlaFetchPerf['wsStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_START')[0].startTime - modalStartTime);
                darlaFetchPerf['wsEnd'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_END')[0].startTime - modalStartTime);
                rapidCustomData.push(darlaFetchPerf);

                for (var i in window._adPerfBeaconData.positions) {
                    var darlaPositionPerf = {};
                    var posInfo = window._adPerfBeaconData.positions.hasOwnProperty(i) && window._adPerfBeaconData.positions[i];
                    if(posInfo) {
                        darlaPositionPerf['name'] = i + '_info';
                        darlaPositionPerf['valid'] = posInfo.valid ? 1 : 0;
                        if (posInfo.valid) {
                            darlaPositionPerf['renderStart'] =  Math.round(window.performance.getEntriesByName('ADSTART_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['renderEnd'] =  Math.round(window.performance.getEntriesByName('ADEND_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['bookId'] = posInfo.bookId;
                            darlaPositionPerf['creativeId'] = posInfo.creativeId;
                            darlaPositionPerf['placementId'] = posInfo.placementId;
                            darlaPositionPerf['size'] = posInfo.size;
                            darlaPositionPerf['backfill'] = posInfo.backfill ? 1 : 0;
                            if (posInfo.backfill) {
                                darlaPositionPerf['pstmsg'] = Math.round(window.performance.getEntriesByName('PSTMSG_' + i)[0].startTime - modalStartTime);
                            }
                        }
                        rapidCustomData.push(darlaPositionPerf);
                    }
                }

                if (rapidInstance) {
                    var customPerfData = {'utm': rapidCustomData};
                    var rapidPerfData = {perf_usertime: customPerfData};
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        if (D && C) {
            C.positions = {"FPAD":{"clean":"my-adsFPAD","dest":"my-adsFPAD-iframe","metaSize":1,"fdb":"true,","supports":{"resize-to":1,"exp-ovr":1,"exp-push":1,"lyr":1}},"LREC":{"pos":"LREC","clean":"my-adsLREC","dest":"my-adsLREC-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":0,"lyr":0}},"MAST":{"pos":"MAST","clean":"my-adsMAST","dest":"my-adsMAST-iframe","fr":"expIfr_exp","rmxp":0,"metaSize":true,"w":970,"h":250,"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"closeBtn":{"mode":2,"useShow":1,"adc":0}},"TXTL":{"pos":"TXTL","id":"TXTL","clean":"my-adsTXTL","dest":"my-adsTXTL-iframe","w":120,"h":170,"fr":"expIfr_exp","rmxp":0,"css":".ad-tl2b {overflow:hidden; text-align:left;} p {margin:0px;} a {color:#020e65;} a:hover {color:#0078ff;} .y-fp-pg-controls {margin-top:5px; margin-bottom:5px;} #tl1_slug {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px; color:#999;} #fc_align a {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px;} a:link {text-decoration:none;}"}};
            C.k2={"res":{"rate":100,"pos":["LREC","MAST","FPAD","LREC2","LREC3","LREC-0","LREC2-0","LREC3-0","MAST-0","LDRB-0","SPL2-0","SPL-0","MON-0"]}};
            C.k2E2ERate=100;
C.k2Rate=100;
C.so=1;

            C.events = {"DEFAULT":{"clw":{"LREC-0":{"blocked_by":"MON-0"},"MON-0":{"blocked_by":"LREC-0"},"MAST-0":{"blocked_by":"LDRB-0,SPL-0"},"SPL-0":{"blocked_by":"LDRB-0,MAST-0"},"LDRB-0":{"blocked_by":"MAST-0,SPL-0"}}}};
            
                    C.onStartRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_START');
            }
        };
                    C.onFinishRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_END');
            }
        };
                        C.onFinishParse = function(eventName, result) {
                var ps = result.ps(),
                    modalOpen = false,
                    position, posItem, curAd, curEvt,
                    validPositions = {},
                    isMONFetch = false;
                            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('AD_PARSE');
                modalOpen = true;
                window._adPerfBeaconData.spaceId = result.spaceID;
                window._adPerfBeaconData.pvid = result.pvid;
                curEvt = DARLA.evtSettings(eventName);
                if (curEvt.property) {
                    window._adPerfBeaconData.property = curEvt.property;
                }
            }
                        function fireBeacon(position, size, pageMode) {
            if (position && size && pageMode) {
                try {
                    var img = new Image();
                    img.src = '/p.gif?beaconType=darla&pos=' + position + '&size=' + size + '&mode=' +pageMode + '&bucket=' + 201 + '&rid=' + "2r783utbfats3";
                } catch (e) {
                    if (console) {
                        console.log('failed to send darla beacon :', e);
                    }
                }
            }
        }

                if (ps && ps.length) {
                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
            for (i = 0, l = ps.length; i < l; i++) {
                position = ps[i];
                posItem = result.item(position);
                if (posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    validPositions[position] = false;
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = false;
                    }
                } else {
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = true;
                    }
                    validPositions[position] = true;
                }
            }
            if (YMedia && YMedia.Af && YMedia.Af.Event && YMedia.Af.Event.fire) {
                YMedia.Af.Event.fire('sidekickrefresh', validPositions);
            }
        }

                    for (i = 0, l = ps.length; i < l; i++) {
                        position = ps[i];
                        posItem = result.item(position);
                                    if (modalOpen) {
                if (posItem.err || posItem.hasErr) {
                    window._adPerfBeaconData.positions[position] = {error: true};
                } else if (posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    window._adPerfBeaconData.positions[position] = {size: '1x1'};
                } else {
                    window._adPerfBeaconData.positions[position] = {valid: true};
                    if (posItem.meta && posItem.meta.y) {
                        window._adPerfBeaconData.positions[position].size = posItem.meta.y.size;
                        window._adPerfBeaconData.positions[position].bookId = posItem.meta.y.bookID;
                        window._adPerfBeaconData.positions[position].creativeId = posItem.meta.y.creativeID;
                        window._adPerfBeaconData.positions[position].placementId = posItem.meta.y.placementID;
                    }
                    window._pendingAds.push(position);
                }
            }
                        if (posItem && posItem.conf && posItem.conf.clean) {
                            curAd = document.getElementById(posItem.conf.clean);
                            if (curAd) {
                                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                var posName = position.split('-')[0];
                if ((posName === "LDRB" || posName === "MAST" || posName === "SPL" || posName === "SPL2") &&
                   (!posItem.hasErr && posItem.size + '' !== '1x1')) {
                    curAd.className = curAd.className.replace('D-n', 'D-ib');
                    if (posName !== "SPL" && posName !== "SPL2") {
                        curAd.className = curAd.className + " Bdb-Grey-1";
                    }
                }
            }
            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                if (position.match(/^SPL/)) {
                    if (posItem.hasErr || posItem.size + '' === '1x1') {
                        if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                            YMedia.one(curAd).setStyle('padding-top', '0px');
                        }
                    } else if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                        var splashWrap = YMedia.one('#' + posItem.conf.clean + '-wrap');
                        splashWrap.setStyle('padding-top', (5/12*100)+'%');
                        splashWrap.setStyle('width', '100%');
                        splashWrap.setStyle('height', '0px');
                        splashWrap.addClass('Mt-20 Mb-50');
                    }
                }
            }
            if ((eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') && position.indexOf("LREC") > -1) {
              if (posItem && posItem.conf && posItem.conf.clean) {
                var fallbackDiv = document.getElementById(posItem.conf.clean + "-fallback");
                }
                if ((posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1'))  && (!isMONFetch || position !== 'LREC-0')) {
                    var lrecBackfillString = "<a href=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=20&mc=click&pli=16563095&PluID=0&ord=${CACHEBUSTER}\" style=\"background:url(https:\/\/www.yahoo.com\/sy\/nn\/lib\/metro\/300_250_Human_Touch_mail.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;margin:auto;\"><\/a><img width=\"1\" height=\"1\" alt=\"\" src=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=19&mc=imp&pli=16563095&PluID=0&ord=${CACHEBUSTER}&rtu=-1\" style=\"display:none\">";
                    if (fallbackDiv) {
                       fallbackDiv.innerHTML = lrecBackfillString;
                       fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                    }
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    curAd.className = curAd.className.replace(/hl-ad-LREC/, '');
                } else {
                    if (fallbackDiv && fallbackDiv.className.indexOf("D-n") < 0) {
                      fallbackDiv.className += ' D-n';
                    }
                    if (isMONFetch && position === 'LREC-0') {
                        curAd.parentElement.style.height = '600px';
                        var curAd = document.getElementById(posItem.conf.clean);
                        if (curAd.className.indexOf("D-n") < 0  && curAd.className.indexOf("D-ib") >= 0) {
                          curAd.className = curAd.className.replace(/D-ib/, 'D-n');
                        }
                    }
                }
            }
            if (eventName === 'prefetch' && position.indexOf("LREC") > -1) {
                curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
            }
            if (eventName === 'fetch_selective_ad_lrec2' || eventName === 'fetch_selective_ad_lrec3') {
                var fallbackDiv;
                if (position === 'LREC2' || position === 'LREC3') {
                    fallbackDiv = document.getElementById("my-adsLREC2-fallback");
                } else {
                    fallbackDiv = document.getElementById("my-ads" + position + "-fallback");
                }

                if (fallbackDiv) {
                    if (posItem.hasErr || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1')) {
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                        curAd.className = curAd.className.replace('Ht-250', '');
                        // hide div if ad fetch failed or is 1x1
                        if (curAd.className.indexOf("D-n") < 0) {
                            curAd.className += ' D-n';
                        }
                        fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                    } else {
                        if (fallbackDiv.className.indexOf("D-n") < 0) {
                            fallbackDiv.className += ' D-n';
                        }
                        curAd.className = curAd.className.replace('D-n', '');
                    }

                    if (position === 'LREC3') {
                        var lrec2Div = document.getElementById("my-adsLREC2");
                        if (lrec2Div && lrec2Div.className.indexOf("D-n") < 0) {
                            lrec2Div.className += ' D-n';
                        }
                    }
                }
            }


                            }
                        }
                    }
                }
                            if (modalOpen && window._pendingAds.length === 0) {
                window._adRenderComplete();
            }
            };

                        C.onStartPosRender = function(posItem) {
                if (window.performance  && window.performance.now) {
                    var ltime = window.performance.now(),
                        posId = posItem && posItem.pos;
                    _adLT.push(['ADSTART_'+posId, Math.round(ltime)]);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADSTART_' + posId);
                }
            };

                        C.onBeforeStartPosRender = function(posItem) {
                if (posItem && safeframeOptinPositions && safeframeOptinPositions[posItem.pos]) {
                    if (posItem.html && posItem.html.match(/<!--[^>]*sfoptout[^>]*-->/)) {
                        return true;
                    }
                }
            };

                        C.onFinishPosRender = function(posId, reqList, posItem) {
                var curAd = document.getElementById("my-ads"+posId),
                    adIndex,
                    posSize = (posItem && posItem.meta && posItem.meta.value("size", "y"));

                // Get clean div for ad position in case defined
                if (posItem && posItem.conf && posItem.conf.clean) {
                    curAd = document.getElementById(posItem.conf.clean);
                }
                if (curAd) {
                    // Let ad take its original size, remove default height given to ad div
                    curAd.className = curAd.className.replace('Ht-250', '');
                    curAd.className = curAd.className.replace('Ht-MAST', '');

                    if(posSize && posSize != "1x1") {
                        curAd.className = curAd.className.replace('D-n', '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    }
                }

                if (window.performance !== undefined && window.performance.now !== undefined) {
                    var whiteListedAds = {"LREC":"my-adsLREC-base","MAST":"my-adsMAST","TL1":"my-adsTL1","TXTL":"my-adsTXTL","LREC-0":"hl-ad-LREC-0","MON-0":"hl-ad-MON-0","MAST-0":"hl-ad-MAST-0","LDRB-0":"hl-ad-LDRB-0","SPL2-0":"hl-ad-SPL2-0","SPL-0":"hl-ad-SPL-0"},
                      ltime = window.performance.now();
                     _adLT.push(['ADEND_'+posId, Math.round(ltime)]);
                    setTimeout(function () {
                        if (window.YAFT !== undefined && window.YAFT.isInitialized() && whiteListedAds[posId]) {
                            // Trigger custom timing for LREC ad position
                            window.YAFT.triggerCustomTiming(whiteListedAds[posId], '', ltime);
                        }
                    },300);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADEND_' + posId);
                    adIndex = window._pendingAds.indexOf(posId);
                    if (adIndex >= 0) {
                        window._pendingAds.splice(adIndex, 1);
                        if (window._pendingAds.length === 0) {
                            window._adRenderComplete();
                        }
                    }
                }
            };

            C.onBeforePosMsg = function(msg_name, position) {
        // Make these configurable for INTLS
        var maxWidth = 970,
            maxHeight = 600,
            newWidth,
            newHeight,
            origWidth,
            origHeight,
            pos;

        if('MAST' !== position) {
            return;
        }

        if (msg_name === 'resize-to') {
            newWidth = arguments[2];
            newHeight = arguments[3];
        }
        else if (msg_name === 'exp-push' || msg_name === 'exp-ovr') {
            pos = $sf.host.get('MAST'),
            origWidth = pos.conf.w;
            origHeight = pos.conf.h;
            //"exp-ovr" or "exp-push", position id, delta X, delta Y, push (true /false), top increase, left increase, right increase, bottom increase
            newWidth = origWidth + arguments[6] + arguments[7];
            newHeight = origHeight + arguments[5] + arguments[8];
        }
        if(newWidth > maxWidth || newHeight > maxHeight) {
            return true;
        }
    };
                        //call back when the ad is expanded or collapsed
            C.onPosMsg = function (msg_name, data, msg_data)  {
                var visible;
                if(msg_name == "collapse" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdExpanded', '');
                    bodyTag.className += " " +  "mastAdCollapsed";
                }
                if(msg_name == "exp-push" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdCollapsed', '');
                    bodyTag.className += " " +  "mastAdExpanded";
                }

                /* generic ad expansion logic */
                if(msg_name == "collapse") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace(data + "-ad-expanded", '');
                }

                if(msg_name == "exp-ovr") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className += " " + data + "-ad-expanded";
                }

                if (msg_name === 'geom-update') {
                    visible = D.render.RenderMgr.get(data).viewedAt > 0;
                    // geom-update event will always be available when Y is available
                    if (YMedia && visible) {
                        YMedia.Global.fire('ads:beacon', {id: data});
                    }
                }
                                if (msg_name === 'cmsg') {
                    var splashConf = DARLA.posSettings(data)
                    if (YMedia && splashConf && splashConf.clean) {
                        var splashNode = document.getElementById(splashConf.clean + '-wrap');
                        if (splashNode) {
                            if (msg_data === 'splash-expand') {
                                YMedia.one(splashNode).setStyle('padding-top', (9/16*100)+'%');
                                if (typeof splashNode.scrollIntoView === 'function') {
                                    splashNode.scrollIntoView();
                                }
                            } else if (msg_data === 'splash-collapse') {
                                YMedia.one(splashNode).setStyle('padding-top', (5/12*100)+'%');
                            }
                        }
                    }
                }
            };

            

            


            if ("OK" == D.config(C)) {
                setTimeout(function() {
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_RSTART', Math.round(ltime)]);
                    }
                    var w = window,
                        d = document,
                        e = d.documentElement,
                        g = d.getElementsByTagName('body')[0],
                        winWidth = w.innerWidth || e.clientWidth || g.clientWidth;
                    if (true && winWidth < 1024 && window.pageloadValidAds && (window.pageloadValidAds.indexOf('TXTL') >= 0)) {
                        var prefetched = D.prefetched();
                        var txtlindex = prefetched.indexOf('TXTL');
                        if (txtlindex >= 0) {
                            delete(prefetched[txtlindex]);
                            for (var i=0; i < prefetched.length; i++) {
                                if (prefetched.hasOwnProperty(i)) {
                                    D.render(prefetched[i]);
                                }
                            }
                        } else {
                            D.render();
                        }
                    } else {
                        D.render();
                    }
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_REND', Math.round(ltime)]);
                    }
                }, 2);
            }
        }
    </script>
                </div>
        
        

        
        
        <input type="hidden" id="afhistorystate">
    <!-- bottom -->
    
                    <script type="text/javascript" src="/sy/zz/combo?yui:/3.18.0/yui/yui-min.js&/ss/rapid-3.29.1.js&/os/mit/td/aperollup-min-2aa81d5c_desktop_advance.js"></script>
                    <script type="text/javascript" src="/sy/zz/combo?&&/os/mit/td/td-applet-stream-atomic-2.0.484/r-min.js&/os/mit/td/td-applet-mega-header-1.0.204/r-min.js&/os/mit/td/td-applet-viewer-0.1.2153/r-min.js&/os/mit/td/td-applet-navlinks-atomic-0.0.57/r-min.js&/os/mit/td/td-applet-scores-atomic-1.0.10/r-min.js" async defer></script>                                <script type="text/javascript">                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                        YUI.Env.core.push.apply(YUI.Env.core,["loader-angus","loader-ape-af","loader-ape-applet","loader-ape-location","loader-ape-pipe","loader-ape-social","loader-applet-server","loader-assembler","loader-dust-helpers","loader-finance-streamer","loader-highlander-client","loader-live-event-data","loader-mjata","loader-stencil","loader-td-api","loader-td-dev-info","loader-td-lib-entertainment","loader-td-lib-hovercards","loader-td-lib-livevideo","loader-td-lib-social","loader-td-applet-stream-atomic","loader-td-applet-mega-header","loader-td-applet-viewer","loader-td-applet-navlinks-atomic","loader-td-applet-trending-atomic","loader-td-applet-scores-atomic","loader-td-applet-sidekick","loader-td-applet-related-content","loader-td-applet-mega-comments","loader-td-applet-weather-atomic","loader-td-applet-scores-atomic"]);
YUI.add("loader-ape-af",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-beacon":{group:"ape-af",requires:["json-stringify","querystring-stringify-simple","io-base"]},"af-bootstrap":{group:"ape-af",requires:["af-utils"]},"af-cache":{group:"ape-af",requires:["af-beacon","json","gallery-storage-lite"]},"af-compositeview":{group:"ape-af",requires:["parallel","af-utils"]},"af-comscore":{group:"ape-af",requires:["af-beacon","af-config","querystring-stringify-simple"]},"af-config":{group:"ape-af",requires:["af-utils","cookie"]},"af-content":{group:"ape-af",requires:["af-config","af-dom","af-utils","json-parse","mjata-rest-http","node-pluginhost","querystring-stringify"]},"af-cookie":{group:"ape-af",requires:["cookie","json"]},"af-dom":{group:"ape-af",requires:["node-base","node-core"]},"af-dwelltime":{group:"ape-af",requires:["af-rapid","json-stringify"]},"af-eu-tracking":{group:"ape-af",requires:["af-beacon","media-agof-tracking"]},"af-event":{group:"ape-af",requires:["event-custom"]},"af-history":{group:"ape-af",requires:["json"]},"af-message":{group:"ape-af",langBundles:["strings"],requires:["ape-af-templates-message","af-utils","node-base","selector-css3"]},"af-pageviz":{group:"ape-af",requires:["event-custom"]},"af-poll":{group:"ape-af",requires:["af-pageviz"]},"af-rapid":{group:"ape-af",requires:["af-bootstrap","af-utils","media-rapid-tracking","node-core"]},"af-sync":{group:"ape-af",requires:["af-transport","af-utils"]},"af-trans":{group:"ape-af",requires:["node-base","transition","af-utils"]},"af-transport":{group:"ape-af",requires:["af-beacon","af-config","af-utils","json-stringify","tdapi-remotestore"]},"af-utils":{group:"ape-af",requires:["cookie","intl","oop","querystring-parse-simple"]},"af-viewport-loader":{group:"ape-af",requires:["node","event-base"]},"ape-af-lang-strings":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_de-de":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_el-gr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ae":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-au":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-gb":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ie":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-in":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-my":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-nz":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ph":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-sg":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-za":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-cl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-co":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-es":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-mx":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-pe":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ve":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-fr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_id-id":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_it-it":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-nl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_pt-br":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ro-ro":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_sv-se":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_vi-vn":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-hk":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-tw":{group:"ape-af",requires:["intl"]},"ape-af-templates-message":{group:"ape-af",requires:["template-base","dust"]},"gallery-storage-lite":{group:"ape-af",requires:["event-base","event-custom","event-custom-complex","json","node-base"]},"media-agof-tracking":{group:"ape-af"},"media-rapid-tracking":{group:"ape-af",requires:["event-custom","base","node"]},"media-rmp":{group:"ape-af",requires:["node","io-base","async-queue","get"]},"tdapi-remotestore":{group:"ape-af",requires:["mjata-store","mjata-rest-http","json"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-applet",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-applet":{group:"ape-applet",requires:["af-applet-dom","af-applet-model","af-applet-headerview","af-bootstrap","af-compositeview","af-config","af-utils","array-extras","event-custom","node","stencil-tooltip"]},"af-applet-action":{group:"ape-applet",requires:["af-applet-dom","af-utils","node-event-delegate","node-base"]},"af-applet-dom":{group:"ape-applet",requires:["node-core","af-trans"]},"af-applet-editview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-moreinfo","ape-applet-templates-remove"]},"af-applet-headerview":{group:"ape-applet",requires:["af-applet-view","stencil-selectbox"]},"af-applet-init":{group:"ape-applet",requires:["af-applet","af-beacon","af-bootstrap","af-config","af-utils","event-touch","oop"]},"af-applet-load":{group:"ape-applet",requires:["af-applet-dom","af-message","af-transport","ape-applet-templates-reload"]},"af-applet-mgr":{group:"ape-applet",requires:["af-applet-dom","af-beacon","af-content","af-transport","af-utils"]},"af-applet-model":{group:"ape-applet",langBundles:["strings"],requires:["af-bootstrap","af-config","af-sync","base-build","mjata-json","model"]},"af-applet-savesettings":{group:"ape-applet",requires:["af-dom","af-message"]},"af-applet-settingsview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-settingswrap"]},"af-applet-view":{group:"ape-applet",requires:["af-bootstrap","af-utils","base-build","dust","view"]},"af-applet-viewmgr":{group:"ape-applet",requires:["af-applet-dom"]},"af-applets":{group:"ape-applet",requires:["af-applet-action","af-applet-init","af-applet-load","af-applet-mgr","af-applet-savesettings","af-applet-viewmgr","af-dom","af-utils"]},"ape-applet-lang-strings":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_de-de":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_el-gr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ae":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-au":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-gb":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ie":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-in":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-my":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-nz":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ph":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-sg":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-za":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-cl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-co":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-es":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-mx":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-pe":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ve":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-fr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_id-id":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_it-it":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-nl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_pt-br":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ro-ro":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_sv-se":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_vi-vn":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-hk":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-tw":{group:"ape-applet",requires:["intl"]},"ape-applet-templates-moreinfo":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-reload":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-remove":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-settingswrap":{group:"ape-applet",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-location",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-location-detection":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-detection"]},"af-location-panel":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-list","ape-location-templates-location-panel","stencil-toggle","stencil-tooltip"]},"af-locations":{group:"ape-location",requires:["af-sync","af-utils","mjata-model-base","mjata-model","mjata-lazy-modellist","mjata-model-store"]},"ape-location-lang-strings":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_de-de":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_el-gr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ae":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-au":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-gb":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ie":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-in":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-my":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-nz":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ph":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-sg":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-za":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-cl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-co":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-es":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-mx":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-pe":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ve":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-fr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_id-id":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_it-it":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-nl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_pt-br":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ro-ro":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_sv-se":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_vi-vn":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-hk":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-tw":{group:"ape-location",requires:["intl"]},"ape-location-templates-location-detection":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-list":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-panel":{group:"ape-location",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-pipe",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-comet":{group:"ape-pipe",requires:["af-beacon","af-config","comet","event-custom-base","json-parse"]},"af-pipe":{group:"ape-pipe",requires:["af-beacon","af-comet","af-config","af-utils","event-custom-base"]},comet:{group:"ape-pipe"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-social":{group:"ape-social",requires:[]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-dust-helpers",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{dust:{group:"dust-helpers",requires:["stencil-imageloader","moment","intl-helper"]},"dust-helper-intl":{es:!0,group:"dust-helpers",requires:["intl-messageformat"]},"intl-helper":{group:"dust-helpers",requires:["dust-helper-intl"]},"intl-messageformat":{es:!0,group:"dust-helpers"},moment:{group:"dust-helpers"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-mjata",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mjata-bind-model2dom":{group:"mjata",requires:["mjata-model-store","mjata-template","mjata-util","node-base","node-core"]},"mjata-binder":{group:"mjata",requires:["mjata-bind-model2dom"]},"mjata-json":{group:"mjata"},"mjata-lazy-modellist":{group:"mjata",requires:["lazy-model-list","mjata-model-store"]},"mjata-model":{group:"mjata",requires:["model","mjata-json","mjata-model-store"]},"mjata-model-base":{group:"mjata",requires:["base","mjata-model","mjata-modellist","mjata-model-store"]},"mjata-model-store":{group:"mjata",requires:["event-custom","promise"]},"mjata-modellist":{group:"mjata",requires:["model-list","mjata-model-store"]},"mjata-queue":{group:"mjata"},"mjata-rest-http":{group:"mjata",requires:["json-stringify","io-base","io-xdr"]},"mjata-store":{group:"mjata",requires:["mjata-queue","mjata-util"]},"mjata-template":{group:"mjata",requires:["dust"]},"mjata-util":{group:"mjata"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-stencil",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{hammer:{group:"stencil"},stencil:{group:"stencil"},"stencil-base":{group:"stencil",requires:["stencil","yui-base","event-base","event-delegate","event-mouseenter","json-parse","dom-base","node-base","node-pluginhost","selector"]},"stencil-bquery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-style","node-base","node-pluginhost"]},"stencil-carousel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-hover"]},"stencil-fx":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","pluginhost","event-custom","selector-css3"]},"stencil-fx-collapse":{group:"stencil",requires:["stencil-fx","node-style"]},"stencil-fx-fade":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-fx-flip":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-gallery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","transition"]},"stencil-imageloader":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-screen","node-style","node-pluginhost","array-extras"]},"stencil-lightbox":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","shim-plugin"]},"stencil-scrollview":{group:"stencil",requires:["node-base","node-style","hammer"]},"stencil-selectbox":{group:"stencil",requires:["stencil-base"]},"stencil-slider":{group:"stencil",requires:["stencil"]},"stencil-source":{group:"stencil",requires:["node","oop","pluginhost","mjata-util"]},"stencil-source-af":{group:"stencil",requires:["node","stencil-source","mjata-model-store","mjata-bind-model2dom","af-applets"]},"stencil-source-dom":{group:"stencil",requires:["stencil-source"]},"stencil-source-rmp":{group:"stencil",requires:["stencil-source","stencil-source-url","media-rmp"]},"stencil-source-simple":{group:"stencil",requires:["stencil-source"]},"stencil-source-url":{group:"stencil",requires:["stencil-source","io-base","io-xdr"]},"stencil-sticker":{group:"stencil",requires:["node-base","node-style","node-screen"]},"stencil-tabpanel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-pluginhost"]},"stencil-toggle":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-focus","event-mouseenter","json-parse"]},"stencil-tooltip":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","node-style","stencil-source"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-comments",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-mega-comments-mainview":{group:"td-applet-mega-comments",requires:["af-applet-view","af-transport","af-utils","af-rapid","stencil-tooltip","td-applet-mega-comments-templates-styles"]},"td-applet-mega-comments-templates-activities":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-activity-comments"]},"td-applet-mega-comments-templates-activity-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body","td-applet-mega-comments-templates-activity-replies"]},"td-applet-mega-comments-templates-activity-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-avatar":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body"]},"td-applet-mega-comments-templates-comments-body":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-comments-editor-tools"]},"td-applet-mega-comments-templates-comments-editor-tools":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-abuse":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-comment":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-delete":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-reply":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-main":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-form-comment","td-applet-mega-comments-templates-form-reply","td-applet-mega-comments-templates-form-abuse","td-applet-mega-comments-templates-form-delete"]},"td-applet-mega-comments-templates-main-activities":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-activities"]},"td-applet-mega-comments-templates-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-replies-body":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-styles":{group:"td-applet-mega-comments",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-header",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mega-header-plugin-account-switch":{group:"td-applet-mega-header",requires:["node","event-custom","event-move","event-mouseenter","td-applet-mega-header-constants","get","anim","json-parse","escape"]},"mega-header-plugin-autocomplete":{group:"td-applet-mega-header",requires:["node","autocomplete","autocomplete-highlighters","autocomplete-list","td-applet-mega-header-constants"]},"mega-header-plugin-avatar":{group:"td-applet-mega-header",requires:["jsonp","td-applet-mega-header-constants"]},"mega-header-plugin-instant":{group:"td-applet-mega-header",requires:["node","event","af-event","json","jsonp","querystring","td-applet-mega-header-constants"]},"mega-header-plugin-mailpreview":{group:"td-applet-mega-header",requires:["jsonp","af-event","td-applet-mega-header-constants"]},"mega-header-plugin-notifications":{group:"td-applet-mega-header",requires:["af-event","af-cache","td-applet-mega-header-constants"]},"mega-header-plugin-username":{group:"td-applet-mega-header",requires:["jsonp"]},"td-applet-mega-header-constants":{group:"td-applet-mega-header"},"td-applet-mega-header-mainview":{group:"td-applet-mega-header",requires:["af-applet-view","node","td-applet-mega-header-constants","mega-header-plugin-autocomplete","mega-header-plugin-avatar","mega-header-plugin-username","mega-header-plugin-mailpreview","mega-header-plugin-notifications","mega-header-plugin-instant","mega-header-plugin-account-switch"]},"td-applet-mega-header-templates-accountSwitchPanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-autofocus_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-firefoxPromo_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-instant_filters":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-logo":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mail":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mailpreview":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-main":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-topbar","td-applet-mega-header-templates-logo","td-applet-mega-header-templates-profile","td-applet-mega-header-templates-notifications","td-applet-mega-header-templates-mail","td-applet-mega-header-templates-search","td-applet-mega-header-templates-instant_filters"]},"td-applet-mega-header-templates-notificationpanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-notifications":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-profile":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-accountSwitchPanel","td-applet-mega-header-templates-profilePanel"]},"td-applet-mega-header-templates-profilePanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-search":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-autofocus_script"]},"td-applet-mega-header-templates-topbar":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-firefoxPromo_script"]},"td-mega-header-model":{group:"td-applet-mega-header",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-navlinks-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-navlinks-atomic-templates-main":{group:"td-applet-navlinks-atomic",requires:["template-base","dust"]},"td-applet-navlinks-mainview":{group:"td-applet-navlinks-atomic",requires:["af-applet-view"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-related-content",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-related-content-model":{group:"td-applet-related-content",requires:["model","af-event"]},"td-applet-related-content-templates-main":{group:"td-applet-related-content",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-scores-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-scores-atomic-templates-footer":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-header":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-header.chiclet":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-header.collegefootball":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-score-mini-cf"]},"td-applet-scores-atomic-templates-heading-headtohead":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-main.chiclet":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-score-chiclet"]},"td-applet-scores-atomic-templates-main.collegefootball":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-main.scorestrip":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-score-mini"]},"td-applet-scores-atomic-templates-main.scorethin":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-score"]},"td-applet-scores-atomic-templates-main.tease":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-meta-mlb":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-score":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-score-chiclet":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-score-mini":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-score-mini-cf":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-settings":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-tab.header":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-tennis-match":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-tennis.schedule":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-tennis-match"]},"td-applet-scores-atomic-templates-tennis.tournaments":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-scores-atomic-footerview":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-utils"]},"td-scores-atomic-headerview":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-utils"]},"td-scores-atomic-mainview":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-config","af-poll","mjata-binder","stencil-toggle","td-applet-scores-atomic-templates-score","td-scores-atomic-model"]},"td-scores-atomic-model":{group:"td-applet-scores-atomic",requires:["mjata-model","mjata-model-base"]},"td-scores-atomic-modellist":{group:"td-applet-scores-atomic",requires:["mjata-model-base","mjata-modellist","af-sync","td-scores-atomic-model"]},"td-scores-collegefootball-modellist":{group:"td-applet-scores-atomic",requires:["mjata-model-base","mjata-modellist","af-sync","td-scores-atomic-model"]},"td-tennis-schedule-view":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-config","calendar","datatype-date"]},"td-tennis-tournaments-model":{group:"td-applet-scores-atomic",requires:["model","af-sync"]},"td-tennis-tournaments-view":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-config","td-tennis-tournaments-model"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-sidekick",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-sidekick-templates-item":{group:"td-applet-sidekick",requires:["template-base","dust"]},"td-applet-sidekick-templates-main":{group:"td-applet-sidekick",requires:["template-base","dust","td-applet-sidekick-templates-item"]},"td-sidekick-mainview":{group:"td-applet-sidekick",requires:["af-applet-view","af-config","af-event","stencil"]},"td-sidekick-model":{group:"td-applet-sidekick",requires:["af-sync","base-build","model"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-stream-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"stream-actiondrawer-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-cache","af-event","af-utils","event-outside","stencil-fx","stencil-fx-collapse"]},"stream-needtoknow-anim":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-pageviz"]},"stream-onboard-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","stencil-fx","stencil-fx-collapse"]},"td-applet-comments-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-interest-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-stream-appletmodel-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-cookie","af-event","af-utils","af-applet-model","af-beacon","af-poll","td-applet-stream-model-v2","td-applet-stream-items-model-v2"]},"td-applet-stream-atomic-templates-breaking_news":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-debug":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_action":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_desktop":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_flyout":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_share":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-errormsg":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-ad_dislike":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_dislike_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-default":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_clusters":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-featured_ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-featured_ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-featured_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-filmstrip":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-followable":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-gs_tile":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-needtoknow_actions":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-play_icon":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-right_menu":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-right_menu_featured"]},"td-applet-stream-atomic-templates-item-right_menu_featured":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-storyline":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_images":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_upsell":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-items":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-debug"]},"td-applet-stream-atomic-templates-main":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-breaking_news","td-applet-stream-atomic-templates-errormsg"]},"td-applet-stream-atomic-templates-related":{group:"td-applet-stream-atomic",requires
:["template-base","dust"]},"td-applet-stream-atomic-templates-removeditem":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-baseview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","node-scroll-info","af-beacon"]},"td-applet-stream-headerview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view"]},"td-applet-stream-items-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-mainview-v2":{group:"td-applet-stream-atomic",requires:["af-beacon","af-cookie","af-cache","af-event","af-pipe","stencil-imageloader","td-applet-stream-baseview-v2"]},"td-applet-stream-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-onboarding-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-payoff-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-related-model-atomic":{group:"td-applet-stream-atomic",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-trending-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-trending-atomic-mainview":{group:"td-applet-trending-atomic",requires:["af-applet-view","node"]},"td-applet-trending-atomic-templates-main":{group:"td-applet-trending-atomic",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-viewer",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-ads-model":{group:"td-applet-viewer",requires:["af-sync","base-build","model","af-beacon"]},"td-applet-viewer-templates-ad_fdb_thank_you":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-ads_story":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-cards":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-debug","td-applet-viewer-templates-fallback","td-applet-viewer-templates-footer"]},"td-applet-viewer-templates-carousel":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-content_body":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow","td-applet-viewer-templates-ads_story"]},"td-applet-viewer-templates-content_body_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow"]},"td-applet-viewer-templates-credit":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-debug":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-drawer_feedback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-fallback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-footer":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-header":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-share_btns_mega"]},"td-applet-viewer-templates-header_ads":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-lightbox":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel"]},"td-applet-viewer-templates-lightbox_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel","td-applet-viewer-templates-thumbnail_items"]},"td-applet-viewer-templates-mag_slideshow":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-main":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-cards","td-applet-viewer-templates-lightbox"]},"td-applet-viewer-templates-modal_aside_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-modal_lrecs_mega"]},"td-applet-viewer-templates-modal_lrecs_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-reblog":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-sidekicktv":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-slideshow":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-lightbox_mega","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story_cover":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-video","td-applet-viewer-templates-header"]},"td-applet-viewer-templates-thumbnail_items":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-video":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-content_body"]},"td-viewer-ads":{group:"td-applet-viewer",requires:["af-beacon","af-config","af-event"]},"td-viewer-mainview":{group:"td-applet-viewer",requires:["af-applet-view","af-beacon","af-cache","af-config","af-event","stencil","angus-slider","event-tap","td-viewer-ads","td-viewer-slideshow"]},"td-viewer-model":{group:"td-applet-viewer",requires:["af-cache","af-sync","base-build","model","af-beacon"]},"td-viewer-slideshow":{group:"td-applet-viewer",requires:["stencil","angus-slider","event-tap"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-weather-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-weather-atomic-appletmodel":{group:"td-applet-weather-atomic",requires:["mjata-model-base","af-applet-model","mjata-model-store"]},"td-applet-weather-atomic-headerview":{group:"td-applet-weather-atomic",requires:["af-applet-view","mjata-model-store"]},"td-applet-weather-atomic-liteview":{group:"td-applet-weather-atomic",requires:["td-applet-weather-atomic-mainview"]},"td-applet-weather-atomic-mainview":{group:"td-applet-weather-atomic",requires:["af-applet-view","af-message","stencil-fx","stencil-fx-collapse"]},"td-applet-weather-atomic-model":{group:"td-applet-weather-atomic",requires:["model","af-sync"]},"td-applet-weather-atomic-templates-header":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-list-lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.hovercard":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust","td-applet-weather-atomic-templates-list-lite"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-dev-info",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-dev-info":{group:"td-dev-info",requires:["overlay","node-core","td-dev-info-templates-perf"]},"td-dev-info-templates-init":{group:"td-dev-info",requires:["template-base","dust"]},"td-dev-info-templates-perf":{group:"td-dev-info",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-lib-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"social-sharing-lib":{group:"td-lib-social",requires:["base","node-base","event-base","event-custom","stencil-lightbox","stencil-tooltip"]},"td-lib-social-templates-mtf":{group:"td-lib-social",requires:["template-base","dust","td-lib-social-templates-mtf_styles"]},"td-lib-social-templates-mtf_styles":{group:"td-lib-social",requires:["template-base","dust"]},"td-social-email-autocomplete":{group:"td-lib-social",requires:["autocomplete","autocomplete-highlighters","io","json","lang","node-base","node-core","yql"]},"td-social-mtf-popup":{group:"td-lib-social",requires:["base","event-base","io","autosuggest-standalone-loader","autosuggest-compose-utils","td-social-email-autocomplete","node-base","gallery-node-tokeninput"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.applyConfig({"groups":{"ape-af":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-af-0.0.327/","root":"os/mit/td/ape-af-0.0.327/"}}});
YUI.applyConfig({"groups":{"ape-applet":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-applet-0.0.207/","root":"os/mit/td/ape-applet-0.0.207/"}}});
YUI.applyConfig({"groups":{"ape-location":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-location-1.0.9/","root":"os/mit/td/ape-location-1.0.9/"}}});
YUI.applyConfig({"groups":{"ape-pipe":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-pipe-0.0.64/","root":"os/mit/td/ape-pipe-0.0.64/"}}});
YUI.applyConfig({"groups":{"ape-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-social-0.0.5/","root":"os/mit/td/ape-social-0.0.5/"}}});
YUI.applyConfig({"groups":{"dust-helpers":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/dust-helpers-0.0.144/","root":"os/mit/td/dust-helpers-0.0.144/"}}});
YUI.applyConfig({"groups":{"mjata":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/mjata-0.4.35/","root":"os/mit/td/mjata-0.4.35/"}}});
YUI.applyConfig({"groups":{"stencil":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/stencil-3.1.0/","root":"os/mit/td/stencil-3.1.0/"}}});
YUI.applyConfig({"groups":{"td-dev-info":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-dev-info-0.0.30/","root":"os/mit/td/td-dev-info-0.0.30/"}}});
YUI.applyConfig({"groups":{"td-lib-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-lib-social-0.1.207/","root":"os/mit/td/td-lib-social-0.1.207/"}}});
YUI.applyConfig({"modules":{"IntlPolyfill":{"fullpath":"https:\u002F\u002Fs.yimg.com\u002Fzz\u002Fcombo?yui:platform\u002Fintl\u002F0.1.4\u002FIntl.min.js&yui:platform\u002Fintl\u002F0.1.4\u002Flocale-data\u002Fjsonp\u002F{lang}.js","condition":{"name":"IntlPolyfill","trigger":"intl-messageformat","test":function (Y) {
                        return !Y.config.global.Intl;
                    },"when":"before"},"configFn":function (mod) {
                    var lang = 'en-US';
                    if (window.YUI_config && window.YUI_config.lang && window.IntlAvailableLangs && window.IntlAvailableLangs[window.YUI_config.lang]) {
                        lang = window.YUI_config.lang;
                    }
                    mod.fullpath = mod.fullpath.replace('{lang}', lang);
                    return true;
                }}}});
                    });
                });                </script>

<script type="text/javascript">
                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                                        
                        (function(root) {
            root.YUI_config = root.YUI_config || {};
            root.YUI_config.lang = 'en-US';
        }(this));
            var YMedia = YUI({
                
                bootstrap: true,
                lang: 'en-US',
                comboBase: 'https://s.yimg.com/zz/combo?',
                comboSep: '&',
                root: 'yui:' + YUI.version + '/',
                filter: 'min',
                combine: true,
                maxURLLength: 2000,
                groups: {
                    arcade : {
                        base: 'https://s.yimg.com/nn/',
                        combine: true,
                        comboSep: '&',
                        comboBase: 'https://s.yimg.com/zz/combo?',
                        root: '',
                        modules: {
                                                'type_appscontainer_smartphone': {
                        'requires': ['node','node-event-delegate','anim','transition','event-move','stencil','stencil-scrollview'],
                        'path': '/nn/lib/metro/g/appscontainer/appscontainer_smartphone_0.0.18.js'
                    },
                    'type_customizedbutton': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/customizedbutton/customizedbutton_0.0.9.js'
                    },
                    'type_events_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/events/events_0.0.3.js'
                    },
                    'type_geminiads': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/geminiads/geminiads_0.0.4.js'
                    },
                    'type_myy': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid','af-eu-tracking'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_myy_stub_rapid': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app'],
                        'path': '/nn/lib/metro/g/myy/myy_stub_rapid_0.0.4.js'
                    },
                    'type_myy_mobile': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/myy_mobile_0.0.9.js'
                    },
                    'type_myy_viewer': {
                        'requires': ['highlander-client'],
                        'path': '/nn/lib/metro/g/myy/myy_viewer_0.0.10.js'
                    },
                    'type_advance': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/advance_0.0.4.js'
                    },
                    'type_video_manager': {
                        'requires': ['af-content','af-event','base','event-synthetic','node-core','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/video_manager_0.0.129.js'
                    },
                    'type_video_stage': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/video_stage_0.0.8.js'
                    },
                    'type_yahoodotcom_client': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/yahoodotcom_client_0.0.15.js'
                    },
                    'type_myy_scroller': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/myy_scroller_0.0.8.js'
                    },
                    'type_rapidworker_1_1': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_1_0.0.4.js'
                    },
                    'type_rapidworker_1_2': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_2_0.0.3.js'
                    },
                    'type_featurecue': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/featurecue_0.0.14.js'
                    },
                    'type_fp': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_addtomy': {
                        'requires': ['node','event','io','panel','io-base','transition','json-stringify'],
                        'path': '/nn/lib/metro/g/myy/addtomy_0.0.21.js'
                    },
                    'af-applet-basemodel': {
                        'requires': ['af-config','af-sync','af-utils','model'],
                        'path': '/nn/lib/metro/g/myy/af_applet_basemodel_0.0.3.js'
                    },
                    'af-applet-contentmodel': {
                        'requires': ['af-applet-basemodel'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentmodel_0.0.3.js'
                    },
                    'af-applet-baseview': {
                        'requires': ['af-dom','view'],
                        'path': '/nn/lib/metro/g/myy/af_applet_baseview_0.0.3.js'
                    },
                    'af-applet-contentview': {
                        'requires': ['af-applet-baseview','af-trans'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentview_0.0.3.js'
                    },
                    'af-applet-contentsettingsview': {
                        'requires': ['af-applet-contentview','af-utils','ape-applet-templates-settingswrap'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentsettingsview_0.0.3.js'
                    },
                    'af-applet-dd': {
                        'requires': ['af-applet-dom','af-message','event-custom-base'],
                        'path': '/nn/lib/metro/g/myy/af_applet_dd_0.0.4.js'
                    },
                    'type_abu': {
                        'requires': ['af-event'],
                        'path': '/nn/lib/metro/g/myy/abu_0.0.16.js'
                    },
                    'type_abu_event': {
                        'requires': ['event-synthetic','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/abu_event_0.0.2.js'
                    },
                    'type_abu_video': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_0.0.7.js'
                    },
                    'type_abu_video_manager': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','type_abu_video','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_manager_0.0.20.js'
                    },
                    'type_advance_desktop': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop_0.0.8.js'
                    },
                    'type_advance_desktop_viewer': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid','highlander-client'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop-viewer_0.0.3.js'
                    },
                    'type_app_declarations': {
                        'requires': ['af-cookie'],
                        'path': '/nn/lib/metro/g/myy/app_declarations_0.0.6.js'
                    },
                    'type_partner_att_enus_foreseealive': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-alive_0.0.3.js'
                    },
                    'type_partner_att_enus_foreseetrigger': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-trigger_0.0.3.js'
                    },
                    'pure_client_darla': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/pure_client_darla_0.0.2.js'
                    },
                    'type_idletimer': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/idletimer_0.0.1.js'
                    },
                    'type_myycontentdb': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myycontentdb/myycontentdb_0.0.54.js'
                    },
                    'type_uh_init': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myyheader/uh_init_0.0.13.js'
                    },
                    'type_myymail_mainview': {
                        'requires': ['af-applet-contentsettingsview','stencil-selectbox','stencil-toggle'],
                        'path': '/nn/lib/metro/g/myymail/myymail-mainview_0.0.23.js'
                    },
                    'type_myymail_appletmodel': {
                        'requires': ['mjata-model-base','af-applet-contentmodel'],
                        'path': '/nn/lib/metro/g/myymail/myymail-appletmodel_0.0.16.js'
                    },
                    'type_myyrss_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/myyrss/myyrss_0.0.13.js'
                    },
                    'type_nux': {
                        'requires': ['node-base','event-base','panel','io-base','json-stringify','af-transport'],
                        'path': '/nn/lib/metro/g/nux/nux_0.0.44.js'
                    },
                    'type_optin': {
                        'requires': ['node','event','io','panel','io-base'],
                        'path': '/nn/lib/metro/g/optin/optin_0.0.33.js'
                    },
                    'type_changelayout': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/pagenav/changelayout_0.0.40.js'
                    },
                    'type_changetheme': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/changetheme_0.0.50.js'
                    },
                    'type_addmodule': {
                        'requires': ['node','event','io','json-parse','json-stringify'],
                        'path': '/nn/lib/metro/g/pagenav/addmodule_0.0.16.js'
                    },
                    'type_addpage': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/addpage_0.0.27.js'
                    },
                    'type_pagenav': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/pagenav_0.0.35.js'
                    },
                    'type_reco': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/reco/reco_0.0.10.js'
                    },
                    'type_sda': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sda_0.0.37.js'
                    },
                    'type_sdarotate': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sdarotate_0.0.20.js'
                    },
                    'type_signoutcta': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/signoutcta/signoutcta_0.0.9.js'
                    },
                    'type_windowshade_js': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/windowshade/windowshade_0.0.4.js'
                    }
                        }
                    }
                }
            });

            if (!YMedia.config.patches || !YMedia.config.patches.length) {
                YMedia.config.patches = [
                    function patchLangBundlesRequires(Y, loader) {
                        var getRequires = loader.getRequires;
                        loader.getRequires = function (mod) {
                            var i, j, m, name, mods, loadDefaultBundle,
                                locales = Y.config.lang || [],
                                r = getRequires.apply(this, arguments);
                            // expanding requirements with optional requires
                            if (mod.langBundles && !mod.langBundlesExpanded) {
                                mod.langBundlesExpanded = [];
                                locales = typeof locales === 'string' ? [locales] : locales.concat();
                                for (i = 0; i < mod.langBundles.length; i += 1) {
                                    mods = [];
                                    loadDefaultBundle = false;
                                    name = mod.group + '-lang-' + mod.langBundles[i];
                                    for (j = 0; j < locales.length; j += 1) {
                                        m = this.getModule(name + '_' + locales[j].toLowerCase());
                                        if (m) {
                                            mods.push(m);
                                        } else {
                                            // if one of the requested locales is missing,
                                            // the default lang should be fetched
                                            loadDefaultBundle = true;
                                        }
                                    }
                                    if (!mods.length || loadDefaultBundle) {
                                        // falling back to the default lang bundle when needed
                                        m = this.getModule(name);
                                        if (m) {
                                            mods.push(m);
                                        }
                                    }
                                    // adding requirements for each lang bundle
                                    // (duplications are not a problem since they will be deduped)
                                    for (j = 0; j < mods.length; j += 1) {
                                        mod.langBundlesExpanded = mod.langBundlesExpanded.concat(this.getRequires(mods[j]), [mods[j].name]);
                                    }
                                }
                            }
                            return mod.langBundlesExpanded && mod.langBundlesExpanded.length ?
                                    [].concat(mod.langBundlesExpanded, r) : r;
                        };
                    }
            ];
        }
        for (var i = 0; i < YMedia.config.patches.length; i += 1) {YMedia.config.patches[i](YMedia, YMedia.Env._loader);}

        
                        YUI().use('node-base', function(Y) {
                    Y.Global.fire('ymediaReady', {e: YMedia});
                });

                    });
                });        YUI().use('node-base', function(Y) {
        Y.Global.on('ymediaReady', function(data) {
            YMedia = data.e;
    YMedia.applyConfig({"groups":{"td-applet-mega-header":{"base":"/sy/os/mit/td/td-applet-mega-header-1.0.204/","root":"os/mit/td/td-applet-mega-header-1.0.204/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_30345894"] = {"applet_type":"td-applet-mega-header","views":{"main":{"yui_module":"td-applet-mega-header-mainview","yui_class":"TD.Applet.MegaHeaderMainView","config":{"alphatar":{"enabled":true,"urlPath":"https://s.yimg.com/rz/uh/alphatars/","imgType":".png"},"avatar":{"serverCall":false,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"format":"json","_maxage":900}}},"useAvatar":true,"bucket":"201","followable":{"enabled":true,"uri":"/_td_api"},"hasMailPreview":true,"hasMail":true,"hasNotifications":true,"hasProfile":true,"iSearch":{"isEnabled":false,"instantTrending":false,"frcode":"yfp-t-201","frcodeTrending":"fp-tts-201","spaceid":97162737,"cacheMaxAge":30000,"pageViewDelay":1000,"comscoreDelay":3000,"timeout":2500,"highConfidence":true,"autocomplete":{"additionalParams":{"appid":"is","nresults":4},"max_results":4},"api":{"protocol":"https://","host":"search.yahoo.com","path":"/search?","tmpl":"ISRCHA:A0124"},"filterUrls":{"web":"https://search.yahoo.com/search?p=","images":"https://images.search.yahoo.com/search/images?p=","video":"https://video.search.yahoo.com/search/video?p=","news":"https://news.search.yahoo.com/search?p=","local":"https://search.yahoo.com/local/s?p=","answers":"https://answers.search.yahoo.com/search/search_result?p=","shopping":"https://shopping.search.yahoo.com/search?p="}},"isFallback":false,"loginUrl":"https://login.yahoo.com/config/login?.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","logoutUrl":"https://login.yahoo.com/config/login?logout=1&.direct=2&.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","mail":{"compose":{"url":"https://mrd.mail.yahoo.com/compose"},"count":{"api":{"protocol":"https","host":"mg.mail.yahoo.com","path":"/mailservices/v1/newmailcount","query":{"appid":"UnivHeader"}},"refreshInterval":120,"maxCountDisplay":99},"preview":{"prefetch":true,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"q":"select messageInfo.receivedDate, messageInfo.mid, messageInfo.flags.isRead, messageInfo.from.name, messageInfo.subject from ymail.messages where numMid=\"3\" limit 6","format":"json"}},"urls":{"message":"https://mrd.mail.yahoo.com/msg?fid=Inbox&src=hp&mid="}},"url":"https://mail.yahoo.com/"},"miniheader":true,"miniheaderYPos":0,"notifications":{"inlineReader":true,"maxDisplay":0,"maxUpsellsDisplay":10,"count":{"api":{"uri":"/_td_api","requestType":"clustersUpdatesCount"},"refreshInterval":120,"maxCountDisplay":99},"list":{"api":{"uri":"/_td_api","requestType":"clustersList","image_tag":"pc:size=square,pc:size=small_fixed"}}},"search":{"action":"https://search.yahoo.com/search","assistYlc":";_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-","ylc":";_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-","queries":[{"name":"fr","value":"yfp-t-201"},{"name":"fp","value":"1"},{"name":"toggle","value":"1"},{"name":"cop","value":"mss"},{"name":"ei","value":"UTF-8"}],"autocomplete":{"ghostEnabled":false,"host":"https://search.yahoo.com/sugg/gossip/gossip-us-ura/","crumbKey":"gossip","theme":{"highlight":{"color":"black","background":"#c6d7ff"}},"plugin":{"minQueryLength":3,"resultHighlighter":"phraseMatch"}}},"searchFormGlow":true,"searchMiniHeader":false,"accountSwitchData":{"enabled":""}}}},"templates":{"main":{"yui_module":"td-applet-mega-header-templates-main","template_name":"td-applet-mega-header-templates-main"},"mailpreview":{"yui_module":"td-applet-mega-header-templates-mailpreview","template_name":"td-applet-mega-header-templates-mailpreview"},"accountSwitchPanel":{"yui_module":"td-applet-mega-header-templates-accountSwitchPanel","template_name":"td-applet-mega-header-templates-accountSwitchPanel"},"notificationpanel":{"yui_module":"td-applet-mega-header-templates-notificationpanel","template_name":"td-applet-mega-header-templates-notificationpanel"},"topbar":{"yui_module":"td-applet-mega-header-templates-topbar","template_name":"td-applet-mega-header-templates-topbar"}},"i18n":{"ACCOUNT_INFO":"Account Info","ACCOUNT_SWITCH_WELCOME_1":"Easily switch between multiple Yahoo accounts using the new","ACCOUNT_SWITCH_WELCOME_2":"Click \"Add account\" below to get started!","ACCOUNT_SWITCH_COOKIE_ERR":"It looks like you switched accounts. Refresh the browser to view your personalized page.","ACCOUNT_SWITCH_CRUMB_ERR":"Your account data may be out of sync.<br>Refresh the page to see your accounts.","ACCOUNT_SWITCH_ACCOUNT_MANAGER":"Account Manager","ADD_ACCOUNT":"Add account","ADD_MANAGE_ACCOUNT":"Add or manage accounts","ANSWERS":"Answers","CLEAR_SEARCH":"Clear search query","COMPOSE":"Compose","COMPOSE_CAPS":"COMPOSE","CORPMAIL":"Corp Mail","DEVELOPING_NOW":"Developing Now","ENTER_KEY":"Enter","FIREFOX_PROMO_TEXT":"Install the new Firefox","FOLLOW":"Follow","FOLLOWING":"Following","FROM":"From","GO_TO_MAIL":"Go To Mail","GO_TO_MAIL_CAPS":"GO TO MAIL","HAVE_NO_NEW_MESSAGES":"You have no new messages.","HAVE_NO_UPDATES":"Check back later for updates on stories you are following.","HOME":"Home","IMAGES":"Images","LOADING_MAIL":"Loading Mail Preview","LOADING_UPDATES":"Loading Updates","LOCAL":"Local","MAIL":"Mail","MAIL_CAPS":"MAIL","MENU":"Menu","NEW":"New","NEWS":"News","NEW_MESSAGE":"new message.","NEW_MESSAGES":"new messages.","NOT_FOLLOWING":"Follow a story and get updates here.","NOTIFICATIONS":"Notifications","PRESS":"Press ","PROFILE":"Profile","REL_DAYS":"{0}d","REL_DAYS_AGO":"{0} days ago","REL_HOURS":"{0}h","REL_HOURS_AGO":"{0} hours ago","REL_MINS":"{0}m","REL_MINS_AGO":"{0} minutes ago","REL_MONTHS":"{0}mo","REL_MONTHS_AGO":"{0} months ago","REL_SECS":"{0}s","REL_SECS_AGO":"{0} seconds ago","REL_WEEKS":"{0}wk","REL_WEEKS_AGO":"{0} weeks ago","REL_YEARS":"{0}yr","REL_YEARS_AGO":"{0} years ago","SEARCH":"Search","SEARCH_WEB":"Search Web","SETTINGS":"Settings","SETTINGS_CAPS":"SETTINGS","SHOPPING":"Shopping","SIGNIN":"Sign in","SIGNIN_CAPS":"SIGN IN","SIGNOUT":"Sign Out","SIGNOUT_ALL":"Sign out all","SIGNOUT_CAPS":"SIGN OUT","START_TYPING":"Start typing...","SUBJECT":"Subject","TO_SAVE_FOLLOWS":" to save and get updates.","TO_SEARCH":" to search.","TO_VIEW_NOTIF":" to view your notifications","TO_VIEW_MAIL":" to view your mail","UNABLE_TO_PREVIEW_MAIL":"We are unable to preview your mail.","UNABLE_TO_FETCH_UPDATES":"Check back later for updates on stories you are following.","UNFOLLOW":"Unfollow","VIDEO":"Video","WEB":"Web","WELCOME_BACK":"Welcome back","YOU_HAVE":"You have","SKIP_ASIDE":"Skip to Related content","SKIP_MAIN":"Skip to Main content","SKIP_NAV":"Skip to Navigation"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-viewer":{"base":"/sy/os/mit/td/td-applet-viewer-0.1.2153/","root":"os/mit/td/td-applet-viewer-0.1.2153/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000101"] = {"applet_type":"td-applet-viewer","models":{"viewer":{"yui_module":"td-viewer-model","yui_class":"TD.Viewer.Model","config":{"dataFetch":{"NUM_PREFETCH_ITEMS":20,"NUM_RENDER_ITEMS":1,"NUM_BATCH_PHOTOS":25,"STALE_DATA_FETCH":1800},"cache":{"max-age":{"SLIDESHOW":300,"STORY":600,"VIDEO":3600},"stale-while-revalidate":{"SLIDESHOW":43200,"STORY":43200,"VIDEO":43200}},"uiConfig":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false},"config":{"ui.enableMyComments":true}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"embeddedIframes":{"iframeEmbedHost":"https://s.yimg.com","iframeEmbedMaxPolls":5,"iframeEmbedUrl":"https://s.yimg.com/os/yc/html/embed-iframe-min.7b3468ff.html","instagram":{"className":"instagram-media","name":"instagram","scriptSrc":"//platform.instagram.com/en_US/embeds.js"},"twitter":{"className":"twitter-tweet","name":"tweet","scriptSrc":"//platform.twitter.com/widgets.js"}},"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableMobileLREC":false,"enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalExtraLargeAds":true,"enableModalLRECBackfill":true,"enableModalOmnitureBeacon":true,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableModalVideoDocking":false,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","mobileAdPos":"LREC-9,LREC2-9","mobileAdPosNonHosted":"LREC-9","mobileAdPosOne":"hl-ad-LREC-9","mobileAdPosTwo":"hl-ad-LREC2-9","mobileAdPositionThreshold":250,"modalExtLargeAdPosName":"LDRB2","modalExtLargeAdPosNameAddIndex":false,"modalExtLargeAdPosMaxNum":3,"nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"videoEnrichment":true,"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"enableMyComments":1,"promo":"{\"sports\":{\"image\":\"https://s.yimg.com/dh/ap/sports/mlb/fantasy_baseball_promo_640x70.jpg\",\"title\":\"Sign up for Yahoo Fantasy Baseball\",\"url\":\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16497578&PluID=0&\"}}","lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"},"enableCategories":true,"enableEntities":true,"useUuidPrefix":true,"optimisticPrefetch":false}},"ads":{"yui_module":"td-ads-model","yui_class":"TD.Ads.Model","config":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"}},"applet_model":{"models":["viewer","ads"]}},"views":{"main":{"yui_module":"td-viewer-mainview","yui_class":"TD.Viewer.MainView","config":{"ads":{"enableAdsFeedback":0,"enableVideoSpaceid":1,"curveball":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"},"displayAds":{"config":{"LDRP-9":{"w":"768","h":"1"},"LDRB-9":{"w":"728","h":"90","supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LREC-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"LREC2-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"WPS-9":{"w":"320","h":"50"},"WP-9":{"w":"320","h":"50"},"sa":"LREC='300x250;1x1' LREC2='300x250;1x1' LREC3='300x250;1x1' MON='300x600;1x1' megamodal=true","LREC3-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"LDRB2-9":{"supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LDRB3-9":{"supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LDRB4-9":{"supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LDRB5-9":{"supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"MAST-9":{"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"fdb":false,"closeBtn":{"adc":0,"mode":2,"useShow":1},"metaSize":true,"z":11},"MON-9":{"w":"300","h":"600","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"SPL-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"SPL2-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"FSRVY-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1,"bg":1,"lyr":1},"z":11},"FOOT9-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11},"FOOT-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11}},"enabled":1,"enabledAuto":false,"enabledDeferRenderAds":false,"forced_modalspaceid":"","enabledMultiAds":false,"positions":{"aboveFold":["LREC-9"],"belowFold":[],"custom":["MAST-9","LDRB-9","SPL-9","SPL2-9"]},"modalposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FSRVY-9,FOOT9-9,MON-9","lrecTimeout":"","enableK2BeaconConf":true,"enableBucketSiteAttribute":true,"offnetSpaceid":"1197762606","offnetposition":"LREC-9","magazineposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FOOT-9,MON-9"},"YVAP":{"accountId":"904","playContext":"default","spaceId":null},"displayAdsFP":{"enabled":0,"pos":"FPAD,LREC","config":[{"id":"FPAD","dest":"my-adsFPAD","pos":"FPAD","clean":"my-adsFPAD-base","h":"250","w":"300"},{"id":"LREC","dest":"my-adsLREC","pos":"LREC","clean":"my-adsLREC-base","h":"250","w":"300"}]},"modalYVAP":{"news":"145","finance":"193","sports":"82","gma":"145","default":"904"},"modalBackfillYVAP":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalPlayContext":{"news":"default","finance":"default","sports":"default","gma":"gma","autos":"bmprpreover","beauty":"bmprpreover","celebrity":"bmprpreover","food":"bmprpreover","health":"bmprpreover","makers":"bmprpreover","movies":"bmprpreover","music":"bmprpreover","parenting":"bmprpreover","politics":"bmprpreover","style":"bmprpreover","tech":"bmprpreover","travel":"bmprpreover","tv":"bmprpreover"},"modalBackfillExpName":"sidekickTVlrecbackfillHTML5","modalBackfillExpType":"right-rail-autoplay","modalBackfillAdTitlePrefix":"UP NEXT: ","modalBackfillYVAPHTML5":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalBackfillExpNameHTML5":"sidekickTVlrecbackfillHTML5"},"category":"","hlView":true,"js":{"videoplayer":"https://yep.video.yahoo.com/js/3/videoplayer-min.js?r=&ypv=","omniture":"https://s.yimg.com/os/mit/media/m/news/omniture-mega-min-78f5e30.js","embed":["click-capture-min","utils"]},"redirectNoContent":true,"rendered":false,"search":{"action":"https://search.yahoo.com/search","frcode":"yfp-t-201-m"},"spaceid":0,"ui":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false},"config":{"ui.enableMyComments":true}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"embeddedIframes":{"iframeEmbedHost":"https://s.yimg.com","iframeEmbedMaxPolls":5,"iframeEmbedUrl":"https://s.yimg.com/os/yc/html/embed-iframe-min.7b3468ff.html","instagram":{"className":"instagram-media","name":"instagram","scriptSrc":"//platform.instagram.com/en_US/embeds.js"},"twitter":{"className":"twitter-tweet","name":"tweet","scriptSrc":"//platform.twitter.com/widgets.js"}},"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableMobileLREC":false,"enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalExtraLargeAds":true,"enableModalLRECBackfill":true,"enableModalOmnitureBeacon":true,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableModalVideoDocking":false,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","mobileAdPos":"LREC-9,LREC2-9","mobileAdPosNonHosted":"LREC-9","mobileAdPosOne":"hl-ad-LREC-9","mobileAdPosTwo":"hl-ad-LREC2-9","mobileAdPositionThreshold":250,"modalExtLargeAdPosName":"LDRB2","modalExtLargeAdPosNameAddIndex":false,"modalExtLargeAdPosMaxNum":3,"nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"videoEnrichment":true,"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"enableMyComments":1,"promo":"{\"sports\":{\"image\":\"https://s.yimg.com/dh/ap/sports/mlb/fantasy_baseball_promo_640x70.jpg\",\"title\":\"Sign up for Yahoo Fantasy Baseball\",\"url\":\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16497578&PluID=0&\"}}","lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"},"clusterEnabled":false}}},"templates":{"main":{"yui_module":"td-applet-viewer-templates-main","template_name":"td-applet-viewer-templates-main"},"cards":{"yui_module":"td-applet-viewer-templates-cards","template_name":"td-applet-viewer-templates-cards"},"carousel":{"yui_module":"td-applet-viewer-templates-carousel","template_name":"td-applet-viewer-templates-carousel"},"story":{"yui_module":"td-applet-viewer-templates-story","template_name":"td-applet-viewer-templates-story"},"reblog":{"yui_module":"td-applet-viewer-templates-reblog","template_name":"td-applet-viewer-templates-reblog"},"video":{"yui_module":"td-applet-viewer-templates-video","template_name":"td-applet-viewer-templates-video"},"slideshow":{"yui_module":"td-applet-viewer-templates-slideshow","template_name":"td-applet-viewer-templates-slideshow"},"header":{"yui_module":"td-applet-viewer-templates-header","template_name":"td-applet-viewer-templates-header"},"content_body":{"yui_module":"td-applet-viewer-templates-content_body","template_name":"td-applet-viewer-templates-content_body"},"content_body_mega":{"yui_module":"td-applet-viewer-templates-content_body_mega","template_name":"td-applet-viewer-templates-content_body_mega"},"story_cover":{"yui_module":"td-applet-viewer-templates-story_cover","template_name":"td-applet-viewer-templates-story_cover"},"lightbox":{"yui_module":"td-applet-viewer-templates-lightbox","template_name":"td-applet-viewer-templates-lightbox"},"lightbox_mega":{"yui_module":"td-applet-viewer-templates-lightbox_mega","template_name":"td-applet-viewer-templates-lightbox_mega"},"fallback":{"yui_module":"td-applet-viewer-templates-fallback","template_name":"td-applet-viewer-templates-fallback"},"ads_story":{"yui_module":"td-applet-viewer-templates-ads_story","template_name":"td-applet-viewer-templates-ads_story"},"header_ads":{"yui_module":"td-applet-viewer-templates-header_ads","template_name":"td-applet-viewer-templates-header_ads"},"footer":{"yui_module":"td-applet-viewer-templates-footer","template_name":"td-applet-viewer-templates-footer"},"share_btns":{"yui_module":"td-applet-viewer-templates-share_btns","template_name":"td-applet-viewer-templates-share_btns"},"share_btns_mega":{"yui_module":"td-applet-viewer-templates-share_btns_mega","template_name":"td-applet-viewer-templates-share_btns_mega"},"mag_slideshow":{"yui_module":"td-applet-viewer-templates-mag_slideshow","template_name":"td-applet-viewer-templates-mag_slideshow"},"drawer":{"yui_module":"td-applet-viewer-templates-drawer_feedback","template_name":"td-applet-viewer-templates-drawer_feedback"},"thank_you":{"yui_module":"td-applet-viewer-templates-ad_fdb_thank_you","template_name":"td-applet-viewer-templates-ad_fdb_thank_you"},"sidekicktv":{"yui_module":"td-applet-viewer-templates-sidekicktv","template_name":"td-applet-viewer-templates-sidekicktv"}},"i18n":{"PREVIOUS":"Previous","Next":"Next","MORE":"...","LESS":"less","NOCONTENT_FUNNY":"Feeding the engineers.  Will be back soon...","NOCONTENT_READMORE":"Read more on \"{0}\"","OFFNETWORK":"Read more on {0}","ELLIPSIS":"{0} ...","VIDEO_DURATION":"Duration {0}","SPONSORED":"Sponsored","SWIPE_FOR_NEXT":"Swipe for next article","INSTALL_NOW":"Install now","FULL_ARTICLE":"Read Full Article","ARTICLE_SUMMARY":"Read Summary","READ_MORE":"Read More","AD":"Advertisement","SHARE_EMAIL":"Share to Mail","CLOSE":"Close","RELATED_NEWS":"Related news","START_SLIDESHOW":"Start Slideshow","CONTINUE_READING":"Continue Reading","CLICK_FOR_VIDEOS":"Click here to watch the video","CLICK_FOR_PHOTOS":"Click here to view photos","AD_FDB1":"It's offensive to me","AD_FDB2":"I keep seeing this","AD_FDB3":"It's not relevant to me","AD_FDB4":"Something else","AD_FDB_HEADING":"Why don't you like this ad ?","AD_REVIEW":"We'll review and make changes needed.","AD_THANKYOU":"Thank you for your feedback","AD_SUBMIT":"Submit","AD_FDB_ERORR":"Please select option. To help us making your experience better.","AD_FDB_SOMETHING_ELSE":"Tell us, why you don't like this ad?","DONE":"Done","READ_ARTICLE":"Read more","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","REBLOG":"Reblog","REBLOG_TUMBLR":"Reblog on Tumblr","UNDO":"Undo","SIGN_IN_TO_LIKE":"Sign in to like","LIKABLE_ARTICLE_SIGN_IN":"Sign in to see more stories you like.","SIGN_IN_2":"Sign in","READ_FULL_ARTICLE":"Read full article","COMMENTS":"Comments","FACEBOOK":"Share","TWITTER":"Tweet","EMAIL":"Email","CLOSE_VIEWER":"Close this content, you can also use the Escape key at anytime"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};window.ViewerClickCapture||(window.ViewerClickCapture=function(){function a(b,c,d){return b?b.tagName===c&&b.className&&b.className.indexOf(d)>=0?b:a(b.parentNode,c,d):!1}function b(b){var c=a(b.target,"A","js-content-viewer");c&&(d=b,b.preventDefault&&b.preventDefault())}function c(){!e&&f.removeEventListener&&f.removeEventListener("click",b),e=!0}var d,e,f=document.documentElement;return f.addEventListener&&(f.addEventListener("click",b),window.setTimeout(function(){c()},4e3)),{clear:function(){d=null},last:function(){return d},disable:c}}());window.ViewerUtils||!function(){function a(){function a(a){return a&&"object"==typeof a&&"number"==typeof a.length&&g.call(a)===h||!1}function b(a){return"string"==typeof a||a&&"object"==typeof a&&g.call(a)===i||!1}function c(a){return"undefined"==typeof a}function d(a){return null===a}function e(a){return c(a)||d(a)}var f=Object.prototype,g=f.toString,h="[object Array]",i="[object String]";return{isArray:Array.isArray||a,isNull:d,isString:b,isUndefined:c,isVoid:e}}function b(b,c,d){var e=a();if(!b)return d;if(!c)return b;!e.isArray(c)&&e.isString(c)&&(c=c.split("."));for(var f=0,g=c.length;b&&g>f;f++)b=b[c[f]];return e.isVoid(b)?d:b}window.ViewerUtils={getObjValue:b}}();
YMedia.applyConfig({"groups":{"td-applet-navlinks-atomic":{"base":"https://s.yimg.com/os/mit/td/td-applet-navlinks-atomic-0.0.57/","root":"os/mit/td/td-applet-navlinks-atomic-0.0.57/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000195"] = {"applet_type":"td-applet-navlinks-atomic","views":{"main":{"yui_module":"td-applet-navlinks-mainview","yui_class":"TD.Applet.NavlinksMainView"}},"i18n":{"TITLE":"navlinks-atomic","TOPICS":"Topics"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-stream-atomic":{"base":"/sy/os/mit/td/td-applet-stream-atomic-2.0.484/","root":"os/mit/td/td-applet-stream-atomic-2.0.484/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000173"] = {"applet_type":"td-applet-stream-atomic","models":{"stream":{"yui_module":"td-applet-stream-model-v2","yui_class":"TD.Applet.StreamModel2","data":{"ccode":"mega_global_ranking_hlv2_up_based","more_items":[{"type":"ad","id":"31834384097","title":"Comment assurer l'inassurable ?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=INA.4KAGIS8_YYTJro6T4aiDQsDMcv7ru_bWpLvOpD8bh20gVfBPNjLlgNUdxtGn9QQASv_B0gced4ze61DrLBQ73y4Io72GlBBE_OZW6n1tkUY39KDphlA3mGuuvEhdG37cWXiGuLGTL68.B8vCyHGyHlMzGse3bYZbhFB6M1LUoqH3SThjWe78Bp6O_yb0a5IW3phs0i1BiuLeWYANvjxmST.w5q4rPTgJPqdSTAZmN7EXE4ZHjKXx.ZMFX9uqNWkMzOB6TvE7sGesiglWaZsiB3y4d0bJyO74FJghmebyA9Wk5V8MSwFY1Q1_eu6BeNIuJagMQUYsW0i.9z0rDkbWyOB5NTQJqE4O6sPNIDYsFD19aFCN.3rvoDf0djUXd8m0iISTrzAAwFjVZFHZZcBYKfb8O8TIFvFdE.7zYUdjgfIk5fpmVo10N0nUI3Zkwj0G9jEDSAJTrh6E2OyasRCqqze5hpuYsfnbH43HRBTbjwn8dJfdek9DU_BX4wp7QcXMeYaH7A.Ih1N4mR_U2QBAFmzxD9uNJpvYYA--%26lp=https%3A%2F%2Faxagmdfrfr.solution.weborama.fr%2Ffcgi-bin%2Fdispatch.fcgi%3Fa.A%3Dcl%26a.si%3D2927%26a.te%3D95%26a.ra%3D%5BRANDOM%5D%26g.lu%3D","publisher":"Axa","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=2_kmTvkGIS_Hm0pvFOwFjTbpRlx6TP6F3mDs3RLTpHrQsUu40jhgVYPk3nU22EQx3pxTUVGnlmsy3K8a.j90zcVgNQzHh81GlYVMxbaGE6oJucHHoAMZJCe2mTsjXI1slnxQI4z.NEjtd301X65jrZYwuiJPFE2hctVzRncJd8gRE7mBIf2Nj2OTcYHco26byNIwLlb12ZzWkfsnM40SyaXiQz.7Bz8c5ej.Rz2b18qcnWu_t_JXnzkNyKfVrWuozRHoYhZmL3xeGLRmZZAmKcDj..6KDpWf4f9QXWt6NgABKnKXRDRCSMRbW1m3EFJVyd9WSNqi2l3.78g6F73fWpjrLU7dURF1fX7TePp1NQvyYMzr8G.3AfORqZ.7AETJAIgHsTYXa.utdyhCrBLGtJ58t597er1ipg--&ap=13","imprTrackingUrls":["https://axagmdfrfr.solution.weborama.fr/fcgi-bin/dispatch.fcgi?a.A=im&a.si=2927&a.te=95&a.he=1&a.wi=1&a.hr=p&a.ra=[RANDOM]"],"ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15n54ls1g(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31834384097,dmn$c31834384097,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1355646,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/_j99Ewg4ky4gyLwENQMb0A--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1458725652073-9347.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Il est possible d'assurer toutes sortes de choses, des papilles gustatives aux fusÃ©es ! Comment innover pour toujours mieux protÃ©ger ?","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31834384097","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":0},{"type":"unknown","id":"54a57d96-1e27-3f6a-95c3-cbcf3aa15885","instrument":"4000015430S00002","is_eligible":"true"},{"type":"unknown","id":"f41a5c18-1af2-37a0-b792-ac363d595d86","instrument":"4000019820S00008","is_eligible":"false"},{"type":"unknown","id":"cbff8bca-58c2-3e0c-94ab-d5fc0e671ba2","instrument":"4000020880S00008","is_eligible":"false"},{"type":"unknown","id":"04ee1523-953b-3c74-825e-e41f233511f9","instrument":"4000009470S00001","is_eligible":"true"},{"type":"ad","id":"31703404649","title":"Les joueurs du monde entier ont attendu ce jeu!","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=o0BOtdMGIS93vAZ7dOrgitI9KgEXZZOpbhG16FIymqEjXFVjcbumYmaPk_77J6oZCKLmYbYrpFlg0_eQbnUknS.b0WkJWc5OZWVfiPQkIqk0MVmTPYph5_M5hZjIt44.Y0T12GpqBH7t6J6o242UPfTyvh8Idi3rDIaq5FUUtPZISVCEfE_n3mEsRR2N4eYzg5amR7FI1RlgPvXActV46u1JbUJtA7WG_L0P.I5YSoVvpXkZooFNfHPjlXK5MSPTapfoRMtYFPGVteU8tcSntO7hdNYby8pFmWesv2osCuh9B7ic9FjeeDC1c8bC_lMuvbHOScCJfqen0LQ4qmiM_N_sSXP2Gk5Zl3aaU4ZwEIN.SJmXwQsdmclegbd1sYSa3_YY_zK9a7.mQxlRXVbGHr37rKuAOjMbp_bfuz4azCEtOBHN7TnRRZL84LdIS3eswSFUpFohRjlBbYoYwwS7D5RrSRHCCEJoy39VTN0niAkdVSHD6JJi3ml6ifNAieq0ywKVirk4NTFej8pe8knQXLDYzeqaTUrkCW.7tAsqKLZaj.x6BK8KZ_V1ETFrVoGf.bzo4hP5vdnvzpsqZeKUxbnFyA4RSAENda_1T6JfbFkxyVIWbPDy9fJAjqBMlzyQPMgUiIM-%26lp=http%3A%2F%2Fwww.mnbasd77.com%2Faff_c%3Foffer_id%3D1384%26aff_id%3D1065%26url_id%3D4561%26source%3Dysa%26aff_sub%3DFR%26aff_sub2%3Dn244%26aff_sub3%3D31703404649%26aff_sub4%3DBoundary%26aff_sub5%3DB","publisher":"Plarium","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=6eu4iBYGIS_jOvziwlgqWD.x8sBxrc3EmFlkxDW3QBYAoItGEOSCM0uZkdbQOSLlIJ2bcT1zt4vtlabe6kQx8NEDUrVv47fVuciMv.ucuLSJbogeY_cVn1XZgKW7Fw5v46NCanB1CB.08cHbp46YZXVr_quo2RWnTqN6fPS7x9mOQK2ij.OoTClf82AWwoUAh8dwDQebx2jOKS4iY95SrabnJTDIDAk8UDRBasVD5W6ToPFaJlLb518eSUHzPpUOcJb.SzTwm7YmFBNIlqkKN3aHArbBgGLRPoSn59OpB4VH5IhGNXuISf8qorxoH5vZo7.EIOa_i9..fPFgzqdvIwwNSTFqgelc4KnHGnY5LdlMGAFkb5Le.atXP6pMxA27OxLI9g--&ap=18","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15jn63hq3(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31703404649,dmn$plarium.com,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$5584,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/DgYfTZXzge.YZ1nidgd0kA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454677871802-8151.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Pourquoi il te faut jouer Ã  ce jeu MMO de stratÃ©gie gratuit et addictif? Le MMORPG le plus excitant auquel tu as jamais jouÃ© ! Ne passe pas Ã  cÃ´tÃ©!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":2,"cposy":2,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31703404649","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":1},{"type":"unknown","id":"323cb4e3-0ef4-3ab1-a318-77517096f12d","instrument":"4000010110S00001","is_eligible":"true"},{"type":"unknown","id":"90697814-c5d7-3765-bd75-060cff09304f","instrument":"4000003720S00004","is_eligible":"true"},{"type":"unknown","id":"def25dd2-415f-322f-99df-0d58bc5d9244","instrument":"4000015490S00001","is_eligible":"true"},{"type":"unknown","id":"fde58dc5-8b80-352d-be12-6449c39a6ba6","instrument":"4000023160S00008","is_eligible":"false"},{"type":"ad","id":"31770753203","title":"Nouvelle Renault Megane","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=q7GgHUwGIS8sskdI4SPZffYE2Qt4r1abggRRf8_ybRHdEhXp47DRRjnVkHPEPtLNxRVTCCdF8QZe7VlgI5bolsHDrwj9mpfRTHFjiwRGRDdSkFaNX_ZZTsX2tpDMqj0UjM.Bh_9auMao3lSmaRkNKlEPNDz7po4T3m5cSLTsEZaju0xxn_PJ1S95O2daKio12H8CvGlodnjoBIfNEJG_36IzoNoHv.kGSd4Mz2_7u_Fe_NX4lGf7NUPEel5Fbuj2LcSJ3R5AqJ3NlJbxJrHJJsYQVl0xYRzWbRv67.A3.j2VJOK1bBeaKQkAJJ1PA4EgRX6MgbhKEKMhUowurBIOoYCh6iFEnELtoDO3HoHYe4oBuqL0LeNdnJqwp3jnopWtSN8bRW7EXIXf_UpY2n4tjCugGdPjV540dsbBphkGrKlbeEleR9cEVaCafBwtgpvRpLKvsJ5A.LoQivWJ8IsHu0QauM9mWEIAKlM3BSKgMBNCEhuFuUJBz5w-%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F302647485%3B129744907%3Br","publisher":"Renault","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=rm_xzZIGIS9qsTo_O59ri5C93giz89f701eyZZBy7_a8xdi9ovfTyTggtoPPuH9_UtGBTXdxBY2iCCEdP57HKyyGs4MOOp_16VncuAkLvjyjiktsfrwKTAurkEx0T2BpXRXxzW4iFfcGq.dTeW106rkkR2iXo9.PFsxkePLCbDJfHbIl03omnYQvA_srqion7017zYZByUhedkC5tw8qimv8jX4wwgTfFSqmzvsemYSPF5Utx2BL2y080npCN_J8JOX3aG21pSXTxDhm.LluJGPF3oUQzCMIrWq7jIx4_ih8Sm_AlckM51E1RqtaQ163AdBu7BOFdTRheBbJhigtWusuC9fI2kC3DjJYkNbHUUByAUMPT42p_YnQnfrf_SJco4zN9TauM.M-&ap=23","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15n5onot4(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31770753203,dmn$c31770753203,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1116468,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/wEkxFWLjTHT6LdG_yPTdWA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1457951182152-2637.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ©couvrez Nouvelle Renault Megane, Ã  partir de 229â¬/mois, sans apport, sans condition de reprise, et avec 4 ans dâentretien et de garantie.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":3,"cposy":3,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31770753203","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":2},{"type":"unknown","id":"fad6edbf-7b21-3b6f-9622-08198d88dac3","instrument":"4000019310S00001","is_eligible":"true"},{"type":"unknown","id":"e1a20db3-5bfb-3c07-8864-513edb8c26c8","instrument":"4000022340S00008","is_eligible":"false"},{"type":"unknown","id":"f9268284-32a7-3be0-83f8-cc6612107482","instrument":"4000023160S00008","is_eligible":"false"},{"type":"unknown","id":"684a89d7-1d27-3936-80b1-3abb3a933593","instrument":"4000019520S00001","is_eligible":"true"},{"type":"ad","id":"31760810223","title":"Investissez dans le neuf","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=TH_Zv8UGIS.G9_ZkqefgBBzTuSnwcn6lbCveS0fgfFDH.dcJxy3w4tL97T9mrb5tXJDBm3NeIRoOuWRIrDbqQiKiBZhd.lp1St7v.kvsgfHsvT.Q.v966oMWnwocEBruWkYPROfzRu60zXfyMhLmQ5.kx7kTIiuFbiAmf9fxoXEnWPQGmvNwUf3Dicz6CPjunUNR7E0_phdqxfYo9uwLyqQWSNydK9TH8gS3_havQ3V27xnDNtSBSqvzbkBEwbxC1etTpYquFCdeLLEfA6GAXD9NGs6KbPtZdUXbULf5C3PJlBF5iw3NSbwQqK9iQG2iZ3vjGmYk1QKUZOVV5rTStmv6qWYiNp4COF3dqo8X.UyErk5cmLnHYAz3tWE5DmkDdMjINLzqK.a.1XnZRsoKVIUisVeWJoQowcwWI76_go88pnREoVfBuC0K.RvaIgsp0JDkiXVTUq3Z.zjAHRTWHI8yEMAtBZNv_1mZdp.anWRBTItYAB7XrNybNXcPHYpu%26lp=http%3A%2F%2Fwww4.smartadserver.com%2Fcall%2Fcliccommand%2F15488205%2F%5Btimestamp%5D%3F","publisher":"Nexity","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=UZNLDhUGIS_jnbilaEPz4AQWnh4W4L8TTKjcZDk1Uiot9TdX2__FIarqSo1PPR0HUH.F4svvsnz0_N7jO3aT0Rip9zZzIAw4aXYSkOVwQYOQuuSAp3e4ANRHyRr18PO0cBET1XTqN8AxQJ2aoY7Cue0Z6E8pd1yu6dpLASyv8gMYz4iFau72yub7oqfiGXXHIxa.a4oO5wtghRbr.7qf.ZhFrk3e69gnNSbtIB638o8kyVe16VKx2D7JyEKk._x5ldEDHIQX8dsc5rqtMd.nVkVXNaRDQGu0u7WdQCSnsCS0pSWKkkRX45vWCn36ABhcGfdpJmYREiqOCHexAgxS4cKIpj04ChZ5.x7tvra2VUM9EJ8lJxmZVdxvjvJEMVL1c9GSbOJ9&ap=28","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15tp5vuv5(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31760810223,dmn$investir.nexity.fr,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1329679,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/C7XMA2jVl9P.UGaw_5Lw1w--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1457363149869-3992.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Du 7 mars au 10 avril, bÃ©nÃ©ficiez jusquâÃ  63000â¬ de rÃ©duction dâimpÃ´t avec la Loi Pinel.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":4,"cposy":4,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31760810223","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":3},{"type":"unknown","id":"bf3516af-3d77-3c74-a1d0-5639a6c90136","instrument":"4000010280S00001","is_eligible":"true"},{"type":"unknown","id":"2e344f1d-749f-3a7c-b3f9-3e73b22a989b","instrument":"4000013270S00008","is_eligible":"false"},{"type":"unknown","id":"c5864ba4-fcc3-3c51-a789-c288c6507563","instrument":"4000014330S00002","is_eligible":"true"},{"type":"unknown","id":"f96f5d3d-fcb1-3b8b-bf30-c50dbea3fa1a","instrument":"4000024330S00008","is_eligible":"false"},{"type":"ad","id":"31774677320","title":"DÃ©couvrez les nouveaux Samsung Galaxy S7 !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=GthVXjsGIS921DcOxN6HWwvEisyakwFw1Yi634OSFujFY_rMYBdHBdHyx7Ms5u_XzfmUkpR4QPb.qetxoIeG9jam97eZRp34QY34tn7Y2Agis8ZZ4oZV7sd3jE.Xlx9TBU9IH9o.wkfQha.ODFVKhLnlE80qGZO0dW44UE4Rci5uBJAfGogRmUn2JOBsUolJPDmWo1eJ.CUj3TPeSQ3mcckfz6CelnXyU49Fxh4eJJfRsYeP5JDvF2rDqLULO9Qz82O_A8jh05PfLjphtBhjW5BYzOePyOqDVi9JSnmAWfpkF10dPEqE1j1SP2Ak_KCgiV_2Iz23eDR8e5ibwSTEIHuCoHq2dCejaaGjUBZd7p7LuduqWzNQd0ajp0zat4VMnJzIPBVySi6zeaChdr0zTCuO3J80qVeFI7I1diZhgMQ_DthkMVycVrqLgFGYSWeLz3wROpES8B6Au_U9q7uqLjYsBvZc38nk2kc6JVSaXmqXBam_.tZPU79n3JOc36LLRo_msqWrjWwm8ZYPFk_Hn9.DDQ5rnlOn_.bSuAEUxHuaYV9vbx4qBOBi1_NhtpjV992TRZbedYHDuVbpc9UjtAisHyb0DjZopZdPqw5IAgqNa.YHCr8qNavMlUVxu.6gmXN1T_f9ZqlXlgI8I4RFf89UdIOB3h8NlUeEQ48EPqqmiQijZZj8M52UUq929louaSwUXyGppj_P0rlw6n6eEbgGyIeBLyNVwdc9YkmCrSUJkdoR3Z7jRGlN_sXg2u.7DFoIzzmVKWiMe67d5wDlBNgfQSzqwsCocSqZvoIWbAbYdjXu.dUO2D97O1jEZ5wl.n1eExmRSmeyUvVk6N_GocnnqKn7xDw_GY90pzDs7tPbFG3Y0ZR.Kqup0LxapoiY0TlTETOs48QwA6Hw6TrVPCIv0JFnwPdvp4K_JS8.BKJXGk6ZRq7HJ9loBbWg4hTDgRg-%26lp=https%3A%2F%2Fsamsung-france.demdex.net%2Fevent%3Fd_event%3Dclick%26d_src%3D38086%26+d_site%3D%5B%25tp_PublisherName%25%5D%26d_creative%3D%5B%25tp_AdID%25%5D%26d_placement%3D%5B%25tp_PlacementID%25%5D%26d_campaign%3D%5B%25tp_CampaignID%25%5D%26d_rd%3Dhttps%253A%252F%252Fad.atdmt.com%252Fc%252Fgo%253Bp%253D11252200563958%253Bev.a%253D1%253Bidfa%253D%253Bidfa_lat%253D%253Baaid%253D%253Baaid_lat%253D%253Bcache%253D%255B%2525RANDOM%2525%255D%250A","publisher":"Samsung","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=.a_Q8a8GIS.vKXV7auq4aKCyXolw3q6b0LKC7EAUFQ0_ijksJA2ZO.IaX18MozZGBxwKhaSBYg1ja8VtdXgz7eTH3klQuExAt9fjZsuYBtKqxHtApfWS2mNkj3EVsisW1XwQ.82yIIlKk4saZHkmHLLwxF2_lhh4nBNXSuqnVNyV2V1g.75sZymJp03mBh4EFSwrGE60mSB_WSoX2XCgCzGrtDejJsAbL0RRPghEmblQUAbzu1pzbEInM7vSnbuFZrZBdy_NWBOKVlgXEC4VnzGon.nNYyjtN9lTDMkX6PlL6_jFh39mOL8MBQ33Xw3LPuTePp4ZngU7uARW6aTwVZdNkPeZPVhTtdgMEOeQJ9Hb7lLjFfZ3XQoixYgjU4c2.V2SBU0_9_wY&ap=33","imprTrackingUrls":["https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=17026953&PluID=0&ord=1458927491&rtu=-1"],"ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15n1h50qt(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31774677320,dmn$c31774677320,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1352882,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/VNGC2attdVJYfCqKLglevA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1457970867480-5198.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Et si vous repoussiez les limites du smartphone ? DÃ©couvrez les derniers smartphones de la gamme Galaxy !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":5,"cposy":5,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31774677320","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":4},{"type":"unknown","id":"920a04ab-737e-3677-8a38-1b28a1eb0481","instrument":"4000012640S00001","is_eligible":"true"},{"type":"unknown","id":"c807af21-9333-3979-93b1-c9494315e74b","instrument":"4000008140S00001","is_eligible":"true"},{"type":"unknown","id":"5b8114e6-1ea5-3867-bf16-c01b8e12884e","instrument":"4000026610S00008","is_eligible":"false"},{"type":"unknown","id":"391463a0-f022-11e5-ba9f-3e9d50212ac1","instrument":"4000000610A100S00004","is_eligible":"true"},{"type":"ad","id":"31487731525","title":"Baskets pour homme dÃ¨s 30â¬ !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=AsysUUUGIS9cIvG.D85rL0llUQxch6dwTPJuesvsu.HGuj7ehsZo_S4M1szjpSt86Mquj_G8LpWueuTeTVfYSKe8wYfG.oC7r_pXdqZGInyQk3rZdds0UrhNv3FZawuuL2oFbc767nv1ZESJ0mPwmOHx8cOS7SoAqNmc35dGc8Rq2K8JCj27boNmeSRHRMT2cHc48.4DXDRjGUwDofdVfgRrs3QOqztsoyx1ZzmecG.iqryu0PoDq6ES3qc.HaTBku38TuXoHtG4SsSL8Cr1DAMuJsDkCImYpXeRhg8DgrS6IO8IOoLMoLO7RliECTiB2LtRBkS1SaM3XvCIXKmEd32IiDsjnb_xmaN2MQgK6T.CVdlGvbRwmuoBtymU_w7ogMy1ISajO3sVGK.IzAJkn42i0_uctcXEvchCCylHjF9zI_q.Vlv_AC5OH4arokoXhidp1IbqstOyarAnRYACl7JKLJK9jtoCCroU4tLtov.AlPtWV6KKPzMVeOdxhCCzjGlT.q7x3rHmFAKFO0L3w4e7xk9IyvQsvHYIaTLwthL9WS3ofTtzvzirtbfAsEvjbdWWj7PVHTWy0UoKXfIlnGbg80JC6bJ9%26lp=https%3A%2F%2Ftrack.adform.net%2FC%2F%3Fbn%3D5598989%3Bcpdir%3Dhttps%3A%2F%2Fwww.zalando.fr%2Fbaskets-basses-homme%2F_blanc%2F%3Fwmc%3DDIS33_RH_FR_DS_2015208.%26opc%3D2211","publisher":"Zalando","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=eHGY9FgGIS._sfOGf9j0h1d4dNZS8WdKbGPYJoariUl4ePf1.1tor._aLYrkuqGWTOyX.FEb5AWZjlaOMOQVZY5gdPcMzKdKxCwLSGrIFyixQCdawD7swcNg055Hd1haSUyj_BQop9Mat_dRnZqrczT0cIKFun7DgZkokaVoXNGZige_at5zy2PauMt7qPTFfEZHwKtyJq.TFREkxM.ra.iaKvL3IIzjtUJa_U6eJ4gnBvK4PBQXiJRQJCMt6cjQ5AnktWeGtMVsZ209eAlEElN5nzcSyvWR8geEYfRTPAcWPS9658AGAOMhcKvHCggbWvKZDJFplXnEcROT7pNYyoLhlswUcowMbgHHVnV.ZyUEWYKDUVTdo88D5_GMrOMkmvGw.atjuIk-&ap=38","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15kpjvjcl(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31487731525,dmn$zalando.fr,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$978823,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/jlDrRfHMh8Z0rmCOr.tNAQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1448883421638-9512.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Large choix de baskets vous attendent sur Zalando. Livraison et retour gratuits !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":6,"cposy":6,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31487731525","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":5},{"type":"unknown","id":"a4d0207a-0d12-325a-81fd-7f30e1a765e0","instrument":"4000004150S00008","is_eligible":"false"},{"type":"unknown","id":"93d14d5f-f1d1-3555-a125-710959578482","instrument":"4000010340S00001","is_eligible":"true"},{"type":"unknown","id":"db941921-6254-3266-8a26-bd1cab744077","instrument":"4000015390S00008","is_eligible":"false"},{"type":"unknown","id":"6b41840a-33a7-3e66-b28a-bb5d29024753","instrument":"4000019460S00001","is_eligible":"true"},{"type":"ad","id":"31697955466","title":"Une escapade dans la nature britannique","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=OPFltl4GIS8MPlVvQoanQ7gPBamd.cDhKrQWZUqrI8wFXefphIFjDzqIpZevpiVqMDPsdH0FfcyDMOW26UQ1Vf.wf.O.EYsTc2J0VlN1bfkhXcIE6yRvmV1_UN8KY0UzEmyj7LLIi3mPRpt5beK2qTx1tTHa6eJvL4eeaHZLe596.Vh6agx18yGGQJv45pnsnHGAZ7Q3DCPJg7hVTmBBw7fQdXXdAEN.YKlqi5fxzQcA6M9_fwzJJdtoUyY3DHkIBahBze9ErGosc3G6P6WAzCWCWi7YEex1EXuyGaS65t9g3efAJuosgPfhdojEP7XUeTes9RtAcev36X00MU.F.oNjYQV6GwnqbyEvmt4qk95e2ofK4orWNltyDQdoPn606I9mD7o6YROdJ1SgiTOvG7uM6Q.hZczyexIoWPzWSeZuKYJu7a7RVx.2ZCNDdKhUQaEBtqzfCssDJxerGjADgDtkBAW37Vnu3LZzIzGzlO_pMIKGN1JuzLBktxnEBtRq4pBcrlVOwafT6dMtydOi8WTtLL3LtTqWsjQeqOEZ5jhzAbtkjloTLj.dfpjMO4YSs.BbeWuKbpCF4T2hEK3xybFx5xYHqt7AH4Q06Z45W39IFhhUkqvVYkZgoSsix3CRqLb8ezlxRoFIYMoBLw--%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN6198.282706.YAHOO%2FB9362362.128254145%3Bdc_trk_aid%3D301262097%3Bdc_trk_cid%3D68690228%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D","publisher":"VisitBritain","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=0LlJbNcGIS.aavGrVL7UOBG.XViZjluKr4uz1aDN_BX3e6HLObW4JtC3UM04rXET1Jlxgp3iL23bs7ZLm4hDoIHwb6lPNKWKhN2yw_FLGMIBx.rOLWwyx77mPqORPDicepCFJgl7i_aiIPHMlwxa0DjzFBUevN_64obdx3BMyiRSyLJ05GH.O6JH0AUesmhx7J3ZH13kzL1LQSOdCamq.ipf5EZEVrrE7c9Oy_2CG1ozoLSB9d5VSQagxnpNpKni5spbCUm5Z2GPx.N7DYKARm8FVJtE2KXg9UZ3hkQlvMotW3IxrwV2YFP6gAPYXl1hG4CoRCQ7dG17nPYCufVY3783ORLPDg7sypoVaVNzXAMC4176cqV_0fTHGGpXTAgfYglT.M4kkwHQ&ap=43","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15rer9nct(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31697955466,dmn$visitbritain.com,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1322287,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/h.eiqCececSsdiXoVMP9hw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454518171875-6773.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Campagne sauvage, ruines romaines, forÃªts mÃ©diÃ©vales et hautes montagnes, partez Ã  la dÃ©couverte des paysages britanniques.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":7,"cposy":7,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31697955466","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":6},{"type":"unknown","id":"87b3d6d0-1462-3c07-ba52-88eb28ab1463","instrument":"4000010060S00001","is_eligible":"true"},{"type":"unknown","id":"e30138cf-e4a3-3e54-9026-3d7c98a1e66a","instrument":"4000008660S00001","is_eligible":"true"},{"type":"unknown","id":"cabec8c0-339c-30b9-b39d-c9ccb149dca7","instrument":"4000018270S00008","is_eligible":"false"},{"type":"unknown","id":"3c3fc306-b6b3-354c-ad31-2ab293ae32b2","instrument":"4000021730S00008","is_eligible":"false"},{"type":"ad","id":"31764868087","title":"Jeu concours GRDF : tentez de gagner 3000â¬","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=GdNAU7YGIS9f_z9yqM33nSnkWLW5jSQOyEkbB.hShfjQYf8AEOUMvNL3SSFV7_4xOjfpqW5XRRT9iQwOwcZ36yyESEBAEvEKwspjPDsYXJ4tOCH0T.c.eGTxARBDE2ZCJTioc4LCsquIyo90oNpVyJdyt6mixZ6JlNmgwbpXDJEVr4hEc3oIn9b7qKLZQvw8iyaiWoI9Ki6G.dhFl4KDM_j7F9ckdK_z.Q9eaOhnChpTjRNYQ1NdVhQzVjlwy2.ZXudeuWaDotNd4R_PcHP6kHCY.76ApttLOSqGpKqdL2lNYXFKKm.qJjUG_YH9cguO_kXwcy58kXCLuGRzFianSiIztRwdGx6sCcR.WAdweF5zfV6.z54I8NLDwBBCUbFf0YbG8Wot24oYH4U418unV8V5A4b1PXplOv9B6F2w036_pP13Uvgj2aHW_dCTntl8JU_jczw_nKK0d5Shfz0Ach4_IAVmhYaUnhf86fCkwpAzzjk8Tpl0wqn4sJkZEcfbaRAzdqG8ky0O8XzJ6d95qDZ3IA--%26lp=http%3A%2F%2Fclic.reussissonsensemble.fr%2Fclick.asp%3Fref%3D736478%26site%3D14987%26type%3Dtext%26tnb%3D5","publisher":"GRDF","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=9HQBhQcGIS9CaRgu7wGDqlG_f3Ug8PMJIsZUOAi9QPFEJrAPfT1VrAWY0yJoeYOzIPobGRTjinlsml_hzblVnHmULiGyzuAhx9mpGAOKlTylLVs3WLyA6cC9N3fOvi2.ouD0LH9xRUzqlUtR6FLY51OXvOwH__Xk27HB5rgz.SpFyVGNMEF3u.sQVOzhHUyf_KuR7Z1BaYU4Ngj3cwlo2cHTw2UQmgy.ZXDXZbR.Asp3cmkXnEuyRdBcPFLyZaGapxWaU2uHW.E.dXJN.ylm0gZazTW0v5gdly4UD__EMotO6c_t.fLU1DZIGjUTKhiRDIAzjnz1wagyCx1fGKOJJR1krFY5zkW_Bg6KX4vm5HrfCQkWe3OUk_rHV62pRh70CJY3LActmII-&ap=48","imprTrackingUrls":["https://banniere.reussissonsensemble.fr/view.asp?ref=736478&site=14987&type=text&tnb=5"],"ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(1669dmubd(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31764868087,dmn$clic.reussissonsensemble.fr,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1158075,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/wtrIUO2JMr0hhbff7WyNQQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1457615382967-5473.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Testez vos connaissances sur le gaz naturel et tentez de gagner 3000â¬ !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":8,"cposy":8,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31764868087","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":7},{"type":"unknown","id":"ac9ebcef-b04f-3a20-879d-e0baa7b1ffd4","instrument":"4000012810S00008","is_eligible":"false"},{"type":"unknown","id":"34866a84-0a45-34ec-a0b1-655fd5f6cb6b","instrument":"4000019310S00008","is_eligible":"false"},{"type":"unknown","id":"6dc8ee86-d47b-3c93-9749-3838662e025e","instrument":"4000021320S00008","is_eligible":"false"},{"type":"unknown","id":"c-e1b5759b-0840-4d17-9e7d-f250cc6d6221","instrument":"4000000970S00001","is_eligible":"false"},{"type":"ad","id":"31748183947","title":"Jusqu'Ã  -40% sur votre assurance auto sur-mesure ?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=bOiS488GIS93rPfO4Dnip9_4nN5FVq8C_FV.FDnXnmmDt7j4xEUpZ3scK25A7gaaoTRj.6.baxzDnqm43chUuNExia2244G18f0O4oHXnYKP9iWkus5ngzbmper2oaRywL38LYz7glfkZM0ibFeCz9Dfl831FHlzOF5_t0wkqTfubkAAzhXkP6cT2yD2H4CkCu6GTVBsG0KEARBw2eEz4J1R_GttH0wclnjc0lz9C64xX0_MA5rLThiFLDTNRMMIYTLM5y0qh1Dzkk3KmGpHsnCWhO0KaQHxPMWHMBmX6uONyj3UNi2_gzIuXXp6qNa_xru7rQDHKpQAQi2gT1.BfGVQPvaMAdBQPjiA0iEhQAZvdfTwMkHEJfRrnzBsbS.c3XNeRUaVCtVHmLXrIS0VBhKuftJhC9RkmLD1iiWlF5nyj6oByzXCqOnt3WwrOlvmOyEJt.JRAPoBFG3GUiVcpLnWg_Ffd8AVSPvvNeksdJqnE.U-%26lp=http%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F297611419%3B124711560%3Bc","publisher":"Axa","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=_nk8NBQGIS..fY6jR51f28.5fH9cNXyzduJw3efX7lNAuOViXw72nEoT0fuvVbUwbblshUbqyHT07SRJdytxA1S9NM0VkS6oZh7l6qLxmyeh1_hfKyoj3A5hoaUcKEdcJ3GOXp2wbZ8QtYL_w12r0rP_Lb5AoGZj78zYaCkSR9YOdV4yOnqXbY8vOsZVOb49rTO4AZZIQXteQG8O3_tkIHr5M6bIsMVWz1vr.lp9PvwoclPcFmNbc9j4Jqqj0bhU13KlpFZrFExe8Ejft.Ch8sbluw2g8DxvzA44yB1lXC_QekFkzUi1B5C2Ivcoz.UriIE256c0XjYIbhaR4FwcyipEInQA2EXedOlYLcUT0R_tdUkbiFoJQuiFDV7BJlYBLsDocOwnQw--&ap=53","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15gct7if8(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31748183947,dmn$axa.fr,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$984886,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/OV5mVQCjpfh6DOMOLbXUcg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1455524450605-7928.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"A la recherche d'une assurance auto modulable et adaptÃ©e Ã  votre budget ? DÃ©couvrez toutes les offres AXA personnalisÃ©es !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":9,"cposy":9,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31748183947","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":8},{"type":"unknown","id":"922918a5-b841-3e7a-861e-c39f07664399","instrument":"4000021760S00008","is_eligible":"false"},{"type":"unknown","id":"ed5e4df8-300f-3dcb-90c8-f8d0027dfbb6","instrument":"4000011930S00001","is_eligible":"true"},{"type":"unknown","id":"b94f51e3-f4d4-3fe0-9e0e-a878a72d07e1","instrument":"4000017270S00008","is_eligible":"false"},{"type":"unknown","id":"524706e2-68fc-3183-a3d2-dc7eaac3ba6c","instrument":"4000001390S00004","is_eligible":"true"},{"type":"ad","id":"31756508597","title":"Birchbox fÃªte le Printemps !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=ZN3J7vIGIS_KOdqgUxIYWwQoQ3QgNHAyL_BWpqdsGjs2FJX3ZLSgo2p1L9ieY4Z.CJt6bSLbPXpEmlZe4mAbGcJ48WdlmJLasDN3K2uWfNw6.OUH9dCZi75Wd6Ppm3Z8s31fxoLkqwTbPoJSpsnb.txrxdnPb03qmfVKtW7k5.NmGAv3maTbZBTaCihb.AJzvjZcwLumQK61zn9BFmZw6r2dP_TESr4KX7MuZdUTyewiZJ4RaMug3OLSZUNZakA5REc6B3UK58RXX71e8Aq4iWS5GM5FQ8yBIzo3fhOI0gMZgVxXN7hHc4_Ymhi1dUsFAD9THfqato4bNoe6VoFuUZwRAvB5TTCpPTkXep.qbn2MFq6nF2uIXZ1cCxLqogbkJS6Rhul1UTCBFNuKnKZHeXDcMuUHDE27RcycA7hpfsGufPTQfEZS3KzunPhtNOlLNXfRvz.BJTUp_qOXsov652qmiq6QzznPzmoQ7nw1rDf8QIDsHKFJxjsXeMgqD1ZQnxFiKt6jj.7RIl0hY5Jx7tkSYn3e32JQYfPwfK_GBtugvUqNxmgTPHrTtNvbyRhXZZ0TqrdWC9feC3PrQ0CQ%26lp=https%3A%2F%2Fbirchbox.fr%2Fbox%3Futm_source%3Dnativeads%26utm_medium%3DGemini%26utm_campaign%3Dinteret%26utm_term%3Dhorsmarque%26utm_content%3Dinteret","publisher":"Birchbox","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=UkNQxiMGIS9amvj4kWWKm.6Mxq50J8btWHSO3N9GvvvjcCU33QOjxDVZzDPrY9bVH3IYJGSD9qjhK3k0NIgskS_mcomnh3svef2gsPyqiLrAJPHz0f_ioGnG6fnjQd9sfUfLjeGGbXztXzJSYQNWqaA8X.IqLRZXrLMqOSwl28kFrBd3qbQ4AZYZaJhv7fgrYrC_ZehyABUlvB.wrOIIp1Nfr1hhwQw8ixRX.E.c7FuMnE2sEr2dOYUI11BWUKyldbviPg0sbxJr2CHuJa0J3MmReTmzv6j_RJLouB1LdeOHPbXT4j9NO85jGmuTbySgWZSGtpYcAAC7Nr7UCSL3BU06UnQzTcZtYrJWaWUBtnzohwUFY0oZT3oBDnQE3QuBeDfKWjObmvnk&ap=58","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15m7mj4nc(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31756508597,dmn$birchbox.fr,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1168738,pbid$1,seid$4250754))&r=1458927491998&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/9qGb1UrlfxClIKq3JbxhoA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1456845118285-6915.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Pour 13â¬ recevez une sÃ©lection de produits de beautÃ© dans une box transformÃ©e en tiroir. Inclus dans toutes les Box, un vernis CiatÃ© !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":10,"cposy":10,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31756508597","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":9},{"type":"unknown","id":"44e32950-1e10-389b-b736-4ed867e6fd70","instrument":"4000019940S00008","is_eligible":"false"},{"type":"unknown","id":"8498f6bd-5db4-3bee-a86c-de8cd9da1ce4","instrument":"4000020290S00008","is_eligible":"false"},{"type":"unknown","id":"77c47e97-c2ff-3333-8f0c-2cd40c152402","instrument":"4000020570S00008","is_eligible":"false"},{"type":"unknown","id":"e6b3fa29-2129-3031-92cd-6cd5eed9968f","instrument":"4000015980S00002","is_eligible":"true"},{"type":"ad","id":"31762450023","title":"Le tÃ©lÃ©phone est le reflet de son utilisateur","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=N3e6RNoGIS8YSgNTGBhA48BzSANmcntMcoA8RElTjndbdlHHVbLO4qm7QIriSa5pzmG06xSB.f_h2pJU0NpDCVEI35W37En6LVmOeyFN3lz_JbDRclqRUW7LDHSQ_11_S2OvJsHYjJl1lMAl6Pi7rbhkDm0lCkHr.FmIvSKvGyAWWDrhE7MGdmJCF.GbwifZ9uc9fML3XvjxaRgvcpfUYwP.ox8O74zm_QQpC_dDDy8Qu_2SzyXRo61oJLpn5ss3dQ8c1mZypUvgVbB.daOV4ThMyV4K7h1FnAmmtAbD.26WuP4GZBRs01wHXXzx_DfY1iyfqEogSi6qYIxVzyOIuOkX0EPhZCRpfOXWdOzouFKeGvXi5yEWRJY3N8bpFJhiqSJQ8qXETsGot9VpKmEIBpS5EpUt_3O6PePPjXIYrrBvfuRw1PaGfLGaztpA7pC13hHon6S3vijjvSCls7nmfWObdr1gze7v1S.FRK9ColeBBSr.3raUCOTkwfFlTwBj3f_CprZiuhkPBQkhnqQK3Ijm_bdoVMzuZoXtT4huVgKR_Ipp28pva4DV4CdsdyaZc3CV%26lp=https%3A%2F%2Fbs.serving-sys.com%2FBurstingPipe%2FadServer.bs%3Fcn%3Dtf%26c%3D20%26mc%3Dclick%26pli%3D16865133%26PluID%3D0%26ord%3D%5Btimestamp%5D","publisher":"Samsung","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=fBquGm8GIS.xlRecy3e8Batj4ik91He901XOQmLQm1_2dZO7Y.azrFeYi961kcJQg4T85QxQd1lg7kvDItApXYkxbubJQEmQfOCwtVK2ii.LkO8PMnqdbQtSpT12V3OhWQMXS0kK.mG8q1ilbbe3qGWkHB06UOPLLZ21hiMGr8UjSBKdZ5O0WhCJ5RnNkT1HOwuFKXeRjuEhUVN_lJ5yOqRPQ5QKmi_ODviRBSzUBp6R_9i08Gy9vZ5w4rmzRkorpxMpad33mLyQFYKfgoTn0gube6adcFZN8qtPs0_riXBJpajo8HSI142Y16Uhs4pIMgx.HULhLWNMdYWD95lb3Y6Mqn574DWE5eHFrARkhxdbzrIEtq9MAeqUHHpVniEf4HDNjfc_gAVmOg--&ap=63","imprTrackingUrls":["https://pixel.adlooxtracking.com/ads/image.php?banniere=scsmgglxardonentvads&campagne=starcom_samsunggalaxya_radiumone&client=starcom_samsung_galaxya216&type=regie_quotidienne"],"ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15m4oq989(gid$58a15aa8-f2b0-11e5-bc7a-008cfa5b3354-7fe6c8aaf700,st$1458927491998000,li$0,cr$31762450023,dmn$samsung.com,srv$3,exp$1458934691998000,ct$27,v$1.0,adv$1340700,pbid$1,seid$4250754))&r=1458927491999&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/GduOaSjv5wVq4Yx0ECzMRw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1457530148947-2528.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Disponibles en trois coloris, les nouveaux Galaxy A, Ã  la pointe de la technologie, permettent Ã  chacun dâaffirmer sa singularitÃ©. The way you are.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":11,"cposy":11,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31762450023","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":10},{"type":"unknown","id":"a55fcf01-1897-3fb4-ac1c-ffeec1c96489","instrument":"4000017660S00008","is_eligible":"false"},{"type":"unknown","id":"b5447582-23df-30d7-8d42-d583c173524e","instrument":"4000015630S00008","is_eligible":"false"},{"type":"unknown","id":"127b1255-7ea6-3c4a-8176-563f5029f123","instrument":"4000025540S00008","is_eligible":"false"},{"type":"unknown","id":"3b281fd7-3873-33fe-b093-0f036527e403","instrument":"4000011470S00001","is_eligible":"true"},{"type":"unknown","id":"7e2b3310-b09a-11e4-937f-43197d438b25","instrument":"4000003940S00004","is_eligible":"true"},{"type":"unknown","id":"38e39195-f1f0-30c7-a15e-6dfc182fdf79","instrument":"4000010710S00001","is_eligible":"true"},{"type":"unknown","id":"190844b8-f8e9-314d-a32f-fd56e3186795","instrument":"4000015750S00001","is_eligible":"true"},{"type":"unknown","id":"192fc3c3-99d0-357b-9f54-ebc6ecaf967f","instrument":"4000000880S00008","is_eligible":"false"},{"type":"unknown","id":"0023a8a5-2353-3bc4-829b-e413829da9ca","instrument":"4000012400S00001","is_eligible":"true"},{"type":"unknown","id":"1e7a9246-61a3-30a0-b45c-491c17c982aa","instrument":"4000017660S00008","is_eligible":"false"},{"type":"unknown","id":"6bca27b2-f6cc-3f5e-8df3-da1f97aa73dd","instrument":"4000020280S00008","is_eligible":"false"},{"type":"unknown","id":"ec61426f-7f47-3698-bb62-2f988389155d","instrument":"4000017270S00008","is_eligible":"false"},{"type":"unknown","id":"c-6de5abb4-ef1c-4edd-a4eb-9af32dc4ec69","instrument":"4000001120S00001","is_eligible":"false"},{"type":"unknown","id":"a1bf9583-bf24-3966-9ce3-2defe2608c26","instrument":"4000020160S00008","is_eligible":"false"},{"type":"unknown","id":"b87d38c0-1dfd-3948-97f2-a8b5aeb2ab29","instrument":"4000011260S00001","is_eligible":"true"},{"type":"unknown","id":"79f426b0-d20c-3be3-a7a3-0ea478d790ba","instrument":"4000021570S00002","is_eligible":"true"},{"type":"unknown","id":"b9519155-5b75-3727-b5b2-3eefed69df38","instrument":"4000017720S00008","is_eligible":"false"},{"type":"unknown","id":"6115c533-38f5-3dda-84de-4ba4b3f28fea","instrument":"4000023760S00008","is_eligible":"false"},{"type":"unknown","id":"c-4919b084-6f3e-4f1d-8ad8-8c55a4d3ebee","instrument":"4000004650S00001","is_eligible":"false"},{"type":"unknown","id":"a3c18a96-f8cd-3ec6-a9a1-a1404450d1a3","instrument":"4000011430S00001","is_eligible":"true"},{"type":"unknown","id":"5659093a-77fc-3ce4-84af-b162645b1476","instrument":"4000009960S00008","is_eligible":"false"},{"type":"unknown","id":"ec04ab13-2d86-3cd1-bd43-0f3637de8220","instrument":"4000009620S00001","is_eligible":"true"},{"type":"unknown","id":"980841bc-5b3c-30ef-aa4a-35c2086eebf9","instrument":"4000011870S00002","is_eligible":"true"},{"type":"unknown","id":"c-04e65d8e-1bf9-4dec-b45e-4ed7f0ce1d67","instrument":"4000001330S00001","is_eligible":"false"},{"type":"unknown","id":"d2c97aef-b0ff-3519-b770-2080891c4e21","instrument":"4000016920S00008","is_eligible":"false"},{"type":"unknown","id":"4c0f9551-281c-3af1-8635-6e21ac0bdfec","instrument":"4000002240S00008","is_eligible":"false"},{"type":"unknown","id":"e9ad9331-c07b-3840-872e-2c1a4993d9a9","instrument":"4000007500S00001","is_eligible":"true"},{"type":"unknown","id":"84e0183f-f0e2-3d8f-a9c0-642b5940c0d5","instrument":"4000007480S00001","is_eligible":"true"},{"type":"unknown","id":"b2b4f58c-7fcb-3008-bcf2-7439e9a43c26","instrument":"4000007510S00001","is_eligible":"true"},{"type":"unknown","id":"31c16080-f1cb-11e5-9dff-e83ee67e1005","instrument":"4000000460A100S00004","is_eligible":"true"},{"type":"unknown","id":"1404e348-d8e2-3413-bfe8-f9fe7fc78e28","instrument":"4000001110S00008","is_eligible":"false"},{"type":"unknown","id":"0263b477-3e53-3596-8a17-2190dfcea512","instrument":"4000010310S00001","is_eligible":"true"},{"type":"unknown","id":"707e3ac7-cb20-32ae-852a-924c6caa5e2c","instrument":"4000016299S00008","is_eligible":"false"},{"type":"unknown","id":"c-a710adab-ee3d-45a6-8077-24e2f3083acc","instrument":"4000005290S00001","is_eligible":"false"},{"type":"unknown","id":"305cced9-9ac6-3acf-aa5a-924a0e1e253f","instrument":"4000013930S00001","is_eligible":"true"},{"type":"unknown","id":"46247d40-39e5-3f8d-91b0-fb3f6499e9f3","instrument":"4000011470S00001","is_eligible":"true"},{"type":"unknown","id":"4e1e4354-6008-3381-a933-129e7b0d3d65","instrument":"4000006640S00004","is_eligible":"true"},{"type":"unknown","id":"4c6e6fb4-e244-367c-aa0e-842e7550dc1d","instrument":"4000011000S00002","is_eligible":"true"},{"type":"unknown","id":"8b438a61-b2bf-34a3-8c60-e214d7d22a29","instrument":"4000018870S00008","is_eligible":"false"},{"type":"unknown","id":"ceef7cb3-2235-35ab-b66e-d6e476e8f96d","instrument":"4000018540S00008","is_eligible":"false"},{"type":"unknown","id":"680cee5a-e792-3126-ace5-fdb5ca5a0083","instrument":"4000007480S00001","is_eligible":"true"},{"type":"unknown","id":"2e38e79e-95dc-3aeb-8a2c-c2df7b107d0d","instrument":"4000015490S00008","is_eligible":"false"},{"type":"unknown","id":"4a890a50-bee8-303e-9435-5cd05bca25e6","instrument":"4000019230S00008","is_eligible":"false"},{"type":"unknown","id":"905589fe-d12b-3326-9031-a5c16946c44d","instrument":"4000002400S00008","is_eligible":"false"},{"type":"unknown","id":"e6443cbf-95ce-3eba-9eaa-8c41256e1835","instrument":"4000016840S00008","is_eligible":"false"},{"type":"unknown","id":"4341a5e6-2ed8-392f-92d7-bd94eea18373","instrument":"4000021680S00008","is_eligible":"false"},{"type":"unknown","id":"e42f8554-c203-3a97-a52c-329323ef1eed","instrument":"4000008430S00001","is_eligible":"true"},{"type":"unknown","id":"ea218914-54e9-38bf-b440-bf88c6ea6024","instrument":"4000015900S00001","is_eligible":"true"},{"type":"unknown","id":"078869be-140d-38a9-8557-b768dd870471","instrument":"4000000920S00008","is_eligible":"false"},{"type":"unknown","id":"e933ce48-31b8-33d0-8d64-f848d5f7b495","instrument":"4000013400S00001","is_eligible":"true"},{"type":"unknown","id":"d1e5e672-aa76-3122-a857-09033c91acea","instrument":"4000008470S00001","is_eligible":"true"},{"type":"unknown","id":"63783dcb-4b45-3bb3-804a-079db1db987c","instrument":"4000010810S00001","is_eligible":"true"},{"type":"unknown","id":"82791967-7029-313e-92a2-1e517846d776","instrument":"4000020320S00008","is_eligible":"false"},{"type":"unknown","id":"adc35351-1c73-3d14-b476-2fccec1293bf","instrument":"4000011930S00002","is_eligible":"true"},{"type":"unknown","id":"8d8aac2b-bd3d-3eb1-9cca-a7e736cfa368","instrument":"4000017240S00008","is_eligible":"false"},{"type":"unknown","id":"37e706b5-1476-3508-acd9-099aa892768d","instrument":"4000015650S00008","is_eligible":"false"},{"type":"unknown","id":"e322561f-4e5b-3a62-a3a3-ed1c46c4535a","instrument":"4000019600S00008","is_eligible":"false"},{"type":"unknown","id":"5116a1fb-bb4c-30f2-a805-d0021b5ff046","instrument":"4000014990S00001","is_eligible":"true"},{"type":"unknown","id":"7ce869ac-a502-37f0-90c9-6d6c5ce5fb8a","instrument":"4000019940S00008","is_eligible":"false"},{"type":"unknown","id":"a38ae990-68ef-3c01-9938-0992e4a86ccf","instrument":"4000017420S00008","is_eligible":"false"},{"type":"unknown","id":"e355e108-2635-3719-abf3-d4306126ae81","instrument":"4000013280S00001","is_eligible":"true"},{"type":"unknown","id":"d1a84cf0-f061-11e5-b57e-58d8f0f8cd23","instrument":"4000000450A100S00004","is_eligible":"true"},{"type":"unknown","id":"59493e64-29e6-3073-8dc8-dac5ac99bc25","instrument":"4000018910S00008","is_eligible":"false"},{"type":"unknown","id":"3e620b4c-f510-3022-95c6-22adabeb625c","instrument":"4000010300S00001","is_eligible":"true"},{"type":"unknown","id":"1e444154-9dc3-327a-9d67-4c725102216f","instrument":"4000008120S00001","is_eligible":"true"},{"type":"unknown","id":"541cad1b-1265-381c-9ca6-0d8e69d529a3","instrument":"4000014680S00008","is_eligible":"false"},{"type":"unknown","id":"9aa4b24d-4758-3b71-b73f-8225967ac63e","instrument":"4000004330S00004","is_eligible":"true"},{"type":"unknown","id":"4a213e62-33f6-316a-a8fd-59aa0e5d54f9","instrument":"4000010460S00001","is_eligible":"true"},{"type":"unknown","id":"ab896815-da09-3070-b499-b45db663d32b","instrument":"4000016010S00008","is_eligible":"false"},{"type":"unknown","id":"9daa163b-5bb6-39d8-9cf4-581c22f1fabc","instrument":"4000010020S00001","is_eligible":"true"},{"type":"unknown","id":"bc98c235-b875-357c-9c98-d6b8e9205517","instrument":"4000017390S00008","is_eligible":"false"},{"type":"unknown","id":"954166c8-2493-31e0-9721-76bf6a4920cb","instrument":"4000006960S00001","is_eligible":"true"},{"type":"unknown","id":"eee7fb70-981c-3112-a1d0-6b3fa2e5f943","instrument":"4000015570S00008","is_eligible":"false"},{"type":"unknown","id":"e5e374a8-b530-34bb-a1b7-8347947d2c73","instrument":"4000020210S00008","is_eligible":"false"},{"type":"unknown","id":"0d150f11-4848-3447-b7d1-c24732f98da6","instrument":"4000022350S00008","is_eligible":"false"},{"type":"unknown","id":"db54ac04-d33c-3697-93b0-afe57930f70e","instrument":"4000020290S00008","is_eligible":"false"},{"type":"unknown","id":"36b305fb-211d-3a28-b2af-bc00a06c95a9","instrument":"4000014980S00008","is_eligible":"false"},{"type":"unknown","id":"6182882c-0ce5-34a2-b96c-b81b91ae8a2a","instrument":"4000008580S00001","is_eligible":"true"},{"type":"unknown","id":"0529c52b-1580-38fc-b980-e727927ce060","instrument":"4000018360S00008","is_eligible":"false"},{"type":"unknown","id":"2ba66ca4-9947-37bb-83fc-ea545fd21e79","instrument":"4000000780S00008","is_eligible":"false"},{"type":"unknown","id":"09389d55-b2ef-3a57-91d9-48c70ff40827","instrument":"4000010380S00002","is_eligible":"true"},{"type":"unknown","id":"274ecb9c-a391-35c3-a878-e883f900c6b6","instrument":"4000017420S00008","is_eligible":"false"},{"type":"unknown","id":"4229eb9f-e8a6-3ff4-a0e0-e8df671c32b5","instrument":"4000017010S00008","is_eligible":"false"},{"type":"unknown","id":"5d0ba603-4327-3fc9-a567-7cf6be35025e","instrument":"4000018190S00001","is_eligible":"true"},{"type":"unknown","id":"fcb50311-6cda-31c5-a7c8-bc89b453c627","instrument":"4000009160S00001","is_eligible":"true"},{"type":"unknown","id":"85a8eadc-7f31-3ef7-9086-41098d6d0d32","instrument":"4000016039S00008","is_eligible":"false"},{"type":"unknown","id":"b756f356-f771-3f35-83d5-097fe5b20373","instrument":"4000014510S00001","is_eligible":"false"},{"type":"unknown","id":"debe9d59-b96b-3c27-8e2f-6a70851b0c92","instrument":"4000016810S00001","is_eligible":"true"},{"type":"unknown","id":"dfd3143f-b2d3-32fa-84fc-65a698235af0","instrument":"4000006180S00001","is_eligible":"true"},{"type":"unknown","id":"44c8c441-c631-3d79-a290-67a45ba46677","instrument":"4000017420S00008","is_eligible":"false"},{"type":"unknown","id":"6413c1dd-fbbf-3034-9fb0-3d7e3fa9085a","instrument":"4000009310S00001","is_eligible":"true"},{"type":"unknown","id":"f9bae691-4afa-3262-b848-b6b833d928e7","instrument":"4000008950S00002","is_eligible":"true"},{"type":"unknown","id":"e31ed40c-b8cb-3b78-b74a-b97e83cbdcef","instrument":"4000002880S00004","is_eligible":"true"},{"type":"unknown","id":"e2ef67c9-ad7e-3576-85aa-b5a14a600445","instrument":"4000017330S00001","is_eligible":"true"},{"type":"unknown","id":"feab6960-c565-3f7b-8ee0-e7f87b946488","instrument":"4000014780S00008","is_eligible":"false"},{"type":"unknown","id":"bf6f41cd-d586-3d08-90e5-58541919f1cd","instrument":"4000015170S00008","is_eligible":"false"},{"type":"unknown","id":"2870d929-afe9-3dc2-a8dd-006b6a537fce","instrument":"4000011670S00001","is_eligible":"true"},{"type":"unknown","id":"92822301-5d90-3fe9-937e-4ee7e4f24c87","instrument":"4000015689S00001","is_eligible":"true"},{"type":"unknown","id":"7b8ae99d-5012-3aca-aa99-f44d7795a023","instrument":"4000010920S00001","is_eligible":"true"},{"type":"unknown","id":"140fee10-ec8e-3fe7-a256-0f5657f5bc88","instrument":"4000006370S00002","is_eligible":"true"},{"type":"unknown","id":"0a4f828b-4ee1-3e7f-9a90-a0034993646d","instrument":"4000018620S00008","is_eligible":"false"},{"type":"unknown","id":"c21a4af4-ef40-338f-861b-f1c4ab26dcff","instrument":"4000000940S00008","is_eligible":"false"},{"type":"unknown","id":"622443ab-0c4c-3301-bbee-35c5493d3827","instrument":"4000019080S00008","is_eligible":"false"},{"type":"unknown","id":"dc421e4d-134d-3fb3-b201-86d699dce4c1","instrument":"4000017080S00008","is_eligible":"false"},{"type":"unknown","id":"97933f9f-16a3-3aa6-b71c-d730946a4c13","instrument":"4000005010S00004","is_eligible":"true"},{"type":"unknown","id":"7ed5de6b-230a-3806-b824-0d4869c31099","instrument":"4000005410S00008","is_eligible":"false"},{"type":"unknown","id":"2908f28d-2bdc-32fd-b199-bea55aceb0f1","instrument":"4000013180S00008","is_eligible":"false"},{"type":"unknown","id":"5ea1b689-a094-3a75-983a-02cce4cd5fd2","instrument":"4000008360S00001","is_eligible":"true"},{"type":"unknown","id":"9f691fd0-e2c6-36fc-ae5b-01a025256880","instrument":"4000015170S00008","is_eligible":"false"},{"type":"unknown","id":"49e0240d-fc4b-3466-9a43-6096596a5371","instrument":"4000018540S00008","is_eligible":"false"},{"type":"unknown","id":"dca80e42-f6e8-3569-843b-ab097eb49f0d","instrument":"4000011430S00008","is_eligible":"false"},{"type":"unknown","id":"c888eb0b-d6e8-33fd-8b17-de935f396a3b","instrument":"4000019010S00008","is_eligible":"false"},{"type":"unknown","id":"74fb5e35-4b4b-3aee-9d68-4f7f2d89132c","instrument":"4000003950S00008","is_eligible":"false"},{"type":"unknown","id":"c8439cc4-5dd1-3aeb-9b8b-2fc7e203a46c","instrument":"4000007740S00001","is_eligible":"true"},{"type":"unknown","id":"08c16199-baf9-3783-b704-3c078128fa28","instrument":"4000014110S00008","is_eligible":"false"},{"type":"unknown","id":"c980f507-19e9-37aa-add9-ba94957bb391","instrument":"4000000890A100S00004","is_eligible":"true"},{"type":"unknown","id":"edeefb02-1ebf-3a10-8e0e-caa9c3a750ee","instrument":"4000011430S00001","is_eligible":"true"},{"type":"unknown","id":"ee2f7ea5-6bdd-3c6a-b512-a123522ead7d","instrument":"4000007520S00001","is_eligible":"true"},{"type":"unknown","id":"d4150101-e7d6-30b9-b718-6c8263adb209","instrument":"4000010700S00002","is_eligible":"true"},{"type":"unknown","id":"e88f582b-8bf0-3a44-82e3-02823c489e19","instrument":"4000016030S00008","is_eligible":"false"},{"type":"unknown","id":"fd239fed-4950-3e1d-90fd-947d7ba588dc","instrument":"4000012050S00001","is_eligible":"true"}],"continuation":"","more":172,"enrichment_ids":[],"cposy":26,"stream_items":[{"id":"id-3599436","cauuid":"b2ef655a-f7f1-3008-9c4d-9de92e6b6f93","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ss_cid":"b2ef655a-f7f1-3008-9c4d-9de92e6b6f93","refcnt":4,"subsec":1851,"imgt":"ss","g":"b2ef655a-f7f1-3008-9c4d-9de92e6b6f93","aid":"id-3599436","expb":0,"ct":1,"pkgt":"need_to_know","grpt":"roundup","cnt_tpc":"Need To Know"},"link":"http://news.yahoo.com/officials-us-killed-senior-islamic-state-leader-141746901--politics.html?nf=1","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"id-3599387","cauuid":"da3c7ee3-16fa-302f-8073-9d09541f4826","link":"https://www.yahoo.com/tech/microsoft-launches-ai-chatbot-on-twitter-and-it-132424697.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3599399","cauuid":"73f9eb08-591d-3a13-8130-acd5678758b0","link":"https://finance.yahoo.com/news/young-broke-scared-irs-millennial-152014893.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3599380","cauuid":"748b248f-4c9c-3be5-a420-f511c95de13b","link":"http://sports.yahoo.com/blogs/nba-ball-dont-lie/nba-threatens-lost-charlotte-all-star-game-over-nc-anti-trans-law-235244562.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3599366","cauuid":"4a67bf10-c821-39c3-b0ad-3934c423c254","link":"https://www.yahoo.com/katiecouric/how-motor-city-is-shifting-gears-192341852.html","type":"video","viewer_eligible":true,"viewer_fetch":true}]},{"id":"b4578bd7-ca70-3f7d-8b65-e3a701c3ac29","i13n":{"cpos":2,"cposy":6,"bpos":1,"pos":1,"ss_cid":"60dfcfe7-6edf-438d-9790-9da1cea32277","refcnt":2,"imgt":"ss","g":"b4578bd7-ca70-3f7d-8b65-e3a701c3ac29","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"http://observer.com/2016/03/bombshell-clinton-foundation-donors-flight-from-justice-aided-by-hillary-allies/","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"92822301-5d90-3fe9-937e-4ee7e4f24c87","link":"http://finance.yahoo.com/news/rudy-giuliani-says-hillary-clinton-184920233.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"f1170531-6bde-33e8-bfc4-2fcf77834448","link":"http://www.upi.com/Top_News/US/2016/03/24/Rudy-Guiliani-calls-Hillary-Clinton-founding-member-of-Islamic-State/9691458836387/","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"31815148360","i13n":{"cpos":3,"cposy":9,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31815148360","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=R9LIKCUGIS_7IHZOlDGJft7ds66AOPpbHr6VDp1I3AkI5ZKKNzdU1fFd.CdGxGjvPUFlkQNYRHHzaCA1iuovU2PSU64dl5dSom3Q5DNAGwBveCu20PSx72Y_I0RSPygSRgxxKHUL.5VqGMLOF5j30LLW8qZifASKd6X6EaY6FWLpK1eIDOaek8lCK5eS2Vor0otCdaUeGF87MgCwBTmMb8I9MPo0piW_6_6JmWdPVmYJ29OwRZUk3iluA4Sescfb5cSPu.CYnUmS0zsmN8YoffPyMwr3fprEruvAQYLIhd6jaAz1F89d1sNc5FNT4lFDf2hzTyENs0gD8TmhqlvQ7pQvAxNyIbOMN2DPYm3drAZcEP7T_z6FLTLCkBup9WJeINi.loCBHRF74btK7ZH4fcdW4.V30vU7HlW9fkngqgxRGFIuHDoPM3jcglt1fNSzR5K3Fmaczqw9f1.UrqDEI8Xq2Sm8T19oZTjXXIkGpjSkCTnqbB5ClTUideBguq5lNS8-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ff237962d-3d60-4fc4-9627-5445dad3618f%3Fad%3D1","type":"ad"},{"id":"cfce9891-804e-3e00-b0fb-f73ff3fb3ea6","i13n":{"cpos":4,"cposy":10,"bpos":1,"pos":1,"imgt":"ss","g":"cfce9891-804e-3e00-b0fb-f73ff3fb3ea6","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Politics"},"link":"http://mashable.com/2016/03/25/trump-bad-plls/","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"3c099d53-6258-3e0c-9057-f50cbe9daf63","i13n":{"cpos":5,"cposy":11,"bpos":1,"pos":1,"imgt":"ss","g":"3c099d53-6258-3e0c-9057-f50cbe9daf63","ct":1,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"U.S."},"link":"http://www.usatoday.com/story/news/nation-now/2016/03/24/indiana-woman-missing-decades-found-living-texas/82238858/","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"2a15c483-d121-3568-b78b-bcd85ae3dbc0","i13n":{"cpos":6,"cposy":12,"bpos":1,"pos":1,"subsec":1742,"imgt":"ss","g":"2a15c483-d121-3568-b78b-bcd85ae3dbc0","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Entertainment"},"link":"http://news.yahoo.com/alleged-ted-cruz-sex-scandal-133900795.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"9eece9ff-bbde-3c0c-a47e-b8a7d58106a5","i13n":{"cpos":7,"cposy":13,"bpos":1,"pos":1,"ss_cid":"8a2d2196-5f8c-4671-8d1f-c85818ea49f2","refcnt":2,"imgt":"ss","g":"9eece9ff-bbde-3c0c-a47e-b8a7d58106a5","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Sports"},"link":"http://www.sbnation.com/lookit/2016/3/25/11303122/kansas-garbage-time-score-maryland-frank-mason","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"cf4925a2-cb00-322f-a633-caf982c37828","link":"http://www.testudotimes.com/maryland-terps-basketball/2016/3/23/11290058/kansas-ncaa-tournament-game-2016-predictions","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"8bcc0aef-8123-3844-92e4-66b7257d2aa3","link":"http://sports.yahoo.com/news/top-seeded-kansas-tops-no-5-maryland-79-040639582--ncaab.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"31745180003","i13n":{"cpos":8,"cposy":16,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31745180003","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=85AriqcGIS8MZ79iHkje30.iY.jkqebt9LQXEc27M8ppPJAEdm9sBNFStHL2mYd5AQBSlJ_w.Qb_YnVCglus4Tii7dgHNgi2UU6jEPPlQTGHwFK8g0rKIk63IGwu1xjRPE8bkmV06lVlo.c8Q_iffroMDTIg2A5ISxwoF2xTXJYzrahVfml5z2dvKFeyb.M0bcFxW9x5ZC2kDPv0gMxOk1.7xc7.zo.v9sf5oD6UPPfnt986LuFvWC66wNuGqcwrKhiWVSGBw30.k5o09xhlnNDCXKq92ggdGRdJ.0yjQkXvDpBvC3lmBIz2WRyYqhTXKSt5n8YGQWKGU2MxAPa_az6KfXj7UnOcdZoEMpmy3vEzpIETM02fbGImi65.BtzgOuqbnfmAWKRk8s0A1SfW.GV3rlexuNoaM1dz5UD4LCnsIzxtRb.3mHKeiqjJbf1KlqsgM8uq5uY.qc._r0hw_KarbcPTJBtAk9U7Jv_EX13iKcrsf5B8dvS.ArlzgWO74qhD.7XlYqLc_5.n5E_IOV_.G_YBmatDJbG9NU4Jm2ey0t4tdarSiIAAzllWm81nUshXbUUknlOBZUEF3VkSidlvaQbh.whF6s.DX95m4GEXnkGSxNu4Gu33Tpw-%26lp=https%3A%2F%2Fwww.winamax.fr%2Flanding%2Flanding_leads.php%3Fldg%3Dgrilles%26banid%3D29181%26utm_source%3DYAHOO%26utm_medium%3DCPC%26utm_campaign%3DNATIVE_filrouge_grilles_vectoriel","type":"ad"},{"id":"44e687f8-1cf4-3493-999b-034c5d09f25d","i13n":{"cpos":9,"cposy":17,"bpos":1,"pos":1,"imgt":"ss","g":"44e687f8-1cf4-3493-999b-034c5d09f25d","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"News"},"link":"http://www.nytimes.com/2016/03/27/nyregion/glen-grays-the-mailman-cuffed-in-brooklyn.html?_r=0","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"510b3264-c0ab-3e9c-834a-c3584bb0f9e1","i13n":{"cpos":10,"cposy":18,"bpos":1,"pos":1,"ss_cid":"19ec3c70-0714-4193-b7be-1eac25be006a","refcnt":2,"imgt":"ss","g":"510b3264-c0ab-3e9c-834a-c3584bb0f9e1","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Sports"},"link":"http://www.yardbarker.com/college_basketball/articles/grayson_allen_brushes_aside_hug_from_dillon_brooks/s1_127_20549869","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"b0c5651d-b549-3254-b4e1-5381fa4098c7","link":"http://www.charlotteobserver.com/sports/college/article67895707.html","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"684a89d7-1d27-3936-80b1-3abb3a933593","link":"http://sports.yahoo.com/news/coach-k-lectured-dillon-brooks-010000952.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"f2f21ddf-3ec1-3ee1-a2fb-99b8d4f99cf5","i13n":{"cpos":11,"cposy":21,"bpos":1,"pos":1,"ss_cid":"77c1e3ee-c972-454c-9d04-9b9b226d701f","refcnt":2,"subsec":19,"imgt":"ss","g":"f2f21ddf-3ec1-3ee1-a2fb-99b8d4f99cf5","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Business"},"link":"http://finance.yahoo.com/news/car-everyone-raving-york-auto-122000719.html","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"ba2b71ee-652e-3e49-9a01-d4004d30938b","link":"http://www.usatoday.com/videos/money/cars/2016/03/24/82219158/","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"f54e1b76-350b-39f4-a77f-d268ac32300e","link":"https://www.yahoo.com/autos/2017-chevrolet-camaro-zl1-convertible-214500021.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"cc55924e-8763-304e-8368-b0fe11718aac","i13n":{"cpos":12,"cposy":24,"bpos":1,"pos":1,"ss_cid":"64c93bda-32bb-40b4-8383-4a3cd79fc328","refcnt":2,"imgt":"ss","g":"cc55924e-8763-304e-8368-b0fe11718aac","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Entertainment"},"link":"http://www.popsugar.com/entertainment/Justice-League-Set-Pictures-40670997","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"743ba47d-e391-32bd-8730-1c104f62c22a","link":"https://www.yahoo.com/tv/batman-v-superman-est-160m-211319900.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"01f60368-ef17-387f-967f-3e0d33c180c2","link":"https://www.yahoo.com/movies/batman-v-superman-cast-director-respond-negative-reviews-172058692.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]}],"fetched":12,"interest_data":{"WIKIID:Islamic_state":{"name":"Islamic State"},"WIKIID:Ash_Carter":{"name":"Defense Secretary Ash Carter"},"WIKIID:Iraq":{"name":"Iraq"},"WIKIID:The_Pentagon":{"name":"Pentagon"},"WIKIID:Joseph_Dunford":{"name":"Joseph Dunford"},"WIKIID:Syria":{"name":"Syria"},"WIKIID:Barack_Obama":{"name":"President Barack Obama"},"YCT:001000661":{"name":"Politics & Government"},"YCT:001000713":{"name":"Unrest, Conflicts & War"},"WIKIID:Hillary_Clinton":{"name":"Hillary Clinton"},"WIKIID:Clinton_Foundation":{"name":"Clinton Foundation"},"WIKIID:Allen_Stanford":{"name":"Allen Stanford"},"YCT:001000681":{"name":"Government"},"YCT:001000667":{"name":"Crime & Justice"},"YCT:001000780":{"name":"Society"},"WIKIID:Donald_Trump":{"name":"Donald Trump"},"YCT:001000671":{"name":"Elections"},"WIKIID:Indiana_State_Police":{"name":"Indiana State Police"},"YCT:001000288":{"name":"Relationships"},"WIKIID:Twitter":{"name":"Twitter"},"WIKIID:Ted_Cruz":{"name":"Ted Cruz"},"YCT:001000031":{"name":"Arts & Entertainment"},"YCT:001000075":{"name":"Media"},"YCT:001000085":{"name":"Social Media"},"YCT:001000001":{"name":"Sports & Recreation"},"WIKIID:Borough_president":{"name":"Borough president"},"WIKIID:Oregon":{"name":"Oregon"},"WIKIID:Grayson_Allen":{"name":"Grayson Allen"},"YCT:001000069":{"name":"Celebrities"},"WIKIID:Hyundai":{"name":"Hyundai"},"WIKIID:Concept_car":{"name":"Concept car"},"WIKIID:BMW":{"name":"BMW"},"YCT:001000992":{"name":"Transportation"},"YCT:001000993":{"name":"Autos"},"WIKIID:Justice_League":{"name":"Justice League"},"YCT:001000076":{"name":"Movies"}}}},"items":{"yui_module":"td-applet-stream-items-model-v2","yui_class":"TD.Applet.StreamItemsModel2"},"applet_model":{"yui_module":"td-applet-stream-appletmodel-v2","yui_class":"TD.Applet.StreamAppletModel2","config":{"ads":{"adchoices_text":true,"adchoices_url":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","ad_polices":true,"advt_text":false,"count":25,"contentType":"","fallback":0,"feedback":true,"frequency":4,"inline_video":true,"pu":"www.yahoo.com","se":4250754,"related_ct_se":"5454650","related_ct_single_ad":true,"related_dedupe_ads":true,"spaceid":"2023538075","sponsored_url":"http://help.yahoo.com/kb/index?page=content&y=PROD_FRONT&locale=en_US&id=SLN14553","start_index":1,"timeout":0,"type":"STRM,STRM_CONTENT,STRM_VIDEO","version":2,"endpoint":"","related_start_index":3},"category":"","js":{"attribution_pos":"bottom","click_context":false,"content_events":true,"filters":false,"full_content_event":false,"pluck_item_fields":"id,cauuid,payoffId,events,i13n,link,type,videoUuid,viewer_eligible,viewer_fetch,video_ad_beacons,video_ad_assets","pluck_today_fields":"image,title,summary,publisher,more_link_text","poll_interval":60000,"related_collapse":true,"related_content":true,"related_count":4,"restore_filter":false,"restore_state":false,"restore_state_storage":"history","show_read":true,"sticker":false,"sticker_toptarget":"","summary_pos":"left","xhr_ss_timeout":500,"stories_like_this":0,"login_url":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US&.src=fpctx&.done="},"linkSupplement":null,"onboarding":{"count":3,"debug":false,"pos":-1,"result":3,"topic":false},"pagination":true,"remoteConfig":{"endpoint":"/_td_remote","params":{"disableAppClass":true,"footer":false,"i13n":{"auto":false},"noAbbreviation":true,"rendermode":"hovercard"},"passLocdropCrumb":true,"type":"td-applet-weather"},"request_xhtrace":0,"signed_in":false,"ss_endpoint":"ga-hr.slingstone.yahoo.com@score/v9/homerun/en-US/unified/mega","ss_timeout":300,"template":{"main":"td-applet-stream-atomic:main","drawer":"td-applet-stream-atomic:drawer_desktop","drawer_action":"td-applet-stream-atomic:drawer_action","drawer_share":"td-applet-stream-atomic:drawer_share"},"timeout":800,"total":170,"ui":{"backfill_property_region":false,"big_click_target":true,"bleed":false,"bnb_enabled":true,"bnb_link_out":false,"cat_label_enabled":true,"comments":true,"comments_allow_supression":true,"comments_inline":true,"comments_inline_ranking":"highestRated","comments_offnet":false,"comments_editorial_suppression":true,"comments_request_count":5,"comments_update":true,"comments_update_maxidle":300000,"comments_update_monitoring":true,"comments_update_throttle":1750,"comments_writes_enabled":true,"followable":true,"followable_signedout":true,"dislike":false,"dollar_sign":true,"editorial_edition":{"label":"EDITION_DEFAULT","top":true,"shouldUpdateLVtimeInCookie":true},"enable_featured_ad_video_gap":true,"enable_hovercolor":true,"enable_stream_notify":false,"enrich_tweet":0,"enrichment":{"types":"","unique":false},"entity_max":9,"exclude_types":"","exclusive_type":"","fauxdal":true,"featured_align":"left","featured_delayed_defer":false,"featured_percent_width":45,"featured_width":460,"featured_height":258,"featured_width_portrait":200,"featured_height_portrait":200,"first_featured_treatment":true,"fixed_layout":false,"fptoday_thumbs_sparse":false,"headline_wrapper":true,"hq_ad_template":"featured_ad","i13n_key_tar_qa":false,"image_quality_override":true,"incremental_count":0,"incremental_delay":300,"incremental_history":100,"inline_filters":false,"inline_filters_article_min":10,"inline_filters_max":6,"inline_video":true,"item_template":"items","like":true,"limit_summary_height":true,"link_to_finance":false,"sticker_top":"94px","mms":false,"needtoknow_actions":true,"needtoknow_template":"filmstrip","needtoknow_storyline_min":4,"ntk_1_link_out":false,"ntk_2_link_out":false,"ntk_3_link_out":false,"ntk_4_link_out":false,"ntk_5_link_out":false,"ntk_allow_no_cauuid":true,"ntk_bypassA3c":false,"off_network_tab":false,"open_in_tab":false,"defaultDrawerAction":{"title":"ACTION_DEFAULT","description":"Content preferences","url":"https://settings.yahoo.com/interests"},"payoffDrawerActions":{"local":{"title":"ACTION_LOCAL","description":"More local news","urlPrefix":"https://news.yahoo.com/local/"},"finance":{"title":"ACTION_FINANCE","description":"Manage portfolios","url":"https://finance.yahoo.com/portfolios"},"sports":{"actionsEnabled":false,"attributionEnabled":false,"title":"ACTION_SPORTS","description":"Edit my teams","url":"https://sports.yahoo.com/myteams"}},"payoffs":"","payoffExpTime":24,"payoffFinanceArticleCount":2,"payoffInterval":6,"payoffInvalidTime":0.5,"payoffSportsLocalTeams":false,"payoffLocalArticleCount":3,"payoffLocalHeadliner":true,"payoffStartIndex":1,"pcsExclusions":false,"preview_enabled":false,"preview_lcp":true,"pubtime_format":"ONE_UNIT_ABBREVIATED","pubtime_maxage":3600,"ribbon_headers":false,"sfl":false,"sfl_get_started_string":"SFL_LINK","scrollbuffer":900,"scroll_node_selector":"body","show_tooltips":true,"smart_crop":true,"stateful_subtle_hint":true,"storyline_cap_image":2,"storyline_cap_noimage":3,"storyline_count":2,"storyline_elapsed_time":true,"storyline_elapsed_time_threshold":720,"storyline_enabled":true,"storyline_items_to_show":2,"storyline_multi_image":true,"storyline_multi_image_roundup":true,"storyline_newsy_disabled":false,"storyline_upsell_count":0,"storyline_upsell_image":"img:190x107|2|80","storyline_upsell_index":3,"stream_actions":true,"stream_actions_minimal":true,"stream_actions_reblog":true,"stream_actions_share":false,"stream_actions_share_panel":true,"stream_actions_likable":true,"stream_actions_tumblr":true,"stream_debug":false,"stream_payoff_actions":false,"stream_item_maxwidth":"none","stream_item_image_fallback":"https://s.yimg.com/dh/ap/default/151015/stream-gradient-230x130.png","stream_item_large_image_fallback":"https://s.yimg.com/dh/ap/default/150902/stream-gradient.png","stream_single_item_image_fallback":"https://s.yimg.com/dh/ap/default/150903/stream-gradient-single-3.png","summary":true,"summary_length":160,"template_label":"js-stream-dense","template_suffix":"","today_count":5,"today_snippet_count":5,"fc_related_minCount":2,"today_featured_image_tag":"pc:size=medium","tap_for_summary":false,"templates":{"all":{"batch_max":20,"gap":0,"max":170,"start":1,"batch":2,"total":2,"last":7},"featured":{"batch_max":20,"gap":0,"max":170,"portrait":false,"start":0,"batch":0,"total":0},"featured_ad":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":2,"total":2,"last":7},"inline_video":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":0,"total":0}},"thumbnail":true,"thumbnail_align":"right","thumbnail_hover":true,"thumbnail_size":160,"thumbnail_specs":{"featured":"img:190x107|2|80","featured_square":"img:190x190|2|80","inline_video":"img:190x107|2|80","items":"img:190x107|2|80","items_large":"img:190x190|2|80","storyline_square":"img:70x70|2|90","related":"img:165x120|2|80","featured_roundup":"img:702x274|1|95","roundup_wide":"img:170x80|2|80"},"toJpg":true,"truncate_summary":false,"tweet_action":true,"uis_inferred_finance":false,"ult_count":10,"ult_host":"hsrd.yahoo.com","ult_links":false,"use_ss_roundup_slot":true,"view_article_button":false,"viewer":true,"viewer_include_all":true,"viewer_off_network":false,"viewer_reblog":false,"viewer_action":true,"visit_state_threshold":1800000,"preview":0,"related_host":"","stream_payoff_v2":1,"stream_payoff_vizify":0,"stream_rc4":0,"show_jump_to_historical_items":0,"item_templates":["items"]},"weather":false,"woeid":7153327,"tz":"Europe/Paris","xcc":"fr"},"settings":{"size":9,"woeid":7153327},"models":["stream","items","related"]},"interest":{"yui_module":"td-applet-interest-model-v2","yui_class":"TD.Applet.InterestModel2"},"comments":{"yui_module":"td-applet-comments-model-v2","yui_class":"TD.Applet.CommentsModel2"},"related":{"yui_module":"td-applet-stream-related-model-atomic","yui_class":"TD.Applet.StreamRelatedModelAtomic"}},"views":{"main":{"yui_module":"td-applet-stream-mainview-v2","yui_class":"TD.Applet.StreamMainView2"},"drawer":{"yui_module":"stream-actiondrawer-v2"}},"templates":{"main":{"yui_module":"td-applet-stream-atomic-templates-main","template_name":"td-applet-stream-atomic-templates-main"},"related":{"yui_module":"td-applet-stream-atomic-templates-related","template_name":"td-applet-stream-atomic-templates-related"},"drawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_desktop","template_name":"td-applet-stream-atomic-templates-drawer_desktop"},"removedItem":{"yui_module":"td-applet-stream-atomic-templates-removeditem","template_name":"td-applet-stream-atomic-templates-removeditem"},"drawerAction":{"yui_module":"td-applet-stream-atomic-templates-drawer_action","template_name":"td-applet-stream-atomic-templates-drawer_action"},"drawerSharePanel":{"yui_module":"td-applet-stream-atomic-templates-drawer_share","template_name":"td-applet-stream-atomic-templates-drawer_share"},"breakingNews":{"yui_module":"td-applet-stream-atomic-templates-breaking_news","template_name":"td-applet-stream-atomic-templates-breaking_news"},"flyoutDrawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_flyout","template_name":"td-applet-stream-atomic-templates-drawer_flyout"},"items":{"yui_module":"td-applet-stream-atomic-templates-items","template_name":"td-applet-stream-atomic-templates-items"},"item_default":{"yui_module":"td-applet-stream-atomic-templates-item-default","template_name":"td-applet-stream-atomic-templates-item-default"},"item_default_clusters":{"yui_module":"td-applet-stream-atomic-templates-item-default_clusters","template_name":"td-applet-stream-atomic-templates-item-default_clusters"},"item_ad":{"yui_module":"td-applet-stream-atomic-templates-item-ad","template_name":"td-applet-stream-atomic-templates-item-ad"},"item_ad_dislike":{"yui_module":"td-applet-stream-atomic-templates-item-ad_dislike","template_name":"td-applet-stream-atomic-templates-item-ad_dislike"},"item_featured_ad":{"yui_module":"td-applet-stream-atomic-templates-item-featured_ad","template_name":"td-applet-stream-atomic-templates-item-featured_ad"},"item_featured":{"yui_module":"td-applet-stream-atomic-templates-item-featured","template_name":"td-applet-stream-atomic-templates-item-featured"},"item_inline_video":{"yui_module":"td-applet-stream-atomic-templates-item-inline_video","template_name":"td-applet-stream-atomic-templates-item-inline_video"}},"i18n":{"AD_FDB_HEADING":"Why don't you like this ad?","AD_FDB_TELL_US":"Tell us why","AD_FDB_THANKYOU":"Thank you for your feedback. We will remove this and make the changes needed.","AD_FDB1":"It is offensive to me","AD_FDB2":"It is not relevant to me","AD_FDB3":"I keep seeing this","AD_FDB4":"Something else","ABCLOGO":"ABC","ACTION_MORE_LIKE_THIS":"Got it! Tell us more about what you like:","ACTION_FEWER_LIKE_THIS":"Got it! Tell us more about what you dislike:","ACTION_DEFAULT":"Content preferences Â»","ACTION_SPORTS":"Edit my teams Â»","ACTION_FINANCE":"Manage portfolios Â»","ACTION_LOCAL":"More local news Â»","ACTION_STORY_REMOVED":"Story removed","ACTION_SEE_LESS":"Okay. You'll see fewer like this.","ACTION_SEE_MORE":"Great! You'll see more like this.","ACTION_SEE_NO_MORE":"This item has been removed from your stream.","ACTION_SIGN_IN_TO_DISLIKE":"{linkstart}Sign-in{linkend} and we'll show you less like this in the future.","ACTION_SIGN_IN_TO_DISLIKE_TUMBLR":"{linkstart}Sign-in{linkend} to dislike","ACTION_SIGN_IN_TO_LIKE":"{linkstart}Sign-in{linkend} and we'll show you more like this in the future.","ACTION_SIGN_IN_TO_LIKE_SIMPLE":"Sign in to like","ACTION_SIGN_IN_TO_LIKE_TUMBLR":"{linkstart}Sign-in{linkend} to like","ACTION_SIGN_IN_TO_PERSONALIZE":"{linkstart}Sign-in{linkend} to personalize!","ACTION_SIGN_IN_TO_SAVE":"{linkstart}Sign-in{linkend} to save this story to read later.","ACTION_SIGN_IN_TO_SAVE_SHORT":"Sign in to save","ACTION_TELL_LIKE":"Tell us more about what you like:","ACTION_TELL_DISLIKE":"Tell us more about what you dislike:","ACTION_MORE_LESS":"Show more or less of","ACTION_OPTIONS":"Options","ACTION_REMOVE":"Remove from my stream","ACTION_STORIES_LIKE_THIS":"Stories like this","ACTION_OPEN":"Open","ACTION_CLOSE":"Close","ADCHOICES":"AdChoices","ADVERTISEMENT":"Ad","ALL_ARTICLES":"All Articles","ALL_CELEBRITY":"All Celebrity News","ALL_FINANCE":"All Finance","ALL_MOVIES":"All Movies News","ALL_MUSIC":"All Music News","ALL_NEWS":"All News","ALL_SPORTS":"All Sports","ALL_STORIES":"All Stories","ALL_TRAVEL":"All Travel Ideas","ALL_TV":"All TV News","AROUND_THE_WEB":"Around The Web","ALSO_LIKE":"You may also like","ASTRO_GO_TO":"Go to Horoscopes Â»","ASTRO_NAME_AQUARIUS":"Aquarius","ASTRO_NAME_ARIES":"Aries","ASTRO_NAME_CANCER":"Cancer","ASTRO_NAME_CAPRICORN":"Capricorn","ASTRO_NAME_GEMINI":"Gemini","ASTRO_NAME_LEO":"Leo","ASTRO_NAME_LIBRA":"Libra","ASTRO_NAME_PISCES":"Pisces","ASTRO_NAME_SAGITTARIUS":"Sagittarius","ASTRO_NAME_SCORPIO":"Scorpio","ASTRO_NAME_TAURUS":"Taurus","ASTRO_NAME_VIRGO":"Virgo","ASTRO_TITLE":"Today's overview","AUCTION":"Action","AUTHOR_AT_PUBLISHER":"{author} at {publisher}","BREAKING_NEWS":"BREAKING NEWS","CNBCLOGO":"","COMMENT_ARTICLE_SIGN_IN":"Sign in to perform this action.","COMMENTS":"Comments","COMMENTS_ZERO":"Start the conversation","CONTENT_PREF":"Content preferences","DISLIKE":"Dislike","DONE":"Done","DYNAMIC_FILTERS":"You Might Like","EDITION_DEFAULT":"Need To Know","FEATURED_MOVIE":"Featured Movie","FLAGGED_COMMENT":"Flagged as inappropriate. Thanks for your feedback.","FOLLOW":"Follow","FOLLOW_GAME":"Follow Game","FOLLOWING":"Following {name}","FOLLOWING_STORY":"Following","FOLLOW_UPDATES":"Follow to receive updates on","FULL_HOROSCOPE":"Full Horoscope","GAME_PERIOD:baseball":"Innings","GAME_PERIOD:football":"Quarters","GAME_PERIOD_1":"1st","GAME_PERIOD_2":"2nd","GAME_PERIOD_3":"3rd","GAME_PERIOD_4":"4th","GAME_COVERAGE":"See Full Coverage","GAME_FINAL":"Final","GAME_RECAP":"Game Recap","GAME_TITLE":"{away_team} vs. {home_team}","HALFTIME":"Halftime","HOROSCOPE":"Horoscope","IN_THEATERS_NOW":"In Theaters Now","IN_THEATERS_TODAY":"In Theaters Today","INSTALL_APP":"Install now","IN_WATCHLIST":"In watchlist","IS_IN_FAVORITES":"is in your favorites","JUMP_TO_HISTORICAL_ITEMS":"Go to where you left off","LATEST_NEWS":"Latest News","LESS":"Less","LIKABLE_ARTICLE_SIGN_IN":"Sign in to save your preferences.","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","LIVE":"Live","LIVE_GAME":"Live","LOAD_MORE":"Load more stories","LOCAL":"Local","LOCAL_BLOWING_SNOW":"Blowing Snow","LOCAL_BLUSTERY":"Blustery","LOCAL_CELSIUS":"Â°C","LOCAL_CLEAR":"Clear","LOCAL_CLOUDY":"Cloudy","LOCAL_COLD":"Cold","LOCAL_DRIZZLE":"Drizzle","LOCAL_DUST":"Dust","LOCAL_FAIR":"Fair","LOCAL_FOGGY":"Foggy","LOCAL_FAHRENHEIT":"Â°F","LOCAL_FREEZING_DRIZZLE":"Freezing Drizzle","LOCAL_FREEZING_RAIN":"Freezing Rain","LOCAL_GO_TO":"Go to Weather Â»","LOCAL_HAIL":"Hail","LOCAL_HAZE":"Haze","LOCAL_HEAVY_SNOW":"Heavy Snow","LOCAL_HOT":"Hot","LOCAL_HURRICANE":"Hurricane","LOCAL_ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","LOCAL_ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LOCAL_LIGHT_SNOW_SHOWERS":"Light Snow Showers","LOCAL_MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","LOCAL_MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","LOCAL_MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","LOCAL_MOSTLY_CLOUDY":"Mostly Cloudy","LOCAL_NEWS":"Local news","LOCAL_NO_STORIES":"Sorry, there are no news articles related to this location. Try setting your location to the nearest large city for more stories.","LOCAL_PARTLY_CLOUDY":"Partly Cloudy","LOCAL_SCATTERED_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_THUNDERSTORMS":"Scattered Thunderstorms","LOCAL_SEVERE_THUNDERSTORMS":"Severe Thunderstorms","LOCAL_SHOWERS":"Showers","LOCAL_SLEET":"Sleet","LOCAL_SMOKY":"Smoky","LOCAL_SNOW":"Snow","LOCAL_SNOW_FLURRIES":"Snow Flurries","LOCAL_SNOW_SHOWERS":"Snow Showers","LOCAL_SUNNY":"Sunny","LOCAL_THUNDERSHOWERS":"Thundershowers","LOCAL_THUNDERSTORMS":"Thunderstorms","LOCAL_TITLE":"{city} News","LOCAL_TORNADO":"Tornado","LOCAL_TROPICAL_STORM":"Tropical Storm","LOCAL_UNKNOWN":"Unknown Conditions","LOCAL_WINDY":"Windy","MIDFIELD":"midfield","MLB_BALLS":"Balls","MLB_ERRORS":"Errors","MLB_HITS":"Hits","MLB_OUTS":"Outs","MLB_RUNS":"Runs","MLB_STRIKES":"Strikes","MORE":"More","MORE_FROM_ROUNDUP":"More from this Roundup","MORE_FROM_TOPIC":"More from {topic} news","MYQUOTES_ADD_MORE":"Add more investments to your {0}Portfolio{1} to stay up to date whenever you visit Yahoo Finance.","MYQUOTES_LOGIN":"{0}Sign-in{1} to see the latest news from the investments in your portfolio.","MYTEAMS_ADD_TEAMS":"{0}Add your favorite teams{1} to start getting news about them today.","MYTEAMS_LOGIN":"{0}Sign-in{1} to get news for your favorite teams.","MYTEAMS_NO_CONTENT":"We can't find recent news for your teams. {0}Edit your teams{1} or visit the {2}All Sports news{3}","MYSAVES":"My Saves","NEW_SINCE_YOUR_LAST_VISIT":"New since your last visit","NEW_STORIES":"New for you","NEW_STORIES_COUNT":"View {new_item_count} new {new_item_count, plural, one {story} other {stories}}","NEXT":"Next","NFL_PROGRESS":"on {field_side} {yard_line}","NO_STORIES_HEADER":"We couldn't find any new stories for you.","NO_STORIES_BODY":"Please check back later or {0}try again{1}","OFF":"OFF","OFFNETWORK":"Read this on {0}","ONBOARDING_CONFIRMATION_INITIAL":"Got it! Almost there","ONBOARDING_CONFIRMATION_ONE":"Got it! One more time","ONBOARDING_CONFIRMATION_ZERO":"Got it! Youâre done","ONBOARDING_TITLE1_FINAL":"Here are some stories based on your preferences","ONBOARDING_TITLE1_INITIAL":"Which would you rather read?","ONBOARDING_TITLE1_ONE":"Now which do you prefer?","ONBOARDING_TITLE1_ZERO":"Awesome! What is your final pick?","OPENS_TODAY":"Opens Today","PORTFOLIO":"Go to your portfolio","PORTFOLIO_GAINERS":"Gainers","PORTFOLIO_LOSERS":"Losers","PORTFOLIO_MARKET_CLOSED":"Market close","PLAY":"Play","PLAY_VIDEO":"Play Video","PLAYING":"playing","PLAYING_NEAR_YOU":"Playing near you","PREVIEW_GAME":"Preview Game","PREVIOUS":"Previous","PREVIOUSLY_VIEWED":"From your last visit","PROGRESS":"{possession_team} {down} & {to_go} at {field_side} {yard_line}","READ_MORE":"Read More","REBLOG":"Reblog on Tumblr","REMOVE":"Remove","SAVE":"Save","SCORE":"Score!","SCORE_ATTRIBUTION":"Click here to track the {display_name} &rarr;","SEE_ALL_STORIES":"See all stories Â»","SEE_DETAILS":"See details Â»","SEE_MORE_STORIES":"See more stories Â»","SEE_FULL_BOXSCORE":"See full boxscore Â»","SFL_HEADER":"Hi {0}, you have no saves. Here's how to get started:","SFL_LINK":"Get started now on the {0}Yahoo Homepage{1}.","SFL_LINK_ATT":"Get started now on the {0}att.net Homepage{1}.","SFL_LINK_FRONTIER":"Get started now on the {0}Frontier Yahoo Homepage{1}.","SFL_LINK_ROGERS":"Get started now on the {0}Rogers Yahoo Homepage{1}.","SFL_LINK_VERIZON":"Get started now on the {0}Verizon Yahoo Homepage{1}.","SFL_STEP_ONE":"Step 1","SFL_STEP_TWO":"Step 2","SFL_TITLE_ONE":"Click on {0} in the stream and on articles across Yahoo to save stories for later.","SFL_TITLE_TWO":"Access your saves from the profile menu {0} on desktop & tablet or menu {1} on your smartphone.","SHARE_THIS":"SHARE THIS","SHOPPING":"Shopping","SIGN_IN":"Sign in!","SIGN_IN_2":"Sign in","SIGN_IN_FOR_MORE":"Looking for past stories?","SIGN_IN_TO_SEE_MORE_TEAMS":" to save your preferences. You'll see more of this team next time you visit.","SLIDESHOW_COUNT":"({0} photos)","SLIDESHOW_PREVIEW_COUNT":"1 of {0}","SPONSORED":"Sponsored","STATUS":"{time} {period}","STORE":"Store","STORIES":"Stories:","STORIES_ABOUT":"Stories about:","STORYLINE_NEXT":"Next","STORYLINE_PREVIOUS":"Previous","STORYLINE_UPSELL":"Follow these developing stories to get updates when you come back to Yahoo","SYNOPSIS":"Synopsis","TIME_AGO":"ago","TIME_DAY":"day","TIME_H":"h","TIME_M":"m","TITLE":"Recommended for You","TITLE_LOCAL":"{city} News","TO":"to","TWITTER_REPLY":"Reply","TWITTER_RETWEET":"Retweet","TWITTER_FAVORITE":"Favorite","UNFOLLOW":"Unfollow","UNDO":"Undo","UPCOMING_GAME":"Today at {game_time}","UP_OVER":"Up over","DOWN_OVER":"Down over","VIEW":"View","VIEW_ALL":"View All","VIEW_SLIDESHOW":"View Slideshow","VIEW_FULL_ARTICLE":"VIEW FULL ARTICLE","VIEW_FULL_QUOTE":"View full quote Â»","YAHOO_MOVIES":"Yahoo Movies","YAHOO_ORIGINALS":"Yahoo Originals","YCT:001000931":"Technology","YCUSTOM:COMMERCE":"Commerce","YCUSTOM:MYQUOTES":"My Quotes","YCUSTOM:MYTEAMS":"My Teams","YLABEL:autos":"Autos","YLABEL:business":"Business","YLABEL:beauty":"Beauty","YLABEL:celebrity":"Celebrity","YLABEL:diy":"DIY","YLABEL:entertainment":"entertainment","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:lifestyle":"Lifestyle","YLABEL:makers":"Projects","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:politics":"Politics","YLABEL:science":"science","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:technology":"Technology","YLABEL:travel":"Travel","YLABEL:trending":"Trending","YLABEL:tv":"TV","YLABEL:us":"US","YLABEL:world":"World","YPROP:FINANCE":"Business","YPROP:TOPSTORIES":"All Stories","YPROP:LOCAL":"Local","YPROP:NEWS":"News","YPROP:OMG":"Entertainment","YPROP:SCIENCE":"Science","YPROP:SPORTS":"Sports","YPROV:ABCNEWS":"News","YPROV:ap.org":"AP","YPROV:CNBC":"CNBC","YPROV:NBCSPORT":"NBC Sports","YPROV:reuters.com":"Reuters","YPROV:ROLLINGSTONES":"Rolling Stone","YPROV:us.sports.yahoo.com":"Experts","YTYPE:VIDEO":"Video","YPROP:STYLE":"Style","YMAG:food":"Food","YMAG:tech":"Tech","YMAG:travel":"Travel"},"i13n":{"pv":2,"pp":{"ccode_st":"mega_global_ranking_hlv2_up_based"}},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-trending-atomic":{"base":"/sy/os/mit/td/td-applet-trending-atomic-0.0.50/","root":"os/mit/td/td-applet-trending-atomic-0.0.50/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_32209491"] = {"applet_type":"td-applet-trending-atomic","views":{"main":{"yui_module":"td-applet-trending-atomic-mainview","yui_class":"TD.Applet.TrendingAtomicMainView","config":{"clickableTitles":true,"rotationEnabled":true,"rotationInterval":10000,"giftsEnabled":true}}},"templates":{"main":{"yui_module":"td-applet-trending-atomic-templates-main","template_name":"td-applet-trending-atomic-templates-main"}},"i18n":{"GIFTS_TITLE":"Easter Searches","TITLE":"Trending","TRENDING_NEWS":"Trending News","TRENDING_NOW":"Trending Now"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-weather-atomic":{"base":"/sy/os/mit/td/td-applet-weather-atomic-0.0.25/","root":"os/mit/td/td-applet-weather-atomic-0.0.25/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_63794"] = {"applet_type":"td-applet-weather-atomic","models":{"applet_model":{"yui_module":"td-applet-weather-atomic-appletmodel","yui_class":"TD.Applet.WeatherAppletModel","models":["weather"],"data":{"i13n":{"sec":"app-wea","bucket":"201"},"current":7153327,"favorite":null,"useplus":false},"user_settings":{"unit":"f","curloc":"7153327"},"config":{"moreinfo":{"link":"http://www.weather.com/?par=yahoonews","img":"http://l.yimg.com/dh/ap/default/130915/wcl1.gif"},"enableFav":false,"enableWeatherYQLP":true,"baseLink":"https://weather.yahoo.com","showWidgetLink":true,"urlGeneratorRules":{"enabled":false,"url_format_local":"","url_format_international":"","url_format_local_ajax":"","local_country_code":[]},"daysLimit":4,"fixed_layout":false}},"weather":{"yui_module":"td-applet-weather-atomic-model","yui_class":"TD.Applet.WeatherModel","data":{"weatherDataList":[{"location":{"woeid":"7153327","city":"Poitou-Charentes","country":"FR"},"link":{"href":"https://weather.yahoo.com/fr/state/poitou-charentes-7153327/"},"timezone":"CET","woeid":"7153327","current":{"temp":{"unit":"f","now":53},"condition":{"description":"Cloudy","code":26,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/cloudy_day_night.png"}}}},"forecast":{"day":[{"woeid":7153327,"condition":{"description":" Scattered Thunderstorms","code":39,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png"}}},"timezone":"Europe/Paris","temp":{"high":56,"low":45,"unit":"f"},"label":"Today","day_of_week":"5"},{"woeid":7153327,"condition":{"description":" Scattered Thunderstorms","code":39,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png"}}},"timezone":"Europe/Paris","temp":{"high":65,"low":44,"unit":"f"},"label":"Sat","day_of_week":"6"},{"woeid":7153327,"condition":{"description":" Scattered Thunderstorms","code":39,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png"}}},"timezone":"Europe/Paris","temp":{"high":53,"low":44,"unit":"f"},"label":"Sun","day_of_week":"0"},{"woeid":7153327,"condition":{"description":" Scattered Thunderstorms","code":39,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png"}}},"timezone":"Europe/Paris","temp":{"high":53,"low":46,"unit":"f"},"label":"Mon","day_of_week":"1"}]},"accordianState":"e"}],"currentLoc":{"woeid":"7153327","city":"Poitou-Charentes","country":"FR"}}}},"views":{"main":{"yui_module":"td-applet-weather-atomic-mainview","yui_class":"TD.Applet.WeatherMainView"},"header":{"yui_module":"td-applet-weather-atomic-headerview","yui_class":"TD.Applet.WeatherHeaderView","config":{}}},"templates":{"main":{"yui_module":"td-applet-weather-atomic-templates-main","template_name":"td-applet-weather-atomic-templates-main"}},"i18n":{"CURRENT_LOCATION":"Current Location","WEATHER":"Weather","HIGH":"High","LOW":"Low","TODAY":"Today","TOMORROW":"Tomorrow","SUNDAY":"Sunday","MONDAY":"Monday","TUESDAY":"Tuesday","WEDNESDAY":"Wednesday","THURSDAY":"Thursday","FRIDAY":"Friday","SATURDAY":"Saturday","ABBR_SUNDAY":"Sun","ABBR_MONDAY":"Mon","ABBR_TUESDAY":"Tue","ABBR_WEDNESDAY":"Wed","ABBR_THURSDAY":"Thu","ABBR_FRIDAY":"Fri","ABBR_SATURDAY":"Sat","BLOWING_SNOW":"Blowing Snow","BLUSTERY":"Blustery","CLEAR":"Clear","CLOUDY":"Cloudy","COLD":"Cold","DRIZZLE":"Drizzle","DUST":"Dust","FAIR":"Fair","FREEZING_DRIZZLE":"Freezing Drizzle","FREEZING_RAIN":"Freezing Rain","FOGGY":"Foggy","FORECAST":"Forecast","FULL_FORECAST":"Full forecast","HAIL":"Hail","HAZE":"Haze","HEAVY_SNOW":"Heavy Snow","HOT":"Hot","HURRICANE":"Hurricane","ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LIGHT_SNOW_SHOWERS":"Light Snow Showers","MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","MIXED_RAIN_AND_SLEET":"Mixed Rain and Sleet","MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","MOSTLY_CLOUDY":"Mostly Cloudy","PARTLY_CLOUDY":"Partly Cloudy","SCATTERED_SHOWERS":"Scattered Showers","SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","SCATTERED_THUNDERSTORMS":" Scattered Thunderstorms","SEARCH_CITY":"Kindly search your city","SEVERE_THUNDERSTORMS":"Severe Thunderstorms","SHOWERS":"Showers","SLEET":"Sleet","SMOKY":"Smoky","SNOW":"Snow","SNOW_FLURRIES":"Snow Flurries","SNOW_SHOWERS":"Snow Showers","SUNNY":"Sunny","THUNDERSHOWERS":"Thundershowers","THUNDERSTORMS":"Thunderstorms","TORNADO":"Tornado","TROPICAL_STORM":"Tropical Storm","WINDY":"Windy","WEATHER_CHICLET_ERROR":"Visit {linkstart}Yahoo Weather{linkend} for a detailed forecast.","GOTO_WEATHER":"See more Â»","NO_WEATHER_MESSAGE":"Visit {linkstart}Yahoo Weather{linkend} for the latest forecast or {detectstart}click here{detectend} to select your precise location.","TEN_DAY":"10 day"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-scores-atomic":{"base":"https://s.yimg.com/os/mit/td/td-applet-scores-atomic-1.0.10/","root":"os/mit/td/td-applet-scores-atomic-1.0.10/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_63796"] = {"applet_type":"td-applet-scores-atomic","models":{"scores":{"yui_module":"td-scores-atomic-modellist","yui_class":"TD.Scores.ModelList","data":[{"gameid":"ncaab.g.201603250618","comet_channel":"/sports/games/ncaab.g.201603250618","start_time":"Fri, 25 Mar 2016 23:10:00 +0000","start_ts":1458947400000,"last_update":"2016-03-25 04:08:37","lastUpdated":"2016-03-25 04:08:37","lastUpdatedTs":"1458904117","status":{"type":"pregame","label":"7:10 pm ET","link":{"href":"https://sports.yahoo.com/ncaab/iowa-state-cyclones-virginia-cavaliers-201603250618/"}},"league":{"id":"ncaab","abbr":"NCAAB","link":{"href":"https://sports.yahoo.com/college-basketball/"}},"home_team":{"id":"ncaab.vaf","sportsid":"ncaab.t.618","first_name":"Virginia","last_name":"Cavaliers","display_name":"Virginia","abbr":"UVA","league_rank":"1","link":{"href":"https://sports.yahoo.com/ncaab/teams/vaf/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/vaf.png"}}},"away_team":{"id":"ncaab.iao","sportsid":"ncaab.t.277","first_name":"Iowa State","last_name":"Cyclones","display_name":"Iowa St.","abbr":"IA ST","league_rank":"4","link":{"href":"https://sports.yahoo.com/ncaab/teams/iao/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/dh/ap/default/151102/IowaState_70x70.png"}}},"seq_num":"1","seq":"1","tv_coverage":{"channels":["CBS"]},"fav":"false","region":"Midwest","relativeTime":"in 6 hours","startDay":"Mar 25","absoluteTime":"3/25 5:38 PM UTC","icon":"IconScoresNcaab"},{"gameid":"ncaab.g.201603250423","comet_channel":"/sports/games/ncaab.g.201603250423","start_time":"Fri, 25 Mar 2016 23:27:00 +0000","start_ts":1458948420000,"last_update":"2016-03-25 04:08:37","lastUpdated":"2016-03-25 04:08:37","lastUpdatedTs":"1458904117","status":{"type":"pregame","label":"7:27 pm ET","link":{"href":"https://sports.yahoo.com/ncaab/wisconsin-badgers-notre-dame-fighting-irish-201603250423/"}},"league":{"id":"ncaab","abbr":"NCAAB","link":{"href":"https://sports.yahoo.com/college-basketball/"}},"home_team":{"id":"ncaab.nbf","sportsid":"ncaab.t.423","first_name":"Notre Dame","last_name":"Fighting Irish","display_name":"Notre Dame","abbr":"ND","league_rank":"6","link":{"href":"https://sports.yahoo.com/ncaab/teams/nbf/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/nbf.png"}}},"away_team":{"id":"ncaab.wbg","sportsid":"ncaab.t.657","first_name":"Wisconsin","last_name":"Badgers","display_name":"Wisconsin","abbr":"WIS","league_rank":"7","link":{"href":"https://sports.yahoo.com/ncaab/teams/wbg/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/wbg.png"}}},"seq_num":"1","seq":"1","tv_coverage":{"channels":["TBS"]},"fav":"false","region":"East","relativeTime":"in 6 hours","startDay":"Mar 25","absoluteTime":"3/25 5:38 PM UTC","icon":"IconScoresNcaab"},{"gameid":"ncaab.g.201603250553","comet_channel":"/sports/games/ncaab.g.201603250553","start_time":"Sat, 26 Mar 2016 01:40:00 +0000","start_ts":1458956400000,"last_update":"2016-03-25 04:08:37","lastUpdated":"2016-03-25 04:08:37","lastUpdatedTs":"1458904117","status":{"type":"pregame","label":"9:40 pm ET","link":{"href":"https://sports.yahoo.com/ncaab/gonzaga-bulldogs-syracuse-orange-201603250553/"}},"league":{"id":"ncaab","abbr":"NCAAB","link":{"href":"https://sports.yahoo.com/college-basketball/"}},"home_team":{"id":"ncaab.sci","sportsid":"ncaab.t.553","first_name":"Syracuse","last_name":"Orange","display_name":"Syracuse","abbr":"SYRA","league_rank":"10","link":{"href":"https://sports.yahoo.com/ncaab/teams/sci/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/sci.png"}}},"away_team":{"id":"ncaab.gaj","sportsid":"ncaab.t.233","first_name":"Gonzaga","last_name":"Bulldogs","display_name":"Gonzaga","abbr":"GONZ","league_rank":"11","link":{"href":"https://sports.yahoo.com/ncaab/teams/gaj/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/gaj.png"}}},"seq_num":"1","seq":"1","tv_coverage":{"channels":["CBS"]},"fav":"false","region":"Midwest","relativeTime":"in 8 hours","startDay":"Mar 25","absoluteTime":"3/25 5:38 PM UTC","icon":"IconScoresNcaab"},{"gameid":"ncaab.g.201603250413","comet_channel":"/sports/games/ncaab.g.201603250413","start_time":"Sat, 26 Mar 2016 01:57:00 +0000","start_ts":1458957420000,"last_update":"2016-03-25 04:08:37","lastUpdated":"2016-03-25 04:08:37","lastUpdatedTs":"1458904117","status":{"type":"pregame","label":"9:57 pm ET","link":{"href":"https://sports.yahoo.com/ncaab/indiana-hoosiers-north-carolina-tar-heels-201603250413/"}},"league":{"id":"ncaab","abbr":"NCAAB","link":{"href":"https://sports.yahoo.com/college-basketball/"}},"home_team":{"id":"ncaab.nav","sportsid":"ncaab.t.413","first_name":"North Carolina","last_name":"Tar Heels","display_name":"N. Carolina","abbr":"NC","league_rank":"1","link":{"href":"https://sports.yahoo.com/ncaab/teams/nav/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://sp.yimg.com/j/assets/ipt/north-carolina_70.png"}}},"away_team":{"id":"ncaab.iai","sportsid":"ncaab.t.271","first_name":"Indiana","last_name":"Hoosiers","display_name":"Indiana","abbr":"IND","league_rank":"5","link":{"href":"https://sports.yahoo.com/ncaab/teams/iai/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/i/us/sp/v/ncaab/teams/83/70x70/iai.png"}}},"seq_num":"1","seq":"1","tv_coverage":{"channels":["TBS"]},"fav":"false","region":"East","relativeTime":"in 8 hours","startDay":"Mar 25","absoluteTime":"3/25 5:38 PM UTC","icon":"IconScoresNcaab"}],"config":{"enablePipe":true}},"applet_model":{"settings":{"league":"ncaab","count":6,"lang":"en-US","useSportacularLogos":true,"useWhiteSportacularLogos":true,"rendermode":"scorethin","timeout":300,"rapidStr":{"tabRapidStr":"t1:a4;t2:scrbrd;sec:scrbrd;elm:tab;elmt:day;itc:1;","dropdownRapidStr":"t1:a4;t2:scrbrd;sec:scrbrd;elm:itm;elmt:cat;itc:1;","gameRapidStr":"t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;","moreRapidStr":"t1:a4;t2:scrbrd;sec:scrbrd;elm:itm;elmt:mr;itc:0;"},"showTennis":false},"config":{"sizes":[3,4,5,6,7,8,9,10],"scores_links":{"trending":"http://sports.yahoo.com/","my":"http://sports.yahoo.com/","mlb":"http://sports.yahoo.com/mlb/scoreboard","nba":"http://sports.yahoo.com/nba/scoreboard","ncaab":"http://sports.yahoo.com/college-basketball/scoreboard/","ncaaf":"http://sports.yahoo.com/college-football/scoreboard/","nfl":"http://sports.yahoo.com/nfl/scoreboard","nhl":"http://sports.yahoo.com/nhl/scoreboard","tennis":"http://sports.yahoo.com/tennis/scoreboard","soccer.l.fbgb":"http://sports.yahoo.com/soccer/premier-league/scoreboard/","soccer.l.fbchampions":"http://sports.yahoo.com/soccer/champions-league/scoreboard/","soccer.l.fbes":"http://sports.yahoo.com/soccer/la-liga/scoreboard/","soccer.l.fbde":"http://sports.yahoo.com/soccer/bundesliga/scoreboard/","soccer.l.fbit":"http://sports.yahoo.com/soccer/serie-a/scoreboard/","soccer.l.fbfr":"http://sports.yahoo.com/soccer/ligue-1/"},"states":{"not-live":{"interval":300000},"live":{"interval":30000}},"pipe":{"min_polling_interval":300000}},"models":["scores"],"state":{"range":"curr","teams":""}}},"views":{"main":{"yui_module":"td-scores-atomic-mainview","yui_class":"TD.Scores.MainView","config":{"auto_update":true}},"footer":{"yui_module":"td-scores-atomic-footerview","yui_class":"TD.Scores.FooterView"},"header":{"yui_module":"td-scores-atomic-headerview","yui_class":"TD.Scores.HeaderView"}},"templates":{"header":{"yui_module":"td-applet-scores-atomic-templates-header","template_name":"td-applet-scores-atomic-templates-header"},"main":{"yui_module":"td-applet-scores-atomic-templates-main.scorethin","template_name":"td-applet-scores-atomic-templates-main.scorethin"},"footer":{"yui_module":"td-applet-scores-atomic-templates-footer","template_name":"td-applet-scores-atomic-templates-footer"},"settings":{"yui_module":"td-applet-scores-atomic-templates-settings","template_name":"td-applet-scores-atomic-templates-settings"}},"i18n":{"SCOREBOARD":"Scoreboard","TRENDING":"Trending","MY_TEAMS":"My Teams","NFL":"NFL","MLB":"MLB","NBA":"NBA","NHL":"NHL","MLS":"MLS","NCAAF":"NCAAF","NCAAB":"NCAAB","TENNIS":"Tennis","TENNIS_SETS":"Sets","HIGHLIGHTS":"Highlights","TENNIS_SCHED_DATE":"Date","TENNIS_SCHED_TOURNAMENT":"Tournament","TENNIS_SCHED_LOCATION":"Location","TENNIS_SCHED_SURFACE":"Surface","TENNIS_SCHED_CHAMPION":"Champion","TENNIS_ROUND_1":"First Round","TENNIS_ROUND_2":"Second Round","TENNIS_ROUND_3":"Third Round","TENNIS_ROUND_4":"Fourth Round","TENNIS_ROUND_5":"Quarterfinals","TENNIS_ROUND_6":"Semifinals","TENNIS_ROUND_7":"Finals","TENNIS_SIDE1_BOXSCORE":"Side 1 Boxscore","TENNIS_SIDE2_BOXSCORE":"Side 2 Boxscore","TOURNAMENT_HASNT_STARTED":"Tournament hasn't started","TENNIS_LIVE":"LIVE","TENNIS_LOADING_MESSAGE":"Loading... Please Wait","TENNIS_ALL_GAMES":"All Games","TENNIS_ROUNDS_1_4":"Rounds 1-4","TENNIS_FINALS":"Finals","TENNIS_TBD":" - TBD","ALL_TOURNAMENTS":"All Tournaments Today","BRACKET_VIEW":"Bracket view","GTYPE_PLURAL_":"All Types","GTYPE_PLURAL_mens-singles":"Men's Singles","GTYPE_PLURAL_womens-singles":"Women's Singles","GTYPE_PLURAL_mens-doubles":"Men's Doubles","GTYPE_PLURAL_womens-doubles":"Women's Doubles","PREMIER_LEAGUE":"Premier League","CHAMPIONS_LEAGUE":"Champions League","LA_LIGA":"La Liga","BUNDESLIGA":"Bundesliga","SERIE_A":"Serie A","LIGUE_1":"Ligue 1","YESTERDAY":"Yesterday","TODAY":"Today","TOMORROW":"Tomorrow","LAST_WEEK":"Last Week","CURRENT_WEEK":"Current Week","NEXT_WEEK":"Next Week","MORE_SCORES":"More scores","ADD_TO_CALENDAR":"Add to Calendar","SET_FAVORITE_TEAMS":"Set favorite teams","FINAL":"Final","LIVE":"Live","DRAW":"Draw","FULLTIME":"Full Time","FULLTIME_ABBR":"FT","HALFTIME":"Half","HALFTIME_ABBR":"HT","POSTPONED":"Postponed","POSTPONED_ABBR":"P","OWN_GOAL_ABBR":"og","PENALTY_ABBR":"pen","NO_GAMES":"No Games Scheduled","WORLD_CUP":"World Cup","WOMENS_WORLD_CUP":"Women's World Cup","Monday":"Mon","Tuesday":"Tue","Wednesday":"Wed","Thursday":"Thu","Friday":"Fri","Saturday":"Sat","Sunday":"Sun","GO_TO_SPORTS":"Go to Sports","SEE_ALL":"See All","V":"v","WATCH_LATER":"Watch Later","WATCH_NOW":"Watch Now"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-activitylist-atomic":{"base":"/sy/os/mit/td/td-applet-activitylist-atomic-0.0.60/","root":"os/mit/td/td-applet-activitylist-atomic-0.0.60/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000171"] = {"applet_type":"td-applet-activitylist-atomic","templates":{"list":{"yui_module":"td-applet-activitylist-atomic-templates-list.tumblr","template_name":"td-applet-activitylist-atomic-templates-list.tumblr"}},"i18n":{"ACTION_POST":"Posted","FAVORITE":"favorite","FIND_OUT_WHY":"Find out why Â»","LIVE":"Live","REASON:BREAKING":"Breaking","REASON:COMMENTS":"{count, number, integer} Comments","REASON:EVERGREEN":"","REASON:EXCLUSIVE":"Exclusive","REASON:NEW":"New","REASON:NEW_ON":"New on {site}","REASON:ONLYONYAHOO":"Only on Yahoo","REASON:POPULARONSEARCH":"Popular on Search","REASON:READINGNOW":"{count, number, integer} Reading Now","REASON:SEARCHVISITS":"{count, number, integer} Search Visits","REASON:SHARES":"{count, number, integer} Shares","REASON:SOCIALVISITS":"{count, number, integer} Social Visits","REASON:VIDEOPLAYS":"{count, number, integer} Video Plays","REASON:VIEWS":"{count, number, integer} Views","REASON:VISITS":"{count, number, integer} Visits","REASON:WATCHINGNOW":"{count, number, integer} Watching Now","REASON:WATCHLIVE":"Watch Live","REPLY":"reply","RETWEET":"retweet","TIME_FORMAT":"h:mm A","TIME_FORMAT_SAME_DAY":"[Today]","TIME_FORMAT_NEXT_DAY":"[Tomorrow]","TIME_FORMAT_NEXT_WEEK":"dddd","TIME_FORMAT_ELSE":"MMM D","TITLE":"Only from Yahoo","TWEET_NAVIGATE":"navigate to tweet","VIA_TWITTER":"via Twitter","YLABEL:autos":"Autos","YLABEL:beauty":"Beauty","YLABEL:diy":"DIY","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:parenting":"Parenting","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:travel":"Travel","YLABEL:tv":"TV","YPROP:autos":"Yahoo Autos","YPROP:beauty":"Yahoo Beauty","YPROP:diy":"Yahoo DIY","YPROP:finance":"Yahoo Finance","YPROP:food":"Yahoo Food","YPROP:games":"Yahoo Games","YPROP:gma":"Yahoo News","YPROP:health":"Yahoo Health","YPROP:homes":"Yahoo Homes","YPROP:ivy":"Yahoo Screen","YPROP:makers":"Yahoo Makers","YPROP:movies":"Yahoo Movies","YPROP:music":"Yahoo Music","YPROP:news":"Yahoo News","YPROP:omg":"Yahoo Celebrity","YPROP:parenting":"Yahoo Parenting","YPROP:politics":"Yahoo Politics","YPROP:shopping":"Yahoo Shopping","YPROP:sports":"Yahoo Sports","YPROP:style":"Yahoo Style","YPROP:tech":"Yahoo Tech","YPROP:travel":"Yahoo Travel","YPROP:tv":"Yahoo TV","YPROP:yahoo":"Yahoo"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
    window.Af = window.Af || {};
    window.Af.config = window.Af.config || {};
    window.Af.config.spaceid = "2023538075";
    window.Af.context= {
        crumb : '17ZKVJ/y05e',
        guid : '',
        mcCrumb: 'iJ7yisH05cS',
        ucsCrumb: 'uWD2fwL3vWJ',
        device: 'desktop',
        rid : '2r783utbfats3',
        default_page : 'p1',
        _p : 'p1',
        site : 'fp',
        lang : 'en-US',
        region : 'US',
        authed: 0,
        enable_dd : '',
        default_appletinit : 'viewport',
        locdrop_crumb: 'NnluUzYxeWtKLlY-', woeid: '7153327',
        ssl: 1,
        
        bucket: '201'
        
        
    };

    window.Af.config.transport = {
        crumbForGET: true,
        xhr: '/fpjs',
        consolidate: true,
        timeout: 6000
    };

    window.Af.config.onepush = {
        subscribeTimeout : 5000,
        subscribeMaxTries : 1,
        trackInitComet : true,
        trackLatency : true,
        latencySampleSize : 50,
        publicCometHost: 'https://comet.yahoo.com/comet',
        shutdown: false,
        trackShutdown: false
    };

    window.Af.config.beacon = {
        beaconUncaughtErr: true,
        sampleSizeUncaughtErr: 1,
        maxUncaughtErrCount: 5,
        beaconPageLoadPerf: false,
        sampleSize : 1,
        pathPrefix : '/_td_api/beacon',
        batch: false,
        batchInterval: 3
    };

    window.Af.config.pipe = {
        msg_throttle: 2000,
        err_maxstreak: 2
    };

    window.Af.config.td = {
        remote: '/_td_remote',
        xhr: '/_td_api'
    };

    
    YUI.namespace('Env.My.settings').context = {
        uh_js_file : '',
                videoAsyncEnabled: 0,
        videoplayerScriptElementId: 'videoplayerJs',
        videoplayerUrl: 'https://www.yahoo.com/sy/rx/builds/6.256.0.1456781727/en-us/videoplayer-nextgen-flash-min.js',
        videoAutoplay: 1,
        videoLooping: 0,
        videoForcedError: 0,
        videoFullscreen: 1,
        videoHtml5: 0,
        videoMinControls: 0,
        videoCmsEnv: 'prod',
        videoMustWatch: 1,
        videoMWSticky: 0,
        videoMute: 1,
        videoQosRate: 1,
        videoBuffering: 0,
        videoRelated: 0,
        videoYwaRate: 0,
        videoMaxLoops: 3,
        videoModeEnabled: 0,
        videoTiltbackModeEnabled: 0,
        videoSSButton: 0,
        videoPausescreen: 0,
        videoSingleVideo: 0,
        videoMaxInstances: 12,
        videoInitEvent: 'domready',
        videoExpName: 'advance',
        videoComscoreC4: 'US fp',
        videoplayerScrollThrottle: '200',
        
        themeCssEnabled : false,
        themeBgImageEnabled: false,
        transparentPixelImage: 'https://s.yimg.com/os/mit/media/m/base/images/transparent-95031.png',
        pageMessage: {
            type: '',
            msg: ''
        },
        servingBeaconUrl: 'https://bs.serving-sys.com/BurstingPipe/ActivityServer.bs',
        enablePerfBeacon: '0',
        test_id: '201',
        customizedEnabled: false,
        trendingNowOffScreen: 0
    };

    YUI.namespace('Env.Af.Perf').secondYUIUseStart = (new Date()).getTime();

    var yuiPreloadModules = ["type_advance_desktop_viewer","type_video_manager","type_idletimer","type_sdarotate","type_sda"];

    yuiPreloadModules = yuiPreloadModules.concat((function (appletTypes) {
        if (!appletTypes || appletTypes.length === 0) {
            return [];
        }
        var yui_modules = [],
            types = YMedia.Array.hash(appletTypes);
        YMedia.Object.each(window.Af.bootstrap, function (bootstrap, guid) {
            if (!types[bootstrap.applet_type]) {
                 return;
            }
            YMedia.Array.each(['models', 'views', 'templates'], function (type) {
                YMedia.Object.each(bootstrap[type], function (config, name) {
                    if (config.yui_module) {
                         yui_modules.push(config.yui_module);
                    }
                });
            });
        });
        return yui_modules;
    })([]));

    YMedia.use(yuiPreloadModules, function (Y, NAME) {
        YUI.namespace('Env.Af.Perf').secondYUIUseStop = (new Date()).getTime();
        var pageMessage = YUI.Env.My.settings.context.pageMessage,
        enablePerfBeacon = YUI.Env.My.settings.context.enablePerfBeacon,
        test_id = YUI.Env.My.settings.context.test_id,
        pausePerfBeacon = false,
        perfBeacon = {}
                ,
        rapidConfig = rapidPageConfig.rapidConfig,
        YWA_CF_MAP = rapidPageConfig.ywaCF,
        YWA_ACTION_MAP = rapidPageConfig.ywaActionMap,
        YWA_OUTCOME_MAP = rapidPageConfig.ywaOutcomeMap;
        if(YAHOO.i13n) {
            YAHOO.i13n.WEBWORKER_FILE = '/lib/metro/g/myy/rapidworker_1_2_0.0.3.js';
            YAHOO.i13n.SPACEID = '2023538075';
            YAHOO.i13n.TEST_ID = '201';
        }
        if (pageMessage.msg != '') {
            Y.Af.Message.show('body', {level: pageMessage.type, content: pageMessage.msg});
        }

        if (1 === 1) {
            Y.Lang.later(45000, null, function() {
                var failedModules  = Y.all("#myColumns .js-applet .App-loading");
                failedModules.each(function (module) {
                    module.setContent('Sorry! We are temporarily unable to load the content. Please refresh or try again later.');
                    module.removeClass('App-loading');
                    module.addClass('App-failed');
                });
            });
        }
        
        
        
        

        YUI.namespace('Env.Af.Perf').YMyAppCreateStart = (new Date()).getTime();
        YMedia.My.App = new Y.My.App(
            {                i13nConfig: {
                    rapid: rapidConfig,
                    rapidInstance: rapidPageConfig.rapidSingleInstance && YAHOO && YAHOO.i13n && YAHOO.i13n.rapidInstance || null,
                    ywaMaps: {
                        YWA_OUTCOME_MAP: YWA_OUTCOME_MAP,
                        YWA_CF_MAP: YWA_CF_MAP,
                        YWA_ACTION_MAP: YWA_ACTION_MAP
                    }
                },
                inlineViewer: 0,
                summaryView: 0,
                stickerTarget: "#SearchBar-Wrapper-Mini",
                magazineViewerEnabled: 1,
                viewerBlacklistDisable: 1,
                viewer: {
                    viewerAnimation: "slide",
                    pageAnimation: "zoom",
                    scrollTopAmount: 0,
                    highlanderMode: "mega-modal"
                }}
        );
        

        var firePerfBeacon = function(e) {
            var key, value, beaconNode, queryString = [];

            if (Y.Object.size(perfBeacon) < 1 || pausePerfBeacon) {
                return;
            }
            pausePerfBeacon = true;
            for (key in perfBeacon) {
                if (perfBeacon.hasOwnProperty(key)) {
                    queryString.push(key+'='+perfBeacon[key]);
                }
            }
            queryString.push('test='+test_id);
            beaconNode = Y.Node.create('<img src="myperf.php?'+queryString.join('&')+'" style="width:1px;height:1px;position:absolute;left:-900px;">');
            Y.one('body').append(beaconNode);
        };

        if (enablePerfBeacon == '1') {
            Y.one('window').once('scroll', firePerfBeacon);
            Y.Lang.later(10000, null, firePerfBeacon);

            YMedia.My.App.after('appletsinit', function (e) {
                if (pausePerfBeacon == true) {
                    return;
                }
                perfBeacon[e.applet.type] = (new Date()) - myYahoostartTime;
            });
        }
        YMedia.on('appletsinit', function (e) {
            
        });
        if (Y.IdleTimer) {
    Y.IdleTimer.start(parseInt(5400000,10));
    Y.IdleTimer.on(['idle','hidden'], function(e){
        if (e && e.type) {
            var isModalOpen = Y.one('html').hasClass('Reader-open'),
                location = Y.config.win.location,
                type = e.type,
                paramMap = {
                    idle: 'vi',
                    hidden: 'vh'
                },
                url = location.protocol + '//' + location.host,
                expires = new Date();

            if (paramMap[type]) {
                expires.setMinutes(expires.getMinutes() + parseInt(1,10));
                Y.Cookie.set('autorf', paramMap[type], {expires: expires, domain: location.hostname});

                if (isModalOpen) {
                    Y.config.win.location = url
                } else {
                    Y.config.win.location.replace(url);
                }
            }
        }
    });
}
    });
            });
     });
</script>
    <!-- bottom -->

    <!-- Comscore -->
            <!-- Begin comScore Tag -->
		<script>
		  var _comscore = _comscore || [];
		  _comscore.push({
		    c1: "2",
		    c2: "7241469",
		    c5: "2023538075",
		    c7: "https://www.yahoo.com/"
		  });
		  (function() {
		    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		    s.src = "/sy/lq/lib/3pm/cs_0.2.js";
		    el.parentNode.insertBefore(s, el);
		  })();
		</script>
		<noscript>
		  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=7241469&c7=https%3A%2F%2Fwww.yahoo.com%2F&c5=2023538075&cv=2.0&cj=1" />
		</noscript>
		<!-- End comScore Tag -->

    <!-- Nielsen -->
    

    <!-- Lighthouse  -->
    

    <!-- yaft -->
            <script type="text/javascript" src="/sy/zz/combo?/os/yaft/yaft-0.3.3.min.js&/os/yaft/yaft-plugin-aftnoad-0.1.3.min.js&os/yaft/yaft-plugin-harbeacon-0.1.3.min.js"> </script>        
        <script type="text/javascript">
            if (window.YAFT !== undefined) {
                var __yaftConfig = {
                    modules: ["UH","mega-uh","mega-topbar","applet_p_","ad-north-base","fea-","my-adsFPAD-base","my-adsLREC-base","my-adsMAST","my-adsTXTL","content-modal-","hl-ad-LREC-","modal-sidekick-","hl-ad-LREC-0","hl-ad-MON-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","hl-ad-SPL-0"], 
                    modulesExclude: ["UH-Search","UH-ColWrap","mega-uh-wrapper"],
                    canShowVisualReport: false,
                    useNormalizeCoverage: true,
                    generateHAR: false,
                    includeOnlyAft2: true,
                    useNativeStartRender : true,
                    modulesAft2Container: ["hl-viewer"],
                    maxWaitTime: 6000
                };
                __yaftConfig.plugins = [];                __yaftConfig.plugins.push({
                     name: 'aftnoad',
                     isPre: true,
                     config: {
                         useNormalizeCoverage: true,
                         adModules:["ad-north-base","my-adsFPAD-base","my-adsLREC-base","my-adsTL1","my-adsMAST","hl-ad-LREC-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","my-adsTXTL","hl-ad-SPL-0"]
                     }
                });
                            __yaftConfig.plugins.push({
                name: 'har',
                config: {
                    sec: 'har',
                    rapid: function() { if(YMedia && YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) {return YMedia.My.App.getRapidTracker(); } else {return null;}},
                    aftThreshold: 15000,
                    onlySlowest: false
                }
            });
            __yaftConfig.generateHAR = true;
                        if (window.LH && window.LH.isInitialized) {
            window.LH.tag('b',{val: '201'});               
            window.LH.tag("l", {val: false});
        }
                        window.aft2CB = function(data, error) {
        if (!error) {
            var aft2 = Math.round(data.aft);
            var vic2 = data.visuallyComplete;
            var srt2 = Math.round(data.startRender);

            if (window.LH && window.LH.isInitialized) {
                var lhRecord = function lhRecord(name, type, startTime, duration) {
                    window.LH.record(name, {
                        name: name, type: type, startTime: startTime, duration: duration
                    });
                };

                lhRecord('AFT', 'mark', aft2, 0);
                lhRecord('AFT2', 'mark', aft2, 0);
                lhRecord('VIC2', 'mark', vic2, 0);
                lhRecord('STR2', 'mark', srt2, 0);
                window.LH.beacon({
                    clearMarks:false,
                    clearMeasures: false,
                    clearCustomEntries: true,
                    clearTags: false
                });
            }

            var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
            if (rapidInstance) {
                var afterPageLoad = {
                    AFT: aft2,
                    AFT2: aft2,
                    STR: srt2,
                    VIC: vic2
                };
                var perfData = {
                    perf_commontime: {afterPageLoad: afterPageLoad}
                };
                var pageParamsObject = null;            if (rapidInstance.getRefererSpaceid) {
                pageParamsObject  = {
                    _s: rapidInstance.getRefererSpaceid(),
                    _S: (window.Af && window.Af.config && window.Af.config.spaceid)
                };
            }
                rapidInstance.beaconPerformanceData(perfData, pageParamsObject);
            }
        }
    };
                window.YAFT.init(__yaftConfig, function(data, error) {
                    if (!error) {
                        try {
                                    if (window.LH && window.LH.isInitialized) {
            var results = [0], module = '', index = '', lhRecord = '';
            for (module in data.modulesReport) {
                for (index in data.modulesReport[module].resources) {
                    results.push(Math.round(data.modulesReport[module].resources[index].durationFromNStart));
                }
            }
            lhRecord = function (ma, va, custom) {
                var res = '';                                
                if (undefined === custom) {
                    if (va && !isNaN(va)) {
                        res = Math.round(va);
                    } else {
                        res = 0;
                    }
                } else {
                    res = va;
                }
                    window.LH.record(ma, { name: ma, type: 'mark', startTime: res, duration: 0 });
            };
                            
            lhRecord('AFT', data.aft);
            lhRecord('AFT1', data.aft);
            if (data.aftNoAd) {
                lhRecord('AFTNOAD', data.aftNoAd);
            }
            lhRecord('X_FB1', 48);
            lhRecord('X_FBN', 77);
            lhRecord('PLT_CACHED_REQs', data.httpRequests.onloadCached);
            lhRecord('COSTLY_RESOURCE', Math.max.apply(null, results));
            lhRecord('StartRender', data.startRender);
            if (screen) {
                lhRecord('SCR_H', screen.height);
                lhRecord('SCR_W', screen.width);
            }
            if (window.FPAD_rendered) {
                lhRecord('xAFT', data.aft);
                lhRecord('xPLT', data.pageLoadTime);
                if (window.rtAdStart && window.rtAdDone) {
                    lhRecord('ADSTART_FPAD', Math.round(rtAdStart));
                    lhRecord('ADEND_FPAD', Math.round(rtAdDone));
                }
            }

            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    lhRecord(_adLT[i][0], _adLT[i][1]);
                }
            }
            window.LH.beacon({ clearMarks: false, clearMeasures: false, clearCustomEntries: true, clearTags: false });   
        }
                                    var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
        if(rapidInstance) {
            var initialPageLoad = {
                AFT: Math.round(data.aft),
                AFT1: Math.round(data.aft),
                STR: data.startRender,
                VIC: data.visuallyComplete,
                PLT: data.pageLoadTime,
                DOMC: data.domElementsCount,
                HTTPC: data.httpRequests.count,
                CP: data.totalCoveragePercentage,
                NCP: data.normTotalCoveragePercentage
            };

            if(data.aftNoAd) {
                initialPageLoad.AFTNOAD = Math.round(data.aftNoAd);
            }              

            var customPerfData = {},
                pagePerfData = {},
                results = [];
            
            var yaftResults = [0], yaftModule = '', yaftIndex = '';
            // Find costly resource time
            for (yaftModule in data.modulesReport) {
                for (yaftIndex in data.modulesReport[yaftModule].resources) {
                    yaftResults.push(Math.round(data.modulesReport[yaftModule].resources[yaftIndex].durationFromNStart));
                }
            }
            pagePerfData['COSTLY_RESOURCE'] = Math.max.apply(null, yaftResults);
            pagePerfData['X_FB1'] = 48;
            pagePerfData['X_FBN'] = 77; 
   
            // Log ad perf data to rapid perf metric
            if (window.FPAD_rendered) {
                pagePerfData['xAFT'] = data.aft;
                pagePerfData['xPLT'] = data.pageLoadTime;
                if (window.rtAdStart && window.rtAdDone) {
                    pagePerfData['ADSTART_FPAD'] = Math.round(rtAdStart);
                    pagePerfData['ADEND_FPAD'] = Math.round(rtAdDone);
                }
            }
            
            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    pagePerfData[_adLT[i][0]]  = _adLT[i][1];
                }
            }
                        if(resourceTimingAssets) {
                for(i in resourceTimingAssets) {
                    if (resourceTimingAssets.hasOwnProperty(i)) {
                        resourceName = window.performance.getEntriesByName(resourceTimingAssets[i]);
                        if(resourceName && resourceName.length) {
                            var resourceFinish = resourceName[0].responseEnd;
                            pagePerfData[i] = Math.round(resourceFinish);
                        }
                    }
                }
            }

            customPerfData['utm'] = pagePerfData;
            var perfData = {
                perf_commontime: {initialPageLoad: initialPageLoad},
                perf_usertime: customPerfData
            };
            rapidInstance.beaconPerformanceData(perfData);
        }
                        } catch (e) {}                                                        
                    }
               });
            }
      </script>

    <!-- Via https/1.1 ir13.fp.ir2.yahoo.com[D992B319] (YahooTrafficServer), http/1.1 usproxy1.fp.ne1.yahoo.com[628AF396] (YahooTrafficServer) -->
    <!-- sid=2023538075 -->
    

    </body>
</html>
<!-- myproperty:myservice-us:0:Success -->
<!-- slw88.fp.ne1.yahoo.com compressed/chunked Fri Mar 25 17:38:11 UTC 2016 -->
