ï»¿<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>China.com - Your guide on traveling and living in China</title>
<meta name="keywords" content="China,travel,lifestyle,learn Chinese,news,videos,business,films,sports,reports" />
<meta name="description" content="English.china.com is a one-stop shop for everything about China â news, events, culture, people, lifestyle, language. It also provides information about traveling and living in China." />
<meta name="auther" content="F7 13489" />
<!-- /etc/htmlhead.shtml Start -->
<link href="/css/style.css?20141205.1" rel="stylesheet" />
<!--[if lte IE 6]> 
<script src="/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.page-select-language-list em, .page-nav .page-openSearch em, .page-focus-prevnext a, .page-video-right .item-text, .page-watched .play, .page-video-right .item-icon, .page-video-right .item-watch'); 
</script>
<![endif]-->
<!-- /etc/htmlhead.shtml End -->
<!-- /etc/goto3g.shtml Start -->

<!-- /etc/goto3g.shtml End -->
</head>

<body>
<!-- /etc/channelhomehead.shtml Start -->
<div class="page-head">
  <div class="page-top-bg" id="page-top">
    <div class="maxWidth page-top">
      <div class="page-logo"><a href="/index.html"><img src="/img/logo.png" alt="china.com" width="208" height="46" /></a></div>
      <div class="page-language">
        <em>language:English</em>
      </div>
      <div class="page-top-right">
          <div class="page-top-time">Thursday, October 24, 2013</div>
          <div class="page-select-language" id="page-select-language">
              <div class="page-show-language" style="display: block;">Language</div>
              <div class="page-select-language-tit" style="display: none;"><i>Language</i></div>
              <div class="page-select-language-list" style="display: none;">
                <a href="http://www.china.com/index.html" class="langCn"><em>www</em></a>
                <a href="http://english.china.com/index.html" class="langEn"><em>english</em></a>
                <a href="http://german.china.com/index.html" class="langDe"><em>german</em></a>
                <a href="http://italy.china.com/index.html" class="langIt"><em>italy</em></a>
                <a href="http://portuguese.china.com/index.html" class="langPt"><em>portuguese</em></a>
                <a href="http://french.china.com/index.html" class="langFr"><em>french</em></a>
                <a href="http://russian.china.com/index.html" class="langRu"><em>russian</em></a>
                <a href="http://espanol.china.com/index.html" class="langEs"><em>espanol</em></a>
                <a href="http://malay.china.com/index.html" class="langMy"><em>malay</em></a>
                <a href="http://vietnamese.china.com/index.html" class="langVn"><em>vietnamese</em></a>
                <a href="http://laos.china.com/index.html" class="langLa"><em>laos</em></a>
                <a href="http://cambodian.china.com/index.html" class="langKh"><em>cambodian</em></a>
                <a href="http://thai.china.com/index.html" class="langTh"><em>thai</em></a>
                <a href="http://indonesian.china.com/index.html" class="langId"><em>indonesian</em></a>
                <a href="http://filipino.china.com/index.html" class="langPh"><em>filipino</em></a>
                <a href="http://myanmar.china.com/index.html" class="langMm"><em>myanmar</em></a>
                <a href="http://japanese.china.com/index.html" class="langJp"><em>japanese</em></a>
                <a href="http://korean.china.com/index.html" class="langKr"><em>korean</em></a>
                <a href="http://mongol.china.com/index.html" class="langMn"><em>mongol</em></a>
                <a href="http://nepal.china.com/index.html" class="langNp"><em>nepal</em></a>
                <a href="http://hindi.china.com/index.html" class="langIn"><em>hindi</em></a>
                <a href="http://bengali.china.com/index.html" class="langMd"><em>bengali</em></a>
                <a href="http://turkish.china.com/index.html" class="langTr"><em>turkish</em></a>
                <a href="http://persian.china.com/index.html" class="langIr"><em>persian</em></a>
                <a href="http://arabic.china.com/index.html" class="langAe"><em>arabic</em></a>
              </div>
            </div>
            <div class="page-search">
              <form id="web-search" name="web-search" method="get" action="http://www.google.com/search">
                <input type="hidden" name="sitesearch" value="english.china.com" />
                <input type="text" name="q" class="web-search-keyword" placeholder="Search here..." />
                <input type="submit" name="button" class="web-search-but" value="Search Site" />
              </form>
            </div>
      </div>
    </div>
  </div>
  <div class="page-nav-bg" id="page-nav">
    <ul class="page-nav maxWidth">
      <li class="page-openSearch"><a href="#"><em>Open search</em></a></li>
      <li class="small-logo"><a href="/index.html"><img src="/img/small-logo.png" /></a></li>
      <li><a href="/news/index.html">News</a></li>
      <li><a href="/video/index.html">Video</a></li>
      <li><a href="/audio/index.html">Audio</a></li>
      <li><a href="/photos/index.html">Photos</a></li>
      <li><a href="/travel/index.html">Travel</a></li>
      <li><a href="/lifestyle/index.html">Lifestyle</a></li>
      <li><a href="/chinese/index.html">Learn Chinese</a></li>
      <li><a href="http://mail.china.com/en/" class="ext" target="_blank">Free Mail</a></li>
    </ul>
  </div>
</div><!-- page-head End -->
<!-- /etc/channelhomehead.shtml End -->

<div class="maxWidth"><div id='CH_ENG_CHT_00001' class='adclass' pushtype='no'></div></div>

<div class="page-main maxWidth">
  <div class="page-left">
    <div class="page-focus" id="page-focus">
      <div class="page-focus-body" id="page-focus-body">
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.chinadaily.com.cn/china/2015-07/16/content_21294670.htm" title="Wan Li: A life in photos"><img src="http://img04.english.china.com/news/topphotos/china/189/20150716/419485_105053_680x330.jpg" width="680" height="330" alt="Wan Li: A life in photos" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.chinadaily.com.cn/china/2015-07/16/content_21294670.htm" title="Wan Li: A life in photos" class="title_default">Wan Li: A life in photos</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/07/15/4082s887474.htm" title="Disney Unveils Attractions at Planned Resort in Shanghai"><img src="http://img01.english.china.com/news/topphotos/showbiz/1211/20150716/419484_105052.jpg" width="680" height="330" alt="Disney Unveils Attractions at Planned Resort in Shanghai" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/07/15/4082s887474.htm" title="Disney Unveils Attractions at Planned Resort in Shanghai" class="title_default">Disney Unveils Attractions at Planned Resort in Shanghai</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://en.people.cn/n/2015/0715/c98649-8920793.html" title="Single Bamboo Drifting Goes into Campus in SW China"><img src="http://img04.english.china.com/home/topphoto/1295/20150715/419118_104972.jpg" width="680" height="330" alt="Single Bamboo Drifting Goes into Campus in SW China" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://en.people.cn/n/2015/0715/c98649-8920793.html" title="Single Bamboo Drifting Goes into Campus in SW China" class="title_default">Single Bamboo Drifting Goes into Campus in SW China</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/07/15/3821s887415.htm" title="NASA Sees Pluto up Close with Historic Flyby"><img src="http://img01.english.china.com/home/topphoto/1295/20150715/419108_104963.jpg" width="680" height="330" alt="NASA Sees Pluto up Close with Historic Flyby" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/07/15/3821s887415.htm" title="NASA Sees Pluto up Close with Historic Flyby" class="title_default">NASA Sees Pluto up Close with Historic Flyby</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/11234/2015/07/14/2821s887342.htm" title="Dujiangyan's Panda Retirement Home"><img src="http://img03.english.china.com/news/topphotos/china/189/20150715/418559_104748.jpg" width="680" height="330" alt="Dujiangyan's Panda Retirement Home" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/11234/2015/07/14/2821s887342.htm" title="Dujiangyan's Panda Retirement Home" class="title_default">Dujiangyan's Panda Retirement Home</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
      </div>
      <div id="page-focus-console"></div>
      <div class="page-focus-prevnext">
        <a href="#" id="page-focus-prev">Previous focus</a>
        <a href="#" id="page-focus-next">Next focus</a>
      </div>
    </div><!-- page-focus End -->
    
    <div class="page-latest">
      <h2 class="modTit"><strong><a href="/news/index.html">LATEST NEWS</a></strong></h2>
      <div class="page-latest-body" id="page-latest">
        <div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/07/16/1821s887524.htm" title="China's Internet Giants Reprimanded over Obscene Clip"><img src="http://img01.english.china.com/news/china/54/20150716/419490_105056_200x120.jpg" width="200" height="120" alt="China's Internet Giants Reprimanded over Obscene Clip" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/07/16 09:00:41</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/07/16/1821s887524.htm" title="China's Internet Giants Reprimanded over Obscene Clip" class="title_default">China's Internet Giants Reprimanded over Obscene Clip</a></h3>
            <p class="item-infor" title="China's Internet regulator reprimanded two major web portals for their failure to prevent a sex video taken in a Beijing fitting room from going viral online.">China's Internet regulator reprimanded two major web portals for their failure to prevent a sex video taken in a Beijing fitting room from going viral online.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/07/15/2743s887491.htm" title="Japan to Expand Military Role despite Protests"><img src="http://img01.english.china.com/news/world/55/20150715/419294_105009.jpg" width="200" height="150" alt="Japan to Expand Military Role despite Protests" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/07/15 20:15:03</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/07/15/2743s887491.htm" title="Japan to Expand Military Role despite Protests" class="title_default">Japan to Expand Military Role despite Protests</a></h3>
            <p class="item-infor" title="A Japanese parliamentary committee has approved security legislation to expand their military role.">A Japanese parliamentary committee has approved security legislation to expand their military role.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/07/15/3781s887485.htm" title=""Monkey King: Hero Is Back" Sets New Record for Chinese Animation"><img src="http://img02.english.china.com/news/showbiz/58/20150715/419278_105008.jpg" width="160" height="120" alt=""Monkey King: Hero Is Back" Sets New Record for Chinese Animation" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/07/15 20:13:24</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/07/15/3781s887485.htm" title=""Monkey King: Hero Is Back" Sets New Record for Chinese Animation" class="title_default">"Monkey King: Hero Is Back" Sets New Record for Chinese Animation</a></h3>
            <p class="item-infor" title="Animated movie Monkey King: Hero Is Back has set a record for the fastest Chinese animation to surpass the 200 million yuan mark. ">Animated movie Monkey King: Hero Is Back has set a record for the fastest Chinese animation to surpass the 200 million yuan mark. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/photo/2015-07/15/c_134413387.htm" title="Russia's 2018 World Cup Stadium under Construction"><img src="http://img03.english.china.com/news/sports/57/20150715/419197_104991.jpg" width="200" height="120" alt="Russia's 2018 World Cup Stadium under Construction" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/07/15 19:19:44</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/photo/2015-07/15/c_134413387.htm" title="Russia's 2018 World Cup Stadium under Construction" class="title_default">Russia's 2018 World Cup Stadium under Construction</a></h3>
            <p class="item-infor" title="Photo taken on July 14, 2015 shows the construction site of the Rostov Arena in Rostov-on-don, Russia. Russia will host the FIFA World Cup soccer tournament in 2018.">Photo taken on July 14, 2015 shows the construction site of the Rostov Arena in Rostov-on-don, Russia. Russia will host the FIFA World Cup soccer tournament in 2018.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/metro/Shanghai-Disneyland-unveils-details-of-key-highlights/shdaily.shtml" title="Shanghai Disneyland Unveils Details of Key Highlights"><img src="http://img01.english.china.com/news/china/54/20150715/419117_104971.jpg" width="200" height="120" alt="Shanghai Disneyland Unveils Details of Key Highlights" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/07/15 17:46:48</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/metro/Shanghai-Disneyland-unveils-details-of-key-highlights/shdaily.shtml" title="Shanghai Disneyland Unveils Details of Key Highlights" class="title_default">Shanghai Disneyland Unveils Details of Key Highlights</a></h3>
            <p class="item-infor" title="Shanghai Disney Resort will feature six themed lands filled with world-class attractions and live shows -- many appearing for the first time at a Disney amusement park.">Shanghai Disney Resort will feature six themed lands filled with world-class attractions and live shows -- many appearing for the first time at a Disney amusement park.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/07/15/2743s887473.htm" title="China's Economy Grows 7 Pct in Q2, Slightly Better than Expected"><img src="" width="" height="" alt="China's Economy Grows 7 Pct in Q2, Slightly Better than Expected" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/07/15 17:40:57</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/07/15/2743s887473.htm" title="China's Economy Grows 7 Pct in Q2, Slightly Better than Expected" class="title_default">China's Economy Grows 7 Pct in Q2, Slightly Better than Expected</a></h3>
            <p class="item-infor" title="China's economy has posted 7-percent growth year on year in the second quarter of 2015. ">China's economy has posted 7-percent growth year on year in the second quarter of 2015. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/2015-07/15/content_21283816.htm" title="UK restricts students' right to work"><img src="http://img04.english.china.com/news/world/55/20150715/418565_104751.jpg" width="160" height="103" alt="UK restricts students' right to work" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/07/15 09:03:45</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/2015-07/15/content_21283816.htm" title="UK restricts students' right to work" class="title_default">UK restricts students' right to work</a></h3>
            <p class="item-infor" title="Chinese students who study in Britain won't be affected much by new rules on the right to work announced by the security and immigration minister.">Chinese students who study in Britain won't be affected much by new rules on the right to work announced by the security and immigration minister.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-07/14/c_134412357.htm" title="New Horizons' Pluto flyby makes history"><img src="" width="" height="" alt="New Horizons' Pluto flyby makes history" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/07/15 09:00:57</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-07/14/c_134412357.htm" title="New Horizons' Pluto flyby makes history" class="title_default">New Horizons' Pluto flyby makes history</a></h3>
            <p class="item-infor" title="NASA's New Horizons performed the first-ever flyby of Pluto on Tuesday, about 12,472 kilometers (7,750 miles) above the surface, making the closest approach to the dwarf planet.">NASA's New Horizons performed the first-ever flyby of Pluto on Tuesday, about 12,472 kilometers (7,750 miles) above the surface, making the closest approach to the dwarf planet.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/07/15/3561s887397.htm" title="Apple Pay Launches in UK with some Big Banks Missing"><img src="http://img03.english.china.com/news/business/56/20150715/418562_104750_200x120.jpg" width="200" height="120" alt="Apple Pay Launches in UK with some Big Banks Missing" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/07/15 09:00:19</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/07/15/3561s887397.htm" title="Apple Pay Launches in UK with some Big Banks Missing" class="title_default">Apple Pay Launches in UK with some Big Banks Missing</a></h3>
            <p class="item-infor" title="UK became the first country outside the United States to get iPhone-maker Apple's new mobile payment system Apple Pay, as it went live on Tuesday. ">UK became the first country outside the United States to get iPhone-maker Apple's new mobile payment system Apple Pay, as it went live on Tuesday. </p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/metro/society/City-considers-lucky-draw-for-car-plates/shdaily.shtml" title="City considers âlucky drawâ for car plates"><img src="" width="" height="" alt="City considers âlucky drawâ for car plates" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/07/15 08:58:23</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/metro/society/City-considers-lucky-draw-for-car-plates/shdaily.shtml" title="City considers âlucky drawâ for car plates" class="title_default">City considers âlucky drawâ for car plates</a></h3>
            <p class="item-infor" title="Shanghai traffic authorities are considering a new scheme - a lucky draw - for car plates every month and do away with the current auction system that was criticized by a local lawmaker.">Shanghai traffic authorities are considering a new scheme - a lucky draw - for car plates every month and do away with the current auction system that was criticized by a local lawmaker.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/2015victoryanniv/2015-07/15/content_21283653.htm" title="35 million Chinese died during 14-year Japanese invasion"><img src="http://img01.english.china.com/news/china/54/20150715/418560_104749.jpg" width="160" height="103" alt="35 million Chinese died during 14-year Japanese invasion" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/07/15 08:57:44</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/2015victoryanniv/2015-07/15/content_21283653.htm" title="35 million Chinese died during 14-year Japanese invasion" class="title_default">35 million Chinese died during 14-year Japanese invasion</a></h3>
            <p class="item-infor" title="During the 14-year Japanese invasion of China, the Chinese suffered more than 35 million military and nonmilitary casualties.">During the 14-year Japanese invasion of China, the Chinese suffered more than 35 million military and nonmilitary casualties.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.globaltimes.cn/content/932017.shtml" title="TCM Therapies in A Heat Wave"><img src="http://img02.english.china.com/news/china/54/20150714/418449_104710.jpg" width="200" height="120" alt="TCM Therapies in A Heat Wave" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/07/14 20:51:46</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://www.globaltimes.cn/content/932017.shtml" title="TCM Therapies in A Heat Wave" class="title_default">TCM Therapies in A Heat Wave</a></h3>
            <p class="item-infor" title="As China sweats through the scorching summer heat, people are flooding into traditional Chinese medicine hospitals and clinics in the hope of curing their winter ailments in the summer, an age-old practice that is still popular today.">As China sweats through the scorching summer heat, people are flooding into traditional Chinese medicine hospitals and clinics in the hope of curing their winter ailments in the summer, an age-old practice that is still popular today.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/2015-07/14/content_21279661.htm" title="Iran, Big Powers Clinch Landmark Nuclear Deal after Marathon Talks"><img src="http://img01.english.china.com/news/world/55/20150714/418447_104706.jpg" width="200" height="120" alt="Iran, Big Powers Clinch Landmark Nuclear Deal after Marathon Talks" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/07/14 20:47:34</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/2015-07/14/content_21279661.htm" title="Iran, Big Powers Clinch Landmark Nuclear Deal after Marathon Talks" class="title_default">Iran, Big Powers Clinch Landmark Nuclear Deal after Marathon Talks</a></h3>
            <p class="item-infor" title="A historic agreement has been reached over the Iranian nuclear issue between Iran and six world major countries, a diplomatic source confirmed to Xinhua on Tuesday.">A historic agreement has been reached over the Iranian nuclear issue between Iran and six world major countries, a diplomatic source confirmed to Xinhua on Tuesday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://en.people.cn/n/2015/0714/c90779-8920236.html" title="Ping-pong Olympic Champion in Dispute with Club over Unpaid Bonuses"><img src="http://img01.english.china.com/news/sports/57/20150714/418444_104705.jpg" width="200" height="120" alt="Ping-pong Olympic Champion in Dispute with Club over Unpaid Bonuses" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/07/14 20:45:43</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://en.people.cn/n/2015/0714/c90779-8920236.html" title="Ping-pong Olympic Champion in Dispute with Club over Unpaid Bonuses" class="title_default">Ping-pong Olympic Champion in Dispute with Club over Unpaid Bonuses</a></h3>
            <p class="item-infor" title="Zhang Jike and his table tennis club were both fined 50,000 yuan after Zhang failed to appear for the ninth round of the match at the China Table Tennis Super League and the club failed to notify the Table Tennis and Badminton Centre in advance.">Zhang Jike and his table tennis club were both fined 50,000 yuan after Zhang failed to appear for the ninth round of the match at the China Table Tennis Super League and the club failed to notify the Table Tennis and Badminton Centre in advance.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/07/14/3123s887356.htm" title="Lady Gaga Checks Into 'American Horror Story: Hotel'"><img src="http://img01.english.china.com/news/showbiz/58/20150714/418443_104704.jpg" width="200" height="120" alt="Lady Gaga Checks Into 'American Horror Story: Hotel'" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/07/14 20:43:24</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/07/14/3123s887356.htm" title="Lady Gaga Checks Into 'American Horror Story: Hotel'" class="title_default">Lady Gaga Checks Into 'American Horror Story: Hotel'</a></h3>
            <p class="item-infor" title=""American Horror Story: Hotel", the upcoming fifth season of the FX horror anthology television series American Horror Story has released a teaser, which highlights the joining of American pop star Lady Gaga.">"American Horror Story: Hotel", the upcoming fifth season of the FX horror anthology television series American Horror Story has released a teaser, which highlights the joining of American pop star Lady Gaga.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-07/14/c_134412008.htm" title="China to Stick to Prudent Monetary Policy: PBOC"><img src="http://img01.english.china.com/news/business/56/20150714/418439_104701.jpg" width="200" height="120" alt="China to Stick to Prudent Monetary Policy: PBOC" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/07/14 20:36:05</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-07/14/c_134412008.htm" title="China to Stick to Prudent Monetary Policy: PBOC" class="title_default">China to Stick to Prudent Monetary Policy: PBOC</a></h3>
            <p class="item-infor" title="The People's Bank of China, the central bank, announced it "will continue to implement the prudent monetary policy, and improve the financial system's capability to serve the real economy".">The People's Bank of China, the central bank, announced it "will continue to implement the prudent monetary policy, and improve the financial system's capability to serve the real economy".</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/07/13/3744s887244.htm" title="Alibaba Partners for Mould-breaking Home Entertainment Package"><img src="http://img01.english.china.com/news/business/56/20150714/417780_104492_200x120.jpg" width="200" height="120" alt="Alibaba Partners for Mould-breaking Home Entertainment Package" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/07/14 09:19:11</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/07/13/3744s887244.htm" title="Alibaba Partners for Mould-breaking Home Entertainment Package" class="title_default">Alibaba Partners for Mould-breaking Home Entertainment Package</a></h3>
            <p class="item-infor" title="Alibaba has partnered with Hunan TV and film production and distribution company DMG Entertainment to launch the country's first home entertainment platform.">Alibaba has partnered with Hunan TV and film production and distribution company DMG Entertainment to launch the country's first home entertainment platform.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-07/13/c_134408677.htm" title="China's Tianhe-2 tops world supercomputers, again"><img src="" width="" height="" alt="China's Tianhe-2 tops world supercomputers, again" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/07/14 09:17:33</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-07/13/c_134408677.htm" title="China's Tianhe-2 tops world supercomputers, again" class="title_default">China's Tianhe-2 tops world supercomputers, again</a></h3>
            <p class="item-infor" title="China's Tianhe-2 supercomputer was named the world's fastest supercomputer at the International Supercomputing Conference in Frankfurt, Germany.">China's Tianhe-2 supercomputer was named the world's fastest supercomputer at the International Supercomputing Conference in Frankfurt, Germany.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/07/14/3981s887265.htm" title="Singer Jay Chou Welcomes a Baby Girl"><img src="http://img01.english.china.com/news/showbiz/58/20150714/417777_104491_200x120.jpg" width="200" height="120" alt="Singer Jay Chou Welcomes a Baby Girl" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/07/14 09:15:49</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/07/14/3981s887265.htm" title="Singer Jay Chou Welcomes a Baby Girl" class="title_default">Singer Jay Chou Welcomes a Baby Girl</a></h3>
            <p class="item-infor" title="Mandopop singer Jay Chou is officially a dad. It's been reported that his model wife, Hannah Quinlivan, gave birth to a baby girl on Sunday. <a href="" isAddSummary="common" class="link" ></a>">Mandopop singer Jay Chou is officially a dad. It's been reported that his model wife, Hannah Quinlivan, gave birth to a baby girl on Sunday. <a href="" isAddSummary="common" class="link" ></a></p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/business/finance/Index-up-239-as-stocks-rally-continues/shdaily.shtml" title="Index up 2.39% as stocks rally continues"><img src="" width="" height="" alt="Index up 2.39% as stocks rally continues" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/07/14 09:14:18</em><em class="hide">July 16 2015 09:01:07</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/business/finance/Index-up-239-as-stocks-rally-continues/shdaily.shtml" title="Index up 2.39% as stocks rally continues" class="title_default">Index up 2.39% as stocks rally continues</a></h3>
            <p class="item-infor" title="Chinese stocks extended their rally for a third trading day as government moves to prevent a market rout took effect and more shares resumed trading.">Chinese stocks extended their rally for a third trading day as government moves to prevent a market rout took effect and more shares resumed trading.</p>
          </div>
        </div>
      </div>
      <div class="page-latest-more">
        <a href="#" id="page-latest-show-more"><em class="page-latest-more-icon">&nbsp;</em></a>
        <a href="/news/index.html" id="page-latest-click-more"><em class="page-latest-more-icon">Show More</em></a>
      </div>
    </div><!-- page-latest End -->
    <div class="page-left-ad"><div id='CH_ENG_TL_00001' class='adclass' pushtype='no'></div></div> 
    <div class="page-video">
      <h2 class="modTit"><strong><a href="/video/index.html">VIDEO</a></strong></h2>
      <div class="page-video-body">
        <div class="page-video-left">
          <script type="text/javascript" src="http://c.wrating.com/v2_pre.js"></script>
<!--noscript-->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="420" height="234">
            <param name="movie" value="http://english.china.com/videoPlayer/video.swf"/>
            <param name="quality" value="high"/>
            <param name="bgcolor" value="#ffffff"/>
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="true"/>
            <param name="wmode" value="Opaque">
            <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/07/0707todayinhistory.mp4&loop=1&autoplay=0"/>
            <!--[if!IE]>
            -->
            <object type="application/x-shockwave-flash" data="http://english.china.com/videoPlayer/video.swf" width="420" height="234">
              <param name="quality" value="high"/>
              <param name="bgcolor" value="#ffffff"/>
              <param name="allowScriptAccess" value="sameDomain"/>
              <param name="allowFullScreen" value="true"/>
              <param name="wmode" value="Opaque">
              <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/07/0707todayinhistory.mp4&loop=1&autoplay=0"/>
              <!--<![endif]-->
              <!--[if gte IE 6]>
              -->
              <p>
                Either scripts and active content are not permitted to run or Adobe Flash Player version 11.4.0 or greater is not installed.
              </p>
              <!--<![endif]-->
              <a href="http://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player"/>
              </a>
              <!--[if!IE]>--></object>
            <!--<![endif]-->
          </object>
          <!--/noscript-->
          <a href="/home/videobig/1299/20150707/412685.html" class="video-tit">Today in History: The War against Japanese Aggression, 1937.7.7</a>
        </div>
        <div class="page-video-right" id="page-video-right">
          <div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20150505/365661.html"><img src="http://img02.english.china.com/home/videosmall/1301/20150505/365674_88751.jpg" width="245" height="125" alt="My Chinese Life: Mark O'Connell--Driving Chinese Golf up to Par" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20150505/365661.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">My Chinese Life: Mark O'Connell--Driving Chinese Golf up to Par</strong></h3>
              <p class="item-infor">Coming from the most famous golf course in South Africa, Mark injects new life into the Chinese game. </p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div><div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20150420/350839.html"><img src="http://img04.english.china.com/home/videosmall/1301/20150421/352084_82774.jpg" width="245" height="125" alt="My Chinese Life: Andile Munyai--Back to School in Beijing" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20150420/350839.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">My Chinese Life: Andile Munyai--Back to School in Beijing</strong></h3>
              <p class="item-infor"></p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div><!-- page-video End -->
    <div class="page-mods">
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/travel/index.html">TRAVEL</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners"><img src="http://img03.english.china.com/travel/listright/mostpopular/1534/20150506/366181_88898.jpg" width="330" height="190" alt="Food Awards 2015: the winners" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners" class="title_default">Food Awards 2015: the winners</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/6566/2014/12/25/44s858261.htm" title="Winter Nadam Kicks Off in North China">Winter Nadam Kicks Off in North China</a></li><li><a href="http://english.cri.cn/6566/2014/09/29/44s845986.htm" title="To Experience Authentic Taiwan Folk Art in Beijing">To Experience Authentic Taiwan Folk Art in Beijing</a></li><li><a href="http://english.cri.cn/6566/2014/09/16/44s844347.htm" title="Italian Style Street">Italian Style Street</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/lifestyle/index.html">LIFESTYLE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.china.com/lifestyle/other/154/20140923/131702.html"><img src="http://img03.english.china.com/home/lifepic/1307/20140923/131709_42205.jpg" width="330" height="190" alt="2014 Hilton Beijing Food & Wine Experience" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.china.com/lifestyle/other/154/20140923/131702.html">2014 Hilton Beijing Food & Wine Experience</a></div>
            <div class="focusTopic_txt"><p><a href="" isAddSummary="" class="link" ></a></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://en.damai.cn/event/tickets_66155/">Hit FM Live: Organic Stereo</a></li><li><a href="http://en.damai.cn/event/tickets_66154/">Hit FM Live: DJ Dave Liang</a></li><li><a href="http://en.damai.cn/event/tickets_61544/">TNT Theatre Presents 'Romeo and Juliet'</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/audio/index.html">AUDIO</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/07/15/3801s887500.htm" title="New Horizons Calls Home, Confirming Successful Pluto Flyby"><img src="http://img02.english.china.com/audio/smallfocus/79/20150715/419297_105011.jpg" width="330" height="190" alt="New Horizons Calls Home, Confirming Successful Pluto Flyby" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/07/15/3801s887500.htm" title="New Horizons Calls Home, Confirming Successful Pluto Flyby" class="title_default">New Horizons Calls Home, Confirming Successful Pluto Flyby</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12394/2015/07/15/2743s887473.htm" title="China's Economy Grows 7 Pct in Q2, Slightly Better than Expected" class="title_default">China's Economy Grows 7 Pct in Q2, Slightly Better than Expected</a></li><li><a href="http://english.cri.cn/12394/2015/07/14/3941s887351.htm" title="Mexico Hunts Escaped Drug Lord" class="title_default">Mexico Hunts Escaped Drug Lord</a></li><li><a href="http://english.cri.cn/12394/2015/07/15/4021s887394.htm" title="Gold Culture Treasure Returned to China" class="title_default">Gold Culture Treasure Returned to China</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/chinese/index.html">LEARN CHINESE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love"><img src="http://img04.english.china.com/home/learnpic/1315/20141124/211729_51886.jpg" width="330" height="190" alt="China's First Love" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love">China's First Love</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12514/2014/10/16/2001s848110.htm">Top 10 Popular Chinese TV Dramas Overseas</a></li><li><a href="http://english.cri.cn/12514/2014/10/17/2001s848240.htm">çµç¶ Chinese Pipa</a></li><li><a href="http://english.cri.cn/12514/2014/09/25/2001s845407.htm">Useful Shopping Sentences in Chinese</a></li>
          </ul>
        </div>

      </div>
    </div><!-- page-mods End -->
    <!--<div class="page-left-ad"><a href="#"><img src="/file/left-ad-2.jpg" /></a></div> page-left-ad End -->
    <div class="page-photos">
      <h2 class="modTit"><strong><a href="/photos/index.html">PHOTOS</a></strong></h2>
      <div class="page-focus" id="page-photos">
        <div class="page-focus-body" id="page-photos-body">
          <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://en.people.cn/n/2015/0715/c98649-8920668.html" title="The Lotus Blossoms in China's No.1 Lotus Village"><img src="http://img04.english.china.com/photos/focus/84/20150715/419124_104974.jpg" width="680" height="425" alt="The Lotus Blossoms in China's No.1 Lotus Village" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://en.people.cn/n/2015/0715/c98649-8920668.html" title="The Lotus Blossoms in China's No.1 Lotus Village" class="title_default">The Lotus Blossoms in China's No.1 Lotus Village</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-07/14/c_134412023.htm" title="Summer Universiade closes in Gwangju, S. Korea"><img src="http://img01.english.china.com/photos/focus/84/20150714/418465_104715.jpg" width="680" height="425" alt="Summer Universiade closes in Gwangju, S. Korea" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-07/14/c_134412023.htm" title="Summer Universiade closes in Gwangju, S. Korea" class="title_default">Summer Universiade closes in Gwangju, S. Korea</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.ecns.cn/visual/hd/2015/07-13/70988.shtml" title="Zoo tries all methods to keep animals cool in Jinan"><img src="http://img03.english.china.com/photos/focus/84/20150714/417782_104493_680x425.jpg" width="680" height="425" alt="Zoo tries all methods to keep animals cool in Jinan" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.ecns.cn/visual/hd/2015/07-13/70988.shtml" title="Zoo tries all methods to keep animals cool in Jinan" class="title_default">Zoo tries all methods to keep animals cool in Jinan</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.ecns.cn/visual/hd/2015/07-13/70983.shtml" title="Sanfu Paste: Traditional Chinese Medicine Treatment in Summer"><img src="http://img02.english.china.com/photos/focus/84/20150713/417653_104469.jpg" width="680" height="425" alt="Sanfu Paste: Traditional Chinese Medicine Treatment in Summer" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.ecns.cn/visual/hd/2015/07-13/70983.shtml" title="Sanfu Paste: Traditional Chinese Medicine Treatment in Summer" class="title_default">Sanfu Paste: Traditional Chinese Medicine Treatment in Summer</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/07/12/4082s887097.htm" title="Dunhuang Witnesses Travel Peak as Summer Holidays Approaching"><img src="http://img04.english.china.com/photos/focus/84/20150713/416907_104215_680x425.jpg" width="680" height="425" alt="Dunhuang Witnesses Travel Peak as Summer Holidays Approaching" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/07/12/4082s887097.htm" title="Dunhuang Witnesses Travel Peak as Summer Holidays Approaching" class="title_default">Dunhuang Witnesses Travel Peak as Summer Holidays Approaching</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.ecns.cn/visual/hd/2015/07-12/70892.shtml#nextpage" title="Paddy Harvest Starts in China's Jiangxi"><img src="http://img02.english.china.com/photos/focus/84/20150712/416548_104150.jpg" width="680" height="425" alt="Paddy Harvest Starts in China's Jiangxi" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.ecns.cn/visual/hd/2015/07-12/70892.shtml#nextpage" title="Paddy Harvest Starts in China's Jiangxi" class="title_default">Paddy Harvest Starts in China's Jiangxi</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        </div>
        <div id="page-photos-console"></div>
        <div class="page-focus-prevnext">
          <a href="#" id="page-photos-prev">Previous focus</a>
          <a href="#" id="page-photos-next">Next focus</a>
        </div>
      </div>
    </div><!-- page-photos End -->
  </div><!-- page-left End -->
  <div class="page-right">
    
    <div class="page-right-ad noMarginTop">
      <div id='CH_ENG_HZH_00001' class='adclass' pushtype='no'></div>
    </div><!-- page-right-ad End -->
    <!--include virtual="/etc/right_top_ad_index.shtml" -->
      
    <!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://english.cri.cn/12394/2015/06/25/Zt2982s884527.htm"><img src="http://img01.english.china.com/home/imgtj/5829/20150626/404782_100497.jpg" width="300" height="90" alt="likeqiang" /></a><a href="http://propellertv.co.uk/radio"><img src="http://img04.english.china.com/home/imgtj/5829/20150624/402926_99972.jpg" width="300" height="90" alt="propellertv" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->

    <!-- page-right-ad End -->
    
    <div class="page-watched">
      <h2 class="modTit"><strong>Most Watched</strong></h2>
      <div class="page-watched-body" id="rank-video">
      </div>
    </div><!-- page-watched End -->
    <div class="page-popular">
      <h2 class="modTit"><strong>Most Popular</strong></h2>
      <ul class="page-popular-body" id="rank-list"></ul>
    </div><!-- page-popular End -->
    <div class="page-right-ad"><div id='CH_ENG_AN_00001' class='adclass' pushtype='no'></div></div> 
    <div class="page-tochina">
      <h2 class="modTit"><strong>Tune in to China</strong></h2>
      <div class="page-tochina-body">
        <div class="item radio-news">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="title">-</h3>
              <h4 id="playtime">-</h4>
              <a href="http://english.cri.cn/7146/2012/12/03/301s736372.htm" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/am846.wsx" class="item-play"></a>
          </div>
        </div>
        <div class="item radio-ez">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="nexttitle">-</h3>
              <h4 id="nexttime">-</h4>
              <a href="http://english.cri.cn/easyfm/ezplaytime.html" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/fm915.wsx" class="item-play"></a>
          </div>
        </div>
      </div>
    </div><!-- page-tochina End -->

    
    <div class="page-hotListening">
      <h2 class="page-hotListening-tit"><strong>Hot Listening</strong></h2>
      <div class="page-hotListening-body">
        <div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/pik.htm"><img src="http://img03.english.china.com/audio/right/hot/1691/20131224/4933_1370.jpg" width="120" height="90" alt="People in the Know" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/pik.htm">People in the Know</a><a href="http://english.cri.cn/cribb/plus/pik.htm" class="icon-horn"></a></h3>
            <p class="item-infor">PIK gives you insights to the world through interviews.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/today.htm"><img src="http://img01.english.china.com/audio/right/hot/1691/20131224/4931_1369.jpg" width="120" height="90" alt="Today" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/today.htm">Today</a><a href="http://english.cri.cn/cribb/plus/today.htm" class="icon-horn"></a></h3>
            <p class="item-infor">A news magazine show with in-depth panel discussions.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/easymorning.html"><img src="http://img04.english.china.com/audio/right/hot/1691/20131224/4928_1368.jpg" width="120" height="90" alt="EZ Morning" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/easymorning.html">EZ Morning</a><a href="http://english.cri.cn/easyfm/easymorning.html" class="icon-horn"></a></h3>
            <p class="item-infor">It lights up your mornings with interesting chit-chats.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/ezwheel.html"><img src="http://img01.english.china.com/audio/right/hot/1691/20131224/4927_1367.jpg" width="120" height="90" alt="More to Learn" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/ezwheel.html">More to Learn</a><a href="http://english.cri.cn/easyfm/ezwheel.html" class="icon-horn"></a></h3>
            <p class="item-infor">More to Learn is filled up with English stories and anecdotes.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/hour.html"><img src="http://img01.english.china.com/audio/right/hot/1691/20131224/4923_1366.jpg" width="120" height="90" alt="The Beijing Hour" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/hour.html">The Beijing Hour</a><a href="http://english.cri.cn/easyfm/hour.html" class="icon-horn"></a></h3>
            <p class="item-infor">It opens up the world to you with latest news updates.</p>
          </div>
        </div>
      </div>
    </div><!-- page-hotListening End -->

    <div class="page-mobile">
      <h2 class="modTit"><strong>Mobile</strong></h2>
      <div class="page-mobile-body">
        <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=539404062&mt=8&s=143441" class="iphone" title="Mobile Iphone">Iphone</a>
      </div>
    </div><!-- page-mobile End -->

    <div class="page-cooperation">
      <h2 class="modTit"><strong>Cooperation</strong></h2>
      <div class="page-cooperation-body">
        <a href="http://gc.wrating.com/click.php?a=&c=860099-1000099998&cs=341_285_2559_860010_400000000&ds=354_355_356_357_358&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dmobi.mgeek.TunnyBrowser%26referrer%3Dchannel_id%253Dchinacom%2526utm_source%253Dchinacom"><img src="/file/logo-dolphin-140-90.png" height="90" width="140"></a>
      </div>
    </div><!-- page-cooperation End -->

  </div><!-- page-right End -->
</div><!-- page-main End -->

<script>
window.collectMethod_rank = window.collectMethod_rank || [];
collectMethod_rank.push(function () {
  setRank("rank-video", 3, "http://english.china.com/js/english_43_day.js", "video", function(){
    setRank("rank-list", 5, "http://english.china.com/js/english_22_day.js", "list");
  });
});
</script>

<!-- /etc/channelsitemap.shtml Start -->
<div class="page-map">
  <div class="page-map-body maxWidth">
  <dl class="item">
      <dt><a href="http://english.china.com/news/index.html">News:</a></dt>
      <dd>
        <a href="http://english.china.com/news/china/index.html">China</a>
        <a href="http://english.china.com/news/world/index.html">World</a>
        <a href="http://english.china.com/news/business/index.html">Business</a>
        <a href="http://english.china.com/news/sports/index.html">Sports</a>
        <a href="http://english.china.com/news/showbiz/index.html">Showbiz</a>
		<a href="http://english.china.com/audio/index.html">Audio</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/video/index.html">Video:</a></dt>
      <dd>
        <a href="http://english.china.com/video/c4/index.html">C4</a>
        <a href="http://english.china.com/video/life/index.html">My Chinese Life</a>
        <a href="http://english.china.com/video/thesoundstage/index.html">The Sound Stage</a>
        <a href="http://english.china.com/video/chinarevealed/index.html">China Revealed</a>
        <a href="http://english.china.com/video/showbiz/index.html">Showbiz Video</a>
        <a href="http://english.china.com/video/tour/index.html">Travel Video</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/photos/index.html">Photos:</a></dt>
      <dd>
        <a href="http://english.china.com/photos/china/index.html">China</a>
        <a href="http://english.china.com/photos/world/index.html">World</a>
        <a href="http://english.china.com/photos/fun/index.html">Fun</a>
        <a href="http://english.china.com/photos/travel/index.html">Travel</a>
        <a href="http://english.china.com/photos/entertainment/index.html">Entertainment</a>
        <a href="http://english.china.com/photos/sports/index.html">Sports</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/travel/index.html">Travel:</a></dt>
      <dd>
        <a href="http://english.china.com/travel/beijing/index.html">Beijing</a>
        <a href="http://english.china.com/travel/shanghai/index.html">Shanghai</a>
        <a href="http://english.china.com/travel/guangzhou/index.html">Guangzhou</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/lifestyle/index.html">Lifestyle:</a></dt>
      <dd>        
        <a href="http://english.china.com/lifestyle/livemusic/index.html">Live Music</a>
        <a href="http://english.china.com/lifestyle/opera/index.html">Opera & Classical</a>
        <a href="http://english.china.com/lifestyle/movies/index.html">Movies</a>
        <a href="http://english.china.com/lifestyle/traditionalshows/index.html">Traditional Shows</a>
        <a href="http://english.china.com/lifestyle/exhibitions/index.html">Exhibitions</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/chinese/index.html">Learn Chinese:</a></dt>
      <dd>
        <a href="http://english.china.com/chinese/studio/index.html">Chinese Studio</a>
        <a href="http://english.china.com/chinese/living/index.html">Living Chinese</a>
        <a href="http://english.china.com/chinese/everyday/index.html">Everyday Chinese</a>
        <a href="http://english.china.com/chinese/justforfun/index.html">Just For Fun</a>
        <a href="http://english.china.com/chinese/culture/index.html">Chinese Culture</a>
        <a href="http://english.china.com/chinese/buzzwords/index.html">Buzzwords</a>        
      </dd>
    </dl>      
  </div>
</div><!-- page-map End -->
<!-- /etc/channelsitemap.shtml End -->

<div class="page-link">
  <div class="page-link-body maxWidth">
    <a href="http://english.cri.cn/">CRIENGLISH.com</a>|<a href="http://www.chinadaily.com.cn/">China Daily</a>|<a href="http://www.xinhuanet.com/english/">Xinhua</a>|<a href="http://www.china.org.cn/index.htm">China.org.cn</a>|<a href="http://english.cntv.cn/">CNTV</a>
  </div>
</div><!-- page-link End -->

<!-- /etc/channelcopyright.shtml Start -->
<div class="page-footer">
  <div class="page-foot-link">
    <a href="/about/">About China.com</a>|<a href="/about/gmg.html">About GMG</a>|<a href="/ad/">Ad Services</a>|<a href="/contact/">Contact Information</a>|<a href="/copyright/">Copyright Notice</a>
  </div>
  <p><!--E-mail to:<a href="mailto:english@bj.china.com">english@bj.china.com</a><br />-->Copyright &copy; China.com All Rights Reserved</p>
</div><!-- page-footer End -->

<script src="/js/require.min.js" data-main="/js/main"></script>

<!-- START WRating v1.0 -->
<script type="text/javascript" src="http://c.wrating.com/a1.js">
</script>
<script type="text/javascript">
var vjAcc="860010-0446010000";
var wrUrl="http://c.wrating.com/";
vjTrack("");
</script>
<noscript><img src="http://c.wrating.com/a.gif?a=&c=860010-0446010000" width="1" 
height="1"/></noscript>
<!-- END WRating v1.0 -->

<!-- Start Alexa Certify Javascript #13481-->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S6Upi1awA+00a/", domain:"china.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S6Upi1awA+00a/" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript #13481-->


<!-- Start Google Analytics #16010-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60581848-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics #16010-->

<!-- /etc/channelcopyright.shtml End -->
</body>
</html>