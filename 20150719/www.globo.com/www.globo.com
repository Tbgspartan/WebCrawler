



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='18/07/2015 21:56:28' /><meta property='busca:modified' content='18/07/2015 21:56:28' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/c482b37cd4ff.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/12ba0da8a716.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class=""><div class="v-separator"></div><a target="_top" href="http://m.g1.globo.com" accesskey="n" class="barra-item-g1 link-produto">g1</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://m.globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto">globoesporte</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto">gshow</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto">famosos &amp; etc</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://m.techtudo.com.br" accesskey="b" class="barra-item-tech link-produto">tecnologia</a></li><li class=""><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "globo.com/globo.com/home", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo" href="http://m.globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-nacional/">
                                                <span class="titulo">Jornal Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/radar-g1/platb/">
                                                    <span class="titulo">TrÃ¢nsito</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/volei/">
                                                <span class="titulo">VÃ´lei</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-america/">
                                                    <span class="titulo">Copa AmÃ©rica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                <span class="titulo">Estrelas</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/superstar/2015">
                                                <span class="titulo">SuperStar</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/esquenta/">
                                                    <span class="titulo">Esquenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/superstar/2015">
                                                    <span class="titulo">SuperStar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/pe-na-cova/index.html">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/moda/index.html">
                                                <span class="titulo">Moda no Ego</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-07-1821:54:51Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/sp/futebol/brasileirao-serie-a/jogo/18-07-2015/corinthians-atletico-mg/" class="foto " title="Corinthians sai na frente do Galo; siga (Marcos Ribolli)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/iQQUeWeocCZoHSPm6XSp_q-WZuw=/filters:quality(10):strip_icc()/s2.glbimg.com/3QfLClL65E3SfhtjEkBRnyinK6M=/416x194:1498x565/335x115/s.glbimg.com/es/ge/f/original/2015/07/18/pp95731_1.jpg" alt="Corinthians sai na frente do Galo; siga (Marcos Ribolli)" title="Corinthians sai na frente do Galo; siga (Marcos Ribolli)"
         data-original-image="s2.glbimg.com/3QfLClL65E3SfhtjEkBRnyinK6M=/416x194:1498x565/335x115/s.glbimg.com/es/ge/f/original/2015/07/18/pp95731_1.jpg" data-url-smart_horizontal="jg0zD7iMTIyPDERZ4byhQunxt1A=/400xorig/smart/filters:strip_icc()/" data-url-smart="jg0zD7iMTIyPDERZ4byhQunxt1A=/400xorig/smart/filters:strip_icc()/" data-url-feature="jg0zD7iMTIyPDERZ4byhQunxt1A=/400xorig/smart/filters:strip_icc()/" data-url-tablet="D-GIsKilHLABAdqla1oN8YsOg6c=/340xorig/smart/filters:strip_icc()/" data-url-desktop="rlMOFRsn0pMHx-pDumpnEXNQ9ZM=/335xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Corinthians sai na frente do Galo; siga</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/jogos-pan-americanos/no-ar/jogos-pan-americanos-toronto-2015.html#/glb-feed-post/55aaf50e113393787b880dd6" class="foto " title="Thiago vira maior da histÃ³ria do Pan com ouro do Brasil no revezamento (Satiro Sodre/SSPress)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/VUby7TOlTgx3T1UejJ2epC5TtoM=/filters:quality(10):strip_icc()/s2.glbimg.com/mIJKQ1SbSdX7p2Br6M1WN85OTOA=/0x320:2362x870/335x78/s.glbimg.com/es/ge/f/original/2015/07/18/a71q3315.jpg" alt="Thiago vira maior da histÃ³ria do Pan com ouro do Brasil no revezamento (Satiro Sodre/SSPress)" title="Thiago vira maior da histÃ³ria do Pan com ouro do Brasil no revezamento (Satiro Sodre/SSPress)"
         data-original-image="s2.glbimg.com/mIJKQ1SbSdX7p2Br6M1WN85OTOA=/0x320:2362x870/335x78/s.glbimg.com/es/ge/f/original/2015/07/18/a71q3315.jpg" data-url-smart_horizontal="izoIsCYK5cZBVN5TVoo-sbmVZiU=/90x56/smart/filters:strip_icc()/" data-url-smart="izoIsCYK5cZBVN5TVoo-sbmVZiU=/90x56/smart/filters:strip_icc()/" data-url-feature="izoIsCYK5cZBVN5TVoo-sbmVZiU=/90x56/smart/filters:strip_icc()/" data-url-tablet="j2RUEWiUyLoXCn3r2kb7bNRju3M=/340xorig/smart/filters:strip_icc()/" data-url-desktop="ro2B80c8phwFTZt9T4y2V4574XY=/335xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Thiago vira maior da histÃ³ria do Pan com ouro do Brasil no revezamento</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Siga as finais da nataÃ§Ã£o e a cobertura completa do Pan" href="http://globoesporte.globo.com/jogos-pan-americanos/no-ar/jogos-pan-americanos-toronto-2015.html">Siga as finais da nataÃ§Ã£o e a cobertura completa do Pan</a></div></li><li><div class="mobile-grid-partial"><a title="Yane Marques conquista a medalha de ouro no pentatlo" href="http://globoesporte.globo.com/jogos-pan-americanos/noticia/2015/07/com-mae-na-torcida-yane-marques-leva-o-ouro-e-bi-no-pentatlo-moderno.html">Yane Marques conquista a medalha de ouro no pentatlo</a></div></li></ul></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/babilonia/Vem-por-ai/noticia/2015/07/rafael-recebe-cantada-de-cecilia.html" class="foto " title="&#39;BabilÃ´nia&#39;: ex investe em Rafa (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/dLhDOYXBbDj6XyuIOagOUX_KnnA=/filters:quality(10):strip_icc()/s2.glbimg.com/f0eQjJpGaUiij91oyrc2R7pIa-s=/0x50:690x393/155x77/s.glbimg.com/et/gs/f/original/2015/07/17/rafael.jpg" alt="&#39;BabilÃ´nia&#39;: ex investe em Rafa (TV Globo)" title="&#39;BabilÃ´nia&#39;: ex investe em Rafa (TV Globo)"
         data-original-image="s2.glbimg.com/f0eQjJpGaUiij91oyrc2R7pIa-s=/0x50:690x393/155x77/s.glbimg.com/et/gs/f/original/2015/07/17/rafael.jpg" data-url-smart_horizontal="cF5zTxf1wLWlDhksVVZIcHEFXr0=/90x56/smart/filters:strip_icc()/" data-url-smart="cF5zTxf1wLWlDhksVVZIcHEFXr0=/90x56/smart/filters:strip_icc()/" data-url-feature="cF5zTxf1wLWlDhksVVZIcHEFXr0=/90x56/smart/filters:strip_icc()/" data-url-tablet="ZYe8Qx2Yn40ZzenQVAb61MVvJaE=/160xorig/smart/filters:strip_icc()/" data-url-desktop="hRt9ecFVQ_VpdbsO_xvUZ9N3CuY=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;BabilÃ´nia&#39;: ex investe em Rafa</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Beatriz interrompe papo" href="http://gshow.globo.com/novelas/babilonia/Vem-por-ai/noticia/2015/07/beatriz-interrompe-conversa-amigavel-entre-regina-e-ines.html">Beatriz interrompe papo</a></div></li></ul></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/rj/futebol/brasileirao-serie-a/jogo/18-07-2015/flamengo-gremio/" class="foto " title="Guerrero deixa o seu no Maraca, e Fla bate GrÃªmio (Marcello Dias/Futura Press)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/3_-fOOeQ8UuilgKg3MxhIU_Asnw=/filters:quality(10):strip_icc()/s2.glbimg.com/HbkhOC6tZmsgB8GYYnLyn29ZIiw=/339x422:2026x1676/155x115/s.glbimg.com/es/ge/f/original/2015/07/18/20150718_guerrero-flamengo-x-gremio_futurapress.jpg" alt="Guerrero deixa o seu no Maraca, e Fla bate GrÃªmio (Marcello Dias/Futura Press)" title="Guerrero deixa o seu no Maraca, e Fla bate GrÃªmio (Marcello Dias/Futura Press)"
         data-original-image="s2.glbimg.com/HbkhOC6tZmsgB8GYYnLyn29ZIiw=/339x422:2026x1676/155x115/s.glbimg.com/es/ge/f/original/2015/07/18/20150718_guerrero-flamengo-x-gremio_futurapress.jpg" data-url-smart_horizontal="u1_A5gQVAwQ3eYr3acQLiEnvCsc=/90x56/smart/filters:strip_icc()/" data-url-smart="u1_A5gQVAwQ3eYr3acQLiEnvCsc=/90x56/smart/filters:strip_icc()/" data-url-feature="u1_A5gQVAwQ3eYr3acQLiEnvCsc=/90x56/smart/filters:strip_icc()/" data-url-tablet="fdsMv6430eYpYLFptIXbWTH8KTE=/160xorig/smart/filters:strip_icc()/" data-url-desktop="w0HhAg8L3VVWGY1eB3tBvdrDi8s=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Guerrero deixa o seu no Maraca, e Fla bate GrÃªmio</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/rs/futebol/brasileirao-serie-a/jogo/18-07-2015/internacional-goias/" class="foto " title="Inter vence com reservas e GoiÃ¡s pode ir para Z-4 (Wesley Santos/AgÃªncia PressDigital)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/uf7l50_mTkbRc5xaWu6KL1_dRW8=/filters:quality(10):strip_icc()/s2.glbimg.com/PSYLxIOPJMRvHVbgqwKxi29Ygdg=/1330x296:2269x992/155x115/s.glbimg.com/es/ge/f/original/2015/07/18/mg_0287.jpg" alt="Inter vence com reservas e GoiÃ¡s pode ir para Z-4 (Wesley Santos/AgÃªncia PressDigital)" title="Inter vence com reservas e GoiÃ¡s pode ir para Z-4 (Wesley Santos/AgÃªncia PressDigital)"
         data-original-image="s2.glbimg.com/PSYLxIOPJMRvHVbgqwKxi29Ygdg=/1330x296:2269x992/155x115/s.glbimg.com/es/ge/f/original/2015/07/18/mg_0287.jpg" data-url-smart_horizontal="rM6qdwKwhmIPWB1Oc4aYiUWtwTc=/90x56/smart/filters:strip_icc()/" data-url-smart="rM6qdwKwhmIPWB1Oc4aYiUWtwTc=/90x56/smart/filters:strip_icc()/" data-url-feature="rM6qdwKwhmIPWB1Oc4aYiUWtwTc=/90x56/smart/filters:strip_icc()/" data-url-tablet="u5Aw2SAxkcUM6iQNcX0jwy33suo=/160xorig/smart/filters:strip_icc()/" data-url-desktop="jhKNv6NdNJ3Wjn_xAAoftC4n92U=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Inter vence com reservas e GoiÃ¡s pode ir para Z-4</h2></div></a></div></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/jornal-nacional/noticia/2015/07/relatorio-mostra-que-petrobras-teve-prejuizo-em-contrato-com-braskem.html" class=" " title="&#39;JN&#39;: Petrobras ficou no prejuÃ­zo com a Braskem"><div class="conteudo"><h2>&#39;JN&#39;: Petrobras ficou no prejuÃ­zo com a Braskem</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://epocanegocios.globo.com/Informacao/Acao/noticia/2015/07/marcelo-odebrecht-organizou-jantar-pedido-de-lula.html" class=" " title="Odebrecht fez jantar a pedido de Lula, diz PF"><div class="conteudo"><h2>Odebrecht fez jantar a pedido de Lula, diz PF</h2></div></a></div></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/07/cunha-diz-que-nao-existe-pauta-de-vinganca-contra-o-planalto.html" class=" " title="Cunha adota tom moderado e nega &#39;pauta de vinganÃ§a&#39; contra Planalto"><div class="conteudo"><h2>Cunha adota tom moderado e nega &#39;pauta de vinganÃ§a&#39; contra Planalto</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="PMDB enxerga o rompimento como ato de &#39;descontrole&#39;" href="http://oglobo.globo.com/brasil/rompimento-de-cunha-visto-como-ato-de-descontrole-pelo-pmdb-16823163">PMDB enxerga o rompimento como ato de &#39;descontrole&#39;</a></div></li></ul></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="mobile-grid-partial"><a href="http://gshow.globo.com/tv/noticia/2015/07/larissa-se-afunda-no-vicio-e-grazi-massafera-e-exaltada-nas-redes.html" class="foto " title="Grazi agita a web na cena de crack (Felipe Monteiro/ Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/tG6y44mznrUjapFN_UOLNEpC0oE=/filters:quality(10):strip_icc()/s2.glbimg.com/4ae7iHFRc50m_7SmN-oGWt9vJw4=/75x35:596x294/155x77/s.glbimg.com/et/gs/f/original/2015/07/16/larissa-roy-sam-verdades-secretas.jpg" alt="Grazi agita a web na cena de crack (Felipe Monteiro/ Gshow)" title="Grazi agita a web na cena de crack (Felipe Monteiro/ Gshow)"
         data-original-image="s2.glbimg.com/4ae7iHFRc50m_7SmN-oGWt9vJw4=/75x35:596x294/155x77/s.glbimg.com/et/gs/f/original/2015/07/16/larissa-roy-sam-verdades-secretas.jpg" data-url-smart_horizontal="fFAFDzSvBPU6RshUotm1edLdm6k=/90x56/smart/filters:strip_icc()/" data-url-smart="fFAFDzSvBPU6RshUotm1edLdm6k=/90x56/smart/filters:strip_icc()/" data-url-feature="fFAFDzSvBPU6RshUotm1edLdm6k=/90x56/smart/filters:strip_icc()/" data-url-tablet="nYzn3PnBiEK1qMduIPQrTH34nbg=/160xorig/smart/filters:strip_icc()/" data-url-desktop="bR56yyOZPviMVTNsv-r1JnvIVYQ=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Grazi agita a web na cena de crack</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Verdades: Alex vai atrÃ¡s" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/07/em-plena-lua-de-mel-alex-ira-atras-de-angel-no-quarto-dela.html">Verdades: Alex vai atrÃ¡s</a></div></li></ul></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/busca/?q=gol">brasileirÃ£o</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-flamengo-apos-bate-rebate-guerrero-manda-para-o-fundo-da-rede-aos-40-do-1o-tempo/4331528/"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-flamengo-apos-bate-rebate-guerrero-manda-para-o-fundo-da-rede-aos-40-do-1o-tempo/4331528/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4331528.jpg" alt="Na estreia pelo Fla no MaracanÃ£, Guerrero marca e garante triunfo" /><div class="time">00:45</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">Flamengo 1 x 0 GrÃªmio</span><span class="title">Na estreia pelo Fla no MaracanÃ£, Guerrero marca e garante triunfo</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-inter-apos-confusao-na-area-eduardo-pega-o-rebote-e-abre-o-placar-aos-11-do-2o-tempo/4331675/"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-inter-apos-confusao-na-area-eduardo-pega-o-rebote-e-abre-o-placar-aos-11-do-2o-tempo/4331675/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4331675.jpg" alt="Gol! GoleirÃ£o sai mal, e Eduardo nÃ£o desperdiÃ§a a oportunidade" /><div class="time">00:45</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">internacional 2 x 1 goiÃ¡s</span><span class="title">Gol! GoleirÃ£o sai mal, e Eduardo nÃ£o desperdiÃ§a a oportunidade</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-inter-renan-falha-feio-e-bola-sobra-para-eduardo-sasha-marcar-aos-42-do-2o-tempo/4331904/"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-inter-renan-falha-feio-e-bola-sobra-para-eduardo-sasha-marcar-aos-42-do-2o-tempo/4331904/"><div class="image-container"><div class="image-wrapper"><img src="http://s2.glbimg.com/1U5-Bt61o1FP1je7z6NooTLAgnQ=/0x18:636x350/335x175/s.glbimg.com/en/ho/f/original/2015/07/18/inter.jpg" alt="CaÃ§ando borboleta! Renan deixa cruzamento escapar, e Sasha faz" /><div class="time">00:45</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">Internacional 2 x 1 GoiÃ¡s</span><span class="title">CaÃ§ando borboleta! Renan deixa cruzamento escapar, e Sasha faz</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-flamengo-apos-bate-rebate-guerrero-manda-para-o-fundo-da-rede-aos-40-do-1o-tempo/4331528/"><span class="subtitle">Flamengo 1 x 0 GrÃªmio</span><span class="title">Na estreia pelo Fla no MaracanÃ£, Guerrero marca e garante triunfo</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-inter-apos-confusao-na-area-eduardo-pega-o-rebote-e-abre-o-placar-aos-11-do-2o-tempo/4331675/"><span class="subtitle">internacional 2 x 1 goiÃ¡s</span><span class="title">Gol! GoleirÃ£o sai mal, e Eduardo nÃ£o desperdiÃ§a a oportunidade</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/globocom/tempo-real/v/gol-do-inter-renan-falha-feio-e-bola-sobra-para-eduardo-sasha-marcar-aos-42-do-2o-tempo/4331904/"><span class="subtitle">Internacional 2 x 1 GoiÃ¡s</span><span class="title">CaÃ§ando borboleta! Renan deixa cruzamento escapar, e Sasha faz</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-esporte"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/globocom/tempo-real/v/gol-do-flamengo-apos-bate-rebate-guerrero-manda-para-o-fundo-da-rede-aos-40-do-1o-tempo/4331528/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/globocom/tempo-real/v/gol-do-inter-apos-confusao-na-area-eduardo-pega-o-rebote-e-abre-o-placar-aos-11-do-2o-tempo/4331675/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/globocom/tempo-real/v/gol-do-inter-renan-falha-feio-e-bola-sobra-para-eduardo-sasha-marcar-aos-42-do-2o-tempo/4331904/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais<strong> vÃ­deos</strong><span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/noticia/2015/07/video-mostra-acidente-com-carro-da-policia-em-avenida-de-sp.html" class="foto" title="Policial perde controle de viatura da Rota e causa acidente com feridos; veja vÃ­deo (ReproduÃ§Ã£o de VÃ­deo/Cinegrafista amador)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/YSF_daM1gyAkTxDPaIG2niN8uRo=/filters:quality(10):strip_icc()/s2.glbimg.com/E3A00TU5nIQDXVEyua__mX1z4rA=/64x55:406x239/335x180/s.glbimg.com/en/ho/f/original/2015/07/18/acidente-rota.jpg" alt="Policial perde controle de viatura da Rota e causa acidente com feridos; veja vÃ­deo (ReproduÃ§Ã£o de VÃ­deo/Cinegrafista amador)" title="Policial perde controle de viatura da Rota e causa acidente com feridos; veja vÃ­deo (ReproduÃ§Ã£o de VÃ­deo/Cinegrafista amador)"
                data-original-image="s2.glbimg.com/E3A00TU5nIQDXVEyua__mX1z4rA=/64x55:406x239/335x180/s.glbimg.com/en/ho/f/original/2015/07/18/acidente-rota.jpg" data-url-smart_horizontal="zLHTG2gS10i4mcoWI_EKcvC546Q=/90x0/smart/filters:strip_icc()/" data-url-smart="zLHTG2gS10i4mcoWI_EKcvC546Q=/90x0/smart/filters:strip_icc()/" data-url-feature="zLHTG2gS10i4mcoWI_EKcvC546Q=/90x0/smart/filters:strip_icc()/" data-url-tablet="O8pG39jYE3Zw21zMK5P7EGblSpk=/220x125/smart/filters:strip_icc()/" data-url-desktop="4x-NlRfCbVfSjoCf6Om4v99Pt48=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Policial perde controle de viatura da Rota e causa acidente com feridos; veja vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 20:50:41" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/espirito-santo/noticia/2015/07/motorista-desvia-de-gato-e-atropela-e-mata-casal-em-santa-maria-es.html" class="foto" title="Motorista desvia de gato,
 perde controle e mata casal atropelado em cidade do ES (AndrÃ© FalcÃ£o/ TV Gazeta)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/BBCTbhtcnynSCzyatXCsvkBBPvQ=/filters:quality(10):strip_icc()/s2.glbimg.com/Hgulzv-GWDwTWm9U3f5WDiMhK7w=/108x45:865x550/120x80/s.glbimg.com/jo/g1/f/original/2015/07/18/20150718101407.jpg" alt="Motorista desvia de gato,
 perde controle e mata casal atropelado em cidade do ES (AndrÃ© FalcÃ£o/ TV Gazeta)" title="Motorista desvia de gato,
 perde controle e mata casal atropelado em cidade do ES (AndrÃ© FalcÃ£o/ TV Gazeta)"
                data-original-image="s2.glbimg.com/Hgulzv-GWDwTWm9U3f5WDiMhK7w=/108x45:865x550/120x80/s.glbimg.com/jo/g1/f/original/2015/07/18/20150718101407.jpg" data-url-smart_horizontal="DANZd1152QSmTEWkSaFeDdNyUFg=/90x0/smart/filters:strip_icc()/" data-url-smart="DANZd1152QSmTEWkSaFeDdNyUFg=/90x0/smart/filters:strip_icc()/" data-url-feature="DANZd1152QSmTEWkSaFeDdNyUFg=/90x0/smart/filters:strip_icc()/" data-url-tablet="UE9gHXG4expRneS3Dwi3bA1bSvg=/70x50/smart/filters:strip_icc()/" data-url-desktop="I6OCDRKEKB_SlFqtCrS31WtLqDQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Motorista desvia de gato,<br /> perde controle e mata casal atropelado em cidade do ES</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 20:57:00" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/sertanejo/noticia/2015/07/filho-de-cristiano-araujo-sente-falta-do-pai-chama-muito-por-ele-diz-mae.html" class="foto" title="MÃ£e do filho de Cristiano AraÃºjo diz que crianÃ§a sente falta do pai: &#39;Chama por ele&#39; (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/VqAQUkMj4QRA2M-DIxw1Uc4N8vE=/filters:quality(10):strip_icc()/s2.glbimg.com/8Zm2tDbesX703s1N3QPbSGlGiXs=/0x14:639x441/120x80/s.glbimg.com/jo/eg/f/original/2015/07/18/filho_cristiano_araujo.jpg" alt="MÃ£e do filho de Cristiano AraÃºjo diz que crianÃ§a sente falta do pai: &#39;Chama por ele&#39; (ReproduÃ§Ã£o/Instagram)" title="MÃ£e do filho de Cristiano AraÃºjo diz que crianÃ§a sente falta do pai: &#39;Chama por ele&#39; (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/8Zm2tDbesX703s1N3QPbSGlGiXs=/0x14:639x441/120x80/s.glbimg.com/jo/eg/f/original/2015/07/18/filho_cristiano_araujo.jpg" data-url-smart_horizontal="8rpA9huVCKSebvEkVNldei1gODc=/90x0/smart/filters:strip_icc()/" data-url-smart="8rpA9huVCKSebvEkVNldei1gODc=/90x0/smart/filters:strip_icc()/" data-url-feature="8rpA9huVCKSebvEkVNldei1gODc=/90x0/smart/filters:strip_icc()/" data-url-tablet="PWcXfwfP7xnmu70oP7S2_Itcmr0=/70x50/smart/filters:strip_icc()/" data-url-desktop="n0vLSdei-1jz-_K3v6Flrvtee2Y=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>MÃ£e do filho de Cristiano <br />AraÃºjo diz que crianÃ§a sente falta do pai: &#39;Chama por ele&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/casos-de-policia/jovem-de-22-anos-volta-para-casa-apos-sumir-por-causa-de-namoro-virtual-16832113.html#ixzz3gGzHGgVD" class="" title="Jovem que vendeu carro e  fugiu apÃ³s iniciar namoro virtual volta para casa no RJ e se cala (ReproduÃ§Ã£o/Facebook)" rel="bookmark"><span class="conteudo"><p>Jovem que vendeu carro e  fugiu apÃ³s iniciar namoro virtual volta para casa no RJ e se cala</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 18:03:22" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/noticias/rio/nao-realizei-ultimo-pedido-dela-diz-padastro-da-ex-mulher-do-cantor-ferrugem-que-morreu-apos-cirurgia-plastica-16832286.html" class="foto" title="Ex-mulher de pagodeiro morta apÃ³s plÃ¡sticas pagou R$ 30 mil por procedimento (Urbano Erbiste/Extra)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/cF4XahaRreuuEctRqu-omSQ6WAs=/filters:quality(10):strip_icc()/s2.glbimg.com/qC1ZP21f3HWaDeyJ41jeSywNzEE=/73x100:414x327/120x80/s.glbimg.com/en/ho/f/original/2015/07/18/enterro.jpg" alt="Ex-mulher de pagodeiro morta apÃ³s plÃ¡sticas pagou R$ 30 mil por procedimento (Urbano Erbiste/Extra)" title="Ex-mulher de pagodeiro morta apÃ³s plÃ¡sticas pagou R$ 30 mil por procedimento (Urbano Erbiste/Extra)"
                data-original-image="s2.glbimg.com/qC1ZP21f3HWaDeyJ41jeSywNzEE=/73x100:414x327/120x80/s.glbimg.com/en/ho/f/original/2015/07/18/enterro.jpg" data-url-smart_horizontal="8goPU8LEPKujc5EDDs4eeyJ0NRc=/90x0/smart/filters:strip_icc()/" data-url-smart="8goPU8LEPKujc5EDDs4eeyJ0NRc=/90x0/smart/filters:strip_icc()/" data-url-feature="8goPU8LEPKujc5EDDs4eeyJ0NRc=/90x0/smart/filters:strip_icc()/" data-url-tablet="9o9TkyMnSS2FEcKdZC6ciLd8S3A=/70x50/smart/filters:strip_icc()/" data-url-desktop="OAGh0HpXoDmiiBMdqToV-a3J_8w=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ex-mulher de pagodeiro morta apÃ³s plÃ¡sticas pagou R$ 30 mil por procedimento</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 10:07:33" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/07/alem-do-armazenamento-conheca-funcoes-pouco-conhecidas-do-cabo-usb.html" class="foto" title="Descubra funÃ§Ãµes &#39;secretas&#39; da conexÃ£o USB que vocÃª nunca imaginou e saiba usar (TechTudo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/uXgaelQ_gTETgXEU_WA8KFY5hlc=/filters:quality(10):strip_icc()/s2.glbimg.com/oVPs9Sa6x57N7DwVSmSJduxwl6Y=/86x63:564x381/120x80/s.glbimg.com/po/tt2/f/original/2015/03/18/usb.jpg" alt="Descubra funÃ§Ãµes &#39;secretas&#39; da conexÃ£o USB que vocÃª nunca imaginou e saiba usar (TechTudo)" title="Descubra funÃ§Ãµes &#39;secretas&#39; da conexÃ£o USB que vocÃª nunca imaginou e saiba usar (TechTudo)"
                data-original-image="s2.glbimg.com/oVPs9Sa6x57N7DwVSmSJduxwl6Y=/86x63:564x381/120x80/s.glbimg.com/po/tt2/f/original/2015/03/18/usb.jpg" data-url-smart_horizontal="RZY4iPINa8Rt2SwwQebnjp4zlWk=/90x0/smart/filters:strip_icc()/" data-url-smart="RZY4iPINa8Rt2SwwQebnjp4zlWk=/90x0/smart/filters:strip_icc()/" data-url-feature="RZY4iPINa8Rt2SwwQebnjp4zlWk=/90x0/smart/filters:strip_icc()/" data-url-tablet="HfEaB8Fj2Wv0-wxsYfx2M9KjeYU=/70x50/smart/filters:strip_icc()/" data-url-desktop="pYE6iuFVuCKFhozPA8F346ErioA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Descubra funÃ§Ãµes &#39;secretas&#39; da conexÃ£o USB que vocÃª nunca imaginou e saiba usar</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 14:22:33" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/listas/noticia/2015/07/apps-para-android-skype-wifimapper-e-outros-destaques-da-semana.html" class="" title="App grÃ¡tis que &#39;descobre&#39; a senha de qualquer Wi-Fi chega para Android e vira top da semana" rel="bookmark"><span class="conteudo"><p>App grÃ¡tis que &#39;descobre&#39; a senha de qualquer Wi-Fi chega para Android e vira top da semana</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 20:50:41" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pi/piaui/noticia/2015/07/direcao-do-cem-vai-apurar-divulgacao-de-imagens-de-menor-morto-no-piaui.html" class="foto" title="Centro de detenÃ§Ã£o apura vazamento de imagens de menor morto no PiauÃ­ (Catarina Costa/G1 PI)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/caas-l3oAeWsT-KAZ7d0W4yFEgM=/filters:quality(10):strip_icc()/s2.glbimg.com/fALgUb86KWViAaP0VrdVYpmBkfo=/0x0:3452x2304/120x80/s.glbimg.com/jo/g1/f/original/2015/07/17/img_4447.jpg" alt="Centro de detenÃ§Ã£o apura vazamento de imagens de menor morto no PiauÃ­ (Catarina Costa/G1 PI)" title="Centro de detenÃ§Ã£o apura vazamento de imagens de menor morto no PiauÃ­ (Catarina Costa/G1 PI)"
                data-original-image="s2.glbimg.com/fALgUb86KWViAaP0VrdVYpmBkfo=/0x0:3452x2304/120x80/s.glbimg.com/jo/g1/f/original/2015/07/17/img_4447.jpg" data-url-smart_horizontal="w1XrufUFmIkgMDsULUuMkm1NwyY=/90x0/smart/filters:strip_icc()/" data-url-smart="w1XrufUFmIkgMDsULUuMkm1NwyY=/90x0/smart/filters:strip_icc()/" data-url-feature="w1XrufUFmIkgMDsULUuMkm1NwyY=/90x0/smart/filters:strip_icc()/" data-url-tablet="gwcPclZX2kCOamwtlz0gS5eaJTo=/70x50/smart/filters:strip_icc()/" data-url-desktop="uKusk8qqdOOuUBrLsp-PZAqz17s=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Centro de detenÃ§Ã£o apura vazamento de imagens de menor morto no PiauÃ­</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 21:08:33" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/bahia/noticia/2015/07/estouro-de-champagne-motivou-morte-em-pizzaria-dizem-testemunhas.html" class="foto" title="Estouro de champanhe foi a motivaÃ§Ã£o de assassinato em pizzaria, diz testemunha (Imagens / TV Bahia)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/8lfIrVE2b492RBruzMzJyKAJJMY=/filters:quality(10):strip_icc()/s2.glbimg.com/aGfuboFwmmc0U_T8HdxhDC2PS4s=/171x100:458x292/120x80/s.glbimg.com/jo/g1/f/original/2015/07/18/fabio2.jpg" alt="Estouro de champanhe foi a motivaÃ§Ã£o de assassinato em pizzaria, diz testemunha (Imagens / TV Bahia)" title="Estouro de champanhe foi a motivaÃ§Ã£o de assassinato em pizzaria, diz testemunha (Imagens / TV Bahia)"
                data-original-image="s2.glbimg.com/aGfuboFwmmc0U_T8HdxhDC2PS4s=/171x100:458x292/120x80/s.glbimg.com/jo/g1/f/original/2015/07/18/fabio2.jpg" data-url-smart_horizontal="ky9w7fz6NIOHZlaTfI_YoeftG9w=/90x0/smart/filters:strip_icc()/" data-url-smart="ky9w7fz6NIOHZlaTfI_YoeftG9w=/90x0/smart/filters:strip_icc()/" data-url-feature="ky9w7fz6NIOHZlaTfI_YoeftG9w=/90x0/smart/filters:strip_icc()/" data-url-tablet="YxxKr7lr2AnT-QHU3Hg2kizHK_4=/70x50/smart/filters:strip_icc()/" data-url-desktop="pBVEjqKmE2RNYplZOYC_AoYaQng=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Estouro de champanhe foi<br /> a motivaÃ§Ã£o de assassinato em pizzaria, diz testemunha</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/calleri-rouba-festa-de-tevez-para-si-com-golaco-de-letra-por-cobertura.html" class="foto" title="Na volta de Tevez, Boca Jrs. vence com direito a gol de placa e vira lÃ­der; assista (Reuters)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/z0nm8BdMjW2KMX6Uf7jtJa7jfbA=/filters:quality(10):strip_icc()/s2.glbimg.com/EYVjVQCRh9YQAOEzDiVYCufed3c=/434x372:3186x1850/335x180/s.glbimg.com/es/ge/f/original/2015/07/18/2015-07-18t203049z_296853931_gf10000163621_rtrmadp_3_soccer-argentina.jpg" alt="Na volta de Tevez, Boca Jrs. vence com direito a gol de placa e vira lÃ­der; assista (Reuters)" title="Na volta de Tevez, Boca Jrs. vence com direito a gol de placa e vira lÃ­der; assista (Reuters)"
                data-original-image="s2.glbimg.com/EYVjVQCRh9YQAOEzDiVYCufed3c=/434x372:3186x1850/335x180/s.glbimg.com/es/ge/f/original/2015/07/18/2015-07-18t203049z_296853931_gf10000163621_rtrmadp_3_soccer-argentina.jpg" data-url-smart_horizontal="lwTWa9TNOZR48PShwaG_o6AA4_w=/90x0/smart/filters:strip_icc()/" data-url-smart="lwTWa9TNOZR48PShwaG_o6AA4_w=/90x0/smart/filters:strip_icc()/" data-url-feature="lwTWa9TNOZR48PShwaG_o6AA4_w=/90x0/smart/filters:strip_icc()/" data-url-tablet="YF9NQ3dyw4Hjv3EZHimY8KNvIw8=/220x125/smart/filters:strip_icc()/" data-url-desktop="pn4YXM_BaNJfzYV5_xLl1CNWY7g=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Na volta de Tevez, Boca Jrs. vence com direito a gol de placa e vira lÃ­der; assista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 16:05:13" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/gilmar-ferreira/flamengo-encontra-seu-camisa-10-deve-anunciar-segunda-feira-contratacao-de-ederson-da-lazio-16830534.html" class="foto" title="Fla segue na busca por um 10 e mira meia do Lazio que tem passagem pela SeleÃ§Ã£o (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/WzEsktXzAxAEPDEqu_rI4ZImtv4=/filters:quality(10):strip_icc()/s2.glbimg.com/sgyuYYAhdzvgLTxgUpLQK1G26bQ=/669x293:1402x782/120x80/s.glbimg.com/es/ge/f/original/2014/11/11/ederson-lazio-div.jpg" alt="Fla segue na busca por um 10 e mira meia do Lazio que tem passagem pela SeleÃ§Ã£o (DivulgaÃ§Ã£o)" title="Fla segue na busca por um 10 e mira meia do Lazio que tem passagem pela SeleÃ§Ã£o (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/sgyuYYAhdzvgLTxgUpLQK1G26bQ=/669x293:1402x782/120x80/s.glbimg.com/es/ge/f/original/2014/11/11/ederson-lazio-div.jpg" data-url-smart_horizontal="Fbwsfh6pAYchaFTva6WzhDyvjNU=/90x0/smart/filters:strip_icc()/" data-url-smart="Fbwsfh6pAYchaFTva6WzhDyvjNU=/90x0/smart/filters:strip_icc()/" data-url-feature="Fbwsfh6pAYchaFTva6WzhDyvjNU=/90x0/smart/filters:strip_icc()/" data-url-tablet="MMP-gR0bIuCRw05nwLBWLD5jVF0=/70x50/smart/filters:strip_icc()/" data-url-desktop="wt1N8Ddn0-TbpEvqe2f4NGiLros=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fla segue na busca por um<br /> 10 e mira meia do Lazio que tem passagem pela SeleÃ§Ã£o</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 16:30:33" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/07/fla-anuncia-contratacao-de-zagueiro-cesar-que-sera-apresentado-na-terca.html" class="foto" title="Fla anuncia contrataÃ§Ã£o de zagueiro do Benfica; vÃ­nculo de emprÃ©stimo vai atÃ© junho (DivulgaÃ§Ã£o/Flamengo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/9-vSoXIX0Sp64YI3KKjvyBCfFNo=/filters:quality(10):strip_icc()/s2.glbimg.com/eWTZi8taytKS0LJKjFD9Ds1r8ss=/97x26:490x287/120x80/s.glbimg.com/es/ge/f/original/2015/07/18/cesar_flamengo.jpg" alt="Fla anuncia contrataÃ§Ã£o de zagueiro do Benfica; vÃ­nculo de emprÃ©stimo vai atÃ© junho (DivulgaÃ§Ã£o/Flamengo)" title="Fla anuncia contrataÃ§Ã£o de zagueiro do Benfica; vÃ­nculo de emprÃ©stimo vai atÃ© junho (DivulgaÃ§Ã£o/Flamengo)"
                data-original-image="s2.glbimg.com/eWTZi8taytKS0LJKjFD9Ds1r8ss=/97x26:490x287/120x80/s.glbimg.com/es/ge/f/original/2015/07/18/cesar_flamengo.jpg" data-url-smart_horizontal="I2xqlWTdzI0TjF0mKa0QqRq-IA4=/90x0/smart/filters:strip_icc()/" data-url-smart="I2xqlWTdzI0TjF0mKa0QqRq-IA4=/90x0/smart/filters:strip_icc()/" data-url-feature="I2xqlWTdzI0TjF0mKa0QqRq-IA4=/90x0/smart/filters:strip_icc()/" data-url-tablet="e__oD_EEj9-uir4T5xRmTFg92GQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="l9BqmwInBusxKBUoCMKHMxwonp4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fla anuncia contrataÃ§Ã£o de zagueiro do Benfica; vÃ­nculo de emprÃ©stimo vai atÃ© junho</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 20:50:41" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/botafogo/noticia/2015/07/direcao-libera-e-rodrigo-pimpao-nao-joga-mais-pelo-botafogo.html" class="" title="PimpÃ£o Ã© liberado e deixa o Botafogo apÃ³s vitÃ³ria sobre NÃ¡utico que garantiu a lideranÃ§a da SÃ©rie B (Mauro Horita)" rel="bookmark"><span class="conteudo"><p>PimpÃ£o Ã© liberado e deixa o Botafogo apÃ³s vitÃ³ria sobre NÃ¡utico que garantiu a lideranÃ§a da SÃ©rie B</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 16:15:28" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/07/cruz-azul-aceita-pagar-us-2-milhoes-e-luis-fabiano-pode-deixar-sao-paulo.html" class="foto" title="Mexicanos aceitam pagar US$ 2 milhÃµes, e Fabuloso deve deixar o SÃ£o Paulo (Ãrico Leonan / saopaulofc.net)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Ab8EG57IMqfUmF1VF3LpV77RmOw=/filters:quality(10):strip_icc()/s2.glbimg.com/zENCjw9-Gwuq7HJdIIxRdLFVox8=/504x73:808x275/120x80/s.glbimg.com/es/ge/f/original/2015/07/16/dsc_0793.jpg" alt="Mexicanos aceitam pagar US$ 2 milhÃµes, e Fabuloso deve deixar o SÃ£o Paulo (Ãrico Leonan / saopaulofc.net)" title="Mexicanos aceitam pagar US$ 2 milhÃµes, e Fabuloso deve deixar o SÃ£o Paulo (Ãrico Leonan / saopaulofc.net)"
                data-original-image="s2.glbimg.com/zENCjw9-Gwuq7HJdIIxRdLFVox8=/504x73:808x275/120x80/s.glbimg.com/es/ge/f/original/2015/07/16/dsc_0793.jpg" data-url-smart_horizontal="MLJQIX53a3M7RfIvPK8K9-BARmw=/90x0/smart/filters:strip_icc()/" data-url-smart="MLJQIX53a3M7RfIvPK8K9-BARmw=/90x0/smart/filters:strip_icc()/" data-url-feature="MLJQIX53a3M7RfIvPK8K9-BARmw=/90x0/smart/filters:strip_icc()/" data-url-tablet="9YgIBtY1Tys4mDfe9o6ag-rBsK0=/70x50/smart/filters:strip_icc()/" data-url-desktop="9q1DocthOh_BKpnNYVsfUb3lYS4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Mexicanos aceitam pagar <br />US$ 2 milhÃµes, e Fabuloso<br /> deve deixar o SÃ£o Paulo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 17:58:14" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/imagem-michael-bisping-fica-com-dedao-lacerado-apos-luta-com-thales-leites.html" class="foto" title="Bisping fica com lasca do dedÃ£o pendurada apÃ³s bater brasileiro por pontos no UFC (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/B2_as6KGQVfXI_arCbiXNx1eBI8=/filters:quality(10):strip_icc()/s2.glbimg.com/0tEScHR0HNSRvqRZyCRbrUwupR0=/0x0:690x460/120x80/s.glbimg.com/es/ge/f/original/2015/07/18/dedao.jpg" alt="Bisping fica com lasca do dedÃ£o pendurada apÃ³s bater brasileiro por pontos no UFC (ReproduÃ§Ã£o)" title="Bisping fica com lasca do dedÃ£o pendurada apÃ³s bater brasileiro por pontos no UFC (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/0tEScHR0HNSRvqRZyCRbrUwupR0=/0x0:690x460/120x80/s.glbimg.com/es/ge/f/original/2015/07/18/dedao.jpg" data-url-smart_horizontal="CFY3Zr_VKLMlzJMdZmhWeHe95SM=/90x0/smart/filters:strip_icc()/" data-url-smart="CFY3Zr_VKLMlzJMdZmhWeHe95SM=/90x0/smart/filters:strip_icc()/" data-url-feature="CFY3Zr_VKLMlzJMdZmhWeHe95SM=/90x0/smart/filters:strip_icc()/" data-url-tablet="2IO1HEVPMEjPH3z0L37VHC7GrdE=/70x50/smart/filters:strip_icc()/" data-url-desktop="ntuvA_-0Mx5QJiEh-r5_u4YBY_k=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Bisping fica com lasca do dedÃ£o pendurada apÃ³s bater brasileiro por pontos no UFC</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/volei/noticia/2015/07/brasil-ignora-torcida-rival-vence-italia-e-fecha-1-fase-como-lider-do-grand-prix.html" class="" title="Brasil ignora torcida rival, vence anfitriÃ£ ItÃ¡lia por 3 a 0 e avanÃ§a como lÃ­der no Grand Prix de vÃ´lei (DivulgaÃ§Ã£o / FIVB)" rel="bookmark"><span class="conteudo"><p>Brasil ignora torcida rival, vence anfitriÃ£ ItÃ¡lia por <br />3 a 0 e avanÃ§a como lÃ­der no Grand Prix de vÃ´lei</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 16:36:01" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/ja-no-brasil-neymar-lembra-pool-party-em-vegas-nasci-pra-ser-feliz.html" class="foto" title="Neymar posta foto antiga de pool party em Vegas: &#39;Nasci para ser feliz e nÃ£o perfeito&#39; (ReproduÃ§Ã£o / Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Zof-IYMOtBwMYUluAwwuNblp2Qc=/filters:quality(10):strip_icc()/s2.glbimg.com/tOVlkfHSiCRmtsOwnJPv8sVBjqQ=/0x106:635x530/120x80/s.glbimg.com/es/ge/f/original/2015/07/18/neymar.png" alt="Neymar posta foto antiga de pool party em Vegas: &#39;Nasci para ser feliz e nÃ£o perfeito&#39; (ReproduÃ§Ã£o / Instagram)" title="Neymar posta foto antiga de pool party em Vegas: &#39;Nasci para ser feliz e nÃ£o perfeito&#39; (ReproduÃ§Ã£o / Instagram)"
                data-original-image="s2.glbimg.com/tOVlkfHSiCRmtsOwnJPv8sVBjqQ=/0x106:635x530/120x80/s.glbimg.com/es/ge/f/original/2015/07/18/neymar.png" data-url-smart_horizontal="RCSI26Y9xYSaVhJT7rlmbzCYDvc=/90x0/smart/filters:strip_icc()/" data-url-smart="RCSI26Y9xYSaVhJT7rlmbzCYDvc=/90x0/smart/filters:strip_icc()/" data-url-feature="RCSI26Y9xYSaVhJT7rlmbzCYDvc=/90x0/smart/filters:strip_icc()/" data-url-tablet="XEcATsXFNxNVXRFhY2UTSmYHjBs=/70x50/smart/filters:strip_icc()/" data-url-desktop="L_scs1Wp2lIz4woMl9iqTYeVpb4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Neymar posta foto antiga de pool party em Vegas: &#39;Nasci para ser feliz e nÃ£o perfeito&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 17:18:16" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/retratos-da-bola/ronaldinho-gaucho-vai-show-de-ludmilla-faz-churrasco-para-vizinha-emenda-pagode-torcedor-pede-nao-se-meta-em-balada-16832300.html" class="foto" title="R10 vai a show, emenda churrasco com pagode e fÃ£ diz: &#39;NÃ£o se meta em balada&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/nhPhBz6n0kJQyypTr8GhMrFs-EQ=/filters:quality(10):strip_icc()/s2.glbimg.com/S9s8lz5eT9geyQ8yEbI_PKtWr1A=/19x70:443x352/120x80/s.glbimg.com/en/ho/f/original/2015/07/18/ronaldinho.jpg" alt="R10 vai a show, emenda churrasco com pagode e fÃ£ diz: &#39;NÃ£o se meta em balada&#39; (ReproduÃ§Ã£o)" title="R10 vai a show, emenda churrasco com pagode e fÃ£ diz: &#39;NÃ£o se meta em balada&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/S9s8lz5eT9geyQ8yEbI_PKtWr1A=/19x70:443x352/120x80/s.glbimg.com/en/ho/f/original/2015/07/18/ronaldinho.jpg" data-url-smart_horizontal="jC4zBqGeJt70nuGhMHW3uAzaHHA=/90x0/smart/filters:strip_icc()/" data-url-smart="jC4zBqGeJt70nuGhMHW3uAzaHHA=/90x0/smart/filters:strip_icc()/" data-url-feature="jC4zBqGeJt70nuGhMHW3uAzaHHA=/90x0/smart/filters:strip_icc()/" data-url-tablet="zbbykKuBf6EOSn1aXGhOvMo3c98=/70x50/smart/filters:strip_icc()/" data-url-desktop="5-iZNB340ty8Ekd_Rd2VnJlIng8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>R10 vai a show, emenda churrasco com pagode e fÃ£ diz: &#39;NÃ£o se meta em balada&#39;</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/fatima-bernardes-tem-tarde-em-familia-no-shopping.html" class="foto" title="FÃ¡tima curte tarde em shopping com Bonner, filhos e a nora; veja as fotos (Fabio Moreno/Agnews)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/_S0_pw3DDupJBkjKm-WrmwTAx70=/filters:quality(10):strip_icc()/s2.glbimg.com/mvt75uUfX40Uboq_q-vhhs-8Uw8=/0x66:620x399/335x180/e.glbimg.com/og/ed/f/original/2015/07/18/img_1535.jpg" alt="FÃ¡tima curte tarde em shopping com Bonner, filhos e a nora; veja as fotos (Fabio Moreno/Agnews)" title="FÃ¡tima curte tarde em shopping com Bonner, filhos e a nora; veja as fotos (Fabio Moreno/Agnews)"
                data-original-image="s2.glbimg.com/mvt75uUfX40Uboq_q-vhhs-8Uw8=/0x66:620x399/335x180/e.glbimg.com/og/ed/f/original/2015/07/18/img_1535.jpg" data-url-smart_horizontal="WtwtcNZEl9QIEAktmMQIVN1c_sE=/90x0/smart/filters:strip_icc()/" data-url-smart="WtwtcNZEl9QIEAktmMQIVN1c_sE=/90x0/smart/filters:strip_icc()/" data-url-feature="WtwtcNZEl9QIEAktmMQIVN1c_sE=/90x0/smart/filters:strip_icc()/" data-url-tablet="O7lZpavO9SOoc_TqcpKoYGUvYp4=/220x125/smart/filters:strip_icc()/" data-url-desktop="wsscmVFoCjF2EbZwzYrnBf0jNbs=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>FÃ¡tima curte tarde em shopping com Bonner, filhos e a nora; veja as fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 18:46:58" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/tv/noticia/2015/07/claudia-leitte-se-emociona-ao-rever-quarto-que-dividia-na-bahia.html" class="foto" title="Claudia Leitte se emociona ao rever quarto que dividia com pais e irmÃ£o na Bahia (Pedro Curi/ Gshow)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Fwhcc-i5tdgqlB8WxxYl-OAMDO0=/filters:quality(10):strip_icc()/s2.glbimg.com/kiaS-QO-sRyQhPlDniK6KyFmKjc=/121x65:579x370/120x80/s.glbimg.com/et/gs/f/original/2015/07/18/foto_1_-_pedro_curi_-_tv_glboo.jpg" alt="Claudia Leitte se emociona ao rever quarto que dividia com pais e irmÃ£o na Bahia (Pedro Curi/ Gshow)" title="Claudia Leitte se emociona ao rever quarto que dividia com pais e irmÃ£o na Bahia (Pedro Curi/ Gshow)"
                data-original-image="s2.glbimg.com/kiaS-QO-sRyQhPlDniK6KyFmKjc=/121x65:579x370/120x80/s.glbimg.com/et/gs/f/original/2015/07/18/foto_1_-_pedro_curi_-_tv_glboo.jpg" data-url-smart_horizontal="I93C6pxh6RLiAcdyzI1i82NdF_Q=/90x0/smart/filters:strip_icc()/" data-url-smart="I93C6pxh6RLiAcdyzI1i82NdF_Q=/90x0/smart/filters:strip_icc()/" data-url-feature="I93C6pxh6RLiAcdyzI1i82NdF_Q=/90x0/smart/filters:strip_icc()/" data-url-tablet="wLGsY2A3C9USTiolkeUS-SEkV5c=/70x50/smart/filters:strip_icc()/" data-url-desktop="F-Z9OgRBN-zWpX6qic7aJGq_72s=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Claudia Leitte se emociona <br />ao rever quarto que dividia <br />com pais e irmÃ£o na Bahia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 16:11:41" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/noticias/rio/apos-repercussao-de-fotos-gari-carioca-ja-tem-assessor-pensa-em-posar-nua-16831288.html#ixzz3gGsqFUFf" class="foto" title="ApÃ³s repercussÃ£o de fotos, gari carioca tem assessor e admite fazer ensaio pelada (Facebook)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/yXHv8DOi0ZDjnYfJbKd5hu_ec1w=/filters:quality(10):strip_icc()/s2.glbimg.com/PXuVuPj_QihRIKccc6EmHBVjDAI=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/07/18/gari.jpg" alt="ApÃ³s repercussÃ£o de fotos, gari carioca tem assessor e admite fazer ensaio pelada (Facebook)" title="ApÃ³s repercussÃ£o de fotos, gari carioca tem assessor e admite fazer ensaio pelada (Facebook)"
                data-original-image="s2.glbimg.com/PXuVuPj_QihRIKccc6EmHBVjDAI=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/07/18/gari.jpg" data-url-smart_horizontal="h5hW_JX3UVdTndMPv2o45fSwA-E=/90x0/smart/filters:strip_icc()/" data-url-smart="h5hW_JX3UVdTndMPv2o45fSwA-E=/90x0/smart/filters:strip_icc()/" data-url-feature="h5hW_JX3UVdTndMPv2o45fSwA-E=/90x0/smart/filters:strip_icc()/" data-url-tablet="CUi-raue8bKstmJBaEx44xDhBUE=/70x50/smart/filters:strip_icc()/" data-url-desktop="YOkIYRzmIw-ZR2dLG2HXHLACowo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ApÃ³s repercussÃ£o de fotos, gari carioca tem assessor e admite fazer ensaio pelada</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 12:39:02" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/televisao/noticia/2015/07/verdades-secretas-yasmin-brunet-admite-desconforto-em-cenas-de-sexo.html" class="" title="Yasmin Brunet revela dificuldade com &#39;book rosa&#39; em novela: &#39;NÃ£o pode ligar que peito estÃ¡ de fora&#39; (Globo/Pedro Curi)" rel="bookmark"><span class="conteudo"><p>Yasmin Brunet revela dificuldade com &#39;book rosa&#39; em novela: &#39;NÃ£o pode ligar que peito estÃ¡ de fora&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 19:35:34" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/paparazzo/noticia/2015/07/natalia-casassola-diz-ser-fa-de-sexo-tres-e-conjugacao-do-prazer.html" class="foto" title="No Paparazzo, Natalia Casassola diz ser fÃ£ de sexo a trÃªs: &#39;ConjugaÃ§Ã£o do prazerâ (Roberto Teixeira / Paparazzo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/BnUq9TUNfA63JJjA4vlui07dCMc=/filters:quality(10):strip_icc()/s2.glbimg.com/4S2gedZit1CvQj2Oay4tlLjBreg=/40x339:328x532/120x80/s.glbimg.com/jo/eg/f/original/2015/07/16/3fgjngfmfg.jpg" alt="No Paparazzo, Natalia Casassola diz ser fÃ£ de sexo a trÃªs: &#39;ConjugaÃ§Ã£o do prazerâ (Roberto Teixeira / Paparazzo)" title="No Paparazzo, Natalia Casassola diz ser fÃ£ de sexo a trÃªs: &#39;ConjugaÃ§Ã£o do prazerâ (Roberto Teixeira / Paparazzo)"
                data-original-image="s2.glbimg.com/4S2gedZit1CvQj2Oay4tlLjBreg=/40x339:328x532/120x80/s.glbimg.com/jo/eg/f/original/2015/07/16/3fgjngfmfg.jpg" data-url-smart_horizontal="yjbyBraivlnsZGTYmLoqFjEx1_s=/90x0/smart/filters:strip_icc()/" data-url-smart="yjbyBraivlnsZGTYmLoqFjEx1_s=/90x0/smart/filters:strip_icc()/" data-url-feature="yjbyBraivlnsZGTYmLoqFjEx1_s=/90x0/smart/filters:strip_icc()/" data-url-tablet="ZeZt7ZqmwgevRTjlE4am2YJZ9PQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="_QTBWpBG1hHuG3k8hnmXDWXbl3A=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>No Paparazzo, Natalia Casassola diz ser fÃ£ de sexo <br />a trÃªs: &#39;ConjugaÃ§Ã£o do prazerâ</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 21:47:08" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/gravidez/noticia/2015/07/fernanda-gentil-exibe-barrigao-de-gravida-e-agradece-obrigada-senhor.html" class="foto" title="Fernanda Gentil faz foto com afilhado e marido para exibir barriga de grÃ¡vida e agradece (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/9q8ph7p6CnjAM1QNYGP4tSy-M4Y=/filters:quality(10):strip_icc()/s2.glbimg.com/KeBY-VreB-v8MtFvug4q4mroUZY=/0x93:1080x813/120x80/s.glbimg.com/jo/eg/f/original/2015/07/18/fernandagentic.jpg" alt="Fernanda Gentil faz foto com afilhado e marido para exibir barriga de grÃ¡vida e agradece (ReproduÃ§Ã£o/Instagram)" title="Fernanda Gentil faz foto com afilhado e marido para exibir barriga de grÃ¡vida e agradece (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/KeBY-VreB-v8MtFvug4q4mroUZY=/0x93:1080x813/120x80/s.glbimg.com/jo/eg/f/original/2015/07/18/fernandagentic.jpg" data-url-smart_horizontal="TI5FNf8ycbDqwQU4IIEfIf7NMzs=/90x0/smart/filters:strip_icc()/" data-url-smart="TI5FNf8ycbDqwQU4IIEfIf7NMzs=/90x0/smart/filters:strip_icc()/" data-url-feature="TI5FNf8ycbDqwQU4IIEfIf7NMzs=/90x0/smart/filters:strip_icc()/" data-url-tablet="VqC43p9KsQz18aIwKXGxJNWn2eY=/70x50/smart/filters:strip_icc()/" data-url-desktop="FtqdVGqtVDDiIpEP0lW0FtfHE9E=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fernanda Gentil faz foto com afilhado e marido para exibir barriga de grÃ¡vida e agradece</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/nicole-bahls-fica-surpresa-ao-saber-pela-internet-que-ex-tentou-se-reconciliar-com-ela-nao-sei-do-que-ele-esta-falando-16823274.html" class="" title="Nicole Bahls se surpreende ao ouvir sobre boato de reconciliaÃ§Ã£o: &#39;Estou sozinha, sÃ³ trabalho&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>Nicole Bahls se surpreende ao ouvir sobre boato de reconciliaÃ§Ã£o: &#39;Estou sozinha, sÃ³ trabalho&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 17:24:28" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/isabella-santoni-posa-de-noivinha-punk-so-de-top-hot-pant-atica-seguidores-casa-comigo-16829268.html" class="foto" title="Santoni atiÃ§a seguidores ao mostrar look com hot pant usado em &#39;MalhaÃ§Ã£o&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/7EzlB_cdjkkE065KeJr2muvWFG4=/filters:quality(10):strip_icc()/s2.glbimg.com/tjUHpqWR2yXuIBEawL6_LYwlpBA=/63x84:334x265/120x80/s.glbimg.com/en/ho/f/original/2015/07/18/santoni.jpg" alt="Santoni atiÃ§a seguidores ao mostrar look com hot pant usado em &#39;MalhaÃ§Ã£o&#39; (ReproduÃ§Ã£o)" title="Santoni atiÃ§a seguidores ao mostrar look com hot pant usado em &#39;MalhaÃ§Ã£o&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/tjUHpqWR2yXuIBEawL6_LYwlpBA=/63x84:334x265/120x80/s.glbimg.com/en/ho/f/original/2015/07/18/santoni.jpg" data-url-smart_horizontal="N5SVB1pszSsrPsJTkVkfsKffxgw=/90x0/smart/filters:strip_icc()/" data-url-smart="N5SVB1pszSsrPsJTkVkfsKffxgw=/90x0/smart/filters:strip_icc()/" data-url-feature="N5SVB1pszSsrPsJTkVkfsKffxgw=/90x0/smart/filters:strip_icc()/" data-url-tablet="8OK5HNMZb3DxI26X3KNnufqTBvg=/70x50/smart/filters:strip_icc()/" data-url-desktop="uv-ZHh32Z_wli-gClOYsmt7xlBc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Santoni atiÃ§a seguidores <br />ao mostrar look com hot <br />pant usado em &#39;MalhaÃ§Ã£o&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-07-18 16:09:29" class="chamada chamada-principal mobile-grid-full"><a href="http://casavogue.globo.com/LazerCultura/Comida-bebida/Receita/noticia/2015/05/receita-de-hamburguer-com-bacon.html" class="foto" title="Cheeseburger de 3 andares mata a fome com bacon e vÃ¡rios tipos de queijo; anote (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/UEC4PIGHr225XqncMX4ZoEX1Tlo=/filters:quality(10):strip_icc()/s2.glbimg.com/jeQxi-3HwoPPchNEtdKDFRQSnIU=/14x109:421x381/120x80/e.glbimg.com/og/ed/f/original/2015/05/27/empire_state.jpg" alt="Cheeseburger de 3 andares mata a fome com bacon e vÃ¡rios tipos de queijo; anote (DivulgaÃ§Ã£o)" title="Cheeseburger de 3 andares mata a fome com bacon e vÃ¡rios tipos de queijo; anote (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/jeQxi-3HwoPPchNEtdKDFRQSnIU=/14x109:421x381/120x80/e.glbimg.com/og/ed/f/original/2015/05/27/empire_state.jpg" data-url-smart_horizontal="1lV6y9kJVVWz8jrkZIKdzo3zcp0=/90x0/smart/filters:strip_icc()/" data-url-smart="1lV6y9kJVVWz8jrkZIKdzo3zcp0=/90x0/smart/filters:strip_icc()/" data-url-feature="1lV6y9kJVVWz8jrkZIKdzo3zcp0=/90x0/smart/filters:strip_icc()/" data-url-tablet="-SbO7iLVxciMFht_VtN0_fu4P88=/70x50/smart/filters:strip_icc()/" data-url-desktop="7xF_xIk993gYEV-eTPBDLVrPlNQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Cheeseburger de 3 andares mata a fome com bacon e vÃ¡rios tipos de queijo; anote</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2014/index.html", "logo": "http://s2.glbimg.com/kX8ylHaY-BqxGG2GesdMgR7SxQc=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_38x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2014/rss", "logo_tv": "http://s2.glbimg.com/tKm4gbARSp2eHSq6zr2alVV82hc=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2014/index.html", "logo": "http://s2.glbimg.com/kX8ylHaY-BqxGG2GesdMgR7SxQc=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_38x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2014/rss", "logo_tv": "http://s2.glbimg.com/tKm4gbARSp2eHSq6zr2alVV82hc=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior "><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="tecnologia &amp; games"><span class="word word-0">tecnologia</span><span class="word word-1">&</span><span class="word word-2">games</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/review/alcatel-one-touch-pop-c5.html" title="Rival do Moto E de R$300 com TV e dois chips passa por testes"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/W5KRXIDgFXKIRPgXX3v2GCvaEjE=/filters:quality(10):strip_icc()/s2.glbimg.com/pnzbq4-2xQn_8F6DBlFEx2Xnxnw=/0x479:4000x2602/245x130/s.glbimg.com/po/tt2/f/original/2015/07/17/alcatel_one_touch_pop_c5_3.jpg" alt="Rival do Moto E de R$300 com TV e dois chips passa por testes (Lucas Mendes/TechTudo)" title="Rival do Moto E de R$300 com TV e dois chips passa por testes (Lucas Mendes/TechTudo)"
             data-original-image="s2.glbimg.com/pnzbq4-2xQn_8F6DBlFEx2Xnxnw=/0x479:4000x2602/245x130/s.glbimg.com/po/tt2/f/original/2015/07/17/alcatel_one_touch_pop_c5_3.jpg" data-url-smart_horizontal="gfkqx402pFgdRj01bDYOnBhY5Rk=/90x56/smart/filters:strip_icc()/" data-url-smart="gfkqx402pFgdRj01bDYOnBhY5Rk=/90x56/smart/filters:strip_icc()/" data-url-feature="gfkqx402pFgdRj01bDYOnBhY5Rk=/90x56/smart/filters:strip_icc()/" data-url-tablet="KGPPGI9raRO6sco94YmnYHYaoYk=/160x96/smart/filters:strip_icc()/" data-url-desktop="_Ear3nI1JpSN0U2PA8O9VWlAybo=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Rival do Moto E de R$300 com TV e dois chips passa por testes</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/07/virtua-racing-super-monaco-gp-conheca-os-melhores-jogos-de-formula-1.html" title="Relembre os melhores games de F-1 lanÃ§ados em todos os tempos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/vTO-CXdeXY_iCXLgpQwzfUW1lNg=/filters:quality(10):strip_icc()/s2.glbimg.com/aW14xDPLGy8sP-4vFELPcPLmsFE=/4x16:688x379/245x130/s.glbimg.com/po/tt2/f/original/2015/07/10/f1-ps3.jpg" alt="Relembre os melhores games de F-1 lanÃ§ados em todos os tempos ( DivulgaÃ§Ã£o)" title="Relembre os melhores games de F-1 lanÃ§ados em todos os tempos ( DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/aW14xDPLGy8sP-4vFELPcPLmsFE=/4x16:688x379/245x130/s.glbimg.com/po/tt2/f/original/2015/07/10/f1-ps3.jpg" data-url-smart_horizontal="cJChDdZb70b6Kj7HrBD3ZIGxmPw=/90x56/smart/filters:strip_icc()/" data-url-smart="cJChDdZb70b6Kj7HrBD3ZIGxmPw=/90x56/smart/filters:strip_icc()/" data-url-feature="cJChDdZb70b6Kj7HrBD3ZIGxmPw=/90x56/smart/filters:strip_icc()/" data-url-tablet="EJdSUPIOtHqe7J5GvqFFc-uA3-E=/160x96/smart/filters:strip_icc()/" data-url-desktop="isFJ7c9Md5U7N7DmiRVxfR3dBHA=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Relembre os melhores games de F-1 lanÃ§ados em todos os tempos</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/07/oito-capas-que-deixam-seu-celular-a-prova-dagua.html" title="Confira capinhas &#39;mÃ¡gicas&#39; que deixam seu smart Ã  prova d&#39;Ã¡gua"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/OxiaHhAIllX-msoQT-ijnj7B-4Q=/filters:quality(10):strip_icc()/s2.glbimg.com/Ku8iax1qQRo1fi2-tbbQEwvkNO8=/54x51:560x319/245x130/s.glbimg.com/po/tt2/f/original/2015/07/18/capa-para-destaque_copia.jpg" alt="Confira capinhas &#39;mÃ¡gicas&#39; que deixam seu smart Ã  prova d&#39;Ã¡gua (DivulgaÃ§Ã£o)" title="Confira capinhas &#39;mÃ¡gicas&#39; que deixam seu smart Ã  prova d&#39;Ã¡gua (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/Ku8iax1qQRo1fi2-tbbQEwvkNO8=/54x51:560x319/245x130/s.glbimg.com/po/tt2/f/original/2015/07/18/capa-para-destaque_copia.jpg" data-url-smart_horizontal="4qvEwxhQ2A42ldTxR6vz0w3WSEc=/90x56/smart/filters:strip_icc()/" data-url-smart="4qvEwxhQ2A42ldTxR6vz0w3WSEc=/90x56/smart/filters:strip_icc()/" data-url-feature="4qvEwxhQ2A42ldTxR6vz0w3WSEc=/90x56/smart/filters:strip_icc()/" data-url-tablet="njFHExfFH94Tgitv5gyhwrJxP3w=/160x96/smart/filters:strip_icc()/" data-url-desktop="RJUfhX98Lt8D6UARTTIRfkGq1bk=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Confira capinhas &#39;mÃ¡gicas&#39; que deixam seu smart Ã  prova d&#39;Ã¡gua</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/07/galaxy-note-5-samsung-pode-incluir-canetinha-auto-ejetavel.html" title="Novo Galaxy pode vir com caneta revolucionÃ¡ria e  &#39;humilhar&#39; iPhone"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/SM5IJRQN1y_JnTXHhcoKU8CJv4w=/filters:quality(10):strip_icc()/s2.glbimg.com/8CD-LjedFMNHqEIeGAOa8A4yluo=/80x56:656x361/245x130/s.glbimg.com/po/tt2/f/original/2015/07/18/galaxy-note-s-pen-auto-ejetavel-destaque.jpg" alt="Novo Galaxy pode vir com caneta revolucionÃ¡ria e  &#39;humilhar&#39; iPhone (DivulgaÃ§Ã£o)" title="Novo Galaxy pode vir com caneta revolucionÃ¡ria e  &#39;humilhar&#39; iPhone (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/8CD-LjedFMNHqEIeGAOa8A4yluo=/80x56:656x361/245x130/s.glbimg.com/po/tt2/f/original/2015/07/18/galaxy-note-s-pen-auto-ejetavel-destaque.jpg" data-url-smart_horizontal="x3bBBO6fblC_2tPoV1CdnGQ6krg=/90x56/smart/filters:strip_icc()/" data-url-smart="x3bBBO6fblC_2tPoV1CdnGQ6krg=/90x56/smart/filters:strip_icc()/" data-url-feature="x3bBBO6fblC_2tPoV1CdnGQ6krg=/90x56/smart/filters:strip_icc()/" data-url-tablet="FDopBEDkmc5sTJ6ajSKK35AI3xw=/160x96/smart/filters:strip_icc()/" data-url-desktop="uwZdPJXwKekXiRtv48Cshz47IiA=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Novo Galaxy pode vir com caneta revolucionÃ¡ria e  &#39;humilhar&#39; iPhone</p></span></a></li></ul><div class="destaque-inferior analytics-area analytics-id-B"><ul><li class="techtudo"><a href="http://www.techtudo.com.br/" title="Teste agora a velocidade da sua internet"><span class="logo"></span><span class="title">Teste agora a velocidade da sua internet &rsaquo;</span></a></li><li class="exty"><a href="http://www.techtudo.com.br/tudo-sobre/extensao-globo-com.html" title="Adicione e veja as notÃ­cias em tempo real"><span class="logo"></span><span class="title">Adicione e veja as notÃ­cias em tempo real &rsaquo;</span></a></li></ul></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Moda/fotos/2015/07/moletom-estilo-conforto.html" alt="Fashionistas em peso aderem ao moletom como uniforme &#39;street style&#39;"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/vCxLWlVwPKnDlFhKnejIaTssriE=/filters:quality(10):strip_icc()/s2.glbimg.com/0JDMqwxYE4Mo3ha8qavruz5Pywk=/130x2:1125x644/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/moletom.jpg" alt="Fashionistas em peso aderem ao moletom como uniforme &#39;street style&#39; (ReproduÃ§Ã£o)" title="Fashionistas em peso aderem ao moletom como uniforme &#39;street style&#39; (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/0JDMqwxYE4Mo3ha8qavruz5Pywk=/130x2:1125x644/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/moletom.jpg" data-url-smart_horizontal="eyKlhORMTewSqd2tiyDLcEdIgeA=/90x56/smart/filters:strip_icc()/" data-url-smart="eyKlhORMTewSqd2tiyDLcEdIgeA=/90x56/smart/filters:strip_icc()/" data-url-feature="eyKlhORMTewSqd2tiyDLcEdIgeA=/90x56/smart/filters:strip_icc()/" data-url-tablet="L2pxwcUSltY4c7a28oR5szo1gvE=/122x75/smart/filters:strip_icc()/" data-url-desktop="DvkTmJWJJu2wTA9W7Ks-xFPz8DM=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>do esportivo ao superfeminino</h3><p>Fashionistas em peso aderem ao moletom como uniforme &#39;street style&#39;</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/moda-tendencias/noticia/2015/07/tapecaria-o-casaco-do-inverno-da-vez-tem-padronagem-que-remete-tapetes.html" alt="Casacos &#39;tem-que-ter&#39; deste inverno remetem a estamparia de tapetes"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/9H40k6iz7g50afOreX7cCNdUDa8=/filters:quality(10):strip_icc()/s2.glbimg.com/c3qDbHcf3mJtUyI5gDlSZ7Ojkps=/123x111:448x320/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/vogue.jpg" alt="Casacos &#39;tem-que-ter&#39; deste inverno remetem a estamparia de tapetes (ReproduÃ§Ã£o)" title="Casacos &#39;tem-que-ter&#39; deste inverno remetem a estamparia de tapetes (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/c3qDbHcf3mJtUyI5gDlSZ7Ojkps=/123x111:448x320/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/vogue.jpg" data-url-smart_horizontal="e7pmJLi904MhcEt-RUWmE2JBQmI=/90x56/smart/filters:strip_icc()/" data-url-smart="e7pmJLi904MhcEt-RUWmE2JBQmI=/90x56/smart/filters:strip_icc()/" data-url-feature="e7pmJLi904MhcEt-RUWmE2JBQmI=/90x56/smart/filters:strip_icc()/" data-url-tablet="-gH6DHnjVuImiFAjPO2Wa3qaC0o=/122x75/smart/filters:strip_icc()/" data-url-desktop="Y65Z7jGQVt7zAk8_QwU0a1-L0-8=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>peÃ§as &#39;da vovÃ³&#39;</h3><p>Casacos &#39;tem-que-ter&#39; deste inverno remetem a estamparia de tapetes</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Beleza/noticia/2015/07/pro-aprenda-usar-os-produtos-de-cabelo-como-uma-profissional.html" alt="&#39;Marie Claire&#39; ensina a usar produtos indispensÃ¡veis para ter cabelo perfeitos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/BRJYq226qBOWkE43OJGlQHT6mXk=/filters:quality(10):strip_icc()/s2.glbimg.com/B2IWPoobFrv_F2BFPMmPx7G259s=/0x18:620x418/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/marie_claire.jpg" alt="&#39;Marie Claire&#39; ensina a usar produtos indispensÃ¡veis para ter cabelo perfeitos (ReproduÃ§Ã£o)" title="&#39;Marie Claire&#39; ensina a usar produtos indispensÃ¡veis para ter cabelo perfeitos (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/B2IWPoobFrv_F2BFPMmPx7G259s=/0x18:620x418/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/marie_claire.jpg" data-url-smart_horizontal="8FteP5L4vKSZKrReO4WdiPyMPJo=/90x56/smart/filters:strip_icc()/" data-url-smart="8FteP5L4vKSZKrReO4WdiPyMPJo=/90x56/smart/filters:strip_icc()/" data-url-feature="8FteP5L4vKSZKrReO4WdiPyMPJo=/90x56/smart/filters:strip_icc()/" data-url-tablet="WjKMbgb0osuybiT3Xcw1LT9NeXw=/122x75/smart/filters:strip_icc()/" data-url-desktop="ZTLWTP5wR0DX9Y2aAzxghz1Vyy8=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Silicone, serum, shampoo a seco e mais</h3><p>&#39;Marie Claire&#39; ensina a usar produtos indispensÃ¡veis para ter cabelo perfeitos</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://ela.oglobo.globo.com/decoracao/a-transformacao-do-mobiliario-nacional-da-heranca-portuguesa-brasilidade-criativa-16810958" alt="Da heranÃ§a portuguesa Ã  brasilidade criativa, veja peÃ§as que fizeram histÃ³ria"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/xziAM3bYWSjcjtGzhBalY5tFMP4=/filters:quality(10):strip_icc()/s2.glbimg.com/XYK2v75oCn8ax7ZJsGBJSuW48og=/78x161:403x370/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/ela.jpg" alt="Da heranÃ§a portuguesa Ã  brasilidade criativa, veja peÃ§as que fizeram histÃ³ria (ReproduÃ§Ã£o)" title="Da heranÃ§a portuguesa Ã  brasilidade criativa, veja peÃ§as que fizeram histÃ³ria (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/XYK2v75oCn8ax7ZJsGBJSuW48og=/78x161:403x370/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/ela.jpg" data-url-smart_horizontal="ikcq2JN-E92b3CW9JYwlAkI9s1k=/90x56/smart/filters:strip_icc()/" data-url-smart="ikcq2JN-E92b3CW9JYwlAkI9s1k=/90x56/smart/filters:strip_icc()/" data-url-feature="ikcq2JN-E92b3CW9JYwlAkI9s1k=/90x56/smart/filters:strip_icc()/" data-url-tablet="hPGsLXDB7pwE06qDgkC-ohvzvv0=/122x75/smart/filters:strip_icc()/" data-url-desktop="qxwHHlo8moJnZNgYGhHFucRa35c=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Zanine, Niemeyer, IrmÃ£os Campana e mais</h3><p>Da heranÃ§a portuguesa Ã  brasilidade criativa, veja peÃ§as que fizeram histÃ³ria</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/casa-e-decoracao/fotos/veja-ideias-para-renovar-cabeceira-da-cama-e-dar-cara-nova-ao-quarto.htm" alt="Renove a cabeceira da cama para dar outra cara ao dÃ©cor; veja ideias no GNT"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/XbM2UZjWtiu9PpDIplOMvnI6TwU=/filters:quality(10):strip_icc()/s2.glbimg.com/-kD8Kv-ZugCGbP52WRTRwCfRdvg=/163x13:738x384/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/gnt.jpg" alt="Renove a cabeceira da cama para dar outra cara ao dÃ©cor; veja ideias no GNT (ReproduÃ§Ã£o)" title="Renove a cabeceira da cama para dar outra cara ao dÃ©cor; veja ideias no GNT (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/-kD8Kv-ZugCGbP52WRTRwCfRdvg=/163x13:738x384/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/gnt.jpg" data-url-smart_horizontal="PV3pV7AzfJdsom5h0ox66zVON0Y=/90x56/smart/filters:strip_icc()/" data-url-smart="PV3pV7AzfJdsom5h0ox66zVON0Y=/90x56/smart/filters:strip_icc()/" data-url-feature="PV3pV7AzfJdsom5h0ox66zVON0Y=/90x56/smart/filters:strip_icc()/" data-url-tablet="mtX82K2QQ-nkewViegyS0Ig5CF8=/122x75/smart/filters:strip_icc()/" data-url-desktop="L9Ki-ChCSqvCOOu3YyEwTxXaVsU=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>sonhos de consumo</h3><p>Renove a cabeceira da cama para dar outra cara ao dÃ©cor; veja ideias no GNT</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/jogos-americanos-divertidos-para-mesa-veja-modelos/" alt="Selecionamos jogos americanos divertidos para todas as idades"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/3NktOTteYNxQTfIQmwaHkOtXb6k=/filters:quality(10):strip_icc()/s2.glbimg.com/6oma04qQmfXxRDST59EbS74zk1g=/24x13:553x355/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/jogo_americano_fita.jpg" alt="Selecionamos jogos americanos divertidos para todas as idades (ReproduÃ§Ã£o)" title="Selecionamos jogos americanos divertidos para todas as idades (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/6oma04qQmfXxRDST59EbS74zk1g=/24x13:553x355/155x100/s.glbimg.com/en/ho/f/original/2015/07/18/jogo_americano_fita.jpg" data-url-smart_horizontal="73TbiOpyZEIDAaRtGDahirIubMI=/90x56/smart/filters:strip_icc()/" data-url-smart="73TbiOpyZEIDAaRtGDahirIubMI=/90x56/smart/filters:strip_icc()/" data-url-feature="73TbiOpyZEIDAaRtGDahirIubMI=/90x56/smart/filters:strip_icc()/" data-url-tablet="jMjv0EysSZqslc8w5eIoReF4tU4=/122x75/smart/filters:strip_icc()/" data-url-desktop="o_Gmlb8GuLaR8Ki1qqk7gWhy57I=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>charme e praticidade</h3><p>Selecionamos jogos americanos divertidos para todas as idades</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Celebridades/fotos/2015/07/poxa-vida-hein-conheca-os-segurancas-mais-gatos-dos-famosos.html" title="Fotos: quem sÃ£o os 9 seguranÃ§as mais gatos das celebridades"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/SDTL6SnBnbr0aHm7-sFiwbG4jRM=/filters:quality(10):strip_icc()/s2.glbimg.com/Q8LCDtEa5TWw63B8y0qI03pAWRA=/157x183:1797x1052/245x130/s.glbimg.com/en/ho/f/original/2015/07/18/greg_lenz_1.jpg" alt="Fotos: quem sÃ£o os 9 seguranÃ§as mais gatos das celebridades (ReproduÃ§Ã£o)" title="Fotos: quem sÃ£o os 9 seguranÃ§as mais gatos das celebridades (ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/Q8LCDtEa5TWw63B8y0qI03pAWRA=/157x183:1797x1052/245x130/s.glbimg.com/en/ho/f/original/2015/07/18/greg_lenz_1.jpg" data-url-smart_horizontal="1ODy6wOamfheEObcLzIdpbYSzkE=/90x56/smart/filters:strip_icc()/" data-url-smart="1ODy6wOamfheEObcLzIdpbYSzkE=/90x56/smart/filters:strip_icc()/" data-url-feature="1ODy6wOamfheEObcLzIdpbYSzkE=/90x56/smart/filters:strip_icc()/" data-url-tablet="xTukVEK_Q0t2Ar5Xzi6kJYaF3R8=/160x95/smart/filters:strip_icc()/" data-url-desktop="GvmPbORZrzeTYcXBmbzSTv1UIxE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Fotos: quem sÃ£o os 9 seguranÃ§as mais gatos das celebridades</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistamonet.globo.com/Listas/noticia/2015/07/famosas-internacionais-que-se-separaram-durante-gravidez.html" title="Listamos dez famosas que se separaram durante a gravidez "><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/dDXvK8qR_rXZoNZcBr1FV_1xvxU=/filters:quality(10):strip_icc()/s2.glbimg.com/ZKbWX0sbeKSHy-YraopJ0I7VKI8=/0x51:600x369/245x130/s.glbimg.com/en/ho/f/original/2015/07/18/monet.jpg" alt="Listamos dez famosas que se separaram durante a gravidez  (ReproduÃ§Ã£o)" title="Listamos dez famosas que se separaram durante a gravidez  (ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/ZKbWX0sbeKSHy-YraopJ0I7VKI8=/0x51:600x369/245x130/s.glbimg.com/en/ho/f/original/2015/07/18/monet.jpg" data-url-smart_horizontal="3i7FYm7AoaG3e1YwiqJrNXum3dk=/90x56/smart/filters:strip_icc()/" data-url-smart="3i7FYm7AoaG3e1YwiqJrNXum3dk=/90x56/smart/filters:strip_icc()/" data-url-feature="3i7FYm7AoaG3e1YwiqJrNXum3dk=/90x56/smart/filters:strip_icc()/" data-url-tablet="9CBo-WmH1pXsCynY-ofnr6Mr8TI=/160x95/smart/filters:strip_icc()/" data-url-desktop="ppXEPSC4SA1HjjWepSplwYwi8VQ=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Listamos dez famosas que se separaram durante a gravidez </p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/uau-priscila-pires-exibe-cintura-fininha-em-dia-de-piscina-com-os-filhos.html" title="Priscila Pires chama atenÃ§Ã£o por barriga &#39;chapada&#39; com os filhos "><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/nyWGAYKXAwIe_4Bw4rPF_s9mQOc=/filters:quality(10):strip_icc()/s2.glbimg.com/GznmUwDXLYtvMZr01EC6sL1Cr8I=/40x62:536x326/245x130/s.glbimg.com/en/ho/f/original/2015/07/18/priscila-pires.jpg" alt="Priscila Pires chama atenÃ§Ã£o por barriga &#39;chapada&#39; com os filhos  (ReproduÃ§Ã£o/Instagram)" title="Priscila Pires chama atenÃ§Ã£o por barriga &#39;chapada&#39; com os filhos  (ReproduÃ§Ã£o/Instagram)"
                    data-original-image="s2.glbimg.com/GznmUwDXLYtvMZr01EC6sL1Cr8I=/40x62:536x326/245x130/s.glbimg.com/en/ho/f/original/2015/07/18/priscila-pires.jpg" data-url-smart_horizontal="2PSr9GRCkGN5kYwP786ExNJmXU0=/90x56/smart/filters:strip_icc()/" data-url-smart="2PSr9GRCkGN5kYwP786ExNJmXU0=/90x56/smart/filters:strip_icc()/" data-url-feature="2PSr9GRCkGN5kYwP786ExNJmXU0=/90x56/smart/filters:strip_icc()/" data-url-tablet="CU290aUKgoqonA-dqy1ZavJ3TlM=/160x95/smart/filters:strip_icc()/" data-url-desktop="eTOLRyeoF-6EB68LQxJOuq8ndso=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Priscila Pires chama atenÃ§Ã£o por barriga &#39;chapada&#39; com os filhos </p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/QUEM-News/noticia/2015/07/tanquinho-isis-valverde-levanta-camiseta-em-dia-de-academia.html" title="Nos EUA, Isis Valverde faz graÃ§a ao bater ponto na academia"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/YpMTDSsqf6ap6pZR-_FxAMe8tac=/filters:quality(10):strip_icc()/s2.glbimg.com/kcVciZQZMvSELfkVh-qK0dOmooY=/15x31:550x315/245x130/e.glbimg.com/og/ed/f/original/2015/07/18/11254122_679010505533367_899843962_n.jpg" alt="Nos EUA, Isis Valverde faz graÃ§a ao bater ponto na academia (ReproduÃ§Ã£o)" title="Nos EUA, Isis Valverde faz graÃ§a ao bater ponto na academia (ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/kcVciZQZMvSELfkVh-qK0dOmooY=/15x31:550x315/245x130/e.glbimg.com/og/ed/f/original/2015/07/18/11254122_679010505533367_899843962_n.jpg" data-url-smart_horizontal="V3AEta9Ip0SdXw3-xlxgp8e6bLc=/90x56/smart/filters:strip_icc()/" data-url-smart="V3AEta9Ip0SdXw3-xlxgp8e6bLc=/90x56/smart/filters:strip_icc()/" data-url-feature="V3AEta9Ip0SdXw3-xlxgp8e6bLc=/90x56/smart/filters:strip_icc()/" data-url-tablet="kQa7Ptvt3tzoUwwZOi2cWEIVK4k=/160x95/smart/filters:strip_icc()/" data-url-desktop="d39a7xy4HomzBmlkEkd449Z-Y9g=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Nos EUA, Isis Valverde faz graÃ§a ao bater ponto na academia</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/babilonia/index.html">BABILÃNIA</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2014/index.html">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/07/mari-e-bruna-se-entendem-de-vez-apos-climao.html" title="&#39;I love ParaisÃ³polis&#39;: Mari e Bruna se entendem de vez apÃ³s climÃ£o"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/GwzyBqa9Xz1vwxu5x5sh0Wwwl4Q=/filters:quality(10):strip_icc()/s2.glbimg.com/tzjXhQKvuhIRL6IqTBxR5OxIVeU=/44x71:652x394/245x130/s.glbimg.com/et/gs/f/original/2015/07/16/bruna_e_mari.jpg" alt="&#39;I love ParaisÃ³polis&#39;: Mari e Bruna se entendem de vez apÃ³s climÃ£o (TV Globo)" title="&#39;I love ParaisÃ³polis&#39;: Mari e Bruna se entendem de vez apÃ³s climÃ£o (TV Globo)"
                    data-original-image="s2.glbimg.com/tzjXhQKvuhIRL6IqTBxR5OxIVeU=/44x71:652x394/245x130/s.glbimg.com/et/gs/f/original/2015/07/16/bruna_e_mari.jpg" data-url-smart_horizontal="GiYddsxkv8I7RkMKHdI7TWa8N_g=/90x56/smart/filters:strip_icc()/" data-url-smart="GiYddsxkv8I7RkMKHdI7TWa8N_g=/90x56/smart/filters:strip_icc()/" data-url-feature="GiYddsxkv8I7RkMKHdI7TWa8N_g=/90x56/smart/filters:strip_icc()/" data-url-tablet="KfoGkm0gq_M_xrpn6wl9T6x2i2Q=/160x95/smart/filters:strip_icc()/" data-url-desktop="rJgJ0tqAlEX5ILfSqLlr_Xc1W18=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;I love ParaisÃ³polis&#39;: Mari e Bruna se entendem de vez apÃ³s climÃ£o</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/07/vitoria-visita-antiga-casa-de-emilia-e-bernardo.html" title="&#39;AlÃ©m do Tempo&#39;: VitÃ³ria visita antiga casa de EmÃ­lia e Bernardo"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/qw8b9J9LDteLrfZGHUhWr0UoKQY=/filters:quality(10):strip_icc()/s2.glbimg.com/QzTh-rrXSaPNOa-d1aBPdeVBCwA=/65x68:598x351/245x130/s.glbimg.com/et/gs/f/original/2015/07/16/vitoria-irene-ravache.jpg" alt="&#39;AlÃ©m do Tempo&#39;: VitÃ³ria visita antiga casa de EmÃ­lia e Bernardo (Ellen Soares/Gshow)" title="&#39;AlÃ©m do Tempo&#39;: VitÃ³ria visita antiga casa de EmÃ­lia e Bernardo (Ellen Soares/Gshow)"
                    data-original-image="s2.glbimg.com/QzTh-rrXSaPNOa-d1aBPdeVBCwA=/65x68:598x351/245x130/s.glbimg.com/et/gs/f/original/2015/07/16/vitoria-irene-ravache.jpg" data-url-smart_horizontal="NycTnBIAimrWMX17NsKMT67iGOo=/90x56/smart/filters:strip_icc()/" data-url-smart="NycTnBIAimrWMX17NsKMT67iGOo=/90x56/smart/filters:strip_icc()/" data-url-feature="NycTnBIAimrWMX17NsKMT67iGOo=/90x56/smart/filters:strip_icc()/" data-url-tablet="HravP1d5SwFDn87XHWMJlXYP3iE=/160x95/smart/filters:strip_icc()/" data-url-desktop="wCm0z-guRl2kuKWsMa_p_hzL4JY=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;AlÃ©m do Tempo&#39;: VitÃ³ria visita antiga casa de EmÃ­lia e Bernardo</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2014/Vem-por-ai/noticia/2015/07/teaser-de-malhacao-volta-de-nat-pega-todos-de-surpresa.html" title="Volta de Nat surpreende todos em &#39;MalhaÃ§Ã£o&#39;; assista ao teaser"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/MZj6opIU7lBPR-Mg75vRjBuvF1U=/filters:quality(10):strip_icc()/s2.glbimg.com/Q3uxKRz1oZFSEWaHVvcMKzLdkDY=/0x35:690x402/245x130/s.glbimg.com/et/gs/f/original/2015/07/18/nat.jpg" alt="Volta de Nat surpreende todos em &#39;MalhaÃ§Ã£o&#39;; assista ao teaser (TV Globo)" title="Volta de Nat surpreende todos em &#39;MalhaÃ§Ã£o&#39;; assista ao teaser (TV Globo)"
                    data-original-image="s2.glbimg.com/Q3uxKRz1oZFSEWaHVvcMKzLdkDY=/0x35:690x402/245x130/s.glbimg.com/et/gs/f/original/2015/07/18/nat.jpg" data-url-smart_horizontal="YCPbCS1oU0okFlILhfEdChEn4_U=/90x56/smart/filters:strip_icc()/" data-url-smart="YCPbCS1oU0okFlILhfEdChEn4_U=/90x56/smart/filters:strip_icc()/" data-url-feature="YCPbCS1oU0okFlILhfEdChEn4_U=/90x56/smart/filters:strip_icc()/" data-url-tablet="ZSdiU5j2gO2V-LX48AiaKiRRY2M=/160x95/smart/filters:strip_icc()/" data-url-desktop="XJtb6cGM5AYIFCoHUBYc6E3RXaA=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Volta de Nat surpreende todos<br /> em &#39;MalhaÃ§Ã£o&#39;; assista ao teaser</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/tv/noticia/2015/07/video-confira-entrevista-completa-de-cristiano-araujo-no-estrelas.html" title="Veja: &#39;Estrelas&#39; exibe Ã­ntegra de entrevista de Cristiano AraÃºjo"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/5z5iSYSGyGtksmV1rKDn1u90HGI=/filters:quality(10):strip_icc()/s2.glbimg.com/JKIAHUZa_x2ofRGX0rF1Zb460QU=/293x0:4842x2411/245x130/s.glbimg.com/jo/eg/f/original/2015/07/18/248179.jpg" alt="Veja: &#39;Estrelas&#39; exibe Ã­ntegra de entrevista de Cristiano AraÃºjo (Globo/ Gabriel Nascimento)" title="Veja: &#39;Estrelas&#39; exibe Ã­ntegra de entrevista de Cristiano AraÃºjo (Globo/ Gabriel Nascimento)"
                    data-original-image="s2.glbimg.com/JKIAHUZa_x2ofRGX0rF1Zb460QU=/293x0:4842x2411/245x130/s.glbimg.com/jo/eg/f/original/2015/07/18/248179.jpg" data-url-smart_horizontal="ZGpybYcwsjezjeQBtAvBdRheTKg=/90x56/smart/filters:strip_icc()/" data-url-smart="ZGpybYcwsjezjeQBtAvBdRheTKg=/90x56/smart/filters:strip_icc()/" data-url-feature="ZGpybYcwsjezjeQBtAvBdRheTKg=/90x56/smart/filters:strip_icc()/" data-url-tablet="JsXbfy6156bcbsfSf6B7QZIrJP8=/160x95/smart/filters:strip_icc()/" data-url-desktop="YM5Wkg9RFkCYFJDNrePXltpfVLM=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Veja: &#39;Estrelas&#39; exibe Ã­ntegra de entrevista de Cristiano AraÃºjo</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/cultura/musica/marcos-valle-sobe-ao-palco-no-rio-em-dois-eventos-que-mostram-seu-momento-sinforronico-16821759" title="Marcos Valle volta Ã s raÃ­zes no Municipal e em show de baiÃ£o com Kassin e Jeneci (DivulgaÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/aIv3jRMK0yPVDBATKf4hRr9WiaI=/filters:quality(10):strip_icc()/s2.glbimg.com/rTImuv_WQdqshnTIS9YNbIExNFE=/322x176:940x550/215x130/s.glbimg.com/en/ho/f/original/2015/07/18/marcos_valle.jpg"
                data-original-image="s2.glbimg.com/rTImuv_WQdqshnTIS9YNbIExNFE=/322x176:940x550/215x130/s.glbimg.com/en/ho/f/original/2015/07/18/marcos_valle.jpg" data-url-smart_horizontal="ADQH_RKimu4LQqyjS0nPz7NpZ5U=/90x56/smart/filters:strip_icc()/" data-url-smart="ADQH_RKimu4LQqyjS0nPz7NpZ5U=/90x56/smart/filters:strip_icc()/" data-url-feature="ADQH_RKimu4LQqyjS0nPz7NpZ5U=/90x56/smart/filters:strip_icc()/" data-url-tablet="u8LWY8pxO2cVFVpD5LunMb1jevc=/120x80/smart/filters:strip_icc()/" data-url-desktop="_DXrgyJ3gdLBH8XRl9sXoRTT_ZQ=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Marcos Valle volta Ã s raÃ­zes no Municipal e em show de baiÃ£o com Kassin e Jeneci</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Musica/noticia/2015/07/reveja-momentos-marcantes-da-aniversariante-baby-do-brasil.html" title="Baby 62 anos! Lembre vÃ­deos clÃ¡ssicos na telinha desde a EmÃ­lia de &#39;Pirlimpimpim&#39; (TomÃ¡s Rangel)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/2M7GtQXJ0Y66HOHvJSRkhzsYRXc=/filters:quality(10):strip_icc()/s2.glbimg.com/QUpUYnV8N32_PxoNEeTdnzF86wA=/152x42:593x309/215x130/e.glbimg.com/og/ed/f/original/2015/07/02/20150605_quem_baby_0131_alta_credtomasrangel.jpg"
                data-original-image="s2.glbimg.com/QUpUYnV8N32_PxoNEeTdnzF86wA=/152x42:593x309/215x130/e.glbimg.com/og/ed/f/original/2015/07/02/20150605_quem_baby_0131_alta_credtomasrangel.jpg" data-url-smart_horizontal="vb3zOyNtOg4e2eU_0dW5qlJNMEM=/90x56/smart/filters:strip_icc()/" data-url-smart="vb3zOyNtOg4e2eU_0dW5qlJNMEM=/90x56/smart/filters:strip_icc()/" data-url-feature="vb3zOyNtOg4e2eU_0dW5qlJNMEM=/90x56/smart/filters:strip_icc()/" data-url-tablet="HMt0EHC3xYVgSRv-doJa9fyE6p8=/120x80/smart/filters:strip_icc()/" data-url-desktop="5oFeyfZi_AM0DGxXuMFzsW39taQ=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Baby 62 anos! Lembre vÃ­deos clÃ¡ssicos na telinha desde <br />a EmÃ­lia de &#39;Pirlimpimpim&#39;</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/tecnologia/noticia/2015/07/tidal-de-jay-z-e-processado-por-gravadora-de-lil-wayne-diz-site.html" title="Gravadora pede US$ 50 mi ao Tidal, do rapper Jay Z, por hospedar Ã¡lbum de Lil Wayne (Getty Images)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/tseCLXxXv56-k4NW4pYZJftFIw8=/filters:quality(10):strip_icc()/s2.glbimg.com/k0McXKZ7UYgn8S8mGndRq3Sxdlo=/2x75:425x331/215x130/e.glbimg.com/og/ed/f/original/2015/07/17/jay-z.jpg"
                data-original-image="s2.glbimg.com/k0McXKZ7UYgn8S8mGndRq3Sxdlo=/2x75:425x331/215x130/e.glbimg.com/og/ed/f/original/2015/07/17/jay-z.jpg" data-url-smart_horizontal="EQn11oJOsS-WExHFKNZ3r2nYad0=/90x56/smart/filters:strip_icc()/" data-url-smart="EQn11oJOsS-WExHFKNZ3r2nYad0=/90x56/smart/filters:strip_icc()/" data-url-feature="EQn11oJOsS-WExHFKNZ3r2nYad0=/90x56/smart/filters:strip_icc()/" data-url-tablet="Op1BL2WMiZNMzi4OL5TB_dEVPYs=/120x80/smart/filters:strip_icc()/" data-url-desktop="9ytk8H3CoRyCNQYO8Te7Y_CY8oU=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Gravadora pede US$ 50 mi <br />ao Tidal, do rapper Jay Z, por hospedar Ã¡lbum de Lil Wayne</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/07/video-intimo-de-laura-keller-foi-gravado-pelo-marido-ao-meu-lado.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">VÃ­deo Ã­ntimo de Laura Keller com outra mulher foi gravado pelo marido, confirma atriz</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://oglobo.globo.com/brasil/estou-levando-um-calote-diz-dono-de-porsche-apreendido-na-casa-da-dinda-16822858" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;Estou levando um calote&#39;, diz dono de Porsche apreendido na Casa da Dinda</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/noticias/rio/ex-mulher-do-cantor-ferrugem-morre-apos-fazer-lipoaspiracao-16823082.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Aos 28 anos, ex-mulher de cantor de pagode morre por complicaÃ§Ãµes apÃ³s fazer lipo</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/noticias/rio/gari-carioca-chama-atencao-pela-beleza-faz-sucesso-nas-redes-sociais-16811722.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Com 12 mil seguidores no Face, gari do RJ questiona: &#39;Por que devem ser feias?&#39;</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/politica/noticia/2015/07/horas-apos-romper-com-governo-cunha-autoriza-cpi-do-bndes.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Horas apÃ³s romper com governo, Eduardo Cunha autoriza CPI do BNDES</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/motor/formula-1/noticia/2015/07/morre-jules-bianchi-aos-25-anos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Jules Bianchi morre aos 25 anos, nove meses apÃ³s grave acidente no GP do JapÃ£o</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/motor/formula-1/noticia/2015/07/massa-e-outros-pilotos-postam-mensagens-em-homenagem-bianchi.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Massa e outros pilotos prestam tributo a Bianchi, morto aos 25 anos; veja mensagens </span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/esporte/fluminense/bianca-leao-musa-do-fluminense-da-as-boas-vindas-ronaldinho-gaucho-veio-para-agregar-16810954.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Musa do Flu dÃ¡ as boas vindas para R10: &#39;A fase Ã© boa e nÃ£o vai atrapalhar o Fred&#39;</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/07/manchester-united-bate-america-mex-na-estreia-de-schweinsteiger.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">United estreia super reforÃ§o Schweinsteiger em torneio contra BarÃ§a, Chelsea e PSG</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/famosos/retratos-da-bola/zagueiro-marquinhos-sua-noiva-carol-cabrino-curtem-as-praias-de-los-cabos-no-mexico-16821683.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Marquinhos mostra a noiva musa em mesÃ£o de frente para praia paradisÃ­aca</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/07/zero-ciume-kadu-moliterno-mostra-namorada-de-fio-dental-em-rede-social.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Com pau de selfie, Kadu Moliterno tira foto da amada de fio-dental de oncinha</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/07/carol-portaluppi-posa-de-fio-dental-em-praia-da-croacia.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Carol Portaluppi mostra o corpo bonito e curte muito a vida em praia da CroÃ¡cia</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/noite/noticia/2015/07/scheila-carvalho-usa-look-curtinho-e-posta-foto-antes-de-sair-de-casa.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Scheila Carvalho posa antes de sair de casa e mostra visual de arrancar suspiros</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/famosos/ex-bbb-fani-adia-casamento-preciso-retomar-meu-eixo-16822709.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ex-BBB Fani decide adiar o seu casamento<br />com namorado mÃ©dico para &#39;retomar o eixo&#39;</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/moda/fotos/2015/07/famosas-curtem-ferias-de-julho-em-praias-mundo-afora-veja-os-biquinis-de-sabrina-sato-e-outras.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Famosas curtem as fÃ©rias de julho em praias mundo afora; veja biquÃ­nis de Sato e outras</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://tvg.globo.com/programas/pe-na-cova/index.html">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://tvg.globo.com/programas/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/5dec817a5e49.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){setTimeout(function(){var bannerMobiles = $(".opec-mobile .tag-manager-publicidade-container");bannerMobiles.each(function(index,banner){var $banner = $(banner);if($banner.height() < 15){$banner.parent().addClass('without-opec');}});}, 1300);}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 4000);}
</script><style> .opec-x60{height:auto}</style><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 10};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 18/07/2015 21:56:30 -->
