



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "767-2428262-6504398";
                var ue_id = "0B1R33RR1N5FW5CAPWXE";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="0B1R33RR1N5FW5CAPWXE" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-c54cc773.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1487564148._CB297803232_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3693178366._CB298945358_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
!function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c?c:a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";a(2)},{2:2}],2:[function(a,b,c){"use strict";!function(){var a,b,c=function(a){return"[object Array]"===Object.prototype.toString.call(a)},d=function(a,b){for(var c=0;c<a.length;c++)c in a&&b.call(null,a[c],c)},e=[],f=!1,g=!1,h=function(){var a=[],b=[],c={};return d(e,function(e){var f="";d(e.dartsite.split("/"),function(b){""!==b&&(b in c||(c[b]=a.length,a.push(b)),f+="/"+c[b])}),b.push(f)}),{iu_parts:a,enc_prev_ius:b}},i=function(){var a=[];return d(e,function(b){var c=[];d(b.sizes,function(a){c.push(a.join("x"))}),a.push(c.join("|"))}),a},j=function(){var a=[];return d(e,function(b){a.push(k(b.targeting))}),a.join("|")},k=function(a,b){var c,d=[];for(var e in a){c=[];for(var f=0;f<a[e].length;f++)c.push(encodeURIComponent(a[e][f]));b?d.push(e+"="+encodeURIComponent(c.join(","))):d.push(e+"="+c.join(","))}return d.join("&")},l=function(){var a=+new Date;g||document.readyState&&"loaded"!==document.readyState||(g=!0,f&&imdbads.cmd.push(function(){for(var b=0;b<e.length;b++)generic.monitoring.record_metric(e[b].name+".fail",csm.duration(a))}))};window.tinygpt={define_slot:function(a,b,c,d){e.push({dartsite:a.replace(/\/$/,""),sizes:b,name:c,targeting:d})},set_targeting:function(b){a=b},callback:function(a){for(var c,d,f={},g=+new Date,h=0;h<e.length;h++)c=e[h].dartsite,d=e[h].name,a[h][c]?f[d]=a[h][c]:window.console&&console.error&&console.error("Unable to correlate GPT response for "+d);imdbads.cmd.push(function(){for(var a=0;a<e.length;a++)ad_utils.slot_events.trigger(e[a].name,"request",{timestamp:b}),ad_utils.slot_events.trigger(e[a].name,"tagdeliver",{timestamp:g});ad_utils.gpt.handle_response(f)})},send:function(){var d,g,m=[],n=function(a,b){c(b)&&(b=b.join(",")),b&&m.push(a+"="+encodeURIComponent(""+b))};if(0===e.length)return void tinygpt.callback({});n("gdfp_req","1"),n("correlator",Math.floor(4503599627370496*Math.random())),n("output","json_html"),n("callback","tinygpt.callback"),n("impl","fifs"),n("json_a","1");var o=h();n("iu_parts",o.iu_parts),n("enc_prev_ius",o.enc_prev_ius),n("prev_iu_szs",i()),n("prev_scp",j()),n("cust_params",k(a,!0)),d=document.createElement("script"),g=document.getElementsByTagName("script")[0],d.async=!0,d.type="text/javascript",d.src="http://pubads.g.doubleclick.net/gampad/ads?"+m.join("&"),d.id="tinygpt",d.onload=d.onerror=d.onreadystatechange=l,f=!0,g.parentNode.insertBefore(d,g),b=+new Date}}}()},{}]},{},[1]);</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['c'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['556633926141'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-2402319567._CB298018164_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"b2ff25160061d57bb4fff1b32476da011e922034",
"2016-02-05T18%3A04%3A25GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50135;
generic.days_to_midnight = 0.5802661776542664;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3230827532._CB299577821_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'c']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=556633926141;ord=556633926141?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=556633926141?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=556633926141?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=02-05&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59467714/?ref_=nv_nw_tn_1"
> 'MacGyver' Movie Reboot And New TV Series Pilot On The Way
</a><br />
                        <span class="time">2 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59464622/?ref_=nv_nw_tn_2"
> Super Bowl Weekend Preview: 'Panda 3' Over 'Hail, Caesar!', 'Pride and Prejudice and Zombies' & 'The Choice'
</a><br />
                        <span class="time">23 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59468138/?ref_=nv_nw_tn_3"
> Susan Sarandon Responds to Piers Morganâs Criticism of Her Cleavage With More Cleavage (Photos)
</a><br />
                        <span class="time">22 minutes ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0266697/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTMxNjE0MzcwM15BMl5BanBnXkFtZTYwMzAxOTc3._V1._SY315_CR0,0,410,315_.jpg",
            titleYears : "2003",
            rank : 170,
                    headline : "Kill Bill: Vol. 1"
    },
    nameAd : {
            clickThru : "/name/nm2225369/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTQ1NzU2NTkxNl5BMl5BanBnXkFtZTcwNzMxOTUxOQ@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 34,
            headline : "Jennifer Lawrence"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYqLND_hTef3-LDEWvp2cZlA9IO5HSHyRRcGEJNcVlqcP9wD5TKaq5ct4KToPoM1IPyVr3ghYWg%0D%0APsjv4uTs9MD5w8Zbj7dTVjbDya0RXYvxnafEeEXRjWCzFC_sTMBNMsokgaf0SRVNmgzEcpuf72xf%0D%0AWEABROnq-cSflZ8P03-wQ_WZT1ezgpjgivSxGWQg-Gk9--sZGEzXXBxGIaJeVchPDg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYtl6eQTq_GKYJSWieK5QZbjUWLvrEjZeJaJ0nqt2-uVl0Z92x7x9YLngZPRd_2WUjSyGrv7-bR%0D%0AG2fsGG8E1-q7D2rHGNLWcP6gbAG33aRy4IGuqH5zHn4mZlbswQAcVKUoFebDXitYlUwYzc4cMeaT%0D%0Aso3nEqiQvLzll1KkEG3KGkRfD1SPGzcIHy66fM4RmHTu%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYsZv16ZQe1g8mNwryvjC2_cml3eB2efkayTUKE1Va2riDIb7fDcCp0hlt8r0uz6FhMBRY5m4bT%0D%0AuPkLX_A2HrVQpOZNkayzWGEhKOCxwXRYCDcXNeYxEEVrTvw_Vw4IIsUeaJVubW6IN4vi6lIRcpv6%0D%0AZpdbBu_JjU9E3BkJQhb_H1KfNUyPvYb3vaatmvXyYn9X%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=556633926141;ord=556633926141?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=556633926141;ord=556633926141?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2279650585?ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403211182&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2279650585" data-source="bylist" data-id="ls002322762" data-rid="0B1R33RR1N5FW5CAPWXE" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Life after college graduation is not exactly going as planned for Will and Jillian who find themselves lost in a sea of increasingly strange jobs." alt="Life after college graduation is not exactly going as planned for Will and Jillian who find themselves lost in a sea of increasingly strange jobs." src="http://ia.media-imdb.com/images/M/MV5BMjM4NzQyNDk0MV5BMl5BanBnXkFtZTgwNjcxMDk4NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM4NzQyNDk0MV5BMl5BanBnXkFtZTgwNjcxMDk4NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Life after college graduation is not exactly going as planned for Will and Jillian who find themselves lost in a sea of increasingly strange jobs." title="Life after college graduation is not exactly going as planned for Will and Jillian who find themselves lost in a sea of increasingly strange jobs." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Life after college graduation is not exactly going as planned for Will and Jillian who find themselves lost in a sea of increasingly strange jobs." title="Life after college graduation is not exactly going as planned for Will and Jillian who find themselves lost in a sea of increasingly strange jobs." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1468846/?ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403211182&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Get a Job </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1692448025?ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403211182&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1692448025" data-source="bylist" data-id="ls002922459" data-rid="0B1R33RR1N5FW5CAPWXE" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="In the midst of a dazzling and prolific career at the forefront of modern jazz innovation, Miles Davis virtually disappears from public view for a period of five years in the late 1970s. Alone and holed up in his home, he is beset by chronic pain from a deteriorating hip, his musical voice stifled and numbed by drugs and pain medications, his mind haunted by unsettling ghosts from the past. A wily music reporter, Dave Braden forces his way into Davis' house and, over the next couple of days, the two men unwittingly embark on a wild and sometimes harrowing adventure to recover a stolen tape of the musician's latest compositions." alt="In the midst of a dazzling and prolific career at the forefront of modern jazz innovation, Miles Davis virtually disappears from public view for a period of five years in the late 1970s. Alone and holed up in his home, he is beset by chronic pain from a deteriorating hip, his musical voice stifled and numbed by drugs and pain medications, his mind haunted by unsettling ghosts from the past. A wily music reporter, Dave Braden forces his way into Davis' house and, over the next couple of days, the two men unwittingly embark on a wild and sometimes harrowing adventure to recover a stolen tape of the musician's latest compositions." src="http://ia.media-imdb.com/images/M/MV5BZTA5NjNjZjItMzQyNy00YmQ3LTliZjEtYzYwNTEyNTY3ZjA4XkEyXkFqcGdeQXVyMjg1NTAzMjQ@._V1_SY298_CR108,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BZTA5NjNjZjItMzQyNy00YmQ3LTliZjEtYzYwNTEyNTY3ZjA4XkEyXkFqcGdeQXVyMjg1NTAzMjQ@._V1_SY298_CR108,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="In the midst of a dazzling and prolific career at the forefront of modern jazz innovation, Miles Davis virtually disappears from public view for a period of five years in the late 1970s. Alone and holed up in his home, he is beset by chronic pain from a deteriorating hip, his musical voice stifled and numbed by drugs and pain medications, his mind haunted by unsettling ghosts from the past. A wily music reporter, Dave Braden forces his way into Davis' house and, over the next couple of days, the two men unwittingly embark on a wild and sometimes harrowing adventure to recover a stolen tape of the musician's latest compositions." title="In the midst of a dazzling and prolific career at the forefront of modern jazz innovation, Miles Davis virtually disappears from public view for a period of five years in the late 1970s. Alone and holed up in his home, he is beset by chronic pain from a deteriorating hip, his musical voice stifled and numbed by drugs and pain medications, his mind haunted by unsettling ghosts from the past. A wily music reporter, Dave Braden forces his way into Davis' house and, over the next couple of days, the two men unwittingly embark on a wild and sometimes harrowing adventure to recover a stolen tape of the musician's latest compositions." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="In the midst of a dazzling and prolific career at the forefront of modern jazz innovation, Miles Davis virtually disappears from public view for a period of five years in the late 1970s. Alone and holed up in his home, he is beset by chronic pain from a deteriorating hip, his musical voice stifled and numbed by drugs and pain medications, his mind haunted by unsettling ghosts from the past. A wily music reporter, Dave Braden forces his way into Davis' house and, over the next couple of days, the two men unwittingly embark on a wild and sometimes harrowing adventure to recover a stolen tape of the musician's latest compositions." title="In the midst of a dazzling and prolific career at the forefront of modern jazz innovation, Miles Davis virtually disappears from public view for a period of five years in the late 1970s. Alone and holed up in his home, he is beset by chronic pain from a deteriorating hip, his musical voice stifled and numbed by drugs and pain medications, his mind haunted by unsettling ghosts from the past. A wily music reporter, Dave Braden forces his way into Davis' house and, over the next couple of days, the two men unwittingly embark on a wild and sometimes harrowing adventure to recover a stolen tape of the musician's latest compositions." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt0790770/?ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403211182&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Miles Ahead </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2581640473?ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403211182&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2581640473" data-source="bylist" data-id="ls002322762" data-rid="0B1R33RR1N5FW5CAPWXE" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Louisa lives in a quaint town in the English countryside. With no clear direction in her life, the quirky and creative 26-year-old goes from one job to the next in order to help her family make ends meet. Her normally cheery outlook is put to the test, however, when she faces her newest career challenge: Taking a job at the local &quot;castle,&quot; she becomes caregiver and companion to Will Traynor, a wealthy young banker who became wheelchair bound in an accident two years prior, and whose whole world changed dramatically in the blink of an eye." alt="Louisa lives in a quaint town in the English countryside. With no clear direction in her life, the quirky and creative 26-year-old goes from one job to the next in order to help her family make ends meet. Her normally cheery outlook is put to the test, however, when she faces her newest career challenge: Taking a job at the local &quot;castle,&quot; she becomes caregiver and companion to Will Traynor, a wealthy young banker who became wheelchair bound in an accident two years prior, and whose whole world changed dramatically in the blink of an eye." src="http://ia.media-imdb.com/images/M/MV5BMTQ2NjE4NDE2NV5BMl5BanBnXkFtZTgwOTcwNDE5NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2NjE4NDE2NV5BMl5BanBnXkFtZTgwOTcwNDE5NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Louisa lives in a quaint town in the English countryside. With no clear direction in her life, the quirky and creative 26-year-old goes from one job to the next in order to help her family make ends meet. Her normally cheery outlook is put to the test, however, when she faces her newest career challenge: Taking a job at the local &quot;castle,&quot; she becomes caregiver and companion to Will Traynor, a wealthy young banker who became wheelchair bound in an accident two years prior, and whose whole world changed dramatically in the blink of an eye." title="Louisa lives in a quaint town in the English countryside. With no clear direction in her life, the quirky and creative 26-year-old goes from one job to the next in order to help her family make ends meet. Her normally cheery outlook is put to the test, however, when she faces her newest career challenge: Taking a job at the local &quot;castle,&quot; she becomes caregiver and companion to Will Traynor, a wealthy young banker who became wheelchair bound in an accident two years prior, and whose whole world changed dramatically in the blink of an eye." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Louisa lives in a quaint town in the English countryside. With no clear direction in her life, the quirky and creative 26-year-old goes from one job to the next in order to help her family make ends meet. Her normally cheery outlook is put to the test, however, when she faces her newest career challenge: Taking a job at the local &quot;castle,&quot; she becomes caregiver and companion to Will Traynor, a wealthy young banker who became wheelchair bound in an accident two years prior, and whose whole world changed dramatically in the blink of an eye." title="Louisa lives in a quaint town in the English countryside. With no clear direction in her life, the quirky and creative 26-year-old goes from one job to the next in order to help her family make ends meet. Her normally cheery outlook is put to the test, however, when she faces her newest career challenge: Taking a job at the local &quot;castle,&quot; she becomes caregiver and companion to Will Traynor, a wealthy young banker who became wheelchair bound in an accident two years prior, and whose whole world changed dramatically in the blink of an eye." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2674426/?ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403211182&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Me Before You </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403211182&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','HeroWidget',{wb:1});}
                if(typeof uet === 'function'){uet("cf");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/video/?ref_=hm_hm_jk_hc_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>George Clooney, Cast Let Loose With the Coen Brothers</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt0475290/?ref_=hm_hm_jk_hc_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="George Clooney, Josh Brolin, Scarlett Johansson, Channing Tatum and Jonah Hill in Hail, Caesar! (2016)" alt="George Clooney, Josh Brolin, Scarlett Johansson, Channing Tatum and Jonah Hill in Hail, Caesar! (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjQyODc3MTI2NF5BMl5BanBnXkFtZTgwNDMxMjU2NzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQyODc3MTI2NF5BMl5BanBnXkFtZTgwNDMxMjU2NzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2548282649?ref_=hm_hm_jk_hc_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2548282649" data-source="bylist" data-id="ls033701341" data-rid="0B1R33RR1N5FW5CAPWXE" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hm_jk_hc" data-ref="hm_hm_jk_hc_i_2"> <img itemprop="image" class="pri_image" title="Hail, Caesar! (2016)" alt="Hail, Caesar! (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjMzOTcyNzc2NV5BMl5BanBnXkFtZTgwMTM0Mzk2NzE@._V1_SY250_CR10,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMzOTcyNzc2NV5BMl5BanBnXkFtZTgwMTM0Mzk2NzE@._V1_SY250_CR10,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Hail, Caesar! (2016)" title="Hail, Caesar! (2016)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Hail, Caesar! (2016)" title="Hail, Caesar! (2016)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">What happens when <a href="/name/nm0000123/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_jk_hc_lk1">George Clooney</a>, <a href="/name/nm1475594/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_jk_hc_lk2">Channing Tatum</a>, <a href="/name/nm0000982/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_jk_hc_lk3">Josh Brolin</a>, and the <a href="/name/nm0001053/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_jk_hc_lk4">Coen</a> <a href="/name/nm0001054/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_jk_hc_lk5">Brothers</a> get together? A lot of fun! Watch our chat with the stars of <i><a href="/title/tt0475290/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_jk_hc_lk6">Hail, Caesar!</a></i> and learn about what it's like to go off script with the famed directors and how they love for George to get a good slap to the face!</p> <p class="seemore"><a href="/imdbpicks/video/?ref_=hm_hm_jk_hc_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402452622&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch our chat with the <i>Hail, Caesar!</i> stars</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
                if(typeof uet === 'function'){uet("af");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/xfiles-13-most-surprising-guest-stars/ls033131563?ref_=hm_pks_xf_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402141582&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>"X-Files": The 13 Most Surprising Guest Stars</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/xfiles-13-most-surprising-guest-stars/ls033131563?ref_=hm_pks_xf_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402141582&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage#image1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTg1NjU1Mjg2N15BMl5BanBnXkFtZTgwODg2OTI1MDE@._UY150_CR0,0,201,150_SY150_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg1NjU1Mjg2N15BMl5BanBnXkFtZTgwODg2OTI1MDE@._UY150_CR0,0,201,150_SY150_SX201_AL_UY300_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/xfiles-13-most-surprising-guest-stars/ls033131563?ref_=hm_pks_xf_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402141582&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage#image2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjIyMjQ4NDQ5NV5BMl5BanBnXkFtZTgwNjAzNzE4MTE@._UY150_CR0,0,201,150_SY150_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIyMjQ4NDQ5NV5BMl5BanBnXkFtZTgwNjAzNzE4MTE@._UY150_CR0,0,201,150_SY150_SX201_AL_UY300_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/xfiles-13-most-surprising-guest-stars/ls033131563?ref_=hm_pks_xf_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402141582&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage#image3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNzY4MzY1MjEwMF5BMl5BanBnXkFtZTcwMDA5MjQ1Nw@@._UY150_CR0,0,201,150_SY150_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzY4MzY1MjEwMF5BMl5BanBnXkFtZTcwMDA5MjQ1Nw@@._UY150_CR0,0,201,150_SY150_SX201_AL_UY300_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">From <a href="/name/nm0000608/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402141582&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_xf_lk1">Burt Reynolds</a> to <a href="/name/nm0005351/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402141582&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_xf_lk2">Ryan Reynolds</a>, take a look at some of the biggest names who've guest-starred on "<a href="/title/tt0106179/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402141582&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_xf_lk3">The X-Files</a>."</p> <p class="seemore"><a href="/imdbpicks/xfiles-13-most-surprising-guest-stars/ls033131563?ref_=hm_pks_xf_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402141582&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See our full list</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59467714?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQwODA5NjE3NV5BMl5BanBnXkFtZTcwOTIwMTUzNw@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59467714?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >'MacGyver' Movie Reboot And New TV Series Pilot On The Way</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>The Playlist</a></span>
    </div>
                                </div>
<p>You can't keep old TV series from being warmed over like last week's roast quite like Hollywood can, and once again, they are trying to make "<a href="/title/tt0088559?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">MacGyver</a>" happen. Twice over. Earlier this week, Deadline reported that CBS ordered a pilot for a new "<a href="/title/tt0088559?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">MacGyver</a>" TV show, one that would focus on the ...                                        <span class="nobr"><a href="/news/ni59467714?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59464622?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Super Bowl Weekend Preview: 'Panda 3' Over 'Hail, Caesar!', 'Pride and Prejudice and Zombies' & 'The Choice'</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59468138?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Susan Sarandon Responds to Piers Morganâs Criticism of Her Cleavage With More Cleavage (Photos)</a>
    <div class="infobar">
            <span class="text-muted">22 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59468139?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Netflix Will Debut David LynchâProduced Doc My Beautiful Broken Brain on March 18, Also Known As the Day You Cry the Hardest in Your Brief, Wonderful Life</a>
    <div class="infobar">
            <span class="text-muted">25 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59468132?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Ratings: CBSâ âThe Big Bang Theoryâ Takes Big Bite Out of Competition</a>
    <div class="infobar">
            <span class="text-muted">27 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59464034?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQzMzczMDY3Nl5BMl5BanBnXkFtZTgwOTM4NzQ2MDE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59464034?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >âGravityâsâ Jonas Cuaron to Write, Direct New Zorro Movie âZâ</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p><a href="/name/nm0190861?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Jonas Cuaron</a> is set to write and direct a new Zorro movie titled âZâ for <a href="/company/co0504823?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Lantica Media</a> and <a href="/company/co0086773?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Sobini Films</a>, the companies announced Thursday. They will produce the new film from award-winning writer of <a href="/title/tt1454468?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk4">Gravity</a>, <a href="/name/nm0190861?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk5">JonÃ¡s CuarÃ³n</a>. CuarÃ³n recently wrote and directed <a href="/title/tt3147312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk6">Desierto</a> which won the International ...                                        <span class="nobr"><a href="/news/ni59464034?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59467714?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >'MacGyver' Movie Reboot And New TV Series Pilot On The Way</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>The Playlist</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59464622?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Super Bowl Weekend Preview: 'Panda 3' Over 'Hail, Caesar!', 'Pride and Prejudice and Zombies' & 'The Choice'</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59468129?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Watch: Netflix Trailers For David Lynch Produced 'My Beautiful Broken Brain' & Alex Gibney's 'Cooked'</a>
    <div class="infobar">
            <span class="text-muted">39 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>The Playlist</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59467931?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >âMockingjayâ Helps Propel New Parisâ Studios Kremlin Into Limelight</a>
    <div class="infobar">
            <span class="text-muted">46 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59467547?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BODQ2NjE2NDY3Ml5BMl5BanBnXkFtZTgwNjQxNzA1NTE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59467547?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Orange Is the New Black: Renewed for Seasons Five, Six, & Seven</a>
    <div class="infobar">
            <span class="text-muted">just now</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000130?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>TVSeriesFinale.com</a></span>
    </div>
                                </div>
<p>The ladies on <a href="/title/tt2372162?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Orange Is the New Black</a> can expect to spend a lot more time in prison. Netflix has renewed their popular series for three years -- seasons five, six, and seven. Season four is scheduled to debut on June 17th. As part of the deal, showrunner <a href="/name/nm0463176?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Jenji Kohan</a> has committed to remain with the...                                        <span class="nobr"><a href="/news/ni59467547?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59464046?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >âWayward Pinesâ Season 2 Casts Jason Patric in Leading Role</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59467714?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >'MacGyver' Movie Reboot And New TV Series Pilot On The Way</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>The Playlist</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59464047?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Dan Stevens to Topline FXâs âLegionâ Pilot with Aubrey Plaza & Jean Smart</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59468132?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Ratings: CBSâ âThe Big Bang Theoryâ Takes Big Bite Out of Competition</a>
    <div class="infobar">
            <span class="text-muted">27 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59455002?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk4ODU3MDE0Nl5BMl5BanBnXkFtZTgwNTQwNjQ4NjE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59455002?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Ariel Winter Shares Body-Positive Post After Showing Breast Reduction Scars at SAG Awards: 'Embrace All That You Are'</a>
    <div class="infobar">
            <span class="text-muted">2 February 2016 6:20 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p><a href="/name/nm1736769?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Ariel Winter</a> is proud of her breast reduction scars - and wants everyone to feel just as confident. After spending Sunday defending her decision to wear a dress that showed her surgery scars to the SAG Awards, Winter, 18, took to Instagram Tuesday to encourage her followers to feel just as ...                                        <span class="nobr"><a href="/news/ni59455002?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59467923?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Todd Gurley -- I Could Be a Huge Movie Star ... Let's Make Billions!!!</a>
    <div class="infobar">
            <span class="text-muted">50 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000358?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>TMZ</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59467924?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Future -- Finally, I Get to Be a Doting Dad (Video)</a>
    <div class="infobar">
            <span class="text-muted">53 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000358?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>TMZ</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59467821?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Paris Jackson Admits She Attends AA Meetings, Says She Won't Let "Ridiculous" Expectations From Fans Affect Her</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59467878?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Hillary Clinton: 'Selfie-ing' Might Have to Stop If She's Elected President</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NewsDeskWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1086383360/rg784964352?ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402293022&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Chris Hemsworth in Ghostbusters (2016)" alt="Still of Chris Hemsworth in Ghostbusters (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTY0NDA5ODEzN15BMl5BanBnXkFtZTgwNzU4NDE5NzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0NDA5ODEzN15BMl5BanBnXkFtZTgwNzU4NDE5NzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1086383360/rg784964352?ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402293022&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Stills </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm883483904/rg1528338944?ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402293022&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Goat (2016)" alt="Goat (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTgwMzI2NjY5N15BMl5BanBnXkFtZTgwOTIwNTI5NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgwMzI2NjY5N15BMl5BanBnXkFtZTgwOTIwNTI5NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm883483904/rg1528338944?ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402293022&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1521018112/rg3854605056?ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402293022&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Uzo Aduba" alt="Uzo Aduba" src="http://ia.media-imdb.com/images/M/MV5BMjQ1MTQzMTAwN15BMl5BanBnXkFtZTgwNjEyNTI5NzE@._V1_SY201_CR32,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQ1MTQzMTAwN15BMl5BanBnXkFtZTgwNjEyNTI5NzE@._V1_SY201_CR32,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1521018112/rg3854605056?ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402293022&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Recent Red Carpet Stunners </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=2-5&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000492?ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jennifer Jason Leigh" alt="Jennifer Jason Leigh" src="http://ia.media-imdb.com/images/M/MV5BMTU1MzU2NTg0Nl5BMl5BanBnXkFtZTcwMjcyNjY0MQ@@._V1_SY172_CR8,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU1MzU2NTg0Nl5BMl5BanBnXkFtZTcwMjcyNjY0MQ@@._V1_SY172_CR8,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000492?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Jennifer Jason Leigh</a> (54) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001648?ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Charlotte Rampling" alt="Charlotte Rampling" src="http://ia.media-imdb.com/images/M/MV5BMTU0MTgyMjIzMF5BMl5BanBnXkFtZTcwOTc3NTA0Nw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0MTgyMjIzMF5BMl5BanBnXkFtZTcwOTc3NTA0Nw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001648?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Charlotte Rampling</a> (70) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0838911?ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jeremy Sumpter" alt="Jeremy Sumpter" src="http://ia.media-imdb.com/images/M/MV5BMjI4ODUzOTY0M15BMl5BanBnXkFtZTgwNTI5MjA0MTE@._V1_SY172_CR8,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4ODUzOTY0M15BMl5BanBnXkFtZTgwNTI5MjA0MTE@._V1_SY172_CR8,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0838911?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Jeremy Sumpter</a> (27) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0790688?ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Michael Sheen" alt="Michael Sheen" src="http://ia.media-imdb.com/images/M/MV5BMTg0MjI4NjY2M15BMl5BanBnXkFtZTcwOTgwNjQzMg@@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0MjI4NjY2M15BMl5BanBnXkFtZTcwOTgwNjQzMg@@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0790688?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Michael Sheen</a> (47) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001473?ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Laura Linney" alt="Laura Linney" src="http://ia.media-imdb.com/images/M/MV5BMTMyMDc3Mzc2M15BMl5BanBnXkFtZTcwMjc5OTcyMg@@._V1_SY172_CR4,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMyMDc3Mzc2M15BMl5BanBnXkFtZTcwMjc5OTcyMg@@._V1_SY172_CR4,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001473?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Laura Linney</a> (52) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=2-5&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BornTodayWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/february-tv-premieres/ls031481824?ref_=hm_pks_feb_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400720722&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>TV Premieres: What's On in February</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/february-tv-premieres/ls031481824?ref_=hm_pks_feb_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400720722&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010)" alt="The Walking Dead (2010)" src="http://ia.media-imdb.com/images/M/MV5BMTk1MjI1NjI0MV5BMl5BanBnXkFtZTcwODQ5MzA3Mw@@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk1MjI1NjI0MV5BMl5BanBnXkFtZTcwODQ5MzA3Mw@@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/february-tv-premieres/ls031481824?ref_=hm_pks_feb_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400720722&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Bob Odenkirk in Better Call Saul (2015)" alt="Bob Odenkirk in Better Call Saul (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTAxOTQ0MjUzMzJeQTJeQWpwZ15BbWU4MDY0NTAxNzMx._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAxOTQ0MjUzMzJeQTJeQWpwZ15BbWU4MDY0NTAxNzMx._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/february-tv-premieres/ls031481824?ref_=hm_pks_feb_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400720722&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Vikings (2013)" alt="Vikings (2013)" src="http://ia.media-imdb.com/images/M/MV5BOTEzNzI3MDc0N15BMl5BanBnXkFtZTgwMzk1MzA5NzE@._V1_SY298_CR14,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTEzNzI3MDc0N15BMl5BanBnXkFtZTgwMzk1MzA5NzE@._V1_SY298_CR14,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Plan your binge-watching and DVRing accordingly with our exhaustive list of new and returning TV shows due out this month.</p> <p class="seemore"><a href="/imdbpicks/february-tv-premieres/ls031481824?ref_=hm_pks_feb_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400720722&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See the complete list of premieres</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg97491712?ref_=hm_ph_ath_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400727302&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Athletes Who Became Screen Stars</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4272208640/rg97491712?ref_=hm_ph_ath_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400727302&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Dolph Lundgren in Rocky IV (1985)" alt="Still of Dolph Lundgren in Rocky IV (1985)" src="http://ia.media-imdb.com/images/M/MV5BMTM3MjQ3OTQ3N15BMl5BanBnXkFtZTcwNjA1ODY2NA@@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM3MjQ3OTQ3N15BMl5BanBnXkFtZTcwNjA1ODY2NA@@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm998223872/rg97491712?ref_=hm_ph_ath_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400727302&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Bruce Lee in Op&eacute;ration dragon (1973)" alt="Still of Bruce Lee in Op&eacute;ration dragon (1973)" src="http://ia.media-imdb.com/images/M/MV5BNjIzNjY0MTg3M15BMl5BanBnXkFtZTcwMTQxNzMyNw@@._V1_SY201_CR29,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjIzNjY0MTg3M15BMl5BanBnXkFtZTcwMTQxNzMyNw@@._V1_SY201_CR29,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2837288960/rg97491712?ref_=hm_ph_ath_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400727302&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Jason Lee in My Name Is Earl (2005)" alt="Still of Jason Lee in My Name Is Earl (2005)" src="http://ia.media-imdb.com/images/M/MV5BMTU1ODM0MDI3OV5BMl5BanBnXkFtZTYwMTg3ODk2._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU1ODM0MDI3OV5BMl5BanBnXkFtZTYwMTg3ODk2._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">We're taking a look at some famous former athletes who transitioned successfully to movie and television careers.</p> <p class="seemore"><a href="/gallery/rg97491712?ref_=hm_ph_ath_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2400727302&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Check out the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','RecsWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt2713180/trivia?item=tr2302636&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2713180?ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Fury (2014)" alt="Fury (2014)" src="http://ia.media-imdb.com/images/M/MV5BMjA4MDU0NTUyN15BMl5BanBnXkFtZTgwMzQxMzY4MjE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA4MDU0NTUyN15BMl5BanBnXkFtZTgwMzQxMzY4MjE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt2713180?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Fury</a></strong> <p class="blurb">The weapon Wardaddy (<a href="/name/nm0000093?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Brad Pitt</a>) refers to as a "grease gun" in the final battle scene was an M3 submachine gun.</p> <p class="seemore"><a href="/title/tt2713180/trivia?item=tr2302636&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TriviaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;5_D3yQv6ZC0&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/5_D3yQv6ZC0/?ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Fictional Movie or TV Football Team You Would Root For</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5_D3yQv6ZC0/?ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Replacements (2000)" alt="The Replacements (2000)" src="http://ia.media-imdb.com/images/M/MV5BMTQyMDQwMDgwNl5BMl5BanBnXkFtZTgwOTU5ODA5MjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQyMDQwMDgwNl5BMl5BanBnXkFtZTgwOTU5ODA5MjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5_D3yQv6ZC0/?ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Longest Yard (1974)" alt="The Longest Yard (1974)" src="http://ia.media-imdb.com/images/M/MV5BMTI2Njg2OTQ5OF5BMl5BanBnXkFtZTcwMjY5NDgyMQ@@._V1_SY207_CR3,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI2Njg2OTQ5OF5BMl5BanBnXkFtZTcwMjY5NDgyMQ@@._V1_SY207_CR3,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5_D3yQv6ZC0/?ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Family Guy (1999-)" alt="Family Guy (1999-)" src="http://ia.media-imdb.com/images/M/MV5BMTk4MzM0MTU2MV5BMl5BanBnXkFtZTgwMTIwMzg3MjE@._V1_SY207_CR19,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk4MzM0MTU2MV5BMl5BanBnXkFtZTgwMTIwMzg3MjE@._V1_SY207_CR19,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5_D3yQv6ZC0/?ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Coach (1989-1997)" alt="Coach (1989-1997)" src="http://ia.media-imdb.com/images/M/MV5BMTI5NTU5NDY4M15BMl5BanBnXkFtZTcwNzM2MTM1MQ@@._V1_SY207_CR2,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5NTU5NDY4M15BMl5BanBnXkFtZTcwNzM2MTM1MQ@@._V1_SY207_CR2,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5_D3yQv6ZC0/?ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Blue Mountain State (2010-2011)" alt="Blue Mountain State (2010-2011)" src="http://ia.media-imdb.com/images/M/MV5BMjQxOTI0MjcxM15BMl5BanBnXkFtZTcwOTY1MDk5NA@@._V1_SY207_CR2,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQxOTI0MjcxM15BMl5BanBnXkFtZTcwOTY1MDk5NA@@._V1_SY207_CR2,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Which fictional football team from movies or TV earns your support by being the type of team you would root for? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/252901933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/5_D3yQv6ZC0/?ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2403210822&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','PollWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BOTUyMTQ2MTI2OF5BMl5BanBnXkFtZTgwNjU4OTc3NzE@._UX500_CR20,25,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTUyMTQ2MTI2OF5BMl5BanBnXkFtZTgwNjU4OTc3NzE@._UX500_CR20,25,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjI4OTM0OTU0MF5BMl5BanBnXkFtZTgwNDg4MDE4NzE@._UX750_CR140,130,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4OTM0OTU0MF5BMl5BanBnXkFtZTgwNDg4MDE4NzE@._UX750_CR140,130,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-30"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/video/imdb/vi937996825/?ref_=hm_crq_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402415382&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Get Streaming With 'Chi-Raq'</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi937996825?ref_=hm_crq_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402415382&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-30&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi937996825" data-rid="0B1R33RR1N5FW5CAPWXE" data-type="single" class="video-colorbox" data-refsuffix="hm_crq" data-ref="hm_crq_i_1"> <img itemprop="image" class="pri_image" title="Chi-Raq (2015)" alt="Chi-Raq (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUxOTE2MzQ1NF5BMl5BanBnXkFtZTgwMDYxNTkzNzE@._UX600_CR110,40,307,230_BR15_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUxOTE2MzQ1NF5BMl5BanBnXkFtZTgwMDYxNTkzNzE@._UX600_CR110,40,307,230_BR15_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> <img alt="Chi-Raq (2015)" title="Chi-Raq (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Chi-Raq (2015)" title="Chi-Raq (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/video/imdb/vi937996825/?ref_=hm_crq_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402415382&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-30&pf_rd_t=15061&pf_rd_i=homepage" > <i>Chi-Raq</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb"><a href="/name/nm0000490/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402415382&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_crq_lk1">Spike Lee</a>'s <i><a href="/title/tt4594834/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402415382&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_crq_lk2">Chi-Raq</a></i> is available to <a href="/offsite/?page-action=offsite-amazon&token=BCYv0sTihBbWmdq7hNyOcxkZxi65xFMbFqOmE7gYRIyR0af4NdkCVQEdhyEiBP_AGX4NTR-cPU2J%0D%0AH4-SYEHpVFd17N5a-fZ0qOTeZoyw4BJXOg111dfbD8MQ5M-5rusSqNO-PeRoM0t78-Sv-CAhzPlK%0D%0A6se-HIgaGdsAlbfVuR_7RjKr0xQiuG2MAMV444lA9dkiavKeKXaYXVrFTNlGVZAiUl_f0_sG5JKV%0D%0ABxpoeAoZSZwXlzjR7EFRMRCRe_lo-LHPpjsuUJd2qM91bta_Ee_gZcY8VSaFU4Mafr_MVyp1iHc%0D%0A&ref_=hm_crq_lk3">stream now on Prime</a>. In celebration of this good news, we're serving up one of our favorite clips from the movie, featuring <a href="/name/nm0152638/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402415382&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_crq_lk4">Dave Chappelle</a>.</p> <p class="seemore"><a href="/video/imdb/vi937996825/?ref_=hm_crq_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402415382&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=center-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch the clip</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=556633926141;ord=556633926141?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=556633926141?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=556633926141?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0475290"></div> <div class="title"> <a href="/title/tt0475290?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Hail, Caesar! </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3797868"></div> <div class="title"> <a href="/title/tt3797868?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The Choice </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1374989"></div> <div class="title"> <a href="/title/tt1374989?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Pride and Prejudice and Zombies </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3319920"></div> <div class="title"> <a href="/title/tt3319920?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> RÃ©gression </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4935334"></div> <div class="title"> <a href="/title/tt4935334?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Southbound </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1702429"></div> <div class="title"> <a href="/title/tt1702429?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Que viva Eisenstein! </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2338424"></div> <div class="title"> <a href="/title/tt2338424?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Tumbledown </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402982742&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402983022&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2267968"></div> <div class="title"> <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Kung Fu Panda 3 </a> <span class="secondary-text">$41.3M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1663202"></div> <div class="title"> <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> The Revenant </a> <span class="secondary-text">$12.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Star Wars: Episode VII - The Force Awakens </a> <span class="secondary-text">$11.1M</span> </div> <div class="action"> <a href="/showtimes/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2025690"></div> <div class="title"> <a href="/title/tt2025690?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> The Finest Hours </a> <span class="secondary-text">$10.3M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2869728"></div> <div class="title"> <a href="/title/tt2869728?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Ride Along 2 </a> <span class="secondary-text">$8.4M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BoxOfficeListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1431045"></div> <div class="title"> <a href="/title/tt1431045?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Deadpool </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1608290"></div> <div class="title"> <a href="/title/tt1608290?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Zoolander 2 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1292566"></div> <div class="title"> <a href="/title/tt1292566?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> How to Be Single </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4897822"></div> <div class="title"> <a href="/title/tt4897822?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Where to Invade Next </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4399594"></div> <div class="title"> <a href="/title/tt4399594?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Fitoor </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src="http://ia.media-imdb.com/images/G/01/imdb/html/twitter-1645856857._CB299761032_.html?config=%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D"></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TwitterWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','FacebookWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/list/ls033703263?ref_=hm_rom_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402261522&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Top 15 Romance Movies From the Past 35 Years</h3> </a> </span> </span> <p class="blurb">Love is in the air ... and on the big screen. With Valentine's Day just around the corner, we run down the top 15 romance movies from the past 35 years as ranked by IMDb users.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/list/ls033703263?ref_=hm_rom_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402261522&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Ryan Gosling and Rachel McAdams in The Notebook (2004)" alt="Still of Ryan Gosling and Rachel McAdams in The Notebook (2004)" src="http://ia.media-imdb.com/images/M/MV5BMTMxODE3NDQwNl5BMl5BanBnXkFtZTcwMjUxMTg0NA@@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMxODE3NDQwNl5BMl5BanBnXkFtZTcwMjUxMTg0NA@@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/list/ls033703263?ref_=hm_rom_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2402261522&pf_rd_r=0B1R33RR1N5FW5CAPWXE&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Check out the complete list</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYqrQgCb6RGvjZVH4_HbAv3eSJ52_2OVFW838YdQD2lfy8CM3HKZITVx01BklKAU4HuyUi3dmrv%0D%0AYvoMZoO8fC5Bln5YtLl-iJ3oRB3ZuabfJ8hecYo-h7kRuvNrJG2PABLjycP0irL6JBi-HOuxVdz4%0D%0AasV4PdLP2qoqShdNZ5pkq1fL1nwNFHuqVpYDMhOIeaGGQpvjKyN1N0c2N88gupPf1o4yKo27-hus%0D%0At62jIGuHIYM%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYl6qB2kWrUAfcPRGZEZzACaPIgLwvhZxFxvm1EfU-7Z-9MlDmOJNcp3zSoOsvulhDS7goiwQVz%0D%0Al6_Lny1kVP1kz2DC3_8MPXdZUKO1REDCDpZV-cAvXsacW9QUawc-C8PCol1oY3oCDqy4I_gDXLAc%0D%0A-sHDhqMNgxt4zWZPukfgwECM8CNkEV04c6xN2-sbzO5IK8WnpzzepzvukCmN0OwghA%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYk5EfxsM3usTXrDiZTTc8VnaylP_MsUmodqTnbUcXM2mp5zcK22OB85CE8HqSywqAOzdpqlrMF%0D%0AQpEMh-9QbDCaUxs1swga22ygomVQb1hu9tKrfJ5Q-MZj4QE6xG_EVErKYK7EPk58pbbg2YDCRpzA%0D%0ADXw0ET-P-2QDx3_49JN2-cLXhkOV7lWW1O0ug7Ye_usDiLN76T1n3SwOhzANxhEfWHuOMDi8VSxU%0D%0A71ossJfKpsafiFdHSKs7f7UCiiukHI6DQONKtD9MXQ4WNjVrr7y435YUyJx2X5Hd677F0Bl5qKgY%0D%0AyKgSx5hb0ggtf-7-8nkrs-vVLydPRG6ETrUQgouDIIl_aMutnAkwx4WgGJjBrZWaxDQYWoYn0Uur%0D%0AiCoXqfvCa5c7eCTCBiAl3ACjkCEX26mtQQYhL_6OCcQwp4rUTgk%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYmodNOs8ubbDyR4RoCuHUeai-JHXQswA_xJWVEat5fI9BxjESANcpRWAT84Pv92p8lM5sC02WA%0D%0AJIBK8j5QDz_QyBLGopag2kBrG7q9U8SDzsCgm_HPdE34zN1omToVdByL6RnrXF3xqOu9kXkXCi9x%0D%0AmbMjDOYFnySBh1KOwKUg6bC4Hr365o-GlUrZedWtvRay6u8tBzY17zLqyqjEYikfKQ%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYtYo_myd2Lv1C3fYev-yuCHTj5S1JcYgy5C0HyuRaE-_AQvOjJWEnZVRjlwesbsvg1K997LnQF%0D%0As6jY5A0WntEsVyefcST0F3XPQ94OE6TJ23LnkGAPVUC45b1N4N__Rs3GHZzCc3LJ90eY4Lxf5WN7%0D%0Alt9VGgsMmb60LWzAvKBvO_LH3p3VIXeZWVQ5UxW8t60P%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYmvTLsNdXLkbXrQ-Tu8PD82fzqJLAPcq0_uaLoyHJFqiTE4Td7mFdeWhJwrQ74F6vpSH19R7DM%0D%0A6USUfKlbPWnYRWHUgOsz_Sx8wJIgqrkc8t047-KnK8duPjtuPTydaiR1ooCFIQOXVtmt0wFVoIXg%0D%0AHLVRioktr0cKDoHSH6dlBpb0aFyLeP-tOsWiJXkzqk_J%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYmBP27uxzDuWwUnWeDdSKW6-I4TWIE4xSSqlBG6vkWmFMerQG5sZLzXbZx7jVduLuMbuFDSlTY%0D%0AcvmitubMP6WiYofFh8HpRw0yumozxgK7TmWKMmmwNeRGuJ6bJaz0imdjWzlDOfT_YfQcoTi8N2RU%0D%0AAokAm_-HvMiyf8uBC3L0wtsiIqUyPV_OYYpfpRHnN-Y2JsFAjdtl5PTPX-cd4cndRw%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYoNrS_CWlox_QGpaCzHu6RGTEw39NndfcF9s_79J9VSle050FWWeY3Yqk4R-inu6iFYuKprf4Y%0D%0Aqm0JiaM0b8xwk6fHB6iFNXtbmkg6R58hZ2Q9rGuN5imuwyM2rIZ2yEhriQdOU6gUjTEB8NMV7NJv%0D%0AzPM-L6VO46afk_arcj58z61KHeVHPD5RCRQ9xiCZQMlm%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYkdQYRuEKn9bRlkaXlA-eD8xc2f5E15nKjZy96tpfcBtolhAZMcvutohalR68eOhdB7gs0tR3E%0D%0A7nexmQmBUlgGGWPrQn-Ftag2FEiaCZYPY9AjLbCgqGaLarE1XUu0lHiGhpiBrPOpJ9Rw4tu4l9Os%0D%0AKIbWIOT6AGZz0Dgv_IaSsswipFMzFdwi-O_wiYdb4DVxltrNkrkf-XUSNCQ5T3Uia7bAZ70GmHb-%0D%0AKEtEcK8TqHOESIYsB07rJmlJtE96eu50%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYhenvRaMPRNkBA3MzzlRxgpxeYrB5LjhraoTw5uceFfbXizGTMPMtoO14e9dAKzUOlQizDZG7i%0D%0AAnj5yrnN1UPbHow4o45Q3kVK8OyqMQXw4IzSYP4QsBb5P4tSGxw8cmGS8_1557H4tSKdJgk57Qx5%0D%0ANSzQoHwwjRMb-s6WQqUGnvAudUylqrTDKww63JQzeoIC3PYktbyiWTUvJSPLqBqUQc1j9VwGdbgd%0D%0A_OPDmeaiplc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYjG98WjcA4gggWFwXM1rBc22ZitHX9UPnppE-xNYonSfEoDwjLYp5dvTef6zomtKCKLosQnJVx%0D%0ATEYQrCCk0j6Y2CVfI7856IcEivhI5QjhK9sKHdl9D1sKK7VpWQvCuB22eKhcVHokHmX87ZsXHCsL%0D%0ARTrU4D666BoXRAE-V8ZYw0WDiOnE3uGYE0XwwnmtbRlKFChAa77xXj7H68OypZDSkhJtzV68wuvP%0D%0AlJ-s0EcbZ8g%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYnBej23TYASgRAVa6whb-c7g1wR6CuU4a6D14Nn1SPWi3uVeSuxnAVXLTt6LawL4YEjoJTqjKl%0D%0A_1B2B-CirnZNai2tA9IVFGl1PtLgIDvPwCI3_TQRvQhhirlEn-FmfsfiPfC3cMm2TDOJEGkuLKxP%0D%0A6lINqRfO_wbTmoFsiKuYCwuJn5mjDnqXadu27qwymjIltA8YHrDLejdWwcql3gc_c6DVkeFLcNLp%0D%0AduJy0IWitNc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYnLSladoURCBARSp8nBFoy0MxLopNi_jxJpTIDot7aC9LTTcTjzGTYV4iOvVR-NmOnoi59kJNo%0D%0ACS18x5csViTvJdHwtoBny9OFo6NUdcMJG27C5a19nTcvuREW57kK3yZ45G6EUmkCIuWJzK_R6YHR%0D%0AhCJHbDjVpbMTJYvFrQhdQMZfB-eJfJy0jHPoIbo9_d6WFHJ26HRpO7C9CmhtNy6XdR5t-e3kDxxv%0D%0A7QHJ-wvI41c%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYvxoxKvyfoFwCzLr1oZxHUu3K6bgly0e3ZEnSD6FVG8aBXGITnAcLFKml6v1Kzg22g6v5UPqsQ%0D%0APnO2o7p64a6Q-m1c5TUjzLs5WaCBcfC6aXts2xfhZziCPhCMDB517DlNsDA_wdcY4SukT21U9vs_%0D%0AB4e0YSmhUF6jRk1QpkoMIkE-E-sWmPrZUB2wzbftTGFpDVcSynWDwPP6o8dIDl2QhzVHQze45y-T%0D%0ANR5mJbc0v9w%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYg69yWPVND7TeVZkyYTHMVRYv3MMr7D7YDphxOjjatV4wZkqxgBKSph5y4j2nNVG7cg0w2IpjE%0D%0AzQ58odPhqHCuVEIMVXJferWtbclDTI7LPreZ-uEkWzbrTsH_jcTYVJ67tenkCYqFaiUqbqyqdXIl%0D%0Aq34NDZifrOVAAB14SYiyIAY_jmPUwHNz4yxtU-zxmcmSKv79WfEJbRjrXWtyMsRsoJsZEP6lE0cm%0D%0A6_Hw1tD-mavfnmQQ6SsVn7kaRSCL5T98%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYvDcB45SGE1GI6XkhCfxIvDWebMStks8mXigFXh7X7pD2qE8Ur0KPhjpf60XYkU8BQ134LgNBN%0D%0ATCmJEzddbygZLsfjDSiQn9DYjQpqW1kIjTpwvL8AaNoipdw_SU4-hVq-ROfu7KVmrt3xCifEF47i%0D%0A2CteHZa6aeHs7s7F9kvaC5g%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYu-7FeLZi6a6HX-qQojcZDy8nEZhysdxJlXq_9PisHIHTY5qla3rf_c15XQ9sGGX-MPqyNTSY-%0D%0AEE-yjFEy3YUOE_giGmbUQHss-yUnOiCYPzK_ah9hiVY-Y2JQ63TVVwXVpiaatYVFI-de6Mu8LKbg%0D%0ARmrNc4ICvlQrLfPvpZ0VTik%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-774728307._CB299450936_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-47944033._CB297958161_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=01013cc5e6efd7e1577f130ad3335dc872cd9ac08e1c9c8a170131abf4180c100283",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=556633926141"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-1675743762._CB299577842_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=556633926141&ord=556633926141";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="287"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
