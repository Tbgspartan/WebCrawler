ï»¿<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>China.com - Your guide on traveling and living in China</title>
<meta name="keywords" content="China,travel,lifestyle,learn Chinese,news,videos,business,films,sports,reports" />
<meta name="description" content="English.china.com is a one-stop shop for everything about China â news, events, culture, people, lifestyle, language. It also provides information about traveling and living in China." />
<meta name="auther" content="F7 13489" />
<!-- /etc/htmlhead.shtml Start -->
<link href="/css/style.css?20141205.1" rel="stylesheet" />
<!--[if lte IE 6]> 
<script src="/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.page-select-language-list em, .page-nav .page-openSearch em, .page-focus-prevnext a, .page-video-right .item-text, .page-watched .play, .page-video-right .item-icon, .page-video-right .item-watch'); 
</script>
<![endif]-->
<!-- /etc/htmlhead.shtml End -->
<!-- /etc/goto3g.shtml Start -->

<!-- /etc/goto3g.shtml End -->
</head>

<body>
<!-- /etc/channelhomehead.shtml Start -->
<div class="page-head">
  <div class="page-top-bg" id="page-top">
    <div class="maxWidth page-top">
      <div class="page-logo"><a href="/index.html"><img src="/img/logo.png" alt="china.com" width="208" height="46" /></a></div>
      <div class="page-language">
        <em>language:English</em>
      </div>
      <div class="page-top-right">
          <div class="page-top-time">Thursday, October 24, 2013</div>
          <div class="page-select-language" id="page-select-language">
              <div class="page-show-language" style="display: block;">Language</div>
              <div class="page-select-language-tit" style="display: none;"><i>Language</i></div>
              <div class="page-select-language-list" style="display: none;">
                <a href="http://www.china.com/index.html" class="langCn"><em>www</em></a>
                <a href="http://english.china.com/index.html" class="langEn"><em>english</em></a>
                <a href="http://german.china.com/index.html" class="langDe"><em>german</em></a>
                <a href="http://italy.china.com/index.html" class="langIt"><em>italy</em></a>
                <a href="http://portuguese.china.com/index.html" class="langPt"><em>portuguese</em></a>
                <a href="http://french.china.com/index.html" class="langFr"><em>french</em></a>
                <a href="http://russian.china.com/index.html" class="langRu"><em>russian</em></a>
                <a href="http://espanol.china.com/index.html" class="langEs"><em>espanol</em></a>
                <a href="http://malay.china.com/index.html" class="langMy"><em>malay</em></a>
                <a href="http://vietnamese.china.com/index.html" class="langVn"><em>vietnamese</em></a>
                <a href="http://laos.china.com/index.html" class="langLa"><em>laos</em></a>
                <a href="http://cambodian.china.com/index.html" class="langKh"><em>cambodian</em></a>
                <a href="http://thai.china.com/index.html" class="langTh"><em>thai</em></a>
                <a href="http://indonesian.china.com/index.html" class="langId"><em>indonesian</em></a>
                <a href="http://filipino.china.com/index.html" class="langPh"><em>filipino</em></a>
                <a href="http://myanmar.china.com/index.html" class="langMm"><em>myanmar</em></a>
                <a href="http://japanese.china.com/index.html" class="langJp"><em>japanese</em></a>
                <a href="http://korean.china.com/index.html" class="langKr"><em>korean</em></a>
                <a href="http://mongol.china.com/index.html" class="langMn"><em>mongol</em></a>
                <a href="http://nepal.china.com/index.html" class="langNp"><em>nepal</em></a>
                <a href="http://hindi.china.com/index.html" class="langIn"><em>hindi</em></a>
                <a href="http://bengali.china.com/index.html" class="langMd"><em>bengali</em></a>
                <a href="http://turkish.china.com/index.html" class="langTr"><em>turkish</em></a>
                <a href="http://persian.china.com/index.html" class="langIr"><em>persian</em></a>
                <a href="http://arabic.china.com/index.html" class="langAe"><em>arabic</em></a>
              </div>
            </div>
            <div class="page-search">
              <form id="web-search" name="web-search" method="get" action="http://www.google.com/search">
                <input type="hidden" name="sitesearch" value="english.china.com" />
                <input type="text" name="q" class="web-search-keyword" placeholder="Search here..." />
                <input type="submit" name="button" class="web-search-but" value="Search Site" />
              </form>
            </div>
      </div>
    </div>
  </div>
  <div class="page-nav-bg" id="page-nav">
    <ul class="page-nav maxWidth">
      <li class="page-openSearch"><a href="#"><em>Open search</em></a></li>
      <li class="small-logo"><a href="/index.html"><img src="/img/small-logo.png" /></a></li>
      <li><a href="/news/index.html">News</a></li>
      <li><a href="/video/index.html">Video</a></li>
      <!--<li><a href="/audio/index.html">Audio</a></li>-->
      <!--<li><a href="/photos/index.html">Photos</a></li>-->
      <li><a href="/travel/index.html">Travel</a></li>
      <!--<li><a href="/lifestyle/index.html">Lifestyle</a></li>-->
      <li><a href="/chinese/index.html">Learn Chinese</a></li>
	  <li><a href="http://english.china.com/cityguide/">City Guide</a></li>
      <li><a href="http://mail.china.com/en/" class="ext" target="_blank">Free Mail</a></li>
    </ul>
  </div>
</div><!-- page-head End -->
<!-- /etc/channelhomehead.shtml End -->

<div class="maxWidth">
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 #17286  Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=206" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 End -->
</div>

<div class="page-main maxWidth">
  <div class="page-left">
    <div class="page-focus" id="page-focus">
      <div class="page-focus-body" id="page-focus-body">
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2016/02/22/4203s917533.htm" title="People Hold Activities to Celebrate Lantern Festival"><img src="http://img03.mini.abroad.imgcdc.com/english/news/topphotos/china/189/20160222/577953_151370.jpg.680x330.jpg" width="680" height="330" alt="People Hold Activities to Celebrate Lantern Festival" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2016/02/22/4203s917533.htm" title="People Hold Activities to Celebrate Lantern Festival" class="title_default">People Hold Activities to Celebrate Lantern Festival</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2016-02/21/c_135117874.htm" title="Chinese Icebreaker "Xuelong" Arrives at Casey Station"><img src="http://img04.mini.abroad.imgcdc.com/english/home/topphoto/1295/20160222/577544_151207.jpg.680x330.jpg" width="680" height="330" alt="Chinese Icebreaker "Xuelong" Arrives at Casey Station" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2016-02/21/c_135117874.htm" title="Chinese Icebreaker "Xuelong" Arrives at Casey Station" class="title_default">Chinese Icebreaker "Xuelong" Arrives at Casey Station</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2016-02/21/c_135116593.htm" title="Documentary film "Fire at Sea" wins Golden Bear at Berlin"><img src="http://img04.mini.abroad.imgcdc.com/english/news/topphotos/showbiz/1211/20160221/577224_151127.jpg.680x330.jpg" width="565" height="250" alt="Documentary film "Fire at Sea" wins Golden Bear at Berlin" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2016-02/21/c_135116593.htm" title="Documentary film "Fire at Sea" wins Golden Bear at Berlin" class="title_default">Documentary film "Fire at Sea" wins Golden Bear at Berlin</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.chinadaily.com.cn/culture/2016-02/19/content_23560115.htm" title="Celebrating 'Eat Lovesickness' in Guizhou"><img src="http://img01.abroad.imgcdc.com/english/news/topphotos/showbiz/1211/20160220/576993_151068_680x330.jpg" width="680" height="330" alt="Celebrating 'Eat Lovesickness' in Guizhou" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.chinadaily.com.cn/culture/2016-02/19/content_23560115.htm" title="Celebrating 'Eat Lovesickness' in Guizhou" class="title_default">Celebrating 'Eat Lovesickness' in Guizhou</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.shanghaidaily.com/national/Sea-of-flowers-embraces-spring-in-S-China/shdaily.shtml" title="Sea of flowers embraces spring in S China"><img src="http://img03.abroad.imgcdc.com/english/news/topphotos/china/189/20160220/576989_151066_680x330.jpg" width="680" height="330" alt="Sea of flowers embraces spring in S China" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.shanghaidaily.com/national/Sea-of-flowers-embraces-spring-in-S-China/shdaily.shtml" title="Sea of flowers embraces spring in S China" class="title_default">Sea of flowers embraces spring in S China</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
      </div>
      <div id="page-focus-console"></div>
      <div class="page-focus-prevnext">
        <a href="#" id="page-focus-prev">Previous focus</a>
        <a href="#" id="page-focus-next">Next focus</a>
      </div>
    </div><!-- page-focus End -->
    
    <div class="page-latest">
      <h2 class="modTit"><strong><a href="/news/index.html">LATEST NEWS</a></strong></h2>
      <div class="page-latest-body" id="page-latest">
        <div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/22/3685s917565.htm" title="Chinese Film Wins Cinematography Award in Berlin"><img src="http://img02.abroad.imgcdc.com/english/news/showbiz/58/20160222/577958_151373_200x120.jpg" width="200" height="120" alt="Chinese Film Wins Cinematography Award in Berlin" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/02/22 16:37:28</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/22/3685s917565.htm" title="Chinese Film Wins Cinematography Award in Berlin" class="title_default">Chinese Film Wins Cinematography Award in Berlin</a></h3>
            <p class="item-infor" title="The cinematography award at the Berlin International Film Festival went to Mark Lee Ping-Bing for his camera work for the Chinese film Crosscurrent, directed by Yang Chao.">The cinematography award at the Berlin International Film Festival went to Mark Lee Ping-Bing for his camera work for the Chinese film Crosscurrent, directed by Yang Chao.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/business/tech/2016-02/22/content_23590218.htm" title="Huawei launches new 2-in-1 style laptop at Barcelona Congress"><img src="http://img01.mini.abroad.imgcdc.com/english/news/business/56/20160222/577956_151372.jpg.200x120.jpg" width="160" height="103" alt="Huawei launches new 2-in-1 style laptop at Barcelona Congress" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/22 16:35:22</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/business/tech/2016-02/22/content_23590218.htm" title="Huawei launches new 2-in-1 style laptop at Barcelona Congress" class="title_default">Huawei launches new 2-in-1 style laptop at Barcelona Congress</a></h3>
            <p class="item-infor" title="The telecom giant launched a product that combines a smartphone and a laptop, to further explore the wide international consumer market.">The telecom giant launched a product that combines a smartphone and a laptop, to further explore the wide international consumer market.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/22/c_135117971.htm" title="London mayor says to support Brexit in EU referendum"><img src="" width="" height="" alt="London mayor says to support Brexit in EU referendum" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/22 16:34:27</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/22/c_135117971.htm" title="London mayor says to support Brexit in EU referendum" class="title_default">London mayor says to support Brexit in EU referendum</a></h3>
            <p class="item-infor" title="London Mayor Boris Johnson announced Sunday that he will back the call for Britain to leave the European Union in a national referendum on the country's EU membership.">London Mayor Boris Johnson announced Sunday that he will back the call for Britain to leave the European Union in a national referendum on the country's EU membership.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/22/3941s917525.htm" title="Xinjiang to Face Guangdong in CBA Playoffs Semi-finals"><img src="http://img02.mini.abroad.imgcdc.com/english/news/sports/57/20160222/577548_151209.jpg.200x120.jpg" width="200" height="120" alt="Xinjiang to Face Guangdong in CBA Playoffs Semi-finals" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/22 07:26:02</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/22/3941s917525.htm" title="Xinjiang to Face Guangdong in CBA Playoffs Semi-finals" class="title_default">Xinjiang to Face Guangdong in CBA Playoffs Semi-finals</a></h3>
            <p class="item-infor" title="Liaoning, Guangdong, Xinjiang and Sichuan all have reached the semi-finals of the CBA playoffs. Xinjiang booked the final spot in the semifinals following a 103-94 win over the Beijing Ducks on Sunday night.
">Liaoning, Guangdong, Xinjiang and Sichuan all have reached the semi-finals of the CBA playoffs. Xinjiang booked the final spot in the semifinals following a 103-94 win over the Beijing Ducks on Sunday night.
</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/22/3941s917520.htm" title="Chelsea Defeat Manchester City 5-1 to Reach FA Cup Quarter-finals"><img src="http://img01.mini.abroad.imgcdc.com/english/news/sports/57/20160222/577547_151208.jpg.200x120.jpg" width="200" height="120" alt="Chelsea Defeat Manchester City 5-1 to Reach FA Cup Quarter-finals" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/22 07:25:18</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/22/3941s917520.htm" title="Chelsea Defeat Manchester City 5-1 to Reach FA Cup Quarter-finals" class="title_default">Chelsea Defeat Manchester City 5-1 to Reach FA Cup Quarter-finals</a></h3>
            <p class="item-infor" title="Chelsea has earned a spot in the quarter-finals of the English FA Cup, rolling over Manchester City 5-1 earlier this Monday morning.">Chelsea has earned a spot in the quarter-finals of the English FA Cup, rolling over Manchester City 5-1 earlier this Monday morning.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/22/c_135118016.htm" title="U.S., Israel Start Joint Drill on Ballistic Missile Defense"><img src="" width="" height="" alt="U.S., Israel Start Joint Drill on Ballistic Missile Defense" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/22 07:24:28</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/22/c_135118016.htm" title="U.S., Israel Start Joint Drill on Ballistic Missile Defense" class="title_default">U.S., Israel Start Joint Drill on Ballistic Missile Defense</a></h3>
            <p class="item-infor" title="Israel and the United States started a joint exercise on Sunday focusing on defense against the threat of ballistic missiles, the Israeli army said.">Israel and the United States started a joint exercise on Sunday focusing on defense against the threat of ballistic missiles, the Israeli army said.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/22/c_135118149.htm" title="Huawei, ZTE, Samsung Launch Products ahead of MWC"><img src="" width="" height="" alt="Huawei, ZTE, Samsung Launch Products ahead of MWC" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/22 07:22:53</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/22/c_135118149.htm" title="Huawei, ZTE, Samsung Launch Products ahead of MWC" class="title_default">Huawei, ZTE, Samsung Launch Products ahead of MWC</a></h3>
            <p class="item-infor" title="Chinese telecommunications companies Huawei and ZTE, as well as Korean Samsung all launched products on Sunday ahead of the opening of the Mobile World Congress (MWC) here on Monday.">Chinese telecommunications companies Huawei and ZTE, as well as Korean Samsung all launched products on Sunday ahead of the opening of the Mobile World Congress (MWC) here on Monday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/21/4061s917421.htm" title="Documentary "Fire at Sea" Wins Golden Bear in 66th Berlinale"><img src="http://img01.abroad.imgcdc.com/english/news/showbiz/58/20160221/577517_151188_200x120.jpg" width="200" height="120" alt="Documentary "Fire at Sea" Wins Golden Bear in 66th Berlinale" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/02/21 20:35:31</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/21/4061s917421.htm" title="Documentary "Fire at Sea" Wins Golden Bear in 66th Berlinale" class="title_default">Documentary "Fire at Sea" Wins Golden Bear in 66th Berlinale</a></h3>
            <p class="item-infor" title="The Italian documentary film "Fire at Sea" won the Golden Bear, the top jury prize awarded to the best film, in the 66th Berlin International Film Festival on Saturday.">The Italian documentary film "Fire at Sea" won the Golden Bear, the top jury prize awarded to the best film, in the 66th Berlin International Film Festival on Saturday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/sports/2016-02/21/content_23573322.htm" title="Errani wins by a mile in Dubai against Strycova"><img src="http://img04.mini.abroad.imgcdc.com/english/news/sports/57/20160221/577516_151187.jpg.200x120.jpg" width="200" height="120" alt="Errani wins by a mile in Dubai against Strycova" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/21 20:34:25</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/sports/2016-02/21/content_23573322.htm" title="Errani wins by a mile in Dubai against Strycova" class="title_default">Errani wins by a mile in Dubai against Strycova</a></h3>
            <p class="item-infor" title="Errani took the first set within half a hour. "Yes, I was surprised about winning the match so fast and so straight, but Barbora also had not her best day. It is amazing that I won," said the Italian who cashed in 465,480 U. S. dollars prize money. ">Errani took the first set within half a hour. "Yes, I was surprised about winning the match so fast and so straight, but Barbora also had not her best day. It is amazing that I won," said the Italian who cashed in 465,480 U. S. dollars prize money. </p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/21/2702s917470.htm" title="Lock-up Shares Worth 42.3 Bln Yuan to Become Tradable"><img src="" width="" height="" alt="Lock-up Shares Worth 42.3 Bln Yuan to Become Tradable" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/21 20:33:56</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/21/2702s917470.htm" title="Lock-up Shares Worth 42.3 Bln Yuan to Become Tradable" class="title_default">Lock-up Shares Worth 42.3 Bln Yuan to Become Tradable</a></h3>
            <p class="item-infor" title="Lock-up shares worth 42.3 billion yuan (6.5 billion U.S. dollars) will become eligible for trade on China's stock markets in the coming week.">Lock-up shares worth 42.3 billion yuan (6.5 billion U.S. dollars) will become eligible for trade on China's stock markets in the coming week.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/21/c_135117042.htm" title="Tens of thousands rally in major U.S. cities to protest conviction of former New York policeman"><img src="http://img03.abroad.imgcdc.com/english/news/world/55/20160221/577514_151186_200x120.jpg" width="200" height="120" alt="Tens of thousands rally in major U.S. cities to protest conviction of former New York policeman" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/21 20:33:37</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/21/c_135117042.htm" title="Tens of thousands rally in major U.S. cities to protest conviction of former New York policeman" class="title_default">Tens of thousands rally in major U.S. cities to protest conviction of former New York policeman</a></h3>
            <p class="item-infor" title="Tens of thousands of people rallied Saturday in more than 30 American cities to protest the conviction of Peter Liang, a former New York police officer of Chinese descent.">Tens of thousands of people rallied Saturday in more than 30 American cities to protest the conviction of Peter Liang, a former New York police officer of Chinese descent.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://en.people.cn/n3/2016/0221/c90785-9019112.html" title="President Xi's media tour draws positive feedback from reporters, journalism academia"><img src="http://img02.abroad.imgcdc.com/english/news/china/54/20160221/577513_151185_200x120.jpg" width="200" height="120" alt="President Xi's media tour draws positive feedback from reporters, journalism academia" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/02/21 20:33:15</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://en.people.cn/n3/2016/0221/c90785-9019112.html" title="President Xi's media tour draws positive feedback from reporters, journalism academia" class="title_default">President Xi's media tour draws positive feedback from reporters, journalism academia</a></h3>
            <p class="item-infor" title="President Xi Jinping's inspection tour to three leading news providers on Friday was lauded by reporters, experts and students majoring in journalism.">President Xi Jinping's inspection tour to three leading news providers on Friday was lauded by reporters, experts and students majoring in journalism.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/21/2743s917448.htm" title="Beijing's Average PM2.5 Density in 2015 Drops by 9.9 Pct from 2013"><img src="http://img03.abroad.imgcdc.com/english/news/china/54/20160221/577232_151130_200x120.jpg" width="200" height="120" alt="Beijing's Average PM2.5 Density in 2015 Drops by 9.9 Pct from 2013" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/02/21 09:14:43</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/21/2743s917448.htm" title="Beijing's Average PM2.5 Density in 2015 Drops by 9.9 Pct from 2013" class="title_default">Beijing's Average PM2.5 Density in 2015 Drops by 9.9 Pct from 2013</a></h3>
            <p class="item-infor" title="The decrease comes after the city's two-year long effort to curb pollution.">The decrease comes after the city's two-year long effort to curb pollution.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/21/c_135116752.htm" title="Errani wins Dubai Open in straight sets"><img src="http://img02.abroad.imgcdc.com/english/news/sports/57/20160221/577231_151129_200x120.jpg" width="200" height="120" alt="Errani wins Dubai Open in straight sets" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/21 09:13:29</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/21/c_135116752.htm" title="Errani wins Dubai Open in straight sets" class="title_default">Errani wins Dubai Open in straight sets</a></h3>
            <p class="item-infor" title="Italy's Sara Errani was victorious over Czech Barbora Strycova 6-0, 6-2 in the final of the WTA Dubai Open. ">Italy's Sara Errani was victorious over Czech Barbora Strycova 6-0, 6-2 in the final of the WTA Dubai Open. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/2016-02/20/content_23571175.htm" title="WHO dispels 4 biggest rumors about Zika and microcephaly"><img src="http://img01.mini.abroad.imgcdc.com/english/news/world/55/20160221/577230_151128.jpg.200x120.jpg" width="160" height="108" alt="WHO dispels 4 biggest rumors about Zika and microcephaly" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/21 09:10:01</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/2016-02/20/content_23571175.htm" title="WHO dispels 4 biggest rumors about Zika and microcephaly" class="title_default">WHO dispels 4 biggest rumors about Zika and microcephaly</a></h3>
            <p class="item-infor" title="While the Zika virus is spreading explosively in the Americas, rumors about the virus and microcephaly are also swirling.">While the Zika virus is spreading explosively in the Americas, rumors about the virus and microcephaly are also swirling.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2016/02/20/4182s917342.htm" title=""To Kill a Mockingbird" Author Harper Lee Dies at 89"><img src="http://img02.abroad.imgcdc.com/english/news/showbiz/58/20160220/576995_151069_200x120.jpg" width="200" height="120" alt=""To Kill a Mockingbird" Author Harper Lee Dies at 89" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2016/02/20 16:51:51</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2016/02/20/4182s917342.htm" title=""To Kill a Mockingbird" Author Harper Lee Dies at 89" class="title_default">"To Kill a Mockingbird" Author Harper Lee Dies at 89</a></h3>
            <p class="item-infor" title="Harper Lee, who wrote one of America's most enduring literary classics, "To Kill a Mockingbird," has died at the age of 89.">Harper Lee, who wrote one of America's most enduring literary classics, "To Kill a Mockingbird," has died at the age of 89.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/sports/tennis/Strycova-Garcia-reach-Dubai-semifinals/shdaily.shtml" title="Strycova, Garcia reach Dubai semifinals"><img src="http://img04.abroad.imgcdc.com/english/news/sports/57/20160220/576991_151067_200x120.jpg" width="200" height="120" alt="Strycova, Garcia reach Dubai semifinals" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2016/02/20 16:48:29</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/sports/tennis/Strycova-Garcia-reach-Dubai-semifinals/shdaily.shtml" title="Strycova, Garcia reach Dubai semifinals" class="title_default">Strycova, Garcia reach Dubai semifinals</a></h3>
            <p class="item-infor" title="BARBORA Strycova of Czech Republic advanced to the Dubai Championships semifinals by outlasting former world No.1 Ana Ivanovic of Serbia 7-6 (5), 6-3 on Thursday.">BARBORA Strycova of Czech Republic advanced to the Dubai Championships semifinals by outlasting former world No.1 Ana Ivanovic of Serbia 7-6 (5), 6-3 on Thursday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/business/2016-02/20/content_23567811.htm" title="China reveals consumer price basket changes"><img src="http://img01.abroad.imgcdc.com/english/news/business/56/20160220/576986_151064_200x120.jpg" width="200" height="120" alt="China reveals consumer price basket changes" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2016/02/20 16:40:46</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/business/2016-02/20/content_23567811.htm" title="China reveals consumer price basket changes" class="title_default">China reveals consumer price basket changes</a></h3>
            <p class="item-infor" title="China's top statistics agency revealed changes to its consumer price basket on Friday, lowering the weight given to food while increasing that of other goods to reflect new consumption patterns.">China's top statistics agency revealed changes to its consumer price basket on Friday, lowering the weight given to food while increasing that of other goods to reflect new consumption patterns.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2016-02/20/c_135115845.htm" title="WHO dispels 4 biggest rumors about Zika and microcephaly"><img src="http://img04.abroad.imgcdc.com/english/news/world/55/20160220/576984_151063_200x120.jpg" width="200" height="120" alt="WHO dispels 4 biggest rumors about Zika and microcephaly" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2016/02/20 16:38:56</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2016-02/20/c_135115845.htm" title="WHO dispels 4 biggest rumors about Zika and microcephaly" class="title_default">WHO dispels 4 biggest rumors about Zika and microcephaly</a></h3>
            <p class="item-infor" title="While the Zika virus is spreading explosively in the Americas, where an increasing number of microcephaly cases are being reported, rumors about the virus and microcephaly are also swirling.">While the Zika virus is spreading explosively in the Americas, where an increasing number of microcephaly cases are being reported, rumors about the virus and microcephaly are also swirling.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://en.people.cn/n3/2016/0220/c90785-9018799.html" title="Xi underscores CPC's leadership in news reporting"><img src="http://img03.abroad.imgcdc.com/english/news/china/54/20160220/576983_151062_200x120.jpg" width="200" height="120" alt="Xi underscores CPC's leadership in news reporting" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2016/02/20 16:38:02</em><em class="hide">February 23 2016 02:38:44</em></div>
            <h3 class="item-tit"><a href="http://en.people.cn/n3/2016/0220/c90785-9018799.html" title="Xi underscores CPC's leadership in news reporting" class="title_default">Xi underscores CPC's leadership in news reporting</a></h3>
            <p class="item-infor" title="Chinese President Xi Jinping presides over a symposium after touring China's three leading news providers in Beijing, capital of China, on Feb. 19, 2016. Xi on Friday ordered news media run by the Communist Party of China (CPC) and the Chinese government to strictly follow the Party's leadership.">Chinese President Xi Jinping presides over a symposium after touring China's three leading news providers in Beijing, capital of China, on Feb. 19, 2016. Xi on Friday ordered news media run by the Communist Party of China (CPC) and the Chinese government to strictly follow the Party's leadership.</p>
          </div>
        </div>
      </div>
      <div class="page-latest-more">
        <a href="#" id="page-latest-show-more"><em class="page-latest-more-icon">&nbsp;</em></a>
        <a href="/news/index.html" id="page-latest-click-more"><em class="page-latest-more-icon">Show More</em></a>
      </div>
    </div><!-- page-latest End -->
    <div class="page-left-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=207" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 End -->
	</div> 
    <div class="page-video">
      <h2 class="modTit"><strong><a href="/video/index.html">VIDEO</a></strong></h2>
      <div class="page-video-body">
        <div class="page-video-left">
          <script type="text/javascript" src="http://c.wrating.com/v2_pre.js"></script>
<!--noscript-->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="420" height="234">
            <param name="movie" value="http://english.china.com/videoPlayer/video.swf"/>
            <param name="quality" value="high"/>
            <param name="bgcolor" value="#ffffff"/>
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="true"/>
            <param name="wmode" value="Opaque">
            <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/mychineselife/2015/12/1230joshweb.mp4&loop=1&autoplay=0"/>
            <!--[if!IE]>
            -->
            <object type="application/x-shockwave-flash" data="http://english.china.com/videoPlayer/video.swf" width="420" height="234">
              <param name="quality" value="high"/>
              <param name="bgcolor" value="#ffffff"/>
              <param name="allowScriptAccess" value="sameDomain"/>
              <param name="allowFullScreen" value="true"/>
              <param name="wmode" value="Opaque">
              <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/mychineselife/2015/12/1230joshweb.mp4&loop=1&autoplay=0"/>
              <!--<![endif]-->
              <!--[if gte IE 6]>
              -->
              <p>
                Either scripts and active content are not permitted to run or Adobe Flash Player version 11.4.0 or greater is not installed.
              </p>
              <!--<![endif]-->
              <a href="http://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player"/>
              </a>
              <!--[if!IE]>--></object>
            <!--<![endif]-->
          </object>
          <!--/noscript-->
          <a href="/home/videobig/1299/20151231/537959.html" class="video-tit">Along the Silk Road: Josh Summers--Rediscover Xinjiang</a>
        </div>
        <div class="page-video-right" id="page-video-right">
          <div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20160128/559220.html"><img src="http://img02.abroad.imgcdc.com/english/home/videosmall/1301/20160128/559293_147037.jpg" width="245" height="125" alt="Along the Silk Road: Elise Anderson--An American Xinjiang Idol" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20160128/559220.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road: Elise Anderson--An American Xinjiang Idol</strong></h3>
              <p class="item-infor">Elise Anderson became a local celebrity because of her passion for the Uyghur performing arts. </p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div><div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20151231/537864.html"><img src="http://img03.abroad.imgcdc.com/english/home/videosmall/1301/20151231/537873_141302.jpg" width="245" height="125" alt="Along the Silk Road: Joy Bostwick--The Silk Road Artist" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20151231/537864.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road: Joy Bostwick--The Silk Road Artist</strong></h3>
              <p class="item-infor">American artist documents the beauty of Xinjiang with her paintings.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div><!-- page-video End -->
    <div class="page-mods">
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/travel/index.html">TRAVEL</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners"><img src="http://img03.mini.abroad.imgcdc.com/english/travel/listright/mostpopular/1534/20150506/366181_88898.jpg.330x190.jpg" width="330" height="190" alt="Food Awards 2015: the winners" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners" class="title_default">Food Awards 2015: the winners</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12394/2015/08/11/2743s891245.htm" title="Japanese War Orphan Recounts Past" class="title_default">Japanese War Orphan Recounts Past</a></li><li><a href="http://english.cri.cn/6566/2014/12/25/44s858261.htm" title="Winter Nadam Kicks Off in North China" class="title_default">Winter Nadam Kicks Off in North China</a></li><li><a href="http://english.cri.cn/6566/2014/09/29/44s845986.htm" title="To Experience Authentic Taiwan Folk Art in Beijing" class="title_default">To Experience Authentic Taiwan Folk Art in Beijing</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/chinese/index.html">LEARN CHINESE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love"><img src="http://img03.mini.abroad.imgcdc.com/english/home/learnpic/1315/20141124/211729_51886.jpg.330x190.jpg" width="330" height="190" alt="China's First Love" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love" class="title_default">China's First Love</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12514/2014/10/16/2001s848110.htm" title="Top 10 Popular Chinese TV Dramas Overseas" class="title_default">Top 10 Popular Chinese TV Dramas Overseas</a></li><li><a href="http://english.cri.cn/12514/2014/10/17/2001s848240.htm" title="çµç¶ Chinese Pipa" class="title_default">çµç¶ Chinese Pipa</a></li><li><a href="http://english.cri.cn/12514/2014/09/25/2001s845407.htm" title="Useful Shopping Sentences in Chinese" class="title_default">Useful Shopping Sentences in Chinese</a></li>
          </ul>
        </div>

      </div>
      
      
    </div><!-- page-mods End -->
    <!--<div class="page-left-ad"><a href="#"><img src="/file/left-ad-2.jpg" /></a></div> page-left-ad End -->
    <!-- page-photos End -->
  </div><!-- page-left End -->
  <div class="page-right">
    
    <div class="page-right-ad noMarginTop">
      <!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=208" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 End -->
    </div><!-- page-right-ad End -->
    <!--include virtual="/etc/right_top_ad_index.shtml" -->
      
    ï»¿<!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://english.cri.cn/12394/2015/09/21/Zt2821s896890.htm"><img src="http://img04.abroad.imgcdc.com/english/home/imgtj/5829/20151014/482283_124639.jpg" width="293" height="88" alt="20151014" /></a><a href="http://english.cri.cn/12394/2015/09/02/Zt2821s894283.htm"><img src="http://img01.abroad.imgcdc.com/english/home/imgtj/5829/20150908/482287_124640.jpg" width="293" height="88" alt="20150908" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->

    <!-- page-right-ad End -->
    
    <div class="page-watched">
      <h2 class="modTit"><strong>Most Watched</strong></h2>
      <div class="page-watched-body" id="rank-video">
      </div>
    </div><!-- page-watched End -->
    <div class="page-popular">
      <h2 class="modTit"><strong>Most Popular</strong></h2>
      <ul class="page-popular-body" id="rank-list"></ul>
    </div><!-- page-popular End -->
    <div class="page-right-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=209" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 End -->
	</div> 
    <div class="page-tochina">
      <h2 class="modTit"><strong>Tune in to China</strong></h2>
      <div class="page-tochina-body">
        <div class="item radio-news">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="title">-</h3>
              <h4 id="playtime">-</h4>
              <a href="http://english.cri.cn/7146/2012/12/03/301s736372.htm" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/am846.wsx" class="item-play"></a>
          </div>
        </div>
        <div class="item radio-ez">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="nexttitle">-</h3>
              <h4 id="nexttime">-</h4>
              <a href="http://english.cri.cn/easyfm/ezplaytime.html" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/fm915.wsx" class="item-play"></a>
          </div>
        </div>
      </div>
    </div><!-- page-tochina End -->

    
    <div class="page-hotListening">
      <h2 class="page-hotListening-tit"><strong>Hot Listening</strong></h2>
      <div class="page-hotListening-body">
        <div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/pik.htm"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4933_1370.jpg" width="120" height="90" alt="People in the Know" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/pik.htm" class="title_default">People in the Know</a><a href="http://english.cri.cn/cribb/plus/pik.htm" class="icon-horn"></a></h3>
            <p class="item-infor">PIK gives you insights to the world through interviews.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/today.htm"><img src="http://img02.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4931_1369.jpg" width="120" height="90" alt="Today" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/today.htm" class="title_default">Today</a><a href="http://english.cri.cn/cribb/plus/today.htm" class="icon-horn"></a></h3>
            <p class="item-infor">A news magazine show with in-depth panel discussions.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/easymorning.html"><img src="http://img01.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4928_1368.jpg" width="120" height="90" alt="EZ Morning" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/easymorning.html" class="title_default">EZ Morning</a><a href="http://english.cri.cn/easyfm/easymorning.html" class="icon-horn"></a></h3>
            <p class="item-infor">It lights up your mornings with interesting chit-chats.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/ezwheel.html"><img src="http://img04.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4927_1367.jpg" width="120" height="90" alt="More to Learn" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/ezwheel.html" class="title_default">More to Learn</a><a href="http://english.cri.cn/easyfm/ezwheel.html" class="icon-horn"></a></h3>
            <p class="item-infor">More to Learn is filled up with English stories and anecdotes.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/hour.html"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4923_1366.jpg" width="120" height="90" alt="The Beijing Hour" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/hour.html" class="title_default">The Beijing Hour</a><a href="http://english.cri.cn/easyfm/hour.html" class="icon-horn"></a></h3>
            <p class="item-infor">It opens up the world to you with latest news updates.</p>
          </div>
        </div>
      </div>
    </div><!-- page-hotListening End -->

    <div class="page-mobile">
      <h2 class="modTit"><strong>Mobile</strong></h2>
      <div class="page-mobile-body">
        <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=539404062&mt=8&s=143441" class="iphone" title="Mobile Iphone">Iphone</a>
      </div>
    </div><!-- page-mobile End -->

    <div class="page-cooperation">
      <h2 class="modTit"><strong>Cooperation</strong></h2>
      <div class="page-cooperation-body">
        <a href="http://gc.wrating.com/click.php?a=&c=860099-1000099998&cs=341_285_2559_860010_400000000&ds=354_355_356_357_358&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dmobi.mgeek.TunnyBrowser%26referrer%3Dchannel_id%253Dchinacom%2526utm_source%253Dchinacom"><img src="/file/logo-dolphin-140-90.png" height="90" width="140"></a>
      </div>
    </div><!-- page-cooperation End -->

  </div><!-- page-right End -->
</div><!-- page-main End -->

<script>
window.collectMethod_rank = window.collectMethod_rank || [];
collectMethod_rank.push(function () {
  // setRank("rank-video", 3, "http://english.china.com/js/english_43_day.js", "video", function(){
  //   setRank("rank-list", 5, "http://english.china.com/js/english_22_day.js", "list");
  // });

  setRank2015("rank-video", 3, "day_top", "194", "http://rank.china.com/rank/cri/english/day/rank.js", "video", function(){
    setRank2015("rank-list", 5, "day_top", "104", "http://rank.china.com/rank/cri/english/day/rank.js", "list");
  });
});
</script>

<!-- /etc/channelsitemap.shtml Start -->
<div class="page-map">
  <div class="page-map-body maxWidth">
  <dl class="item">
      <dt><a href="http://english.china.com/news/index.html">News:</a></dt>
      <dd>
        <a href="http://english.china.com/news/china/index.html">China</a>
        <a href="http://english.china.com/news/world/index.html">World</a>
        <a href="http://english.china.com/news/business/index.html">Business</a>
        <a href="http://english.china.com/news/sports/index.html">Sports</a>
        <a href="http://english.china.com/news/showbiz/index.html">Showbiz</a>
		<a href="http://english.china.com/audio/index.html">Audio</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/video/index.html">Video:</a></dt>
      <dd>
        <a href="http://english.china.com/video/c4/index.html">C4</a>
        <a href="http://english.china.com/video/life/index.html">My Chinese Life</a>
        <a href="http://english.china.com/video/thesoundstage/index.html">The Sound Stage</a>
        <a href="http://english.china.com/video/chinarevealed/index.html">China Revealed</a>
        <a href="http://english.china.com/video/showbiz/index.html">Showbiz Video</a>
        <a href="http://english.china.com/video/tour/index.html">Travel Video</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/photos/index.html">Photos:</a></dt>
      <dd>
        <a href="http://english.china.com/photos/china/index.html">China</a>
        <a href="http://english.china.com/photos/world/index.html">World</a>
        <a href="http://english.china.com/photos/fun/index.html">Fun</a>
        <a href="http://english.china.com/photos/travel/index.html">Travel</a>
        <a href="http://english.china.com/photos/entertainment/index.html">Entertainment</a>
        <a href="http://english.china.com/photos/sports/index.html">Sports</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/travel/index.html">Travel:</a></dt>
      <dd>
        <a href="http://english.china.com/travel/beijing/index.html">Beijing</a>
        <a href="http://english.china.com/travel/shanghai/index.html">Shanghai</a>
        <a href="http://english.china.com/travel/guangzhou/index.html">Guangzhou</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/lifestyle/index.html">Lifestyle:</a></dt>
      <dd>        
        <a href="http://english.china.com/lifestyle/livemusic/index.html">Live Music</a>
        <a href="http://english.china.com/lifestyle/opera/index.html">Opera & Classical</a>
        <a href="http://english.china.com/lifestyle/movies/index.html">Movies</a>
        <a href="http://english.china.com/lifestyle/traditionalshows/index.html">Traditional Shows</a>
        <a href="http://english.china.com/lifestyle/exhibitions/index.html">Exhibitions</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/chinese/index.html">Learn Chinese:</a></dt>
      <dd>
        <a href="http://english.china.com/chinese/studio/index.html">Chinese Studio</a>
        <a href="http://english.china.com/chinese/living/index.html">Living Chinese</a>
        <a href="http://english.china.com/chinese/everyday/index.html">Everyday Chinese</a>
        <a href="http://english.china.com/chinese/justforfun/index.html">Just For Fun</a>
        <a href="http://english.china.com/chinese/culture/index.html">Chinese Culture</a>
        <a href="http://english.china.com/chinese/buzzwords/index.html">Buzzwords</a>        
      </dd>
    </dl>      
  </div>
</div><!-- page-map End -->
<!-- /etc/channelsitemap.shtml End -->

<div class="page-link">
  <div class="page-link-body maxWidth">
    <a href="http://english.cri.cn/">CRIENGLISH.com</a>|<a href="http://www.chinadaily.com.cn/">China Daily</a>|<a href="http://www.xinhuanet.com/english/">Xinhua</a>|<a href="http://www.china.org.cn/index.htm">China.org.cn</a>|<a href="http://english.cntv.cn/">CNTV</a>
  </div>
</div><!-- page-link End -->

<!-- /etc/channelcopyright.shtml Start -->
<div class="page-footer">
  <div class="page-foot-link">
    <a href="/about/">About China.com</a>|<a href="/about/gmg.html">About GMG</a>|<a href="/ad/">Ad Services</a>|<a href="/contact/">Contact Information</a>|<a href="/copyright/">Copyright Notice</a>
  </div>
  <p><!--E-mail to:<a href="mailto:english@bj.china.com">english@bj.china.com</a><br />-->Copyright &copy; China.com All Rights Reserved</p>
</div><!-- page-footer End -->

<script src="/js/require.min.js" data-main="/js/main"></script>

<!-- START WRating v1.0 -->
<script type="text/javascript" src="http://c.wrating.com/a1.js">
</script>
<script type="text/javascript">
var vjAcc="860010-0446010000";
var wrUrl="http://c.wrating.com/";
vjTrack("");
</script>
<noscript><img src="http://c.wrating.com/a.gif?a=&c=860010-0446010000" width="1" 
height="1"/></noscript>
<!-- END WRating v1.0 -->

<!-- Start Alexa Certify Javascript #13481-->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S6Upi1awA+00a/", domain:"china.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S6Upi1awA+00a/" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript #13481-->


<!-- Start Google Analytics #16010-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60581848-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics #16010-->

<!-- /etc/channelcopyright.shtml End -->
</body>
</html>