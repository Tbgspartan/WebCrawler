



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "827-4769938-5781087";
                var ue_id = "0EQEXAWBNC23P4ABZKMK";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="0EQEXAWBNC23P4ABZKMK" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-b2f08f0c.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-903233829._CB298432192_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3796876714._CB298540748_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
!function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c?c:a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";a(2)},{2:2}],2:[function(a,b,c){"use strict";!function(){var a,b,c=function(a){return"[object Array]"===Object.prototype.toString.call(a)},d=function(a,b){for(var c=0;c<a.length;c++)c in a&&b.call(null,a[c],c)},e=[],f=!1,g=!1,h=function(){var a=[],b=[],c={};return d(e,function(e){var f="";d(e.dartsite.split("/"),function(b){""!==b&&(b in c||(c[b]=a.length,a.push(b)),f+="/"+c[b])}),b.push(f)}),{iu_parts:a,enc_prev_ius:b}},i=function(){var a=[];return d(e,function(b){var c=[];d(b.sizes,function(a){c.push(a.join("x"))}),a.push(c.join("|"))}),a},j=function(){var a=[];return d(e,function(b){a.push(k(b.targeting))}),a.join("|")},k=function(a,b){var c,d=[];for(var e in a){c=[];for(var f=0;f<a[e].length;f++)c.push(encodeURIComponent(a[e][f]));b?d.push(e+"="+encodeURIComponent(c.join(","))):d.push(e+"="+c.join(","))}return d.join("&")},l=function(){var a=+new Date;g||document.readyState&&"loaded"!==document.readyState||(g=!0,f&&imdbads.cmd.push(function(){for(var b=0;b<e.length;b++)generic.monitoring.record_metric(e[b].name+".fail",csm.duration(a))}))};window.tinygpt={define_slot:function(a,b,c,d){e.push({dartsite:a.replace(/\/$/,""),sizes:b,name:c,targeting:d})},set_targeting:function(b){a=b},callback:function(a){for(var c,d,f={},g=+new Date,h=0;h<e.length;h++)c=e[h].dartsite,d=e[h].name,a[h][c]?f[d]=a[h][c]:window.console&&console.error&&console.error("Unable to correlate GPT response for "+d);imdbads.cmd.push(function(){for(var a=0;a<e.length;a++)ad_utils.slot_events.trigger(e[a].name,"request",{timestamp:b}),ad_utils.slot_events.trigger(e[a].name,"tagdeliver",{timestamp:g});ad_utils.gpt.handle_response(f)})},send:function(){var d,g,m=[],n=function(a,b){c(b)&&(b=b.join(",")),b&&m.push(a+"="+encodeURIComponent(""+b))};if(0===e.length)return void tinygpt.callback({});n("gdfp_req","1"),n("correlator",Math.floor(4503599627370496*Math.random())),n("output","json_html"),n("callback","tinygpt.callback"),n("impl","fifs"),n("json_a","1");var o=h();n("iu_parts",o.iu_parts),n("enc_prev_ius",o.enc_prev_ius),n("prev_iu_szs",i()),n("prev_scp",j()),n("cust_params",k(a,!0)),d=document.createElement("script"),g=document.getElementsByTagName("script")[0],d.async=!0,d.type="text/javascript",d.src="http://pubads.g.doubleclick.net/gampad/ads?"+m.join("&"),d.id="tinygpt",d.onload=d.onerror=d.onreadystatechange=l,f=!0,g.parentNode.insertBefore(d,g),b=+new Date}}}()},{}]},{},[1]);</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['a'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['026331569865'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-3708655860._CB297127365_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"93dbb01b37bb4e5735cd5c9c277f8fd2f944b0d6",
"2016-02-22T18%3A03%3A31GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50189;
generic.days_to_midnight = 0.5808911919593811;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3230827532._CB299577821_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'a']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=026331569865;ord=026331569865?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=026331569865?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=026331569865?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                        <li><a href="/youtube-originals/?ref_=nv_sf_yto_5"
>YouTube Originals</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=02-22&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59532631/?ref_=nv_nw_tn_1"
> 'Justice League' Is Happening, As of April 11
</a><br />
                        <span class="time">1 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59529810/?ref_=nv_nw_tn_2"
> 'Deadpool' Tops $490M Worldwide; 'Risen', 'Witch' & 'Mermaid' Open Strong
</a><br />
                        <span class="time">21 February 2016 4:52 PM, UTC</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59532789/?ref_=nv_nw_tn_3"
> Oscars: 5 Possible Upsets That Could Rock Hollywoodâs Big Night
</a><br />
                        <span class="time">1 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0095765/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjE2OTc3NzA3MF5BMl5BanBnXkFtZTcwMDg2NzIwNw@@._V1._SY315_CR6,0,410,315_.jpg",
            titleYears : "1988",
            rank : 56,
                    headline : "Cinema Paradiso"
    },
    nameAd : {
            clickThru : "/name/nm0000098/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjA4MDU4MjIyMl5BMl5BanBnXkFtZTcwOTM2OTE0Nw@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 110,
            headline : "Jennifer Aniston"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYmbH1ztJnCDKIvWpg-5-pfN0_ns-vn_FnQFi48BNpPcfpSfzKK8ioLPrzn-TSi2LCZ2f_G-83U%0D%0A-eQp3xgl8jNC5l44Lf7ZBgoilHMd45Fc05exNddC9wVHrd2BukHQqVfqYsS3j857teerwMgwykb1%0D%0AY1yAJz1LFtq9-3Zibn8HbsnxPMuzXVfYgT2ji9dlwOSisp4afluzUVG9W0100hyENQ%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYgO-3SCz1OcFJugKvSKC8-oBoWchQ-NYyWQRU8iKVnC-pgDmScs_zGPTJ6OEQGsD1omfg5mpPY%0D%0Ap43pVGXnNwf_Zx15vO9-eLDaG-S8aW21TGBZ8vfpm4RH3pVK3OqFFvTn7tMdB3bD--sTcBiw7BIL%0D%0ApRNISQRkIm_0OH3yW7A6VG7pZjhfz1R-jCtaCs7lOGt8%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYkYqNj5lLs6jI3l2KNu0cyx4SbViOk7HlpFoLzG0vhcPZ4iA8uxqpxpUn3cHfGnyToAeehnVh5%0D%0ASx9ltuT7uQqkmyO0F5tY2YpIZJFsZvOlNo8884giBvh2XhlSyU3bs5sG8dYrtXISZpvuBGgJvANp%0D%0AhcPu-9b7jKbFnPjq5DKBUsovMkVFK4DAL0gjiP97cG_t%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=026331569865;ord=026331569865?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=026331569865;ord=026331569865?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3922375961?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806342&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1" data-video="vi3922375961" data-source="bylist" data-id="ls031574778" data-rid="0EQEXAWBNC23P4ABZKMK" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Cate Blanchett's first IMDb role was a memorable one for the 'Carol' star. Hear from Cate about what she learned playing Mrs. Haines in &quot;Police Rescue: The Loaded Boy&quot; back in 1993." alt="Cate Blanchett's first IMDb role was a memorable one for the 'Carol' star. Hear from Cate about what she learned playing Mrs. Haines in &quot;Police Rescue: The Loaded Boy&quot; back in 1993." src="http://ia.media-imdb.com/images/M/MV5BMTc1MDI0MDg1NV5BMl5BanBnXkFtZTgwMDM3OTAzMTE@._V1_SY298_CR2,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc1MDI0MDg1NV5BMl5BanBnXkFtZTgwMDM3OTAzMTE@._V1_SY298_CR2,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Cate Blanchett's first IMDb role was a memorable one for the 'Carol' star. Hear from Cate about what she learned playing Mrs. Haines in &quot;Police Rescue: The Loaded Boy&quot; back in 1993." title="Cate Blanchett's first IMDb role was a memorable one for the 'Carol' star. Hear from Cate about what she learned playing Mrs. Haines in &quot;Police Rescue: The Loaded Boy&quot; back in 1993." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Cate Blanchett's first IMDb role was a memorable one for the 'Carol' star. Hear from Cate about what she learned playing Mrs. Haines in &quot;Police Rescue: The Loaded Boy&quot; back in 1993." title="Cate Blanchett's first IMDb role was a memorable one for the 'Carol' star. Hear from Cate about what she learned playing Mrs. Haines in &quot;Police Rescue: The Loaded Boy&quot; back in 1993." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/awards-central/video/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806342&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1" > Cate Blanchett </a> </div> </div> <div class="secondary ellipsis"> On Her First IMDb Credit </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4191139097?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806342&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2" data-video="vi4191139097" data-source="bylist" data-id="ls002252034" data-rid="0EQEXAWBNC23P4ABZKMK" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="The adventure of an orphaned boy named Pete and his best friend Elliott, who just so happens to be a dragon." alt="The adventure of an orphaned boy named Pete and his best friend Elliott, who just so happens to be a dragon." src="http://ia.media-imdb.com/images/M/MV5BMjA4NDYxNzYzOF5BMl5BanBnXkFtZTgwNzU1NzcwODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA4NDYxNzYzOF5BMl5BanBnXkFtZTgwNzU1NzcwODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="The adventure of an orphaned boy named Pete and his best friend Elliott, who just so happens to be a dragon." title="The adventure of an orphaned boy named Pete and his best friend Elliott, who just so happens to be a dragon." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The adventure of an orphaned boy named Pete and his best friend Elliott, who just so happens to be a dragon." title="The adventure of an orphaned boy named Pete and his best friend Elliott, who just so happens to be a dragon." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2788732/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806342&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2" > Pete's Dragon </a> </div> </div> <div class="secondary ellipsis"> Teaser Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi768324889?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806342&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3" data-video="vi768324889" data-source="bylist" data-id="ls002309697" data-rid="0EQEXAWBNC23P4ABZKMK" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Charlie is a playboy who's convinced that relationships are dead. His two best friends bet him that if he sticks to one woman for one month, he's bound to get attached. Charlie denies this until he crosses paths with the beautiful and mysterious Eva. They may agree to a casual affair, but eventually Charlie is questioning whether he may actually want more." alt="Charlie is a playboy who's convinced that relationships are dead. His two best friends bet him that if he sticks to one woman for one month, he's bound to get attached. Charlie denies this until he crosses paths with the beautiful and mysterious Eva. They may agree to a casual affair, but eventually Charlie is questioning whether he may actually want more." src="http://ia.media-imdb.com/images/M/MV5BMTY2NDY5NDUzMV5BMl5BanBnXkFtZTgwMTEyNDQ3NzE@._V1_SY298_CR1,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY2NDY5NDUzMV5BMl5BanBnXkFtZTgwMTEyNDQ3NzE@._V1_SY298_CR1,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Charlie is a playboy who's convinced that relationships are dead. His two best friends bet him that if he sticks to one woman for one month, he's bound to get attached. Charlie denies this until he crosses paths with the beautiful and mysterious Eva. They may agree to a casual affair, but eventually Charlie is questioning whether he may actually want more." title="Charlie is a playboy who's convinced that relationships are dead. His two best friends bet him that if he sticks to one woman for one month, he's bound to get attached. Charlie denies this until he crosses paths with the beautiful and mysterious Eva. They may agree to a casual affair, but eventually Charlie is questioning whether he may actually want more." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Charlie is a playboy who's convinced that relationships are dead. His two best friends bet him that if he sticks to one woman for one month, he's bound to get attached. Charlie denies this until he crosses paths with the beautiful and mysterious Eva. They may agree to a casual affair, but eventually Charlie is questioning whether he may actually want more." title="Charlie is a playboy who's convinced that relationships are dead. His two best friends bet him that if he sticks to one woman for one month, he's bound to get attached. Charlie denies this until he crosses paths with the beautiful and mysterious Eva. They may agree to a casual affair, but eventually Charlie is questioning whether he may actually want more." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4871980/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806342&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3" > The Perfect Match </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806342&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','HeroWidget',{wb:1});}
                if(typeof uet === 'function'){uet("cf");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_hd" > <h3>Welcome to Oscars&#174; Week</h3> </a> </span> </span> <p class="blurb">As anticipation builds for the <a href="/awards-central/oscars?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_lk1">2016 Academy Awards</a> this Sunday, we've got awards season covered with galleries, features, original videos, and more.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-shockers/ls033360574?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ3NjAzMzg3Nl5BMl5BanBnXkFtZTYwODA1MTM2._UX402_CR0,130,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3NjAzMzg3Nl5BMl5BanBnXkFtZTYwODA1MTM2._UX402_CR0,130,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/oscars-shockers/ls033360574?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_cap_pri_1" > 11 Biggest Oscar Shockers of All Time </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/facts-about-chris-rock/ls015186022?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjIzNzc5NDc3Ml5BMl5BanBnXkFtZTgwMzQ4ODkzMzE@._UX600_CR65,50,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIzNzc5NDc3Ml5BMl5BanBnXkFtZTgwMzQ4ODkzMzE@._UX600_CR65,50,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/facts-about-chris-rock/ls015186022?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_cap_pri_2" > How Well Do You Know Oscars Host Chris Rock? </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/oscars-cards/?imageid=rm3427984640&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjIzMzM1ODc0N15BMl5BanBnXkFtZTgwMDg0NDc5NzE@._UX402_CR0,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIzMzM1ODc0N15BMl5BanBnXkFtZTgwMDg0NDc5NzE@._UX402_CR0,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/oscars-cards/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_cap_pri_3" > Oscars Nominees Trading Cards </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410132902&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_sm" class="position_bottom supplemental" >Visit Awards Central on IMDb</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
                if(typeof uet === 'function'){uet("af");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/supporting-actor-winner?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413779442&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_bsm_hd" > <h3>25 Years of Best Supporting Actor Oscar Winners</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/supporting-actor-winner?imageid=rm686214144&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413779442&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_bsm_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjE2NTQ4NTY0MF5BMl5BanBnXkFtZTgwMDQ0NzQ5MTE@._UX1900_CR530,190,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE2NTQ4NTY0MF5BMl5BanBnXkFtZTgwMDQ0NzQ5MTE@._UX1900_CR530,190,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/supporting-actor-winner?imageid=rm3419897856&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413779442&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_bsm_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTU3MTg0ODM1MV5BMl5BanBnXkFtZTcwNDgzMTk2Mw@@._UY402_CR190,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU3MTg0ODM1MV5BMl5BanBnXkFtZTcwNDgzMTk2Mw@@._UY402_CR190,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/supporting-actor-winner?imageid=rm1065012224&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413779442&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_bsm_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQxMDgwNTQyMV5BMl5BanBnXkFtZTgwMzI1MTAwMjE@._UX978_CR330,170,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQxMDgwNTQyMV5BMl5BanBnXkFtZTgwMzI1MTAwMjE@._UX978_CR330,170,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">From <a href="/name/nm0799777/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413779442&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_bsm_lk1">J.K. Simmons</a> and <a href="/name/nm0005132/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413779442&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_bsm_lk2">Heath Ledger</a> to <a href="/name/nm0000245/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413779442&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_bsm_lk3">Robin Williams</a>, we take a look back at Best Supporting Actor Oscar Award winners from the last 25 years.</p> <p class="seemore"><a href="/awards-central/supporting-actor-winner?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413779442&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_bsm_sm" class="position_bottom supplemental" >See past Best Supporting Actor Oscar winners</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59532631?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMDA3Yjg1MDgtN2M1Mi00ZDE2LThiMGMtMDI3MDQxMTc4N2VjXkEyXkFqcGdeQXVyMjQzNTUzNQ@@._V1_SY150_CR97,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59532631?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >'Justice League' is happening, as of April 11</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Hitfix</a></span>
    </div>
                                </div>
<p>After years of development, it looks like we will finally get thatÂ <a href="/title/tt0974015?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Justice League</a>Â movie: The first in what should be a series of films is set to begin shooting on April 11, according to Entertainment Weekly. And EW got it straight from the horse's mouth, so to speak: <a href="/name/nm0811583?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">ZackÂ Snyder</a>, who directed ...                                        <span class="nobr"><a href="/news/ni59532631?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59529810?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >'Deadpool' Tops $490M Worldwide; 'Risen', 'Witch' & 'Mermaid' Open Strong</a>
    <div class="infobar">
            <span class="text-muted">21 February 2016 5:52 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59532789?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Oscars: 5 Possible Upsets That Could Rock Hollywoodâs Big Night</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59531162?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >âCarolâ Named Best Picture by Intl. Cinephile Society</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59532662?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >âCaptain America: Civil Warâ: Martin Freemanâs Role Confirmed, New Images Revealed</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Slash Film</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59532723?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTExMzM2NDc4ODheQTJeQWpwZ15BbWU4MDM4MDk3MDgx._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59532723?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >âBatman v Supermanâ Supersized Runtime Revealed</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>Long run times can be Kryptonite to moviegoers, but chances are that wonât be a problem with â<a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Batman v Superman: Dawn of Justice</a>.â According to the AMC TheatersÂ website, the <a href="/company/co0038332?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">DC Comics</a> movie will clock in at two hours and 31 minutes, or 151 minutes. That is slightly longerÂ than â<a href="/title/tt0770828?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Man of Steel</a>,â which...                                        <span class="nobr"><a href="/news/ni59532723?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59529810?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >'Deadpool' Tops $490M Worldwide; 'Risen', 'Witch' & 'Mermaid' Open Strong</a>
    <div class="infobar">
            <span class="text-muted">21 February 2016 5:52 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59531968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Dan Mazerâs Rock Star Comedy âStiffâ in the Works as Movie</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59531162?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >âCarolâ Named Best Picture by Intl. Cinephile Society</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59532929?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Pathe, IMAX sign five-theatre deal</a>
    <div class="infobar">
            <span class="text-muted">40 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0056136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>ScreenDaily</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59532630?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjQxODIwOTIwN15BMl5BanBnXkFtZTgwMjk5Mjg5NjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59532630?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >'Agents of S.H.I.E.L.D.' actress accidentally dropped a Huge spoiler about Grant Ward</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Hitfix</a></span>
    </div>
                                </div>
<p>Warning: Actual Spoiler For <a href="/title/tt2364582?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Agents Of S.H.I.E.L.D.</a> inside!! Itâs a rare day when a major character from comic books or superhero television stays dead. Today is not one of those days. Last week, <a href="/title/tt2364582?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Agents of S.H.I.E.L.D.</a> released their first promo video for the upcoming season. It featured the ...                                        <span class="nobr"><a href="/news/ni59532630?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59531824?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >David Attenborough to Narrate BBC Documentary Series âPlanet Earth IIâ</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59532575?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >âWalking Deadâ Fans Fall in Love With Latest Twist</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59532946?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >âDisneyland 60â Ratings Top Night, James Burrows Tribute Ok, âGood Wifeâ Even</a>
    <div class="infobar">
            <span class="text-muted">28 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59532928?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >âLip Sync Battleâ Kidsâ Spinoff in Development at Spike and Nickelodeon, âLip Sync Battle Jr.â Special to Air this Year</a>
    <div class="infobar">
            <span class="text-muted">30 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59531929?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNzMxNDQ3NTEwN15BMl5BanBnXkFtZTcwNDM5NjM0Mg@@._V1_SY150_CR4,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59531929?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Demi Lovato Criticizes Taylor Swift's Donation to Kesha: "Take Something to Capitol Hill or Actually Speak About Something"</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p>Fresh off of speaking her mind about women's empowerment, <a href="/name/nm1416215?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Demi Lovato</a> is spilling her thoughts once again. <a href="/name/nm2357847?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Taylor Swift</a> donated $250,000 to Kesha in the wake of her ongoing legal battle with Dr. Luke, but Demi doesn't think that is enough to really make a change. "Take something to Capitol Hill or ...                                        <span class="nobr"><a href="/news/ni59531929?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59532953?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Oh, Snap! '90s-Inspired Fashion Is Back</a>
    <div class="infobar">
            <span class="text-muted">31 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59532875?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Lea Michele and Matthew Paetz Have Split</a>
    <div class="infobar">
            <span class="text-muted">48 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59532851?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Gigi Hadid Is "Larger Than Life" Alongside Backstreet Boys' Nick Carter and A.J. McLean for Lip Sync Battle</a>
    <div class="infobar">
            <span class="text-muted">55 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59532853?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Jillian Michaels Surprises Heidi's Mom With Male Strippers for Her 70th Birthday Party: Watch the Hilarious Just Jillian Clip!</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NewsDeskWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3443595776/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413893742&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Bryan Cranston" alt="Bryan Cranston" src="http://ia.media-imdb.com/images/M/MV5BMTg3NzA2OTEyMV5BMl5BanBnXkFtZTgwMjE5MDcwODE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3NzA2OTEyMV5BMl5BanBnXkFtZTgwMjE5MDcwODE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm3443595776/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413893742&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1" > Photos We Love </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2198346240/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413893742&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="Elvis &amp; Nixon (2016)" alt="Elvis &amp; Nixon (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTAxMjM4NTc0MjVeQTJeQWpwZ15BbWU4MDgxMzg4MDgx._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAxMjM4NTc0MjVeQTJeQWpwZ15BbWU4MDgxMzg4MDgx._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2198346240/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413893742&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1442781952/rg784964352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413893742&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Still of Jessica Chastain and Oakes Fegley in Pete's Dragon (2016)" alt="Still of Jessica Chastain and Oakes Fegley in Pete's Dragon (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjMxODM3MjgwMV5BMl5BanBnXkFtZTgwMzgwNzAxODE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMxODM3MjgwMV5BMl5BanBnXkFtZTgwMzgwNzAxODE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1442781952/rg784964352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413893742&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3" > Latest Stills </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=2-22&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1392388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Elodie Yung" alt="Elodie Yung" src="http://ia.media-imdb.com/images/M/MV5BMjMxMDkyNTAzMV5BMl5BanBnXkFtZTcwNzI3NzIzOQ@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMxMDkyNTAzMV5BMl5BanBnXkFtZTcwNzI3NzIzOQ@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1392388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Elodie Yung</a> (35) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000106?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Drew Barrymore" alt="Drew Barrymore" src="http://ia.media-imdb.com/images/M/MV5BMjEyOTMzODIxM15BMl5BanBnXkFtZTcwODM2NTY5Mg@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyOTMzODIxM15BMl5BanBnXkFtZTcwODM2NTY5Mg@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000106?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Drew Barrymore</a> (41) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005048?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Thomas Jane" alt="Thomas Jane" src="http://ia.media-imdb.com/images/M/MV5BOTgwMjMwNzAxNF5BMl5BanBnXkFtZTcwNzIwMDg4Ng@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTgwMjMwNzAxNF5BMl5BanBnXkFtZTcwNzIwMDg4Ng@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005048?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Thomas Jane</a> (47) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1901842?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Dichen Lachman" alt="Dichen Lachman" src="http://ia.media-imdb.com/images/M/MV5BMTk0NTExODI0OF5BMl5BanBnXkFtZTgwODcxMTkwODE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0NTExODI0OF5BMl5BanBnXkFtZTgwODcxMTkwODE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1901842?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Dichen Lachman</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005394?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Jeri Ryan" alt="Jeri Ryan" src="http://ia.media-imdb.com/images/M/MV5BMTQ3NDExNTU2OV5BMl5BanBnXkFtZTcwMDI4MTQ1Mg@@._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3NDExNTU2OV5BMl5BanBnXkFtZTcwMDI4MTQ1Mg@@._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005394?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Jeri Ryan</a> (48) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=2-22&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BornTodayWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-6"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYui85sBnhElUv4SZWy8tgTlNYFNgCwgjmxojG11DquktfyBLFejZJ_vuYdha6NZFCn1TQd15KY%0D%0Ay9JxHLnBxglP8aw7eNV1x4Pp2OJw-3bquZ2PyBC94JCaUc3lR7GauSCMoD7N3XIuDeP9i2fQQOoj%0D%0AumiWWySKsXT6s9jz8Vpa2vFtDPMd_8ETVgG70z38ZmYwU9socUBdUiSQWC6cgeAsYxP_1tkcM6dU%0D%0ADly4ooSiMoYcztbHwJO9yUEuB2UvJwVm%0D%0A&ref_=hm_pop_hd" > <h3>IMDb Asks With Angela Bassett and Freida Pinto</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYkWz4f8Hq3dTgOjzdpoLNTNjU1GZ5hW5rH1t3J4NL3YaoIAGkezkb7lfp2LegTWkjtFKk5Dvz1%0D%0ABmEc3YU-xls0QMoa7z2UnYVWQAJovpSAf0aSbc4gOJDwITNwI_4liCDTiveZdBIAuBojOh-hVGxW%0D%0A5Zs642kmcajoZeotdbRUMz7PSaRbzXP5NbJPtqFcQfUVD6_OSdTyY282Acjb_4gNivC6nQgW66pr%0D%0A-0GY0q2xj_Tq07cB0NGLUzY6DLQqr7xM%0D%0A&ref_=hm_pop_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY0ODEwNTcwOV5BMl5BanBnXkFtZTgwMDYyMjcwODE@._UX614_CR0,170,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0ODEwNTcwOV5BMl5BanBnXkFtZTgwMDYyMjcwODE@._UX614_CR0,170,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYpU1BqyhxklYO9ctyDZeGcWNQhULYcjnU_PfNPV-Q_1PNWK-Gz5gXkffOAo_6Ki6PXpGmzgjUo%0D%0AUVWRr6r_YiqKpllfJ-3NCCER-K3W5b2uaxPZcPcovzqvvwoC9Bp4aCHBV8K-TzEr3Mk0HVmJAggW%0D%0AEnRfimJbhRi3SjeJ5RQeNIp5fAkf_pxIkiCXo8pysDtnvCKqjX1RTtbci1hvLHvlVb-q8aZpi8v3%0D%0Az7OHCrPzx-TRZ2OxB_UKBhXjzceq8VCk%0D%0A&ref_=hm_pop_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTU4NDQ0NDYxN15BMl5BanBnXkFtZTgwMTcyMjcwODE@._UX1650_CR930,280,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4NDQ0NDYxN15BMl5BanBnXkFtZTgwMTcyMjcwODE@._UX1650_CR930,280,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Join host <a href="/name/nm0005278/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk1">Jerry O'Connell</a> for a live conversation with <a href="/name/nm0000291/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk2">Angela Bassett</a>, star of <i><a href="/title/tt3300542/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk3">London Has Fallen</a></i>. We'll also be talking with <a href="/name/nm2951768/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk4">Freida Pinto</a>, star of <i><a href="/title/tt2101383/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk5">Knight of Cups</a></i>. Tune into <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYqJlVh6Ut8UAvKVwBPmsEfO6N1Hf_9TmZfgtDL5aB1sbxVhfcF2MNhhMhveTEnJEZGsNzScKst%0D%0APmddzyhIKRuA3oIhGJkxmHwhTHUwstUoy8Yyw3Axdddo_kOO6s6WJIrb0Ru2BMqlw6-Inz6G3zvT%0D%0A3kcwOzZD4mRtRdxgfJ0XsW-nI8PHhaon9U85jlzcGnqSOmOpSDXW_SHiKqLI_Fe8QsKqA90uFx2m%0D%0Ady8AYdcci4ymNocbD_7jjcfqfkS24rTo%0D%0A&ref_=hm_pop_lk6">Amazon.com/IMDbAsks</a> on Tuesday, Feb. 23, at 7 p.m. ET/4 p.m. PT to watch, live chat, and even ask a question yourself! This livestream is best viewed on laptops, desktops, and tablets.</p> <p class="seemore"><a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411890602&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYn-V63OSU0WqrZAshxwrWRCd83Q2DQztDddOpKCGHEJyIao2VmtYAjQHzrFPd5aD2SwlIpvPza%0D%0AEKW2tNJoZCBgS9ilQKa3mGlKn1J3HXBH_OgMCzAKgWzlWbzusf5xP_LJN8vPjV0X1bbU28Gqlu39%0D%0Ah3M6MkhAtJLP4EDpyDcEvP5xEJ9XKvdlS4j9e0aejAf3Wgy4AvAarx9iirdMj8VA5myPKxItwLvm%0D%0AwBG2ARfQjoazTuodCgRNzPfQ7Fq6KtUb%0D%0A&ref_=hm_pop_sm" class="position_bottom supplemental" >Tune in here for the one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/video/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410080062&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_jl_hd" > <h3>Jennifer Lawrence's 'Embarrassing' First IMDb Credit</h3> </a> </span> </span> <p class="blurb"><a href="/name/nm2225369/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410080062&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_jl_lk1">Jennifer Lawrence</a>, the <a href="/awards-central/oscars/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410080062&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_jl_lk2">Oscar-nominated</a> star of <i><a href="/title/tt2446980/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410080062&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_jl_lk3">Joy</a></i>, shares the funny story behind her first on-screen role.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1062122009?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410080062&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_jl_i_1" data-video="vi1062122009" data-source="bylist" data-id="ls031574778" data-rid="0EQEXAWBNC23P4ABZKMK" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ac_acd_jl" data-ref="hm_ac_acd_jl_i_1"> <img itemprop="image" class="pri_image" title="Joy (2015)" alt="Joy (2015)" src="http://ia.media-imdb.com/images/M/MV5BNzYwNzg1MTk4NF5BMl5BcG5nXkFtZTgwMTc2NjI2NzE@._UX1000_CR190,44,624,351_SY351_SX624_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzYwNzg1MTk4NF5BMl5BcG5nXkFtZTgwMTc2NjI2NzE@._UX1000_CR190,44,624,351_SY351_SX624_AL_UY702_UX1248_AL_.jpg" /> <img alt="Joy (2015)" title="Joy (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Joy (2015)" title="Joy (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/video/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2410080062&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_jl_sm" class="position_bottom supplemental" >Watch Jennifer Lawrence discuss her first IMDb credit</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Indie Focus: Ride 'The Wave'</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3616916/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413780082&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1" > <img itemprop="image" class="pri_image" title="The Wave (2015)" alt="The Wave (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg5Mjg0MjgxMl5BMl5BanBnXkFtZTgwNjUzNDg0NzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg5Mjg0MjgxMl5BMl5BanBnXkFtZTgwNjUzNDg0NzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3469653273?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413780082&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2" data-video="vi3469653273" data-rid="0EQEXAWBNC23P4ABZKMK" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Clip from the disaster film 'The Wave'" alt="Clip from the disaster film 'The Wave'" src="http://ia.media-imdb.com/images/M/MV5BMTQ3ODYzMDI5MF5BMl5BanBnXkFtZTgwMTE5NDQ3NjE@._V1_SY250_CR9,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3ODYzMDI5MF5BMl5BanBnXkFtZTgwMTE5NDQ3NjE@._V1_SY250_CR9,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Clip from the disaster film 'The Wave'" title="Clip from the disaster film 'The Wave'" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Clip from the disaster film 'The Wave'" title="Clip from the disaster film 'The Wave'" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">In this Norwegian disaster film, a mountain road above a fjord collapses, creating a violent tsunami.</p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','RecsWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt2322441/trivia?item=tr2374496&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2322441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="Cinquante nuances de Grey (2015)" alt="Cinquante nuances de Grey (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg1OTIyMTAxM15BMl5BanBnXkFtZTgwMjA3NDM1NzE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg1OTIyMTAxM15BMl5BanBnXkFtZTgwMjA3NDM1NzE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt2322441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Fifty Shades of Grey</a></strong> <p class="blurb">Despite being a musician before an actress, <a href="/name/nm1357683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Rita Ora</a> was not included on the film's soundtrack despite asking the producers. The soundtrack includes music from Beyonce, Ellie Goulding, The Weeknd, Sia, and original compositions from <a href="/name/nm0000384?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk2">Danny Elfman</a>.</p> <p class="seemore"><a href="/title/tt2322441/trivia?item=tr2374496&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TriviaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;62pgenRHYDM&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/62pgenRHYDM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd" > <h3>Poll: Oscars 2016: Best Performance by an Actor in a Supporting Role</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/62pgenRHYDM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="Christian Bale" alt="Christian Bale" src="http://ia.media-imdb.com/images/M/MV5BMTkxMzk4MjQ4MF5BMl5BanBnXkFtZTcwMzExODQxOA@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxMzk4MjQ4MF5BMl5BanBnXkFtZTcwMzExODQxOA@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/62pgenRHYDM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Tom Hardy" alt="Tom Hardy" src="http://ia.media-imdb.com/images/M/MV5BMTQ3ODEyNjA4Nl5BMl5BanBnXkFtZTgwMTE4ODMyMjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3ODEyNjA4Nl5BMl5BanBnXkFtZTgwMTE4ODMyMjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/62pgenRHYDM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="Mark Ruffalo" alt="Mark Ruffalo" src="http://ia.media-imdb.com/images/M/MV5BMTI5MjMwNjAzNF5BMl5BanBnXkFtZTcwMzkyNDg1Mw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5MjMwNjAzNF5BMl5BanBnXkFtZTcwMzkyNDg1Mw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/62pgenRHYDM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4" > <img itemprop="image" class="pri_image" title="Mark Rylance" alt="Mark Rylance" src="http://ia.media-imdb.com/images/M/MV5BMTA1MDY3MTc2OTReQTJeQWpwZ15BbWU3MDc2MTU1NzE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA1MDY3MTc2OTReQTJeQWpwZ15BbWU3MDc2MTU1NzE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/62pgenRHYDM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5" > <img itemprop="image" class="pri_image" title="Sylvester Stallone" alt="Sylvester Stallone" src="http://ia.media-imdb.com/images/M/MV5BMTQwMTk3NDU2OV5BMl5BanBnXkFtZTcwNTA3MTI0Mw@@._V1_SY207_CR4,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQwMTk3NDU2OV5BMl5BanBnXkFtZTcwNTA3MTI0Mw@@._V1_SY207_CR4,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Which of the 2016 Academy Awards nominees for Best Performance by an Actor in a Supporting Role do you think should win? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/252686466?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/62pgenRHYDM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2413806322&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','PollWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2407462162&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_hd" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2407462162&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTcyMzUxODM5N15BMl5BanBnXkFtZTgwMTA0Mjc5NzE@._UX550_CR90,50,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcyMzUxODM5N15BMl5BanBnXkFtZTgwMTA0Mjc5NzE@._UX550_CR90,50,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2407462162&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk0MTk2MzUwMF5BMl5BanBnXkFtZTgwNzY5MTk4NzE@._SX450_CR30,40,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0MTk2MzUwMF5BMl5BanBnXkFtZTgwNzY5MTk4NzE@._SX450_CR30,40,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2407462162&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjA5NDA0ODYwMV5BMl5BanBnXkFtZTgwMTc0ODk3NzE@._SX600_CR120,10,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5NDA0ODYwMV5BMl5BanBnXkFtZTgwMTc0ODk3NzE@._SX600_CR120,10,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2407462162&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_4" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTc0OTIzMjI0OV5BMl5BanBnXkFtZTgwNjk5MTk4NzE@._UX500_CR50,40,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc0OTIzMjI0OV5BMl5BanBnXkFtZTgwNjk5MTk4NzE@._UX500_CR50,40,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2407462162&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_sm" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=026331569865;ord=026331569865?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=026331569865?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=026331569865?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854202&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2404233"></div> <div class="title"> <a href="/title/tt2404233?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854202&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Gods of Egypt </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1083452"></div> <div class="title"> <a href="/title/tt1083452?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854202&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Eddie the Eagle </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1712261"></div> <div class="title"> <a href="/title/tt1712261?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854202&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Triple 9 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2652118"></div> <div class="title"> <a href="/title/tt2652118?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854202&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Crouching Tiger, Hidden Dragon: Sword of Destiny </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt5313980"></div> <div class="title"> <a href="/title/tt5313980?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854202&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Tere Bin Laden Dead or Alive </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3219604"></div> <div class="title"> <a href="/title/tt3219604?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854202&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> The Last Man on the Moon </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854202&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2411854742&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1431045"></div> <div class="title"> <a href="/title/tt1431045?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Deadpool </a> <span class="secondary-text">$55.0M</span> </div> <div class="action"> <a href="/showtimes/title/tt1431045?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2267968"></div> <div class="title"> <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Kung Fu Panda 3 </a> <span class="secondary-text">$12.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3231054"></div> <div class="title"> <a href="/title/tt3231054?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Risen </a> <span class="secondary-text">$11.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4263482"></div> <div class="title"> <a href="/title/tt4263482?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> The Witch </a> <span class="secondary-text">$8.7M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1292566"></div> <div class="title"> <a href="/title/tt1292566?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> How to Be Single </a> <span class="secondary-text">$8.2M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BoxOfficeListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2948356"></div> <div class="title"> <a href="/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Zootopia </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3300542"></div> <div class="title"> <a href="/title/tt3300542?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> London Has Fallen </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3553442"></div> <div class="title"> <a href="/title/tt3553442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Whiskey Tango Foxtrot </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3147312"></div> <div class="title"> <a href="/title/tt3147312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Desierto </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3702652"></div> <div class="title"> <a href="/title/tt3702652?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> The Other Side of the Door </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src="http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3626940295._CB298540565_.html?config=%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D"></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TwitterWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','FacebookWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2408639802&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_hd" > <h3>It's Oscars&#174; Season on IMDb</h3> </a> </span> </span> <p class="blurb">Check out IMDb's full coverage of all the major awards events, including the <a href="/awards-central/oscars/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2408639802&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_lk1">2016 Academy Awards</a>.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2408639802&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_i_1" > <img itemprop="image" class="pri_image" title="Leonardo DiCaprio at event of The EE British Academy Film Awards (2016)" alt="Leonardo DiCaprio at event of The EE British Academy Film Awards (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTc0NjA5NjA5OF5BMl5BanBnXkFtZTgwNDUyMjIwODE@._V1_SX700_CR0,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc0NjA5NjA5OF5BMl5BanBnXkFtZTgwNDUyMjIwODE@._V1_SX700_CR0,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2408639802&pf_rd_r=0EQEXAWBNC23P4ABZKMK&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_sm" class="position_bottom supplemental" >Visit our Awards Central section</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYoCGarQemQz7Vug3oDyDucLDjJjdHTcNxFoyDVR6afZougPgUameczJBx3YRdfnB_dGQALYnZs%0D%0A_s4Igu7yIW0j49MO8KBVi4BHDFtIIK2HqeTKfIJXcehpQ6x4n0heKZF1AtW26mRIXzLTEiFzPefE%0D%0A8ICZ_baWD2k-SU-hByVIvPpHROmaUKetDc3XgcnAP7ZPiDcWc7RPHzVLjhP1RWU3D3XJodJVYcoe%0D%0A7jiCLGfJt9I%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYrNoxxmy4VpPxCLATsTHqX1tg0yjTgCYny3JDEeSe1nI7TO6DuPXxLx_zE9szJYAqAfs7zYYsd%0D%0AIMF3376oPFlmDIHGOJ3hbKi_-kACY3JKh169ckHOFcK97aBRzn37M1uieZ1z2_AQa8hyrJ-pQfMg%0D%0AiFirU_fv0wAXPiFf8flNbtRRx3N8Wo1qCeQK4qNP2iPd8q7SJJTMbuFFLHFfupCQmw%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYliL1AT_FwT-co1rUH7DEWMEE5aE9gzIluPS_YNoyjm0vM_wXneH9l4Wp3fKnVhAXao8nbNJz8%0D%0AymWV6xYzi4k_dDXaFt6RW2cSYLJusgAnkyLFAG9xxyqsgzwSuiZ6xMhIVM8p75Mid7nfyOJehIPu%0D%0A0cD0UWtKFbzdg9GalncY_VnlsxsrWtJ9q1gD1p8DTay8Uh4uFoZmYXfpRCxus_C9YRVdTjZGOb0Z%0D%0ALR5sORTO5UIsiWK2t8zzf-KjaH8II-gkJ60KiJZGEg71Rar6Jaf0oC5-wkDJNea7YPROLfnVHyk2%0D%0A7Xldu6hbA-aTDPVT3EQE9YnZ4bjuEhIHwr5fHSxNSCUgzzwYwaOaruJAeUBiEQhPA85wXQHfv4yl%0D%0AQJ8o655WCia6bZbIZ-gw0vpOQ3rR4Jzfplp0cDu1gpnH8skquHc%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYos3sZ7tCoC2ChPH-A6QxOqP02PCWYa_1LV2bJTpKbnBMaNpXqcjcdewG4rq2NimwhbJPmt5mT%0D%0Auw-A-VpnZUatQZTZNSM53yAqIeRXW8Nj_w9p4UAFB6faoboCVcMVLjdWk5NUR8SA-mr5_Te7Rvk4%0D%0Ax40VL4D-irSON2_0MLBjSjKWJ8GVEnPA8AuPPtguiOUk5bthyDj27VuA96gplo1n_A%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYhWhmHxcZRXmt4bJHWnTHBVyAa3EtWnXxjtIagSQPnejcB7Wq0Nx-H54h4Y046FaNjBE0-ZxEx%0D%0A8Ojfmr7utmb7b73mh_Z-orxJZlDHzXtpN2ZVzGGSnnzoDW6gkYt6Yz13lXDL9qoWQv1_76bQFoSx%0D%0AdqgHKQOiHCTnw3FbjE5UoHJ7yYJ_PmYrIJIinEdKMdjm%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYrvbDaFJXODrjP5lw5uZpEqbavLWEZNNQMA6hmvaeTUqUBejT9Flp5WPrKGeIp5wUQSCDBZav_%0D%0AeXfJbXyt_AkJSWnkhxwo-6WmONP73SRcOWiqAYLkMg42T1d9lRnI5encXiTFzPhsriJH9V0ZlIYg%0D%0ArRdW9NPCAiuKNdmkDKqWeTrRZQLoJm6VSXFkZ_nsZAmz%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYgl-8sacfGHAtZEUfML1di2LuhpJIZjCjry4XdjujySztNlOuIxlk8dH7mUhh89NgtEG1u6EXs%0D%0A79O6II3zf3bdMidAQY2NB_hVmRJRC3xPUMOxra4sqbNjscSxJ5-7Aq5ZVwtRE3bQZBBm-xmqOIAn%0D%0A78_u4SFITwFk0UH7eUZfgAZB_-Wf6xKcIHAtv0L6xMMFPP9KdMrg6q_efMEJE1OO3g%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYirBzfvIU3pcMn3M6KseYHNTFvwmuZdTGiyxxmWU_-ddCk3Az0UaAAE2AOWiS31MirNytaw5uh%0D%0AqDSIoN4uV8lNOzn-MmboG81qfjBrc09CrhG23L7u3amoAh-DewjTsf1rLJk-GbiwNXZw-3Wx0FUM%0D%0A4RPUvEQpM2rsErCHIvGbZIuUueLVQLEv5d996eo2LVYs%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYsvZlXPQeX8tXNuUuDkHc_oCPQdyV0_gAdXZqrJvgw90a_x2_X7m3QihTew3DqE5EAa7fTzIGQ%0D%0Anr7LVYODQJNT56nFwiBjoe8KOe0wuJoqRVWCuTHLnEiwr4j2Q9ihqNzvp3pkDW3LqDUKpQP101Qv%0D%0AfMmw-4fhqHHFHwlOs4v3p9tK26gWye93vw9dO6kF23H_-sE9AA598JCzsIVZDdpKjJ1x8oEG4CDF%0D%0AZ3LO_Fxb-v9_ma4euEYhuZuC98ZKmQ3g%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYuuZ4vNeN4HJcGA3e31VoLVLm9L_Gae6k4FziE95n6J9TqusqwtfaD40023z2Tcv0pb6NDc4CR%0D%0AU3vFXPAELE69E53JYz-F4kwh8uSSrQ3drBkdWqt43kMj_Xwlg1zf39gXADpxfZgoZM5vAx0zR8Nc%0D%0AM6F8oeAfjQBFki4Ee7xAUzXdQcPEgu_RnfhPf_E2SJKaECmwDTM72Gjpe2iWoyNgtkwr2Zopa58G%0D%0AgVB-3cW9xO8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYk4dCtsmC7_igYkc__QSVDQBu-rJPzfDPW_e5z1QjkkgWxCWEZ6Wb42SyRURsyAdnvRhiBT0RC%0D%0Ax8FoCm3uBScuCEaLlbd35jTjyC_zP5G1tkz5UMMheILaHCsQBBp_mLreyM5N8brZnlVTXlH6OMCq%0D%0ADiERVBjpUsSxjePZBGVS0JSb0toGUFJ5DiVYdj2rdYQbdEeYBPh_w1Aml_qiQkwUEd2zC1kbLwYQ%0D%0ADtNLRD6Y5x0%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYkfL4ho75nDAXqMdFBKnsxTNEDPXPgr4UiBWubbSpXeUYTQDNht3HxxAjbZfAptKz1lTmaoIzR%0D%0ALFwU6gXRWXRYV5QQ4qH8Y5h8j2V9VZfDtGx9eOkhHy3a6fX8J2yeegGQQ0JA4kTAOx856Nq5C8dH%0D%0AFfhIA2TRp3B6XVb2kDHwXj0mmC_diUs8hsQmb1exxUxCucCPCUZFNVen6rg6pRUlQMyoC0z3rAuP%0D%0AYl-m1eNItRg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYiFXF3ZwvyxeHneMptsoor36tW9mnqzCw-aBvaAUPdHSZsc8JbYgztRKog4AG7W04qgyprfd4Y%0D%0A-NmGg50TgyqvBHFrp3vhg83SDRT1DrPMcT0UIrAwL-51w10e3qw3EEx79bAUznUo3190rbFCsO23%0D%0AHjMkk6rRsVlWhbLESubkxRxco1RGxGZkUhqlLSRTbMdK5gC7ZNk_ijxtZ7DSgt6oC_e4t6kl7oE1%0D%0Ae0j81jJKv2s%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYp63N-93BC2bSgdbaHIyU514MNNPBNLE5crMgFhGHWt52b5blmt4Tfndsabl_ewh2b2JkCi6wa%0D%0ABdWF_aN4RLnRLcwGYn4UFLiEGKuIujv9Z_UmcMpCYH8v1yvMIuM_KXBRju7MxwuQnZe9c9RWloha%0D%0AMHoaqcqHrOBCcd8cwTcrA7Vc9PXqsgHTfiIuEabAyGI4idLmD4k0v0L88bBatS2T773iw5SLhs31%0D%0ANfw1z75gyec%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYu1yN6BXE7aXo4wrnrS93HxEZV3pJFsYQU20Qff6_5n8MM1pRjfg53ueYsyHVALzJm4vGU0bXR%0D%0A-NQW17SkjP8pJ5Y-QBoj0eUZ0eQueGlGCCOBZ8QAS0QVwCNIJzSaMolo45FuxgChMBXiFNBZHpJy%0D%0ArEpFfs9f2p7UjpDB2KRQ_O88-r2DAIU6KP3cGYRMFwjvSu-D7mbkqiCEnIvAITQ3PjHACglA4Ie7%0D%0AuZANwheYlUsTGQUjJS6Xy75uQalmNgTB%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYlzuzTkcNE6Ai9Kfu1mZjLh62b3QDAhAgDlxXPzr57lmtdzoKS4AF5MnCy87_gc58vKj-m9KDv%0D%0AeioOqov28np-XsusiQ2V88M2z_hKCldGjjfHA25mh2DgDG3OHEQRGhTyUW6rLvT8o_2x4jmzduWY%0D%0AdazmAsSkUovLqre74I3O3TM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYlpOY-mGh5Z-R0CaNATYoWGnoPuT4Su35Ep88aWfFN14Mc2lzk31OFOyQEJB4SESCBGP0tfapf%0D%0AAowkRxPuQ_i--fS2M0Lj8PYqze0i18dC9td2O8UIHmajfV7DybEPFvx4nCkG1vq-HDUV1yRQFQu9%0D%0AuLdZv0dMw-KrP4mHtL6k1Og%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-774728307._CB299450936_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-816966180._CB298601395_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=01011d53497d4072b393865e7977ae843a7b6f5b74408cb661b23c7eee954c7a2d8e",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=026331569865"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-1675743762._CB299577842_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=026331569865&ord=026331569865";
    },"unable to request AAN pixel");
</script>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="513"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
