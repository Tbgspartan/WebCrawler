



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "492-6394727-0104437";
                var ue_id = "17PSAFEZKJN2NMPCGWFY";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        

        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="17PSAFEZKJN2NMPCGWFY" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-9757997b.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1965580546._CB290605844_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-1180111305._CB293333875_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-520887519._CB293333839_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['380667480887'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-2214936824._CB289610996_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"2c55d68fd7c0007ffd2d29f8f2b531e474ad7f79",
"2015-10-31T18%3A51%3A48GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 43692;
generic.days_to_midnight = 0.5056944489479065;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=380667480887;ord=380667480887?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=380667480887?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=380667480887?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                            <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/falltv/?ref_=nv_tvv_fall_1"
>Fall TV</a></li>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_2"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_3"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_4"
>Top Rated TV Shows</a></li>
                            <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_5"
>Most Popular TV Shows</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_6"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_7"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_3"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=10-31&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59140590/?ref_=nv_nw_tn_1"
> 'Burnt' and 'Crisis' Hope to Make Top 5 in a Weak October Weekend
</a><br />
                        <span class="time">29 October 2015 2:23 PM, UTC</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59143234/?ref_=nv_nw_tn_2"
> 'Happy Days' Star Al Molinaro Dies at 96
</a><br />
                        <span class="time">17 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59144181/?ref_=nv_nw_tn_3"
> Box Office: âBurnt,â âOur Brand Is Crisisâ Fizzle Over Slow Holiday Weekend
</a><br />
                        <span class="time">3 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0395169/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTM3MTE5NDcxOF5BMl5BanBnXkFtZTYwNTAxNjc3._V1._SY315_CR30,0,410,315_.jpg",
            titleYears : "2004",
            rank : 169,
                    headline : "Hotel Rwanda"
    },
    nameAd : {
            clickThru : "/name/nm0350453/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjAwMDQ0MjQzMF5BMl5BanBnXkFtZTcwNDczNjQ4NA@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 88,
            headline : "Jake Gyllenhaal"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYnPO7-Y6chYedBsCNWmKqT50DTY2phPaUlxU5YsKbvaB_9W_J_zE_HGEl6nZfiwrZVcGyiN5cL%0D%0AkXv7J8sU6X1PhciDL0i0ne46t8z4AtoG_f6bCaS0ebRV3RfneyKZ1rmAzoXCR2ePJ5hqrhM3F5iV%0D%0Ay5wdEIlskMk4oBGgm_xcu1opRAuAEJ4yrg-DP1EZCzlykGVjAANBxFUoM7dwsxH5pw%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYl2K_mZaiFZEFUL82xqIhMTKxkpqQST6sxpN2MBNczajZ5NTFznDfSvBJFgaimnLGK1dORdU8j%0D%0AVJ35SKQymkwcQ83mvvTtM2i6VmYUy_dMqHapiQVJDXrjNFfQzL4z83bRTu4lf0faDKMNO2Um7z-6%0D%0AWHCs6twqosc1mkYY7xiitaUEfNNShyWi4WaFXvPqi83C%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYnrC2CK3isc87rq_Gw0VjoKcyTbJ4s8GouwQ1SXSRpufDNfFuH2z9zKSTSEGVuATmtFLEuFr3H%0D%0AYe8s6w97tbNoaJgb0vDJ4rWMF3ujBj_ZzULfOMLausWrCzhEk4-eMshlh3aJ50TOvCaR38YMIw-s%0D%0AEubBHocLSa5znX9TYtB3gzichx64ClyNPlkFzbAKTnZTQOfIS6NgMEnTtSy_kZ0orQ%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=380667480887;ord=380667480887?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=380667480887;ord=380667480887?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi497725977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi497725977" data-source="bylist" data-id="ls002922459" data-rid="17PSAFEZKJN2NMPCGWFY" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Set in France during the mid-1970s, Vanessa, a former dancer, and her husband Roland, an American writer, travel the country together. They seem to be growing apart, but when they linger in one quiet, seaside town they begin to draw close to some of its more vibrant inhabitants, such as a local bar/cafe-keeper and a hotel owner." alt="Set in France during the mid-1970s, Vanessa, a former dancer, and her husband Roland, an American writer, travel the country together. They seem to be growing apart, but when they linger in one quiet, seaside town they begin to draw close to some of its more vibrant inhabitants, such as a local bar/cafe-keeper and a hotel owner." src="http://ia.media-imdb.com/images/M/MV5BMTUyNDU2MDMxM15BMl5BanBnXkFtZTgwMzAzMTk1NjE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUyNDU2MDMxM15BMl5BanBnXkFtZTgwMzAzMTk1NjE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Set in France during the mid-1970s, Vanessa, a former dancer, and her husband Roland, an American writer, travel the country together. They seem to be growing apart, but when they linger in one quiet, seaside town they begin to draw close to some of its more vibrant inhabitants, such as a local bar/cafe-keeper and a hotel owner." title="Set in France during the mid-1970s, Vanessa, a former dancer, and her husband Roland, an American writer, travel the country together. They seem to be growing apart, but when they linger in one quiet, seaside town they begin to draw close to some of its more vibrant inhabitants, such as a local bar/cafe-keeper and a hotel owner." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Set in France during the mid-1970s, Vanessa, a former dancer, and her husband Roland, an American writer, travel the country together. They seem to be growing apart, but when they linger in one quiet, seaside town they begin to draw close to some of its more vibrant inhabitants, such as a local bar/cafe-keeper and a hotel owner." title="Set in France during the mid-1970s, Vanessa, a former dancer, and her husband Roland, an American writer, travel the country together. They seem to be growing apart, but when they linger in one quiet, seaside town they begin to draw close to some of its more vibrant inhabitants, such as a local bar/cafe-keeper and a hotel owner." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3707106/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > By the Sea </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3014242841?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3014242841" data-source="bylist" data-id="ls002309697" data-rid="17PSAFEZKJN2NMPCGWFY" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="A self-help seminar inspires a sixty-something woman to romantically pursue her younger co-worker." alt="A self-help seminar inspires a sixty-something woman to romantically pursue her younger co-worker." src="http://ia.media-imdb.com/images/M/MV5BMTUzNTIwNjQyMV5BMl5BanBnXkFtZTgwNzg4MDY2NDE@._V1_SY298_CR123,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzNTIwNjQyMV5BMl5BanBnXkFtZTgwNzg4MDY2NDE@._V1_SY298_CR123,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A self-help seminar inspires a sixty-something woman to romantically pursue her younger co-worker." title="A self-help seminar inspires a sixty-something woman to romantically pursue her younger co-worker." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A self-help seminar inspires a sixty-something woman to romantically pursue her younger co-worker." title="A self-help seminar inspires a sixty-something woman to romantically pursue her younger co-worker." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3766394/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Hello, My Name Is Doris </a> </div> </div> <div class="secondary ellipsis"> Theatrical Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi665498137?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi665498137" data-source="bylist" data-id="ls002653141" data-rid="17PSAFEZKJN2NMPCGWFY" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="When a terrorist escapes custody during a routine handover, Will Holloway must team with disgraced MI5 Intelligence Chief Harry Pearce to track him down before an imminent terrorist attack on London." alt="When a terrorist escapes custody during a routine handover, Will Holloway must team with disgraced MI5 Intelligence Chief Harry Pearce to track him down before an imminent terrorist attack on London." src="http://ia.media-imdb.com/images/M/MV5BMjQyNzAxNjQwN15BMl5BanBnXkFtZTgwMTIyNTMwNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQyNzAxNjQwN15BMl5BanBnXkFtZTgwMTIyNTMwNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="When a terrorist escapes custody during a routine handover, Will Holloway must team with disgraced MI5 Intelligence Chief Harry Pearce to track him down before an imminent terrorist attack on London." title="When a terrorist escapes custody during a routine handover, Will Holloway must team with disgraced MI5 Intelligence Chief Harry Pearce to track him down before an imminent terrorist attack on London." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="When a terrorist escapes custody during a routine handover, Will Holloway must team with disgraced MI5 Intelligence Chief Harry Pearce to track him down before an imminent terrorist attack on London." title="When a terrorist escapes custody during a routine handover, Will Holloway must team with disgraced MI5 Intelligence Chief Harry Pearce to track him down before an imminent terrorist attack on London." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3321300/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > MI-5 </a> </div> </div> <div class="secondary ellipsis"> U.S. Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2260738322&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/feature/halloween-horror-creatures-infographic/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261196082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_ig_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261196082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Movie Monsters That Ruled the Big Screen by Decade</h3> </a> </span> </span> <p class="blurb">IMDb looks at the creatures that have appeared most frequently in movies over the years.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/halloween-horror-creatures-infographic/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261196082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_ig_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261196082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjQzMDk4NzQxN15BMl5BcG5nXkFtZTgwMzgzNDYxNzE@._UX624_CR0,776,624,351_SY351_SX624_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQzMDk4NzQxN15BMl5BcG5nXkFtZTgwMzgzNDYxNzE@._UX624_CR0,776,624,351_SY351_SX624_AL_UY702_UX1248_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/feature/halloween-horror-creatures-infographic/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261196082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_ig_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261196082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Check out our ghoulish infographic </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/feature/halloween/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259534622&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259534622&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Scare Up a Spooky Halloween</h3> </a> </span> </span> <p class="blurb">Check out our Halloween Entertainment Guide and gather all the inspiration you need to enjoy a frightfully fun Halloween season.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/halloween/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259534622&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259534622&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="[Rec] (2007)" alt="[Rec] (2007)" src="http://ia.media-imdb.com/images/M/MV5BMjExNzIwNjc5M15BMl5BanBnXkFtZTcwOTI2MTAzNA@@._V1_SY230_CR51,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjExNzIwNjc5M15BMl5BanBnXkFtZTcwOTI2MTAzNA@@._V1_SY230_CR51,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/halloween/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259534622&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259534622&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Dracula A.D. 1972 (1972)" alt="Dracula A.D. 1972 (1972)" src="http://ia.media-imdb.com/images/M/MV5BMTk5MTg4NjAxMV5BMl5BanBnXkFtZTgwOTUzODU5NTE@._V1_SX307_CR0,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk5MTg4NjAxMV5BMl5BanBnXkFtZTgwOTUzODU5NTE@._V1_SX307_CR0,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/feature/halloween/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259534622&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259534622&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Explore the dark reaches of our Halloween Entertainment Guide </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59140590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNjEzNTk2OTEwNF5BMl5BanBnXkFtZTgwNzExMTg0NjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59140590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >'Burnt' and 'Crisis' Hope to Make Top 5 in a Weak October Weekend</a>
    <div class="infobar">
            <span class="text-muted">29 October 2015 3:23 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p>Last year, the top twelve films tallied a mere $79.9 million over the final weekend in October. It was the sixth worst weekend of 2014. Looking over the list of newcomers hitting theaters this weekend*<a href="/title/tt2503944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Burnt</a>, <a href="/title/tt1018765?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Our Brand is Crisis</a> and <a href="/title/tt1727776?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">Scouts Guide to the Zombie Apocalypse</a>*and the lackluster returns of...                                        <span class="nobr"><a href="/news/ni59140590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59143234?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >'Happy Days' Star Al Molinaro Dies at 96</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144181?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Box Office: âBurnt,â âOur Brand Is Crisisâ Fizzle Over Slow Holiday Weekend</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59143226?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Brad Pittâs âWorld War Zâ Sequel Draws Dennis Kelly for New Draft</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59143325?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Michael B. Jordan, Ryan Coogler to Reteam for Education Scandal Movie âWrong Answerâ</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59143325?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTg4MDE4NjE3M15BMl5BanBnXkFtZTgwNTE4MDc0NjE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59143325?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Michael B. Jordan, Ryan Coogler to Reteam for Education Scandal Movie âWrong Answerâ</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>After working together on â<a href="/title/tt2334649?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Fruitvale Station</a>â and â<a href="/title/tt3076658?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Creed</a>,â <a href="/name/nm0430107?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Michael B. Jordan</a> and director <a href="/name/nm3363032?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk4">Ryan Coogler</a> are planning to reunite for â<a href="/title/tt0480059?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk5">Wrong Answer</a>,â a movie about the standardized-test cheating scandal that rocked Atlantaâs education system in 2013, reports the New York Times. The project is in the ...                                        <span class="nobr"><a href="/news/ni59143325?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59140590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >'Burnt' and 'Crisis' Hope to Make Top 5 in a Weak October Weekend</a>
    <div class="infobar">
            <span class="text-muted">29 October 2015 3:23 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59143226?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Brad Pittâs âWorld War Zâ Sequel Draws Dennis Kelly for New Draft</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59143130?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Bella Thorneâs Thriller âKeep Watchingâ Bought by Screen Gems</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144255?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Why Hulk Likely Won't Get A Solo Movie Anytime Soon</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59144222?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTg3MzYxMjAzN15BMl5BanBnXkFtZTgwMTk3MTczMDE@._V1_SY150_CR62,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59144222?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Top TV Witches From Charmed, Buffy, Sabrina, Tvd, East End and More</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>TVLine.com</a></span>
    </div>
                                </div>
<p>TVLineâs monthlong saluteÂ to small-screen spookies comes to an end with the top TV witches of all time â and we think weâve saved the best for last. FromÂ Charmedâs Halliwell sisters to <a href="/title/tt2288064?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Witches of East End</a>âs Beauchamp clan toÂ American Horror Storyâs fan-favoriteÂ Coven, we left no cauldron unturned ...                                        <span class="nobr"><a href="/news/ni59144222?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59143234?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >'Happy Days' Star Al Molinaro Dies at 96</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144307?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >World Series Ratings Hit 6-Year Game 3 High, âGrimmâ Returns Down, â20/20â² Surges With Leah Remini Interview</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59144221?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >TVLine's Performers of the Week (tie): Carrie Coon and Elizabeth Henstridge</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59143087?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Ultimate DJ: Yahoo Cancels Simon Cowell Reality Series</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000130?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>TVSeriesFinale.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59144323?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjIyMzU1Mjg5NV5BMl5BanBnXkFtZTcwNjQ2Mjc2NA@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59144323?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Will Smith Thanks His Daughter For "Correcting" His Heart and "Teaching" Him How to Love</a>
    <div class="infobar">
            <span class="text-muted">31 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>Popsugar.com</a></span>
    </div>
                                </div>
<p>Oct. 31 may be Halloween, but it also marks a very special day for <a href="/name/nm0000226?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Will Smith</a>'s daughter, <a href="/name/nm2405238?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Willow</a>: her 15th birthday! In honor of Willow's big day, Will took to Facebook to share a flashback photo of the two, writing, "Happy Birthday, my Bean. 15 years old today!! When you were born it took about 2 ...                                        <span class="nobr"><a href="/news/ni59144323?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59142495?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Katie Holmes Speaking Out About Scientology Critic Leah Remini in 20/20 Special</a>
    <div class="infobar">
            <span class="text-muted">30 October 2015 7:33 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59142386?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Jerry Heller Sues Dr. Dre, Cube and 'Compton' Producers for Defamation</a>
    <div class="infobar">
            <span class="text-muted">30 October 2015 6:38 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000358?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>TMZ</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59144287?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Reese Witherspoon Gets Emotional: Tears Up Talking About Her Husband and Three Kids</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144280?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Kylie Jenner -- I Make A Hell Of A Zombie ... But Not Much Of An Actress</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000358?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>TMZ</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/ash-vs-evil-dead-rm2840781312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ash vs Evil Dead (2015-)" alt="Ash vs Evil Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTk0MDA1ODAyNF5BMl5BanBnXkFtZTgwNTYxMzYxNzE@._V1_SY201_CR25,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0MDA1ODAyNF5BMl5BanBnXkFtZTgwNTYxMzYxNzE@._V1_SY201_CR25,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/falltv/galleries/ash-vs-evil-dead-rm2840781312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "Ash vs. Evil Dead" - Season One Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2907955712/rg63806208?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="River Phoenix" alt="River Phoenix" src="http://ia.media-imdb.com/images/M/MV5BMTg0Mjg1NzYzOF5BMl5BanBnXkFtZTgwMDI3MzYxNzE@._V1_SY201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0Mjg1NzYzOF5BMl5BanBnXkFtZTgwMDI3MzYxNzE@._V1_SY201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2907955712/rg63806208?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Remembering River Phoenix </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/scream-queens-rm1649467904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Scream Queens (2015-)" alt="Scream Queens (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTY3MDMyNjUwMF5BMl5BanBnXkFtZTgwMDA3MTYxNzE@._V1_SY201_CR44,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY3MDMyNjUwMF5BMl5BanBnXkFtZTgwMDA3MTYxNzE@._V1_SY201_CR44,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/falltv/galleries/scream-queens-rm1649467904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "Scream Queens" - New Photos </a> </div> </div> </div> </div> </div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=10-31&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005305?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Piper Perabo" alt="Piper Perabo" src="http://ia.media-imdb.com/images/M/MV5BMTU4NjU2MjIyMV5BMl5BanBnXkFtZTYwNDk5MDM1._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4NjU2MjIyMV5BMl5BanBnXkFtZTYwNDk5MDM1._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005305?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Piper Perabo</a> (39) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1404825?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Vanessa Marano" alt="Vanessa Marano" src="http://ia.media-imdb.com/images/M/MV5BMTMyMzgyMDIyNF5BMl5BanBnXkFtZTcwMjA0MjM0Ng@@._V1_SY172_CR18,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMyMzgyMDIyNF5BMl5BanBnXkFtZTcwMjA0MjM0Ng@@._V1_SY172_CR18,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1404825?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Vanessa Marano</a> (23) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001392?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Peter Jackson" alt="Peter Jackson" src="http://ia.media-imdb.com/images/M/MV5BMTY1MzQ3NjA2OV5BMl5BanBnXkFtZTcwNTExOTA5OA@@._V1_SY172_CR4,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY1MzQ3NjA2OV5BMl5BanBnXkFtZTcwNTExOTA5OA@@._V1_SY172_CR4,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001392?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Peter Jackson</a> (54) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000551?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Dermot Mulroney" alt="Dermot Mulroney" src="http://ia.media-imdb.com/images/M/MV5BMzM2ODM3NTc3NF5BMl5BanBnXkFtZTYwOTYwMzM0._V1_SY172_CR12,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzM2ODM3NTc3NF5BMl5BanBnXkFtZTYwOTYwMzM0._V1_SY172_CR12,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000551?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Dermot Mulroney</a> (52) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001006?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="John Candy" alt="John Candy" src="http://ia.media-imdb.com/images/M/MV5BMzQ3MzQzODUxMF5BMl5BanBnXkFtZTcwMjY5MTkyOA@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzQ3MzQzODUxMF5BMl5BanBnXkFtZTcwMjY5MTkyOA@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001006?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">John Candy</a> (1950-1994) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=10-31&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/galleries/the-walking-dead?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258122742&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_wd_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258122742&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>TV Spotlight: Latest Photos From "The Walking Dead"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/the-walking-dead-rm3952271872?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258122742&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_wd_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258122742&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010-)" alt="The Walking Dead (2010-)" src="http://ia.media-imdb.com/images/M/MV5BMjI0NjUwNjI0OV5BMl5BanBnXkFtZTgwOTYyMjMxNzE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0NjUwNjI0OV5BMl5BanBnXkFtZTgwOTYyMjMxNzE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/the-walking-dead-rm2492654080?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258122742&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_wd_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258122742&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010-)" alt="The Walking Dead (2010-)" src="http://ia.media-imdb.com/images/M/MV5BNjU1OTQ5MjcyNV5BMl5BanBnXkFtZTgwNDQyMjMxNzE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjU1OTQ5MjcyNV5BMl5BanBnXkFtZTgwNDQyMjMxNzE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/falltv/galleries/the-walking-dead?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258122742&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_wd_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258122742&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See the full Season 6 gallery </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Indie Focus: 'CodeGirl' Trailer</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt5086438/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261163662&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261163662&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="CodeGirl (2015)" alt="CodeGirl (2015)" src="http://ia.media-imdb.com/images/M/MV5BNzQyMzc5NzU2NF5BMl5BanBnXkFtZTgwODg0MzQxNzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzQyMzc5NzU2NF5BMl5BanBnXkFtZTgwODg0MzQxNzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4221940249?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261163662&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261163662&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi4221940249" data-rid="17PSAFEZKJN2NMPCGWFY" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="CodeGirl (2015)" alt="CodeGirl (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTY3MTYyMDgxMl5BMl5BanBnXkFtZTgwNDQzOTUxNzE@._V1_SY250_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY3MTYyMDgxMl5BMl5BanBnXkFtZTgwNDQzOTUxNzE@._V1_SY250_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="CodeGirl (2015)" title="CodeGirl (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="CodeGirl (2015)" title="CodeGirl (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Watch the trailer for this inspiring documentary about high school girls from around the world designing computer apps in an attempt to win The Technovation Challenge.</p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt1099212/trivia?item=tr0757571&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1099212?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Twilight (2008)" alt="Twilight (2008)" src="http://ia.media-imdb.com/images/M/MV5BMTQ2NzUxMTAxN15BMl5BanBnXkFtZTcwMzEyMTIwMg@@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2NzUxMTAxN15BMl5BanBnXkFtZTcwMzEyMTIwMg@@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt1099212?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Twilight</a></strong> <p class="blurb"><a href="/name/nm2769412?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Stephenie Meyer</a> wrote a parallel novel (unfinished) to "Twilight" called "Midnight Sun." It tells the story of the first book from Edward's point of view. In it, he comments that the arrival of Bella to the school has all of the boys "acting like they're in first grade and she's the shiny new toy." This thought is spoken aloud by Jessica during Bella's first lunch in the cafeteria.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt1099212/trivia?item=tr0757571&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-25"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;m8D0RnGp0lo&quot;}">
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/m8D0RnGp0lo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: BBC's 25 Greatest American Films</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/m8D0RnGp0lo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Citizen Kane (1941)" alt="Citizen Kane (1941)" src="http://ia.media-imdb.com/images/M/MV5BMTQ2Mjc1MDQwMl5BMl5BanBnXkFtZTcwNzUyOTUyMg@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2Mjc1MDQwMl5BMl5BanBnXkFtZTcwNzUyOTUyMg@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/m8D0RnGp0lo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Der Pate (1972)" alt="Der Pate (1972)" src="http://ia.media-imdb.com/images/M/MV5BMjEyMjcyNDI4MF5BMl5BanBnXkFtZTcwMDA5Mzg3OA@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyMjcyNDI4MF5BMl5BanBnXkFtZTcwMDA5Mzg3OA@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/m8D0RnGp0lo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="VÃ©rtigo. De entre los muertos (1958)" alt="VÃ©rtigo. De entre los muertos (1958)" src="http://ia.media-imdb.com/images/M/MV5BNzY0NzQyNzQzOF5BMl5BanBnXkFtZTcwMTgwNTk4OQ@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzY0NzQyNzQzOF5BMl5BanBnXkFtZTcwMTgwNTk4OQ@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/m8D0RnGp0lo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Rumrejsen Ã¥r 2001 (1968)" alt="Rumrejsen Ã¥r 2001 (1968)" src="http://ia.media-imdb.com/images/M/MV5BNDYyMDgxNDQ5Nl5BMl5BanBnXkFtZTcwMjc1ODg3OA@@._V1_SY207_CR7,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDYyMDgxNDQ5Nl5BMl5BanBnXkFtZTcwMjc1ODg3OA@@._V1_SY207_CR7,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/m8D0RnGp0lo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Searchers (1956)" alt="The Searchers (1956)" src="http://ia.media-imdb.com/images/M/MV5BMTc4NzIzMDUyNV5BMl5BanBnXkFtZTgwNjMxNjEwMzE@._V1._CR8,2,317,492_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc4NzIzMDUyNV5BMl5BanBnXkFtZTgwNjMxNjEwMzE@._V1._CR8,2,317,492_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">From the list of the top 25 of the greatest American films of all-time as chosen by the BBC, which one do you think is the greatest? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/249025302?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"> <a href="/poll/m8D0RnGp0lo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2258109242&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=380667480887;ord=380667480887?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=380667480887?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=380667480887?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2503944"></div> <div class="title"> <a href="/title/tt2503944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Burnt </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1018765"></div> <div class="title"> <a href="/title/tt1018765?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Our Brand Is Crisis </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1727776"></div> <div class="title"> <a href="/title/tt1727776?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Scouts Guide to the Zombie Apocalypse </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1817771"></div> <div class="title"> <a href="/title/tt1817771?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Freaks of Nature </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3774694"></div> <div class="title"> <a href="/title/tt3774694?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Love </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3044244"></div> <div class="title"> <a href="/title/tt3044244?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Les merveilles </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3735398"></div> <div class="title"> <a href="/title/tt3735398?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Carter High </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1533089"></div> <div class="title"> <a href="/title/tt1533089?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Tab Hunter Confidential </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259856202&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2259871862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3659388"></div> <div class="title"> <a href="/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Martian </a> <span class="secondary-text">$15.7M</span> </div> <div class="action"> <a href="/showtimes/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1051904"></div> <div class="title"> <a href="/title/tt1051904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Goosebumps </a> <span class="secondary-text">$15.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3682448"></div> <div class="title"> <a href="/title/tt3682448?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Bridge of Spies </a> <span class="secondary-text">$11.4M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1618442"></div> <div class="title"> <a href="/title/tt1618442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> The Last Witch Hunter </a> <span class="secondary-text">$10.8M</span> </div> <div class="action"> <a href="/showtimes/title/tt1618442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2510894"></div> <div class="title"> <a href="/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Hotel Transylvania 2 </a> <span class="secondary-text">$8.9M</span> </div> <div class="action"> <a href="/showtimes/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2379713"></div> <div class="title"> <a href="/title/tt2379713?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Spectre </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2452042"></div> <div class="title"> <a href="/title/tt2452042?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The Peanuts Movie </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1895587"></div> <div class="title"> <a href="/title/tt1895587?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Spotlight </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3203606"></div> <div class="title"> <a href="/title/tt3203606?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Trumbo </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381111"></div> <div class="title"> <a href="/title/tt2381111?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Brooklyn </a> <span class="secondary-text"></span> </div> <div class="action"> Opens 11/4 </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/feature/halloween/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2253913402&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_rhs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2253913402&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Halloween Entertainment Guide</h3> </a> </span> </span> <p class="blurb">We've got all you need to scare up a frightfully fun Halloween season.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/halloween/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2253913402&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_rhs_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2253913402&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Sweeney Todd: Fleet Sokaginin Seytan Berberi (2007)" alt="Sweeney Todd: Fleet Sokaginin Seytan Berberi (2007)" src="http://ia.media-imdb.com/images/M/MV5BMTQzMzg1MjUyM15BMl5BanBnXkFtZTcwNzAxNjQzMw@@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQzMzg1MjUyM15BMl5BanBnXkFtZTcwNzAxNjQzMw@@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/feature/halloween/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2253913402&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hal_rhs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2253913402&pf_rd_r=17PSAFEZKJN2NMPCGWFY&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Get spooked for Halloween </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYnWFMemgEfSR_Oh1ooRfSragjVwRD9CLgEqKS_Y5LHxaB9ayQxt5kfDZOTCNOd-VZIdhrkKMy4%0D%0A3Wx1-WknJf7P4XneQcxP7yUKs4cKPylL3B0nWa0xhBbq7-fwta7yo8qI4Urck-Vsy1WboHRmtzXL%0D%0APrWIVOMbFiajfYlAuB-rTp6gMZHYqO38LhxFqeZpxTxfNI_3LuBOf3sTjt1EMzhP8ayrMO_4_9mE%0D%0A8dVicLxpq5g%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYmwYOfevgkvlE6Spl6tCvA8YGTdkmkLE2nJR9AQzXCzO5cjA_z1AAncz0ujvuwb9NfxWqW8aSm%0D%0AnM333xa69Un2EYawvnTVGbwsj7qwAlScxgECbk6XUV7mgu0RalJFdGr54HutcKE1-t-jiLmdmg_0%0D%0ARDn-YnQuZZHyp3Pwls9tqMs_iEYlN3MXiLK899KwGlcxLpa6xiLd8V6O1G0iLvBBWQ%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYpK8tXNx9rxeVqq6w3408cyGPBsp_RrGHuQz7kdPgc3PFB0zNyXMukQNsoaFxhYgH7ikrdbHwx%0D%0AOHy1Ds9nNCm4dLH2gr31rq3hkpFxuKSvEVdcu9-rDbPc0IiJ2ygkYhVUrW-hN-c3d_C70lu-mJli%0D%0A607W7UltTwHY2yAR49D20xVu_8eF4kXzN6HVScqy2V4F2keZNsd2FSTwBBfb3ClDfZsImf1i8Xnc%0D%0AeBBef0pWqNd4lhKVgCTu75Q-Gg4LYcAlsATwElB06GIiwRhj0Q2rWI8RIF16WZO4yvrJ8z-A5OVe%0D%0A0cOaXzC9EThTmZ-iurx4o2uWy_W9I5gIaHZkA4N-daYBbZSxLRq9Lq0Hn3IngT5qOuhqbzVPG6eT%0D%0AIW-37frQr9efzsUqL5nNVjF961kvWQ%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYmxi0AsKRnjnDIGm3GjqpQYUlywKFqOcIjHWz5bHC-SvVN5WUEhn1WDIoxs7Pnnug87aej-gxu%0D%0AftlHsIcgZ3AgQTY0s7aYSnfhWvJ_50urGzRA3tFJqYHAgMivyzja2a5GT89kCYWDK7s7wySg0lC7%0D%0AN0f2L7SS3ArZW2XtdJvv-42gpG_GZdkM1XMTiWF997yj7zDYypYAcxqLtxvaTXsrOA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYjM8x3MUvlSharaVXNxFf_M2SbvBz_xASkUCWKaxWV6jbvkRwZQ6YJPwWxEVJTc6Rg1x_BLjir%0D%0AKmmR8-M7DhYNzWezhoqbWdoJ9AwHPR5YJ-tuO-dMLrntGuiWGeNvYWRvNQ1z_2gvp6nwsCuZYO-9%0D%0AQP9dGRG_TaDHYaJAf-HyzZ3N1DVCdROtcgThrEYK0Tjj%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYgtI8I2iPiOo4tXfUslwRzfUO_6bJ2wtsc7oJqbqI9I1NZSiympEq73ushZSGJRrU6wd2Oddo5%0D%0A8IKtCFTaGwav4B6cSVU8cTHIbWDlSQ9g9R1O91lguFntnHD0FG_iltXqOgAd9YCADfFQ8c3w2J1k%0D%0Anfj8s4g9vKNAQD8xm9A9WGgtMCG_6vjjbWRw27MIefOPfePaiV_E5T8gLs99IkSI3Q%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYsDDrJIT034lBA0hLrAkD77ek990D8FXSzPnrMxkRuaK8NxHi7cq2bgz9RV7J1w8IwsMvkAWuv%0D%0ApEdCmZKvFFWurdkdO0xZJ8Cm8j6j6rDpKeKaz_bCqvFFN5zypxTEnSeu1miAsrmhudc1s80zItEz%0D%0AfgkJecnoxbX22bQBIX1gO_7D8wTzEi0q_luhIUHblGdHTehQJY9odzofuwtHs18QEmBaAdOoVOua%0D%0Au4KwnCFLjQjLJviPMFIBBkYo2h58N4IOAdY3Fsliop-ikBejXg273Q%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYtHXOBTmz9jrY73S6qXZipqIBIdjHcHyVmKfwtueUWVgzpTsNySPuhQUOnvugdw6KL68O7tAPj%0D%0AJhgmIc8gpwYHdIoE4tGI1MrZjdsUhdXrerwovDEzKrPv4DhK5akTLdyVxwaw0AsZrZ0Vw3TEQL7m%0D%0ALGh_OZQj1FCsTZacY8TgeZ8qKhcbTTVMmy8HPq3QMjB4vDLb7vxtTQ7UEky8VbyoeA%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYjwfDdlQAG_vrDpV9gUesf_htVtErzBAfrnQAESv9TjAx-F_6s075Fax5JQOnD-f7pQTcQQTha%0D%0AsFXSqK3zHolnNruE8lDaUIjXMJ3I-xzpcAGVoy21Qx-3GOGyDvIR6-yL3FSr7Mq-wZPquNe_Sf7_%0D%0AhnUMbnuURKumTumTRTLpB6k-mIQurC94P2i-SIX7i_il%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYsrmr2zwS4mUPtCdTQQrvvtCeXgL03bnzUtqGR5Jf2gfVChne4aPJz76wELa3CoqUPEXyXxhh3%0D%0A49nTEufHHBrwLRwMrg07lPnrAaVzjU27ytvBsS-q2nVkb-E4LZj7evvsFRC_UYVSFf1W9jSqL1Q8%0D%0AR2VaPJ67iQB4pxbEHEotmLfBtCcwYqvi-j-kzPl0AhXV-5lUhQ1U2o8alQ39NLFeV_tmAxki9M0C%0D%0Aw7m2y3htjpBfYJWyuv3XV0446JmaZDfn%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYi9YYGL80Lte_98xDuenzIZmIZ4mmz7NW3MYYhfF0Sa7XDac5VLvkwk05irQonqzgdHKk8cdUM%0D%0ADUENzMEjF0nLyJ_yfsPnobbvcztfFlXRmwo-76AnPvW-sBeO0J4_-wvrai4ET4aAd4PrHqD-11nr%0D%0AK33tjjDEBcorpi_I7hlKKzJP515_Rv1HnAj-o4zt__Hkb-f3PHgDUI9Jc7fAREjDezFrr1ysMMef%0D%0ARu43IPCRROo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYuGzuLOfqQUgs-kyztNKUrmimW4PSQIPFr0IYsPHtH3OOqovPj0Pe5nZoYfatFCzppU4SQomFy%0D%0A3oosnU-c1NF_cvgS3vJ0hdxBZYzsdqHqUqt41EJfQAjO7nmpNqL9aaIlsJ1nXKv8r4p33dzqIEXO%0D%0AgpGDzhv80_s1qhEKMguufzKhFETf1WPU5qAj_xBk6YTyECaw3apuPoOvHb4gqmO5Q-D__6gNP7Ft%0D%0AIFrT-uxNnPc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYq8qDJKCmrDgMrdN0UQdi98_7eHw1-BpL9zmMbf3rUTXK8yUqGV3huvfmgBvgFORXWlOMu9wYV%0D%0A0DA7867_tDP2jSVmGnaa5YTa30vG5IRAIeRV5-veswc-OmrePsyv9LWhmH7q0KODE4ERW6PWczzT%0D%0AjN49E8KeoATUH-hhk2qGjnLzXWrn9njHlpngdaXtHFzJqWwpgl-m7nls8tFKlXwT7C-9Z6OlS2yJ%0D%0AAx48hHfSNh0%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYgeeUqfwRg8wJkQxAlqkNVenFOYrgHni7Cc0LZccbnscDxyt9Mani0CzqiG3JP86JOff1ymN0w%0D%0A6TnEyoQW_FG2wBNmUnFjOzaGVPAlllBGimzJYy_dYiVkH-RAgafVQnRE4KHvLLqCSz02NHFlUb7C%0D%0ADpIJYoTISFq1UzU5QILg5y_dv14zty5eicNtmYs5m5DLXxt92eCWvEK97h0iy0P81dcVGwPHqoUv%0D%0Axk0OK9l6ycQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYqCelJqxwvVABO9ZdtPcWzRNskNVRyCoPdcHzYY5PEw-YT3h_6nJ1w4jRBtjZjPlbyD5wa87Qc%0D%0ArHgyXgP_xEo4jvZSI7dGmWKpO9XJO8yudFejdub-Hn8PPEKYessAQEpurwKYeSoqttvqdR0wOBr6%0D%0AhddEPRr-fpvp_hIhkSd4xAdfkYrmY6YHxYMOwM3Zei8-03a3p5F0XrQhWCRsvMNjYJHyh1aI5MMj%0D%0AKx7PVAJwRgQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYr1qPhE6lsSJR-x0Z5yGnBaeLKEfRx0OuioYOmGxAUivR2GEFwctXSdur9o775oOLFFQpFiB9z%0D%0Ayvxz2HGCWKjv2vWLTXLZGFPzz_OU2ZHaGjsLysqdrcQJiaqYphBF0TXgW_Z6WRxCkFXteAfeyA7N%0D%0Ac82sVazIri0YFW-m4NcmGvehprzboAK3cqFQZKXV3_yO16DX9NtEqtk88ue_SZvQRy5J7awmkYUr%0D%0AM_50H6_coN2GvUHlxXxKR52T-RY1CCuW%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYnZ4mArmU1v-fv3dVwEqIOC3aKLi-G8ascvQqHK9fVCNPwZQHMcdkvNhZXc-3FtvkzrGQnmzTs%0D%0AzMemuxZ5FNYFP53IwZvqfT-P99ExZ9dKTlQnyPqyXuTaxgQtfuX0xWV1Ytfnnidi9fQQ2SlhHnQB%0D%0AqEgYfbAS_e7eUlVG7OmTu04%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYqxFu1e286EQSBCAFf8ZIByZhhuROt2XM02TkVlgA5OlmC3S0N_yWAtgtH40N4feUrLN9FYYtO%0D%0AdkXzuc4zAkFTjhbVNIwjRgHszEhzEp38yQP3WWsl7yNDEX9eeRzox7JtfXrXS1H2rH1OKpfcuudW%0D%0A3kD99Y_Mjo_PEPtkP8AeRNY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-1062213378._CB290996593_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-1776171563._CB291276595_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1238345358._CB290967079_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3920146857._CB292796746_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-1294823147._CB290767939_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=010159e0260cfa6f3ef82b7f6cabc749155fc825aca38b297d8be1d371b5f89b3342",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=380667480887"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=380667480887&ord=380667480887";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="240"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
