(function() {
  var creativeDefinition = {
    customScriptUrl: '',
    isDynamic: false,
    delayedImpression: false,
    standardEventIds: {
      DISPLAY_TIMER: '72',
      INTERACTION_TIMER: '73',
      INTERACTIVE_IMPRESSION: '74',
      MANUAL_CLOSE: '75',
      BACKUP_IMAGE_IMPRESSION: '76',
      EXPAND_TIMER: '77',
      FULL_SCREEN: '78',
      VIDEO_PLAY: '79',
      VIDEO_VIEW_TIMER: '80',
      VIDEO_COMPLETE: '81',
      VIDEO_INTERACTION: '82',
      VIDEO_PAUSE: '83',
      VIDEO_MUTE: '84',
      VIDEO_REPLAY: '85',
      VIDEO_MIDPOINT: '86',
      VIDEO_STOP: '87',
      VIDEO_UNMUTE: '88',
      DYNAMIC_CREATIVE_IMPRESSION: '',
      HTML5_CREATIVE_IMPRESSION: ''
    },
    exitEvents: [
      {
        name: 'clickTAG',
        reportingId: '22851877',
        url: 'http://money.cnn.com/technology/2020-visionaries/',
        targetWindow: '_blank',
        windowProperties: ''
      }
    ],
    timerEvents: [
    ],
    counterEvents: [
    ],
    childFiles: [
      {
        name: 'copy1b.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/copy1b.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'copy1.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/copy1.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: '20-20_logo.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/20-20_logo.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'line_frame1.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/line_frame1.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'img1b.jpg',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/img1b.jpg',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'text2b.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/text2b.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'text3a.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/text3a.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'img1a2.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/img1a2.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: '20.20visionaries_300x250_v5_edge.js',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/20.20visionaries_300x250_v5_edge.js',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'text2a.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/text2a.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'copy1a.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/copy1a.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'partner_logo.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/partner_logo.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'cnn_logo.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/cnn_logo.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'img4a.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/img4a.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'inassociation_txt.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/inassociation_txt.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'text3b.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/text3b.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'img2a.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/img2a.png',
        isVideo: false,
        transcodeInformation: null
      },
      {
        name: 'dot22.png',
        url: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/images/dot22.png',
        isVideo: false,
        transcodeInformation: null
      }
    ],
    videoFiles: [
    ],
    videoEntries: [
    ],
    primaryAssets: [
      {
        id: '41419394',
        artworkType: 'HTML5',
        displayType: 'BANNER',
        width: '300',
        height: '250',
        servingPath: '/ads/richmedia/studio/pv2/41419882/20160215082518683/web/20.20visionaries_300x250_v5.html',
        zIndex: '1000000',
        customCss: '',
        flashArtworkTypeData: null,
        htmlArtworkTypeData: {
          isTransparent: false,
          sdkVersion: '01_102' // Duplicating sdk version in subsequent field as version format not the same.
        },
        floatingDisplayTypeData: null,
        expandingDisplayTypeData: null,
        imageGalleryTypeData: null,
        pageSettings:null,
layoutsConfig: null,
layoutsApi: null
      }
    ]
  }
  var rendererDisplayType = '';
  rendererDisplayType += 'html_';
  var rendererFormat = 'inpage';
  var rendererName = rendererDisplayType + rendererFormat;

  var creativeId = '91342252237';
  var adId = '0';
  var templateVersion = '200_120';
  var studioObjects = window['studioV2'] = window['studioV2'] || {};
  var creativeObjects = studioObjects['creatives'] = studioObjects['creatives'] || {};
  var creativeKey = [creativeId, adId].join('_');
  var creative = creativeObjects[creativeKey] = creativeObjects[creativeKey] || {};
  creative['creativeDefinition'] = creativeDefinition;
  var adResponses = creative['adResponses'] || [];
  for (var i = 0; i < adResponses.length; i++) {
    adResponses[i].creativeDto && adResponses[i].creativeDto.csiEvents &&
        (adResponses[i].creativeDto.csiEvents['pe'] =
            adResponses[i].creativeDto.csiEvents['pe'] || (+new Date));
  }
  var loadedLibraries = studioObjects['loadedLibraries'] = studioObjects['loadedLibraries'] || {};
  var versionedLibrary = loadedLibraries[templateVersion] = loadedLibraries[templateVersion] || {};
  var typedLibrary = versionedLibrary[rendererName] = versionedLibrary[rendererName] || {};
  if (typedLibrary['bootstrap']) {
    typedLibrary.bootstrap();
  }
})();
