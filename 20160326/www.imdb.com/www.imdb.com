



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "322-3833553-3051259";
                var ue_id = "01ZVWAA19M28HAF6S8YN";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="01ZVWAA19M28HAF6S8YN" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-ea4ac15c.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-903233829._CB298432192_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3796876714._CB298540748_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
!function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c?c:a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";a(2)},{2:2}],2:[function(a,b,c){"use strict";!function(){var a,b,c=function(a){return"[object Array]"===Object.prototype.toString.call(a)},d=function(a,b){for(var c=0;c<a.length;c++)c in a&&b.call(null,a[c],c)},e=[],f=!1,g=!1,h=function(){var a=[],b=[],c={};return d(e,function(e){var f="";d(e.dartsite.split("/"),function(b){""!==b&&(b in c||(c[b]=a.length,a.push(b)),f+="/"+c[b])}),b.push(f)}),{iu_parts:a,enc_prev_ius:b}},i=function(){var a=[];return d(e,function(b){var c=[];d(b.sizes,function(a){c.push(a.join("x"))}),a.push(c.join("|"))}),a},j=function(){var a=[];return d(e,function(b){a.push(k(b.targeting))}),a.join("|")},k=function(a,b){var c,d=[];for(var e in a){c=[];for(var f=0;f<a[e].length;f++)c.push(encodeURIComponent(a[e][f]));b?d.push(e+"="+encodeURIComponent(c.join(","))):d.push(e+"="+c.join(","))}return d.join("&")},l=function(){var a=+new Date;g||document.readyState&&"loaded"!==document.readyState||(g=!0,f&&imdbads.cmd.push(function(){for(var b=0;b<e.length;b++)generic.monitoring.record_metric(e[b].name+".fail",csm.duration(a))}))};window.tinygpt={define_slot:function(a,b,c,d){e.push({dartsite:a.replace(/\/$/,""),sizes:b,name:c,targeting:d})},set_targeting:function(b){a=b},callback:function(a){for(var c,d,f={},g=+new Date,h=0;h<e.length;h++)c=e[h].dartsite,d=e[h].name,a[h][c]?f[d]=a[h][c]:window.console&&console.error&&console.error("Unable to correlate GPT response for "+d);imdbads.cmd.push(function(){for(var a=0;a<e.length;a++)ad_utils.slot_events.trigger(e[a].name,"request",{timestamp:b}),ad_utils.slot_events.trigger(e[a].name,"tagdeliver",{timestamp:g});ad_utils.gpt.handle_response(f)})},send:function(){var d,g,m=[],n=function(a,b){c(b)&&(b=b.join(",")),b&&m.push(a+"="+encodeURIComponent(""+b))};if(0===e.length)return void tinygpt.callback({});n("gdfp_req","1"),n("correlator",Math.floor(4503599627370496*Math.random())),n("output","json_html"),n("callback","tinygpt.callback"),n("impl","fifs"),n("json_a","1");var o=h();n("iu_parts",o.iu_parts),n("enc_prev_ius",o.enc_prev_ius),n("prev_iu_szs",i()),n("prev_scp",j()),n("cust_params",k(a,!0)),d=document.createElement("script"),g=document.getElementsByTagName("script")[0],d.async=!0,d.type="text/javascript",d.src="http://pubads.g.doubleclick.net/gampad/ads?"+m.join("&"),d.id="tinygpt",d.onload=d.onerror=d.onreadystatechange=l,f=!0,g.parentNode.insertBefore(d,g),b=+new Date}}}()},{}]},{},[1]);</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['585833729892'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4261578659._CB296130991_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"e454823af1dbb50044dfb934a031bfd8b1231ee4",
"2016-03-26T18%3A03%3A27GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 46593;
generic.days_to_midnight = 0.53927081823349;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3230827532._CB299577821_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=585833729892;ord=585833729892?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=585833729892?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=585833729892?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                        <li><a href="/youtube-originals/?ref_=nv_sf_yto_5"
>YouTube Originals</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=03-26&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_3"
>Sundance</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_6"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_7"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_8"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_9"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_10"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_11"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59663616/?ref_=nv_nw_tn_1"
> âBatman v Supermanâ Hurtling Toward $170 Million Weekend With $82 Million Opening Day
</a><br />
                        <span class="time">3 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59660464/?ref_=nv_nw_tn_2"
> âZootopia,â âStar Warsâ Push Disney to $1 Billion at International Box Office for 2016
</a><br />
                        <span class="time">25 March 2016 3:54 PM, UTC</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59660771/?ref_=nv_nw_tn_3"
> CBS Renews 11 Series Including âBlue Bloods,â âSurvivorâ and âMomâ
</a><br />
                        <span class="time">25 March 2016 5:00 PM, UTC</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt2948356/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjMwNTg2NzkwNl5BMl5BanBnXkFtZTgwMzIxMzIyODE@._UY520_CR210,100,410,315_BR15_CT25_.jpg",
            titleYears : "2016",
            rank : 144,
                    headline : "Zootopia"
    },
    nameAd : {
            clickThru : "/name/nm0185819/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTYxNzUyMTQ4M15BMl5BanBnXkFtZTcwNzE3NjQzMg@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 149,
            headline : "Daniel Craig"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYpJTGMVwbcHBITesvVXPk_hgkZlqWVyX92u9cBRxofFzOqMspyPZwXfF_PLVnCOM4XK_lEDtEj%0D%0ASWyRoU8YJiCamwdw45VI7Vau5VzUdSl8WYqeUyQTM_oUv27L-cuSXyZ_qAJknq4N3wNUhaBIQ9Jx%0D%0ASQRnRWZgjrN1waes_3ZtBS6Usf-ehqnCbH522H8CufvsWrWT0utNYMv5QueW5Mte-g%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYirwVjOjoTsacDJoicrN1ymxnrXfQOrkekWb2NnrA5Ww7ibCdeo5KZ1cfAsz3ekNUTfZP7aE-2%0D%0AOe2FsYDAbO65kmgGk0VaHjmc5qbjo4WPs-p4R_lggfcVLOrCpTSYQ1R28bnvyvtUM4GBkobqjcl6%0D%0AsVcPDfluJt7fThFziD-OZLE4ZG2zc320eMGuCf-oLrz8%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYlWCZCJChhZTQxLnG_iWl9U9S0elwWVnXbWqNraD1-eurX391X27Or2MzvcOcuQ7rwiklUv4X_%0D%0A5GSpy9LmJEqQXzKXJb0uLcOUGL018SePbnblmEVYE3dGU_RhptJ115M24L1hFOvHrCrWE-x4NMgG%0D%0Awp2Dyg-PIvliEQPr_Xug-bB4KkI5HevvfVk0nBimwmCF%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=585833729892;ord=585833729892?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=585833729892;ord=585833729892?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1029879065?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447077742&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1" data-video="vi1029879065" data-source="bylist" data-id="ls053181649" data-rid="01ZVWAA19M28HAF6S8YN" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="The Turtles return to save the city from a dangerous threat." alt="The Turtles return to save the city from a dangerous threat." src="http://ia.media-imdb.com/images/M/MV5BMjAzODQyNDQxOV5BMl5BanBnXkFtZTgwMjYxMzUwODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAzODQyNDQxOV5BMl5BanBnXkFtZTgwMjYxMzUwODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="The Turtles return to save the city from a dangerous threat." title="The Turtles return to save the city from a dangerous threat." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The Turtles return to save the city from a dangerous threat." title="The Turtles return to save the city from a dangerous threat." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3949660/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447077742&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1" > Teenage Mutant Ninja Turtles </a> </div> </div> <div class="secondary ellipsis"> Extended TV Spot </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2256450841?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447077742&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2" data-video="vi2256450841" data-source="bylist" data-id="ls002322762" data-rid="01ZVWAA19M28HAF6S8YN" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Here's the true story of two young men, David Packouz and Efraim Diveroli, who won a $300 million contract from the Pentagon to arm America's allies in Afghanistan." alt="Here's the true story of two young men, David Packouz and Efraim Diveroli, who won a $300 million contract from the Pentagon to arm America's allies in Afghanistan." src="http://ia.media-imdb.com/images/M/MV5BMjA4Nzg0NjUyNV5BMl5BanBnXkFtZTgwNjYxNDM0ODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA4Nzg0NjUyNV5BMl5BanBnXkFtZTgwNjYxNDM0ODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Here's the true story of two young men, David Packouz and Efraim Diveroli, who won a $300 million contract from the Pentagon to arm America's allies in Afghanistan." title="Here's the true story of two young men, David Packouz and Efraim Diveroli, who won a $300 million contract from the Pentagon to arm America's allies in Afghanistan." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Here's the true story of two young men, David Packouz and Efraim Diveroli, who won a $300 million contract from the Pentagon to arm America's allies in Afghanistan." title="Here's the true story of two young men, David Packouz and Efraim Diveroli, who won a $300 million contract from the Pentagon to arm America's allies in Afghanistan." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2005151/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447077742&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2" > War Dogs </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi58635545?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447077742&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3" data-video="vi58635545" data-source="bylist" data-id="ls002252034" data-rid="01ZVWAA19M28HAF6S8YN" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="There are big changes brewing in Gotham, and if he wants to save the city from The Joker's hostile takeover, Batman may have to drop the lone vigilante thing, try to work with others and maybe, just maybe, learn to lighten up." alt="There are big changes brewing in Gotham, and if he wants to save the city from The Joker's hostile takeover, Batman may have to drop the lone vigilante thing, try to work with others and maybe, just maybe, learn to lighten up." src="http://ia.media-imdb.com/images/M/MV5BMTkwMjcwNzE1MV5BMl5BanBnXkFtZTgwNzE2MTE0ODE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkwMjcwNzE1MV5BMl5BanBnXkFtZTgwNzE2MTE0ODE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="There are big changes brewing in Gotham, and if he wants to save the city from The Joker's hostile takeover, Batman may have to drop the lone vigilante thing, try to work with others and maybe, just maybe, learn to lighten up." title="There are big changes brewing in Gotham, and if he wants to save the city from The Joker's hostile takeover, Batman may have to drop the lone vigilante thing, try to work with others and maybe, just maybe, learn to lighten up." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="There are big changes brewing in Gotham, and if he wants to save the city from The Joker's hostile takeover, Batman may have to drop the lone vigilante thing, try to work with others and maybe, just maybe, learn to lighten up." title="There are big changes brewing in Gotham, and if he wants to save the city from The Joker's hostile takeover, Batman may have to drop the lone vigilante thing, try to work with others and maybe, just maybe, learn to lighten up." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4116284/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447077742&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3" > The Lego Batman Movie </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447077742&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','HeroWidget',{wb:1});}
                if(typeof uet === 'function'){uet("cf");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>It's On: 'Batman v Superman'</h3> </span> </span> <p class="blurb">Excited for <i><a href="/title/tt2975590/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446546102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bvs_lk1">Batman v Superman</a></i>? View photos of <a href="/imdbpicks/batman-superman-villains-through-the-years/rg2411895552?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446546102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bvs_lk2">Batman and Superman villains</a> through the years. Plus, <a href="/name/nm0251986/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446546102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bvs_lk3">Jesse Eisenberg</a> chats with IMDb about playing Lex Luthor.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/batman-superman-villains-through-the-years/rg2411895552?imageid=rm1390138368&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446546102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bvs_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNzIzNjIxMjc1MV5BMl5BanBnXkFtZTgwODMyMjIxMTE@._UX630_CR0,190,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzIzNjIxMjc1MV5BMl5BanBnXkFtZTgwODMyMjIxMTE@._UX630_CR0,190,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/batman-superman-villains-through-the-years/rg2411895552?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446546102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bvs_cap_pri_1" > Batman and Superman Villains Through the Years </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1937683737?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446546102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bvs_i_2" data-video="vi1937683737" data-source="bylist" data-id="ls033648938" data-rid="01ZVWAA19M28HAF6S8YN" data-type="playlist" class="video-colorbox" data-refsuffix="hm_bvs" data-ref="hm_bvs_i_2"> <img itemprop="image" class="pri_image" title="Batman v Superman: Dawn of Justice (2016)" alt="Batman v Superman: Dawn of Justice (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTk4NTYxODA1OV5BMl5BanBnXkFtZTgwNzY0MjIwNTE@._V1_SY230_CR10,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk4NTYxODA1OV5BMl5BanBnXkFtZTgwNzY0MjIwNTE@._V1_SY230_CR10,0,307,230_AL_UY460_UX614_AL_.jpg" /> <img alt="Batman v Superman: Dawn of Justice (2016)" title="Batman v Superman: Dawn of Justice (2016)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Batman v Superman: Dawn of Justice (2016)" title="Batman v Superman: Dawn of Justice (2016)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/video/imdb/vi1937683737?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446546102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bvs_cap_pri_2" > Jesse Eisenberg on Playing Lex Luthor and More </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
                if(typeof uet === 'function'){uet("af");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/favorite-movie-easter-eggs/ls032308671?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561542&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_egg_hd" > <h3>Our Favorite Movie Easter Eggs</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/favorite-movie-easter-eggs/ls032308671?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561542&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_egg_i_1#image1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNTAzOTkzNTc3M15BMl5BanBnXkFtZTcwNDA3NDkxNA@@._UX900_CR260,80,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTAzOTkzNTc3M15BMl5BanBnXkFtZTcwNDA3NDkxNA@@._UX900_CR260,80,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/favorite-movie-easter-eggs/ls032308671?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561542&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_egg_i_2#image6" > <img itemprop="image" class="pri_image" title="Still of Kenny Baker in Star Wars: Episode IV - A New Hope (1977)" alt="Still of Kenny Baker in Star Wars: Episode IV - A New Hope (1977)" src="http://ia.media-imdb.com/images/M/MV5BMTI5MTc3MTAzMV5BMl5BanBnXkFtZTcwMzIyMTIyMw@@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5MTc3MTAzMV5BMl5BanBnXkFtZTcwMzIyMTIyMw@@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/favorite-movie-easter-eggs/ls032308671?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561542&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_egg_i_3#image10" > <img itemprop="image" class="pri_image" title="Still of Julia Roberts in Erin Brockovich (2000)" alt="Still of Julia Roberts in Erin Brockovich (2000)" src="http://ia.media-imdb.com/images/M/MV5BMTk2MzMyNzQ5OV5BMl5BanBnXkFtZTcwOTk0OTczNQ@@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk2MzMyNzQ5OV5BMl5BanBnXkFtZTcwOTk0OTczNQ@@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">We've rounded up those blink-and-you'll-miss-them props, references, cameos, and inside jokes that directors drop in for eagle-eyed fans. Here are 23 of our all-time favorites.</p> <p class="seemore"><a href="/imdbpicks/favorite-movie-easter-eggs/ls032308671?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561542&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_egg_sm" class="position_bottom supplemental" >View our list of Easter eggs</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59663616?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNTE5NzU3MTYzOF5BMl5BanBnXkFtZTgwNTM5NjQxODE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59663616?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >âBatman v Supermanâ Hurtling Toward $170 Million Weekend With $82 Million Opening Day</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>â<a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Batman v Superman: Dawn of Justice</a>â is proving its immunity to the kryptonite of bad reviews, as the superhero battle pic took in $82 million on its opening Friday. Warner Bros.â massive DC Comics tentpoleÂ should finish the weekend with more thanÂ $171 million, based on early studio estimates. The ...                                        <span class="nobr"><a href="/news/ni59663616?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59660464?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >âZootopia,â âStar Warsâ Push Disney to $1 Billion at International Box Office for 2016</a>
    <div class="infobar">
            <span class="text-muted">25 March 2016 4:54 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59660771?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >CBS Renews 11 Series Including âBlue Bloods,â âSurvivorâ and âMomâ</a>
    <div class="infobar">
            <span class="text-muted">25 March 2016 6:00 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59662614?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >The Voice: Miley Cyrus, Alicia Keys Confirmed as Season 11 Coaches</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59663165?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >âTeenage Mutant Ninja Turtles 2â Adds Fred Armisen As New Footage Unveiled â WonderCon</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59663616?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNTE5NzU3MTYzOF5BMl5BanBnXkFtZTgwNTM5NjQxODE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59663616?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >âBatman v Supermanâ Hurtling Toward $170 Million Weekend With $82 Million Opening Day</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>â<a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Batman v Superman: Dawn of Justice</a>â is proving its immunity to the kryptonite of bad reviews, as the superhero battle pic took in $82 million on its opening Friday. Warner Bros.â massive DC Comics tentpoleÂ should finish the weekend with more thanÂ $171 million, based on early studio estimates. The ...                                        <span class="nobr"><a href="/news/ni59663616?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59661297?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Ethan Hawke on Remembering Philip Seymour Hoffman and Learning to Sing for Chet Baker Biopic</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59662875?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Robert De Niro Defends âPersonalâ Decision To Screen Anti-Vaccination Docu At Tribeca Film Fest As Protests Mount</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59661980?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Patricia Richardson Withdraws as Ken Howardâs Replacement as SAG-aftra President</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59663165?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >âTeenage Mutant Ninja Turtles 2â Adds Fred Armisen As New Footage Unveiled â WonderCon</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59662614?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTc3MzE1MzcxNl5BMl5BanBnXkFtZTcwNTM1MTA2OA@@._V1_SY150_CR2,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59662614?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >The Voice: Miley Cyrus, Alicia Keys Confirmed as Season 11 Coaches</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>TVLine.com</a></span>
    </div>
                                </div>
<p> <a href="/title/tt1839337?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">The Voice</a> will be getting its first-ever double dose of girl power this fall. Recording artists <a href="/name/nm1415323?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Miley Cyrus</a> and <a href="/name/nm1006024?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Alicia Keys</a> have been announcedÂ as coaches for the NBC competition seriesâ 11th season, joining franchise vets <a href="/name/nm1747013?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">Adam Levine</a> and <a href="/name/nm1627901?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk5">Blake Shelton</a>. Related<a href="/title/tt1839337?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk6">The Voice</a> Recap: They Won Their ...                                        <span class="nobr"><a href="/news/ni59662614?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59662946?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >âPreacherâ Reveals First Look at Eugene Root at WonderCon</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59661785?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Marvel's Most Wanted: Oded Fehr Cast as 'Well-Known' Comic Book Character in Agents of S.H.I.E.L.D. Spinoff Pilot</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59660772?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >âTogethernessâ Cancelled After Two Seasons on HBO</a>
    <div class="infobar">
            <span class="text-muted">25 March 2016 5:55 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59662317?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >âShadowhuntersâ Cast Tease âMalec,â Season Finale Cliffhanger</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59662664?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNjk1MjIxNjUxNF5BMl5BanBnXkFtZTcwODk2NzM4Mg@@._V1_SY150_CR2,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59662664?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Jennifer Aniston Works Out How Many Times A Day? The Actress Dishes On Her Morning Routine</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p>Early to rise makes a woman healthy, wealthy and wise. <a href="/name/nm0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Jennifer Aniston</a> is living proof that eating right, exercising and taking care of your well-being does a body good - and keeps you looking impeccable. "If Iâm working, Iâm up at 4:30, 5 a.m.," the actress, 47, told Well and Good about ...                                        <span class="nobr"><a href="/news/ni59662664?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59662743?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >James Van Der Beek and Wife Kimberly Welcome Baby No. 4</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59662983?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Chris Hemsworth on His Decision to Move His Family Back to Australia: 'It's a Much More Simple Life'</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59660948?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Why Ashton Kutcher Stopped Getting Personal on Social Media</a>
    <div class="infobar">
            <span class="text-muted">25 March 2016 6:30 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59661297?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Ethan Hawke on Remembering Philip Seymour Hoffman and Learning to Sing for Chet Baker Biopic</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NewsDeskWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg3250756352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445538582&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_hd" > <h3>Photos We Love: Week of March 25</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm882187776/rg3250756352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445538582&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Lupita Nyong'o in The Jungle Book (2016)" alt="Lupita Nyong'o in The Jungle Book (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTk2NjUxMjgxNl5BMl5BanBnXkFtZTgwNTMzOTkzODE@._V1_SY201_CR49,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk2NjUxMjgxNl5BMl5BanBnXkFtZTgwNTMzOTkzODE@._V1_SY201_CR49,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3348504064/rg3250756352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445538582&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="Scream: The TV Series (2015)" alt="Scream: The TV Series (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjQ0MDM1MzU2NV5BMl5BanBnXkFtZTgwMjQ5OTkzODE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQ0MDM1MzU2NV5BMl5BanBnXkFtZTgwMjQ5OTkzODE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4031520256/rg3250756352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445538582&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Still of Don Cheadle in Miles Ahead (2015)" alt="Still of Don Cheadle in Miles Ahead (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTY4NzgzNTQzNV5BMl5BanBnXkFtZTgwNzk5MzE0ODE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4NzgzNTQzNV5BMl5BanBnXkFtZTgwNzk5MzE0ODE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg3250756352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445538582&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_sm" class="position_bottom supplemental" >See the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=3-26&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0461136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Keira Knightley" alt="Keira Knightley" src="http://ia.media-imdb.com/images/M/MV5BMTYwNDM0NDA3M15BMl5BanBnXkFtZTcwNTkzMjQ3OA@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwNDM0NDA3M15BMl5BanBnXkFtZTcwNTkzMjQ3OA@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0461136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Keira Knightley</a> (31) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000426?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Jennifer Grey" alt="Jennifer Grey" src="http://ia.media-imdb.com/images/M/MV5BMjE4NTExMTcxN15BMl5BanBnXkFtZTcwMjYwMDc4Ng@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE4NTExMTcxN15BMl5BanBnXkFtZTcwMjYwMDc4Ng@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000426?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Jennifer Grey</a> (56) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005182?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Leslie Mann" alt="Leslie Mann" src="http://ia.media-imdb.com/images/M/MV5BMTU4MTI1MTAwMF5BMl5BanBnXkFtZTcwNjUxMTQ5Mg@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4MTI1MTAwMF5BMl5BanBnXkFtZTcwNjUxMTQ5Mg@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005182?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Leslie Mann</a> (44) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Amy Smart" alt="Amy Smart" src="http://ia.media-imdb.com/images/M/MV5BMTY2Njc4NDA2N15BMl5BanBnXkFtZTcwOTQ1NDUwMw@@._V1_SY172_CR14,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY2Njc4NDA2N15BMl5BanBnXkFtZTcwOTQ1NDUwMw@@._V1_SY172_CR14,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Amy Smart</a> (40) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1165660?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="T.R. Knight" alt="T.R. Knight" src="http://ia.media-imdb.com/images/M/MV5BMjA3ODgyNzE4Nl5BMl5BanBnXkFtZTgwOTQwODYwMTE@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA3ODgyNzE4Nl5BMl5BanBnXkFtZTgwOTQwODYwMTE@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1165660?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">T.R. Knight</a> (43) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=3-26&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BornTodayWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/the-walking-dead-photos?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446532102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_wd_hd" > <h3>TV Spotlight: Season 6 Photos From "The Walking Dead"</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/the-walking-dead-photos?imageid=rm2203131392&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446532102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_wd_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk2MjA5ODE0NV5BMl5BanBnXkFtZTgwNzIwMTI0ODE@._UX1500_CR500,300,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk2MjA5ODE0NV5BMl5BanBnXkFtZTgwNzIwMTI0ODE@._UX1500_CR500,300,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/the-walking-dead-photos?imageid=rm2219908608&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446532102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_wd_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTkyOTM2MTM5Nl5BMl5BanBnXkFtZTgwNjIwMTI0ODE@._UX800_CR0,110,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkyOTM2MTM5Nl5BMl5BanBnXkFtZTgwNjIwMTI0ODE@._UX800_CR0,110,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/imdbpicks/the-walking-dead-photos?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446532102&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_wd_sm" class="position_bottom supplemental" >See the full Season 6 gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>WonderCon 2016 Photos</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/wondercon-2016-celebrities/rg3569523456?imageid=rm1608982272&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447009882&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wc_ph_i_1" > <img itemprop="image" class="pri_image" title="Stephen Amell" alt="Stephen Amell" src="http://ia.media-imdb.com/images/M/MV5BMjM0ODQ5MzYwNl5BMl5BanBnXkFtZTgwODQ0ODQ0ODE@._V1_SY230_CR22,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM0ODQ5MzYwNl5BMl5BanBnXkFtZTgwODQ0ODQ0ODE@._V1_SY230_CR22,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/wondercon-2016-celebrities/rg3569523456?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447009882&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wc_ph_cap_pri_1" > Celebrities </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/wondercon-2016-cosplay/rg3418528512?imageid=rm3336969984&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447009882&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wc_ph_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjMyNzU1NzE4NF5BMl5BanBnXkFtZTgwNDkxODQ0ODE@._V1_SY230_CR5,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMyNzU1NzE4NF5BMl5BanBnXkFtZTgwNDkxODQ0ODE@._V1_SY230_CR5,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/wondercon-2016-cosplay/rg3418528512?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2447009882&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wc_ph_cap_pri_2" > Cosplayers </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Celebrities and cosplayers are spending the weekend at WonderCon 2016 in Los Angeles. Our galleries capture the fun and excitement.</p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-9"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Indie Focus: 'Last Days in the Desert'</h3> </span> </span> <p class="blurb">Watch an imagined chapter from Jesus' 40 days of fasting and praying in the desert. On his way out of the wilderness, Jesus struggles with the Devil over the fate of a family in crisis.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3513054/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446634022&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1" > <img itemprop="image" class="pri_image" title="Last Days in the Desert (2015)" alt="Last Days in the Desert (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ2NDk0MTc1MV5BMl5BanBnXkFtZTgwMTU3MjM0ODE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2NDk0MTc1MV5BMl5BanBnXkFtZTgwMTU3MjM0ODE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3548296473?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446634022&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2" data-video="vi3548296473" data-rid="01ZVWAA19M28HAF6S8YN" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Last Days in the Desert (2015)" alt="Last Days in the Desert (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjI5Nzc5NjUwM15BMl5BanBnXkFtZTgwMTU2MTU3MzE@._UX700_UY484_CR0,66,444,250_SY250_SX444_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI5Nzc5NjUwM15BMl5BanBnXkFtZTgwMTU2MTU3MzE@._UX700_UY484_CR0,66,444,250_SY250_SX444_AL_UY500_UX888_AL_.jpg" /> <img alt="Last Days in the Desert (2015)" title="Last Days in the Desert (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Last Days in the Desert (2015)" title="Last Days in the Desert (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-11"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_hd" > <h3>Now Trending on Amazon Video</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYq1Hpt4B0uMVn3a5nkPwR1ijSrWBZMxxwjZBWiDGBmJOWj-wDrqy1cguwy7UiNP3-6ZqBTJvhR%0D%0A7RZwNGr9ieAC3ginhWkwj5WjF5B0_yuYJS0YwdKhM_wt_TyXFWU89PSY9-QcN-BW9d4lyPY00K4p%0D%0AIiv3Nk1AJ4hed2MX3_h0_sNzddeWxrPq2COg0c85BQZhuYl5i-TNzC0SbDLayl5eBDF2k3vYVVBL%0D%0AnDCKGN1-HlnPGQd8mkcLXC7SxJ2K-_n9Q1b8ciLfJj45mlCAarYOeA%0D%0A&ref_=hm_aiv_i_1" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010)" alt="The Walking Dead (2010)" src="http://ia.media-imdb.com/images/M/MV5BMTk1MjI1NjI0MV5BMl5BanBnXkFtZTcwODQ5MzA3Mw@@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk1MjI1NjI0MV5BMl5BanBnXkFtZTcwODQ5MzA3Mw@@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="The Walking Dead (2010)" title="The Walking Dead (2010)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="The Walking Dead (2010)" title="The Walking Dead (2010)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYla4OgzxsJlxLhTacZav0AWD07HR8ZnoDTzZmM4oIbmxiPXpBS73NFTRKQMUsCGQL4KeMDO4ex%0D%0A45-LqH9BWAvHjhZ88YloGTQK5pZv_8VzKcCeenspMAn0yFDrC2tKX8hPDZgPgdIabl2ktP0Yd_gU%0D%0AFnNqmdkm8V67TW6sXRS15dCFqo9gFzg36AW5htvrdzZdBGAOS8UlLeDQBl9FB_Pisr3QOiXWcUtZ%0D%0Aq0_yfB4CksjKk3QW0TcmvHajZVK2nNQOttdkLuCbEQaQhmCX6N3xvg%0D%0A&ref_=hm_aiv_cap_pri_1" > "The Walking Dead - Season 6" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYm8G6LzuA8VzinQhu7ciA4VEeHtHt0ikfTP6tiJj9Kzb-DU6QeWkK6LgKPcxZuKraFJV7v0vp5%0D%0A98CLw46yKeIHYhUWPckUu28fBLN2NzAaGGzRv_L10o2-f-MYMshV2F8VOxpM3zdWbbY6Gv-xZA3Q%0D%0AJ3ijsSyYe4rWKN4EojCR6AHFSk2mIED2Gpr3CwyMnXNy6qFzfmFSNvtIY_f9wGU8n2n-47wh7Tom%0D%0AHw5tQmJZmDqeVi5GJlO0HjWsNgeMlQcqKXqZrLreYoOPapDCZIZ3cQ%0D%0A&ref_=hm_aiv_i_2" > <img itemprop="image" class="pri_image" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYpxsl6zGLohbwhFYElBFWKP-SvE6ehr2mJzj54rC0aPL3Whx6OL1T_KYcIWcG-p2ec2KFN1mfW%0D%0AuyMtXYxtJeOA9PIyuchdLd48XaZKAt3uZCJUtywDOa_5tGladChOQ4HTNObCuvmrfCknLg_Iqu8S%0D%0AGbFfpb5ptPgKeeieticTGdaaN4p7Wcwmnldn1EOJdYYFcQVcCjDSBV0-xH7UNR6GayjOyZl2dMzV%0D%0AaVPB9qtXtrLrmZBVPLEKZVyFuzaA2NY9BS38gwfw88VekEJHs4VMzg%0D%0A&ref_=hm_aiv_cap_pri_2" > <i>The Hunger Games: Mockingjay Part 2</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYvhBtd29eZNTTjImM1Yzgdj3Yi-KBRyC-MULm4eL-t4iesCyKY_YTXAHc1VKNBPUJbF-ZUm3WV%0D%0AA70EAg02LZ5wAIEuU9qeMYPO8NJH1Wcgu4X-QSj7rTvDrXew_3bD3SCajL_Tsn3X_-_gBekgmJHV%0D%0A2liym34L2U2gzQ1m_NT-zu1JprryfFScWUk_sJB_VXGzk7d_8T-RhxWxki6SntzYvMoOyZF8pGH3%0D%0Aluf-PYA6Fg0WX1hyltuWv1zreHW1zh__%0D%0A&ref_=hm_aiv_i_3" > <img itemprop="image" class="pri_image" title="Elizabeth McGovern, Maggie Smith, Hugh Bonneville, Jim Carter, Raquel Cassidy, Brendan Coyle, Kevin Doyle, Joanne Froggatt, Phyllis Logan, Lesley Nicol, Penelope Wilton, Allen Leech, Rob James-Collier, Michelle Dockery, Ed Speleers, Sophie McShera, Laura Carmichael and Lily James in Downton Abbey (2010)" alt="Elizabeth McGovern, Maggie Smith, Hugh Bonneville, Jim Carter, Raquel Cassidy, Brendan Coyle, Kevin Doyle, Joanne Froggatt, Phyllis Logan, Lesley Nicol, Penelope Wilton, Allen Leech, Rob James-Collier, Michelle Dockery, Ed Speleers, Sophie McShera, Laura Carmichael and Lily James in Downton Abbey (2010)" src="http://ia.media-imdb.com/images/M/MV5BMTg2ODI2NTUwN15BMl5BanBnXkFtZTgwMTMwMzU0MjE@._V1_SY172_CR20,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg2ODI2NTUwN15BMl5BanBnXkFtZTgwMTMwMzU0MjE@._V1_SY172_CR20,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Elizabeth McGovern, Maggie Smith, Hugh Bonneville, Jim Carter, Raquel Cassidy, Brendan Coyle, Kevin Doyle, Joanne Froggatt, Phyllis Logan, Lesley Nicol, Penelope Wilton, Allen Leech, Rob James-Collier, Michelle Dockery, Ed Speleers, Sophie McShera, Laura Carmichael and Lily James in Downton Abbey (2010)" title="Elizabeth McGovern, Maggie Smith, Hugh Bonneville, Jim Carter, Raquel Cassidy, Brendan Coyle, Kevin Doyle, Joanne Froggatt, Phyllis Logan, Lesley Nicol, Penelope Wilton, Allen Leech, Rob James-Collier, Michelle Dockery, Ed Speleers, Sophie McShera, Laura Carmichael and Lily James in Downton Abbey (2010)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Elizabeth McGovern, Maggie Smith, Hugh Bonneville, Jim Carter, Raquel Cassidy, Brendan Coyle, Kevin Doyle, Joanne Froggatt, Phyllis Logan, Lesley Nicol, Penelope Wilton, Allen Leech, Rob James-Collier, Michelle Dockery, Ed Speleers, Sophie McShera, Laura Carmichael and Lily James in Downton Abbey (2010)" title="Elizabeth McGovern, Maggie Smith, Hugh Bonneville, Jim Carter, Raquel Cassidy, Brendan Coyle, Kevin Doyle, Joanne Froggatt, Phyllis Logan, Lesley Nicol, Penelope Wilton, Allen Leech, Rob James-Collier, Michelle Dockery, Ed Speleers, Sophie McShera, Laura Carmichael and Lily James in Downton Abbey (2010)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYsYgHRQ4ZQo6no-EtNFUV1SR4cOj-VgUk9i2oKTl8xfoymYw3yxPOqui9ICil6FBJmG0b1KgOI%0D%0Ao-Yp95Chh-S2E-e9Zm6LQvUhM7P5ky5jmKNH1cgFUk7jEgXw-TuchugYTS_YfqiEQgdIKegXZaZn%0D%0AfNEp7WbJLWlWG_-w3WTBmK6DlZOPW3z3JtNBKhGg9ZFLaed2sSxi9nHXRG4YzaMzO774pmnJuzAN%0D%0AWpyRUCYYPRaoHV-YGi3QfLPrv6Ekkt_jQAfnJVt8LN8Ib1sj8BB4tQ%0D%0A&ref_=hm_aiv_cap_pri_3" > "Downton Abbey - The Final Season" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYhdxsDtSog_ASrA1nHPwU0TU1aZhYQ9qAK_h6Hyl-tOsUOg6I-PicUR6-pjxVHcioejRi8H7pz%0D%0ATkVcB2gdmP7aEKlp9JyKIbdbmgLlbPoTzXkj1g8WT1I7yq_tahu9h4mkdQLQLmyECgZn4hA2akeJ%0D%0AkW6ZH5BvZ2bdJdoucw9QcAETDTkiayfvv6knY7kfUn6z7tg5IxbolmOF0HI53CYh74M3Q-q3UPC5%0D%0AEn3vnP0TdhKRwzr5aEEqxQirPfDlX76UX4KJOBLfa57zj13CyGvb8Q%0D%0A&ref_=hm_aiv_i_4" > <img itemprop="image" class="pri_image" title="Vikings (2013)" alt="Vikings (2013)" src="http://ia.media-imdb.com/images/M/MV5BOTEzNzI3MDc0N15BMl5BanBnXkFtZTgwMzk1MzA5NzE@._V1_SY172_CR8,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTEzNzI3MDc0N15BMl5BanBnXkFtZTgwMzk1MzA5NzE@._V1_SY172_CR8,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Vikings (2013)" title="Vikings (2013)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Vikings (2013)" title="Vikings (2013)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYpZbExM3cH_729wlyeHmGjPT3lteDIwCBdAWcSt6YqUtx_jyeUyzFvLriKo5qLguEz5mTmQSYi%0D%0Agvq1LZ2i8PcoYJciteCJnlEjWqb18u9zwa_AEVIBpeFR4xdWVz-HKlH5LK7PGw7HPezpLaurIrAP%0D%0A19VdJPhLvCUmyhggorIqszcNkOsapOKk2MpQTehTY8HQQ-UcL685hVSd_dLg3Ilnmpb-qIGJjHWK%0D%0AXcB_0N4Lg5MZLgc1DViMbC4kqVp1PyiwOZU-tfHy9DRT-brEZYjoTA%0D%0A&ref_=hm_aiv_cap_pri_4" > "Vikings - Season 4" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYm6GuqydFR1yMoC24TF0nXT2IzDqb-fLyOkaZIwW6ewaWjq2HpPGIFDY5Ggmv_HMCxXjh7nnBJ%0D%0A_qzaWItfV2yt5T9KshlxZ3wU72Fzz3pmuPD9bBZ0NjyxIGFa29sv8H28v3TsatqU5kHYxrA5daZh%0D%0A-sLsvalN4T26g8dq1uPJyjMqzI_w894YNxwxMdC_PWdHOEuOhxmqvrq-xYh4ZgRXHlGkYMJ-tMNU%0D%0AhyqqYpG6kqdlsUfndH-QO1elNP0-5OTf%0D%0A&ref_=hm_aiv_i_5" > <img itemprop="image" class="pri_image" title="Brad Pitt, Christian Bale, Steve Carell and Ryan Gosling in The Big Short (2015)" alt="Brad Pitt, Christian Bale, Steve Carell and Ryan Gosling in The Big Short (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Brad Pitt, Christian Bale, Steve Carell and Ryan Gosling in The Big Short (2015)" title="Brad Pitt, Christian Bale, Steve Carell and Ryan Gosling in The Big Short (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Brad Pitt, Christian Bale, Steve Carell and Ryan Gosling in The Big Short (2015)" title="Brad Pitt, Christian Bale, Steve Carell and Ryan Gosling in The Big Short (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYqk4tnI_x3bLdv7U8wq3vLK4EVyhYCh-iXirgp2FLpuGHdnmgbtnzLprOZue39plDe1_jTQYJ3%0D%0AbCWT5toCahdZG9b-fPblYhDaf5L-NnlT9gnuv9mfmFCfEcBBTyCpPISe_x6oZ_9QkkW7Ybm4NmBm%0D%0AI06wDCJmfs3S6qHEx4-GgDWGH8wOL3vcGFoJiKkshvF3rIKAfUDy00p9dzvRU9C4cu5CSZs7BYwO%0D%0AkA3Tdt3lNLAAfcScfgSdpPdasrQRUpuVUSOaC3wKUxT0zQuSEJ9CZw%0D%0A&ref_=hm_aiv_cap_pri_5" > <i>The Big Short</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Right now you can watch "The Walking Dead - Season 6," <i>The Hunger Games: Mockingjay Part 2</i>, "Vikings - Season 4," and other trending TV shows and movies on Amazon Video, many of which are included with Prime.</p> <p class="seemore"><a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446872662&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_sm" class="position_bottom supplemental" >See which movies and shows are trending on Amazon Video</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','RecsWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt3569230/trivia?item=tr2733906&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3569230?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="ÎÎ¯Î´ÏÎ¼Î¿Î¹ Î¸ÏÏÎ»Î¿Î¹ (2015)" alt="ÎÎ¯Î´ÏÎ¼Î¿Î¹ Î¸ÏÏÎ»Î¿Î¹ (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjE0MjkyODQ3NF5BMl5BanBnXkFtZTgwNDM1OTk1NjE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE0MjkyODQ3NF5BMl5BanBnXkFtZTgwNDM1OTk1NjE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt3569230?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Legend</a></strong> <p class="blurb"><a href="/name/nm0362766?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Tom Hardy</a> and <a href="/name/nm0079273?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk2">Paul Bettany</a> had previously appeared together in <a href="/title/tt0258816?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk3">The Reckoning</a> (2002).</p> <p class="seemore"><a href="/title/tt3569230/trivia?item=tr2733906&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TriviaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;xEPYQU8lJLw&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/xEPYQU8lJLw/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd" > <h3>Poll: In Memory of Akira Kurosawa on His Birthday!</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/xEPYQU8lJLw/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="Los siete samurai (1954)" alt="Los siete samurai (1954)" src="http://ia.media-imdb.com/images/M/MV5BMTc5MDY1MjU5MF5BMl5BanBnXkFtZTgwNDM2OTE4MzE@._V1_SY207_CR4,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc5MDY1MjU5MF5BMl5BanBnXkFtZTgwNDM2OTE4MzE@._V1_SY207_CR4,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/xEPYQU8lJLw/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Rashomon (1950)" alt="Rashomon (1950)" src="http://ia.media-imdb.com/images/M/MV5BMjEzMzA4NDE2OF5BMl5BanBnXkFtZTcwNTc5MDI2NQ@@._V1._CR0,0,503,683_SY207_CR6,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzMzA4NDE2OF5BMl5BanBnXkFtZTcwNTc5MDI2NQ@@._V1._CR0,0,503,683_SY207_CR6,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/xEPYQU8lJLw/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="Ran (1985)" alt="Ran (1985)" src="http://ia.media-imdb.com/images/M/MV5BNTEyNjg0MDM4OF5BMl5BanBnXkFtZTgwODI0NjUxODE@._V1_SY207_CR2,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTEyNjg0MDM4OF5BMl5BanBnXkFtZTgwODI0NjUxODE@._V1_SY207_CR2,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/xEPYQU8lJLw/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4" > <img itemprop="image" class="pri_image" title="Yojimbo (1961)" alt="Yojimbo (1961)" src="http://ia.media-imdb.com/images/M/MV5BMjAwNTQ3ODUyMl5BMl5BanBnXkFtZTgwNDg5ODQyNjE@._V1_SY207_CR1,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAwNTQ3ODUyMl5BMl5BanBnXkFtZTgwNDg5ODQyNjE@._V1_SY207_CR1,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/xEPYQU8lJLw/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5" > <img itemprop="image" class="pri_image" title="Att leva (1952)" alt="Att leva (1952)" src="http://ia.media-imdb.com/images/M/MV5BMTcyMDU0MTQzNV5BMl5BanBnXkFtZTcwOTk2NDQyMQ@@._V1._CR12,28,314,446_SY207_CR3,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcyMDU0MTQzNV5BMl5BanBnXkFtZTcwOTk2NDQyMQ@@._V1._CR12,28,314,446_SY207_CR3,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">"In a mad world, only the mad are sane," said Akira Kurosawa. The legendary Japanese painter, writer and director became one of the most influential filmmakers in the world, and his legacy lives on today with the upcoming <a href="/title/tt2404435?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1"><i>Magnificent Seven</i></a> remake. In honor of his birthday, which of the following Kurosawa films is your favorite?<br /><br />Discuss <a href="http://www.imdb.com/board/bd0000088/nest/254316053?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk2">here</a> after voting.</p> <p class="seemore"><a href="/poll/xEPYQU8lJLw/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2445260082&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','PollWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=585833729892;ord=585833729892?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=585833729892?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=585833729892?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561842&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2975590"></div> <div class="title"> <a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561842&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Batman v Superman: Dawn of Justice </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561842&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3760922"></div> <div class="title"> <a href="/title/tt3760922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561842&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> My Big Fat Greek Wedding 2 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1490785"></div> <div class="title"> <a href="/title/tt1490785?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561842&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> I Saw the Light </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2133196"></div> <div class="title"> <a href="/title/tt2133196?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561842&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Born to Be Blue </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3096858"></div> <div class="title"> <a href="/title/tt3096858?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561842&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> They're Watching </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446561842&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446562822&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2948356"></div> <div class="title"> <a href="/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Zootopia </a> <span class="secondary-text">$37.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3410834"></div> <div class="title"> <a href="/title/tt3410834?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Allegiant </a> <span class="secondary-text">$29.0M</span> </div> <div class="action"> <a href="/showtimes/title/tt3410834?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4257926"></div> <div class="title"> <a href="/title/tt4257926?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Miracles from Heaven </a> <span class="secondary-text">$14.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1179933"></div> <div class="title"> <a href="/title/tt1179933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> 10 Cloverfield Lane </a> <span class="secondary-text">$12.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt1179933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1431045"></div> <div class="title"> <a href="/title/tt1431045?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Deadpool </a> <span class="secondary-text">$8.0M</span> </div> <div class="action"> <a href="/showtimes/title/tt1431045?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BoxOfficeListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2937696"></div> <div class="title"> <a href="/title/tt2937696?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Everybody Wants Some!! </a> <span class="secondary-text"></span> </div> <div class="action"> Opens Wednesday, Mar. 29 </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4824308"></div> <div class="title"> <a href="/title/tt4824308?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> God's Not Dead 2 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0790770"></div> <div class="title"> <a href="/title/tt0790770?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Miles Ahead </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2192016"></div> <div class="title"> <a href="/title/tt2192016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> The Dark Horse </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4191580"></div> <div class="title"> <a href="/title/tt4191580?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Meet the Blacks </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src="http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3626940295._CB298540565_.html?config=%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D"></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TwitterWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','FacebookWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/sxsw-2016/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2443502022&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_hd" > <h3>Get 'Hardcore Henry' Scoop</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/sxsw-2016/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2443502022&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Still of Haley Bennett in Hardcore Henry (2015)" alt="Still of Haley Bennett in Hardcore Henry (2015)" src="http://ia.media-imdb.com/images/M/MV5BMzA5OTQ2MTM2N15BMl5BanBnXkFtZTgwMjA0MTk5NzE@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzA5OTQ2MTM2N15BMl5BanBnXkFtZTgwMjA0MTk5NzE@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/sxsw-2016/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2443502022&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_cap_pri_1" > <i>Hardcore Henry</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Get an in-depth look at <i>Hardcore Henry</i> as director Ilya Naishuller narrates the latest trailer.</p> <p class="seemore"><a href="/imdbpicks/sxsw-2016/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2443502022&pf_rd_r=01ZVWAA19M28HAF6S8YN&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_sm" class="position_bottom supplemental" >Visit the section</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYrfDi3OvT7BSsWvQIu5bENWALeVKF_2SuFEGNW0z8e0JSnW7GaQMhINokStHz1pLWHZpAtb-jU%0D%0AbISc_PCAGvSSe_VLvMQ4nkvx64QLauvVdbINrYoSGmDoU1hyvtabA__eCSYvFWCnvuAqfkQfd2on%0D%0AB-RbnM-HxvCA-YcmjmoWj1CVnzH5GBVUdYpL7ZRUuGf0iZk9dJSPUsTIKQHUsQ1o5yvcSbLRE5tz%0D%0AOhkEfh5jC4U%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYnVZoFrDSwGGutF6g77Kh1YkANCmF0n-KO4GcFJKRpgoJjnZoix5lhhvIRcHDzTV3quQNSnJCV%0D%0AuuoHKN7Ct5bJ4_XoeBIexbO69AoTEKu2ul7HTQZL51rkHRi8TYukLOBxdRh4wWzDs8-B-lPznTWZ%0D%0APyFhpw4n-82-16FvgJN8kM837cC7lk_Eb7pXoGxCasyia0xinXP8Uw_v3q_XVm2OBg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYlMsrjDdUwnGFHFN88nrjEO-EmnxKsMyPW99glDMWp5ZE9xfpMSP6_d-m2jevKSnIINQu1A5KU%0D%0AcqWVtrlHr0KbWAJhtgeyF2H9MI57iaDz6GKJJhdbm1VQqu3R6qDaFXAUz8y_rPZTlPbKVn5wNCnq%0D%0AoyL8XlB024-RaxZANi0R0T9-bNSE_yj8hj4-BWME2CJMMnLLzUT_FUaDnuTQKBS_2mRq0qHFZ031%0D%0AVmiX-TYmTgvFGx1W1EXEcrc7seprCeAXXuoREoT7HCu5UN1lQ1fRWvzd7V2EcQtvJ_0P4RKt5pji%0D%0AXOyESR7gaOwsJPSQR3l_l85HApl6ImsDDWWAFbjsPPk0_yhzNqj3XYNfZ0_35JK8BU8I3nYvYmk7%0D%0AY_6FvLnmTOZBDa_QYvRj5YGg6Z6NoBklJe2Qbkv9iJ8T1p1F9IA%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYnnIYUFweDAYzcLlI6aC3_xY4KDpMBnm3mlZ6H8qQHetN7dfl6bUYqxGrKBuc9HtKf6bijIC4W%0D%0Ao4PUPtV2EDFMJydoEVJRvQwCNuFQ3twcGMZUCKbE7K4GXQtYCiP2leYM6wxMysHqaC8iDAXW4hnh%0D%0AZigXNqoB3oY8KnPLw9Jn-7qq01pdzdvGnaLx7i8tt9FnqyH_m4NpdWuITV1lU5VkeA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYj1jSSp3Xs8bCGoo5yUBkze3j8RS_5MPBCkXm1voOs5JyWA-Ak9s01S23JS0iOsm9_sshGQeMl%0D%0AMwi6ffmnTj0ze8hMSLmY2ETGBGexG74LN5YIhC5pr0HEeSfZussqbVAVktp0wtSJKroT5wm5p_2m%0D%0AtH44gIcXxyvrmC7R9NiUB77V6k_fcbF8hzMyHmMY2fzL%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYrtisGJqgDmYYZ3YH7EdilVg0ZM4CE4kSY7gMASS7ZbypVP5Jkc_ktcfrgNs68mhmfIbrp387h%0D%0AfscONNUuP5NTyyFhnEMBAzVVs6_Wn25GdUdVFOjBPxlS28W8xUimKfxBKgyWhfo2h1dSPMQHspOL%0D%0AfR38kpZox0p0WN-wyfY0GKBoEdvNUPrbxHKNUTnP0CyH%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYnXIymqe8x6p7pDfr7HBT5YPs333zHHFmnT3-Y0LodG1HRfCLJni_SGHEqFOHBRZBU7Jjb6KsA%0D%0AUDiGLBn0SGMNcfRMgF3wRZc24vDArSazMaTtGLfiBCgPtph-y8fAS53iwOiqtWWvxF9jKiBudCxB%0D%0Aad8X2KhUySz5UBCDdqyaAisRpEurdg-emGeKQDAwAXslcSLhnwCDmoB_vpxZ_vEaRA%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYpFGU-PvFKG5_t9AeTpmV3XvviYm-5X_VBddLxUErxL1siDFLtAOL7s841hdBMMECRr6czQbcW%0D%0ASp4VcBWrNL6V8eaDqqr9l-nH2EdCFSDOHZqTI_2Q83TJNepVx9npJcd1gNsi1GTGNGQ15GueJ4uY%0D%0ArUqp7Bf6CNMBnKVRH255bHXTuRFA_bJmOp4p_RkYCs3A%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYm1CBXqJeIZkXK5zkdnkyPtHwntUXob6GCQgqGHZCB8yyQqvpDCwQ9pJauM7-VNNolmtb3e4Te%0D%0AQMmRTmqk_wns1SFqV_BGVipRyQ01iWjnorZ-TTruFQpaLy_fMbmWFb1c4pV0PKO1GB3DkZCUiQWT%0D%0A5cpgbe62vN9W1k-B0UVS9W38oV-qVQJBKfN-yehe0ttkdaH_BvgmwS0BWePdz7sN1gWZEPkDahzV%0D%0AddSCjrey65cAQW_8xI57xPS8YmUF6bir%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYiWllj5uKpVhCgY97RIRhhzAlU_egePwmKIoaR73xL-5ejuigYqJN1dDy9cmxfi0uFU667vVtH%0D%0A7V6JYzlIpkyaWH48unj9tGObMI8zVPUd_tbKEfXQemz3XgVI060iMmGOwLQ7AvrtIl49dEkv8b5r%0D%0AKR400SNyBaWGEgSEkmbF1RlS_F5s6Kdvq9TTsP8VyKJA8KJHiRZc1RxLzV8_jR_C17crlOGnJVOr%0D%0AbQs3xVfUwek%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYoq1OHbHJH0jbztC8X_cVviuuPoGQZntmu-FRRxytr9Ydw_dW1E7tqChOSDed1znRi37XxV1HT%0D%0Au4pFvKhtNxFKVUzzL73PrjVieQeVoIwIqpIgTBbmCeMXu9L2ID3f4sTSJfIll7OiqOxhFdCDlDYI%0D%0A7TJ6QFco3dNM0SwyT9FsXHc0_gQL0KKhn77ENMifiFaHvjKcynnUy9koW_D4YmctWZL9JFP4AudT%0D%0AmaVCvo8fWjs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYg4349bRG4XhWooyn_ZBRQ3Ob9cnpi7NRcakuXmTN8s2hBd4Acn3RCbCuQVz_b8kBOTzM2bT0U%0D%0ASj1zdi2-mDuEK-fG6VQ5g19qnWj6BASUmXx7qiGi35bkeQ2IRQgPzQJwiXeJrdCTOQHRajg1Vkc-%0D%0AVtQSUsQJdZzH2qSm2JDR0VwNQTVUEZPr1oS3c3j-Da6Psw0m0BIyCLnl-LY9UIDGuurh9nU34euI%0D%0APRkMM8f-Xr0%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYmMvF8xhX-SU6kzZ5TSn4-hoTvu-7TD0g0dyuvM_Bd7A03Ilt7ktzYGiCutOw-2UXWfPaGFbrO%0D%0AfjAxoBMY-7kJVNZFObQN6SRreYJnmboQPY49pKciBgkV2PL2w9H2XslBZlfjRM7yJoBlZcPjUGVv%0D%0AhAN7GnYWyjovmdqdPr9QvS9bGV4l2FhYH4m1nS-7hmrMIsQr1kbSQW2ZIhp1u3Mg4PfJ6CjYH8gr%0D%0AC80yuN2FHlE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYj9NlLr6pj5QcbrKVSkqdbXkIlIeyAZxgLnsvSTghRX1QKBB57YayAlio1jCs9BaDkcCsqc7ds%0D%0AkuKGrKFXT8dLXYoAduV53wiEVqa0bcKyCGeWaB7euyaL-JSbDYFPlvWapiB4ktt0x_2CK6L1ANe3%0D%0AHp2i9-0Hp2QJRMe89GEuXcKaY9oHIbsLcHlaHbS6xmxnZg5H5X6yy9IqYAf5NQ9urvpXUyB1u0K2%0D%0AzO8xaN6uIjI%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYsr9BktlRx27FuyzxZaH7W8ktzcEyA0Vcl3UUHnDn-6Q_ImaceiODuTe7sOrtW8M1hJs9REehE%0D%0AH8Ww5YNhNpDd2rMVKbYF87LofNs5arKMkW_i3epKTwYgnUFKf7kXr2bFWM3yBivQ_f7Y2rQRPwtB%0D%0AKuj0CEBGYhUoZ9Bl8D0fOXAXhDgsfCWhxpa4yZBtZqCa7VULkv3HVQbubYhzb-3eyxZs6emPW-DC%0D%0AeoDOALZmYUdLxjfr9wUZbei9yVPcgyTe%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYm7AXIMmh64K9l2rJxKl43PKjcQ9VC2BQ815ICqx9VYCy_kaCFI8CkUhmQNM6IPp-GO4U3U-ko%0D%0AZNxeyurYdcreoqaVmkfug9lzGLMG4L7NYFg6YlvZKiHVUSvXIByM8omM_PIiXhWpcVUzeU0JrIMf%0D%0AxUpnxrrvWIyju_RWqJQgoTE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYmINkGv2Lez6hUNLIUHA_wcCS4O7EMVjsPz-4SuInPR2kb_vQYDRr5i3yr235NvCD-1hTxceKX%0D%0A99FaIoEIMmmcdgOazocmC03T9xNyeD3cwZsi7wT4tO2PlIRULrHFZrzBbolb3TrL-080F0JbRYhD%0D%0AEMDo08DAfYka3M4smg5oSmU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-2997851314._CB293837866_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-816966180._CB298601395_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101b0eaf4bba886f613720fc7dbfb4f562b6f6acfd4882b8258e855ee83bcb10c56",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=585833729892"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-458185442._CB294884543_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=585833729892&ord=585833729892";
    },"unable to request AAN pixel");
</script>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="499"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
